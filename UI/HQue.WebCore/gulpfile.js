/// <binding AfterBuild='clean, min' />
/// <reference path="appscript/routescript.js" />
/// <binding Clean='clean' />

/// <binding AfterBuild='clean, min' />

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    project = require("./project.json");

var ts = require('gulp-typescript');
var clean = require('gulp-clean');

var paths = {
    webroot: "./" + project.webroot + "/"
};

paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";
paths.appScript = paths.webroot + "appScript";

gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);

});

gulp.task("clean", ["clean:js", "clean:css", "clean:script"]);

gulp.task("min:js", function () {
    gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
    gulp.src([paths.css, "!" + paths.minCss])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("clean:script", function (cb) {
    rimraf(paths.appScript + "/", cb);

});

//gulp.task('watch.a1', ['min'], function () {
//    return gulp.watch('AppScript/stockTransfer/*', ['min']);
//});

gulp.task("script", function () {
    gulp.src(['AppScript/**/*']).pipe(gulp.dest(paths.appScript));
});

gulp.task("model", function () {
    gulp.src(['AppScript/model/**/*.js']).pipe(concat(paths.appScript + "/model.js")).pipe(gulp.dest("."));
});

gulp.task("sales", function () {
    gulp.src(['AppScript/sales/salesappscript.js','AppScript/routeScript.js']).pipe(concat(paths.appScript + "/sales.js")).pipe(gulp.dest("."));
});

gulp.task("min", ["script", "model", "min:js", "min:css"]);





var destPath = './wwwroot/libs/';

// Delete the dist directory
gulp.task('clean:ts', function () {
    return gulp.src(destPath)
        .pipe(clean());
});

gulp.task("scriptsNStyles", () => {
    gulp.src([
            'core-js/client/**',
            'systemjs/dist/system.src.js',
            'reflect-metadata/**',
            'rxjs/**',
            'zone.js/dist/**',
            '@angular/**',
            'jquery/dist/jquery.*js',
            'bootstrap/dist/js/bootstrap.*js',
            '@angular2-material/**/*',
            'ng2-bootstrap/**',
            'moment/**'
    ], {
        cwd: "node_modules/**"
    })
        .pipe(gulp.dest("./wwwroot/libs"));
});

var tsProject = ts.createProject('scripts/tsconfig.json');
gulp.task('ts', function (done) {
    //var tsResult = tsProject.src()
    var tsResult = gulp.src([
            "scripts/*.ts",
            "scripts/model/*.ts",
            "scripts/stock-transfer/*.ts",
    ])
        .pipe(ts(tsProject), undefined, ts.reporter.fullReporter());
    return tsResult.js.pipe(gulp.dest('./wwwroot/appScripts'));
});

gulp.task('watch', ['watch.ts']);

gulp.task('watch.ts', ['ts'], function () {
    return gulp.watch('scripts/**/*', ['ts']);
});

gulp.task('default', ['scriptsNStyles', 'watch']); 
 