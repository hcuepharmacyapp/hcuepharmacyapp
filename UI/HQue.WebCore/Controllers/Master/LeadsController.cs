﻿using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Master
{
    [Route("[controller]")]
    public class LeadsController : BaseController
    {
        public LeadsController(ConfigHelper configHelper) : base(configHelper)
        {
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult AcceptedLeadsList()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult RejectedLeadsList()
        {
            return View();
        }
    }
}
