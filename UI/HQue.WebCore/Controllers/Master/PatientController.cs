﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Master;
using HQue.Biz.Core.Master;
using Utilities.Helpers;
namespace HQue.WebCore.Controllers.Master
{
    [Route("[controller]")]
    public class PatientController : BaseController
    {
        private readonly PatientManager _patientManager;
        private readonly ConfigHelper _configHelper;
        public PatientController(PatientManager patientManager, ConfigHelper configHelper) : base(configHelper)
        {
            _patientManager = patientManager;
            _configHelper = configHelper;
        }
        [Route("[action]")]
        public IActionResult Search()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult Detail()
        {
            return View();
        }
        
        //saving a customer code Newly Added by DURGA on 18-04-2017 
        // Changed by Violet 06-06-2017 Start
        [HttpPost]
        [Route("[action]")]
        public async Task<Patient> PatientCreate([FromBody]Patient model)
        {
                model.SetLoggedUserDetails(User);
                return await _patientManager.PatientSave(model);
        }
        
        [HttpPost]
        [Route("[action]")]
        public async Task<Patient> PatientUpdate([FromBody]Patient model)
        {
            model.SetLoggedUserDetails(User);
            return await _patientManager.PatientUpdate(model);
        }
        // End

        [HttpPost]
        [Route("[action]")]
        public Task<IEnumerable<Patient>> List([FromBody]Patient model, int department)
        {
            //if (_configHelper.AppConfig.OfflineMode == true || (!string.IsNullOrEmpty(model.EmpID)))
            //{
                model.SetLoggedUserDetails(User);
                return _patientManager.PatientOfflineList(model, User.AccountId(), User.InstanceId(), department);
            //}
            //else
            //{
            //    if (model.patientSearchType == 1)
            //    {
            //        model.SetLoggedUserDetails(User);
            //        return _patientManager.PatientOfflineList(model, User.AccountId(), User.InstanceId(), department);
            //    }
            //    else
            //     return _patientManager.PatientList(model,User.AccountId(),User.InstanceId());
            //}
        }
        [HttpPost]
        [Route("[action]")]
        public Task<IEnumerable<Patient>> LocalList([FromBody]Patient model,int department)
        {
            //if (_configHelper.AppConfig.OfflineMode == true || (!string.IsNullOrEmpty(model.EmpID)))
            //{
            //}
            //else
            //{
            //    if (model.patientSearchType == 1)
            //    {
            //        model.SetLoggedUserDetails(User);
            //        return _patientManager.PatientOfflineList(model, User.AccountId(), User.InstanceId());
            //    }
            //    else
            //        return _patientManager.PatientList(model, User.AccountId(), User.InstanceId());
            //}
            model.SetLoggedUserDetails(User);
            return _patientManager.PatientOfflineList(model, User.AccountId(), User.InstanceId(), department);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<decimal?> GetCustomerDiscount([FromBody]Patient customer)
        {
            customer.AccountId = User.AccountId();
            return await _patientManager.GetCustomerDiscount(customer);
        }
        [HttpGet]
        [Route("[action]")]
        public Task<IEnumerable<Patient>> GetPatientName(string patientName,int department, int isActive)
        {
           // department = 1;
            return _patientManager.PatientSearchListLocal(patientName, User.AccountId(), User.InstanceId(), department, isActive);
        }
        // Newly Added by Violet for Sales history Patientlist on 09/06/2017
        [HttpGet]
        [Route("[action]")]
        public Task<IEnumerable<Patient>> GetSalesPatientName(string patientName, int department)
        {
            // department = 1;
            return _patientManager.PatientSearchSalesListLocal(patientName, User.AccountId(), User.InstanceId(), department);
        }


        [HttpGet]
        [Route("[action]")]
        public Task<IEnumerable<Patient>> GetPatientMobile(string patientMobile, int department)
        {
            // department = 1;
            return _patientManager.PatientSearchMobile(patientMobile, User.AccountId(), User.InstanceId(), department);
        }
        [Route("[action]")]
        public Task<IEnumerable<Patient>> GetPatientId(string patientId)
        {
            return _patientManager.PatientSearchListLocalId(patientId, User.AccountId(), User.InstanceId());
        }
        //[Route("[action]")]
        //public Task<IEnumerable<Patient>> GetPatientId(string patientId, string instanceid)
        //{
        //    return _patientManager.PatientSearchListLocalId(patientId, User.AccountId(), instanceid);//User.InstanceId()
        //}
        [Route("[action]")]
        public Task<IEnumerable<Patient>> Patientlist(string patientName, int department)
        {
            // Removed Global patient list
            //if (_configHelper.AppConfig.OfflineMode == true)
            //{
            return _patientManager.PatientSearchListLocal(patientName, User.AccountId(), User.InstanceId(), department, 0);
            //}
            //else
            //{
            //      return _patientManager.PatientSearchList(patientName);
            //}
        }
        
        [Route("[action]")]
        public Task<IEnumerable<Patient>> GetShortPatientName(string patientName)
        {
            return _patientManager.PatientSearchShortListLocal(patientName, User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public Task<IEnumerable<Patient>> PatientShortlist(string patientName)
        {
            if (_configHelper.AppConfig.OfflineMode == true)
            {
                return _patientManager.PatientSearchShortListLocal(patientName, User.AccountId(), User.InstanceId());
            }
            else
            {
                return _patientManager.PatientSearchList(patientName);
            }
        }

        [Route("[action]")]
        public async Task<bool> CustPaymentStatus(string CustName, string CustMobile)
        {
            var result = await _patientManager.CustPaymentStatus(User.AccountId(), User.InstanceId(), CustName, CustMobile);

            return result;
        }
    }
}
