﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure;
using Utilities.Helpers;
using HQue.Biz.Core.Master;
using HQue.Contract.Infrastructure.Misc;
using System;
using HQue.Contract.Infrastructure.Inventory;
using System.IO;
using HQue.Contract.Base;

namespace HQue.WebCore.Controllers.Master
{
    [Route("[controller]")]
    public class ProductController : BaseController
    {
        private readonly ProductManager _productManager;

        public ProductController(ProductManager productManager, ConfigHelper configHelper) : base(configHelper)
        {
            _productManager = productManager;
        }

        [Route("[action]")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<Product> Create([FromBody]Product model)
        {
            //Remove below line after db change
            //model.Code = "PRO000";
            model.SetLoggedUserDetails(User);
            return await _productManager.Save(model);
        }

        [Route("[action]")]
        public Task<List<Product>> InstanceProduct(string productName = null)
        {
            Product model = new Product();
            model.SetLoggedUserDetails(User);
            return _productManager.InstanceList(productName,model.InstanceId);
 		}

        /// <summary>
        /// //Added for Reports, InstanceId passed from JS
        /// </summary>
        [Route("[action]")]
        public Task<List<Product>> InstanceProductList(string instanceid, string productName = null) 
        {
            //Product model = new Product();
            //model.SetLoggedUserDetails(User);
            return _productManager.InstanceList(productName, instanceid); //model.InstanceId
        }
        //alternate product search newly added by nandhini
        [HttpGet]
        [Route("[action]")]
        public Task<Product> getGenericDetails(string genericName)
        {
            var product = new Product
            {
                GenericName = genericName
            };
            product.SetLoggedUserDetails(User);
            return _productManager.getGenericDetails(product);
        }

        //alternate product search newly added by nandhini
        //Return Purchase Product
        [Route("[action]")]
        public Task<List<Product>> ReturnProduct(string productName = null)
        {
            Product model = new Product();
            model.SetLoggedUserDetails(User);
            return _productManager.ReturnPurchaseList(productName, model.InstanceId);
        }
        //added by nandhini for sales return list product search
        [Route("[action]")]
        public Task<List<Product>> SalesReturnProduct(string productName = null)
        {
            Product model = new Product();
            model.SetLoggedUserDetails(User);
            return _productManager.SalesReturnProductList(productName, model.InstanceId);
        }
        //Sales History Product
        [Route("[action]")]
        public Task<List<Product>> SalesProduct(string productName = null)
        {
            Product model = new Product();
            model.SetLoggedUserDetails(User);
            return _productManager.SalesHistoryList(productName, model.InstanceId);
        }
        [Route("[action]")]
        public async Task<List<MasterProduct>> ProductList(string productName = null)
        {
            //model.SetLoggedUserDetails(User);
            return  await _productManager.List(productName,User.AccountId(),User.InstanceId());
        }
        //Added by bala for Get Account wise product list on 1/June/2017
        [Route("[action]")]
        public async Task<List<MasterProduct>> AccountWiseProductList(string productName = null)
        {
            return await _productManager.AcountWiseProductList(productName, User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public async Task<List<MasterProduct>> NonHiddenProductList(string productName = null)
        {
            return await _productManager.NonHiddenProductList(productName, User.AccountId(), User.InstanceId());
        }

        // newly added by Violet Raj 16-03-17 for stock ledger report
        [Route("[action]")]
        public async Task<List<MasterProduct>> StockProductList(string productName = null)
        {
            //model.SetLoggedUserDetails(User);
            return await _productManager.StockProductList(productName, User.AccountId(), User.InstanceId());//User.InstanceId()
        }

        // Newly Added Gavaskar 22-02-2017 Start
        [Route("[action]")]
        public async Task<List<MasterProduct>> InventoryUpdateProductList(string productName = null)
        {
            return await _productManager.InventoryUpdateProductList(productName, User.AccountId(), User.InstanceId());
        }
        // Newly Added Gavaskar 22-02-2017 End

        [HttpGet]
        [Route("[action]")]
        public Task<List<MasterProduct>> LocalProductList(string productName = null)
        {            
            return _productManager.LocalProductList(productName, User.AccountId(), User.InstanceId());
        }
		
		[HttpGet]
        [Route("[action]")]
        public Task<List<ProductMaster>> getProducts(string productName = null)
        {
            return _productManager.GetProductMaster(productName);
        }
        //By San
        [Route("[action]")]
        public Task<List<Product>> productNameList(string productName = null)
        {
            return _productManager.productNameList(productName);
        }

        [Route("[action]")]
        public Task<List<Product>> GenericList(string genericName = null)
        {
            return _productManager.GenericList(genericName);
        }

        [Route("[action]")]
        public Task<List<ProductStock>> GenericSearchList(string genericName = null)
        {
            return _productManager.GenericSearchList(genericName);
        }

        [Route("[action]")]
        public Task<List<Product>> ManufacturerList(string manufacturercName = null)
        {
            return _productManager.ManufacturerList(manufacturercName);
        }

        [Route("[action]")]
        public Task<List<Product>> ScheduledCategoryList(string scheduledCatName = null)
        {
            return _productManager.ScheduledCategoryList(scheduledCatName);
        }

        [Route("[action]")]
        public Task<List<Product>> TypeList(string typeName = null)
        {
            return _productManager.TypeList(typeName);
        }

        [Route("[action]")]
        public Task<List<Product>> CategoryList(string categoryName = null)
        {
            return _productManager.CategoryList(categoryName);
        }
        [Route("[action]")]
        public Task<List<BaseDomainValues>> SubCategoryList(string categoryName = null)
        {
            return _productManager.SubCategoryList(categoryName);
        }

      
        [Route("[action]")]
        public Task<List<Product>> CommodityCodeList(string commodityName = null)
        {
            return _productManager.CommodityCodeList(commodityName);
        }

        [Route("[action]")]
        public IActionResult List()
        {
            if (User.AccountId() != "18204879-99ff-4efd-b076-f85b4a0da0a3")
            {
               Response.Redirect("/Dashboard");
            }
            return View();
        }
        [Route("[action]")]
        public IActionResult ProductsList()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult ProductListing()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Product> UpdateProduct([FromBody]Product model)
        {            
            model.SetLoggedUserDetails(User);
            model.InstanceId = User.InstanceId(); //For offline Added by Poongodi on 05/06/2017
            return await _productManager.UpdateProductData(model);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Product>> ListData([FromBody]Product model)
        {
            model.SetLoggedUserDetails(User);
            return await _productManager.ListPager(model);
        }
                
        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Product>> MasterListData([FromBody]Product model)
        {
            model.SetLoggedUserDetails(User);
            return await _productManager.MasterListPager(User.AccountId(), User.InstanceId(), model);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Product>> EntireMasterListData([FromBody]Product model)
        {
            model.SetLoggedUserDetails(User);
            return await _productManager.EntireMasterListPager(User.AccountId(), User.InstanceId(), model);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<IEnumerable<Product>> ListProduct([FromBody]Product model)
        {
            model.SetLoggedUserDetails(User);
            return await _productManager.ListProductPager(model);
        }
 		[Route("[action]")]
        public async Task<PagerContract<Product>> listUpdate([FromBody]DateRange data)
        {
            return await _productManager.ListingPager(User.InstanceId(), data.FromDate, data.ToDate);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Product>> Export([FromBody]Product model)
        {
            model.SetLoggedUserDetails(User);
            return await _productManager.Export(model);
        }
        [Route("[action]")]
        public IActionResult HcueProductMaster()
        {
            if (User.AccountId() != "756efa5e-66e1-46ff-a3b2-3f282d746d93")
            {
                Response.Redirect("/Dashboard");
            }
           
            return View();
        }
        
        [HttpPost]
        [Route("[action]")]
        public async Task<IEnumerable<ProductMaster>> HcueProductMasterList([FromBody]ProductMaster model)
        {
            model.SetLoggedUserDetails(User);
            return await _productManager.HcueProductMasterListPager(model);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ProductMaster> UpdateHcueProductMaster([FromBody]ProductMaster data)
        {
            data.SetLoggedUserDetails(User);
            return await _productManager.UpdateHcueProductMaster(data);
        }

        //Product Multi Edit
        [Route("[action]")]
        public IActionResult MultiEdit(string noGst)
        {
            ViewBag.NoGst = noGst;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IEnumerable<ProductMaster>> MultiProductList([FromBody]ProductMaster model)
        {
            model.SetLoggedUserDetails(User);
            return await _productManager.HcueProductMasterListPager(model);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ProductMaster> UpdateMultiProduct([FromBody]ProductMaster data)
        {
            data.SetLoggedUserDetails(User);
            return await _productManager.UpdateHcueProductMaster(data);
        }

        [Route("[action]")]
        public Task<List<ProductMaster>> GetProductMasterGeneric(string genericName = null)
        {
            return _productManager.GetProductMasterGeneric(genericName);
        }

        [Route("[action]")]
        public Task<ProductMaster> ProductMasterUpdateCount()
        {
            return _productManager.ProductMasterUpdateCount();
        }

        [HttpPost]
        [Route("[action]")]
        public Task<bool> ReorderMinMaxBulkUpdate(DateTime fromDate, DateTime toDate, int minReorderFactor, int maxReorderFactor)
        {
            ProductInstance data = new ProductInstance();
            data.SetLoggedUserDetails(User);
            return _productManager.ReorderMinMaxBulkUpdate(data, fromDate, toDate, minReorderFactor, maxReorderFactor);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Product>> BulkUpdate([FromBody]List<Product> model)
        {
            Product userData = new Product();
            userData.SetLoggedUserDetails(User);
            await _productManager.BulkProductUpdate(model,userData);
            return model;
        }


        [Route("[action]")]
        public async Task<ActionResult> ExportProductMaster()
        {            
            Product userData = new Product();
            userData.SetLoggedUserDetails(User);
            var dataFile = await _productManager.ExportProductMaster(userData.AccountId);
            FileInfo info = new FileInfo(dataFile);
            HttpContext.Response.ContentType = "text/csv";
            return File(info.OpenRead(), "text/csv", info.Name);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Product>> GetOnlineProductCodes([FromBody]List<Product> data)
        {
            if (data != null)
            {
                data[0].AccountId = User.AccountId();
            }
            return await _productManager.GetOnlineProductCodes(data);
        }
    }
}
