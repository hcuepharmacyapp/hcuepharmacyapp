﻿using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Master
{
    [Route("[controller]")]
    public class VendorController : BaseController
    {
        private readonly ConfigHelper _configHelper;
        public VendorController(ConfigHelper configHelper) : base(configHelper)
        {
            _configHelper = configHelper;
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult Index(string id = null)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Vendor/list");
            }
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.id = id;
            }
            return View();
        }

        [Route("[action]")]
        public IActionResult Edit(string id = null)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Vendor/list");
            }
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.id = id;
            }
            return View();
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        // Added by Gavaskar VendorMulti Edit page load 14-08-2017
        [Route("[action]")]
        public IActionResult VendorMultiEdit()
        {
            return View();
        }
    }
}
