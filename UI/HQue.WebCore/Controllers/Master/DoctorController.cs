﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Master;
using Utilities.Helpers;
using HQue.Biz.Core.Master;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Report;
using HQue.Contract.Infrastructure;

namespace HQue.WebCore.Controllers.Master
{
    [Route("[controller]")]
    public class DoctorController : BaseController
    {
        private readonly DoctorManager _doctorManager;
        private readonly ReportManager _reportManager;

        public DoctorController(DoctorManager doctorManager, ConfigHelper configHelper, ReportManager reportManager) : base(configHelper)
        {
            _doctorManager = doctorManager;
            _reportManager = reportManager;
        }

        //[HttpPost]
        [Route("[action]")]
        public Task<List<Doctor>> DoctorList(string doctorName = null)
        {
            return _doctorManager.DoctorList(doctorName);
        }

        [Route("[action]")]
        public Task<List<Doctor>> LocalDoctorList(string doctorName = null)
        {
            return _doctorManager.LocalDoctorList(doctorName,User.AccountId(),User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public Task<List<Doctor>> DoctorListProfile(string doctorName = null)
        {
            return _doctorManager.DoctorListProfile(doctorName, User.AccountId());
        }

        [Route("[action]")]
        public IActionResult Doctor()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Doctor>> GetDoctorList([FromBody]Doctor doctor)
        {
            doctor.SetLoggedUserDetails(User);
            return await _doctorManager.GetDoctorList(doctor);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Doctor> CreateDoctor([FromBody]Doctor doctor)
        {
            doctor.SetLoggedUserDetails(User);
            return await _doctorManager.CreateDoctor(doctor);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<Doctor> GetDoctorById(string id)
        {
            return await _doctorManager.GetDoctorById(id);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Doctor> UpdateDoctor([FromBody]Doctor doctor1)
        {
            doctor1.SetLoggedUserDetails(User);
            return await _doctorManager.UpdateDoctor(doctor1);
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GetDoctorSalesList(string DoctorId)
        {            
            ViewBag.DoctorId = DoctorId;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> GetDoctorSales(string DoctorId)
        {
            return await _reportManager.GetDoctorSalesList(User.AccountId(), DoctorId);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Doctor> SaveDefaultDoctor([FromBody]Doctor doc1)
        {
            doc1.SetLoggedUserDetails(User);
            return await _doctorManager.SaveDefaultDoctor(doc1);
        }

        [Route("[action]")]
        public async Task<Doctor> GetDefaultDoctor()
        {
            return await _doctorManager.GetDefaultDoctor(User.AccountId(), User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Doctor> ResetDefaultDoctor([FromBody]Doctor doc)
        {
            doc.SetLoggedUserDetails(User);
            return await _doctorManager.ResetDefaultDoctor(doc);
        }

        [Route("[action]")]
        public Task<List<Doctor>> LocalShortDoctorList(string ShortDoctorName = null)
        {
            return _doctorManager.LocalShortDoctorList(ShortDoctorName, User.AccountId(), User.InstanceId());
        }

    }
}
