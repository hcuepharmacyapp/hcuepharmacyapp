﻿using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Settings;
using HQue.Biz.Core.Inventory;
using System.Threading.Tasks;
namespace HQue.WebCore.Controllers.Inventory
{
    [Route("[controller]")]
    public class StockTransferController : BaseController
    {
        private readonly ConfigHelper _configHelper;
        private readonly StockTransferManager _stockManager;
        public StockTransferController(StockTransferManager stockManager, ConfigHelper configHelper) : base(configHelper)
        {
            _stockManager = stockManager;
            _configHelper = configHelper;
        }
        [Route("[action]")]
        public IActionResult Index()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }
        [Route("[action]")]
        public IActionResult List()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }
        [Route("[action]")]
        public IActionResult Accept()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }
        [Route("[action]")]
        public IActionResult AcceptList()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult StockTransfer()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }
        [Route("[action]")]
        public IActionResult StockTransferProduct()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult StockTransferProductItem()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult TransferSettings()
        {
            return View();
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<TransferSettings> GetTransferSetting()
        {
            TransferSettings ts = new TransferSettings();
            ts.SetLoggedUserDetails(User);
            return await _stockManager.GetTransferSetting(ts);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<TransferSettings> UpdateTransferSetting([FromBody]TransferSettings ts)
        {
            ts.SetLoggedUserDetails(User);
            return await _stockManager.UpdateTransferSetting(ts);
        }

        //Added by Settu for indent transfer with stock reducing
        [Route("[action]")]
        public IActionResult ActiveIndentList()
        {
            return View();
        }      
    }
}
