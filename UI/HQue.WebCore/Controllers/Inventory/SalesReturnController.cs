﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using HQue.Contract.Infrastructure;
using HQue.Biz.Core.Inventory;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace HQue.WebCore.Controllers.Inventory
{
    [Route("[controller]")]
    public class SalesReturnController : BaseController
    {
        private readonly SalesReturnManager _salesReturnManager;
        private Response _respsonse;
        private readonly ConfigHelper _configHelper;
        private readonly SalesManager _salesManager;
        private readonly SalesSaveManager _salesSaveManager;

        public SalesReturnController(SalesReturnManager salesReturnManager, ConfigHelper configHelper, Response response, SalesManager salesManager, SalesSaveManager salesSaveManager) : base(configHelper)
        {
            _salesReturnManager = salesReturnManager;
            _respsonse= response;
            _configHelper = configHelper;
            _salesManager = salesManager;
            _salesSaveManager = salesSaveManager;
        }

        [Route("[action]")]
        public IActionResult Index(string id)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Sales/list");
            }
            var salesId = id;
            ViewBag.Id = salesId;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Response> Index([FromBody]SalesReturn model)
        {
            try
            {
                model.SetLoggedUserDetails(User);
                /*Prefix Added by Poongodi on 14/06/2017*/
                if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true" &&
                    _configHelper.AppConfig.OfflineMode == false)
                {

                    model.Prefix = "O";
                  
                }
                //GST section Added by Poongodi on 01/07/2017
                if (model.TaxRefNo == 1)
                {
                    foreach (var item in model.SalesReturnItem)
                    {
                        item.GstTotal = item.ProductStock.GstTotal;
                        item.Cgst = item.GstTotal / 2;
                        item.Sgst = item.GstTotal / 2;
                        item.Igst = item.GstTotal;
                        item.VAT = null;

                    }
                }
                else
                {
                    foreach (var item in model.SalesReturnItem)
                    {
                        item.GstTotal = null;

                    }
                }
                //by San 22-11-2017
                //model.ReturnTotal = model.ReturnTotal >= 0 ? model.ReturnTotal : 0;
                //if (model.isEnableRoundOff == true)
                //{
                //    model.NetAmount = Math.Round((decimal)model.ReturnTotal, 0, MidpointRounding.AwayFromZero);
                //    model.RoundOffNetAmount = model.NetAmount - model.ReturnTotal;
                //}
                //else
                //{
                //    model.NetAmount = model.ReturnTotal;
                //    model.RoundOffNetAmount = 0;
                //}

                await _salesReturnManager.Save(model,User.AccountId(),User.InstanceId());

                _respsonse.errorMessage = "ok";
                _respsonse.errorStatus = 0;

                //Added by Sarubala on 03-10-17 to send sms for Mediplus on Cancel
                var isCancelEditSms = await _salesManager.getSettingsIsCancelEditSms(User.AccountId(), User.InstanceId());
                
                bool internet = NetworkInterface.GetIsNetworkAvailable();

                if (internet == true)
                {
                    if (isCancelEditSms == true && model.CancelType == 1)
                    {
                        //Added by Sarubala on 13-11-17
                        foreach(var item in model.SalesReturnItem)
                        {
                            decimal? discount = (model.Discount > 0 ? model.Discount : 0) + (item.Discount > 0 ? item.Discount : 0);
                            item.BillValue = (item.Total - ((discount / 100) * item.Total));
                        }                        
                        
                        Sales sale = await _salesManager.GetSalesDetails(model.SalesId);
                        sale.FinalValue = Math.Round(decimal.Parse(String.Format("{0:0.0#}", model.CancelBillValue)), MidpointRounding.AwayFromZero); //Added by Sarubala on 13-11-17
                        sale.SalesReturn.Id = model.Id; //Added by Sarubala on 22-11-17
                        
                        string mode = "Cancel";
                        await _salesSaveManager.SendSmsOnEdit(sale, mode);
                    }
                }                    

            }
            catch (Exception ex)
            {
                _respsonse.errorMessage = ex.Message;
                _respsonse.errorStatus = 1;
            }
            return _respsonse;
        }

        [Route("[action]")]
        public async Task<SalesReturn> SalesReturnDetail(string id)
        {
            return await _salesReturnManager.GetSalesReturnDetails(id);
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }


        [Route("[action]")]
        public IActionResult NewList()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult CancelList()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult NewCancelList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<SalesReturn>> ListData([FromBody]SalesReturn model)
        {
            model.SetLoggedUserDetails(User);
            return await _salesReturnManager.ListPager(model);
        }

        [Route("[action]")]
        public IActionResult EditReturnOnly(string id)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/SalesReturn/List");
            }
            ViewBag.ReturnId = id;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<SalesReturn> GetReturnOnlyDetail(string id)
        {
            return await _salesReturnManager.GetReturnOnlyDetails(id);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<SalesReturn> updateReturn([FromBody]SalesReturn sr)
        {
            sr.SetLoggedUserDetails(User);
            sr.IsUpdateNetAmt = true;
            return await _salesReturnManager.UpdateSalesReturn(sr);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<List<SalesReturnItem>> GetReturnItemAuditList(string id)
        {
            return await _salesReturnManager.GetReturnItemAuditList(id);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<string> GetSalesReturnIdByRetrunNo(int SeriesType, string SeriesTypeValue, string InvNo)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            string baseUrl = HttpContext.Request.Host.ToString();
            string id = await _salesReturnManager.GetSalesReturnIdByRetrunNo(SeriesType, SeriesTypeValue, InvNo, User.AccountId(), User.InstanceId());

            return id;
        }


    }
}
