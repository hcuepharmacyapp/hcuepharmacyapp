﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using HQue.Contract.Infrastructure;
using HQue.Biz.Core.Inventory;
using System.Collections.Generic;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.Misc;
using System.Globalization;
using System.Net.NetworkInformation;
using HQue.Biz.Core.Accounts;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.Helpers;
using HQue.Contract.Infrastructure.Master;
using System.Linq;
using HQue.Contract.Base;



namespace HQue.WebCore.Controllers.Inventory
{
    [Route("[controller]")]
    public class SalesController : BaseController
    {
        private readonly IHostingEnvironment _environment;
        private readonly SalesManager _salesManager;
        private readonly ConfigHelper _configHelper;
        private readonly SalesReturnManager _salesReturnManager;
        private readonly SalesSaveManager _salesSaveManager;
        private readonly PaymentManager _paymentManager;
        private readonly ProductStockManager _productStockManager;
        public SalesController(SalesManager salesManager, IHostingEnvironment environment, ConfigHelper configHelper, SalesReturnManager salesReturnManager, SalesSaveManager salesSaveManager, PaymentManager paymentManager, ProductStockManager productStockManager) : base(configHelper)
        {
            _environment = environment;
            _salesManager = salesManager;
            _configHelper = configHelper;
            _salesReturnManager = salesReturnManager;
            _salesSaveManager = salesSaveManager;
            _paymentManager = paymentManager;
            _productStockManager = productStockManager;
        }
        [Route("[action]")]
        public IActionResult Index(string id)
        {           

            ViewBag.CompleteSaleType = _salesManager.getCompleteSaleType(User.AccountId(), User.InstanceId());
            ViewBag.Id = id;
            // ViewBag.IsSalesTemplateEnabled =  _productStockManager.GetFeatureAccess(2);                  

            return View();
        }

        [Route("[action]")]
        public IActionResult NewIndex(string id)
        {
            ViewBag.CompleteSaleType = _salesManager.getCompleteSaleType(User.AccountId(), User.InstanceId());
            ViewBag.Id = id;

            return View();
        }


        [Route("[action]")]
        public IActionResult QuickSale()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        //public async Task<Sales> Index([FromForm]Sales sales, DateTime InvoiceDate, IFormFile file)
        public async Task<Sales> Index([FromBody]Sales sales) // Modified for 30 items not getting called issue
        {
            sales.InitializeExecutionQuery();
            var ReturnId = sales.SalesReturn.Id;
            var CreatedBy = sales.CreatedBy;
            DateTime InvoiceDate = Convert.ToDateTime(sales.InvoiceDate);
            sales.SetLoggedUserDetails(User);
            sales = _salesManager.RemoveReturnItems(sales);
            sales.SalesReturn.SetLoggedUserDetails(User);

            if (sales.isEnableSalesUser == true)
            {
                if (string.IsNullOrEmpty(sales.Id))
                {
                    sales.CreatedBy = sales.SalesReturn.CreatedBy = CreatedBy;
                    sales.UpdatedBy = sales.SalesReturn.UpdatedBy = CreatedBy;
                }
                else
                {
                    sales.UpdatedBy = sales.SalesReturn.UpdatedBy = CreatedBy;
                }
            }

            bool OfflineStatus = _configHelper.AppConfig.OfflineMode;
            sales.OfflineStatus = OfflineStatus;
            Sales result = new Sales();
            /*Prefix Added by Poongodi on 14/06/2017*/

            sales.SalesEditMode = !sales.IsNew; //Added by Sarubala on 03-10-17 to send sms for mediplus
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true" && _configHelper.AppConfig.OfflineMode == false)
            {
                //Added if condition for Edit by Manivannan on 19-Jul-2017
                if (string.IsNullOrEmpty(sales.Id))
                {
                    sales.Prefix = "O";
                    sales.SalesReturn.Prefix = "O";
                }
            }

            if (sales.SalesItem.Count > 0)
            {
                sales.GSTEnabled = User.GSTEnabled() == "True";
                result = await _salesManager.Save(sales, InvoiceDate, User.InstanceId(), false);
                //Save bulk data - starts
                if (sales.WriteExecutionQuery && result.isEmpID == false)
                {
                    await _salesManager.SaveBulkData(sales);
                    if (sales.SaveFailed)
                    {
                        return sales;
                    }
                }
                //Save bulk data - ends
                sales.SalesReturn.SalesId = result.Id;
                await _salesReturnManager.HandleReturnedItems(sales.SalesReturn, result.Id);
            }
            else
            {
                sales.SalesReturn.IsAlongWithSale = false;
            }

            // By San -- 30-05-2017
            if (sales.SalesReturn.SalesReturnItem.Count > 0)
            {

                /*Added by Poongodi on 30/06/2017*/
                sales.SalesReturn.TaxRefNo = User.GSTEnabled() == "True" ? 1 : 0;
                sales.SalesReturn.GSTEnabled = User.GSTEnabled() == "True";
                /*The below sectionadded by Poongodi on 30/06/2017*/
                if (sales.SalesReturn.TaxRefNo == 1)
                {

                    foreach (var item in sales.SalesReturn.SalesReturnItem)
                    {
                        item.GstTotal = item.GstTotal;
                        item.Cgst = item.GstTotal / 2;
                        item.Sgst = item.GstTotal / 2;
                        item.Igst = item.GstTotal;
                        item.VAT = null;
                    }
                }
                else
                {

                    foreach (var item in sales.SalesReturn.SalesReturnItem)
                    {
                        item.GstTotal = null;
                    }
                }

                //Added by Settu to capture credit sales return bills in voucher
                sales.SalesReturn.InvoiceSeries = sales.InvoiceSeries;
                if (sales.PaymentType == "Credit")
                {
                    sales.SalesReturn.Voucher.ParticipantId = sales.SalesReturn.PatientId;
                    sales.SalesReturn.Voucher.TransactionType = (int)HelperDataAccess.TransactionType.SalesReturn;
                    sales.SalesReturn.Voucher.VoucherType = (int)HelperDataAccess.VoucherType.DebitNote;
                    sales.SalesReturn.Voucher.Factor = (int)HelperDataAccess.VoucherFactor.SalesReturn;
                    if (sales.NetAmount < 0)
                    {
                        sales.SalesReturn.Voucher.OriginalAmount = Math.Abs(Convert.ToDecimal(sales.NetAmount));
                        sales.SalesReturn.Voucher.Amount = sales.SalesReturn.Voucher.OriginalAmount;
                    }
                }
                if (sales.SalesReturn.SalesId != null)
                {
                    if (string.IsNullOrEmpty(sales.SalesReturn.Id))
                    {
                        sales.SalesReturn.LoyaltyId = sales.LoyaltyId;
                    }
                    result.SalesReturn = await _salesReturnManager.Save(sales.SalesReturn, sales.SalesReturn.AccountId, sales.SalesReturn.InstanceId);
                    sales.SalesReturn.SalesReturnItem = await _salesSaveManager.GetProductForReturn(sales);
                }
                else
                {
                    result.SalesReturn = await _salesReturnManager.SaveReturnInSale(sales.SalesReturn);
                }
                result.SalesReturn.Voucher = await _salesReturnManager.saveVoucherNote(result.SalesReturn);
            }
            else if (ReturnId != null)
            {
                //Added by Settu to remove credit sales return amount in voucher
                result.SalesReturn.AccountId = sales.AccountId;
                result.SalesReturn.InstanceId = sales.InstanceId;
                result.SalesReturn.Id = ReturnId;
                await _salesReturnManager.UpdateExistingVoucher(result.SalesReturn);
            }

            //Added by settu, 
            try
            {
                //Added by Sarubala on 03-10-17
                var isCancelEditSms = await _salesManager.getSettingsIsCancelEditSms(User.AccountId(), User.InstanceId());

                //if(sales.SalesReturn.SalesReturnItem.Count > 0 && sales.SalesReturn.SalesId != null)
                //{
                //    await _salesReturnManager.Save(sales.SalesReturn, sales.SalesReturn.AccountId, sales.SalesReturn.InstanceId);
                //    sales.SalesReturn.SalesReturnItem = await _salesSaveManager.GetProductForReturn(sales);
                //}
                //else
                //{
                //    await _salesReturnManager.SaveReturnInSale(sales.SalesReturn);
                //}

                bool internet = NetworkInterface.GetIsNetworkAvailable();

                if (internet == true)
                {
                    if (sales.IsCreated)
                    {
                        if (sales.SalesReturn.SalesId != null) // 
                        {
                            await _salesSaveManager.SendSaleCommunication(sales);
                        }
                        else
                        {
                            //await _salesSaveManager.SendSaleCommunication(sales, result);
                            await _salesSaveManager.SendSaleReturnCommunication(sales, result.SalesReturn);
                        }
                        //Added by Sarubala on 03-10-17 to send sms for Mediplus on Edit
                        if (sales.SalesEditMode == true && isCancelEditSms == true)
                        {
                            string mode = "Edit";
                            await _salesSaveManager.SendSmsOnEdit(sales, mode);
                        }
                    }
                }
            }
            catch (Exception)
            {
                sales.SaveFailed = true;
                sales.ErrorLog = new BaseErrorLog()
                {
                    ErrorMessage = "Sent Sms or Email Failed.",
                };
            }
            return result;
        }

        //Added by Sarubala for other sales page on 21-01-2019 - start
        [Route("[action]")]
        public IActionResult OtherSales(string id)
        {
            string reqagenttype = Request.Headers["user-agent"].ToString();
            if (reqagenttype.Contains("iPhone ") || reqagenttype.Contains("Android "))
            {
                ViewBag.IsMobileDevice = true;
            }
            else
            {
                ViewBag.IsMobileDevice = false;
            }

            ViewBag.CompleteSaleType = _salesManager.getCompleteSaleType(User.AccountId(), User.InstanceId());
            ViewBag.Id = id;
            return View();
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<Sales> OtherSales([FromBody]Sales sales)
        {
            sales.InitializeExecutionQuery();

            if(!string.IsNullOrEmpty(sales.Mobile) && !string.IsNullOrWhiteSpace(sales.Mobile))
            {
                Patient patient = new Patient();
                patient.Mobile = sales.Mobile;
                patient.SetLoggedUserDetails(User);
                var mobileResult = await _salesSaveManager.getPatientDetails(patient);
                if(mobileResult.Count() > 0)
                {
                    sales.PatientId = mobileResult.FirstOrDefault().Id;
                    sales.Name = mobileResult.FirstOrDefault().Name;
                    sales.EmpID = mobileResult.FirstOrDefault().EmpID;
                }                               

            }

            var CreatedBy = sales.CreatedBy;
            DateTime InvoiceDate = Convert.ToDateTime(sales.InvoiceDate);
            sales.SetLoggedUserDetails(User);
            if (sales.isEnableSalesUser == true)
            {
                if (string.IsNullOrEmpty(sales.Id))
                {
                    sales.CreatedBy = CreatedBy;
                    sales.UpdatedBy = CreatedBy;
                }
                else
                {
                    sales.UpdatedBy = CreatedBy;
                }
            }

            bool OfflineStatus = _configHelper.AppConfig.OfflineMode;
            sales.OfflineStatus = OfflineStatus;
            Sales result = new Sales();           

            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true" && _configHelper.AppConfig.OfflineMode == false)
            {
                //Added if condition for Edit by Manivannan on 19-Jul-2017
                if (string.IsNullOrEmpty(sales.Id))
                {
                    sales.Prefix = "O";
                }
            }

            if (sales.SalesItem.Count > 0)
            {
                sales.GSTEnabled = User.GSTEnabled() == "True";
                result = await _salesManager.Save(sales, InvoiceDate, User.InstanceId(), false);
                //Save bulk data - starts
                if (sales.WriteExecutionQuery && result.isEmpID == false)
                {
                    await _salesManager.SaveBulkData(sales);
                    if (sales.SaveFailed)
                    {
                        return sales;
                    }
                }
                //Save bulk data - ends                
            }

            return result;
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<bool> SendOTPToAPI(string mobileNo)
        {
            return await _salesManager.SendOTPToAPI(mobileNo);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<bool> ValidateOTP(string otp, string mobileNo)
        {
            return await _salesManager.VerifyOTPAPI(otp, mobileNo);
        }

        //Added by Sarubala for other sales page on 21-01-19 - end


        //Added by Annadurai on 07242017 -Starts 
        [HttpPost]
        [Route("[action]")]
        public async Task<Sales> uploadFile([FromForm] string id, bool file0Index, IFormFile file)
        {
            Sales salesObj = new Sales();
            salesObj.Id = id;
            if (!string.IsNullOrEmpty(Convert.ToString(file)))
            {
                var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
                var fileName = Guid.NewGuid().ToString() + '_' + parsedContentDisposition.FileName.Trim('"');
                var filePath = Path.Combine(_environment.WebRootPath, "uploads\\sales", fileName);

                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }


                if (!string.IsNullOrEmpty(fileName))
                {
                    salesObj.FileName = fileName;
                }
            }
            var result = await _salesManager.UpdateSaleDetailsforFileUpload(salesObj);
            return result;

        }
        //Added by Annadurai on 07242017 - Ends

        [HttpPost]
        [Route("[action]")]
        public async Task<Sales> NewIndex([FromBody]Sales sales) // Modified for 30 items not getting called issue
        {
            DateTime InvoiceDate = Convert.ToDateTime(sales.InvoiceDate);
            sales.SetLoggedUserDetails(User);
            sales = _salesManager.RemoveReturnItems(sales);
            sales.SalesReturn.SetLoggedUserDetails(User);
            bool OfflineStatus = _configHelper.AppConfig.OfflineMode;
            sales.OfflineStatus = OfflineStatus;

            //var result = await _salesManager.Save(sales, InvoiceDate, User.InstanceId(), true);
            //sales.SalesReturn.SalesId = result.Id;
            //await _salesReturnManager.HandleReturnedItems(sales.SalesReturn, result.Id);

            //Added by Durga Reddy
            Sales result = new Sales();
            if (sales.SalesItem.Count > 0)
            {
                sales.GSTEnabled = User.GSTEnabled() == "True";
                result = await _salesManager.Save(sales, InvoiceDate, User.InstanceId(), false);
                sales.SalesReturn.SalesId = result.Id;
                await _salesReturnManager.HandleReturnedItems(sales.SalesReturn, result.Id);
            }
            else
            {
                sales.SalesReturn.IsAlongWithSale = false;
            }


            if (sales.SalesReturn.SalesReturnItem.Count > 0)
            {
                await _salesReturnManager.Save(sales.SalesReturn, sales.SalesReturn.AccountId, sales.SalesReturn.InstanceId);
            }
            return result;
        }


        [HttpPost]
        [Route("[action]")]
        public bool IsInvoiceManualSeriesAvail(string Invocieseries, string Billdate)
        {
            //bool bReturn = _salesManager.IsInvoiceManualSeriesAvail(Invocieseries, User.InstanceId(),User.AccountId(), Convert.ToDateTime(Billdate)).Result;
            bool bReturn = _salesManager.IsInvoiceManualSeriesAvail(Invocieseries, User.InstanceId(), User.AccountId(), DateTime.ParseExact(Billdate, "dd/MM/yyyy", CultureInfo.InvariantCulture)).Result;
            return bReturn;
        }


        [Route("[action]")]
        public async Task<IActionResult> Sale()
        {
            ViewBag.QuickSell = false;
            ViewBag.CardType = await _salesManager.GetCardType(User.AccountId(), User.InstanceId());
            //Modified by Sarubala on 20-10-17
            //ViewBag.CompleteSaleType = await _salesManager.getCompleteSaleType(User.AccountId(), User.InstanceId());
            //ViewBag.shortDoctorName = await _salesManager.GetShortDoctorName(User.AccountId(), User.InstanceId());
            //ViewBag.shortCustomerName = await _salesManager.GetShortCustomerName(User.AccountId(), User.InstanceId());

            SaleSettings setting1 = await _salesManager.GetBaseSaleSettings(User.AccountId(), User.InstanceId());
            ViewBag.CompleteSaleType = setting1.CompleteSaleKeyType;
            ViewBag.shortDoctorName = setting1.ShortDoctorName;
            ViewBag.shortCustomerName = setting1.ShortCustomerName;

            //Added by bala for show stock wise button based on instance count , 1/June/2017
            ViewBag.InstanceCount = await _salesManager.GetInstanceCount(User.AccountId());
            return View(await _salesManager.GetInvoiceNo(User.InstanceId()) as object);
        }



        [Route("[action]")]
        public async Task<IActionResult> NewSale()
        {
            ViewBag.QuickSell = false;
            ViewBag.CardType = await _salesManager.GetCardType(User.AccountId(), User.InstanceId());
            ViewBag.CompleteSaleType = await _salesManager.getCompleteSaleType(User.AccountId(), User.InstanceId());
            ViewBag.shortDoctorName = await _salesManager.GetShortDoctorName(User.AccountId(), User.InstanceId());
            ViewBag.shortCustomerName = await _salesManager.GetShortCustomerName(User.AccountId(), User.InstanceId());
            return View(await _salesManager.GetInvoiceNo(User.InstanceId()) as object);
        }

        [Route("[action]")]
        public async Task<IActionResult> Settings()
        {
            ViewBag.IsLoyaltyPtsEnabled = await _productStockManager.GetFeatureAccess(4);
            return View();
        }


        [Route("[action]")]
        public IActionResult PrintTemplate()
        {
            return View();
        }

        [Route("[action]")]
        public async Task<IActionResult> QuickSaleView()
        {
            ViewBag.QuickSell = true;
            return View("sale", await _salesManager.GetInvoiceNo(User.InstanceId()));
        }
        [Route("[action]")]
        public IActionResult List()
        {
            var accountid = User.AccountId();
            if (accountid == "852eaefa-f670-4a6a-a0e7-db3fa252d576" || accountid == "756efa5e-66e1-46ff-a3b2-3f282d746d93")
                ViewBag.Accountid = true;
            else
                ViewBag.Accountid = false;
            return View();
        }


        [Route("[action]")]
        public IActionResult NewList()
        {
            var accountid = User.AccountId();
            if (accountid == "852eaefa-f670-4a6a-a0e7-db3fa252d576" || accountid == "756efa5e-66e1-46ff-a3b2-3f282d746d93")
                ViewBag.Accountid = true;
            else
                ViewBag.Accountid = false;
            return View();
        }

        //Added by Sarubala on 05-10-17        
        [Route("[action]")]
        public async Task<List<DomainValues>> GetDomainValues()
        {
            return await _salesManager.GetDomainValues(User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public async Task<SalesType> getSalesType()
        {
            return await _salesManager.GetSalesType(User.AccountId(), User.InstanceId());
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<SalesType> updateSalesType([FromBody]SalesType st)
        {
            st.SetLoggedUserDetails(User);
            return await _salesManager.UpdateSalesType(st);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Sales>> ListData([FromBody]Sales model)
        {
            model.SetLoggedUserDetails(User);
            return await _salesManager.ListPager(model, User.InstanceId(), User.AccountId());
        }
        // Added by Violet
        [HttpPost]
        [Route("[action]")]
        public async Task<Sales> getListData([FromBody]Sales model)
        {
            model.SetLoggedUserDetails(User);
            return await _salesManager.getListPager(model, User.InstanceId(), User.AccountId());
        }
        // End
        [Route("[action]")]
        public async Task<Sales> SalesDetail(string id)
        {
            Sales sale = await _salesManager.GetSalesDetails(id);
            if (sale.DiscountType == 1)
            {
                sale.DiscountValue = sale.Discount;
            }
            else
            {
                sale.Discount = sale.DiscountValue;
            }
            var pt = await _salesManager.GetExistingPatientData(sale.PatientId, sale.Mobile, sale.Name, User.AccountId(), User.InstanceId());

            //Added by Sarubala on 28-09-17 to get sales multiple payment details 
            if (sale.NetAmount > 0 && sale.CashType == "MultiplePayment" && sale.PaymentType == "Multiple")
            {
                sale.SalesPayments = await _salesManager.GetSalesPaymentDetails(id, User.AccountId(), User.InstanceId());
            }

            if (pt != null)
            {
                sale.Patient = pt;
            }
            //return await _salesManager.GetSalesDetails(id);
            return sale;
        }

        [Route("[action]")]
        public Task<List<Sales>> GetPatientName(string patientName)
        {
            return _salesManager.PatientSearchList(patientName, User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public Task<List<Sales>> GetPatientNameList(string instanceid, string patientName)
        {
            string _patientName = patientName;
            string _instanceid = instanceid;
            return _salesManager.PatientSearchList(_patientName, User.AccountId(), _instanceid); //User.InstanceId()
        }
        [Route("[action]")]
        public async Task<Sales> Update([FromBody]Sales model)
        {
            return await _salesManager.UpdateSaleDetails(model);
        }
        //Newly Added Gavaskar 24-10-2016 Start
        [Route("[action]")]
        public async Task<string> saveCardType(string CardType)
        {
            CardTypeSettings cardTypeSettings = new CardTypeSettings();
            cardTypeSettings.SetLoggedUserDetails(User);
            cardTypeSettings.CardType = CardType;
            var cardtype = await _salesManager.SaveCardType(cardTypeSettings);
            return CardType;
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<SalesBatchPopUpSettings> saveBatchPopUpSettings([FromBody]SalesBatchPopUpSettings data)
        {
            data.SetLoggedUserDetails(User);
            var SalesBatchPopUpSettings = await _salesManager.saveBatchPopUpSettings(data);
            return SalesBatchPopUpSettings;
        }
        [Route("[action]")]
        public async Task<SaleSettings> SavePrintType(string printType)
        {
            SaleSettings saleSettings = new SaleSettings();
            saleSettings.SetLoggedUserDetails(User);
            saleSettings.PrinterType = Convert.ToInt32(printType);
            return await _salesManager.SavePrintType(saleSettings);
        }
        [Route("[action]")]
        public async Task<SaleSettings> savePatientSearchType(string PatientSearchType)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.PatientSearchType = PatientSearchType;
            var SearchType = await _salesManager.savePatientSearchType(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> saveDoctorSearchType(string DoctorSearchType)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.DoctorSearchType = DoctorSearchType;
            var SearchType = await _salesManager.saveDoctorSearchType(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> isDoctorMandatory(string DoctorMandatoryType)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.DoctorNameMandatoryType = DoctorMandatoryType;
            var SearchType = await _salesManager.isDoctorMandatory(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> saveSalesTypeMandatory(string SalestypeMandatory)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.SaleTypeMandatory = SalestypeMandatory;
            var SearchType = await _salesManager.saveSalesTypeMandatory(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> isMaximumDiscountAvail(string MaximumDiscountAvail)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.MaxDiscountAvail = MaximumDiscountAvail;
            var SearchType = await _salesManager.isMaximumDiscountAvail(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> saveAutoTempStockAdd(bool status)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.AutoTempStockAdd = status;
            var SearchType = await _salesManager.saveAutoTempStockAdd(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> isAutosavecustomer(string AutosaveCustomer)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.AutosaveCustomer = AutosaveCustomer;
            var SearchType = await _salesManager.isAutosavecustomer(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> saveIsCreditInvoiceSeries(bool IsCreditInvoiceSeries)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.IsCreditInvoiceSeries = IsCreditInvoiceSeries;
            var SearchType = await _salesManager.saveIsCreditInvoiceSeries(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> saveDiscountType(string DiscountType)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.DiscountType = DiscountType;
            var SearchType = await _salesManager.saveDiscountType(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> saveisDepartment(bool IsDepartment)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.PatientTypeDept = IsDepartment;
            var SearchType = await _salesManager.saveisDepartment(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> saveCustomseries(string Customseries)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.CustomSeriesInvoice = Customseries;
            var SearchType = await _salesManager.saveCustomseriesInvoice(SaleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<InvoiceSeries> saveInvoiceSeries(int InvoiceSeries)
        {
            InvoiceSeries Invoiceseriesdata = new InvoiceSeries();
            Invoiceseriesdata.SetLoggedUserDetails(User);
            Invoiceseriesdata.InvoiceseriesType = InvoiceSeries;
            var Inv = await _salesManager.saveInvoiceSeries(Invoiceseriesdata);
            return Inv;
        }
        [Route("[action]")]
        public async Task<InvoiceSeriesItem> saveInvoiceSeriesItem(string Invoiceseries, int seriesType)
        {
            var IS = new InvoiceSeriesItem();
            IS.ActiveStatus = "Y";
            IS.SeriesName = Invoiceseries;
            IS.InvoiceseriesType = seriesType;
            IS.SetLoggedUserDetails(User);
            return await _salesManager.SaveInvoiceSeriesItem(IS);
        }
        [Route("[action]")]
        public async Task<SaleSettings> savecancelBillDays(int CancelBillDays)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.PostCancelBillDays = CancelBillDays;
            var CancelDays = await _salesManager.savecancelBillDays(SaleSettings);
            return CancelDays;
        }
        [Route("[action]")]
        public async Task<SaleSettings> saveMaxDiscount(decimal MaxDiscount)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.InstanceMaxDiscount = MaxDiscount;
            var CancelDays = await _salesManager.saveMaxDiscount(SaleSettings);
            return CancelDays;
        }
        [Route("[action]")]
        public async Task<SaleSettings> getPatientSearchType()
        {
            return await _salesManager.getPatientSearchType(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<InvoiceSeries> getInvoiceSeries()
        {
            return await _salesManager.getInvoiceSeries(User.AccountId(), User.InstanceId());
        }

        //Added by Bikas02/07/2018        
        [Route("[action]")]
        public async Task<Patient> getCustomerIDSeriesItem()
        {
            SaleSettings Data = new SaleSettings();

            Data.SetLoggedUserDetails(User);
            Data.AccountId = User.AccountId();
            Data.InstanceId = User.InstanceId();
            return await _salesManager.GetCustomerIDSeriesItem(Data);
        }
        [Route("[action]")]
        public async Task<SaleSettings> getIsDepartmentadded()
        {
            return await _salesManager.getIsDepartmentadded(User.AccountId(), User.InstanceId());
        }
        #region
        //Added by Manivannan for retrieving the Invoice No in sales screen on 30-Jan-2017 begins here
        [Route("[action]")]
        public async Task<string> GetNumericInvoiceNo()
        {
            return await _salesManager.GetNumericInvoiceNo(User.InstanceId());
        }
        [Route("[action]")]
        public async Task<string> GetCustomInvoiceNo(string customSeries)
        {
            return await _salesManager.GetCustomInvoiceNo(User.InstanceId(), customSeries);
        }
        #endregion        
        [Route("[action]")]
        public async Task<SaleSettings> getDoctorSearchType()
        {
            return await _salesManager.getDoctorSearchType(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> getDoctorNameMandatoryType()
        {
            return await _salesManager.getDoctorNameMandatoryType(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> getIsMaxDiscountAvail()
        {
            return await _salesManager.getIsMaxDiscountAvail(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> getSalestypeMandatory()
        {
            return await _salesManager.getSalestypeMandatory(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> getAutoTempstockAdd()
        {
            return await _salesManager.getAutoTempstockAdd(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> getautoSaveisMandatory()
        {
            return await _salesManager.getautoSaveisMandatory(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> getIsCreditInvoiceSeries()
        {
            return await _salesManager.getIsCreditInvoiceSeries(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> getPharmacyDiscountType()
        {
            return await _salesManager.getPharmacyDiscountType(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SalesBatchPopUpSettings> getBatchPopUpSettings()
        {
            return await _salesManager.getBatchPopUpSettings(User.AccountId(), User.InstanceId());
        }

        //Added by Sarubala on 06-11-17
        [Route("[action]")]
        public async Task<int> getSalesPriceSetting()
        {
            return await _salesManager.getSalesPriceSetting(User.InstanceId());
        }

        //Added by Sarubala on 15-11-17
        [Route("[action]")]
        public async Task<SmsSettings> getSmsSettings()
        {
            return await _salesManager.getSmsSettings(User.InstanceId(), User.AccountId());
        }

        //Added by Sarubala on 23-11-17
        [HttpPost]
        [Route("[action]")]
        public async Task<SmsSettings> getSmsSettingsForInstance()
        {
            return await _salesManager.getSmsSettingsForInstance(User.AccountId(), User.InstanceId());
        }

        //Added by Sarubala on 23-11-17
        [HttpPost]
        [Route("[action]")]
        public async Task<SmsSettings> saveSmsSettingsForInstance([FromBody]SmsSettings data)
        {
            data.SetLoggedUserDetails(User);
            return await _salesManager.saveSmsSettingsForInstance(data);
        }

        //Added by Sarubala on 28-11-17
        [Route("[action]")]
        public async Task<long> GetRemainingSmsCount()
        {
            return await _salesManager.GetRemainingSmsCount(User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public async Task<string> getCardTypeDetail()
        {
            return await _salesManager.GetCardType(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<int> GetPrintType()
        {
            return await _salesManager.GetPrintType(User.AccountId(), User.InstanceId());
        }
        //[Route("[action]")]
        //public async Task<string> GetBillPrintStatus()
        //{
        //    return await _salesManager.GetBillPrintStatus(User.AccountId(), User.InstanceId());
        //}
        [Route("[action]")]
        public async Task<Tuple<string, string>> GetBillPrintStatus()
        {
            return await _salesManager.GetBillPrintStatus(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<string> getBatchListDetail()
        {
            return await _salesManager.GetBatchListDetail(User.AccountId(), User.InstanceId());
        }

        //Added by Sarubala on 14-10-17
        [Route("[action]")]
        public async Task<SaleSettings> GetAllSalesSettings()
        {
            return await _salesManager.GetAllSalesSettings(User.AccountId(), User.InstanceId());
        }

        //Added by Sarubala on 20-10-17
        [Route("[action]")]
        public async Task<Sales> GetLastSaleDetails()
        {
            return await _salesManager.GetLastSaleDetails(User.AccountId(), User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<BatchListSettings> saveBatchType(string batchType)
        {
            var bs = new BatchListSettings();
            bs.SetLoggedUserDetails(User);
            bs.BatchListType = batchType;
            return await _salesManager.SaveBatchType(bs);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<BillPrintSettings> saveBillPrintStatus([FromBody] PrintStatusFooter printStatusFooter)
        {
            string printStatus = printStatusFooter.PrintStatus;
            string printFooterNotes = printStatusFooter.PrintFooterNotes;
            var ps = new BillPrintSettings();
            ps.SetLoggedUserDetails(User);
            ps.Status = printStatus;
            ps.FooterNote = printFooterNotes;
            return await _salesManager.SaveBillPrintStatus(ps);
        }
        //[HttpPost]
        //[Route("[action]")]
        //public async Task<SalesBatchPopUpSettings> SavePrintFooterNote([FromBody]string printFooterNotes)
        //{
        //    var ps = new BillPrintSettings();
        //    ps.SetLoggedUserDetails(User);
        //    data.SetLoggedUserDetails(User);
        //    ps.FooterNote = printFooterNotes;
        //    return await _salesManager.savePrintFooterNote(data);
        //}
        //Newly Added Shortcut keys setting Gavaskar 03-02-2017 Start
        [HttpPost]
        [Route("[action]")]
        public async Task<SaleSettings> saveShortCutKeySetting(bool KeyType, bool NewWindowType)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.ShortCutKeysType = KeyType;
            SaleSettings.NewWindowType = NewWindowType;
            return await _salesManager.SaveShortCutKeySetting(SaleSettings);
        }
        [Route("[action]")]
        public async Task<SaleSettings> getShortCutKeySetting()
        {
            return await _salesManager.GetShortCutKeySetting(User.AccountId(), User.InstanceId());
        }
        //Newly Added Shortcut keys setting Gavaskar 03-02-2017 End
        [Route("[action]")]
        public async Task<SaleSettings> getCustomSeriesSelected()
        {
            return await _salesManager.getCustomSeriesSelected(User.AccountId(), User.InstanceId());
        }

        //Added by Sarubala on 03-12-18 - start
        [Route("[action]")]
        public async Task<SaleSettings> getCustomerIdSeriesSettings()
        {
            return await _salesManager.getCustomerIdSeriesSettings(User.AccountId(), User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<SaleSettings> saveCustomerIdSeriesSettings([FromBody]SaleSettings settings)
        {
            settings.SetLoggedUserDetails(User);
            return await _salesManager.saveCustomerIdSeriesSettings(settings);
        }

        //Added by Sarubala on 03-12-18 - end

        [Route("[action]")]
        public async Task<SaleSettings> getCancelBillDays()
        {
            return await _salesManager.getCancelBillDays(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> getMaxDiscountValue()
        {
            return await _salesManager.getMaxDiscountValue(User.AccountId(), User.InstanceId());
        }
        //Newly Added Gavaskar 24-10-2016 End
        [Route("[action]")]
        public async Task<DiscountRules> SaveDiscountRules([FromBody]DiscountRules model)
        {
            model.SetLoggedUserDetails(User);
            return await _salesManager.SaveDiscount(model);
        }
        //Newly Added Gavaskar 08-11-2016 Start
        [HttpPost]
        [Route("[action]")]
        public async Task<InventorySmsSettings> saveSmsSetting([FromBody]InventorySmsSettings inventorySmsSettings)
        {
            inventorySmsSettings.SetLoggedUserDetails(User);
            return await _salesManager.SaveSmsSetting(inventorySmsSettings);
        }
        [Route("[action]")]
        public async Task<InventorySmsSettings> getSmsSetting(InventorySmsSettings inventorySmsSettings)
        {
            //public async Task<InventorySmsSettings> GetSmsSetting(string AccountId, string InstanceId)
            inventorySmsSettings.SetLoggedUserDetails(User);
            return await _salesManager.GetSmsSetting(inventorySmsSettings.AccountId, inventorySmsSettings.InstanceId);
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<SalesType> saveSalesType(string typeName)
        {
            var st = new SalesType();
            st.Name = typeName;
            st.SetLoggedUserDetails(User);
            return await _salesManager.SaveSalesType(st);
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<string> removeSalesType(string id)
        {
            var st = new SalesType();
            st.SetLoggedUserDetails(User);
            return await _salesManager.RemoveSalesType(id, st.AccountId);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<SalesType> saveStatus([FromBody]SalesType sales)
        {
            return await _salesManager.SaveStatus(sales);
        }
        [Route("[action]")]
        public async Task<SalesType> getPrescriptionSetting()
        {
            SalesType st = new SalesType();
            st.SetLoggedUserDetails(User);
            return await _salesManager.GetPrescriptionSetting(st);
        }
        [Route("[action]")]
        public async Task<List<InvoiceSeriesItem>> getInvoiceSeriesItems()
        {
            InvoiceSeriesItem IS = new InvoiceSeriesItem();
            IS.SetLoggedUserDetails(User);
            return await _salesManager.GetInvoiceSeriesItems(IS);
        }

        [Route("[action]")]
        public async Task<DiscountRules> SalesDiscountDetail()
        {
            var instantceId = User.InstanceId();
            DiscountRules discountRules = new DiscountRules();
            discountRules.InstanceId = instantceId;
            return await _salesManager.GetDiscountRules(discountRules);
        }
        [Route("[action]")]
        public async Task<DiscountRules> editDiscountDetail()
        {
            var instantceId = User.InstanceId();
            DiscountRules discountRules = new DiscountRules();
            discountRules.InstanceId = instantceId;
            return await _salesManager.EditDiscountRules(discountRules);
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<DiscountRules> editDiscountDetail1(string type)
        {
            var instantceId = User.InstanceId();
            DiscountRules discountRules = new DiscountRules();
            discountRules.InstanceId = instantceId;
            discountRules.BillAmountType = type;
            return await _salesManager.EditDiscountRules1(discountRules);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<MissedOrder> createMissedOrder([FromBody]MissedOrder data)
        {
            data.SetLoggedUserDetails(User);
            return await _salesManager.CreateMissedOrder(data);
        }
        [Route("[action]")]
        public FileResult Download(string fileName)
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads\\sales", fileName);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "image/jpeg");
        }
        /// <summary>
        /// Get offline status from sales manages
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("[action]")]
        public async Task<Boolean> getOfflineStatus()
        {
            //Enable Pages for offline users Validation added by Poongodi on 10/07/2017
            if (_configHelper.AppConfig.OfflineMode == false)
                return await _salesManager.getOfflineStatus(User.AccountId(), User.InstanceId());
            else
                return false;
        }

        //Added by Sarubala on 18-05-2018
        [HttpGet]
        [Route("[action]")]
        public async Task<Boolean> getOnlineEnabledStatus()
        {
            return await _salesManager.getOnlineEnabledStatus(User.AccountId(), User.UserId());
        }


        //Added by Sarubala on 23-10-17
        [Route("[action]")]
        public async Task<Instance> getInstanceValues()
        {
            Instance data = await _salesManager.GetInstanceValues(User.AccountId(), User.InstanceId());

            if (_configHelper.AppConfig.OfflineMode != false)
            {
                data.OfflineStatus = false;
            }

            return data;
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> searchSales([FromBody]DateRange data, decimal reOrderFactor, int type, bool loadAllProducts)
        {
            return await _salesManager.searchSales(data.FromDate, data.ToDate, reOrderFactor, User.AccountId(), User.InstanceId(), type, loadAllProducts);
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<Boolean> getLastSalesPrintType()
        {
            return await _salesManager.getLastSalesPrintType(User.AccountId(), User.InstanceId());
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<SaleSettings> saveShortName(bool cutomerName, bool doctorName)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.ShortCustomerName = cutomerName;
            SaleSettings.ShortDoctorName = doctorName;
            return await _salesManager.saveShortName(SaleSettings);
        }
        [Route("[action]")]
        public async Task<SaleSettings> getShortName()
        {
            return await _salesManager.getShortName(User.AccountId(), User.InstanceId());
        }
        //[HttpPost]
        [Route("[action]")]
        public async Task<SaleSettings> saveCompleteSaleKeys(string CompleteSaleKeys)
        {
            SaleSettings SaleSettings = new SaleSettings();
            SaleSettings.SetLoggedUserDetails(User);
            SaleSettings.CompleteSaleKeyType = CompleteSaleKeys;
            var CompleteSaleType = await _salesManager.saveCompleteSaleKeys(SaleSettings);
            return CompleteSaleType;
        }
        [Route("[action]")]
        public async Task<SaleSettings> getCompleteSaleKeys()
        {
            return await _salesManager.getCompleteSaleKeys(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> getScanBarcodeOption()
        {
            return await _salesManager.getScanBarcodeOption(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SaleSettings> saveBarcodeOption(string barcodeOption, int autoScanQty)
        {
            SaleSettings saleSettings = new SaleSettings();
            saleSettings.SetLoggedUserDetails(User);
            saleSettings.ScanBarcode = barcodeOption;
            saleSettings.AutoScanQty = autoScanQty;
            var SearchType = await _salesManager.saveBarcodeOption(saleSettings);
            return SearchType;
        }
        [Route("[action]")]
        public async Task<string> GetCustomCreditInvoiceNo(string customSeries)
        {
            return await _salesManager.GetCustomCreditInvoiceNo(User.InstanceId(), customSeries);
        }
        [Route("[action]")]
        public async Task<SaleSettings> getCustomCreditSeriesSelected()
        {
            return await _salesManager.getCustomCreditSeriesSelected(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<Sales> getLastSalesId()
        {
            return await _salesManager.getLastSalesId(User.AccountId(), User.InstanceId());
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<Boolean> getLastSalesSmsType()
        {
            return await _salesManager.getLastSalesSmsType(User.AccountId(), User.InstanceId());
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<Boolean> getLastSalesEmailType()
        {
            return await _salesManager.getLastSalesEmailType(User.AccountId(), User.InstanceId());
        }
        //Method added to get  Custom series for Purchase  on 26/04/2017
        [Route("[action]")]
        public async Task<string> getInvoiceSeriesItemForPurchaseSettings()
        {
            return await _salesManager.getInvoiceSeriesItemForPurchaseSettings(User.AccountId(), User.InstanceId());
        }

        //Code by Sarubala to get GSTin from db - start
        [Route("[action]")]
        public IActionResult UpdateGSTIN()
        {
            return View();
        }

        [Route("[action]")]
        public async Task<string> getGSTINDetail()
        {
            return await _salesManager.getGSTINDetail(User.AccountId(), User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Instance> SaveGstIn([FromBody]Instance data)
        {
            data.SetLoggedUserDetails(User);
            return await _salesManager.SaveGstIn(data);
        }

        //Code by Sarubala to get GSTin from db - end

        // Added by Gavaskar Loyalty Points Start
        [HttpGet]
        [Route("[action]")]
        public async Task<LoyaltyPointSettings> GetLoyaltyPointSettings()
        {
            return await _salesManager.getLoyaltyPointSettings(User.AccountId(), User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<LoyaltyPointSettings> SaveLoyaltyPointSettings([FromBody] LoyaltyPointSettings data)
        {
            data.SetLoggedUserDetails(User);
            return await _salesManager.saveLoyaltyPointSettings(data);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<int> GetfirstLoyaltyCheck(string patientId)
        {
            return await _salesManager.GetfirstLoyaltyCheck(patientId, User.AccountId(), User.InstanceId());

        }

        [HttpGet]
        [Route("[action]")]
        public async Task<LoyaltyPointSettings> GetSaleLoyaltyPointSettings(string loyaltyId)
        {
            return await _salesManager.GetSaleLoyaltyPointSettings(loyaltyId, User.AccountId(), User.InstanceId());
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<List<Product>> GetKindOfProductDataList()
        {
            return await _salesManager.GetKindOfProductDataList(User.AccountId(), User.InstanceId());
        }
        // Added by Gavaskar Loyalty Points End

        //Added by Annadurai on 07032017 for showing History Popup
        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> PreviousSalesDetailsForSelectedCustomer(string Name, string Mobile, string SelectedProduct)
        {
            return await _salesManager.PreviousSalesDetailsForSelectedCustomer(Name, Mobile, User.AccountId(), SelectedProduct);
        }
        // hCue Pharma HSN Code Ref..
        [Route("[action]")]
        [HttpGet]
        public FileResult Download()
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads", "hCue Pharma HSN Code Reference template.xlsx");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "text/csv", "hCue Pharma HSN Code Reference template.xlsx");
        }

        [Route("[action]")]
        public IActionResult ListNew()
        {
            return View();
        }

        [Route("[action]")]
        public async Task<string> GetSalesIdByInvoiceNo(int SeriesType, string SeriesTypeValue, string InvNo, bool returnStatus)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            string baseUrl = HttpContext.Request.Host.ToString();
            string id = await _salesManager.GetSaleIdByInvoiceNo(SeriesType, SeriesTypeValue, InvNo, User.AccountId(), User.InstanceId(), returnStatus);

            return id;
        }

        #region Sales Order , Estimate and Template Functionality
        // Added by Gavaskar Sales Order New Functionality 28-09-2017 Start
        [Route("[action]")]
        public async Task<IActionResult> SalesOrder()
        {
            ViewBag.IsSalesTemplateEnabled = await _productStockManager.GetFeatureAccess(2);
            return View();
        }

        [Route("[action]")]
        public async Task<IActionResult> SalesOrderHistory()
        {
            ViewBag.IsSalesTemplateEnabled = await _productStockManager.GetFeatureAccess(2);
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<SalesOrderEstimate> CreateSalesOrder([FromBody]SalesOrderEstimate data)
        {
            data.SetLoggedUserDetails(User);
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
            && _configHelper.AppConfig.OfflineMode == false)
            {
                data.Prefix = "O";
            }
            return await _salesSaveManager.CreateSalesOrder(data);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> CreateSalesNewTemplate([FromBody]SalesTemplate data)
        {
            try
            {
                data.SetLoggedUserDetails(User);
                if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
               && _configHelper.AppConfig.OfflineMode == false)
                {
                    data.Prefix = "O";
                }
                await _salesSaveManager.CreateSalesNewTemplate(data);
            }
            catch (Exception e)
            {
                HttpContext.Response.Clear();
                HttpContext.Response.StatusCode = 500;
                return Json(new { success = false, errorDesc = e.Message });
            }
            return View("SalesOrder");
        }

        [Route("[action]")]
        public async Task<List<SalesTemplate>> GetTemplateNameList()
        {
            return await _salesSaveManager.GetTemplateNameList(User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public async Task<List<SalesTemplateItem>> getSelectedTemplateNameList(string TemplateId)
        {
            return await _salesSaveManager.getSelectedTemplateNameList(TemplateId, User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public async Task<SalesTemplate> updateTemplate([FromBody]SalesTemplate data)
        {
            data.SetLoggedUserDetails(User);
            return await _salesSaveManager.updateTemplate(data);
        }

        [Route("[action]")]
        public async Task<PagerContract<SalesOrderEstimate>> SalesOrderList([FromBody]SalesOrderEstimate model)
        {
            model.SetLoggedUserDetails(User);
            return await _salesSaveManager.SalesOrderList(model, User.InstanceId());
        }

        [Route("[action]")]
        public async Task<PagerContract<SalesTemplate>> SalesTemplateList([FromBody]SalesTemplate model)
        {
            model.SetLoggedUserDetails(User);
            return await _salesSaveManager.SalesTemplateList(model, User.InstanceId());
        }
        [Route("[action]")]
        public async Task<SalesTemplate> UpdateSalesTemplateStatus([FromBody]SalesTemplate data)
        {
            data.SetLoggedUserDetails(User);
            return await _salesSaveManager.UpdateSalesTemplateStatus(data);
        }

        [Route("[action]")]
        public IActionResult UpdateSalesOrderEstimate(string salesOrderEstimateId)
        {
            if (salesOrderEstimateId != null)
            {
                ViewBag.salesOrderEstimateId = salesOrderEstimateId;
            }
            return View();
        }

        [Route("[action]")]
        public async Task<SalesOrderEstimate> GetSalesOrderById(string salesOrderEstimateId)
        {
            return await _salesSaveManager.GetSalesOrderById(salesOrderEstimateId, User.AccountId(), User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task updateSalesOrderEstimateItem([FromBody]SalesOrderEstimate model)
        {
            await _salesSaveManager.updateSalesOrderEstimateItem(model.SalesOrderEstimateItem.ToList(), model);
        }

        [Route("[action]")]
        public async Task<List<ProductStock>> GetSelectedProductStockList(string TemplateId, string Types)
        {
            return await _salesSaveManager.GetSelectedProductStockList(TemplateId, User.AccountId(), User.InstanceId(), Types);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<bool> ResendSalesOrderEstimateSms([FromBody]SalesOrderEstimate salesOrderEstimate)
        {
            return await _salesSaveManager.ResendSalesOrderEstimateSms(salesOrderEstimate, User.AccountId(), User.InstanceId());
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<bool> ResendSalesOrderEstimateEmail([FromBody]SalesOrderEstimate salesOrderEstimate)
        {
            return await _salesSaveManager.ResendSalesOrderEstimateEmail(salesOrderEstimate, User.AccountId(), User.InstanceId());
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<List<SalesOrderEstimate>> GetSelectedCustomerOrderEstimateList(string patientId)
        {
            return await _salesSaveManager.GetSelectedCustomerOrderEstimateList(patientId, User.AccountId(), User.InstanceId());
        }
        // Added by Gavaskar Sales Order New Functionality 28-09-2017 End
        #endregion Sales Order , Estimate and Template Functionality



        [Route("[action]")]
        public async Task<SaleSettings> SaveRoundOff(int isEnableRoundOff)
        {
            SaleSettings saleSettings = new SaleSettings();
            saleSettings.SetLoggedUserDetails(User);
            saleSettings.IsEnableRoundOff = Convert.ToBoolean(isEnableRoundOff);
            return await _salesManager.SaveRoundOff(saleSettings);
        }

        //Added by Sarubala on 27-03-19 - start
        [Route("[action]")]
        public async Task<bool> getSalesScreenSettings()
        {
            return await _salesManager.GetSalesScreenSettings(User.AccountId(), User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<SaleSettings> saveSalesScreenSettings(int isEnableNewSalesScreen)
        {
            SaleSettings setting = new SaleSettings();
            setting.SetLoggedUserDetails(User);
            setting.IsEnableNewSalesScreen = Convert.ToBoolean(isEnableNewSalesScreen);
            return await _salesManager.SaveSalesScreenSettings(setting);
        }

        //Added by Sarubala on 27-03-19 - end

        [Route("[action]")]
        public async Task<SaleSettings> SaveFreeQty(int isEnableFreeQty)
        {
            SaleSettings saleSettings = new SaleSettings();
            saleSettings.SetLoggedUserDetails(User);
            saleSettings.IsEnableFreeQty = Convert.ToBoolean(isEnableFreeQty);
            return await _salesManager.SaveFreeQty(saleSettings);
        }

        [Route("[action]")]
        public async Task<bool> GetRoundOffSettings()
        {
            return await _salesManager.GetRoundOffSettings(User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public async Task<bool> getFreeQtySettings()
        {
            return await _salesManager.getFreeQtySettings(User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public async Task<bool> GetIsSalesTemplateEnabled()
        {
            return await _productStockManager.GetFeatureAccess(2);
        }


        [Route("[action]")]
        public async Task<SaleSettings> SaveSaleUserSettings(bool isEnableSalesUser)
        {
            SaleSettings saleSettings = new SaleSettings();
            saleSettings.SetLoggedUserDetails(User);
            //saleSettings.IsEnableSalesUser = Convert.ToBoolean(isEnableSalesUser);
            saleSettings.IsEnableSalesUser = isEnableSalesUser;
            return await _salesManager.SaveSaleUserSettings(saleSettings);
        }

        [Route("[action]")]
        public async Task<bool> GetSaleUserSettings()
        {
            return await _salesManager.GetSaleUserSettings(User.AccountId(), User.InstanceId());
        }
    }
    public class PrintStatusFooter
    {
        public string PrintStatus { get; set; }
        public string PrintFooterNotes { get; set; }
    }

}
