﻿using HQue.Biz.Core.Inventory;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Inventory
{
    [Route("[controller]")]
    public class ProductStockController : BaseController
    {
        private readonly ProductStockManager _productStockManager;
        private readonly ConfigHelper _configHelper;
        public ProductStockController(ConfigHelper configHelper, ProductStockManager productStockManager) : base(configHelper)
        {
            _configHelper = configHelper;
            _productStockManager = productStockManager;
        }

        [Route("[action]")]
        public async Task<IActionResult> List()
        {
            ViewBag.IsBarcodeEnabled = await _productStockManager.GetFeatureAccess(1);
            return View();
        }

        [Route("[action]")]
        public async Task<IActionResult> UpdateInventory()
        {
            ViewBag.IsBarcodeEnabled = await _productStockManager.GetFeatureAccess(1);
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/ProductStock/List");
            }
            return View();
        }

        [Route("[action]")]
        public async Task<IActionResult> ProductList()
        {
            ViewBag.IsBarcodeEnabled = await _productStockManager.GetFeatureAccess(1);
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/ProductStock/List");
            }
            return View();
        }

        [Route("[action]")]
        public async Task<IActionResult> InventorySetting()
        {
            ViewBag.IsBarcodeEnabled = await _productStockManager.GetFeatureAccess(1);
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/ProductStock/List");
            }
            return View();
        }

        [Route("[action]")]
        public async Task<IActionResult> ProductMaster()
        {
            ViewBag.IsBarcodeEnabled = await _productStockManager.GetFeatureAccess(1);
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/ProductStock/List");
            }
            return View();
        }

        [Route("[action]")]
        public async Task<IActionResult> SelfConsumption()
        {
            ViewBag.IsBarcodeEnabled = await _productStockManager.GetFeatureAccess(1);
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/ProductStock/List");
            }
            return View();
        }

        [Route("[action]")]
        public async Task<IActionResult> BarcodeProfile()
        {
            ViewBag.IsBarcodeEnabled = await _productStockManager.GetFeatureAccess(1);
            if (ViewBag.IsBarcodeEnabled == true)
            {
                return View();
            }
            else
            {
                return View("Rights");
            }
        }

        [Route("[action]")]
        public async Task<IActionResult> BarcodePRNDesign()
        {
            ViewBag.IsBarcodeEnabled = await _productStockManager.GetFeatureAccess(1);
            if (ViewBag.IsBarcodeEnabled == true)
            {
                return View();
            }
            else
            {
                return View("Rights");
            }
        }

        [Route("[action]")]
        public async Task<IActionResult> BarcodeGeneration()
        {
            ViewBag.IsBarcodeEnabled = await _productStockManager.GetFeatureAccess(1);
            if (ViewBag.IsBarcodeEnabled == true)
            {
                return View();
            }
            else
            {
                return View("Rights");
            }
        }

    }
}
