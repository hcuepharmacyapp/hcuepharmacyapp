﻿using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Inventory
{
    public class EstimateController : BaseController
    {
        public EstimateController(ConfigHelper configHelper) : base(configHelper)
        {

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult List()
        {
            return View();
        }
    }
}
