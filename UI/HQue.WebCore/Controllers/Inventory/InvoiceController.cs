﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;
using HQue.Biz.Core.Inventory;
using HQue.Biz.Core.Inventory.InvoicePrint;
using HQue.Contract.Infrastructure.Inventory;
using HQue.WebCore.ViewModel;
using System;
using System.Text;

namespace HQue.WebCore.Controllers.Inventory
{
    [Route("[controller]")]
    public class InvoiceController : BaseController
    {
        private readonly SalesManager _salesManager;
        private readonly PrintManager _printManager;
        private readonly ConfigHelper _configHelper;

      

        public InvoiceController(SalesManager salesManager, ConfigHelper configHelper, PrintManager printManager) : base(configHelper)
        {
            _salesManager = salesManager;
            _printManager = printManager;
            _configHelper = configHelper;
        }

        //[AllowAnonymous]
        //[Route("[action]")]
        //public async Task<IActionResult> SalesInvoice(string id ,int printerType)
        //{

        //    var model = await _printManager.SalesInvoice(id, printerType);

        //    return PrintResult(model);
        //}
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> SalesInvoice(string id)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            string baseUrl = HttpContext.Request.Host.ToString();

            int printerType = await _salesManager.GetPrintType(User.AccountId(), User.InstanceId());
            
            if (!isOffline && !baseUrl.Contains("localhost") || printerType==0)
            {
                var model = await _printManager.SalesInvoice(id, printerType, User.AccountId(), User.InstanceId(), false);
                model.PrinterType = printerType;
                return PrintResult(model);
            }
            else
            {
                //Notepad download

                var model = await _printManager.SalesInvoice(id, printerType, User.AccountId(), User.InstanceId(), true);
                model.PrinterType = printerType;
                return PrintResult(model);
                
                //File not download

                //var model = await _printManager.SalesInvoice(id, printerType, User.AccountId(), User.InstanceId(), true);
                //byte[] script = Encoding.UTF8.GetBytes("<script>window.close();</script>");
                //HttpContext.Response.Body.Write(script, 0, script.Length);
                //return new EmptyResult();
            }

        }
        
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> salesInvoiceNew(string SalesId, int SeriesType, string SeriesTypeValue, string InvNo, int printerType)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            string baseUrl = HttpContext.Request.Host.ToString();
            string id = SalesId;

            if (!string.IsNullOrEmpty(id))
            {
                if (!isOffline && !baseUrl.Contains("localhost") || printerType == 0)
                {
                    var model = await _printManager.SalesInvoice(id, printerType, User.AccountId(), User.InstanceId(), false);
                    model.PrinterType = printerType;
                    return PrintResult(model);
                }
                else
                {
                    var model = await _printManager.SalesInvoice(id, printerType, User.AccountId(), User.InstanceId(), true);
                    model.PrinterType = printerType;
                    return PrintResult(model);
                }
            }
            return Redirect(Url.Action("ListNew", "Sales"));
           
        }

        //Code added by Sarubala - Return bill print Start
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> ReturnWithoutSalesInvoice(string id, int printer = 0)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            string baseUrl = HttpContext.Request.Host.ToString();

            int printerType;
            if (printer == 0)
                printerType = await _salesManager.GetPrintType(User.AccountId(), User.InstanceId());
            else
                printerType = printer;

            if (!isOffline && !baseUrl.Contains("localhost") || printerType == 0)
            {
                var model = await _printManager.ReturnInvoice(id, printerType, User.AccountId(), User.InstanceId(), false);
                model.PrinterType = printerType;
                return PrintReturnResult(model);
            }
            else
            {
                var model = await _printManager.ReturnInvoice(id, printerType, User.AccountId(), User.InstanceId(), true);
                model.PrinterType = printerType;
                return PrintResult(model);
                //var model = await _printManager.SalesInvoice(id, printerType, User.AccountId(), User.InstanceId(), true);
                //byte[] script = Encoding.UTF8.GetBytes("<script>window.close();</script>");
                //HttpContext.Response.Body.Write(script, 0, script.Length);
                //return new EmptyResult();
            }

        }


        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> ReturnSalesInvoiceView(string id)
        {
            int printerType = 0;
            var model = await _printManager.ReturnInvoice(id, printerType, User.AccountId(), User.InstanceId(), false);
            model.PrinterType = printerType;
            return View("ReturnOnly", model);
            

        }
        //Code added by Sarubala - Return bill print End

        //Method added to view the all sale item details by Poongodion 27/02/2017
        [AllowAnonymous]    
        [Route("[action]")]
        public async Task<IActionResult> SalesViewInvoice(string id)
        {
            int printerType = 0; //Type hardcoded to view the results in new window

            var model = await _printManager.SalesInvoice(id, printerType, User.AccountId(), User.InstanceId(), false);

            return ViewResult(model);
        }
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> Estimate(string id)
        {
            int printerType = await _salesManager.GetPrintType(User.AccountId(), User.InstanceId());
            var model = await _printManager.Estimate(id, printerType);
            return PrintResult(model);
        }

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> GetReturnListById(string salesid, string salesreturnid)
        {
            var obj = new ReturnInvoice();
            if (salesid != null && salesid != "undefined")
            {
                obj = await _salesManager.GetReturnListById(salesid, salesreturnid);
            }
            else
            {
                if (salesreturnid == "salesReturn")
                {
                    var getLastReturnId = await _salesManager.GetLastReturnId(User.Identity.Id(), User.InstanceId());
                    obj = await _salesManager.GetReturnListWithoutSaleById(getLastReturnId.Id);
                }
                else
                {
                    obj = await _salesManager.GetReturnListWithoutSaleById(salesreturnid);
                }                
            }
            return View(obj);
        }

        private IActionResult PrintResult(InvoiceVM model)
        {
            if (string.IsNullOrEmpty(model.FilePath))
                return View("Index", model);

            var info = new FileInfo(model.FilePath);
            HttpContext.Response.ContentType = "application/notepad";
            if(model.PrinterType==12 || model.PrinterType == 16)
                return File(info.OpenRead(), "application/notepad", string.Format("hq_PP_{0}.txt", model.Sales.InvoiceNo));
            else
                return File(info.OpenRead(), "application/notepad", string.Format("hq_{0}.txt", model.Sales.InvoiceNo));
        }

        //Method added to view the all sale item details by Poongodion 27/02/2017
        private IActionResult ViewResult(InvoiceVM model)
        {
            return View("Index", model);
        }
        

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> orderPrint(string vendororderid,int customOrder)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            var model = await  _printManager.orderPrint(vendororderid, User.AccountId(), User.InstanceId(), isOffline, customOrder);
            return OrderPrintResult(model);
        }

        private IActionResult OrderPrintResult(VendorOrder model)
        {
            if (string.IsNullOrEmpty(model.FilePath))
            {
                return View("Index", model);
            }
            var info = new FileInfo(model.FilePath);
            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("hq_{0}.txt", model.OrderId));
        }
        private IActionResult PrintReturnResult(InvoiceVM model)
        {
            if (string.IsNullOrEmpty(model.FilePath))
                return View("ReturnOnly", model);

            var info = new FileInfo(model.FilePath);
            HttpContext.Response.ContentType = "application/notepad";
            if (model.PrinterType == 12 || model.PrinterType == 16)
                return File(info.OpenRead(), "application/notepad", string.Format("hq_PP_{0}.txt", model.Sales.InvoiceNo));
            else
                return File(info.OpenRead(), "application/notepad", string.Format("hq_{0}.txt", model.Sales.InvoiceNo));
        }
        
        

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> TransferPrint(string id,bool transferSettings)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            var model = await _printManager.TransferPrint(id, User.AccountId(), User.InstanceId(), transferSettings, isOffline);
            return TransferPrintResult(model);
        }

        private IActionResult TransferPrintResult(StockTransfer model)
        {
            if (string.IsNullOrEmpty(model.FilePath))
            {
                return View("Index", model);
            }
            var info = new FileInfo(model.FilePath);
            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", model.TransferNo));
        }

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> salesReturnOnlyByReturnId(string SalesReturnId, int SeriesType, string SeriesTypeValue, string InvNo, int printerType)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            string baseUrl = HttpContext.Request.Host.ToString();
            string id = SalesReturnId;

            if (!string.IsNullOrEmpty(id))
            {
                if (!isOffline && !baseUrl.Contains("localhost") || printerType == 0)
                {
                    var model = await _printManager.ReturnInvoice(id, printerType, User.AccountId(), User.InstanceId(), false);
                    model.PrinterType = printerType;
                    return PrintResult(model);
                }
                else
                {
                    var model = await _printManager.ReturnInvoice(id, printerType, User.AccountId(), User.InstanceId(), true);
                    model.PrinterType = printerType;
                    return PrintResult(model);
                }
            }
            return Redirect(Url.Action("ListNew", "Sales"));

        }

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<InvoiceVM> SalesInvoiceContent(string id)
        {
            return await _printManager.SetupInvoiceData(id, User.AccountId(), User.InstanceId());
        }

        // Added by Gavaskar 29-10-2017 Start
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> salesOrderEstimatePrint(string salesOrderEstimateId)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            var model = await _printManager.salesOrderEstimatePrint(salesOrderEstimateId, User.AccountId(), User.InstanceId(), isOffline);
            //return null;
            return salesOrderEstimatePrintResult(model);
        }

        private IActionResult salesOrderEstimatePrintResult(SalesOrderEstimate model)
        {
            if (string.IsNullOrEmpty(model.FilePath))
            {
                return View("Index", model);
            }
            var info = new FileInfo(model.FilePath);
            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("hq_{0}.txt", model.OrderEstimateId));
        }
        // Added by Gavaskar 29-10-2017 End

    }
}
