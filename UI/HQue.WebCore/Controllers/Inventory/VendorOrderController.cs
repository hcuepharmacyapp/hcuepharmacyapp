﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.WebCore.ViewModel;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using HQue.Contract.Infrastructure;
using HQue.Biz.Core.Inventory;
using HQue.Biz.Core.Master;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.Setup;
using System;

namespace HQue.WebCore.Controllers.Inventory
{
    [Route("[controller]")]
    public class VendorOrderController : BaseController
    {
        private readonly VendorOrderManager _vendorOrderManager;
        private readonly VendorManager _vendorManager;
        private readonly PatientManager _patientManager;
        private readonly ConfigHelper _configHelper;
        public VendorOrderController(VendorOrderManager vendorOrderManager, VendorManager vendorManager,ConfigHelper configHelper) : base(configHelper)
        {
            _vendorOrderManager = vendorOrderManager;
            _vendorManager = vendorManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult Index(string id)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/VendorOrder/list");
            }
            if (id != null)
            {
                var productId = id;
                ViewBag.Id = productId;
            }
            if (User.AccountId() == "c503ca6e-f418-4807-a847-b6886378cf0b" || User.AccountId() == "74953f95-b9fc-43bf-bad0-49644bcbcc45" || User.InstanceId() == "d9e1f62d-e4f3-4dc1-95d2-0ecfbb62d1b6")
            {
                //For NagerCOil Pharmacy
                ViewBag.customOrder = 1;
            }
            else
            {
                ViewBag.customOrder = 0;
            }

            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Index([FromBody]List<VendorOrderVM> models)
        {
            models[0].InitializeExecutionQuery();
            /*Prefix added by Poongodi on 21/06/2017*/
            string sPrefix = "";
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
            && _configHelper.AppConfig.OfflineMode == false)
            {
                sPrefix = "O";

            }

            var vendorOrderList = models.Select(model => new VendorOrder
            {
                VendorId = model.VendorId,
                Prefix = sPrefix,
                ToInstanceId = model.selectedVendor.ToInstanceId,
                VendorOrderItem = model.VendorOrderItem.Select(item => new VendorOrderItem
                {
                    ProductId = item.ProductId,
                    Quantity = item.Quantity,
                    ProductMasterID = item.ProductMasterID,
                    ProductName = item.ProductName,
                    Manufacturer = item.Manufacturer,
                    Schedule = item.Schedule,
                    GenericName = item.GenericName,
                    Packing = item.Packing,
                    Category = item.Category,
                    VAT = (item.VAT > 0 ? item.VAT:0),
                    Igst = item.Igst,
                    Cgst = item.Cgst,
                    Sgst = item.Sgst,
                    GstTotal = item.GstTotal,
                    PurchasePrice = item.PurchasePrice,
                    PackageQty = item.PackageQty,
                    PackageSize = item.PackageSize,
                    SellingPrice = item.SellingPrice,
                    ExpireDate = item.ExpireDate,
                    ToInstanceId = item.ToInstanceId,
                    OrderItemSno = item.OrderItemSno,
                    FreeQty = item.FreeQty

                }.SetLoggedUserDetails(User) as VendorOrderItem).ToList()
            }.SetLoggedUserDetails(User) as VendorOrder).ToList();
            vendorOrderList.ForEach(x =>
            {
                x.SetExecutionQuery(models[0].GetExecutionQuery(), models[0].WriteExecutionQuery);
            });
            //TaxRefNo added by Manivannan on 30-Jun-2017
            if (User.GSTEnabled() == "True")
                vendorOrderList.ForEach(x => {
                    x.TaxRefNo = 1;
                });
            else
                vendorOrderList.ToList().ForEach(x => { x.TaxRefNo = 0; });

            var vendorOrder = (await _vendorOrderManager.Save(vendorOrderList)).ToList();
            if (models[0].WriteExecutionQuery)
            {
                (await _vendorOrderManager.SaveBulkOrder(vendorOrderList)).ToList();
                return View();
            }
            return View();
        }
        //add by nandhini
        [Route("[action]")]
        public async Task<OrderSettings> GetOrderSettings()
        {
            OrderSettings orderSettings = new OrderSettings();
            return await _vendorOrderManager.GetOrderSettings(User.AccountId(), User.InstanceId());
        }

        //Added by Sarubala on 24-11-18 - Start
        [Route("[action]")]
        public async Task<OrderSettings> GetHeadOfficeOrderSettings()
        {
            return await _vendorOrderManager.GetHeadOfficeOrderSettings(User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public async Task<List<Instance>> GetInstanceList()
        {
            return await _vendorOrderManager.GetInstanceList(User.AccountId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<OrderSettings> SaveHeadOfficeSettings([FromBody]OrderSettings data)
        {
            data.SetLoggedUserDetails(User);
            data.vendor.SetLoggedUserDetails(User);
            data.vendor.Code = "Vendor000";            
            string val = await _vendorManager.Save(data.vendor);
            return await _vendorOrderManager.saveHeadOfficeSettings(data);
        }

        [Route("[action]")]
        public async Task<string> GetHeadOfficeVendorId()
        {
            return await _vendorOrderManager.GetHeadOfficeVendorId(User.AccountId());
        }

        //Added by Sarubala on 24-11-18 - End

        [HttpPost]
        [Route("[action]")]
        public async Task<OrderSettings> SaveOrderSettings([FromBody]OrderSettings data)
        {
            data.SetLoggedUserDetails(User);
            return await _vendorOrderManager.SaveOrderSettings(data);
        }


        [HttpPost]
        [Route("[action]")]
        public async Task updateVendorOrderItem([FromBody]VendorOrder model)
        {
            await _vendorOrderManager.updateVendorOrderItem(model.VendorOrderItem.ToList());
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Index2([FromBody]List<VendorOrderVM> models)
        {
            /*Prefix added by Poongodi on 21/06/2017*/
            string sPrefix = "";
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
            && _configHelper.AppConfig.OfflineMode == false)
            {
                sPrefix = "O";

            }

            var vendorOrderList = models.Select(model => new VendorOrder
            {
                VendorId = model.VendorId,
                Prefix = sPrefix,
                VendorOrderItem = model.VendorOrderItem.Select(item => new VendorOrderItem
                {
                    ProductId = item.ProductId,
                    Quantity = item.Quantity,
                    ProductMasterID = item.ProductMasterID,
                    ProductName = item.ProductName,
                    Manufacturer = item.Manufacturer,
                    Schedule = item.Schedule,
                    GenericName = item.GenericName,
                    Packing = item.Packing,
                    Category = item.Category,
                    VAT = item.VAT,
                    PurchasePrice = item.PurchasePrice,
                    PackageQty = item.PackageQty,
                    PackageSize = item.PackageSize

                }.SetLoggedUserDetails(User) as VendorOrderItem).ToList()
            }.SetLoggedUserDetails(User) as VendorOrder);

            var vendorOrder = (await _vendorOrderManager.Save(vendorOrderList)).ToList();
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ProductStock> getQuantityForProduct(string productid)
        {
            return await _vendorOrderManager.getQuantityForProduct(productid, User.InstanceId());
        }

        [Route("[action]")]
        public IActionResult List()
        {
            if (User.AccountId() == "c503ca6e-f418-4807-a847-b6886378cf0b" || User.AccountId() == "74953f95-b9fc-43bf-bad0-49644bcbcc45" || User.InstanceId() == "d9e1f62d-e4f3-4dc1-95d2-0ecfbb62d1b6")
            {
                ViewBag.customOrder = 1;
            }
            else
            {
                ViewBag.customOrder = 0;
            }
            return View();
        }

        [Route("[action]")]
        public async Task<PagerContract<VendorOrder>> ListData([FromBody]VendorOrder model)
        {
            model.SetLoggedUserDetails(User);
            return await _vendorOrderManager.ListPager(model, User.InstanceId());
        }

        [Route("[action]")]
        public IActionResult PendingAuthorisationList()
        {
            return View();
        }


        [Route("[action]")]
        public IActionResult MissedOrderList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<MissedOrder>> MissedList([FromBody]MissedOrder model)
        {
            model.SetLoggedUserDetails(User);

            return await _vendorOrderManager.MissedOrderList(model);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<string> CancelMissedOrder(string id)
        {
            return await _vendorOrderManager.cancelMissedOrder(id);
        }

        private Task<IEnumerable<Patient>> PatientList(Patient data)
        {
            return _patientManager.PatientList(data, data.AccountId, data.InstanceId);
        }

        [Route("[action]")]
        public async Task<PagerContract<VendorOrder>> AuthorisationListData([FromBody]VendorOrder model)
        {
            //var model = new VendorOrder();
            model.SetLoggedUserDetails(User);
            return await _vendorOrderManager.GetVendorOrderList(model);
        }

        [Route("[action]")]
        public async Task<VendorOrder> SendAuthoriseOrder([FromBody]VendorOrder model)
        {
            model.SetLoggedUserDetails(User);
            return await _vendorOrderManager.SendAuthoriseOrderData(model);
        }
        [Route("[action]")]
        public async Task<IEnumerable<VendorOrderItem>> getPreviousOrders(string productid)
        {
            return await _vendorOrderManager.getPreviousOrders(productid, User.InstanceId());
        }

        [Route("[action]")]
        public IActionResult OrderBasedSales()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<OrderBasedSales>> GetOrderBasedSales(int daysCount)
        {
            OrderBasedSales obs = new OrderBasedSales();
            obs.SetLoggedUserDetails(User);
            obs.PreviousDays = daysCount;
            return await _vendorOrderManager.GetOrderBasedSales(obs);
        }
        [Route("[action]")]
        public Task<List<Vendor>> getVendors(string vendorname)
        {
            return _vendorOrderManager.getVendors(User.AccountId(), User.InstanceId(), vendorname);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<VendorOrderItem>> getVendorOrders(string id, bool PO, string toInstance = "")
        {
            return await _vendorOrderManager.getVendorOrders(id, PO, User.InstanceId(), User.AccountId(), toInstance);
        }



        [Route("[action]")]
        public IActionResult Update(string vendorOrderId)
        {
            if (vendorOrderId != null)
            {
                ViewBag.vendorOrderId = vendorOrderId;
            }
            return View();
        }
        //add by nandhini 24.4.17
        [Route("[action]")]
        public IActionResult Settings()
        {
            return View();
        }



        [Route("[action]")]
        public async Task<VendorOrder> getVenodrOrderById(string vendororderid)
        {
            return await _vendorOrderManager.GetVenodrOrderById(vendororderid, User.AccountId(), User.InstanceId());
        }



        [HttpPost]
        [Route("[action]")]
        public async Task<bool> resendOrderSms([FromBody]VendorOrder model)
        {


            return await _vendorOrderManager.resendOrderSms(model, User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<bool> resendOrderMail([FromBody]VendorOrder model)
        {
            return await _vendorOrderManager.resendOrderMail(model, User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> getProductDetails([FromBody]string[] prodsId, string fromDate, string toDate, int type)
        {
            return await _vendorOrderManager.getProductDetails(User.AccountId(), User.InstanceId(), prodsId, fromDate, toDate, type);
        }

        //To get head office Id for stock transfer offline sync by Settu
        [Route("[action]")]
        public async Task<Instance> getHeadOfficeVendor()
        {
            return await _vendorOrderManager.getHeadOfficeVendor(User.AccountId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> ReorderByMinMax()
        {
            return await _vendorOrderManager.ReorderByMinMax(User.AccountId(), User.InstanceId());
        }

        //Added by Sarubala on 20-11-17
        [Route("[action]")]
        public async Task<bool> GetIsOrderCreateSms()
        {
            return await _vendorOrderManager.GetIsOrderCreateSms(User.InstanceId());
        }
    }
}
