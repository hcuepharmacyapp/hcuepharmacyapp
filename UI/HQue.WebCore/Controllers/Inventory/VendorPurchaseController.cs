﻿using HQue.Biz.Core.Inventory;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Inventory;
using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Utilities.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.Contract.Infrastructure.Master;
using System.Linq;

namespace HQue.WebCore.Controllers.Inventory
{
    [Route("[controller]")]
    public class VendorPurchaseController : BaseController
    {
        private readonly IHostingEnvironment _environment;
        private readonly VendorPurchaseManager _vendorPurchaseManager;
        private readonly ConfigHelper _configHelper;
        private readonly VendorPurchaseReturnManager _vendorPurchaseReturnManager;
        public VendorPurchaseController(VendorPurchaseManager vendorPurchaseManager, VendorPurchaseReturnManager vendorPurchaseReturnManager, IHostingEnvironment environment, ConfigHelper configHelper) : base(configHelper)
        {
            _environment = environment;
            _vendorPurchaseManager = vendorPurchaseManager;
            _vendorPurchaseReturnManager = vendorPurchaseReturnManager;
            _configHelper = configHelper;
        }
        [Route("[action]")]
        public async Task<IActionResult> Index(string id)
        {
            if (id != null)
            {
                ViewBag.Id = id;
            }            
            var List = await _vendorPurchaseManager.getBuyInvoiceDateEditSetting(User.AccountId(), User.InstanceId());
            if (List != null)
            {
                ViewBag.BuyInvoiceDateEdit = List.BuyInvoiceDateEdit;
            }
            if(ViewBag.BuyInvoiceDateEdit == null)
            {
                ViewBag.BuyInvoiceDateEdit = 0;
            }
            ViewBag.addNewProductAccess = User.HasPermission(UserAccess.AddNewProduct);
            ViewBag.UserId = User.UserId();
            if (User.AccountId() == "c503ca6e-f418-4807-a847-b6886378cf0b" || User.AccountId() == "74953f95-b9fc-43bf-bad0-49644bcbcc45" || User.InstanceId() == "d9e1f62d-e4f3-4dc1-95d2-0ecfbb62d1b6")
                ViewBag.customOrder = 1;
            else
                ViewBag.customOrder = 0;
            return View();
        }
        [Route("[action]")]
        public IActionResult Index2(string id)
        {
            if (id != null)
            {
                ViewBag.Id = id;
            }
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<object> Index([FromBody]VendorPurchase model)
        {
            try
            {
                model.InitializeExecutionQuery();
                model.SetLoggedUserDetails(User);
                model.Instance.Name = User.InstanceName();
                //Prefix Added by Poongodi on 14/06/2017
                if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
                     && _configHelper.AppConfig.OfflineMode == false)
                    model.Prefix = "O";

                if (User.GSTEnabled() == "True")
                {
                    model.TaxRefNo = 1;
                }
                else
                {
                    model.TaxRefNo = 0;
                }

                //Added by Sarubala on 24-05-2018 to avoid global product duplication
                int globalProductCount = model.VendorPurchaseItem.Where(item => !string.IsNullOrEmpty(item.ProductStock.Product.ProductMasterID)).GroupBy(p => p.ProductStock.ProductId).Where(g => (g.Count() > 1)).Count();
                if(globalProductCount > 0)
                {
                    model.WriteExecutionQuery = false;
                }

                // Added Purchasewith Return Gavaskar 10-07-2017
                model = _vendorPurchaseManager.RemoveReturnItems(model);
                model.VendorReturn.SetLoggedUserDetails(User);

                VendorPurchase result = new VendorPurchase();

                result = await _vendorPurchaseManager.Save(model);
                if (model.WriteExecutionQuery)
                {
                    await _vendorPurchaseManager.SaveBulkData(model);
                    if (model.SaveFailed)
                    {
                        return model;
                    }
                }
                model.VendorReturn.VendorPurchaseId = result.Id;

                if (model.VendorReturn.VendorPurchaseReturnItem.Count > 0)
                {

                    if (model.VendorReturn.VendorPurchaseId != null)
                    {
                        model.VendorReturn.VendorId = model.VendorId;
                        model.VendorReturn.TaxRefNo = User.GSTEnabled() == "True" ? 1 : 0;
                        if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true" && _configHelper.AppConfig.OfflineMode == false)
                            model.VendorReturn.Prefix = "O";
                        result.VendorReturn = await _vendorPurchaseReturnManager.savePurchaseReturn(model.VendorReturn);
                    }

                }

                if (!string.IsNullOrEmpty(model.DraftName))
                {
                    await _vendorPurchaseManager.DeleteDraft(model.draftVendorPurchaseId, User.InstanceId());
                }
            }
            catch (Exception e)
            {
                HttpContext.Response.Clear();
                HttpContext.Response.StatusCode = 500;
                return Json(new { success = false, errorDesc = e.Message });
            }
            return model;
        }

        //Check Invoice Number already exist or not start on 11Aug2017
        [HttpPost]
        [Route("[action]")]
        public async Task<dynamic> validateInvoice([FromBody]VendorPurchase model)
        {
            try
            {
                model.SetLoggedUserDetails(User);
                await _vendorPurchaseManager.ValidateInvoice(model);
                return true;
            }
            catch (Exception e)
            {
                HttpContext.Response.Clear();
                HttpContext.Response.StatusCode = 500;
                return Json(new { success = false, errorDesc = e.Message });
            }
        }
        //End 11Aug2017

        //[HttpPost]
        //[Route("[action]")]
        //public async Task<DraftVendorPurchase> SaveDraft([FromBody]DraftVendorPurchase model)
        //{
        //    model.SetLoggedUserDetails(User);
        //    model.DraftAddedOn = CustomDateTime.IST;
        //    return await _vendorPurchaseManager.SaveDraft(model);
        //}
        [HttpPost]
        [Route("[action]")]
        public async Task<DraftVendorPurchase> SaveDraft([FromBody]DraftVendorPurchase model)
        {
            try
            {
                model.SetLoggedUserDetails(User);
                model.DraftAddedOn = CustomDateTime.IST;
                return await _vendorPurchaseManager.SaveDraft(model);
            }
            catch (Exception ex)
            {
                DraftVendorPurchase obj = new DraftVendorPurchase();
                obj.errorMessage = ex.StackTrace;
                return obj;
            }
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<DraftVendorPurchase> UpdateDraft([FromBody]DraftVendorPurchase model)
        {
            try
            {
                model.SetLoggedUserDetails(User);
                await _vendorPurchaseManager.DeleteDraft(model.Id, User.InstanceId());
                model.DraftAddedOn = CustomDateTime.IST;
                return await _vendorPurchaseManager.SaveDraft(model);
            }
            catch (Exception ex)
            {
                DraftVendorPurchase obj = new DraftVendorPurchase();
                obj.errorMessage = ex.StackTrace;
                return obj;
            }
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<int> DeleteDraft(string draftVendorPurchaseId)
        {
            return await _vendorPurchaseManager.DeleteDraft(draftVendorPurchaseId, User.InstanceId());
        }
        [Route("[action]")]
        public async Task<List<DraftVendorPurchase>> GetAllDrafts()
        {
            return await _vendorPurchaseManager.GetAllDrafts(User.InstanceId());
        }
        [Route("[action]")]
        public async Task<List<DraftVendorPurchase>> GetDraftsByInstance()
        {
            return await _vendorPurchaseManager.GetDraftsByInstance(User.InstanceId());
        }
        [Route("[action]")]
        public async Task<Product> getProductGST(string productId)
        {
            return await _vendorPurchaseManager.GetProductGST(User.AccountId(), User.InstanceId(), productId);
        }
        [Route("[action]")]
        public async Task<List<DraftVendorPurchaseItem>> DraftItemsByDraftId(string draftId)
        {
            return await _vendorPurchaseManager.DraftItemsByDraftId(User.InstanceId(), draftId);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<DcVendorPurchaseItem> SaveDcVendorPurchaseItem([FromBody]DcVendorPurchaseItem model)
        {
            try
            {
                model.SetLoggedUserDetails(User);
                model.ProductStock.SetLoggedUserDetails(User);
                model.ProductStock.Product.SetLoggedUserDetails(User);
                if(User.GSTEnabled() == "True")  //Added by Sarubala on 14-07-17 to include TaxRefNo
                {
                    model.TaxRefNo = 1;
                }
                await _vendorPurchaseManager.SaveDcVendorPurchaseItem(model);
                return model;
            }
            catch
            {
                HttpContext.Response.Clear();
                HttpContext.Response.StatusCode = 500;
                return new DcVendorPurchaseItem();
            }
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<List<DcVendorPurchaseItem>> getDCItems(string id)
        {
            return await _vendorPurchaseManager.getDCItems(id, User.AccountId(), User.InstanceId());
        }
        //added by anndhini on 13/4/18
        [HttpGet]
        [Route("[action]")]
        public async Task<List<DcVendorPurchaseItem>> getDCItemsDropDown(string id,string dcNo)
        {
            return await _vendorPurchaseManager.getDCItemsDropDown(id, dcNo, User.AccountId(), User.InstanceId());
        }
        //POCancel method added by Poongodi on 06/03/2017
        [HttpGet]
        [Route("[action]")]
        public async Task<int> POCancel(string id)
        {
            return await _vendorPurchaseManager.POCancel(User.AccountId(), User.InstanceId(), id, User.Identity.Id());
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<string> removeDcItem([FromBody]DcVendorPurchaseItem item1)
        {
            return await _vendorPurchaseManager.removeDcItem(item1);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<DcVendorPurchaseItem> updateDcItem([FromBody]DcVendorPurchaseItem item)
        {
            item.SetLoggedUserDetails(User);
            item.ProductStock.Product.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.updateDcItem(item);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<TempVendorPurchaseItem>> SaveTempVendorPurchaseItem([FromBody]List<TempVendorPurchaseItem> data)
        {
            try
            {
                foreach(var model in data) {
                    model.SetLoggedUserDetails(User);
                    model.ProductStock.SetLoggedUserDetails(User);
                    model.ProductStock.Product.SetLoggedUserDetails(User);
                    //model.VendorPurchaseId = "temp";
                    model.FreeQty = 0;
                    model.Discount = 0;
                    model.ProductStock.CST = 0;
                    if (User.GSTEnabled() == "True")  //Added by Sarubala on 14-07-17 to include TaxRefNo
                    {
                        model.TaxRefNo = 1;
                    }
                }
                await _vendorPurchaseManager.SaveVendorPurchaseItem(data);
                return data;
            }
            catch (Exception e)
            {
                HttpContext.Response.Clear();
                HttpContext.Response.StatusCode = 500;
                //return Json(new { success = false, errorDesc = e.Message });
                return new List<TempVendorPurchaseItem>();
            }
        }
        //[HttpPost]
        //[Route("[action]")]
        //public async Task<IActionResult> Index([FromForm]VendorPurchase model, IFormFile file)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(Convert.ToString(file)))
        //        {
        //            var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
        //            var fileName = Guid.NewGuid().ToString() + '_' + parsedContentDisposition.FileName.Trim('"');
        //            var filePath = Path.Combine(_environment.WebRootPath, "uploads\\purchase", fileName);
        //            using (var fileStream = new FileStream(filePath, FileMode.Create))
        //            {
        //                await file.CopyToAsync(fileStream);
        //            }
        //            if (!string.IsNullOrEmpty(fileName))
        //            {
        //                model.FileName = fileName;
        //            }
        //        }
        //        model.SetLoggedUserDetails(User);
        //        await _vendorPurchaseManager.Save(model);
        //    }
        //    catch (Exception e)
        //    {
        //        HttpContext.Response.Clear();
        //        HttpContext.Response.StatusCode = 500;
        //        return Json(new { success = false, errorDesc = e.Message });
        //    }
        //    return View();
        //}
        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult Settings()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<VendorPurchase>> ListData([FromBody]VendorPurchase model)
        {
            model.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.ListPager(model, User.InstanceId(), User.AccountId());
        }

        //added by nandhini for purchase history vendor anme search
        [HttpPost]
        [Route("[action]")]
        public Task<IEnumerable<Vendor>> GetPurchaseVendorName(string vendorName)
        {
            return _vendorPurchaseManager.VendorSearchPurchaseListLocal(vendorName, User.AccountId(), User.InstanceId());
        }
        //end

        //PO Cancel Section Added by Poongodi on 07/03/2017
        [Route("[action]")]
        public IActionResult CancelList()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<VendorPurchase>> CancelListData([FromBody]VendorPurchase model)
        {
            model.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.CancelListPager(model, User.InstanceId(), User.AccountId());
        }
        //End here
        [HttpPost]
        [Route("[action]")]
        public async Task<VendorPurchase> PopulateVendorPurchaseData(string id)
        {
            return await _vendorPurchaseManager.GetVendorPurchaseData(id);
        }
        [Route("[action]")]
        public bool IsTempPurchaseItemAvail(string productId)
        {
            var user = User.Identity;
            var instanceId = user.InstanceId();
            bool bReturn = _vendorPurchaseManager.IsTempPurchaseItemAvail(productId, instanceId).Result;
            return bReturn;
        }
        [Route("[action]")]
        public async Task<List<TempVendorPurchaseItem>> loadTempVendorPurchaseItem(string productId)
        {
            var user = User.Identity;
            var instanceId = user.InstanceId();
            var vendorPurchaseItems = await _vendorPurchaseManager.loadTempVendorPurchaseItem(productId, instanceId);
            return vendorPurchaseItems;
        }
        [Route("[action]")]
        public async Task<IActionResult> Update(string id)
        {
            if (id != null)
            {
                ViewBag.Id = id;
            }
            var List = await _vendorPurchaseManager.getBuyInvoiceDateEditSetting(User.AccountId(), User.InstanceId());
            if (List != null)
            {
                ViewBag.BuyInvoiceDateEdit = List.BuyInvoiceDateEdit;
            }
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<VendorPurchase> EditVendorPurchaseData(string id)
        {
            return await _vendorPurchaseManager.EditVendorPurchase(id);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<VendorPurchase> UpdateVendorPurchase([FromBody]VendorPurchase model)
        {
            model.EditMode = true;
            model.InitializeExecutionQuery();
            model.Instance.Name = User.InstanceName();
            var ReturnId = model.VendorReturn.Id; // Added by Gavaskar 05-09-2017

            model.SetLoggedUserDetails(User);
            try
            {
                // Added by Gavaskar Purchase With Return
                model = _vendorPurchaseManager.RemoveReturnItems(model);
                VendorPurchase result = new VendorPurchase();
                result = await _vendorPurchaseManager.UpdateVendorPurchaseData(model, User.InstanceId(), User.AccountId());
                if(model.WriteExecutionQuery)
                {
                    await _vendorPurchaseManager.SaveBulkData(model);
                    if (model.SaveFailed)
                    {
                        return model;
                    }
                }
                model.VendorReturn.VendorPurchaseId = result.Id;
                await _vendorPurchaseManager.HandleReturnedItems(model.VendorReturn, result.Id);
               
                if (model.VendorReturn.VendorPurchaseReturnItem.Count > 0)
                {
                    model.VendorReturn.SetLoggedUserDetails(User);
                    
                   
                    if (model.VendorReturn.VendorPurchaseId != null)
                    {
                        model.VendorReturn.VendorId = model.VendorId;
                        model.VendorReturn.TaxRefNo = User.GSTEnabled() == "True" ? 1 : 0;
                        if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true" && _configHelper.AppConfig.OfflineMode == false)
                            model.VendorReturn.Prefix = "O";
                        result.VendorReturn = await _vendorPurchaseReturnManager.savePurchaseReturn(model.VendorReturn);
                    }
                }
               
                return model;
            }
            catch (Exception e)
            {
                VendorPurchase obj = new VendorPurchase();
                obj.errorMessage = e.Message.ToString();
                return obj;
            }  
        }
        [Route("[action]")]
        public async Task<PurchaseSettings> SaveTaxType(int TaxType)
        {
            PurchaseSettings PurchaseSettings = new PurchaseSettings();
            PurchaseSettings.SetLoggedUserDetails(User);
            PurchaseSettings.TaxType = TaxType;
            var Inv = await _vendorPurchaseManager.SaveTaxType(PurchaseSettings);
            return Inv;
        }
        // Added Gavaskar 07-02-2017 Start 
        [HttpPost]
        [Route("[action]")]
        public async Task<PurchaseSettings> saveBuyInvoiceDateEditOption(int InvoiceDateEditOption)
        {
            PurchaseSettings PurchaseSettings = new PurchaseSettings();
            PurchaseSettings.SetLoggedUserDetails(User);
            PurchaseSettings.BuyInvoiceDateEdit = InvoiceDateEditOption;
            return await _vendorPurchaseManager.SaveBuyInvoiceDateEditOption(PurchaseSettings);
        }
        [Route("[action]")]
        public async Task<PurchaseSettings> getBuyInvoiceDateEditSetting()
        {
            return await _vendorPurchaseManager.getBuyInvoiceDateEditSetting(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<PurchaseSettings> getBillSeriesType()
        {
            return await _vendorPurchaseManager.getBillSeriesType(User.AccountId(), User.InstanceId());
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<PurchaseSettings> saveBillSeriesType([FromBody]PurchaseSettings ps)
        {
            ps.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.saveBillSeriesType(ps);
        }
        // Added Gavaskar 07-02-2017 End 
        [Route("[action]")]
        public async Task<PurchaseSettings> getTaxtype()
        {
            return await _vendorPurchaseManager.getTaxtypedata(User.AccountId(), User.InstanceId());
        }


        //Added by Sarubala on 11/05/18 - start
        [Route("[action]")]
        public async Task<PurchaseSettings> getInvoiceValueSettings()
        {
            return await _vendorPurchaseManager.getInvoiceValueSettings(User.AccountId(), User.InstanceId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PurchaseSettings> saveInvoiceValueSettings([FromBody]PurchaseSettings ps)
        {
            ps.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.saveInvoiceValueSettings(ps);
        }

        //Added by Sarubala on 11/05/18 - end

        [Route("[action]")]
        public async Task<TaxSeriesItem> saveTaxTypeseriesItem(string TaxTypeSeries)
        {
            var TS = new TaxSeriesItem();
            TS.ActiveStatus = "Y";
            TS.TaxSeriesName = TaxTypeSeries;
            TS.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.saveTaxTypeseriesItem(TS);
        }
        [Route("[action]")]
        public async Task<List<TaxSeriesItem>> getCustomTaxSeriesItems()
        {
            TaxSeriesItem IS = new TaxSeriesItem();
            IS.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.getCustomTaxSeriesItems(IS);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<ProductSearchSettings> SaveSearchType(string searchType)
        {
            var type1 = new ProductSearchSettings();
            type1.SetLoggedUserDetails(User);
            type1.SearchType = searchType;
            return await _vendorPurchaseManager.saveSearchType(type1);
        }
        [Route("[action]")]
        public async Task<string> getSearchType()
        {
            return await _vendorPurchaseManager.getSearchType(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<bool> getAllowDecimal()
        {
            return await _vendorPurchaseManager.GetAllowDecimalSetting(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public async Task<string> getAllowCompletePurchaseKeyType() 
        {
            return await _vendorPurchaseManager.GetAllowCompletePurchaseKeyTypeSetting(User.AccountId(), User.InstanceId()); 
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<PurchaseSettings> saveAllowDecimalSetting([FromBody]bool val1)
        {
            PurchaseSettings ps = new PurchaseSettings();
            ps.SetLoggedUserDetails(User);
            ps.IsAllowDecimal = val1;
            return await _vendorPurchaseManager.SaveAllowDecimalSetting(ps);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<PurchaseSettings> saveAllowCompletePurchaseKeyType([FromBody]string val1)
        {
            PurchaseSettings ps = new PurchaseSettings();
            ps.SetLoggedUserDetails(User);
            ps.CompletePurchaseKeyType = val1;
            return await _vendorPurchaseManager.SaveAllowCompletePurchaseKeyType(ps);
        }

        //Added by Sarubala on 26-09-17
        [HttpPost]
        [Route("[action]")]
        public async Task<PurchaseSettings> saveisSortByLowestPriceSetting([FromBody]bool val1)
        {
            PurchaseSettings ps = new PurchaseSettings();
            ps.SetLoggedUserDetails(User);
            ps.IsSortByLowestPrice = val1;
            return await _vendorPurchaseManager.SaveisSortByLowestPriceSetting(ps);
        }

        //[HttpPost]
        //[Route("[action]")]
        //public async Task<VendorPurchase> UpdateVendorPurchase([FromForm]VendorPurchase model, IFormFile file)
        //{
        //    if (!string.IsNullOrEmpty(Convert.ToString(file)))
        //    {
        //        var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
        //        var fileName = Guid.NewGuid().ToString() + '_' + parsedContentDisposition.FileName.Trim('"');
        //        var filePath = Path.Combine(_environment.WebRootPath, "uploads\\purchase", fileName);
        //        //await file.SaveAsAsync(filePath);
        //        using (var fileStream = new FileStream(filePath, FileMode.Create))
        //        {
        //            await file.CopyToAsync(fileStream);
        //        }
        //        if (!string.IsNullOrEmpty(fileName))
        //        {
        //            model.FileName = fileName;
        //        }
        //    }
        //    model.SetLoggedUserDetails(User);
        //    await _vendorPurchaseManager.UpdateVendorPurchaseData(model);
        //    return model;
        //}
        [HttpGet]
        [Route("[action]")]
        public async Task<VendorPurchase> GetGRNO()
        {
            VendorPurchase v = new VendorPurchase();
            v.SetLoggedUserDetails(User);
            //Prefix added By Poongodi on 10/10/2017
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true" && _configHelper.AppConfig.OfflineMode == false)
                v.Prefix = "O";
            return await _vendorPurchaseManager.getGRNumber(v);
        }
        [Route("[action]")]
        public FileResult Download(string fileName)
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads\\purchase", fileName);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "image/jpeg");
        }
        [Route("[action]")]
        public async Task<PurchaseDetails> getPurchaseDetails(string id, string name)
        {
            ProductStock ps = new ProductStock();
            ps.ProductId = id;
            ps.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.getPurchaseDetails(ps, name, User.InstanceId());
        }
        [Route("[action]")]
        public async Task<VendorPurchaseItem> getPurchasePrice(string id)
        {
            return await _vendorPurchaseManager.getPurchasePrice(id);
        }
        /// <summary>
        /// Get offline status from sales manages
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("[action]")]
        public async Task<Boolean> getOfflineStatus()
        {
            return await _vendorPurchaseManager.getOfflineStatus(User.AccountId(), User.InstanceId());
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<bool> inActiveProduct([FromBody] VendorPurchaseItem model)
        {
            return await _vendorPurchaseManager.inActiveProduct(model, User.InstanceId(), User.AccountId());
        }
        [Route("[action]")]
        public async Task<bool> getEnableSelling()
        {
            return await _vendorPurchaseManager.getEnableSelling(User.AccountId(), User.InstanceId());
        }
        [HttpPost]
        [Route("[action]")]
        public async Task saveEnableSelling([FromBody]bool enableSelling)
        {
            PurchaseSettings ps = new PurchaseSettings();
            ps.SetLoggedUserDetails(User);
            ps.EnableSelling = enableSelling;
            await _vendorPurchaseManager.saveEnableSelling(ps);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<bool> checkPaymentstatus(string id)
        {
            return await _vendorPurchaseManager.checkPaymentstatus(id);
        }

        [Route("[action]")]
        public async Task<IEnumerable<TaxValues>> GetTaxValues()
        {
            //TaxValues modal = new TaxValues();
            //modal.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.GetTaxValues(User.AccountId());
        }
        [Route("[action]")]
        public async Task<IEnumerable<TaxValues>> GetAllTaxValues()
        {            
            return await _vendorPurchaseManager.GetTaxValues(User.AccountId(),"all");
        }
        //added by nandhini 30.11.17
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> saveTaxSeries([FromBody]TaxValues TaxValues)
        {
           
            try
            {
                TaxValues.SetLoggedUserDetails(User);
                await _vendorPurchaseManager.saveTaxSeries(TaxValues);
            }
            catch (Exception e)
            {
                HttpContext.Response.Clear();
                HttpContext.Response.StatusCode = 500;
                return Json(new { success = false, errorDesc = e.Message });
            }
            return View("Settings");
        }
        [Route("[action]")]
        public async Task<TaxValues> UpdateTaxValuesStatus([FromBody]TaxValues TaxValues)
        {
            TaxValues.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.UpdateTaxValuesStatus(TaxValues);
        }
        
        [Route("[action]")]
        public async Task<bool> GetBarcodeGenerationIsEnabled()
        {
            return await _vendorPurchaseManager.GetFeatureAccess(1);
        }

        [Route("[action]")]
        public async Task<bool> DeleteTempVendorPurchaseItem([FromBody]TempVendorPurchaseItem model)
        {          
            model.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.DeleteTempVendorPurchaseItem(model);            
        }
    }
}
