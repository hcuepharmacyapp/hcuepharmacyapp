﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Biz.Core.Inventory;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Inventory;
using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;
using System;
using System.IO;
using Utilities.Report;
using System.Text;
using HQue.WebCore.ViewModel;
using System.Linq;

namespace HQue.WebCore.Controllers.Inventory
{
    [Route("[controller]")]
    public class VendorPurchaseReturnController : BaseController
    {
        private readonly VendorPurchaseReturnManager _vendorPurchaseReturnManager;
        private readonly ConfigHelper _configHelper;
        public VendorPurchaseReturnController(VendorPurchaseReturnManager vendorPurchaseReturnManager, ConfigHelper configHelper) : base(configHelper)
        {
            _vendorPurchaseReturnManager = vendorPurchaseReturnManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult Index(string id)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/VendorPurchase/list");
            }
            ViewBag.Id = id;
            return View();
        }

        [Route("[action]")]
        public async Task<VendorPurchaseReturn> VendorPurchaseReturnDetail(string id)
        {
            return await _vendorPurchaseReturnManager.GetVendorPurchaseReturnDetails(id);
        }

        //Added by Violet to implement new purchase Return
        [HttpGet]
        [Route("[action]")]
        //public async Task<List<ProductStock>> GetPurchaseReturnItem(string productName) //, string VendorId
        public async Task<List<VendorPurchaseItem>> GetPurchaseReturnItem(string productName) //, string VendorId
        {
            return await _vendorPurchaseReturnManager.GetPurchaseReturnItem(productName, User.InstanceId()); //, VendorId
        }
        
        [HttpPost]
        [Route("[action]")]
        public async Task<bool> Index([FromBody]VendorPurchaseReturn model)
        {
            model.SetLoggedUserDetails(User);
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
                && _configHelper.AppConfig.OfflineMode == false)
                model.Prefix = "O";
            await _vendorPurchaseReturnManager.Save(model);

            return true;
        }

        //Added by Lawrence
        [HttpPost]
        [Route("[action]")]
        public async Task<bool> List([FromBody]VendorPurchaseReturn vendorPurchaseReturn)
        {
            vendorPurchaseReturn.SetLoggedUserDetails(User);
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
                && _configHelper.AppConfig.OfflineMode == false)
                vendorPurchaseReturn.Prefix = "O";
            if (Convert.ToString(User.GSTEnabled()).ToLower() == "true")
            {
                vendorPurchaseReturn.TaxRefNo = 1;
            }
            else
            {
                vendorPurchaseReturn.TaxRefNo = 0;
            }
            await _vendorPurchaseReturnManager.saveReturn(vendorPurchaseReturn);

            return true;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<bool> ReturnIndex([FromBody]VendorPurchaseReturn model)
        {
            model.SetLoggedUserDetails(User);
            /*Prefix Added by Poongodi on 14/06/2017*/
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
                 && _configHelper.AppConfig.OfflineMode == false)
                model.Prefix = "O";
            if (Convert.ToString(User.GSTEnabled()).ToLower() == "true")
            {
                model.TaxRefNo = 1;
            }
            await _vendorPurchaseReturnManager.SaveBulkReturn(model);

            return true;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<VendorPurchaseReturn>> ListData([FromBody]VendorPurchaseReturn model, bool IsInvoiceDate = false, bool isFromReport = false)
        {
            string instanceId = model.InstanceId;
            model.SetLoggedUserDetails(User);
            if (instanceId == "undefined")
                model.InstanceId = null;
            if (!User.IsAdmin())
                model.InstanceId = User.InstanceId();
            return await _vendorPurchaseReturnManager.ListPager(model, IsInvoiceDate, isFromReport);
        }

        [Route("[action]")]
        public IActionResult ReturnIndex()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/NonMovingStockReport/list");
            }
            return View();
        }

        //Newly Added Gavaskar 25-10-2016 Start
        [Route("[action]")]
        public IActionResult ExpireReturnIndex()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/ExpireProductReport/list");
            }
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<bool> ExpireReturnIndex([FromBody]VendorPurchaseReturn model)
        {
            model.SetLoggedUserDetails(User);
            /*Prefix Added by Poongodi on 14/06/2017*/
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
                 && _configHelper.AppConfig.OfflineMode == false)
                model.Prefix = "O";
            if(User.GSTEnabled() == "True")
            {
                model.TaxRefNo = 1;
            }

            await _vendorPurchaseReturnManager.SaveBulkReturn(model);

            return true;
        }
        //Newly Added Gavaskar 25-10-2016 End

        [Route("[action]")]
        public async Task<VendorPurchaseReturn> getBulkReturnExpire()
        {
            VendorPurchaseReturn vpr = new VendorPurchaseReturn();
            vpr.SetLoggedUserDetails(User);            
            var vendorPurchaseReturn = await _vendorPurchaseReturnManager.GetExpiremovingStocks(vpr.InstanceId);
            vendorPurchaseReturn.GSTEnabled = User.GSTEnabled() == "True";
            return vendorPurchaseReturn;
        }

        [Route("[action]")]
        public async Task<VendorPurchaseReturn> getBulkReturn()
        {
            VendorPurchaseReturn vpr = new VendorPurchaseReturn();
            vpr.SetLoggedUserDetails(User);
            var vendorPurchaseReturn = await _vendorPurchaseReturnManager.GetNonmovingStocks(vpr.InstanceId);
            vendorPurchaseReturn.GSTEnabled = User.GSTEnabled() == "True";
            return vendorPurchaseReturn;
        }

        // Added by Settu to implement payment status & remarks
        [HttpPost]
        [Route("[action]")]
        public async Task<bool> updateVendorPurchaseReturn([FromBody]VendorPurchaseReturn model)
        {
            model.SetLoggedUserDetails(User);
            return await _vendorPurchaseReturnManager.updateVendorPurchaseReturn(model);
        }

        [Route("[action]")]
        public async Task<ActionResult> printdmA4(string id)
        {
            var vendorPurchaseReturn = new VendorPurchaseReturn();
            vendorPurchaseReturn.Page.PageNo = 1;
            vendorPurchaseReturn.Page.PageSize = 1;
            vendorPurchaseReturn.Id = id;
            var list = await _vendorPurchaseReturnManager.ListPager(vendorPurchaseReturn, false, false);
            var data = list.List.Where(x => x.Id == id).FirstOrDefault();

            string name = string.Format("./wwwroot/temp/purchasereturn_{0}.txt", data.VendorPurchase.Instance.Id);

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(GeneratePurchaseReport_A4(data), false, Encoding.UTF8);

            }

            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("hq_{0}.txt", data.ReturnNo));
        }


        #region A4_Generic_template
        private string GeneratePurchaseReport_A4(VendorPurchaseReturn model)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 80;
            rpt.Rows = 40;

            WriteReportHeader_A4(rpt, model);
            WriteReportItems_A4(rpt, model);
            WriteReportFooter_A4(rpt, model);

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A4(PlainTextReport rpt, VendorPurchaseReturn model)
        {
            rpt.NewLine();
            rpt.Write(model.VendorPurchase.Instance.Name, 30, 22, Alignment.Center);
            rpt.NewLine();
            rpt.Write(model.VendorPurchase.Instance.FullAddress, 30, 16, Alignment.Center);
            rpt.NewLine();
            if (model.TaxRefNo != null && model.TaxRefNo == 1)
            {
                rpt.Write("DL No: " + model.VendorPurchase.Instance.DrugLicenseNo + " / " + "GSTIN: " + model.VendorPurchase.Instance.GsTinNo, 50, 16, Alignment.Center);
            }
            else
                rpt.Write("DL No: " + model.VendorPurchase.Instance.DrugLicenseNo + " / " + "TIN: " + model.VendorPurchase.Instance.TinNo, 50, 16, Alignment.Center);

            rpt.Write();
            rpt.NewLine();
            rpt.Write("Purchase Return", 30, 22, Alignment.Center);

            rpt.Drawline();
            rpt.NewLine();
            rpt.Drawline();
            rpt.Write(model.VendorPurchase.Vendor.Name + "," + model.VendorPurchase.Vendor.Address + ", " + model.VendorPurchase.Vendor.Area + "," + model.VendorPurchase.Vendor.City + "-" + model.VendorPurchase.Vendor.Pincode, 50);
            rpt.NewLine();
            rpt.Write("DL :" + model.VendorPurchase.Vendor.DrugLicenseNo, 50);
            rpt.NewLine();
            if (model.TaxRefNo != null && model.TaxRefNo == 1)
            {
                rpt.Write("GSTIN:" + model.VendorPurchase.Vendor.GsTinNo, 50);
            }
            else
            {
                rpt.Write("TIN:" + model.VendorPurchase.Vendor.TinNo, 50);
            }
            rpt.Write("", 18, 50);

            rpt.Write();

            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Return No : " + model.ReturnNo, 30, 1);
            rpt.Write("Return Date: " + string.Format("{0:dd/MM/yy}", model.ReturnDate), 30, 50);

            rpt.Write();
            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Particulars", 23);
            rpt.Write("Batch", 8, 24);
            rpt.Write("Ex.Dt", 5, 33);
            rpt.Write("Qty", 5, 39, Alignment.Right);
            rpt.Write("F.Q", 3, 46, Alignment.Right);
            rpt.Write("Disc", 4, 51, Alignment.Right);
            rpt.Write("Price", 7, 56, Alignment.Right);
            if (model.TaxRefNo != null && model.TaxRefNo == 1)
            {
                rpt.Write("GST%", 5, 64, Alignment.Right);
            }
            else
            {
                rpt.Write("VAT%", 5, 64, Alignment.Right);
            }
            rpt.Write("Amount", 8, 70, Alignment.Right);
            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportItems_A4(PlainTextReport rpt, VendorPurchaseReturn model)
        {
            int len = 0;
            foreach (var items in model.VendorPurchaseReturnItem)
            {
                rpt.NewLine();
                rpt.Write(items.ProductStock.Product.Name, 23);
                rpt.Write(items.ProductStock.BatchNo, 8, 24);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, 33);
                rpt.Write(string.Format("{0:0}", items.Quantity), 5, 39, Alignment.Right);
                rpt.Write(string.Format("{0:0}", 0), 3, 46, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", 0), 4, 51, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", (items.ReturnTotal - (model.TaxRefNo == 1 ? items.GSTAmount1 : items.VatPrice)) / items.Quantity), 7, 56, Alignment.Right);
                if (model.TaxRefNo != null && model.TaxRefNo == 1)
                {
                    rpt.Write(string.Format("{0:0.00}", items.GstTotal), 5, 64, Alignment.Right);

                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 64, Alignment.Right);
                }
                rpt.Write(string.Format("{0:0.00}", (items.ReturnTotal - (model.TaxRefNo == 1 ? items.GSTAmount1 : items.VatPrice))), 8, 70, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportFooter_A4(PlainTextReport rpt, VendorPurchaseReturn model)
        {            

            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", (model.NetReturn - (model.TaxRefNo == 1 ? model.TotalGSTAmount : model.TotalVATAmount))), 19, 60, Alignment.Right);
          
            rpt.NewLine();
            rpt.Write("Discount: " + string.Format("{0:0.00}", 0), 19, 60, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Round Off: " + string.Format("{0:0.00}", (Math.Round((decimal)model.NetReturn, MidpointRounding.AwayFromZero)) - model.NetReturn), 19, 60, Alignment.Right);
            rpt.NewLine();
            if (model.TaxRefNo == 1)
            {
                if (model.VendorPurchase.Vendor.LocationType == 2)
                {
                    rpt.Write("GST(IGST:" + string.Format("{0:0.00}", model.TotalGSTAmount) + "): " +
                       string.Format("{0:0.00}", model.TotalGSTAmount), 55, 24, Alignment.Right);
                }
                else
                {
                    if (model.TotalGSTAmount > 0)
                    {
                        string stxt = model.VendorPurchase.Instance.isUnionTerritory == false ? "+SGST:" : "+UTGST:";
                        rpt.Write("GST(CGST:" + string.Format("{0:0.00}", model.TotalCGSTAmount)
                            + stxt + string.Format("{0:0.00}", model.TotalSGSTAmount) + "): " +
                            string.Format("{0:0.00}", model.TotalGSTAmount), 55, 24, Alignment.Right);
                    }
                    else
                        rpt.Write("GST: " + string.Format("{0:0.00}", model.TotalGSTAmount), 55, 24, Alignment.Right);
                }
            }
            else
            {
                rpt.Write("Vat: " + string.Format("{0:0.00}", model.TotalVATAmount), 19, 60, Alignment.Right);
            }
            rpt.NewLine();
            rpt.Write("Net Amt: " + string.Format("{0:0.00}", Math.Round((decimal)model.NetReturn, MidpointRounding.AwayFromZero)), 19, 60, Alignment.Right);
            rpt.Write();
        }
        #endregion

    }
}
