﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using HQue.WebCore.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilities.Report;
using Utilities.Helpers;
using HQue.Biz.Core.Inventory;
using HQue.Contract.Infrastructure.Settings;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.WebCore.Controllers.Inventory
{
    [Route("[controller]")]
    public class PurchaseInvoiceController : BaseController
    {
        private readonly VendorPurchaseManager _vendorPurchaseManager;

        public PurchaseInvoiceController(VendorPurchaseManager vendorPurchaseManager,  ConfigHelper configHelper) : base(configHelper)
        {
            _vendorPurchaseManager = vendorPurchaseManager;
        }

        /// <summary>
        /// Regular bill template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> Index(string id = null)
        {
            var model = new InvoiceVM();
            if (!string.IsNullOrEmpty(id))
            {
                model.VendorPurchase = await _vendorPurchaseManager.GetVendorPurchaseDetails(id);
                if (model.VendorPurchase != null)
                {
                    model.VendorPurchase.InWords = (Math.Round((decimal)model.VendorPurchase.RoundOff, MidpointRounding.AwayFromZero)).ConvertNumbertoWords();
                }
            }

            return View(model);
        }

        /// <summary>
        /// Work around solution for dotmatrix 4Inch generic template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> printdm(string id = null)
        {
            var model = new InvoiceVM();
            if (!string.IsNullOrEmpty(id))
            {
                model.VendorPurchase = await _vendorPurchaseManager.GetVendorPurchaseDetails(id);
                if (model.VendorPurchase != null)
                {
                    model.VendorPurchase.InWords = NumberToText.ConvertNumbertoWords((long)model.VendorPurchase.RoundOff);
                }
            }

            string name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", model.VendorPurchase.InstanceId);

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(GeneratePurchaseReport_4InchRoll(model), false, Encoding.UTF8);

            }

            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("{0}.txt", model.VendorPurchase.InvoiceNo));


            //HttpContext.Response.Headers.Add("Content-Type", "text/plain");
            //return File(info.OpenRead(), "text/plain");

        }

        /// <summary>
        /// Work around solution for dotmatrix A4 generic template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> printdmA4NH(string id = null)
        {
            var model = new InvoiceVM();
            if (!string.IsNullOrEmpty(id))
            {
                model.VendorPurchase = await _vendorPurchaseManager.GetVendorPurchaseDetails(id);
                if (model.VendorPurchase != null)
                {
                    model.VendorPurchase.InWords = NumberToText.ConvertNumbertoWords((long)model.VendorPurchase.RoundOff);
                }
            }

            string name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", model.VendorPurchase.InstanceId);

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(GeneratePurchaseReport_A4_NoHeader(model), false, Encoding.UTF8);

            }

            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("{0}.txt", model.VendorPurchase.InvoiceNo));

        }

        /// <summary>
        /// Work around solution for dotmatrix A4 generic template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> printdmA4(string id = null)
        {
            var model = new InvoiceVM();
            if (!string.IsNullOrEmpty(id))
            {
                model.VendorPurchase = await _vendorPurchaseManager.GetVendorPurchaseDetails(id);
                if (model.VendorPurchase != null)
                {
                    model.VendorPurchase.InWords = NumberToText.ConvertNumbertoWords((long)model.VendorPurchase.RoundOff);
                }
            }

            string name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", model.VendorPurchase.InstanceId);

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(await GeneratePurchaseReport_A4(model), false, Encoding.UTF8);

            }

            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("hq_{0}.txt", model.VendorPurchase.InvoiceNo));

            //HttpContext.Response.Headers.Add("Content-Type", "text/plain");
            //return File(info.OpenRead(), "text/plain");

        }

        /// <summary>
        /// Work around solution for dotmatrix A4 with header generic template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> printdmA5h(string id = null)
        {
            var model = new InvoiceVM();
            if (!string.IsNullOrEmpty(id))
            {
                model.VendorPurchase = await _vendorPurchaseManager.GetVendorPurchaseDetails(id);
                if (model.VendorPurchase != null)
                {
                    model.VendorPurchase.InWords = NumberToText.ConvertNumbertoWords((long)model.VendorPurchase.RoundOff);
                }
            }

            string name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", model.VendorPurchase.InstanceId);

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(GeneratePurchaseReport_A5_WithHeader(model), false, Encoding.UTF8);
            }

            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("{0}.txt", model.VendorPurchase.InvoiceNo));

        }



        #region A4_Generic_template
        private async Task<string> GeneratePurchaseReport_A4(InvoiceVM data)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 80;
            rpt.Rows = 40;
            var purchaseSettings = await _vendorPurchaseManager.getBuyInvoiceDateEditSetting(User.AccountId(), User.InstanceId());

            WriteReportHeader_A4(rpt, data, purchaseSettings);
            WriteReportItems_A4(rpt, data, purchaseSettings);
            WriteReportFooter_A4(rpt, data);

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A4(PlainTextReport rpt, InvoiceVM data, PurchaseSettings purchaseSettings)
        {
            //rpt.WriteLine();
            /*GST Related columns added by Poongodi on 04/07/2017*/
            rpt.NewLine();
            rpt.Write(data.VendorPurchase.Instance.Name, 30, 22, Alignment.Center);
            rpt.NewLine();
            rpt.Write(data.VendorPurchase.Instance.FullAddress, 30, 16, Alignment.Center);
            //rpt.Write(data.vendorPurchase.Instance.Address + "," + data.vendorPurchase.Instance.Area, 25, 22, Alignment.Center);
            //rpt.NewLine();
            //rpt.Write(data.vendorPurchase.Instance.City + "-" + data.vendorPurchase.Instance.State + data.vendorPurchase.Instance.Pincode, 25, 22, Alignment.Center);
            //rpt.NewLine();
            rpt.NewLine();
            if (data.VendorPurchase.TaxRefNo != null && data.VendorPurchase.TaxRefNo == 1)
            {
                rpt.Write("DL No: " + data.VendorPurchase.Instance.DrugLicenseNo + " / " + "GSTIN: " + data.VendorPurchase.Instance.GsTinNo, 50, 16, Alignment.Center);
            }
            else
                rpt.Write("DL No: " + data.VendorPurchase.Instance.DrugLicenseNo + " / " + "TIN: " + data.VendorPurchase.Instance.TinNo, 50, 16, Alignment.Center);

            //rpt.Drawline();

            rpt.Write();
            rpt.NewLine();
            rpt.Write("Purchase Bill", 30, 22, Alignment.Center);

            rpt.Drawline();
            rpt.NewLine();
            rpt.Drawline();
            //rpt.NewLine();
            rpt.Write(data.VendorPurchase.Vendor.Name + "," + data.VendorPurchase.Vendor.Address + ", " + data.VendorPurchase.Vendor.Area + "," + data.VendorPurchase.Vendor.City + "-" + data.VendorPurchase.Vendor.Pincode, 50);
            rpt.NewLine();
            rpt.Write("DL :" + data.VendorPurchase.Vendor.DrugLicenseNo, 50);
            rpt.NewLine();
            //rpt.Write(, 50);
            if (data.VendorPurchase.TaxRefNo != null && data.VendorPurchase.TaxRefNo == 1)
            {
                rpt.Write("GSTIN:" + data.VendorPurchase.Vendor.GsTinNo, 50);
            }
            else
            {
                rpt.Write("TIN:" + data.VendorPurchase.Vendor.TinNo, 50);
            }
            //rpt.NewLine();
            //rpt.Write(, 50);
            rpt.Write("", 18, 50);

            rpt.Write();

            rpt.Drawline();

            rpt.NewLine();
            rpt.Write("Invoice No : " + data.VendorPurchase.InvoiceNo, 30, 1);
            rpt.Write("Invoice Date: " + string.Format("{0:dd/MM/yy}", data.VendorPurchase.InvoiceDate), 30, 50);

            rpt.Write();
            rpt.Drawline();

            //
            rpt.NewLine();
            rpt.Write("GR No: " + data.VendorPurchase.BillSeries + data.VendorPurchase.GoodsRcvNo, 30, 1);
            //added by nandhini 26.12.17 for grn date
            rpt.Write("GRN Date    : " + string.Format("{0:dd/MM/yy}", data.VendorPurchase.GrnDate), 30, 50);
          
            //rpt.Write("PO No. : " + data.vendorPurchase.VendorOrder.OrderId, 25, 25);
            //rpt.NewLine();
            //rpt.Write("PO Date. : " + string.Format("{0:dd/MM/yy}", data.vendorPurchase.VendorOrder.OrderDate), 25, 25);
            rpt.Write();
            rpt.Drawline();
            //rpt.NewLine();

            //  
            var startPosition = 0;
            var preWidth = 0;
            var width = 0;
            rpt.NewLine();
            rpt.Write("Particulars", (purchaseSettings.EnableMarkup == true ? preWidth = 17 : preWidth = 23));
            rpt.Write("Batch", width = 8, startPosition = startPosition + preWidth + (preWidth = width) - width);
            rpt.Write("Exp.", width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width);
            rpt.Write("Qty", width = 4, startPosition = startPosition + preWidth + (preWidth = width) - width);
            rpt.Write("FQty", width = 5, startPosition = startPosition + preWidth + (preWidth = width) - width);
            rpt.Write("Price", width = 7, startPosition = startPosition + preWidth + (preWidth = width) - width);
            rpt.Write("Disc", width = 5, startPosition = startPosition + preWidth + (preWidth = width) - width);
            rpt.Write("S.Disc", width = 7, startPosition = startPosition + preWidth + (preWidth = width) - width);
            if (purchaseSettings.EnableMarkup == true)
            {
                rpt.Write("Mkup%", width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width);
            }
            if (data.VendorPurchase.TaxRefNo != null && data.VendorPurchase.TaxRefNo == 1)
            {
                rpt.Write("GST%", width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width);
            }
            else
            {
                rpt.Write("VAT%", width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width);
            }
            rpt.Write("Amount", width = 9, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Center);
            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportItems_A4(PlainTextReport rpt, InvoiceVM data, PurchaseSettings purchaseSettings)
        {
            /*GST Related columns added by Poongodi on 04/07/2017*/

            int len = 0;
            foreach (var items in data.VendorPurchase.VendorPurchaseItem)
            {
                var startPosition = 0;
                var preWidth = 0;
                var width = 0;
                rpt.NewLine();
                rpt.Write(items.ProductStock.Product.Name, (purchaseSettings.EnableMarkup == true ? preWidth = 17 : preWidth = 23));
                rpt.Write(items.ProductStock.BatchNo, width = 8, startPosition = startPosition + preWidth + (preWidth = width) - width);
                rpt.Write(string.Format("{0:MM-yy}", items.ProductStock.ExpireDate), width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width);
                rpt.Write(string.Format("{0:0}", items.PackageQty), width = 4, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Right);
                rpt.Write(string.Format("{0:0}", items.FreeQty), width = 4, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", items.PackagePurchasePrice), width = 7, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", items.Discount), width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", items.SchemeDiscountPerc), width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Right);
                if (purchaseSettings.EnableMarkup == true)
                {
                    rpt.Write(string.Format("{0:0.00}", items.MarkupPerc), width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Right);
                }
                if (data.VendorPurchase.TaxRefNo != null && data.VendorPurchase.TaxRefNo == 1)
                {
                    rpt.Write(string.Format("{0:0.00}", (items.GstTotal)), width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Right);
                }
                else
                {
                    rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), width = 6, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Right);
                }
                rpt.Write(string.Format("{0:0.00}", items.Total), width = 9, startPosition = startPosition + preWidth + (preWidth = width) - width, Alignment.Right);
                //if(data.vendorPurchase.Discount>0)
                //{
                //    rpt.Write(string.Format("{0:0.00}", items.TotalWithDiscount), 8, 73, Alignment.Right);
                //}
                //else
                //{
                //    rpt.Write(string.Format("{0:0.00}", items.Total), 8, 73, Alignment.Right);
                //}

                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportFooter_A4(PlainTextReport rpt, InvoiceVM data)
        {
            /*GST Related columns added by Poongodi on 04/07/2017*/
            decimal? net = Math.Round((decimal)(data.VendorPurchase.TotalPurchaseValue + data.VendorPurchase.VatValue), MidpointRounding.AwayFromZero);
            //if (data.VendorPurchase.NoteType == "credit")
            //    net = net - data.VendorPurchase.NoteAmount;
            //else if (data.VendorPurchase.NoteType == "credit")
            //    net = net + data.VendorPurchase.NoteAmount;

            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.VendorPurchase.TotalPurchaseValue), 19, 60, Alignment.Right);
            //if (data.vendorPurchase.Discount > 0)
            //{
            //    rpt.Write("Total: " + string.Format("{0:0.00}", data.vendorPurchase.TotalOnly), 19, 60, Alignment.Right);
            //}
            //else
            //{
            //    rpt.Write("Total: " + string.Format("{0:0.00}", data.vendorPurchase.GrossTotal), 19, 60, Alignment.Right);
            //}           
            rpt.NewLine();
            rpt.Write("Discount: " + string.Format("{0:0.00}", data.VendorPurchase.TotalDiscountValue), 19, 60, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Credit Note: " + string.Format("{0:0.00}", data.VendorPurchase.NoteType=="credit"? data.VendorPurchase.NoteAmount:0), 19, 60, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Debit Note: " + string.Format("{0:0.00}", data.VendorPurchase.NoteType == "debit"? data.VendorPurchase.NoteAmount: 0), 19, 60, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Round Off: " + string.Format("{0:0.00}", data.VendorPurchase.RoundOffValue), 19, 60, Alignment.Right);
            rpt.NewLine();
            if (data.VendorPurchase.TaxRefNo == 1)
            {
                if (data.VendorPurchase.Vendor.LocationType == 2)
                {
                    rpt.Write("GST(IGST:" + string.Format("{0:0.00}", (data.VendorPurchase.VatValue - data.VendorPurchase.FreeQtyTotalTaxValue)) + (data.VendorPurchase.IsApplyTaxFreeQty == true ? "+FQty Tax:" + string.Format("{0:0.00}", data.VendorPurchase.FreeQtyTotalTaxValue) : "") + "): " +
                       string.Format("{0:0.00}", data.VendorPurchase.VatValue), 55, 24, Alignment.Right);
                }
                else
                {
                    if (data.VendorPurchase.VatValue > 0)
                    {
                        string stxt = data.VendorPurchase.Instance.isUnionTerritory == false ? "+SGST:" : "+UTGST:";
                        rpt.Write("GST(CGST:" + string.Format("{0:0.00}", ((data.VendorPurchase.VatValue - data.VendorPurchase.FreeQtyTotalTaxValue) / 2))
                            + stxt + string.Format("{0:0.00}", ((data.VendorPurchase.VatValue - data.VendorPurchase.FreeQtyTotalTaxValue) / 2)) + (data.VendorPurchase.IsApplyTaxFreeQty == true ? "+FQty Tax:" + string.Format("{0:0.00}", data.VendorPurchase.FreeQtyTotalTaxValue) : "") + "): " +
                            string.Format("{0:0.00}", data.VendorPurchase.VatValue), 55, 24, Alignment.Right);
                    }
                    else
                        rpt.Write("GST: " + string.Format("{0:0.00}", (data.VendorPurchase.VatValue - data.VendorPurchase.FreeQtyTotalTaxValue)), 55, 24, Alignment.Right);
                }
            }
            else
            {
                rpt.Write("Vat" + (data.VendorPurchase.IsApplyTaxFreeQty == true ? "(FQty Tax:" + string.Format("{0:0.00}", data.VendorPurchase.FreeQtyTotalTaxValue) + ")" : "") + ": " + string.Format("{0:0.00}", data.VendorPurchase.VatValue), 55, 24, Alignment.Right);
            }
            rpt.NewLine();
            rpt.Write("Net Amt: " + string.Format("{0:0.00}", Math.Round((decimal)(net + (data.VendorPurchase.DebitAmount) - (data.VendorPurchase.CreditAmount))), MidpointRounding.AwayFromZero), 19, 60, Alignment.Right);
            //if (data.vendorPurchase.Discount > 0)
            //{
            //    rpt.Write("Net Amt: " + string.Format("{0:0.00}", (decimal?)Math.Ceiling((double)data.vendorPurchase.NetTotalWithVat)), 19, 60, Alignment.Right);
            //}
            //else
            //{
            //    rpt.Write("Net Amt: " + string.Format("{0:0.00}", (decimal?)Math.Ceiling((double)data.vendorPurchase.NetTotal)), 19, 60, Alignment.Right);
            //}


            rpt.Write();

            //rpt.NewLine();
            //rpt.Write("Total: " + string.Format("{0:0.00}", data.vendorPurchase.GrossTotalWithVat), 20);
            //rpt.Write("Discount Amount: " + string.Format("{0:0.00}", data.vendorPurchase.DiscountInValue), 30, 30);
            //rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.vendorPurchase.NetTotalWithVat), 19, 60, Alignment.Right);
            //rpt.Write();
        }
        #endregion

        #region 4InchRoll_Generic _template
        private string GeneratePurchaseReport_4InchRoll(InvoiceVM data)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 50;
            rpt.Rows = 30;

            //some leading blank lines
            rpt.NewLine();
            rpt.NewLine();

            WriteReportHeader_4InchRoll(rpt, data);
            WriteReportItems_4InchRoll(rpt, data);
            WriteReportFooter_4InchRoll(rpt, data);

            //some trailing blank lines
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_4InchRoll(PlainTextReport rpt, InvoiceVM data)
        {
            rpt.NewLine();
            rpt.Write(data.VendorPurchase.Instance.Name, 30, 10, Alignment.Center);
            rpt.NewLine(); 
            rpt.Write(data.VendorPurchase.Instance.FullAddress, 30, 8);
            //rpt.Write(data.vendorPurchase.Instance.Address + "," + data.vendorPurchase.Instance.Area, 27, 10, Alignment.Center);
            //rpt.NewLine();
            //rpt.Write(data.vendorPurchase.Instance.City+"-" + data.vendorPurchase.Instance.State + data.vendorPurchase.Instance.Pincode, 28, 10, Alignment.Center);
            rpt.NewLine();
            rpt.Write("DL No-"+data.VendorPurchase.Instance.DrugLicenseNo +"/"+"TIN No-"+ data.VendorPurchase.Instance.TinNo, 30, 8, Alignment.Center);
            rpt.NewLine();
            rpt.Write("Purchase Bill", 30, 10, Alignment.Center);
            rpt.Drawline(1);
            rpt.NewLine();
            rpt.Drawline(1);
            rpt.Write(data.VendorPurchase.Vendor.Name +","+ data.VendorPurchase.Vendor.Address+"," + data.VendorPurchase.Vendor.Area +","+ data.VendorPurchase.Vendor.City+"," + data.VendorPurchase.Vendor.Pincode, 25);
            
            rpt.NewLine();
            rpt.Write("DL :" + data.VendorPurchase.Instance.DrugLicenseNo, 25, 1);
            //rpt.Write( + " " , 25, 1, TextAdjustment.Wrap);
            
            rpt.NewLine();
            rpt.Write("TIN:" + data.VendorPurchase.Instance.TinNo, 25, 1);
            //rpt.Write( + "-" + data.vendorPurchase.Vendor.Pincode, 25, 1, TextAdjustment.Wrap);
            rpt.Write("CST:", 18, 31);
            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            rpt.Write("Invoice No: " + data.VendorPurchase.InvoiceNo, 30, 1);
            rpt.Write("Invoice Date:" + string.Format("{0:dd/MM/yy}", data.VendorPurchase.InvoiceDate), 25, 25);
            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            rpt.Write("GR No: " + data.VendorPurchase.BillSeries + data.VendorPurchase.GoodsRcvNo, 30, 1);
            rpt.Write("GR Date:" + string.Format("{0:dd/MM/yy}", data.VendorPurchase.InvoiceDate), 25, 25);
            //rpt.Write("PO No. : " + data.vendorPurchase.VendorOrder.OrderId, 25, 25);
            //rpt.NewLine();
            //rpt.Write("PO Date. : " + string.Format("{0:dd/MM/yy}", data.vendorPurchase.VendorOrder.OrderDate), 25, 25);
            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            rpt.Write("Particulars", 12, 1);
            rpt.Write("Batch", 6, true);
            rpt.Write("Exp.D", 5, true);
            rpt.Write("Qty", 3, true, Alignment.Center);
            rpt.Write("Price", 6, true);
            rpt.Write("VAT", 3, true);
            rpt.Write("Amount", 6, true, Alignment.Right);
            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportItems_4InchRoll(PlainTextReport rpt, InvoiceVM data)
        {
            int len = 0;
            foreach (var items in data.VendorPurchase.VendorPurchaseItem)
            {
                rpt.NewLine();
                rpt.Write(items.ProductStock.Product.Name, 12, 1);
                rpt.Write(items.ProductStock.BatchNo, 6, true);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 5, true);
                rpt.Write(string.Format("{0:0}", items.PackageQty), 3, true, Alignment.Center);
                rpt.Write(string.Format("{0:0.00}", items.PackagePurchasePrice), 6, true, Alignment.Right);
                rpt.Write(string.Format("{0:0}%", items.ProductStock.VAT), 3, true, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", items.DiscountValue), 6, true, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(1);
        }

        private void WriteReportFooter_4InchRoll(PlainTextReport rpt, InvoiceVM data)
        {
            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.VendorPurchase.TotalPurchaseValue), 17, 31, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Discount: " + string.Format("{0:0.00}", data.VendorPurchase.TotalDiscountValue), 17, 31, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Credit Note: " + string.Format("{0:0.00}", 0), 17, 31, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Debit Note: " + string.Format("{0:0.00}", 0), 17, 31, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Round Off: " + string.Format("{0:0.00}", 0), 17, 31, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Vat: " + string.Format("{0:0.00}", data.VendorPurchase.Vat), 17, 31, Alignment.Right);
            rpt.NewLine();
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", (decimal?)Math.Ceiling((double)data.vendorPurchase.NetTotalWithVat)), 17, 31, Alignment.Right);
            rpt.Write("Net Amt: " + string.Format("{0:0.00}", (decimal?)Math.Round((double)(data.VendorPurchase.TotalPurchaseValue + data.VendorPurchase.VatValue), MidpointRounding.AwayFromZero)), 17, 31, Alignment.Right);
            rpt.Write();
        }
        #endregion

        #region A5_WithHeader_Generic _template
        private string GeneratePurchaseReport_A5_WithHeader(InvoiceVM data)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 63;
            rpt.Rows = 30;

            //some leading blank lines
            rpt.NewLine();
            rpt.NewLine();

            WriteReportHeader_A5_WithHeader(rpt, data);
            WriteReportItems_A5_WithHeader(rpt, data);
            WriteReportFooter_A5_WithHeader(rpt, data);

            //some trailing blank lines
            rpt.NewLine();
            rpt.NewLine();
            rpt.Write();

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A5_WithHeader(PlainTextReport rpt, InvoiceVM data)
        {
            rpt.NewLine();
            rpt.Write(data.VendorPurchase.Instance.Name, 25, 18, Alignment.Center);
            rpt.NewLine(); 
                rpt.Write(data.VendorPurchase.Instance.FullAddress, 25, 15);
            //rpt.Write(data.vendorPurchase.Instance.Address + "," + data.vendorPurchase.Instance.Area, 25, 18, Alignment.Center);
            //rpt.NewLine();
            //rpt.Write(data.vendorPurchase.Instance.City + "-" + data.vendorPurchase.Instance.State + data.vendorPurchase.Instance.Pincode, 25, 18, Alignment.Center);
            rpt.NewLine();
            rpt.Write("DL No   : " + data.VendorPurchase.Instance.DrugLicenseNo + " / " + "TIN :" + data.VendorPurchase.Instance.TinNo, 40, 10, Alignment.Center);
            rpt.NewLine();
            rpt.Write("Purchase Bill", 25, 18, Alignment.Center);
            rpt.Drawline(2);
            rpt.NewLine();
            rpt.Drawline(2);
            rpt.Write(data.VendorPurchase.Vendor.Name +"," + data.VendorPurchase.Vendor.Address+"," + data.VendorPurchase.Vendor.Area +","+ data.VendorPurchase.Vendor.City + "-" + data.VendorPurchase.Vendor.Pincode, 40, 2);
            rpt.NewLine();
            rpt.Write("DL :" + data.VendorPurchase.Instance.DrugLicenseNo, 40, 2);
            rpt.NewLine();
            //rpt.Write(data.vendorPurchase.Vendor.Address + " " + data.vendorPurchase.Vendor.Area, 40, 2);
            rpt.Write("TIN:" + data.VendorPurchase.Instance.TinNo, 40, 2);
            rpt.Write("CST:", 18, 41);
            //rpt.NewLine();
            //rpt.Write(data.vendorPurchase.Vendor.City + "-" + data.vendorPurchase.Vendor.Pincode, 40, 2);
            


            rpt.NewLine();
            rpt.Drawline(2);
            rpt.Write("Invoice No: " + data.VendorPurchase.InvoiceNo, 40, 2);
            rpt.Write("Invoice Date:" + string.Format("{0:dd/MM/yyyy HH:mm:ss}", data.VendorPurchase.InvoiceDate), 18, 35);
            rpt.Write();
            rpt.Drawline(2);
            //
            rpt.NewLine();
            rpt.Write("GR No: " + data.VendorPurchase.BillSeries + data.VendorPurchase.GoodsRcvNo, 40, 2);
            rpt.Write("GR Date: " + string.Format("{0:dd/MM/yy}", data.VendorPurchase.InvoiceDate), 18, 35);
            //rpt.Write("PO No:" + data.vendorPurchase.VendorOrder.OrderId, 18, 35);
            //rpt.NewLine();
            
            //rpt.Write("PO Date:" + string.Format("{0:dd/MM/yy}", data.vendorPurchase.VendorOrder.OrderDate), 18, 35);
            rpt.Write();
            rpt.Drawline(2);
            //

            rpt.NewLine();
            rpt.Write("Particulars", 20, 2);
            rpt.Write("Batch", 6, true);
            rpt.Write("Exp.D", 6, true);
            rpt.Write("Qty", 4, true, Alignment.Center);
            rpt.Write("Price", 6, true);
            rpt.Write("VAT", 3, true);
            rpt.Write("Amount", 6, true, Alignment.Right);
            rpt.Write();
            rpt.Drawline(2);
        }

        private void WriteReportItems_A5_WithHeader(PlainTextReport rpt, InvoiceVM data)
        {
            int len = 0;
            foreach (var items in data.VendorPurchase.VendorPurchaseItem)
            {

                rpt.NewLine();
                rpt.Write(items.ProductStock.Product.Name, 20, 2);
                rpt.Write(items.ProductStock.BatchNo, 6, true);
                rpt.Write(string.Format("{0:MM/yy}", items.ProductStock.ExpireDate), 6, true);
                rpt.Write(string.Format("{0:0}", items.PackageQty), 4, true, Alignment.Center);
                rpt.Write(string.Format("{0:0.00}", items.PackagePurchasePrice), 6, true, Alignment.Right);
                rpt.Write(string.Format("{0:0}%", items.ProductStock.VAT), 3, true, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", items.DiscountValue), 6, true, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline(2);
        }

        private void WriteReportFooter_A5_WithHeader(PlainTextReport rpt, InvoiceVM data)
        {

            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.VendorPurchase.TotalPurchaseValue), 18, 40, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Discount: " + string.Format("{0:0.00}", data.VendorPurchase.TotalDiscountValue), 18, 40, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Credit Note: " + string.Format("{0:0.00}", 0), 18, 40, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Debit Note: " + string.Format("{0:0.00}", 0), 18, 40, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Round Off: " + string.Format("{0:0.00}", 0), 18, 40, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Vat: " + string.Format("{0:0.00}", data.VendorPurchase.Vat), 18, 40, Alignment.Right);
            rpt.NewLine();
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", (decimal?)Math.Ceiling((double)data.vendorPurchase.NetTotalWithVat)), 18, 40, Alignment.Right);
            rpt.Write("Net Amt: " + string.Format("{0:0.00}", (decimal?)Math.Round((double)(data.VendorPurchase.TotalPurchaseValue + data.VendorPurchase.VatValue), MidpointRounding.AwayFromZero)), 18, 40, Alignment.Right);
            rpt.Write();
        }
        #endregion
        #region A4_NoHeader_Generic_template
        private string GeneratePurchaseReport_A4_NoHeader(InvoiceVM data)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 80;
            rpt.Rows = 40;

            WriteReportHeader_A4_NoHeader(rpt, data);
            WriteReportItems_A4_NoHeader(rpt, data);
            WriteReportFooter_A4_NoHeader(rpt, data);

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A4_NoHeader(PlainTextReport rpt, InvoiceVM data)
        {
            //Write header
            rpt.NewLine();
            rpt.Write("-", 5, 10);
            rpt.NewLine();
            rpt.Write();

            rpt.NewLine();
            rpt.Write(data.VendorPurchase.InvoiceNo, 20, 60);

            rpt.NewLine();
            rpt.NewLine();
            rpt.Write(string.Format("{0:dd/MM/yyyy HH:mm:ss}", data.VendorPurchase.InvoiceDate), 20, 60);

            rpt.NewLine();
            rpt.NewLine();
            rpt.Write("", 20, 60);

            rpt.NewLine();
            rpt.Write();

        }

        private void WriteReportItems_A4_NoHeader(PlainTextReport rpt, InvoiceVM data)
        {
            int len = 0;
            foreach (var items in data.VendorPurchase.VendorPurchaseItem)
            {
                rpt.NewLine();
                rpt.Write(items.ProductStock.Product.Name, 35);
                rpt.Write(items.ProductStock.BatchNo, 8, 36);
                rpt.Write(string.Format("{0:dd-MM-yy}", items.ProductStock.ExpireDate), 8, 45);
                rpt.Write(string.Format("{0:0}", items.PackageQty), 4, 55);
                rpt.Write(string.Format("{0:0.00}", items.PackagePurchasePrice), 6, 59, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", (items.ProductStock.VAT)), 5, 67);
                rpt.Write(string.Format("{0:0.00}", items.DiscountValue), 6, 74, Alignment.Right);
                rpt.Write();
                len++;
            }

            for (int i = 0; i < 10 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }

        private void WriteReportFooter_A4_NoHeader(PlainTextReport rpt, InvoiceVM data)
        {

            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.VendorPurchase.TotalPurchaseValue), 19, 60, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Discount: " + string.Format("{0:0.00}", data.VendorPurchase.TotalDiscountValue), 19, 60, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Credit Note: " + string.Format("{0:0.00}", 0), 19, 60, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Debit Note: " + string.Format("{0:0.00}", 0), 19, 60, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Round Off: " + string.Format("{0:0.00}", 0), 19, 60, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Vat: " + string.Format("{0:0.00}", data.VendorPurchase.Vat), 19, 60, Alignment.Right);
            rpt.NewLine();
            //rpt.Write("Net Amt: " + string.Format("{0:0.00}", (decimal?)Math.Ceiling((double)data.vendorPurchase.NetTotalWithVat)), 19, 60, Alignment.Right);
            rpt.Write("Net Amt: " + string.Format("{0:0.00}", (decimal?)Math.Round((double)(data.VendorPurchase.TotalPurchaseValue + data.VendorPurchase.VatValue), MidpointRounding.AwayFromZero)), 19, 60, Alignment.Right);
            rpt.Write();

            //rpt.NewLine();
            //rpt.Write("Total: " + string.Format("{0:0.00}", data.vendorPurchase.GrossTotalWithVat), 20);
            //rpt.Write("Discount Amount: " + string.Format("{0:0.00}", data.vendorPurchase.DiscountInValue), 30, 30);
            //rpt.Write("Net Amount: " + string.Format("{0:0.00}", data.vendorPurchase.NetTotalWithVat), 19, 60, Alignment.Right);
            //rpt.Write();
        }
        #endregion


    }
}
