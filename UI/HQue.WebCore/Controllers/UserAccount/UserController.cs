﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using HQue.Contract.Infrastructure.UserAccount;
using Utilities.UserSession;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Setup;
using Utilities.Enum;
using HQue.Biz.Core.UserAccount;
using HQue.Biz.Core.Setup;
using HQue.Biz.Core.Replication;
using System.Linq;
using System;
using HQue.DataAccess.Accounts;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Biz.Core.Master;
using HQue.Biz.Core.Inventory;
using HQue.Biz.Core.Sync;

namespace HQue.WebCore.Controllers.UserAccount
{
    [Route("[controller]")]
    public class UserController : BaseController
    {
        private readonly UserManager _userManager;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly SyncHelper _syncHelper;
        private readonly PaymentDataAccess _payds;
        private readonly ProductManager _productManager;
        private readonly ProductStockManager _productStockManager;
        private readonly AccountSetupManager _accountSetupManager;
        private readonly SyncManager _SyncManager;
        public UserController(UserManager userManager, InstanceSetupManager instanceSetupManager, SyncHelper syncHelper, PaymentDataAccess payds, ProductManager productManager, ConfigHelper configHelper, ProductStockManager productStockManager, AccountSetupManager accountSetupManager, SyncManager syncManager) : base(configHelper)
        {
            _userManager = userManager;
            _instanceSetupManager = instanceSetupManager;
            _syncHelper = syncHelper;
            _payds = payds;
            _productManager = productManager;
            _productStockManager = productStockManager;
            _accountSetupManager = accountSetupManager;
            _SyncManager = syncManager;
        }

        [AllowAnonymous]
        [Route("/")]
        [Route("[action]")]
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

#if AZURE
        [RequireHttps] //  Open this for Dev push
#endif        
        [AllowAnonymous]
        [Route("/")]
        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> Login(HQueUser user)
        {
            var result = await _syncHelper.HandleSync(user);

            if (result.Item1)
            {

                result.Item2.IsGstEnabled = ConfigHelper.AppConfig.IsGstEnabled; //GSTEnabled added by Poongodi on 29/06/2017
                var authUser = result.Item2;

                var ShortCutList = await _instanceSetupManager.GetShortCutKeySetting(authUser.AccountId, authUser.InstanceId);
                if (ShortCutList != null)
                {
                    authUser.ShortCutKeysType = Convert.ToString(ShortCutList.ShortCutKeysType);
                    authUser.NewWindowType = Convert.ToString(ShortCutList.NewWindowType);
                }


                authUser.OfflineStatus = ConfigHelper.AppConfig.OfflineMode;
                //authUser.IsGstEnabled = ConfigHelper.AppConfig.IsGstEnabled; //GSTEnabled added by Poongodi on 29/06/2017 //Commented by Manivannan on 03-Jul-2017 since already added at top
                var identity = new CustomIdentity(authUser);
                var principal = new ClaimsPrincipal(identity);
                /*Added by Poongodi For finyear validation 30/03/2017*/
                //Commented by Poongodi on 10/04/2017 - Not needed
                /* if (authUser.AccountId.ToString() != "0a2465f6-10c3-4b22-a993-8d5d2939d580")
                 { 
                 var finyeardata = new FinYearMaster();
                 finyeardata = await _payds.getFinyearAvailable(authUser.AccountId);
                 if (finyeardata.IsBillNumberReset == null)
                 {
                     await HttpContext.Authentication.SignInAsync("Cookies", principal);
                     return RedirectToAction("Index1", "Dashboard");
                 }
                 } */
                // Added by Settu for multi instance identification
                var InstanceCount = await _instanceSetupManager.GetTotalInstanceCount(result.Item2.AccountId);
                identity.AddClaim(new Claim(CustomIdentity.HasMultipleInstance, (InstanceCount > 1 ? "true" : "false")));
                ////Added by sumathi on 10-11-18 for reverse sync filter
                //var SyncEnabled = await _accountSetupManager.GetCustomSettingsDetail(authUser.AccountId, authUser.InstanceId,5);
                //identity.AddClaim(new Claim(CustomIdentity.IsSyncEnabled, ((bool)SyncEnabled ? "true" : "false")));

                //Added by Mani for GST Null Value identification
                var gstNullAvailable = 0;
                if (authUser.InstanceId != null)
                {
                    gstNullAvailable = await _productManager.GetGstNullAvailable(authUser.AccountId, authUser.InstanceId);
                    identity.AddClaim(new Claim(CustomIdentity.GSTNullAvailable, gstNullAvailable.ToString()));
                }

                switch (result.Item2.UserType)
                {
                    case UserType.HQueAdmin:
                        await HttpContext.Authentication.SignInAsync("Cookies", principal);
                        return RedirectToAction("Index", "AccountSetup");
                    case UserType.DistributorUser:
                        await HttpContext.Authentication.SignInAsync("Cookies", principal);
                        return RedirectToAction("DistributorBranchList", "DistributorReport");
                    case UserType.HQueToolUser:
                        await HttpContext.Authentication.SignInAsync("Cookies", principal);
                        return RedirectToAction("Settings", "Tool");
                }

                if (authUser.HasInstanceId)
                {

                    //Added by Sarubala on 28-12-18 to stop Offline users in online login - start
                    if (Convert.ToInt32(authUser.Instance.InstanceTypeId) == 1 && !ConfigHelper.AppConfig.OfflineMode)
                    {
                        ModelState.AddModelError("UserId", "Invalid Id (Fully Offline User)");
                        return View(user);
                    }
                    //Added by Sarubala on 28-12-18 - end

                    identity.AddClaim(new Claim(CustomIdentity.OfflineMode, ConfigHelper.AppConfig.OfflineMode.ToString()));
                    identity.AddClaim(new Claim(CustomIdentity.LoggedinDate, DateTime.Now.ToString("dd/MM/yyyy")));

                    //Added by Sarubala on 19-12-18
                    if (authUser.Instance.InstanceTypeId != null)
                    {
                        identity.AddClaim(new Claim(CustomIdentity.InstanceTypeId, authUser.Instance.InstanceTypeId.ToString()));
                    }
                    else
                    {
                        identity.AddClaim(new Claim(CustomIdentity.InstanceTypeId, "0"));
                    }

                    principal = new ClaimsPrincipal(identity);
                    await HttpContext.Authentication.SignInAsync("Cookies", principal);
                    await _syncHelper.SyncOnlineData(authUser);
                    //Offline version number method added by POongodi on 05/07/2017
                    if (authUser.OfflineStatus)
                    {
                        authUser.Instance.AccountId = authUser.AccountId;
                        var bresult = await _instanceSetupManager.UpdateOfflineVersion(authUser.Instance, ConfigHelper.AppConfig.Version, authUser.Id);
                    }
                    var AccessRightList = authUser.RoleAccess.Where(y => y.AccessRight == 2).Select(z => z).ToList();
                    //added by nandhini on 13.11.17

                    var LoginHistory = await _userManager.LoginHistory(authUser.AccountId, authUser.InstanceId, authUser.UserId);

                    var t = _productStockManager.ClosingStockScheduler(authUser.AccountId, authUser.InstanceId, authUser.Id); //Added by Poongodi on 17/10/2017

                    //  var sync = _SyncManager.SyncDataDeleteScheduler(authUser.AccountId, authUser.InstanceId, authUser.InstanceId); //Added by Sumathi on 30/11/2018 - Removed as per Poongodi's Suggesstion

                    if (AccessRightList.Count > 0 && AccessRightList.First().AccessRight == 2)
                    {

                        return RedirectToAction("Index", "Sales"); // Sales screen Landing page chenged by Violet  
                    }
                    else
                    {
                        var AccessRightAdmin = authUser.RoleAccess.Where(y => y.AccessRight != 1).Select(z => z).ToList();
                        if (AccessRightAdmin.Count > 0)
                        {
                            return await NavigateToView(AccessRightAdmin.First().AccessRight as int? ?? 20, ConfigHelper.AppConfig.IsSqlServer);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Sales"); // Sales screen Landing page chenged by Violet  
                        }

                    }

                    // return RedirectToAction("Index", "Dashboard");                    

                }

                if (ConfigHelper.AppConfig.IsSqlServer)
                {
                    var instanceList = await _instanceSetupManager.List(authUser.AccountId);
                    if (instanceList.Count == 1)
                    {
                        authUser.InstanceId = instanceList.First().Id;
                        authUser.Instance = await _instanceSetupManager.GetById(authUser.InstanceId);

                        //Added by Sarubala on 28-12-18 to stop Offline users in online login - start
                        if (Convert.ToInt32(authUser.Instance.InstanceTypeId) == 1 && !ConfigHelper.AppConfig.OfflineMode)
                        {
                            ModelState.AddModelError("UserId", "Invalid Id (Fully Offline User)");
                            return View(user);
                        }
                        //Added by Sarubala on 28-12-18 - end

                        //Offline version number method added by POongodi on 05/07/2017
                        if (authUser.OfflineStatus)
                        {
                            var bresult = await _instanceSetupManager.UpdateOfflineVersion(authUser.Instance, ConfigHelper.AppConfig.Version, identity.Claims.FirstOrDefault().Value);
                        }
                        ShortCutList = await _instanceSetupManager.GetShortCutKeySetting(authUser.AccountId, authUser.InstanceId);
                        if (ShortCutList != null)
                        {
                            authUser.ShortCutKeysType = Convert.ToString(ShortCutList.ShortCutKeysType);
                            authUser.NewWindowType = Convert.ToString(ShortCutList.NewWindowType);
                        }

                        identity = new CustomIdentity(authUser);
                        principal = new ClaimsPrincipal(identity);
                        await HttpContext.Authentication.SignInAsync("Cookies", principal);
                        //added by nandhini on 13.11.17

                        //Added by Sarubala on 29-12-18
                        if (authUser.Instance.InstanceTypeId != null)
                        {
                            identity.AddClaim(new Claim(CustomIdentity.InstanceTypeId, authUser.Instance.InstanceTypeId.ToString()));
                        }
                        else
                        {
                            identity.AddClaim(new Claim(CustomIdentity.InstanceTypeId, "0"));
                        }

                        var LoginHistory = await _userManager.LoginHistory(authUser.AccountId, authUser.InstanceId, authUser.UserId);

                        var AccessRightList = authUser.RoleAccess.Where(y => y.AccessRight == 2).Select(z => z).ToList();
                        var t = _productStockManager.ClosingStockScheduler(authUser.AccountId, authUser.InstanceId, authUser.Id); //Added by Poongodi on 17/10/2017

                        // var sync = _SyncManager.SyncDataDeleteScheduler(authUser.AccountId, authUser.InstanceId, authUser.InstanceId); //Added by Sumathi on 30/11/2018  Removed as per Poongodi's Suggesstion

                        if (AccessRightList.Count > 0 && AccessRightList.First().AccessRight == 2)
                        {
                            return RedirectToAction("Index", "Sales"); //Sales screen Landing page chenged by Violet  
                        }
                        else
                        {
                            var AccessRightAdmin = authUser.RoleAccess.Where(y => y.AccessRight != 1).Select(z => z).ToList();

                            if (AccessRightAdmin.Count > 0)
                            {
                                return await NavigateToView(AccessRightAdmin.First().AccessRight as int? ?? 20, ConfigHelper.AppConfig.IsSqlServer);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Sales"); //Sales screen Landing page chenged by Violet  
                            }

                        }

                        // return RedirectToAction("Index", "Dashboard");
                    }

                    if (!instanceList.Any())
                    {
                        await HttpContext.Authentication.SignInAsync("Cookies", principal);
                        return RedirectToAction("List", "InstanceSetup");
                    }
                }

                await HttpContext.Authentication.SignInAsync("Cookies", principal);

                return RedirectToAction("SelectInstance");
            }

            ModelState.AddModelError("UserId", "Invalid user name or password");
            return View(user);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<string> LoginBySupport([FromBody]HQueUser user)
        {
            var result = await _syncHelper.HandleSync(user);

            if (result.Item1)
            {
                if (result.Item2.UserType == 5)
                    return "Success";
            }

            return "Failure";
        }


        public async Task<IActionResult> NavigateToView(int AccessNumber, bool isSqlServer)
        {
            if (AccessNumber == UserAccess.Sell)
            {
                return RedirectToAction("Index", "Sales");

            }
            if (AccessNumber == UserAccess.Buy)
            {
                return RedirectToAction("Index", "VendorPurchase");

            }
            if (AccessNumber == UserAccess.StockTransfer)
            {
                return RedirectToAction("Index", "StockTransfer");

            }
            if (AccessNumber == UserAccess.Leads && isSqlServer)
            {
                return RedirectToAction("List", "Leads");

            }
            if (AccessNumber == UserAccess.Order)
            {
                return RedirectToAction("Index", "VendorOrder");

            }
            if (AccessNumber == UserAccess.Profile && isSqlServer)
            {
                return RedirectToAction("Index", "Vendor");

            }
            if (AccessNumber == UserAccess.Inventory)
            {
                return RedirectToAction("list", "ProductStock");

            }
            if (AccessNumber == UserAccess.Reports && isSqlServer)
            {
                return RedirectToAction("HomeDeliveryList", "SalesReport");

            }
            if (AccessNumber == UserAccess.Branch && isSqlServer)
            {
                return RedirectToAction("Index", "InstanceSetup");

            }
            if (AccessNumber == UserAccess.Accounts && isSqlServer)
            {
                return RedirectToAction("Index", "PettyCash");

            }
            if (AccessNumber == UserAccess.UserManagement && isSqlServer)
            {
                return RedirectToAction("Index", "UserManagement");

            }
            if (AccessNumber == UserAccess.Dashboard)
            {
                return RedirectToAction("Index", "Dashboard");

            }

            return RedirectToAction("Index", "Dashboard");

        }

        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> SelectInstance()
        {
            var instances = await _instanceSetupManager.List(User.AccountId());
            //ViewBag.Instance = instances.Select(x => new SelectListItem {Value = x.Id, Text = x.Name}).ToList(); //Commented by Lawrence
            //var InstanceItem = instances.Select(x => new SelectListItem { Value = x.Id, Text = (x.Name + (x.Area != null ? (", " + x.Area) : "")) }).ToList();
            var InstanceItem = instances.Select(x => new SelectListItem { Value = x.Id, Text = (x.Area != null ? (x.Area) + ", " : "") + x.Name }).ToList();
            foreach (var item in InstanceItem)
            {
                if (item.Text.Length > 30)
                {
                    item.Text = item.Text.Substring(0, 30).TrimEnd(',').TrimEnd();
                }
                else
                    item.Text = item.Text.Trim().TrimEnd(',');
            }
            ViewBag.Instance = InstanceItem;
            return View();
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> SelectInstance(HQueUser user)
        {
            user.AccountId = User.AccountId();

            var identity = new CustomIdentity(User.Claims);
            var instance = await GetInstanceData(user);
            //Offline version number method added by POongodi on 05/07/2017
            if (ConfigHelper.AppConfig.OfflineMode)
            {
                var bresult = await _instanceSetupManager.UpdateOfflineVersion(instance, ConfigHelper.AppConfig.Version, identity.Claims.FirstOrDefault().Value);
            }

            user.IsGstEnabled = ConfigHelper.AppConfig.IsGstEnabled;
            instance.Name = instance.Name + (!string.IsNullOrEmpty(instance.Area) ? (", " + instance.Area) : "");
            if (instance.Name.Length > 30)
            {
                instance.Name = instance.Name.Substring(0, 30).TrimEnd(',').TrimEnd();
            }
            else
                instance.Name = instance.Name.TrimEnd(',');
            identity.AddClaim(new Claim(CustomIdentity.InstanceId, user.InstanceId));


            var ShortCutList = await _instanceSetupManager.GetShortCutKeySetting(user.AccountId, user.InstanceId);
            if (ShortCutList != null)
            {
                user.ShortCutKeysType = Convert.ToString(ShortCutList.ShortCutKeysType);
                user.NewWindowType = Convert.ToString(ShortCutList.NewWindowType);
            }
            identity.AddClaim(new Claim(CustomIdentity.InstanceName, instance.Name));
            identity.AddClaim(new Claim(CustomIdentity.HasMultipleInstanceAccess, "true"));
            identity.AddClaim(new Claim(CustomIdentity.InstancePhone, string.IsNullOrEmpty(instance.Phone) ? "" : instance.Phone));
            identity.AddClaim(new Claim(CustomIdentity.InstanceMobile, string.IsNullOrEmpty(instance.Mobile) ? "" : instance.Mobile));
            identity.AddClaim(new Claim(CustomIdentity.OfflineMode, ConfigHelper.AppConfig.OfflineMode.ToString()));
            identity.AddClaim(new Claim(CustomIdentity.OfflineInstalled, instance.IsOfflineInstalled.ToString()));
            identity.AddClaim(new Claim(CustomIdentity.LoggedinDate, DateTime.Now.ToString("dd/MM/yyyy")));
            identity.AddClaim(new Claim(CustomIdentity.IsComposite, instance.Gstselect.ToString()));
            user.OfflineStatus = ConfigHelper.AppConfig.OfflineMode;

            //Added by Sarubala on 19-12-18
            if (instance.InstanceTypeId != null)
            {
                identity.AddClaim(new Claim(CustomIdentity.InstanceTypeId, instance.InstanceTypeId.ToString()));
            }
            else
            {
                identity.AddClaim(new Claim(CustomIdentity.InstanceTypeId, "0"));
            }

            //Added by Sarubala on 28-12-18 to stop Offline users in online login - start
            if (Convert.ToInt32(instance.InstanceTypeId) == 1 && !ConfigHelper.AppConfig.OfflineMode)
            {
                ModelState.AddModelError("UserId", "Invalid Id (Fully Offline User)");
                return RedirectToAction("Login");
            }
            //Added by Sarubala on 28-12-18 - end

            var gstNullAvailable = 0;
            if (user.InstanceId != null)
                gstNullAvailable = await _productManager.GetGstNullAvailable(user.AccountId, user.InstanceId);
            identity.AddClaim(new Claim(CustomIdentity.GSTNullAvailable, gstNullAvailable.ToString()));

            if (user.NewWindowType == null)
            {
                identity.AddClaim(new Claim(CustomIdentity.NewWindowType, "True"));
            }
            else
            {
                identity.AddClaim(new Claim(CustomIdentity.NewWindowType, user.NewWindowType));
            }

            if (user.NewWindowType == null)
            {
                identity.AddClaim(new Claim(CustomIdentity.ShortCutKeysType, "True"));
            }
            else
            {
                identity.AddClaim(new Claim(CustomIdentity.ShortCutKeysType, user.ShortCutKeysType));
            }

            var principal = new ClaimsPrincipal(identity);
            await HttpContext.Authentication.SignInAsync("Cookies", principal);
            await _syncHelper.SyncOnlineData(user);

            var UserId = principal.Claims.ToList();
            //var LoginUserId = UserId[3].Value;
            var LoginUserId = UserId.Find(x => x.Type == ClaimTypes.Email).Value; // Changed by Manivannan on 03-Jul-2017

            var HqueUserId = await _instanceSetupManager.GetHqueUserId(user.AccountId, LoginUserId);
            //if (HqueUserId != null)
            //{
            var UserAccessList = await _instanceSetupManager.GetUserAccess(user.AccountId, HqueUserId.Id);
            //added by nandhini on 13.11.17
            var LoginHistory = await _userManager.LoginHistory(user.AccountId, user.InstanceId, User.UserId());

            var AccessRight = UserAccessList.Where(y => y.AccessRight == 2).Select(z => z).ToList();
            var t = _productStockManager.ClosingStockScheduler(user.AccountId, user.InstanceId, HqueUserId.Id); //Added by Poongodi on 17/10/2017

            //  var sync = _SyncManager.SyncDataDeleteScheduler(user.AccountId, user.InstanceId, HqueUserId.Id); //Added by Sumathi on 30/11/2018 -  Removed as per Poongodi's Suggesstion

            if (AccessRight.Count > 0 && AccessRight.First().AccessRight == 2)
            {
                return RedirectToAction("Index", "Sales"); // Sales screen Landing page chenged by Violet  
            }
            else
            {
                var AccessRightAdmin = UserAccessList.Where(y => y.AccessRight != 1).Select(z => z).ToList();
                if (AccessRightAdmin.Count > 0)
                {
                    return await NavigateToView(AccessRightAdmin.First().AccessRight as int? ?? 20, ConfigHelper.AppConfig.IsSqlServer);
                }
                else
                {
                    return RedirectToAction("Index", "Sales"); // Sales screen Landing page chenged by Violet  
                }
            }

        }

        private async Task<Instance> GetInstanceData(HQueUser user)
        {
            if (ConfigHelper.AppConfig.IsSqlServer)
                return await _instanceSetupManager.GetById(user.InstanceId);
            return
                (await _instanceSetupManager.List(User.AccountId())).First(x => x.InstanceId == user.InstanceId);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> LogOut()
        {
            var OfflineStatus = false;
            OfflineStatus = ConfigHelper.AppConfig.OfflineMode;
            await HttpContext.Authentication.SignOutAsync("Cookies");
            await _userManager.logOutHistory(User.AccountId(), User.InstanceId(), User.UserId(), OfflineStatus);//added by nandhini 14.11.17
            return RedirectToAction("Login");

        }

        [AllowAnonymous]
        [Route("/ForgotPassword")]
        [Route("[action]")]
        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [Route("/ForgotPassword")]
        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> ForgotPassword(HQueUser user)
        {
            var result = await _userManager.ValidateEmail(user);

            if (result.Item1)
            {
                string key = result.Item2.PasswordResetKey;
                ModelState.AddModelError("UserId", "Check your registered email for reset password link");
            }

            ModelState.AddModelError("UserId", "Invalid Email");
            return View(user);
        }

        [AllowAnonymous]
        [Route("/ResetPassword")]
        [Route("[action]")]
        [HttpGet]
        public async Task<ViewResult> ResetPassword(string key)
        {
            if (await _userManager.ValidateResetKey(key))
                ViewBag.Key = key;
            else
                ViewBag.Key = string.Empty;

            return View();
        }

        [AllowAnonymous]
        [Route("/ResetPassword")]
        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> ResetPassword(HQueUser user)
        {
            var result = await _userManager.UpdateResetPassword(user);
            return RedirectToAction("Login");
        }

        [Route("[action]")]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> ChangePassword(HQueUser user)
        {
            user.UserId = User.UserId();
            var result = await _userManager.ChangePassword(user);
            return RedirectToAction("Login");
        }


        public int ValidateLogtime()
        {
            int nResult = 0;
            if (User.LoggedinDate() != null)
            {
                if (Convert.ToDateTime(User.LoggedinDate().ToString()) != Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"))
                    )
                {
                    HttpContext.Authentication.SignOutAsync("Cookies");

                    RedirectToAction("Login");
                    nResult = 1;
                }
            }
            return nResult;
        }
    }
}
