﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using HQue.Biz.Core.Setup;
using HQue.Biz.Core.UserAccount;
using HQue.Contract.Infrastructure.UserAccount;
using Utilities.Helpers;
using Utilities.UserSession;
using HQue.Biz.Core.Replication;
using HQue.Contract.Infrastructure.Setup;
using Utilities.Enum;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.WebCore.Controllers.UserAccount
{
    [Route("[controller]")]
    public class WebUserController : Controller
    {
        private readonly UserManager _userManager;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly SyncHelper _syncHelper;
        public WebUserController(UserManager userManager, InstanceSetupManager instanceSetupManager, SyncHelper syncHelper,
          ConfigHelper configHelper)
        {
            _userManager = userManager;
            _instanceSetupManager = instanceSetupManager;
            _syncHelper = syncHelper;
        }


        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
