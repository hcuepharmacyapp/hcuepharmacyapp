﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Biz.Core.UserAccount;
using HQue.Contract.Infrastructure.UserAccount;
using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.UserAccount
{
    [Route("[controller]")]
    public class UserManagementController : BaseController
    {
        private readonly UserAccessManager _userAccessManager;
        private readonly UserManager _userManager;
        private readonly ConfigHelper _configHelper;

        public UserManagementController(UserAccessManager userAccessManager,UserManager userManager, ConfigHelper configHelper) : base(configHelper)
        {
            _userAccessManager = userAccessManager;
            _userManager = userManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult Index()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Index([FromBody] HQueUser model)
        {
            model.SetLoggedUserDetails(User);
            model = await _userManager.Save(model);
            return View(model);
        }

        [Route("[action]")]
        public async Task<IEnumerable<HQueUser>> UserAccessList(HQueUser model)
        {
            model.SetLoggedUserDetails(User);
            model.Id = User.Identity.Id();
            return await _userManager.UserAccessList(model);
        }

        [Route("[action]")]
        public async Task<bool> Update([FromBody]HQueUser model)
        {
            await _userAccessManager.DeleteUserAccess(model);

            model.SetLoggedUserDetails(User);
            await _userAccessManager.UpdateUserAccess(model);
            return true;
        }

        //Added by Sarubala on 20-11-17
        [Route("[action]")]
        public async Task<bool> GetIsUserCreateSms()
        {
            return await _userAccessManager.GetIsUserCreateSms(User.InstanceId());
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<HQueUser> AddSalesPerson([FromBody] HQueUser model)
        {
            model.SetLoggedUserDetails(User);
            model.UserId = User.UserId();
            return await _userManager.AddSalesPerson(model);
        }
        //added by nandhini on 4/1/18
        [HttpPost]
        [Route("[action]")]
        public async Task<HQueUser> updateUserValuesStatus([FromBody] HQueUser model)
        {
            return await _userManager.updateUserValuesStatus(model);
        }
        [Route("[action]")]
        public async Task<IEnumerable<HQueUser>> SalesPersonList()
        {
            var user = new HQueUser();
            user.SetLoggedUserDetails(User);
            user.UserType = 6;
            //user.Status = 0;
            return await _userManager.SalesPersonList(user);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<HQueUser> validateExistUser([FromBody] HQueUser model)
        {
            model.SetLoggedUserDetails(User);
            return await _userManager.validateExistUser(model);
        }


    }
}
