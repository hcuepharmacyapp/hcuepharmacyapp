﻿using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Accounts
{
    [Route("[controller]")]
    public class CustomerReceiptController : BaseController
    {
        private readonly ConfigHelper _configHelper;
        public CustomerReceiptController(ConfigHelper configHelper) : base(configHelper)
        {
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult Index()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }


        //[Route("[action]")]
        //public IActionResult CustomerDetails()
        //{
        //    return View();
        //}

        [Route("[action]")]
        public IActionResult Cheque()
        {
            return View();
        }

        [Route("[action]")]
        //Customer receipt shown by PatientId instead of patient mobile & name - Settu
        public IActionResult CustomerDetails(string PatientId)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }

            ViewBag.PatientId = PatientId;
            return View();
        }
    }
}
