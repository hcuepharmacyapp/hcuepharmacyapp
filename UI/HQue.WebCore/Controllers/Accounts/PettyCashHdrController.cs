﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Accounts;
using Utilities.Helpers;
using HQue.Biz.Core.Accounts;
using HQue.Contract.Infrastructure.Misc;

namespace HQue.WebCore.Controllers.Accounts
{
    [Route("[controller]")]
    public class PettyCashHdrController : BaseController
    {

        private readonly ConfigHelper _configHelper;
      
        public PettyCashHdrController(ConfigHelper configHelper) : base(configHelper)
        {
            _configHelper = configHelper;
          
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult Index()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }

       

        [Route("[action]")]
        public IActionResult List()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }
    }
}
