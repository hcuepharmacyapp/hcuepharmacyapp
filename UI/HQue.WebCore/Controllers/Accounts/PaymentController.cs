﻿using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Accounts
{
    [Route("[controller]")]
    public class PaymentController : BaseController
    {
        private readonly ConfigHelper _configHelper;
        public PaymentController(ConfigHelper configHelper) : base(configHelper)
        {
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult Index()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Payment/History");
            }
            return View();
        }

        [Route("[action]")]
        public IActionResult PaymentReport()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Payment/History");
            }
            return View();
        }

        [Route("[action]")]
        public IActionResult History()
        {
            return View();
        }


        [Route("[action]")]
        public IActionResult FinancialYear()
        {
            return View();
        }
    }
}
