﻿using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Accounts
{
    [Route("[controller]")]
    public class PettyCashController : BaseController
    {
        private readonly ConfigHelper _configHelper;
        public PettyCashController(ConfigHelper configHelper) : base(configHelper)
        {
            _configHelper = configHelper;
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult Index()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }

        [Route("[action]")]
        public IActionResult List()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }
    }
}
