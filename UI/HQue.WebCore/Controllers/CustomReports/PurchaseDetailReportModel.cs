﻿using System.Collections.Generic;

namespace HQue.WebCore.Controllers.CustomReports
{
    public class PurchaseDetailReportModel
    {
        private List<ReportField> _fields;

        public PurchaseDetailReportModel()
        {
            _fields = new List<ReportField>();
        }

        public List<ReportField> Fields
        {
            get
            {
                _fields.Add(new ReportField() { id = "vendorpurchaseitem.PackageSize", label = "Package Size", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchaseitem.PackageQty", label = "Package Quantity", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchaseitem.PackagePurchasePrice", label = "Package Purchase Price", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchaseitem.PackageSellingPrice", label = "Package Selling Price", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchaseitem.Quantity", label = "Purchased Quantity", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchaseitem.PurchasePrice", label = "Cost per unit", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchaseitem.FreeQty", label = "Free Quantity", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchaseitem.Discount", label = "Discount on item", type = "double", size = 15 });

                return _fields;
            }
        }

        public string GetJoin(int ReportType)
        {
            return " inner join vendorpurchaseitem on vendorpurchaseitem.vendorpurchaseid = vendorpurchase.id "; ;
        }
    }
}
