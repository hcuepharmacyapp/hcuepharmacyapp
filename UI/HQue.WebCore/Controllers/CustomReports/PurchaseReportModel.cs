﻿using System.Collections.Generic;

namespace HQue.WebCore.Controllers.CustomReports
{
    public class PurchaseReportModel
    {
        private List<ReportField> _fields;

        public PurchaseReportModel()
        {
            _fields = new List<ReportField>();
        }


        public List<ReportField> Fields
        {
            get
            {
                _fields.Add(new ReportField() { id = "vendorpurchase.VendorId", label = "Vendor Id", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.InvoiceNo", label = "Invoice Number", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.InvoiceDate", label = "Invoice Date", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.VerndorOrderId", label = "Verndor Order Id", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.Discount", label = "Discount on invoice", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.Credit", label = "Credit balance", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.PaymentType", label = "Payment Type", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.ChequeNo", label = "Cheque No", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.Cheque Date", label = "Cheque Date", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.GoodsRcvNo", label = "Goods Receivable No.", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "vendorpurchase.Comments", label = "Invoice Comments", type = "string", size = 15 });
           
                return _fields;
            }



        }

        public List<ReportField> AllFields
        {
            get
            {
                var tmp = Fields;

                var fldlist = new PurchaseDetailReportModel().Fields;
                foreach (var f in fldlist)
                    _fields.Add(f);

                fldlist = new ProductStockReportModel().Fields;
                foreach (var f in fldlist)
                    _fields.Add(f);

                fldlist = new ProductReportModel().Fields;
                foreach (var f in fldlist)
                    _fields.Add(f);

                return _fields;
            }

        }

    }
}
