﻿using System.Collections.Generic;

namespace HQue.WebCore.Controllers.CustomReports
{
    public class SalesReportModel
    {
        private List<ReportField> _fields;
         
        public SalesReportModel()
        {
            _fields = new List<ReportField>();
        }

        
        public List<ReportField> Fields
        {
            get
            {
                _fields.Add(new ReportField() { id= "sales.PatientId", label="Patient Id", type="string", size=15 });
                _fields.Add(new ReportField() { id = "sales.InvoiceNo", label = "Invoice Number", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.InvoiceDate", label = "Invoice Date", type = "date", size = 5 });
                _fields.Add(new ReportField() { id = "sales.Name", label = "Patient Name", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.Mobile", label = "Patient Mobile", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.Email", label = "Patient email", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.DOB", label = "Patient Date of Birth", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "sales.Age", label = "Patient Age", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "sales.Gender", label = "Patient Gender", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.Address", label = "Patient address", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.Pincode", label = "Patient Pincode", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.DoctorName", label = "Doctor Name", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.DoctorMobile", label = "Doctor Mobile", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.PaymentType", label = "Payment Type", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.DeliveryType", label = "Delivery Type", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "sales.Credit", label = "Credit Balance", type = "double", size = 10 });
                _fields.Add(new ReportField() { id = "sales.Discount", label = "Discount on bill", type = "double", size = 10 });

                return _fields;
            }



        }

        public List<ReportField> AllFields
        {
            get
            {
                var tmp = Fields;

                var fldlist = new SalesDetailReportModel().Fields;
                foreach (var f in fldlist)
                    _fields.Add(f);

                fldlist = new ProductStockReportModel().Fields;
                foreach (var f in fldlist)
                    _fields.Add(f);

                fldlist = new ProductReportModel().Fields;
                foreach (var f in fldlist)
                    _fields.Add(f);

                return _fields;
            }

        }


    }
}
