﻿using System.Collections.Generic;

namespace HQue.WebCore.Controllers.CustomReports
{
    public class SalesDetailReportModel
    {
        private List<ReportField> _fields;

        public SalesDetailReportModel()
        {
            _fields = new List<ReportField>();
        }

        public List<ReportField> Fields
        {
            get
            {
                _fields.Add(new ReportField() { id = "salesitem.Quantity", label = "Quantity", type = "double", size = 15 });
                _fields.Add(new ReportField() { id = "salesitem.Discount", label = "Discount On Item", type = "double", size = 15 });
                return _fields;
            }
        }

        public string GetJoin(int ReportType)
        {
            return " inner join salesitem on salesitem.salesid = sales.id "; ;
        }

    }
}
