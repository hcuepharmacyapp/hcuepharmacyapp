﻿using System.Collections.Generic;

namespace HQue.WebCore.Controllers.CustomReports
{
    public class ProductReportModel
    {
        private List<ReportField> _fields;

        public ProductReportModel()
        {
            _fields = new List<ReportField>();
        }

        public List<ReportField> Fields
        {
            get
            {
                _fields.Add(new ReportField() { id = "product.Code", label = "Product Code", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.Name", label = "Product Name", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.Manufacturer", label = "Product Manufacturer", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.KindName", label = "Product Kind", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.StrengthName", label = "Product Strength", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.Type", label = "Product Type", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.Schedule", label = "Product Schedule", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.Category", label = "Product Category", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.GenericName", label = "Product Generic Name", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.CommodityCode", label = "Product Commodity Code", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "product.Packing", label = "Product Packing", type = "string", size = 15 });

                return _fields;
            }
        }

        public string GetJoin()
        {
            return " inner join product on productstock.ProductId = product.id ";
        }
    }
}
