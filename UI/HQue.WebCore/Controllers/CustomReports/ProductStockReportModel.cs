﻿using System.Collections.Generic;

namespace HQue.WebCore.Controllers.CustomReports
{
    public class ProductStockReportModel
    {
        private List<ReportField> _fields;

        public ProductStockReportModel()
        {
            _fields = new List<ReportField>();
        }

        public List<ReportField> Fields
        {
            get
            {
                _fields.Add(new ReportField() { id = "productstock.BatchNo", label = "Batch Number", type = "string", size = 15 });
                _fields.Add(new ReportField() { id = "productstock.ExpireDate", label = "Expire Date", type = "double", size = 5 });
                _fields.Add(new ReportField() { id = "productstock.VAT", label = "VAT", type = "double", size = 5 });
                _fields.Add(new ReportField() { id = "productstock.CST", label = "CST", type = "double", size = 5 });
                _fields.Add(new ReportField() { id = "productstock.SellingPrice", label = "MRP", type = "double", size = 5 });
                _fields.Add(new ReportField() { id = "productstock.Stock", label = "Stock", type = "double", size = 5 });
                
                return _fields;
            }
        }


        public string GetJoin(int ReportType)
        {
            var retVal = "";

            switch (ReportType)
            {
                case 1:
                    retVal = " inner join productstock on salesitem.ProductStockId = productstock.id ";
                    break;
                case 2:
                    retVal = " inner join productstock on vendorpurchaseitem.ProductStockId = productstock.id ";
                    break;
            }

            return retVal;
        }
    }
}
