﻿namespace HQue.WebCore.Controllers.CustomReports
{
    public class ReportField
    {
        public string id { get; set; }
        public string label { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public bool IsGrouped { get; set; }
        public string Aggregate { get; set; }
        public bool IsGrandTotal { get; set; }
    }

    public class ReportFieldA
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public bool IsGrouped { get; set; }
        public string Aggregate { get; set; }
        public bool IsShowTotal { get; set; }
    }
}
