﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HQue.WebCore.Controllers.CustomReports
{
    public class ReportGenerator
    {
        private static int _reportType = 1;

        public ReportGenerator()
        {
        }

        public static string GenerateReport(ReportData data)
        {
            if (data.selectedFields.Count <= 0)
                return "";

            _reportType = data.reportType;

            var tables = new List<string>();

            switch (_reportType)
            {
                case 1:
                    if (CheckColumnExists(data.selectedFields, "sales")) { tables.Add("sales"); }
                    if (CheckColumnExists(data.selectedFields, "salesitem")) { tables.Add("salesitem"); }
                    if (CheckColumnExists(data.selectedFields, "productstock")) { tables.Add("salesitem"); tables.Add("productstock"); }
                    if (CheckColumnExists(data.selectedFields, "product")) { tables.Add("salesitem"); tables.Add("productstock"); tables.Add("product"); }
                    break;
                case 2:
                    if (CheckColumnExists(data.selectedFields, "vendorpurchase")) { tables.Add("vendorpurchase"); }
                    if (CheckColumnExists(data.selectedFields, "vendorpurchaseitem")) { tables.Add("vendorpurchaseitem"); }
                    if (CheckColumnExists(data.selectedFields, "productstock")) { tables.Add("vendorpurchaseitem"); tables.Add("productstock"); }
                    if (CheckColumnExists(data.selectedFields, "product")) { tables.Add("vendorpurchaseitem"); tables.Add("productstock"); tables.Add("product"); }
                    break;

                case 3:
                    if (CheckColumnExists(data.selectedFields, "productstock")) { tables.Add("productstock"); }
                    if (CheckColumnExists(data.selectedFields, "product")) { tables.Add("productstock"); tables.Add("product"); }
                    break;
            }
               
            

            return GetSQL(data, tables.Distinct().ToList());
        }

        public static string GetSQL(ReportData data, List<string> tables)
        {
            StringBuilder sSQL = new StringBuilder();

            sSQL.AppendLine("SELECT ");

            var lastCol = data.selectedFields.Last().Id;
            var getInstanceId = "";
            foreach (var c in data.selectedFields)
            {
                sSQL.Append("      ");

                if (!c.IsGrouped || c.Aggregate == null)
                    sSQL.Append((c.Type != "date")? c.Id : string.Format(" CONVERT(VARCHAR(10), {0}, 105)", c.Id));
                else
                    sSQL.Append(string.Format("{0}({1})", c.Aggregate, c.Id));

                sSQL.Append(" AS [" + c.Id + "]");

                if (c.Id != lastCol)
                    sSQL.AppendLine(",");
            }
            sSQL.Append(" FROM ");
            
            for(int i =0; i < tables.Count; i++)
            {
                if (i == 0)
                {
                    sSQL.AppendLine(tables[i]);
                    if(tables[i]!= "product")
                    getInstanceId = tables[i];
                }

                if (i > 0)
                {
                    sSQL.AppendLine(GetJoinStatement(tables[i]));
                    if(getInstanceId=="")
                    {
                        getInstanceId = tables[i];
                    }
                }
            }

            if (!string.IsNullOrEmpty(data.filter))
            {
                sSQL.Append(" WHERE ");
                sSQL.AppendLine(data.filter);
                sSQL.AppendLine(" AND "+getInstanceId + ".InstanceId='##'");
            }
            else
            {                
                if (getInstanceId != "")
                {
                    sSQL.Append(" WHERE ");
                    sSQL.AppendLine(getInstanceId+".InstanceId='##'");
                }
               
            }

            if(data.selectedFields.Any(c=> c.IsGrouped == true))
            {
                sSQL.Append(" GROUP BY ");
                var aggrLst = data.selectedFields.Where(c => c.Aggregate == null).ToList();
                foreach (var c in aggrLst)
                {
                    //sSQL.Append(string.Format(" {0} ", grp.Id));
                    sSQL.Append((c.Type != "date") ? c.Id : string.Format(" CONVERT(VARCHAR(10), {0}, 105)", c.Id));

                    if (!aggrLst.Last().Equals(c))
                        sSQL.Append(", ");
                }
            }

            // needs attention rework
            var sql = sSQL.ToString().Replace("= '-", "= getdate() -");
            //sql = sql.ToString().Replace("#'", "");  //By Santhiyagu

            return sql;
        }


        private static bool CheckColumnExists(List<ReportFieldA> list, string tableName)
        {
            foreach(var f in list)
            {
                var str = f.Id.Split('.');
                if (str[0].Trim() == tableName)
                    return true;
            }
         
            return false;
        }


        private static string GetJoinStatement(string tableName)
        {
            var stmt = "";
            switch (tableName)
            {
                case "salesitem":
                    stmt = new SalesDetailReportModel().GetJoin(_reportType);
                    break;
                case "productstock":
                    stmt = new ProductStockReportModel().GetJoin(_reportType);
                    break;
                case "product":
                    stmt = new ProductReportModel().GetJoin();
                    break;
                case "vendorpurchaseitem":
                    stmt = new PurchaseDetailReportModel().GetJoin(_reportType);
                    break;
            }

            return stmt;
        }

    }
}
