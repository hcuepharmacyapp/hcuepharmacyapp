﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using HQue.Biz.Core.Report;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Reports;

namespace HQue.WebCore.Controllers.CustomReports
{
    [Route("[controller]")]
    public class CustomReportsController : BaseController
    {
        private readonly CustomReportManager _customReportManager;

        public CustomReportsController(CustomReportManager customReportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _customReportManager = customReportManager;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult CustomReportsList()
        {
            if (User.InstanceId() != "85d3fdc9-1426-4d3e-8a96-16c78088eaa8")
            {
                Response.Redirect("/CustomReports/List");
            }
            return View();
        }

        [Route("[action]")]
        public IActionResult IndexNew()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult UpdateReport(string id)
        {
            if (User.InstanceId() != "85d3fdc9-1426-4d3e-8a96-16c78088eaa8")
            {
                Response.Redirect("/CustomReports/List");
            }
            if (id != null)
            {
                ViewBag.Id = id;
            }          
            return View();
        }
        //[Route("[action]")]
        //public IActionResult IndexNew(string id)
        //{
        //    if (id != null)
        //    {
        //        ViewBag.Id = id;
        //    }
        //    return View();
        //}

        [Route("[action]")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult RenderReport()
        {
            return View();
        }

        [Route("[action]")]
        public List<ReportField> GetFilters(int type)
        {
            List<ReportField> retVal = null;

            switch (type)
            {
                case 1:
                    retVal = new SalesReportModel().AllFields;
                    break;
                case 2:
                    retVal = new PurchaseReportModel().AllFields;
                    break;
                case 3:
                    retVal = new InventoryReportModel().AllFields;
                    break;
            }


            return retVal;
        }


        [Route("[action]")]
        public Task<List<CustomReport>> GetReportList(string instanceId)
        {
            return _customReportManager.List(User.InstanceId());
        }

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<CustomDevReport> EditReportData(string id)
        {
            var result= await _customReportManager.EditReportData(id);

            return result;
        }

        [Route("[action]")]
        public async Task<string> SaveCustomReport([FromBody]ReportData data)
        {
            if (data == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return "No data to save";
            }
            if (string.IsNullOrEmpty(data.reportName))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return "Invalid report name to save";
            }

            var sql = ReportGenerator.GenerateReport(data);

            CustomReport customReport = new CustomReport();

            customReport.SetLoggedUserDetails(User);
            customReport.ReportName = data.reportName;
            customReport.Query = sql;
            customReport.ReportType = data.reportType.ToString();
            customReport.ConfigJSON = JsonConvert.SerializeObject(data);

            await _customReportManager.Save(customReport);

            return sql;
        }

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ReportResponse> RunReport([FromBody]ReportRequest req)
        {
            var rptConfig = _customReportManager.GetCustomReport(req.id);
            var config = JsonConvert.DeserializeObject<ReportData>(rptConfig.ConfigJSON);
            var resp = new ReportResponse();
            resp.reportName = rptConfig.ReportName;

            foreach (var f in config.selectedFields)
                resp.columns.Add(new Column() { field = f.Id.Replace(".", "_"), title = f.Label, type = "string", width = "100px" });

            resp.data = await _customReportManager.GetReportData(rptConfig.Query.Replace("##", User.InstanceId()));

            return resp;
        }

        [Route("[action]")]
        public IActionResult Settings()
        {
            return View();
        }
                
        [Route("[action]")]
        public async Task<string> SaveDevCustomReport([FromBody]CustomDevReport data)
        {
            if (data == null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return "No data to save";
            }
            if (string.IsNullOrEmpty(data.ReportName))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return "Invalid report name to save";
            }

            CustomDevReport customReport = new CustomDevReport();

            customReport.SetLoggedUserDetails(User);
            customReport.ReportName = data.ReportDevData.reportName= data.ReportName;
            customReport.Query = data.Query;
            customReport.ReportType =  "1";
            data.ReportDevData.reportType = 1;
            data.ReportDevData.selectedFields = data.selectedFields;
            customReport.FilterJSON = JsonConvert.SerializeObject(data.FilterJSON);

            customReport.ConfigJSON = JsonConvert.SerializeObject(data.ReportDevData);

            await _customReportManager.Save(customReport);

            return data.Query;
        }

        [Route("[action]")]
        public async Task<string> UpdateDevCustomReport([FromBody]CustomDevReport data)
        {
            if (data == null || data.Query==null)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return "No data to save";
            }
            if (string.IsNullOrEmpty(data.ReportName))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return "Invalid report name to save";
            }

            CustomDevReport customReport = new CustomDevReport();

            customReport.SetLoggedUserDetails(User);
            customReport.ReportName = data.ReportDevData.reportName = data.ReportName;
            customReport.Query = data.Query;
            customReport.ReportType = "1";
            data.ReportDevData.reportType = 1;
            data.ReportDevData.selectedFields = data.selectedFields;
            customReport.Id =data.Id;
            customReport.FilterJSON = JsonConvert.SerializeObject(data.FilterJSON);

            customReport.ConfigJSON = JsonConvert.SerializeObject(data.ReportDevData);

            await _customReportManager.UpdateReport(customReport);

            return data.Query;
        }

        [Route("[action]")]
        public Task<List<CustomDevReport>> GetDevReportList(string instanceId)
        {
            return _customReportManager.GetDevReportList(User.InstanceId());
        }

        [Route("[action]")]
        public IActionResult RenderDevReport(string id)
        {
            if(id!=null)
            {
                ViewBag.Id = id;
            }
            return View();
        }

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ReportResponse> RunDevReport([FromBody]ReportDevRequest req)
        {
            var rptConfig = _customReportManager.GetDevCustomReport(req.id);
            var config = JsonConvert.DeserializeObject<ReportData>(rptConfig.ConfigJSON);
            var configFilter = JsonConvert.DeserializeObject(rptConfig.FilterJSON);
            var resp = new ReportResponse();
            resp.reportName = rptConfig.ReportName;
           // resp.data.Add(configFilter);
            foreach (var f in config.selectedFields)
                resp.columns.Add(new Column() { field = f.Id.Replace(" ", ""), title = f.Label, type = "string", width = "100px" });

            //resp.data = await _customReportManager.GetReportData(rptConfig.Query.Replace("##", User.InstanceId()));
            if(req.instanceId!="" && req.instanceId!=null)
            {
                req.instanceId = req.instanceId;
            }
            else
            {
                req.instanceId = User.InstanceId();
            }
            resp.data = await _customReportManager.GetdevReportData(rptConfig.Query.Replace("[##]", "'" + req.instanceId + "'").Replace("[#dt@From#]", "'"+req.fromDate+"'").Replace("[#dt@To#]", "'" + req.toDate + "'").Replace("[#select@Vendor#]", "'" + req.vendor + "'").Replace("[#select@Product#]", "'" + req.productId + "'"));
            ViewBag.Title1 = resp.reportName;
            return resp;
        }

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ReportResponse> RunDevFilter([FromBody]ReportRequest req)
        {
            var rptConfig = _customReportManager.GetDevCustomReport(req.id);
            var configFilter = JsonConvert.DeserializeObject(rptConfig.FilterJSON);
            var resp = new ReportResponse();
            resp.reportName = rptConfig.ReportName;
            resp.data.Add(configFilter);
            return resp;
        }
        
    }

    public class ReportRequest
    {
        public string id { get; set; }
    }

    public class ReportDevRequest
    {
        public string id { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string vendor { get; set; }
        public string productId { get; set; }
        public string instanceId { get; set; }
    }

    public class ReportResponse
    {
        public ReportResponse()
        {
            this.columns = new List<Column>();
            this.data = new JArray();
        }

        public string reportName { get; set; }
        public List<Column> columns { get; set; }
        public JArray data { get; set; }
        //public string data { get; set; }
    }

    public class ReportData
    {
        public List<ReportFieldA> selectedFields { set; get; }
        public string filter { set; get; }
        public string reportName { set; get; }
        public int reportType { set; get; }
    }

   
    public class Column
    {
        public Column()
        {
            //this.attributes = new JProperty("class", "text-right field-report");
        }
        public string field { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string width { get; set; }
        public JProperty attributes { get; set; }
    }



}
