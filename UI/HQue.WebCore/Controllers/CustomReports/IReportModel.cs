﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.WebCore.Controllers.CustomReports
{
    public interface IReportModel
    {
        List<ReportField> Fields { get; }

        List<ReportField> AllFields { get; }
    }
}
