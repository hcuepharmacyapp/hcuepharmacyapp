﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.WebCore.Controllers.CustomReports
{
    public class InventoryReportModel : IReportModel
    {
        private List<ReportField> _fields;

        public InventoryReportModel()
        {
            _fields = new List<ReportField>();
        }

        public List<ReportField> AllFields
        {
            get
            {
                var allFields = new List<ReportField>();

                foreach (var f in new ProductStockReportModel().Fields)
                    _fields.Add(f);

                foreach (var f in new ProductReportModel().Fields)
                    _fields.Add(f);

                return Fields;
            }   
        }

        public List<ReportField> Fields
        {
            get
            {
                 return _fields;

            }
        }
    }
}
