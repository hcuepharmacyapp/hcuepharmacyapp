﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Biz.Core.Sync;
using Utilities.Helpers;
using System.IO;
using Utilities.Helpers;
using System.Globalization;
using HQue.Biz.Core.UserAccount;
using HQue.Biz.Core.Master;
using HQue.Biz.Core.Inventory;
// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.WebCore.Controllers
{
    public class SyncData : BaseController
    {

        private readonly SyncDataManager _syncDataManager;
        private readonly UserManager _userManager;
        protected readonly ConfigHelper _configHelper;
        private readonly ProductManager _productManager;
        private readonly ProductStockManager _productStockManager;
        public SyncData(SyncDataManager syncDataManager, ConfigHelper configHelper, ProductManager productManager, ProductStockManager productStockManager) : base(configHelper)
        {
            _syncDataManager = syncDataManager;
            _configHelper = configHelper;
            _productManager = productManager;
            _productStockManager = productStockManager;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }


        public async Task<ActionResult> GetOnlineData()
        {
            var InstanceId = User.InstanceId();
            if (_configHelper.AppConfig.OfflineMode && User.IsHQueToolUser())
            {
                InstanceId = await _productStockManager.GetProductStockInstanceId();
            }
            var data = await _syncDataManager.ExportOnlineData(InstanceId);

            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(data);
            writer.Flush();
            stream.Position = 0;

            //FileInfo info = new FileInfo(dataFile);
            HttpContext.Response.ContentType = "application/notepad";
            return File(stream, "application/notepad", string.Format("hq_db_{0}.txt", InstanceId));
        }

        public async Task<string> CheckProductStockInstanceId()
        {
            var InstanceId = User.InstanceId();
            if (_configHelper.AppConfig.OfflineMode && User.IsHQueToolUser())
            {
                InstanceId = await _productStockManager.GetProductStockInstanceId();
                if (string.IsNullOrEmpty(InstanceId))
                {
                    return "Branch Not Found";
                }
            }
            return InstanceId;
        }

        public async Task<string> CheckSyncDataExist()
        {
            string tst = System.Environment.GetEnvironmentVariable("ProgramFiles") + "\\hcue\\syncData";

            string data = "";

            if (Directory.GetFiles(tst).Any())
            {
                data = "Failed";
            }
            else
            {
                data = "Success";
            }

            return data;
        }

        public int CheckSyncDataExistCount()
        {
            string tst = System.Environment.GetEnvironmentVariable("ProgramFiles") + "\\hcue\\syncData";
            
            if (Directory.Exists(tst))
                return Directory.GetFiles(tst).Count();
            else
            {
                tst = System.Environment.GetEnvironmentVariable("ProgramFiles(x86)") + "\\hcue\\syncData";
                if (Directory.Exists(tst))
                    return Directory.GetFiles(tst).Count();
                else
                    return 0;
            }
        }

        public async Task<bool> CheckFirstTimeAutoSync()
        {
            if (_configHelper.AppConfig.OfflineMode)
            {
                //Dangerous part - start
                //Dont add AccountId as param in CheckProductExist method here, 
                //it will do auto sync for support login at customer place
                //and customer data will be flushed
                var checkDataCount = await _productManager.CheckProductExist(null);
                //Dangerous part - end

                if (checkDataCount == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<ActionResult> GetOnlinePatientData()
        {
            var data = await _syncDataManager.GetOnlinePatientData(User.AccountId(), User.InstanceId());
            //var data = await _syncDataManager.ExportOnlineData(User.InstanceId());

            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(data);
            writer.Flush();
            stream.Position = 0;

            //FileInfo info = new FileInfo(dataFile);
            HttpContext.Response.ContentType = "application/notepad";
            return File(stream, "application/notepad", string.Format("hq_db_{0}.txt", User.InstanceId()));
        }
        public async Task<int> ValidateLogtime()
        {
            int nResult = 0;
            if (User.LoggedinDate() != null)
            {
                //if (DateTime.ParseExact(User.LoggedinDate().ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) != DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture)
                //    ) 
                if (User.LoggedinDate().ToString() != DateTime.Now.ToString("dd/MM/yyyy"))
                {

                    //_configHelper.HttpContextAccessor.HttpContext.Request.Cookies.Keys.Add("Logout");
                    var OfflineStatus = false;
                    OfflineStatus = ConfigHelper.AppConfig.OfflineMode;
                    await HttpContext.Authentication.SignOutAsync("Cookies");
                    await _userManager.logOutHistory(User.AccountId(), User.InstanceId(),User.UserId(), OfflineStatus);
                    RedirectToAction("Login");

                    nResult = 1;

                }
            }
            return nResult;
        }

    }
    }
