﻿using System.Collections.Generic;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Printing
{
    public class UploadToBlobStorageAsJson 
    {
        private const string CONTENT_TYPE = "application/json";

        private readonly object obj;
        private readonly string containerPath;
        private readonly string blobAddressUri;
        private readonly Dictionary<string, string> metadata;
        private readonly ConfigHelper _configHelper;
        private string json;
        private string v1;
        private string v2;
        private Dictionary<string, string> dictionary;

        public UploadToBlobStorageAsJson(object obj,
                                            string containerPath,
                                            string blobAddressUri,
                                            ConfigHelper configHelper,
                                            Dictionary<string, string> metadata)
        {
            this.obj = obj;
            this.containerPath = containerPath;
            this.blobAddressUri = blobAddressUri;
            this.metadata = metadata;
            this._configHelper = configHelper;
        }

        public UploadToBlobStorageAsJson(string json, string v1, string v2, Dictionary<string, string> dictionary)
        {
            this.json = json;
            this.v1 = v1;
            this.v2 = v2;
            this.dictionary = dictionary;
        }

        public void Apply(CloudStorageAccount model)
        {
            var client = model.CreateCloudBlobClient();

            var container = client.GetContainerReference(containerPath);

            //if (!container.Exists())
            //    container.Create();

            var blobReference = container.GetBlockBlobReference(blobAddressUri);

            var blockBlob = blobReference;
            UploadToContainer(blockBlob);
        }

        private void UploadToContainer(CloudBlockBlob blockBlob)
        {
            SetBlobProperties(blockBlob);

            using (var ms = new MemoryStream())
            {
                LoadStreamWithJson(ms);
                //blockBlob.UploadFromStream(ms);
            }
        }

        private void SetBlobProperties(CloudBlockBlob blobReference)
        {
            blobReference.Properties.ContentType = CONTENT_TYPE;
            foreach (var meta in metadata)
            {
                blobReference.Metadata.Add(meta.Key, meta.Value);
            }
        }

        private void LoadStreamWithJson(Stream ms)
        {
            var json = JsonConvert.SerializeObject(obj);
            StreamWriter writer = new StreamWriter(ms);
            writer.Write(json);
            writer.Flush();
            ms.Position = 0;
        }

        private string GetConnectionString()
        {
            return _configHelper.AppConfig.StorageConnection;
        }

        public string SaveJsonTemplate(string json)
        {
            var uploadToBlobStorage = new UploadToBlobStorageAsJson(json,
                                                "test",
                                                "testObject",
                                                new Dictionary<string, string>());

            var storageAccount = CloudStorageAccount.Parse(GetConnectionString());

            uploadToBlobStorage.Apply(storageAccount);

            return json;
        }
    }
}
