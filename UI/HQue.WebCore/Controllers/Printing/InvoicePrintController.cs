﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using HQue.WebCore.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Utilities.Report;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using HQue.Biz.Core.Inventory;
using HQue.Contract.Infrastructure.Settings;
using System.Linq;

namespace HQue.WebCore.Controllers.Printing
{
    [Route("[controller]")]
    public class InvoicePrintController : BaseController
    {
        private readonly SalesManager _salesManager;
        private readonly IHostingEnvironment _environment;
        private readonly ConfigHelper _configHelper;
        private int sno = 1;
        private decimal? totalQuantity = 0;

        public InvoicePrintController(SalesManager salesManager, IHostingEnvironment environment, ConfigHelper configHelper) : base(configHelper)
        {
            _salesManager = salesManager;
            _environment = environment;
            _configHelper = configHelper;
        }

        /// <summary>
        /// Regular bill template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<IActionResult> Index(string id = null)
        {
            var model = new InvoiceVM();
            if (!string.IsNullOrEmpty(id))
            {
                model.Sales = await _salesManager.GetSalesDetails(id);

            }

            return View(model);
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<string> UploadStationary(IFormFile file)
        {
            var fileName = "";

            if (!string.IsNullOrEmpty(Convert.ToString(file)))
            {
                var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
                fileName = Guid.NewGuid().ToString() + '_' + parsedContentDisposition.FileName.Trim('"');
                fileName = fileName.Replace(" ", ""); //Added by Sarubala on 11-06-19 to remove space in filename
                var filePath = Path.Combine(_environment.WebRootPath, "uploads\\templates", fileName);
                using (FileStream fs = System.IO.File.Create(filePath))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
                //await file.SaveAsAsync(filePath);
            }


            return fileName;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<TemplateData> GetTemplate(string t)
        {
            var getTemplate = await _salesManager.GetCustomTemplate(User.AccountId(), User.InstanceId());

            return JsonConvert.DeserializeObject<TemplateData>(getTemplate);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task SaveTemplate([FromBody]dynamic templateData)
        {
            if (templateData == null)
                return;

            SaleSettings saleSettings = new SaleSettings();
            saleSettings.SetLoggedUserDetails(User);

            TemplateData data = JsonConvert.DeserializeObject<TemplateData>(templateData.ToString());

            saleSettings.CustomTempJSON = JsonConvert.SerializeObject(data);
            await _salesManager.SaveCustomTemplate(saleSettings);
            //await HttpContext.Response.WriteAsync("Success", null);
        }
        /// <summary>
        /// Work around solution for dotmatrix 4Inch generic template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("[action]")]
        public async Task<ActionResult> printTemplate(string id = null, string templateFile = null)
        {
            //TemplateData tdata = JsonConvert.DeserializeObject<TemplateData>(templateData);

            //TemplateData tdata  = await ReadAzureBlob();

            var getTemplate = await _salesManager.GetCustomTemplate(User.AccountId(), User.InstanceId());
            if (getTemplate == "")
            {
                return RedirectToAction("PrintTemplate", "Sales");
                //return Red("/Sales/PrintTemplate");                
            }
            TemplateData tdata = JsonConvert.DeserializeObject<TemplateData>(getTemplate);

            var model = new InvoiceVM();
            if (!string.IsNullOrEmpty(id))
            {
                model.Sales = await _salesManager.GetSalesDetails(id);
                model.Sales = await SalesItemAmount(model.Sales);
                model.Sales.InvoiceSeries = Convert.ToString(model.Sales.Prefix) + Convert.ToString(model.Sales.InvoiceSeries); //Addedby Poongodi on 19/06/2017

                //Added by Sarubala on 08-11-17
                if (model.Sales.PaymentType == "Multiple" && model.Sales.SalesPayments.Count == 1 && model.Sales.SalesPayments.First().PaymentInd == 5)
                {
                    model.Sales.PaymentType = "eWallet";
                }
            }

            //string name = string.Format("./temp/salebill_{0}.txt", User.InstanceId());
            string name = string.Format("./wwwroot/temp/salebill_{0}.txt", User.InstanceId());

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(GenerateReport(model, tdata), false, Encoding.UTF8);

            }

            HttpContext.Response.ContentType = "application/notepad";
            if (tdata.PaperType == "CUSTOM")
            {
                return File(info.OpenRead(), "application/notepad", string.Format("hq_PP_{0}.txt", model.Sales.InvoiceNo));
            }
            else
            {
                return File(info.OpenRead(), "application/notepad", string.Format("hq_{0}.txt", model.Sales.InvoiceNo));
            }

        }



        /// <summary>
        /// View InvoiceDetails 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="templateFile"></param>
        /// <returns></returns>
        [Route("[action]")]
        //View template method added by Poongodi to viewthe Sale item details
        public async Task<ActionResult> viewTemplate(string id = null, string templateFile = null)
        {
            //TemplateData tdata = JsonConvert.DeserializeObject<TemplateData>(templateData);

            //TemplateData tdata  = await ReadAzureBlob();

            var getTemplate = await _salesManager.GetCustomTemplate(User.AccountId(), User.InstanceId());
            if (getTemplate == "")
            {
                return RedirectToAction("PrintTemplate", "Sales");
                //return Red("/Sales/PrintTemplate");                
            }
            TemplateData tdata = JsonConvert.DeserializeObject<TemplateData>(getTemplate);

            var model = new InvoiceVM();
            if (!string.IsNullOrEmpty(id))
            {
                model.Sales = await _salesManager.GetSalesDetails(id);
                model.Sales.InvoiceSeries = Convert.ToString(model.Sales.Prefix) + Convert.ToString(model.Sales.InvoiceSeries);
            }

            //string name = string.Format("./temp/salebill_{0}.txt", User.InstanceId());
            string name = string.Format("./wwwroot/temp/salebill_{0}.txt", User.InstanceId());

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(GenerateReport(model, tdata), false, Encoding.UTF8);

            }

            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("view_hq_PP_{0}.txt", model.Sales.InvoiceNo));

        }

        #region Generic _template
        private string GenerateReport(InvoiceVM data, TemplateData templateData)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = templateData.Columns;
            rpt.Rows = 30;

            //if(templateData.PaperType=="CUSTOM")
            // rpt.Content.Append("#F" + templateData.FontSize + "#");
            if (!data.IsReturnsOnly)
                WriteReportDetails(data, templateData, rpt);
            else
                WriteReturnReportDetails(data, templateData, rpt);

            return rpt.Content.ToString();
        }
        //Added by Bikas on 15-05-18
        private async Task<IInvoice> SalesItemAmount(IInvoice data)
        {
            data.TotalMRPValue = 0;
          
            foreach (IInvoiceItem item in data.InvoiceItems)
            {
                
                var price = item.MRP * item.Quantity;
                if (item.ReturnedQty != null && item.ReturnedQty < 0)
                {
                    price = item.ProductStock.MRP * item.ReturnedQty;
                }
                data.TotalMRPValue += Convert.ToDecimal(price);
            }
            return data;
        }

        private int maxLineNo = 0;

        private void WriteReportHeader(InvoiceVM data, TemplateData templateData, PlainTextReport rpt)
        {
            ///rpt.NewLine();
            Debug.WriteLine("New Line");
            foreach (LayoutData l in GetHeaderList(templateData))
            {
                for (int i = maxLineNo; maxLineNo < l.LineNo; i++)
                {
                    Debug.WriteLine("New Line");
                    rpt.NewLine();
                    maxLineNo++;
                }
                Debug.WriteLine(l.FieldName);
                if (l.FieldType > 0)
                    rpt.Write(l.Text, l.Width, l.Offset, (Alignment)l.TextAlign);
                else
                    rpt.Write(GetReportHeaderData(data, l.FieldName), l.Width, l.Offset, (Alignment)l.TextAlign);
            }

            rpt.Write();
        }

        private void WriteReportDetails(InvoiceVM data, TemplateData templateData, PlainTextReport rpt)
        {

            GetFooterList(templateData);
            var nFirstLine = GetFirstLineInSection(templateData, 1);
            var nLastLine = GetLastLineInSection(templateData, 1);

            WriteReportHeader(data, templateData, rpt);

            int len = 0;
            foreach (var item in data.Sales.InvoiceItems)
            {
                if (item.Total == 0)
                {
                    //break; // Commented by San
                    item.Quantity = item.ReturnedQty;
                    var xyz = data.Sales.SalesReturnItem.Where(x => x.ProductStockId == item.ProductStock.Id).FirstOrDefault();
                    if (xyz != null)
                    {
                        item.Cgst = xyz.Cgst;
                        item.Sgst = xyz.Sgst;
                        item.Igst = xyz.Igst;
                        item.GstTotal = xyz.GstTotal;
                        item.GstTotalValue = xyz.Quantity * (xyz.SellingPrice == null ? xyz.ProductStock.SellingPrice : xyz.SellingPrice);
                    }
                }
                bool bPageBreak = false;

                foreach (LayoutData l in GetDetailsList(templateData))
                {
                    for (int i = maxLineNo; maxLineNo < l.LineNo; i++)
                    {
                        rpt.NewLine();
                        maxLineNo++;
                    }

                    // if (maxLineNo >= nFirstLine - 1) 
                    if ((maxLineNo >= nFirstLine - 1) && templateData.PaperType == "CUSTOM")
                    {
                        bPageBreak = true;
                        break;
                    }

                    Debug.WriteLine(string.Format("{0} , {1} , {2} ", l.FieldName, l.Width, l.Offset));
                    rpt.Write(GetReportDetailData(item, l.FieldName), l.Width, l.Offset, (Alignment)l.TextAlign);
                }


                if (bPageBreak)
                {
                    rpt.Write();
                    WriteReportFooter(data, templateData, rpt, true);
                    rpt.NewLine();
                    maxLineNo = 0;
                    len = 0;
                    WriteReportHeader(data, templateData, rpt);
                    maxLineNo++;

                    rpt.NewLine();
                    foreach (LayoutData l in GetDetailsList(templateData))
                    {
                        for (int i = maxLineNo; maxLineNo < l.LineNo; i++)
                        {
                            rpt.NewLine();
                            maxLineNo++;
                        }

                        if (maxLineNo >= nFirstLine - 1)
                        {
                            bPageBreak = true;
                            break;
                        }

                        Debug.WriteLine(string.Format("{0} , {1} , {2} ", l.FieldName, l.Width, l.Offset));
                        rpt.Write(GetReportDetailData(item, l.FieldName), l.Width, l.Offset, (Alignment)l.TextAlign);
                    }
                }

                rpt.Write();
                rpt.NewLine();
                maxLineNo++;
                len++;
            }

            if (templateData.PaperType == "CUSTOM")
            {
                for (int i = 0; i < 5 - len; i++)
                {
                    rpt.NewLine();
                    maxLineNo++;
                }
            }

            rpt.Write();

            WriteReportFooter(data, templateData, rpt, false);

        }

        private void WriteReturnReportDetails(InvoiceVM data, TemplateData templateData, PlainTextReport rpt)
        {

            GetFooterList(templateData);
            var nFirstLine = GetFirstLineInSection(templateData, 1);
            var nLastLine = GetLastLineInSection(templateData, 1);

            WriteReportHeader(data, templateData, rpt);

            int len = 0;
            foreach (var item in data.Sales.SalesReturnItem)
            {
                //if (item.Total == 0)
                //{
                //    item.Quantity = item.ReturnedQty;
                //}

                if (item.Quantity <= 0)  //Added by Sarubala to skip 0 quantity in cutom template print on 19/06/17
                {
                    continue;
                }

                item.Quantity = item.Quantity * (-1);
                bool bPageBreak = false;

                foreach (LayoutData l in GetDetailsList(templateData))
                {
                    for (int i = maxLineNo; maxLineNo < l.LineNo; i++)
                    {
                        rpt.NewLine();
                        maxLineNo++;
                    }

                    // if (maxLineNo >= nFirstLine - 1) 
                    if ((maxLineNo >= nFirstLine - 1) && templateData.PaperType == "CUSTOM")
                    {
                        bPageBreak = true;
                        break;
                    }

                    Debug.WriteLine(string.Format("{0} , {1} , {2} ", l.FieldName, l.Width, l.Offset));
                    rpt.Write(GetReportDetailData2(item, l.FieldName), l.Width, l.Offset, (Alignment)l.TextAlign);
                }


                if (bPageBreak)
                {
                    rpt.Write();
                    WriteReportFooter(data, templateData, rpt, true);
                    rpt.NewLine();
                    maxLineNo = 0;
                    len = 0;
                    WriteReportHeader(data, templateData, rpt);
                    maxLineNo++;

                    rpt.NewLine();
                    foreach (LayoutData l in GetDetailsList(templateData))
                    {
                        for (int i = maxLineNo; maxLineNo < l.LineNo; i++)
                        {
                            rpt.NewLine();
                            maxLineNo++;
                        }

                        if (maxLineNo >= nFirstLine - 1)
                        {
                            bPageBreak = true;
                            break;
                        }

                        Debug.WriteLine(string.Format("{0} , {1} , {2} ", l.FieldName, l.Width, l.Offset));
                        rpt.Write(GetReportDetailData2(item, l.FieldName), l.Width, l.Offset, (Alignment)l.TextAlign);
                    }
                }

                rpt.Write();
                rpt.NewLine();
                maxLineNo++;
                len++;
            }

            for (int i = 0; i < 5 - len; i++)
            {
                rpt.NewLine();
                maxLineNo++;
            }

            rpt.Write();

            WriteReportFooter(data, templateData, rpt, false);

        }

        private void WriteReportFooter(InvoiceVM data, TemplateData templateData, PlainTextReport rpt, bool isPageBreak)
        {
            Debug.WriteLine("New Line");
            foreach (LayoutData l in GetFooterList(templateData))
            {
                if (maxLineNo > l.LineNo)
                    maxLineNo = l.LineNo - 1;

                for (int i = maxLineNo; maxLineNo < l.LineNo; i++)
                {
                    rpt.NewLine();
                    maxLineNo++;
                }
                Debug.WriteLine(l.FieldName);
                if (l.FieldType > 0)
                {
                    if (!l.Text.Contains("-------"))
                    {
                        rpt.Write(l.Text, l.Width, l.Offset, (Alignment)l.TextAlign);
                    }
                    else
                    {
                        //if (templateData.PaperType == "CUSTOM")
                        rpt.NewLine();
                        rpt.Write(l.Text, rpt.Columns);
                        if (templateData.PaperType == "CUSTOM")
                            rpt.WriteLine();
                    }


                    //if(templateData.PaperType != "CUSTOM" && l.Text != null && l.Text.Contains("-------"))
                    // rpt.NewLine();

                }
                else
                {
                    if (!isPageBreak)
                    {
                        if (!data.IsReturnsOnly)
                            rpt.Write(GetReportFooterData(data, l.FieldName), l.Width, l.Offset, (Alignment)l.TextAlign);
                        else
                            rpt.Write(GetReportFooterData2(data, l.FieldName), l.Width, l.Offset, (Alignment)l.TextAlign);

                        //if (templateData.PaperType != "CUSTOM")
                        //    rpt.NewLine();
                    }
                    else
                    {
                        rpt.Write("Continue...", l.Width, l.Offset, (Alignment)l.TextAlign);
                        rpt.NewLine();
                    }
                        
                }
                //if (templateData.PaperType != "CUSTOM" && l.Text != null && l.Text.Contains("-------"))
                //rpt.NewLine();

            }

            rpt.Write();
        }

        private int GetLastLineInSection(TemplateData templateData, int section)
        {
            var retVal = 0;
            foreach (LayoutData item in templateData.DataItems)
            {
                if (item.Section == section)
                {
                    if (item.LineNo > retVal)
                        retVal = item.LineNo;
                }
            }

            return retVal;
        }

        private int GetFirstLineInSection(TemplateData templateData, int section)
        {
            var retVal = 9999;
            foreach (LayoutData item in templateData.DataItems)
            {
                if (item.Section == section)
                {
                    if (item.LineNo < retVal)
                        retVal = item.LineNo;
                }
            }

            return retVal;
        }
        private List<LayoutData> GetHeaderList(TemplateData templateData)
        {
            List<LayoutData> retVal = new List<LayoutData>();

            string[] header = new string[] { "Pharmacy Name",
                                                "Address",
                                                "Building Name",
                                                "Street Name",
                                                "Area",
                                                "Pharmacy Mobile",
                                                "Landline Number",
                                                "City",
                                                "Drug license number",
                                                "Tin number",
                                                "GSTIN",
                                                "CST number",
                                                "Doctor name",
                                                "Doctor Mobile",
                                                "Doctor Address",
                                                "Patient name",
                                                "Patient Mobile",
                                                "Patient GSTin",
                                                "Patient Address",
                                                "Patient Pan",
                                                "Patient DrugLicenseNo",
                                                "Patient ShortName",
                                                "Patient City",
                                                "Patient Pincode",
                                                "Bill Number",
                                                "Bill Time",
                                                "PaymentType",
                                                "Cheque",
                                                "Cheque Date",
                                                "Card Number",
                                                "Card Date",
                                                "UserName",
                                                "Transaction Date",
                                                "ID number",
                                                "HEADER"};

            foreach (LayoutData h in templateData.DataItems)
                if (Array.IndexOf(header, h.FieldName) >= 0 || h.Section == 0)
                    retVal.Add(h);


            foreach (LayoutData h in retVal)
                Debug.Write(h.FieldName);

            retVal.Sort(delegate (LayoutData data1, LayoutData data2)
            {
                return data1.Offset.CompareTo(data2.Offset);
            });

            retVal.Sort(delegate (LayoutData data1, LayoutData data2)
            {
                return data1.LineNo.CompareTo(data2.LineNo);
            });



            Debug.WriteLine("");
            foreach (LayoutData h in retVal)
                Debug.Write(h.FieldName);

            return retVal;
        }

        private List<LayoutData> GetDetailsList(TemplateData templateData)
        {
            List<LayoutData> retVal = new List<LayoutData>();

            string[] details = new string[] { "SNo","Product Name","Manufacturer","RackNo","BoxNo",
                                                "Batch No",
                                                "Expire Date",
                                                "Quantity",
                                                "Purchase Price",
                                                "Price",
                                                "Price/unit WOT",
                                                "Mrp/Unit",
                                                "Selling Price/Unit",
                                                "Mrp/Strip",
                                                "Selling/Strip",
                                                "MRP after Discount",
                                                "Selling Price after Discount",
                                                "MRP Value after Discount",
                                                "Selling Price Value after Discount",
                                                "IGST%",
                                                "IGSTValue",
                                                "CGST%",
                                                "CGSTValue",
                                                "SGST%",
                                                "SGSTValue",
                                                "GSTTotal",
                                                "HsnCode",
                                                "Discount %",
                                                "Discount Amount",
                                                "VAT %",
                                                "Vat Amount",
                                                "Unit/Strip",
                                                "GSTAmount",
                                                "Amount","Amount Without GST"};

            foreach (LayoutData h in templateData.DataItems)
                if (Array.IndexOf(details, h.FieldName) >= 0)
                    retVal.Add(h);

            foreach (LayoutData h in retVal)
                Debug.Write(h.FieldName);

            retVal.Sort(delegate (LayoutData data1, LayoutData data2)
            {
                return data1.Offset.CompareTo(data2.Offset);
            });

            retVal.Sort(delegate (LayoutData data1, LayoutData data2)
            {
                return data1.LineNo.CompareTo(data2.LineNo);
            });

            return retVal;
        }

        private List<LayoutData> GetFooterList(TemplateData templateData)
        {
            List<LayoutData> retVal = new List<LayoutData>();

            string[] details = new string[] { "Gross Total",
                                                "Gross Value",
                                                "Discount",
                                                "Total Tax Amt",
                                                "Round Off",
                                                "Net Amount",
                                                "Net Amt WO Discount",
                                                "MRP Total",
                                                "Savings Amount",
                                                "Amount in Words",
                                                "Payment Type",
                                                "Total Items",
                                                "Total Quantity",
                                                "Cheque No",
                                                "ChequeDate",
                                                "CardNumber",
                                                "CardDate",
                                                "Return Disc",
                                                "Return Charges",
                                                "Return Amount",
                                                "Payable Amount",
                                                "IGST Total",
                                                "CGST Total",
                                                "SGST Total",
                                                "GSTTotal Amount",
                                                "GST Total%",
                                                "FOOTER" ,
                                                "User Name",
                                                "Cess Amount",
                                                "Net Amount with Cess",};

            foreach (LayoutData h in templateData.DataItems)
                if (Array.IndexOf(details, h.FieldName) >= 0 || h.Section == 1)
                    retVal.Add(h);

            foreach (LayoutData h in retVal)
                h.Section = 1;

            retVal.Sort(delegate (LayoutData data1, LayoutData data2)
            {
                return data1.Offset.CompareTo(data2.Offset);
            });

            retVal.Sort(delegate (LayoutData data1, LayoutData data2)
            {
                return data1.LineNo.CompareTo(data2.LineNo);
            });

            return retVal;
        }

        private string GetReportHeaderData(InvoiceVM data, string FieldName)
        {
            var result = "";


            switch (FieldName)
            {
                case "Pharmacy Name":
                    result = data.Sales.Instance.Name;
                    break;
                case "Address":
                    result = data.Sales.Instance.FullAddress;
                    break;
                case "Building Name":
                    result = data.Sales.Instance.BuildingName;
                    break;
                case "Street Name":
                    result = data.Sales.Instance.StreetName;
                    break;
                case "Area":
                    result = data.Sales.Instance.Area;
                    break;
                case "Pharmacy Mobile":
                    result = data.Sales.Instance.Mobile;
                    break;
                case "Landline Number":
                    result = data.Sales.Instance.Phone;
                    break;
                case "City":
                    result = data.Sales.Instance.City;
                    break;
                case "Drug license number":
                    result = data.Sales.Instance.DrugLicenseNo;
                    break;
                case "Tin number":
                    result = data.Sales.Instance.TinNo;
                    break;
                case "CST number":
                    result = data.Sales.Instance.TinNo;
                    break;
                case "GSTIN":
                    result = data.Sales.Instance.GsTinNo;
                    break;
                case "Doctor name":
                    result = data.Sales.DoctorName.ToUpper();
                    break;
                case "Doctor Mobile":
                    result = data.Sales.DoctorMobile;
                    break;
                case "Patient name":
                    result = data.Sales.Name.ToUpper();
                    break;
                case "Patient Mobile":
                    result = data.Sales.Mobile;
                    break;
                case "Patient GSTin":
                    result = data.Sales.Patient.GsTin;
                    break;
                case "Patient Address":
                    result = data.Sales.Address;
                    break;
                case "Patient Pan":
                    result = data.Sales.Patient.Pan;
                    break;
                case "Patient DrugLicenseNo":
                    result = data.Sales.Patient.DrugLicenseNo;
                    break;
                case "Patient ShortName":
                    result = data.Sales.Patient.ShortName;
                    break;
                case "Patient City":
                    result = data.Sales.Patient.City;
                    break;
                case "Patient Pincode":
                    result = data.Sales.Pincode;
                    break;
                case "Bill Number":
                    result = data.Sales.InvoiceSeries + data.Sales.InvoiceNo;
                    break;
                case "Bill Time":
                    result = Convert.ToDateTime(data.Sales.SalesCreatedAt).ToString("hh:mm tt");
                    break;
                case "PaymentType":
                    result = data.Sales.PaymentType;
                    break;
                case "Cheque":
                    result = data.Sales.ChequeNo;
                    break;
                case "Cheque Date":
                    result = string.Format("{0:dd/MM/yy}", data.Sales.ChequeDate);
                    break;
                case "Card Number":
                    result = data.Sales.CardNo;
                    break;
                case "Card Date":
                    result = string.Format("{0:dd/MM/yy}", data.Sales.CardDate);
                    break;
                case "UserName":
                    result = data.Sales.HQueUser.Name;
                    break;
                case "Transaction Date":
                    result = string.Format("{0:dd/MM/yy}", data.Sales.InvoiceDate);
                    break;
                case "ID number":
                    result = data.Sales.Patient.EmpID;
                    break;
            }

            return (result == null) ? "" : result;
        }

        private string GetReportDetailData(IInvoiceItem item, string FieldName)
        {
            var result = "";

            switch (FieldName)
            {
                case "SNo":
                    result = string.Format("{0:0}", sno++);
                    break;
                case "Product Name":
                    result = item.ProductStock.Product.Name;
                    break;
                case "Manufacturer":
                    result = item.ProductStock.Product.Manufacturer;
                    break;
                case "RackNo":
                    result = item.ProductStock.Product.RackNo;
                    break;
                case "BoxNo":
                    result = item.ProductStock.Product.BoxNo;
                    break;
                case "Batch No":
                    result = item.ProductStock.BatchNo;
                    break;
                case "Expire Date":
                    result = string.Format("{0:MM/yy}", item.ProductStock.ExpireDate);
                    break;
                case "Quantity":
                    result = string.Format("{0:0}", item.Quantity);
                    totalQuantity += item.Quantity;
                    break;
                case "Purchase Price":
                    result = string.Format("{0:0.00}", item.ProductStock.PurchasePrice);
                    break;
                case "Price":
                    result = string.Format("{0:0.00}", item.ProductStock.VatInPrice);
                    break;
                case "Price/unit WOT":
                    if (item.Quantity >= 0)
                        result = string.Format("{0:0.00}", (item.Total - ((item.Total * item.GstTotal) / (100 + item.GstTotal))) / item.Quantity);
                    else
                        result = string.Format("{0:0.00}", (-1) * (item.Total - ((item.Total * item.GstTotal) / (100 + item.GstTotal))) / item.Quantity);
                    break;
                case "Mrp/Unit":
                    result = string.Format("{0:0.00}", item.ProductStock.MRP != null ? item.ProductStock.MRP : item.MRP);
                    break;
                case "Selling Price/Unit":
                    result = string.Format("{0:0.00}", item.SellingPrice != null ? item.SellingPrice : item.ProductStock.SellingPrice);
                    break;
                case "Mrp/Strip":
                    result = string.Format("{0:0.00}", item.MRPPerStrip);
                    break;
                case "Selling/Strip":
                    result = string.Format("{0:0.00}", item.SellingPricePerStrip);
                    break;
                case "MRP after Discount":
                    result = string.Format("{0:0.00}", item.MRPAfterDiscount);
                    break;
                case "Selling Price after Discount":
                    result = string.Format("{0:0.00}", item.SellingPriceAfterDiscount);
                    break;
                case "MRP Value after Discount":
                    result = string.Format("{0:0.00}", item.MRPValueAfterDiscount);
                    break;
                case "Selling Price Value after Discount":
                    result = string.Format("{0:0.00}", item.SellingPriceValueAfterDiscount);
                    break;
                case "IGST%":
                    result = string.Format("{0:0.00}", item.Igst);
                    break;
                case "IGSTValue":
                    result = string.Format("{0:0.00}", item.IgstValue);
                    break;
                case "CGST%":
                    result = string.Format("{0:0.00}", item.Cgst);
                    break;
                case "CGSTValue":
                    result = string.Format("{0:0.00}", item.CgstValue);
                    break;
                case "SGST%":
                    result = string.Format("{0:0.00}", item.Sgst);
                    break;
                case "SGSTValue":
                    result = string.Format("{0:0.00}", item.SgstValue);
                    break;
                case "GSTTotal":
                    result = string.Format("{0:0.00}", item.GstTotal);
                    break;
                case "Discount %":
                    result = string.Format("{0:0}%", item.Discount);
                    break;
                case "Discount Amount":
                    result = string.Format("{0:0.00}", item.DiscountAmount);
                    break;
                case "VAT %":
                    result = string.Format("{0:0}%", item.ProductStock.VAT);
                    break;
                case "Vat Amount":
                    result = string.Format("{0:0.00}", item.VatAmount);
                    break;
                case "Unit/Strip":
                    result = string.Format("{0:0}", item.ProductStock.PackageSize);
                    break;
                case "GSTAmount":
                    result = string.Format("{0:0.00}", ((item.Total * item.GstTotal) / (100 + item.GstTotal)));
                    break;
                case "HsnCode":
                    result = string.IsNullOrEmpty(item.ProductStock.HsnCode) ? item.ProductStock.Product.HsnCode : item.ProductStock.HsnCode;
                    break;
                case "Amount":
                    result = string.Format("{0:0.00}", item.Total);
                    break;
                case "Amount Without GST":
                    result = string.Format("{0:0.00}", item.Total - ((item.Total * item.GstTotal) / (100 + item.GstTotal)));
                    break;

            }

            return (result == null) ? "" : result;
        }

        private string GetReportDetailData2(SalesReturnItem item, string FieldName)
        {
            var result = "";

            decimal? totalAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? returnTotalAmount = 0;

            totalAmount += (item.SellingPrice == null ? item.ProductStock.SellingPrice : item.SellingPrice) * (item.Quantity);
            returnTotalAmount += (item.SellingPrice == null ? item.ProductStock.SellingPrice : item.SellingPrice) * (item.Quantity);
            totReturnDiscount = totReturnDiscount + ((item.Discount / 100) * item.OnlyReturnAmount);

            item.IgstValue = ((item.OnlyReturnAmount - totReturnDiscount) - (((item.OnlyReturnAmount - totReturnDiscount) * 100) / (item.Igst + 100)));
            decimal? getGst = ((item.OnlyReturnAmount - totReturnDiscount) - (((item.OnlyReturnAmount - totReturnDiscount) * 100) / ((item.Cgst + item.Sgst) + 100)));
            item.CgstValue = getGst / 2;
            item.SgstValue = getGst / 2;


            switch (FieldName)
            {
                case "SNo":
                    result = string.Format("{0:0}", sno++);
                    break;
                case "Product Name":
                    result = item.ProductStock.Product.Name;
                    break;
                case "Manufacturer":
                    result = item.ProductStock.Product.Manufacturer;
                    break;
                case "RackNo":
                    result = item.ProductStock.Product.RackNo;
                    break;
                case "BoxNo":
                    result = item.ProductStock.Product.BoxNo;
                    break;
                case "Batch No":
                    result = item.ProductStock.BatchNo;
                    break;
                case "Expire Date":
                    result = string.Format("{0:MM/yy}", item.ProductStock.ExpireDate);
                    break;
                case "Quantity":
                    result = string.Format("{0:0}", item.Quantity);
                    totalQuantity += item.Quantity;
                    break;
                case "Purchase Price":
                    result = string.Format("{0:0.00}", item.ProductStock.PurchasePrice);
                    break;
                case "Price":
                    result = string.Format("{0:0.00}", item.ProductStock.VatInPrice);
                    break;
                case "Price/unit WOT":
                    result = string.Format("{0:0.00}", (-1) * (item.OnlyReturnAmount - ((item.OnlyReturnAmount * item.GstTotal) / (100 + item.GstTotal))) / item.Quantity);
                    break;
                case "Mrp/Unit":
                    result = string.Format("{0:0.00}", item.ProductStock.MRP != null ? item.ProductStock.MRP : item.SellingPrice);
                    break;
                case "Selling Price/Unit":
                    result = string.Format("{0:0.00}", item.SellingPrice != null ? item.SellingPrice : item.ProductStock.SellingPrice);
                    break;
                case "Discount %":
                    result = string.Format("{0:0}%", item.Discount);
                    break;
                case "Discount Amount":
                    result = string.Format("{0:0.00}", item.DiscountAmount);
                    break;
                case "VAT %":
                    result = string.Format("{0:0}%", item.ProductStock.VAT);
                    break;
                case "Vat Amount":
                    result = string.Format("{0:0.00}", item.VatAmount);
                    break;
                case "Unit/Strip":
                    result = string.Format("{0:0}", item.ProductStock.PackageSize);
                    break;
                case "Amount":
                    result = string.Format("{0:0.00}", item.OnlyReturnAmount);
                    break;
                case "IGST%":
                    result = string.Format("{0:0.00}", item.Igst);
                    break;
                case "IGSTValue":
                    {
                        result = string.Format("{0:0.00}", item.IgstValue);
                        break;
                    }
                case "CGST%":
                    result = string.Format("{0:0.00}", item.Cgst);
                    break;
                case "CGSTValue":
                    result = string.Format("{0:0.00}", item.CgstValue);
                    break;
                case "SGST%":
                    result = string.Format("{0:0.00}", item.Sgst);
                    break;
                case "SGSTValue":
                    result = string.Format("{0:0.00}", item.SgstValue);
                    break;
                case "GSTTotal":
                    result = string.Format("{0:0.00}", (item.GstTotal == 0) ? (item.Cgst + item.Sgst) : item.GstTotal);
                    break;
                case "HsnCode":
                    result = (item.ProductStock.HsnCode == null) ? item.ProductStock.Product.HsnCode : item.ProductStock.HsnCode;
                    break;
                case "GSTAmount":
                    result = string.Format("{0:0.00}", ((item.OnlyReturnAmount * item.GstTotal) / (100 + item.GstTotal)));
                    break;
                case "Amount Without GST":
                    result = string.Format("{0:0.00}", item.OnlyReturnAmount - ((item.OnlyReturnAmount * item.GstTotal) / (100 + item.GstTotal)));
                    break;
            }

            return (result == null) ? "" : result;
        }

        private string GetReportFooterData(InvoiceVM data, string FieldName)
        {
            var result = "";
            decimal? totalAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? returnTotalAmount = 0;

            if (data.Sales.SalesReturnItem.Count > 0)
            {
                foreach (var items in data.Sales.SalesReturnItem)
                {
                    totalAmount += (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    returnTotalAmount += (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.ReturnedQuantity);
                    totReturnDiscount = totReturnDiscount + ((items.Discount / 100) * items.OnlyReturnAmount);

                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = getGst / 2;
                    items.SgstValue = getGst / 2;
                }
            }

            switch (FieldName)
            {

                case "Gross Total":
                    if (data.Sales.SalesReturnItem.Count > 0)
                    {
                        result = string.Format("{0:0.00}", (data.Sales.PurchasedTotal + totalAmount) - totalAmount);
                    }
                    else
                    {
                        result = string.Format("{0:0.00}", data.Sales.PurchasedTotal);
                    }
                    break;
                case "Gross Value":
                    if (data.Sales.SalesReturnItem.Count > 0)
                    {
                        decimal? gst = (data.Sales.SalesReturnItem.Sum(x => x.SgstValue) + data.Sales.SalesReturnItem.Sum(x => x.CgstValue));
                        result = string.Format("{0:0.00}", (data.Sales.PurchasedTotal - gst - data.Sales.DiscountInValue));
                    }
                    else
                    {
                        decimal? gst = (data.Sales.CgstTotalValue + data.Sales.SgstTotalValue);
                        result = string.Format("{0:0.00}", data.Sales.PurchasedTotal - gst - data.Sales.DiscountInValue);
                    }
                    break;
                case "Discount":
                    if (data.Sales.SalesReturnItem.Count > 0)
                    {
                        //result = string.Format("{0:0.00}", data.Sales.DiscountInValue - totReturnDiscount); // By San 01-06-2017
                        result = string.Format("{0:0.00}", (data.Sales.DiscountInValue));
                    }
                    else
                    {
                        result = string.Format("{0:0.00}", data.Sales.DiscountInValue);
                    }
                    break;
                case "Total Tax Amt":
                    result = string.Format("{0:0.00}", data.Sales.Vat);
                    break;
                case "Round Off":
                    if (data.Sales.SalesReturnItem.Count > 0)
                    {
                        //decimal? Retval = Math.Round((decimal)data.Sales.Net - (decimal)totalAmount, MidpointRounding.AwayFromZero);
                        //decimal? RetroundOff = data.Sales.Net - totalAmount;
                        //var returnROff = Retval - RetroundOff;
                        decimal retAmount = data.Sales.RoundoffNetAmount as decimal? ?? 0; //RoundOffCalculation(data.Sales.Net+totalAmount, totalAmount);
                        result = string.Format("{0:0.00}", retAmount);
                    }
                    else
                    {
                        result = string.Format("{0:0.00}", data.Sales.RoundoffNetAmount);
                    }
                    break;
                case "Net Amount":
                    if (data.Sales.SalesReturnItem.Count > 0)
                    {

                        decimal? SalesDiscount = 0;

                        if (data.Sales.DiscountInValue > 0)
                        {
                            SalesDiscount = data.Sales.DiscountInValue + totReturnDiscount;
                        }
                        else
                        {
                            SalesDiscount = totReturnDiscount + data.Sales.DiscountInValue;
                        }
                        //decimal retAmount = RoundOffCalculation(data.Sales.Net + totalAmount, totalAmount);
                        //result = string.Format("{0:0.00}", ((data.Sales.PurchasedTotal + totalAmount)- SalesDiscount));
                        result = string.Format("{0:0.00}", data.Sales.NetAmount);
                    }
                    else
                    {
                        result = string.Format("{0:0.00}", data.Sales.NetAmount);
                    }
                    break;
                case "Net Amt WO Discount":
                    //result = string.Format("{0:0.00}", (decimal?)Math.Round((double)data.Sales.Net+ (double)totalAmount, MidpointRounding.AwayFromZero)- data.Sales.DiscountInValue);
                    result = string.Format("{0:0.00}", (decimal)data.Sales.NetAmount);
                    break;
                case "MRP Total":
                    result = string.Format("{0:0.00}", data.Sales.TotalMRPValue);
                    break;
                case "Savings Amount":
                    {
                        decimal tempResult;
                        if (data.Sales.SalesReturnItem.Count > 0)
                        {
                            decimal? SalesDiscount = 0;

                            if (data.Sales.DiscountInValue > 0)
                            {
                                SalesDiscount = data.Sales.DiscountInValue + totReturnDiscount;
                            }
                            else
                            {
                                SalesDiscount = totReturnDiscount + data.Sales.DiscountInValue;
                            }

                            tempResult = Convert.ToDecimal(data.Sales.TotalMRPValue) - Convert.ToDecimal(data.Sales.NetAmount);
                        }
                        else
                        {
                            tempResult = Convert.ToDecimal(data.Sales.TotalMRPValue) - Convert.ToDecimal(data.Sales.NetAmount);
                        }
                        result = string.Format("{0:0.00}", tempResult);
                    }
                    break;
                case "Amount in Words":
                    result = data.Sales.AmountinWords;
                    break;
                case "Payment Type":
                    result = data.Sales.PaymentType;
                    break;
                case "Total Items":
                    result = string.Format("{0:0}", data.Sales.InvoiceItems.Count());
                    break;
                case "Total Quantity":
                    result = string.Format("{0:0}", totalQuantity);
                    break;
                case "Cheque No":
                    result = string.Format("{0:0}", data.Sales.ChequeNo);
                    break;
                case "ChequeDate":
                    result = string.Format("{0:dd/MM/yy}", data.Sales.ChequeDate);
                    break;
                case "CardNumber":
                    result = string.Format("{0:0}", data.Sales.CardNo);
                    break;
                case "CardDate":
                    result = string.Format("{0:dd/MM/yy}", data.Sales.CardDate);
                    break;
                case "Return Disc":
                    {
                        result = string.Format("{0:0.00}", totReturnDiscount);
                        break;
                    }
                case "Return Amount":
                    {
                        result = string.Format("{0:0.00}", (totalAmount - totReturnDiscount));
                        break;
                    }
                case "Payable Amount":
                    {
                        if (data.Sales.SalesReturnItem.Count > 0)
                        {
                            //decimal retAmount = RoundOffCalculation(data.Sales.Net+ totalAmount, totalAmount);

                            decimal? SalesDiscount = 0;

                            if (data.Sales.DiscountInValue > 0)
                            {
                                SalesDiscount = data.Sales.DiscountInValue + totReturnDiscount;
                                //SalesDiscount = data.Sales.DiscountInValue;
                            }
                            else
                            {
                                SalesDiscount = totReturnDiscount + data.Sales.DiscountInValue;
                                //SalesDiscount = totReturnDiscount;
                            }
                            //var NetAmt = ((decimal)(((data.Sales.PurchasedTotal + totalAmount) - SalesDiscount)) - (totalAmount - totReturnDiscount)) + retAmount;
                            //result = string.Format("{0:0.00}", (decimal?)Math.Round((double)NetAmt, MidpointRounding.AwayFromZero));
                            result = string.Format("{0:0.00}", data.Sales.NetAmount);
                        }
                        else
                        {
                            result = string.Format("{0:0.00}", data.Sales.NetAmount);
                        }

                    }
                    break;
                case "GST Total%":
                    {
                        var IgstPer = (data.Sales.InvoiceItems.Sum(x => x.Cgst) + data.Sales.InvoiceItems.Sum(x => x.Sgst));
                        if (IgstPer == 0)
                            IgstPer = (decimal)IgstPer;
                        else
                            IgstPer = data.Sales.InvoiceItems.Sum(x => x.Igst);

                        result = string.Format("{0:0}", IgstPer);
                        break;
                    }
                case "IGST Total":
                    {
                        var IgstPer = data.Sales.InvoiceItems.Sum(x => x.IgstValue);
                        if (IgstPer == 0)
                            IgstPer = (decimal)IgstPer;

                        result = string.Format("{0:0.00}", IgstPer);
                        break;
                    }
                case "CGST Total":
                    {
                        var CgstPer = data.Sales.InvoiceItems.Sum(x => x.CgstValue);
                        if (CgstPer == 0)
                            CgstPer = (decimal)CgstPer;

                        result = string.Format("{0:0.00}", CgstPer);
                        break;
                    }
                case "SGST Total":
                    {
                        var SgstPer = data.Sales.InvoiceItems.Sum(x => x.SgstValue);
                        if (SgstPer == 0)
                            SgstPer = (decimal)SgstPer;

                        result = string.Format("{0:0.00}", SgstPer);
                        break;
                    }
                case "GSTTotal Amount":
                    {
                        var GSTVal = (data.Sales.InvoiceItems.Sum(x => x.CgstValue) + data.Sales.InvoiceItems.Sum(x => x.SgstValue));
                        if (GSTVal == 0)
                            GSTVal = (decimal)GSTVal;
                        else
                            GSTVal = data.Sales.InvoiceItems.Sum(x => x.IgstValue);

                        result = string.Format("{0:0.00}", GSTVal);
                        break;
                    }
                case "User Name":
                    result = data.Sales.HQueUser.Name;
                    break;
                case "Cess Amount":
                    {
                        //Saroja cess calculation:
                        //var GstPer = data.Sales.InvoiceItems.Sum(x => x.GstTotalValue);
                        //var MrpTemp = data.Sales.InvoiceItems.Sum(x => x.MRPValueAfterDiscount);
                        //var temp1 = MrpTemp - GstPer;
                        //var CessAmt = temp1 * 1 / 100;

                        //Normal cess calculation:
                        var CessAmt = (data.Sales.NetAmount * 1 / 100);
                        if (CessAmt == 0)
                            CessAmt = (decimal)CessAmt;

                        result = string.Format("{0:0.0000}", CessAmt);               
                        break;
                    }
                case "Net Amount with Cess":
                    {
                        var CessAmt = (data.Sales.NetAmount * 1 / 100);
                        if (CessAmt == 0)
                            result = Convert.ToDecimal(data.Sales.NetAmount).ToString();

                        result = string.Format("{0:0.0000}", (CessAmt + Convert.ToDecimal(data.Sales.NetAmount)));

                        break;
                    }
                default:
                    break;
                
            }

            return (result == null) ? "" : result;
        }

        private string GetReportFooterData2(InvoiceVM data, string FieldName)
        {
            var result = "";
            decimal? totalAmount = 0;
            decimal? totReturnDiscount = 0;
            decimal? returnTotalAmount = 0;

            if (data.Sales.SalesReturnItem.Count > 0)
            {
                foreach (var items in data.Sales.SalesReturnItem)
                {
                    totalAmount += (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.Quantity);
                    returnTotalAmount += (items.SellingPrice == null ? items.ProductStock.SellingPrice : items.SellingPrice) * (items.Quantity);
                    totReturnDiscount = totReturnDiscount + ((items.Discount / 100) * items.OnlyReturnAmount);

                    items.IgstValue = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / (items.Igst + 100)));
                    decimal? getGst = ((items.OnlyReturnAmount - totReturnDiscount) - (((items.OnlyReturnAmount - totReturnDiscount) * 100) / ((items.Cgst + items.Sgst) + 100)));
                    items.CgstValue = getGst / 2;
                    items.SgstValue = getGst / 2;
                }
            }


            switch (FieldName)
            {
                case "Gross Total":
                    result = string.Format("{0:0.00}", returnTotalAmount);
                    break;
                case "Gross Value":
                    decimal? CgstValue = data.Sales.SalesReturnItem.Sum(x => x.CgstValue);
                    decimal? SgstValue = data.Sales.SalesReturnItem.Sum(x => x.SgstValue);
                    result = string.Format("{0:0.00}", returnTotalAmount - (CgstValue + SgstValue) - totReturnDiscount - data.Sales.DiscountInValue);
                    break;
                case "Discount":
                    result = string.Format("{0:0.00}", totReturnDiscount * (-1));
                    break;
                case "Total Tax Amt":
                    result = string.Format("{0:0.00}", data.Sales.Vat);
                    break;
                case "Round Off":
                    //decimal? retAmount = ((decimal?)Math.Round((double)(returnTotalAmount - totReturnDiscount)))- (totalAmount- totReturnDiscount);
                    decimal? retAmount = (decimal)data.Sales.RoundoffNetAmount;
                    result = string.Format("{0:0.00}", retAmount);
                    break;
                case "Net Amount":
                    //result = string.Format("{0:0.00}", returnTotalAmount - totReturnDiscount);
                    result = string.Format("{0:0.00}", 0);

                    break;
                case "Net Amt WO Discount":
                    //result = string.Format("{0:0.00}", (decimal?)Math.Round((double)returnTotalAmount, MidpointRounding.AwayFromZero) - totReturnDiscount);
                    result = string.Format("{0:0.00}", (decimal)data.Sales.NetAmount);
                    break;
                case "MRP Total":
                    result = string.Format("{0:0.00}", data.Sales.TotalMRPValue);
                    break;
                case "Payment Type":
                    result = string.Empty;
                    break;
                case "Total Items":
                    result = string.Format("{0:0}", data.Sales.SalesReturnItem.Count());
                    break;
                case "Total Quantity":
                    result = string.Format("{0:0}", totalQuantity);
                    break;
                case "Cheque No":
                    result = string.Empty;
                    break;
                case "ChequeDate":
                    result = string.Empty;
                    break;
                case "CardNumber":
                    result = string.Empty;
                    break;
                case "CardDate":
                    result = string.Empty;
                    break;
                case "Return Disc":
                    {
                        result = string.Format("{0:0.00}", totReturnDiscount);
                        break;
                    }
                case "Return Charges":
                    {
                        result = string.Format("{0:0.00}", data.Sales.SalesReturn.ReturnCharges);
                        break;
                    }
                case "Return Amount":
                    {
                        if (data.Sales.SalesReturn.ReturnCharges > 0 && data.Sales.SalesReturn.ReturnCharges != null)
                        {
                            //result = string.Format("{0:0.00}", ((decimal?)Math.Round((double)(returnTotalAmount - totReturnDiscount), MidpointRounding.AwayFromZero)+ (Math.Round((decimal)data.Sales.SalesReturn.ReturnCharges))));
                            result = string.Format("{0:0.00}", ((decimal)data.Sales.NetAmount - (decimal)data.Sales.SalesReturn.ReturnCharges));
                        }
                        else
                        {
                            //result = string.Format("{0:0.00}", (decimal?)Math.Round((double)(returnTotalAmount - totReturnDiscount), MidpointRounding.AwayFromZero));
                            result = string.Format("{0:0.00}", (decimal)data.Sales.NetAmount);
                        }

                        break;
                    }
                case "Payable Amount":
                    if (data.Sales.SalesReturn.ReturnCharges > 0 && data.Sales.SalesReturn.ReturnCharges != null)
                    {
                        //result = string.Format("{0:0.00}", ((decimal?)Math.Round((double)(returnTotalAmount - totReturnDiscount), MidpointRounding.AwayFromZero) + (Math.Round((decimal)data.Sales.SalesReturn.ReturnCharges))));
                        result = string.Format("{0:0.00}", ((decimal)data.Sales.NetAmount - ((decimal)data.Sales.SalesReturn.ReturnCharges)));
                    }
                    else
                    {
                        //result = string.Format("{0:0.00}", (decimal?)Math.Round((double)(returnTotalAmount - totReturnDiscount), MidpointRounding.AwayFromZero));
                        result = string.Format("{0:0.00}", (decimal?)data.Sales.NetAmount);
                    }
                    break;
                case "GST Total%":
                    {
                        var IgstPer = (data.Sales.SalesReturnItem.Sum(x => x.Cgst) + data.Sales.SalesReturnItem.Sum(x => x.Sgst));
                        if (IgstPer == 0)
                        {
                            IgstPer = data.Sales.SalesReturnItem.Sum(x => x.Igst);
                        }
                        result = string.Format("{0:0}", IgstPer);
                        break;
                    }
                case "GSTTotal Amount":
                    {
                        var gstVal = (data.Sales.SalesReturnItem.Sum(x => x.CgstValue) + data.Sales.SalesReturnItem.Sum(x => x.SgstValue));
                        result = string.Format("{0:0.00}", gstVal);
                        break;
                    }
                case "IGST Total":
                    {
                        var IgstPer = data.Sales.SalesReturnItem.Sum(x => x.IgstValue);
                        if (IgstPer == 0)
                            IgstPer = (decimal)IgstPer;

                        result = string.Format("{0:0.00}", IgstPer);
                        break;
                    }
                case "CGST Total":
                    {
                        var CgstPer = data.Sales.SalesReturnItem.Sum(x => x.CgstValue);
                        if (CgstPer == 0)
                            CgstPer = (decimal)CgstPer;

                        result = string.Format("{0:0.00}", CgstPer);
                        break;
                    }
                case "SGST Total":
                    {
                        var SgstPer = data.Sales.SalesReturnItem.Sum(x => x.SgstValue);
                        if (SgstPer == 0)
                            SgstPer = (decimal)SgstPer;

                        result = string.Format("{0:0.00}", SgstPer);
                        break;
                    }
                case "User Name":
                    result = data.Sales.HQueUser.Name;
                    break;
                case "Cess Amount":
                    {
                        //Saroja cess calculation:
                        //var SgstPer = data.Sales.InvoiceItems.Sum(x => x.SgstValue);
                        //var CessAmt = SgstPer * 1 / 100;

                        //Normal cess calculation:
                        var CessAmt = (data.Sales.NetAmount * 1 / 100);
                        if (CessAmt == 0)
                            CessAmt = (decimal)CessAmt;

                        result = string.Format("{0:0.0000}", CessAmt);
                        break;
                    }
                case "Net Amount with Cess":
                    {
                        var CessAmt = (data.Sales.NetAmount * 1 / 100);
                        if (CessAmt == 0)
                            result = Convert.ToDecimal(data.Sales.NetAmount).ToString();

                        result = string.Format("{0:0.0000}", (CessAmt + Convert.ToDecimal(data.Sales.NetAmount)));

                        break;
                    }
            }

            return (result == null) ? "" : result;
        }

        private void WriteReportFooter_4InchRoll(PlainTextReport rpt, InvoiceVM data)
        {
            rpt.NewLine();
            rpt.Write("Total: " + string.Format("{0:0.00}", data.Sales.PurchasedTotal), 15, 1);
            rpt.Write("Discount: " + string.Format("{0:0.00}", data.Sales.DiscountInValue), 15, 16);
            rpt.Write("Net Amt: " + string.Format("{0:0.00}", data.Sales.Net), 17, 31, Alignment.Right);
            rpt.Write();
        }
        #endregion

        #region save Template to azure storage

        private async Task uploadToAzure(string json)
        {
            // Retrieve storage account from connection string.
            var conn = GetConnectionString();
            Debug.WriteLine(conn);
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(conn);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("templates");

            // Retrieve reference to a blob named "myblob".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(string.Format("sales_{0}.txt", User.InstanceId()));

            //await container.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            //container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            // Create or overwrite the "myblob" blob with contents from a local file.
            //using (var fileStream = System.IO.File.OpenRead(@"path\myfile"))
            //{
            //    blockBlob.UploadFromStream(fileStream);
            //}

            using (var ms = new MemoryStream())
            {
                LoadStreamWithJson(ms, json);
                await blockBlob.UploadFromStreamAsync(ms);
            }


        }

        private async Task<TemplateData> ReadAzureBlob()
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(GetConnectionString());

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("templates");

            // Retrieve reference to a blob named "myblob.txt"
            CloudBlockBlob blockBlob2 = container.GetBlockBlobReference(string.Format("sales_{0}.txt", User.InstanceId()));

            string text;
            using (var memoryStream = new MemoryStream())
            {
                await blockBlob2.DownloadToStreamAsync(memoryStream);
                text = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
            }

            return JsonConvert.DeserializeObject<TemplateData>(text);
        }

        private void LoadStreamWithJson(Stream ms, string data)
        {
            //var json = JsonConvert.DeserializeObject<TemplateData>(data);
            StreamWriter writer = new StreamWriter(ms);
            writer.Write(data);
            writer.Flush();
            ms.Position = 0;
        }

        private string GetConnectionString()
        {
            return _configHelper.AppConfig.StorageConnection;
        }

        private String TitleCaseString(String s)
        {
            if (s == null) return s;

            String[] words = s.Split(' ');
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i].Length == 0) continue;

                Char firstChar = Char.ToUpper(words[i][0]);
                String rest = "";
                if (words[i].Length > 1)
                {
                    rest = words[i].Substring(1).ToLower();
                }
                words[i] = firstChar + rest;
            }
            return String.Join(" ", words);
        }

        //private decimal RoundOffCalculation(decimal? Net, decimal? totReturnAmount)
        //{
        //    decimal? Retval = Math.Round((decimal)Net - (decimal)totReturnAmount, MidpointRounding.AwayFromZero);
        //    decimal? RetroundOff = Net - totReturnAmount;

        //    return (decimal)(Retval - RetroundOff);
        //}

        #endregion

        //Return Print // by San 02-06-2017
        [Route("[action]")]
        public async Task<ActionResult> PrintReturnTemplate(string id = null, string templateFile = null)
        {
            //TemplateData tdata = JsonConvert.DeserializeObject<TemplateData>(templateData);

            //TemplateData tdata  = await ReadAzureBlob();

            var getTemplate = await _salesManager.GetCustomTemplate(User.AccountId(), User.InstanceId());
            if (getTemplate == "")
            {
                return RedirectToAction("PrintTemplate", "Sales");
                //return Red("/Sales/PrintTemplate");                
            }
            TemplateData tdata = JsonConvert.DeserializeObject<TemplateData>(getTemplate);

            var model = new InvoiceVM();
            if (!string.IsNullOrEmpty(id))
            {
                model.Sales = await _salesManager.GetReturnOnlyDetails(id);
                model.IsReturnsOnly = true;
                //model.Sales.SalesReturnItem.Add(model.Sales);
            }

            //string name = string.Format("./temp/salebill_{0}.txt", User.InstanceId());
            string name = string.Format("./wwwroot/temp/salebill_{0}.txt", User.InstanceId());

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(GenerateReport(model, tdata), false, Encoding.UTF8);

            }

            HttpContext.Response.ContentType = "application/notepad";
            if (tdata.PaperType == "CUSTOM")
            {
                return File(info.OpenRead(), "application/notepad", string.Format("hq_PP_{0}.txt", model.Sales.InvoiceNo));
            }
            else
            {
                return File(info.OpenRead(), "application/notepad", string.Format("hq_{0}.txt", model.Sales.InvoiceNo));
            }

        }
        /// <summary>





    }


}
