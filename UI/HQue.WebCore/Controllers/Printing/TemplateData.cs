﻿using System.Collections.Generic;

namespace HQue.WebCore.Controllers.Printing
{
    public class TemplateData
    {
        public string TemplateName { get; set; }
        public string PaperType { get; set; }
        public int Columns { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Rows { get; set; }
        public int FontSize { get; set; }
        public int TopMargin { get; set; }
        public int LeftMargin { get; set; }
        public int RightMargin { get; set; }
        public string StationaryFile { get; set; }
        public List<LayoutData> DataItems { get; set; }

        public TemplateData()
        {
            this.DataItems = new List<LayoutData>();
        }
    }

    public class LayoutData
    {
        public LayoutData()
        {
            this.TextAlign = 1;
        }

        public string FieldName { get; set; }
        public int LineNo { get; set; }
        public int Width { get; set; }
        public int Offset { get; set; }
        public string Alignment { get; set; }
        public int FieldType { get; set; }
        public string Text { get; set; }
        public int TextAlign { get; set; }
        public int Section { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int h { get; set; }
        public int w { get; set; }
        public string Fill { get; set; }
        public int FontSize { get; set; }
        public int FontWeight { get; set; }
        public int Itemfontsize { get; set; }
    }

}
