﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HQue.Biz.Core.ETL;
using Utilities.Helpers;
using System.Collections.Generic;

namespace HQue.WebCore.Controllers.ETL
{
    [Route("[controller]")]
    public class ImportCustomerController : BaseController
    {
        private readonly ImportCustomer _importCustomer;
        private readonly IHostingEnvironment _environment;

        public ImportCustomerController(ImportCustomer importCustomer, IHostingEnvironment environment, ConfigHelper configHelper)
            : base(configHelper)
        {
            _importCustomer = importCustomer;
            _environment = environment;
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult Index(bool? success)
        {
            ViewBag.Success = success;
            return View();
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file)
        {
            int missedItemsCount = 0;
            List<int> missedItems = new List<int>();
            try
            {
                missedItems = await _importCustomer.ImportData(file.OpenReadStream(), User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id());
                missedItemsCount = missedItems.Count;
                ViewBag.missedItems = missedItems;
                ViewBag.missedItemsCount = missedItemsCount;
            }
            catch(Exception ex)
            {
                //return RedirectToAction("Index", new { success = false, missedItems = missedItemsCount });
                ViewBag.success = false;
                return View();
            }
            //return RedirectToAction("Index", new { success = true, missedItems = missedItemsCount });
            
            ViewBag.success = true;
            
            return View();
        }

        [Route("[action]")]
        [HttpGet]
        public FileResult Download()
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads", "CustomerImport.csv");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "text/csv","Template.csv");
        }
    }
}
