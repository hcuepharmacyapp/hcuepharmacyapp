﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HQue.Biz.Core.ETL;
using Utilities.Helpers;
using System.Collections.Generic;
using System.Text;

namespace HQue.WebCore.Controllers.ETL
{
    [Route("[controller]")]
    public class ImportStockController : BaseController
    {
        private readonly Import _import;
        private readonly IHostingEnvironment _environment;
        private readonly ConfigHelper _configHelper;

        public ImportStockController(Import import, IHostingEnvironment environment, ConfigHelper configHelper)
            : base(configHelper)
        {
            _import = import;
            _environment = environment;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult Index(bool? success)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            ViewBag.Success = success;
            return View();
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file)
        {
            int missedItemsCount = 0;
            List<int> missedItems = new List<int>();
/*Modified by Martin on 16/06/2017*/
            try
            {
                //check file format
                //if not .csv or ttx then throw error
                var fileExt = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                if (!(fileExt == ".csv" )) //|| fileExt == ".ttx"
                    throw new Exception("Not a valid file format! Upload only .csv formats.");//or .ttx 

                var dataStream = file.OpenReadStream();
                if (fileExt == ".ttx")
                    dataStream = ConvertTTX_To_CSV(dataStream).FileStream;

                dataStream.Position = 0;
                missedItems = await _import.ImportData(dataStream, User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id());
                missedItemsCount = missedItems.Count;
                ViewBag.missedItems = missedItems;
                ViewBag.missedItemsCount = missedItemsCount;
            }
            catch(Exception ex)
            {
                //return RedirectToAction("Index", new { success = false, errorDesc = ex.Message, missedItems = missedItemsCount });
                ViewBag.success = false;
                ViewBag.message = ex.Message;
              //  return Json(new { success = false, errorDesc = ex.Message, showPanel = true });

                return View();
            }
            //return RedirectToAction("Index", new { success = true, missedItems = missedItemsCount });
            
            ViewBag.success = true;
            
            return View();
        }

        [Route("[action]")]
        [HttpGet]
        public FileResult Download()
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads", "Import.csv");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "text/csv","Template.csv");
        }
//Method Added by Martin on 16/06/2017

        private FileStreamResult ConvertTTX_To_CSV(Stream fs)
        {
            var mem = new MemoryStream();
            StreamWriter csvWriter = new StreamWriter(mem, Encoding.UTF8);
            var sr = new StreamReader(fs);
            int i = 0;
            bool IsHeaderFound = false;
            Dictionary<string, ColumnPosition> cols = new Dictionary<string, ColumnPosition>();

            while (!sr.EndOfStream)
            {
                //check for header
                var line = sr.ReadLine().TrimEnd('"').TrimStart('"');

                if (IsHeaderFound)
                {
                    var data = GetRowData(cols, line);

                    if (!string.IsNullOrEmpty(data))
                        csvWriter.WriteLine(data);
                  
                    
                }
                else if (line.StartsWith("QTY"))
                {
                    IsHeaderFound = true;

                    #region logic
                    //string colName = "";
                    //bool doCount = false;
                    //int colLen = 0;

                    //for(int c=0; c<line.Length; c++)
                    //{
                    //    if (line.Substring(c, 1) == " ")
                    //        doCount = true;

                    //    if (line.Substring(c, 1) != " " && doCount)
                    //    {
                    //        doCount = false;

                    //        if (colName.Trim() != "PRODUCT")
                    //        {
                    //            cols.Add(colName, colLen);
                    //            colLen = 0;
                    //            colName = "";
                    //        }
                    //    }

                    //    colName += line.Substring(c, 1);
                    //    colLen++;
                    //}
                    #endregion

                    cols.Add("QTY", new ColumnPosition() {StartPos=0, Length=9});
                    cols.Add("PACK", new ColumnPosition() { StartPos = 9, Length = 7});
                    cols.Add("PRODUCT NAME", new ColumnPosition() { StartPos = 16, Length = 21 });
                    cols.Add("BATCH", new ColumnPosition() { StartPos = 37, Length = 9 });
                  cols.Add("VAT", new ColumnPosition() { StartPos = 46, Length = 6 });
                    cols.Add("MRP", new ColumnPosition() { StartPos = 52, Length = 9 });
                    cols.Add("EXPIRY", new ColumnPosition() { StartPos = 61, Length = 6 });
                    cols.Add("RATE", new ColumnPosition() { StartPos = 67, Length = 10 });
                    cols.Add("AMOUNT", new ColumnPosition() { StartPos = 77, Length = 10 });
                    cols.Add("DISC", new ColumnPosition() { StartPos = 87, Length = 5 });
                    csvWriter.WriteLine(""); //Insert Blank line  for header by Poongodi on 20/06/2017
                }


            }


            csvWriter.Flush();

            return new FileStreamResult(mem, "text/plain");
        }

        private string GetRowData(Dictionary<string, ColumnPosition> cols, string data)
        {
            if (String.IsNullOrEmpty(data))
                return "";

            var rowData = new StringBuilder();

            rowData.Append(data.Substring(cols["PRODUCT NAME"].StartPos, cols["PRODUCT NAME"].Length));
            rowData.Append(",");

            var vat = data.Substring(cols["VAT"].StartPos, cols["VAT"].Length);
            rowData.Append(vat);
            rowData.Append(",");

            rowData.Append("01/");
            rowData.Append(data.Substring(cols["EXPIRY"].StartPos, cols["EXPIRY"].Length));
            rowData.Append(",");

            rowData.Append(data.Substring(cols["BATCH"].StartPos, cols["BATCH"].Length));
            rowData.Append(",");

            #region extract pack size
            //var units = data.Substring(cols["PACK"].StartPos, cols["PACK"].Length);
            //if (units.Trim().EndsWith("TAB") || units.Trim().EndsWith("CAP") || units.IndexOf('*') > 0)
            //{
            //    units = units.Replace(" ", "");
            //    if (units.IndexOf("*T") > 0 || units.IndexOf("*C") > 0)
            //        units = units.Substring(units.IndexOf('*'));
            //    else
            //        units = "1";

            //    units = System.Text.RegularExpressions.Regex.Replace(units, @"[^\d]", "");
            //}
            //else {
            //    units = "1";
            //}
            #endregion

            var units = "1";
            rowData.Append(units);
            rowData.Append(",");

            var costPrice = Convert.ToDecimal(data.Substring(cols["RATE"].StartPos, cols["RATE"].Length));
            costPrice = costPrice - (costPrice * Convert.ToDecimal(vat)  / (100+ Convert.ToDecimal(vat)));
            rowData.Append(costPrice.ToString());
            rowData.Append(",");

            rowData.Append(data.Substring(cols["MRP"].StartPos, cols["MRP"].Length));
            rowData.Append(",");

            //BARCODE
            rowData.Append(",");

            rowData.Append(data.Substring(cols["QTY"].StartPos, cols["QTY"].Length));
            rowData.Append(",");

            //CST
            rowData.Append("0");
            rowData.Append(",");

            //FREE QTY
            rowData.Append("0");
            rowData.Append(",");
    
            //VENDOR NAME
            rowData.Append("Plus Pharmacy");
            rowData.Append(",,,,,,,,");
            
            return rowData.ToString();
        }


    }

    public class ColumnPosition
    {
        public int StartPos { get; set; }
        public int Length { get; set; }
    }
}
