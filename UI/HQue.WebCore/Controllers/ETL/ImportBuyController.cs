﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Master;
using HQue.Biz.Core.ETL;
using HQue.Biz.Core.Inventory;
using System.Linq;
using DataAccess;
using HQue.Contract.Infrastructure.Dashboard;
using System.Data;
using System.Dynamic;
using System.Globalization;
using HQue.Biz.Core.Master;
using HQue.Contract.Base;
using HQue.DataAccess.Inventory;
using System.Text.RegularExpressions;
using HQue.Contract.Infrastructure.Settings;

namespace HQue.WebCore.Controllers.ETL
{
    [Route("[controller]")]
    public class ImportBuyController : BaseController
    {
        private readonly Import _import;
        private readonly IHostingEnvironment _environment;
        private readonly VendorDataAccess _vendorDataAccess;
        private readonly VendorPurchaseManager _vendorPurchaseManager;
        private readonly ProductDataAccess _productDataAccess;
        private readonly ConfigHelper _configHelper;
        private readonly VendorPurchaseDataAccess _vendorPurchaseDataAccess;
        public object ScriptManager { get; private set; }
        public ImportBuyController(Import import, IHostingEnvironment environment, VendorDataAccess vendorDataAccess,
            VendorPurchaseManager vendorPurchaseManager, ProductDataAccess productDataAccess, ConfigHelper configHelper, VendorPurchaseDataAccess vendorPurchaseDataAccess)
            : base(configHelper)
        {
            _import = import;
            _environment = environment;
            _vendorDataAccess = vendorDataAccess;
            _productDataAccess = productDataAccess;
            _vendorPurchaseManager = vendorPurchaseManager;
            _configHelper = configHelper;
            _vendorPurchaseDataAccess = vendorPurchaseDataAccess;
        }
        [Route("[action]")]
        [HttpGet]
        public IActionResult Index(bool? success, string message)
        {
            ViewBag.Success = success;
            ViewBag.Message = message;
            ViewBag.ShowPanel = true;
            return View();
        }
        // Newly Added Gavaskar 22-12-2016 Start
        [Route("[action]")]
        [HttpGet]
        public IActionResult PurchaseImportSetting()
        {
            ViewBag.ShowPanel = true;
            return View();
        }
        [Route("[action]")]
        public Task<List<TemplatePurchaseMaster>> BuyImportSetting(TemplatePurchaseMaster templatePurchaseMaster)
        {
            return _vendorDataAccess.BuyImportSetting(templatePurchaseMaster);
        }
        // Newly Added Gavaskar 22-12-2016 End
        [Route("[action]")]
        [HttpPost]
        public async Task<dynamic> PurchaseImportSetting(IFormFile file, int vendorRowPos, int productRowPos)
        {
            try
            {
                ViewBag.ShowPanel = true;
                //var ListItem = new List<dynamic>();
                var v = await GetVendorPurchaseItem_Mapping(file.OpenReadStream(), vendorRowPos, productRowPos);
                return v;
                // ListItem = v;
                // return v;
                // return Json(new {v});
            }
            catch (Exception e)
            {
                HttpContext.Response.Clear();
                HttpContext.Response.StatusCode = 500;
                ViewBag.Message = e.Message;
                return View();
            }
        }
        private async Task<dynamic> GetVendorPurchaseItem_Mapping(Stream stream, int vendorRowPos, int productRowPos)
        {
            var VendorList = new List<string>();
            var ProductList = new List<string>();
            var sr = new StreamReader(stream);
            string[] mymodel;
            int i = 0;
            int j = 0;
            while (!sr.EndOfStream)
            {
                var item = sr.ReadLine().Replace("\"", "");
                if (i == vendorRowPos - 1)
                {
                    var split = item.Split(',');
                    var splitted = split.Where(x => x.Trim() != "");
                    splitted.ToList().ForEach(y => { VendorList.Add(y); });
                    //VendorList.Concat(splitted);
                    break;
                }
                i++;
            }
            //mymodel.vendor = VendorList;
            j = 1 + i;
            while (!sr.EndOfStream)
            {
                var item = sr.ReadLine().Replace("\"", "");
                if (j == productRowPos - 1)
                {
                    var split = item.Split(',');
                    var splitted = split.Where(x => x.Trim() != "");
                    splitted.ToList().ForEach(y => { ProductList.Add(y); });
                    //ProductList.Concat(splitted);
                    break;
                }
                j++;
            }
            //mymodel.product = ProductList;
            mymodel = VendorList.Concat(ProductList).ToArray();
            // vendorPurchase.VendorPurchaseItem5 = list;
            //return Vendor;
            return mymodel;
        }
        [HttpPost]
        [Route("[action]")]
        public async Task SaveBuyTemplateSetting([FromBody] TempBuyTemplateSettings tempBuyTemplateSettings)
        {
            var listTemplates = tempBuyTemplateSettings.templatePurchaseChildMaster;
            var settingsMaster = tempBuyTemplateSettings.templatePurchaseChildMasterSettings;
            listTemplates.ForEach(x => { x.SetLoggedUserDetails(User); });
            await _vendorPurchaseManager.SaveBuyTemplateSetting(listTemplates, settingsMaster.VendorId);
            settingsMaster.SetLoggedUserDetails(User);
            await _vendorPurchaseManager.SaveBuyTemplateChildSetting(settingsMaster, settingsMaster.VendorId);
        }
        [Route("[action]")]
        [HttpGet]
        public async Task<TempBuyTemplateSettings> getBuyTemplateSetting(string vendorId)
        {
            var AccountId = User.AccountId();
            var InstanceId = User.InstanceId();
            var listTemplates = await _vendorDataAccess.getBuyTemplatePurchaseChildMaster(AccountId, InstanceId, vendorId);
            var settingsMaster = await _vendorDataAccess.getBuyTemplatePurchaseChildMasterSettings(AccountId, InstanceId, vendorId);
            TempBuyTemplateSettings tempBuyTemplateSettings = new TempBuyTemplateSettings();
            tempBuyTemplateSettings.templatePurchaseChildMaster = listTemplates;
            tempBuyTemplateSettings.templatePurchaseChildMasterSettings = settingsMaster;
            return tempBuyTemplateSettings;
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file, string vendor2, string customType, string defaultType)
        {
            try
            {
                ViewBag.ShowPanel = false;
                VendorPurchase v = await GetVendorPurchaseItem(file.OpenReadStream(), vendor2, customType, defaultType);
                v.SetLoggedUserDetails(User);
                v = await _vendorPurchaseManager.GetAlternateName(v);
                ViewBag.Success = true;
                return View(v);
                // return RedirectToAction("Index", new { success = true, message = string.Empty, showPanel = true });
            }
            catch (Exception e)
            {
                HttpContext.Response.Clear();
                HttpContext.Response.StatusCode = 500;
                ViewBag.Message = e.Message;
                //Json(new { success = false, errorDesc = e.Message, showPanel = true   });
                ViewBag.Success = false;
                return View();
                //return Json(new { success = false, errorDesc = e.Message, showPanel = true });
            }
        }

        //Newly created by Manivannan on 20-Oct-2017
        [Route("[action]")]
        [HttpPost]
        public async Task<VendorPurchase> UploadFile([FromForm] IFormFile file, string vendor2, string customType, string defaultType)
        {
            try
            {
                VendorPurchase v = await GetVendorPurchaseItem(file.OpenReadStream(), vendor2, customType, defaultType);
                v.SetLoggedUserDetails(User);
                v = await _vendorPurchaseManager.GetAlternateName(v);
                //HttpContext.Response.StatusCode = 200;
                return v;

            }
            catch (Exception e)
            {
                VendorPurchase errorVP = new VendorPurchase();
                errorVP.ImportError = e.Message;
                return errorVP;
            }
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<AlternateVendorProduct> SaveAlternateName([FromBody]AlternateVendorProduct model)

        {
            if (model.AlternatePackageSize == null)
            {
                model.AlternatePackageSize = "";
            }
            model.SetLoggedUserDetails(User);
            var getMaster = await _productDataAccess.GetProductByMasterId(model.ProductId);
            if (getMaster != null)
            {
                Product product = new Product();
                product.SetLoggedUserDetails(User);
                product.Name = getMaster.Name;
                product.ProductMasterID = getMaster.Id;
                //var count = await _productDataAccess.TotalProductCount(product);
                product.Name = product.Name.ToUpper();
                //var getChar = product.Name.Substring(0, 2);
                //product.Code = (getChar.ToUpper() + (count + 1)).ToString();
                var obj = await _productDataAccess.Save(product);
                model.ProductId = obj.Id;
            }
            return await _productDataAccess.saveAlternateName(model);
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<List<AlternateVendorProduct>> updatePackageSize([FromBody]List<AlternateVendorProduct> data)

        {
            foreach (AlternateVendorProduct item in data)
            {
                item.SetLoggedUserDetails(User);
            }
            return await _productDataAccess.updatePackageSize(data);
        }

        //[Route("[action]")]
        //[HttpPost]
        //public async Task<IActionResult> Index(IFormFile file)
        //{
        //    try
        //    {
        //        await _import.ImportBuyData(file.OpenReadStream(), User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id());
        //    }
        //    catch (Exception ex)
        //    {
        //        return RedirectToAction("Index", new { success = false, message = ex.Message });
        //    }
        //    return RedirectToAction("Index", new { success = true, message = string.Empty });
        //}
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> ImportData([FromBody]VendorPurchase model, IFormFile file)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(file)))
                {
                    var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
                    var fileName = Guid.NewGuid().ToString() + '_' + parsedContentDisposition.FileName.Trim('"');
                    var filePath = Path.Combine(_environment.WebRootPath, "uploads\\purchase", fileName);
                    //await file.SaveAsAsync(filePath);
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        model.FileName = fileName;
                    }
                }
                model.SetLoggedUserDetails(User);
                model.Credit = 0;
                await _vendorPurchaseManager.Save(model);
                return RedirectToAction("Index", new { success = true, message = string.Empty, showPanel = true });
            }
            catch (Exception e)
            {
                HttpContext.Response.Clear();
                HttpContext.Response.StatusCode = 500;
                return Json(new { success = false, errorDesc = e.Message, showPanel = true });
            }
        }
        [Route("[action]")]
        [HttpGet]
        public FileResult Download()
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads", "BuyImport.csv");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "text/csv", "BuyTemplate.csv");
        }
        private async System.Threading.Tasks.Task<VendorPurchase> GetVendorPurchaseItem_Old(Stream stream)
        {
            var isFirstRow = true;
            var isSecondRow = true;
            var isThirdRow = true;
            var vendorPurchase = new VendorPurchase();
            var vendorPurchaseItemList = new List<VendorPurchaseItem>();
            string errorMessage = "";
            var sr = new StreamReader(stream);
            while (!sr.EndOfStream)
            {
                var item = sr.ReadLine();
                var split = item.Split(',');
                //[0]ProductName,[1]VAT,[2]ExpireDate(dd/mm/yyyy),[3]BatchNo,[4]MRP,[5]Barcode,[6]Stock
                if (isFirstRow)
                {
                    isFirstRow = false;
                    continue;
                }
                // Vendor Purchase data
                if (isSecondRow)
                {
                    //check if vendor name exists
                    Vendor vendor = new Vendor();
                    vendor.Name = split[0];
                    vendor.SetLoggedUserDetails(User);
                    var v = await _vendorDataAccess.GetVendorId(vendor);
                    if (v != null)
                        vendor = v;
                    //try
                    //{
                    if (split[1] == "")
                    {
                        errorMessage = "Invoice Number is Mandatory";
                        throw new Exception(errorMessage);
                    }
                    if (split[4] == "")
                    {
                        errorMessage = "Amount is Mandatory";
                        throw new Exception(errorMessage);
                    }
                    vendorPurchase.Vendor = vendor;
                    vendorPurchase.VendorId = vendor.Id;
                    vendorPurchase.InvoiceNo = split[1];
                    vendorPurchase.InvoiceDate = Convert.ToDateTime(split[2]);
                    DateTime today = DateTime.Now;
                    if (vendorPurchase.InvoiceDate >= today)
                    {
                        errorMessage = "Invoice Date is Greater than Current Date";
                        throw new Exception(errorMessage);
                    }
                    if (split[3] != "")
                    {
                        vendorPurchase.Discount = Convert.ToDecimal(split[3]);
                    }
                    vendorPurchase.Credit = Convert.ToDecimal(split[4]);
                    isSecondRow = false;
                    continue;
                    //}
                    //catch (Exception ex)
                    //{
                    //    throw new Exception(ex.Message);
                    //    //return RedirectToAction("Index", new { success = true, message = ex });
                    //}
                }
                // Vendor Purchase Item data
                if (isThirdRow)
                {
                    isThirdRow = false;
                    continue;
                }
                //check product exists
                Product product = new Product();
                product.Name = split[0];
                //product.AccountId = User.AccountId();
                //product.InstanceId = User.InstanceId();
                product.SetLoggedUserDetails(User);
                product = await _productDataAccess.GetPurchaseImportProductById(product);
                if (product == null)
                    product.Name = split[0];
                //try
                //{
                if (split[1] == "")
                {
                    errorMessage = "Batch Number is Mandatory";
                    throw new Exception(errorMessage);
                }
                var vendorPurchaseItem = new VendorPurchaseItem();
                vendorPurchaseItem.ProductStock = new ProductStock();
                vendorPurchaseItem.ProductStock.ProductId = product.Id;
                vendorPurchaseItem.ProductStock.Product.Name = product.Name;
                vendorPurchaseItem.ProductStock.BatchNo = split[1];
                vendorPurchaseItem.ProductStock.ExpireDate = Convert.ToDateTime(split[2]);
                decimal result;
                if (decimal.TryParse(split[3], out result))
                {
                    vendorPurchaseItem.ProductStock.GstTotal = Convert.ToDecimal(split[3]);
                }
                else
                {
                    errorMessage = "GST is Mandatory";
                    throw new Exception(errorMessage);
                }
                if (decimal.TryParse(split[4], out result))
                {
                    vendorPurchaseItem.PackageSize = Convert.ToDecimal(split[4]);
                }
                else
                {
                    errorMessage = "Units/Strip is Mandatory";
                    throw new Exception(errorMessage);
                }
                if (decimal.TryParse(split[5], out result))
                {
                    vendorPurchaseItem.PackageQty = Convert.ToDecimal(split[5]);
                }
                else
                {
                    errorMessage = "No.Strips is Mandatory";
                    throw new ArgumentException(errorMessage);
                }
                if (decimal.TryParse(split[6], out result))
                {
                    vendorPurchaseItem.PackagePurchasePrice = Convert.ToDecimal(split[6]);
                }
                else
                {
                    errorMessage = "Price/Strip is Mandatory";
                    throw new ArgumentException(errorMessage);
                }
                if (decimal.TryParse(split[7], out result))
                {
                    vendorPurchaseItem.PackageSellingPrice = Convert.ToDecimal(split[7]);
                    vendorPurchaseItem.PackageMRP = vendorPurchaseItem.PackageSellingPrice;
                }
                else
                {
                    errorMessage = "MRP/Strip is Mandatory";
                    throw new ArgumentException(errorMessage);
                }
                if (decimal.TryParse(split[8], out result))
                {
                    vendorPurchaseItem.FreeQty = Convert.ToDecimal(split[8]);
                }
                else
                {
                    vendorPurchaseItem.FreeQty = 0;
                }
                if (decimal.TryParse(split[9], out result))
                {
                    vendorPurchaseItem.Discount = Convert.ToDecimal(split[9]);
                }
                else
                {
                    vendorPurchaseItem.Discount = 0;
                }
                if (decimal.TryParse(split[10], out result))
                {
                    vendorPurchaseItem.ProductStock.CST = Convert.ToDecimal(split[10]);
                }
                else
                {
                    vendorPurchaseItem.ProductStock.CST = 0;
                }
                //Added by Violet Raj
                if (split.Length == 12)
                {
                    if (split[11].Length < 6)
                    {
                        vendorPurchaseItem.RackNo = split[11];
                    }
                    else
                    {
                        errorMessage = "Enter RackNo at least 5 characters";
                        throw new ArgumentException(errorMessage);
                    }
                }
                else
                {
                    vendorPurchaseItem.RackNo = null;
                }// end
                vendorPurchaseItemList.Add(vendorPurchaseItem);
                //}
                //catch (Exception ex)
                //{
                //    throw;
                //}
            }
            vendorPurchase.VendorPurchaseItem = vendorPurchaseItemList;
            return vendorPurchase;
        }
        private async System.Threading.Tasks.Task<VendorPurchase> GetVendorPurchaseItem(Stream stream, string vendorId, string customType, string defaultType)
        {
            var isFirstRow = true;
            var isSecondRow = true;
            var isThirdRow = true;
            var vendorPurchase = new VendorPurchase();
            var vendorPurchaseItemList = new List<VendorPurchaseItem>();
            string errorMessage = "";
            Int32 index;
            int i = 0;
            int vendorCount = 0;
            bool isAllowDecimal = await _vendorPurchaseDataAccess.GetAllowDecimalSetting(User.AccountId(), User.InstanceId());
            PurchaseSettings purchaseSettings = await _vendorPurchaseDataAccess.getBuyInvoiceDateEditSetting(User.AccountId(), User.InstanceId());

            var getTaxList = await _vendorPurchaseManager.GetTaxValues(User.AccountId());

            if (purchaseSettings == null)
            {
                purchaseSettings = new PurchaseSettings();
            }
            if (defaultType != "" && (customType == null || customType == ""))
            {
                var sr = new StreamReader(stream);
                while (!sr.EndOfStream)
                {

                    var item = sr.ReadLine();
                    if (item.Contains('\"'))
                    {
                        errorMessage = "Please Remove unwanted Special Character in Import File";
                        throw new Exception(errorMessage);
                    }
                    if (sr.EndOfStream && isThirdRow)
                    {
                        errorMessage = "Please fill product details in Import file";
                        throw new Exception(errorMessage);
                    }

                    var split = item.Split(',');
                    //split[0] = VendorName,split[1] = InvoiceNo,split[2] = InvoiceDate(dd-MMM-yyyy),split[3] = Discount,split[4] = Amount
                    if (isFirstRow)
                    {
                        isFirstRow = false;
                        continue;
                    }
                    // Vendor Purchase data
                    if (isSecondRow)
                    {
                        //check if vendor name exists
                        Vendor vendor = new Vendor();
                        vendor.Name = split[0];
                        if (Convert.ToString(vendor.Name) == "")
                        {
                            errorMessage = "Please fill VendorName in Import File";
                            throw new Exception(errorMessage);
                        }
                        vendor.SetLoggedUserDetails(User);
                        var v = await _vendorDataAccess.GetVendorId(vendor);
                        if (v != null)
                        {
                            vendor = v;
                        }
                        else
                        {
                            errorMessage = "Invalid VendorName";
                            throw new Exception(errorMessage);
                        }

                        if (split[1] == "")
                        {
                            errorMessage = "Invoice Number is Mandatory";
                            throw new Exception(errorMessage);
                        }

                        vendorPurchase.Vendor = vendor;
                        vendorPurchase.VendorId = vendor.Id;
                        vendorPurchase.TaxRefNo = User.GSTEnabled() == "True" ? 1 : 0; //Altered by Sarubala on 25-09-17
                        vendorPurchase.InvoiceNo = split[1];
                        //vendorPurchase.InvoiceDate = Convert.ToDateTime(split[2]);
                        DateTime today = DateTime.Now;
                        if (split[2] != "")
                        {
                            vendorPurchase.InvoiceDate = Convert.ToDateTime(split[2]);

                            if (vendorPurchase.InvoiceDate >= today)
                            {
                                errorMessage = "Invoice Date is Greater than Current Date";
                                throw new Exception(errorMessage);
                            }
                        }

                        if (Convert.ToString(vendorPurchase.InvoiceDate) == "")
                        {
                            errorMessage = "Invoice Date is Mandatory";
                            throw new Exception(errorMessage);
                        }

                        if (split[3] != "")
                        {
                            vendorPurchase.Discount = Convert.ToDecimal(split[3]);
                            if (vendorPurchase.Discount < 0)
                            {
                                errorMessage = "Invoice discount should be greater than 0";
                                throw new Exception(errorMessage);
                            }
                            if ((double)vendorPurchase.Discount > 99.99)
                            {
                                errorMessage = "Invoice discount should be lessthan 100%";
                                throw new Exception(errorMessage);
                            }
                        }
                        if (Convert.ToString(vendorPurchase.Discount) == "")
                        {
                            errorMessage = "Please fill Discount in Import File";
                            throw new Exception(errorMessage);
                        }
                        if (split[4] == "")
                        {
                            errorMessage = "Amount is Mandatory";
                            throw new Exception(errorMessage);
                        }
                        vendorPurchase.Credit = Convert.ToDecimal(split[4]);
                        if (vendorPurchase.Credit < 0)
                        {
                            errorMessage = "Invoice amount should be greater than 0";
                            throw new Exception(errorMessage);
                        }

                        if (split.Length >= 6 && purchaseSettings.EnableMarkup == true)
                        {
                            if (split[5] != "")
                            {
                                vendorPurchase.MarkupPerc = Convert.ToDecimal(split[5]);
                                if (vendorPurchase.MarkupPerc < 0)
                                {
                                    errorMessage = "Invoice markup perc should be greater than or equal to 0";
                                    throw new Exception(errorMessage);
                                }
                                if ((double)vendorPurchase.MarkupPerc > 99.99)
                                {
                                    errorMessage = "Invoice markup perc should be less than 100%";
                                    throw new Exception(errorMessage);
                                }
                            }
                        }

                        /*  === Added by sumathi on 25-11-18 to validate invoice number duplicate === */
                        int InvoiceCount = await _vendorPurchaseDataAccess.GetInvoiceCount(User.AccountId(), User.InstanceId(), vendorPurchase.VendorId, vendorPurchase.InvoiceNo);

                        if (InvoiceCount > 0)
                        {
                            errorMessage = "Invoice Number already Exists";
                            throw new Exception(errorMessage);
                        }
                        /* ===End==== */

                        isSecondRow = false;
                        continue;
                        //}
                        //catch (Exception ex)
                        //{
                        //    throw new Exception(ex.Message);
                        //    //return RedirectToAction("Index", new { success = true, message = ex });
                        //}
                    }
                    // Vendor Purchase Item data
                    if (isThirdRow)
                    {
                        isThirdRow = false;
                        continue;
                    }
                    //check product exists
                    Product product = new Product();
                    product.Name = split[0];
                    //for removing special char added by nandhini 19.5.17
                    /*{
                        string input;                        
                        if (product.Name.Contains('{'))
                        {
                            input = product.Name.Replace('{', '(');
                            product.Name = input;
                        }
                        if (product.Name.Contains('}'))
                        {
                            input = product.Name.Replace('}', ')');
                            product.Name = input;
                        }                       
                        
                    }*/

                    //product.AccountId = User.AccountId();
                    //product.InstanceId = User.InstanceId();

                    if (split[0] == "")
                    {
                        errorMessage = "Product Name is Mandatory";
                        throw new Exception(errorMessage);
                    }

                    if (split[1] == "")
                    {
                        errorMessage = "Batch Number is Mandatory";
                        throw new Exception(errorMessage);
                    }
                    //for removing special char added by nandhini 29.5.18
                    {
                        string input;
                        if (product.Name.Contains('{'))
                        {
                            input = product.Name.Replace('{', '(');
                            product.Name = input;
                        }
                        if (product.Name.Contains('}'))
                        {
                            input = product.Name.Replace('}', ')');
                            product.Name = input;
                        }


                    }

                    product.SetLoggedUserDetails(User);
                    product = await _productDataAccess.GetPurchaseImportProductById(product);
                    if (product == null)
                        product.Name = split[0];

                    var vendorPurchaseItem = new VendorPurchaseItem();
                    vendorPurchaseItem.ProductStock = new ProductStock();
                    vendorPurchaseItem.ProductStock.ProductId = product.Id;
                    vendorPurchaseItem.ProductStock.Product.Name = product.Name;
                    var tmpProductMaster = await _productDataAccess.GetProductMaster(product.Name.Trim());

                    //Added by Sarubala on 02-01-19
                    var tmpProduct = await _productDataAccess.GetProductByName(product.Name);

                    //added by Violet 03-05-2017
                    //Modified by Sarubala on 02-01-19
                    //if (tmpProductMaster.Count > 0 || tmpProduct.Count > 0)
                    //{
                    //    vendorPurchaseItem.ProductStock.Product.Manufacturer = (!string.IsNullOrEmpty(tmpProduct[0].Manufacturer) && !string.IsNullOrWhiteSpace(tmpProduct[0].Manufacturer)) ? tmpProduct[0].Manufacturer : tmpProductMaster[0].Manufacturer;
                    //    vendorPurchaseItem.ProductStock.Product.GenericName = (!string.IsNullOrEmpty(tmpProduct[0].GenericName) && !string.IsNullOrWhiteSpace(tmpProduct[0].GenericName)) ? tmpProduct[0].GenericName : tmpProductMaster[0].GenericName;
                    //    vendorPurchaseItem.ProductStock.Product.Schedule = (!string.IsNullOrEmpty(tmpProduct[0].Schedule) && !string.IsNullOrWhiteSpace(tmpProduct[0].Schedule)) ? tmpProduct[0].Schedule : tmpProductMaster[0].Schedule;
                    //    vendorPurchaseItem.ProductStock.Product.CommodityCode = (!string.IsNullOrEmpty(tmpProduct[0].CommodityCode) && !string.IsNullOrWhiteSpace(tmpProduct[0].CommodityCode)) ? tmpProduct[0].CommodityCode : tmpProductMaster[0].CommodityCode;
                    //    vendorPurchaseItem.ProductStock.Product.Packing = (!string.IsNullOrEmpty(tmpProduct[0].Packing) && !string.IsNullOrWhiteSpace(tmpProduct[0].Packing)) ? tmpProduct[0].Packing : tmpProductMaster[0].Packing;
                    //    vendorPurchaseItem.ProductStock.Product.Category = (!string.IsNullOrEmpty(tmpProduct[0].Category) && !string.IsNullOrWhiteSpace(tmpProduct[0].Category)) ? tmpProduct[0].Category : tmpProductMaster[0].Category;
                    //    vendorPurchaseItem.ProductStock.Product.Type = (!string.IsNullOrEmpty(tmpProduct[0].Type) && !string.IsNullOrWhiteSpace(tmpProduct[0].Type)) ? tmpProduct[0].Type : tmpProductMaster[0].Type;
                    //}

                    if (tmpProduct.Count > 0)
                    {
                        vendorPurchaseItem.ProductStock.Product.Manufacturer = (!string.IsNullOrEmpty(tmpProduct[0].Manufacturer) && !string.IsNullOrWhiteSpace(tmpProduct[0].Manufacturer)) ? tmpProduct[0].Manufacturer : "";
                        vendorPurchaseItem.ProductStock.Product.GenericName = (!string.IsNullOrEmpty(tmpProduct[0].GenericName) && !string.IsNullOrWhiteSpace(tmpProduct[0].GenericName)) ? tmpProduct[0].GenericName : "";
                        vendorPurchaseItem.ProductStock.Product.Schedule = (!string.IsNullOrEmpty(tmpProduct[0].Schedule) && !string.IsNullOrWhiteSpace(tmpProduct[0].Schedule)) ? tmpProduct[0].Schedule : "";
                        vendorPurchaseItem.ProductStock.Product.CommodityCode = (!string.IsNullOrEmpty(tmpProduct[0].CommodityCode) && !string.IsNullOrWhiteSpace(tmpProduct[0].CommodityCode)) ? tmpProduct[0].CommodityCode : "";
                        vendorPurchaseItem.ProductStock.Product.Packing = (!string.IsNullOrEmpty(tmpProduct[0].Packing) && !string.IsNullOrWhiteSpace(tmpProduct[0].Packing)) ? tmpProduct[0].Packing : "";
                        vendorPurchaseItem.ProductStock.Product.Category = (!string.IsNullOrEmpty(tmpProduct[0].Category) && !string.IsNullOrWhiteSpace(tmpProduct[0].Category)) ? tmpProduct[0].Category : "";
                        vendorPurchaseItem.ProductStock.Product.Type = (!string.IsNullOrEmpty(tmpProduct[0].Type) && !string.IsNullOrWhiteSpace(tmpProduct[0].Type)) ? tmpProduct[0].Type : "";
                    }
                    else if (tmpProductMaster.Count > 0)
                    {
                        vendorPurchaseItem.ProductStock.Product.Manufacturer = (!string.IsNullOrEmpty(tmpProductMaster[0].Manufacturer) && !string.IsNullOrWhiteSpace(tmpProductMaster[0].Manufacturer)) ? tmpProductMaster[0].Manufacturer : "";
                        vendorPurchaseItem.ProductStock.Product.GenericName = (!string.IsNullOrEmpty(tmpProductMaster[0].GenericName) && !string.IsNullOrWhiteSpace(tmpProductMaster[0].GenericName)) ? tmpProductMaster[0].GenericName : "";
                        vendorPurchaseItem.ProductStock.Product.Schedule = (!string.IsNullOrEmpty(tmpProductMaster[0].Schedule) && !string.IsNullOrWhiteSpace(tmpProductMaster[0].Schedule)) ? tmpProductMaster[0].Schedule : "";
                        vendorPurchaseItem.ProductStock.Product.CommodityCode = (!string.IsNullOrEmpty(tmpProductMaster[0].CommodityCode) && !string.IsNullOrWhiteSpace(tmpProductMaster[0].CommodityCode)) ? tmpProductMaster[0].CommodityCode : "";
                        vendorPurchaseItem.ProductStock.Product.Packing = (!string.IsNullOrEmpty(tmpProductMaster[0].Packing) && !string.IsNullOrWhiteSpace(tmpProductMaster[0].Packing)) ? tmpProductMaster[0].Packing : "";
                        vendorPurchaseItem.ProductStock.Product.Category = (!string.IsNullOrEmpty(tmpProductMaster[0].Category) && !string.IsNullOrWhiteSpace(tmpProductMaster[0].Category)) ? tmpProductMaster[0].Category : "";
                        vendorPurchaseItem.ProductStock.Product.Type = (!string.IsNullOrEmpty(tmpProductMaster[0].Type) && !string.IsNullOrWhiteSpace(tmpProductMaster[0].Type)) ? tmpProductMaster[0].Type : "";
                    }

                    vendorPurchaseItem.ProductStock.BatchNo = split[1];


                    decimal result;
                    DateTime result1;
                    if (DateTime.TryParse(split[2], out result1))
                    {
                        vendorPurchaseItem.ProductStock.ExpireDate = Convert.ToDateTime(split[2]);
                    }
                    else
                    {
                        errorMessage = "ExpireDate is Mandatory";
                        throw new Exception(errorMessage);
                    }

                    //hCue Template Option -- Added by Annadurai


                    //if (decimal.TryParse(split[3], out result))
                    //{
                    //    vendorPurchaseItem.ProductStock.VAT = Convert.ToDecimal(split[3]);
                    //    string vatValueforHcueTemplate = split[3];        

                    //    ValidateVatforImport(vendorPurchaseItem.ProductStock.VAT, vatValueforHcueTemplate);                        
                    //}
                    //else
                    //{
                    //    errorMessage = "Vat is Mandatory";
                    //    throw new Exception(errorMessage);
                    //}
                    //added by nandhini 12.7.17

                    if (decimal.TryParse(split[3], out result))
                    {
                        //if(getTaxList.Count()>0)
                        //{
                        //    var gstValue = Convert.ToDouble(split[3]).ToString("0.00");
                        //    bool isGetTax = getTaxList.Any(x => x.Tax == Convert.ToDecimal(gstValue));
                        //    if(isGetTax)
                        //        vendorPurchaseItem.ProductStock.GstTotal = Convert.ToDecimal(split[3]);
                        //    else
                        //        vendorPurchaseItem.ProductStock.GstTotal = null;
                        //}
                        //else                       
                        vendorPurchaseItem.ProductStock.GstTotal = Convert.ToDecimal(split[3]);

                    }
                    else
                    {
                        errorMessage = "GST is Mandatory";
                        throw new Exception(errorMessage);
                    }
                    //end
                    if (decimal.TryParse(split[4], out result))
                    {
                        vendorPurchaseItem.PackageSize = Convert.ToDecimal(split[4]);
                    }
                    //else
                    //{
                    //    errorMessage = "Units/Strip is Mandatory";
                    //    throw new Exception(errorMessage);
                    //}
                    if (vendorPurchaseItem.PackageSize <= 0)
                    {
                        errorMessage = "Units/Strip should be greater than 0";
                        throw new Exception(errorMessage);
                    }
                    if (vendorPurchaseItem.PackageSize == null)
                    {
                        vendorPurchaseItem.AlternatePackageSize = Convert.ToString(split[4]);
                    }
                    if (decimal.TryParse(split[5], out result))
                    {
                        vendorPurchaseItem.PackageQty = Convert.ToDecimal(split[5]);

                    }
                    else
                    {
                        errorMessage = "No.Strips is Mandatory";
                        throw new ArgumentException(errorMessage);
                    }
                    if (vendorPurchaseItem.PackageQty <= 0)
                    {
                        errorMessage = "No.Strips should be greater than 0";
                        throw new Exception(errorMessage);
                    }
                    if (decimal.TryParse(split[6], out result))
                    {
                        vendorPurchaseItem.PackagePurchasePrice = Convert.ToDecimal(split[6]);

                    }
                    else
                    {
                        errorMessage = "Price/Strip is Mandatory";
                        throw new ArgumentException(errorMessage);
                    }
                    if (vendorPurchaseItem.PackagePurchasePrice <= 0)
                    {
                        errorMessage = "Price/Strip should be greater than 0";
                        throw new Exception(errorMessage);
                    }
                    if (decimal.TryParse(split[7], out result))
                    {
                        vendorPurchaseItem.PackageSellingPrice = Convert.ToDecimal(split[7]);

                        //if (vendorPurchaseItem.PackagePurchasePrice > (vendorPurchaseItem.PackageSellingPrice + (vendorPurchaseItem.PackageSellingPrice * (vendorPurchaseItem.ProductStock.GstTotal / 100))))
                        if (vendorPurchaseItem.PackagePurchasePrice + (vendorPurchaseItem.PackagePurchasePrice * vendorPurchaseItem.ProductStock.GstTotal / 100) > vendorPurchaseItem.PackageSellingPrice)
                        {
                            errorMessage = "MRP Price must be Greater than Cost Price + GST";
                            throw new Exception(errorMessage);
                        }

                        vendorPurchaseItem.PackageMRP = vendorPurchaseItem.PackageSellingPrice;
                    }
                    else
                    {
                        errorMessage = "MRP/Strip is Mandatory";
                        throw new ArgumentException(errorMessage);
                    }
                    if (decimal.TryParse(split[8], out result))
                    {
                        vendorPurchaseItem.FreeQty = Convert.ToDecimal(split[8]);
                        if (vendorPurchaseItem.FreeQty < 0)
                        {
                            errorMessage = "Free Qty should be greater than 0";
                            throw new Exception(errorMessage);
                        }
                    }
                    //else
                    //{
                    //    vendorPurchaseItem.FreeQty = 0;
                    //}
                    if (Convert.ToString(vendorPurchaseItem.FreeQty) == "")
                    {
                        errorMessage = "Please fill FreeQty in Import File";
                        throw new Exception(errorMessage);
                    }
                    if (decimal.TryParse(split[9], out result))
                    {
                        vendorPurchaseItem.Discount = Convert.ToDecimal(split[9]);
                        if (vendorPurchaseItem.Discount < 0)
                        {
                            errorMessage = "Product discount should be greater than 0";
                            throw new Exception(errorMessage);
                        }

                        if ((double)vendorPurchaseItem.Discount > 99.99)
                        {
                            errorMessage = "Product discount should be less  than 100%";
                            throw new Exception(errorMessage);
                        }


                        if ((double)vendorPurchase.Discount + (double)vendorPurchaseItem.Discount > 99.99)
                        {
                            errorMessage = "Product + Invoice discount should be less  than 100%";
                            throw new Exception(errorMessage);
                        }

                    }
                    if (Convert.ToString(vendorPurchaseItem.Discount) == "")
                    {
                        errorMessage = "Please fill Discount in Import File";
                        throw new Exception(errorMessage);
                    }
                    //else
                    //{
                    //    vendorPurchaseItem.Discount = 0;
                    //}


                    //if (decimal.TryParse(split[10], out result))
                    //{
                    //    vendorPurchaseItem.ProductStock.CST = Convert.ToDecimal(split[10]);
                    //    if (vendorPurchaseItem.ProductStock.CST < 0)
                    //    {
                    //        errorMessage = "Product cst should be greater than 0";
                    //        throw new Exception(errorMessage);
                    //    }
                    //}
                    //else
                    //{
                    //    vendorPurchaseItem.ProductStock.CST = 0;
                    //}

                    //added by nandhini 12.7.17

                    if (split[10] != "")
                    {
                        vendorPurchaseItem.ProductStock.HsnCode = Convert.ToString(split[10]);
                    }
                    if (split[10] == "")
                    {
                        errorMessage = "Please fill  HsnCode in Import File";
                        throw new Exception(errorMessage);
                    }
                    //end
                    //RackNo added by Violet 03-05-2017
                    if (split.Length >= 12)
                    {
                        if (split[11].Length < 6)
                        {
                            vendorPurchaseItem.RackNo = split[11];
                        }
                        else
                        {
                            errorMessage = "Enter RackNo at least 5 characters";
                            throw new ArgumentException(errorMessage);
                        }
                    }
                    else
                    {
                        vendorPurchaseItem.RackNo = null;
                    }
                    //if (Convert.ToString(vendorPurchaseItem.RackNo) == "")
                    //{
                    //    errorMessage = "Please fill RackNo in Import File";
                    //    throw new Exception(errorMessage);
                    //}
                    //BoxNo added by Settu 18-05-2017
                    if (split.Length >= 13)
                    {
                        if (split[12].Length < 6)
                        {
                            vendorPurchaseItem.ProductStock.Product.BoxNo = split[12];
                        }
                        else
                        {
                            errorMessage = "Enter BoxNo at least 5 characters";
                            throw new ArgumentException(errorMessage);
                        }
                    }
                    else
                    {
                        vendorPurchaseItem.ProductStock.Product.BoxNo = null;
                    }
                    //if (Convert.ToString(vendorPurchaseItem.ProductStock.Product.BoxNo) == "")
                    //{
                    //    errorMessage = "Please fill BoxNo in Import File";
                    //    throw new Exception(errorMessage);
                    //}

                    if (split.Length >= 14)
                    {
                        if (split[13] != "")
                        {
                            vendorPurchaseItem.SchemeDiscountPerc = Convert.ToDecimal(split[13]);
                            if (vendorPurchaseItem.SchemeDiscountPerc < 0)
                            {
                                errorMessage = "Product scheme discount should be greater than or equal to 0";
                                throw new Exception(errorMessage);
                            }

                            if ((double)vendorPurchaseItem.SchemeDiscountPerc > 99.99)
                            {
                                errorMessage = "Product scheme discount should be less than 100%";
                                throw new Exception(errorMessage);
                            }
                        }
                    }
                    if (split.Length >= 15 && purchaseSettings.EnableMarkup == true)
                    {
                        if (split[14] != "")
                        {
                            vendorPurchaseItem.MarkupPerc = Convert.ToDecimal(split[14]);
                            if (vendorPurchaseItem.MarkupPerc < 0)
                            {
                                errorMessage = "Product markup perc should be greater than or equal to 0";
                                throw new Exception(errorMessage);
                            }

                            if ((double)vendorPurchaseItem.MarkupPerc > 99.99)
                            {
                                errorMessage = "Product markup perc should be less than 100%";
                                throw new Exception(errorMessage);
                            }


                            if ((double)(vendorPurchase.MarkupPerc == null ? 0 : vendorPurchase.MarkupPerc) + (double)(vendorPurchaseItem.MarkupPerc == null ? 0 : vendorPurchaseItem.MarkupPerc) > 99.99)
                            {
                                errorMessage = "Product markup + Invoice markup perc should be less than 100%";
                                throw new Exception(errorMessage);
                            }
                        }
                    }

                    if (!isAllowDecimal)
                    {
                        if (vendorPurchaseItem.PackageSize % 1 != 0 && vendorPurchaseItem.PackageSize != null)
                        {
                            errorMessage = "Decimal value not allowed in Unit/Strips";
                            throw new ArgumentException(errorMessage);
                        }
                        if (vendorPurchaseItem.PackageQty % 1 != 0)
                        {
                            errorMessage = "Decimal value not allowed in No.of strips";
                            throw new ArgumentException(errorMessage);
                        }
                        if (vendorPurchaseItem.FreeQty % 1 != 0)
                        {
                            errorMessage = "Decimal value not allowed in Free strips";
                            throw new ArgumentException(errorMessage);
                        }
                    }
                    else
                    {
                        if ((vendorPurchaseItem.PackageQty + vendorPurchaseItem.FreeQty) % 1 != 0)
                        {
                            errorMessage = "Free strips decimal value must be equal to " + (1 - (vendorPurchaseItem.PackageQty % 1)) + " for No.of strips " + vendorPurchaseItem.PackageQty;
                            throw new ArgumentException(errorMessage);
                        }
                    }
                    vendorPurchaseItemList.Add(vendorPurchaseItem);
                    //}
                    //catch (Exception ex)
                    //{
                    //    throw;
                    //}
                }

                vendorPurchase.VendorPurchaseItem = vendorPurchaseItemList;
                return vendorPurchase;

            }
            else
            {
                TemplatePurchaseChildMaster templatePurchaseChildMaster = new TemplatePurchaseChildMaster();
                TemplatePurchaseChildMasterSettings templatePurchaseChildMasterSettings = new TemplatePurchaseChildMasterSettings();
                Vendor vendor = new Vendor();
                var AccountId = User.AccountId();
                var InstanceId = User.InstanceId();
                var TemplatePurchaseMaster = await _vendorDataAccess.GetTemplatePurchaseMaster(templatePurchaseChildMaster, AccountId, InstanceId, vendorId);
                var TemplatePurchaseChildMasterSettings = await _vendorDataAccess.getBuyTemplatePurchaseChildMasterSettings(AccountId, InstanceId, vendorId);
                var headerOption = TemplatePurchaseChildMasterSettings.HeaderOption;
                var vendorRowPos = TemplatePurchaseChildMasterSettings.VendorRowPos;
                var productRowPos = TemplatePurchaseChildMasterSettings.ProductRowPos;
                var dateFormat = TemplatePurchaseChildMasterSettings.DateFormat;
                vendor.Id = vendorId;
                vendor.SetLoggedUserDetails(User);
                var ven = await _vendorDataAccess.GetVendorName(vendor);
                if (ven != null)
                    vendor = ven;
                var VendorName = vendor.Name;
                vendorPurchase.Vendor = vendor;
                vendorPurchase.VendorId = vendor.Id;
                vendorPurchase.TaxRefNo = User.GSTEnabled() == "True" ? 1 : 0; // Added by Sarubala on 25-09-17
                // if(TemplatePurchaseChildMasterSettings.VendorId== vendorPurchase.VendorId)
                bool isAllowDecimalCustom = await _vendorPurchaseDataAccess.GetAllowDecimalSetting(User.AccountId(), User.InstanceId());
                if (TemplatePurchaseChildMasterSettings.VendorId == vendorId)
                {
                    var sr = new StreamReader(stream);
                    while (!sr.EndOfStream)
                    {
                        var item = sr.ReadLine();
                        if (item.Contains('\"'))
                        {
                            errorMessage = "Please Remove unwanted Special Character in Import File";
                            throw new Exception(errorMessage);
                        }
                        var split = item.Split(',');
                        var splitted = split.Where(x => x.Trim() != "");
                        if (headerOption == true)
                        {
                            // Vendor Purchase data
                            if (i == vendorRowPos)
                            {
                                //check if vendor name exists
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "VendorName");
                                if (index != 9999)
                                {
                                    vendor.Name = split[index];
                                    vendor.SetLoggedUserDetails(User);
                                    var v = await _vendorDataAccess.GetVendorId(vendor);
                                    if (v != null)
                                        vendor = v;
                                    vendorPurchase.Vendor = vendor;
                                    vendorPurchase.VendorId = vendor.Id;
                                    vendorPurchase.TaxRefNo = User.GSTEnabled() == "True" ? 1 : 0; // Added by Sarubala on 25-09-17
                                    if (VendorName != vendor.Name)
                                    {
                                        errorMessage = "Upload File Vendor Name Not Matched The Selected Vendor Name";
                                        throw new Exception(errorMessage);
                                    }

                                }
                                //mandatory fields removed on 22.8.2017 as discussed with sundhar


                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "InvoiceNo");
                                //if (split[index] == "")
                                //{
                                //    errorMessage = "Invoice Number is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                if (index != 9999)
                                {

                                    vendorPurchase.InvoiceNo = split[index];

                                }
                                else
                                {
                                    vendorPurchase.InvoiceNo = null;
                                }
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "InvoiceDate");
                                //if (split[index] == "")
                                //{
                                //    errorMessage = "Please fill Invoice Date in Import File";
                                //    throw new Exception(errorMessage);
                                //}
                                if (dateFormat == "MM/yy")
                                {
                                    var dateFormat1 = "dd/MM/yy";
                                    if (split[index].Split('/').Count() != 3)
                                    {
                                        vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat, CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat1, CultureInfo.InvariantCulture);
                                    }
                                }
                                if (dateFormat == "MM-yy")
                                {
                                    var dateFormat1 = "dd-MM-yy";
                                    if (split[index].Split('-').Count() != 3)
                                    {
                                        vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat, CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat1, CultureInfo.InvariantCulture);
                                    }
                                }
                                if (dateFormat == "MM-yyyy")
                                {
                                    var dateFormat1 = "dd-MM-yyyy";
                                    if (split[index].Split('-').Count() != 3)
                                    {
                                        vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat, CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat1, CultureInfo.InvariantCulture);
                                    }
                                }
                                if (dateFormat == "MM/yyyy")
                                {
                                    var dateFormat1 = "dd/MM/yyyy";
                                    if (split[index].Split('/').Count() != 3)
                                    {
                                        vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat, CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {

                                        vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat1, CultureInfo.InvariantCulture);
                                    }
                                }
                                //vendorPurchase.InvoiceDate = Convert.ToDateTime(split[index]);
                                DateTime today = DateTime.Now;
                                if (vendorPurchase.InvoiceDate >= today)
                                {
                                    errorMessage = "Invoice Date is Greater than Current Date";
                                    throw new Exception(errorMessage);
                                }


                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "VendorDiscount");
                                if (index != 9999)
                                {

                                    vendorPurchase.Discount = Convert.ToDecimal(split[index]);

                                }
                                else
                                {
                                    vendorPurchase.Discount = null;
                                }

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "VendorMarkupPerc");
                                if (index != 9999 && purchaseSettings.EnableMarkup == true)
                                {
                                    if (split.Length > index)
                                    {
                                        if (split[index] != "")
                                        {
                                            vendorPurchase.MarkupPerc = Convert.ToDecimal(split[index]);
                                            if (vendorPurchase.MarkupPerc < 0)
                                            {
                                                errorMessage = "Invoice markup perc should be greater than or equal to 0";
                                                throw new Exception(errorMessage);
                                            }
                                            if ((double)vendorPurchase.MarkupPerc > 99.99)
                                            {
                                                errorMessage = "Invoice markup perc should be less than 100%";
                                                throw new Exception(errorMessage);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    vendorPurchase.MarkupPerc = null;
                                }
                                //if (Convert.ToString(vendorPurchase.Discount) == "")
                                //{
                                //    errorMessage = "Please fill VendorDiscount in Import File";
                                //    throw new Exception(errorMessage);
                                //}
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Amount");
                                //if (split[index] == "")
                                //{
                                //    errorMessage = "Amount is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                vendorPurchase.Credit = Convert.ToDecimal(split[index]);
                                vendorCount = splitted.Count();
                            }
                            if (i >= productRowPos)
                            {
                                //check product exists
                                Product product = new Product();
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ProductName");
                                product.Name = split[index - vendorCount];
                                //if (product.Name == "")
                                //{
                                //    errorMessage = "Please fill Product Name in Import File";
                                //    throw new Exception(errorMessage);
                                //}
                                // product = await _productDataAccess.GetByProductId(product);
                                product.SetLoggedUserDetails(User);
                                product = await _productDataAccess.GetPurchaseImportProductById(product);
                                if (product == null)
                                    product.Name = split[index - vendorCount];
                                var vendorPurchaseItem = new VendorPurchaseItem();
                                vendorPurchaseItem.ProductStock = new ProductStock();
                                vendorPurchaseItem.ProductStock.ProductId = product.Id;
                                vendorPurchaseItem.ProductStock.Product.Name = product.Name;

                                vendorPurchaseItem.ProductStock.Product = product; //Added by Sarubala on 03-01-19

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "BatchNo");
                                //if (split[index - vendorCount] == "")
                                //{
                                //    errorMessage = "Batch Number is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                if (index != 9999)
                                {
                                    vendorPurchaseItem.ProductStock.BatchNo = split[index - vendorCount];
                                }
                                else
                                {
                                    vendorPurchaseItem.ProductStock.BatchNo = null;
                                }

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ExpiryDate");

                                //Added by Sarubala on 23-01-2018 to add additional date formats
                                if (dateFormat == "MM-yy" || dateFormat == "MM/yy")
                                {
                                    vendorPurchaseItem.ProductStock.ExpireDate = DateTime.ParseExact(split[index - vendorCount], dateFormat, CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    vendorPurchaseItem.ProductStock.ExpireDate = DateTime.ParseExact(split[index - vendorCount], dateFormat, CultureInfo.InvariantCulture);
                                    vendorPurchaseItem.ProductStock.ExpireDate = Convert.ToDateTime(split[index - vendorCount]);
                                }

                                decimal result;
                                //purchase setting header option Yes -- Added by Annadurai
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "GstTotal");
                                if (decimal.TryParse(split[index - vendorCount], out result))
                                {
                                    vendorPurchaseItem.ProductStock.GstTotal = Convert.ToDecimal(split[index - vendorCount]);
                                    string vatValforCustomTemplateYesHeaderOption = split[index - vendorCount];
                                    //ValidateVatforImport(vendorPurchaseItem.ProductStock.GstTotal, vatValforCustomTemplateYesHeaderOption);                                   
                                }
                                //else
                                //{
                                //    errorMessage = "GST is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                //index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Vat");
                                //if (decimal.TryParse(split[index - vendorCount], out result))
                                //{
                                //    vendorPurchaseItem.ProductStock.VAT = Convert.ToDecimal(split[index - vendorCount]);
                                //    string vatValforCustomTemplateYesHeaderOption = split[index - vendorCount];
                                //    ValidateVatforImport(vendorPurchaseItem.ProductStock.VAT, vatValforCustomTemplateYesHeaderOption);
                                //}
                                //else
                                //{
                                //    errorMessage = "Vat is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Units/Strip");
                                if (decimal.TryParse(split[index - vendorCount], out result))
                                {
                                    vendorPurchaseItem.PackageSize = Convert.ToDecimal(split[index - vendorCount]);
                                }
                                //else
                                //{
                                //    errorMessage = "Units/Strip is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                if (vendorPurchaseItem.PackageSize <= 0)
                                {
                                    errorMessage = "Units/Strip should be greater than 0";
                                    throw new Exception(errorMessage);
                                }
                                if (!isAllowDecimalCustom)
                                {
                                    if (vendorPurchaseItem.PackageSize % 1 != 0 && vendorPurchaseItem.PackageSize != null)
                                    {
                                        errorMessage = "Decimal value not allowed in Unit/Strips";
                                        throw new ArgumentException(errorMessage);
                                    }
                                }
                                if (vendorPurchaseItem.PackageSize == null)
                                {
                                    vendorPurchaseItem.AlternatePackageSize = Convert.ToString(split[index - vendorCount]);
                                }


                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "No.Strips");
                                if (decimal.TryParse(split[index - vendorCount], out result))
                                {
                                    vendorPurchaseItem.PackageQty = Convert.ToDecimal(split[index - vendorCount]);
                                }
                                //else
                                //{
                                //    errorMessage = "No.Strips is Mandatory";
                                //    throw new ArgumentException(errorMessage);
                                //}
                                if (vendorPurchaseItem.PackageQty <= 0)
                                {
                                    errorMessage = "No.Strips should be greater than 0";
                                    throw new Exception(errorMessage);
                                }
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Price/Strip");
                                if (decimal.TryParse(split[index - vendorCount], out result))
                                {
                                    vendorPurchaseItem.PackagePurchasePrice = Convert.ToDecimal(split[index - vendorCount]);
                                }
                                //else
                                //{
                                //    errorMessage = "Price/Strip is Mandatory";
                                //    throw new ArgumentException(errorMessage);
                                //}
                                if (vendorPurchaseItem.PackagePurchasePrice <= 0)
                                {
                                    errorMessage = "Price/Strip should be greater than 0";
                                    throw new Exception(errorMessage);
                                }
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "MRP/Strip");
                                if (decimal.TryParse(split[index - vendorCount], out result))
                                {
                                    vendorPurchaseItem.PackageSellingPrice = Convert.ToDecimal(split[index - vendorCount]);
                                    vendorPurchaseItem.PackageMRP = vendorPurchaseItem.PackageSellingPrice;

                                    if (vendorPurchaseItem.PackagePurchasePrice + (vendorPurchaseItem.PackagePurchasePrice * vendorPurchaseItem.ProductStock.GstTotal / 100) > vendorPurchaseItem.PackageSellingPrice)
                                    {
                                        errorMessage = "MRP Price must be Greater than Cost Price + GST";
                                        throw new Exception(errorMessage);
                                    }


                                }

                                //else
                                //{
                                //    errorMessage = "MRP/Strip is Mandatory";
                                //    throw new ArgumentException(errorMessage);
                                //}
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "FreeQty");
                                if (index != 9999)
                                {
                                    if (decimal.TryParse(split[index - vendorCount], out result))
                                    {
                                        vendorPurchaseItem.FreeQty = Convert.ToDecimal(split[index - vendorCount]);
                                    }

                                }
                                else
                                {
                                    vendorPurchaseItem.FreeQty = 0;
                                }
                                //if (Convert.ToString(vendorPurchaseItem.FreeQty) == "")
                                //{
                                //    errorMessage = "Please fill FreeQty in Import File";
                                //    throw new Exception(errorMessage);
                                //}

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ProductDiscount");
                                if (index != 9999)
                                {
                                    if (decimal.TryParse(split[index - vendorCount], out result))
                                    {
                                        vendorPurchaseItem.Discount = Convert.ToDecimal(split[index - vendorCount]);
                                    }

                                }
                                else
                                {
                                    vendorPurchaseItem.Discount = 0;
                                }
                                //if (Convert.ToString(vendorPurchaseItem.Discount) == "")
                                //{
                                //    errorMessage = "Please fill Discount in Import File";
                                //    throw new Exception(errorMessage);
                                //}
                                //index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Cst");
                                //if (index != 9999)
                                //{
                                //    if (decimal.TryParse(split[index - vendorCount], out result))
                                //    {
                                //        vendorPurchaseItem.ProductStock.CST = Convert.ToDecimal(split[index - vendorCount]);
                                //    }
                                //    else
                                //    {
                                //        vendorPurchaseItem.ProductStock.CST = 0;
                                //    }
                                //}
                                //else
                                //{
                                //    vendorPurchaseItem.ProductStock.CST = 0;
                                //}
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "HsnCode");
                                if (index != 9999)
                                {
                                    // if (decimal.TryParse(split[index - vendorCount], out result))
                                    //{
                                    vendorPurchaseItem.ProductStock.HsnCode = Convert.ToString(split[index - vendorCount]);
                                    // }

                                }
                                //if (vendorPurchaseItem.ProductStock.HsnCode == "")
                                //{
                                //    errorMessage = "Please fill HsnCode in Import File";
                                //    throw new Exception(errorMessage);
                                //}

                                //RackNo added by Violet 03 - 05 - 2017
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "RackNo");
                                if (index != 9999)
                                {
                                    if (split.Length > (index - vendorCount))
                                    {
                                        if (split[index - vendorCount].Length < 6)
                                        {
                                            vendorPurchaseItem.RackNo = split[index - vendorCount];
                                        }
                                        else
                                        {
                                            errorMessage = "Enter RackNo at least 5 characters";
                                            throw new ArgumentException(errorMessage);
                                        }
                                    }

                                }
                                //if (vendorPurchaseItem.RackNo=="")
                                //{
                                //    errorMessage = "Please fill RackNo in Import File";
                                //    throw new Exception(errorMessage);
                                //}
                                //BoxNo added by Settu 19-05-2017
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "BoxNo");
                                if (index != 9999)
                                {
                                    if (split.Length > (index - vendorCount))
                                    {
                                        if (split[index - vendorCount].Length < 6)
                                        {
                                            vendorPurchaseItem.ProductStock.Product.BoxNo = split[index - vendorCount];
                                        }
                                        else
                                        {
                                            errorMessage = "Enter BoxNo at least 5 characters";
                                            throw new ArgumentException(errorMessage);
                                        }
                                    }

                                }

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ProductMarkupPerc");
                                if (index != 9999 && purchaseSettings.EnableMarkup == true)
                                {
                                    if (split.Length > (index - vendorCount))
                                    {
                                        if (split[index - vendorCount] != "")
                                        {
                                            vendorPurchaseItem.MarkupPerc = Convert.ToDecimal(split[index - vendorCount]);
                                            if (vendorPurchaseItem.MarkupPerc < 0)
                                            {
                                                errorMessage = "Product markup perc should be greater than or equal to 0";
                                                throw new ArgumentException(errorMessage);
                                            }

                                            if ((double)vendorPurchaseItem.MarkupPerc > 99.99)
                                            {
                                                errorMessage = "Product markup perc should be less than 100%";
                                                throw new ArgumentException(errorMessage);
                                            }


                                            if ((double)(vendorPurchase.MarkupPerc == null ? 0 : vendorPurchase.MarkupPerc) + (double)(vendorPurchaseItem.MarkupPerc == null ? 0 : vendorPurchaseItem.MarkupPerc) > 99.99)
                                            {
                                                errorMessage = "Product markup + Invoice markup perc should be less than 100%";
                                                throw new ArgumentException(errorMessage);
                                            }
                                        }
                                    }
                                }
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "SchemeDiscountPerc");
                                if (index != 9999)
                                {
                                    if (split.Length > (index - vendorCount))
                                    {
                                        if (split[index - vendorCount] != "")
                                        {
                                            vendorPurchaseItem.SchemeDiscountPerc = Convert.ToDecimal(split[index - vendorCount]);
                                            if (vendorPurchaseItem.SchemeDiscountPerc < 0)
                                            {
                                                errorMessage = "Product scheme discount should be greater than or equal to 0";
                                                throw new ArgumentException(errorMessage);
                                            }

                                            if ((double)vendorPurchaseItem.SchemeDiscountPerc > 99.99)
                                            {
                                                errorMessage = "Product scheme discount should be less than 100%";
                                                throw new ArgumentException(errorMessage);
                                            }
                                        }
                                    }
                                }
                                //if (vendorPurchaseItem.ProductStock.Product.BoxNo == "")
                                //{
                                //    errorMessage = "Please fill BoxNo in Import File";
                                //    throw new Exception(errorMessage);
                                //}
                                vendorPurchaseItemList.Add(vendorPurchaseItem);
                            }
                        }
                        else
                        {
                            // Vendor Purchase data
                            if (i == vendorRowPos)
                            {
                                //check if vendor name exists
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "VendorName");
                                if (index != 9999)
                                {
                                    vendor.Name = split[index];
                                    vendor.SetLoggedUserDetails(User);
                                    var v = await _vendorDataAccess.GetVendorId(vendor);
                                    if (v != null)
                                        vendor = v;
                                    vendorPurchase.Vendor = vendor;
                                    vendorPurchase.VendorId = vendor.Id;
                                }
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "InvoiceNo");
                                //if (split[index] == "")
                                //{
                                //    errorMessage = "Invoice Number is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                if (index != 9999)
                                {
                                    if (split[index] != "")
                                    {
                                        vendorPurchase.InvoiceNo = split[index];
                                    }
                                }
                                else
                                {
                                    vendorPurchase.InvoiceNo = null;

                                }

                                //added by  nandhini 17.8.2017
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "InvoiceDate");
                                if (split[index] == "")
                                {
                                    errorMessage = "Please fill Invoice Date in Import File";
                                    throw new Exception(errorMessage);
                                }

                                vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat, CultureInfo.InvariantCulture);
                                DateTime today = DateTime.Now;
                                if (vendorPurchase.InvoiceDate >= today)
                                {
                                    errorMessage = "Invoice Date is Greater than Current Date";
                                    throw new Exception(errorMessage);
                                }
                                //end
                                //index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "InvoiceDate");
                                //vendorPurchase.InvoiceDate = DateTime.ParseExact(split[index], dateFormat, CultureInfo.InvariantCulture);
                                ////Convert.ToDateTime(invoiceDate);
                                //DateTime today = DateTime.Now;
                                //if (vendorPurchase.InvoiceDate >= today)
                                //{
                                //    errorMessage = "Invoice Date is Greater than Current Date";
                                //    throw new Exception(errorMessage);
                                //}
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "VendorDiscount");
                                if (index != 9999)
                                {
                                    if (split[index] != "")
                                    {
                                        vendorPurchase.Discount = Convert.ToDecimal(split[index]);
                                    }
                                }
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Amount");
                                //if (split[index] == "")
                                //{
                                //    errorMessage = "Amount is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                vendorPurchase.Credit = Convert.ToDecimal(split[index]);

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "VendorMarkupPerc");
                                if (index != 9999 && purchaseSettings.EnableMarkup == true)
                                {
                                    if (split.Length > index)
                                    {
                                        if (split[index] != "")
                                        {
                                            vendorPurchase.MarkupPerc = Convert.ToDecimal(split[index]);
                                            if (vendorPurchase.MarkupPerc < 0)
                                            {
                                                errorMessage = "Invoice markup perc should be greater than or equal to 0";
                                                throw new Exception(errorMessage);
                                            }
                                            if ((double)vendorPurchase.MarkupPerc > 99.99)
                                            {
                                                errorMessage = "Invoice markup perc should be less than 100%";
                                                throw new Exception(errorMessage);
                                            }
                                        }
                                    }
                                }
                            }
                            if (i >= vendorRowPos)
                            {
                                //check product exists
                                Product product = new Product();
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ProductName");
                                product.Name = split[index];
                                //for removing special char added by nandhini 19.5.17
                                {
                                    string input;
                                    if (product.Name.Contains('{'))
                                    {
                                        input = product.Name.Replace('{', '(');
                                        product.Name = input;
                                    }
                                    if (product.Name.Contains('}'))
                                    {
                                        input = product.Name.Replace('}', ')');
                                        product.Name = input;
                                    }

                                }
                                // product = await _productDataAccess.GetByProductId(product);
                                product.SetLoggedUserDetails(User);
                                product = await _productDataAccess.GetPurchaseImportProductById(product);
                                if (product == null)
                                    product.Name = split[index];
                                var vendorPurchaseItem = new VendorPurchaseItem();
                                vendorPurchaseItem.ProductStock = new ProductStock();
                                vendorPurchaseItem.ProductStock.ProductId = product.Id;
                                vendorPurchaseItem.ProductStock.Product.Name = product.Name;

                                vendorPurchaseItem.ProductStock.Product = product; //Added by Sarubala on 13-03-19

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "BatchNo");
                                //if (split[index] == "")
                                //{
                                //    errorMessage = "Batch Number is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                vendorPurchaseItem.ProductStock.BatchNo = split[index];
                                //added by  nandhini 17.8.2017
                                //index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ExpiryDate");
                                //index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ExpiryDate");
                                //vendorPurchaseItem.ProductStock.ExpireDate = Convert.ToDateTime(split[index - vendorCount]);
                                //end

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ExpiryDate");
                                vendorPurchaseItem.ProductStock.ExpireDate = DateTime.ParseExact(split[index], dateFormat, CultureInfo.InvariantCulture);

                                decimal result;
                                //purchase setting header option NO -- Added by Annadurai
                                // Gavaskar include 17-08-2017 Start
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "GstTotal");
                                if (decimal.TryParse(split[index], out result))
                                {
                                    vendorPurchaseItem.ProductStock.GstTotal = Convert.ToDecimal(split[index]);
                                    string vatValforCustomTemplateNoHeaderOption = split[index];
                                    //ValidateVatforImport(vendorPurchaseItem.ProductStock.GstTotal, vatValforCustomTemplateNoHeaderOption);                                    
                                }
                                //else
                                //{
                                //    errorMessage = "GST is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}

                                // Gavaskar include 17-08-2017 End

                                // Commented by Gavaskar 10-08-2017 Purchase setting Vat Start
                                //index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Vat");
                                //if (decimal.TryParse(split[index], out result))
                                //{
                                //    vendorPurchaseItem.ProductStock.VAT = Convert.ToDecimal(split[index]);
                                //    string vatValforCustomTemplateNoHeaderOption = split[index];
                                //    ValidateVatforImport(vendorPurchaseItem.ProductStock.VAT, vatValforCustomTemplateNoHeaderOption);
                                //}
                                //else
                                //{
                                //    errorMessage = "Vat is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                // Commented by Gavaskar 10-08-2017 Purchase setting Vat End

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Units/Strip");
                                if (decimal.TryParse(split[index], out result))
                                {
                                    vendorPurchaseItem.PackageSize = Convert.ToDecimal(split[index]);
                                }
                                if (vendorPurchaseItem.PackageSize == null)
                                {
                                    vendorPurchaseItem.AlternatePackageSize = Convert.ToString(split[index - vendorCount]);
                                }
                                //else
                                //{
                                //    errorMessage = "Units/Strip is Mandatory";
                                //    throw new Exception(errorMessage);
                                //}
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "No.Strips");
                                if (decimal.TryParse(split[index], out result))
                                {
                                    vendorPurchaseItem.PackageQty = Convert.ToDecimal(split[index]);
                                }
                                //else
                                //{
                                //    errorMessage = "No.Strips is Mandatory";
                                //    throw new ArgumentException(errorMessage);
                                //}
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Price/Strip");
                                if (decimal.TryParse(split[index], out result))
                                {
                                    vendorPurchaseItem.PackagePurchasePrice = Convert.ToDecimal(split[index]);
                                }
                                //else
                                //{
                                //    errorMessage = "Price/Strip is Mandatory";
                                //    throw new ArgumentException(errorMessage);
                                //}
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "MRP/Strip");
                                if (decimal.TryParse(split[index], out result))
                                {
                                    vendorPurchaseItem.PackageSellingPrice = Convert.ToDecimal(split[index]);

                                    vendorPurchaseItem.PackageMRP = vendorPurchaseItem.PackageSellingPrice;
                                }

                                //else
                                //{
                                //    errorMessage = "MRP/Strip is Mandatory";
                                //    throw new ArgumentException(errorMessage);
                                //}
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "FreeQty");
                                if (index != 9999)
                                {
                                    if (decimal.TryParse(split[index], out result))
                                    {
                                        vendorPurchaseItem.FreeQty = Convert.ToDecimal(split[index]);
                                    }
                                    else
                                    {
                                        vendorPurchaseItem.FreeQty = 0;
                                    }
                                }
                                else
                                {
                                    vendorPurchaseItem.FreeQty = 0;
                                }
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ProductDiscount");
                                if (index != 9999)
                                {
                                    if (decimal.TryParse(split[index], out result))
                                    {
                                        vendorPurchaseItem.Discount = Convert.ToDecimal(split[index]);
                                    }
                                    else
                                    {
                                        vendorPurchaseItem.Discount = 0;
                                    }
                                }
                                else
                                {
                                    vendorPurchaseItem.Discount = 0;
                                }
                                // Commented by Gavaskar 10-08-2017 Purchase setting Cst Start
                                //index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "Cst");
                                //if (index != 9999)
                                //{
                                //    if (decimal.TryParse(split[index], out result))
                                //    {
                                //        vendorPurchaseItem.ProductStock.CST = Convert.ToDecimal(split[index]);
                                //    }
                                //    else
                                //    {
                                //        vendorPurchaseItem.ProductStock.CST = 0;
                                //    }
                                //}
                                //else
                                //{
                                //    vendorPurchaseItem.ProductStock.CST = 0;
                                //}
                                // Commented by Gavaskar 10-08-2017 Purchase setting Cst End

                                // Added by Gavaskar Hsncode 17-08-2017 Start
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "HsnCode");
                                if (index != 9999)
                                {
                                    vendorPurchaseItem.ProductStock.HsnCode = Convert.ToString(split[index]);
                                }
                                //if (vendorPurchaseItem.ProductStock.HsnCode == "")
                                //{
                                //    errorMessage = "Please fill HsnCode in Import File";
                                //    throw new Exception(errorMessage);
                                //}
                                // Added by Gavaskar Hsncode 17-08-2017 End

                                //RackNo added by Violet 03-05-2017
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "RackNo");
                                if (index != 9999)
                                {
                                    if (split.Length > index)
                                    {
                                        if (split[index].Length < 6)
                                        {
                                            vendorPurchaseItem.RackNo = split[index];
                                        }
                                        else
                                        {
                                            errorMessage = "Enter RackNo at least 5 characters";
                                            throw new ArgumentException(errorMessage);
                                        }
                                    }
                                    else
                                    {
                                        vendorPurchaseItem.RackNo = null;
                                    }
                                }
                                else
                                {
                                    vendorPurchaseItem.RackNo = null;
                                }
                                //BoxNo added by Settu 19-05-2017
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "BoxNo");
                                if (index != 9999)
                                {
                                    if (split.Length > index)
                                    {
                                        if (split[index].Length < 6)
                                        {
                                            vendorPurchaseItem.ProductStock.Product.BoxNo = split[index];
                                        }
                                        else
                                        {
                                            errorMessage = "Enter RackNo at least 5 characters";
                                            throw new ArgumentException(errorMessage);
                                        }
                                    }
                                    else
                                    {
                                        vendorPurchaseItem.ProductStock.Product.BoxNo = null;
                                    }
                                }
                                else
                                {
                                    vendorPurchaseItem.ProductStock.Product.BoxNo = null;
                                }

                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "ProductMarkupPerc");
                                if (index != 9999 && purchaseSettings.EnableMarkup == true)
                                {
                                    if (split.Length > index)
                                    {
                                        if (split[index] != "")
                                        {
                                            vendorPurchaseItem.MarkupPerc = Convert.ToDecimal(split[index]);
                                            if (vendorPurchaseItem.MarkupPerc < 0)
                                            {
                                                errorMessage = "Product markup perc should be greater than or equal to 0";
                                                throw new ArgumentException(errorMessage);
                                            }

                                            if ((double)vendorPurchaseItem.MarkupPerc > 99.99)
                                            {
                                                errorMessage = "Product markup perc should be less than 100%";
                                                throw new ArgumentException(errorMessage);
                                            }


                                            if ((double)(vendorPurchase.MarkupPerc == null ? 0 : vendorPurchase.MarkupPerc) + (double)(vendorPurchaseItem.MarkupPerc == null ? 0 : vendorPurchaseItem.MarkupPerc) > 99.99)
                                            {
                                                errorMessage = "Product markup + Invoice markup perc should be less than 100%";
                                                throw new ArgumentException(errorMessage);
                                            }
                                        }
                                    }
                                }
                                index = GetTemplatePurchaseMasterItem(TemplatePurchaseMaster, "SchemeDiscountPerc");
                                if (index != 9999)
                                {
                                    if (split.Length > (index))
                                    {
                                        if (split[index] != "")
                                        {
                                            vendorPurchaseItem.SchemeDiscountPerc = Convert.ToDecimal(split[index]);
                                            if (vendorPurchaseItem.SchemeDiscountPerc < 0)
                                            {
                                                errorMessage = "Product scheme discount should be greater than or equal to 0";
                                                throw new ArgumentException(errorMessage);
                                            }

                                            if ((double)vendorPurchaseItem.SchemeDiscountPerc > 99.99)
                                            {
                                                errorMessage = "Product scheme discount should be less than 100%";
                                                throw new ArgumentException(errorMessage);
                                            }
                                        }
                                    }
                                }

                                vendorPurchaseItemList.Add(vendorPurchaseItem);
                            }
                        }
                        i++;
                    }
                    vendorPurchase.VendorPurchaseItem = vendorPurchaseItemList;
                    return vendorPurchase;
                }
                else
                {
                    errorMessage = "Please select the correct Vendor Name";
                    throw new Exception(errorMessage);
                }
            }
        }
        //end 22.7.2017
        private void ValidateVatforImport(decimal? vatValue, string valStringValue)
        {
            string errorMessage = string.Empty;

            if (vatValue < 0)
            {
                errorMessage = "Vat should be greater than 0";
                throw new Exception(errorMessage);
            }
            if (vatValue > 100)
            {
                errorMessage = "Vat should not be greater than 100";
                throw new Exception(errorMessage);
            }
            if (valStringValue.IndexOf(".") > 0)
            {
                if (valStringValue.Split('.')[1].Length > 2)
                {
                    errorMessage = "Vat should be between 0.00 and 100";
                    throw new Exception(errorMessage);
                }
                else if (vatValue > 100 && valStringValue.Split('.')[1].Length > 0)
                {
                    errorMessage = "Vat should be between 99.99 and 100";
                    throw new Exception(errorMessage);
                }

            }
        }
        public int GetTemplatePurchaseMasterItem(List<TemplatePurchaseChildMaster> data, string searchText)
        {
            int index = -1;
            var items = data.Where(x => x.VendorHeaderName == searchText && x.VendorHeaderName != "").Select(x => x);
            if (items.First().HeaderName != "")
            {
                index = items.First().HeaderIndex;
            }
            else
            {
                index = 9999;
            }
            //if (items.Count > 0)
            //{
            //}
            return index;
        }
        public class TempBuyTemplateSettings
        {
            public List<TemplatePurchaseChildMaster> templatePurchaseChildMaster { get; set; }
            public TemplatePurchaseChildMasterSettings templatePurchaseChildMasterSettings { get; set; }
        }
        // Newly Added Gavaskar 21-02-2017 Start
        [Route("[action]")]
        public async Task<IEnumerable<Vendor>> BuyImportVendorList(Vendor vendor)
        {
            vendor.SetLoggedUserDetails(User);
            return await _vendorPurchaseManager.VendorList(vendor);
        }
        // Newly Added Gavaskar 21-02-2017 End
        internal class datatable
        {
        }
    }
}
