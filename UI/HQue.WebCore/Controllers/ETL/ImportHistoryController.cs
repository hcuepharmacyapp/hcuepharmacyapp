﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HQue.Biz.Core.ETL;
using Utilities.Helpers;
using System.Collections.Generic;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace HQue.WebCore.Controllers.ETL
{
    [Route("[controller]")]
    public class ImportHistoryController : BaseController
    {
        private readonly Import _import;
        private readonly IHostingEnvironment _environment;
        private readonly ConfigHelper _configHelper;

        public ImportHistoryController(Import import, IHostingEnvironment environment, ConfigHelper configHelper)
            : base(configHelper)
        {
            _import = import;
            _environment = environment;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult Index(bool? success)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            ViewBag.Success = success;
            return View();
        }

        public async Task UploadStock(IFormFile file)
        {
            int missedItemsCount = 0;
            List<int> missedItems = new List<int>();
            /*Modified by Martin on 16/06/2017*/
            try
            {
                //check file format
                //if not .csv or ttx then throw error
                var fileExt = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                if (!(fileExt == ".csv")) //|| fileExt == ".ttx"
                    throw new Exception("Not a valid file format! Upload only .csv formats.");//or .ttx 

                var dataStream = file.OpenReadStream();                

                dataStream.Position = 0;
                missedItems = await _import.ImportData(dataStream, User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id());
                missedItemsCount = missedItems.Count;
                ViewBag.missedItems = missedItems;
                ViewBag.missedItemsCount = missedItemsCount;

                ViewBag.Success = true;
            }
            catch (Exception ex)
            {                
                ViewBag.Success = false;
                ViewBag.message = ex.Message;                
            }                      
            
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file, string importtype)
        {
            ViewBag.ImportType = importtype;            

            try
            {
                if (ConfigHelper.AppConfig.OfflineMode) //Added by Sarubala on 30-12-18 to enable stock import in offline
                {
                    await UploadStock(file);
                }
                else
                {
                    if (importtype == "productstock")
                    {
                        await _import.ImportStockOnline(file.OpenReadStream(), User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id(), 2, "");

                        ViewBag.success = true;
                    }
                    else
                    {
                        //check file format
                        //if not .csv or ttx then throw error
                        var fileExt = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                        if (!(fileExt == ".csv" || fileExt == ".ttx"))
                            throw new Exception("Not a valid file format! Upload only .csv or .ttx formats.");

                        var fileName = string.Format("{0}_{1}", file.FileName, User.Identity.InstanceId());
                        await UploadFileToAzure(file.OpenReadStream(), fileName);

                        switch (importtype)
                        {
                            case "purchases":
                                await _import.ImportHistoricalPurchases(fileName, User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id());
                                break;
                            case "sales":
                                await _import.ImportSalesPurchases(fileName, User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id());
                                break;
                            case "vendor":
                                await _import.ImportVendorMaster(fileName, User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id());
                                break;
                            case "productmaster":
                                await _import.ImportProductMaster(fileName, User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id());
                                break;
                            case "productstock": //Added by Poongodi on 02/08/2017
                                await _import.ImportProductStock(fileName, User.Identity.InstanceId(), User.Identity.AccountId(), User.Identity.Id());
                                break;

                        }

                        ViewBag.success = true;
                    }
                }
                
            }
            catch(Exception ex)
            {
                //return RedirectToAction("Index", new { success = false, errorDesc = ex.Message, missedItems = missedItemsCount });
                ViewBag.success = false;
                ViewBag.message = ex.Message;
              //  return Json(new { success = false, errorDesc = ex.Message, showPanel = true });

                return View();
            }
            //return RedirectToAction("Index", new { success = true, missedItems = missedItemsCount });            
            
            return View();
        }

        [Route("[action]")]
        [HttpGet]
        public FileResult SalesDownload()
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads", "Sales_Historical_template.csv");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "text/csv","Template.csv");
        }

        [Route("[action]")]
        [HttpGet]
        public FileResult PurchaseDownload()
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads", "Purchase_template.csv");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "text/csv", "Template.csv");
        }

        [Route("[action]")]
        [HttpGet]
        public FileResult VendorDownload()
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads", "Suppliers_template.csv");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "text/csv", "Template.csv");
        }

        [Route("[action]")]
        [HttpGet]
        public FileResult ProductDownload()
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads", "ProductMaster_template.csv");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "text/csv", "Template.csv");
        }
        [Route("[action]")]
        [HttpGet]
        public FileResult StockDownload()
        {
            var filePath = Path.Combine(_environment.WebRootPath, "uploads", "Import.csv");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "text/csv", "Template.csv");
        }
        private async Task<bool> UploadFileToAzure(Stream fileStream, string fileName)
        {
            try
            {
                // Retrieve storage account from connection string.
                //CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=hcuepharma;AccountKey=VwbOzsUhsjZpR0cKd9luGClEG3eFrMbrPtmcc7XTsWFywwV+M2Qukw7OK1H+L83sbin2oU08piLRhwz0WPglYg==;EndpointSuffix=core.windows.net");

                //discussed with poongodi for key manual add 4/7/18
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_configHelper.AppConfig.StorageConnection);


                //CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=hcuepharma;AccountKey=TctnlEF+LdslH5XanzkIoFqH1Mc2+9UVytVo59rfrsZ3HKYD56WPzd6T+HnXiqyuu9xbE2WxbuSAcHzIl1dIPw==;EndpointSuffix=core.windows.net");


                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference("files");

                // Retrieve reference to a blob named "myblob".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

                // Create or overwrite the "myblob" blob with contents from a local file.
                using (fileStream)
                {
                    await blockBlob.UploadFromStreamAsync(fileStream);
                }
            }
            catch(Exception e)
            {
                return false;
            }

            return true;
        }

    }

   
}
