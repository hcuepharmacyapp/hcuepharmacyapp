﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HQue.Biz.Core.ETL;
using Utilities.Helpers;
using System.Collections.Generic;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.NetworkInformation;
using HQue.Contract.Infrastructure.Master;
using HQue.Biz.Core.Master;
using HQue.ServiceConnector.Connector;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Inventory;

namespace HQue.WebCore.Controllers.ETL
{
    [Route("[controller]")]
    public class ImportProductController : BaseController
    {
        private readonly Import _import;

        private readonly ConfigHelper _configHelper;
        private readonly SalesManager _salesManager;
        private readonly ProductManager _productManager;
        public ImportProductController(Import import, ProductManager productManager, ConfigHelper configHelper, SalesManager salesManager)
            : base(configHelper)
        {
            _import = import;
            _productManager = productManager;
            _configHelper = configHelper;
            _salesManager = salesManager;
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult Index(bool? success, string exporttype)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            ViewBag.Success = success;
            ViewBag.ExportType = exporttype;
            return View();
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> Index(bool success, string exporttype)
        {

            try
            {
                bool bInternet = NetworkInterface.GetIsNetworkAvailable();
                string fileName = null;
                ViewBag.ExportType = exporttype;
                if (_configHelper.AppConfig.OfflineMode)
                {
                    if (bInternet)
                    {
                      
                        if (exporttype == "product")
                        {
                            Product userData = new Product();
                            userData.SetLoggedUserDetails(User);
                            var dataFile = await _productManager.ExportProductMaster(userData.AccountId);
                            FileInfo info = new FileInfo(dataFile);
                            HttpContext.Response.ContentType = "text/csv";
                            //fileName = string.Format("./wwwroot/temp/ProductMaster_{0}.csv", userData.AccountId);
                            fileName = string.Format("ProductMaster_{0}.csv", userData.AccountId);
                            await UploadFileToAzure(info.OpenRead(), fileName);
                            var syncproduct = new SyncProduct
                            {
                                fname = string.Format("ProductMaster_{0}.csv", userData.AccountId),
                                AccountId = User.AccountId(),
                                Userid = userData.CreatedBy

                            };
                            await ExportProducttoOnline(syncproduct);
                            //ViewBag.success = true;
                            //return RedirectToAction("Index", new { success = true, ExportType = exporttype });

                        }
                        else if (exporttype == "sales" || exporttype == "purchase") /*Added by Poongodi on 22/09/2017*/
                        {
                            Sales userData = new Sales();
                            userData.SetLoggedUserDetails(User);
                            MissedItems msdItem = new MissedItems();
                            msdItem.AccountId = userData.AccountId;
                            msdItem.InstanceId = userData.InstanceId;
                            msdItem.Type = exporttype;
                            var result = await GetMissedSalesItem(msdItem);
                            if (result.ItemList.Count > 0)
                            {
                                bool t = await _import.WriteMissedItemToSyncQue(result.ItemList,exporttype);
                            }
                        
                        }
                       
                    }

                    else
                    {
                        throw new Exception("Please check internet connectivity to sync Products.");

                    }
                }

              
            }
            catch (Exception ex)
            {
                //return RedirectToAction("Index", new { success = false, errorDesc = ex.Message, missedItems = missedItemsCount });
                ViewBag.success = false;
                ViewBag.message = ex.Message;


                return View();
            }

 
            return RedirectToAction("Index", new { ExportType = exporttype, success = true });
            //return View();
        }


        [Route("[action]")]
        private async Task<bool> UploadFileToAzure(Stream fileStream, string fileName)
        {
            try
            {
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=hcuepharma;AccountKey=VwbOzsUhsjZpR0cKd9luGClEG3eFrMbrPtmcc7XTsWFywwV+M2Qukw7OK1H+L83sbin2oU08piLRhwz0WPglYg==;EndpointSuffix=core.windows.net");


                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference("files");

                // Retrieve reference to a blob named "myblob".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

                // Create or overwrite the "myblob" blob with contents from a local file.
                using (fileStream)
                {
                    await blockBlob.UploadFromStreamAsync(fileStream);
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public async Task<string> ExportProducttoOnline(SyncProduct entity)
        {
            var sc = new RestConnector();
            var result = await sc.PostAsyc<string>(_configHelper.InternalApi.SyncProductUrl, entity);
            return result;
        }
        /// <summary>
        /// To Get  Missed Sales from Online added by Poongodi on 22/09/2017
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<MissedDetail> GetMissedSalesItem([FromBody]MissedItems entity)
        {
            var sc = new RestConnector();
            var result = await sc.PostAsyc<MissedDetail>(_configHelper.InternalApi.SyncMissedItem, entity);
            return result;
            // return await _etlDataAccess.GetMissedSalesItem(sAccountid, sinstanceId);

        }

    }


}
