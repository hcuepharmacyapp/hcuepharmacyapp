﻿using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Reminder
{
    public class ReminderController : BaseController
    {
        public ReminderController(ConfigHelper configHelper) : base(configHelper)
        {
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Customer()
        {
            return View();
        }

        public IActionResult Personal()
        {
            return View();
        }
    }
}
