﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Setup;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Setup;
using HQue.Biz.Core.Report;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class HierarchyReportController : BaseController
    {
        private readonly ReportManager _reportManager;
        private readonly InstanceSetupManager _instanceSetupManager;

        public HierarchyReportController(ReportManager reportManager, InstanceSetupManager instanceSetupManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
            _instanceSetupManager = instanceSetupManager;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> SelectInstance()
        {
           return await _instanceSetupManager.List(User.AccountId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> LowProductStockList(string id)
        {
            if (!string.IsNullOrEmpty(id) && id != "undefined")
            {
                return await _reportManager.LowProductStockReportList(id);
            }
            else
            {
                return await _reportManager.LowProductStockReportList(User.InstanceId());
            }
        }

        [Route("[action]")]
        public async Task<Instance> GetInstanceData()
        {
            if (User == null)
                return new Instance();

            return await _instanceSetupManager.GetById(User.InstanceId());
        }
    }
}