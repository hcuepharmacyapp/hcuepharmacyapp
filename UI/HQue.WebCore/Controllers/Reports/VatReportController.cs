﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Report;
using Utilities.Helpers;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class VatReportController : BaseController
    {
        private readonly ReportManager _reportManager;

        public VatReportController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
        }
        [HttpGet]
        [Route("[action]")]
        public IActionResult List(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }
        [HttpGet]
        [Route("[action]")]
        public IActionResult LocalPurchase(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<VendorPurchaseItem>> ListData([FromBody]DateRange data, string type,int filter, string sInstanceId)
        {
            return await _reportManager.PurchaseReportList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate,filter);
        }

        [Route("[action]")]
        public IActionResult ReturnList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<VendorPurchaseReturnItem>> ReturnListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            return await _reportManager.PurchaseReturnReportList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult SalesList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<SalesItem>> SalesListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            return await _reportManager.SalesReportListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult SalesReturnList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<SalesReturnItem>> SalesReturnListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            return await _reportManager.SalesReturnList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult ListCst()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<VendorPurchaseItem>> ListDataCst([FromBody]DateRange data, string type, string sInstanceId)
        {
            return await _reportManager.PurchaseReportListCst(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult ReturnListCst()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<VendorPurchaseReturnItem>> ReturnListDataCst([FromBody]DateRange data, string type, string sInstanceId)
        {
            return await _reportManager.PurchaseReturnReportListCst(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }
        [Route("[action]")]
        public IActionResult LocalSalesList()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult LocalSales()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<SalesItem>> LocalSalesListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            return await _reportManager.LocalSalesReportListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }
        //Consolidated VAT report created for "Ragavendra Medicals" by Poongodi on 17022017
        [Route("[action]")]
        public IActionResult SalesConsolidatedVAT()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> SalesConsolidatedVATListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalSaleConsolidatedVATListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockInventoryData>> StockInventoryListData()
        {
            return await _reportManager.StockInventoryList(User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public IActionResult StockInventoryList()
        {
            return View();
        }

        //[HttpPost]
        //[Route("action")]
        //public async Task<List<StockInventory>> StockInventoryListData([FromBody]DateRange data, string type)
        //{
        //    return await _reportManager.StockInventoryList(User.AccountId(), User.InstanceId());
        //}

        //[HttpPost]
        //[Route("action")]
        //public async Task<List<StockInventory>> StockInventoryListData([FromBody]DateRange data, string type)
        //{
        //    return await _reportManager.StockInventoryList(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        //}

        //[Route("[action]")]
        //public IActionResult ReturnList()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[Route("[action]")]
        //public async Task<List<VendorPurchaseReturnItem>> ReturnListData([FromBody]DateRange data, string type)
        //{
        //    return await _vatReportManager.BuyReturnReportList(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        //}

        //[Route("[action]")]
        //public IActionResult BuySummaryList()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[Route("[action]")]
        //public async Task<List<VendorPurchaseItem>> PurchaseSummaryListData([FromBody]DateRange data, string type)
        //{
        //    return await _vatReportManager.BuySummaryList(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        //}
    }
}
