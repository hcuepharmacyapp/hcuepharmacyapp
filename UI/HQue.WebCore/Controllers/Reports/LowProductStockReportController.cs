﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Biz.Core.Report;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Inventory;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class LowProductStockReportController : BaseController
    {
        private readonly ReportManager _reportManager;

        public LowProductStockReportController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> LowProductStockList()
        {
            return await _reportManager.LowProductStockReportList(User.InstanceId());
        }

        
    }
}
