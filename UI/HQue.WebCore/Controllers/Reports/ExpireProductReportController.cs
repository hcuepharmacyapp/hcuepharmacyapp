﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Setup;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Report;
using HQue.Biz.Core.Setup;
using HQue.Contract.Infrastructure.Misc;

using HQue.Contract.Infrastructure.Master;
using HQue.Biz.Core.Master;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class ExpireProductReportController : BaseController
    {
        private readonly ReportManager _reportManager;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly VendorManager _vendorManager;

        public ExpireProductReportController(ReportManager reportManager, InstanceSetupManager instanceSetupManager, VendorManager vendorManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
            _instanceSetupManager = instanceSetupManager;
            _vendorManager = vendorManager;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> SelectInstance()
        {
            return await _instanceSetupManager.List(User.AccountId());
        }
        //date parameter added by nandhini 13-9-17
        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> ExpireProductList([FromBody]DateRange data, string id)
        {
            if (!string.IsNullOrEmpty(id) && id != "undefined")
            {
                return await _reportManager.ExpireProductList(id, data.FromDate, data.ToDate);
            }
            else
            {
                return await _reportManager.ExpireProductList(User.InstanceId(), data.FromDate, data.ToDate);
            }
        }

        [Route("[action]")]
        public async Task<Instance> GetInstanceData()
        {
            if (User == null)
                return new Instance();

            return await _instanceSetupManager.GetById(User.InstanceId());
        }

        //Newly Added Gavaskar 25-10-2016 Start
        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> UpdateIsMovingStockExpire([FromBody]List<ProductStock> productStock)
        {
            await _reportManager.UpdateIsMovingStockExpire(productStock);
            return productStock;
        }
        //Newly Added Gavaskar 25-10-2016 End

        [Route("[action]")]
        public async Task<IEnumerable<Vendor>> VendorList(Vendor vendor)
        {
            vendor.SetLoggedUserDetails(User);
            return await _vendorManager.VendorList(vendor);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ProductStock[]> vendorUpdate([FromBody]List<ProductStock> productStocks)
        {
            return await _reportManager.vendorExpireUpdate(productStocks);
        }
    }
}
