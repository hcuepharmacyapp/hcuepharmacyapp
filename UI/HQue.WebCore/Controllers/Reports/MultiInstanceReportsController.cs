﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Biz.Core.Report;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Inventory;
using System;
using HQue.Contract.Infrastructure.Setup;
using System.Linq;
using HQue.Contract.Infrastructure.Reports;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class MultiInstanceReportsController : BaseController
    {
        private readonly ReportManager _reportManager;

        public MultiInstanceReportsController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
        }

        [Route("[action]")]
        public IActionResult SalesSummary()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //public async Task<List<Sales>> SalesSummaryListData(DateTime fromDate, DateTime toDate, string rptType,bool allInstances, string instanceIds)
        public async Task<List<SalesSummaryDayWiseReport>> SalesSummaryListData([FromBody]ReportSearchRequest reportSearchRequest)
        {
            DateTime fromDate = reportSearchRequest.fromDate;
            DateTime toDate = reportSearchRequest.toDate;
            string rptType = reportSearchRequest.rptType;
            bool allInstances = reportSearchRequest.allInstances;
            List<string> instanceIds = reportSearchRequest.instanceIds;
            return await _reportManager.SalesSummaryListData(User.AccountId(), rptType, allInstances, instanceIds, fromDate, toDate);
        }

    }

    public class ReportSearchRequest
    {
        public DateTime fromDate;
        public DateTime toDate;
        public string rptType;
        public bool allInstances;
        public List<string> instanceIds;
    }
}
