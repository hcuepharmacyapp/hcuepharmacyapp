﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Setup;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Reports;
using HQue.Biz.Core.Report;
using HQue.Biz.Core.Setup;
using System.Linq;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Master;
using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Utilities.Report;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class StockReportController : BaseController
    {
        private readonly ReportManager _reportManager;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly ConfigHelper _configHelper;

        public StockReportController(ReportManager reportManager, InstanceSetupManager instanceSetupManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
            _instanceSetupManager = instanceSetupManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult AllBranchConsolidated()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> SelectInstance()
        {
            return await _instanceSetupManager.List(User.AccountId());
        }

        [HttpPost]
        [Route("[action]")]
        //Stock Type parameter added by Poongodi on 13/03/2017
        public async Task<List<ProductStock>> StockProductList(string category, string manufacturer,string sRackNo,string sBoxNo, string id,string sStockType, bool isProductWise, string alphabet1, string alphabet2, [FromBody]DateRange data)
        {
            if (category != "undefined" && category != "null")
                return await _reportManager.StockReportList(id, User.AccountId(), sStockType, isProductWise, category, "", "", "", "", "", data.FromDate, data.ToDate);
            else if (manufacturer != "undefined" && manufacturer != "null")
                return await _reportManager.StockReportList(id, User.AccountId(), sStockType, isProductWise, "", manufacturer, "", "", "", "", data.FromDate, data.ToDate);
            else if (sRackNo != "undefined" && sRackNo != "null")
                return await _reportManager.StockReportList(id, User.AccountId(), sStockType, isProductWise, "", "", sRackNo, "", "", "", data.FromDate, data.ToDate);
            else if (sBoxNo != "undefined" && sBoxNo != "null")
                return await _reportManager.StockReportList(id, User.AccountId(), sStockType, isProductWise, "", "", "", sBoxNo, "", "", data.FromDate, data.ToDate);
            else if (alphabet1 != "undefined" && alphabet1 != "null")
                return await _reportManager.StockReportList(id, User.AccountId(), sStockType, isProductWise, "", "", "", sBoxNo, alphabet1, alphabet2, data.FromDate, data.ToDate);
            else
                return await _reportManager.StockReportList(id, User.AccountId(), sStockType, isProductWise, "", "", "", "", "", "", data.FromDate, data.ToDate);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> GetAllBranchConsolidatedList()
        {
            return await _reportManager.StockAllBranchConsolidatedList(User.AccountId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> StockProductListByCategory(string id, string sStockType, string category)  
        {
            return await _reportManager.StockReportCategoryList(id, sStockType, category);  
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> StockProductListByManufacturer(string id, string sStockType, string manufacturer)
        {
            return await _reportManager.StockReportManufacturerList(id, sStockType, manufacturer);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> StockProductListByLocation(string id, string sStockType, string sSearchText,string sSearchType)
        {
            return await _reportManager.StockProductListByLocation(id, sStockType, sSearchText, sSearchType);
        }
        [Route("[action]")]
        public IActionResult StockLedger()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult TempStockList()
        {
            return View();
        }
        

        [Route("[action]")]
        public IActionResult SelfConsumptionList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<TempVendorPurchaseItem>> TempStockListData([FromBody]DateRange data, string type, string sInstanceId,string productId)
        {            
            return await _reportManager.TempStockListData(type, User.AccountId(), sInstanceId, productId, data.FromDate, data.ToDate);
        }

        //added by nandhini for product list report
        [Route("[action]")]
        public IActionResult ProductList()
        {
            ViewBag.InstanceName = User.InstanceName();
            return View();
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<List<Product>> productListData(string type)
        {
            return await _reportManager.productListData(type,User.AccountId(), User.InstanceId()); //User.InstanceId()
        }

        //end
        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockLedger>> GetStockLedger( [FromBody]StockLedger data)
        {
            return await _reportManager.GetStockLedger(data, User.AccountId(), User.InstanceId()); //User.InstanceId()
        }

        [Route("[action]")]
        public IActionResult StockAdjustmentReport()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockAdjustment>> GetStockAdjustmentReport([FromBody]StockAdjustment sa, string type)
        {
            string currentInstanceId = sa.InstanceId;
            sa.SetLoggedUserDetails(User);
            sa.InstanceId = currentInstanceId;
            return await _reportManager.GetStockAdjustmentReport(sa, type);
        }

        [Route("[action]")]
        public IActionResult StockValueReport()
        {
            return View();
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<List<StockValueReport>> GetStockValueReport()
        {
            return await _reportManager.GetStockValueReport(User.AccountId(), User.InstanceId());
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<List<StockValueReport>> GetSavedReport()
        {
            StockValueReport svr = new Contract.Infrastructure.Reports.StockValueReport();
            svr.SetLoggedUserDetails(User);
            return await _reportManager.GetSavedReport(svr);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockValueReport>> SaveReport([FromBody]List<StockValueReport> svlist)
        {            
            svlist.First().SetLoggedUserDetails(User);
            return await _reportManager.SaveStockValueReport(svlist);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockValueReport>> GetUniqueSavedReport(string reportNo)
        {
            return await _reportManager.GetUniqueSavedReport(reportNo, User.InstanceId(), User.AccountId());
        }

        [Route("[action]")]
        public IActionResult ProductAgeAnalyseList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> ProductAgeAnalyseListData([FromBody]DateRange data, string type, string sInstanceId, string productId)
        {
            return await _reportManager.ProductAgeAnalyseListData(type, User.AccountId(), sInstanceId, productId, data.FromDate, data.ToDate);
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<List<SelfConsumption>> selfConsumptionListData([FromBody]DateRange data, string type, string sInstanceId, string productId, string invoiceNo)
        {
            return await _reportManager.selfConsumptionListData(type, User.AccountId(), sInstanceId, productId, invoiceNo, data.FromDate, data.ToDate);
        }

        //added by Gavaskar 05-07-2017 for Stock Transfer Report
        [Route("[action]")]
        public IActionResult StockTransferReport()
        {
            ViewBag.InstanceName = User.InstanceName();
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockTransferItem>> stockTransferList([FromBody]DateRange data, string type, string sInstanceId)
        {
            return await _reportManager.StockTransferList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult PhysicalStockHistory()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<PhysicalStockHistory>> physicalStockHistoryList([FromBody]DateRange date, string type, string InstanceId, string ProductId, string UserName, string UserId)
        {
            return await _reportManager.physicalStockHistoryList(date.FromDate, date.ToDate, type, User.AccountId(), InstanceId, ProductId, UserName, UserId);
        }
        [Route("[action]")]
        public IActionResult StockMovementReport()
        {
            ViewBag.InstanceName = User.InstanceName();
            //var sMsg = _reportManager.ClosingStockLog(User.AccountId(), User.InstanceId());
            //ViewBag.Message = sMsg.Result.ToString();
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockMovement>> StockMovementList([FromBody]DateRange data, string type, string sInstanceId)
        {
             
         return await _reportManager.StockMovementList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
            
        }

        [Route("[action]")]
        public IActionResult StockTransferDetail()
        {
            ViewBag.InstanceName = User.InstanceName();
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockTransferItem>> StockTransferDetailList([FromBody]DateRange data, string type, string sInstanceId, string sReportType)
        {
            return await _reportManager.StockTransferDetail(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, sReportType);
        }

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> StockListDataForPrint(string category, string manufacturer, string sRackNo, string sBoxNo, string id, string sStockType, bool isProductWise, string alphabet1, string alphabet2, DateTime fromDate, DateTime toDate)
        {
            /* bool isOffline = _configHelper.AppConfig.OfflineMode;*/            
            List<ProductStock> PSList = new List<ProductStock>();

            if (category == "undefined" || category == "null" || category == null)
            {
                category = "";
            }
            if (manufacturer == "undefined" || manufacturer == "null" || manufacturer == null)
            {
                manufacturer = "";
            }
            if (sRackNo == "undefined" || sRackNo == "null" || sRackNo == null)
            {
                sRackNo = "";
            }
            if (sBoxNo == "undefined" || sBoxNo == "null" || sBoxNo == null)
            {
                sBoxNo = "";
            }
            if (alphabet1 == "undefined" || alphabet1 == "null" || alphabet1 == null)
            {
                alphabet1 = "";
                alphabet2 = "";
            }

            PSList = await _reportManager.StockListDataForPrint(id, User.AccountId(), sStockType, isProductWise, category, manufacturer, sRackNo, sBoxNo, alphabet1, alphabet2, fromDate, toDate);

            if (PSList.Count == 0)
                return RedirectToAction("List", "StockReport");

            string name = "";

            name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Stock");


            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                //if (printType == "1")
                writer.Write(GenerateStockReport_A4(PSList, isProductWise), false, Encoding.UTF8);
                //else
                //    writer.Write(GenerateStockReport_A4PageBreak(List), false, Encoding.UTF8);
            }

            HttpContext.Response.ContentType = "application/notepad";
            //if (printType == "1")
            return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Stock" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            //else
            //    return File(info.OpenRead(), "application/notepad", string.Format("hq_SHed_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
        }

        private string GenerateStockReport_A4(List<ProductStock> List, bool isProductWise)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 95;
            rpt.Rows = 38;

            WriteReportHeader_A4(rpt, List, isProductWise);
            WriteReportItems_A4(rpt, List, isProductWise);

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A4(PlainTextReport rpt, List<ProductStock> List, bool isProductWise)
        {
            
            rpt.NewLine();
            rpt.Write(List[0].Product.Instance.Name, 88, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write(List[0].Product.Instance.FullAddress1, 88, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write("DL No   : " + List[0].Product.Instance.DrugLicenseNo + " / " + "GSTIN :" + List[0].Product.Instance.GsTinNo, 88, 0, Alignment.Center);

            rpt.NewLine();
            if (List[0].BatchNo != null)
            {
                rpt.Write("Stock Report - " + "Batch Wise", 88, 0, Alignment.Center);
            }
            else
            {
                rpt.Write("Stock Report - " + "Product Wise", 88, 0, Alignment.Center);
            }
            rpt.Write();
            rpt.NewLine();
            rpt.Write();
            rpt.Drawline();
            rpt.NewLine();
            if ((User.AccountId() == "c503ca6e-f418-4807-a847-b6886378cf0b" || User.AccountId() == "67067991-0e68-4779-825e-8e1d724cd68b" || User.AccountId() == "b322f4ad-c054-441a-8a43-c950dd205e50" || User.AccountId() == "63ED272D-1122-4E04-AFE8-C5A0F13B4F0E") && isProductWise)
            {
                rpt.Write("S.No ", 7, 0);
                rpt.Write("Product", 20, 8);
                rpt.Write("RackNo", 6, 53);
                rpt.Write("BoxNo", 5, 60);
                rpt.Write("Stock", 7, 66, Alignment.Right);
                rpt.Write();
                rpt.Drawline();
            }
            else { 
                rpt.Write("S.No ", 7, 0);
            
                rpt.Write("Product", 20, 8);
            if (List[0].BatchNo != null)
            {
                rpt.Write("Batch", 5, 29);
            }
            if (List[0].ExpireDate.ToFormat() != "01-Jan-0001")
            {
                rpt.Write("Exp", 5, 35);
            }
            rpt.Write("Cate", 5, 41);
            rpt.Write("MFR", 4, 48);
            rpt.Write("RackNo", 6, 53);
            rpt.Write("BoxNo", 5, 60);
            rpt.Write("Stock", 7, 66, Alignment.Right);
            //rpt.Write("C.Pri(GST)", 8, 73); //rpt.Write("MRP", 4, 82);
            rpt.Write("MRP/Str", 7, 74, Alignment.Right);
            if (!isProductWise)
                rpt.Write("Cost Value", 13, 81, Alignment.Right);
            rpt.Write();
            rpt.Drawline();
            }
        }

        private void WriteReportItems_A4(PlainTextReport rpt, List<ProductStock> List, bool isProductWise)
        {
            int len = 0;
            string proName = null;
            decimal? CostValue = 0;

            for (var x = 0; x < List.Count; x++)
            {
                rpt.NewLine();
                 if ((User.AccountId()== "67067991-0e68-4779-825e-8e1d724cd68b" || User.AccountId() == "b322f4ad-c054-441a-8a43-c950dd205e50" || User.AccountId() == "63ED272D-1122-4E04-AFE8-C5A0F13B4F0E") && isProductWise)
                  
                    {
                    rpt.Write((x + 1).ToString(), 7, 0);
                    proName = List[x].Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");
                    rpt.Write(proName.ToUpper(), 20, 8);
                    rpt.Write(List[x].Product.RackNo, 7, 53, Alignment.Center);
                    rpt.Write(List[x].Product.BoxNo, 5, 60);
                    rpt.Write(string.Format("{0:0}", List[x].Stock), 7, 66, Alignment.Right);
                    rpt.Write();
                }
                else
                {
                    rpt.Write((x + 1).ToString(), 7, 0);
                    proName = List[x].Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");
                    rpt.Write(proName.ToUpper(), 20, 8);
                    if (List[x].BatchNo != null)
                    {
                        int lenBatchNo = List[x].BatchNo.Length;
                        rpt.Write((lenBatchNo >= 5) ? List[x].BatchNo.Substring(lenBatchNo - 5) : List[x].BatchNo, 5, 29);
                    }
                    if (List[x].ExpireDate.ToFormat() != "01-Jan-0001")
                    {
                        rpt.Write(string.Format("{0:MM/yy}", List[x].ExpireDate), 5, 35);
                    }
                    rpt.Write(List[x].Product.Category, 5, 41);
                    rpt.Write(List[x].Product.Manufacturer, 4, 48, Alignment.Center);
                    rpt.Write(List[x].Product.RackNo, 7, 53, Alignment.Center);
                    rpt.Write(List[x].Product.BoxNo, 5, 60);
                    rpt.Write(string.Format("{0:0}", List[x].Stock), 7, 66, Alignment.Right);
                    decimal? mrpStrip = 0;
                    if (List[x].PackageSize > 0)
                        mrpStrip = (List[x].SellingPrice * List[x].PackageSize);
                    rpt.Write(string.Format("{0:0}", mrpStrip), 7, 74, Alignment.Right);
                    if (List[x].BatchNo != null)
                    {
                        decimal? cst = (List[x].CostPriceGst) == null ? 0 : (List[x].CostPriceGst);
                        rpt.Write(string.Format("{0:0}", (cst == null) ? 0 : cst), 13, 82, Alignment.Right);
                        CostValue = CostValue + ((cst == null) ? 0 : cst);
                    }

                    rpt.Write();
                    len++;
                }
            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();
            rpt.Write();
            rpt.Drawline();
            if (!isProductWise)
            {
                rpt.Write("Total Cost Value(GST): " + string.Format("{0:0.00}", CostValue), 45, 0, Alignment.Center);
                rpt.Write();
                rpt.Drawline();
            }
        }

    }
}
