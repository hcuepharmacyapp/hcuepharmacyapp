﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Setup;
using Utilities.Helpers;
using HQue.Biz.Core.Setup;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class BranchReportController : BaseController
    {
        private readonly InstanceSetupManager _instanceSetupManager;

        public BranchReportController(InstanceSetupManager instanceSetupManager, ConfigHelper configHelper) : base(configHelper)
        {
            _instanceSetupManager = instanceSetupManager;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IEnumerable<Instance>> ListData()
        {
            return await _instanceSetupManager.ReportList(User.AccountId());
        }
    }
}
