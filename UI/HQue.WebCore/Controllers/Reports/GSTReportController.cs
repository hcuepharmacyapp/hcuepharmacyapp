﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Report;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Reports;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class GSTReportController : BaseController
    {
        private readonly ReportManager _reportManager;

        public GSTReportController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
        }
         
        [HttpGet]
        [Route("[action]")]
        public IActionResult GSTR3B(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
       
        public async Task<List<GSTReport>> GSTR3BListData([FromBody]DateRange data, string type,int filter, string sInstanceId,string sSearchType, string sGsttinNo, int nInvoiceDt)
        {
            return await _reportManager.GST3BReportList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate,filter,sSearchType,sGsttinNo,nInvoiceDt);
        }

        // added for GST REPORTS by Nandhini on 29/08/2017
        [HttpGet]
        [Route("[action]")]
        public IActionResult HSNReport(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
     
        public async Task<List<GSTReport>> hsnReportListData()
        {
            return await _reportManager.hsnReportList(User.AccountId());
        }

        [Route("[action]")]
        public IActionResult ControlReport()
        {
            return View();
        }
       


        [HttpPost]
        [Route("[action]")]

        public async Task<List<ControlReport>> ControlListData([FromBody]DateRange data, int filter, string sInstanceId)
        {
            return await _reportManager.controlReportList( User.AccountId(), sInstanceId, data.FromDate, data.ToDate, filter);
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GSTR1Purchase(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }
        [HttpPost]
        [Route("[action]")]

        public async Task<List<GSTReport>> GSTR1PurchaseListData([FromBody]DateRange data, string type, int filter, string sInstanceId, string sSearchType, string sGsttinNo,int isRegister, int nInvoiceDt)
        {
            return await _reportManager.GSTR1PurchaseList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, filter, sSearchType, sGsttinNo,0, isRegister, nInvoiceDt);
        }
        [HttpGet]
        [Route("[action]")]
        public IActionResult GSTR2PurchaseSummary(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }
        //Added by Bikas for GST Report 06/06/18
        [HttpGet]
        [Route("[action]")]
        public IActionResult SalesConsolidatedGST(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }
        //Added by Bikas for GST Return Report 07/06/18
        [HttpGet]
        [Route("[action]")]
        public IActionResult SalesConsolidatedGSTReturn(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }

        //Added by Bikas for GST Report 20/06/18
        [HttpGet]
        [Route("[action]")]
        public IActionResult PurchaseConsolidatedGST(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }

        //Added by Bikas for GST Report 21/06/18
        [HttpGet]
        [Route("[action]")]
        public IActionResult PurchaseReturnConsolidatedGST(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }



        [HttpPost]
        [Route("[action]")]
       
        public async Task<List<GSTReport>> GSTR2PurchaseSummaryData([FromBody]DateRange data, string type, int filter, string sInstanceId, string sSearchType, string sGsttinNo, int isRegister, int nInvoiceDt)
        {
            return await _reportManager.GSTR1PurchaseList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, filter, sSearchType, sGsttinNo,1, isRegister, nInvoiceDt);
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GSTR2PurchaseReturn(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }
        [HttpPost]
        [Route("[action]")]

        public async Task<List<GSTReport>> GSTR2PurchaseReturnData([FromBody]DateRange data, string type, int filter, string sInstanceId, string sSearchType, string sGsttinNo, int isRegister, int nInvoiceDt)
        {
            return await _reportManager.GSTR2PurchaseReturn(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, filter, sSearchType, sGsttinNo, isRegister, nInvoiceDt);
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GSTR1Sales(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }
        [HttpPost]
        [Route("[action]")]

        public async Task<List<GSTReport>> GSTR1SalesListData([FromBody]DateRange data, string type, int filter, string sInstanceId, string sSearchType, string sGsttinNo)
        {
            return await _reportManager.GSTR1SalesList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, filter, sSearchType, sGsttinNo, 0); // Added by Sarubala on 04-09-17
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GSTR1SalesReturn(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }
        [HttpPost]
        [Route("[action]")]

        public async Task<List<GSTReport>> GSTR1SalesReturnData([FromBody]DateRange data, string type, int filter, string sInstanceId, string sSearchType, string sGsttinNo)
        {
            return await _reportManager.GSTR1SalesReturn(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, filter, sSearchType, sGsttinNo,1);
        }

        //end

        //Added by Sarubala on 01-09-17 - start       
        [Route("[action]")]
        public IActionResult GSTR1B2CS()
        {
           
            return View();
        }
        [HttpPost]
        [Route("[action]")]

        public async Task<List<GSTReport>> GSTR1B2CSListData([FromBody]DateRange data, string type, int filter, string sInstanceId, string sSearchType, string sGsttinNo)
        {            
            return await _reportManager.GSTR1SalesList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, filter, sSearchType, sGsttinNo, 1);
        }
        //Added by Sarubala on 01-09-17 - end

        [HttpPost]
        [Route("[action]")]
        public async Task<List<GSTIn>> getGstinData ()
        {
            return await _reportManager.GetGstinData(User.AccountId() );
        }

        //Added by Bikas for GST Report 06/06/18
        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesCoordinateGst>> SalesConsolidatedGSTListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalSalesConsolidatedGSTListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        //Added by Bikas for GST Report 07/06/18
        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesCoordinateGstReturn>> SalesConsolidatedGSTReturnListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalSalesConsolidatedGSTReturnListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        //Added by Bikas for GST Report 20/06/18
        [HttpPost]
        [Route("[action]")]
        public async Task<List<PurchaseConsolidatedGst>> PurchaseConsolidatedGSTListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalPurchaseConsolidatedGSTListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        //Added by Bikas for GST Report 21/06/18
        [HttpPost]
        [Route("[action]")]
        public async Task<List<PurchaseReturnConsolidatedGst>> PurchaseReturnConsolidatedGSTListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalPurchaseReturnConsolidatedGSTListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        [Route("[action]")]

        public async Task<string[]> ValidateGSTR3B()
        {
            return await _reportManager.ValidateGSTR3B(User.AccountId());
        }


        [Route("[action]")]
        public IActionResult GSTR4TaxRateWiseTurnOver()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Sales>> GSTR4TaxRateWiseTurnOver([FromBody]DateRange data, string sInstanceId, string sSearchType, string sGsttinNo)
        {
            var fromDate = data.FromDate;
            var toDate = data.ToDate;
            var returnValue = await _reportManager.gstr4TaxRateWiseTurnOver(User.AccountId(), sInstanceId, fromDate, toDate, sGsttinNo);

            return returnValue;
        }

    }
}
