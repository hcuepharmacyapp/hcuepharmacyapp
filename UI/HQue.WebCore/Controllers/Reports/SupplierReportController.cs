﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Inventory;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Accounts;
using System.Collections.Generic;
using HQue.Biz.Core.Report;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class SupplierReportController : BaseController
    {
        private readonly ConfigHelper _configHelper;
        private readonly SupplierReportManager _supplierReportManager;
        public SupplierReportController(SupplierReportManager supplierReportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _supplierReportManager = supplierReportManager;
        }

        [Route("[action]")]
        public IActionResult SupplierWiseBalance()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult SupplierWisePurchaseReturn()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult SupplierLedger()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult ExpiryReturn()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult SupplierPayment()
        {
            return View();
        }

        [Route("[action]")]
        public async Task<List<Payment>> SupplierWiseBalanceList(string instanceId)
        {
            return await _supplierReportManager.SupplierWiseBalanceList(User.AccountId(), instanceId);
        }
        [Route("[action]")]
        public Task<List<Vendor>> GetSupplierName(string supplier, string mobile)
        {
            return _supplierReportManager.GetSupplierName(supplier, mobile, User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public Task<List<Vendor>> GetSupplierByInstance(string instanceId)
        {
            if (instanceId == "undefined")
                instanceId=User.InstanceId();

            return _supplierReportManager.GetSupplierByInstance(User.AccountId(), instanceId);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Payment>> SupplierDetailsList([FromBody]Payment data,string instanceId)
        {
            data.AccountId = User.AccountId();
            data.InstanceId = instanceId;
            return await _supplierReportManager.SupplierDetailsList(data);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<VendorPurchaseReturn>> SupplierWisePurchaseReturnDetail([FromBody]SupplierFilter data, bool IsInvoiceDate = false)
        {
            data.AccountId = User.AccountId();
            if (data.InstanceId == null || data.InstanceId == "")
                data.InstanceId = User.InstanceId();

            if (data.InstanceId == "undefined")
                data.InstanceId = null;

            if (!User.HasPermission(UserAccess.Admin))
                data.InstanceId = User.InstanceId();

            return await _supplierReportManager.SupplierWisePurchaseReturnDetail(data, IsInvoiceDate);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Payment>> SupplierPaymentDetails([FromBody]Payment data, string InstanceId)
        {
            data.AccountId = User.AccountId();
            data.InstanceId = InstanceId;
            return await _supplierReportManager.SupplierPaymentDetails(data);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<Payment>> SupplierLedgerDetails([FromBody]Payment data, string InstanceId)
        {
            data.AccountId = User.AccountId();
            data.InstanceId = InstanceId;
            return await _supplierReportManager.SupplierLedgerDetails(data);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<VendorPurchaseReturn>> SupplierWiseExpiryReturnDetail([FromBody]SupplierFilter data)
        {
            data.AccountId = User.AccountId();
            //data.InstanceId = User.InstanceId(); // InstanceId passed from js
            return await _supplierReportManager.SupplierWiseExpiryReturnDetail(data);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Payment>> supplierWisePaymentReceiptCancellationList([FromBody]Payment data,string InstanceId)
        {
            data.AccountId = User.AccountId();
            data.InstanceId = InstanceId;
            return await _supplierReportManager.supplierWisePaymentReceiptCancellationList(data);
        }

    }
}
