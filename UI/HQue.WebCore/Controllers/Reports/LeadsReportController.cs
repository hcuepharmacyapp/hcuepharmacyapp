﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Biz.Core.Report;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Leads;
using HQue.Contract.Infrastructure.Misc;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class LeadsReportController : BaseController
    {
        private readonly ReportManager _reportManager;

        public LeadsReportController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Leads>> ListData([FromBody]DateRange data, string type)
        {
            return await _reportManager.LeadsReportList(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        }

    }
}
