﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Communication;
using HQue.Biz.Core.Report;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class SmsReportController : BaseController
    {
        private readonly ReportManager _reportManager;

        public SmsReportController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
        }

        [Route("[action]")]
        public IActionResult SmsList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<CommunicationLog>> SmsListData()
        {
            return await _reportManager.SmsUserList(User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public IActionResult SmsBranchReport()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<CommunicationLog>> SmsBranchListData([FromBody]DateRange data, string type)
        {
            return await _reportManager.SmsBranchList(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        }
    }
}
