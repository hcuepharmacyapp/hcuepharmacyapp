﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using HQue.Biz.Core.Report;
using HQue.Biz.Core.Setup;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.UserAccount;
using Microsoft.AspNetCore.Authorization;
using System;
using Utilities.Report;
using HQue.Contract.Infrastructure.Master;
using System.Linq;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class SalesReportController : BaseController
    {
        private readonly ReportManager _reportManager;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly ConfigHelper _configHelper;

        public SalesReportController(ReportManager reportManager, InstanceSetupManager instanceSetupManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
            _instanceSetupManager = instanceSetupManager;
            _configHelper = configHelper;
        }


        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult DateWiseSalesReport()
        {
            var getGstSelect = GetInstanceData();
            ViewBag.currInst = getGstSelect.Result.Gstselect;

            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //Created by Manivannan on 19-Jul-2017
        public async Task<Tuple<IEnumerable<Sales>, IEnumerable<Sales>>> DateWiseSalesReportData([FromBody]Sales model, string type, string sInstanceId, int level)
        {
            var fromDate = (DateTime)model.InvoiceDate;
            var toDate = (DateTime)model.SalesCreatedAt;
            var returnValue = await _reportManager.SalesReportDetailMultiLevel(type, User.AccountId(), sInstanceId, level, fromDate, toDate, model);
            if (level == 3) {
                var salesList = returnValue.Where(x=>!x.IsReturn);
                var returnsList = returnValue.Where(x => x.IsReturn);
                return Tuple.Create(salesList, returnsList);
            }
            else
            {
                return Tuple.Create<IEnumerable<Sales>, IEnumerable<Sales>>(returnValue as IEnumerable<Sales>, null);
            }
        }

        [Route("[action]")]
        public IActionResult TaxWiseSalesReport()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //Created by Manivannan on 24-Jul-2017
        public async Task<Tuple<IEnumerable<Sales>, IEnumerable<Sales>>> BranchWiseSalesGroupReportData([FromBody]Sales model, string type, string sInstanceId, int level, double gstTotal)
        {
            var fromDate = (DateTime)model.InvoiceDate;
            var toDate = (DateTime)model.SalesCreatedAt;
            var returnValue = await _reportManager.SalesReportTaxGroupMultiLevel(type, User.AccountId(), sInstanceId, level, fromDate, toDate, model, gstTotal);
            if (level == 3)
            {
                var salesList = returnValue.Where(x => !x.IsReturn);
                var returnsList = returnValue.Where(x => x.IsReturn);
                return Tuple.Create(salesList, returnsList);
            }
            else
            {
                return Tuple.Create<IEnumerable<Sales>, IEnumerable<Sales>>(returnValue as IEnumerable<Sales>, null);
            }
        }

        [Route("[action]")]
        public IActionResult ItemWiseSalesReport()
        {
            var getGstSelect = GetInstanceData();
            ViewBag.currInst = getGstSelect.Result.Gstselect;

            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //Created by Sarubala on 26-Jul-2017
        public async Task<Tuple<List<Sales>, List<Sales>>> ItemWiseSalesReportData([FromBody]Sales model, string type, string sInstanceId, int level, string productId)
        {
            var fromDate = (DateTime)model.InvoiceDate;
            var toDate = (DateTime)model.SalesCreatedAt;
           return await _reportManager.ItemWiseSalesReportMultiLevel(type, User.AccountId(), sInstanceId, level, fromDate, toDate, model, productId);            
        }

        // Added by Bikas 14/06/2018
        [Route("[action]")]
        public IActionResult BranchWiseSalesReport()
        {
            var getGstSelect = GetInstanceData();
            ViewBag.currInst = getGstSelect.Result.Gstselect;

            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Tuple<IEnumerable<Sales>, IEnumerable<Sales>>> BranchWiseSalesReportData([FromBody]Sales model, string type, string sInstanceId, int level)
        {
            var fromDate = (DateTime)model.InvoiceDate;
            var toDate = (DateTime)model.SalesCreatedAt;
            var returnValue = await _reportManager.BranchWiseSalesReportDetailMultiLevel(type, User.AccountId(), sInstanceId, level, fromDate, toDate, model);
            if (level == 3)
            {
                var salesList = returnValue.Where(x => !x.IsReturn);
                var returnsList = returnValue.Where(x => x.IsReturn);
                return Tuple.Create(salesList, returnsList);
            }
            else
            {
                return Tuple.Create<IEnumerable<Sales>, IEnumerable<Sales>>(returnValue as IEnumerable<Sales>, null);
            }
        }




        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<SalesItem>> ListData([FromBody]DateRange data, string type, string sInstanceId, string kind)
        {
            //return await _reportManager.SalesReportList(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (kind == "undefined")
                kind = null;
            if (kind == "F")
            {
                kind = "F&B";
            }
            if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.SalesReportList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, kind);

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> CustomerDetailListData([FromBody]DateRange data, string Name,string Mobile, string Instanceid)
        {
            if (Instanceid == "undefined")
                Instanceid = null;
           if (!User.HasPermission(UserAccess.Admin))
                Instanceid = User.InstanceId();

            return await _reportManager.SalesReportCustomerDetailList(Name, Mobile, User.AccountId(),Instanceid, data.FromDate, data.ToDate);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> ProductwiseListData([FromBody]DateRange data, string Id,string Name,string Mobile)
        {
            return await _reportManager.SalesProductwiseListData(Id,Name,Mobile, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> ProductWiseDetailListInSales([FromBody]DateRange data, string Id, string Name, string Mobile, string Instanceid)  
        {
            if (Instanceid == "undefined")
                Instanceid = null;
           if (!User.HasPermission(UserAccess.Admin))
                Instanceid = User.InstanceId();

            return await _reportManager.SalesProductwiseListData(Id, Name, Mobile, User.AccountId(), Instanceid, data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult NonSaleCustomers() 
        {
            return View();
        }
        
        [HttpPost]
        [Route("[action]")]
        public async Task<List<Sales>> NonSaleCustomersList([FromBody]DateRange data, string Instanceid) 
        {
            if (Instanceid == "undefined")
                Instanceid = null;
           if (!User.HasPermission(UserAccess.Admin))
                Instanceid = User.InstanceId();

            return await _reportManager.NonSaleCustomersListData(User.AccountId(), Instanceid, data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult ReturnList()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult CustomerWise()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult ProductWise()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        //ReturnNo Filter added by Violet On 15-06-2017
        public async Task<List<SalesReturnItem>> ReturnListData([FromBody]DateRange data, string type, string sInstanceId, int nIssummary,int fromvalues, int tovalues, int SearchType,string kind)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (kind == "undefined")
                kind = null;
            if (kind == "F")
            {
                kind = "F&B";
            }
            if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.SalesReturnReportList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, nIssummary, fromvalues, tovalues, SearchType, kind);
        }
        [Route("[action]")]
        public IActionResult HomeDeliveryList()
        {
            return View();
        }
        //add by nandhini for door delivery report
        [Route("[action]")]
        public IActionResult DoorDeliveryList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
       
        public async Task<List<Sales>> doorDeliveryListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.doorDeliveryListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
            
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<Sales>> HomeDeliveryListData([FromBody]DateRange data, string type, string sInstanceId, string invoiceType, string invoiceSearchType, int fromInvoice, int toInvoice)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.HomeDeliveryList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, invoiceType, invoiceSearchType, fromInvoice, toInvoice);
            //return await _reportManager.HomeDeliveryList(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult DrugWiseList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> DrugWiseListData([FromBody]DateRange date, string type, string instanceid, string invoiceSeries, int fromInvoice, int toInvoice, string productVal)
        {
            if (!User.HasPermission(UserAccess.Admin)) // Added by Sarubala on 14-09-17
                instanceid = User.InstanceId();

            if (instanceid != "undefined" && instanceid != "null")
                return await _reportManager.DrugWiseReportList(type, User.AccountId(), instanceid, date.FromDate, date.ToDate, invoiceSeries, fromInvoice, toInvoice, productVal); //User.InstanceId()
            else if(instanceid == "undefined")
                return await _reportManager.DrugWiseReportList(type, User.AccountId(), null, date.FromDate, date.ToDate, invoiceSeries, fromInvoice, toInvoice, productVal); 
            else
                return await _reportManager.DrugWiseReportList(type, User.AccountId(), User.InstanceId(), date.FromDate, date.ToDate, invoiceSeries, fromInvoice, toInvoice, productVal);
        }

        [Route("[action]")]
        public IActionResult SalesList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<SalesItem>> SalesListData([FromBody]DateRange data, string type, string sInstanceId, string invoiceType, string invoiceSearchType, int fromInvoice, int toInvoice)
        {
            //return await _reportManager.SalesList(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate); 
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.SalesList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, invoiceType, invoiceSearchType, fromInvoice, toInvoice);
            
        }

        

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> SalesCustomerwiseListData([FromBody]DateRange data, string Name, string Mobile, string Instanceid)
        {
            if (Instanceid == "undefined")
                Instanceid = null;
           if (!User.HasPermission(UserAccess.Admin))
                Instanceid = User.InstanceId();

            return await _reportManager.SalesCustomerWiseList(Name, Mobile, User.AccountId(), Instanceid, data.FromDate, data.ToDate);// User.InstanceId()
        }

        [Route("[action]")]
        public IActionResult SalesSummaryList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<Sales>> SalesSummaryListData([FromBody]DateRange data, string type, string sInstanceId, string InvoiceSeries, int fromInvoice, int toInvoice, int paymentType)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.SalesSummaryList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, InvoiceSeries, fromInvoice, toInvoice, paymentType);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> salesSummaryCustomerwiselistData([FromBody]DateRange data, string Name,string Mobile, string Instanceid)
        {
            if (Instanceid == "undefined")
                Instanceid = null;
           if (!User.HasPermission(UserAccess.Admin))
                Instanceid = User.InstanceId();

            return await _reportManager.SalesSummaryCustomerwiseList(Name,Mobile, User.AccountId(), Instanceid, data.FromDate, data.ToDate); //User.InstanceId()
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesReturnItem>> SalesSummaryReturnListData([FromBody]DateRange data, string type)
        {
            return await _reportManager.SalesSummaryReturnList(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult BillCancelList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesReturnItem>> BillCancelListData([FromBody]SalesReturnItem sri, string type)
        {
            if (sri.InstanceId == "undefined")
                sri.InstanceId = null;

            if (!User.HasPermission(UserAccess.Admin))
                sri.InstanceId = User.InstanceId();

            return await _reportManager.BillCancelListData(User.AccountId(),sri.InstanceId,sri, type);
        }

        [Route("[action]")]
        public IActionResult SalesUserList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> SalesUserListData([FromBody]SalesItem data, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.SalesUserList(data, User.AccountId(), sInstanceId);
        }

        [HttpPost]
        [Route("[action]")]
        public string ExportData([FromBody]dynamic data)
        {
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            StringBuilder output = new StringBuilder();
            foreach (var cols in data)
            {
                foreach (var col in cols)
                {
                    dynamic val = JObject.Parse(JsonConvert.SerializeObject(col));
                    output.Append(val.value);
                    output.Append(",");
                }
                output.AppendLine();
            }


            string fileName = string.Format("./temp/sales_summary_{0}.csv", User.InstanceId());

            FileInfo info = new FileInfo(fileName);
            if (info.Exists)
            {
                info.Delete();
            }

            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(output.ToString(), false, Encoding.UTF8);

            }

            HttpContext.Response.ContentType = "application/json";
            return fileName;

        }

        [Route("[action]")]
        public async Task<Instance> GetInstanceData()
        {
            if (User == null)
                return new Instance();            
            return await _instanceSetupManager.GetById(User.InstanceId()); ;
        }

        [Route("[action]")]
        public string GetUserData()
        {
            string userName = string.Empty;
            userName=User.Name();
            return userName;
        }
        [HttpPost]

        [Route("[action]")]
        // added invoice series filter by Violet on 19/06/2017
        public async Task<List<SalesReturn>> GetReturnsByDate([FromBody]DateRange data, string type, string sInstanceId, string invoiceType, string invoiceSearchType,int fromInvoice, int toInvoice)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.GetReturnsByDate(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, invoiceType, invoiceSearchType, fromInvoice, toInvoice);
        }

        [HttpPost]

        [Route("[action]")]
        public async Task<List<SalesReturn>> GetSalesDetailReturnsByDate([FromBody]DateRange data, string type)
        {
            return await _reportManager.GetSalesDetailReturnsByDate(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        }

        [Route("[action]")]
        public IActionResult SalesMargin()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> SalesMarginProductWiseList([FromBody]DateRange data, string filter, string instanceId)
        {
            if (instanceId == "undefined")
                instanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                instanceId = User.InstanceId();

            return await _reportManager.SalesMarginProductWiseList(filter,User.AccountId(), instanceId, data.FromDate, data.ToDate);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<InvoiceSeriesItem>> getInvoiceSeriesItems(int filter, string branchid = null)
        {
            bool flag;
            InvoiceSeriesItem IS = new InvoiceSeriesItem();
            IS.SetLoggedUserDetails(User);
            IS.InvoiceseriesType = filter;

            if (branchid == "undefined")
                flag = true;
            else
                flag = false;
            return await _reportManager.GetInvoiceSeriesItems(IS, flag);

        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesReturn>> GetReturnsByDateSalesList([FromBody]DateRange data, string type, string sInstanceId, string invoiceType, string invoiceSearchType)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.GetReturnsByDateSalesList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, invoiceType, invoiceSearchType);
        }

        [Route("[action]")]
        public Task<List<HQueUser>> GetUserName(string userName)
        {

            return _reportManager.GetUserName(userName, User.AccountId(), User.InstanceId());

        }

        [Route("[action]")]
        public Task<List<HQueUser>> GetUserId(string userId)
        {

            return _reportManager.GetUserId(userId, User.AccountId(), User.InstanceId());

        }

        //Added by Sarubala for Doctor wise Report on 28/08/17 - start
        [Route("[action]")]
        public IActionResult DoctorwiseReport()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]        
        public async Task<List<Doctor>> DoctorwiseReportList([FromBody]DateRange data, string type, string sInstanceId, string filterType, string doctorId, string productId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.DoctorwiseReportList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, filterType, doctorId, productId);
        }
        //Added by Sarubala for Doctor wise Report on 28/08/17 - end

        //add by nandhini for consolidated sales return report 9/6/17
        [Route("[action]")]
        public IActionResult ConsolidatedSalesReturnReport()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesReturn>> consolidatedsalesReturnReportList([FromBody]DateRange data, string type, string sInstanceId, string invoiceType, string cashType)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.consolidatedsalesReturnReportList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, invoiceType, cashType);
        }
       


        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> consolidatedsalesreturnReportPrint(string sInstanceId,string from, string to, string printType, string invoiceSeries, string cashType)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            if (printType == null)
            {
                printType = "1";
            }
            List<FileStreamResult> listResult = new List<FileStreamResult>();
            List<SalesReturn> List1 = new List<SalesReturn>();
            DateTime? frmDt;
            DateTime? toDt;
            if (from != "" && from != "undefined" && from != null && to != "" && to != "undefined" && to != null)
            {
                frmDt = DateTime.Parse(from);
                toDt = DateTime.Parse(to);
            }
            else
            {
                frmDt = null;
                toDt = null;
            }
            List1 = await _reportManager.consolidatedsalesReturnReportList("", User.AccountId(), sInstanceId, (DateTime)frmDt, (DateTime)toDt, invoiceSeries, cashType);

            if (sInstanceId != null)
            {
                List1[0].Instance = await _instanceSetupManager.GetById(sInstanceId);
            }
            else {
                List1[0].Instance = new Instance();
                    }




            string name = "";
            name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Schedule");
            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
               
                writer.Write(GenerateConsolidatedsalesreturnReport_A4(List1), false, Encoding.UTF8);
               
            }
            HttpContext.Response.ContentType = "application/notepad";
            
            return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Sales" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            
        }
        private string GenerateConsolidatedsalesreturnReport_A4(List<SalesReturn> List1)
        {
            PlainTextReport rpt = new PlainTextReport();
            rpt.Columns = 95;
            rpt.Rows = 38;
            WriteConsolidatedsalesreturnReportHeader_A4(rpt,  List1);
            WriteConsolidatedsalesreturnReportItems_A4(rpt, List1);
           
            return rpt.Content.ToString();
        }
        private void WriteConsolidatedsalesreturnReportHeader_A4(PlainTextReport rpt, List<SalesReturn> List1)
        {

            if (List1[0].Instance.Name != null)
            {
                rpt.NewLine();
                rpt.Write(List1[0].Instance.Name, 95, 0, Alignment.Center);
                rpt.NewLine();
                rpt.Write(List1[0].Instance.FullAddress, 95, 0, Alignment.Center);
                rpt.NewLine();
                rpt.Write("DL No   : " + List1[0].Instance.DrugLicenseNo + " / " + "GSTIN :" + List1[0].Instance.GsTinNo, 95, 0, Alignment.Center);
            }
            else
            {
                rpt.NewLine();
                rpt.Write(User.Name() + " - " + "All Branch", 95, 0, Alignment.Center);
            }
            rpt.NewLine();
            rpt.Write();
            rpt.Write("Consolidated Sales Return Report", 95, 0, Alignment.Center);

            rpt.Write();
            rpt.NewLine();
            rpt.Write();
            rpt.Drawline();
            rpt.NewLine();
            rpt.Write("S.No", 4, 0, Alignment.Right);         
            rpt.Write("R.Date", 9, 5);
            rpt.Write("R.No", 10, 15, Alignment.Right);
            rpt.Write("Customer", 27, 26);   
            rpt.Write("Gross", 9, 55, Alignment.Right);
            rpt.Write("GSTValue", 9, 65, Alignment.Right);
            rpt.Write("R.Off", 5, 75);
            rpt.Write("F.Amt", 10, 82, Alignment.Right);
            rpt.Write("Branch", 11, 42);
            rpt.Write(); 
            rpt.Drawline();
        }


        private void WriteConsolidatedsalesreturnReportItems_A4(PlainTextReport rpt, List<SalesReturn> List1)
        {
            int len = 0;
            decimal? total = 0;
            foreach (var item in List1)
            {
                rpt.NewLine();
                len++;
                rpt.Write(len.ToString(), 4, 0, Alignment.Right);              
                rpt.Write(string.Format("{0:dd/MM/yy}", item.ReturnDate), 9, 5);
                rpt.Write(item.ReturnNo.Replace(" ", ""), 10, 15, Alignment.Right);
                rpt.Write(item.Sales.Name, 27, 26, Alignment.Left);              
                rpt.Write(string.Format("{0:0.00}", item.Sales.vatOther_Gross), 9, 55, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", item.Sales.vatOther), 9, 65, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", item.Sales.ConsolidatedRoundOff), 5, 75, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", item.Sales.FinalValue), 10, 82, Alignment.Right);
                rpt.Write(item.Sales.Instance.Name, 11,42);
                rpt.Write();
                total += item.Sales.FinalValue;

            }
            for (int i = 0; i < 5 - len; i++)
            {
                rpt.NewLine();
            }
            rpt.Write();
            rpt.Drawline(1);
            rpt.Write("Total: " + string.Format("{0:0.00}", total), 92, 0, Alignment.Right);
            rpt.Write();

        }
         //end 

        [Route("[action]")]
        public IActionResult ConsolidatedSalesReport()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Sales>> consolidatedsalesReportList([FromBody]DateRange data, string type, string sInstanceId, string invoiceType, string cashType)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.consolidatedsalesReportList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, invoiceType, cashType);
        }
        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> consolidatedsalesReportPrint(string sInstanceId, string from, string to, string printType,string invoiceSeries, string cashType)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            if (printType == null)
            {
                printType = "1";
            }
            List<FileStreamResult> listResult = new List<FileStreamResult>();
            List<Sales> List = new List<Sales>();
            DateTime? frmDt;
            DateTime? toDt;
            if (from != "" && from != "undefined" && from != null && to != "" && to != "undefined" && to != null)
            {
                frmDt = DateTime.Parse(from);
                toDt = DateTime.Parse(to);
            }
            else
            {
                frmDt = null;
                toDt = null;
            }
            List = await _reportManager.consolidatedsalesReportList("", User.AccountId(), sInstanceId, (DateTime)frmDt, (DateTime)toDt, invoiceSeries, cashType);

            



            string name = "";
            name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Schedule");
            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                if (List.Count > 0)
                {
                    if (sInstanceId!=null) {
                    List[0].Instance = await _instanceSetupManager.GetById(sInstanceId);
                    }
                    else
                    {
                        List[0].Instance = new Instance();
                       
                    }
                    writer.Write(GenerateConsolidatedsalesReport_A4(List), false, Encoding.UTF8);
                }
                else
                {
                    writer.Write("", false, Encoding.UTF8);
                }
                
            }
            HttpContext.Response.ContentType = "application/notepad";
           
            return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Sales" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            
        }


        private string GenerateConsolidatedsalesReport_A4(List<Sales> List)
        {
            PlainTextReport rpt = new PlainTextReport();
            rpt.Columns = 95;
            rpt.Rows = 38;
            WriteConsolidatedsalesReportHeader_A4(rpt, List);
            WriteConsolidatedsalesReportItems_A4(rpt, List);
            
            return rpt.Content.ToString();
        }
        private void WriteConsolidatedsalesReportHeader_A4(PlainTextReport rpt, List<Sales> List)
        {
            if (List[0].Instance.Name != null)
            {
                rpt.NewLine();
                rpt.Write(List[0].Instance.Name, 95, 0, Alignment.Center);
                rpt.NewLine();
                rpt.Write(List[0].Instance.FullAddress, 95, 0, Alignment.Center);
                rpt.NewLine();
                //rpt.Write("DL No   : " + List[0].Instance.DrugLicenseNo + " / " + "TIN :" + List[0].Instance.TinNo, 95, 0, Alignment.Center);
                rpt.Write("DL No   : " + List[0].Instance.DrugLicenseNo + " / " + "GSTIN :" + List[0].Instance.GsTinNo, 95, 0, Alignment.Center);
            }
            else
            {                
                rpt.NewLine();
                rpt.Write(User.Name() + " - " + "All Branch", 95, 0, Alignment.Center);
            }
            rpt.NewLine();
            rpt.Write();
            rpt.Write("Consolidated Sales Report", 95, 0, Alignment.Center);

            rpt.Write();
            rpt.NewLine();
            rpt.Write();
            rpt.Drawline();
            rpt.NewLine();
            rpt.Write("S.no", 4, 0, Alignment.Right);          
            rpt.Write("B.Date", 9, 5);
            rpt.Write("B.No", 10, 15, Alignment.Right);
            rpt.Write("Customer", 27, 26);          
            //rpt.Write("5.2%Gross", 9, 40, Alignment.Center);
            //rpt.Write("5.2%Vat", 7, 50);                    
            rpt.Write("Gross", 9, 55, Alignment.Right);
            rpt.Write("GSTValue", 9, 65, Alignment.Right);
            rpt.Write("R.Off", 5, 75);
            rpt.Write("F.Amt",10, 82, Alignment.Right);
            rpt.Write("Branch", 11,42);
            rpt.Write();
            rpt.Drawline();
        }
        
        private void WriteConsolidatedsalesReportItems_A4(PlainTextReport rpt, List<Sales> List)
        {      
            int len = 0;
            decimal? total = 0;
            foreach (var item in List)
            {
                rpt.NewLine();
                len++;
                rpt.Write(len.ToString(), 4, 0, Alignment.Right);              
                rpt.Write(string.Format("{0:dd/MM/yy}", item.InvoiceDate), 9,5);
                rpt.Write(item.InvoiceNo.Replace(" ", ""), 10, 15, Alignment.Right);
                rpt.Write(item.Name, 27, 26, Alignment.Left);              
                /*Other VAt and Gross Added by Poongodi on 30/05/2017*/
                rpt.Write(string.Format("{0:0.00}", item.vatOther_Gross), 9, 55, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", item.vatOther), 9, 65, Alignment.Right);               
                rpt.Write(string.Format("{0:0.00}", item.ConsolidatedRoundOff), 5, 75, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", item.FinalValue), 10, 82, Alignment.Right);
                rpt.Write(item.Instance.Name,11,42);
                rpt.Write();
                total += item.FinalValue;

            }
            for (int i = 0; i < 5 - len; i++)
            {
                rpt.NewLine();
            }         
            rpt.Write();
            rpt.Drawline(1);
            rpt.Write("Total: "+string.Format("{0:0.00}", total), 92, 0, Alignment.Right);
            rpt.Write();

        }














    }
}
