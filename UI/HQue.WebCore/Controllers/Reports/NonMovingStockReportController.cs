﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using HQue.Biz.Core.Report;
using HQue.Biz.Core.Setup;
using HQue.Contract.Infrastructure.Setup;

using HQue.Contract.Infrastructure.Master;
using HQue.Biz.Core.Master;
using HQue.Contract.Infrastructure.Misc;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class NonMovingStockReportController : BaseController
    {
        private readonly ReportManager _reportManager;
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly VendorManager _vendorManager;


        public NonMovingStockReportController(ReportManager reportManager, InstanceSetupManager instanceSetupManager, VendorManager vendorManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
            _instanceSetupManager = instanceSetupManager;
            _vendorManager = vendorManager;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> SelectInstances()
        {
            return await _instanceSetupManager.List(User.AccountId());
        }

        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi on 09/02/2017 for delhi requirement
        public async Task<List<ProductStock>> StockProductList(string sInstanceId, [FromBody]DateRange data)
        {
            return await _reportManager.NonMovingStockReportList(sInstanceId, data.FromDate, data.ToDate); 
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> UpdateIsMovingStock([FromBody]List<ProductStock> productStocks)
        //public async Task<ProductStock[]> UpdateIsMovingStock([FromBody]ProductStock[] productStocks)
        {
            //List<ProductStock> productStocks = new List<ProductStock>();
            //productStocks.Add(productStock[0]);

            await _reportManager.UpdateIsMovingStock(productStocks);
            return productStocks;
        }

        //Newly Added Gavaskar 25-10-2016 Start
        [Route("[action]")]
        public async Task<IEnumerable<Vendor>> VendorList(Vendor vendor)
        {
            vendor.SetLoggedUserDetails(User);
            return await _vendorManager.VendorList(vendor);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ProductStock[]> vendorUpdate([FromBody]List<ProductStock> productStocks)
        {
            return await _reportManager.vendorNonMovingUpdate(productStocks);
        }

        //Newly Added Gavaskar 25-10-2016 End

    }
}
