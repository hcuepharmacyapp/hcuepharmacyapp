﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Report;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Reports;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class TallyReportController : BaseController
    {
        private readonly ReportManager _reportManager;

        public TallyReportController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult SalesReportTally(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }

        //Added by Bikas for Tally Report 22/06/18
        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesReportTally>> SalesReportTallyListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalSalesReportTallyListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }
      
        //Added by Bikas for Tally Report 25/06/18
        [HttpGet]
        [Route("[action]")]
        public IActionResult SalesReturnReportTally(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }


        
        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesreturnReportTally>> SalesReturnReportTallyListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalSalesReturnReportTallyListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }
        
        //Added by Bikas for Tally Report 26/06/18

        [HttpGet]
        [Route("[action]")]
        public IActionResult PurchaseReportTally(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<PurchaseReportTally>> PurchaseReportTallyListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalPurchaseReportTallyListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        //Added by Bikas for Tally Report 27/06/18

        [HttpGet]
        [Route("[action]")]
        public IActionResult PurchaseReturnReportTally(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<PurchaseReturnReportTally>> PurchaseReturnReportTallyListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalPurchaseReturnReportTallyListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult JournalReportTally(int filter)
        {
            ViewBag.filter = filter;
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<JournalReport>> JournalListData([FromBody]DateRange data, string type, string filterType, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
            if (!User.IsAdmin())
                sInstanceId = User.InstanceId();

            return await _reportManager.LocalJournalListData(filterType, type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }

    }
}
