﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using HQue.Biz.Core.Report;
using HQue.Contract.Infrastructure.Audits;
using HQue.Contract.Infrastructure.Reports;
using System;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class BuyReportController : BaseController
    {
        private readonly ReportManager _reportManager;
        public BuyReportController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
        }
        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult DcList()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        //Productid and vendorid parameter added for Purchase report
        public async Task<List<VendorPurchaseItem>> ListData([FromBody]DateRange data, string type, string sInstanceId, string sProductId, string sVendorId, bool IsInvoiceDate)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;

           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.BuyReportList(type, User.AccountId(), sInstanceId, sProductId, sVendorId, data.FromDate, data.ToDate, IsInvoiceDate);
        }
        [HttpPost]
        [Route("[action]")]        
        public async Task<List<DcVendorPurchaseItem>> DcListData([FromBody]DateRange data, string type, string sInstanceId, string sProductId, string sVendorId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.DcListData(type, User.AccountId(), sInstanceId, sProductId, sVendorId, data.FromDate, data.ToDate);
        }
        #region Purchase Margin Report Created by POongodi on 20/02/2017
        [Route("[action]")]
        public IActionResult PurchaseMargin()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<VendorPurchaseItem>> PurchaseMarginData([FromBody]DateRange data, string type, string sInstanceId, bool IsInvoiceDate)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.BuyMarginList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, IsInvoiceDate);
        }
        #endregion
        [HttpPost]
        [Route("[action]")]
        public async Task<List<VendorPurchaseItem>> ProductWiselistData([FromBody]DateRange data, string Id)
        {
            return await _reportManager.BuyReporProductwisetList(Id, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<VendorPurchaseItem>> VendorWiselistData([FromBody]DateRange data, string Id)
        {
            return await _reportManager.BuyReportVendorwiseList(Id, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        }
        [Route("[action]")]
        public IActionResult ReturnList()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<VendorPurchaseReturnItem>> ReturnListData([FromBody]DateRange data, string type, string sInstanceId, bool IsInvoiceDate)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.BuyReturnReportList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, IsInvoiceDate);
        }
        [Route("[action]")]
        public IActionResult BuySummaryList()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult BuySalesList()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult BuyReturnSummary()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult PurchaseReturnStatus()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult Purchasewise()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<VendorPurchaseItem>> PurchaseSummaryListData([FromBody]DateRange data, string type,string sInstanceId, bool IsInvoiceDate)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.BuySummaryList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, IsInvoiceDate);
        }
        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<ProductPurchaseSalesVM>> purchaseSalesListData([FromBody]DateRange data, string type, string sInstanceId)
        {
            return await _reportManager.BuySalesList(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate);
        }
        [HttpPost]
        [Route("[action]")]
        //Added By San - 22-02-2017
        public async Task<List<VendorPurchaseReturn>> ReturnSummaryListData([FromBody]DateRange data, string type,string sInstanceId, bool IsInvoiceDate)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.ReturnSummaryListData(type, User.AccountId(), sInstanceId, data.FromDate, data.ToDate, IsInvoiceDate);
        }
        //View in Vendor
        [HttpGet]
        [Route("[action]")]
        public IActionResult VendorListView(string id)
        {
            var Vendorid = id;
            ViewBag.vendorId = Vendorid;
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<VendorPurchaseItem>> VendorListViewData(string id)
        {
            return await _reportManager.VendorView(User.AccountId(), User.InstanceId(),id);
        }
        [Route("[action]")]
        public IActionResult PurchaseOrder()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<VendorOrderItem>> PurchaseOrderData([FromBody]VendorOrder order1, string type,string instanceid)
        {
            order1.SetLoggedUserDetails(User);
            //added by nandhini19.5.17
            if (string.IsNullOrEmpty(instanceid))
            {
                instanceid = "";
            }
            order1.InstanceId = instanceid;

            if (order1.InstanceId == "undefined")
                order1.InstanceId = null;

            if (!User.HasPermission(UserAccess.Admin))
                order1.InstanceId = User.InstanceId();

            return await _reportManager.PurchaseOrderData(type, order1);           
        }
        [Route("[action]")]
        public IActionResult PurchaseModified()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<VendorPurchaseItemAudit>> PurchaseModifiedList([FromBody]VendorPurchaseItemAudit data, string sInstanceId)
        {
            if (sInstanceId == "undefined")
                sInstanceId = null;
           if (!User.HasPermission(UserAccess.Admin))
                sInstanceId = User.InstanceId();

            return await _reportManager.PurchaseModifiedList(data, User.AccountId(), sInstanceId);
          //  return null;
        }
        
        [HttpPost]
        [Route("[action]")]
        //Created by Sarubala on 21-Jul-2017
        public async Task<Tuple<List<VendorPurchase>, List<VendorPurchase>>> BranchWisePurchaseReportData([FromBody]VendorPurchase model, string type, string sInstanceId, int level)
        {
            var fromDate = (DateTime)model.CreatedAt; // Modified by Sarubala on 11-08-17
            var toDate = (DateTime)model.SearchDateEnd;                        
            return await _reportManager.PurchaseReportDetailMultiLevel(type, User.AccountId(), sInstanceId, level, fromDate, toDate, model);
        }

        [Route("[action]")]
        public IActionResult BranchWisePurchaseReport()
        {
            return View();
        }

    }
}
