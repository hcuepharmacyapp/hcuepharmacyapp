﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Inventory;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Communication;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class PatientHistoryController : BaseController
    {
        private readonly SalesManager _salesManager;
        private readonly ConfigHelper _configHelper;
        public PatientHistoryController(SalesManager salesManager, ConfigHelper configHelper) : base(configHelper)
        {
            _salesManager = salesManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult PatientList()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult PatientListBulkSms()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/PatientHistory/PatientList");
            }
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Sales>> ListData([FromBody]Sales model)
        {
            model.SetLoggedUserDetails(User);
            return await _salesManager.PatientList(model);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Sales>> ListDataBulkSms([FromBody]Sales model)
        {
            model.SetLoggedUserDetails(User);
            return await _salesManager.PatientListBulkSms(model);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task sendSms([FromBody]CustomerSms customerSms)
        {
            Sales data = new Sales();
            data.SetLoggedUserDetails(User);
            
            data.Instance.Name = User.InstanceName();
            data.Instance.Phone = User.InstancePhone();
            data.Instance.Mobile = User.InstanceMobile();            
            await _salesManager.SendSms(customerSms,data);
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<int> CheckOBPaymentPaid(string customerid)
        {
            
           var rtnVal = await _salesManager.CheckOBPaymentPaid(User.AccountId(), User.InstanceId(), customerid);

            return rtnVal;
        }

        [Route("[action]")]
        public IActionResult PatientInformation(string id, string name, string mobile)
        {
            var patientMobile = id;
            // ViewBag.Mobile = patientMobile;
            ViewBag.Mobile = mobile;
            ViewBag.Name = name;
            ViewBag.PatientId = id;
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Sales> CustomerInformation([FromBody]Sales data)
        {
            return await _salesManager.CustomerInformation(data);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Sales>> PatientSaleList([FromBody]Sales data)
        {
            data.SetLoggedUserDetails(User);
            return await _salesManager.PatientSalesPager(data);
        }

        //Added by Sarubala on 20-11-17
        [Route("[action]")]
        public async Task<bool> GetCustomerBulkSmsSetting()
        {
            return await _salesManager.GetCustomerBulkSmsSetting(User.InstanceId());
        }
    }
}
