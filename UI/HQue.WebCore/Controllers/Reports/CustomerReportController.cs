﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Inventory;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Accounts;
using System.Collections.Generic;
using HQue.Biz.Core.Report;
using HQue.Contract.Infrastructure.Misc;
using System.IO;
using System;
using System.Text;
using Utilities.Report;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class CustomerReportController : BaseController
    {
        private readonly ConfigHelper _configHelper;
        private readonly CustomerReportManager _customerReportManager;
        public CustomerReportController(CustomerReportManager customerReportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _customerReportManager = customerReportManager;
        }

        [Route("[action]")]
        public IActionResult CustomerWiseBalance()
        {
            return View();
        }
        [Route("[action]")]
        public IActionResult CustomerLedger()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult TillReport()
        {
            return View();
        }

        [Route("[action]")]
        public async Task<List<CustomerPayment>> CustomerWiseBalanceList(string InstanceId)
        {
            return await _customerReportManager.CustomerWiseBalanceList(User.AccountId(), InstanceId);
        }
        
        [HttpPost]
        [Route("[action]")]
        public async Task<List<CustomerPayment>> customerDetailsList([FromBody]CustomerPayment data,string instanceid)
        {
            data.AccountId = User.AccountId();
            data.InstanceId = instanceid;
            return await _customerReportManager.customerDetailsList(data);
        }

        [Route("[action]")]
        public Task<List<CustomerPayment>> GetCustomerName(string instanceid,string customer, string mobile)
        {
            return _customerReportManager.GetCustomerList(customer, mobile, User.AccountId(), instanceid);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<PettyCashHdr>> GetPettyCashDates(System.DateTime selectedDate)
        {
            var userId = User.Identity.Id();
                       
            return await _customerReportManager.GetPettycashDates(User.AccountId(), User.InstanceId(),userId, selectedDate);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<dynamic>> GetPettycashDetails(string pettyId)
        {
          
            return await _customerReportManager.GetPettycashDetails(pettyId);
        }

        // Added Gavaskar 07-03-2017

        [Route("[action]")]
        public IActionResult PaymentReceiptCancellation()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<CustomerPayment>> customerWisePaymentReceiptCancellationList([FromBody]CustomerPayment data, string InstanceId)
        {
            data.AccountId = User.AccountId();
            data.InstanceId = InstanceId;
            return await _customerReportManager.customerWisePaymentReceiptCancellationList(data);
        }

        [Route("[action]")]
        public IActionResult CustomerPayment()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<CustomerPayment>> customerPaymentDetails([FromBody]CustomerPayment data, string instanceId)
        {
            data.AccountId = User.AccountId();
            data.InstanceId = instanceId;
            return await _customerReportManager.customerPaymentDetails(data);
        }

       
        [Route("[action]")]
        public async Task<ActionResult> ListDataForPrint(string pettyId,DateTime tillDateTime, string userName)
        {            
            List<dynamic> List = new List<dynamic>();                     
            List = await _customerReportManager.GetPettycashDetails(pettyId);

            string name = "";
            
            name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Schedule");

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(GenerateTillReport_A4(List, tillDateTime,userName), false, Encoding.UTF8);
                
            }

            HttpContext.Response.ContentType = "application/notepad";
            return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Till" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
           

        }


        private string GenerateTillReport_A4(List<dynamic> List, DateTime tillDateTime, string uName)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 95;
            rpt.Rows = 38;

            WriteReportHeader_A4(rpt, List, tillDateTime, uName);
            WriteReportItems_A4(rpt, List);

            return rpt.Content.ToString();
        }

        private void WriteReportHeader_A4(PlainTextReport rpt, List<dynamic> List, DateTime tillDateTime, string uName)
        {

            rpt.NewLine();
            rpt.Write(List[0].insname, 95, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write(List[0].address,95, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write("DL No   : " + List[0].drugLicenseno + " / " + "GSTIN :" + List[0].gstinno, 95, 0, Alignment.Center);

            //rpt.Drawline();

            rpt.Write();
            rpt.NewLine();
            rpt.NewLine();

            rpt.Write("TILL SAVED DATE & TIME: "+ tillDateTime, 95, 0);
            rpt.NewLine();
            rpt.Write("User Name: " + uName, 95, 0);
            rpt.NewLine();
            rpt.Drawline();
            rpt.Write("S.No", 4, 0, Alignment.Right);           

            rpt.Write("Details", 25, 5);
            rpt.Write("Inward", 13, 31, Alignment.Right);
            rpt.Write("OutWard", 13, 45, Alignment.Right);
            rpt.Write("Denamination", 15, 60);
            rpt.Write("Amount", 13, 76, Alignment.Right);            

            rpt.Write();
            rpt.Drawline();
        }



        private void WriteReportItems_A4(PlainTextReport rpt, List<dynamic> List)
        {
            int len = 0;
            decimal totalAmount = 0;
            decimal cashInHand = 0;
            decimal difftotal = 0;
            string status = string.Empty;
            for (var x = 0; x < List.Count; x++)
            {
               rpt.NewLine();

                if(List[x].SlNo != null)
                    rpt.Write(string.Format("{0}", List[x].SlNo), 4, 0, Alignment.Right);

                rpt.Write(List[x].Details, 25, 5);
                rpt.Write(string.Format("{0:0.00}", List[x].Inward != null ? List[x].Inward : 0), 13, 31, Alignment.Right);
                rpt.Write(string.Format("{0:0.00}", List[x].Outward != null ? List[x].Outward : 0), 13, 45, Alignment.Right);
                rpt.Write(List[x].Denamination != null ? List[x].Denamination : "", 15, 60);
                rpt.Write(string.Format("{0:0.00}", List[x].Amount != null ? List[x].Amount :0 ), 13, 76, Alignment.Right);
                totalAmount += List[x].Amount;
                cashInHand = List[0].Total;
                difftotal = List[0].difftotal;
                status = List[0].Status;
                rpt.Write();
                    len++;
            }
           
            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();


            rpt.Write("Total: " + totalAmount, 25, 65, Alignment.Right);
            rpt.NewLine();
            rpt.Write("Till Total: " + cashInHand, 30, 0, Alignment.Right);
            rpt.Write("Difference Amount: " + difftotal, 30, 35, Alignment.Right);
            rpt.Write("Status: " + status, 25, 67, Alignment.Right);
            rpt.Write();
        }




    }
}
