﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure;
using Utilities.Helpers;
using HQue.Biz.Core.Report;
using HQue.Contract.Infrastructure.Inventory;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class DistributorReportController : BaseController
    {
        private readonly ReportManager _reportManager;

        public DistributorReportController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
        }

        [Route("[action]")]
        public IActionResult DistributorBranchList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<Instance>> DistributorBranchListData([FromBody]Instance model)
        { 
            model.SetLoggedUserDetails(User);
            return await _reportManager.DistributorBranchList(new object[] { User.UserId() });
        }

        [Route("[action]")]
        public IActionResult DistributorLowStockList(string id = null)
        {
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.id = id;
            }
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> DistributorLowStockListData(string id)
        {
            return await _reportManager.DistributorLowProductStockList(new object[] { User.UserId(),id});
        }
    }
}
