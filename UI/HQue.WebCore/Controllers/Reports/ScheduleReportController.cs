﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Misc;
using Utilities.Helpers;
using HQue.Biz.Core.Report;
using System.IO;
using System.Text;
using Utilities.Report;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Linq;

namespace HQue.WebCore.Controllers.Reports
{
    [Route("[controller]")]
    public class ScheduleReportController : BaseController
    {
        private readonly ReportManager _reportManager;
        private readonly ConfigHelper _configHelper;

        public ScheduleReportController(ReportManager reportManager, ConfigHelper configHelper) : base(configHelper)
        {
            _reportManager = reportManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<SalesItem>> ListData([FromBody]DateRange data, string type, string search, string schedule, string frombillno, string tobillno, string instanceid)
        {
            if (instanceid != null && instanceid != "undefined")
                return await _reportManager.ScheduleReportList(type, User.AccountId(), instanceid, schedule, data.FromDate, data.ToDate, search,frombillno, tobillno); //User.InstanceId()
            else
                return await _reportManager.ScheduleReportList(type, User.AccountId(), User.InstanceId(), schedule, data.FromDate, data.ToDate, search, frombillno, tobillno);
        }


        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> ListDataForPrint(string sInstanceId, string type, string schedule, string frombillno, string tobillno, string from, string to, string printType,string search)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;

            if (printType == null)
                printType = "1";

            List<FileStreamResult> listResult = new List<FileStreamResult>();
            List<SalesItem> List = new List<SalesItem>();
            DateTime? frmDt;
            DateTime? toDt;
            if (from != "" && from != "undefined" && from != null && to != "" && to != "undefined" && to != null)
            {
                frmDt = DateTime.Parse(from);
                toDt = DateTime.Parse(to);
            }
            else
            {
                frmDt = null;
                toDt = null;
            }


            List = await _reportManager.ScheduleReportListforPrint(type, User.AccountId(), sInstanceId, schedule, frmDt, toDt, frombillno, tobillno,search);



            //string name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Schedule");
            string name = "";           
            //string scheduleName = "";
            //if (isOffline)
            //{
            //    if (printType == "1")
            //        scheduleName = "hq_SH_";
            //    else
            //        scheduleName = "hq_SHed_";

            //    name = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\downloads\\" + scheduleName + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + "_Schedule.txt";
            //}
            //else
                name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Schedule");


            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                if (printType == "1")
                    writer.Write(GenerateStockReport_A4(List), false, Encoding.UTF8);
                else
                    writer.Write(GenerateStockReport_A4PageBreak(List), false, Encoding.UTF8);
            }

            HttpContext.Response.ContentType = "application/notepad";
            if (printType == "1")
                return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            else
                return File(info.OpenRead(), "application/notepad", string.Format("hq_SHed_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));

            //if (!isOffline)
            //{
            //    HttpContext.Response.ContentType = "application/notepad";
            //    if (printType == "1")
            //        return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            //    else
            //        return File(info.OpenRead(), "application/notepad", string.Format("hq_SHed_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            //}
            //else
            //{
            //    byte[] script = Encoding.UTF8.GetBytes("<script>window.close();</script>");
            //    HttpContext.Response.Body.Write(script, 0, script.Length);
            //    return new EmptyResult();
            //}

        }


        private string GenerateStockReport_A4(List<SalesItem> List)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 95;
            rpt.Rows = 38;

            WriteReportHeader_A4(rpt, List);
            WriteReportItems_A4(rpt, List);
            //WriteReportFooter_A4(rpt, List);

            return rpt.Content.ToString();
        }

        ////Changed for Page Header and to drag the Date Group to the next page
        //private string GenerateStockReport_A4(List<SalesItem> List)
        //{
        //    PlainTextReport rpt = new PlainTextReport();

        //    rpt.Columns = 90;
        //    rpt.Rows = 38;

        //    string prevDate = "";
        //    //int itemsHeight = 56; //70-10 (Page Height - Header Height)
        //    int itemsHeight = 47;
        //    //int dateCount = 0;
        //    int lineCount = 0; //to track the no. of lines added for a page
        //    int lineIter = 0;

        //    int totalCount = List.Count();

        //    List<SalesItem> groupedList = new List<SalesItem>();

        //    List.ForEach(x =>
        //    {
        //        lineCount++;
        //        lineIter++;
        //        if (prevDate != Convert.ToDateTime(x.Sales.InvoiceDate).ToString("yyyy-MM-dd")) //if the date changes
        //        {
        //            //dateCount++;
        //            if (lineCount == 1) // even for the very first item of the entire print increase the lines count by 2
        //            {
        //                lineCount += 2;
        //            }
        //            else
        //            {
        //                lineCount += 3; // whenever the date changes increase the lines count by 3
        //            }
        //        }

        //        if (lineCount <= itemsHeight) //if the line count for a page <= the maximum items height then add it to the group list
        //        {
        //            groupedList.Add(x);
        //        }
        //        if (lineCount > itemsHeight || totalCount == lineIter)  //if the line count for a page exceeds the maximum items height then write header, items and footer with the group list. Then clear the group list and add the current item.
        //        {
        //            WriteReportHeader_A4(rpt, groupedList);
        //            WriteReportItems_A4(rpt, groupedList);
        //            //WriteReportFooter_A4(rpt, itemsHeight + 1, itemsHeight); //passing itemsHeight + 1 for not printing the extra line.
        //            //for (int i = 0; i < 5; i++)
        //            for (int i = 0; i < 3; i++)
        //            {
        //                rpt.NewLine();
        //            }
        //            groupedList.RemoveAll(y => true);
        //            lineCount = 0;
        //            groupedList.Add(x);
        //        }
        //        prevDate = Convert.ToDateTime(x.Sales.InvoiceDate).ToString("yyyy-MM-dd");
        //    });


        //    return rpt.Content.ToString();
        //}

        private string GenerateStockReport_A4PageBreak(List<SalesItem> List)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 95;
            rpt.Rows = 38;

            int itemsHeight = 56; //70-10 (Page Height - Header Height)

            SalesItem prevX = null;
            List<SalesItem> groupedList = new List<SalesItem>();
            int listCount = 0;
            List.ForEach(x =>
            {
                listCount++;
                if (groupedList.Count == 0 || Convert.ToDateTime(prevX.Sales.InvoiceDate).ToString("yyyy-MM-dd") == Convert.ToDateTime(x.Sales.InvoiceDate).ToString("yyyy-MM-dd"))
                {
                    groupedList.Add(x);
                }
                else
                {
                    WriteReportHeader_A4(rpt, groupedList);
                    WriteReportItems_A4(rpt, groupedList);
                    WriteReportFooter_A4(rpt, groupedList.Count(), itemsHeight);

                    groupedList.RemoveAll(y => true);
                    groupedList.Add(x);
                }
                prevX = x;
                if (listCount == List.Count() || groupedList.Count == itemsHeight)
                {
                    WriteReportHeader_A4(rpt, groupedList);
                    WriteReportItems_A4(rpt, groupedList);
                    WriteReportFooter_A4(rpt, groupedList.Count(), itemsHeight);

                    groupedList.RemoveAll(y => true);
                }
            }
            );


            return rpt.Content.ToString();
        }

        private void WriteReportFooter_A4(PlainTextReport rpt, int len, int itemsHeight)
        {

            int emptyBreaks = 0;

            emptyBreaks = itemsHeight - len;

            rpt.Write();
            rpt.Drawline();
            //for (int i = 0; i < 5 - len; i++)
            //    rpt.NewLine();
            if (emptyBreaks > 0)
            {
                for (int i = 0; i < emptyBreaks; i++)
                    rpt.NewLine();
            }

        }

        private void WriteReportHeader_A4(PlainTextReport rpt, List<SalesItem> List)
        {
            //rpt.WriteLine();

            rpt.NewLine();
            rpt.Write(List[0].Sales.Instance.Name, 88, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write(List[0].Sales.Instance.FullAddress1, 88, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write("DL No   : " + List[0].Sales.Instance.DrugLicenseNo + " / " + "GSTIN :" + List[0].Sales.Instance.GsTinNo, 88, 0, Alignment.Center);

            //rpt.Drawline();

            rpt.Write();
            rpt.NewLine();
            rpt.Write();
            rpt.Drawline();
            rpt.NewLine();
            rpt.Write("S.No", 4, 0, Alignment.Right);
            //rpt.Write("Bill.No", 30, 8);
            //rpt.Write("Bill.Date", 30, 16);
            //rpt.Write("Patient", 50, 30);
            //rpt.Write("Doctor", 30, 45);
            //rpt.Write("Product", 50, 55);
            //rpt.Write("Manufacturer", 70, 80);
            //rpt.Write("Batch", 30, 90);
            //rpt.Write("Expire", 40, 100);
            //rpt.Write("Quantity", 40, 110);
            //rpt.Write("Signature", 30, 120);

            rpt.Write("B.No", 5, 5, Alignment.Right);
            //rpt.Write("B Date", 8, 12);
            rpt.Write("Patient", 12, 11);
            rpt.Write("Doctor", 13, 26);
            rpt.Write("Product", 17, 40, Alignment.Center);
            rpt.Write("MFR", 3, 58);
            rpt.Write("Batch", 8, 62, Alignment.Right);
            rpt.Write("Exp", 5, 72);
            rpt.Write("Qty", 5, 79, Alignment.Right);
            rpt.Write("Sign", 4, 85);

            rpt.Write();
            rpt.Drawline();
        }



        private void WriteReportItems_A4(PlainTextReport rpt, List<SalesItem> List)
        {
            int len = 0;
            var index = 0;
            string proName = null;
            //foreach (var items in List)
            for (var x = 0; x < List.Count; x++)
            {
                if (x == 0)
                {
                    index++;
                    rpt.NewLine();
                    rpt.Write("Bill Date:", 12, 1);
                    rpt.Write(string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate), 10, 12);
                    rpt.NewLine();
                    rpt.NewLine();

                    rpt.Write(string.Format("{0:0}", index), 4, 0, Alignment.Right);

                    rpt.Write(List[x].Sales.InvoiceSeries + List[x].Sales.InvoiceNo, 5, 5, Alignment.Right);
                    rpt.Write(List[x].Sales.NameAddress.ToUpper(), 12, 11);
                    rpt.Write(List[x].Sales.DoctorName != "" ? "Dr." + List[x].Sales.DoctorName.ToUpper() : "", 13, 26);
                    proName = List[x].ProductStock.Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");

                    rpt.Write(proName.ToUpper(), 17, 40);
                    rpt.Write(List[x].ProductStock.Product.Manufacturer.ToUpper(), 3, 58);
                    rpt.Write(List[x].ProductStock.BatchNo, 8, 62, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", List[x].ProductStock.ExpireDate), 5, 72);
                    rpt.Write(string.Format("{0:0}", List[x].Quantity), 5, 79, Alignment.Right);

                    rpt.Write();

                    if ((!string.IsNullOrEmpty(List[x].Sales.Address) && !string.IsNullOrWhiteSpace(List[x].Sales.Address)) || (!string.IsNullOrEmpty(List[x].Sales.DoctorAddress) && !string.IsNullOrWhiteSpace(List[x].Sales.DoctorAddress)))
                    {
                        rpt.NewLine();
                        rpt.Write(List[x].Sales.Address, 12, 11);
                        rpt.Write(List[x].Sales.DoctorAddress, 13, 26);
                        if (List[x].Sales.Address.Length > 12 || List[x].Sales.DoctorAddress.Length > 13)
                        {
                            rpt.NewLine();
                            string tempAddress = List[x].Sales.Address.Length > 12 ? List[x].Sales.Address.Substring(12) : "";
                            string tempAddress1 = List[x].Sales.DoctorAddress.Length > 13 ? List[x].Sales.DoctorAddress.Substring(13) : "";
                            rpt.Write(tempAddress, 12, 11);
                            rpt.Write(tempAddress1, 13, 26);
                        }
                    }

                    len++;
                }
                else
                {
                    string date1 = string.Format("{0:dd/MM/yy}", List[x - 1].Sales.InvoiceDate);
                    string date2 = string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate);
                    index++;
                    rpt.NewLine();
                    if (date1 != date2)
                    {
                        rpt.NewLine();
                        //rpt.Write("Bill Date:", 8, 12);                        
                        rpt.Write("Bill Date:", 12, 1);
                        rpt.Write(string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate), 10, 12);
                        rpt.NewLine();
                        rpt.NewLine();
                    }
                    rpt.Write(string.Format("{0}", index), 4,0, Alignment.Right);
                    if ((List[x - 1].Sales.InvoiceNo != List[x].Sales.InvoiceNo) || (List[x - 1].Sales.InvoiceSeries != List[x].Sales.InvoiceSeries))
                    {
                        rpt.Write(List[x].Sales.InvoiceSeries + List[x].Sales.InvoiceNo, 5, 5, Alignment.Right);
                        rpt.Write(List[x].Sales.NameAddress.ToUpper(), 12, 11);
                        rpt.Write(List[x].Sales.DoctorName != "" ? "Dr." + List[x].Sales.DoctorName.ToUpper() : "", 13, 26);
                    }
                    else
                    {
                        rpt.Write("", 5, 5);
                        rpt.Write("", 12, 11);
                        rpt.Write("", 13, 26);
                    }
                    proName = List[x].ProductStock.Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");
                    rpt.Write(proName.ToUpper(), 17, 40);
                    rpt.Write(List[x].ProductStock.Product.Manufacturer.ToUpper(), 3, 58);
                    rpt.Write(List[x].ProductStock.BatchNo, 8, 62, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", List[x].ProductStock.ExpireDate), 5, 72);
                    rpt.Write(string.Format("{0:0}", List[x].Quantity), 5, 79, Alignment.Right);

                    rpt.Write();

                    if ((!string.IsNullOrEmpty(List[x].Sales.Address) && !string.IsNullOrWhiteSpace(List[x].Sales.Address)) || (!string.IsNullOrEmpty(List[x].Sales.DoctorAddress) && !string.IsNullOrWhiteSpace(List[x].Sales.DoctorAddress)))
                    {
                        rpt.NewLine();
                        rpt.Write(List[x].Sales.Address, 12, 11);
                        rpt.Write(List[x].Sales.DoctorAddress, 13, 26);
                        if (List[x].Sales.Address.Length > 12 || List[x].Sales.DoctorAddress.Length > 13)
                        {
                            rpt.NewLine();
                            string tempAddress = List[x].Sales.Address.Length > 12 ? List[x].Sales.Address.Substring(12) : "";
                            string tempAddress1 = List[x].Sales.DoctorAddress.Length > 13 ? List[x].Sales.DoctorAddress.Substring(13) : "";
                            rpt.Write(tempAddress, 12, 11);
                            rpt.Write(tempAddress1, 13, 26);
                        }
                    }

                    len++;
                }

            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }


        //For Schedule Print 2

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> ListDataForPrint2(string sInstanceId, string type, string schedule, string frombillno, string tobillno, string from, string to, string printType, string search)
        {

            bool isOffline = _configHelper.AppConfig.OfflineMode;
            if (printType == null)
                printType = "1";

            List<FileStreamResult> listResult = new List<FileStreamResult>();
            List<SalesItem> List = new List<SalesItem>();
            DateTime? frmDt;
            DateTime? toDt;
            if (from != "" && from != "undefined" && from != null && to != "" && to != "undefined" && to != null)
            {
                frmDt = DateTime.Parse(from);
                toDt = DateTime.Parse(to);
                
            }
            else
            {
                frmDt = null;
                toDt = null;
              
            }


            List = await _reportManager.ScheduleReportListforPrint(type, User.AccountId(), sInstanceId, schedule, frmDt, toDt, frombillno, tobillno,search);



            // string name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Schedule");

            string name = "";
           // string scheduleName = "";
            

            //if (isOffline)
            //{
            //    if (printType == "1")
            //        scheduleName = "hq_SH_";
            //    else
            //        scheduleName = "hq_SHed_";

            //    name = System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\downloads\\" + scheduleName + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + "_Schedule.txt";
            //}
            //else
                name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Schedule");


            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                if (printType == "1")
                    writer.Write(GenerateStockReport1_A4(List, frmDt, toDt), false, Encoding.UTF8);
                else
                    writer.Write(GenerateStockReport_A4PageBreak1(List), false, Encoding.UTF8);
            }

            HttpContext.Response.ContentType = "application/notepad";
            if (printType == "1")
                return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            else
                return File(info.OpenRead(), "application/notepad", string.Format("hq_SHed_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));

            //if (!isOffline)
            //{
            //    HttpContext.Response.ContentType = "application/notepad";
            //    if (printType == "1")
            //        return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            //    else
            //        return File(info.OpenRead(), "application/notepad", string.Format("hq_SHed_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            //}
            //else
            //{
            //    byte[] script = Encoding.UTF8.GetBytes("<script>window.close();</script>");
            //    HttpContext.Response.Body.Write(script, 0, script.Length);
            //    return new EmptyResult();
            //}

        }
        //For Schedule Print 2

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> ListDataForPrint4(string sInstanceId, string type, string schedule, string frombillno, string tobillno, string from, string to, string printType, string search)
        {

            bool isOffline = _configHelper.AppConfig.OfflineMode;
            if (printType == null)
                printType = "1";

            List<FileStreamResult> listResult = new List<FileStreamResult>();
            List<SalesItem> List = new List<SalesItem>();
            DateTime? frmDt;
            DateTime? toDt;
            if (from != "" && from != "undefined" && from != null && to != "" && to != "undefined" && to != null)
            {
                frmDt = DateTime.Parse(from);
                toDt = DateTime.Parse(to);

            }
            else
            {
                frmDt = null;
                toDt = null;

            }


            List = await _reportManager.ScheduleReportListforPrint(type, User.AccountId(), sInstanceId, schedule, frmDt, toDt, frombillno, tobillno, search);

            string name = "";
            
            name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Schedule");


            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                if (printType == "1")
                    writer.Write(GenerateStockReport3_A4(List, frmDt, toDt), false, Encoding.UTF8);
                else
                    writer.Write(GenerateStockReport3_A4PageBreak1(List), false, Encoding.UTF8);
            }

            HttpContext.Response.ContentType = "application/notepad";
            if (printType == "1")
                return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            else
                return File(info.OpenRead(), "application/notepad", string.Format("hq_SHed_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
            

        }
        private string GenerateStockReport3_A4(List<SalesItem> List, DateTime? frmDt, DateTime? toDt)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 90;
            rpt.Rows = 38;

            WriteReportHeader3_A4(rpt, List, frmDt, toDt);
            WriteReportItems3_A4(rpt, List);

            return rpt.Content.ToString();
        }

        private void WriteReportHeader3_A4(PlainTextReport rpt, List<SalesItem> List, DateTime? frmDt, DateTime? toDt)
        {
            //rpt.WriteLine();

            rpt.NewLine();
            rpt.Write(List[0].Sales.Instance.Name, 78, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write(List[0].Sales.Instance.FullAddress1, 78, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write("DL No   : " + List[0].Sales.Instance.DrugLicenseNo + " / " + "GSTIN :" + List[0].Sales.Instance.GsTinNo, 78, 0, Alignment.Center);

            //rpt.Drawline();

            rpt.Write();
            rpt.NewLine();
            rpt.NewLine();
            if (frmDt != null && toDt != null)
                rpt.Write("Scheduled / Prescription Register – Date -" + String.Format("{0:d/M/yyyy}", frmDt) + " to " + String.Format("{0:d/M/yyyy}", toDt), 78, 0, Alignment.Center);
            else
                rpt.Write("Scheduled / Prescription Register ", 78, 0, Alignment.Center);


            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            rpt.Write("S.No", 4, 0, Alignment.Right);

            rpt.Write("B.No", 5, 5, Alignment.Right);

            rpt.Write("Patient", 12, 11);
            rpt.Write("Doctor", 15, 24);
            rpt.Write("Product", 15, 40, Alignment.Center);
            rpt.Write("Type", 4, 56);
            rpt.Write("Qty", 5, 61, Alignment.Right);
            rpt.Write("MFR", 4, 67);
            rpt.Write("Batch", 7, 72, Alignment.Right);
            rpt.Write("Exp", 5, 80);
            rpt.Write("Sign", 4, 86);

            rpt.NewLine();
            
            //rpt.Write();
            //rpt.NewLine();
            //rpt.Write("Patient add", 23, 11);
            //rpt.Write();
            //rpt.NewLine();
            //rpt.Write("Doc add", 21, 11);
            //rpt.Write();
            rpt.Drawline();
        }
        private void WriteReportItems3_A4(PlainTextReport rpt, List<SalesItem> List)
        {
            int len = 0;
            var index = 0;
            string proName = null;
            int isDoctor = 1;
            //foreach (var items in List)
            for (var x = 0; x < List.Count; x++)
            {

                if (x == 0)
                {
                    index++;
                    rpt.NewLine();
                    rpt.Write("Bill Date:", 12, 1);
                    rpt.Write(string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate), 10, 12);
                    rpt.NewLine();
                    rpt.NewLine();
                    rpt.Write(string.Format("{0:0}", index), 5, 0, Alignment.Right);

                    rpt.Write(List[x].Sales.InvoiceSeries + List[x].Sales.InvoiceNo, 5, 5, Alignment.Right);
                    rpt.Write(List[x].Sales.NameAddress.ToUpper(), 12, 11);
                    rpt.Write(List[x].Sales.DoctorName != "" ? "Dr." + List[x].Sales.DoctorName.ToUpper() : "", 15, 24);
                    proName = List[x].ProductStock.Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");

                    rpt.Write(proName.ToUpper(), 15, 40);
                    rpt.Write(string.Format("{0:0}", List[x].ProductStock.Product.Type), 4, 56);
                    rpt.Write(string.Format("{0:0}", List[x].Quantity), 5, 61, Alignment.Right);
                    rpt.Write(List[x].ProductStock.Product.Manufacturer.ToUpper(), 4, 67);
                    rpt.Write(List[x].ProductStock.BatchNo, 7, 72, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", List[x].ProductStock.ExpireDate), 5, 80);
                    
                    rpt.Write();
                    len++;
                    isDoctor = 2;
                    //if (List[x].Sales.NameAddress.ToUpper() != "")
                    //{
                    //    rpt.NewLine();
                    //    rpt.Write(List[x].Sales.NameAddress.ToUpper(), 26, 11);
                    //    isDoctor = 1;
                    //}
                    if ((!string.IsNullOrEmpty(List[x].Sales.Address) && !string.IsNullOrWhiteSpace(List[x].Sales.Address)) || (!string.IsNullOrEmpty(List[x].Sales.DoctorAddress) && !string.IsNullOrWhiteSpace(List[x].Sales.DoctorAddress)))
                    {
                        rpt.NewLine();
                        rpt.Write(List[x].Sales.Address, 12, 11);
                        rpt.Write(List[x].Sales.DoctorAddress, 15, 24);
                        isDoctor = 1;
                        if (List[x].Sales.Address.Length > 12 || List[x].Sales.DoctorAddress.Length > 15)
                        {
                            rpt.NewLine();
                            string tempAddress = List[x].Sales.Address.Length > 12 ? List[x].Sales.Address.Substring(12) : "";
                            string tempAddress1 = List[x].Sales.DoctorAddress.Length > 15 ? List[x].Sales.DoctorAddress.Substring(15) : "";
                            rpt.Write(tempAddress, 12, 11);
                            rpt.Write(tempAddress1, 15, 24);
                        }                 
                    }
                    
                }
                else
                {
                    string date1 = string.Format("{0:dd/MM/yy}", List[x - 1].Sales.InvoiceDate);
                    string date2 = string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate);
                    index++;
                    rpt.NewLine();

                    if (date1 != date2)
                    {
                        rpt.NewLine();
                        rpt.Write("Bill Date:", 12, 1);
                        rpt.Write(string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate), 10, 12);
                        rpt.NewLine();
                        rpt.NewLine();
                    }
                    rpt.Write(string.Format("{0:0}", index), 5, 0, Alignment.Right);
                    if ((List[x - 1].Sales.InvoiceNo != List[x].Sales.InvoiceNo) || (List[x - 1].Sales.InvoiceSeries != List[x].Sales.InvoiceSeries))
                    {
                        rpt.Write(List[x].Sales.InvoiceSeries + List[x].Sales.InvoiceNo, 5, 5, Alignment.Right);
                        rpt.Write(List[x].Sales.NameAddress.ToUpper(), 12, 11);
                        rpt.Write(List[x].Sales.DoctorName != "" ? "Dr." + List[x].Sales.DoctorName.ToUpper() : "", 15, 24);
                        isDoctor = 2;
                    }
                    else
                    {
                        if (isDoctor == 2)
                            rpt.Write(List[x].Sales.NameAddress.ToUpper(), 26, 11);

                        isDoctor = 1;

                    }
                    proName = List[x].ProductStock.Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");
                    rpt.Write(proName.ToUpper(), 15, 40);
                    rpt.Write(string.Format("{0:0}", List[x].ProductStock.Product.Type), 4, 56);
                    rpt.Write(string.Format("{0:0}", List[x].Quantity), 5, 61, Alignment.Right);
                    rpt.Write(List[x].ProductStock.Product.Manufacturer.ToUpper(), 4, 67);
                    rpt.Write(List[x].ProductStock.BatchNo, 7, 72, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", List[x].ProductStock.ExpireDate), 5, 80);

                    //if (isDoctor == 2)
                    //{
                    //    if (List[x].Sales.NameAddress != "" && List[x].Sales.NameAddress != null)
                    //    {
                    //        rpt.NewLine();
                    //        rpt.Write(List[x].Sales.NameAddress.ToUpper(), 26, 11);
                    //        isDoctor = 1;
                    //    }
                    //}

                    if ((!string.IsNullOrEmpty(List[x].Sales.Address) && !string.IsNullOrWhiteSpace(List[x].Sales.Address)) || (!string.IsNullOrEmpty(List[x].Sales.DoctorAddress) && !string.IsNullOrWhiteSpace(List[x].Sales.DoctorAddress)))
                    {
                        rpt.NewLine();
                        rpt.Write(List[x].Sales.Address, 12, 11);
                        rpt.Write(List[x].Sales.DoctorAddress, 15, 24);
                        isDoctor = 1;
                        if (List[x].Sales.Address.Length > 12 || List[x].Sales.DoctorAddress.Length > 15)
                        {
                            rpt.NewLine();
                            string tempAddress = List[x].Sales.Address.Length > 12 ? List[x].Sales.Address.Substring(12) : "";
                            string tempAddress1 = List[x].Sales.DoctorAddress.Length > 15 ? List[x].Sales.DoctorAddress.Substring(15) : "";
                            rpt.Write(tempAddress, 12, 11);
                            rpt.Write(tempAddress1, 15, 24);
                        }
                    }

                    rpt.Write();
                    len++;
                }

            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }
        private string GenerateStockReport1_A4(List<SalesItem> List, DateTime? frmDt, DateTime? toDt)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 90;
            rpt.Rows = 38;

            WriteReportHeader1_A4(rpt, List, frmDt, toDt);
            WriteReportItems1_A4(rpt, List);

            return rpt.Content.ToString();
        }

        private void WriteReportHeader1_A4(PlainTextReport rpt, List<SalesItem> List, DateTime? frmDt, DateTime? toDt)
        {
            //rpt.WriteLine();

            rpt.NewLine();
            rpt.Write(List[0].Sales.Instance.Name, 78, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write(List[0].Sales.Instance.FullAddress1, 78, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write("DL No   : " + List[0].Sales.Instance.DrugLicenseNo + " / " + "GSTIN :" + List[0].Sales.Instance.GsTinNo, 78, 0, Alignment.Center);

            //rpt.Drawline();

            rpt.Write();
            rpt.NewLine();
            rpt.NewLine();
            if (frmDt!=null && toDt!=null)
                 rpt.Write("Scheduled / Prescription Register – Date -"+ String.Format("{0:d/M/yyyy}", frmDt) +" to "+ String.Format("{0:d/M/yyyy}", toDt), 78,0, Alignment.Center);
            else
                rpt.Write("Scheduled / Prescription Register ",78, 0, Alignment.Center);

           
            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            rpt.Write("S.No", 4, 0, Alignment.Right);         

            rpt.Write("B No", 5, 5, Alignment.Right);
            
            
            rpt.Write("Doctor", 16, 11);
            rpt.Write("Product", 17, 31, Alignment.Center);
            rpt.Write("Type", 4, 50);
            rpt.Write("Qty", 5, 56, Alignment.Right);
            rpt.Write("MFR", 4, 62);
            rpt.Write("Batch", 10, 67, Alignment.Right);
            rpt.Write("Exp", 5, 79);
            rpt.Write("Sign", 4, 86);

            rpt.NewLine();
            rpt.Write("Patient", 26, 11);
            rpt.Write();
            rpt.Drawline();
        }



        private void WriteReportItems1_A4(PlainTextReport rpt, List<SalesItem> List)
        {
            int len = 0;
            var index = 0;
            string proName = null;
            int isDoctor = 1;
            //foreach (var items in List)
            for (var x = 0; x < List.Count; x++)
            {
               
                if (x == 0)
                {
                    index++;
                    rpt.NewLine();
                    rpt.Write("Bill Date:", 12, 1);
                    rpt.Write(string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate), 10, 12);
                    rpt.NewLine();
                    rpt.NewLine();
                    rpt.Write(string.Format("{0:0}", index), 5,0, Alignment.Right);

                    rpt.Write(List[x].Sales.InvoiceSeries + List[x].Sales.InvoiceNo, 5, 5, Alignment.Right);
                    //rpt.Write(List[x].Sales.NameAddress.ToUpper(), 13, 11);
                    rpt.Write(List[x].Sales.DoctorName != "" ? "Dr." + List[x].Sales.DoctorName.ToUpper() : "", 16, 11);
                    proName = List[x].ProductStock.Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");

                    rpt.Write(proName.ToUpper(), 17, 31);
                    rpt.Write(string.Format("{0:0}", List[x].ProductStock.Product.Type), 4, 50);
                    rpt.Write(string.Format("{0:0}", List[x].Quantity), 5, 56, Alignment.Right);                    
                    rpt.Write(List[x].ProductStock.Product.Manufacturer.ToUpper(), 4, 62);
                    rpt.Write(List[x].ProductStock.BatchNo, 10, 67, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", List[x].ProductStock.ExpireDate), 5, 79);
                    //rpt.NewLine();
                    //rpt.Write(List[x].Sales.DoctorName.ToUpper(), 12, 11);

                    rpt.Write();
                    len++;
                    isDoctor = 2;
                    if(List[x].Sales.NameAddress.ToUpper()!="")
                    {
                        rpt.NewLine();
                        rpt.Write(List[x].Sales.NameAddress.ToUpper(), 26, 11);
                        isDoctor =1;
                    }

                }
                else
                {
                    string date1 = string.Format("{0:dd/MM/yy}", List[x - 1].Sales.InvoiceDate);
                    string date2 = string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate);
                    index++;
                    rpt.NewLine();
                   
                    if (date1 != date2)
                    {
                        rpt.NewLine();
                        //rpt.Write("Bill Date:", 8, 12);                        
                        rpt.Write("Bill Date:", 12, 1);
                        rpt.Write(string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate), 10, 12);
                        rpt.NewLine();
                        rpt.NewLine();
                    }
                    rpt.Write(string.Format("{0:0}", index), 5,0, Alignment.Right);
                    if ((List[x - 1].Sales.InvoiceNo != List[x].Sales.InvoiceNo) || (List[x - 1].Sales.InvoiceSeries != List[x].Sales.InvoiceSeries))
                    {
                        rpt.Write(List[x].Sales.InvoiceSeries + List[x].Sales.InvoiceNo, 5, 5, Alignment.Right);
                        //rpt.Write(List[x].Sales.NameAddress.ToUpper(), 13, 11);
                        rpt.Write(List[x].Sales.DoctorName!=""?"Dr." +List[x].Sales.DoctorName.ToUpper():"", 16, 11);
                        isDoctor = 2;
                    }
                    else
                    {
                        //rpt.Write("", 5, 5);
                       if(isDoctor==2)
                            rpt.Write(List[x].Sales.NameAddress.ToUpper(), 26, 11);

                        isDoctor = 1;
                      
                    }
                    proName = List[x].ProductStock.Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");
                    rpt.Write(proName.ToUpper(), 17, 31);
                    rpt.Write(string.Format("{0:0}", List[x].ProductStock.Product.Type), 4, 50);
                    rpt.Write(string.Format("{0:0}", List[x].Quantity), 5, 56, Alignment.Right);
                    rpt.Write(List[x].ProductStock.Product.Manufacturer.ToUpper(), 4, 62);
                    rpt.Write(List[x].ProductStock.BatchNo, 10, 67, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", List[x].ProductStock.ExpireDate), 5, 79);
                    
                    if(isDoctor==2)
                    {
                        if (List[x].Sales.NameAddress != "" && List[x].Sales.NameAddress != null)
                        {
                            rpt.NewLine();
                            //rpt.Write("", 5, 5);
                            rpt.Write(List[x].Sales.NameAddress.ToUpper(), 26, 11);
                            isDoctor = 1;
                        }
                    }

                    rpt.Write();
                    len++;
                }

            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }
        private string GenerateStockReport3_A4PageBreak1(List<SalesItem> List)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 90;
            rpt.Rows = 38;

            int itemsHeight = 56; //70-10 (Page Height - Header Height)

            SalesItem prevX = null;
            List<SalesItem> groupedList = new List<SalesItem>();
            int listCount = 0;
            List.ForEach(x =>
            {
                listCount++;
                if (groupedList.Count == 0 || Convert.ToDateTime(prevX.Sales.InvoiceDate).ToString("yyyy-MM-dd") == Convert.ToDateTime(x.Sales.InvoiceDate).ToString("yyyy-MM-dd"))
                {
                    groupedList.Add(x);
                }
                else
                {
                    WriteReportHeader_A4(rpt, groupedList);
                    WriteReportItems_A4(rpt, groupedList);
                    WriteReportFooter_A4(rpt, groupedList.Count(), itemsHeight);

                    groupedList.RemoveAll(y => true);
                    groupedList.Add(x);
                }
                prevX = x;
                if (listCount == List.Count() || groupedList.Count == itemsHeight)
                {
                    WriteReportHeader_A4(rpt, groupedList);
                    WriteReportItems_A4(rpt, groupedList);
                    WriteReportFooter_A4(rpt, groupedList.Count(), itemsHeight);

                    groupedList.RemoveAll(y => true);
                }
            }
            );


            return rpt.Content.ToString();
        }



        private string GenerateStockReport_A4PageBreak1(List<SalesItem> List)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 90;
            rpt.Rows = 38;

            int itemsHeight = 56; //70-10 (Page Height - Header Height)

            SalesItem prevX = null;
            List<SalesItem> groupedList = new List<SalesItem>();
            int listCount = 0;
            List.ForEach(x =>
            {
                listCount++;
                if (groupedList.Count == 0 || Convert.ToDateTime(prevX.Sales.InvoiceDate).ToString("yyyy-MM-dd") == Convert.ToDateTime(x.Sales.InvoiceDate).ToString("yyyy-MM-dd"))
                {
                    groupedList.Add(x);
                }
                else
                {
                    WriteReportHeader_A4(rpt, groupedList);
                    WriteReportItems_A4(rpt, groupedList);
                    WriteReportFooter_A4(rpt, groupedList.Count(), itemsHeight);

                    groupedList.RemoveAll(y => true);
                    groupedList.Add(x);
                }
                prevX = x;
                if (listCount == List.Count() || groupedList.Count == itemsHeight)
                {
                    WriteReportHeader_A4(rpt, groupedList);
                    WriteReportItems_A4(rpt, groupedList);
                    WriteReportFooter_A4(rpt, groupedList.Count(), itemsHeight);

                    groupedList.RemoveAll(y => true);
                }
            }
            );


            return rpt.Content.ToString();
        }

        
        
        //For Schedule Print 3

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<ActionResult> ListDataForPrint3(string sInstanceId, string type, string schedule, string frombillno, string tobillno, string from, string to, string printType, string search)
        {
            bool isOffline = _configHelper.AppConfig.OfflineMode;
            if (printType == null)
                printType = "1";

            List<FileStreamResult> listResult = new List<FileStreamResult>();
            List<SalesItem> List = new List<SalesItem>();
            DateTime? frmDt;
            DateTime? toDt;
            if (from != "" && from != "undefined" && from != null && to != "" && to != "undefined" && to != null)
            {
                frmDt = DateTime.Parse(from);
                toDt = DateTime.Parse(to);
            }
            else
            {
                frmDt = null;
                toDt = null;
            }
            
            List = await _reportManager.ScheduleReportListforPrint(type, User.AccountId(), sInstanceId, schedule, frmDt, toDt, frombillno, tobillno, search);
                        
            string name = "";
            name = string.Format("./wwwroot/temp/purchasebill_{0}.txt", "Schedule");

            FileInfo info = new FileInfo(name);
            if (info.Exists)
            {
                info.Delete();
            }
            using (StreamWriter writer = info.CreateText())
            {
                writer.Write(GenerateStockReport2_A4(List, frmDt, toDt), false, Encoding.UTF8);                
            }

            HttpContext.Response.ContentType = "application/notepad";

            return File(info.OpenRead(), "application/notepad", string.Format("hq_SH_{0}.txt", "Schedule" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss")));
        }
        
        private string GenerateStockReport2_A4(List<SalesItem> List, DateTime? frmDt, DateTime? toDt)
        {
            PlainTextReport rpt = new PlainTextReport();

            rpt.Columns = 90;
            rpt.Rows = 38;

            WriteReportHeader2_A4(rpt, List, frmDt, toDt);
            WriteReportItems2_A4(rpt, List);

            return rpt.Content.ToString();
        }

        private void WriteReportHeader2_A4(PlainTextReport rpt, List<SalesItem> List, DateTime? frmDt, DateTime? toDt)
        {
            rpt.NewLine();
            rpt.Write(List[0].Sales.Instance.Name, 78, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write(List[0].Sales.Instance.FullAddress1, 78, 0, Alignment.Center);
            rpt.NewLine();
            rpt.Write("DL No   : " + List[0].Sales.Instance.DrugLicenseNo + " / " + "GSTIN :" + List[0].Sales.Instance.GsTinNo, 78, 0, Alignment.Center);
            
            rpt.Write();
            rpt.NewLine();
            rpt.NewLine();
            if (frmDt != null && toDt != null)
                rpt.Write("Scheduled / Prescription Register – Date -" + String.Format("{0:d/M/yyyy}", frmDt) + " to " + String.Format("{0:d/M/yyyy}", toDt), 78, 0, Alignment.Center);
            else
                rpt.Write("Scheduled / Prescription Register ", 78, 0, Alignment.Center);


            rpt.Write();
            rpt.Drawline(1);
            rpt.NewLine();
            rpt.Write("S.No", 4, 0, Alignment.Right);

            rpt.Write("B No", 7, 5, Alignment.Right);

            rpt.Write("Doctor", 30, 13);
            rpt.Write("Patient", 26, 45);

            rpt.NewLine();
            rpt.Write("Product", 30, 13);
            rpt.Write("Type", 4, 44);
            rpt.Write("MFR", 4, 50);
            rpt.Write("Batch", 12, 56, Alignment.Right);
            rpt.Write("Qty", 5, 70, Alignment.Right);            
            rpt.Write("Exp", 5, 76);
            rpt.Write("Sign", 4, 85);
            
            rpt.Write();
            rpt.Drawline();
        }
        

        private void WriteReportItems2_A4(PlainTextReport rpt, List<SalesItem> List)
        {
            int len = 0;
            var index = 0;
            string proName = null;
            
            for (var x = 0; x < List.Count; x++)
            {
                if (x == 0)
                {
                    index++;
                    rpt.NewLine();
                    rpt.Write("Bill Date:", 12, 1);
                    rpt.Write(string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate), 10, 12);
                    rpt.NewLine();
                    rpt.NewLine();
                    rpt.Write(string.Format("{0}", index), 4, 0, Alignment.Right);

                    rpt.Write(List[x].Sales.InvoiceSeries + List[x].Sales.InvoiceNo, 7, 5, Alignment.Right);                    
                    rpt.Write(List[x].Sales.DoctorName.ToUpper(), 30, 13);
                    rpt.Write(List[x].Sales.NameAddress.ToUpper(), 26, 45);

                    rpt.NewLine();
                    proName = List[x].ProductStock.Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");

                    rpt.Write(proName.ToUpper(), 30, 13);
                    rpt.Write(List[x].ProductStock.Product.Manufacturer.ToUpper(), 4, 44);
                    rpt.Write(List[x].ProductStock.Product.Type.ToUpper(), 4, 50);
                    rpt.Write(List[x].ProductStock.BatchNo, 12, 56, Alignment.Right);
                    rpt.Write(string.Format("{0:0}", List[x].Quantity), 5, 70, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", List[x].ProductStock.ExpireDate), 5, 76);

                    rpt.Write();
                    len++;
                }
                else
                {
                    string date1 = string.Format("{0:dd/MM/yy}", List[x - 1].Sales.InvoiceDate);
                    string date2 = string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate);
                    index++;
                    rpt.NewLine();
                    if (date1 != date2)
                    {
                        rpt.NewLine();                                             
                        rpt.Write("Bill Date:", 12, 1);
                        rpt.Write(string.Format("{0:dd/MM/yy}", List[x].Sales.InvoiceDate), 10, 12);
                        rpt.NewLine();
                        rpt.NewLine();
                    }
                    rpt.Write(string.Format("{0}", index), 4, 0, Alignment.Right);
                    if ((List[x - 1].Sales.InvoiceNo != List[x].Sales.InvoiceNo) || (List[x - 1].Sales.InvoiceSeries != List[x].Sales.InvoiceSeries))
                    {
                        rpt.Write(List[x].Sales.InvoiceSeries + List[x].Sales.InvoiceNo, 7, 5, Alignment.Right);                        
                        rpt.Write(List[x].Sales.DoctorName.ToUpper(), 30, 13);
                        rpt.Write(List[x].Sales.NameAddress.ToUpper(), 26, 45);
                        rpt.NewLine();
                    }
                    else
                    {
                        rpt.Write("", 7, 5);
                        rpt.Write("", 30, 13);
                        rpt.Write("", 26, 45);
                        //rpt.NewLine();
                    }
                    proName = List[x].ProductStock.Product.Name.Replace("{", "{{");
                    proName = proName.Replace("}", "}}");
                    rpt.Write(proName.ToUpper(), 30, 13);
                    rpt.Write(List[x].ProductStock.Product.Manufacturer.ToUpper(), 4, 44);
                    rpt.Write(List[x].ProductStock.Product.Type.ToUpper(), 4, 50);
                    rpt.Write(List[x].ProductStock.BatchNo, 12, 56, Alignment.Right);
                    rpt.Write(string.Format("{0:0}", List[x].Quantity), 5, 70, Alignment.Right);
                    rpt.Write(string.Format("{0:MM/yy}", List[x].ProductStock.ExpireDate), 5, 76);
                   

                    rpt.Write();
                    len++;
                }

            }

            for (int i = 0; i < 5 - len; i++)
                rpt.NewLine();

            rpt.Write();
            rpt.Drawline();
        }

    }
}
