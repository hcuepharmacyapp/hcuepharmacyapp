﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Utilities.Helpers;
using System.Security.Claims;
using DataAccess.QueryBuilder;

namespace HQue.WebCore.Controllers
{
    public class BaseController : Controller
    {
        protected readonly ConfigHelper ConfigHelper;
        public BaseController(ConfigHelper configHelper)
        {
            
            ConfigHelper = configHelper;
           
            //if ( (configHelper.HttpContextAccessor.HttpContext.Request.QueryString.Value.ToString() !="")&& !(configHelper.HttpContextAccessor.HttpContext.Request.QueryString.ToString().Contains("/Login")  ) && configHelper.HttpContextAccessor.HttpContext.Request.Cookies.Keys.Count == 1)
           
            //{
            //    configHelper.HttpContextAccessor.HttpContext.Authentication.SignOutAsync("Cookies");
            //    RedirectToRoute(new { Controller = "User", Action = "Logout" });
            //    //Response.Redirect(Url.Action("/User/Login"));
              
            //    //RedirectToAction("Login");
            //}

        }
       
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            ViewBag.Config = ConfigHelper;
            //if (context.HttpContext.Request.QueryString.Value.ToString()!="" && !(context.HttpContext.Request.QueryString.Value.Contains("/login")) &&
            //        (context.HttpContext.Request.Cookies.Keys.Count ==1))
            //    context.Result = new RedirectResult("/User/Login");
            base.OnActionExecuted(context);
        }
    }
}
