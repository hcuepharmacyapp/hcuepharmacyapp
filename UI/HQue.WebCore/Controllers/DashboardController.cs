﻿using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Dashboard;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Biz.Core.Report;

namespace HQue.WebCore.Controllers
{
    [Route("[controller]")]
    public class DashboardController : BaseController
    {
        private readonly ReportManager _reportManager;
        public DashboardController(ConfigHelper configHelper, ReportManager reportManager) : base(configHelper)
        {
            _reportManager = reportManager;
        }
        public IActionResult Index()
        {
            ViewBag.InstanceId = User.InstanceId();
            return View();
        }
        /*Added by Poongodi For finyear validation 30/03/2017*/
        [Route("[action]")]
        public IActionResult Index1()
        {

            return View();
        }
        [Route("[action]")]
        public IActionResult AllPharmaDetails()
        {
            if (User.AccountId() != "4edd19c2-3fe3-4ba2-8083-83a06ccd6c88")
            {
                Response.Redirect("/Dashboard");
            }
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<getAllPharmaItem>> getAllPharmaDetails([FromBody]DateRange data, string type, int registerType)
        {
            return await _reportManager.getAllPharmaList(type, data.FromDate, data.ToDate, registerType);
        }
    }
}
