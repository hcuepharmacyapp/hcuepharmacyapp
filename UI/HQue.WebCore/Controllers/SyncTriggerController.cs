﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using HQue.Biz.Core.Replication;

namespace HQue.WebCore.Controllers
{
    [Route("[controller]")]
    public class SyncTriggerController : Controller
    {
        private readonly ISyncTrigger _syncTrigger;

        
        public SyncTriggerController(ISyncTrigger syncTrigger)
        {
            _syncTrigger = syncTrigger;
        }

        [AllowAnonymous]
        [Route("[action]")]
        public async Task<bool> Index()
        {
            try
            {
                await _syncTrigger.StartSync();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }
    }
}
