﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Master;
using Utilities.Helpers;
using HQue.Contract.Infrastructure;
using HQue.Biz.Core.Master;

namespace HQue.WebCore.Controllers.Data.Master
{
    [Route("[controller]")]
    public class VendorDataController : Controller
    {
        private readonly VendorManager _vendorManager;

        public VendorDataController(VendorManager vendorManager)
        {
            _vendorManager = vendorManager;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<string> Index([FromBody]Vendor model)
        {
            //if (!ModelState.IsValid)
            //    return View(model);

            //Remove below line after db change
            model.Code = "Vendor000";
            model.SetLoggedUserDetails(User);
            return await _vendorManager.Save(model);
            //return null;
        }

        [Route("[action]")]
        public async Task<object> Update([FromBody] Vendor model)
        {
            //if (!ModelState.IsValid)
            //    return View(model);

            //Remove below line after db change
            model.Code = "Vendor000";
            model.InstanceId = User.InstanceId();  //For offline Added by Poongodi on 05/06/2017
            model.SetLoggedUserDetails(User);
            var objVendor = await _vendorManager.Update(model);
            if (objVendor != null)
                return objVendor;
            else
                return null;
        }

        [Route("[action]")]
        public async Task<Vendor> GetById([FromBody]Vendor vendor)
        {
            vendor.SetLoggedUserDetails(User);
            var result = await _vendorManager.GetById(vendor);
            return result;
        }

        [Route("[action]")]
        public async Task<bool> PaymentStatus(string vendorId)
        {            
            var result = await _vendorManager.PaymentStatus(User.AccountId(), User.InstanceId(), vendorId);

            return result;
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<int> CheckOBPaymentPaid(string customerid)
        {
            
           var rtnVal = await _vendorManager.CheckOBPaymentPaid(User.AccountId(), User.InstanceId(), customerid);

            return rtnVal;
        }


        [Route("[action]")]
        public async Task<PagerContract<Vendor>> ListData([FromBody]Vendor model)
        {
            model.SetLoggedUserDetails(User);
            return await _vendorManager.ListPager(model);
        }

        [Route("[action]")]
        public async Task<IEnumerable<Vendor>> VendorList(Vendor vendor)
        {
            vendor.SetLoggedUserDetails(User);           
            return await _vendorManager.VendorList(vendor);
        }

        [Route("[action]")]
        public async Task<IEnumerable<Vendor>> VendorDataList(Vendor vendor, int vendorStatus)
        {
            vendor.SetLoggedUserDetails(User);
            vendor.Status = vendorStatus;
            return await _vendorManager.VendorDataList(vendor);
        }

        [Route("[action]")]
        public Task <List<AlternateVendorProduct>> CheckExisitingProductList(string vendorId)
        {
          
            return _vendorManager.CheckExisitingProductList(vendorId);
        }

        [Route("[action]")]
        public Task<List<State>> GetState(string stateName)
        {            
            var result = _vendorManager.GetState(stateName);
            return result;
        }
        //Added By Gavaskar Vendor Bulk Update  14-08-2017

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Vendor>> BulkUpdate([FromBody]List<Vendor> model)
        {
            Vendor userData = new Vendor();
            userData.SetLoggedUserDetails(User);
            await _vendorManager.BulkProductUpdate(model, userData);
            return model;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<VendorPurchaseFormula> SavePurchaseFormula([FromBody]VendorPurchaseFormula model)
        {
            model.SetLoggedUserDetails(User);
            return await _vendorManager.SavePurchaseFormula(model);
        }

        [Route("[action]")]
        public async Task<List<VendorPurchaseFormula>> GetPurchaseFormulaList(string formulaName)
        {
            return await _vendorManager.GetPurchaseFormulaList(User.AccountId(), formulaName);
        }

        //Added by Sarubala on 17-11-17
        [Route("[action]")]
        public async Task<bool> GetVendorCreateSmsSetting()
        {
            return await _vendorManager.GetVendorCreateSmsSetting(User.InstanceId());
        }
    }
}
