﻿using HQue.Biz.Core.Leads;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Leads;
using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Data.Master
{
    [Route("[controller]")]
    public class LeadsDataController : Controller
    {
        private readonly LeadsManager _leadsManager;

        public LeadsDataController(LeadsManager leadsManager)
        {
            _leadsManager = leadsManager;
        }

        [Route("[action]")]
        public async System.Threading.Tasks.Task<PagerContract<Leads>> ListData([FromBody]Leads model)
        {
            model.SetLoggedUserDetails(User);
            return await _leadsManager.ListPager(model);
        }

        [Route("[action]")]
        public async System.Threading.Tasks.Task<ActionResult> UpdateStatus([FromBody]Leads model)
        {
            model.SetLoggedUserDetails(User);
            await _leadsManager.UpdateStatus(model);
            return View();
        }
    }
}
