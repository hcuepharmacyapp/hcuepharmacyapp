﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Dashboard;
using Utilities.Helpers;
using HQue.Biz.Core.Dashboard;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.WebCore.Controllers.Data
{
    [Route("[controller]")]
    public class DashboardDataController : Controller
    {
        private readonly DashboardManager _dashboardManager;
        private readonly ConfigHelper _configHelper;

        public DashboardDataController(DashboardManager dashboardManager,ConfigHelper configHelper)
        {
            _dashboardManager = dashboardManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public Task<IEnumerable<NameValue>> LowStock(string InstanceId)
        {
            return _dashboardManager.GetTopLowStockItem(User.AccountId(), InstanceId);
        }

        
        [Route("[action]")]
        public Task<IEnumerable<NameValue>> TrendingSales(string InstanceId)
        {
            return _dashboardManager.TrendingTopSales(User.AccountId(), InstanceId);
        }

        [Route("[action]")]
        public async Task<BarGraph> GetPendingLeads(string InstanceId)
        {
            if (_configHelper.AppConfig.IsSqlServer)
                return await _dashboardManager.GetPendingLeads(User.AccountId(), InstanceId);
            return new BarGraph();
        }

        [Route("[action]")]
        public Task<IEnumerable<NameValue>> AboutToExpireProduct(string InstanceId)
        {
            return _dashboardManager.AboutToExpireProduct(User.AccountId(), InstanceId);
        }

        //Newly added by Manivannan on 25-02-2017
        [Route("[action]")]
        public Task<IEnumerable<Instance>> LoadInstances()
        {
            return _dashboardManager.LoadInstances(User.AccountId());
        }

        // Newly Added Gavaskar 27-10-2016 Start
        [Route("[action]")]
        public Task<IEnumerable<NameValue>> Sales(string InstanceId)
        {
            return _dashboardManager.Sales(InstanceId, User.AccountId());
        }

        [Route("[action]")]
        public Task<IEnumerable<NameValue>> Purchase(string InstanceId)
        {
            
            return _dashboardManager.Purchase(InstanceId, User.AccountId());
        }

        // Newly Added Gavaskar 27-10-2016 End

        // Newly Added Gavaskar 15-11-2016 Start
        [Route("[action]")]
        public Task<IEnumerable<NameValue>> CurrentStock(string InstanceId)
        {
            return _dashboardManager.CurrentStock(User.AccountId(), InstanceId);
            //return null;
        }

        // Newly Added Gavaskar 15-11-2016 End
    }
}
