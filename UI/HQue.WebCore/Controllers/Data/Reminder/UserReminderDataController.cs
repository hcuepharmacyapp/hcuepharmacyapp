﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Biz.Core.Reminder;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Reminder;
using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Data.Reminder
{
    [Route("[controller]")]
    public class UserReminderDataController : Controller
    {
        private readonly UserReminderManager _userReminderManager;

        public UserReminderDataController(UserReminderManager userReminderManager)
        {
            _userReminderManager = userReminderManager;
        }

        [Route("[action]")]
        [HttpPost]
        public Task<bool> CancelReminder(string id)
        {
            return _userReminderManager.CancelReminder(id);
        }

        [Route("[action]")]
        [HttpPost]
        public Task<bool> SaveRemark([FromBody] ReminderRemark data)
        {
            data.SetLoggedUserDetails(User);
            return _userReminderManager.SaveRemark(data);
        }

        [Route("[action]")]
        public async Task<int> OpenReminderCount()
        {
            return await _userReminderManager.OpenReminderCount();
        }

        #region PersonalReminder
        [Route("[action]")]
        public async Task<List<UserReminder>> PersonalReminderList(bool isUpcoming)
        {
            return await _userReminderManager.PersonalReminderList(isUpcoming);
        }

        [Route("[action]")]
        public async Task<PagerContract<UserReminder>> AllPersonalReminder()
        {
            return await _userReminderManager.PersonalReminderList();
        }

        #endregion

        #region CustomerReminder

        [Route("[action]")]
        [HttpPost]
        public async Task<UserReminder> CreateCustomerReminder([FromBody] CustomerReminder data)
        {
            data.SetLoggedUserDetails(User);
            data.HqueUserId = User.Identity.Id();
            return await _userReminderManager.CreateCustomerReminder(data);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<List<CustomerReminder>> CustomerReminderList(bool isUpcoming, [FromBody]CustomerReminder customerReminder)
        {
            return await _userReminderManager.CustomerReminderList(isUpcoming, customerReminder);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<PagerContract<CustomerReminder>> AllCustomerReminder([FromBody] CustomerReminder data)
        {
            data.AccountId = User.AccountId();
            data.InstanceId = User.InstanceId();
            return await _userReminderManager.CustomerReminderList(data);
        }
        #endregion
    }
}
