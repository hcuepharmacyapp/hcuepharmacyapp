﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Accounts;
using Utilities.Helpers;
using HQue.Biz.Core.Accounts;
using HQue.Contract.Infrastructure.Misc;

namespace HQue.WebCore.Controllers.Data.Accounts
{
    [Route("[controller]")]
    public class PettyCashSettingsDataController : Controller
    {
        private readonly PettyCashSettingsManager _pettyCashSettingsManager;

        public PettyCashSettingsDataController(PettyCashSettingsManager pettyCashSettingsManager)
        {
            _pettyCashSettingsManager = pettyCashSettingsManager;
        }

        //[Route("[action]")]
        //[HttpPost]
        //public async Task<PettyCash> Index([FromBody]PettyCash model)
        //{
        //    model.SetLoggedUserDetails(User);
        //    model.UserId = User.Identity.Id();
        //    await _pettyCashManager.Save(model);
        //    return null;
        //}

        //[Route("[action]")]
        //public async Task<PagerContract<PettyCash>> ListData([FromBody]DateRange data, string type)
        //{
        //    return await _pettyCashManager.ListPager(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate);
        //}

        [Route("[action]")]
        [HttpPost]
        public async Task<PettyCashSettings> Index([FromBody]PettyCashSettings model)
        {
            model.SetLoggedUserDetails(User);
            //model.UserId = User.Identity.Id();
            await _pettyCashSettingsManager.Save(model);
            return null;
        }
    }
}