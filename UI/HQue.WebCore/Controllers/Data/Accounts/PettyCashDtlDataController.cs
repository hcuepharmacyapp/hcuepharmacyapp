﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Accounts;
using Utilities.Helpers;
using HQue.Biz.Core.Accounts;
using HQue.Contract.Infrastructure.Misc;
using System;
using System.Collections.Generic;

namespace HQue.WebCore.Controllers.Data.Accounts
{
   
    [Route("[controller]")]
    public class PettyCashDtlDataController : Controller
    {
        private readonly PettyCashDtlManager _pettyCashDtlManager;
        private readonly ConfigHelper _configHelper;
        public PettyCashDtlDataController(PettyCashDtlManager pettyCashDtlManager, ConfigHelper configHelper)
        {
            _configHelper = configHelper;
            _pettyCashDtlManager = pettyCashDtlManager;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<PettyCashDtl> Save([FromBody]PettyCashDtl model)
        {
            model.SetLoggedUserDetails(User);
            model.UserId = User.Identity.Id();
            await _pettyCashDtlManager.Save(model);
            return null;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<PettyCashDtl> SaveAll([FromBody]List<PettyCashDtl> model)
        {
            var dtl = model;
            foreach(PettyCashDtl pDtl in model)
            {
                pDtl.SetLoggedUserDetails(User);
                pDtl.UserId = User.Identity.Id();
                await _pettyCashDtlManager.Save(pDtl);
            }
            
            return null;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PettyCashDtl> Update([FromBody]PettyCashDtl model)
        {
            model.SetLoggedUserDetails(User);
            await _pettyCashDtlManager.Update(model);

            return model;
        }

        [Route("[action]")]
        public async Task<PagerContract<PettyCashDtl>> ListData([FromBody]DateRange data, string type)
        {
            var userid = User.Identity.Id();
            bool offlinestatus = Convert.ToBoolean(_configHelper.AppConfig.OfflineMode);
            return await _pettyCashDtlManager.ListPager(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate,userid, offlinestatus);
        }
        [Route("[action]")]
        public async Task<string> GetLatestupdatedDate([FromBody]DateRange data)
        {
            var userid = User.Identity.Id();
            string dateStr =  await _pettyCashDtlManager.getLatestupdatedDate(User.AccountId(), User.InstanceId(), userid);
            if (string.IsNullOrEmpty(dateStr))
            {
                dateStr = DateTime.Now.ToString();
            }
            DateTime dt = DateTime.Parse(dateStr);
            dateStr = dt.ToString("yyyy-MM-dd HH:mm:ss");
            return dateStr;
        }
     }
}
