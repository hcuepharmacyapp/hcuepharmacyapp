﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Accounts;
using Utilities.Helpers;
using HQue.Biz.Core.Accounts;
using HQue.Contract.Infrastructure.Inventory;
using System.Linq;

namespace HQue.WebCore.Controllers.Data.Accounts
{
    [Route("[controller]")]
    public class CustomerReceiptDataController : Controller
    {
        private readonly CustomerReceiptManager _customerReceiptManager;

        public CustomerReceiptDataController(CustomerReceiptManager customerReceiptManager)
        {
            _customerReceiptManager = customerReceiptManager;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Sales>> ListData(string customerId)
        {
            return await _customerReceiptManager.CustomerPaymentList(User.AccountId(), User.InstanceId(), customerId);
        }



        [HttpPost]
        [Route("[action]")]
        public async Task updateChequeStatus([FromBody]Sales model)
        {
            await _customerReceiptManager.updateChequeStatus(User.AccountId(), User.InstanceId(), model);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<List<Sales>> CustomerListDetails(string customerMobile, string customerName)
        {
            return await _customerReceiptManager.CustomerPaymentDetails(User.AccountId(), User.InstanceId(), customerMobile, customerName);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<CustomerPayment> GetCustomerBalance(string customerMobile, string customerName)
        {
            return await _customerReceiptManager.GetCustomerBalance(User.AccountId(), User.InstanceId(), customerMobile, customerName);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<List<Sales>> CustomerListData()
        {
            return await _customerReceiptManager.CustomerList(User.AccountId(), User.InstanceId());
        }
        [Route("[action]")]
        public Task<List<Sales>> searchCustomer(string keyword, string type)
        {
            return _customerReceiptManager.searchCustomer(User.AccountId(), User.InstanceId(), keyword, type);
        }


        [Route("[action]")]
        public Task<List<Sales>> GetCustomerCheque(string PatientId)
        {
            return _customerReceiptManager.GetCustomerCheque(User.AccountId(), User.InstanceId(), PatientId);
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<Sales> SaveCustomerPayment([FromBody]Sales data)
        {
            data.SetLoggedUserDetails(User);
            var result = data.CustomerPayments.Select(x =>
            {
                x.Remarks = data.Remarks;
                x.PaymentType = data.PaymentType;
                x.ChequeDate = data.ChequeDate;
                x.ChequeNo = data.ChequeNo;
                x.CardDate = data.CardDate;
                x.CardNo = data.CardNo;
                return x;
            }).ToList();
            data.CustomerPayments = result;
            return await _customerReceiptManager.SaveCustomerPayment(data);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<CustomerPayment> UpdateCustomerPayment([FromBody]CustomerPayment data)
        {
            data.SetLoggedUserDetails(User);
            return await _customerReceiptManager.UpdateCustomerPayment(data);
        }
    }
}
