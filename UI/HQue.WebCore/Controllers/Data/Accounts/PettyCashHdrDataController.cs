﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Accounts;
using Utilities.Helpers;
using HQue.Biz.Core.Accounts;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Inventory;
using System;

namespace HQue.WebCore.Controllers.Data.Accounts
{
    [Route("[controller]")]
    public class PettyCashHdrDataController: Controller
    {
        private readonly ConfigHelper _configHelper;
        private readonly PettyCashHdrManager _pettyCashHdrManager;
        public PettyCashHdrDataController(PettyCashHdrManager pettyCashHdrManager, ConfigHelper configHelper)
        {
            _configHelper = configHelper;
            _pettyCashHdrManager = pettyCashHdrManager;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<PettyCashHdr> Index([FromBody]PettyCashHdr model)
        {
            model.SetLoggedUserDetails(User);
            model.UserId = User.Identity.Id();
            await _pettyCashHdrManager.Save(model);
            return null;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<PettyCashHdr> Update([FromBody]PettyCashHdr model)
        {
            model.SetLoggedUserDetails(User);
            await _pettyCashHdrManager.Update(model);

            return model;
        }

        [Route("[action]")]
        public async Task<decimal> getSalesValue([FromBody] DateRange data)
        {
            Sales sale = new Sales();
            sale.SetLoggedUserDetails(User);
            bool offlinestatus = Convert.ToBoolean(_configHelper.AppConfig.OfflineMode);
            return await _pettyCashHdrManager.GetSalesValue(User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate, sale.CreatedBy, offlinestatus);
        }

        [Route("[action]")]
        public async Task<decimal> getVendorValue([FromBody] DateRange data)
        {
            Sales sale = new Sales();
            sale.SetLoggedUserDetails(User);
            bool offlinestatus = Convert.ToBoolean(_configHelper.AppConfig.OfflineMode);
            return await _pettyCashHdrManager.GetVendorValue(User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate, sale.CreatedBy, offlinestatus);
        }

        [Route("[action]")]
        public async Task<decimal> getCustomerValue([FromBody] DateRange data)
        {
            Sales sale = new Sales();
            sale.SetLoggedUserDetails(User);
            bool offlinestatus = Convert.ToBoolean(_configHelper.AppConfig.OfflineMode);
            return await _pettyCashHdrManager.GetCustomerValue(User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate, sale.CreatedBy, offlinestatus);
        }


        [Route("[action]")]
        public async Task<PagerContract<PettyCashHdr>> ListData([FromBody]DateRange data, string type)
        {
            var userId = User.Identity.Id();
            return await _pettyCashHdrManager.ListPager(type, User.AccountId(), User.InstanceId(), data.FromDate, data.ToDate, userId);
        }

        [Route("[action]")]
        public async Task<string> GetLatestupdatedDate([FromBody]DateRange data)
        {
            var userid = User.Identity.Id();
            string dateStr = await _pettyCashHdrManager.getLatestupdatedDate(User.AccountId(), User.InstanceId(), userid);
            if (string.IsNullOrEmpty(dateStr))
            {
                dateStr = DateTime.Now.AddDays(-1).ToString();
            }
            DateTime dt = DateTime.Parse(dateStr);
            dateStr =  dt.ToString("yyyy-MM-dd HH:mm:ss");
            return dateStr;
        }
    }
}
