﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Accounts;
using Utilities.Helpers;
using HQue.Biz.Core.Accounts;
using HQue.Biz.Core.Inventory;
using HQue.Contract.Infrastructure.Inventory;
using System;
using HQue.Biz.Core.UserAccount;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.WebCore.Controllers.Data.Accounts
{
    [Route("[controller]")]
    public class PaymentDataController : Controller
    {
        private readonly PaymentManager _paymentManager;
        private readonly SalesManager _salesManager;
        private readonly UserManager _userManager;
        public PaymentDataController(PaymentManager paymentManager, SalesManager salesManager,
            UserManager userManager)
        {
            _paymentManager = paymentManager;
            _salesManager = salesManager;
            _userManager = userManager;
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<List<Payment>> Index([FromBody]Payment payment1)
        {
            payment1.SetLoggedUserDetails(User);
            return await _paymentManager.GetVendorInvoice(payment1, User.InstanceId());
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<Payment> Save([FromBody]Payment data)
        {
            data.SetLoggedUserDetails(User);
            data.TransactionDate = CustomDateTime.IST;
            return await _paymentManager.Save(data);
        }
        [HttpPost]
        [Route("[action]")]
        //InstanceId parameter added by Poongodi for Delhi Requirement
        public async Task<List<Payment>> ReportListData(string sInstanceId)
        {
            // return await _paymentManager.PaymentReportList(User.AccountId(), User.InstanceId());
            return await _paymentManager.PaymentReportList(User.AccountId(), sInstanceId);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<CustomerPayment>> ListData(string customername, string mobile)
        {
            return await _paymentManager.CustomerPaymentList(User.AccountId(), User.InstanceId(), customername, mobile);
        }
        //[HttpGet]
        //[Route("[action]")]
        //public async Task<List<Sales>> CustomerListDetails(string customerMobile, string customerName)
        //{
        //    return await _paymentManager.CustomerPaymentDetails(User.AccountId(), User.InstanceId(), customerMobile, customerName);
        //}
        [HttpGet]
        [Route("[action]")]
        //Customer receipt shown by PatientId instead of patient mobile & name - Settu
        public async Task<Sales> CustomerListDetails(string PatientId)
        {
            return await _paymentManager.CustomerPaymentDetails(User.AccountId(), User.InstanceId(), PatientId);
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<List<CustomerPayment>> CustomerListData()
        {
            return await _paymentManager.CustomerList(User.AccountId(), User.InstanceId());
        }
        //history added by nandhini 20.9.17
        [Route("[action]")]
        [HttpPost]
        public async Task<List<Payment>> History([FromBody]Payment history)
        {   
            return await _paymentManager.GetVendorHistory(history, User.InstanceId());
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<Payment> Update([FromBody]Payment data)
        {
            data.SetLoggedUserDetails(User);
            data.TransactionDate = CustomDateTime.IST;
            return await _paymentManager.Update(data);
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<Payment> Delete([FromBody]Payment data)
        {
            data.SetLoggedUserDetails(User);
            data.TransactionDate = CustomDateTime.IST;
            data.Debit = 0;
            return await _paymentManager.Delete(data);
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<FinYearMaster> saveFinancialYear([FromBody]FinYearMaster data)
        {
            data.SetLoggedUserDetails(User);
            return await _paymentManager.saveFinancialYear(data);
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<FinYearMaster> saveFinyearStatus([FromBody]FinYearMaster data)
        {
            if (data.IsBillNumberReset != null)
            {
                var user = new HQueUser();
                user.UserId = data.EmailId;
                user.Password = data.Password;
                var result = await _userManager.ValidateUser(user);
                if (result.Item1)
                {
                    if (result.Item2.UserType == 2)
                    {
                        data.SetLoggedUserDetails(User);
                        return await _paymentManager.saveFinyearStatus(data);
                    }
                    else
                    {
                        data.SaveFailed = true;
                        data.ErrorMessage = "You are not authorized person to reset Financial Year.";
                    }
                }
                else
                {
                    data.SaveFailed = true;
                    data.ErrorMessage = "Invalid User Id or Password";
                }
                return data;
            }
            else
            {
                data.SaveFailed = true;
                return data;
            }
        }
        [Route("[action]")]
        public async Task<FinYearMaster> getFinyearAvailable()
        {
            return await _paymentManager.getFinyearAvailable(User.AccountId());
        }
    }
}
