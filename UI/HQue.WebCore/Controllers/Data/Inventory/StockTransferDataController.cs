﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Helpers;
using HQue.Biz.Core.Inventory;
using HQue.Contract.Infrastructure;
using System;
using HQue.Contract.Infrastructure.Misc;
using System.Net.NetworkInformation;
using HQue.Contract.Infrastructure.Master;

namespace HQue.WebCore.Controllers.Data.Inventory
{
    public class StockTransferDataController : Controller
    {
        private readonly StockTransferManager _stockTransferManager;
        private readonly ConfigHelper _configHelper;

        public StockTransferDataController(StockTransferManager stockTransferManager, ConfigHelper configHelper)
        {
            _configHelper = configHelper;
            _stockTransferManager = stockTransferManager;
        }

        [HttpPost]
        public async Task<StockTransfer> Index([FromBody]StockTransfer stockTransfer)
        {
            stockTransfer.InitializeExecutionQuery();
            stockTransfer.SetLoggedUserDetails(User);
            stockTransfer.TransferBy = User.Name(); //Added by Poongodi on 31/05/2016
                                                    /*Prefix Added by Poongodi on 14/06/2017*/
            stockTransfer.Instance.Name = User.InstanceName();
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
                 && _configHelper.AppConfig.OfflineMode == false)
            {
                stockTransfer.Prefix = "O";

            }
            if (Convert.ToString(User.GSTEnabled()).ToLower() == "true")
            {
                stockTransfer.TaxRefNo = 1;
            }
            else
            {
                stockTransfer.TaxRefNo = 0;
            }

            var result = await _stockTransferManager.Save(stockTransfer);
            if (stockTransfer.WriteExecutionQuery)
            {
                await _stockTransferManager.SaveBulkData(stockTransfer);
                if (stockTransfer.SaveFailed)
                {
                    return stockTransfer;

                }
            }
            //Commented by Settu for stock validation failed case
            //var transferNo = await _stockTransferManager.GetTransferNo(User.InstanceId());
            //return transferNo;
            return result;
        }

        public async Task<string> GetTransfereNo()
        {
            /*Prefix added by POongodi on 11/10/2017*/
            string sPrefix = "";
            if (Convert.ToString(User.OfflineInstalled()).ToLower() == "true"
                && _configHelper.AppConfig.OfflineMode == false)
            {
                sPrefix = "O";

            }
            var transferNo = await _stockTransferManager.GetTransferNo(User.InstanceId(), sPrefix);
            return transferNo;
        }

        public async Task<IEnumerable<Instance>> GetFilterInstance(bool forAccept)
        {
            return await _stockTransferManager.GetFilterInstance(forAccept);
        }

        [HttpPost]
        public async Task<PagerContract<StockTransfer>> AcceptList([FromBody]StockTransfer stockTransfer, bool forAccept)
        {
            stockTransfer.SetLoggedUserDetails(User);

            var result = await _stockTransferManager.ListPager(stockTransfer, forAccept);

            return result;
        }

        public async Task<Instance> TransferInstance()
        {
            return await _stockTransferManager.GetTransferInstance(User.InstanceId());
        }

        public async Task<List<Instance>> GetInstancesByAccountId()
        {
            return await _stockTransferManager.GetInstancesByAccountId(User.AccountId(), User.InstanceId());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <param name="stocktransferItem"></param>
        /// <returns></returns>
        public async Task<List<StockTransferItem>> AccetpReject(string id, int status, [FromBody]List<StockTransferItem> stocktransferItem = null)
        {

            if (status == 3)
            {
                stocktransferItem[0].IsAccept = true;
            }
            else
            {
                stocktransferItem[0].IsAccept = false;
            }
            stocktransferItem[0].Instance.Name = User.InstanceName();
            stocktransferItem[0].Instance.Id = User.InstanceId();
            if (stocktransferItem.Count > 0)
            {
                stocktransferItem[0].InitializeExecutionQuery();
                stocktransferItem.ForEach(x =>
                {
                    x.SetExecutionQuery(stocktransferItem[0].GetExecutionQuery(), stocktransferItem[0].WriteExecutionQuery);
                    x.ProductStock.SetExecutionQuery(stocktransferItem[0].GetExecutionQuery(), stocktransferItem[0].WriteExecutionQuery);
                });
            }
            var result = await _stockTransferManager.AcceptReject(id, status, stocktransferItem);
            if (stocktransferItem[0].WriteExecutionQuery)
            {
                await _stockTransferManager.SaveBulkDataAcceptReject(stocktransferItem);
                if (stocktransferItem[0].SaveFailed)
                {
                    return stocktransferItem;
                }
            }
            return stocktransferItem;
        }
        //Added by Settu for indent transfer with stock reducing
        public async Task<List<VendorOrder>> GetActiveIndentList(string OrderId, int Type, DateTime? fromDate = null, DateTime? toDate = null)
        {
            if (!fromDate.HasValue && !toDate.HasValue)
            {
                fromDate = null;
                toDate = null;
            }

            return await _stockTransferManager.GetActiveIndentList(OrderId, Type, fromDate, toDate);
        }

        public async Task<List<ProductStock>> getAcceptedTransferList(string TransferNo)
        {
            return await _stockTransferManager.getAcceptedTransferList(TransferNo);
        }


        [Route("[action]")]
        public async Task<List<StockTransferItem>> GetStockTransferOnline(string transferId)
        {
            var transfer = new ProductSearch
            {
                Id = transferId.ToString(),               
                InstanceId = User.InstanceId()

            };
            return await _stockTransferManager.GetStockTransferOnline(transfer);
        }
    }
}
