﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Settings;
using Utilities.Helpers;
using HQue.Biz.Core.Inventory;
using System.Net.NetworkInformation;

namespace HQue.WebCore.Controllers.Data.Inventory
{
    [Route("[controller]")]
    public class ProductStockDataController : Controller
    {
        private readonly ProductStockManager _productStockManager;
        private readonly VendorPurchaseManager _vendorPurchaseManager;
        private readonly ConfigHelper _configHelper;
        public ProductStockDataController(ProductStockManager productStockManager, ConfigHelper configHelper, VendorPurchaseManager vendorPurchaseManager)
        {
            _productStockManager = productStockManager;
            _configHelper = configHelper;
            _vendorPurchaseManager = vendorPurchaseManager;
        }
        [Route("[action]")]
        public Task<List<ProductStock>> InStockProductList(string productName, int showExpDateQty)
        {
            return _productStockManager.GetInGetInStockItemList(User.AccountId(), User.InstanceId(), productName, showExpDateQty);
        }
        [Route("[action]")]
        public Task<List<ProductStock>> InStockProductForSales(string productName)
        {
            return _productStockManager.GetInStockProductForSales(User.AccountId(), User.InstanceId(), productName);
        }
        [Route("[action]")]
        public Task<List<ProductStock>> GetAllStockProductList(string productName)
        {
            return _productStockManager.GetInAllStockItemList(productName, User.InstanceId());
        }
        //added for return in sales page
        [Route("[action]")]
        public Task<List<ProductStock>> GetInAllStockItemListForReturn(string productName)
        {
            return _productStockManager.GetInAllStockItemListForReturn(productName, User.InstanceId());
        }
        [Route("[action]")]
        public Task<List<ProductStock>> InStockProductListForInstance(string productName, string instanceId, int showExpDateQty)
        {
            return _productStockManager.GetInGetInStockItemList(User.AccountId(), instanceId, productName, showExpDateQty);
        }
        [Route("[action]")]
        public Task<List<ProductStock>> AllStockProductList(string productName, string instanceid)
        {
            return _productStockManager.GetAllStockItemList(productName, instanceid);
        }
        [Route("[action]")]
        public Task<List<ProductStock>> GetAllProductsinProductStock(string productName)
        {
            return _productStockManager.GetAllProductsinProductStock(productName, User.InstanceId());
        }
        [Route("[action]")]
        public Task<List<ProductStock>> GetAllProductsinProductStockList(string instanceid, string productName = null)
        {
            return _productStockManager.GetAllProductsinProductStock(productName, instanceid);//User.InstanceId()
        }
        [Route("[action]/batch")]
        public Task<List<ProductStock>> InStockProduct(string productId, int showExpDateQty = 0, bool blnQtyType = true)
        {
            var product = new Product
            {
                Id = productId
            };
            product.SetLoggedUserDetails(User);
            return _productStockManager.GetInGetInStockItem(product, showExpDateQty, blnQtyType);
        }
        [Route("[action]/productBatchTransfer")]
        public Task<List<ProductStock>> InStockProductTransfer(string productId, int showExpDateQty, bool blnQtyType = true)
        {
            var product = new Product
            {
                Id = productId
            };
            product.SetLoggedUserDetails(User);
            return _productStockManager.GetInGetInStockItem(product, showExpDateQty, blnQtyType);
        }


        [Route("[action]")]
        public async Task<BranchWiseStock> InBranchWiseStock(string productid, string productname)
        {
            var product = new ProductSearch
            {
                Id = productid.ToString(),
                Name = productname,
                AccountId = User.AccountId(),
                InstanceId = User.InstanceId()

            };
            //Online and offline validation added by Poongodi on 06/06/2017
            bool bInternet = NetworkInterface.GetIsNetworkAvailable();
            if (_configHelper.AppConfig.OfflineMode)
            {
                if (bInternet)
                {

                    return await _productStockManager.OnlineBranchWiseStock(product);

                }
                else
                {
                    var branchWiseStock = new BranchWiseStock();
                    branchWiseStock.ProductStockList = null;
                    branchWiseStock.VendorPurchaseItemList = null;
                    return branchWiseStock;
                }

            }
            else
            {
                bool? bLowPrice = false;
                var settings = await _vendorPurchaseManager.getTaxtypedata(User.AccountId(), User.InstanceId());
                if (settings != null && settings.IsSortByLowestPrice != null)
                    bLowPrice = settings.IsSortByLowestPrice;

                return await _productStockManager.GetBranchWiseStockItem(product, bLowPrice);
            }
        }

        [Route("[action]/batch")]
        public Task<List<ProductStock>> getBathcDetails(string productId)
        {
            var product = new Product { Id = productId };
            product.SetLoggedUserDetails(User);
            return _productStockManager.getBathcDetails(product);
        }
        [HttpGet]
        [Route("[action]")]
        public Task<Product> GetProductDetails(string productId)
        {
            var product = new Product
            {
                Id = productId
            };
            product.SetLoggedUserDetails(User);
            return _productStockManager.GetProductDetails(product);
        }
        [Route("[action]")]
        public async Task<PagerContractInventory<ProductStock>> ListData([FromBody]Product model)
        {
            model.SetLoggedUserDetails(User);
            return await _productStockManager.ListPager(model); //, nGetcount
        }
        [Route("[action]")]
        public async Task<PagerContract<Product>> ProductListData([FromBody]Product model)
        {
            model.SetLoggedUserDetails(User);
            return await _productStockManager.ProductListPager(model);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> GetInventoryData([FromBody]Product model)
        {
            model.SetLoggedUserDetails(User);
            return await _productStockManager.GetAllProductStock(model);
        }
        [Route("[action]")]
        public async Task<bool> UpdateProductStock([FromBody]ProductStock model)
        {
            model.SetLoggedUserDetails(User);
            await _productStockManager.UpdateStock(model);
            return true;
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> SaveNewStock([FromBody]List<ProductStock> pstock)
        {
            ProductStock ps = new ProductStock();
            ps.SetLoggedUserDetails(User);
            return await _productStockManager.saveNewStock(pstock, User.AccountId(), User.InstanceId(), ps.CreatedBy);
        }
        // Added Gavaskar 16-02-2017 Start 
        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> inventoryDataUpdate([FromBody]List<ProductStock> pstock)
        {
            ProductStock ps = new ProductStock();
            ps.SetLoggedUserDetails(User);
            return await _productStockManager.inventoryDataUpdate(pstock, User.AccountId(), User.InstanceId(), ps.CreatedBy);
        }
        // Added Gavaskar 16-02-2017 End 
        [HttpPost]
        [Route("[action]")]
        public async Task<List<SelfConsumption>> SaveStockConsumption([FromBody]List<SelfConsumption> pstock)
        {
            SelfConsumption sf = new SelfConsumption();
            sf.SetLoggedUserDetails(User);
            return await _productStockManager.saveStockConsumption(pstock, sf.AccountId, sf.InstanceId, sf.CreatedBy);
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<List<ProductStock>> GetNewOpenedInventory(string prodId, string InvoiceNo)
        {
            ProductStock ps = new ProductStock();
            ps.SetLoggedUserDetails(User);
            return await _productStockManager.getNewOpenedInventory(ps, prodId, InvoiceNo);
        }
        [Route("[action]")]
        public async Task<InventorySettings> InventorySetting([FromBody]InventorySettings model)
        {
            model.SetLoggedUserDetails(User);
            return await _productStockManager.SaveSetting(model);
        }
        [Route("[action]")]
        public async Task<InventorySettings> EditInventorySetting()
        {
            var instantceId = User.InstanceId();
            InventorySettings inventorySettings = new InventorySettings();
            inventorySettings.InstanceId = instantceId;
            return await _productStockManager.EditInventorySetting(inventorySettings);
        }
        [Route("[action]")]
        public async Task<ProductStock> updateProductInventory([FromBody]ProductStock model, string filtertype)
        {
            model.SetLoggedUserDetails(User);
            return await _productStockManager.UpdateProductInventory(model, filtertype);
        }
        [Route("[action]")]
        public async Task<ProductStock> updateTransferProductStockInventory([FromBody]ProductStock model)
        {
            model.SetLoggedUserDetails(User);
            return await _productStockManager.updateTransferProductStockInventory(model);
        }
        [Route("[action]")]
        public async Task<bool> updateTransferProductInventory([FromBody]ProductStock model)
        {
            model.SetLoggedUserDetails(User);
            return await _productStockManager.updateTransferProductInventory(model);
        }
        [Route("[action]")]
        public async Task<ProductStock> updateSingleProductStock([FromBody]ProductStock model)
        {
            model.SetLoggedUserDetails(User);
            return await _productStockManager.UpdateSingleProductStock(model);
        }
        [HttpGet]
        [Route("[action]")]
        public Task<List<ProductStock>> GetProductAllBatch(string productId)
        {
            var product = new Product
            {
                Id = productId
            };
            product.SetLoggedUserDetails(User);
            return _productStockManager.getAllProductBatch(product);
        }
        [HttpGet]
        [Route("[action]")]
        public Task<List<ProductStock>> GetProductBatchForReturn(string productId)
        {
            var product = new Product
            {
                Id = productId
            };
            product.SetLoggedUserDetails(User);
            return _productStockManager.getProductBatchForReturn(product);
        }
        [HttpGet]
        [Route("[action]")]
        public Task<List<ProductStock>> GetActiveProductBatch(string productId)
        {
            var product = new Product
            {
                Id = productId
            };
            product.SetLoggedUserDetails(User);
            return _productStockManager.getActiveProductBatch(product);
        }
        [Route("[action]")]
        public Task<List<Product>> TempStockProductList(string productName, string instanceid)
        {
            return _productStockManager.TempStockProductList(productName, instanceid, User.AccountId());
        }
        [Route("[action]")]
        public Task<List<Product>> GetEancodeExistData(string eancode)
        {
            return _productStockManager.GetEancodeExistData(eancode, User.InstanceId(), User.AccountId());
        }

        //Added by Settu on 04/07/17 for physical stock verification popup implementation
        [Route("[action]")]
        [HttpPost]
        public Task<List<PhysicalStockHistory>> getPhysicalStockProducts()
        {
            return _productStockManager.getPhysicalStockProducts(User.AccountId(), User.InstanceId());
        }

        [Route("[action]")]
        public async Task<bool> updatePhysicalStockHistory([FromBody]List<PhysicalStockHistory> list)
        {
            foreach (var model in list)
            {
                model.SetLoggedUserDetails(User);
            }
            return await _productStockManager.updatePhysicalStockHistory(list);
        }

        [Route("[action]")]
        public async Task<InventorySettings> UpdatePhysicalStockPopupSettings([FromBody]InventorySettings model)
        {
            model.SetLoggedUserDetails(User);
            return await _productStockManager.UpdatePhysicalStockPopupSettings(model);
        }

        [Route("[action]")]
        public async Task<bool> updateBarcodeProfile([FromBody]BarcodeProfile data)
        {
            data.SetLoggedUserDetails(User);
            return await _productStockManager.updateBarcodeProfile(data);
        }

        [Route("[action]")]
        public async Task<List<BarcodeProfile>> getBarcodeProfileList(string name)
        {
            return await _productStockManager.getBarcodeProfileList(name);
        }

        [Route("[action]")]
        public async Task<bool> updateBarcodePrnDesign([FromBody]BarcodePrnDesign data)
        {
            data.SetLoggedUserDetails(User);
            return await _productStockManager.updateBarcodePrnDesign(data);
        }

        [Route("[action]")]
        public async Task<List<BarcodePrnDesign>> getBarcodePrnDesignList(string name)
        {
            return await _productStockManager.getBarcodePrnDesignList(name);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> generateBarcode([FromBody]List<ProductStock> productStock, string BarcodeProfileId)
        {
            return await _productStockManager.generateBarcode(productStock, BarcodeProfileId);
        }

        // Added by Gavaskar Kind Of Product Master Settings Start 14-12-2017
        [Route("[action]")]
        public async Task<IEnumerable<KindProductMaster>> GetKindOfProductList()
        {
            return await _productStockManager.GetKindOfProductList(User.AccountId(), "all");
        }
        // Added by Gavaskar Kind Of Product Master Settings End 14-12-2017
    }
}
