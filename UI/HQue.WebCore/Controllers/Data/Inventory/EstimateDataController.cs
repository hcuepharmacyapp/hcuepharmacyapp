﻿using System.Threading.Tasks;
using HQue.Biz.Core.Inventory.Estimates;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Estimates;
using Microsoft.AspNetCore.Mvc;

namespace HQue.WebCore.Controllers.Data.Inventory
{
    [Route("[controller]")]
    public class EstimateDataController : Controller
    {
        private readonly EstimateManager _estimateManager;

        public EstimateDataController(EstimateManager estimateManager)
        {
            _estimateManager = estimateManager;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<Estimate> Create([FromBody]Estimate data)
        {
            return await _estimateManager.Save(data);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<Estimate> Details(string id)
        {
            return await _estimateManager.GetEstimateDetails(id);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<PagerContract<Estimate>> List([FromBody]Estimate data)
        {
            return await _estimateManager.ListPager(data);
        }
    }
}
