﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using HQue.Contract.Infrastructure.Settings;
using HQue.Biz.Core.Settings;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Base;
using Utilities.Enum;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Infrastructure.Master;
using HQue.Biz.Core.Master;
using HQue.Contract.Infrastructure.Inventory;
using System.Net.NetworkInformation;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.WebCore.Controllers.Settings
{
    [Route("[controller]")]
    public class ToolController : BaseController
    {
        private readonly IHostingEnvironment _environment;
        private readonly ToolManager _toolManager;
        private readonly ConfigHelper _configHelper;
        private readonly ProductManager _productManager;
        public ToolController(ToolManager toolManager, IHostingEnvironment environment, ProductManager productManager, ConfigHelper configHelper) : base(configHelper)
        {
            _environment = environment;
            _toolManager = toolManager;
            _productManager = productManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<Tools> Index([FromForm]Tools tool, IFormFile file)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(file)))
            {
                var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
                var fileName = Guid.NewGuid().ToString() + '_' + parsedContentDisposition.FileName.Trim('"');
                var filePath = Path.Combine(_environment.WebRootPath, "uploads\\tool", fileName);
                //await file.SaveAsAsync(filePath);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
                //await file.CopyToAsync(new FileStream(filePath, FileMode.Create));

                if (!string.IsNullOrEmpty(fileName))
                {
                    tool.Image = fileName;
                }
            }
            tool.SetLoggedUserDetails(User);
            var result = await _toolManager.Save(tool);

            return result;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Tools>> ListData()
        {
            return await _toolManager.List();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Tools> updateStatus([FromBody]Tools model)
        {
            model.SetLoggedUserDetails(User);
            await _toolManager.updateStatus(model);

            return model;
        }

        [Route("[action]")]
        public IActionResult Faq()
        {
            return View();
        }

        [Route("[action]")]
        public IActionResult NegativeStock()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Tools>> NegativeStockList()
        {
            return await _toolManager.NegativeStockList();
        }

        [Route("[action]")]
        public IActionResult IssueList()
        {
            return View();
        }

        [Route("[action]")]
        public async Task<PagerContract<Requirements>> issueListData([FromBody]Requirements model, int type, string status)
        {
            model.SetLoggedUserDetails(User);
            return await _toolManager.ListPager(model, type, status);
        }

        [Route("[action]")]
        public async Task<bool> updateIssueStatus([FromBody]Requirements model)
        {
            model.SetLoggedUserDetails(User);
            await _toolManager.updateStatus(model);
            return true;
        }

        [Route("[action]")]
        public IActionResult BranchList()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Tools>> getBranchList(string accountId, string branchId, string name)
        {
            return await _toolManager.getBranchList(accountId, branchId, name);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Tools> updateOfflineStatus([FromBody]Tools model, bool status)
        {
            Instance instance = new Instance();
            instance.Id = model.InstanceId;
            instance.Name = model.InstanceName;
            model.OfflineStatus = status;
            instance.SetLoggedUserDetails(User);
            instance.OfflineStatus = status;
            instance = await _toolManager.updateOfflineStatus(instance, User.UserId());
            model.UpdatedBy = User.UserId();
            model.UpdatedAt = instance.UpdatedAt;
            return model;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Tools> updateOfflineVersionNo([FromBody]Tools model)
        {
            var offlineVersionNo = ConfigHelper.AppConfig.Version.ToString();
            Instance instance = new Instance();
            instance.Id = model.InstanceId;
            instance.OfflineVersionNo = int.Parse(offlineVersionNo);
            instance.SetLoggedUserDetails(User);
            instance = await _toolManager.updateOfflineVersionNo(instance, User.UserId());
            model.UpdatedBy = User.UserId();
            model.UpdatedAt = instance.UpdatedAt;
            model.OfflineVersionNo = instance.OfflineVersionNo;
            return model;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Account>> getAccountList()
        {
            return await _toolManager.getAccountList();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> getBranchListData()
        {
            return await _toolManager.getBranchListData();
        }

        [Route("[action]")]
        public IActionResult Settings(string aid)
        {
            if (User.IsHQueToolUser())
            {
                ViewBag.AccountId = aid;
            }
            else
            {
                return RedirectToAction("Index", "Sales");
            }
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<dynamic> GetInstanceSettingsList(string accountId, string branchId)
        {
            if (User.IsHQueToolUser())
            {
                return await _toolManager.GetInstanceSettingsList(accountId, branchId);
            }
            return RedirectToAction("Index", "Sales");
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> UpdateHcueProductMasterSettings([FromBody]List<Instance> model)
        {
            model[0].CreatedBy = User.Identity.Id();
            model[0].UpdatedBy = User.Identity.Id();
            return await _toolManager.UpdateHcueProductMasterSettings(model);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> updateHcuePurchasePriceSettings([FromBody]List<Instance> model)
        {
            model[0].CreatedBy = User.Identity.Id();
            model[0].UpdatedBy = User.Identity.Id();
            return await _toolManager.updateHcuePurchasePriceSettings(model);
        }
        //added by nandhini
        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> updateSaleBillcsv([FromBody]List<Instance> model)
        {
            model[0].CreatedBy = User.Identity.Id();
            model[0].UpdatedBy = User.Identity.Id();
            return await _toolManager.updateSaleBillcsv(model);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<Instance>> updateSalesPriceSettings([FromBody]List<Instance> model, String SalesPrice)
        {
            model[0].CreatedBy = User.Identity.Id();
            model[0].UpdatedBy = User.Identity.Id();
            return await _toolManager.updateSalesPriceSettings(model, SalesPrice);
        }
        //added by nandhini
        [Route("[action]")]
        public IActionResult LiveList()
        {
            return View();
        }
        //Added by nandhini on 06-11-17
        [HttpPost]
        [Route("[action]")]
        public async Task<int> getSalesPriceSettings(string insId)
        {
            return await _toolManager.getSalesPriceSetting(insId);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<int> getSaleBillcsv(string insId)
        {
            return await _toolManager.getSaleBillcsv(insId);
        }
        //Added by Sarubala on 15-11-17
        [Route("[action]")]
        public async Task<Setting> getexportToCsvSetting()
        {
            return await _toolManager.getexportToCsvSetting(User.AccountId(), User.InstanceId());
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<Tools>> salesReport(string data)
        {
            return await _toolManager.salesReport(data);

        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<Tools>> offlineReport()
        {
            return await _toolManager.offlineReport();

        }
        //end

        //Added by Sarubala on 25-11-17
        [HttpPost]
        [Route("[action]")]
        public async Task<Instance> getSmsCount(string accountId, string instanceId)
        {
            return await _toolManager.getSmsCount(accountId, instanceId);
        }

        //Added by Sarubala on 27-11-17
        [HttpPost]
        [Route("[action]")]
        public async Task<List<SmsConfiguration>> GetSmsConfiguration(string accountId, string instanceId)
        {
            return await _toolManager.GetSmsConfiguration(accountId, instanceId);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<SmsConfiguration> SaveSmsConfiguration([FromBody]SmsConfiguration data)
        {
            string accId = data.AccountId;
            string insId = data.InstanceId;
            data.SetLoggedUserDetails(User);
            data.AccountId = accId;
            data.InstanceId = insId;
            return await _toolManager.SaveSmsConfiguration(data);
        }

        // Added by Gavaskar Stock Ledger Edit 16-12-2017 Start 
        [Route("[action]")]
        public IActionResult StockLedgerEdit()
        {
            return View();
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<PagerContract<StockLedgerUpdateHistory>> StockLedgerEditList(string AccountId, string InstanceId, string ProductName = null)
        {
            //model.SetLoggedUserDetails(User);
            return await _toolManager.StockLedgerEditList(AccountId, InstanceId, ProductName);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<StockLedgerUpdateHistory>> BulkStockLedgerUpdate([FromBody]List<StockLedgerUpdateHistory> model, string AccountId, string InstanceId)
        {
            //StockLedgerUpdateHistory userData = new StockLedgerUpdateHistory();
            //model.SetLoggedUserDetails(User);

            model[0].CreatedBy = User.Identity.Id();
            model[0].UpdatedBy = User.Identity.Id();
            model[0].AccountId = AccountId;
            model[0].InstanceId = InstanceId;
            bool internet = NetworkInterface.GetIsNetworkAvailable();
            await _toolManager.BulkStockLedgerUpdate(model, internet);
            return model;
            //return null;
        }
        [Route("[action]")]
        public Task<List<Product>> InstanceProduct(string InstanceId, string productName = null)
        {
            Product model = new Product();
            model.SetLoggedUserDetails(User);
            return _productManager.InstanceList(productName, InstanceId);
        }
        [HttpPost]
        [Route("[action]")]
        public async Task<dynamic> BulkUpdateProductStockOfflineToOnline(string AccountId, string InstanceId)
        {
            bool internet = NetworkInterface.GetIsNetworkAvailable();
            if (internet == true)
            {
                return await _toolManager.BulkUpdateProductStockOfflineToOnline(AccountId, InstanceId);
            }
            else
            {
                return internet;
            }

        }
        [HttpPost]
        [Route("[action]")]
        public async Task<List<ProductStock>> BulkUpdateNegativeStockOfflineToOnline(string AccountId, string InstanceId)
        {
            bool internet = NetworkInterface.GetIsNetworkAvailable();
            if (internet == true)
            {
                return await _toolManager.BulkUpdateNegativeStockOfflineToOnline(AccountId, InstanceId);
            }
            else
            {
                List<ProductStock> psList = new List<ProductStock>();
                ProductStock ps = new ProductStock();
                ps.StatusText = "Internet Not Available";
                psList.Add(ps);
                return psList;
            }
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<List<ProductStock>> GetNagativeStockList(string AccountId, string InstanceId)
        {
            return await _toolManager.GetNagativeStockList(AccountId, InstanceId);
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<string> GetReverseSyncOfflineToOnlineCompare(string AccountId, string InstanceId)
        {
            bool internet = NetworkInterface.GetIsNetworkAvailable();
            if (_configHelper.AppConfig.OfflineMode == false)
            {
                if (internet == true)
                {
                    return "Online";
                }
                else
                {
                    return "Internet Not Available";
                }
            }
            else
            {
                if (internet == true)
                {
                    return await _toolManager.GetReverseSyncOfflineToOnlineCompare(AccountId, InstanceId);
                }
                else
                {
                    return "Internet Not Available";
                }
            }
           
           
        }
        // Added by Gavaskar Stock Ledger Edit 16-12-2017 End 
    }
}
