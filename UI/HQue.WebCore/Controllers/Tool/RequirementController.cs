﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using HQue.Contract.Infrastructure.Settings;
using HQue.Biz.Core.Settings;

namespace HQue.WebCore.Controllers.Settings
{
    [Route("[controller]")]
    public class RequirementController : BaseController
    {
        private readonly IHostingEnvironment _environment;
        private readonly ToolManager _toolManager;
        private readonly ConfigHelper _configHelper;
        public RequirementController(ToolManager toolManager, IHostingEnvironment environment, ConfigHelper configHelper) : base(configHelper)
        {
            _environment = environment;
            _toolManager = toolManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<Requirements> getInstanceName()
        {
            Requirements rq = new Requirements();
            rq.InstanceName = User.InstanceName();
            rq.SetLoggedUserDetails(User);
            rq.RequirementNo = await _toolManager.GetRequirementNo(rq.InstanceId);           
            return rq;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<List<Requirements>> saveRequirement([FromBody]List<Requirements> reqList)
        {
            reqList.First().SetLoggedUserDetails(User);
            return await _toolManager.saveRequirement(reqList, User.AccountId(), User.InstanceId(), reqList.First().CreatedBy);            
        }



    }
}