﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Setup;
using HQue.Biz.Core.Setup;
using Utilities.Helpers;

namespace HQue.WebCore.Controllers.Setup
{
    [Route("[controller]")]
    public class InstanceSetupController : BaseController
    {
        private readonly InstanceSetupManager _instanceSetupManager;
        private readonly ConfigHelper _configHelper;

        public InstanceSetupController(InstanceSetupManager instanceSetupManager, ConfigHelper configHelper) : base(configHelper)
        {
            _instanceSetupManager = instanceSetupManager;
            _configHelper = configHelper;
        }

        [Route("[action]")]
        public async Task<ViewResult> Index()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            ViewBag.InstanceCount = Convert.ToInt32(await _instanceSetupManager.GetInstanceCount(User.AccountId()));
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Index([FromBody]Instance model)
        {
            // if (!ModelState.IsValid)
            //     return View(model);

            model.SetLoggedUserDetails(User);

            var id = model.InstanceId;

            model = await _instanceSetupManager.Save(model);
            return View(model);
        }

        [Route("[action]")]
        public async Task<IActionResult> List()
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            ViewBag.InstanceCount = Convert.ToInt32(await _instanceSetupManager.GetInstanceCount(User.AccountId()));
            ViewBag.TotalInstanceCount = Convert.ToInt32(await _instanceSetupManager.GetTotalInstanceCount(User.AccountId()));
            
            return View();
        }

        [Route("[action]")]
        public async Task<PagerContract<Instance>> ListData([FromBody]Instance model)
        {
            model.SetLoggedUserDetails(User);
            return await _instanceSetupManager.ListPager(model);
        }


        [Route("[action]")]
        public IActionResult Update(string id)
        {
            var testOffline = User.OfflineStatus();
            if (_configHelper.AppConfig.OfflineMode == false && testOffline == "True")
            {
                Response.Redirect("/Dashboard");
            }
            if (id != null)
            {
                ViewBag.Id = id;
            }
            return View();
        }

        [Route("[action]")]
        public async Task<Instance> GetById(string instanceId)
        {
            return await _instanceSetupManager.GetById(instanceId);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Instance> UpdateInstance([FromBody]Instance model)
        {
            model.SetLoggedUserDetails(User);
            await _instanceSetupManager.UpdateInstance(model);

            return model;
        }
    }
}
