﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HQue.Contract.Infrastructure.Setup;
using HQue.Biz.Core.Setup;
using Utilities.Helpers;
using HQue.Contract.Infrastructure;
using HQue.Contract.Infrastructure.Settings;
using System.Collections.Generic;
using HQue.Contract.Infrastructure.Inventory;

namespace HQue.WebCore.Controllers.Setup
{
    [Route("[controller]")]
    public class AccountSetupController : BaseController
    {
        private readonly AccountSetupManager _accountSetupManager;

        public AccountSetupController(AccountSetupManager accountSetupManager, ConfigHelper configHelper) : base(configHelper)
        {
            _accountSetupManager = accountSetupManager;
        }

        [Route("[action]")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<bool> Index([FromBody]Account model)
        {
            model.SetLoggedUserDetails(User);

            await _accountSetupManager.Save(model);
            return true;
        }

        [Route("[action]")]
        public IActionResult List()
        {
            if (!User.IsAdmin())
            {
                Response.Redirect("/Dashboard");
            }
            return View();                
        }

        [Route("[action]")]
        public async Task<PagerContract<Account>> ListData([FromBody] Account model)
        {
            return await _accountSetupManager.ListPager(model);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<bool> UpdateGroup([FromBody] Account model)
        {
            model.SetLoggedUserDetails(User);
            await _accountSetupManager.UpdateGroupDetails(model);
            return true;
        }


        [HttpGet]
        [Route("[action]")]
        public async Task<Account> EditGroup1(string id)
        {
            return await _accountSetupManager.GetGroupDetails(id);
        }

        [Route("[action]")]
        public IActionResult EditGroup(string id)
        {
            if (id != null)
            {
                ViewBag.Id = id;
            }
            return View();
        }

        [Route("[action]")]
        public IActionResult CustomizedFeatures()
        {
            if (!User.IsAdmin())
            {
                return View("Rights");
            }
            else
            {
                return View();
            }
        }

        [Route("[action]")]
        public async Task<List<CustomSettingsDetail>> GetCustomSettings(string AccountId, string InstanceId)
        {
            return await _accountSetupManager.GetCustomSettings(AccountId, InstanceId);
        }

        [Route("[action]")]
        public async Task<List<CustomSettingsDetail>> SaveCustomSettings([FromBody]List<CustomSettingsDetail> list)
        {
            return await _accountSetupManager.SaveCustomSettings(list);
        }

        //Added by Sarubala on 14-12-2018
        [Route("[action]")]
        public async Task<List<DomainValues>> GetInstanceType()
        {
            return await _accountSetupManager.GetInstanceType();
        }

    }
}
