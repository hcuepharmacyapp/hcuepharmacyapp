﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Setup;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using HQue.Biz.Core.Setup;
using HQue.Contract.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Security.Claims;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.UserAccount;
using System.Text;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Biz.Core.Master;
using HQue.Biz.Core.Inventory;
using HQue.DataAccess.Master;
using System.Globalization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace HQue.WebCore.Controllers.Setup
{
    [Route("[controller]")]
    public class RegistrationController : BaseController
    {
        private readonly IHostingEnvironment _environment;
        private readonly RegistrationManager _registrationManager;
        private readonly AccountSetupManager _accountSetupManager;
        private readonly ProductManager _productManager;
        private readonly ProductStockManager _productStockManager;
        private readonly ConfigHelper _configHelper;
        private readonly ProductDataAccess _productDataAccess;
        //public RegistrationController(SalesManager salesManager, IHostingEnvironment environment, ConfigHelper configHelper) : base(configHelper)
        public RegistrationController(IHostingEnvironment environment, ConfigHelper configHelper, RegistrationManager registrationManager, AccountSetupManager accountSetupManager, ProductManager productManager, ProductStockManager productStockManager, ProductDataAccess productDataAccess) : base(configHelper)
        {
            _environment = environment;
            _configHelper = configHelper;
            _registrationManager = registrationManager;
            _accountSetupManager = accountSetupManager;
            _productManager = productManager;
            _productStockManager = productStockManager;
            _productDataAccess = productDataAccess;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }


        [Route("[action]")]
        public IActionResult Branch(string instanceId)
        {
            if (instanceId != null)
            {
                ViewBag.instanceId = instanceId;
            }
            if (User.AccountId() == "c503ca6e-f418-4807-a847-b6886378cf0b" || User.InstanceId() == "d9e1f62d-e4f3-4dc1-95d2-0ecfbb62d1b6")
            {
                //For NagerCOil Pharmacy
                ViewBag.indentOrder = 1;
            }
            else
            {
                ViewBag.indentOrder = 0;
            }
            return View();
        }

        [Route("[action]")]
        public async Task<Registration> EditBranch(string instanceId)
        {
            return await _registrationManager.EditBranch(instanceId);
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<Account> GetAccountDetails()
        {
            return await _accountSetupManager.GetGroupDetails(User.AccountId());
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<Registration> SaveBranch([FromBody]Registration model)
        {
            var user = User.Identity;

            model.instance.SetLoggedUserDetails(User);

            model.instance.Mobile = model.instance.ContactMobile;

            //model.instance.Address = $"{model.instance.BuildingName}, {model.instance.StreetName}";
            //if (!String.IsNullOrEmpty(model.instance.Address))
            //    model.instance.Address = model.instance.Address.Replace(", ", "");

            var address = "";
            if (!String.IsNullOrEmpty(model.instance.BuildingName))
            {
                address += model.instance.BuildingName + ", ";
            }
            if (!String.IsNullOrEmpty(model.instance.StreetName))
            {
                address += model.instance.StreetName + ", ";
            }
            if (address.Length >= 2)
            {
                address = address.Substring(0, address.Length - 2);
            }

            model.instance.Address = address;
            

            if (model.instance.Id == "" || model.instance.Id == null)
            {
                //model.instance.Phone = model.instance.ContactMobile;
                model.instance.SetLoggedUserDetails(User);
               
                model.instance.Timings.Select(timing => { timing.SetLoggedUserDetails(User); timing.AccountId = model.instance.AccountId; timing.InstanceId = model.instance.Id; return timing; }).ToList();

                var result = await _registrationManager.CreateBranch(model);

                var product = await CreateTestProduct(result.instance.Id);
                await SaveTestProductStock(result.instance.Id, product);
                var ps = await GetTestProductStock(product, result.instance.Id);
                await saveTestStockConsumption(ps.FirstOrDefault(), result.instance.Id);

                return result;
                //return await _registrationManager.CreateBranch(model);
            }
            else
            {
                //*Added by Poongodi on 04/07/2017*/
                if (_configHelper.AppConfig.OfflineMode == true)
                {
                    model.instance.OfflineStatus = false;
                }
                model.instance.Timings.Select(timing => { timing.SetLoggedUserDetails(User); timing.AccountId = model.instance.AccountId; timing.InstanceId = model.instance.Id; return timing; }).ToList();

                return await _registrationManager.UpdateBranch(model);
            }
        }

        public async Task<Product> CreateTestProduct(string instanceid)
        {
            Product product = new Product
            {
                Name = "TestProduct",
                Manufacturer = "TestManufacture",
                Type = "",
                Schedule = "H",
                Category = "",
                GenericName = "TestGeneric",
                PackageSize = 1,
                Igst = 0,
                Cgst = 0,
                Sgst = 0,
                GstTotal = 0
            };
            product.SetLoggedUserDetails(User);
            product.InstanceId = instanceid;

            var isProductExist = await _productDataAccess.CheckUniqueProduct(product);

            if(Convert.ToBoolean(isProductExist) == false)
            {
                return await _productDataAccess.GetImportProductById(product);
            }
            else
            {
                return await _productManager.Save(product);
            }
            
        }

        public async Task SaveTestProductStock(string instanceid, Product product)
        {
            List<ProductStock> PSList = new List<ProductStock>();
           
            ProductStock ps = new ProductStock
            {
                VAT = 0,
                GstTotal = 0,
                PackageSize = 1,
                Igst = 0,
                Cgst = 0,
                Sgst = 0,
                BatchNo = "TP1",                
                ExpireDate = DateTime.UtcNow.AddYears(2),
                PackagePurchasePrice = 1,
                packageQty = 1,
                Stock = 1,
                NewStockQty = 1,
                PurchasePrice = 1,
                SellingPrice = 1,
                MRP = 1,
                Product = product
            };

            ps.InstanceId = instanceid;
            ps.SetLoggedUserDetails(User);
            PSList.Add(ps);
            await _productStockManager.saveNewStock(PSList, User.AccountId(), instanceid, ps.CreatedBy);
        }

        public async Task<List<ProductStock>> GetTestProductStock(Product product, string instanceid)
        {
            product.SetLoggedUserDetails(User);
            product.InstanceId = instanceid;
            return await _productStockManager.GetAllProductStock(product);
        }

        public async Task saveTestStockConsumption(ProductStock ps, string instanceid)
        {
            ps.Stock = ps.Stock - 1;
            ps.SetLoggedUserDetails(User);
            ps.InstanceId = instanceid;
            ps.transQuantity = 1;

            List<SelfConsumption> PSSelfConsumptionList = new List<SelfConsumption>();            
            SelfConsumption sconsume = new SelfConsumption
            {
                ProductStockId = ps.Id,
                Consumption = 1,
                ConsumptionNotes = "Test Entry",
                ProductStock = ps
            };
            sconsume.SetLoggedUserDetails(User);
            sconsume.InstanceId = instanceid;
            PSSelfConsumptionList.Add(sconsume);

            await _productStockManager.saveStockConsumption(PSSelfConsumptionList, sconsume.AccountId, instanceid, sconsume.CreatedBy);
        }

        // GET: /<controller>/
        [Route("[action]")]
        public IActionResult Index(string accountId, string instanceId)
        {
            if (accountId != null && instanceId != null)
            {
                ViewBag.accountId = accountId;
                ViewBag.instanceId = instanceId;
            }
            return View();
        }

        

        //[Route("[action]")]
        //public int[] GetPermissions()
        //{
        //    List<int> permissions = new List<int>();

        //    if (User.HasPermission(UserAccess.PharmacyInfo)) permissions.Add(UserAccess.PharmacyInfo);
        //    if (User.HasPermission(UserAccess.PharmacyEdit)) permissions.Add(UserAccess.PharmacyEdit);
        //    if (User.HasPermission(UserAccess.CreateLicense)) permissions.Add(UserAccess.CreateLicense);
        //    if (User.HasPermission(UserAccess.Payment)) permissions.Add(UserAccess.Payment);

        //    return permissions.ToArray();
        //}


        //[Route("[action]")]
        //public Task<List<HQueUser>> GetBDOs()
        //{
        //    return _registrationManager.BDOList();
        //}

        //[Route("[action]")]
        //public async Task<string> AssignBDO(string instanceId, string bdoId)
        //{
        //    await _registrationManager.AssignBDO(instanceId, bdoId);
        //    return bdoId;
        //}

        ////Newly Added Gavaskar 04-10-2016 Start
        //[Route("[action]")]
        //public async Task<bool> SendOTP(string mobileno)
        //{
        //    BaseRegistrationOTP registrationOTP = new BaseRegistrationOTP();
        //    registrationOTP.OTPType = 1;
        //    //registrationOTP.OTPNumber = otp;

        //    var user = User.Identity;
        //    var accountid = user.AccountId();
        //    var userid = user.Id();

        //    registrationOTP.AccountId = accountid;
        //    registrationOTP.UserId = userid;
        //    // SendInviteCommunication(model);
        //    var mobileExists = await _registrationManager.SaveOTP(registrationOTP, mobileno);
        //    return !mobileExists;
        //}

        //[Route("[action]")]
        //public async Task<bool> ConfirmOTP(string OTP)
        //{
        //    BaseRegistrationOTP registrationOTP = new BaseRegistrationOTP();
        //    registrationOTP.OTPType = 1;
        //    registrationOTP.OTPNumber = OTP;

        //    var user = User.Identity;
        //    var accountid = user.AccountId();
        //    var userid = user.Id();

        //    registrationOTP.AccountId = accountid;
        //    registrationOTP.UserId = userid;
        //    return await _registrationManager.ConfirmOTP(registrationOTP, OTP);
        //}

        

        //[HttpPost]
        //[Route("[action]")]
        //public async Task<Registration> Create([FromBody]Registration model)
        //{
        //    //lr.add(model);
        //    //model.SetLoggedUserDetails(User);

        //    var bdoPermissions = GetPermissions();

        //    var user = User.Identity;
        //    if (bdoPermissions.Contains(UserAccess.PharmacyInfo) || bdoPermissions.Contains(UserAccess.PharmacyEdit))
        //        model.instance.BDOId = user.Id();

        //    if (model.account.RegisterType == 1)
        //    {
        //        model.account.IsApproved = true;
        //    }
        //    else
        //    {
        //        if (await _registrationManager.GetRegisterType(model.account.Id))
        //        {
        //            model.account.IsApproved = false;
        //        }
        //    }


        //    if (model.account.Id == "" || model.account.Id == null)
        //    {
        //        model.account.SetLoggedUserDetails(User);
        //        model.instance.SetLoggedUserDetails(User);
        //        //model.instance.Phone = model.instance.ContactMobile;


        //        model.instance.Timings.Select(timing => { timing.SetLoggedUserDetails(User); timing.AccountId = model.account.Id; timing.InstanceId = model.instance.Id; return timing; }).ToList();

        //        model.account.Payments.Select(payment => { payment.SetLoggedUserDetailsNonInstance(User); payment.AccountId = model.account.Id; return payment; }).ToList();

        //        return await _registrationManager.Save(model, bdoPermissions);
        //    }
        //    else
        //    {
        //        model.account.UpdatedBy = user.Id();
        //        model.instance.Timings.Select(timing => { timing.SetLoggedUserDetails(User); timing.AccountId = model.account.Id; timing.InstanceId = model.instance.Id; return timing; }).ToList();
        //        model.account.Payments.Select(payment =>
        //        {
        //            if (payment.Id == null)
        //            {
        //                payment.SetLoggedUserDetailsNonInstance(User); payment.AccountId = model.account.Id; return payment;
        //            }
        //            else
        //            {
        //                payment.AccountId = model.account.Id; return payment;
        //            }
        //        }).ToList();
        //        return await _registrationManager.Update(model, bdoPermissions);
        //    }
        //}

        [HttpPost]
        [Route("[action]")]
        public async Task<IFormFile[]> uploadFile([FromForm] string accountid, string instanceid, string createdBy, string updatedBy,
           bool file0Index, bool file1Index, bool file2Index, bool file3Index, bool file4Index, IFormFile[] file)
        {

            bool[] Indexes = { file0Index, file1Index, file2Index, file3Index, file4Index };
            var fileIndexes = Indexes.ToList().Select((val, ix) => val ? ix : -1).Where(x => x >= 0).ToArray();

            var existingImages = await _registrationManager.GetBranchImages(accountid, instanceid);
            var existImgIndex = 0;
            //var submittedImgIndex = 0;
            List<PharmacyUploads> delImages = new List<PharmacyUploads>();
            foreach (var existingImg in existingImages)
            {
                if (Indexes[existImgIndex])
                {
                    await _registrationManager.DeleteBranchImage(existingImg.Id);
                    //delImages.Add(existingImg);
                    
                }
                existImgIndex++;
                //submittedImgIndex++;
            }
            

            for (int i = 0; i < fileIndexes.Count(); i++)
            //int i=-1;
             //foreach (var fileIndex in fileIndexes)
             {
                //i++;
                if (!string.IsNullOrEmpty(Convert.ToString(file[i])))
                {

                    var parsedContentDisposition = ContentDispositionHeaderValue.Parse(file[i].ContentDisposition);
                    var fileName = Guid.NewGuid().ToString() + '_' + parsedContentDisposition.FileName.Replace(" ", "_").Trim('"');
                    if (!Directory.Exists(_environment.WebRootPath + "\\uploads\\Registration"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\uploads\\Registration");
                    }
                    var filePath = Path.Combine(_environment.WebRootPath, "uploads\\Registration", fileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file[i].CopyToAsync(fileStream);
                    }

                    BasePharmacyUploads PharmacyUploads = new BasePharmacyUploads();

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        PharmacyUploads.FileName = fileName;
                    }
                    var imageUploads = new PharmacyUploads();
                    imageUploads.AccountId = accountid;
                    imageUploads.InstanceId = instanceid;
                    imageUploads.CreatedBy = createdBy;
                    imageUploads.UpdatedBy = updatedBy;
                    imageUploads.FileName = fileName; // file[i].FileName

                    imageUploads.FileNumber = fileIndexes[i];

                    await _registrationManager.SaveFiles(imageUploads);
                }

            }
            return file;
        }

        //[Route("[action]")]
        //public async Task<Registration> EditRegistration(string accountId, string instanceId)
        //{
        //    return await _registrationManager.EditRegistration(accountId, instanceId);
        //}

        //[HttpPost]
        //[Route("[action]")]
        //public async Task<Registration> createLicense([FromBody]Registration model)
        //{
        //    model.account.User.SetLoggedUserDetails(User);
        //    model.account.User.AccountId = model.account.Id;
        //    //model.account.User.InstanceId = model.instance.Id;
        //    model.account.User.InstanceId = null;
        //    if (model.account.RegisterType != 1)
        //    {
        //        HQueUser user = await _registrationManager.CreateLicense(model.account);
        //    }
        //    return model;
        //}

        //[Route("[action]")]
        //public async Task<int> AddLicense(string accountId, int addnLicense)
        //{
        //    return await _registrationManager.AddLicense(accountId, addnLicense);
        //}

        //[Route("[action]")]
        //public IActionResult List()
        //{
        //    return View();
        //}

        [Route("[action]")]
        public IActionResult List(int type)
        {
            ViewBag.Type = type;
            return View();
        }


        //[Route("[action]")]
        //public async Task<List<Account>> AccountSearchList(int registrationType)
        //{
        //    var bdoPermissions = GetPermissions();
        //    var bdo = User.Identity;
        //    var bdoId = bdo.Id();
        //    return await _registrationManager.ListPager(registrationType, bdoPermissions, bdoId);
        //}

        // Added by Gavaskar 01-12-2017 Start
        [HttpGet]
        [Route("[action]")]
        public async Task<string> GetGstSelectedCustomer()
        {
            return await _registrationManager.GetGstSelectedCustomer(User.AccountId());
        }
        [HttpGet]
        [Route("[action]")]
        public async Task<string> GetGstSelectedVendor()
        {
            return await _registrationManager.GetGstSelectedVendor(User.AccountId());
        }
        // Added by Gavaskar 01-12-2017 End
    }
}
