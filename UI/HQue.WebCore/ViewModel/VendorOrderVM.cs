﻿using HQue.Contract.Infrastructure.Inventory;
using System.Collections.Generic;
using HQue.Contract.Base;

namespace HQue.WebCore.ViewModel
{
    public class VendorOrderVM:BaseContract
    {

        public string VendorId { get; set; }

        public string ProductId { get; set; }

        public decimal Quantity { get; set; }
        public List<VendorOrderItem> VendorOrderItem { get; set; }
        public VendorOrder selectedVendor { get; set; }        
    }
}
