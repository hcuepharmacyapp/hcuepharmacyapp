// window.addEventListener('DOMContentLoaded', function() {
//     var $datepicker = document.querySelector('.date');

//     $datepicker.DatePickerX.init({
//         mondayFirst: true,
//         format: 'dd-mm-yyyy',
//         todayButton: true,
//         todayButtonLabel: 'Today',
//         clearButton: true,
//         clearButtonLabel: 'Clear'
//     });

// });

$(function() {
    // $("#standard").customselect();
    // $(".select-option").customselect({ search: false });
});

// $(function() {
//     $('#track-chart').highcharts({
//         chart: {
//             type: 'line'
//         },
//         title: {
//             text: ''
//         },
//         subtitle: {
//             text: ''
//         },
//         xAxis: {
//             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
//                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
//             ]
//         },
//         yAxis: {
//             title: {
//                 text: 'Temperature'
//             },
//             labels: {
//                 formatter: function() {
//                     return this.value + '°';
//                 }
//             }
//         },
//         tooltip: {
//             crosshairs: true,
//             shared: true
//         },
//         plotOptions: {
//             spline: {
//                 marker: {
//                     radius: 4,
//                     lineColor: '#666666',
//                     lineWidth: 1
//                 }
//             }
//         },
//         series: [{
//             name: 'Tokyo',
//             marker: {
//                 symbol: 'square'
//             },
//             data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, {
//                 y: 26.5,
//                 marker: {
//                     symbol: 'url(https://www.highcharts.com/samples/graphics/sun.png)'
//                 }
//             }, 23.3, 18.3, 13.9, 9.6]

//         }, {
//             name: 'London',
//             marker: {
//                 symbol: 'diamond'
//             },
//             data: [{
//                 y: 3.9,
//                 marker: {
//                     symbol: 'url(https://www.highcharts.com/samples/graphics/snow.png)'
//                 }
//             }, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]ch
//         }]
//     });
// });
// error message
window.onload = function() {
        $('.one').click(
            function() {
                $.miniNoty({

                    message: 'Click me to go other website!',
                })
                return false;
            }
        )
    }
    // material design form
$(window, document, undefined).ready(function() {

    $('input').blur(function() {
        var $this = $(this);
        if ($this.val())
            $this.addClass('used');
        else
            $this.removeClass('used');
    });

    var $ripples = $('.ripples');

    $ripples.on('click.Ripples', function(e) {

        var $this = $(this);
        var $offset = $this.parent().offset();
        var $circle = $this.find('.ripplesCircle');

        var x = e.pageX - $offset.left;
        var y = e.pageY - $offset.top;

        $circle.css({
            top: y + 'px',
            left: x + 'px'
        });

        $this.addClass('is-active');

    });

    $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
        $(this).removeClass('is-active');
    });

});

$(function() {
    // $('select').selectric();

    // $("html").niceScroll({
    //     cursoropacitymin: 0.3,
    //     background: "#f5f5f5",
    //     cursorborder: "0",
    //     cursorminheight: 30,
    //     cursorcolor: '#333',
    //     cursorborderradius: '0px',
    //     cursorwidth: 6,
    //     autohidemode: true,
    //     horizrailenabled: true
    // });
    // $("html").niceScroll({ styler: "fb", cursorcolor: "#000" });
});
// global sucess message
function sucess_message() {
    var x = document.getElementById("sucess-message")
    x.className = "show";
    setTimeout(function() { x.className = x.className.replace("show", ""); }, 3000);
}
// global error message
function error_message() {
    var x = document.getElementById("error-message")
    x.className = "show";
    setTimeout(function() { x.className = x.className.replace("show", ""); }, 3000);
}

$(document).ready(function() {




    $('table tr td').each(function(index, element) {
        var th = $('table tr:first-child th').eq($(this).index()).text();
        $(this).attr('data-title', th);
    });

    $('.slideContentHeader').prepend('<span class="indicator">+</span> ');
    $('.slideContentHeader').click(function() {
        $(this).parent().find('.slideContent').slideToggle("slow");
    });
    $('.slideContentHeader').click(function() {
        $(this).toggleClass("active");
    });
    $('.slideContentHeader').click(function() {
        $(this).find('.indicator').toggleClass("rotate");
    });


    $('#upload').change(function() {
        var filename = $('input[type=file]').val().split('\\').pop();
        console.log(filename, $('#file'));
        var lastIndex = filename.lastIndexOf("\\");
        $('#file').val(filename);
    });


    // product name dropdown
    // $('#list-dropdown').keyup(function(){
    //     var sval=$(this).val();
    //     if(sval==''){   
    //     $('.product-list-dropdown').animate({opacity:"0",'visibility', 'hidden'}, 'fast');
    //     }else{
    //     $('.product-list-dropdown').animate({opacity:"1",'visibility', 'visible'});
    //     }
    // });


    // select option 
    // $('select').selectric();

    $('form:first *:input[type!=hidden]:first').focus();

    // login steps
    $(".login-click").click(function() {
        $(".login-form").css("display", "none");
        $(".choose-section").css("display", "block");
    });
    // forget password steps
    $(".forgot-click").click(function() {
        $(".login-form").css("display", "none");
        $(".forget-form").css("display", "block");
    });

    $('.progress-in').stop().animate({ width: $('.progress-val').text() }, {
        duration: 2000,
        step: function(now) {
            $('.progress-val').text(Math.ceil(now) + '%');
        }
    });

    // image uploadFile preview
    $("#uploadFile1").on("change", function() {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test(files[0].type)) { // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function() { // set image data as background of div

                $(".imagePreview1").css("background-image", "url(" + this.result + ")");

                $(".imagePreview1").addClass('current');

            }
        }
    });

    $('.imagePreview1').click(function() {
        $('#uploadFile1').trigger('click');
    });

    $("#uploadFile2").on("change", function() {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test(files[0].type)) { // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function() { // set image data as background of div
                $(".imagePreview2").css("background-image", "url(" + this.result + ")");
                $(".imagePreview2").addClass('current');
            }
        }
    });
    $('.imagePreview2').click(function() {
        $('#uploadFile2').trigger('click');
    });

    $("#uploadFile3").on("change", function() {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test(files[0].type)) { // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function() { // set image data as background of div
                $(".imagePreview3").css("background-image", "url(" + this.result + ")");
                $(".imagePreview3").addClass('current');
            }
        }
    });
    $('.imagePreview3').click(function() {
        $('#uploadFile3').trigger('click');
    });

    $("#uploadFile4").on("change", function() {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test(files[0].type)) { // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function() { // set image data as background of div
                $(".imagePreview4").css("background-image", "url(" + this.result + ")");
                $(".imagePreview4").addClass('current');
            }
        }
    });
    $('.imagePreview4').click(function() {
        $('#uploadFile4').trigger('click');
    });

    $("#uploadFile5").on("change", function() {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test(files[0].type)) { // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function() { // set image data as background of div
                $(".imagePreview5").css("background-image", "url(" + this.result + ")");
                $(".imagePreview5").addClass('current');
            }
        }
    });
    $('.imagePreview5').click(function() {
        $('#uploadFile5').trigger('click');
    });

    // header dropdown
    $(".header-menu").click(function() {
        $(".menu-dropdown").slideToggle(500);
    });
    // left menu 
    $("#sidebar .menu-action").click(function() {
        $(".main").toggleClass("hide-menu");
    });
    // export dropdown
    $(".export-files").click(function() {
        $(".export-dropdown").slideToggle(500);
    });
    // export dropdown
    $(".manage-columns").click(function() {
        $(".columns-dropdown").slideToggle(500);
    });

    // grid change
    $(".grid-change").click(function() {
        $(this).find('img').toggle();
        $(".row").toggleClass("grid-with");
        $('.dashboard-section').matchHeight();
    });

    // date picker


    // matchHeight
    $('.equal-height').matchHeight();
    $('.dashboard-section').matchHeight();
    $('.purchase-import-page p').matchHeight();
    $('.form-inner-field label').matchHeight();


    // tabs
    $('.common-tab li').click(function() {
        var tab_id = $(this).attr('data-tab');

        $('.common-tab li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    });


    // Add vender 
    $(".add-vender").click(function() {
        $(".vender-list").css("display", "none");
        $(".add-vender-page").css("display", "block");
    });
    $(".save-vender").click(function() {
        $(".add-vender-page").css("display", "none");
        $(".vender-list").css("display", "block");
    });

    // Add vender 
    $(".add-new-drug").click(function() {
        $(".drug-database-list").css("display", "none");
        $(".add-drug-page").css("display", "block");
    });
    $(".save-drug").click(function() {
        $(".add-drug-page").css("display", "none");
        $(".drug-database-list").css("display", "block");
    });

    // Add branch 
    $(".add-branch").click(function() {
        $(".branch-list").css("display", "none");
        $(".add-branch-page").css("display", "block");
    });
    $(".save-branch").click(function() {
        $(".add-branch-page").css("display", "none");
        $(".branch-list").css("display", "block");
    });

    // accordion
    $('.accordiontitle').append('<span class="arrowaccordion"></span>')

    $(".accordioncontent").hide();
    $(".accordioncontent:eq(0)").fadeIn();
    $(".accordiontitle").click(function() {
        $(".accordioncontent").slideUp();
        $(".accordiontitle").removeClass("active");
        $(this).addClass("active");
        ele_id = '.' + ($(this).attr('id'));
        $(ele_id).slideDown();
    });


    $('.otp-popup').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.otp-section').fadeIn();
        return false;
    });

    $('.overlayClick').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.form-popup').fadeIn();
        return false;
    });
    $('.addnew-drug').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.form-popup').fadeIn();
        return false;
    });
    $('.alternats').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.alternats-tablets').fadeIn();
        return false;
    });
    $('.header-offers').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.offer-popup-section').fadeIn();
        return false;
    });
    $('.add-stock').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.add-stock-setion').fadeIn();
        return false;
    });
    $('.missed-orders').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.missed-orders-setion').fadeIn();
        return false;
    });
    $('.add-product').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.add-product-setion').fadeIn();
        return false;
    });
    $('.add-dc').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.add-dc-setion').fadeIn();
        return false;
    });
    // success message
    $('.success-message').on('click', function() {
        var post_id = $(this).attr('data-post-id');
        $('#form_post_id').val(post_id);
        $('.success-lightbox').fadeIn();
        return false;
    });
    $('.success-ok').on('click', function() {
        $(this).closest('.popup-overlay').fadeOut();
        return false;
    });
    // $('document').on('keyup', function(evt) {
    //     if (evt.keyCode == 27) {
    //         alert('Esc key pressed.');
    //     }
    // });

    $('.popup-close').on('click', function() {
        $(this).closest('.popup-overlay').fadeOut();
        return false;
    });

    // ripple effect 
    var addMulitListener = function(el, events, callback) {
        // Split all events to array
        var e = events.split(' ');

        // Loop trough all elements
        Array.prototype.forEach.call(el, function(element, i) {
            // Loop trought all events and add event listeners to each
            Array.prototype.forEach.call(e, function(event, i) {
                element.addEventListener(event, callback, false);
            });
        });
    };

    addMulitListener(document.querySelectorAll('[material]'), 'click touchstart', function(e) {
        var ripple = this.querySelector('.ripple');
        var eventType = e.type;
        if (ripple == null) {
            ripple = document.createElement('span');
            ripple.classList.add('ripple');
            this.insertBefore(ripple, this.firstChild);
            if (!ripple.offsetHeight && !ripple.offsetWidth) {
                var size = Math.max(e.target.offsetWidth, e.target.offsetHeight);
                ripple.style.width = size + 'px';
                ripple.style.height = size + 'px';
            }
        }
        // Remove animation effect
        ripple.classList.remove('animate');

        // get click coordinates by event type
        if (eventType == 'click') {
            var x = e.pageX;
            var y = e.pageY;
        } else if (eventType == 'touchstart') {
            var x = e.changedTouches[0].pageX;
            var y = e.changedTouches[0].pageY;
        }
        x = x - this.offsetLeft - ripple.offsetWidth / 2;
        y = y - this.offsetTop - ripple.offsetHeight / 2;

        // set new ripple position by click or touch position
        ripple.style.top = y + 'px';
        ripple.style.left = x + 'px';
        ripple.classList.add('animate');
    });
    // overall discount
    $("#overall-discount").on('click', function() {
        if ($(this).is(':checked')) {
            $(".overall-discount-section").css("display", "block");
            $(".billbased-discount-section").css("display", "none");
            $(".slab-discount-section").css("display", "none");
            $(".category-discount-section").css("display", "none");
            $(".product-discount-section").css("display", "none");
        } else {
            $(".overall-discount-section").css("display", "none");
        }
    });
    // bill based discount
    $("#bill-based-discount").on('click', function() {
        if ($(this).is(':checked')) {
            $(".billbased-discount-section").css("display", "block");
            $(".slab-discount-section").css("display", "none");
            $(".overall-discount-section").css("display", "none");
            $(".product-discount-section").css("display", "none");
            $(".overall-discount-section").css("display", "none");
        } else {
            $(".billbased-discount-section").css("display", "none");
        }
    });
    // slab discount
    $("#slab-discount").on('click', function() {
        if ($(this).is(':checked')) {
            $(".overall-discount-section").css("display", "none");
            $(".billbased-discount-section").css("display", "none");
            $(".slab-discount-section").css("display", "block");
            $(".category-discount-section").css("display", "none");
            $(".product-discount-section").css("display", "none");
        } else {
            $(".slab-discount-section").css("display", "none");
        }
    });
    // category discount
    $("#category-discount").on('click', function() {
        if ($(this).is(':checked')) {
            $(".overall-discount-section").css("display", "none");
            $(".billbased-discount-section").css("display", "none");
            $(".slab-discount-section").css("display", "none");
            $(".category-discount-section").css("display", "block");
            $(".product-discount-section").css("display", "none");
        } else {
            $(".category-discount-section").css("display", "none");
        }
    });
    // product discount
    $("#product-discount").on('click', function() {
        if ($(this).is(':checked')) {
            $(".overall-discount-section").css("display", "none");
            $(".billbased-discount-section").css("display", "none");
            $(".slab-discount-section").css("display", "none");
            $(".category-discount-section").css("display", "none");
            $(".product-discount-section").css("display", "block");
        } else {
            $(".product-discount-section").css("display", "none");
        }
    });
    $("#Credit").on('click', function() {
        if ($(this).is(':checked')) {
            $(".amount-details").css("display", "block");
        } else {
            $(".amount-details").css("display", "none");
        }
    });
    $("#Full").on('click', function() {
        if ($(this).is(':checked')) {
            $(".amount-details").css("display", "none");
        } else {
            $(".amount-details").css("display", "block");
        }
    });
    $("#Card").on('click', function() {
        if ($(this).is(':checked')) {
            $(".card-details").css("display", "block");
        } else {
            $(".card-details").css("display", "none");
        }
    });
    $("#Cash").on('click', function() {
        if ($(this).is(':checked')) {
            $(".card-details").css("display", "none");
        } else {
            $(".card-details").css("display", "block");
        }
    });
    // purchase card details start
    $("#PaymentCredit").on('click', function() {
        if ($(this).is(':checked')) {
            $(".purchase-credit-details").css("display", "block");
        } else {
            $(".purchase-credit-details").css("display", "none");
        }
    });
    $("#PaymentFull").on('click', function() {
        if ($(this).is(':checked')) {
            $(".purchase-credit-details").css("display", "none");
        } else {
            $(".purchase-credit-details").css("display", "block");
        }
    });
    // purchase card details start
    // purchase card details start
    $("#PaymentCard").on('click', function() {
        if ($(this).is(':checked')) {
            $(".purchase-card-details").css("display", "block");
            $(".purchase-cheque-details").css("display", "none");
        } else {
            $(".purchase-card-details").css("display", "none");

        }
    });
    $("#PaymentCash").on('click', function() {
        if ($(this).is(':checked')) {
            $(".purchase-card-details").css("display", "none");
        } else {
            $(".purchase-card-details").css("display", "block");
        }
    });
    // purchase card details end
    // purchase cheque details start
    $("#PaymentCheque").on('click', function() {
        if ($(this).is(':checked')) {
            $(".purchase-cheque-details").css("display", "block");
            $(".purchase-card-details").css("display", "none");
        } else {
            $(".purchase-cheque-details").css("display", "none");

        }
    });
    $("#PaymentCash").on('click', function() {
        if ($(this).is(':checked')) {
            $(".purchase-cheque-details").css("display", "none");
        } else {
            $(".purchase-cheque-details").css("display", "block");
        }
    });

    // purchase cheque details end
    $("#Delivery").on('click', function() {
        if ($(this).is(':checked')) {
            $(".door-delivery").css("display", "block");
        } else {
            $(".door-delivery").css("display", "none");
        }
    });
    $("#Counter").on('click', function() {
        if ($(this).is(':checked')) {
            $(".door-delivery").css("display", "none");
        } else {
            $(".door-delivery").css("display", "block");
        }
    });

    // starttime.addEventListener('keyup', function(e) {
    //     if (e.keyCode !== 8) {
    //         if (this.value.length === 2) {
    //             this.value = this.value += ' : ';
    //         }
    //     }
    //     this.value.length === 6
    // });
    // endtime.addEventListener('keyup', function(e) {
    //     if (e.keyCode !== 8) {
    //         if (this.value.length === 2) {
    //             this.value = this.value += ' : ';
    //         }
    //     }
    //     this.value.length === 6
    // });


    // error message
    $('#demo-success').on('click', function() {
        notify({
            type: "success", //alert | success | error | warning | info
            title: "Success",
            position: {
                x: "right", //right | left | center
                y: "top" //top | bottom | center
            },
            icon: '<img src="images/paper_plane.png" />',
            // message: "jQuery Notify.js Demo. Super simple Notify plugin."
        });
    });

    $('#demo-warning').on('click', function() {
        notify({
            type: "warning", //alert | success | error | warning | info
            title: "Warning",
            theme: "dark-theme",
            position: {
                x: "right", //right | left | center
                y: "top" //top | bottom | center
            },
            icon: '<img src="images/paper_plane.png" />',
            // message: "jQuery Notify.js Demo. Super simple Notify plugin."
        });
    });
    $('#demo-error').on('click', function() {
        notify({
            type: "error", //alert | success | error | warning | info
            title: "Error",
            theme: "dark-theme",
            position: {
                x: "right", //right | left | center
                y: "top" //top | bottom | center
            },
            icon: '<img src="images/paper_plane.png" />',
            // message: "jQuery Notify.js Demo. Super simple Notify plugin."
        });
    });
    $('#demo-info').on('click', function() {
        notify({
            type: "info", //alert | success | error | warning | info
            title: "Info",
            theme: "dark-theme",
            position: {
                x: "right", //right | left | center
                y: "top" //top | bottom | center
            },
            icon: '<img src="images/paper_plane.png" />',
            // message: "jQuery Notify.js Demo. Super simple Notify plugin."
        });
    });


    $('.dropdown .trigger-drop i').on('click', function() {
        $(this).toggleClass('fa-flip-vertical');
        $(this).parent().toggleClass('active');
        $('.dropdown .drop').toggle();
        $('.dropdown .drop .trigger-sub i').removeClass('fa-flip-vertical').parent().removeClass('active');
        $('.dropdown .drop .drop-sub').slideUp();
        return false;
    });
    $('.dropdown .drop .trigger-sub i').on('click', function() {
        $(this).toggleClass('fa-flip-vertical');
        $(this).parent().toggleClass('active');
        $('.dropdown .drop .drop-sub').slideToggle(150);
        return false;
    });

    // $starttime();
    // $endtime();

});
