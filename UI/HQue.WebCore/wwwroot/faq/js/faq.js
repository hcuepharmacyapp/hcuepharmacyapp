$(document).ready(function() {

    $('.searchtrigger').on('click', function() {
        $('.search-block').addClass('open', function() {
            $('#search-text').focus()
        });

        // $('.search-block').addClass('open');

        return false;
    });
    $('.search-close').on('click', function() {
        $('.search-block').removeClass('open');
    });

    // Banner cover image call javascript function start
    $('.cover-image').each(function() {
        var imgUrl = $(this).find('.image-hide').attr('src');
        $(this).css('background-image', 'url(' + imgUrl + ')');
    });
    var secHeg = $('.banner-decs-text').innerHeight() / 2;
    $('.banner-decs-text').css({
        marginTop: -secHeg
    });

    var tabOffset = $('.header-menu-section').offset().top
    $(window).scroll(function() {
        var scroll = $(window).scrollTop() + $('header').innerHeight();

        //alert(tabOffset);
        if (scroll > tabOffset) {
            $('.header-menu-section').addClass('sticky');
            $('.header-menu-section').css({ top: $('header').innerHeight() })
        } else {
            $('.header-menu-section').removeClass('sticky');
        }
    });

    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 300
            }, 800, function() {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });

    $('.example').okzoom({
        width: 200,
        height: 200,
        border: "1px solid black",
        shadow: "0 0 5px #000"
    });

    $("#preloader_container").css("opacity", "1")
        .css("display", "flex");
    setTimeout(function() {
        $("#preloader_container").css("opacity", "0");
        setTimeout(function() {
            $("#preloader_container").css("display", "none");
        }, 900);
    }, 1500)

    // scroll top fiunction
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    $('.scroll-to-top').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 800);
    });

    //this is how you init your accordion.
    // $('.accordion-1').accordion();

    // $('.accordion-2').accordion({
    //     //this is how you set animationtime, if you want, else remove
    //     animationTime: 500,
    // });

    // $('.accordion-2 [data-accordion="title"]').bind('click', function() {
    //     var self = this;
    //     setTimeout(function() {
    //         theOffset = $(self).offset();
    //         $('body,html').animate({
    //             scrollTop: theOffset.top - 150
    //         });
    //     }, 310);
    // });


    // $('.popup-gallery').each(function() {
    //     $(this).magnificPopup({
    //         delegate: 'a',
    //         type: 'image',
    //         tLoading: 'Loading image #%curr%...',
    //         mainClass: 'mfp-img-mobile',
    //         gallery: {
    //             enabled: true,
    //             navigateByImgClick: true,
    //             preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    //         },
    //         image: {
    //             tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
    //             titleSrc: function(item) {
    //                 return item.el.attr('title') + '<small></small>';
    //             }
    //         }
    //     });
    // });

});
