app.controller('customerReminderCtrl', function ($scope, reminderService, ModalService, toastr, $rootScope) {

    $scope.reminderFilter = true;
    $scope.list = [];
    $scope.search = {};

    $scope.customerReminder = function () {
        if ($scope.reminderFilter != null) {
            $.LoadingOverlay("show");
            reminderService.customerReminderList($scope.reminderFilter, $scope.search).then(function (response) {
                $scope.list = response.data;
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        else {
            $.LoadingOverlay("show");
            reminderService.allCustomerReminder($scope.search).then(function (response) {
                $scope.list = response.data.list;
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    }

    $scope.getReminder = function (upcoming) {
        $('#customerUpcomingReminder').removeClass('active');
        $('#customerMissedReminder').removeClass('active');
        $('#customerAllReminder').removeClass('active');

        $scope.reminderFilter = upcoming;
        if (upcoming)
            $('#customerUpcomingReminder').addClass('active');
        else if (upcoming === false)
            $('#customerMissedReminder').addClass('active');
        else
            $('#customerAllReminder').addClass('active');
        $scope.customerReminder();
    }

    

    $scope.customerReminder();
    
    $scope.deleteReminder = function (id) {
        if (!confirm("Sure to delete?"))
            return;

        reminderService.cancelUserReminder(id).then(function (response) {
            $scope.customerReminder();
            $.LoadingOverlay("hide");
            toastr.success('Reminder deleted successfully');
            hcueUserReminderGlobal();
        }, function () {
            $.LoadingOverlay("hide");
        });
        return false;
    }

    $scope.dismissReminder = function (id , remarksFor) {
        var remark = { userReminderId: id, remarksFor: remarksFor };
        reminderService.saveRemark(remark).then(function (response) {
            $scope.customerReminder();
            $.LoadingOverlay("hide");
            toastr.success('Reminder dismissed');
            hcueUserReminderGlobal();
        }, function () {
            $.LoadingOverlay("hide");
        });
        return false;
    }

    $scope.PopupAddNew = function () {
        var m = ModalService.showModal({
            controller: "addReminderCtrl",
            templateUrl: 'addReminder',
            inputs: {
                reminderType: 1
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
        return false;
    };

    $rootScope.$on("doSelectPatient", function (data) {
        $scope.PopupAddNew();
    });

    $("#menuCustomerReminder").removeClass("active").addClass("active");
    $("#menuPersonalReminder").removeClass("active");

    $scope.$on('customer-reminder-added', function (event, args) {
        $scope.customerReminder();
    });
});