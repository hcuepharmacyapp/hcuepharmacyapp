app.controller('addReminderCtrl', function ($scope, reminderService, patientService, productService, $rootScope, close,toastr,cacheService,ModalService) {


    $scope.reminderType = 1;
    $scope.selectedProduct = { qty: 0 };
    $scope.selectedCustomer = null;
    $scope.patientSearchData = {};
    $scope.addedProduct = [];
    $scope.model = { reminderFrequency: "16", reminderTime: "9" };

    $scope.init = function (value) {
        $scope.reminderType = value;
    }

    $scope.minDate = new Date();
    var d = new Date();

    $scope.popup1 = {
        opened: false
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,

    };


    $scope.close = function (result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
        $(".modal-backdrop").hide();
    };

    $scope.resetCustomer = function () {
        $scope.selectedCustomer = null;
    }

    $scope.getProducts = function (val) {

        return productService.drugFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.addProduct = function () {
        if ($scope.selectedProduct.product == null) {
            alert("Select a product");
            return;
        }
        if ($scope.selectedProduct.qty == "" || $scope.selectedProduct.qty == 0) {
            alert("Enter a valid quantity");
            return;
        }
        var product = { name: $scope.selectedProduct.product.name, qty: $scope.selectedProduct.qty };
        $scope.addedProduct.push(product);
        $scope.selectedProduct.product = null;
        $scope.selectedProduct.qty = 0;
        document.getElementById('drugName').focus();
    }

    $scope.removeProduct = function (product) {
        if (!confirm("Sure to remove ?"))
            return;
        var index = $scope.addedProduct.indexOf(product);
        $scope.addedProduct.splice(index, 1);
    }

    $scope.search = function () {
        $.LoadingOverlay("show");
        $scope.selectedCustomer = null;
        $scope.patientSearchData.status = 1;
        patientService.list($scope.patientSearchData,1).then(function (response) {
            var list = response.data;
            if (list.length > 0)
                $scope.selectedCustomer = list[0];
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.addReminder = function () {
        $scope.close("NO");
        var data = getSaveData();

        reminderService.addReminnder(data).then(function (response) {
            $rootScope.$broadcast('customer-reminder-added', { any: {} });

            $.LoadingOverlay("hide");
            $scope.model = {};
            $scope.addedProduct = [];
            toastr.success('Reminder added successfully');
            hcueUserReminderGlobal();
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    function getSaveData() {
        var products = [];

        for (var i = 0; i < $scope.addedProduct.length; i++) {
            var product = { productName: $scope.addedProduct[i].name, quantity: $scope.addedProduct[i].qty, reminderFrequency: $scope.model.reminderFrequency };
            products.push(product);
        }

        var data = {
            startDate: $scope.model.startDate,
            reminderTime: $scope.model.reminderTime,
            reminderFrequency: $scope.model.reminderFrequency,
            reminderType: $scope.reminderType,
            description: $scope.model.description,
            remarks: $scope.model.remarks,
            reminderProduct: products
        };


        if ($scope.selectedCustomer != null) {
            data.reminderPhone = $scope.selectedCustomer.mobile;
            data.customerName = $scope.selectedCustomer.name;
            data.customerAge = $scope.selectedCustomer.age;
            data.customerGender = $scope.selectedCustomer.gender;
        }

        return data;
    }

    $scope.PopupAddNew = function () {
        $scope.close("NO");
        cacheService.set('selectedPatient', $scope.patientSearchData);
        var m = ModalService.showModal({
            controller: "patientCreateCtrl",
            templateUrl: 'createPatient',
            inputs: {
                mode: "Create"
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });

    };

    var patient = cacheService.get('selectedPatient');
    if (patient != null) {
        cacheService.remove('selectedPatient')
        $scope.resetCustomer();
        $scope.patientSearchData.mobile = patient.mobile;
        $scope.search();
    }
});