app.controller('personalReminderCtrl', function ($scope, reminderService, $rootScope, toastr) {

    $scope.reminderFilter = true;

    $scope.list = [];

    $scope.personalReminder = function () {
        if ($scope.reminderFilter != null) {
            $.LoadingOverlay("show");
            reminderService.personalReminderList($scope.reminderFilter).then(function (response) {
                $scope.list = response.data;
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        else {
            $.LoadingOverlay("show");
            reminderService.allPersonalReminder().then(function (response) {
                $scope.list = response.data.list;
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    }

    $scope.getReminder = function (upcoming) {
        $('#personalUpcomingReminder').removeClass('active');
        $('#personalMissedReminder').removeClass('active');
        $('#personalAllReminder').removeClass('active');

        $scope.reminderFilter = upcoming;
        if (upcoming)
            $('#personalUpcomingReminder').addClass('active');
        else if (upcoming === false)
            $('#personalMissedReminder').addClass('active');
        else
            $('#personalAllReminder').addClass('active');

        $scope.personalReminder();
    }

    $scope.personalReminder();

    $scope.deleteReminder = function (id) {
        if (!confirm("Sure to delete?"))
            return;

        reminderService.cancelUserReminder(id).then(function (response) {
            $scope.personalReminder();
            $.LoadingOverlay("hide");
            toastr.success('Reminder deleted successfully');
            hcueUserReminderGlobal();
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.dismissReminder = function (id, remarksFor) {
        var remark = { userReminderId: id, remarksFor: remarksFor };
        reminderService.saveRemark(remark).then(function (response) {
            $scope.personalReminder();
            $.LoadingOverlay("hide");
            toastr.success('Reminder dismissed');
            hcueUserReminderGlobal();
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $("#menuPersonalReminder").removeClass("active").addClass("active");
    $("#menuCustomerReminder").removeClass("active");

    $scope.$on('personal-reminder-added', function (event, args) {
        $scope.personalReminder();
    });
});