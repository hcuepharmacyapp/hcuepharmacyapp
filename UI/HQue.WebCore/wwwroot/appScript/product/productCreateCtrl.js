app.controller('productCreateCtrl', function ($scope, productModel, productService, toastr, close, $filter, poproductname, $rootScope, cacheService, GSTEnabled) {

    var product = productModel;
    $scope.GSTEnabled = GSTEnabled;
    $scope.product = product;
    var ele = "";
    if (poproductname != null && poproductname != "") {
        $scope.product.name = poproductname;
    }
    $scope.subcatogoryList = [];
  
    $scope.products = angular.copy(product);
    $scope.focusInput = true;
    $scope.init = function () {
        return productService.subcategoryFilterData($scope.subcatid).then(function (response) {
            return response.data.map(function (item) {
                $scope.subcatogoryList.push(item);
                return item;



            });
        });
    }

    $scope.init();



    $rootScope.$on("ProductCreateEvent", function () {
        $scope.close("No");
        cacheService.set("isProductCreateOpened", false);
    });

    $scope.close = function (result) {
        close(result, 500);
        $(".modal-backdrop").hide();
    };

    //$(document).keydown(function (e) {
    angular.element(document).keydown(function (e) {
        if (e.ctrlKey && e.keyCode == 67) {
            event.preventDefault();
            $scope.close('No');//Ctrl + C (cancel)
        }
    });

    //$scope.keydownEve(e);

    $scope.onPatientSelect = function (obj, event, nextid) {
        $scope.products = obj;
        var ele = document.getElementById("productName");
        ele.focus();
        //$scope.focusNextField(nextid);
    };


    $scope.focusNextField = function (nextid) {
        var ele = document.getElementById(nextid);
        ele.focus();
    };
    $scope.productCreate = function () {
        $scope.close('NO');
        productService.create($scope.product)
            .then(function (response) {
                $scope.productForm.$setPristine();
                toastr.success('Product added successfully. ' + (response.data.code == null ? '' : 'Product Code is [' + response.data.code + ']'));
                $scope.product = {};
            }, function () {
                toastr.error('Product already exists', 'error');
            });
    };


    $scope.getSubCategory1 = function (subcategory)
    {
        $scope.products.subcategory = subcategory;
    }

    $scope.productsCreate = function () {
         
        $scope.close('NO');
        $scope.product = $scope.products;
        if ($scope.product.name != null && $scope.products.name.name != undefined) {
            $scope.product.productMasterId = $scope.products.name.id;
            $scope.product.name = $scope.products.name.name;
        }
        if ($scope.product.genericName != null && $scope.products.genericName.genericName != undefined) {
            $scope.product.genericName = $scope.products.genericName.genericName;
        } 


        if ($scope.product.manufacturer != null && $scope.products.manufacturer.manufacturer != undefined) {
            $scope.product.manufacturer = $scope.products.manufacturer.manufacturer;
        }


        if ($scope.product.schedule != null && $scope.products.schedule.schedule != undefined) {
            $scope.product.schedule = $scope.products.schedule.schedule;
        }

        if ($scope.product.type != null && $scope.products.type.type != undefined) {
            $scope.product.type = $scope.products.type.type;
        }

        //Added by Annadurai on 17/05/2017
        if ($scope.product.schedule == null) {
            $scope.product.schedule = 'H';
        }

        //End here
        if ($scope.product.category != null && $scope.products.category.category != undefined) {
            $scope.product.category = $scope.products.category.category;
        }
        //Added by arun for subcategory
        if ($scope.product.subcategory != null && $scope.products.subcategory !== undefined)
            $scope.product.subcategory = $scope.products.subcategory;


        if ($scope.product.commodityCode != null && $scope.products.commodityCode.commodityCode != undefined) {
            $scope.product.commodityCode = $scope.products.commodityCode.commodityCode;
        }


        productService.create($scope.product)
            .then(function (response) {
                toastr.success('Product added successfully. ' + (response.data.code == null ? '' : 'Product Code is [' + response.data.code + ']'));
                $scope.product = {};
                $scope.productForm.$setPristine();
            }, function (error) {
                console.log(error);
                toastr.error('Product already exists', 'error');
            });
    };

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            window.location.assign("/product/index");
        }
    };

    $scope.getProduct = function (val) {
        $scope.products = angular.copy(product);
        return productService.getProducts(val).then(function (response) {
            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                return item;
            });
        });
    };

    $scope.getGeneric = function (val) {

        return productService.genericFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.getManufacturer = function (val) {

        return productService.manufacturerFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.getSchedule = function (val) {

        if (val == "" || val == null) {

        } else {
            if (val.length > 0) {
                return productService.scheduleFilterData(val).then(function (response) {
                    return response.data.map(function (item) {
                        return item;
                    });
                });
            }
        }


    };

    $scope.getType = function (val) {

        if (val == "" || val == null) {

        } else {
            if (val.length > 0) {
                return productService.typeFilterData(val).then(function (response) {
                    return response.data.map(function (item) {
                        return item;
                    });
                });
            }
        }


    };

    $scope.getCategory = function (val) {

        if (val == "" || val == null) {

        } else {
            if (val.length > 0) {
                return productService.categoryFilterData(val).then(function (response) {
                    return response.data.map(function (item) {
                        return item;
                    });
                });
            }
        }


    };


    $scope.getSubCategory = function () {
        $scope.subcatid = 3;
        $scope.subcatogoryList = [];
        return productService.subcategoryFilterData($scope.subcatid).then(function (response) {
            return response.data.map(function (item) {
                $scope.subcatogoryList.push(item);
                return item;
            });
        });
    }
    $scope.getCommodity = function (val) {

        if (val == "" || val == null) {

        } else {
            if (val.length > 0) {
                return productService.commodityFilterData(val).then(function (response) {
                    return response.data.map(function (item) {
                        return item;
                    });
                });
            }

        }
    };
    $scope.vatErrorMessage = false;
    $scope.changeVat = function (vat) {
        if (vat != "" || vat != undefined) {
            if (vat > 100) {
                $scope.vatErrorMessage = true;
            } else {
                $scope.vatErrorMessage = false;
            }
        }
    };

    //GST related starts

    $scope.igstErrorMessage = false;
    $scope.changeIGST = function () {
        var igst = $scope.products.igst;
        if (igst != "" || igst != undefined) {
            if (igst > 100) {
                $scope.igstErrorMessage = true;
            } else {
                $scope.igstErrorMessage = false;
            }
        }
    };
    $scope.cgstErrorMessage = false;
    $scope.changeCGST = function () {
        var cgst = $scope.products.cgst;
        if (cgst != "" || cgst != undefined) {
            if (cgst > 100) {
                $scope.cgstErrorMessage = true;
            } else {
                $scope.cgstErrorMessage = false;
            }
        }
    };
    $scope.sgstErrorMessage = false;


    $scope.changeSGST = function () {
        var sgst = $scope.products.sgst;
        if (sgst != "" || sgst != undefined) {
            if (sgst > 100) {
                $scope.sgstErrorMessage = true;
            } else {
                $scope.sgstErrorMessage = false;
            }
        }
    };


    $scope.changeGST = function (value) {
        var sgst = parseFloat($scope.products.gstTotal) || 0;
        var igst = parseFloat($scope.products.igst) || 0;
        if (value == "txtgstTotal_01") {
            $scope.products.cgst = (sgst / 2).toFixed(2);
            $scope.products.sgst = (sgst / 2).toFixed(2);
            $scope.products.igst = sgst;
        }
    };
    $scope.gstTotalErrorMessage = false;
    $scope.changeGSTTotal = function () {
        var gstTotal = $scope.products.gstTotal;
        if (gstTotal != "" || gstTotal != undefined) {
            if (gstTotal > 100) {
                $scope.gstTotalErrorMessage = true;
            } else {
                $scope.gstTotalErrorMessage = false;
            }
        }
    };
    //$scope.checkPercentage = function (e, value) {
    //    var char = e.key;
    //    if (parseFloat(value + char) > 100 && e.keyCode!=8) {
    //        e.preventDefault();
    //        return false;
    //    }
    //    return true;
    //};
    $scope.checkAllProductfields = function (ind) {
        if (ind > 1) {
            if ($scope.products == undefined) {
                var ele = document.getElementById("productName");
                ele.focus();
                return false;
            } else {
                if ($scope.products.name == undefined || Object.keys($scope.products.name).length == 0) {
                    var ele = document.getElementById("productName");
                    ele.focus();
                    return false;
                }

            }
        }
        if (ind > 2) {
            if ($scope.products == undefined) {
                var ele = document.getElementById("txtgenericName");
                ele.focus();
                return false;
            } else {
                if ($scope.products.genericName == undefined || $scope.products.genericName == null) {
                    var ele = document.getElementById("txtgenericName");
                    ele.focus();
                    return false;
                }
            }
        }
        if (ind > 3) {
            if ($scope.products == undefined) {
                var ele = document.getElementById("manufacturer_01");
                ele.focus();
                return false;
            } else {
                if ($scope.products.manufacturer == undefined || $scope.products.manufacturer == null) {
                    var ele = document.getElementById("manufacturer_01");
                    ele.focus();
                    return false;
                }
            }
        }
        if (ind > 14) {
            if ($scope.products == undefined) {
                var ele = document.getElementById("txtPackageSize");
                ele.focus();
                return false;
            } else {
                if ($scope.products.packageSize == undefined || $scope.products.packageSize == null) {
                    var ele = document.getElementById("txtPackageSize");
                    ele.focus();
                    return false;
                }
            }
        }
    };
    $scope.keyEnter = function (e, nextid, currentid) {

        ele = document.getElementById(nextid);

        if (e.which == 13) {


            if (currentid == "commodityCode") {
                if ($scope.GSTEnabled) {
                    ele = document.getElementById("txthsn");
                } else {
                    ele = document.getElementById("txtVat");
                }
            }
            if (currentid == "txtigst") {
                if ($scope.products.igst == "" || $scope.products.igst == undefined) {
                    return false;
                }
            }

            if (currentid == "txtcgst") {
                if ($scope.products.cgst == "" || $scope.products.cgst == undefined) {
                    return false;
                }
            }


            if (currentid == "txtsgst") {
                if ($scope.products.sgst == "" || $scope.products.sgst == undefined) {
                    return false;
                }
            }

            if (currentid == "txtgstTotal_01") {
                if ($scope.products.gstTotal == "" || $scope.products.gstTotal == undefined) {
                    return false;
                }
            }
            if (currentid == "txtPackageSize") {
                if ($scope.products.packageSize == "" || $scope.products.packageSize == undefined) {
                    return false;
                }
            }
            ele.focus();
            e.preventDefault();
        }
    };

    $scope.getTaxValues = function () {
        productService.getTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;            
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };

    //by San 
    $scope.getTaxValues();


});


