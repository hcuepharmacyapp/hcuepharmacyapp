app.controller('updateproductlistingCtrl', function ($scope, toastr, productService, productModel, pagerServcie) {

    console.log("Listing");
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
  
     
   

    var product = productModel;

    $scope.search = product;

    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    $scope.kindNames = [
            { value: 'OTC', label: 'OTC' },
           { value: 'Prescription', label: 'Prescription' },
           { value: 'FMCG', label: 'FMCG' },
           { value: 'F&B', label: 'F&B' }
    ];
    function pageSearch() {
        productService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].status == 1) {
                    $scope.list[i].status = 'Active';
                }
                else {
                    $scope.list[i].status = 'InActive';
                }
            }
            $scope.selectedIndex = -1;
        }, function () { });
    }

    $scope.getData = function () {
        productService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    }
    $scope.productSearch = function (productName, obj) {
        $.LoadingOverlay("show");
        if (productName != undefined && productName != null) {
            $scope.search.name = productName;
        }
        else if ($scope.selectedProduct != null) {
            $scope.search.name = $scope.selectedProduct.title;
        }
        else {
            $scope.search.name = null;
        }


        $scope.search.page.pageNo = 1;
        productService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            //$scope.exportList = $scope.list.name;

            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].status == 1) {
                    $scope.list[i].status = 'Active';
                }
                else {
                    $scope.list[i].status = 'InActive';
                }

            }
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.productSearch();





    $scope.UpdateList = function () {
    //    $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        console.log(JSON.stringify(data));
        productService.updateList(data).then(function (response) {
            console.log(JSON.stringify(response))
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
           
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }


    function showDetails(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $scope.editProducts(dataItem);
    }
});