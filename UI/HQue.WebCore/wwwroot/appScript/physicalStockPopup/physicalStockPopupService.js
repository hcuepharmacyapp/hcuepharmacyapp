app.factory('physicalStockPopupService', function ($http) {
    return {
        getPhysicalStockProducts: function () {
            return $http.post('/ProductStockData/getPhysicalStockProducts');
        },
        updatePhysicalStockHistory: function (data) {
            return $http.post('/ProductStockData/updatePhysicalStockHistory', data);
        },
        getInventorySettings: function () {
            return $http.post('/ProductStockData/EditInventorySetting');
        },
        updatePhysicalStockPopupSettings: function (data) {
            return $http.post('/ProductStockData/UpdatePhysicalStockPopupSettings', data);
        },
    };
});