app.controller('physicalStockHistoryCtrl', function ($scope, $filter, toastr, close, ModalService, physicalStockPopupService, params) {

    $scope.gridOptions = {
        enableFiltering: false,
        enableSorting: false,
        enableColumnMenus: false
    };
    $scope.gridOptions.enableHorizontalScrollbar = 0;

    $scope.gridOptions.columnDefs = [
      { name: 'product.id', enableCellEdit: false, displayName: 'S.No', width: 50, cellTemplate: 'snoTemplate' },
      { name: 'product.name', enableCellEdit: false, displayName: 'Product Name', width: 270 },
      { name: 'product.rackNo', enableCellEdit: false, displayName: 'Rack/Box No', width: 150, cellTemplate: 'rackBoxTemplate' },
      { name: 'physicalStock', displayName: 'Physical Stock', width: 141.80, type: 'number', editableCellTemplate: 'physicalStockTemplate' }
    ];
    $scope.gridOptions.data = params[0].list;
    if ($scope.gridOptions.data[0].openedAt.indexOf('Z') == $scope.gridOptions.data[0].openedAt.length - 1)
        $scope.openedAtDate = $filter('date')($scope.gridOptions.data[0].openedAt.substring(0, $scope.gridOptions.data[0].openedAt.length - 1), 'dd/MM/yyyy h:mm a');
    else
        $scope.openedAtDate = $filter('date')($scope.gridOptions.data[0].openedAt, 'dd/MM/yyyy h:mm a');

    $scope.updatePhysicalStockHistory = function () {
        if (!confirm("Are you sure to save the data?"))
            return;

        $.LoadingOverlay("show");
        physicalStockPopupService.updatePhysicalStockHistory($scope.gridOptions.data).then(function (response) {
            if (response.data == true) {
                toastr.success("Data saved successfully.");
                closePopup();
            }
            else {
                toastr.info("Data seems to be already saved from another screen.");
                closePopup();
            }
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });        
    }

    function closePopup() {
        close('No', 100);
        $(".modal-backdrop").hide();
    }

    $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;
        window.setTimeout(function () {
            $scope.gridApi.cellNav.scrollToFocus($scope.gridOptions.data[0], $scope.gridOptions.columnDefs[3]);
        }, 1000);
    };
    
});