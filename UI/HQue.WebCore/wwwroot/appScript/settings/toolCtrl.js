app.controller('toolCtrl', function ($scope, toolModel, toolService,toastr) {


    $scope.minDate = new Date();
    var d = new Date();
    //File Upload
    $scope.SelectedFileForUpload = null;

    //File Select event 
    $scope.selectFileforUpload = function (file) {
        $scope.SelectedFileForUpload = file[0];
    };
   

    var tools = toolModel;
   

    $scope.tool = tools;


    $scope.toolCreate = function () {
       
        toolService.create($scope.tool, $scope.SelectedFileForUpload).then(function (response) {
            $scope.toolsForm.$setPristine();
            toastr.success('Image added successfully');
            $scope.tool = {};
            $scope.toolsList();
        }, function () {            
        });
    }

    $scope.toolsList = function ()
    {
        toolService.list().then(function (response) {
            $scope.list = response.data;
        }, function () { });
    }

    $scope.toolsList();

    $scope.changeStatus = function (position, list)
    {
        $scope.currentStatus = "";
        angular.forEach(list, function (subscription, index) {
            if (position != index)
                subscription.status = false;
            else {
                //$scope.tool.push(subscription);
                $scope.tool.id = subscription.id;
                if (subscription.status == true) {
                    $scope.tool.status = 1;
                }
                else {
                    $scope.tool.status = 0;
                }
            }
        });

        toolService.updateList($scope.tool).then(function (response) {
            $scope.list = response.data;
        }, function () { });
       //$scope.test = list;
     }

});

