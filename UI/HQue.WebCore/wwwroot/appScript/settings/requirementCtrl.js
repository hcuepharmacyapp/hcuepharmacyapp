app.controller('requirementCtrl', function ($scope, requirementModel, toolService, toastr) {


    $scope.minDate = new Date();   
    
    var requirements = requirementModel;

    $scope.requirement = requirements;
    $scope.requirementList = [];

    $scope.requirementtab = "tabSelected";
    $scope.issuetab = "tabNormal";
    $scope.todayDate = new Date();
    $scope.addRequirement = true;    
    $scope.instanceName = null;
    $scope.requirement.description = null;
    $scope.requirement.type = 1;

    $scope.requirementType = function () {

        $scope.requirementtab = "tabSelected";
        $scope.issuetab = "tabNormal";
        $scope.addRequirement = true;
        $scope.requirement.type = 1;
        $scope.requirementList = [];
        $scope.requirement.description = null;
        $scope.todayDate = new Date();
        $scope.requirement.gatheredBy = null;
    }

    $scope.issueType = function () {
                
        $scope.requirementtab = "tabNormal";
        $scope.issuetab = "tabSelected";
        $scope.addRequirement = false;
        $scope.requirement.type = 2;
        $scope.requirementList = [];
        $scope.requirement.description = null;
        $scope.todayDate = new Date();
        $scope.requirement.gatheredBy = null;
    }

    $scope.addRequirements = function (item) {
        $scope.requirementList.push(angular.copy(item));
        $scope.requirement.description = null;

    }

    $scope.saveRequirements = function () {
        $.LoadingOverlay("show");
        if ($scope.requirement.description != null) {
            $scope.requirementList.push(angular.copy($scope.requirement));
        }
        toolService.save($scope.requirementList).then(function (response) {
            
            toastr.success("Saved Succesfully");
            $scope.todayDate = new Date();
            $scope.addRequirement = true;
            $scope.instanceName = null;
            $scope.requirement.description = null;
            $scope.requirement.gatheredBy = null;            
            $scope.requirementList = [];
            if ($scope.requirement.type == 1) {
                $scope.requirementtab = "tabSelected";
                $scope.issuetab = "tabNormal";
            }
            else {
                $scope.requirementtab = "tabNormal";
                $scope.issuetab = "tabSelected";
            }
            $scope.getInstance();
            $.LoadingOverlay("hide");
        }, function(error){
            toastr.error("Error Occurred");
            $.LoadingOverlay("hide");
        });        
    }    

    $scope.getInstance = function () {
        toolService.getInstanceName().then(function (response) {
            $scope.instanceName = response.data.instanceName;
            $scope.requirement.requirementNo = response.data.requirementNo;
        }, function (error) {
            console.log(error);
        });
    }

    
});

