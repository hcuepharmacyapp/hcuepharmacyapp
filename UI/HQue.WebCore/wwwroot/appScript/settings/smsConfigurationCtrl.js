app.controller('smsConfigurationCtrl', function ($scope, smsConfigurationModel, instanceModel, toastr, toolService, $filter, close, branchDetail) {
    
    $scope.shouldBeOpen = true;
    $scope.branch1 = null;
    $scope.smsConfiguration = angular.copy(smsConfigurationModel);
    $scope.availableSms = 0;
    $scope.usedSms = 0;
    $scope.balanceSms = 0;
    $scope.list = null;
    $scope.instance = angular.copy(instanceModel);
    $scope.editCount = 0;

    if (branchDetail != null && branchDetail != '') {
        $scope.branch1 = branchDetail;
    }

    $scope.close = function (result) {
        close(result, 100);
        $(".modal-backdrop").hide();
    };

    $scope.editSmsCount = function (item) {
        if ($scope.editCount > 0) {
            return false;
        }
        $scope.smsConfiguration = item;
        $scope.smsConfiguration.isEdit = true;
        $scope.editCount++;
        var ele = document.getElementById("smsCount");
        ele.focus();
    }

    $scope.clear = function () {        
        $scope.editCount = 0;
        $scope.smsConfiguration = angular.copy(smsConfigurationModel);
        $scope.getSmsDetail();
        $scope.getInstanceSmsCount();
        var ele = document.getElementById("smsCount");
        ele.focus();
    }

    $scope.save = function () {

        if ($scope.smsConfiguration.isEdit == true) {
            var temp = 0;
            for (var i = 0; i < $scope.list.length; i++) {
                temp = temp + parseInt($scope.list[i].smsCount);
            }
            if (parseInt(temp) < $scope.usedSms) {
                $scope.smsConfiguration.smsCount = $scope.smsConfiguration.smsCountBeforeEdit;
                toastr.error("Sms Count is lesser than balance sms count.")
                return false;
            }
        }

        $scope.smsConfiguration.instanceId = $scope.branch1.instanceId;
        $scope.smsConfiguration.accountId = $scope.branch1.accountId;
        toolService.saveSmsConfiguration($scope.smsConfiguration).then(function (response) {
            if ($scope.smsConfiguration.isEdit == true) {
                $scope.editCount--;
            }
            $scope.smsConfiguration = angular.copy(smsConfigurationModel);
            $scope.getSmsDetail();
            $scope.getInstanceSmsCount();
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getSmsDetail = function () {
        toolService.getSmsConfiguration($scope.branch1.accountId, $scope.branch1.instanceId).then(function (response) {
            $scope.list = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getSmsDetail();

    $scope.getInstanceSmsCount = function () {
        toolService.getSmsCount($scope.branch1.accountId, $scope.branch1.instanceId).then(function (response) {
            $scope.instance = response.data;

            $scope.availableSms = $scope.instance.totalSmsCount;
            $scope.usedSms = $scope.instance.sentSmsCount;
            $scope.balanceSms = $scope.instance.totalSmsCount - $scope.instance.sentSmsCount;

        }, function (error) {
            console.log(error);
        });
    }
    
    $scope.getInstanceSmsCount();

});