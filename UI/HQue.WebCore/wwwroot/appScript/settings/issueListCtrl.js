app.controller('issueListCtrl', function ($scope, toastr, toolService, requirementModel, pagerServcie, $filter) {

    var requirement = requirementModel;

    $scope.search = requirement;

    $scope.list = [];
    $scope.search.type = 0;
    $scope.search.statusType = "";

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination
    
    $scope.updateStatus = function (req) {
        $.LoadingOverlay("show");
        toolService.updateStatus(req).then(function (response) {
            alert('Status has been updated successfully.');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.changeOptions = function () {      
        if ($scope.search.select === 'issue') {
            $scope.search.type = 2;
        }
        else if ($scope.search.select === 'requirement') {
            $scope.search.type = 1;
        }
        else {
            $scope.search.type = 0;
        }
        $scope.IssueList();
    };

    $scope.changeStatusType = function () {        
        $scope.IssueList();
    };

    $scope.getDate = function (date) {
        return $filter('date')(date, 'dd/MM/yyyy');
    };

    $scope.getStatus = function (status) {
        if (status === "") {
            return "New"
        }
        else {
            return status;
        }
    };    

    $scope.isIssueType = function (type) {
        if ($scope.search.select === 'issue') {
            return "Issue";
        }
        else if ($scope.search.select === 'requirement') {
            if (type === 1) {
                return "Req";
            }
            else {
                return "Requirement";
            }
        }
        else {
            if (type === 1) {
                return "Issue/Req";
            }
            else {
                return "Issue/Requirement";
            }            
        }
    };

    function pageSearch() {
        $.LoadingOverlay("show");
        toolService.issueList($scope.search.type, $scope.search.statusType, $scope.search).then(function (response) {
            $scope.list = response.data.list;
            $scope.setNullStatus();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.IssueList = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        toolService.issueList($scope.search.type, $scope.search.statusType, $scope.search).then(function (response) {
            $scope.list = response.data.list;
            $scope.setNullStatus();
            pagerServcie.init(response.data.noOfRows, pageSearch);
             $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.IssueList();

    $scope.setNullStatus = function () {
        for (var i = 0; i < $scope.list.length; i++) {
            if ($scope.list[i].status === "" || $scope.list[i].status === undefined) {
                $scope.list[i].status = "New";
        }             
    }
}

});