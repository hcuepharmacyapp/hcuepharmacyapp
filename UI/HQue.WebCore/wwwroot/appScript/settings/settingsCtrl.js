app.controller('settingsCtrl', function ($scope, toastr, toolService, $filter,$location, smsSettingsModel) {

    $scope.list = [];
    $scope.branchList = [];    
    $scope.showBranchList=true; 
    $scope.accountName = "";
    $scope.IsHidden = true;
    $scope.showsalebillcsvSetting = true;
    $scope.IsHidden1 = true;
    $scope.IsHidden2 = true;
    $scope.showSmsSettings = true; //Added by Sarubala on 23-11-17
    $scope.smsSettingsModel = angular.copy(smsSettingsModel);
    $scope.selectAllSmsSettings = false;

    $scope.ShowHide = function () {
        $scope.IsHidden = $scope.IsHidden ? false : true;
    };
    $scope.ShowHide1 = function () {
        $scope.IsHidden1 = $scope.IsHidden1 ? false : true;
    };
    $scope.ShowHide2 = function () {
        $scope.IsHidden2 = $scope.IsHidden2 ? false : true;
    };
    $scope.salebillSetting = function () {
        $scope.showsalebillcsvSetting = $scope.showsalebillcsvSetting ? false : true;
    };
    //Added by Sarubala on 23-11-17 - start
    $scope.smsSettings = function () {
        $scope.showSmsSettings = $scope.showSmsSettings ? false : true;
        var instanceId = $location.search().instanceId;

        toolService.getSmsSettingsForInstance(instanceId).then(function (response) {
            $scope.smsSettingsModel = response.data;

            if($scope.smsSettingsModel.isSalesCreateSms && $scope.smsSettingsModel.isSalesEditSms && $scope.smsSettingsModel.isSalesEstimateSms && $scope.smsSettingsModel.isSalesOrderSms &&
            $scope.smsSettingsModel.isVendorCreateSms && $scope.smsSettingsModel.isCustomerBulkSms &&
            $scope.smsSettingsModel.isOrderCreateSms && $scope.smsSettingsModel.isUserCreateSms){
                $scope.selectAllSmsSettings = true;
            }

        }, function (error) {
            console.log(error);
        });
    };

    $scope.changeSmsSettings = function () {
        if ($scope.selectAllSmsSettings == true) {
            $scope.smsSettingsModel.isSalesCreateSms = true;
            $scope.smsSettingsModel.isSalesEditSms = true;
            $scope.smsSettingsModel.isSalesEstimateSms = true;
            $scope.smsSettingsModel.isSalesOrderSms = true;
            $scope.smsSettingsModel.isVendorCreateSms = true;
            $scope.smsSettingsModel.isCustomerBulkSms = true;
            $scope.smsSettingsModel.isOrderCreateSms = true;
            $scope.smsSettingsModel.isUserCreateSms = true;

        }
        else {
            $scope.smsSettingsModel.isSalesCreateSms = false;
            $scope.smsSettingsModel.isSalesEditSms = false;
            $scope.smsSettingsModel.isSalesEstimateSms = false;
            $scope.smsSettingsModel.isSalesOrderSms = false;
            $scope.smsSettingsModel.isVendorCreateSms = false;
            $scope.smsSettingsModel.isCustomerBulkSms = false;
            $scope.smsSettingsModel.isOrderCreateSms = false;
            $scope.smsSettingsModel.isUserCreateSms = false;
        }
    }

    $scope.setSelectAll = function () {
        if ($scope.smsSettingsModel.isSalesCreateSms && $scope.smsSettingsModel.isSalesEditSms && $scope.smsSettingsModel.isSalesEstimateSms && $scope.smsSettingsModel.isSalesOrderSms &&
            $scope.smsSettingsModel.isVendorCreateSms && $scope.smsSettingsModel.isCustomerBulkSms &&
            $scope.smsSettingsModel.isOrderCreateSms && $scope.smsSettingsModel.isUserCreateSms) {
            $scope.selectAllSmsSettings = true;
        }
        else {
            $scope.selectAllSmsSettings = false;
        }
    }

    $scope.saveSmsSettings = function () {
        $scope.smsSettingsModel.instanceId = $location.search().instanceId;
        toolService.saveSmsSettingsForInstance($scope.smsSettingsModel).then(function (response) {
            toastr.success("Sms Settings saved successfully");
        }, function (error) {
            console.log(error);
            toastr.error("Error Occurred");
        })
    }

    //Added by Sarubala on 23-11-17 - end
 
    $scope.selectedAccount = {
       "Name": "",
        "id": ""
    };
    $scope.selectedBranch = {
        "instanceId": "",
        "name": ""
    };

    $scope.init = function (Accountid) {

        var getAccountid = $location.search().aid;
        if (!angular.isUndefinedOrNull(getAccountid))
        {
            $scope.showBranchList=false;
            $scope.getInstanceSettingsList(getAccountid);
        }
        else
        {
            $scope.showBranchList = true;
            $scope.getInstanceSettingsList(Accountid);
        }
    };
   
    $scope.getInstanceSettingsList = function (Accountid) {

        var getinstanceId = $location.search().instanceId;
        if (angular.isUndefinedOrNull(getinstanceId))
            getinstanceId = "";

            $.LoadingOverlay("show");
            toolService.getInstanceSettingsList(Accountid, getinstanceId).then(function (response) {
                $.LoadingOverlay("hide");
                $scope.list = response.data;
                $scope.accountName = $scope.list[0].accountName;
            }, function () {
                $.LoadingOverlay("hide");
            });
    }

    $scope.getAccountList = function () {
        $.LoadingOverlay("show");
        toolService.getAccountList().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.accountList = response.data;
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getAccountList();

    $scope.reset = function () {              
       
        $scope.accountList = [];
        $scope.getAccountList();
        $scope.init("");
    }

    $scope.selectAll = function () {
        var toggleStatus = $scope.isAllSelected;

        angular.forEach($scope.list, function (itm) {
            itm.isChecked = toggleStatus;
        });

    };
    $scope.selectAll1 = function () {
        var toggleStatus = $scope.isAllSelected1;

        angular.forEach($scope.list, function (itm) {
            itm.isChecked = toggleStatus;
        });

    };
    $scope.selectSalesBillcsv = function () {
        var toggleStatus = $scope.SalesBillcsv;

        angular.forEach($scope.list, function (itm) {
            itm.isChecked = toggleStatus;
        });

    };
    $scope.addInstance = function (status, index, branch) {

        var count = 0;
        var listcount = $scope.list.length;
        angular.forEach($scope.list, function (itm, ind) {
            if (itm.isChecked)
                count++;                            
        });
    }

    /*   */
    $scope.updateHcueProductMasterSettings = function () {
        toolService.updateHcueProductMasterSettings($scope.list).then(function (response) {
            $.LoadingOverlay("hide");       
            toastr.success("hcue Global Product Master setting updated successfully.");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.updateHcuePurchasePriceSettings = function () {
        toolService.updateHcuePurchasePriceSettings($scope.list).then(function (response) {
            $.LoadingOverlay("hide");          
            toastr.success("Purchase LowestPrice updated successfully.");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    
    $scope.updateSalesPriceSettings = function () {
        var settingsVal = 0;
        if ($scope.SalesPrice == true) {
            settingsVal = 3;
        } else {
            settingsVal = 0;
        }
        toolService.updateSalesPriceSettings($scope.list, settingsVal).then(function (response) {
            $.LoadingOverlay("hide");
          $scope.list = response.data;
            //$scope.accountName = $scope.list[0].accountName;
          $scope.getSalesPriceSetting();
          toastr.success("Sales - Purchase Price billing enable setting updated successfully.");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.updateSaleBillcsv = function () {
        toolService.updateSaleBillcsv($scope.list).then(function (response) {
            $.LoadingOverlay("hide");
            $scope.getSaleBillcsv();
            toastr.success("Sale Bill to CSV updated successfully.");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.getSaleBillcsv = function () {
        var getinstanceId = $location.search().instanceId;
        toolService.getSaleBillcsv(getinstanceId).then(function (response) {
            if (response.data == 1) {
                $scope.SalesBillcsv = true;
            } else {
                $scope.SalesBillcsv = false;
            }

        }, function (error) {
            console.log(error);

        });
    };
  
    $scope.salesPriceSettings = 0;
    $scope.getSalesPriceSetting = function () {
        var getinstanceId = $location.search().instanceId;
        toolService.getSalesPriceSettings(getinstanceId).then(function (response) {
            if (response.data == 3) {
                $scope.SalesPrice = true;
            } else {
                $scope.SalesPrice = false;
            }
            
        }, function (error) {
            console.log(error);
        });
    };    

    $scope.getSalesPriceSetting();
    $scope.getSaleBillcsv();
    $scope.toggleAll = function () {
        var toggleStatus = !$scope.isAllSelected;
        angular.forEach($scope.list, function (itm) { itm.selected = toggleStatus; });
    }

    $scope.optionToggled = function () {
        $scope.isAllSelected = $scope.list.every(function (itm) { return itm.selected; })
    }
    
    angular.isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null || val === ""
    }

});