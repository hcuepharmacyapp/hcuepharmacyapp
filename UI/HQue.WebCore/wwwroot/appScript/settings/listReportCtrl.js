app.controller('listReportCtrl',['$scope', 'toastr', 'toolService', '$filter', function ($scope, toastr, toolService, $filter) {
  
  
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
  
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    //var data = {
    //    fromDate: $scope.from
    //}
 
    $scope.salesReport = function () {
       
        toolService.salesReport($scope.from).then(function (response) {         
            $scope.data = response.data;           
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.offlineReport = function () {
        toolService.offlineReport().then(function (response) {
            $scope.data1 = response.data;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    //onload issue fixed
    $scope.salesReport();
    $scope.offlineReport();
    $scope.order = ['groupName', 'pharmaName', 'city', 'area', 'salesnet', 'purchasenet', 'totalsales','totalpurchasecount'];
    
}]);