app.factory('toolService', function ($http) {
    return {
        list: function () {
            return $http.post('/tool/listData');
        },
        getInstanceName: function() {
            return $http.get('/requirement/getInstanceName');
        },
        save: function (data){
            return $http.post('/requirement/saveRequirement', data);
        },
        create: function (tool, file) {
            var formData = new FormData();
            formData.append('file', file);           
            angular.forEach(tool, function (value, key) {               
                    if (value === "" || value === null || value === undefined)
                        return;
                    formData.append(key, value);
            });

            return $http.post('/tool/index', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });
        },
        updateList: function (list) {
            return $http.post('/tool/updateStatus', list);
        },
        negativeStockList: function () {
            return $http.post('/tool/negativeStockList');
        },

        getInstanceData: function () {
            return $http.post('/salesReport/GetInstanceData');
        },

        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },

        issueList: function (type, status, searchData) {
            return $http.post('/tool/issueListData?type=' + type + '&status=' + status, searchData);
        },

        updateStatus: function (req) {
            return $http.post('/tool/updateIssueStatus', req);
        },
        getBranchList: function (accountId,branchId,name) {
            return $http.post('/tool/getBranchList?accountId=' + accountId + '&branchId=' + branchId + '&name=' + name);
        },
        updateOfflineStatus: function (branch,status) {
            return $http.post('/tool/updateOfflineStatus?status=' + status, branch);
        },
        updateOfflineVersionNo: function (branch) {
            return $http.post('/tool/updateOfflineVersionNo', branch);
        },
        getAccountList: function () {
            return $http.post('/tool/getAccountList');
        },
        getBranchListData: function () {
            return $http.post('/tool/getBranchListData');
        },
        getInstanceSettingsList: function (accountId, branchId) {
            return $http.post('/tool/getInstanceSettingsList?accountId=' + accountId + '&branchId=' + branchId);
        },
        updateHcueProductMasterSettings: function (branch) {
            return $http.post('/tool/updateHcueProductMasterSettings', branch);
        },
        //added by nandhini
        updateHcuePurchasePriceSettings: function (branch) {
            return $http.post('/tool/updateHcuePurchasePriceSettings', branch);
        },
        updateSaleBillcsv: function (branch) {
            return $http.post('/tool/updateSaleBillcsv', branch)
        },
        updateSalesPriceSettings: function (branch, SalesPrice) {
            return $http.post('/tool/updateSalesPriceSettings?&SalesPrice=' + SalesPrice, branch );
        },
     
        salesReport: function (data) {
            return $http.post('/tool/salesReport?data=' + data);
        },
        offlineReport: function () {
            return $http.post('/tool/offlineReport');
        },
        getSalesPriceSettings: function (insId) {
            return $http.post('/tool/getSalesPriceSettings?insId=' + insId, insId);
        },
        getexportToCsvSetting: function () {
            return $http.get('/tool/getexportToCsvSetting');
        },//added by nandhini for export to csv setting
        getSaleBillcsv: function (insId) {
            return $http.post('/tool/getSaleBillcsv?insId=' + insId, insId);
            },       
        //end
        //Added by Sarubala on 27-11-17
        getSmsCount: function (data1, data2) {
            return $http.post('/tool/getSmsCount?accountId=' + data1 + '&instanceId=' + data2);
        },
        getSmsConfiguration: function (data1, data2) {
            return $http.post('/tool/getSmsConfiguration?accountId=' + data1 + '&instanceId=' + data2);
        },
        saveSmsConfiguration: function (data) {
            return $http.post('/tool/saveSmsConfiguration', data);
        },
        getCustomSettings: function (accountId, instanceId) {
            return $http.post('/accountSetup/GetCustomSettings?AccountId=' + accountId + '&InstanceId=' + instanceId);
        },
        saveCustomSettings: function (data) {
            return $http.post('/accountSetup/SaveCustomSettings', data);
        },
        // Added by Gavaskar 25-12-2017 Start
        stockLedgerEditList: function (accountId, instanceId,productName) {
            return $http.post('/tool/StockLedgerEditList?AccountId=' + accountId + '&InstanceId=' + instanceId + '&ProductName=' + productName);
        },
        bulkStockLedgerUpdate: function (data, accountId, instanceId) {
            return $http.post('/tool/BulkStockLedgerUpdate?AccountId=' + accountId + '&InstanceId=' + instanceId, data);
        },
        InstancedrugFilter: function (filter, instanceId) {
            return $http.get('/tool/InstanceProduct?productName=' + escape(filter) + '&InstanceId=' + instanceId);
        },
        bulkUpdateProductStockOfflineToOnline: function (accountId, instanceId) {
            return $http.post('/tool/BulkUpdateProductStockOfflineToOnline?AccountId=' + accountId + '&InstanceId=' + instanceId);
        },
        bulkUpdateNegativeStockOfflineToOnline: function (accountId, instanceId) {
            return $http.post('/tool/BulkUpdateNegativeStockOfflineToOnline?AccountId=' + accountId + '&InstanceId=' + instanceId);
        },
        getNagativeStockList: function (accountId, instanceId) {
            return $http.get('/tool/GetNagativeStockList?AccountId=' + accountId + '&InstanceId=' + instanceId);
        },
        getReverseSyncOfflineToOnlineCompare: function (accountId, instanceId) {
            return $http.get('/tool/GetReverseSyncOfflineToOnlineCompare?AccountId=' + accountId + '&InstanceId=' + instanceId);
        },
        // Added by Gavaskar 25-12-2017 End
    }
});