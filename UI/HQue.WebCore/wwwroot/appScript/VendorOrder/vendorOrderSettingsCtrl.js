app.controller('vendorOrderSettingsCtrl', function ($scope, vendorOrderService, $filter, toastr, orderSettingsModel, vendorModel, vendorService) {

    $scope.orderSettings = orderSettingsModel;
    $scope.orderSettings.vendor = vendorModel;
    $scope.instanceList = null;
    $scope.vendor = vendorModel;
    $scope.selectedInstance = null;
    $scope.selectedInstanceName = null;
    $scope.isAlreadyHeadOfficeEnabled = false;

    $scope.ChangeDate = function () {
        if ($scope.NoOfDaysFromDate == 'CustomDate') {
            getCustomDateSeries();
        }
    }


    $scope.getDateSettings = function () {

        $.LoadingOverlay("show");

        vendorOrderService.getOrderSettings().then(function (response) {
            $scope.orderSettings.dateSettingType = response.data.DateSettingType;
            $scope.orderSettings.noOfDaysFromDate = response.data.noOfDaysFromDate != undefined ? response.data.noOfDaysFromDate : 0;
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });
    }

    $scope.getDateSettings();

    $scope.getHeadOfficeOrderSettings = function () {
        $.LoadingOverlay("show");

        vendorOrderService.getHeadOfficeOrderSetting().then(function (response) {
            $scope.orderSettings.isHeadOfficeEnabled = response.data.isHeadOfficeEnabled;
            $scope.orderSettings.headOffice_InstanceId = response.data.headOffice_InstanceId != undefined ? response.data.headOffice_InstanceId : null;
            $scope.selectedInstanceName = response.data.headOffice_InstanceName;
            $scope.isAlreadyHeadOfficeEnabled = $scope.orderSettings.isHeadOfficeEnabled;
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });
    }

    $scope.getHeadOfficeOrderSettings();

    $scope.saveDateSettings = function () {

        $.LoadingOverlay("show");
        //Default option removed from Set Custom Days by Settu
        if ($scope.orderSettings.dateSettingType == 2) {
            if ($scope.orderSettings.noOfDaysFromDate == null || $scope.orderSettings.noOfDaysFromDate == undefined || $scope.orderSettings.noOfDaysFromDate == "")
                $scope.orderSettings.noOfDaysFromDate = 0;
            if ($scope.orderSettings.noOfDaysFromDate < 0) {
                toastr.error("Enter Correct Custom Days");
                $.LoadingOverlay("hide");
                return;
            }
        }

        vendorOrderService.saveOrderSettings($scope.orderSettings).then(function (response) {

            toastr.success("Days Settings Saved Successfully");
            $scope.getDateSettings();
            $scope.getHeadOfficeOrderSettings();
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            toastr.error("Error Occurred", "Error");
            $.LoadingOverlay("hide");
        });
    }

    $scope.getAllInstances = function () {
        if ($scope.orderSettings.isHeadOfficeEnabled) {
            $.LoadingOverlay("show");
            vendorOrderService.getInstanceList().then(function (response) {
                $scope.instanceList = response.data;

                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                toastr.error("Error Occurred");
                $.LoadingOverlay("hide");
            })
        } else {
            $scope.instanceList = null;
        }
        
    };
    

    $scope.saveHeadOfficeSettings = function () {

        if ($scope.orderSettings.isHeadOfficeEnabled && $scope.selectedInstance != null) {
            $.LoadingOverlay("show");

            $scope.orderSettings.headOffice_InstanceId = $scope.selectedInstance.id;

            $scope.vendor.mobile1checked = false;
            $scope.vendor.mobile2checked = false;
            $scope.vendor.locationType = 1;
            $scope.vendor.vendorType = "1";
            $scope.vendor.paymentType = "Credit";
            $scope.vendor.name = $scope.selectedInstance.name;
            $scope.vendor.drugLicenseNo = $scope.selectedInstance.drugLicenseNo.toUpperCase();
            $scope.vendor.gsTinNo = $scope.selectedInstance.gsTinNo;
            $scope.vendor.tinNo = $scope.selectedInstance.tinNo != undefined ? $scope.selectedInstance.tinNo : null;
            $scope.vendor.status = 1;
            loadDefaultFormula();
            $scope.vendor.contactPerson = $scope.selectedInstance.contactName != undefined ? $scope.selectedInstance.contactName : null;
            $scope.vendor.mobile = $scope.selectedInstance.contactMobile != undefined ? $scope.selectedInstance.contactMobile : null;
            $scope.vendor.isHeadOfficeVendor = true;

            $scope.orderSettings.vendor = $scope.vendor;
            vendorOrderService.saveHeadOfficeSetting($scope.orderSettings).then(function (response) {

                $scope.instanceList = null;
                $scope.selectedInstance = null;
                $scope.getDateSettings();
                $scope.getHeadOfficeOrderSettings();
                $.LoadingOverlay("hide");
            }, function (error) {
                toastr.error("Error Occurred");
                $.LoadingOverlay("hide");
            })
        }
        
    }

    function loadDefaultFormula() {
        var name = "Default Formula";
        vendorService.getPurchaseFormulaList(name).then(function (response) {
            $scope.defaultFormula = $filter("filter")(response.data, { formulaName: name }, true)[0];
            if ($scope.defaultFormula == null) {
                var model = {
                    id: null,
                    formulaName: name,
                    formulaJSON: '[{"formulaTag":"markup","operation":"+","order":1},{"formulaTag":"discount","operation":"-","order":2},{"formulaTag":"schemediscount","operation":"-","order":3},{"formulaTag":"tax","operation":"+","order":4}]'
                };
                vendorService.savePurchaseFormula(model).then(function (response) {
                    $scope.defaultFormula = response.data;
                    $scope.vendor.vendorPurchaseFormula = $scope.defaultFormula;
                    $scope.moveNext("vendorName", "vendorName");
                }, function (error) {
                    console.log(error);
                });

            }
            if ($scope.defaultFormula != null) {
                $scope.vendor.vendorPurchaseFormula = $scope.defaultFormula;               
            }
        });
    };



});
