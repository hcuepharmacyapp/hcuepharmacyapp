app.controller('missedOrderSearchCtrl', function ($scope, vendorPurchaseService, vendorOrderService, vendorService, missedOrderModel, pagerServcie, productService, productModel, $filter) {
   
    var missedOrder = missedOrderModel;
       
    $scope.search = missedOrder;
    $scope.search.select = null;

    $scope.list = [];    

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    $scope.minDate = new Date();

    $scope.open = function () {
        $scope.popup.opened = true;
    };

    $scope.popup = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    function pageSearch() {
        $.LoadingOverlay("show");
        vendorOrderService.missedList($scope.search).then(function (response) {
            $scope.list = response.data;
            $.LoadingOverlay("hide");
        }, function () { 
            $.LoadingOverlay("hide");
        });
    }
    $scope.clearSearch = function () {        
        $scope.search.select1 = "";
        $scope.search.values = "";
        $scope.search.select = "";
    }
    $scope.missedOrderSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        $scope.search.searchProductId = "";
        if ($scope.search.drugName)
            $scope.search.values = $scope.search.drugName.id;

        vendorOrderService.missedList($scope.search).then(function (response) {
            $scope.list = response.data;
            $scope.vendorPurchase1 = JSON.parse(window.localStorage.getItem("p_dataChange"));
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.missedOrderSearch();

    $scope.cancelOrder = function (val) {

        if (!confirm("Are you sure, Do you want to cancel order ? "))
            return;
        $.LoadingOverlay("show");
        vendorOrderService.cancelOrder(val).then(function (response) {
            $scope.search.select = null;
            $scope.search.values = undefined;
            $scope.search.select1 = undefined;
            $scope.missedOrderSearch();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    

    $scope.changefilters = function () {
        $scope.search.select1 = "";
        $scope.search.values = "";
        $scope.search.invoiceDate = "";
        $scope.search.drugName = undefined;
        if ($scope.search.select == 'orderDate') {
            $scope.dateCondition = true;
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        else {
            $scope.dateCondition = false;
        }

        //if ($scope.search.select == 'quantity' || $scope.search.select == 'mrp' || $scope.search.select == 'discount' || $scope.search.select == 'vat') {
        //    $scope.salesCondition = true;
        //    $scope.dateCondition = false;
        //    $scope.productCondition = false;
        //}
        //else if ($scope.search.select == 'billDate' || $scope.search.select == 'expiry') {
        //    $scope.salesCondition = true;
        //    $scope.dateCondition = true;
        //    $scope.productCondition = false;
        //    $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
        //    //$scope.search.invoiceDate = values;
            
        //}
        //else if ($scope.search.select == 'product') {
        //    $scope.salesCondition = false;
        //    $scope.dateCondition = false;
        //    $scope.productCondition = true;
        //}
        //else {
        //    $scope.salesCondition = false;
        //    $scope.dateCondition = false;
        //    $scope.productCondition = false;
        //}
    };   

     $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper'+ row).slideToggle();
        $('#chip-wrapper'+ row).show();
        if ($('#chip-btn'+ row).text() === '+')
            $('#chip-btn'+ row).text('-');
        else {
            $('#chip-btn'+ row).text('+');
        }
     }

     $scope.currency = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.00'; return N; }
     $scope.currency3 = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.000'; return N; }


    //by San     
    
     $scope.product = productModel;
     
     $scope.getProducts = function (val) {

         return productService.InstancedrugFilter(val).then(function (response) {
             return response.data.map(function (item) {
                 return item;
             });
         });
     }

     $scope.selectedProduct = null;    

});

app.filter('singleDecimal', function ($filter) {
    return function (input) {
        if (isNaN(input)) return input;
        return Math.round(input * 10) / 10;
    };
});

