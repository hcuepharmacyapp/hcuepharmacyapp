app.controller('vendorOrderListCtrl', function ($scope, vendorOrderModel, vendorOrderService, vendorService, pagerServcie, $filter, printingHelper, toastr) {

    var customOrder = document.getElementById("hdnCustomOrder").value;
    var vendorOrder = vendorOrderModel;
    $scope.search = vendorOrder;
    $scope.list = [];
    $scope.vendorStatus = 2;
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    $scope.showSms = false; //Added by Sarubala on 20-11-17
    //pagination
    function pageSearch() {
        $.LoadingOverlay("show");
        vendorOrderService.vendorOrderList($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.customOrderList = [];
    $scope.orderSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        vendorOrderService.vendorOrderList($scope.search).then(function (response) {

            if (customOrder == 1) {
                $scope.customOrderList = response.data.list;
            } else {
                $scope.list = response.data.list;
            }
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.exportDataToCSV = function (vendororder) {
        var vendororder = removeDeletedItems(vendororder)
        if (vendororder.vendorOrderItem.length == 0) {
            toastr.info('No records available. It seems all items deleted.');
            return;
        }        
        var data = ",,,,,\t\t\t                      PurchaseOrder,\n";
        //data = data + ",,,,," + $scope.instance.name + ",\n";
        //data = data + ",,,,," + $scope.instance.fullAddress + ",\n";
        //data = data + ",,,,,PH:" + $scope.instance.phone + "," + "MOBILE:" + $scope.instance.mobile + ",\n";
        //if ($scope.instance.drugLicenseNo == undefined) $scope.instance.drugLicenseNo = "";
        //if ($scope.instance.tinNo == undefined) $scope.instance.tinNo = "";
        //data = data + ",,,,,DL No : " + $scope.instance.drugLicenseNo + ",\n";
        //data = data + ",,,,,TIN No: " + $scope.instance.tinNo + ",\n";
        if (customOrder == 1) {
            data = data + "Sr.no.,Order No,Order Date,Vendor Name,Vendor Mobile,Vendor EMail,Product Name, Unit/Strip, Quantity,Free Qty, Purchase Price/Strip,GstTotal,Mrp/Strip,Status,\n";
        }
        else {
            data = data + "Sr.no.,Order No,Order Date,Vendor Name,Vendor Mobile,Vendor EMail,Product Name, Unit/Strip, Quantity,Free Qty, Purchase Price/Strip,GstTotal,Mrp/Strip,Status,\n";
        }
        for (var j = 0; j < vendororder.vendorOrderItem.length; j++) {
            if (vendororder.vendorOrderItem[j].isDeleted == false)
            {
                data = data + ((vendororder.vendorOrderItem[j] == null || vendororder.vendorOrderItem[j] == undefined) ? "" : j + 1) + ",";
                data = data + (vendororder.prefix == undefined ? "" : vendororder.prefix) + vendororder.orderId + ",";
                data = data + $filter('date')(vendororder.orderDate, 'dd/MM/yyyy') + ",";
                data = data + vendororder.vendor.name + ",";
                data = data + vendororder.vendor.mobile + ",";
                data = data + vendororder.vendor.email + ",";
                data = data + vendororder.vendorOrderItem[j].product.name + ",";
                data = data + vendororder.vendorOrderItem[j].packageSize + ",";
                data = data + vendororder.vendorOrderItem[j].quantity + ",";
                data = data + vendororder.vendorOrderItem[j].freeQty + ",";
                data = data + vendororder.vendorOrderItem[j].purchasePrice.toFixed(2) + ",";
                //if (customOrder == 1) {
                //    data = data + $filter('date')(vendororder.vendorOrderItem[j].expireDate, "MM/yy") + ",";
                //}
                data = data + vendororder.vendorOrderItem[j].gstTotal + ",";
                data = data + vendororder.vendorOrderItem[j].sellingPrice.toFixed(2) + ",";
                data = data + (vendororder.vendorOrderItem[j].isDeleted == true ? "Deleted" :
                                    (vendororder.vendorOrderItem[j].vendorPurchaseId != ""
                                    && vendororder.vendorOrderItem[j].vendorPurchaseId != null
                                    && vendororder.vendorOrderItem[j].vendorPurchaseId != undefined
                                    || vendororder.vendorOrderItem[j].stockTransferId != ""
                                    && vendororder.vendorOrderItem[j].stockTransferId != undefined
                                    && vendororder.vendorOrderItem[j].stockTransferId != null) ? "Converted" : "Pending");
                data = data + ",\n";

            }            
        }
        data = data + ",\n";
        

        var a = $('<a/>', {
            style: 'display:none',
            //href: 'data:application/octet-stream;base64,' + btoa(data),
            href: 'data:application/octet-stream;base64,' + btoa(data),
            download: 'Purchaseorder_' + (vendororder.prefix == undefined ? "" : vendororder.prefix) + vendororder.orderId + '.csv'
        }).appendTo('body')
        a[0].click()
        a.remove();
    };
    $scope.orderSearch();
    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) {
            $scope.vendorList = response.data;
        }, function () { });
    };
    $scope.vendor();
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    };
    $scope.printOrder = function (vendororderid) {
        var vendorOrderPrint = removeDeletedItems(vendororderid)
        if (vendorOrderPrint.vendorOrderItem.length == 0) {
            toastr.info('No records available. It seems all items deleted.');            
            return;
        }
        printingHelper.orderPrint(vendororderid.id, customOrder);
    };

    $scope.resendSms = function (vendorOrder) {
        $.LoadingOverlay("show");
        var vendorOrderSms = removeDeletedConvertedItems(vendorOrder)
        if (vendorOrderSms.vendorOrderItem.length == 0) {
            toastr.info('No records available. It seems all items converted/deleted.');
            $.LoadingOverlay("hide");
            return;
        }
        vendorOrderService.resendOrderSms(vendorOrderSms)
        .then(function (resp) {
            $.LoadingOverlay("hide");
            toastr.success('Sms Sent Successfully');
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
            toastr.error("", "Error");
        });
    };
    $scope.resendMail = function (vendorOrder) {
        $.LoadingOverlay("show");
        var vendorOrderEmail = removeDeletedConvertedItems(vendorOrder)
        if (vendorOrderEmail.vendorOrderItem.length == 0) {
            toastr.info('No records available. It seems all items converted/deleted.');
            $.LoadingOverlay("hide");
            return;
        }
        vendorOrderService.resendOrderMail(vendorOrderEmail)
            .then(function (resp) {
                $.LoadingOverlay("hide");
                toastr.success('Mail Sent Successfully');
            }, function (error) {
                $.LoadingOverlay("hide");
                console.log(error);
                toastr.error("", "Error");
            });
    };

    function removeDeletedConvertedItems(vendorOrder) {
        var vendorOrderSms = angular.copy(vendorOrder);
        var vendorOrderItem = [];
        for (var i = 0; i < vendorOrderSms.vendorOrderItem.length; i++) {
            if (vendorOrderSms.vendorOrderItem[i].isDeleted != true && 
                (vendorOrderSms.vendorOrderItem[i].vendorPurchaseId == "" || vendorOrderSms.vendorOrderItem[i].vendorPurchaseId == null
                || vendorOrderSms.vendorOrderItem[i].vendorPurchaseId == undefined) &&
                (vendorOrderSms.vendorOrderItem[i].stockTransferId == "" || vendorOrderSms.vendorOrderItem[i].stockTransferId == null
                || vendorOrderSms.vendorOrderItem[i].stockTransferId == undefined)) {
                vendorOrderItem.push(vendorOrderSms.vendorOrderItem[i]);
            }
        }
        vendorOrderSms.vendorOrderItem = vendorOrderItem;
        return vendorOrderSms;
    };

    $scope.orderEdit = function (vendorOrder) {
        var vendorOrderEdit = removeDeletedConvertedItems(vendorOrder)
        if (vendorOrderEdit.vendorOrderItem.length == 0) {
            toastr.info('Order edit is not allowed since it seems all items converted/deleted.');
            return;
        }
        window.location.href = "/VendorOrder/Update?vendorOrderId=" + vendorOrder.id;
    };

    function removeDeletedItems(vendorOrder) {
        var vendorOrderSms = angular.copy(vendorOrder);
        var vendorOrderItem = [];
        for (var i = 0; i < vendorOrderSms.vendorOrderItem.length; i++) {
            if (vendorOrderSms.vendorOrderItem[i].isDeleted != true) {
                vendorOrderItem.push(vendorOrderSms.vendorOrderItem[i]);
            }
        }
        vendorOrderSms.vendorOrderItem = vendorOrderItem;
        return vendorOrderSms;
    };

    //Added by Sarubala on 20-11-17
    function getVendorOrderSmsSetting() {
        vendorOrderService.getOrderSmsSetting().then(function (response) {
            $scope.showSms = response.data;
        }, function (error) {
            console.log(error);
        });
    };

    getVendorOrderSmsSetting();
    
    $scope.vendorOrderTotal = function (item) {
        var total = 0;

        for (var i = 0; i < item.length; i++)
        {
            total+= (item[i].purchasePrice + item[i].gstTotal) * item[i].quantity;
        }
        return total;
    };
    
});
