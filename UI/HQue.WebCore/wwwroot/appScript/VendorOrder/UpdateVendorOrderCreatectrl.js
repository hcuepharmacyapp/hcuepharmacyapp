app.controller('UpdateVendorOrderCreateCtrl', function ($scope, vendorOrderService, vendorService, toastr, $filter, ModalService) {
    $scope.list = [];
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.init = function (vendororderid) {
        $.LoadingOverlay("show");
        vendorOrderService.getVenodrOrderById(vendororderid)
        .then(function (resp) {
            $scope.list = resp.data;
            var vendorOrderItem = [];
            for (var i = 0; i < $scope.list.vendorOrderItem.length; i++) {
                if ($scope.list.vendorOrderItem[i].isDeleted != true) {
                    if (!($scope.list.vendorOrderItem[i].vendorPurchaseId == "" || $scope.list.vendorOrderItem[i].vendorPurchaseId == null || $scope.list.vendorOrderItem[i].vendorPurchaseId == undefined)) {
                        $scope.list.vendorOrderItem[i].isConverted = true;
                    }
                    if (!($scope.list.vendorOrderItem[i].stockTransferId == "" || $scope.list.vendorOrderItem[i].stockTransferId == null || $scope.list.vendorOrderItem[i].stockTransferId == undefined)) {
                        $scope.list.vendorOrderItem[i].isConverted = true;
                    }
                    vendorOrderItem.push($scope.list.vendorOrderItem[i]);
                }
            }
            $scope.list.vendorOrderItem = vendorOrderItem;
            window.setTimeout(function () {
                var ele = document.getElementById("txtCustomQuantity0");
                if (ele != null) {
                    ele.focus();
                }
            }, 0);
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    };

    $scope.focusCustomTxtpackageQty = function (id, index) {
        var ele = document.getElementById(id + index);
        ele.focus();
    };
    $scope.focustxtCustomsellingPrice = function (id, index) {
        var ele = document.getElementById(id + index);
        ele.focus();
    };
    $scope.focustxtCustomExpireDate = function (id, index) {
        var ele = document.getElementById(id + index);
        ele.focus();
    };
    $scope.OnExpDateSelect = function (expdate, index) {
        index++;
        var ele = document.getElementById("txtCustomQuantity" + index);
        if (ele != null) {
            ele.focus();
        } else {
            document.getElementById("btnCustomSaveSalesOrder").focus();
        }
    };
    $scope.cancelCustomSalesVendor = function () {
        if (confirm("Are you sure, Do you want to cancel ? ")) {
            window.location.href = "/VendorOrder/List";
        }
    };
    $scope.formateDate = function (index, event, id) {
        var expireDate = document.getElementById(id + index).value;
        var key = expireDate.substring(expireDate.length - 1, expireDate.length);

        if (isNaN(key)) {
            if (!(expireDate.length === 3 && key === "/")) {
                $scope.list.vendorOrderItem[index].expireDate = "";
                return;
            }
        }

        if (expireDate.length === 2) {
            if (parseInt(expireDate.substring(0, 2)) > 12) {
                $scope.list.vendorOrderItem[index].expireDate = "";
                return;
            }
        }

        if (expireDate.length === 3) {
            if (expireDate.substring(2, 3) !== "/") {
                $scope.list.vendorOrderItem[index].expireDate = "";
                return;
            }
        }

        if (expireDate.length === 5) {
            var today = $filter('date')(new Date(), 'MM/yy');
            today = parseInt(today.substring(3, 5));
            if (parseInt(expireDate.substring(3, 5)) < today) {
                $scope.list.vendorOrderItem[index].expireDate = expireDate.substring(0, expireDate.length - 1) + "";
                return;
            }
        }

        if (expireDate.length > 5) {
            $scope.list.vendorOrderItem[index].expireDate = expireDate.substring(0, expireDate.length - 1) + "";
        }

        if (expireDate.length == 2) {
            if (!event.ctrlKey && !event.metaKey && (event.keyCode == 32 || event.keyCode > 46))
                $scope.list.vendorOrderItem[index].expireDate = expireDate + "/";
        }
    };

    $scope.dayDiff = function (expireDate) {
        if (expireDate === undefined || expireDate === null || expireDate.length < 5) {
            return true;
        }
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');

        var date2 = new Date($scope.formatString(expireDate));
        var date1 = new Date($scope.formatString(today));

        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));

        if ($scope.dayDifference < 30) {
            return true;
        }
        return false;
    }

    $scope.formatString = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    };

    $scope.saveCustomSalesOrder = function () {

        for (var j = 0; j < $scope.list.vendorOrderItem.length; j++) {
            //chekcing Quantity
            if ($scope.list.vendorOrderItem[j].quantity == "") {
                document.getElementById("txtCustomQuantity" + j).focus();
                alert("Enter Quantity");
                return false;
                break;
            }
            if ($scope.list.vendorOrderItem[j].quantity == 0) {
                document.getElementById("txtCustomQuantity" + j).focus();
                alert("0 Not Allowed");
                return false;
                break;
            }
            //checking SellingPrice
            if ($scope.list.vendorOrderItem[j].sellingPrice == "") {
                document.getElementById("txtCustomSellingPrice" + j).focus();
                alert("Enter Strip Mrp");
                return false;
                break;
            }
            if ($scope.list.vendorOrderItem[j].sellingPrice == 0) {
                document.getElementById("txtCustomSellingPrice" + j).focus();
                alert("0 not Allowed");
                return false;
                break;
            }
            //checking ExpDate
            //if ($scope.list.vendorOrderItem[j].expireDate == "" || $scope.list.vendorOrderItem[j].expireDate == undefined || $scope.list.vendorOrderItem[j].expireDate == null) {
            //    document.getElementById("txtExpDate" + j).focus();
            //    alert("Enter Expiry Date");
            //    return false;
            //    break;
            //}

            //if ($scope.dayDiff($scope.list.vendorOrderItem[j].expireDate)) {
            //    document.getElementById("txtExpDate" + j).focus();
            //    alert("Expire date must be more than 30 days.");
            //    return false;
            //    break;
            //}
        }

        $.LoadingOverlay("show");
        vendorOrderService.update($scope.list).then(function (response) {
            toastr.success('Order Saved Successfully');
            $.LoadingOverlay("hide");
            window.location.href = "/VendorOrder/List";
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.deleteAllClick = function (deleteAll) {
        if ($scope.deleteAll == true) {
            if (!confirm("Are you sure to delete all pending items?")) {
                $scope.deleteAll = false;
                return;
            }
            toastr.info('Pending items only will be deleted from the order by selecting all.');
        }

        for (var i = 0; i < $scope.list.vendorOrderItem.length; i++) {
            if (($scope.list.vendorOrderItem[i].vendorPurchaseId == "" || $scope.list.vendorOrderItem[i].vendorPurchaseId == null || $scope.list.vendorOrderItem[i].vendorPurchaseId == undefined) && ($scope.list.vendorOrderItem[i].stockTransferId == "" || $scope.list.vendorOrderItem[i].stockTransferId == null || $scope.list.vendorOrderItem[i].stockTransferId == undefined)) {
                $scope.list.vendorOrderItem[i].isDeleted = deleteAll;
            }
        }
    }

    $scope.showStockBranchWise = function (productId, productName) {
        var m = ModalService.showModal({
            "controller": "stockBranchWiseCtrl",
            "templateUrl": 'stockBranchWise',
            "inputs": { "tempProduct": { "id": productId, "name": productName } }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {

            });
        });
    };

    $scope.deleteClick = function (item,i) {
        if (!(item.vendorPurchaseId == "" || item.vendorPurchaseId == null || item.vendorPurchaseId == undefined)) {
            item.isDeleted = false;
            toastr.info('Item can not be deleted from order since it is already converted.');
            return;
        }
        if (!(item.stockTransferId == "" || item.stockTransferId == null || item.stockTransferId == undefined)) {
            item.isDeleted = false;
            toastr.info('Item can not be deleted from order since it is already converted.');
            return;
        }
        if (item.isDeleted == true && !confirm("Are you sure to delete?")) {
            item.isDeleted = false;
            return;
        }
        else {
            $scope.list.vendorOrderItem[i].isDeleted = item.isDeleted;
            return;
        }
    }
});

