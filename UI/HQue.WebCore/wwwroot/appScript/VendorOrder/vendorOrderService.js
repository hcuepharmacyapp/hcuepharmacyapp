app.factory('vendorOrderService', function ($http) {
    return {
        "vendorList": function vendorData() {
            return $http.get('/VendorData/VendorList');
        },

        "productList": function drugData() {
            return $http.get('/product/ProductList');
        },

        "create": function (data) {
            return $http.post('/vendorOrder/index', data);
        },

        "update": function (data) {
            return $http.post('/vendorOrder/updateVendorOrderItem', data);
        },

        "vendorOrderList": function (data) {
            return $http.post('/vendorOrder/listData', data);
        },
        //add by nandhini on 24.4.17for order setting page
        getOrderSettings: function () {
            return $http.get('/vendorOrder/getOrderSettings');
        },
        saveOrderSettings: function (data) {
            return $http.post('/vendorOrder/saveOrderSettings', data);
        },
        //
        "getVendorOrder": function (id, poItem, toInstance) {
            return $http.post('/vendorOrder/getVendorOrders?id=' + id + '&PO=' + poItem + '&toInstance=' + toInstance);
        },

        "pendingAuthorisationList": function (data) {
            return $http.post('/vendorOrder/authorisationListData', data);
        },

        "sendAuthoriseData": function (data) {
            return $http.post('/vendorOrder/sendAuthoriseOrder', data);
        },
        "missedList": function (searchData) {
            return $http.post('/vendorOrder/missedList', searchData);
        },

        "cancelOrder": function (id) {
            return $http.get('/vendorOrder/cancelMissedOrder?id=' + id);
        },
        "getQuantityForProduct": function (productid) {
            return $http.post('/vendorOrder/getQuantityForProduct?productid=' + productid);
        },
        "getPreviousOrders": function (productid) {
            return $http.get('/vendorOrder/getPreviousOrders?productid=' + productid);
        },
        "orderBasedSales": function (daysCount) {
            return $http.post('/vendorOrder/GetOrderBasedSales?daysCount=' + daysCount);
        },
        "getVendors": function (vendorname) {
            return $http.post('/vendorOrder/getVendors?vendorname=' + vendorname);
        },
        "getVenodrOrderById": function (vendororderid) {
            return $http.get('/vendorOrder/getVenodrOrderById?vendororderid=' + vendororderid);
        },
        "resendOrderSms": function (vendorOrder) { return $http.post('/vendorOrder/resendOrderSms', vendorOrder); },
        "resendOrderMail": function (vendorOrder) { return $http.post('/vendorOrder/resendOrderMail', vendorOrder); },

        "getProductDetails": function (prodsId, fromDate, toDate, orderType) {
            return $http.post('/vendorOrder/getProductDetails?fromDate=' + fromDate + '&toDate=' + toDate + "&type=" + orderType, prodsId);
        },
        "getHeadOfficeVendor": function () {
            return $http.get('/vendorOrder/getHeadOfficeVendor');
        },
        "ReorderByMinMax": function () {
            return $http.post('/vendorOrder/ReorderByMinMax');
        },
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        //Added by Sarubala on 20-11-17
        getOrderSmsSetting: function () {
            return $http.get('/vendorOrder/getIsOrderCreateSms');
        },
        getInstanceList: function () {
            return $http.get('/vendorOrder/GetInstanceList');
        },
        saveHeadOfficeSetting: function (data) {
            return $http.post('/vendorOrder/saveHeadOfficeSettings', data);
        },
        getHeadVendorId: function () {
            return $http.get('/vendorOrder/GetHeadOfficeVendorId');
        },
        getHeadOfficeOrderSetting: function () {
            return $http.get('/vendorOrder/GetHeadOfficeOrderSettings');
        },

    };
});


