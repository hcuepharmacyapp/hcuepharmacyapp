app.factory('leadsService', function ($http) {
    return {
        list: function (searchData) {
            return $http.post('/LeadsData/listData', searchData);
        },

        updateStatus: function (data) {
            return $http.put('/LeadsData/UpdateStatus', data);
        },

        //vendorData: function () {
        //    return $http.get('/vendor/VendorList');
        //},

        //vendorDetail: function (id) {
        //    return $http.get('/vendor/GetById?id=' + id);
        //}
    }
});