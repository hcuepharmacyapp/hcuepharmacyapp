app.controller('productStockSearchCtrl', function ($scope, productStockService, productModel, pagerServcie, $filter) {

    $scope.minDate = new Date();
    var d = new Date();


    var product = productModel;
    $scope.search = product;
    $scope.list = [];
    $scope.count = null;
    $scope.availableProductsCount = null;

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function toDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] -0, parts[0]);
    };
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.rowParent = [];
    $scope.InActiveCount=0;
    function pageSearch() {
        $.LoadingOverlay("show");
        productStockService.productList($scope.search).then(function (response) {
            $scope.list = response.data.list;

            $scope.options = { Active: 1, InActive: 2 };

            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].status == 2) {
                    //$scope.list[i].status = 'Active';                   
                    $scope.list[i].status = 2;
                   $scope.InActiveCount += 1;
                }
                else {
                    $scope.list[i].status = 1;
                }

                for (var j = 0; j < $scope.list[i].productStock.length; j++) {
                    if ($scope.list[i].productStock[j].status == 2) {
                        //$scope.list[i].status = 'Active';                   
                        $scope.list[i].productStock[j].status = 2;
                        
                    }
                    else {
                        $scope.list[i].productStock[j].status = 1;
                    }
                    var expDate = $filter('date')($scope.list[i].productStock[j].expireDate, 'dd/MM/yyyy');

                    var val = (toDate(expDate) < $scope.minDate);
                    if (val == true) {
                        $scope.list[i].productStock[j].rowHighlight = "rowHighlight";
                        }
                    else {
                        $scope.list[i].productStock[j].rowHighlight = "";
                    }

                }
            }
             $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.productSelected = function () {
        $scope.selectedProduct;
    }

    $scope.$on("update_getValue", function (event, value) {
        $scope.search.name = value;
    });

    $scope.productStockSearch = function (productName, obj) {
        $.LoadingOverlay("show");
        if (productName != undefined && productName != null) {
            $scope.search.name = productName;
        }
        else if ($scope.selectedProduct != null) {
            $scope.search.name = $scope.selectedProduct.title;
        }
        else {
            $scope.search.name = null;
        }

        $scope.search.page.pageNo = 1;
        productStockService.productList($scope.search).then(function (response) {
            $scope.list = response.data.list;

            //$scope.options = [{ name: "Active", id: 1 }, { name: "InActive", id: 2 }];
             $scope.options = { Active: 1,InActive: 2 };

            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].status == 2) {
                    //$scope.list[i].status = 'Active';                   
                    $scope.list[i].status = 2;
                    $scope.InActiveCount += 1;
                }
                else {
                    $scope.list[i].status = 1;
                }
                for (var j = 0; j < $scope.list[i].productStock.length; j++) {
                    if ($scope.list[i].productStock[j].status == 2) {
                        //$scope.list[i].status = 'Active';                   
                        $scope.list[i].productStock[j].status = 2;
                       
                    }
                    else {
                        $scope.list[i].productStock[j].status = 1;
                    }
                    var expDate= $filter('date')($scope.list[i].productStock[j].expireDate, 'dd/MM/yyyy');

                    var val = (toDate(expDate) < $scope.minDate);
                    if (val == true) {
                        $scope.list[i].productStock[j].rowHighlight = "rowHighlight";
                        $scope.rowParent[i]= "rowHighlight";
                    }
                    else {
                        $scope.list[i].productStock[j].rowHighlight = "";
                        $scope.rowParent[i] = "rowHighlight";
                    }
                  
                }
            }

            $scope.count = response.data.noOfRows;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            if ($scope.count > 0) {
                $scope.availableProducts();
            } else {
                $scope.availableProductsCount = 0;
            }
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.productStockSearch();

    $scope.availableProducts = function () {
        var total = 0;
        for (count = 0; count < $scope.list.length; count++) {
            if ($scope.list[count].stock > 0) {
                total += 1;
            }
        }
        $scope.availableProductsCount = total;
    };

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.updateProduct=function(item)
    {
        $.LoadingOverlay("show");
        productStockService.updateProduct(item).then(function (response) {
            $scope.list = response.data.list;
            $scope.productStockSearch();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.updateSingleProductStock = function (item) {
        $.LoadingOverlay("show");
        productStockService.updateSingleProductStock(item).then(function (response) {
            $scope.list = response.data.list;
            $scope.productStockSearch();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
});