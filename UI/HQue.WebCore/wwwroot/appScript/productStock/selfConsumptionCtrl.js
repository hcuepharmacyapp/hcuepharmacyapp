app.controller('selfConsumptionCtrl', function ($scope, $rootScope, $location, $window, cacheService, ModalService, productStockService, productModel, productStockModel, toastr, $filter, productService, selfConsumptionModel) {

    var product = productModel;
    $scope.search = product;
    $scope.list = [];
    $scope.productStockList = [];
    var productStock = productStockModel;    
    var consumption = selfConsumptionModel;
    $scope.consumption = consumption;
    $scope.consumption.productStock = productStock;
    $scope.consumption.productStock.product = null;
    $scope.selectedProduct = null;

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.minDate = new Date();
        
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,

    };      
    

    $scope.checkQty = function (val1) {

        if (parseFloat($scope.consumption.productStock.stock) < parseFloat(val1))
            $scope.selfConsumption.consumption.$setValidity("checkQtyError", false);
        else
            $scope.selfConsumption.consumption.$setValidity("checkQtyError", true);
    };

    $scope.checkAllfields = function (ind) {

        if (ind > 1) {
            if ($scope.selectedProduct == undefined || $scope.selectedProduct == null) {
                var ele = document.getElementById("productName1");
                ele.focus();
                return false;
            }
        }               
        
    };

    $scope.checkEditQty = function (val1, index) {
        if (val1 != undefined && val1 != "") {            
            if (parseFloat($scope.productStockList[index].productStock.tempQty) < parseFloat(val1)) {
                $scope.productStockList[index].productStock.stock = $scope.productStockList[index].productStock.tempQty;                
            }                
            else {
                $scope.productStockList[index].productStock.stock = $scope.productStockList[index].productStock.tempQty - parseFloat(val1);                
            }            
        }
        else {
            $scope.productStockList[index].productStock.stock = $scope.productStockList[index].productStock.tempQty;            
        }       
            
    }


    $scope.submit = function () {
        if ($scope.consumption.productStock.batchNo != null && $scope.selfConsumption.$valid && ($scope.consumption.consumption != '' && $scope.consumption.consumption != "0")) {
            $scope.addProducts();
            var ele = document.getElementById("productName1");
            ele.focus();
        }
       
    };

    $scope.onProductSelect1 = function () {

        $scope.consumption.consumption = '';
        $scope.consumption.productStock = {};
        $scope.consumption.productStock.product = {};
        $scope.selfConsumption.$setPristine();

        productStockService.productBatch($scope.selectedProduct.product.id).then(function (response) {
            $scope.batchList2 = response.data;

            for (var x = 0; x < $scope.productStockList.length; x++) {
                for (var y = 0; y < $scope.batchList2.length; y++) {
                    if ($scope.productStockList[x].productStock.productStockId == $scope.batchList2[y].id) {
                        $scope.batchList2.splice(y, 1);
                    }
                }
            }
            
            if ($scope.batchList2.length <= 1) {               

                if ($scope.batchList2.length == 0) {
                    var ele = document.getElementById("productName1");
                    ele.focus();
                }
                else if ($scope.batchList2.length == 1) {                    
                    $scope.batchList2[0].productStockId = $scope.batchList2[0].id;
                    $scope.batchList2[0].id = null;                        
                    
                    var product1 = $scope.batchList2[0];
                    if (product1 != null) {
                        var tempProduct = $scope.selectedProduct.product;
                        $scope.consumption.productStock = product1;
                        $scope.consumption.productStock.product = tempProduct;
                        $scope.consumption.productStock.packageSellingPrice = parseFloat($scope.consumption.productStock.packageSize) * parseFloat($scope.consumption.productStock.sellingPrice);
                        $scope.consumption.productStock.packageMRP = parseFloat($scope.consumption.productStock.packageSize) * parseFloat($scope.consumption.productStock.mrp);
                        if ($scope.consumption.productStock.purchasePrice > 0) {
                            var tempAmount = parseFloat($scope.consumption.productStock.purchasePrice) * parseFloat($scope.consumption.productStock.packageSize);
                            $scope.consumption.productStock.packagePurchasePrice = tempAmount;
                        }
                    }
                    
                    $scope.batchList = $scope.batchList2;
                    
                    dayDiff($scope.consumption.productStock.expireDate);
                    $scope.DisableProduct = true;
                    var ele = document.getElementById("consumption");
                    ele.focus();
                }
            }
            else {
              
                $scope.ProductDetails();
            }

        }, function () {
        });                
    };

    $scope.ProductDetails = function () {
        $scope.enablePopup = true;     
        cacheService.set('selectedProduct1', $scope.selectedProduct.product)
        var m = ModalService.showModal({
            controller: "productBatchSearchCtrl",
            templateUrl: 'productBatch'
            , inputs: {
                productId: $scope.selectedProduct.product.id,
                productName: $scope.selectedProduct.product.name,
                items: $scope.productStockList,
                enableWindow: $scope.enablePopup
            }
        }).then(function (modal) {
            modal.element.modal();

            $scope.DisableProduct = true;
            modal.close.then(function (result) {             
                $scope.message = "You said " + result;
            });
        });
        return false;
    };

    $rootScope.$on("productDetail", function (data) {      
        $scope.productSelection();
    });

    $rootScope.$on("productDetailCancel", function (data) {
        $scope.DisableProduct = false;
        var prod = document.getElementById("productName1");
        prod.focus();
    });

    $scope.productSelection = function () {
      
        var product1 = cacheService.get('selectedProduct1');
        var blist = cacheService.get('batchList1');     
        $scope.enablePopup = cacheService.get('enableWindow1');
        if (product1 != null) {
            var tempProduct = $scope.selectedProduct.product;
            $scope.consumption.productStock = product1;
            $scope.consumption.productStock.product = tempProduct;
            $scope.consumption.productStock.packageSellingPrice = parseFloat($scope.consumption.productStock.packageSize) * parseFloat($scope.consumption.productStock.sellingPrice);
            //MRP Implementation by Settu
            $scope.consumption.productStock.packageMRP = parseFloat($scope.consumption.productStock.packageSize) * parseFloat($scope.consumption.productStock.mrp);
            if ($scope.consumption.productStock.purchasePrice > 0) {
                var tempAmount = parseFloat($scope.consumption.productStock.purchasePrice) * parseFloat($scope.consumption.productStock.packageSize);
                $scope.consumption.productStock.packagePurchasePrice = tempAmount;
            }
        }
        if (blist.length > 0) {
            $scope.batchList = blist;
        }

        $scope.DisableProduct = true;
        dayDiff($scope.consumption.productStock.expireDate);
      
        var qty = document.getElementById("consumption");
        qty.focus();
    }


    function dayDiff(expireDate) {
        $scope.highlight = "";
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');

        var date2 = new Date(formatString(expireDate));
        var date1 = new Date(formatString(today));
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));

        if ($scope.dayDifference < 30) {
            var dt = expireDate;
            $scope.highlight = "Expiry";
        }
        else {
            $scope.highlight = "";
        }
    }

    function formatString(format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    }

    $scope.isEdit = 0;

    $scope.editStock = function(stock1, pos1) {
        stock1.isEdit = true;
        $scope.isEdit += 1;
        var ele = document.getElementById("consumption1-"+pos1);
        ele.focus();
    };
    $scope.checkquantity = function (stock1, pos1) {
        if (stock1.consumption != undefined && parseFloat(stock1.consumption) != 0) {
            stock1.isEdit = false;
            $scope.saveStock(stock1, pos1);
        } else {
            stock1.isEdit = true;
        }
    };
    $scope.saveStock = function (stock1, pos1) {
        stock1.isEdit = false;
        if (parseFloat(stock1.consumption) >= 0) {
            if (stock1.consumption <= $scope.productStockList[pos1].productStock.tempQty) {
                $scope.productStockList[pos1].productStock.stock = stock1.productStock.stock;
                $scope.productStockList[pos1].consumption = stock1.consumption;
            }
            else {
                $scope.productStockList[pos1].productStock.stock = $scope.productStockList[pos1].productStock.tempQty;
                $scope.productStockList[pos1].consumption = 0;
            }
        }
        else {
            $scope.productStockList[pos1].productStock.stock = $scope.productStockList[pos1].productStock.tempQty;
            $scope.productStockList[pos1].consumption = 0;
        }
        
        $scope.isEdit -= 1;        
        var ele = document.getElementById("productName1");
        ele.focus();
    }

    $scope.removeStock = function (stock1, pos1) {
        if (!confirm("Are you sure, Do you want to delete ? "))
        {
            var ele = document.getElementById("productName1");
            ele.focus();
            return;
        }
        if (stock1.isEdit == true) {
            $scope.isEdit -= 1;
        }
        $scope.productStockList.splice(pos1, 1);
        var ele = document.getElementById("productName1");
        ele.focus();
    }
   

    $scope.DisableProduct = false;
    $scope.addProducts = function () {
      

        $scope.consumption.productStock.tempQty = $scope.consumption.productStock.stock;
        $scope.consumption.productStock.stock = parseFloat($scope.consumption.productStock.stock) - parseFloat($scope.consumption.consumption);
        $scope.consumption.productStock.id = $scope.consumption.productStock.productStockId;
        $scope.consumption.productStockId = $scope.consumption.productStock.productStockId;

        if ($scope.consumption.consumptionNotes == null || $scope.consumption.consumptionNotes == undefined) {
            $scope.consumption.consumptionNotes = "";
        }
         console.log($scope.consumption.consumptionNotes);
        $scope.consumption.isEdit = false;
       
        var item1 = $scope.consumption;
       
        $scope.productStockList.push(angular.copy(item1));

        $scope.DisableProduct = false;
        $scope.consumption = {};
        $scope.consumption.productStock = {};
        $scope.consumption.productStock.product = {};
        $scope.selectedProduct = null;
        $scope.selfConsumption.$setPristine();
    
        var ele = document.getElementById("productName1");        
        ele.focus();
    };

    $scope.save = function () {        
        $scope.temp = [];
        $.LoadingOverlay("show");        
        
        for (var x = 0; x < $scope.productStockList.length; x++) {
            $scope.temp.push($scope.productStockList[x]);
        }
        productStockService.saveStockConsumption($scope.temp).then(function (response) {
            $scope.productStockList = response.data;            
            $scope.consumption = {};
            $scope.consumption.productStock = {};
            $scope.consumption.productStock.product = {};
            $scope.selectedProduct = null;
            $scope.productStockList = [];
            $scope.selfConsumption.$setPristine();            
            toastr.success('Consumption Saved successfully');
            $scope.isEdit = 0;
            var ele = document.getElementById("productName1");
            ele.focus();
            $.LoadingOverlay("hide");       

        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });        
    }

    $scope.clear = function () {        
        $scope.consumption.consumption = '';
        $scope.consumption.productStock = {};
        $scope.consumption.productStock.product = {};
        $scope.selectedProduct = null;
        $scope.selfConsumption.$setPristine();
        $scope.DisableProduct = false;
        var ele = document.getElementById("productName1");
        ele.focus();
    }

    $scope.getProducts = function (val) {
        return productStockService.drugFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    //MRP Implementation by Settu
    $scope.getEnableSelling = function () {
        productStockService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.getEnableSelling();

});

app.directive('focusBox', function () {
    return {
        "link": function (scope, element, attrs) {

            element.bind("keydown", function (event) {                
                var valueLength = attrs.$$element[0].value.length;                
                //Enter 
                if (attrs.id != "consumption") {
                    if (event.which === 13) {
                        if (valueLength !== 0) {
                            ele = document.getElementById(attrs.nextid);
                            ele.focus();
                        }
                    }
                }
                //BackSpace
                if (event.which === 8) {
                    if (valueLength === 0) {
                        ele = document.getElementById(attrs.previousid);
                        ele.focus();
                        event.preventDefault();
                    }
                }
            });
        }
    };
});

app.directive('numbersOnly', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});


