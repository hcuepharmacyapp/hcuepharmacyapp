app.controller('minMaxReorderCtrl', function ($scope, toastr, $filter, close, ModalService, productService) {

    $scope.open1 = function () {
        var dateVal = $("#fromDate").val();
        if (isValidDate(dateVal) == false || dateVal.length < 10)
            $scope.from = "";
        $scope.popup1.opened = true;
    };
    $scope.focusInput = true;
    
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
    var today = new Date();

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;
    $scope.maxDate = new Date();

    $scope.checkDateChar = function (val) {
        var actualKey = val.key;
        val.key = val.key.replace(/[^0-9/]+/g, '');
        if(actualKey != val.key)
            event.preventDefault();
    }
    
    $scope.checkFromDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#fromDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                var bits = dt.split('/');
                var userDate = new Date(bits[2], bits[1] - 1, bits[0]);
                if (isValidDate(dt) && userDate <= today) {
                    $scope.validFromDate = true;

                    if (toDate != undefined && toDate != null) {
                        $scope.checkToDate1(frmDate, toDate);
                    }
                }
                else {
                    $scope.validFromDate = false;
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else
            $scope.validFromDate = true;
    }

    $scope.checkToDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#toDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                var bits = dt.split('/');
                var userDate = new Date(bits[2], bits[1] - 1, bits[0]);
                if (isValidDate(dt) && userDate <= today) {
                    $scope.validToDate = true;
                    $scope.checkToDate1(frmDate, toDate);
                }
                else {
                    $scope.validToDate = false;
                }
            }
            else {
                $scope.validToDate = false;
            }
        }
        else
            $scope.validToDate = true;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        var dateVal = $("#toDate").val();
        if (isValidDate(dateVal) == false || dateVal.length < 10)
            $scope.to = "";
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.minReorderFactor = null;
    $scope.maxReorderFactor = null;

    $scope.reorderMinMaxBulkUpdate = function () {
        $.LoadingOverlay("show");
        $scope.isSaveProcessing = true;
        productService.reorderMinMaxBulkUpdate($scope.from, $scope.to, $scope.minReorderFactor, $scope.maxReorderFactor).then(function (response) {
            $scope.isSaveProcessing = false;
            if (response.data == true) {
                toastr.success("Min & Max Reorder Qty updated successfully.");
                close('No', 100);
                $(".modal-backdrop").hide();
            }
            else
                toastr.error("Min & Max Reorder Qty update failed.");
                $.LoadingOverlay("hide");
        }, function () {
            $scope.isSaveProcessing = false;
            $.LoadingOverlay("hide");
        });
    }

    $scope.validateMinMax = function () {
        if (parseInt($scope.minReorderFactor) >= parseInt($scope.maxReorderFactor))
            return false;
        else
            return true;
    };
    
    $scope.focusToNextCtrl = function (currentId, nextId) {
        if (document.getElementById(currentId).value != "")
            document.getElementById(nextId).focus();
    }
    
    $scope.showFormulaInfo = false;
    $scope.showHideFormula = function () {
        $scope.showFormulaInfo = !$scope.showFormulaInfo;
    }

});