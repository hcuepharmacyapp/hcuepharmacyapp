app.controller('ProductMasterCtrl', function ($scope, $filter, toastr, productService, productModel, pagerServcie, ModalService,salesService) {

    var product = productModel;

    $scope.search = product;
    $scope.GSTEnabled = true;
    $scope.list = [];
    $scope.subcatogoryList = [];
  
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination
    $scope.init = function()
    {
       
        return productService.subcategoryFilterData($scope.subcatid).then(function (response) {
            return response.data.map(function (item) {
                $scope.subcatogoryList.push(item);
                return item;

                   
                   
            });
        });
    }

    $scope.getProducts = function (val) {

        return productService.drugFilterData(val).then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }

            return output.map(function (item) {
                return item;
            });
        });
    }

    $scope.PopupAddNewProduct = function () {
        var m = ModalService.showModal({
            "controller": "productCreateCtrl",
            "templateUrl": "createProduct",
            "inputs": {
                "mode": "Create",
                "poproductname": '',
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();
            
        });
    };
    $scope.getTaxValues = function () {
        productService.getTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.init();
    $scope.getTaxValues();
    $scope.kindNameOptions = [];

    $scope.getPaymentTypes = function () {
        salesService.getPaymentDomainValues().then(function (response) {
            if (response.data.length > 0) {
                $scope.domainValueList = response.data;

                angular.forEach($scope.domainValueList, function (value, key) {
                    if (value.domainId.replace(/\s+/g, '') == "5") {
                        $scope.kindNameOptions.push(value);
                    }
                });
            }

        }, function (error) {
            console.log(error);
        })
    }

    $scope.getPaymentTypes();
    //$scope.kindNames = [
    //        { value: 'OTC', label: 'OTC' },
    //       { value: 'Prescription', label: 'Prescription' },
    //       { value: 'FMCG', label: 'FMCG' },
    //       { value: 'F&B', label: 'F&B' }
    //];
    function pageSearch() {
        productService.Masterlist($scope.search).then(function (response) {
            $scope.list = response.data.list;
            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].status == 1 || $scope.list[i].status == undefined || $scope.list[i].status == null) {
                    $scope.list[i].status = 'Active';
                }
                else {
                    $scope.list[i].status = 'InActive';
                }
            }
            $scope.selectedIndex = -1;
        }, function () { });
    }
   
    $scope.Entirelist = [];
    $scope.ClicktoDownload = function () {
        $.LoadingOverlay("show");

        $scope.search.page.pageNo = 1;
        productService.EntireMasterlist($scope.search).then(function (response) {
            console.log(JSON.stringify(response.data));
            $scope.Entirelist = response.data.list;
            //$scope.exportList = $scope.list.name;

            for (var i = 0; i < $scope.Entirelist.length; i++) {
                if ($scope.Entirelist[i].status == 1) {
                    $scope.Entirelist[i].status = 'Active';
                }
                else {
                    $scope.Entirelist[i].status = 'InActive';
                }

            }
            $.LoadingOverlay("hide");

          
           
          
        }, function () {
            $.LoadingOverlay("hide");
        });
        angular.element('#ExportID').trigger('click');
    };

    $scope.getData = function () {
        productService.Masterlist($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    };

    //Vat validation added by nandhini  on 18/05/2017
    $scope.vatErrorMessage = false;
    $scope.changeVat = function (vat) {
        if (vat != "" || vat != undefined) {
            if (vat > 100) {
                $scope.vatErrorMessage = true;
            } else {
                $scope.vatErrorMessage = false;
            }
        }
    };

    //GST related starts

    //TFS Id : 647 - make as readonly the gst fileds
    
    $scope.gstTotalErrorMessage = false;
    $scope.changeGSTTotal = function () {
        var gstTotal = $scope.productsItem.gstTotal;
        if (gstTotal != null && gstTotal != ""  ) {
            if (gstTotal >= 100) {
                $scope.gstTotalErrorMessage = true;
            } else {
                $scope.gstTotalErrorMessage = false;
                var gst = gstTotal / 2;
                $scope.productsItem.sgst = gst;
                $scope.productsItem.cgst = gst;
                $scope.productsItem.igst = gstTotal;
            }
        }
        else
        {
            $scope.productsItem.sgst = 0;
            $scope.productsItem.cgst = 0;
            $scope.productsItem.igst = 0;
        }
    };

    //GST related ends
    $scope.onProductSelect = function (obj) {
        $scope.productId = obj.id;
        $scope.search.drugName = obj.name;
    }
    $scope.productSearch = function (productName) {
        $.LoadingOverlay("show");
        $scope.selectedIndex = -1;
        if (productName != undefined && productName != null) {
            $scope.search.name = productName;
        }
        else if ($scope.selectedProduct != null) {
            $scope.search.name = $scope.selectedProduct.title;
        }
        else {
            $scope.search.name = null;
        }


        $scope.search.page.pageNo = 1;
        productService.Masterlist($scope.search).then(function (response) {
          //  console.log(JSON.stringify(response.data));
            $scope.list = response.data.list;
            //$scope.exportList = $scope.list.name;

            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].status == 1 || $scope.list[i].status == undefined || $scope.list[i].status == null) {
                    $scope.list[i].status = 'Active';
                    $scope.list[i].schedule = ($scope.list[i].schedule == '' || $scope.list[i].schedule == undefined) ? '' : $scope.list[i].schedule;
                }
                else {
                    $scope.list[i].status = 'InActive';
                    $scope.list[i].schedule = ($scope.list[i].schedule == '' || $scope.list[i].schedule == undefined) ? '' : $scope.list[i].schedule;
                }

            }
           
           //&& !abort;
               
            //for (j = 0; j < $scope.subcatogoryList.length; j++) {
               
            //    for (i = 0; i < $scope.list.length ; i++) {
            //            if ($scope.subcatogoryList[j].domainId !== undefined && $scope.subcatogoryList[j].domainValue == $scope.list[i].subcategory) {
            //                $scope.list[i].subcategory = $scope.subcatogoryList[j].displayText;
            //               // abort = true;
            //            }
            //        }
            //    }
             

            //angular.forEach($scope.list, function (item) {
            //    angular.forEach($scope.subcatogoryList, function (doc) {
            //        if (isset == false && doc.domainId !== undefined && doc.domainValue == item.subcategory) {
                      
            //            item.subcategory = doc.displayText;
                    
            //            }
                    
            //    });
            //});

            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }



    $scope.productSearch();

    function showDetails(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $scope.editProducts(dataItem);
    }


    $scope.editProducts = function (item, index) {
        //$scope.showPanel = true;
        //for (var i = 0; i < $scope.list.length; i++) {
        //    if (i == index) {
        //        $scope.indexResult = true;
        //    }
        //    //else {
        //    //    $scope.indexResult = false;
        //    //}
        //}
        $scope.selectedIndex;

        $scope.productsItem = JSON.parse(JSON.stringify(item));
        var ngst = $filter("filter")($scope.taxValuesList, { "tax": $scope.productsItem.gstTotal }, true);

        if (ngst.length == 0) {
            $scope.productsItem.gstTotal = null;
          
        }
        //if (!$scope.productsItem.status)
        //    $scope.productsItem.status = 2
        if ($scope.productsItem.status == 'Active')
            $scope.productsItem.status = 1
        else
            $scope.productsItem.status = 2

        $scope.categories = [
           { CategoryID: 1, Name: 'Active' },
          { CategoryID: 2, Name: 'Inactive' },
        ];

        ////$scope.productsItem.kindName = 'OTC';
        //$scope.kindNames = [
        //  { value: 'OTC', label: 'OTC' },
        // { value: 'Prescription', label: 'Prescription' },
        // { value: 'FMCG', label: 'FMCG' },
        // { value: 'F&B', label: 'F&B' }
        //];

        $scope.scheduleList = [
           { schedule: 'H', label: 'H' },
          { schedule: 'H1', label: 'H1' },
          //{ schedule: 'NON-SCHEDULED', label: 'NON-SCHEDULED' },
          { schedule: 'X', label: 'X' }
        ];
        $scope.commodityCodeList = [
            { commodityCode: '2044', label: '2044' },
           { commodityCode: '2081', label: '2081' },
           { commodityCode: '301', label: '301' },
           { commodityCode: '752', label: '752' }
        ];

        $scope.catogoryList = [
            { category: 'LIQUID', label: 'LIQUID' },
           { category: 'POWDER', label: 'POWDER' },
           { category: 'GENERAL', label: 'GENERAL' },
           { category: 'INJECTION', label: 'INJECTION' },
            { category: 'INHALER', label: 'INHALER' },
            { category: 'OINTMENTS', label: 'OINTMENTS' },
            { category: 'DROPS', label: 'DROPS' },
            { category: 'SYRUP', label: 'SYRUP' },
            { category: 'SURGICAL', label: 'SURGICAL' },
            { category: 'FMCG', label: 'FMCG' },
            { category: 'TABLET & CAPSULE', label: 'TABLET & CAPSULE' },
            { category: 'REFREGERATOR', label: 'REFREGERATOR' },

        ];

        //Added by arun for subcategory
        $scope.subcatid = 3;
        //productService.subCategoryList($scope.subcatid).then(function (response) {
        //    $scope.list = response.data.list;
        //});
        
        //Ended by arun for subcategory
       
        $scope.categoryField = $scope.categories[$scope.productsItem.status - 1];
        $scope.productsItem.status = $scope.categoryField.CategoryID;

        //$scope.productsItem.kind = $scope.kindName;
        var productName = document.getElementById("productName");
        $scope.changeGSTTotal();
        productName.focus();
     

    }

    $scope.updateProduct = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true; 
       

        if ($scope.productsItem.genericName != null && $scope.productsItem.genericName.genericName !== undefined)
            $scope.productsItem.genericName = $scope.productsItem.genericName.genericName;

        if ($scope.productsItem.manufacturer != null && $scope.productsItem.manufacturer.manufacturer !== undefined)
            $scope.productsItem.manufacturer = $scope.productsItem.manufacturer.manufacturer;

        if ($scope.productsItem.schedule != null && $scope.productsItem.schedule.schedule !== undefined)
            $scope.productsItem.schedule = $scope.productsItem.schedule.schedule;

        if ($scope.productsItem.type != null && $scope.productsItem.type.type !== undefined)
            $scope.productsItem.type = $scope.productsItem.type.type;

        if ($scope.productsItem.category != null && $scope.productsItem.category.category !== undefined)
            $scope.productsItem.category = $scope.productsItem.category.category;

        if ($scope.productsItem.subcategory != null && $scope.productsItem.subcategory.displayText !== undefined)
            $scope.productsItem.subcategory = $scope.productsItem.subcategory.domainValue;
      

        if ($scope.productsItem.commodityCode != null && $scope.productsItem.commodityCode.commodityCode !== undefined)
            $scope.productsItem.commodityCode = $scope.productsItem.commodityCode.commodityCode;

        if ($scope.productsItem.kindName != null && $scope.productsItem.kindName.value !== undefined)
            $scope.productsItem.kindName = $scope.productsItem.kindName.value;

        if ($scope.productsItem.packageSize == undefined || $scope.productsItem.packageSize == null) {
            var ele = document.getElementById("packaging");
            ele.focus();
            return false;
        }
        productService.update($scope.productsItem).then(function (response) {
            $scope.productSearch();
            toastr.success('Product updated successfully');
            $scope.selectedIndex = -1;
            //$scope.productForm.$setPristine();
            //$scope.showPanel = false;
            $scope.productsItem = {};
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Product already exist', 'Error');
            $scope.isProcessing = false;
        });
    }

    $scope.cancel = function () {
        //$scope.productForm.$setPristine();
        //$scope.productsItem = {};
        //$scope.showPanel = false;
        $scope.selectedIndex = -1;
    }

    //$scope.keyEnter = function (event, e) {
    //    var ele = document.getElementById(e);
    //    if (event.which === 13) // Enter key
    //    {
    //        if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
    //        {
    //            ele.focus();
    //            if (ele.nodeName != "BUTTON")
    //                ele.select();
    //        }
    //    }
    //};

    $scope.focusNextField = function (nextid) {
        var ele = document.getElementById(nextid);
        ele.focus();
    };



    $scope.getGeneric = function (val) {
        return productService.genericFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };




    $scope.getManufacturer = function (val) {

        return productService.manufacturerFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.getSchedule = function (val) {

        return productService.scheduleFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getType = function (val) {

        return productService.typeFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getCategory = function (val) {

        return productService.categoryFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }
    $scope.getSubCategory = function () {
        $scope.subcatogoryList = [];
        $scope.subcatid = 3;
        return productService.subcategoryFilterData($scope.subcatid).then(function (response) {
            return response.data.map(function (item) {
               $scope.subcatogoryList.push(item);
                return item;
              
            });
        });
    }

    $scope.getCommodity = function (val) {

        return productService.commodityFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }



    $scope.getKindName = function (val) {

        return productService.kindNameFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    function exportList() {
        productService.exportList($scope.search).then(function (response) {
            $scope.exportList = response.data.list;

        }, function () { });
    }
 
    $scope.minMaxBulkUpdate = function () {
        var m = ModalService.showModal({
            "controller": "minMaxReorderCtrl",
            "templateUrl": 'minMaxReorderBulkUpdate'
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                
            });
        });
    };

});

app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});
