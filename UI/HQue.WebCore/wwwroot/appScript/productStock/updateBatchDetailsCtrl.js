app.controller('updateBatchDetailsCtrl', function ($scope, $rootScope, $location, $window, toastr, productStockService, cacheService, productModel, ModalService, $filter, productId, productName, close) {
            
    $scope.selectedProduct = null;    
    $scope.Math = window.Math;    
        
    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    
    $scope.cancel = function () {
        if (window.confirm('Are you sure, Do you want to cancel?')) {
            window.location = window.location.origin + window.location.pathname;
        }        
    }    

    $scope.isSelectedItem = 1;
    $scope.shouldBeOpen = true;
    $scope.selectRow = function (val) {
        $scope.isSelectedItem = ($scope.isSelectedItem == val)? null:val;
        console.log(val);
    };    
         
    
    $scope.keyPress = function (e) {
        
        if (e.keyCode == 38) {
            console.log($scope.isSelectedItem);
            if ($scope.isSelectedItem == 1) {
                //return;                
                $scope.isSelectedItem = ($scope.batchList1.length) + 1;
            }
            $scope.isSelectedItem--;
            e.preventDefault();
            //if (($scope.isSelectedItem > 10) && ($scope.isSelectedItem % 2 == 0)) {

            //}
            //else {
            //    e.preventDefault();
            //}
                                            
            console.log($scope.isSelectedItem);
        }
        if (e.keyCode == 40) {
            console.log($scope.isSelectedItem);
            if ($scope.isSelectedItem == $scope.batchList1.length) {
                // return;                                
                $scope.isSelectedItem = 0;
            }
            $scope.isSelectedItem++;
            e.preventDefault();            
            //if (($scope.isSelectedItem > 10) && ($scope.isSelectedItem % 2 == 0)) {

            //}
            //else {
            //    e.preventDefault();
            //}

            console.log($scope.isSelectedItem);
        }
        if (e.keyCode == 13) {
            $scope.submit($scope.batchList1[$scope.isSelectedItem - 1]);
        }
        if (e.keyCode == 27) {
            $scope.close('No');
        }
        
    }
 

    $scope.addDetail = function (val) {
        $scope.submit($scope.batchList1[val - 1]);
    }
    
    $scope.submit = function (selectedProduct) {        
        $scope.close('Yes');
        $.LoadingOverlay("show");
        cacheService.set('selectedProduct1', selectedProduct);        
        $rootScope.$emit("productDetail", selectedProduct);        
        $.LoadingOverlay("hide");
    };

    $scope.close = function (result) {        
        close(result, 100); // close, but give 100ms for bootstrap to animate 
        if (result == "No") {
            $rootScope.$emit("productDetailCancel", null);
        }
        $(".modal-backdrop").hide();
    };


    $("#SampleID").focus();
    function getSalesObject() {
        return {
            salesItem: [],
            discount: 0,
            total: 0,
            TotalQuantity: 0,
            rackNo: "",
            vat: 0,
            cashType: "Full",
            paymentType: "Cash",
            deliveryType: "Counter",
            billPrint: true,
            sendEmail: false,
            sendSms: false,
            doctorMobile: null,
            credit: null,
            cardNo: null,
            cardDate: null,
            cardDigits: null,
            cardName: null,
            isCardSelected: false,
            isCreditEdit: false,
            changeDiscount: "",
        }
    }   


    if (productId != null) {
        $scope.selectedProductName = productName;
        productStockService.getActiveBatch(productId).then(function (response) {
            $scope.batchList1 = response.data;             
            
        }, function () {
        });
    }

    //MRP Implementation by Settu
    $scope.getEnableSelling = function () {
        productStockService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.getEnableSelling();

});


app.directive('focusMe', function ($timeout, $parse) {
    return {
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                console.log('value=', value);
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();                        
                    });
                }
            });
            element.bind('blur', function () {
                console.log('blur')
                scope.$apply(model.assign(scope, false));
            })
        }
    };
});




