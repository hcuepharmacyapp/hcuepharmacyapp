app.controller('productBatchSearchCtrl', function ($scope, $rootScope, $location, $window, toastr, salesService, productStockService, cacheService, productModel, ModalService, $filter, productId, productName, items, close) {
        
    $scope.batchList1 = [];
    $scope.selectedProduct = null;
    $scope.selectedBatch = { reminderFrequency: "0" };
    $scope.Math = window.Math;

    $scope.sales = getSalesObject();    
    var totalQuantity = 0;   
    
    $scope.onProductSelect = function () {
        loadBatch(null);
    };
    $scope.getAvailableStock = function (productStock, editBatch) {
        var addedItem = getAddedItem(productStock.productStockId);
        var newAddedQty = getAddedItemQuantity(addedItem);

        if (editBatch == null)
            return productStock.stock - newAddedQty;
        else if (editBatch.id != null && productStock.productStockId == editBatch.productStockId)
            return productStock.stock + parseInt(editBatch.quantity) - newAddedQty;
        else if (editBatch != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock - (newAddedQty - parseInt(editBatch.quantity));
        }
        else if (editBatch != null && productStock.productStockId != editBatch.productStockId) {
            return productStock.stock - newAddedQty;
        }
        return productStock.stock;
    };    


    function getAddedItemQuantity(addedItems) {
        var qty = 0;
        for (var i = 0; i < addedItems.length; i++) {
            if (addedItems[i].id != null) {
                qty += parseInt(addedItems[i].quantity) - parseInt(addedItems[i].orginalQty)
            }
            else {
                qty += parseInt(addedItems[i].quantity)
            }
        }
        return qty;
    }

    function getAddedItem(id) {
        var addedItems = [];
        for (var i = 0; i < items.length; i++) {
            if (items[i].productStockId == id)
                addedItems.push(items[i]);
        }
        return addedItems;
    };
    function getAddedItems(id, sellingPrice, discount) {

        for (var i = 0; i < items.length; i++) {

            if (items[i].productStockId == id && items[i].sellingPrice == sellingPrice && items[i].discount == discount) {
                return items[i];
            }

        }
        return null;
    };
    $scope.editId = 1;


    $scope.batchSelected = function (batch) {
        $scope.valPurchasePrice = "";
        var totalQuantity = 0;
        $scope.selectedBatch = $scope.batchList[batch];
        for (var i = 0; i < $scope.batchList.length; i++) {
            totalQuantity += parseInt($scope.batchList[i].stock);
        }
        $scope.selectedBatch.totalQuantity = totalQuantity;
        $scope.selectedBatch.discount = 0;
        dayDiff($scope.selectedBatch.expireDate);
    }

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    
    $scope.cancel = function () {
        if (window.confirm('Are you sure, Do you want to cancel?')) {
            window.location = window.location.origin + window.location.pathname;
        }
        $scope.discountInValid = false;
    }    

    $scope.isSelectedItem = 1;
    $scope.shouldBeOpen = true;
    $scope.selectRow = function (val) {
        $scope.isSelectedItem = ($scope.isSelectedItem == val)? null:val;
        console.log(val);
    };    
         
    
    $scope.keyPress = function (e) {
        
        if (e.keyCode == 38) {
            console.log($scope.isSelectedItem);
            if ($scope.isSelectedItem == 1) {
                //return;
                $scope.isSelectedItem = ($scope.batchList1.length) + 1;
            }
            $scope.isSelectedItem--;
            e.preventDefault();
            console.log($scope.isSelectedItem);
        }
        if (e.keyCode == 40) {
            console.log($scope.isSelectedItem);
            if ($scope.isSelectedItem == $scope.batchList1.length) {
                // return;
                $scope.isSelectedItem = 0;
            }
            $scope.isSelectedItem++;
            e.preventDefault();
            console.log($scope.isSelectedItem);
        }
        if (e.keyCode == 13) {            
            $scope.submit($scope.batchList1[$scope.isSelectedItem - 1]);
        }
        if (e.keyCode == 27) {
            $scope.close('No');
        }
        
    }
 

    $scope.addDetail = function (val) {
        $scope.submit($scope.batchList1[val - 1]);
    }
    
    $scope.submit = function (selectedProduct) {
        selectedProduct.totalQuantity = totalQuantity;
        $scope.close('Yes');
        $.LoadingOverlay("show");
        cacheService.set('selectedProduct1', selectedProduct);
        cacheService.set('batchList1', $scope.batchList1);        
        $rootScope.$emit("productDetail", selectedProduct);        
        $.LoadingOverlay("hide");
    };

    $scope.close = function (result) {        
        close(result, 100); // close, but give 100ms for bootstrap to animate 
        if (result == "No") {
            $rootScope.$emit("productDetailCancel", null);
        }
        $(".modal-backdrop").hide();
    };

    function getSalesObject() {
        return {
            salesItem: [],
            discount: 0,
            total: 0,
            TotalQuantity: 0,
            rackNo: "",
            vat: 0,
            cashType: "Full",
            paymentType: "Cash",
            deliveryType: "Counter",
            billPrint: true,
            sendEmail: false,
            sendSms: false,
            doctorMobile: null,
            credit: null,
            cardNo: null,
            cardDate: null,
            cardDigits: null,
            cardName: null,
            isCardSelected: false,
            isCreditEdit: false,
            changeDiscount: "",
        }
    }   


    if (productId != null) {
        $scope.selectedProductName = productName;
        productStockService.productBatch(productId).then(function (response) {
            $scope.batchList1 = response.data;
            totalQuantity = 0;
            var rack = "";
            var tempBatch = [];
            var editBatch = null;

            for (var x = 0; x < items.length; x++) {
                for (var y = 0; y < $scope.batchList1.length; y++) {
                    if (items[x].productStock.productStockId == $scope.batchList1[y].id) {
                        $scope.batchList1.splice(y, 1);
                    }
                }
            }

            for (var i = 0; i < $scope.batchList1.length; i++) {
                $scope.batchList1[i].productStockId = $scope.batchList1[i].id;
                $scope.batchList1[i].id = null;
                var availableStock = $scope.getAvailableStock($scope.batchList1[i], editBatch);
                if (editBatch != null && editBatch.productStockId == $scope.batchList1[i].productStockId)
                    editBatch.availableQty = availableStock;
                totalQuantity += availableStock;

                if (availableStock == 0)
                    continue;

                $scope.batchList1[i].availableQty = availableStock;
                if ($scope.batchList1[i].rackNo != null || $scope.batchList1[i].rackNo != undefined) {
                    rack = $scope.batchList1[i].rackNo;
                }

                tempBatch.push($scope.batchList1[i]);
            }

            console.log(JSON.stringify(editBatch))
            if (editBatch != null && tempBatch.length == 0) {
                var availableStock = $scope.getAvailableStock(editBatch, editBatch);
                editBatch.availableQty = availableStock;
                totalQuantity = availableStock;
                tempBatch.push(editBatch);
            }

            $scope.batchList1 = tempBatch;            
            
        }, function () {
        });
    }

    //MRP Implementation by Settu
    $scope.getEnableSelling = function () {
        productStockService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.getEnableSelling();

});


app.directive('focusMe', function ($timeout, $parse) {
    return {
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                console.log('value=', value);
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();                        
                    });
                }
            });
            element.bind('blur', function () {
                console.log('blur')
                scope.$apply(model.assign(scope, false));
            })
        }
    };
});




