app.controller('vendorPurchaseReturnListCtrl', function ($scope, toastr, $filter, vendorPurchaseService, vendorService, vendorReturnModel, vendorPurchaseModel, pagerServcie, productService) {

    var vendorPurchaseReturn = vendorReturnModel;

    $scope.search = vendorPurchaseReturn;
    $scope.list = [];
    $scope.addproductDetails = {};
    $scope.addselectedproduct = {};
    $scope.getaddedproduct = [];
    $scope.focusInputVendorName = true;
    $scope.isValidQty = false;
    $scope.isBatch = true;
    $scope.editprodItem = false;
    $scope.batchListNew = [];
    $scope.focusReason = false;
    $scope.fsbatchNumber = false;
    $scope.returnStatus = false;
    //$scope.IsReturnDate = "true";
    $scope.vendorStatus = 2;

    $scope.VendorPurchaseReturnItems1 = {
        "Quantity": 0,
        "ProductStockId": 0,
        "ReturnedTotal": 0,
        "ReturnedQuantity": 0,
        "ReturnPurchasePrice": 0,
        "PackageQuantity": 0,
        "ProductStock": {}
    }

    $scope.vendorPurchaseReturn = {
        VendorPurchaseReturnItem: [],
        VendorId: "",
        NetReturn: 0.0
    };

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    $scope.EnablepurchaseReturn = false;
    $scope.returnTotal = 0;
    $scope.retTotal = 0;
    $scope.selectedProduct = null;
    //pagination
    $scope.reason = "Non Moving Return";
    function pageSearch() {
        $.LoadingOverlay("show");
        vendorPurchaseService.returnList($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.vendorPurchaseReturnSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;

        $scope.search.searchProductId = "";
        if ($scope.search.drugName)
            $scope.search.searchProductId = $scope.search.drugName.id;

        vendorPurchaseService.returnList($scope.search).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.vendorPurchaseReturnSearch();

    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) {    //vendorService.vendorData().then(function (response) {
            $scope.vendorList = response.data;
            $scope.popVendorList = response.data;
        }, function () { });
    }
    $scope.vendor();

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.getProducts = function (val) {

        return productService.ReturndrugFilter(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    //$scope.getProducts = function (val) {
    //    return productStockService.drugFilterData(val).then(function (response) {
    //        return response.data.map(function (item) {
    //            return item;
    //        });
    //    });
    //}

    //by San
    $scope.minDate = new Date();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.clearSearch = function () {
        $scope.search.drugName = "";
        $scope.search.searchProductId = "";
        if ($scope.search.vendorPurchase != null && $scope.search.vendorPurchase != undefined) {
            $scope.search.vendorPurchase.GoodsRcvNo = "";
            $scope.search.vendorPurchase.vendor = { vendor: {} };
        }
        $scope.search.invoiceNo = "";
        $scope.search.batchNo = "";
        $scope.search.invoiceDate = "";
        $scope.focusReason = false;
        $scope.vendorName = null;
    }

    $scope.getEnableSelling = function () {
        vendorPurchaseService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getEnableSelling();

    // Added by Settu to implement payment status & remarks
    $scope.editVendorPurchaseReturn = function (item) {
        item.isEdit = true;
    }
    $scope.updateVendorPurchaseReturn = function (item) {
        $.LoadingOverlay("show");
        vendorPurchaseService.updateVendorPurchaseReturn(item).then(function (response) {
            item.isEdit = !response.data;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.PurchaseReturnPopup = function () {
        $scope.EnablepurchaseReturn = true;
        $scope.clearSearch();
        $scope.vendor();
        $scope.focusInputVendorName = true;        
        $scope.isValidQty = false;
        $scope.returnStatus = false;
        $scope.reason = "Non Moving Return";
        $("#venName").trigger('chosen:open');
    }

    $scope.close = function () {
        $scope.EnablepurchaseReturn = false;
        $scope.clearSearch();
        $scope.addproductDetails = {};
        $scope.getaddedproduct = [];
        $scope.addselectedproduct = {};
        $scope.selectedProduct = null;
        $scope.selectedBatch = null;
        $scope.addselectedproduct.productquantity = null;
        $scope.returnTotal = 0;
        $scope.retTotal = 0;
        $scope.vendorNameModel = null;
        $scope.focusInputVendorName = false;
        $scope.returnStatus = false;
        $scope.vendorPurchaseReturn = {
            VendorPurchaseReturnItem: [],
            VendorId: "",
            NetReturn: 0.0
        };
        $.LoadingOverlay("hide");
    }

    $scope.clearProductInfo = function () {
        $scope.selectedProduct = null;
        $scope.addselectedproduct.productquantity = null;
        $scope.selectedBatch = null;
        $scope.isValidQty = false;
        $scope.expiryDate = '';
        $scope.CheckName();
    }

    function applyTaxValues() {
        $scope.vendorPurchaseReturn.totalGstValue = 0;
        for (var i = 0; i < $scope.vendorPurchaseReturn.VendorPurchaseReturnItem.length; i++) {
            if ($scope.vendorNameModel.locationType == 1) {
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].gstTotal = $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].ProductStock.gstTotal;
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].cgst = $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].ProductStock.gstTotal / 2;
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].sgst = $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].ProductStock.gstTotal / 2;
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].igst = 0;
            }
            else if ($scope.vendorNameModel.locationType == 2) {
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].gstTotal = $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].ProductStock.gstTotal;
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].igst = $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].ProductStock.gstTotal;
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].sgst = 0;
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].cgst = 0;
            }
            else {
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].gstTotal = 0;
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].igst = 0;
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].sgst = 0;
                $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].cgst = 0;
            }
            $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].gstAmount = ($scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].Quantity * $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].ReturnPurchasePrice * $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].gstTotal) / (100 + $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].gstTotal);
            $scope.vendorPurchaseReturn.totalGstValue += $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[i].gstAmount || 0;
        }
    }

    $scope.save = function () {
        $scope.returnStatus = true;
        $.LoadingOverlay("show");

        applyTaxValues();

        $scope.vendorPurchaseReturn.vendorId = $scope.VendorId;
        $scope.vendorPurchaseReturn.NetReturn = $scope.returnTotal;
        $scope.vendorPurchaseReturn.TotalReturnedAmount = $scope.retTotal;
        $scope.vendorPurchaseReturn.reason = $scope.reason;

        vendorPurchaseService.createVendorPurchaseReturnItems($scope.vendorPurchaseReturn)
            .then(function (response) {
                if (response.data) {
                    toastr.success('Vendor Returned Successfully');
                }
                $scope.returnStatus = false;
                $scope.close();
                $scope.vendorPurchaseReturnSearch();
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
            });
    };


    function checkExist(productStockId, productname) {
        var status = false;
        for (var i = 0; i < $scope.getaddedproduct.length; i++) {
            if ($scope.getaddedproduct[i].Name == productname && $scope.getaddedproduct[i].ProductStockId == productStockId) {
                status = true;
                break;
            }
        }
        return status;
    }

    $scope.addProduct = function () {

        $scope.expiryDate = '';

        $scope.isValidQty = false;

        if ($scope.addselectedproduct.productquantity == null) {
            var elem = document.getElementById("productqtyId");
            elem.focus();
            return;
        } else if ($scope.vendorName == null) {
            $scope.isValidVendor = true;
            return;
        } else if ($scope.selectedBatch == null) {
            return;
        }

        if ($scope.addselectedproduct != null) {

            $scope.addproductDetails.Name = $scope.addselectedproduct.productStock.product.name;
            $scope.addproductDetails.ProductStockId = $scope.addselectedproduct.productStock.id;
            $scope.addproductDetails.quantity = $scope.addselectedproduct.productquantity;

            if (checkExist($scope.addproductDetails.ProductStockId, $scope.addselectedproduct.productStock.product.name)) {
                var succ = confirm("Same product with same batch already Added");
                if (succ) {
                    document.getElementById("drugName1").focus()
                }
                return;
            }




            $scope.addproductDetails.stock = $scope.productQty;
            $scope.addproductDetails.freeQty = $scope.addselectedproduct.freeQty;
            $scope.addproductDetails.batchNo = $scope.selectedBatchNum;
            $scope.addproductDetails.expireDate = $filter('date')(new Date($scope.addselectedproduct.productStock.expireDate), "MM/yy");
            $scope.addproductDetails.pricewithvat = $scope.addselectedproduct.productStock.vat;
            // GST related
            $scope.addproductDetails.igst = $scope.addselectedproduct.productStock.igst;
            $scope.addproductDetails.cgst = $scope.addselectedproduct.productStock.cgst;
            $scope.addproductDetails.sgst = $scope.addselectedproduct.productStock.sgst;
            $scope.addproductDetails.gstTotal = $scope.addselectedproduct.productStock.gstTotal;

            if ($scope.addselectedproduct.purchasePrice == undefined || $scope.addselectedproduct.purchasePrice == null) { //packagePurchasePrice
                $scope.addproductDetails.purchasePrice = 0;
            } else {
                $scope.addproductDetails.purchasePrice = $scope.addselectedproduct.purchasePrice;
            }


            $scope.addproductDetails.mrp = $scope.addselectedproduct.productStock.mrp;
            $scope.addproductDetails.discount = $scope.addselectedproduct.productStock.discount;

            $scope.addproductDetails.editprodItem = $scope.editprodItem;
            $scope.addproductDetails.returnedTotal = ($scope.addproductDetails.quantity) * $scope.addproductDetails.purchasePrice;

            $scope.getaddedproduct.push($scope.addproductDetails);


            $scope.retTotal = 0;

            for (var i = 0; i < $scope.getaddedproduct.length; i++) {
                if ($scope.getaddedproduct[i].Name == $scope.addselectedproduct.productStock.product.name && $scope.getaddedproduct[i].productStockId == $scope.ProductStockId) {
                } else {

                   
                    $scope.getaddedproduct[i].returnedTotal = parseInt($scope.getaddedproduct[i].quantity) * $scope.getaddedproduct[i].purchasePrice;
                    $scope.getaddedproduct[i].productStock = $scope.addselectedproduct.productStock;
                    $scope.retTotal += $scope.getaddedproduct[i].returnedTotal;
                }
            }



            $scope.VendorPurchaseReturnItems1.ReturnedQuantity = $scope.addproductDetails.quantity == undefined ? 0 : $scope.addproductDetails.quantity;
            $scope.VendorPurchaseReturnItems1.Quantity = $scope.VendorPurchaseReturnItems1.ReturnedQuantity
            $scope.VendorPurchaseReturnItems1.ProductStockId = $scope.addproductDetails.ProductStockId;
            $scope.VendorPurchaseReturnItems1.ReturnedTotal = $scope.addproductDetails.returnedTotal;
            $scope.VendorPurchaseReturnItems1.ProductStock = $scope.addselectedproduct.productStock;
            // GST related
            $scope.VendorPurchaseReturnItems1.igst = $scope.addselectedproduct.igst;
            $scope.VendorPurchaseReturnItems1.cgst = $scope.addselectedproduct.cgst;
            $scope.VendorPurchaseReturnItems1.sgst = $scope.addselectedproduct.sgst;
            $scope.VendorPurchaseReturnItems1.gstTotal = $scope.addselectedproduct.gstTotal;
            $scope.VendorPurchaseReturnItems1.ReturnPurchasePrice = $scope.addproductDetails.purchasePrice;

            $scope.vendorPurchaseReturn.VendorId = $scope.VendorId;
           
            $scope.vendorPurchaseReturn.VendorPurchaseReturnItem.push($scope.VendorPurchaseReturnItems1);
        }

        $scope.emptyValues();
        var elm = document.getElementById("drugName1");
        elm.focus();
    };

    $scope.emptyValues = function () {
        $scope.selectedProduct = null;
        $scope.quantityExceed = 0;

        $scope.addselectedproduct = {};
        $scope.addproductDetails = {};
        $scope.addselectedproduct.productquantity = null;
        $scope.VendorPurchaseReturnItems1 = {};
        $scope.batchListNew = [];
    }

    $scope.removeProductItem = function (item, index) {
        if (!$scope.returnStatus) {
            $scope.getaddedproduct.splice(index, 1);
            $scope.vendorPurchaseReturn.VendorPurchaseReturnItem.splice(index, 1);
            $scope.retTotal = $scope.retTotal - (item.quantity * item.purchasePrice);
            $scope.selectedBatch = '--Select Batch Number--';
            var elem = document.getElementById("drugName1");
            elem.focus();
            $scope.returnStatus = false;
        }
    }

    $scope.editProductItem = function (item, index) {
        if (!$scope.returnStatus) {
            $scope.returnStatus = true;
            $scope.getaddedproduct[index].editprodItem = true;
            $scope.retTotal = $scope.retTotal - (item.quantity * item.purchasePrice);
        }
    }

    $scope.saveProductItem = function (item, index) {

        if (item.quantity > item.stock) {
            $scope.getaddedproduct[index].editprodItem = true;
            return;
        }
        if (item.quantity <= 0) {
            $scope.returnStatus = true;
            return;
        }
        $scope.getaddedproduct[index].editprodItem = false;
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].Quantity = item.quantity;
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].ProductStockId = item.ProductStockId;
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].ReturnedTotal = item.quantity * item.purchasePrice;
        $scope.retTotal = $scope.retTotal + (item.quantity * item.purchasePrice);
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].ReturnedQuantity = item.quantity;
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].ReturnPurchasePrice = item.purchasePrice;
        $scope.returnStatus = false;
    }

    window.onkeyup = function (event) {
        if (event.keyCode == 27) {
            document.getElementById("popupId").click();
        }
    }

    $scope.batchSelected = function (selectedBatch) {
        if (selectedBatch != null && selectedBatch != undefined && (typeof (selectedBatch) == "object")) {
            $scope.selectedBatch = selectedBatch;//$scope.batchList[selectedBatch];
            $scope.selectedBatchNum = $scope.selectedBatch.productStock.batchNo;
            $scope.selectedBatchTotStock = $scope.selectedBatch.productStock.totalStock;
            $scope.productQty = $scope.selectedBatch.productStock.totalStock;
            $scope.expiryDate = $scope.selectedBatch.productStock.expireDate;
            $scope.isValidQty = false;

            $scope.addselectedproduct = $scope.selectedBatch; //.productStock.id = $scope.selectedBatch.productStock
            var ele = document.getElementById("productqtyId");
            ele.focus();
            $scope.addselectedproduct.productquantity = null;
        }
        else {
            $scope.selectedBatchTotStock = undefined;
        }
    }

    $scope.OnProductSelect = function (obj) {
        //$scope.batchListNew = $filter("filter")($scope.batchListNew, { productStock: { productId: obj.productStock.productId } });
        $scope.addselectedproduct = {};
        $scope.ProductStockId = obj.productStock.id;
        $scope.selectedProduct = obj.productStock.name;
        $scope.productQty = obj.productStock.totalStock;
        $scope.selectedBatchTotStock = obj.productStock.totalStock;
        $scope.expiryDate = obj.productStock.expireDate;
        $scope.addselectedproduct = obj;

        var j = 0;
        for (var i = 0; i < $scope.batchList.length; i++) {
            if (obj.productStock.name == $scope.batchList[i].productStock.name) {
                $scope.batchListNew[j] = $scope.batchList[i];
                j++;
            }
        }
        $scope.selectedBatch = $scope.batchListNew[0];
        $scope.selectedBatchNum = $scope.selectedBatch.productStock.batchNo;
        $scope.addselectedproduct.productquantity = null;

        var ele = document.getElementById("productqtyId");
        ele.focus();
    }

    $scope.focusQuantity = function () {
        if ($scope.selectedProduct == null || $scope.selectedProduct == "")
            return;
        $scope.selectedBatch = $scope.batchListNew[0];
        $scope.selectedBatchTotStock = $scope.selectedBatch.productStock.totalStock;
        $scope.selectedBatchNum = $scope.selectedBatch.productStock.batchNo;
        var ele = document.getElementById("productqtyId");
        ele.focus();
    };

    $scope.focusBatchNumber = function () {
        $scope.fsbatchNumber = true;
    }

    $scope.OnselectedVendor = function (data) {
        $scope.VendorId = data.id;
        $scope.vendorName = data.name;
        $scope.isValidVendor = false;
        $scope.focusReason = true;
        if ($scope.vendorNameModel.locationType == undefined || $scope.vendorNameModel.locationType == null || $scope.vendorNameModel.locationType == "") {
            $scope.vendorNameModel.locationType = 1;
        }
    }

    $scope.changeReason = function () {
        $scope.focusReason = false;
        document.getElementById("drugName1").focus();
    }

    $scope.focusToProd = function () {
        $scope.focusReason = false;
        document.getElementById("drugName1").focus();
    }

    $scope.focusToReason = function () {
        $scope.focusReason = true;
    }

    $scope.focusAddProduct = function () {

        if ($scope.addselectedproduct.productquantity == undefined) {
            var ele = document.getElementById("productqtyId")
            ele.focus();
            return;
        }

        if (!$scope.isValidQty) {
            var ele = document.getElementById("BtnaddProduct");
            ele.focus();
        }
    }

    $scope.IsvalidProductQty = function () {

        var productquantity = $scope.addselectedproduct.productquantity || 0;
        var amount = ($scope.addselectedproduct.purchasePrice * productquantity); //Math.round(value, 2);
        $scope.addselectedproduct.amount = amount.toFixed(2);

        if (productquantity == 0) {
            $scope.isValidQuantity = true;
            return;
        } else if ($scope.selectedBatchTotStock != undefined && productquantity > $scope.selectedBatchTotStock) {
            document.getElementById("productqtyId").focus();
            $scope.isValidQty = true;
            $scope.isValidQuantity = false;
            return;
        } else if (($scope.productQty != undefined) && (productquantity > $scope.productQty) && ($scope.selectedBatchTotStock == undefined)) {
            document.getElementById("productqtyId").focus();
            $scope.isValidQty = true;
            $scope.isValidQuantity = false;
            return;
        } else {
            $scope.isValidQty = false;
            $scope.isValidQuantity = false;
            return;
        }
    };

    $scope.getreturnProducts = function (val) {
        if ($scope.selectedBatch != null)
            $scope.isValidQty = false;

        $scope.productName = val;
        $scope.isBatch = false;

        return vendorPurchaseService.getPurchaseReturnItem($scope.productName).then(function (response) {
            $scope.batchList = response.data;
            $scope.batchListNew = [];
            var origArr = response.data;
            var newArr = [],
           origLen = origArr.length,
           found, x, y;

            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if ($filter('uppercase')(origArr[x].productStock.productId) === $filter('uppercase')(newArr[y].productStock.productId)) {
                        found = true;
                        newArr[y].productStock.stock = newArr[y].productStock.stock + origArr[x].productStock.stock;
                        break;
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }

            return newArr.map(function (item) {
                return item;
            });
        });
    };

    $scope.CheckName = function () {
        if ($scope.selectedProduct == null || $scope.selectedProduct == "") {
            $scope.selectedProduct = null;
            $scope.batchList = [];
            $scope.batchListNew = [];
            $scope.addselectedproduct.productquantity = null;
            $scope.addselectedproduct = { purchasePrice: null };
            return;
        }
    }

    $scope.printBill = function (model) {
        window.open("/VendorPurchaseReturn/printdmA4?id=" + model.id);
        //var docDefinition = {
        //    pageSize: { width: 300, height: 600 },
        //    content: [],
        //    defaultStyle: {
        //        fontSize: 10,
        //        bold: false
        //    },
        //    pageMargins: [15, 10, 15, 10]
        //};

        //docDefinition.content.push({ text: model.vendorPurchase.instance.name, alignment: 'center' });
        //docDefinition.content.push({ text: model.vendorPurchase.instance.fullAddress, alignment: 'center' });
        //docDefinition.content.push({ text: "DL No: " + model.vendorPurchase.instance.drugLicenseNo + " / " + (model.taxRefNo ? "GSTIN: " + model.vendorPurchase.instance.gsTinNo : "TIN: " + model.vendorPurchase.instance.tinNo), alignment: 'center' });
        //docDefinition.content.push({ text: " " });
        //docDefinition.content.push({ text: "Purchase Return", alignment: 'center' });
        //docDefinition.content.push({ text: " " });

        //var txt = getText(model.vendorPurchase.vendor.name);
        //txt = txt + (getText(model.vendorPurchase.vendor.address).length > 0 ? "," + model.vendorPurchase.vendor.address : "");
        //txt = txt + (getText(model.vendorPurchase.vendor.area).length > 0 ? "," + model.vendorPurchase.vendor.area : "");
        //txt = txt + (getText(model.vendorPurchase.vendor.city).length > 0 ? "," + model.vendorPurchase.vendor.city : "");
        //txt = txt + (getText(model.vendorPurchase.vendor.pincode).length > 0 ? "," + model.vendorPurchase.vendor.pincode : "");
        //docDefinition.content.push({ text: txt });

        //docDefinition.content.push({ text: "DL :" + model.vendorPurchase.vendor.drugLicenseNo });
        //docDefinition.content.push({ text: (model.taxRefNo ? "GSTIN:" + model.vendorPurchase.vendor.gsTinNo : "TIN:" + model.vendorPurchase.vendor.tinNo) });
        //docDefinition.content.push({ text: " " });

        //docDefinition.content.push({
        //    columns: [
		//		{
		//		    text: "Return No : " + model.returnNo, alignment: 'left'
		//		},
		//		{
		//		    text: "Return Date : " + $filter('date')(model.returnDate, "dd/MM/yy"), alignment: 'right'
		//		}
        //    ]
        //});
        //docDefinition.content.push({ text: " " });

        //txt = {
        //    style: { fontSize: 8 },
        //    table: {                
        //        headerRows: 1,
        //        widths: ['30%', '13%', '13%', '10%', '10%', '10%', '14%'],
        //        body: [
		//			[{ text: 'Particulars', alignment: 'center' },
        //                { text: 'Batch', alignment: 'center' },
        //                { text: 'Exp.Dt.', alignment: 'center' },
        //                { text: 'Qty', alignment: 'center' },
        //                { text: 'Price', alignment: 'center' },
        //                { text: (model.taxRefNo ? 'GST%' : 'VAT%'), alignment: 'center' },
        //                { text: 'Amount', alignment: 'center' }],
        //        ]
        //    }
        //};
        //angular.forEach(model.vendorPurchaseReturnItem, function (item, key) {
        //    txt.table.body.push([
        //    { text: item.productStock.product.name, alignment: 'left' },
        //    { text: item.productStock.batchNo, alignment: 'left' },
        //    { text: $filter('date')(item.productStock.expireDate, "MM-yy"), alignment: 'left' },
        //    { text: item.quantity, alignment: 'right' },
        //    { text: (item.returnPurchasePrice > 0 ? item.returnPurchasePrice : item.productStock.purchasePrice), alignment: 'right' },
        //    { text: (model.taxRefNo ? (item.gstTotal > 0 ? item.gstTotal : item.productStock.gstTotal) : item.productStock.vat), alignment: 'right' },
        //    { text: item.returnTotal, alignment: 'right' }]);
        //});
        //docDefinition.content.push(txt);

        //docDefinition.content.push({ text: " " });
        //docDefinition.content.push({ text: "Net Amt: " + model.netReturn, alignment: 'right' });

        //pdfMake.createPdf(docDefinition).download("hq_" + model.returnNo);
    };

    function getText(input) {
        if (input == null || input == undefined) {
            return "";
        }
        else {
            return input;
        }
    }

});