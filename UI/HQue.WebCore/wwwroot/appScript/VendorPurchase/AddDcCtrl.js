app.controller('addDcCtrl', function ($scope, toastr, vendorPurchaseService, vendorPurchaseModel, productStockModel, vendorService, productService, productModel, $filter, productStockService, close, dCVendorPurchaseItemModel, selectedVendor, GSTEnabled) {
    $scope.minDate = new Date();
    var d = new Date();
    var ele = "";
    //$scope.isIGST = true;
    //$scope.isCGST = true;
    //$scope.isSGST = true;
    $scope.vendorStatus = 1;
    $scope.GSTEnabled = GSTEnabled;
    var dcNo = "";

    //ChequeMaxDate
    var today = new Date();
    today.setMonth(today.getMonth() + 6);
    $scope.chequeMaxDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        "opened": false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };
    $scope.draftCount = "";
    $scope.isFormValid = true;
    $scope.isAllowDecimal1 = false;
    $scope.validateDcDate = function () {
        var val = (toDate($scope.addDc.DcDate.$viewValue) < $scope.minDate);
        $scope.addDc.DcDate.$setValidity("InValidDcDate", val);
        $scope.isFormValid = val;
    };
    $scope.validateFreeQty = function () {
        if ($scope.EditMode) {
            if (($scope.oldDcVendorPurchaseItem.quantity - $scope.dcVendorPurchaseItem.productStock.stock) > ($scope.dcVendorPurchaseItem.packageQty * $scope.dcVendorPurchaseItem.packageSize)) {
                $scope.addDc.packageQuantityDc.$setValidity("InValidPackQtyDc", false);
            } else {
                $scope.addDc.packageQuantityDc.$setValidity("InValidPackQtyDc", true);
            }
        } else {
            $scope.addDc.freeQtyDc.$setValidity("InValidfreeQtyDc", !(($scope.dcVendorPurchaseItem.packageQty || 0) == 0 && ($scope.dcVendorPurchaseItem.freeQty || 0) == 0));
        }
        if ($scope.dcVendorPurchaseItem.packageQty != null && $scope.dcVendorPurchaseItem.packageQty != undefined) {
            if (!$scope.isAllowDecimal1) {
                if (parseFloat($scope.dcVendorPurchaseItem.packageQty) % 1 != 0) {
                    $scope.addDc.packageQuantityDc.$setValidity("InValidRoundQty", false);
                } else {
                    $scope.addDc.packageQuantityDc.$setValidity("InValidRoundQty", true);
                }
            }
        }
        if ($scope.dcVendorPurchaseItem.freeQty != null && $scope.dcVendorPurchaseItem.freeQty != undefined && $scope.dcVendorPurchaseItem.freeQty != "") {
            if (!$scope.isAllowDecimal1) {
                if (parseFloat($scope.dcVendorPurchaseItem.freeQty) % 1 != 0) {
                    $scope.addDc.freeQtyDc.$setValidity("InValidRoundQty", false);
                } else {
                    $scope.addDc.freeQtyDc.$setValidity("InValidRoundQty", true);
                }
            }
        } else {
            $scope.addDc.freeQtyDc.$setValidity("InValidRoundQty", true);
        }
        if ($scope.isAllowDecimal1) {
            var qty = ($scope.dcVendorPurchaseItem.packageQty != null && $scope.dcVendorPurchaseItem.packageQty != undefined && $scope.dcVendorPurchaseItem.packageQty != "") ? parseFloat($scope.dcVendorPurchaseItem.packageQty) : 0;
            var freeqty = ($scope.dcVendorPurchaseItem.freeQty != null && $scope.dcVendorPurchaseItem.freeQty != undefined && $scope.dcVendorPurchaseItem.freeQty != "") ? parseFloat($scope.dcVendorPurchaseItem.freeQty) : 0;
            var tot = (qty + freeqty) % 1;
            if (tot !== 0) {
                $scope.addDc.freeQtyDc.$setValidity("InvalidSumQty", false);
            } else {
                $scope.addDc.freeQtyDc.$setValidity("InvalidSumQty", true);
            }
        }
    };
    function toDate(dateStr) {
        if (dateStr != null) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
    }
    function getIsAllowDecimalSettings() {
        vendorPurchaseService.getDecimalSetting()
            .then(function (response) {
                $scope.isAllowDecimal1 = response.data;
            }, function (error) {
                console.log(error);
            })
    }
    getIsAllowDecimalSettings();
    $scope.updateUI = function () {

        if ($scope.GSTEnabled) {
            $scope.dcVendorPurchaseItem.productStock.igst = "";
            $scope.dcVendorPurchaseItem.productStock.cgst = "";
            $scope.dcVendorPurchaseItem.productStock.sgst = "";
            $scope.dcVendorPurchaseItem.productStock.gstTotal = "";
            //if ($scope.vendorPurchase.selectedVendor.locationType == 0 || $scope.vendorPurchase.selectedVendor.locationType == 1) {
            //    $scope.isCGST = true;
            //    $scope.isSGST = true;
            //    $scope.isIGST = false;
            //} else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
            //    $scope.isCGST = false;
            //    $scope.isSGST = false;
            //    $scope.isIGST = true;
            //} else {
            //    $scope.isIGST = false;
            //    $scope.isCGST = false;
            //    $scope.isSGST = false;
            //}
        } else {
            var cst = document.getElementById("cSTDc");
            var vat = document.getElementById("vatDc");
            $scope.dcVendorPurchaseItem.productStock.vAT = 0;
            $scope.dcVendorPurchaseItem.productStock.cST = 0;
            if ($scope.vendorPurchase.selectedVendor.enableCST) {
                vat.disabled = false;
                cst.disabled = false;
                $scope.vendorPurchase.TaxationType = "CST";
                $scope.dcVendorPurchaseItem.productStock.cST = 2;
            } else {
                vat.disabled = false;
                cst.disabled = true;
                $scope.vendorPurchase.TaxationType = "VAT";
                $scope.dcVendorPurchaseItem.productStock.vAT = 5;
            }
        }


        if ($scope.vendorPurchase.selectedVendor.discount) {
            $scope.dcVendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
        } else {
            $scope.dcVendorPurchaseItem.discount = 0;
        }
        if ($scope.dcVendorPurchaseItem.discount == 0) {
            $scope.dcVendorPurchaseItem.discount = "";
        }
        //Added by Sarubala on 12-09-17
        if ($scope.selectedProduct1 != null && $scope.selectedProduct1 != undefined) {
            if ($scope.vendorPurchase.selectedVendor.locationType == 1) {
                $scope.dcVendorPurchaseItem.productStock.gstTotal = $scope.selectedProduct1.gstTotal;
                $scope.dcVendorPurchaseItem.productStock.cgst = $scope.selectedProduct1.gstTotal / 2;
                $scope.dcVendorPurchaseItem.productStock.sgst = $scope.selectedProduct1.gstTotal / 2;
                $scope.dcVendorPurchaseItem.productStock.igst = 0;
            }
            else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
                $scope.dcVendorPurchaseItem.productStock.gstTotal = $scope.selectedProduct1.gstTotal;
                $scope.dcVendorPurchaseItem.productStock.cgst = 0;
                $scope.dcVendorPurchaseItem.productStock.sgst = 0;
                $scope.dcVendorPurchaseItem.productStock.igst = $scope.selectedProduct1.igst > 0 ? $scope.selectedProduct1.igst : $scope.selectedProduct1.gstTotal;
            }
            else {
                $scope.dcVendorPurchaseItem.productStock.gstTotal = 0;
                $scope.dcVendorPurchaseItem.productStock.cgst = 0;
                $scope.dcVendorPurchaseItem.productStock.sgst = 0;
                $scope.dcVendorPurchaseItem.productStock.igst = 0;
            }
        }

        $scope.onVendorSelect();
    };
    angular.element(document).keydown(function (e) {
        if (e.ctrlKey && e.keyCode == 67) { //Ctrl + C (cancel)
            event.preventDefault();
            $scope.close('No');
        }
    });
    $scope.close = function (result) {
        close(result, 100);
        $(".modal-backdrop").hide();
    };
    $scope.shouldBeOpen = true;
    $scope.EditMode = false;
    $scope.Action = "Add";
    $scope.ResetAction = "Reset";
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    var vendorPurchase = vendorPurchaseModel;
    var dcVendorPurchaseItem = dCVendorPurchaseItemModel;
    dcVendorPurchaseItem.productStock = productStockModel;
    dcVendorPurchaseItem.productStock.product = productModel;
    $scope.product = productModel;
    $scope.vendorPurchase = vendorPurchaseModel;
    $scope.vendorPurchase.dcVendorPurchaseItem = [];
    $scope.dcVendorPurchaseItem = dcVendorPurchaseItem;
    $scope.dcVendorPurchaseItem.productStock.vAT = 5;
    $scope.dcVendorPurchaseItem.tax = 0;
    $scope.dcVendorPurchaseItem.total = 0;
    $scope.dcVendorPurchaseItem.freeQty = "";
    $scope.dcVendorPurchaseItem.discount = "";
    $scope.dcVendorPurchaseItem.orgDiscount = 0;
    $scope.dcVendorPurchaseItem.discountVal = 0;
    $scope.dcVendorPurchaseItem.orderedQty = 0;
    $scope.vendorPurchase.vendorPurchaseItem = [];
    $scope.vendorPurchase.selectedVendor = {};
    $scope.selectedProduct1 = null;
    $scope.Math = window.Math;
    $scope.dcVendorPurchaseItem = { "productStock": { "product": {} } };
    $scope.vendorPurchase.TaxationType = "VAT";
    var d = new Date();
    $scope.dcVendorPurchaseItem.dcDate = d.toISOString();
    $scope.list = [];
    $scope.dcVendorPurchaseItem.dcList = [];
    $scope.editItems = 0;
    $scope.copyedIndex = {};
    $scope.cancelUpdate1 = function () {
        $scope.EditMode = false;
        $scope.addDc.packageQuantityDc.$setValidity("InValidPackQtyDc", true);
        $scope.addDc.drugName1.$setPristine();
        $scope.addDc.DcNo.$setPristine();
        $scope.addDc.batchNoDc.$setPristine();
        $scope.addDc.expDateDc.$setPristine();

        if (!$scope.GSTEnabled) {
            $scope.addDc.vatDc.$setPristine();
        }

        $scope.addDc.packageSizeDc.$setPristine();
        $scope.addDc.packageQuantityDc.$setPristine();
        $scope.addDc.purchasePriceDc.$setPristine();
        $scope.addDc.sellingPriceDc.$setPristine();
        $scope.addDc.mrpDC.$setPristine();
        $scope.addDc.freeQtyDc.$setPristine();
        $scope.addDc.discountDc.$setPristine();
        $scope.addDc.sellingPriceDc.$setValidity("checkMrpError", true);
        $scope.addDc.mrpDC.$setValidity("checkMrpError", true);
        $scope.addDc.packageQuantityDc.$setValidity("InValidRoundQty", true);
        $scope.addDc.freeQtyDc.$setValidity("InValidRoundQty", true);
        $scope.addDc.freeQtyDc.$setValidity("InvalidSumQty", true);
        $scope.isAllowDecimal1 = false;
        var date1 = $scope.dcVendorPurchaseItem.dcDate; //Added by Poongodi on 22/03/2017
        dcNo = $scope.dcVendorPurchaseItem.dcNo;
        $scope.dcVendorPurchaseItem = { "productStock": { "product": {} } };
        $scope.selectedProduct1 = null;
        $scope.dcVendorPurchaseItem.dcDate = d.toISOString();
        $scope.dcVendorPurchaseItem.dcDate = date1;
        getIsAllowDecimalSettings();
        $scope.onVendorSelect();
        //$scope.isIGST = true;
        //$scope.isCGST = true;
        //$scope.isSGST = true;
    };
    $scope.checkAllPurchasefields = function (ind) {
        if (ind > 1) {
            if (Object.keys($scope.vendorPurchase.selectedVendor).length == 0) {
                $("#vendorDc").trigger('chosen:open');
                event.stopPropagation();
                return false;
            }
        }
        if (ind > 3) {
            if ($scope.dcVendorPurchaseItem.dcDate == undefined || $scope.dcVendorPurchaseItem.dcDate == null) {
                ele = document.getElementById("DcDate");
                ele.focus();
                return false;
            }
        }
        if (ind > 4) {
            if ($scope.dcVendorPurchaseItem.dcNo == undefined || $scope.dcVendorPurchaseItem.dcNo == null || $scope.dcVendorPurchaseItem.dcNo == "") {
                ele = document.getElementById("DcNo");
                ele.focus();
                return false;
            }
        }
        if (ind > 5) {
            if ($scope.selectedProduct1 == undefined || $scope.selectedProduct1 == null) {
                ele = document.getElementById("drugName1");
                ele.focus();
                return false;
            }
        }
        if (ind > 6) {
            if ($scope.dcVendorPurchaseItem.productStock.batchNo == undefined || $scope.dcVendorPurchaseItem.productStock.batchNo == null) {
                ele = document.getElementById("batchNoDc");
                ele.focus();
                return false;
            }
        }
        if (ind > 7) {
            if ($scope.dcVendorPurchaseItem.productStock.expireDate == undefined || $scope.dcVendorPurchaseItem.productStock.expireDate == null) {
                ele = document.getElementById("expDateDc");
                ele.focus();
                return false;
            }
        }
        if (ind > 7 && !$scope.GSTEnabled) {
            if ($scope.dcVendorPurchaseItem.productStock.vAT == undefined || $scope.dcVendorPurchaseItem.productStock.vAT == null) {
                ele = document.getElementById("vatDc");
                ele.focus();
                return false;
            }
        }

        //GST validation starts here
        //if (ind > 8) {
        //    if ($scope.dcVendorPurchaseItem.productStock.igst == undefined || $scope.dcVendorPurchaseItem.productStock.igst == null) {
        //         ele = document.getElementById("igstDc");
        //        ele.focus();
        //        return false;
        //    }
        //}
        //if (ind > 9) {
        //    if ($scope.dcVendorPurchaseItem.productStock.cgst == undefined || $scope.dcVendorPurchaseItem.productStock.cgst == null) {
        //         ele = document.getElementById("cgstDc");
        //        ele.focus();
        //        return false;
        //    }
        //}
        //if (ind > 10) {
        //    if ($scope.dcVendorPurchaseItem.productStock.sgst == undefined || $scope.dcVendorPurchaseItem.productStock.sgst == null) {
        //         ele = document.getElementById("sgstDc");
        //        ele.focus();
        //        return false;
        //    }
        //}
        //if (ind > 11) {
        //    if ($scope.dcVendorPurchaseItem.productStock.gstTotal == undefined || $scope.dcVendorPurchaseItem.productStock.gstTotal == null) {
        //         ele = document.getElementById("gstTotalDc");
        //        ele.focus();
        //        return false;
        //    }
        //}
        //GST validation ends here

        if (ind > 8) {
            if ($scope.dcVendorPurchaseItem.packageSize == undefined || $scope.dcVendorPurchaseItem.packageSize == null) {
                ele = document.getElementById("packageSizeDc");
                ele.focus();
                return false;
            }
        }
        if (ind > 9) {
            if ($scope.dcVendorPurchaseItem.packageQty == undefined || $scope.dcVendorPurchaseItem.packageQty == null) {
                ele = document.getElementById("packageQuantityDc");
                ele.focus();
                return false;
            }
        }
        if (ind > 14 && $scope.GSTEnabled) {
            if ($scope.dcVendorPurchaseItem.productStock.gstTotal == undefined || $scope.dcVendorPurchaseItem.productStock.gstTotal == null) {
                ele = document.getElementById("dcgstTotal");
                ele.focus();
                return false;
            }
        }
        if (ind > 15) {
            if ($scope.dcVendorPurchaseItem.packagePurchasePrice == undefined || $scope.dcVendorPurchaseItem.packagePurchasePrice == null) {
                ele = document.getElementById("purchasePriceDc");
                ele.focus();
                return false;
            }
        }
        if (ind > 16) {
            if ($scope.dcVendorPurchaseItem.packageSellingPrice == undefined || $scope.dcVendorPurchaseItem.packageSellingPrice == null) {
                ele = document.getElementById("sellingPriceDc");
                ele.focus();
                return false;
            }
        }
        if (ind > 17) {
            if ($scope.dcVendorPurchaseItem.packageMRP == undefined || $scope.dcVendorPurchaseItem.packageMRP == null) {
                ele = document.getElementById("mrpDC");
                ele.focus();
                return false;
            }
        }
    };
    $scope.onVendorSelect = function () {
        if ($scope.vendorPurchase.selectedVendor.id != null) {
            vendorPurchaseService.getDCItems($scope.vendorPurchase.selectedVendor.id)
                .then(function (response) {
                    $scope.dcVendorPurchaseItem.dcList = response.data;
                }, function () {
                });
        }
        window.setTimeout(function () {
            if (dcNo == "") {
                ele = document.getElementById("DcNo");
                ele.focus();
            }
            else {
                ele = document.getElementById("drugName1");
                ele.focus();
            }
            $scope.dcVendorPurchaseItem.dcNo = dcNo;
            $scope.addDc.DcNo.$setPristine();
        }, 0);
    };
    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) {    //vendorService.vendorData().then(function (response) {
            $scope.vendorList = response.data;
        }, function () { toastr.error('Error Occured', 'Error'); });
    };
    $scope.vendor();
    $scope.getProducts = function (val) {
        return productService.nonHiddenProductList(val).then(function (response) {
            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                return item;
            });
        });
    };
    function setTotal1() {
        $scope.vendorPurchase.total = 0;
        if ($scope.vendorPurchase.discount == "") {
            $scope.vendorPurchase.discount = 0;
        }
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if ($scope.vendorPurchase.vendorPurchaseItem[i].discount == null) $scope.vendorPurchase.vendorPurchaseItem[i].discount = 0;
            $scope.vendorPurchase.total += ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) - $scope.vendorPurchase.vendorPurchaseItem[i].discount;
        }
        setVat();
    }
    function setTotal() {
        $scope.vendorPurchase.total = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            $scope.vendorPurchase.total += $scope.vendorPurchase.vendorPurchaseItem[i].total;
        }
        setTotaldiscount();
    }
    function setTotaldiscount() {
        $scope.vendorPurchase.discountOnBill = 0;
        if ($scope.vendorPurchase.discount == "") {
            $scope.vendorPurchase.discount = 0;
        }
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            var total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty));
            $scope.vendorPurchase.discountOnBill += total - (total * ($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount / 100));
        }
        $scope.vendorPurchase.discountOnBill = $scope.vendorPurchase.discountOnBill * ($scope.vendorPurchase.discount / 100);
    }
    function setVat1() {
        $scope.vendorPurchase.vat = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            var total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * (parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) + parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].freeQty)));
            //$scope.vendorPurchase.vat += total - ((total * 100) / (Number($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT) + 100));
            $scope.vendorPurchase.vat += total * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
        }
    }
    function setVat() {
        $scope.vendorPurchase.tax = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;
        }
        var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax;
        roundingTotal(total);
    }
    function roundingTotal(Total) {
        var afterRoundUp = Math.round(Total);
        $scope.vendorPurchase.grossValue = afterRoundUp;
        $scope.roundedAmount = afterRoundUp - Total;
    }
    function getGoodRNO() {
        vendorPurchaseService.getGRNO().then(function (response) {
            $scope.vendorPurchase.goodsRcvNo = response.data + 1;
        });
    }
    $scope.init = function (id) {
        //$scope.loadDraft();
        if (window.localStorage.getItem("p_data") == null)
            $scope.draftCount = "";
        else
            $scope.draftCount = 1;
    };
    $scope.getValues = function ($event, selectedProduct1) {
        var key1 = $event.which || $event.keyCode;
        if (key1 == 13)
            if (selectedProduct1 != null) {
                $scope.dcVendorPurchaseItem.productStock.productId = selectedProduct1.id;
                vendorPurchaseService.getPurchaseValues($scope.dcVendorPurchaseItem.productStock.productId).then(function (response) {
                    $scope.dcVendorPurchaseItem = response.data;
                });
            }
    };
    $scope.addStock = function () {
        if (!$scope.EditMode) {
            if ($scope.selectedProduct1 != null) {
                if (typeof $scope.selectedProduct1 === "string") {
                    if (!confirm("This is a new product, do you want to add it ?")) {
                        return;
                    }
                    $scope.dcVendorPurchaseItem.productStock.product.name = $scope.selectedProduct1;
                } else {
                    $scope.dcVendorPurchaseItem.productStock.product.name = $scope.selectedProduct1.name;
                    $scope.dcVendorPurchaseItem.productStock.productId = $scope.selectedProduct1.id;
                    $scope.dcVendorPurchaseItem.productStock.product.manufacturer = $scope.selectedProduct1.manufacturer;
                    $scope.dcVendorPurchaseItem.productStock.product.type = $scope.selectedProduct1.type;
                    $scope.dcVendorPurchaseItem.productStock.product.schedule = $scope.selectedProduct1.schedule;
                    $scope.dcVendorPurchaseItem.productStock.product.category = $scope.selectedProduct1.category;
                    $scope.dcVendorPurchaseItem.productStock.product.packing = $scope.selectedProduct1.packing;
                    $scope.dcVendorPurchaseItem.productStock.product.genericName = $scope.selectedProduct1.genericName;
                    if ($scope.selectedProduct1.accountId === null || $scope.selectedProduct1.accountId === undefined || $scope.selectedProduct1.accountId === "") {
                        $scope.dcVendorPurchaseItem.productStock.product.productMasterID = $scope.selectedProduct1.id;
                    } else {
                        $scope.dcVendorPurchaseItem.productStock.product.productMasterID = null;
                    }
                }
                $scope.dcVendorPurchaseItem.isEdit = false;
                $scope.dcVendorPurchaseItem.dcNo = $scope.dcVendorPurchaseItem.dcNo.toUpperCase();
                $scope.dcVendorPurchaseItem.vendorId = $scope.vendorPurchase.selectedVendor.id;
                $scope.dcVendorPurchaseItem.productStock.vendorId = $scope.vendorPurchase.selectedVendor.id;
                $scope.dcVendorPurchaseItem.packageQty = parseFloat($scope.dcVendorPurchaseItem.packageQty) || 0;
                $scope.dcVendorPurchaseItem.freeQty = parseFloat($scope.dcVendorPurchaseItem.freeQty) || 0;
                $scope.dcVendorPurchaseItem.discount = $scope.dcVendorPurchaseItem.discount || 0;
                $scope.dcVendorPurchaseItem.discountVal = 0;
                $scope.dcVendorPurchaseItem.orgDiscount = $scope.dcVendorPurchaseItem.discount;
                //commented for DC percent value shown as double , by bala - 19/17 
                $scope.dcVendorPurchaseItem.discount = Number($scope.dcVendorPurchaseItem.discount); // + Number($scope.dcVendorPurchaseItem.discount);
                if ($scope.dcVendorPurchaseItem.discount > 0) {
                    $scope.dcVendorPurchaseItem.discountVal = ($scope.dcVendorPurchaseItem.packagePurchasePrice * $scope.dcVendorPurchaseItem.packageQty) * ($scope.dcVendorPurchaseItem.discount / 100);
                }
                $scope.dcVendorPurchaseItem.total = ($scope.dcVendorPurchaseItem.packagePurchasePrice * $scope.dcVendorPurchaseItem.packageQty) - $scope.dcVendorPurchaseItem.discountVal;


                if ($scope.GSTEnabled) {
                    $scope.dcVendorPurchaseItem.tax = ($scope.dcVendorPurchaseItem.total) * ($scope.dcVendorPurchaseItem.productStock.gstTotal / 100);
                } else {
                    var _isCST = false;
                    if ($scope.vendorPurchase.TaxationType == "VAT") {
                        $scope.dcVendorPurchaseItem.tax = ($scope.dcVendorPurchaseItem.total) * ($scope.dcVendorPurchaseItem.productStock.vAT / 100);
                        $scope.dcVendorPurchaseItem.productStock.tax = $scope.dcVendorPurchaseItem.productStock.vAT;
                        $scope.dcVendorPurchaseItem.productStock.cST = 0;
                        var cst = document.getElementById("cSTDc");
                        cst.disabled = true;
                    }
                    var vat;
                    if ($scope.vendorPurchase.TaxationType == "CST") {
                        $scope.dcVendorPurchaseItem.tax = ($scope.dcVendorPurchaseItem.total) * ($scope.dcVendorPurchaseItem.productStock.cST / 100);
                        $scope.dcVendorPurchaseItem.productStock.tax = $scope.dcVendorPurchaseItem.productStock.cST;
                        $scope.vendorPurchase.TaxationType = "CST";
                        _isCST = true;
                    }
                }
                $scope.dcVendorPurchaseItem.orderedQty = $scope.dcVendorPurchaseItem.packageQty || 0;
                $scope.dcVendorPurchaseItem.selectedProduct = $scope.selectedProduct1;
                $scope.selectedProduct1 = null;
                $scope.vendorPurchase.dcVendorPurchaseItem.push($scope.dcVendorPurchaseItem);
                setTotal();
                setVat();

                if (!$scope.GSTEnabled) {
                    (_isCST) ? $scope.dcVendorPurchaseItem.productStock.cST = 2 : $scope.dcVendorPurchaseItem.productStock.vAT = 5;
                }

                /////Commented by durga Reddy


                //if ($scope.vendorPurchase.selectedVendor.discount) {
                //    $scope.dcVendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
                //} else {
                //    $scope.dcVendorPurchaseItem.discount = 0;
                //}

                //if (!angular.isUndefined($scope.vendorPurchase.selectedVendor.discount)) {
                //    $scope.dcVendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
                //}





                $.LoadingOverlay("show");
                $scope.isProcessing = true;
                vendorPurchaseService.saveDcItem($scope.dcVendorPurchaseItem)
                    .then(function (response) {
                        $scope.dcVendorPurchaseItem = response.data;
                        $scope.cancelUpdate1();
                        getIsAllowDecimalSettings();
                        //$scope.dcVendorPurchaseItem.dcDate = d.toISOString();                    
                        $.LoadingOverlay("hide");
                        $scope.isProcessing = false;
                    }, function (response) {
                        $.LoadingOverlay("hide");
                        toastr.error(response.data.errorDesc, 'Error');
                        $scope.isProcessing = false;
                    });
            }
        } else {
            $scope.EditMode = false;
            $.LoadingOverlay("show");
            $scope.isProcessing = true;
            $scope.dcVendorPurchaseItem.dcList = null;
            if ($scope.dcVendorPurchaseItem.productStock.packageSize == null || $scope.dcVendorPurchaseItem.productStock.packageSize == undefined) {
                $scope.dcVendorPurchaseItem.productStock.packageSize = $scope.dcVendorPurchaseItem.packageSize;
            }
            vendorPurchaseService.updateDcItem($scope.dcVendorPurchaseItem)
                .then(function (response) {
                    $scope.cancelUpdate1();
                    getIsAllowDecimalSettings();
                    //$scope.dcVendorPurchaseItem.dcDate = d.toISOString();                
                    $.LoadingOverlay("hide");
                    $scope.isProcessing = false;
                }, function () {
                    $.LoadingOverlay("hide");
                    toastr.error(response.data.errorDesc, 'Error');
                    $scope.isProcessing = false;
                });
        }
    };
    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };
    $scope.editPurchase = function (item) {
        if ($scope.EditMode) {
            return;
        }
        item.discount = item.orgDiscount;
        item.packageQty = item.packageQty - item.freeQty;
        $scope.copyedIndex = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
        $scope.vendorPurchaseItem = JSON.parse(JSON.stringify(item));
        $scope.selectedProduct1 = item.selectedProduct1;
        $scope.EditMode = true;
        $scope.previouspurchases = item.previouspurchases;
    };
    $scope.editStock = function (item) {
        $scope.EditMode = true;
        var list1 = $scope.dcVendorPurchaseItem.dcList;

        var ngst = $filter("filter")($scope.taxValuesList, { "tax": item.productStock.gstTotal }, true);

        if (ngst.length == 0) {
            item.productStock.gstTotal = null;
        }
        $scope.dcVendorPurchaseItem = item;
        $scope.oldDcVendorPurchaseItem = item;
        $scope.selectedProduct1 = item.productStock.product;

        if (!$scope.GSTEnabled) {
            item.productStock.vAT = item.productStock.vat;
            item.productStock.cST = item.productStock.cst;
        }


        $scope.dcVendorPurchaseItem.dcList = list1;
    };
    $scope.removeStock = function (item) {
        var del = window.confirm('Are you sure, Do you want to Delete Stock?');
        if (del) {
            $.LoadingOverlay("show");
            vendorPurchaseService.removeDcItem(item)
                .then(function (response) {
                    $scope.onVendorSelect();
                    $.LoadingOverlay("hide");
                }, function () {
                    $.LoadingOverlay("hide");
                });
        } else {
            $.LoadingOverlay("hide");
            $scope.isProcessing = false;
        }
    };
    $scope.doneEditPurchase = function (item, isValid) {
        if (!isValid) {
            item.isEdit = false;
            $scope.editItems = $scope.editItems - 1;
        }
        setTotal();
        setVat();
    };
    $scope.SelectedFileForUpload = null;
    //File Select event 
    $scope.selectFileforUpload = function (file) {
        $scope.SelectedFileForUpload = file[0];
    };
    $scope.save = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        $scope.netHighlight = "";
        var checkNetTotal = 0;
        if ($scope.vendorPurchase.initialValue) {
            checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
            if (checkNetTotal <= 1 && checkNetTotal >= -1) {
                $scope.netHighlight = "";
                purchaseCreate();
                getIsAllowDecimalSettings();
            } else {
                var cancel = window.confirm('Are you sure, Do you want to Update Stock?');
                if (cancel) {
                    purchaseCreate();
                    getIsAllowDecimalSettings();
                } else {
                    $.LoadingOverlay("hide");
                    $scope.netHighlight = "highlight";
                    $scope.isProcessing = false;
                }
            }
        } else {
            purchaseCreate();
            getIsAllowDecimalSettings();
        }
    };
    function purchaseCreate() {
        if ($scope.vendorPurchase.noteAmount == "" || $scope.vendorPurchase.noteAmount == 0 || $scope.vendorPurchase.noteAmount == null) {
            $scope.vendorPurchase.noteAmount = 0;
            $scope.vendorPurchase.NoteType = null;
        } else {
            if ($scope.vendorPurchase.NoteType == 1) {
                $scope.vendorPurchase.NoteType = "credit";
            } else {
                $scope.vendorPurchase.NoteType = "debit";
            }
        }
        vendorPurchaseService.create($scope.vendorPurchase)
            .then(function (response) {
                $scope.list = response.data;
                $scope.vendorPurchaseItems.$setPristine();
                toastr.success('Stock added successfully');
                $scope.productStockSearch();
                $scope.vendorPurchase.selectedVendor = {};
                var iDate = new Date();
                $scope.vendorPurchase = { "total": 0, "discount": 0, "vendorPurchaseItem": [], "invoiceDate": iDate.toISOString() };
                $scope.minDate = new Date();
                var d = new Date();
                $scope.isProcessing = false;
                if (!$scope.GSTEnabled) {
                    document.getElementById("vat").disabled = false;
                    document.getElementById("cST").disabled = true;
                    $scope.vendorPurchaseItem.productStock.cST = 0;
                }
                $.LoadingOverlay("hide");
                //window.localStorage.removeItem('p_data');
                //$scope.draftCount = "";
                getGoodRNO();
                $scope.SelectedFileForUpload = null;
                $scope.roundedAmount = 0;
                $scope.previouspurchases = [];
            }, function (response) {
                $scope.responses = response;
                $.LoadingOverlay("hide");
                toastr.error(response.data.errorDesc, 'Error');
                $scope.isProcessing = false;
            });
    }
    //$scope.initialValue = function () {
    //    if ($scope.vendorPurchase.initialValue == "")
    //        $scope.vendorPurchase.initialValue = null;
    //    var checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
    //    if (checkNetTotal <= 1 && checkNetTotal >= -1) {
    //        $scope.netHighlight = "";
    //    } else {
    //        if ($scope.vendorPurchase.total + $scope.vendorPurchase.tax > 0 && $scope.vendorPurchase.initialValue != null)
    //            $scope.netHighlight = "highlight";
    //        else
    //            $scope.netHighlight = "";
    //    }
    //};
    //function fillProductData(name) {
    //    $.LoadingOverlay("show");
    //    vendorPurchaseService.getPurchaseValues($scope.vendorPurchaseItem.productStock.productId, name).then(function (response) {
    //        $scope.vendorPurchaseItem = response.data.vendorPurchaseItem;
    //        $scope.vendorPurchaseItem.packageQty = $scope.vendorPurchaseItem.packageQty - $scope.vendorPurchaseItem.freeQty;
    //        var date2 = new Date($scope.vendorPurchaseItem.productStock.expireDate);
    //        if (date2 < (d.setMonth(d.getMonth() + 1))) {
    //            $scope.dayDiff($scope.vendorPurchaseItem.productStock.expireDate);
    //        } else {
    //            $scope.highlight = "";
    //            $scope.addDc.expDate.$setValidity("InValidexpDateDc", true);
    //            $scope.isFormValid = true;
    //        }
    //        //var cst = document.getElementById("cST");
    //        //var vat = document.getElementById("vat");
    //        //$scope.vendorPurchaseItem.productStock.vAT = 0;
    //        //$scope.vendorPurchaseItem.productStock.cST = 0;
    //        //if ($scope.vendorPurchase.selectedVendor.enableCST) {
    //        //    vat.disabled = false;
    //        //    cst.disabled = false;
    //        //    $scope.vendorPurchase.TaxationType = "CST";
    //        //    $scope.vendorPurchaseItem.productStock.cST = 2;
    //        //}
    //        //else {
    //        //    vat.disabled = false;
    //        //    cst.disabled = true;
    //        //    $scope.vendorPurchase.TaxationType = "VAT";
    //        //    $scope.vendorPurchaseItem.productStock.vAT = 5;
    //        //}
    //        if ($scope.vendorPurchase.selectedVendor.discount) {
    //            $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
    //            if ($scope.vendorPurchaseItem.discount == 0) {
    //                $scope.vendorPurchaseItem.discount = "";
    //            }
    //        } else {
    //            $scope.vendorPurchaseItem.discount = "";
    //        }
    //        if ($scope.vendorPurchaseItem.freeQty) {
    //            if ($scope.vendorPurchaseItem.freeQty == 0) {
    //                $scope.vendorPurchaseItem.freeQty = "";
    //            }
    //        } else {
    //            $scope.vendorPurchaseItem.freeQty = "";
    //        }
    //        $scope.previouspurchases = response.data.getPreviousPurchase;
    //        $.LoadingOverlay("hide");
    //    },
    //function (response) {
    //    $.LoadingOverlay("hide");
    //    toastr.error(response.data.errorDesc, 'Error');
    //});
    //}
    $scope.onProductSelect = function ($event, selectedProduct1) {
        if (selectedProduct1 != null) {
            $scope.dcVendorPurchaseItem.productStock.productId = selectedProduct1.id;
            //Added by Sarubala on 12/09/17
            $scope.dcVendorPurchaseItem.productStock.hsnCode = selectedProduct1.hsnCode;
            $scope.dcVendorPurchaseItem.productStock.product.rackNo = selectedProduct1.rackNo;
            $scope.dcVendorPurchaseItem.productStock.product.boxNo = selectedProduct1.boxNo;

            var ngst = $filter("filter")($scope.taxValuesList, { "tax": selectedProduct1.gstTotal }, true);

            if (ngst.length == 0) {
                selectedProduct1.gstTotal = null;
            }


            if ($scope.vendorPurchase.selectedVendor.locationType == 1) {
                $scope.dcVendorPurchaseItem.productStock.gstTotal = selectedProduct1.gstTotal;
                $scope.dcVendorPurchaseItem.productStock.cgst = selectedProduct1.gstTotal / 2;
                $scope.dcVendorPurchaseItem.productStock.sgst = selectedProduct1.gstTotal / 2;
                $scope.dcVendorPurchaseItem.productStock.igst = 0;
            }
            else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
                $scope.dcVendorPurchaseItem.productStock.gstTotal = selectedProduct1.gstTotal;
                $scope.dcVendorPurchaseItem.productStock.cgst = 0;
                $scope.dcVendorPurchaseItem.productStock.sgst = 0;
                $scope.dcVendorPurchaseItem.productStock.igst = selectedProduct1.igst > 0 ? selectedProduct1.igst : selectedProduct1.gstTotal;
            }
            else {
                $scope.dcVendorPurchaseItem.productStock.gstTotal = 0;
                $scope.dcVendorPurchaseItem.productStock.cgst = 0;
                $scope.dcVendorPurchaseItem.productStock.sgst = 0;
                $scope.dcVendorPurchaseItem.productStock.igst = 0;
            }

        }
    };
    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            window.localStorage.removeItem('p_data');
            $scope.draftCount = "";
            window.location.assign("/vendorPurchase/index");
        }
    };
    $scope.editCheque = function (item, bool, credit) {
        if (credit == true) {
            item.isCreditEdit = credit;
            item.isChequeEdit = bool;
            $scope.vendorPurchase.chequeNo = null;
            $scope.vendorPurchase.chequeDate = null;
        } else {
            item.isCreditEdit = credit;
            item.isChequeEdit = bool;
            $scope.vendorPurchase.creditNoOfDays = null;
        }
    };
    $scope.currency = function (N) {
        N = parseFloat(N);
        if (!isNaN(N))
            N = N.toFixed(2);
        else
            N = '0.00'; return N;
    };
    $scope.focusVendor = function (event) {
        if (event.which === 13) { // Enter key
            $("#vendorDc").trigger('chosen:open');
        }
    };
    $scope.keyEnter = function (event, nextid, currentid) {
        ele = document.getElementById(nextid);

        if (event.which === 13) {

            if (currentid == "expDateDc") {

                if ($scope.dcVendorPurchaseItem.productStock.expireDate == "" || $scope.dcVendorPurchaseItem.productStock.expireDate == undefined) {
                    return false;
                }
                if (!$scope.GSTEnabled) {
                    ele = document.getElementById("vatDc");
                } else {
                    ele = document.getElementById("dcgstTotal");
                    if ($scope.vendorPurchase.selectedVendor.locationType != 0 && $scope.vendorPurchase.selectedVendor.locationType != 1 && $scope.vendorPurchase.selectedVendor.locationType != 2) {
                        ele = document.getElementById("hsnCodeDc");
                    }
                }
            }

            if (currentid == "dcgstTotal") {

                if ($scope.dcVendorPurchaseItem.productStock.gstTotal == "" || $scope.dcVendorPurchaseItem.productStock.gstTotal == undefined) {
                    return false;
                }
            }

            if (currentid == "vatDc") {
                if ($scope.GSTEnabled) {
                    if ($scope.isIGST) {
                        ele = document.getElementById("dcigst");
                    } else {
                        ele = document.getElementById("dccgst");
                    }
                } else {
                    ele = document.getElementById("hsnCodeDc");
                }
            }
            if (currentid == "dcigst") {
                if ($scope.dcVendorPurchaseItem.productStock.igst == "" || $scope.dcVendorPurchaseItem.productStock.igst == undefined) {
                    return false;
                }
                if (!$scope.isCGST) {
                    ele = document.getElementById("hsnCodeDc");
                } else {
                    ele = document.getElementById("dccgst");
                }
            }
            if (currentid == "dccgst") {
                if ($scope.dcVendorPurchaseItem.productStock.cgst == "" || $scope.dcVendorPurchaseItem.productStock.cgst == undefined) {
                    return false;
                }
                ele = document.getElementById(nextid);
            }
            if (currentid == "dcsgst") {
                if ($scope.dcVendorPurchaseItem.productStock.sgst == "" || $scope.dcVendorPurchaseItem.productStock.sgst == undefined) {
                    return false;
                }
                ele = document.getElementById(nextid);
            }
            if (currentid == "hsnCodeDc") {
                ele = document.getElementById(nextid);
            }
            ele.focus();
        }

        ////BackSpace
        //if (event.which == 8) {
        //    var text = document.getElementById(currentid).value;
        //    if (text.length === 0) {
        //        ele = document.getElementById(previousid);
        //        ele.focus();
        //        event.preventDefault();
        //    }
        //}
    };
    //$scope.draftLoaded = false;
    //$scope.loadDraft = function () {
    //    if (window.localStorage.getItem("p_data") == null)
    //        return;
    //    $scope.draftLoaded = true;
    //    $scope.vendorPurchase = JSON.parse(window.localStorage.getItem("p_data"));
    //    $scope.updateUI();
    //    getGoodRNO();
    //};
    $scope.dayDiff = function (expireDate) {
        var val = (toDate($scope.addDc.expDateDc.$viewValue) > $scope.minDate);
        $scope.addDc.expDateDc.$setValidity("InValidexpDateDc", val);
        $scope.isFormValid = val;
        $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date($scope.formatString($scope.expireDate));
        var date1 = new Date($scope.formatString($scope.today));
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference < 30) {
            $scope.highlight = "highlight";
            $scope.addDc.expDateDc.$setValidity("InValidexpDateDc", false);
            $scope.isFormValid = false;
        } else {
            $scope.addDc.expDateDc.$setValidity("InValidexpDateDc", true);
            $scope.isFormValid = true;
            $scope.highlight = "";
        }
    };
    $scope.formatString = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    };
    $scope.checkMrp = function (purchase, sell, vat) {
        purchase = parseFloat(purchase) || 0;
        sell = parseFloat(sell) || 0;
        vat = parseFloat(vat) || 0;
        if (sell < purchase + (purchase * vat / 100)) {
            $scope.addDc.sellingPriceDc.$setValidity("checkMrpError", false);
        } else {
            $scope.addDc.sellingPriceDc.$setValidity("checkMrpError", true);
        }

        if (!$scope.enableSelling) {
            $scope.dcVendorPurchaseItem.packageMRP = $scope.dcVendorPurchaseItem.packageSellingPrice;
        } else {
            if ($scope.dcVendorPurchaseItem.packageMRP > 0) {
                $scope.checkSellingMrp($scope.dcVendorPurchaseItem.packageSellingPrice, $scope.dcVendorPurchaseItem.packageMRP);
            }
        }
    };
    $scope.checkSellingMrp = function (selling, mrp) {
        if ($scope.enableSelling != true)
            return;
        selling = selling != undefined ? parseFloat(selling) : 0;
        mrp = mrp != undefined ? parseFloat(mrp) : 0;
        if (mrp < selling || (mrp == 0)) {
            $scope.addDc.mrpDC.$setValidity("checkMrpError", false);
        } else {
            $scope.addDc.mrpDC.$setValidity("checkMrpError", true);
        }
    };
    $("#expDateDc").keyup(function (e) {
        if ($(this).val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $(this).val($(this).val() + "/");
        }
    });
    $scope.keyUp = function (e) {
        if ($("#expDateDc").val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $("#expDateDc").val($("#expDateDc").val() + "/");
        }
        if ($("#expDateDc").val().length <= 5) {
            var dt = $("#expDateDc").val();
            if (($("#expDateDc").val().length == 5) && (dt.charAt(2) == '/')) {
                $scope.addDc.expDateDc.$setValidity("InValidexpDateDc", true);
                $scope.dayDiff($scope.dcVendorPurchaseItem.productStock.expireDate);
                $scope.isFormValid = true;
                $scope.highlight = "";
            } else {
                $scope.highlight = "highlight";
                $scope.addDc.expDateDc.$setValidity("InValidexpDateDc", false);
                $scope.isFormValid = false;
            }
        } else {
            $scope.highlight = "highlight";
            $scope.addDc.expDateDc.$setValidity("InValidexpDateDc", false);
            $scope.isFormValid = false;
        }
    };
    var prod = productModel;
    $scope.search = prod;
    $scope.search = {
        "id": null,
        "accountId": null,
        "instanceId": null,
        "code": null,
        "name": null,
        "manufacturer": null,
        "kindName": null,
        "strengthName": null,
        "type": null,
        "schedule": null,
        "category": null,
        "genericName": null,
        "commodityCode": null,
        "packing": null,
        "packageSize": null,
        "vAT": null,
        "price": null,
        "status": null,
        "rackNo": null,
        "select": null,
        "page": {
            "pageNo": 1,
            "pageSize": 50
        },
        "getFilter": "Available"
    };
    $scope.ProductStocklist = [];
    $scope.productStockSearch = function () {
        productStockService.list($scope.search)
            .then(function (response) {
                $scope.ProductStocklist = response.data.list;
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
    };
    $scope.productStockSearch();
    $scope.changeGrossAmount = function (noteAmount) {
        var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax;
        roundingTotal(total);
        var grossvalue = $scope.vendorPurchase.grossValue;
        if ($scope.vendorPurchase.noteAmount != "") {
            //Credit Note (-)
            if ($scope.vendorPurchase.NoteType == 1) {
                if ($scope.vendorPurchase.grossValue > $scope.vendorPurchase.noteAmount) {
                    $scope.errorNoteAmount = false;
                    var netamount = parseInt($scope.vendorPurchase.grossValue) - parseInt($scope.vendorPurchase.noteAmount);
                    $scope.vendorPurchase.grossValue = netamount;
                } else {
                    $scope.errorNoteAmount = true;
                }
            }
            //Debit Note (+)
            if ($scope.vendorPurchase.NoteType == 2) {
                var netamount = parseInt($scope.vendorPurchase.grossValue) + parseInt($scope.vendorPurchase.noteAmount);
                $scope.vendorPurchase.grossValue = netamount;
            }
        } else {
            $scope.vendorPurchase.grossValue = grossvalue;
        }
    };
    $scope.changeType = function () {
        var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax;
        roundingTotal(total);
        $scope.vendorPurchase.noteAmount = 0;
    };
    $scope.discountclick = function (val) {
        $scope.discountType = val;
    };
    $scope.focusInvoice = function (e, nextid, currentid) {
        var text = document.getElementById(currentid).value;
        //Enter
        if (e.keyCode == 13) {
            if (text != "") {
                ele = document.getElementById(nextid);
                ele.focus();
            }
        }
        //BackSpace
        if (e.keyCode == 8) {
            if (text == "") {
                $("#vendor").trigger('chosen:open');
                event.stopPropagation();
                return false;
            }
        }
    };
    $scope.checkPackageSize = function (size) {
        if (size >= 1) {
            $scope.addDc.packageSizeDc.$setValidity("checkPackageSizeErrorDc", true);
        } else {
            $scope.addDc.packageSizeDc.$setValidity("checkPackageSizeErrorDc", false);
        }
        if (size == undefined) {
            $scope.addDc.packageSizeDc.$setValidity("checkPackageSizeErrorDc", true);
        }
    };
    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    };
    $scope.getEnableSelling();

    $scope.changeGST = function (value) {
        var type = $scope.vendorPurchase.selectedVendor.locationType;
        value = parseFloat(value) || 0;
        if ($scope.dcVendorPurchaseItem.packagePurchasePrice != undefined && $scope.dcVendorPurchaseItem.packageSellingPrice != undefined) {
            $scope.checkMrp($scope.dcVendorPurchaseItem.packagePurchasePrice, $scope.dcVendorPurchaseItem.packageSellingPrice, value);
        }
        //Local
        if (type == 0 || type == 1) {
            $scope.dcVendorPurchaseItem.productStock.cgst = (value / 2).toFixed(2);
            $scope.dcVendorPurchaseItem.productStock.sgst = (value / 2).toFixed(2);
        }
        //InterState
        if (type == 2) {
            $scope.dcVendorPurchaseItem.productStock.igst = value;
            $scope.dcVendorPurchaseItem.productStock.cgst = "";
            $scope.dcVendorPurchaseItem.productStock.sgst = "";
        }
    };


    $scope.getTaxValues = function () {
        vendorPurchaseService.getTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;
            if ($scope.taxValuesList.length > 0)
                $scope.dcVendorPurchaseItem.productStock.gstTotal = Object.keys($scope.taxValuesList)[0];
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    //by San 
    $scope.getTaxValues();

});

app.directive('focusBox1', function () {
    return {
        "link": function (scope, element, attrs) {
            element.bind("keydown", function (event) {
                var valueLength = attrs.$$element[0].value.length;
                var value = parseFloat(attrs.$$element[0].value);
                var ele = "";
                //Enter 
                if (event.which === 13) {
                    if (attrs.id === "freeQtyDc" || attrs.id === "discountDc") {
                        ele = document.getElementById(attrs.nextid);
                        ele.focus();
                    } else {
                        if (valueLength !== 0) {
                            if ((angular.isString(scope.selectedProduct1)) && (attrs.nextid == 'batchNoDc')) {
                                ele = document.getElementById("drugName1");
                                ele.focus();
                            } else {
                                if (attrs.id !== "vatDc") {
                                    if (value != 0) {
                                        ele = document.getElementById(attrs.nextid);
                                        ele.focus();
                                    }
                                } else {
                                    if (valueLength !== 0) {
                                        ele = document.getElementById(attrs.nextid);
                                        ele.focus();
                                    }
                                }
                            }
                        }
                    }
                }

            });
        }
    };
});

