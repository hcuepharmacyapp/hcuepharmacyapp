app.factory('vendorPurchaseService', function ($http) {
    return {

        "list": function (searchData) {
            return $http.post('/vendorPurchase/listData', searchData);
        },
        //Added by POongodi on 07/03/2017 to implement PO Cancel
        "cancellist": function (searchData) {
            return $http.post('/vendorPurchase/CancelListData', searchData);
        },

        "create": function (vendorPurchase) {
            return $http.post('/vendorPurchase/index', vendorPurchase);
        },
        //added by nandhini on 26.09.17
        "GetPurchaseVendorName": function (val) {
            return $http.post('/vendorPurchase/GetPurchaseVendorName?vendorName=' + val);
        },

        "GetDraftsByInstance": function () {
            return $http.get('/vendorPurchase/GetDraftsByInstance');
        },

        "DraftItemsByDraftId": function (draftId) {
            return $http.get('/vendorPurchase/DraftItemsByDraftId?draftId=' + draftId);
        },

        "savedDraft": function (draftvendorPurchase) {
            return $http.post('/vendorPurchase/SaveDraft', draftvendorPurchase);
        },

        "updateDraft": function (draftvendorPurchase) {
            return $http.post('/vendorPurchase/UpdateDraft', draftvendorPurchase);
        },

        "getAlldrafts": function () {
            return $http.get("/vendorPurchase/GetAllDrafts");
        },

        "DeleteDraft": function (draftVendorPurchaseId) {
            return $http.post("/vendorPurchase/DeleteDraft?draftVendorPurchaseId=" + draftVendorPurchaseId);
        },

        "saveTempVendorPurchaseItem": function (vendorPurchaseItem) {
            return $http.post('/vendorPurchase/saveTempVendorPurchaseItem', vendorPurchaseItem);
        },
        "getProductGST": function (productId) {
            return $http.get('/vendorPurchase/getProductGST?productId=' + productId);
        },
        "saveDcItem": function (dcitem) {
            return $http.post('/vendorPurchase/saveDcVendorPurchaseItem', dcitem);
        },
        "getDCItems": function (id) {
            return $http.get('/vendorPurchase/getDCItems?id=' + id);
        },
        "getDCItemsDropDown": function (id,dcNo) {
            return $http.get('/vendorPurchase/getDCItemsDropDown?id=' + id + '&dcNo=' + dcNo);
        },
        //POCancel service added by Poongodi on 06/03/2017
        "pocancel": function (id) {
            return $http.get('/vendorPurchase/POCancel?id=' + id);
        },
        "removeDcItem": function (dcitem) {
            return $http.post('/vendorPurchase/removeDcItem', dcitem);
        },
        "updateDcItem": function (item) {
            return $http.post('/vendorPurchase/updateDcItem', item);
        },

        "create1": function (vendorPurchase, file) {
            var formData = new FormData();
            formData.append('file', file);

            angular.forEach(vendorPurchase, function (value, key) {
                if (key === "vendorPurchaseItem") {
                    for (var i = 0; i < vendorPurchase.vendorPurchaseItem.length; i++) {
                        angular.forEach(vendorPurchase.vendorPurchaseItem[i], function (value, key) {
                            if (value === "" || value === null || value === undefined)
                                return;
                            formData.append("vendorPurchaseItem[" + i + "]." + key, value);
                            if (key === "productStock") {
                                angular.forEach(vendorPurchase.vendorPurchaseItem[i].productStock, function (value, key) {
                                    if (value === "" || value === null || value === undefined)
                                        return;
                                    formData.append("vendorPurchaseItem[" + i + "].productStock." + key, value);
                                });
                            }
                        });
                    }
                } else {
                    if (value === "" || value === null || value === undefined)
                        return;
                    formData.append(key, value.toString());
                }
            });

            return $http.post('/vendorPurchase/index', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });
        },
        "createVendorPurchaseReturn": function (vendorPurchaseReturn) {
            return $http.post('/vendorPurchaseReturn/index', vendorPurchaseReturn);
        },
        "createVendorPurchaseReturnItems": function (vendorPurchaseReturn) {
            return $http.post('/vendorPurchaseReturn/list', vendorPurchaseReturn);
        },
        "createVendorPurchaseBulkReturn": function (vendorPurchaseReturn) {
            return $http.post('/vendorPurchaseReturn/returnIndex', vendorPurchaseReturn);
        },

        //Newly Added Gavaskar 26-10-2016 Start
        "createVendorPurchaseBulkReturnExpire": function (vendorPurchaseReturn) {
            return $http.post('/vendorPurchaseReturn/expireReturnIndex', vendorPurchaseReturn);
        },

        "getBulkReturnExpire": function () {
            return $http.get('/vendorPurchaseReturn/getBulkReturnExpire');
        },

        //Newly Added Gavaskar 26-10-2016 End
        "vendorPurchaseReturnDetail": function (vendorPurchaseId) {
            return $http.get('/vendorPurchaseReturn/vendorPurchaseReturnDetail?id=' + vendorPurchaseId);
        },

        "populateOrderData": function (id) {
            return $http.post('/vendorPurchase/populateVendorPurchaseData?id=' + id);
        },
        "returnList": function (searchData, IsInvoiceDate, isFromReport) {
            if (searchData.offlineStatus == null)
                searchData.offlineStatus = 0;
            //console.log(JSON.stringify(searchData));
            return $http.post('/vendorPurchaseReturn/listData?IsInvoiceDate=' + IsInvoiceDate + '&isFromReport=' + isFromReport, searchData);
        },
        "editVendorPurchaseData": function (id) {
            return $http.post('/vendorPurchase/editVendorPurchaseData?id=' + id);
        },



        "SaveTaxType": function (data) {
            return $http.get('/vendorPurchase/SaveTaxType?TaxType=' + data);
        },

        // Added Gavaskar 07-02-2017 Start 

        "saveInvoiceDateEditOption": function (invoiceDateEditOption) {
            var formData = new FormData();
            formData.append('invoiceDateEditOption', invoiceDateEditOption);
            return $http.post('/vendorPurchase/saveBuyInvoiceDateEditOption', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });
        },

        "getBuyInvoiceDateEditSetting": function () {
            return $http.get('/vendorPurchase/getBuyInvoiceDateEditSetting');
        },

        // Added Gavaskar 07-02-2017 End

        "getTaxtype": function () {
            return $http.get('/vendorPurchase/getTaxtype');
        },
        "getCustomTaxSeriesItems": function () {
            return $http.get('/vendorPurchase/getCustomTaxSeriesItems');
        },
        "saveTaxseriesItem": function (data) {
            return $http.get('/vendorPurchase/saveTaxTypeseriesItem?TaxTypeSeries=' + data);
        },

        "saveDecimalSettings": function (data) {
            return $http.post('/vendorPurchase/saveAllowDecimalSetting', data);
        },

        "getDecimalSetting": function () {
            return $http.get('/vendorPurchase/getAllowDecimal');
        },

        "getPurchaseValues": function (id, name) {
            return $http.get('/vendorPurchase/getPurchaseDetails?id=' + id + '&name=' + escape(name));// Altered by Sarubala on 27-09-17
        },

        savecompletePurchaseKeyType: function (data) {
            return $http.post('/vendorPurchase/saveAllowCompletePurchaseKeyType', data);
        },

        getCompletePurchaseSetting: function () {
            return $http.get('/vendorPurchase/getAllowCompletePurchaseKeyType');
        },

        "update": function (vendorPurchase) {
            return $http.post('/vendorPurchase/UpdateVendorPurchase', vendorPurchase);
        },

        "update1": function (vendorPurchase, file) {
            var formData = new FormData();
            formData.append('file', file);
            angular.forEach(vendorPurchase, function (value, key) {
                if (key === "vendorPurchaseItem") {
                    for (var i = 0; i < vendorPurchase.vendorPurchaseItem.length; i++) {
                        angular.forEach(vendorPurchase.vendorPurchaseItem[i], function (value, key) {
                            if (value === "" || value === null || value === undefined)
                                return;
                            formData.append("vendorPurchaseItem[" + i + "]." + key, value);
                            if (key === "productStock") {
                                //var productName = vendorPurchase.vendorPurchaseItem[i].productStock.product.name;
                                //vendorPurchase.vendorPurchaseItem[i].productStock.product = null;
                                //vendorPurchase.vendorPurchaseItem[i].productStock.productName = productName;
                                angular.forEach(vendorPurchase.vendorPurchaseItem[i].productStock, function (value, key) {
                                    if (value === "" || value === null || value === undefined)
                                        return;
                                    formData.append("vendorPurchaseItem[" + i + "].productStock." + key, value);
                                });
                            }
                        });
                    }
                } else {
                    if (value === "" || value === null || value === undefined)
                        return;
                    formData.append(key, value.toString());
                }
            });

            return $http.post('/vendorPurchase/updateVendorPurchase', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });
        },

        "importBuy": function (vendorPurchase) {
            return $http.post('/importBuy/ImportData', vendorPurchase);
        },
        "getGRNO": function () {
            return $http.get('/vendorPurchase/getGRNO');
        },

        "getBulkReturn": function () {
            return $http.get('/vendorPurchaseReturn/getBulkReturn');
        },

        "isTempPurchaseItemAvail": function (productId) {
            //console.log("inside isTempPurchaseItemAvail service");
            return $http.get('/vendorPurchase/isTempPurchaseItemAvail?productId=' + productId);
        },

        "saveSearch": function (type) {
            return $http.post('/vendorPurchase/saveSearchType?searchType=' + type);
        },

        "getSearch": function () {
            return $http.get('/vendorPurchase/getSearchType');
        },

        "loadTempVendorPurchaseItem": function (productId) {
            //console.log("inside loadTempVendorPurchaseItem");
            return $http.get('/vendorPurchase/loadTempVendorPurchaseItem?productId=' + productId);
        },

        // Newly Added Gavaskar 22-12-2016 Start
        "getBuyImportDataFields": function (vendorPurchaseData) {
            return $http.post('/importBuy/BuyImportSetting', vendorPurchaseData);
        },


        "mappingData": function (file, vendorRowPos, productRowPos) {
            var formData = new FormData();
            formData.append('file', file);
            formData.append('vendorRowPos', vendorRowPos);
            formData.append('productRowPos', productRowPos);
            return $http.post('/importBuy/PurchaseImportSetting', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });

            //return $http.post('/importBuy/PurchaseImportSetting');
        },

        "saveBuyTemplateSetting": function (vendorPurchaseData) {
            return $http.post('/importBuy/SaveBuyTemplateSetting', vendorPurchaseData);
        },

        "getBuyTemplateSetting": function (vendorId) {
            return $http.get('/importBuy/getBuyTemplateSetting?vendorId=' + vendorId);
            //var formData = new FormData();
            //formData.append('vendorId', vendorId);
            //return $http.post('/importBuy/getBuyTemplateSetting', formData, {
            //    withCredentials: true,
            //    headers: { 'Content-Type': undefined },
            //    transformRequest: angular.identity
            //});
        },

        // Newly Added Gavaskar 22-12-2016 End

        // Newly Added Gavaskar 21-02-2017 Start

        "vendorData": function () {
            return $http.get('/importBuy/BuyImportVendorList');
        },


        // Newly Added Gavaskar 21-02-2017 End

        "getOfflineStatus": function () {
            return $http.get('/sales/getOfflineStatus');
        },
        getOnlineEnableStatus: function () {
            return $http.get('/sales/getOnlineEnabledStatus');
        },
        "getScanBarcodeOption": function () {
            return $http.get('/sales/getScanBarcodeOption');
        },
        "inActiveProduct": function (vendorPurchaseItem) {
            return $http.post("/vendorPurchase/inActiveProduct", vendorPurchaseItem);
        },
        getBillSeriesType: function () {
            return $http.get("/vendorPurchase/getBillSeriesType");
        },
        saveBillSeriesType: function (data) {
            return $http.post("/vendorPurchase/saveBillSeriesType", data);
        },
        getEnableSelling: function () {
            return $http.get('/vendorPurchase/getEnableSelling');
        },
        saveEnableSelling: function (enableSelling) {
            return $http.post('/vendorPurchase/saveEnableSelling', enableSelling);
        },
        //Added by Manivannan for Upload File
        uploadFile: function (file, vendor, defaultType, customType) {
            var formData = new FormData();
            formData.append('file', file);
            if (vendor != null)
                formData.append('vendor2', vendor);
            if (defaultType != null)
                formData.append('defaultType', defaultType);
            if (customType != null)
                formData.append('customType', customType);
            return $http.post('/ImportBuy/UploadFile', formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }
           );
        },
        // Added by Settu to implement payment status & remarks
        updateVendorPurchaseReturn: function (vendorPurchaseReturn) {
            return $http.post('/vendorPurchaseReturn/updateVendorPurchaseReturn', vendorPurchaseReturn);
        },
        // Added by Violet to implement New Return
        "getPurchaseReturnItem": function (productName) { //, VendorId
            return $http.get('/vendorPurchaseReturn/GetPurchaseReturnItem?productName=' + productName); //+ '&VendorId=' + VendorId
        },
        validateInvoice: function (data) { //, VendorId
            return $http.post("/vendorPurchase/validateInvoice", data); //+ '&VendorId=' + VendorId
        },
        loadActiveIndentList: function (orderId, type) {
            return $http.get('/StockTransferData/GetActiveIndentList?OrderId=' + orderId + '&Type=' + type);
        },
        SaveSortLowestPrice: function (data) {
            return $http.post('/vendorPurchase/saveisSortByLowestPriceSetting', data);
        },
        checkPaymentstatus: function (id) {
            return $http.get('/vendorPurchase/checkPaymentstatus?id=' + id);
        },
        getTaxValues: function () {
            return $http.get('/vendorPurchase/getTaxValues');
        },
        getAllTaxValues: function () {
            return $http.get('/vendorPurchase/GetAllTaxValues');
            },//added by nandhini 05.12.17
        saveTaxSeries: function (taxValues) {
            return $http.post('/vendorPurchase/saveTaxSeries' , taxValues);
        },
        updateTaxValuesStatus: function (taxValues) {
            return $http.post('/vendorPurchase/UpdateTaxValuesStatus', taxValues);
        },
        getBarcodeGenerationIsEnabled: function () {
            return $http.get('/vendorPurchase/GetBarcodeGenerationIsEnabled');
        },
        getInvoiceValueSettings: function () {
            return $http.get('/vendorPurchase/getInvoiceValueSettings');
        },
        saveInvoiceSettings: function (data) {
            return $http.post('/vendorPurchase/saveInvoiceValueSettings', data);
        },

        // Added by Sumathi on 27-Feb-19
        "deleteTempVendorPurchaseItem": function (data) {
            return $http.post('/vendorPurchase/DeleteTempVendorPurchaseItem', data);
        }
    };
});