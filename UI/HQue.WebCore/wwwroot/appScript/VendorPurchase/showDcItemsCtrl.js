app.controller('showDcItemsCtrl', function ($scope, vendorPurchaseModel, productStockModel, productModel, close, dCVendorPurchaseItemModel, DCPOValue, GSTEnabled, vendorPurchaseService) {

    $scope.GSTEnabled = GSTEnabled;
    $scope.PatientArray = [];
    $scope.minDate = new Date();
    var d = new Date();
    //ChequeMaxDate
    var today = new Date();
    today.setMonth(today.getMonth() + 6);
    $scope.chequeMaxDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    $scope.enableSelling = DCPOValue[2].enableSelling;
    $scope.selectedDcNo = {};
    $scope.dcNoFilter = undefined;
    $scope.isAllSelected = false;


    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        "opened": false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        "opened": false
    };

    $scope.open3 = function (purchaseItem) {
        purchaseItem.opened = true;

    };

    $scope.popup3 = {
        "opened": false
    };

    $scope.open4 = function () {
        $scope.popup4.opened = true;
    };

    $scope.popup4 = {
        "opened": false
    };

    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };


    $scope.close = function (result) {
        var object = {};
        var dcpoObject = { "DCItem": $scope.dcVendorPurchaseItem, "POItem": $scope.poVendorPurchaseItem };
        if (result == "Yes") {
            object = { "status": result, "data": dcpoObject };
        } else {
            object = { "status": result, "data": null };
        }
        close(object, 100);

        $(".modal-backdrop").hide();
    };

    $scope.shouldBeOpen = true;
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    var vendorPurchase = vendorPurchaseModel;
    var dcVendorPurchaseItem = dCVendorPurchaseItemModel;
    dcVendorPurchaseItem.productStock = productStockModel;
    dcVendorPurchaseItem.productStock.product = productModel;


    $scope.product = productModel;

    $scope.vendorPurchase = vendorPurchaseModel;
    $scope.dcVendorPurchaseItem = dcVendorPurchaseItem;
    $scope.Math = window.Math;


    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.checkSelectedDcItem = function () {
        if ($scope.selectedDcNo != null) {
            $scope.dcNoFilter = $scope.selectedDcNo.dcNo;
        } else {
            $scope.dcNoFilter = undefined;
        }
        $scope.addCount();
    }

    $scope.selectAll = function () {

        $scope.dcCount = 0;
        if ($scope.dcNoFilter == undefined) {
            angular.forEach($scope.dcVendorPurchaseItem, function (itm) {
                itm.isSelected = $scope.isAllSelected;
                if ($scope.isAllSelected) {
                    $scope.dcCount++;
                }
            });
        } else {
            angular.forEach($scope.dcVendorPurchaseItem, function (itm) {
                if (itm.dcNo == $scope.dcNoFilter) {
                    itm.isSelected = $scope.isAllSelected;
                    if ($scope.isAllSelected) {
                        $scope.dcCount++;
                    }
                }

            });
        }

    }

    $scope.isSelectedItem = 1;

    $scope.keyPressDown = function (e) {

        //if (e.keyCode == 38) {
        //    if ($scope.isSelectedItem == 1) {
        //        //return;
        //        $scope.isSelectedItem = ($scope.dcVendorPurchaseItem.length) + 1;
        //    }
        //    $scope.isSelectedItem--;
        //    var fs = $scope.
        //    e.preventDefault();

        //}
        //if (e.keyCode == 40) {
        //    if ($scope.isSelectedItem == $scope.dcVendorPurchaseItem.length) {
        //        // return;
        //        $scope.isSelectedItem = 0;
        //    }
        //    $scope.isSelectedItem++;
        //    e.preventDefault();
        //}
        //if (e.keyCode == 13) {
        //    $scope.submit($scope.dcVendorPurchaseItem[$scope.isSelectedItem - 1]);
        //}
        if (e.keyCode == 27) {
            $scope.close('No');
        }

    };

    $scope.isPOSelectedItem = 1;

    $scope.poKeyPressDown = function (e) {

        //if (e.keyCode == 38) {
        //    if ($scope.isPOSelectedItem == 1) {
        //        //return;
        //        $scope.isPOSelectedItem = ($scope.poVendorPurchaseItem.length) + 1;
        //    }
        //    $scope.isPOSelectedItem--;
        //    var fs = $scope.
        //    e.preventDefault();
        //}
        //if (e.keyCode == 40) {
        //    if ($scope.isPOSelectedItem == $scope.pocVendorPurchaseItem.length) {
        //        // return;
        //        $scope.isPOSelectedItem = 0;
        //    }
        //    $scope.isPOSelectedItem++;
        //    e.preventDefault();
        //}
        //if (e.keyCode == 13) {
        //    $scope.submit($scope.poVendorPurchaseItem[$scope.isPOSelectedItem - 1]);
        //}

        if (e.keyCode == 27) {
            $scope.close('No');
        }

    };


    $scope.save = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        $scope.netHighlight = "";
        var checkNetTotal = 0;
        if ($scope.vendorPurchase.initialValue) {
            checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
            if (checkNetTotal <= 1 && checkNetTotal >= -1) {
                $scope.netHighlight = "";
                purchaseCreate();
            } else {
                var cancel = window.confirm('Are you sure, Do you want to Update Stock?');
                if (cancel) {
                    purchaseCreate();
                } else {
                    $.LoadingOverlay("hide");
                    $scope.netHighlight = "highlight";
                    $scope.isProcessing = false;
                }
            }
        } else {
            purchaseCreate();
        }

    };


    $scope.modelOptions = {
        "debounce": {
            "default": 500,
            "blur": 250
        },
        "getterSetter": true
    };

    $scope.currency = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.00'; return N; };


    $scope.focusVendor = function (event) {
        if (event.which === 13) { // Enter key
            $("#vendor").trigger('chosen:open');
        }
    };
    $scope.keyEnter = function (event, e, previousid, currentid) {
        var ele = document.getElementById(e);
        //Enterkey
        if (event.which === 13) {
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
            {
                ele.focus();
                if (ele.nodeName != "BUTTON") {
                    ele.select();
                    event.preventDefault();
                }
            }
        }
        //BackSpace
        if (event.which == 8) {
            var text = document.getElementById(currentid).value;
            if (text.length === 0) {
                var ele = document.getElementById(previousid);
                ele.focus();
                event.preventDefault();
            }
        }
    };

    $scope.count = 0;
    $scope.dcCount = 0;
    $scope.addCountPo = function (val) {
        if (val == true) {
            $scope.count++;
        }
        else {
            $scope.count--;
        }
    };
    $scope.addCount = function () {

        $scope.dcCount = 0;
        var listcount = $scope.dcVendorPurchaseItem.length;
        angular.forEach($scope.dcVendorPurchaseItem, function (itm, ind) {
            if (itm.isSelected) {
                $scope.dcCount++;
            }
        });

        if ($scope.dcCount == listcount) {
            $scope.isAllSelected = true;
        } else {
            $scope.isAllSelected = false;
        }
    }

    $scope.addItems = function () {
        $scope.close("Yes");
    };


    if (DCPOValue[0].dcList1 != null && DCPOValue[0].dcList1.length > 0) {
        var dcVendorPurchaseItem = DCPOValue[0].dcList1;
        $scope.dcVendorPurchaseItem = dcVendorPurchaseItem;
        for (var i = 0; i < $scope.dcVendorPurchaseItem.length; i++) {
            $scope.dcVendorPurchaseItem[i].isSelected = false;
        }
    }

    if (DCPOValue[1].poList != null && DCPOValue[1].poList.length > 0) {
        $scope.poVendorPurchaseItem = DCPOValue[1].poList;
        for (var i = 0; i < $scope.poVendorPurchaseItem.length; i++) {
            $scope.poVendorPurchaseItem[i].isSelected = false;
        }
    }


});



