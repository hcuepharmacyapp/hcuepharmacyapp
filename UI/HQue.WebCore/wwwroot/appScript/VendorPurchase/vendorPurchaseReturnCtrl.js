app.controller('vendorPurchaseReturnCtrl', function ($scope, toastr, vendorReturnModel, vendorReturnItemModel, vendorPurchaseService) {

    var vendorPurchaseReturn = vendorReturnModel;
    var vendorPurchaseReturnItem = vendorReturnItemModel;

    $scope.total = 0;
    $scope.Math = window.Math;

    $scope.vendorPurchaseReturn = vendorPurchaseReturn;
    $scope.vendorPurchaseReturn.vendorPurchaseReturnItem = vendorPurchaseReturnItem;
    $scope.returnTotal = 0;

    $scope.init = function (id) {
        $.LoadingOverlay("show");
        $scope.vendorPurchaseReturn.vendorPurchaseId = id;

        vendorPurchaseService.vendorPurchaseReturnDetail($scope.vendorPurchaseReturn.vendorPurchaseId).then(function (response) {

            
            $scope.vendorPurchaseReturn = response.data;

            if ($scope.vendorPurchaseReturn.billSeries != undefined && $scope.vendorPurchaseReturn.billSeries != null) {
                $scope.vendorPurchaseReturn.goodsRcvNoTemp = $scope.vendorPurchaseReturn.billSeries.concat($scope.vendorPurchaseReturn.goodsRcvNo);
            }
            else {
                $scope.vendorPurchaseReturn.goodsRcvNoTemp = $scope.vendorPurchaseReturn.goodsRcvNo;
            }

            for (var i = 0; i < $scope.vendorPurchaseReturn.vendorPurchaseReturnItem.length; i++) {
                var noofstrips = 0;
                /*The below calculation section to avoid shows Return Qty greater than Purchase qty added by Poongodi on 19/04/2017 */
                var stock = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.stock;
                var qty = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].purchaseQuantity;
                var Returnqty = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].returnedQuantity;
                if (Returnqty == "undefined" || Returnqty == null)
                {
                    Returnqty = 0;
                }
                var Calqty = 0;
                if (stock >= (qty - Returnqty))
                {
                    Calqty = (qty - Returnqty);
                }
                else {
                    Calqty = stock;
                }
                    $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.returnedStrips = 0;
                //noofstrips = ($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.stock / $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.packageSize);
                    noofstrips = (Calqty / $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.packageSize);
                /*End Here*/
                    $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.noOfStrips = Math.floor(noofstrips);
                    $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.returnedStrips = ($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].packageQuantity - $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.noOfStrips);
            }
           

            setPrice();
            $.LoadingOverlay("hide");
        }, function () { 
           $.LoadingOverlay("hide");
        });
    }



    function setPrice() {
        var purchasedTotal = 0;
        var GSTEnabled = angular.element(document.getElementById("GSTEnabled")).val();
       
        for (var i = 0; i < $scope.vendorPurchaseReturn.vendorPurchaseReturnItem.length; i++) {

            $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].quantity = "";
            $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].discountValueOriginal = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].discountValue;

            //$scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].returnPurchasePrice = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total + (($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.vat) / 100);

            //var basePrice = ($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].packageQuantity - $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].freeQty) * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].vendorPurchaseItem.packagePurchasePrice;
            // added by Gavaskar 14-09-2017
            //$scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].discountValue = basePrice * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].discount / 100;
            //var pricewithoutvat = basePrice - (basePrice * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].discount / 100);

            //if (GSTEnabled == "True" && $scope.vendorPurchaseReturn.taxRefNo == 1)
            //{
            //    $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].pricewithvat = pricewithoutvat + (pricewithoutvat * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.gstTotal / 100);
            //}
            //else
            //{
            //    $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].pricewithvat = pricewithoutvat + (pricewithoutvat * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.vat / 100);
            //}

            //if ($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.cst > 0) {
            //    $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.total = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total + (($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.cst) / 100);
            //}
            //else
            //{
               
            //    if (GSTEnabled == "True" && $scope.vendorPurchaseReturn.taxRefNo == 1)
            //    {
            //        $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.total = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total + (($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.gstTotal) / 100);
            //    }
            //    else
            //    {
            //        $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.total = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total + (($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.vat) / 100);
            //    }

               
            //}
            
            $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].pricewithvat = ($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].returnPurchasePrice * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].packageSize) * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].packageQuantity;
            purchasedTotal += $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].pricewithvat;

            //$scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].returnPurchasePrice = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.total / $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].purchaseQuantity;
            //purchasedTotal += $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.total
        }

       
        $scope.vendorPurchaseReturn.purchasedTotal = purchasedTotal;
    }
    //if (item1.purchaseQuantity - item1.returnedQuantity - item1.quantity <= 0)
    $scope.disableSave = function (data) {
        for (var x = 0; x < data.length; x++)
            if (data[x].purchaseQuantity - data[x].quantity < 0)
                $scope.returnTotal = 0;        
    };

    $scope.editItem = function (ind,returnStrips, Existstrips) {



        if (Existstrips < returnStrips) {
            $scope.returnTotal = 0;
            
        } else {

         
          

             $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[ind].quantity = 0;
            $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[ind].quantity = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[ind].productStock.packageSize * returnStrips;


            var discount = 0;
            $scope.returnTotal = 0;
           
            for (var i = 0; i < $scope.vendorPurchaseReturn.vendorPurchaseReturnItem.length; i++) {
                if ($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].quantity !== "") {
                    if ($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].quantity > $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].purchaseQuantity) {
                    } else {
                        $scope.returnTotal += ($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].pricewithvat / $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].purchaseQuantity) * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].quantity;
                    }
                }
            }
        }
        $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[ind].discountValue = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[ind].discountValueOriginal * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[ind].quantity;
    }

    $scope.save = function () {
        $.LoadingOverlay("show");




        $scope.vendorPurchaseReturn.vendorPurchaseReturnItem.returnedTotal = $scope.returnTotal;
        var afterRoundUp = Math.round($scope.returnTotal);
        $scope.vendorPurchaseReturn.roundOffValue = afterRoundUp - $scope.returnTotal;
        $scope.vendorPurchaseReturn.totalReturnedAmount = Math.round($scope.returnTotal);
         
        vendorPurchaseService.createVendorPurchaseReturn($scope.vendorPurchaseReturn).then(function (response) {
            $scope.vendorPurchaseReturn = response.data;

 
            toastr.success('Data Saved Successfully');
            window.location.assign('/vendorPurchase/list');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    }

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to cancel?');
        if (cancel) {
            for (var i = 0; i < $scope.vendorPurchaseReturn.vendorPurchaseReturnItem.length; i++) {
                $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].quantity = "";
                $scope.returnTotal = 0;
            }
            window.location.assign('/vendorPurchase/list');
        }
    }

    //$scope.togglePageLoad = function () {
    //    $(".pat-info-btn").text('+');
    //    $(".alter-row-collapse").hide();
    //}

    //$scope.togglePageLoad();

    //$scope.togglePatientDetail = function () {
    //    $(".alter-row-collapse").slideToggle();
    //    $(".alter-row-collapse").show();
    //    if ($(".pat-info-btn").text() === '+')
    //        $(".pat-info-btn").text('-');
    //    else {
    //        $(".pat-info-btn").text('+');
    //    }
    //}

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }
    $scope.getEnableSelling = function () {
        vendorPurchaseService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getEnableSelling();
});
