app.controller('updatevendorPurchaseCtrl', function ($scope, toastr, vendorPurchaseService, vendorPurchaseModel, vendorPurchaseItemModel, productStockModel, vendorService, productService, productModel) {

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.open3 = function (purchaseItem) {
        purchaseItem.opened = true;
        // $scope.popup3.opened = true;
    };

    $scope.popup3 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    var vendorPurchase = vendorPurchaseModel;
    var vendorPurchaseItem = vendorPurchaseItemModel;
    vendorPurchaseItem.productStock = productStockModel;
    vendorPurchaseItem.productStock.product = productModel;

    $scope.product = productModel;

    $scope.vendorPurchase = vendorPurchase;
    $scope.vendorPurchaseItem = vendorPurchaseItem;
    $scope.vendorPurchaseItem.productStock.vat = 5;
    $scope.vendorPurchase.vendorPurchaseItem = [];
    $scope.selectedVendor = {};
    $scope.selectedProduct = null;
    $scope.Math = window.Math;

    $scope.vendorPurchase.invoiceDate = new Date();
    $scope.vendorPurchase.total = 0;
    $scope.vendorPurchase.discount = 0;
    $scope.vendorPurchase.credit = 0;
    $scope.list = [];
    $scope.editItems = 0;
    $scope.totalPayment = 0;

    function setTotal() {
        $scope.vendorPurchase.total = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            $scope.vendorPurchase.total += $scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty;
        }
    }

    function setVat() {
        $scope.vendorPurchase.vat = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            var total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty);
            $scope.vendorPurchase.vat += total * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat / 100);
        }
    }

    $scope.init = function (id) {
        $scope.vendorPurchase.id = id;

        if (id != "") {
            $.LoadingOverlay("show");
            vendorPurchaseService.editVendorPurchaseData($scope.vendorPurchase.id).then(function (response) {
                $scope.vendorPurchase = response.data;
                $scope.selectedVendor.id = $scope.vendorPurchase.vendorId;
               // $scope.vendorPurchase.invoiceDate = new Date();
                for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
                    $scope.vendorPurchase.vendorPurchaseItem[i].isEdit = true;
                    //$scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat = 5.5;
                }
                $scope.editItems = $scope.vendorPurchase.vendorPurchaseItem.length;
                setTotal();
                setVat();
                $scope.totalPayment = $scope.Math.ceil(($scope.vendorPurchase.total + $scope.vendorPurchase.vat) - (($scope.vendorPurchase.discount / 100) * ($scope.vendorPurchase.total + $scope.vendorPurchase.vat)));
                $scope.vendorPurchase.actualPaymentTotal = $scope.totalPayment;
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    }

    //$scope.addStock = function () {

    //    if ($scope.selectedProduct != null) {

    //        if (typeof $scope.selectedProduct === "string") {
    //            if (!confirm("This is a new product, do you want to add it ?")) {
    //                return;
    //            }
    //            $scope.vendorPurchaseItem.productStock.product.name = $scope.selectedProduct;
    //        } else {
    //            $scope.vendorPurchaseItem.productStock.product.name = $scope.selectedProduct.name;
    //            $scope.vendorPurchaseItem.productStock.productId = $scope.selectedProduct.id;
    //        }

    //        //$scope.vat = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) * ($scope.vendorPurchaseItem.productStock.vat / 100);
    //        //$scope.vendorPurchaseItem.total = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) + $scope.vat;

    //        $scope.vendorPurchaseItem.isEdit = false;
    //        $scope.vendorPurchase.vendorId = $scope.selectedVendor.id;
    //        $scope.vendorPurchase.vendorPurchaseItem.push($scope.vendorPurchaseItem);
    //        $scope.vendorPurchaseItems.$setPristine();
    //        $scope.vendorPurchaseItem = { productStock: { product: {} } };
    //        $scope.selectedProduct = null;
    //        $scope.vendorPurchaseItem.productStock.vat = 5;
    //        setTotal();
    //        setVat();
    //    }
    //}

    //$scope.removeStock = function (item) {
    //    $scope.vendorPurchase.total -= (item.productStock.sellingPrice * item.quantity);
    //    var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
    //    $scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
    //    setTotal();
    //    setVat();
    //}

    $scope.editPurchase = function (item) {
        item.isEdit = true;
        $scope.editItems = $scope.editItems + 1;
    }

    $scope.doneEditPurchase = function (item, isValid) {
        if (!isValid) {
            item.isEdit = false;
            $scope.editItems = $scope.editItems - 1;
        }
        setTotal();
        setVat();
    }

    $scope.update = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.update($scope.vendorPurchase).then(function (response) {
            $scope.list = response.data;
            toastr.success('Stock updated successfully');
            window.location.assign('/vendorPurchase/List');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    }

    $scope.vendor = function () {
        vendorService.vendorData().then(function (response) {
            $scope.vendorList = response.data;
        }, function () { toastr.error('Error Occured', 'Error'); });
    }
    $scope.vendor();

    $scope.getProducts = function (val) {

        return productService.drugFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.onProductSelect = function () {
        console.log($scope.selectedProduct);
        var qty = document.getElementById("batchNo");
        qty.focus();
    }

    $scope.modelOptions = {
        debounce: {
            default: 500,
            blur: 250
        },
        getterSetter: true
    };

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            window.location.assign("/VendorPurchase/list");
        }
    }

    $scope.editCheque = function (item, bool) {
        item.isChequeEdit = bool;
    }

});


