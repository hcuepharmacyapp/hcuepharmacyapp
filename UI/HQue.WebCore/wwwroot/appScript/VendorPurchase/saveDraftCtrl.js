app.controller('saveDraftCtrl', function ($scope, draftName, close) {
    $scope.draftName = draftName;
    $scope.saveDraft = function () {
        $scope.close($scope.draftName);
    };
    $scope.close = function (result) {
        close(result, 500);
        $(".modal-backdrop").hide();
    };
});
