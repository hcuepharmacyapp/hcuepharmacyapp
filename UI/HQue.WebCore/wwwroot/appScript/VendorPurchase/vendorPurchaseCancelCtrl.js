app.controller('vendorPurchaseSearchCtrl', function ($scope, toastr, vendorPurchaseService, vendorService, vendorPurchaseModel, vendorModel, pagerServcie, productService, productModel, $filter) {
   
    var vendorPurchase = vendorPurchaseModel;
    vendorPurchase.vendor = vendorModel;
   
    $scope.search = vendorPurchase;

    $scope.list = [];

    $scope.vendorStatus = 2;
    

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        $scope.list = [];
        //var target = $('#purchaseHistory');
        //target.LoadingOverlay("show");
       $.LoadingOverlay("show");
       vendorPurchaseService.cancellist($scope.search).then(function (response) {
            $scope.list = response.data.list;
           // target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function () { 
            $.LoadingOverlay("hide");
            //target.LoadingOverlay("hide");
        });
    }
    $scope.clearSearch = function () {
        $scope.search.drugName = "";
        $scope.search.vendorName = "";
        $scope.search.searchProductId = "";
        $scope.search.goodsRcvNo = "";
        $scope.search.invoiceNo = "";
        $scope.search.vendor = { vendor: {} };
        $scope.search.batchNo = "";
        $scope.search.invoiceDate = "";
        $scope.pageload();
        $scope.disable = true;
        $scope.search.fromDate =  $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.search.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    }

    

    $scope.purchaseSearch = function () {
        //var target = $('#purchaseHistory');
        //target.LoadingOverlay("show");

        //$scope.search.select = 'billDate'
        //$scope.search.select1 = 'equal';
        //$scope.salesCondition = false;
        //$scope.dateCondition = true;
        //$scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');

        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        $scope.search.searchProductId = "";
        if ($scope.search.drugName)
            $scope.search.values = $scope.search.drugName.id;

        vendorPurchaseService.cancellist($scope.search).then(function (response) {

            if (response.data.list == undefined) {
                $scope.list = [];
            } else {
                $scope.list = response.data.list;
            }

           // $scope.list = response.data.list;
            $scope.vendorPurchase1 = JSON.parse(window.localStorage.getItem("p_dataChange"));
            pagerServcie.init(response.data.noOfRows, pageSearch);
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function () {
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        });
    }

  //  $scope.purchaseSearch();


    $scope.pageload = function () {
        $scope.search.select = ''
        $scope.search.select1 = 'equal';
        $scope.salesCondition = false;
        $scope.dateCondition = false;
        $scope.vendorname = false;
        $scope.search.values = '';
        $scope.search.selectValue = '';
        $scope.search.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.search.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.maxDate = new Date();
        $scope.productCondition = false;
        $scope.purchaseSearch();
    }

    $scope.pageload();
    $scope.disable = true;
    $scope.changefilters = function () {
        $scope.search.select1 = "";
        $scope.search.values = "";
        $scope.search.invoiceDate = "";
        $scope.list = [];
        $scope.vendorname = false;
        $scope.search.drugName = undefined;

        if ($scope.search.select == '' || $scope.search.select == undefined)
        {
            $scope.disable = true;
            $scope.search.select = '';
        }
        else
        {
            $scope.disable = false;
        }
        if ($scope.search.select == 'quantity' || $scope.search.select == 'mrp' || $scope.search.select == 'discount' || $scope.search.select == 'vat') {
            $scope.salesCondition = true;
            $scope.dateCondition = false;
       
            $scope.vendorname = false;
            $scope.productCondition = false;
        }
        else if ($scope.search.select == 'billDate') {
            $scope.salesCondition = false; // Modified Gavaskar 01-03-2017
            $scope.dateCondition = true;
            $scope.disable = true;
            $scope.productCondition = false;
            $scope.vendorname = false;
            $scope.maxDate = new Date();
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd'); // Added Gavaskar 01-03-2017
        } else if ($scope.search.select == 'expiry') {
            $scope.salesCondition = false;// Modified Gavaskar 01-03-2017
            $scope.dateCondition = true;
            $scope.vendorname = false;
            $scope.productCondition = false;
            $scope.maxDate = "";
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd'); // Added Gavaskar 01-03-2017
        }
        else if ($scope.search.select == 'product') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.vendorname = false;
            $scope.productCondition = true;
        }
        else if ($scope.search.select == 'vendorName') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
        
            $scope.vendorname = true;
            $scope.productCondition = false;
        }
        else {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
        }
    };

    /*Cancel method added to Cance the PO*/
  
    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) { //vendorService.vendorAllData().then(function (response) { //vendorData
            $scope.vendorList = response.data;
        }, function () { });
    }
    $scope.vendor();

     $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper'+ row).slideToggle();
        $('#chip-wrapper'+ row).show();
        if ($('#chip-btn'+ row).text() === '+')
            $('#chip-btn'+ row).text('-');
        else {
            $('#chip-btn'+ row).text('+');
        }
     }

     $scope.currency = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.00'; return N; }
     $scope.currency3 = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.000'; return N; }


    //by San
     //$scope.minDate = new Date();

     $scope.open1 = function () {
         $scope.popup1.opened = true;
     };

     $scope.popup1 = {
         opened: false
     };
     
     $scope.open2 = function () {
         $scope.popup2.opened = true;
     };

     $scope.popup2 = {
         opened: false
     };
     $scope.open3 = function () {
         $scope.popup3.opened = true;
     };
     $scope.popup3 = {
         opened: false
     };
     $scope.open4 = function () {
         $scope.popup4.opened = true;
     };
     $scope.popup4 = {
         opened: false
     };
     $scope.dateOptions = {
         formatYear: 'yy',
         startingDay: 1
     };
     $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
     $scope.format = $scope.formats[2];
     $scope.altInputFormats = ['M!/d!/yyyy'];
    
     $scope.product = productModel;
     
     $scope.getProducts = function (val) {

         return productService.InstancedrugFilter(val).then(function (response) {
             return response.data.map(function (item) {
                 return item;
             });
         });
     }

     $scope.selectedProduct = null;

     $scope.printBill = function (vendorPurchase) {

         //var isDotMatrix = JSON.parse(window.localStorage.getItem("IsDotMatrix"));
         var isDotMatrix = 1;
         if (isDotMatrix == 1) {
             //var w = window.open("/PurchaseInvoice/printdm?id=" + vendorPurchase.id);
             window.open("/PurchaseInvoice/printdmA4?id=" + vendorPurchase.id);
             //w.window.print();
         }
         else if (isDotMatrix == 2) {
             //window.open("/PurchaseInvoice/printdmA5h?id=" + vendorPurchase.id);
             window.open("/PurchaseInvoice/printdmA4?id=" + vendorPurchase.id);
         }
         else if (isDotMatrix == 3) {
             window.open("/PurchaseInvoice/printdmA4?id=" + vendorPurchase.id);
         }
         else if (isDotMatrix == 4) {
             //window.open("/PurchaseInvoice/printdmA4NH?id=" + vendorPurchase.id);
             window.open("/PurchaseInvoice/printdmA4?id=" + vendorPurchase.id);
         }
         else {
             window.open("/PurchaseInvoice/index?id=" + vendorPurchase.id);
         }
     }

    // Added by arun for keyenter event on 21.03.17 told by poongodi 

    $scope.keyEnter = function (event, e) {



        console.log(e);
        if (event.which === 8) {
            if ($scope.customerHelper.data.patientSearchData.name == "" || $scope.customerHelper.data.patientSearchData.name == undefined) {
                $scope.Namecookie = "";
            }
        }
        var ele = document.getElementById(e);

        if (event.which === 13) // Enter key
        {
                ele.focus();
       
        }
       

        if (event.which === 9) {
            $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
        }



    };


    $scope.updateUI = function (vendorPurchase) {
        var cst = document.getElementById("cST");
        var vat = document.getElementById("vat");
        $scope.search.values = vendorPurchase.name;
        
       
    };

    $scope.onProductSelect = function()
    {
        $scope.search.values = $scope.search.drugName.name;
    }

    $scope.getEnableSelling = function () {
        vendorPurchaseService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getEnableSelling();

});

app.filter('singleDecimal', function ($filter) {
    return function (input) {
        if (isNaN(input)) return input;
        return Math.round(input * 10) / 10;
    };
});