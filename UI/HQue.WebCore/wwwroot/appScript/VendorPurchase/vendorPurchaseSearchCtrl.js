app.controller('vendorPurchaseSearchCtrl', function ($scope, vendorPurchaseService, vendorService, vendorPurchaseModel, vendorModel, pagerServcie, productService, productModel, $filter, toastr, ModalService,patientService) {

    var vendorPurchase = vendorPurchaseModel;
    vendorPurchase.vendor = vendorModel;

    $scope.search = vendorPurchase;

    $scope.list = [];
    $scope.enableSelling = false;


    $scope.isOffline = false;
    $scope.vendorStatus = 2;

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        $scope.list = [];
        //var target = $('#purchaseHistory');
        //target.LoadingOverlay("show");
        $.LoadingOverlay("show");
        vendorPurchaseService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            // target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            // toastr.error("Error Occured", 'Error');
            $.LoadingOverlay("hide");
            //target.LoadingOverlay("hide");
        });
    }
    $scope.clearSearch = function () {
        $scope.search.drugName = "";
        $scope.search.searchProductId = "";
        $scope.search.goodsRcvNo = "";
        $scope.search.invoiceNo = "";
        $scope.search.vendor = { vendor: {} };
        $scope.search.batchNo = "";
        $scope.search.invoiceDate = "";
        $scope.search.name = "";  //Added by Nandini on 26-09-17
        $scope.VendorCondition = false;
        $scope.pageload();
    };

    $scope.prePurchaseSearch = function () {
        if (!$scope.PurchaseHistoryList.$invalid) {
            $scope.purchaseSearch();
        }
    };

    $scope.purchaseSearch = function () {
        //var target = $('#purchaseHistory');
        //target.LoadingOverlay("show");

        //$scope.search.select = 'billDate'
        //$scope.search.select1 = 'equal';
        //$scope.salesCondition = false;
        //$scope.dateCondition = true;
        //$scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');

        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        $scope.search.searchProductId = "";
               if ($scope.search.drugName)
            $scope.search.values = $scope.search.drugName.id;

        vendorPurchaseService.list($scope.search).then(function (response) {

            if (response.data.list == undefined) {
                $scope.list = [];
            } else {
                $scope.list = response.data.list;
            }

            // $scope.list = response.data.list;
            $scope.vendorPurchase1 = JSON.parse(window.localStorage.getItem("p_dataChange"));
            pagerServcie.init(response.data.noOfRows, pageSearch);
            //The following lines added to display the top10records in page load
            if ($scope.search.select1 == "top10") {
                $scope.search.select1 = "equal";

            }

            $scope.showIstenRecords = false;
            if ($scope.ispageload == 1) {
                $scope.showIstenRecords = true;
                $scope.ispageload = 0;
            }
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function (error) {
            //  toastr.error("Error Occured", 'Error');
            console.log(error);
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        });
    };

    //  $scope.purchaseSearch();

    $scope.ispageload = 0;
    $scope.pageload = function () {
        $scope.search.select = '';
        $scope.search.VendorId = '';
        $scope.search.select1 = '';
        $scope.salesCondition = false;
        $scope.dateCondition = false;
        $scope.search.values = '';// $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.search.selectValue = '';// $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.maxDate = new Date();
        $scope.productCondition = false;
        //The following block added to diplay top 10records on pageload
        $scope.search.select1 = "top10";
        $scope.ispageload = 1;
        $scope.productCondition = false;
        $scope.EnterCondition = false;
        $scope.purchaseSearch();
    };

    $scope.pageload();
    $scope.Namecookie = "";
    $scope.getVendor = function (val) {
    
        $scope.Namecookie = val;
       
        return vendorPurchaseService.GetPurchaseVendorName(val).then(function (response) {

           
            return response.data.map(function (item) {
                return item;
      
            });
        });
    };

    $scope.onVendorSelect = function (obj, event) {
        if (obj !== undefined) {           
            $scope.search.VendorId = obj.id;
        }




    };

    $scope.changefilters = function () {
        $scope.search.select1 = "";
        $scope.search.values = "";
        $scope.search.VendorId = "";
        $scope.search.name = "";
        $scope.search.invoiceDate = "";
        $scope.search.drugName = undefined;
        if ($scope.search.select == 'quantity' || $scope.search.select == 'mrp' || $scope.search.select == 'discount' || $scope.search.select == 'vat') {
            $scope.salesCondition = true;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.VendorCondition = false;
            $scope.EnterCondition = false;
        } else if ($scope.search.select == 'billDate') {
            $scope.salesCondition = false; // Modified Gavaskar 01-03-2017
            $scope.dateCondition = true;
            $scope.productCondition = false;
            $scope.EnterCondition = false;
            $scope.VendorCondition = false;
            $scope.maxDate = new Date();
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd'); // Added Gavaskar 01-03-2017
        } else if ($scope.search.select == 'expiry') {
            $scope.salesCondition = false;// Modified Gavaskar 01-03-2017
            $scope.dateCondition = true;
            $scope.productCondition = false;
            $scope.EnterCondition = false;
            $scope.VendorCondition = false;
            $scope.maxDate = "";
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd'); // Added Gavaskar 01-03-2017
        } else if ($scope.search.select == 'product') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = true;
            $scope.EnterCondition = false;
            $scope.VendorCondition = false;
        } else if ($scope.search.select == 'batchNo' || $scope.search.select == 'invoiceNo' || $scope.search.select == 'grNo' || $scope.search.select == 'email' || $scope.search.select == 'mobile') {
            $scope.salesCondition = false;
            $scope.VendorCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.EnterCondition = true;
        }
        else if ($scope.search.select == 'vendorName') {
            $scope.salesCondition = false;
            $scope.VendorCondition = true;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.EnterCondition = false;
            }
else {
            $scope.search.select = '';
            $scope.search.select1 = '';
            $scope.salesCondition = false;
            $scope.VendorCondition = false;
            $scope.dateCondition = false;
            $scope.search.values = '';
            $scope.search.name = '';
            $scope.search.selectValue = '';
            $scope.search.VendorId = '';
            $scope.maxDate = new Date();
            $scope.productCondition = false;
            $scope.search.select1 = "top10";
            $scope.ispageload = 1;
            $scope.productCondition = false;
            $scope.EnterCondition = false;
            $scope.purchaseSearch();
        }
    };

    /*Cancel method added to Cance the PO*/
    $scope.CancelItem = function (id) {
        console.log(id);
        if (!$scope.IsOffline) {
            $.LoadingOverlay("show");

            var r = confirm("Are you sure want to Cancel the selected Vendor Purchase ?");

            if (r == true) {
                console.log(JSON.stringify(id));

                vendorPurchaseService.pocancel(id).then(function (response) {
                    $scope.vendorPurchase = response.data;
                    console.log($scope.vendorPurchase.poStatus);
                    if (response.data == 2) {
                        alert("Cannot cancel available stock is lesser than the Purchase Quantitiy.");
                    } else if (response.data == 1) {
                        alert("Cannot cancel Product Retruned");
                    } else if (response.data == 3) {
                        alert("Cannot cancel Payment made Partially / Fully");
                    } else if (response.data == 4) {
                        alert("Cannot cancel Fully Paid");
                    } else if (response.data == 5) {
                        alert("Cannot cancel product already in DC");
                    } else if (response.data == 6) {
                        alert("Cannot cancel product already in TempStock");
                    } else {
                        alert("Selected Vendor Purchase Cancelled successfully");
                        $scope.purchaseSearch();
                    }
                    // window.location.assign('/SalesReturn/CancelList');
                    $.LoadingOverlay("hide");
                }, function (error) {
                    //  toastr.error("Error Occured", 'Error');
                    console.log(error);
                    $.LoadingOverlay("hide");
                    // toastr.error('Error Occured', 'Error');
                });

            } else {
                $scope.vendorPurchase = [];
                $.LoadingOverlay("hide");
            }
        }



    };

    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) {    //vendorService.vendorData().then(function (response) {
            $scope.vendorList = response.data;
        }, function (error) {
            console.log(error);
            //  toastr.error("Error Occured", 'Error');

        });
    };
    $scope.vendor();

    $scope.paymentProcess = function (val, status) {
        var flag = false;
        if (val != null) {
            vendorPurchaseService.checkPaymentstatus(val).then(function (response) {
                flag = response.data;
                if (flag && status == 2) {
                    //toastr.info("Please change Vendor Status..", 'InActive Vendor');
                    toastr.info("Can't Edit Inactive Vendor's Purchase.. Please change the vendor status", 'InActive Vendor');
                    return;
                }
                if (parseInt(status) != 2) {
                    window.location = "/vendorPurchase/Index?id=" + val;
                }
            });
        }        
    }

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.currency = function (N) {
        N = parseFloat(N);
        if (!isNaN(N)) {
            N = N.toFixed(2);
        } else {
            N = '0.00';
        }
        return N;
    };
    $scope.currency3 = function (N) {
        N = parseFloat(N);
        if (!isNaN(N)) {
            N = N.toFixed(2);
        } else {
            N = '0.000';
        }
        return N;
    };


    //by San
    //$scope.minDate = new Date();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    // Added Gavaskar 01-03-2017  

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.product = productModel;

    $scope.getProducts = function (val) {

        return productService.InstancedrugFilter(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.selectedProduct = null;

    $scope.printBill = function (vendorPurchase) {

        //var isDotMatrix = JSON.parse(window.localStorage.getItem("IsDotMatrix"));
        var isDotMatrix = 1;
        if (isDotMatrix == 1) {
            //var w = window.open("/PurchaseInvoice/printdm?id=" + vendorPurchase.id);
            window.open("/PurchaseInvoice/printdmA4?id=" + vendorPurchase.id);
            //w.window.print();
        } else if (isDotMatrix == 2) {
            //window.open("/PurchaseInvoice/printdmA5h?id=" + vendorPurchase.id);
            window.open("/PurchaseInvoice/printdmA4?id=" + vendorPurchase.id);
        } else if (isDotMatrix == 3) {
            window.open("/PurchaseInvoice/printdmA4?id=" + vendorPurchase.id);
        } else if (isDotMatrix == 4) {
            //window.open("/PurchaseInvoice/printdmA4NH?id=" + vendorPurchase.id);
            window.open("/PurchaseInvoice/printdmA4?id=" + vendorPurchase.id);
        } else {
            window.open("/PurchaseInvoice/index?id=" + vendorPurchase.id);
        }
    };

    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            // toastr.error("Error Occured", 'Error');
            console.log(error);
        });
    };
    $scope.getEnableSelling();

    getOfflineStatus = function () {
        vendorPurchaseService.getOfflineStatus()
            .then(function (response) {
                if (response.data) {
                    //toastr.error("This is for online mode");
                    $scope.isOffline = true;
                    $scope.offlineGreyButton = "offline-grey-button";
                    $scope.paidMsg = $scope.offlineMsg;
                } else {
                    $scope.isOffline = false;
                }
            });
    };
    getOfflineStatus();

    $scope.TransferItem = function (id) {
        $scope.type = 2;

        var data = {
            msgTitle: "",
            msg: "Only stock available items will be transferred. Do you wish to continue?",
            showOk: false,
            showYes: true,
            showNo: true,
            showCancel: false,
            redirectUrl: "",
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {

                if (result == "Yes") {
                    $.LoadingOverlay("show");
                    vendorPurchaseService.loadActiveIndentList(id, $scope.type).then(function (response) {

                        if (response.data.length == 0) {
                            ShowConfirmMsgWindow("It seems stock not available for all items/It is already transferred.");
                        }
                        else {
                            var data = {
                                data: response.data[0],
                                fromVendorPurchase: true,
                            };
                            window.localStorage.setItem("IndentStockTransfer", JSON.stringify(data));
                            window.location = "/StockTransfer/Index";
                        }

                        $.LoadingOverlay("hide");
                    }, function () {
                        $.LoadingOverlay("hide");
                    });
                }
                else {
                    return;
                }

            });
        });
    };

    function ShowConfirmMsgWindow(msg) {
        var data = {
            msgTitle: "",
            msg: msg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: "",
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                return result;
            });
        });
    }
});

app.filter('singleDecimal', function ($filter) {
    return function (input) {
        if (isNaN(input)) {
            return input;
        }
        return Math.round(input * 10) / 10;
    };
});