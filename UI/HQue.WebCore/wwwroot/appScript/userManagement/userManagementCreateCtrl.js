app.controller('userManagementCreateCtrl', function ($scope, hQueUserModel, ModalService, userAccessModel, userManagementService, toastr) {

    var user = hQueUserModel;
    user.access = userAccessModel;

    $scope.user = user;
    $scope.access = user.access;

    $scope.user.roleAccess = [];

    $scope.validAccess = 0;

    $scope.number = 0;

    $scope.checkAccessModel = {
        "Admin": false,
        "Sell": false,
        "SellEdit": false,
        "SellCancel": false,
        "Buy": false,
        "Leads": false,
        "Order": false,
        "Profile": false,
        "Inventory": false,
        "Reports": false,
        "Branch": false,
        "Accounts": false,
        "UserManagement": false,
        "EditMrp": false,
        "PurchaseEdit": false,
        "StockTransfer": false,
        "BillDateEdit": false,
        "Estimate": false,
        "EstimateEdit": false,
        "Dashboard": false,
        "addNewProduct": false,
        "PurchaseCancel": false, //Added by Poongodi on 06/03/2017
        "SalesTempStock": false, //by San
        "StockAdjustMent": false, //by San
        "VendorProfile": false,
        "Settings": false
    };

    $scope.$watchCollection('checkAccessModel', function () {
        $scope.checkResults = [];
        angular.forEach($scope.checkAccessModel, function (value, key) {
            if (value) {
                if (key == "Admin") {
                    $scope.number = 1;
                }

                if (key == "Sell") {
                    $scope.number = 2;
                }
                if (key == "Buy")
                    $scope.number = 3;
                if (key == "Leads")
                    $scope.number = 4;
                if (key == "Order")
                    $scope.number = 5;
                if (key == "Profile")
                    $scope.number = 6;
                if (key == "Inventory")
                    $scope.number = 7;
                if (key == "Reports")
                    $scope.number = 8;
                if (key == "Branch")
                    $scope.number = 9;
                if (key == "Accounts")
                    $scope.number = 10;
                if (key == "UserManagement")
                    $scope.number = 11;
                if (key == "EditMrp")
                    $scope.number = 12;
                if (key == "PurchaseEdit")
                    $scope.number = 13;
                if (key == "StockTransfer")
                    $scope.number = 14;
                if (key == "SellEdit")
                    $scope.number = 15;
                if (key == "SellCancel")
                    $scope.number = 16;
                if (key == "BillDateEdit")
                    $scope.number = 17;
                if (key == "Estimate")
                    $scope.number = 18;
                if (key == "EstimateEdit")
                    $scope.number = 19;
                if (key == "Dashboard")
                    $scope.number = 25;
                if (key == "addNewProduct") { $scope.number = 26; }
                if (key == "PurchaseCancel")  //Added by Poongodi on 06/03/2017
                    $scope.number = 27;
                if (key == "SalesTempStock") 
                    $scope.number = 28;
                if (key == "StockAdjustMent")
                    $scope.number = 29;
                if (key == "VendorProfile")
                    $scope.number = 30;
                if (key == "Settings")
                    $scope.number = 31;

                $scope.checkResults.push($scope.number);
            }
        });
    });

    $scope.updateAccess = function (val1) {
        if (val1) {
            $scope.validAccess++;
        }
        else {
            $scope.validAccess--;
        }
    };


    $scope.userList = [];

    $scope.hcueuserList = function () {
        $.LoadingOverlay("show");
        userManagementService.userAccessList($scope.user).then(function (response) {
            $scope.userList = response.data;

            angular.forEach($scope.userList, function (data) {
                angular.forEach(data.roleAccess, function (obj) {
                    if (obj.accessRight == 1)
                        data.Admin = true;
                    if (obj.accessRight == 2)
                        data.Sell = true;
                    if (obj.accessRight == 3)
                        data.Buy = true;
                    if (obj.accessRight == 4)
                        data.Leads = true;
                    if (obj.accessRight == 5)
                        data.Order = true;
                    if (obj.accessRight == 6)
                        data.Profile = true;
                    if (obj.accessRight == 7)
                        data.Inventory = true;
                    if (obj.accessRight == 8)
                        data.Reports = true;
                    if (obj.accessRight == 9)
                        data.Branch = true;
                    if (obj.accessRight == 10)
                        data.Accounts = true;
                    if (obj.accessRight == 11)
                        data.UserManagement = true;
                    if (obj.accessRight == 12)
                        data.EditMrp = true;
                    if (obj.accessRight == 13)
                        data.PurchaseEdit = true;
                    if (obj.accessRight == 14)
                        data.StockTransfer = true;
                    if (obj.accessRight == 15)
                        data.SellEdit = true;
                    if (obj.accessRight == 16)
                        data.SellCancel = true;
                    if (obj.accessRight == 17)
                        data.BillDateEdit = true;
                    if (obj.accessRight == 18)
                        data.Estimate = true;
                    if (obj.accessRight == 19)
                        data.EstimateEdit = true;
                    if (obj.accessRight == 25)
                        data.Dashboard = true;
                    if (obj.accessRight == 26) { data.addNewProduct = true; }
                    if (obj.accessRight == 27) //Added by Poongodi on 06/03/2017
                        data.PurchaseCancel = true;
                    if (obj.accessRight == 28)
                        data.SalesTempStock = true;
                    if (obj.accessRight == 29)
                        data.StockAdjustMent = true;
                    if (obj.accessRight == 30)
                        data.VendorProfile = true;
                    if (obj.accessRight == 31)
                        data.Settings = true;
                });
            });
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.hcueuserList();

    $scope.create = function () {
        angular.forEach($scope.checkResults, function (value, key) {
            $scope.user.roleAccess.push({ "accessRight": value });
        });
        $.LoadingOverlay("show");
        userManagementService.create($scope.user).then(function (response) {
            toastr.success('User created successfully');
            $scope.hcueuserList();
            $scope.accessItems.$setPristine();
            $scope.user = {
                "roleAccess": []
            };
            $scope.checkAccessModel = {};
            $scope.validAccess = 0;
            $.LoadingOverlay("hide");
        }, function (response) {
            $.LoadingOverlay("hide");
            toastr.error('User already exists', 'error');
        });
    };

    $scope.update = function (item) {
        $.LoadingOverlay("show");
        item.roleAccess.length = 0;
        var accessCount = 0;

        if (item.Admin === true) {
            item.roleAccess.push({ "accessRight": 1 });
        }
        else {
            accessCount++;
        }
        if (item.Sell === true) {
            item.roleAccess.push({ accessRight: 2 });
        }
        else {
            accessCount++;
        }
        if (item.Buy === true) {
            item.roleAccess.push({ accessRight: 3 });
        }
        else {
            accessCount++;
        }
        if (item.Leads === true) {
            item.roleAccess.push({ accessRight: 4 });
        }
        else {
            accessCount++;
        }
        if (item.Order === true) {
            item.roleAccess.push({ accessRight: 5 });
        }
        else {
            accessCount++;
        }
        if (item.Profile === true) {
            item.roleAccess.push({ accessRight: 6 });
        }
        else {
            accessCount++;
        }
        if (item.Inventory === true) {
            item.roleAccess.push({ accessRight: 7 });
        }
        else {
            accessCount++;
        }
        if (item.Reports === true) {
            item.roleAccess.push({ accessRight: 8 });
        }
        else {
            accessCount++;
        }
        if (item.Branch === true) {
            item.roleAccess.push({ accessRight: 9 });
        }
        else {
            accessCount++;
        }
        if (item.Accounts === true) {
            item.roleAccess.push({ accessRight: 10 });
        }
        else {
            accessCount++;
        }
        if (item.UserManagement === true) {
            item.roleAccess.push({ accessRight: 11 });
        }
        else {
            accessCount++;
        }
        if (item.EditMrp === true) {
            item.roleAccess.push({ accessRight: 12 });
        }
        else {
            accessCount++;
        }
        if (item.PurchaseEdit === true) {
            item.roleAccess.push({ accessRight: 13 });
        }
        else {
            accessCount++;
        }
        if (item.StockTransfer === true) {
            item.roleAccess.push({ accessRight: 14 });
        }
        else {
            accessCount++;
        }
        if (item.SellEdit === true) {
            item.roleAccess.push({ accessRight: 15 });
        }
        else {
            accessCount++;
        }
        if (item.SellCancel === true) {
            item.roleAccess.push({ accessRight: 16 });
        }
        else {
            accessCount++;
        }
        if (item.BillDateEdit === true) {
            item.roleAccess.push({ accessRight: 17 });
        }
        else {
            accessCount++;
        }
        if (item.Estimate === true) {
            item.roleAccess.push({ accessRight: 18 });
        }
        else {
            accessCount++;
        }
        if (item.EstimateEdit === true) {
            item.roleAccess.push({ accessRight: 19 });
        }
        else {
            accessCount++;
        }
        if (item.Dashboard === true) {
            item.roleAccess.push({ accessRight: 25 });
        }
        else {
           
        }
        if (item.addNewProduct === true) {
            item.roleAccess.push({ "accessRight": 26 });
        } else {
            accessCount++;
        }
        //The below section added by Poongodi on 06/03/2017   
        if (item.PurchaseCancel === true) {
            item.roleAccess.push({ accessRight: 27 });
        }
        else {
            accessCount++;
        }

        if (item.SalesTempStock === true) {
            item.roleAccess.push({ accessRight: 28 });
        }
        else {
            accessCount++;
        }
        if (item.StockAdjustMent === true) {
            item.roleAccess.push({ accessRight: 29 });
        }
        else {
            accessCount++;
        }
        if (item.VendorProfile === true) {
            item.roleAccess.push({ accessRight: 30 });
        }
        if (item.Settings === true) {
            item.roleAccess.push({ accessRight: 31 });
        }
        else {
            accessCount++;
        }

        if (accessCount == 23 && item.status == 1) {
            toastr.error('Select at least one Access Right');
            $.LoadingOverlay("hide");
        }
        else {
            userManagementService.update(item).then(function (response) {
                toastr.success('Updated user successfully');
                $.LoadingOverlay("hide");
                //window.location.assign('/userManagement/index');
            }, function () {
                $.LoadingOverlay("hide");
            });
        }

    };



    // Sales User Added by San - 01-11-2017

    $scope.showSalesUserPopup = function () {
        $scope.focusInput = true;
        $scope.user.name = "";
        $scope.user.password = "";
        var m = ModalService.showModal({
            "controller": "salesUserCtrl",
            "templateUrl": "salesUser",
        }).then(function (modal) {
            modal.element.modal();
            var ele = document.getElementById("usersName");
            ele.focus();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
                $scope.enablePopup = false; 
               
            });
        });
        return false;
    };


});
