app.controller('salesUserCtrl', function ($scope, hQueUserModel, userManagementService, close, toastr) {

    var user = hQueUserModel;    

    $scope.user = user;
    
    $scope.focusInput = true;
    $scope.minDate = new Date();
    $scope.editMode = false;

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };

    $scope.salesUserList = [];
    $scope.close = function (result) {
        close(result, 100); // close, but give 100ms for bootstrap to animate
        $(".modal-backdrop").hide();
    };

    $scope.addSalesPerson = function (mode) {
    
        if (!angular.isUndefinedOrNull($scope.user.name) && !angular.isUndefinedOrNull($scope.user.password)) {

            if (mode == 'Add')
                $scope.user.isNew = true;
            else
                $scope.user.isNew = false;


            $.LoadingOverlay("show");
            if ($scope.user.isNew == true)
            {
                $scope.user.status = 0;
            }
            else {
                $scope.user.status = $scope.user.status;
            }
            userManagementService.addSalesPerson($scope.user).then(function (response) {
                toastr.success('User created successfully');
                var name = document.getElementById('usersName');
                if (btnAddSalesUser) {
                    name.focus();
                };
                $scope.SalesPersonList();
                $scope.salesUser.$setPristine();
                $scope.user = {};
                $scope.editMode = false;
                $.LoadingOverlay("hide");
            }, function (response) {
                $.LoadingOverlay("hide");
                toastr.info('Please enter a different password');
            });
        }
    };

    $scope.resetSalesUser = function () {       
         $scope.salesUser.$setPristine();
         $scope.user = {};
         $scope.editMode = false;
         var name = document.getElementById('usersName');
         name.focus();
    };
    $scope.updateUserValuesStatus = function (item) {
        $.LoadingOverlay("show");
        userManagementService.updateUserValuesStatus(item).then(function (response) {
            $scope.salesUserUpdateList = response.data;
            toastr.success('User status  updated Successfully');
            $scope.SalesPersonList();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.SalesPersonList = function () {

        $.LoadingOverlay("show");
        userManagementService.SalesPersonList().then(function (response) {
            $scope.salesUserList = response.data;
            $scope.options = { Active: 0, InActive: 2 };
            $.LoadingOverlay("hide");
        }, function (response) {
            $.LoadingOverlay("hide");
            toastr.error('Please enter a different password', 'error');
        });
    };

    
    $scope.editSalesPerson = function (item, ind) {
        $scope.editMode = true;

        $scope.user = JSON.parse(JSON.stringify(item));
        var name = document.getElementById("usersName");
        name.focus();
    };
    $scope.SalesPersonList();    
    $scope.keyEnter = function (nextid) {
        var ele = document.getElementById(nextid);
        ele.focus();
    };

   
    $scope.enableSalesUserSettings = function () {
        $.LoadingOverlay("show");
        userManagementService.enableSalesUserSettings($scope.isEnableSalesUser).then(function (response) {
            $.LoadingOverlay("hide");
            toastr.success("Sales User updated successfully.");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.getSaleUserSettings = function () {
        $.LoadingOverlay("show");
        userManagementService.getSaleUserSettings().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.isEnableSalesUser = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getSaleUserSettings();

    $scope.enableSalesUser = function (isEnable) {
        if (isEnable == true) {
            $scope.enableSalesUserSettings();
        }
        else
            $scope.enableSalesUserSettings();
    };

    angular.isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null || val === ""
    }
});