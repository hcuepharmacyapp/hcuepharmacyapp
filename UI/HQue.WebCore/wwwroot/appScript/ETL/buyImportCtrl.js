app.controller('buyImportCtrl', function ($scope, toastr, vendorPurchaseService, vendorPurchaseModel, alternateVendorProductModel, vendorPurchaseItemModel, productStockModel, vendorService, productService, productModel, ModalService, templatePurchaseChildMasterSettingsModel, $filter) {
    //Check offline status
    var btnUpLoad = document.getElementById("btnUpLoad");
    btnUpLoad.disabled = true;
    $scope.isvalidtemplate = false;
    $scope.IsOffline = false;
    $scope.isOnlineEnabled = false;
    $scope.vendorStatus = 1;
    getOfflineStatus = function () {
        vendorPurchaseService.getOfflineStatus()
            .then(function (response) {
                if (response.data) {
                    $scope.IsOffline = true;
                }
            });
    };
    //
    getOfflineStatus();

    //Added by Sarubala on 18-05-18
    getOnlineEnabledStatus = function () {
        vendorPurchaseService.getOnlineEnableStatus().then(function (response) {
            if (response.data) {
                $scope.isOnlineEnabled = true;
            }
        }, function (error) {
            console.log(error);
        });
    };

    getOnlineEnabledStatus();

    $scope.templatePurchaseChildMasterSettings = templatePurchaseChildMasterSettingsModel;
    $scope.minDate = new Date();
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        "opened": false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.open3 = function (purchaseItem) {
        purchaseItem.opened = true;
        // $scope.popup3.opened = true;
    };
    $scope.popup3 = {
        "opened": false
    };
    $scope.open4 = function () {
        $scope.popup4.opened = true;
    };
    $scope.popup4 = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };
    $scope.completePurchaseKeyType = null;
    angular.element(document).keydown(function (e) {
        switch (e.keyCode) {
            case 67:
                if (e.ctrlKey) {
                    event.preventDefault();
                    $scope.cancel(); //Ctrl + C (cancel)
                }
                break;
            case 82:
                if (e.ctrlKey) {
                    event.preventDefault();
                    document.getElementById("rdCredit").click();  //Ctrl + R //.change()                 
                    $scope.$apply();
                    angular.element(document.getElementById("creditNoOfDays")).focus();
                }
                break;
            case 27:
            case 35:
                if (angular.element(document.getElementById("subAddStock"))[0].disabled == false
                    ||
                    angular.element(document.getElementById("subAddStock"))[0].disabled == undefined) {
                    event.preventDefault();
                    angular.element(document.getElementById("subAddStock")).click();
                }
                break;
        }
    });
    var barcode = document.getElementById("barCode");
    barcode.disabled = true; $scope.isFormValid = true;
    $scope.validateInvoiceDate = function () {
        var val = (toDate($scope.buyForm.invoiceDate.$viewValue) < $scope.minDate);
        $scope.buyForm.invoiceDate.$setValidity("InValidInvoiceDate", val);
        $scope.isFormValid = val;
    };
    $scope.validateFreeQty = function () {
        $scope.vendorPurchaseItems.freeQty.$setValidity("InValidfreeQty", !(parseFloat($scope.vendorPurchaseItem.packageQty || 0) == 0 && (parseFloat($scope.vendorPurchaseItem.freeQty) || 0) == 0));
        if ($scope.vendorPurchaseItem.packageQty != null && $scope.vendorPurchaseItem.packageQty != undefined) {
            if (!$scope.isAllowDecimal) {
                if (parseFloat($scope.vendorPurchaseItem.packageQty) % 1 != 0) {
                    $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidRoundQty", false);
                } else {
                    $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidRoundQty", true);
                }
            }
        }
        if ($scope.vendorPurchaseItem.freeQty != null && $scope.vendorPurchaseItem.freeQty != undefined && $scope.vendorPurchaseItem.freeQty != "") {
            if (!$scope.isAllowDecimal) {
                if (parseFloat($scope.vendorPurchaseItem.freeQty) % 1 != 0) {
                    $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", false);
                } else {
                    $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", true);
                }
            }
        } else {
            $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", true);
        }
        if ($scope.isAllowDecimal) {
            var qty = ($scope.vendorPurchaseItem.packageQty != null && $scope.vendorPurchaseItem.packageQty != undefined && $scope.vendorPurchaseItem.packageQty != "") ? parseFloat($scope.vendorPurchaseItem.packageQty) : 0;
            var freeqty = ($scope.vendorPurchaseItem.freeQty != null && $scope.vendorPurchaseItem.freeQty != undefined && $scope.vendorPurchaseItem.freeQty != "") ? parseFloat($scope.vendorPurchaseItem.freeQty) : 0;
            var tot = (qty + freeqty) % 1;
            if (tot !== 0) {
                $scope.vendorPurchaseItems.freeQty.$setValidity("InvalidSumQty", false);
            } else {
                $scope.vendorPurchaseItems.freeQty.$setValidity("InvalidSumQty", true);
            }
        }
    };
    function getIsAllowDecimalSettings() {
        vendorPurchaseService.getDecimalSetting()
            .then(function (response) {
                $scope.isAllowDecimal = response.data;
            }, function (error) {
                console.log(error);
            })
    }
    $scope.templateType = "Default";
    function toDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    };
    $scope.updateUI = function () {
        //var cst = document.getElementById("cst");
        //var vat = document.getElementById("vat");
        //$scope.vendorPurchaseItem.productStock.vat = 0;
        //$scope.vendorPurchaseItem.productStock.cst = 0;
        //if ($scope.vendorPurchase.selectedVendor.enableCST) {
        //    //vat.disabled = false;
        //   // cst.disabled = false;
        //    $scope.vendorPurchase.TaxationType = "CST";
        //    $scope.vendorPurchaseItem.productStock.cst = 2;
        //} else if ($scope.TaxationType == "CST") {
        //   // vat.disabled = false;
        //   // cst.disabled = false;
        //    $scope.vendorPurchase.TaxationType = "CST";
        //   // $scope.vendorPurchaseItem.productStock.cst = 2;
        //} else {
        //  // vat.disabled = false;
        //   // cst.disabled = true;
        //    $scope.vendorPurchase.TaxationType = "VAT";
        //   // $scope.vendorPurchaseItem.productStock.vat = 5;
        //}
        if ($scope.vendorPurchase.selectedVendor.name != undefined && $scope.vendorPurchase.selectedVendor.name != null) {
            if ($scope.vendorPurchase.selectedVendor.paymentType != undefined && $scope.vendorPurchase.selectedVendor.paymentType != null) {
                $scope.vendorPurchase.paymentType = $scope.vendorPurchase.selectedVendor.paymentType;
                if ($scope.vendorPurchase.paymentType == "Cash") {
                    $scope.editCheque($scope.vendorPurchase, false, false);
                } else if ($scope.vendorPurchase.paymentType == "Credit") {
                    $scope.editCheque($scope.vendorPurchase, false, true);
                    if ($scope.vendorPurchase.selectedVendor.creditNoOfDays != undefined) {
                        $scope.vendorPurchase.creditNoOfDays = $scope.vendorPurchase.selectedVendor.creditNoOfDays;
                    }
                } else {
                    $scope.editCheque($scope.vendorPurchase, true, false);
                }
            } else {
                $scope.vendorPurchase.paymentType = "Credit";
                $scope.editCheque($scope.vendorPurchase, false, true);
            }
        }
        if ($scope.vendorPurchase.vendor != null && $scope.vendorPurchase.vendor == undefined) {
            $scope.vendorPurchase.vendor.locationType = $scope.vendorPurchase.selectedVendor.locationType; // Added by Sarubala on 12/09/17
        }

        $scope.onVendorSelect();
        $scope.isvalidtemplate = false;
    };
    $scope.EditMode = false;
    $scope.AddNewMode = false;
    $scope.Action = "Add";
    $scope.ResetAction = "Reset";
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    var vendorPurchase = vendorPurchaseModel;
    var vendorPurchaseItem = vendorPurchaseItemModel;
    vendorPurchaseItem.productStock = productStockModel;
    vendorPurchaseItem.productStock.product = productModel;
    $scope.product = productModel;
    $scope.vendorPurchase = vendorPurchase;
    $scope.vendorPurchaseItem = vendorPurchaseItem;
    //$scope.vendorPurchaseItem.productStock.vat = 5;
    $scope.vendorPurchaseItem.tax = 0;
    $scope.vendorPurchaseItem.total = 0;
    $scope.vendorPurchaseItem.freeQty = 0;
    $scope.vendorPurchaseItem.discount = 0;
    $scope.vendorPurchaseItem.orgDiscount = 0;
    $scope.vendorPurchaseItem.discountVal = 0;
    $scope.vendorPurchaseItem.orderedQty = 0;
    $scope.vendorPurchase.vendorPurchaseItem = [];
    $scope.vendorPurchase.selectedVendor = {};
    $scope.vendorPurchase.selectedVendor = null;
    $scope.selectedProduct = null;
    $scope.Math = window.Math;
    $scope.vendorPurchase.TaxationType = "VAT";
    $scope.vendorPurchase.invoiceDate = new Date();
    $scope.vendorPurchase.total = 0;
    $scope.vendorPurchase.discount = 0;
    $scope.vendorPurchase.credit = 0;
    $scope.list = [];
    $scope.editItems = 0;
    $scope.copyedIndex = {};
    $scope.calDiscountOnBill = function (vendorPurchaseDiscount) {

        vendorPurchaseDiscount = parseFloat(vendorPurchaseDiscount) || 0;
        if (vendorPurchaseDiscount != 0) {

            if (vendorPurchaseDiscount > 99.99) {
                $scope.discountError = true;
            } else {
                $scope.discountError = false;
            }
        }

        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number(vendorPurchaseDiscount);
            if ($scope.vendorPurchase.vendorPurchaseItem[i].discount > 99.99) {
                $scope.discountError = true;
            } else {
                $scope.discountError = false;
            }
            var qty = 0;

            // Commented by Gavaskar 28-08-2017 Qty, Free Qty Gst Calc issue Start
            //if (parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) < parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].freeQty)) {

            qty = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packageQty);
            //} else {
            //    qty = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) - parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].freeQty);
            //}
            // Commented by Gavaskar 28-08-2017 Qty, Free Qty Gst Calc issue End

            $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);

            $scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;

            $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);

            //if ($scope.vendorPurchaseItem.productStock.vat > 0) {

            //    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat / 100);
            //}
            //else if ($scope.vendorPurchaseItem.productStock.cst > 0)
            // {

            //    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst / 100);
            //    }
            //    else{
            //    $scope.vendorPurchase.tax = $scope.vendorPurchase.vendorPurchaseItem[i].tax;
            //        }
        }

        setTotal();
        setGst();
        // Added by Gavaskar 28-08-2017 highlight issue 
        $scope.initialValue();
    }
    function setTotal1() {
        $scope.vendorPurchase.total = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if ($scope.vendorPurchase.vendorPurchaseItem[i].discount == null) $scope.vendorPurchase.vendorPurchaseItem[i].discount = 0;
            $scope.vendorPurchase.total += ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) - $scope.vendorPurchase.vendorPurchaseItem[i].discount;
        }
        setGst();
    }
    function setTotal() {
        $scope.vendorPurchase.total = 0;
        $scope.vendorPurchase.discountOnBill = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            $scope.vendorPurchase.total += $scope.vendorPurchase.vendorPurchaseItem[i].total;
            $scope.vendorPurchase.discountOnBill += $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;

        }
        //setTotaldiscount();
    }
    //function setTotaldiscount() {
    //    $scope.vendorPurchase.discountOnBill = 0;
    //    for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

    //        //var qty = 0;

    //        //if ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty < $scope.vendorPurchase.vendorPurchaseItem[i].freeQty) {

    //        //    qty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty;

    //        //} else {

    //        //    qty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty;
    //        //}

    //          // var total = $scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty;
    //           var total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty));
    //        $scope.vendorPurchase.discountOnBill += total - (total * ($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount / 100));
    //    }
    //    $scope.vendorPurchase.discountOnBill = $scope.vendorPurchase.discountOnBill * ($scope.vendorPurchase.discount / 100);
    //}
    //function setVat1() {
    //    $scope.vendorPurchase.vat = 0;
    //    for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
    //        var total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty + $scope.vendorPurchase.vendorPurchaseItem[i].freeQty));
    //        //$scope.vendorPurchase.vat += total - ((total * 100) / (Number($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat) + 100));
    //        $scope.vendorPurchase.vat += total * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat / 100);
    //    }
    //}
    function setGst() {

        // Commented by Gavaskar 28-08-2017 Gst Calc issue Start
        //$scope.vendorPurchase.tax = 0;
        //for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
        //    $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;
        //}
        // Commented by Gavaskar 28-08-2017 Gst Calc issue End

        // Added by Gavaskar 28-08-2017 Gst Calc issue Start
        $scope.vendorPurchase.tax = 0;
        $scope.vendorPurchase.igst = 0;
        $scope.vendorPurchase.cgst = 0;
        $scope.vendorPurchase.sgst = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;

            $scope.vendorPurchase.igst += $scope.vendorPurchase.vendorPurchaseItem[i].igst;
            $scope.vendorPurchase.cgst += $scope.vendorPurchase.vendorPurchaseItem[i].cgst;
            $scope.vendorPurchase.sgst += $scope.vendorPurchase.vendorPurchaseItem[i].sgst;
        }
        var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax;


        if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == null) {
            $scope.vendorPurchaseItem.productStock.igst = 0;
            $scope.vendorPurchase.cgst = $scope.vendorPurchase.tax / 2;
            $scope.vendorPurchase.sgst = $scope.vendorPurchase.tax / 2;
        }
        else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
            $scope.vendorPurchaseItem.productStock.cgst = 0;
            $scope.vendorPurchaseItem.productStock.sgst = 0;
            $scope.vendorPurchase.igst = $scope.vendorPurchase.tax;
        }
        else {
            $scope.vendorPurchaseItem.productStock.gstTotal = 0;
            $scope.vendorPurchaseItem.productStock.igst = 0;
            $scope.vendorPurchaseItem.productStock.cgst = 0;
            $scope.vendorPurchaseItem.productStock.sgst = 0;
        }
        // Added by Gavaskar 28-08-2017 Gst Calc issue End

    }
    //File Upload
    $scope.SelectedFileForUpload = null;
    // Added by Annadurai for file exists or not check
    $scope.selectFileforUpload = function (element) {
        var btnUploadOpt = document.getElementById("btnUpLoad");
        $scope.SelectedFileForUpload = element.files[0];
        if (element != null && element != undefined && element.files != undefined) {
            if (element.files[0] && element.files[0].name != '') {
                btnUploadOpt.disabled = false;
            }
            else {
                btnUploadOpt.disabled = true;
            }

        }
    }

    $scope.uploadFile = function (sales) {
        $.LoadingOverlay("show");
        var defaultType = null;
        var customType = null;
        if ($scope.templateType == "Default") {
            defaultType = "Default"
        }
        else if ($scope.templateType == "Custom") {
            customType = "Custom"
        }
        var selectedVendor = null;
        if (vendorPurchase.selectedVendor != null)
            selectedVendor = vendorPurchase.selectedVendor.id;
        vendorPurchaseService.uploadFile($scope.SelectedFileForUpload, selectedVendor, defaultType, customType).then(function (response) {
            var vp = response.data;
            $scope.cc = vp.vendorPurchaseItem;
            var alternateVendorProduct = [];
            for (var i = 0; i < $scope.cc.length; i++) {
                if (($scope.cc[i].packageSize == null && $scope.cc[i].alternatePackageSize != null && $scope.cc[i].productStock.packageSize == undefined) || ($scope.cc[i].productStock.productId == undefined)) {
                    var alternatepackSize = $scope.cc[i].alternatePackageSize == "" ? "-" : $scope.cc[i].alternatePackageSize;
                    var obj = {
                        updatedPackageSize: !isNaN(parseFloat($scope.cc[i].alternatePackageSize)) && isFinite($scope.cc[i].alternatePackageSize) || parseFloat($scope.cc[i].packageSize),
                        productId: $scope.cc[i].productStock.productId,
                        vendorId: vp.vendorId,
                        instanceId: vp.instanceId,
                        alternateProductName: $scope.cc[i].productStock.name,
                        alternatePackageSize: alternatepackSize || $scope.cc[i].packageSize,
                        accountId: vp.accountId,
                        itemIndex: i,
                    };
                    alternateVendorProduct.push(obj)
                }
            }
            if (alternateVendorProduct.length > 0) {
                //window.localStorage.setItem("alternatePackageSizeFromExcel", JSON.stringify(check));
                //window.location = "/VendorPurchase/AlternatepackagesizePopup";
                //return;
                $scope.enablePopup = true;
                var m = ModalService.showModal({
                    "controller": "alternatePackageSizeCtrl",
                    "templateUrl": 'alternatePackageSize',
                    "inputs": {
                        "alternateVendorProductModel": alternateVendorProduct
                    }
                }).then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                        if (result != 'Cancel' && result.length > 0) {
                            for (var i = 0; i < result.length; i++) {
                                vp.vendorPurchaseItem[result[i].itemIndex].packageSize = result[i].updatedPackageSize;
                                vp.vendorPurchaseItem[result[i].itemIndex].productStock.productId = result[i].productId;
                                vp.vendorPurchaseItem[result[i].itemIndex].productStock.packageSize = result[i].updatedPackageSize;
                                vp.vendorPurchaseItem[result[i].itemIndex].productStock.product.name = result[i].productName;
                            }

                            if (vp.importError == null || vp.importError == "") {
                                $.LoadingOverlay("hide");
                                //toastr.success('Uploaded values getting stored');
                                $scope.cc = vp.vendorPurchaseItem;
                                for (var i = 0; i < $scope.cc.length; i++) {
                                    if ($scope.cc[i].packageSize == undefined) {
                                        $scope.cc[i].packageSize = $scope.cc[i].productStock.packageSize
                                    }
                                }
                                window.localStorage.setItem("purchaseImportFromExcel", JSON.stringify(vp));
                                toastr.success('Upload completed successfully');

                                window.location = "/VendorPurchase/Index";
                            }
                            else {
                                $.LoadingOverlay("hide");
                                toastr.error('Upload failed with error: ' + vp.importError);
                            }
                        }
                        $scope.enablePopup = false;
                    })
                });
                return false;
            }

            if (vp.importError == null || vp.importError == "") {
                $.LoadingOverlay("hide");
                //toastr.success('Uploaded values getting stored');
                $scope.cc = vp.vendorPurchaseItem;
                try
                {
                    for (var i = 0; i < $scope.cc.length; i++) {
                        if ($scope.cc[i].packageSize == undefined) {
                            $scope.cc[i].packageSize = $scope.cc[i].productStock.packageSize
                        }
                    }
                
                    window.localStorage.setItem("purchaseImportFromExcel", JSON.stringify(vp));
                    toastr.success('Upload completed successfully');

                    window.location = "/VendorPurchase/Index";
                } catch (e) {
                    toastr.error("Local Storage is full, Please Clear the Browsing data");
                }
            }
             
            else {
                $.LoadingOverlay("hide");
                toastr.error('Upload failed with error: ' + vp.importError);
            }
            return;

        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Upload failed. Please try again.');
            //console.log("fail");
        });
    };

    $scope.showPanel = false;
    $scope.init = function (model) {
        if (model == null)
            return;
        else {
            window.localStorage.setItem("purchaseImportFromExcel", JSON.stringify(model));
            window.location = "/VendorPurchase/Index";
            return;
        }
        $scope.showPanel = true;
        getGoodRNO();
        $scope.initialValue();
        getIsAllowDecimalSettings();
        $.LoadingOverlay("show");
        $scope.vendorPurchase = model;
        $scope.vendorPurchase.selectedVendor = {};
        //  $scope.vendorPurchase.selectedVendor.id = $scope.vendorPurchase.vendorId;
        //$scope.vendorPurchase.invoiceDate = new Date();
        //Added by Sarubala on 26-09-17
        if (model.vendor != null && model.vendor != undefined && model.vendor.id != null && model.vendor.id != undefined) {
            $scope.vendorPurchase.selectedVendor = model.vendor;
        }

        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            $scope.vendorPurchase.vendorPurchaseItem[i].isEdit = false;
            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct = {};

            $scope.vendorPurchase.vendorPurchaseItem[i].productName = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name; //Added by Sarubala on 11-09-17

            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.name = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name;

            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.id;

            $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) + parseFloat
                ($scope.vendorPurchase.vendorPurchaseItem[i].freeQty);


            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.rackNo = $scope.vendorPurchase.vendorPurchaseItem[i].rackNo;
            $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount);
            $scope.vendorPurchase.vendorPurchaseItem[i].discount = Math.abs(Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number($scope.vendorPurchase.discount));
            var qty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty;
            $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);
            //$scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty);
            $scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;
            //if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst > 0) {
            //    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst / 100);
            //    //$scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst / 100);
            //    $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst

            //} else {
            $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);

            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;


            // $scope.vendorPurchase.vendorPurchaseItem[i].packageQty += $scope.vendorPurchase.vendorPurchaseItem[i].freeQty;
        }
        if (model.vendor.paymentType != undefined && model.vendor.paymentType != null) {
            $scope.vendorPurchase.paymentType = model.vendor.paymentType;
            if ($scope.vendorPurchase.paymentType == "Cash") {
                $scope.editCheque($scope.vendorPurchase, false, false);
            } else if ($scope.vendorPurchase.paymentType == "Credit") {
                $scope.editCheque($scope.vendorPurchase, false, true);
                if (model.vendor.creditNoOfDays != undefined) {
                    $scope.vendorPurchase.creditNoOfDays = model.vendor.creditNoOfDays;
                }
            } else {
                $scope.editCheque($scope.vendorPurchase, true, false);
            }
        } else {
            $scope.vendorPurchase.paymentType = "Credit";
            $scope.editCheque($scope.vendorPurchase, false, true);
        }
        $scope.updateUI();
        setTotal();
        setGst();
        // Added Gavaskar 
        $scope.vendorPurchase.initialValue = $scope.vendorPurchase.credit;
        $scope.initialValue();
        getIsAllowDecimalSettings();
        $.LoadingOverlay("hide");
    };
    $scope.alternateVendorProduct = alternateVendorProductModel;
    $scope.saveAlternate = function (val) {
        return productService.saveAlternateName($scope.alternateVendorProduct)
            .then(function (response) {
                $scope.alternateVendorProduct = response.data;
            });
    };
    $scope.alterName = null;
    // expiry date focus by violet raj
    $scope.checkAllPurchasefields = function (event, e, ind) {
        if (ind == 6) {
            if ($scope.vendorPurchaseItem.productStock.expireDate == undefined || $scope.vendorPurchaseItem.productStock.expireDate == null || $scope.vendorPurchaseItems.expDate.$error.InValidexpDate == true) {
                var ele = document.getElementById("expDate");
                ele.focus();
                return false;
            }
            else {
                var ele = document.getElementById(e);
                if (event.which === 13) // Enter key
                {
                    if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
                    {
                        ele.focus();
                        if (ele.nodeName != "BUTTON")
                            ele.select();
                    }
                }
            }
        }
    }


    $scope.discountError = false;


    $scope.addStock = function () {
        if ($scope.selectedProduct != null) {
            if (typeof $scope.selectedProduct === "string") {
                if (!confirm("This is a new product, do you want to add it ?")) {
                    return;
                }
            }
            if ($scope.AddNewMode) {
                $scope.vendorPurchaseItem.productStock.product.name = $scope.selectedProduct.name;
                $scope.vendorPurchaseItem.productName = $scope.selectedProduct.name; //Added by Sarubala on 11/09/17
                $scope.vendorPurchaseItem.productStock.productId = $scope.selectedProduct.id;
                $scope.vendorPurchaseItem.action = "I";
                if ($scope.selectedProduct.accountId === null || $scope.selectedProduct.accountId === undefined || $scope.selectedProduct.accountId === "") {
                    $scope.vendorPurchaseItem.productStock.product.productMasterID = $scope.selectedProduct.id;
                } else {
                    $scope.vendorPurchaseItem.productStock.product.productMasterID = null;
                }
            }
            if ($scope.EditMode) {
                if (($scope.alterName != null) && ($scope.alterName != $scope.selectedProduct.name)) {
                    $scope.alternateVendorProduct.vendorId = $scope.vendorPurchase.selectedVendor.id;
                    $scope.alternateVendorProduct.productId = $scope.selectedProduct.id;
                    $scope.alternateVendorProduct.alternateProductName = $scope.alterName;
                    $scope.vendorPurchaseItem.productName = $scope.selectedProduct.name; //Added by Sarubala on 11/09/17
                    $scope.saveAlternate($scope.alternateVendorProduct);
                    if ($scope.alternateVendorProduct.productId != "") {
                        $scope.selectedProduct.id = $scope.alternateVendorProduct.productId;
                        //$scope.selectedProduct.totalstock = 0;
                        $scope.selectedProduct.accountId = "1"; // Skip add product 
                    }
                }
                if ($scope.alterName == $scope.selectedProduct.name) {
                    for (var k = 0 ; k < $scope.vendorPurchase.vendorPurchaseItem.length; k++) {
                        if ($scope.selectedProduct.name == $scope.vendorPurchase.vendorPurchaseItem[k].productStock.product.name) {
                            $scope.selectedProduct = $scope.vendorPurchase.vendorPurchaseItem[k].productStock.product;
                            $scope.selectedProduct.id = $scope.vendorPurchase.vendorPurchaseItem[k].productStock.productId;
                        }
                    }
                }
                $scope.vendorPurchaseItem.action = "U";
                //Rackno & Boxno added to update from list grid edit option
                $scope.selectedProduct.rackNo = $scope.vendorPurchaseItem.rackNo;
                $scope.selectedProduct.boxNo = $scope.vendorPurchaseItem.productStock.product.boxNo;
                $scope.vendorPurchaseItem.productStock.product = $scope.selectedProduct;
                $scope.vendorPurchaseItem.productStock.productId = $scope.selectedProduct.id;
                if ($scope.selectedProduct.accountId === null || $scope.selectedProduct.accountId === undefined || $scope.selectedProduct.accountId === "") {
                    $scope.vendorPurchaseItem.productStock.product.productMasterID = $scope.selectedProduct.id;
                } else {
                    $scope.vendorPurchaseItem.productStock.product.productMasterID = null;
                }
            }
            $scope.vendorPurchaseItem.isEdit = false;
            $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;
            $scope.vendorPurchaseItem.productStockId = $scope.selectedProduct.id;

            $scope.vendorPurchaseItem.packageQty = parseFloat($scope.vendorPurchaseItem.packageQty) || 0;

            $scope.vendorPurchaseItem.freeQty = parseFloat($scope.vendorPurchaseItem.freeQty) || 0;


            $scope.vendorPurchaseItem.orderedQty = $scope.vendorPurchaseItem.packageQty + $scope.vendorPurchaseItem.freeQty;


            $scope.vendorPurchaseItem.discount = $scope.vendorPurchaseItem.discount || 0;

            var invoicediscount = $scope.vendorPurchase.discount == "" ? 0 : parseFloat($scope.vendorPurchase.discount);
            if ((parseFloat($scope.vendorPurchaseItem.discount) + invoicediscount) > 99.99) {
                $scope.discountError = true;
            } else {
                $scope.discountError = false;
            }



            $scope.vendorPurchaseItem.discountVal = 0;
            $scope.vendorPurchaseItem.orgDiscount = $scope.vendorPurchaseItem.discount;
            $scope.vendorPurchaseItem.discount = Number($scope.vendorPurchaseItem.discount) + Number($scope.vendorPurchase.discount);
            if ($scope.vendorPurchaseItem.discount > 0)
                $scope.vendorPurchaseItem.discountVal = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) * ($scope.vendorPurchaseItem.discount / 100);
            $scope.vendorPurchaseItem.total = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) - $scope.vendorPurchaseItem.discountVal;
            //if ($scope.vendorPurchase.TaxationType == "VAT")
            $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.gstTotal / 100);
            // $scope.vendorPurchaseItem.tax = (($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) - $scope.vendorPurchaseItem.discount) * ($scope.vendorPurchaseItem.productStock.cST / 100);
            //if ($scope.vendorPurchase.TaxationType == "CST")
            //    $scope.vendorPurchaseItem.tax = (($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty)) * ($scope.vendorPurchaseItem.productStock.gstTotal / 100);
            // $scope.vendorPurchaseItem.orderedQty = $scope.vendorPurchaseItem.packageQty || 0;

            //if ($scope.vendorPurchaseItem.freeQty > 0) {
            //    $scope.vendorPurchaseItem.packageQty = Number($scope.vendorPurchaseItem.packageQty) + Number($scope.vendorPurchaseItem.freeQty);
            //}

            //var _isCST = false;
            //if ($scope.vendorPurchase.TaxationType == "VAT") {
            //    $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.vat;
            //    $scope.vendorPurchaseItem.productStock.cst = 0;
            //    var cst = document.getElementById("cst");
            //   // cst.disabled = true;
            //}
            //var vat
            //if ($scope.vendorPurchase.TaxationType == "CST") {
            //    $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.cst;
            //    $scope.vendorPurchase.TaxationType = "CST";
            //    _isCST = true;
            //}
            $scope.vendorPurchaseItem.selectedProduct = $scope.selectedProduct;
            $scope.vendorPurchaseItem.productName = $scope.selectedProduct.name; //Added by Sarubala on 12/09/17
            if (!$scope.EditMode) {
                $scope.vendorPurchaseItem.productStock.productId = $scope.vendorPurchaseItem.selectedProduct.id;
                $scope.vendorPurchaseItem.productStock.product.name = $scope.vendorPurchaseItem.selectedProduct.name;
                $scope.vendorPurchase.vendorPurchaseItem.push($scope.vendorPurchaseItem);
            } else {
                $scope.vendorPurchase.vendorPurchaseItem[$scope.copyedIndex] = $scope.vendorPurchaseItem;
                $scope.EditMode = false;
            }
            $scope.vendorPurchaseItems.$setPristine();
            $scope.vendorPurchaseItem = { productStock: { product: {} } };
            $scope.selectedProduct = null;
            setTotal();
            setGst();
            //(_isCST) ? $scope.vendorPurchaseItem.productStock.cst = 2 : $scope.vendorPurchaseItem.productStock.vat = 5;
            $scope.initialValue();
            getIsAllowDecimalSettings();
            var qty = document.getElementById("drugName");
            qty.focus();
        }
        $scope.Editindex = "";
    }
    $scope.removeStock = function (item) {
        $scope.vendorPurchase.total -= (item.productStock.sellingPrice * item.quantity);
        var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
        $scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
        setTotal();
        setGst();
    }
    $scope.cancelUpdate = function () {
        $scope.EditMode = false;
        $scope.vendorPurchaseItems.$setPristine();
        $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", true);
        $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidRoundQty", true);
        $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", true);
        $scope.vendorPurchaseItems.freeQty.$setValidity("InvalidSumQty", true);
        $scope.vendorPurchaseItem = { productStock: { product: {} } };
        $scope.selectedProduct = null;

        var invoicediscont = parseFloat($scope.vendorPurchase.discount) || 0;

        if ($scope.Editindex != "") {
            if ($scope.vendorPurchase.discount != "" || $scope.vendorPurchase.discount != null) {
                $scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discount = parseFloat($scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discount) + invoicediscont;
            }
            $scope.Editindex = "";
        }
    }
    $scope.Editindex = "";

    $scope.editPurchase = function (item, index) {

        $scope.Editindex = index;


        if ($scope.EditMode) {
            return;
        }
        item.discount = $filter("number")(item.orgDiscount, 2);

        //item.packageQty = parseFloat((item.packageQty - item.freeQty).toFixed(2));



        item.packageQty = parseFloat((item.orderedQty - item.freeQty).toFixed(2));




        $scope.copyedIndex = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);

        $scope.vendorPurchaseItem = JSON.parse(JSON.stringify(item));

        $scope.selectedProduct = item.selectedProduct;

        $scope.alterName = item.selectedProduct.name;
        $scope.EditMode = true;
        ////$scope.dayDiff($scope.vendorPurchaseItem.expireDate);
        ////if ($scope.vendorPurchaseItem.packageQty == null || $scope.vendorPurchaseItem.packageQty == 0) {
        ////    freeqty.disabled = true;
        ////} else {
        ////    freeqty.disabled = false;
        ////}
        //var cst = document.getElementById("cst");
        //if ($scope.vendorPurchase.selectedVendor.enableCST) {
        //    cst.disabled = false;
        //}
    }
    $scope.doneEditPurchase = function (item, isValid) {
        if (!isValid) {
            item.isEdit = false;
            $scope.editItems = $scope.editItems - 1;
        }
        setTotal();
        setGst();
    }
    //$scope.save = function () {
    //    $.LoadingOverlay("show");
    //    $scope.isProcessing = true;
    //    $scope.showPanel = false;
    //    $scope.netHighlight = "";
    //    var checkNetTotal = 0;
    //    $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;
    //    if ($scope.vendorPurchase.initialValue) {
    //        checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
    //        if (checkNetTotal <= 1 && checkNetTotal >= -1) {
    //            $scope.netHighlight = "";
    //            if ($scope.vendorPurchase.selectedVendor.id != undefined)
    //                purchaseCreate();
    //            else {
    //                toastr.error('Select the Vendor Name');
    //                $.LoadingOverlay("hide");
    //                $scope.netHighlight = "highlight";
    //                $scope.isProcessing = false;
    //                $scope.showPanel = true;
    //            }
    //        } else {
    //            var cancel = window.confirm('Are you sure, Do you want to Update Stock?');
    //            if (cancel) {
    //                if ($scope.vendorPurchase.selectedVendor.id != undefined)
    //                    purchaseCreate();
    //                else {
    //                    toastr.error('Select the Vendor Name');
    //                    $.LoadingOverlay("hide");
    //                    $scope.netHighlight = "highlight";
    //                    $scope.isProcessing = false;
    //                    $scope.showPanel = true;
    //                }
    //            } else {
    //                $.LoadingOverlay("hide");
    //                $scope.netHighlight = "highlight";
    //                $scope.isProcessing = false;
    //            }
    //        }
    //    } else {
    //        if ($scope.vendorPurchase.selectedVendor.id != undefined)
    //            purchaseCreate();
    //        else {
    //            toastr.error('Select the Vendor Name');
    //            $.LoadingOverlay("hide");
    //            $scope.netHighlight = "highlight";
    //            $scope.isProcessing = false;
    //            $scope.showPanel = true;
    //        }
    //    }
    //}
    $scope.save = function () {

        if ($scope.vendorPurchase.invoiceNo == null) {
            toastr.error('Fill the Invoice Number');
        }
        var str = "";
        var count = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

            $scope.vendorPurchase.vendorPurchaseItem[i].packageQty = $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty;

            if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId == null || $scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId == undefined) {
                str += "<li>" + $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name + "</li>";
                count++;
            }
        }
        if (count > 0) {
            var htmlContent = "<ul>" + str + "</ul>";
            $.confirm({
                title: "Below are new products, do you want to add automatically?",
                content: htmlContent,
                closeIcon: function () {
                },
                buttons: {
                    yes: {
                        text: 'yes [y]',
                        btnClass: 'primary-button',
                        keys: ['y'],
                        action: function () {
                            prePurchaseCreate();
                        }
                    },
                    no: {
                        text: 'no [n]',
                        btnClass: 'secondary-button',
                        keys: ['n'],
                        action: function () {
                        }
                    }
                }
            });
        } else {
            prePurchaseCreate();
        }
    };
    function prePurchaseCreate() {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        $scope.showPanel = false;
        $scope.netHighlight = "";
        var checkNetTotal = 0;
        $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;
        if ($scope.vendorPurchase.initialValue) {
            checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
            if (checkNetTotal <= 1 && checkNetTotal >= -1) {
                $scope.netHighlight = "";
                if ($scope.vendorPurchase.selectedVendor.id != undefined) {
                    purchaseCreate();
                } else {
                    toastr.error('Select the Vendor Name');
                    $.LoadingOverlay("hide");
                    $scope.netHighlight = "highlight";
                    $scope.isProcessing = false;
                    $scope.showPanel = true;
                }
            } else {
                var cancel = window.confirm('Are you sure, Do you want to Update Stock?');
                if (cancel) {
                    if ($scope.vendorPurchase.selectedVendor.id != undefined) {
                        purchaseCreate();
                    } else {
                        toastr.error('Select the Vendor Name');
                        $.LoadingOverlay("hide");
                        $scope.netHighlight = "highlight";
                        $scope.isProcessing = false;
                        $scope.showPanel = true;
                    }
                } else {
                    $.LoadingOverlay("hide");
                    $scope.netHighlight = "highlight";
                    $scope.isProcessing = false;
                }
            }
        } else {
            if ($scope.vendorPurchase.selectedVendor.id != undefined) {
                purchaseCreate();
            } else {
                toastr.error('Select the Vendor Name');
                $.LoadingOverlay("hide");
                $scope.netHighlight = "highlight";
                $scope.isProcessing = false;
                $scope.showPanel = true;
            }
        }
    }
    $scope.getCompletePurchaseSettings = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getCompletePurchaseSetting().then(function (response) {
            if (response.data.toString() == "1")
                $scope.completePurchaseKeyType = "ESC";
            else if (response.data.toString() == "2")
                $scope.completePurchaseKeyType = "END";
            else
                $scope.completePurchaseKeyType = "ESC";
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        })
    }
    $scope.getCompletePurchaseSettings();
    function purchaseCreate() {
        //$.LoadingOverlay("show");
        //$scope.isProcessing = true;
        vendorPurchaseService.importBuy($scope.vendorPurchase, $scope.SelectedFileForUpload).then(function (response) {
            $scope.list = response.data;
            $scope.vendorPurchaseItems.$setPristine();
            toastr.success('Stock added successfully');
            $scope.vendorPurchase.selectedVendor = {};
            var iDate = new Date();
            $scope.vendorPurchase = { total: 0, discount: 0, vendorPurchaseItem: [], invoiceDate: iDate.toISOString() };
            //$scope.vendorPurchase = { total: 0, discount: 0, vendorPurchaseItem: [], invoiceDate: new Date() };
            $scope.isProcessing = false;
            //document.getElementById("vat").disabled = false;
            //document.getElementById("cst").disabled = false;
            $.LoadingOverlay("hide");
            window.localStorage.removeItem('p_data');
            //getGoodRNO();
        }, function (response) {
            $.LoadingOverlay("hide");
            toastr.error(response.data.errorDesc, 'Error');
            //toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    }
    $scope.initialValue = function () {
        var checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
        if (checkNetTotal <= 1 && checkNetTotal >= -1) {
            $scope.netHighlight = "";
        }
        else {
            if ($scope.vendorPurchase.total + $scope.vendorPurchase.tax > 0 && $scope.vendorPurchase.initialValue != null)
                $scope.netHighlight = "highlight";
            else
                $scope.netHighlight = "";
        }
    }
    $scope.vendor = function () {
        if ($scope.templateType == "Default") {
            vendorService.vendorDataList($scope.vendorStatus).then(function (response) {    //vendorService.vendorData().then(function (response) {
                $scope.vendorList = response.data;
            }, function () { toastr.error('Error Occured', 'Error'); });
        }
        else {
            vendorPurchaseService.vendorData().then(function (response) {
                $scope.vendorList = response.data;
                $scope.vendorList = $filter("filter")(response.data, { status: 1 });

            }, function () { toastr.error('Error Occured', 'Error'); });
        }
    }
    $scope.vendor();
    $scope.isvalidtemplate = false;
    $scope.templateTypeSelection = function () {
        $scope.vendorList = {};
        $scope.vendor();
        $scope.isvalidtemplate = false;
        if ($scope.templateType == "Custom") {
            $scope.isvalidtemplate = true;
        }
    }
    $scope.getProducts = function (val) {
        return productService.nonHiddenProductList(val).then(function (response) {
            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                return item;
            });
        });
    }
    $scope.onVendorSelect = function () {
        window.setTimeout(function () {
            var ele = document.getElementById("invoiceNo");
            ele.focus();
        }, 0);
    };
    $scope.modelOptions = {
        debounce: {
            default: 500,
            blur: 250
        },
        getterSetter: true
    };
    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            window.location.assign("/ImportBuy/Index");
        }
    }
    $scope.editCheque = function (item, bool, credit) {
        if (credit == true) {
            item.isCreditEdit = credit;
            angular.element(document.getElementById("creditNoOfDays")).focus();
            item.isChequeEdit = bool;
            $scope.vendorPurchase.chequeNo = null;
            $scope.vendorPurchase.chequeDate = null;
        }
        else {
            item.isCreditEdit = credit;
            item.isChequeEdit = bool;
            $scope.vendorPurchase.creditNoOfDays = null;
        }
    }
    $scope.currency = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.00'; return N; }
    $scope.keyEnter = function (event, e) {
        var ele = document.getElementById(e);
        if (event.which === 13) // Enter key
        {
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
            {
                ele.focus();
                if (ele.nodeName != "BUTTON")
                    ele.select();
            }
        }
        else if (event.which === 9 && (e == "invoiceDate" || e == "drugName")) {  // TAB Key
            $scope.checkInvoiceNo($scope.vendorPurchase.invoiceNo);
        }
    }
    $scope.saveDraft = function () {
        window.localStorage.setItem("p_data", JSON.stringify($scope.vendorPurchase));
        toastr.success('Daft saved successfully');
    };
    $scope.loadDraft = function () {
        $scope.vendorPurchase = JSON.parse(window.localStorage.getItem("p_data"));
        $scope.updateUI();
    };
    function getGoodRNO() {
        vendorPurchaseService.getGRNO().then(function (response) {
            $scope.vendorPurchase.goodsRcvNo = response.data + 1;
        });
    }
    $scope.PopupAddNewProduct = function () {
        alert("CreateProduct");
        var m = ModalService.showModal({
            controller: "productCreateCtrl",
            templateUrl: 'createProduct',
            inputs: {
                mode: "Create"
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
    };
    $scope.dayDiff = function (expireDate) {
        var val = (toDate($scope.vendorPurchaseItems.expDate.$viewValue) > $scope.minDate);
        $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", val);
        $scope.isFormValid = val;
        $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date($scope.formatString($scope.expireDate));
        var date1 = new Date($scope.formatString($scope.today));
        //var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference < 30) {
            $scope.highlight = "highlight";
            $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", false);
            $scope.isFormValid = false;
        }
        else {
            $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", true);
            $scope.isFormValid = true;
            $scope.highlight = "";
        }

        if ($scope.vendorPurchaseItem.packageQty == null || $scope.vendorPurchaseItem.packageQty == 0) {
            freeqty.disabled = true;
        } else {
            freeqty.disabled = false;
        }
    }
    $scope.formatString = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    }
    $scope.checkMrp = function (purchase, sell, vat) {
        if (sell < purchase + (purchase * vat / 100))
            $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", false);
        else
            $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", true);
    };
    $scope.checkSellingMrp = function () {
        $scope.vendorPurchaseItem.packageMRP = $scope.vendorPurchaseItem.packageSellingPrice;
    };
    $("#expDate").keyup(function (e) {
        if ($(this).val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $(this).val($(this).val() + "/");
        }
    });

    $scope.checkInvoiceNo = function (val) {
        if (val == "" || val == null)
            return;
        if ($scope.vendorPurchase.selectedVendor != null && $scope.vendorPurchase.selectedVendor != "")
            $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;
        $scope.isExist = true;
        var vendorPurchase = $scope.vendorPurchase;
        $.LoadingOverlay("show");
        if ($scope.isExist) {
            vendorPurchaseService.validateInvoice($scope.vendorPurchase)
                .then(function (response) {
                    $.LoadingOverlay("hide");  //toastr.error('Error Occured', 'Error');
                }, function (error) {
                    $.LoadingOverlay("hide");
                    var ele = document.getElementById("invoiceNo");
                    ele.focus();
                    toastr.error('Invoice Number Already Exist', 'Error');
                    $scope.isExist = false;
                });
        }
    }
});
app.directive('numbersOnly', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});