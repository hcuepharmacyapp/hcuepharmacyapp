app.controller('alternatePackageSizeCtrl', function ($scope, alternateVendorProductModel, productService, toastr, $filter, close, $window, vendorService) {

    $scope.data = angular.copy(alternateVendorProductModel);
    $scope.shouldBeOpen = true;
    $scope.updateEnable = false;
    $scope.selectedVendor = {};
    $scope.vendorStatus = 1;
    $scope.focusSelectedProduct = false;
    $scope.dataSelectedProductNameArray = [];
    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) {
            $scope.vendorList = response.data;
            $scope.vendorList = $filter('orderBy')($scope.vendorList, 'name');
            for (var i = 0; i < $scope.vendorList.length; i++) {
                if ($scope.data[0].vendorId == $scope.vendorList[i].id) {
                    $scope.selectedVendor = $scope.vendorList[i];
                }
                if ($scope.data[0].vendorId == undefined) {
                    $scope.selectedVendorDisable = false;
                }
                else {
                    $scope.selectedVendorDisable = true;
                }
            }
        }, function () { toastr.error('Error Occured', 'Error'); });
    }
    $scope.vendor();

    $scope.updatePackageSize = function () {
        for (var i = 0; i < $scope.data.length; i++) {
            $scope.data[i].vendorId = $scope.selectedVendor.id;
            if ($scope.data[i].isAddNewProduct == false || $scope.data[i].isAddNewProduct == undefined) {
                if ($scope.data[i].productId == undefined) {

                    if ($scope.data[i].selectedProduct != undefined) {
                        if ($scope.data[i].selectedProduct.id != undefined) {
                            $scope.data[i].productId = $scope.data[i].selectedProduct.id;
                            $scope.data[i].productName = $scope.data[i].selectedProduct.name;
                        } else {
                            toastr.info('Please select the alternate product name or add as new');
                            return;
                        }
                    }
                    else {
                        toastr.info('Please select the alternate product name or add as new');
                        return;
                    }
                }
                else {
                    $scope.data[i].productName = $scope.data[i].alternateProductName;
                }
            } else {
                $scope.data[i].productName = $scope.data[i].alternateProductName;
            }
        }

        return productService.updatePackageSize($scope.data).then(function (response) {
            $scope.data = response.data;
            if ($scope.data.length > 0) {
                //window.location = "/ImportBuy/Index";
                toastr.success('Mapping completed successfully.Please select your import file now');
                $scope.close($scope.data);
            }
        });
    };
    $scope.getProducts = function (val) {
        return productService.nonHiddenProductList(val).then(function (response) {
            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                item.name = item.name.trim();
                return item;
            });
        });
    }
    $scope.onProductSelect = function ($event, selectedProduct1, itemIndex) {
        if (selectedProduct1 != null) {
            $scope.productId = selectedProduct1.id;
        }

        //if ($scope.data[itemIndex].selectedProduct.id != null && $scope.data[itemIndex].selectedProduct.id != undefined) {
        //    $scope.data[itemIndex].productId = $scope.data[itemIndex].selectedProduct.id;
        //    $scope.compareAlternateList();
        //}
    }

    $scope.enableUpdate = function (val1, itemid) {

        checkAlternatePackSize();
        //$scope.compareAlternateList();

        ele = document.getElementById(itemid);
        ele.focus(itemid);
    };

    function checkAlternatePackSize() {
        var count = 0;

        for (var i = 0; i < $scope.data.length; i++) {
            if ($scope.data[i].updatedPackageSize > 0) {
                count++;
            }
        }

        if (count == $scope.data.length) {
            $scope.updateEnable = true;
        } else {
            $scope.updateEnable = false;
        }
    };

    checkAlternatePackSize();

    $scope.Disable = function () {
        if ($scope.data.updatedPackageSize == null || $scope.data.updatedPackageSize == undefined) {
            $scope.Iscount = true;
        }
        else if ($scope.data.updatedPackageSize.length > 0) {
            $scope.Iscount = false;
        }
    }

    //$scope.close = function (result) {
    //    close(result, 0); // close, but give 100ms for bootstrap to animate
    //    if (result != "Yes") {
    //        result = null;
    //    }

    //    $(".modal-backdrop").hide();
    //};
    $scope.close = function (result) {
        close(result, 0);
        $(".modal-backdrop").hide();
        $.LoadingOverlay("hide");
    };

    $scope.selectAll = function () {
        var toggleStatus = $scope.isAllSelected;

        angular.forEach($scope.data, function (itm) {
            itm.isAddNewProduct = toggleStatus;
        });
        if (toggleStatus) {
            var tempList = JSON.parse(JSON.stringify($scope.data));
            if ($scope.dataSelectedProductNameArray.length == 0)
                $scope.dataSelectedProductNameArray = tempList;
            else {
                angular.forEach(tempList, function (itm) {
                    $scope.dataSelectedProductNameArray.push(itm);
                });
            }
        }
        else {
            var poptempList = JSON.parse(JSON.stringify($scope.data));
            angular.forEach(poptempList, function (index) {
                var idx = index;
                $scope.dataSelectedProductNameArray.splice(index, 1);
            });
        }
    }
    $scope.checkProductList = function () {
        $scope.vendorId = $scope.selectedVendor.id;
        vendorService.checkExisitingProductList($scope.vendorId).then(function (response) {
            $scope.existingAlternateList = response.data;
            //$scope.compareAlternateList();
        })

    };
    //$scope.compareAlternateList = function ()
    //{
    //    angular.forEach($scope.existingAlternateList, function (value1, key1) {
    //        angular.forEach($scope.data, function (value2, key2) {
    //            if (value1.productId === value2.productId && value1.alternateProductName === value2.alternateProductName && value1.alternatePackageSize === value2.alternatePackageSize && value1.updatedPackageSize === value2.updatedPackageSize) {
    //                $scope.data.splice(key2);

    //            }
    //        });
    //    });

    //    }
    $scope.particularProductSelect = function (status, index, list) {

        var count = 0;
        var listcount = $scope.data.length;
        angular.forEach($scope.data, function (itm, ind) {
            if (itm.isAddNewProduct)
                count++;
        });

        if (status)
            $scope.dataSelectedProductNameArray.push(JSON.parse(JSON.stringify(list)));

        else {
            var idx = $scope.dataSelectedProductNameArray.indexOf(index);
            $scope.dataSelectedProductNameArray.splice(idx, 1);
        }

        if (count == listcount) {
            $scope.isAllSelected = true;
        } else {
            $scope.isAllSelected = false;
        }
    }
});