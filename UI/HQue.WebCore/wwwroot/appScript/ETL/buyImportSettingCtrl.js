app.controller("buyImportSettingCtrl", function ($scope, toastr, vendorPurchaseService, templatePurchaseMasterModel, templatePurchaseChildMasterModel, templatePurchaseChildMasterSettingsModel, ModalService, vendorService, $filter) {

    //Check offline status
    $scope.IsOffline = false;
    $scope.isOnlineEnabled = false;
    $scope.vendorStatus = 1;

    getOfflineStatus = function () {
        vendorPurchaseService.getOfflineStatus().then(function (response) {
            if (response.data) {
                $scope.IsOffline = true;
            }
        });
    };
    //
    getOfflineStatus();

    //Added by Sarubala on 18-05-18
    getOnlineEnabledStatus = function () {
        vendorPurchaseService.getOnlineEnableStatus().then(function (response) {
            if (response.data) {
                $scope.isOnlineEnabled = true;
            }
        }, function (error) {
            console.log(error);
        });
    };

    getOnlineEnabledStatus();

    $scope.SelectedFileForUpload = null;
  
    var headerOptions = "";
    $scope.headerOption = "No";
    //$scope.ShowProductRow = true;
    $scope.showPanel = true;
    $scope.listData = {};
    $scope.tempList = {};

    $scope.productRowPos = "";
    $scope.disableSaveButton = true;


    $scope.templatePurchaseMaster = templatePurchaseMasterModel;
    $scope.templatePurchaseChildMaster = templatePurchaseChildMasterModel;
    $scope.templatePurchaseChildMasterSettings = templatePurchaseChildMasterSettingsModel;

    $.LoadingOverlay("show");
  
    vendorPurchaseService.getBuyImportDataFields($scope.templatePurchaseMaster).then(function (response) {

        //dataItem.vendorHeaderName != 'VendorName' || dataItem.vendorHeaderName != 'VendorDiscount'
        //                                                             || dataItem.vendorHeaderName != 'FreeQty' || dataItem.vendorHeaderName != 'ProductDiscount'
        //dataItem.vendorHeaderName != 'Cst'"
        $scope.templatePurchaseMaster = response.data;

        for (var i = 0; i < $scope.templatePurchaseMaster.length; i++) {

            $scope.templatePurchaseMaster[i].isMandatory = true;
            if ($scope.templatePurchaseMaster[i].vendorHeaderName == 'VendorName' || $scope.templatePurchaseMaster[i].vendorHeaderName == 'VendorDiscount' || $scope.templatePurchaseMaster[i].vendorHeaderName == 'FreeQty' || $scope.templatePurchaseMaster[i].vendorHeaderName == 'ProductDiscount' || $scope.templatePurchaseMaster[i].vendorHeaderName == 'Cst' || $scope.templatePurchaseMaster[i].vendorHeaderName == 'RackNo' || $scope.templatePurchaseMaster[i].vendorHeaderName == 'BoxNo' || $scope.templatePurchaseMaster[i].vendorHeaderName == "HsnCode" || $scope.templatePurchaseMaster[i].vendorHeaderName == "InvoiceNo"
                || $scope.templatePurchaseMaster[i].vendorHeaderName == 'VendorMarkupPerc' || $scope.templatePurchaseMaster[i].vendorHeaderName == 'ProductMarkupPerc' || $scope.templatePurchaseMaster[i].vendorHeaderName == 'SchemeDiscountPerc') {
                $scope.templatePurchaseMaster[i].isMandatory = false;
            }
        }



        $.LoadingOverlay("hide");
    }, function () { $.LoadingOverlay("hide"); });



    //File Select event 
    $scope.changeFile = function (file) {
        $scope.SelectedFileForUpload = file[0];
       
    };


    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) {    //vendorService.vendorData().then(function (response) {
            $scope.vendorList = response.data;
        }, function () { toastr.error('Error Occured', 'Error'); });
    }
    $scope.vendor();


    $scope.vendorPurchase = { "selectedVendor": null };

    $scope.headerMultiple = function () {
        //    if ($scope.headerOption == "Yes") {
        //        $scope.productRowPos = "";
        //    } else {
        //        $scope.productRowPos = "";
        //        $scope.headerOption = "No";
        //    }
    }

    $scope.mapping = function () {
        $.LoadingOverlay("show");
        $scope.showPanel = false;
        if ($scope.headerOption == "Yes") {
            $scope.productRowPos = document.getElementById("productRowPos").value;
        } else {
            $scope.productRowPos = "";
            $scope.headerOption = "No";
        }
        $scope.vendorData = "";
        vendorPurchaseService.mappingData($scope.SelectedFileForUpload, $scope.vendorRowPos, $scope.productRowPos).then(function (response) {
            $scope.listData = response.data;


            $scope.vendorData = $scope.listData;

            if ($scope.SelectedFileForUpload.name != "") {
                $scope.showDetails = true;
                $scope.disableSaveButton = false;
            }
            else {
                $scope.showPanel = false;
                $scope.disableSaveButton = true;
            }

            $.LoadingOverlay("hide");
        }, function () { $.LoadingOverlay("hide"); });
    }

    $scope.showDetails = true;
    $scope.showPanel = true;

    getBuyTemplate = function () {

        $scope.vendorId = $scope.vendorPurchase.selectedVendor.id;
        vendorPurchaseService.getBuyTemplateSetting($scope.vendorId).then(function (response) {
            $scope.data = response.data;

           
                //$scope.vendor();

                // $scope.vendorPurchase.selectedVendor = $filter("filter")($scope.vendorList, { "id": $scope.data.templatePurchaseChildMasterSettings.vendorId })[0];

                $scope.templatePurchaseChildMaster = $scope.data.templatePurchaseChildMaster;

               

                if ($scope.data.templatePurchaseChildMaster.length > 0) {

                    $scope.showDetails = false;
                    $scope.showPanel = true;
                    $scope.templatePurchaseChildMasterSettings = $scope.data.templatePurchaseChildMasterSettings;

                    $scope.headerOption = $scope.data.templatePurchaseChildMasterSettings.headerOption;

                    if ($scope.headerOption == true) {
                        $scope.headerOption = "Yes";
                    }
                    else {
                        $scope.headerOption = "No";
                    }
                    $scope.vendorRowPos = $scope.data.templatePurchaseChildMasterSettings.vendorRowPos;
                    $scope.productRowPos = $scope.data.templatePurchaseChildMasterSettings.productRowPos;
                    $scope.dateFormat = $scope.data.templatePurchaseChildMasterSettings.dateFormat;
                    // $scope.vendorList = $scope.templatePurchaseChildMasterSettings.vendorId;
                    // $scope.vendorPurchase.selectedVendor = $scope.templatePurchaseChildMasterSettings.vendorId;
                    // $scope.vendorList  = $scope.templatePurchaseChildMasterSettings.vendorId;
                }

                else {
                   
                    $scope.showDetails = true;
                    $scope.showPanel = true;
                }
           


        }, function () {

        });
    }

     //getBuyTemplate();

    $scope.saveBuyTemplateSetting = function () {
        $.LoadingOverlay("show");
        $scope.showPanel = false;


        $scope.templatePurchaseChildMaster = $scope.templatePurchaseMaster;

        if ($scope.headerOption == "Yes") {
            //$scope.ShowProductRow = false;
            //$scope.headerOptions = true;
            console.log($scope.templatePurchaseChildMasterSettings);
            $scope.templatePurchaseChildMasterSettings.headerOption = true;
            $scope.templatePurchaseChildMasterSettings.vendorRowPos = $scope.vendorRowPos;
            $scope.templatePurchaseChildMasterSettings.productRowPos = $scope.productRowPos;
            $scope.templatePurchaseChildMasterSettings.dateFormat = $scope.dateFormat;
            $scope.templatePurchaseChildMasterSettings.vendorId = $scope.vendorPurchase.selectedVendor.id;


            for (var i = 0; i < $scope.templatePurchaseMaster.length; i++) {


                $scope.templatePurchaseChildMaster[i].templateColumnId = $scope.templatePurchaseMaster[i].templateColumnId;
                $scope.templatePurchaseChildMaster[i].headerIndex = $scope.templatePurchaseMaster[i].vendorData;
                $scope.templatePurchaseChildMaster[i].headerName = $scope.vendorData[$scope.templatePurchaseMaster[i].vendorData];
                $scope.templatePurchaseChildMaster[i].vendorId = $scope.vendorPurchase.selectedVendor.id;
            }


        }
        else {
            //$scope.ShowProductRow = true;
            $scope.productRowPos = "";
            // $scope.headerOptions = false;

            $scope.templatePurchaseChildMasterSettings.headerOption = false;
            $scope.templatePurchaseChildMasterSettings.vendorRowPos = $scope.vendorRowPos;
            $scope.templatePurchaseChildMasterSettings.productRowPos = $scope.productRowPos;
            $scope.templatePurchaseChildMasterSettings.dateFormat = $scope.dateFormat;
            $scope.templatePurchaseChildMasterSettings.vendorId = $scope.vendorPurchase.selectedVendor.id;


            for (var i = 0; i < $scope.templatePurchaseMaster.length; i++) {

                $scope.templatePurchaseChildMaster[i].templateColumnId = $scope.templatePurchaseMaster[i].templateColumnId;
                $scope.templatePurchaseChildMaster[i].headerIndex = $scope.templatePurchaseMaster[i].vendorData;
                $scope.templatePurchaseChildMaster[i].headerName = $scope.vendorData[$scope.templatePurchaseMaster[i].vendorData];
                $scope.templatePurchaseChildMaster[i].vendorId = $scope.vendorPurchase.selectedVendor.id;
            }
        }

        $scope.tempList.templatePurchaseChildMaster = $scope.templatePurchaseChildMaster;
        $scope.tempList.templatePurchaseChildMasterSettings = $scope.templatePurchaseChildMasterSettings;

        vendorPurchaseService.saveBuyTemplateSetting($scope.tempList).then(function (response) {
            $scope.templatePurchaseChildMaster = response.data;
            toastr.success('Purchase Template Saved Successfully');
            $scope.templatePurchaseChildMaster = {};
            $scope.templatePurchaseChildMasterSettings = {};
            $scope.productRowPos = "";
            $scope.vendorRowPos = "";
            $scope.headerOption = "No";
            $scope.showDetails = true;
            $scope.showPanel = true;

           // getBuyTemplate();
            $.LoadingOverlay("hide");
        }, function () { $.LoadingOverlay("hide"); });
    }




});

app.directive('fileInput', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            element.bind('change', function () {
                $parse(attributes.fileInput)
                .assign(scope, element[0].files)
                scope.$apply()
            });
        }
    };
}]);

