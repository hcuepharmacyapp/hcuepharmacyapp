
app.factory('accountModel', function () { return { id: null, code: null, name: null, registrationDate: null, instanceCount: null, phone: null, email: null, address: null, area: null, city: null, state: null, pincode: null, registerType: null, isChain: null, licensePeriod: null, licenseStartDate: null, licenseEndDate: null, licenseCount: null, isAMC: null, isApproved: null, network: null, offlineStatus: null, isBillNumberReset: null, instanceTypeId: null, } });

app.factory('alternateVendorProductModel', function () { return { id: null, accountId: null, instanceId: null, productId: null, vendorId: null, alternateProductName: null, offlineStatus: null, alternatePackageSize: null, updatedPackageSize: null, } });

app.factory('barcodePrnDesignModel', function(){return{id:null,accountId:null,instanceId:null,prnName:null,prnFileData:null,prnFileName:null,prnFieldJson:null,offlineStatus:null,} });

app.factory('barcodeProfileModel', function(){return{id:null,accountId:null,instanceId:null,barcodeGenSeqNo:null,profileName:null,profileJson:null,isDefaultProfile:null,offlineStatus:null,} });

app.factory('batchListSettingsModel', function(){return{id:null,accountId:null,instanceId:null,batchListType:null,offlineStatus:null,} });

app.factory('billPrintSettingsModel', function(){return{id:null,accountId:null,instanceId:null,status:null,footerNote:null,} });

app.factory('cardTypeSettingsModel', function(){return{id:null,accountId:null,instanceId:null,cardType:null,offlineStatus:null,} });

app.factory('closingStockModel', function(){return{id:null,accountId:null,instanceId:null,productStockId:null,transDate:null,closingStock:null,stockValue:null,gstTotal:null,isOffline:null,isSynctoOnline:null,} });

app.factory('communicationLogModel', function () { return { id: null, accountId: null, instanceId: null, userId: null, smsLogId: null, mobile: null, message: null, offlineStatus: null, } });

app.factory('customDevReportModel', function(){return{id:null,accountId:null,instanceId:null,reportName:null,reportType:null,query:null,filterJSON:null,configJSON:null,offlineStatus:null,} });

app.factory('customReportModel', function(){return{id:null,accountId:null,instanceId:null,reportName:null,reportType:null,query:null,configJSON:null,remarks:null,offlineStatus:null,} });

app.factory('customSettingsDetailModel', function(){return{id:null,accountId:null,instanceId:null,customSettingsGroupId:null,isActive:null,offlineStatus:null,} });

app.factory('customSettingsModel', function(){return{id:null,groupId:null,groupName:null,} });

app.factory('customerIDSeriesItemModel', function(){return{id:null,accountId:null,instanceId:null,seriesName:null,activeStatus:null,offlineStatus:null,} });

app.factory('customerPaymentModel', function(){return{id:null,accountId:null,instanceId:null,salesId:null,customerId:null,transactionDate:null,debit:null,credit:null,paymentType:null,chequeNo:null,chequeDate:null,cardNo:null,cardDate:null,remarks:null,offlineStatus:null,bankDeposited:null,amountCredited:null,} });

app.factory('dCVendorPurchaseItemModel', function(){return{id:null,accountId:null,instanceId:null,vendorId:null,dCDate:null,dCNo:null,productStockId:null,packageSize:null,packageQty:null,packagePurchasePrice:null,packageSellingPrice:null,packageMRP:null,quantity:null,purchasePrice:null,offlineStatus:null,freeQty:null,discount:null,isActive:null,isPurchased:null,taxRefNo:null,} });
app.factory('debugModel', function(){return{id:null,name:null,age:null,} });

app.factory('defaultDoctorModel', function () { return { id: null, accountId: null, instanceId: null, doctorId: null, } });

app.factory('deliveryBoyModel', function(){return{id:null,accountId:null,instanceId:null,name:null,imagePath:null,offlineStatus:null,} });

app.factory('discountRulesModel', function(){return{id:null,accountId:null,instanceId:null,amount:null,toAmount:null,discount:null,fromDate:null,toDate:null,billAmountType:null,offlineStatus:null,} });

app.factory('doctorModel', function () { return { id: null, accountId: null, instanceId: null, code: null, name: null, shortName: null, clinicName: null, mobile: null, phone: null, email: null, area: null, city: null, state: null, pincode: null, gsTin: null, offlineStatus: null, commission: null, } });

app.factory('domainMasterModel', function () { return { id: null, accountId: null, instanceId: null, code: null, displayText: null, parentId: null, } });

app.factory('domainValuesModel', function () { return { id: null, accountId: null, instanceId: null, domainId: null, domainValue: null, displayText: null, hasSubdomain: null, subId: null, } });

app.factory('draftVendorPurchaseItemModel', function(){return{id:null,accountId:null,instanceId:null,draftVendorPurchaseId:null,productStockId:null,productName:null,productId:null,batchNo:null,expireDate:null,taxType:null,vAT:null,cST:null,packageSize:null,packageQty:null,packagePurchasePrice:null,packageSellingPrice:null,quantity:null,purchasePrice:null,offlineStatus:null,freeQty:null,discount:null,fromTempId:null,fromDcId:null,} });

app.factory('draftVendorPurchaseModel', function(){return{id:null,accountId:null,instanceId:null,vendorId:null,invoiceNo:null,invoiceDate:null,verndorOrderId:null,draftName:null,draftAddedOn:null,discount:null,credit:null,paymentType:null,chequeNo:null,chequeDate:null,offlineStatus:null,goodsRcvNo:null,comments:null,fileName:null,creditNoOfDays:null,noteType:null,noteAmount:null,} });

app.factory('errorLogModel', function(){return{id:null,accountId:null,instanceId:null,errorMessage:null,errorStackTrace:null,unSavedData:null,data:null,offlineStatus:null,} });


app.factory('estimateItemModel', function(){return{id:null,accountId:null,instanceId:null,estimateId:null,productStockId:null,productId:null,productName:null,reminderFrequency:null,reminderDate:null,quantity:null,discount:null,sellingPrice:null,offlineStatus:null,} });

app.factory('estimateModel', function(){return{id:null,accountId:null,instanceId:null,estimateNo:null,estimateDate:null,patientId:null,name:null,mobile:null,email:null,dOB:null,age:null,gender:null,address:null,pincode:null,doctorName:null,doctorMobile:null,deliveryType:null,billPrint:null,sendSms:null,sendEmail:null,discount:null,offlineStatus:null,} });

app.factory('finYearMasterModel', function(){return{id:null,accountId:null,instanceId:null,finYearStartDate:null,finYearEndDate:null,} });

app.factory('finYearResetStatusModel', function(){return{id:null,accountId:null,instanceId:null,finYearMasterId:null,financialYear:null,resetStatus:null,} });

app.factory('stateModel', function(){return{id:null,stateName:null,stateCode:null,countryId:null,offlineStatus:null,isUnionTerritory:null,} });

app.factory('hQueUserModel', function () { return { id: null, accountId: null, instanceId: null, userId: null, password: null, userType: null, status: null, name: null, mobile: null, address: null, area: null, city: null, state: null, pincode: null, passwordResetKey: null, resetRequestTime: null, offlineStatus: null, isOnlineEnable: null,} });

app.factory('importProcessActivityModel', function(){return{id:null,accountId:null,instanceId:null,fileName:null,isCloud:null,activityDesc:null,status:null,} });

app.factory('instanceClientModel', function(){return{id:null,instanceId:null,clientId:null,lastSyncedAt:null,offlineStatus:null,} });
app.factory('instanceClientsModel', function(){return{id:null,instanceId:null,clientId:null,lastSyncedOn:null,} });

app.factory('instanceModel', function () { return { id: null, accountId: null, externalId: null, code: null, name: null, registrationDate: null, phone: null, mobile: null, drugLicenseNo: null, tinNo: null, contactName: null, contactMobile: null, contactEmail: null, secondaryName: null, secondaryMobile: null, secondaryEmail: null, contactDesignation: null, bDOId: null, bDODesignation: null, bDOName: null, rMName: null, address: null, area: null, city: null, state: null, pincode: null, landmark: null, status: null, drugLicenseExpDate: null, buildingName: null, streetName: null, country: null, lat: null, lon: null, isDoorDelivery: null, isVerified: null, isUnionTerritory: null, isLicensed: null, offlineStatus: null, isOfflineInstalled: null, lastLoggedOn: null, distanceCover: null, gstselect: null, isHeadOffice: null, offlineVersionNo: null, gsTinNo: null, totalSmsCount: null, sentSmsCount: null, instanceTypeId: null, } });

app.factory('inventoryReOrderModel', function(){return{id:null,accountId:null,instanceId:null,productId:null,reOrder:null,offlineStatus:null,status:null,} });

app.factory('inventorySettingsModel', function(){return{id:null,accountId:null,instanceId:null,noOfDays:null,noOfMonth:null,popupStartDate:null,popupEndDate:null,popupStartTime:null,popupEndTime:null,popupInterval:null,popupEnabled:null,lastPopupAt:null,openedPopup:null,offlineStatus:null,} });

app.factory('inventorySmsSettingsModel', function(){return{id:null,accountId:null,instanceId:null,smsType:null,smsOption:null,smsContent:null,offlineStatus:null,} });

app.factory('inventoryUpdateHistoryModel', function(){return{id:null,accountId:null,instanceId:null,productStockId:null,batchNoOld:null,batchNoNew:null,expireDateOld:null,expireDateNew:null,vATOld:null,vATNew:null,packageSizeOld:null,packageSizeNew:null,packagePurchasePriceOld:null,packagePurchasePriceNew:null,sellingPriceOld:null,sellingPriceNew:null,mRPOld:null,mRPNew:null,} });

app.factory('invoiceSeriesItemModel', function(){return{id:null,accountId:null,instanceId:null,seriesName:null,invoiceseriesType:null,activeStatus:null,offlineStatus:null,} });

app.factory('invoiceSeriesModel', function () { return { id: null, accountId: null, instanceId: null, invoiceseriesType: null,offlineStatus: null, } });

app.factory('kindProductMasterModel', function(){return{id:null,accountId:null,kindName:null,isActive:null,} });

app.factory('leadsModel', function(){return{id:null,accountId:null,instanceId:null,patientId:null,externalId:null,leadDate:null,name:null,mobile:null,email:null,dOB:null,age:null,gender:null,address:null,pincode:null,doctorId:null,doctorName:null,doctorMobile:null,doctorSpecialization:null,leadStatus:null,offlineStatus:null,lat:null,lng:null,deliveryMode:null,} });

app.factory('leadsProductModel', function(){return{id:null,accountId:null,instanceId:null,leadsId:null,productId:null,name:null,type:null,dosage:null,bA:null,numberofDays:null,quantity:null,offlineStatus:null,} });

app.factory('loyaltyPointSettingsModel', function () { return { id: null, accountId: null, instanceId: null, loyaltySaleValue: null, loyaltyPoint: null, startDate: null, endDate: null, minimumSalesAmt: null, redeemPoint: null, redeemValue: null, maximumRedeemPoint: null, isActive: null, offlineStatus: null, loyaltyOption: null, isMinimumSale: null, isMaximumRedeem: null, isEndDate: null, } });

app.factory('loyaltyProductPointsModel', function () { return { id: null, accountId: null, instanceId: null, loyaltyId: null, kindName: null, kindOfProductPts: null, isActive: null, offlineStatus: null ,} });

app.factory('missedOrderModel', function(){return{id:null,accountId:null,instanceId:null,patientId:null,patientName:null,patientMobile:null,productId:null,quantity:null,sellingPrice:null,isActive:null,offlineStatus:null,} });

app.factory('orderSettingsModel', function () { return { id: null, accountId: null, instanceId: null, noOfDaysFromDate: null, dateSettingType: null, offlineStatus: null } });

app.factory('patientModel', function () { return { id: null, accountId: null, instanceId: null, code: null, name: null, shortName: null, mobile: null, email: null, dOB: null, age: null, gender: null, address: null, area: null, city: null, state: null, pincode: null, offlineStatus: null, empID: null, discount: null, isSync: null, customerPaymentType: null, patientType: null, isAutoSave: null, pan: null, gsTin: null, drugLicenseNo: null, locationType: null, balanceAmount: null, status: null, loyaltyPoint: null, redeemedPoint: null, customerSeriesId:null,prefix:null, } });

app.factory('patientOrderInstanceStatusModel', function(){return{id:null,patientOrderId:null,acceptanceStatus:null,accountId:null,instanceId:null,duration:null,messageCode:null,pushSentTime:null,distance:null,offlineStatus:null,} });

app.factory('patientOrderModel', function(){return{id:null,patientExtId:null,orderId:null,orderStatus:null,lat:null,lon:null,prescription:null,accountId:null,instanceId:null,amount:null,deliveryBoyId:null,deliveredTime:null,withPharmaId:null,offlineStatus:null,} });

app.factory('paymentModel', function(){return{id:null,accountId:null,instanceId:null,transactionDate:null,vendorId:null,debit:null,credit:null,paymentType:null,paymentMode:null,bankName:null,bankBranchName:null,ifscCode:null,chequeNo:null,chequeDate:null,vendorPurchaseId:null,remarks:null,offlineStatus:null,paymentHistoryStatus:null,status:null,} });

app.factory('paymentTypeModel', function(){return{id:null,accountId:null,instanceId:null,paymentInd:null,paymentCategory:null,paymentType:null,} });
app.factory('pettyCashDtlModel', function () { return { id: null, accountId: null, instanceId: null, userId: null, pettyHdrId: null, pettyItemId: null, description: null, amount: null, transactionDate: null, deletedStatus:false, offlineStatus: null, } });
app.factory('pettyCashHdrModel', function () { return { id: null, accountId: null, instanceId: null, userId: null, clbalance: 0, description: null, totalSales:0, floatingAmount:0, totalExpense:0, vendorPayment:0, transactionDate: null, noof2000: 0, noof1000: 0, noof500: 0, noof200: 0, noof100: 0, noof50: 0, noof20: 0, noof10: 0, others: 0, coins: 0, customerPayment: 0, counterNo: null, offlineStatus: null, } });

app.factory('pettyCashModel', function(){return{id:null,accountId:null,instanceId:null,userId:null,title:null, cashType:null,description:null,amount:null,transactionDate:null,offlineStatus:null,} });

app.factory('pettyCashSettingsModel', function(){return{id:null,accountId:null,instanceId:null,openingBalance:null,amount:null,createHead:null,creditOrDebit:null,offlineStatus:null,} });

app.factory('pharmacyPaymentModel', function(){return{id:null,accountId:null,instanceId:null,paymentDate:null,productType:null,quantity:null,cost:null,discountType:null,discount:null,tax:null,paid:null,balanceReason:null,salePersonType:null,salePersonName:null,salePersonMobile:null,commissionType:null,commission:null,paymentType:null,chequeNo:null,chequeDate:null,chequeBank:null,last4Digits:null,cardName:null,offlineStatus:null,comments:null,} });

app.factory('pharmacyTimingModel', function(){return{id:null,accountId:null,instanceId:null,dayOfWeek:null,timing:null,offlineStatus:null,} });

app.factory('pharmacyUploadsModel', function(){return{id:null,accountId:null,instanceId:null,fileName:null,offlineStatus:null,fileNumber:null,} });

app.factory('physicalStockHistoryModel', function(){return{id:null,accountId:null,instanceId:null,popupNo:null,productId:null,currentStock:null,physicalStock:null,openedAt:null,closedAt:null,offlineStatus:null,} });

app.factory('productInstanceModel', function () { return { id: null, accountId: null, instanceId: null, productId: null, reOrderLevel: null, reOrderQty: null, reOrderModified: null, rackNo: null, boxNo: null, offlineStatus: null, ext_RefId: null, } });

app.factory('productMasterModel', function(){return{id:null,accountId:null,instanceId:null,code:null,name:null,manufacturer:null,kindName:null,strengthName:null,type:null,schedule:null,category:null,genericName:null,commodityCode:null,packing:null,offlineStatus:null,packageSize:null,vAT:null,price:null,status:null,rackNo:null,hsnCode:null,igst:null,cgst:null,sgst:null,gstTotal:null,} });

app.factory('productModel', function(){return{id:null,accountId:null,instanceId:null,code:null,name:null,manufacturer:null,kindName:null,strengthName:null,type:null,schedule:null,category:null,genericName:null,commodityCode:null,packing:null,offlineStatus:null,ext_RefId:null,packageSize:null,vAT:null,price:null,status:null,rackNo:null,productMasterID:null,productOrgID:null,reOrderLevel:null,reOrderQty:null,discount:null,eancode:null,isHidden:null,hsnCode:null,igst:null,cgst:null,sgst:null,gstTotal:null,subcategory:null,} });

app.factory('productSearchSettingsModel', function(){return{id:null,accountId:null,instanceId:null,searchType:null,offlineStatus:null,} });

app.factory('productStockModel', function(){return{id:null,accountId:null,instanceId:null,productId:null,vendorId:null,batchNo:null,expireDate:null,vAT:null,taxType:null,sellingPrice:null,mRP:null,purchaseBarcode:null,barcodeProfileId:null,stock:null,packageSize:null,packagePurchasePrice:null,purchasePrice:null,offlineStatus:null,cST:null,isMovingStock:null,reOrderQty:null,status:null,isMovingStockExpire:null,newOpenedStock:null,newStockInvoiceNo:null,newStockQty:null,stockImport:null,importQty:null,eancode:null,hsnCode:null,igst:null,cgst:null,sgst:null,gstTotal:null,importDate:null,ext_RefId:null,taxRefNo:null,prvStockQty:null,} });

app.factory('product_deletedModel', function(){return{id:null,accountId:null,instanceId:null,code:null,name:null,manufacturer:null,kindName:null,strengthName:null,type:null,schedule:null,category:null,genericName:null,commodityCode:null,packing:null,packageSize:null,vAT:null,price:null,status:null,rackNo:null,} });

app.factory('purchaseSettingsModel', function () { return { id: null, accountId: null, instanceId: null, taxType: null, buyInvoiceDateEdit: null, offlineStatus: null, isAllowDecimal: null, completePurchaseKeyType: null, billSeriesType: null, customSeriesName: null, enableSelling: null, isSortByLowestPrice: null, enableMarkup: null, invoiceValueSetting: null, invoiceValueDifference : null,} });
app.factory('registrationModel', function () {
    //var timing = [
    //    {
    //        instanceid: null, dayOfWeek: null, startTime: null, endTime: null
    //    }
    //];
    return {
        "account": null,
        "instance": null
        //"timing": timing
    }
});

app.factory('registrationOTPModel', function(){return{id:null,accountId:null,userId:null,oTPType:null,oTPNumber:null,} });

app.factory('reminderProductModel', function(){return{id:null,accountId:null,instanceId:null,userReminderId:null,productStockId:null,productName:null,quantity:null,reminderFrequency:null,offlineStatus:null,} });

app.factory('reminderRemarkModel', function(){return{id:null,accountId:null,instanceId:null,userReminderId:null,remarksDate:null,remarksFor:null,remarks:null,offlineStatus:null,} });
app.factory('reminderRemarksModel', function(){return{id:null,accountId:null,instanceId:null,userReminderId:null,remarksDate:null,remarks:null,} });

app.factory('replicationDataModel', function(){return{id:null,accountId:null,instanceId:null,clientId:null,tableName:null,action:null,actionTime:null,offlineStatus:null,transactionID:null,} });

app.factory('requirementModel', function(){return{id:null,accountId:null,instanceId:null,gatheredBy:null,requirementNo:null,description:null,type:null,solvedBy:null,solvedDate:null,remarks:null,status:null,offlineStatus:null,} });

app.factory('saleSettingsModel', function () { return { id: null, accountId: null, instanceId: null, cardType: null, patientSearchType: null, doctorSearchType: null, doctorNameMandatoryType: null, saleTypeMandatory: null, postCancelBillDays: null, isCreditInvoiceSeries: null, discountType: null, customSeriesInvoice: null, printerType: null, customTempJSON: null, shortCutKeysType: null, newWindowType: null, autoTempStockAdd: null, offlineStatus: null, autosaveCustomer: null, maxDiscountAvail: null, instanceMaxDiscount: null, shortCustomerName: null, shortDoctorName: null, completeSaleKeyType: null, scanBarcode: null, autoScanQty: null, patientTypeDept: null, showSalesItemHistory: null, printLogo: null, printCustomFields: null, isCancelEditSMS: null, isEnableRoundOff: null, isEnableSalesUser: null, isEnableFreeQty: null, isEnableCustomerSeries: null, customerIDSeries: null, isEnableNewSalesScreen: null, } });

app.factory('salesBatchPopUpSettingsModel', function(){return{id:null,accountId:null,instanceId:null,quantity:null,batch:null,expiry:null,vat:null,unitMrp:null,stripMrp:null,unitsPerStrip:null,profit:null,rackNo:null,generic:null,mfg: null,category:null,age:null,supplier:null,} });

app.factory('salesItemAuditModel', function(){return{id:null,accountId:null,instanceId:null,salesId:null,productStockId:null,reminderFrequency:null,quantity:null,discount:null,sellingPrice:null,mRP:null,offlineStatus:null,action:null,} });

app.factory('salesItemModel', function () { return { id: null, accountId: null, instanceId: null, salesId: null, saleProductName: null, productStockId: null, reminderFrequency: null, reminderDate: null, quantity: null, itemAmount: null, vAT: null, vatAmount: null, discountAmount: null, discount: null, sellingPrice: null, mRP: null, offlineStatus: null, igst: null, cgst: null, sgst: null, gstTotal: null, gstAmount: null, totalAmount: null, salesOrderEstimateId: null, salesOrderEstimateType: null, salesItemSno: null, loyaltyProductPts: null, } });

app.factory('salesModel', function(){return{id:null,accountId:null,instanceId:null,patientId:null,invoiceSeries:null,prefix:null,invoiceNo:null,invoiceDate:null,name:null,mobile:null,email:null,dOB:null,age:null,gender:null,address:null,pincode:null,doctorName:null,doctorMobile:null,paymentType:null,deliveryType:null,billPrint:null,sendSms:null,sendEmail:null,discount:null,discountValue:null,discountType:null,totalDiscountValue:null,roundoffNetAmount:null,credit:null,fileName:null,offlineStatus:null,cashType:null,cardNo:null,cardDate:null,chequeNo:null,chequeDate:null,cardDigits:null,cardName:null,cancelstatus:null,salesType:null,netAmount:null,saleAmount:null,roundoffSaleAmount:null,bankDeposited:null,amountCredited:null,finyearId:null,doctorId:null,vatAmount:null,salesItemAmount:null,loyaltyPts:null,redeemPts:null,redeemAmt:null,doorDeliveryStatus:null,gstAmount:null,isRoundOff:null,taxRefNo:null,loyaltyId:null,redeemPercent:null,} });

app.factory('salesOrderEstimateItemModel', function(){return{id:null,accountId:null,instanceId:null,salesOrderEstimateId:null,productId:null,productStockId:null,quantity:null,receivedQty:null,batchNo:null,discount:null,expireDate:null,sellingPrice:null,isDeleted:null,isActive:null,isOrder:null,offlineStatus:null,} });

app.factory('salesOrderEstimateModel', function(){return{id:null,accountId:null,instanceId:null,patientId:null,prefix:null,orderEstimateId:null,orderEstimateDate:null,salesOrderEstimateType:null,isEstimateActive:null,finyearId:null,} });

app.factory('salesPaymentModel', function () { return { id: null, accountId: null, instanceId: null, salesId: null, paymentInd: null, subPaymentInd: null, amount: null, isActive: null, cardNo: null, cardDigits: null, cardName: null, cardDate: null, cardTransId: null, chequeNo: null, chequeDate: null, walletTransId: null, offlineStatus: null, } });

app.factory('salesReturnItemModel', function () { return { id: null, accountId: null, instanceId: null, salesReturnId: null, productStockId: null, quantity: null, cancelType: null, discount: null, offlineStatus: null, mrpSellingPrice: null, mRP: null, isDeleted: null, igst:null,cgst:null,sgst:null,gstTotal:null, action : null,} });

app.factory('salesReturnItemModel', function () { return { id: null, accountId: null, instanceId: null, salesReturnId: null, productStockId: null, quantity: null, cancelType: null, discount: null, discountAmount: null, offlineStatus: null, mrpSellingPrice: null, mRP: null, isDeleted: null, igst: null, cgst: null, sgst: null, gstTotal: null, gstAmount: null, totalAmount: null, loyaltyProductPts : null,} });

app.factory('salesReturnModel', function () { return { id: null, accountId: null, instanceId: null, salesId: null, patientId: null, invoiceSeries: null, prefix: null, returnNo: null, returnDate: null, cancelType: null, offlineStatus: null, finyearId: null, isAlongWithSale: null, taxRefNo: null, returnCharges: null, returnChargePercent: null, discount: null, discountValue: null, netAmount: null, roundOffNetAmount: null, totalDiscountValue: null, returnItemAmount: null, gstAmount: null, isRoundOff: null, salesReturnItemLoyaltyProductPts: null, loyaltyId: null,} });

app.factory('salesTemplateItemModel', function(){return{id:null,accountId:null,instanceId:null,templateId:null,productId:null,quantity:null,isDeleted:null,offlineStatus:null,} });

app.factory('salesTemplateModel', function(){return{id:null,accountId:null,instanceId:null,prefix:null,templateNo:null,templateDate:null,templateName:null,isActive:null,finyearId:null,} });

app.factory('salesTypeModel', function(){return{id:null,accountId:null,instanceId:null,name:null,status:null,isActive:null,offlineStatus:null,} });

app.factory('schedulerLogModel', function(){return{id:null,accountId:null,instanceId:null,task:null,executedDate:null,isOffline:null,isSynctoOnline:null,} });

app.factory('selfConsumptionModel', function(){return{id:null,accountId:null,instanceId:null,invoiceNo:null,productStockId:null,consumption:null,consumptionNotes:null,consumedBy:null,offlineStatus:null,} });

app.factory('settingsModel', function () { return { id: null, accountId: null, instanceId: null, enable_GlobalProdut: null, pur_SortByLowestPrice: null, saleBillToCSV: null, salesPriceSetting:null, } });

app.factory('settlementsModel', function(){return{id:null,accountId:null,instanceId:null,voucherId:null,transactionType:null,transactionId:null,adjustedAmount:null,offlineStatus:null,} });

app.factory('smsConfigurationModel', function () { return { id: null, accountId: null, instanceId: null, date: null, remarks: null, smsCount: null, isActive: null, offlineStatus: null, } });

app.factory('smsLogModel', function () { return { id: null, accountId: null, instanceId: null, smsDate: null, logType: null, receiverName: null, invoiceNo: null, invoiceDate: null, referenceId: null, noOfSms: null, openingCount: null, closingCount: null, offlineStatus: null, } });

app.factory('smsSettingsModel', function () { return { id: null, accountId: null, instanceId: null, isVendorCreateSms: null, isSalesCreateSms: null, isSalesEditSms: null, isSalesCancelSms: null, isSalesEstimateSms: null, isSalesTemplateCreateSms: null, isSalesOrderSms: null, isLoginSms: null, isCustomerBulkSms: null, isOrderCreateSms: null, isUserCreateSms: null, offlineStatus: null, } });

app.factory('stateModel', function(){return{id:null,stateName:null,stateCode:null,countryId:null,offlineStatus:null,isUnionTerritory:null,} });

app.factory('stockAdjustmentModel', function(){return{id:null,accountId:null,instanceId:null,productStockId:null,adjustedStock:null,adjustedBy:null,offlineStatus:null,taxRefNo:null,} });

app.factory('stockLedgerUpdateHistoryItemModel', function () { return { id: null, accountId: null, instanceId: null, stockLedgerUpdateId: null, productId: null, productStockId: null, batchNo: null, expireDate: null, currentStock: null, actualStock: null, transactionType: null, } });

app.factory('stockLedgerUpdateHistoryModel', function(){return{id:null,accountId:null,instanceId:null,productStockId:null,importQty:null,currentStock:null,openingStock:null,adjustedStockIn:null,purchaseQty:null,saleReturnQty:null,dcQty:null,tempQty:null,transferInAccQty:null,adjustedStockOut:null,saleQty:null,purchaseDelQty:null,transferOutQty:null,consumptionQty:null,finalIn:null,finalOut:null,diffQty:null,closingStock:null,} });

app.factory('stockTransferItemModel', function(){return{id:null,accountId:null,instanceId:null,transferId:null,productStockId:null,vendorOrderItemId:null,quantity:null,discount:null,productId:null,batchNo:null,expireDate:null,vAT:null,purchasePrice:null,sellingPrice:null,mRP:null,packageSize:null,vendorId:null,toProductStockId:null,acceptedStrip:null,acceptedPackageSize:null,acceptedUnits:null,offlineStatus:null,toInstanceId:null,isStripTransfer:null,igst:null,cgst:null,sgst:null,gstTotal:null,} });

app.factory('stockTransferModel', function(){return{id:null,accountId:null,instanceId:null,fromInstanceId:null,toInstanceId:null,prefix:null,transferNo:null,transferDate:null,fileName:null,comments:null,transferStatus:null,offlineStatus:null,transferBy:null,finyearId:null,taxRefNo:null,} });

app.factory('stockValueReportModel', function(){return{id:null,accountId:null,instanceId:null,reportNo:null,category:null,productCount:null,vat0PurchasePrice:null,vat5PurchasePrice:null,vat145PurchasePrice:null,vatOtherPurchasePrice:null,totalPurchasePrice:null,gst0PurchasePrice:null,gst5PurchasePrice:null,gst12PurchasePrice:null,gst18PurchasePrice:null,gst28PurchasePrice:null,gstOtherPurchasePrice:null,totalMrp:null,profit:null,} });

app.factory('syncSettingsModel', function(){return{id:null,accountId:null,instanceId:null,isProductSync:null,} });
app.factory('sysdiagramsModel', function(){return{name:null,name:null,principal_id:null,diagram_id:null,version:null,definition:null,} });

app.factory('taxSeriesItemModel', function(){return{id:null,accountId:null,instanceId:null,taxSeriesName:null,activeStatus:null,offlineStatus:null,} });

app.factory('taxValuesModel', function () { return { id: null, accountId: null, taxGroupId: null, tax: null, isActive: null,} });

app.factory('tempProductStockModel', function(){return{id:null,accountId:null,instanceId:null,productId:null,vendorId:null,batchNo:null,expireDate:null,vAT:null,sellingPrice:null,purchaseBarcode:null,stock:null,packageSize:null,packagePurchasePrice:null,purchasePrice:null,cST:null,isMovingStock:null,reOrderQty:null,status:null,isMovingStockExpire:null,} });

app.factory('tempSalesItemModel', function(){return{id:null,accountId:null,instanceId:null,salesId:null,productStockId:null,reminderFrequency:null,quantity:null,discount:null,sellingPrice:null,} });

app.factory('tempSalesModel', function(){return{id:null,accountId:null,instanceId:null,patientId:null,invoiceNo:null,invoiceDate:null,name:null,mobile:null,email:null,dOB:null,age:null,gender:null,address:null,pincode:null,doctorName:null,doctorMobile:null,paymentType:null,deliveryType:null,billPrint:null,sendSms:null,sendEmail:null,discount:null,credit:null,fileName:null,cashType:null,cardNo:null,cardDate:null,cardDigits:null,cardName:null,} });

app.factory('tempVendorPurchaseItemModel', function () { return { id: null, accountId: null, instanceId: null, productStockId: null, packageSize: null, packageQty: null, packagePurchasePrice: null, packageSellingPrice: null, packageMRP: null, quantity: null, purchasePrice: null, offlineStatus: null, freeQty: null, discount: null, isActive: null, taxRefNo: null, isDeleted:null, } });

app.factory('templatePurchaseChildMasterModel', function(){return{id:null,accountId:null,instanceId:null,templateColumnId:null,headerIndex:null,headerName:null,vendorId:null,} });

app.factory('templatePurchaseChildMasterSettingsModel', function(){return{id:null,accountId:null,instanceId:null,headerOption:null,vendorRowPos:null,productRowPos:null,dateFormat:null,vendorId:null,} });

app.factory('templatePurchaseMasterModel', function(){return{id:null,accountId:null,instanceId:null,vendorHeaderName:null,templateColumnId:null,} });

app.factory('tmp_VendorPurchase_errorModel', function(){return{id:null,accountId:null,instanceId:null,vendorId:null,invoiceNo:null,invoiceDate:null,verndorOrderId:null,discount:null,credit:null,paymentType:null,chequeNo:null,chequeDate:null,goodsRcvNo:null,comments:null,fileName:null,creditNoOfDays:null,} });

app.factory('toolModel', function(){return{id:null,accountId:null,instanceId:null,toolName:null,description:null,image:null,status:null,offlineStatus:null,} });

app.factory('transferSettingsModel', function(){return{id:null,accountId:null,instanceId:null,productBatchDisplayType:null,transferQtyType:null,transferZeroStock:null,showExpiredQty:null,} });

app.factory('userAccessModel', function(){return{id:null,userId:null,accountId:null,instanceId:null,accessRight:null,offlineStatus:null,} });

app.factory('userReminderModel', function(){return{id:null,accountId:null,instanceId:null,hqueUserId:null,referenceId:null,referenceFrom:null,reminderPhone:null,customerName:null,customerAge:null,customerGender:null,doctorName:null,startDate:null,reminderTime:null,endDate:null,reminderFrequency:null,reminderStatus:null,reminderType:null,description:null,remarks:null,offlineStatus:null,} });

app.factory('vendorModel', function () { return { id: null, accountId: null, instanceId: null, code: null, name: null, mobile: null, phone: null, email: null, address: null, area: null, city: null, state: null, pincode: null, allowViewStock: null, drugLicenseNo: null, tinNo: null, status: null, offlineStatus: null, paymentType: null, creditNoOfDays: null, enableCST: null, contactPerson: null, designation: null, landmark: null, cSTNo: null, discount: null, mobile1: null, mobile2: null, mobile1checked: null, mobile2checked: null, isSendMail: null, taxType: null, gsTinNo: null, locationType: null, stateId: null, ext_RefId: null, vendorType: null, balanceAmount: 0, vendorPurchaseFormulaId: null, isHeadOfficeVendor : null, } });

app.factory('vendorOrderItemModel', function () { return { id: null, accountId: null, instanceId: null, vendorOrderId: null, productId: null, quantity: null, receivedQty: null, vAT: null, packageSize: null, packageQty: null, purchasePrice: null, offlineStatus: null, vendorPurchaseId: null, stockTransferId: null, sellingPrice: null, expireDate: null, toInstanceId: null, isDeleted: null, igst: null, cgst: null, sgst: null, gstTotal: null, orderItemSno: null, freeQty: null} });

app.factory('vendorOrderModel', function () { return { id: null, accountId: null, instanceId: null, vendorId: null, orderId: null, orderDate: null, dueDate: null, pendingAuthorisation: null, authorisedBy: null, offlineStatus: null, finyearId: null, toInstanceId: null, prefix: null, taxRefNoColumn: null, } });

app.factory('vendorPurchaseFormulaModel', function(){return{id:null,accountId:null,instanceId:null,formulaName:null,formulaJSON:null,offlineStatus:null,} });

app.factory('vendorPurchaseItemAuditModel', function(){return{id:null,accountId:null,instanceId:null,vendorPurchaseId:null,productStockId:null,packageSize:null,packageQty:null,packagePurchasePrice:null,packageSellingPrice:null,packageMRP:null,quantity:null,purchasePrice:null,offlineStatus:null,freeQty:null,discount:null,markupPerc:null,schemeDiscountPerc:null,action:null,} });

app.factory('vendorPurchaseItemModel', function () { return { id: null, accountId: null, instanceId: null, vendorPurchaseId: null, productName: null, productStockId: null, packageSize: null, packageQty: null, packagePurchasePrice: null, packageSellingPrice: null, packageMRP: null, quantity: null, purchasePrice: null, offlineStatus: null, freeQty: null, discount: null, status: null, vAT: null, vatValue: null, discountValue: null, fromTempId: null, fromDcId: null, isPoItem: null, hsnCode: null, igst: null, cgst: null, sgst: null, gstTotal: null, gstValue: null, markupPerc: null, markupValue: null, schemeDiscountPerc: null, schemeDiscountValue: null, freeQtyTaxValue: null, vendorPurchaseItemSno :null,} });

app.factory('vendorPurchaseModel', function(){return{id:null,accountId:null,instanceId:null,vendorId:null,prefix:null,invoiceNo:null,invoiceDate:null,initialValue:null,verndorOrderId:null,discount:null,credit:null,paymentType:null,chequeNo:null,chequeDate:null,offlineStatus:null,billSeries:null,goodsRcvNo:null,comments:null,fileName:null,creditNoOfDays:null,noteType:null,noteAmount:null,totalPurchaseValue:null,discountValue:null,totalDiscountValue:null,vatValue:null,roundOffValue:null,netValue:null,returnAmount:null,finyear:null,cancelStatus:null,discountType:null,discountInRupees:null,taxRefNo:null,vendorPurchaseFormulaId:null,markupPerc:null,markupValue:null,schemeDiscountValue:null,isApplyTaxFreeQty:null,freeQtyTotalTaxValue:null,} });

app.factory('vendorReturnItemModel', function(){return{id:null,accountId:null,instanceId:null,vendorReturnId:null,productStockId:null,quantity:null,offlineStatus:null,returnPurchasePrice:null,returnedTotal:null,discount:null,discountValue:null,isDeleted:null,igst:null,cgst:null,sgst:null,gstTotal:null,gstAmount:null,} });

app.factory('vendorReturnModel', function(){return{id:null,accountId:null,instanceId:null,vendorPurchaseId:null,vendorId:null,prefix:null,returnNo:null,returnDate:null,offlineStatus:null,reason:null,finyearId:null,paymentStatus:null,paymentRemarks:null,totalReturnedAmount:null,totalDiscountValue:null,totalGstValue:null,roundOffValue:null,taxRefNo:null,isAlongWithPurchase:null,} });

app.factory('voucherModel', function(){return{id:null,accountId:null,instanceId:null,returnId:null,participantId:null,transactionType:null,voucherType:null,originalAmount:null,factor:null,amount:null,returnAmount:null,status:null,offlineStatus:null,} });