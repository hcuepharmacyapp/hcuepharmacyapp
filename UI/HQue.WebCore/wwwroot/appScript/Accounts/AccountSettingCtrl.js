app.controller('accountSettingsCtrl', function ($scope, pettyCashService, productModel, pettyCashSettingsModel, toastr) {

    var product = productModel;
    $scope.search = product;
    $scope.list = [];

    var pettyCashSettings = pettyCashSettingsModel;
    $scope.pettyCashSetting = pettyCashSettings;
    //editInventorySettings();
   
    $scope.inventorySettingUpdate = function () {

        $.LoadingOverlay("show");
        pettyCashService.saveSettings($scope.pettyCashSetting).then(function (response) {
            toastr.success('No of Days updated successfully');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    //function editInventorySettings() {
    //    pettyCashService.editInventorySettings().then(function (response) {
    //        $scope.pettyCashSetting = response.data;

    //    });
    //};

});