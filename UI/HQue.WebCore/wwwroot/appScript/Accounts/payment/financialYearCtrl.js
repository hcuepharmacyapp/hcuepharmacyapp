app.controller('finalcialYearctrl', function ($scope, paymentService, paymentModel, vendorService, vendorPurchaseModel, toastr, vendorModel) {

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

 
    $scope.saveFinYear = function (from, to) {


        $scope.date = {
            'FinYearStartDate': from,
            'FinYearEndDate': to
        }

        paymentService.saveFinancialYear($scope.date).then(function (response) {
            toastr.success('Financial Year Submitted successfully');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };


    getFinyearAvailable();
    $scope.isFinYeaarAvail = false;
    function getFinyearAvailable() {
        paymentService.getFinyearAvailable().then(function (response) {
            console.log(JSON.stringify(response));

            if (response.data == undefined || response.data == null || response.data == "") {
                $scope.isFinYeaarAvail = false;
            } else {
                console.log(response.data.finYearStartDate + "__" + response.data.finYearEndDate);
                $scope.finYearfrom = response.data.finYearStartDate;
                $scope.finYearto = response.data.finYearEndDate;
                $scope.isFinYeaarAvail = true;
            }
         
        }, function () {
           
        });
    }
  
});