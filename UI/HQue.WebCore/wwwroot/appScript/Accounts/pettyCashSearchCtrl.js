app.controller('pettyCashSearchCtrl', function ($scope, $filter, pettyCashService, pettyCashModel, pagerServcie) {

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    var pettyCash = pettyCashModel;

    $scope.search = pettyCash;
    $scope.type = 'ALL';
    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination$filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
    function pageSearch() {
        pettyCashService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    }

    $scope.pettyCashSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        pettyCashService.list($scope.type, data).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $scope.type = "";
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.pettyCashSearch();

    $scope.Datetype = function (type) {
        $scope.type = type;
        $scope.pettyCashSearch();
    };

});