app.controller('customerDetailsReceiptCtrl', function ($scope, customerReceiptService, customerPaymentModel, salesModel, toastr) {

    document.getElementById("enterAmount").focus();

    $scope.focusNextField = function (nextid) {
        document.getElementById(nextid).focus();
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.minDate = new Date();
    $scope.popup1 = {
        "opened": false
    };
    $scope.paymentData = {
        "paymentType": "Cash",
        "amount": null,
        "chequeNo": null,
        "chequeDate": null,
        "cardNo": null,
        "cardDate": null,
        "remarks": ""
    };

    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    var payment = customerPaymentModel;
    payment.sales = salesModel;

    $scope.search = payment;
    $scope.list = [];
    $scope.customerList = [];
    $scope.payment = payment;

    $scope.selectedCustomer = function () {
        $scope.selectedCustomer;
    };

    $scope.customer = function () {
        customerReceiptService.customerData().then(function (response) {
            $scope.customerList = response.data;
        }, function () { toastr.error('Error Occured', 'Error'); });
    };
    $scope.customer();
    $scope.creditAmount = 0;
    $scope.debitAmount = 0;

    //Customer receipt shown by PatientId instead of patient mobile & name and its dependency changes - Settu
    $scope.init = function (patientId) {
        $scope.totalDue = 0;
        $.LoadingOverlay("show");
        $scope.mobile = "";
        $scope.name = "";
        $scope.patientId = patientId;
        customerReceiptService.customerDetails(patientId).then(function (response) {
            $scope.list = response.data;

            $scope.name = $scope.list.name;
            $scope.mobile = $scope.list.mobile;
            $scope.patientId = $scope.list.patientId;
            $scope.status = $scope.list.patient.status;

            for (var i = 0; i < $scope.list.customerPayments.length; i++) {
                $scope.totalDue += ($scope.list.customerPayments[i].credit - $scope.list.customerPayments[i].debit);
            }
            $scope.recalculateDue();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.Disablesubmit = true;
    $scope.totalAlert = '';
    // Following changes added by Settu to adjust sales return credit amount
    $scope.unHoldTotalDue = 0;
    $scope.recalculateDue = function () {
        $scope.unHoldTotalDue = 0;
        for (var i = 0; i < $scope.list.customerPayments.length; i++) {
            if (!$scope.list.customerPayments[i].hold)
            $scope.unHoldTotalDue += ($scope.list.customerPayments[i].credit - $scope.list.customerPayments[i].debit);
        }
        $scope.unHoldTotalDue = parseFloat($scope.unHoldTotalDue.toFixed(2));
        $scope.adjustBalanceAmount(payment);
    }

    $scope.amountReceived = function (payment) {
        if (payment != null && payment != undefined && payment != "")
            payment = $scope.returnTotalAmt + parseFloat(payment);
        else
            payment = $scope.returnTotalAmt;
        var checkedCount = 0;
        if (payment > $scope.totalDue) {
            $scope.totalAlert = 'Amount Exceeded';
        } else if ($scope.returnTotalAmt == 0) {
            $scope.totalAlert = '';
        }
        for (var i = 0; i < $scope.list.customerPayments.length; i++) {
            if (payment > 0 && $scope.list.customerPayments[i].hold != true) {
                $scope.list.customerPayments[i].paymentAmount = $scope.list.customerPayments[i].credit - $scope.list.customerPayments[i].debit;
                if (payment >= $scope.list.customerPayments[i].paymentAmount) {
                    payment = payment - ($scope.list.customerPayments[i].paymentAmount);
                    $scope.list.customerPayments[i].paymentAmount = $scope.list.customerPayments[i].paymentAmount;
                } else {
                    $scope.list.customerPayments[i].paymentAmount = payment;
                    payment = 0;
                }
            } else {
                $scope.list.customerPayments[i].paymentAmount = 0;
                checkedCount++;
                continue;
            }
        }
        if (checkedCount == $scope.list.customerPayments.length) {
            $scope.Disablesubmit = true;
        } else {
            $scope.Disablesubmit = false;
        }
    };
    $scope.showError = {};

    $scope.changePaidAmount = function (index, amount, due) {
        if (amount > due) {
            $scope.showError[index] = true;
        } else {
            $scope.showError[index] = false;
        }
        var total = 0;
        angular.forEach($scope.list.customerPayments, function (data, index) {

            if (data.paymentAmount != "" || data.paymentAmount != null) {
                total += parseFloat(data.paymentAmount);
            }

        });
        $scope.paymentData.amount = total;
    };


    $scope.changefilters = function () {
        $scope.paymentData.cardNo = "";
        $scope.paymentData.cardDate = "";
        $scope.paymentData.chequeNo = "";
        $scope.paymentData.chequeDate = "";
    };


    $scope.customerReceiptSearch = function () {
        $scope.totalDue = 0;
        $.LoadingOverlay("show");
        customerReceiptService.customerDetails($scope.patientId).then(function (response) {
            $scope.list = response.data;
            if ($scope.list.customerPayments.length === 0 && $scope.list.voucherList === 0) {
                $scope.mobile = "";
                $scope.name = "";
                $scope.totalDue = 0;
                $scope.patientId = "";
            }
            for (var i = 0; i < $scope.list.customerPayments.length; i++) {
                $scope.totalDue += $scope.list.customerPayments[i].credit - $scope.list.customerPayments[i].debit;
            }
            $scope.recalculateDue();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };


    $scope.editPayment = function (item, bool) {
        item.isEdit = bool;
    };

    $scope.savePayments = function (payment) {
        $.LoadingOverlay("show");
       
        $scope.payment.remarks = $scope.paymentData.remarks;

        $scope.payment.chequeNo = $scope.paymentData.chequeNo;

        $scope.payment.chequeDate = $scope.paymentData.chequeDate;

        $scope.payment.cardNo = $scope.paymentData.cardNo;

        $scope.payment.cardDate = $scope.paymentData.cardDate;

        $scope.payment.paymentType = $scope.paymentData.paymentType;
        $scope.payment.customerPayments = payment;
        $scope.paymentData = {
            "paymentType": "Cash",
            "amount": null,
            "chequeNo": null,
            "chequeDate": null,
            "cardNo": null,
            "cardDate": null,
            "remarks": ""
        };
        $scope.payment.voucherList = $scope.list.voucherList;
        $scope.payment.settlementsList = setSettlementsList();
        console.log(JSON.stringify($scope.payment));

        if ($scope.patientId != undefined && $scope.status == 3) //&& (parseInt($scope.totalDue) == parseInt($scope.paymentData.amount))
        {
            var x = (parseInt($scope.totalDue) == parseInt(document.getElementById("enterAmount").value));
            //$scope.payment = {
            //    sales: { patientId: '', patientStatus: 0 }
            //}
            $scope.payment.patientId = $scope.patientId;
            $scope.payment.patientStatus = $scope.status;
        }
        customerReceiptService.save($scope.payment).then(function (response) {
            toastr.success('Customer payment made successfully');
            $scope.creditAmount = 0;
            $scope.debitAmount = 0;
            $scope.settleAdjustment = false;
            $scope.customerReceiptSearch();
            document.getElementById("enterAmount").focus();
            $.LoadingOverlay("hide");
        }, function () {
            document.getElementById("enterAmount").focus();
            $.LoadingOverlay("hide");
        });
    };


    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };

    function setSettlementsList() {
        var settlementsList = [];
        var amount = 0;
        var j = 0;
        for (var i = 0; i < $scope.list.voucherList.length; i++) {
            if ($scope.list.voucherList[i].adjustedAmount > 0) {
                amount = $scope.list.voucherList[i].adjustedAmount;
                for (; j < $scope.list.customerPayments.length; j++) {
                    if ($scope.list.customerPayments[j].paymentAmount > 0) {
                        if ($scope.list.customerPayments[j].balanceAmount == undefined)
                            $scope.list.customerPayments[j].balanceAmount = 0;
                        var paymentAmount = $scope.list.customerPayments[j].balanceAmount > 0 ? $scope.list.customerPayments[j].balanceAmount : $scope.list.customerPayments[j].paymentAmount;
                        $scope.list.customerPayments[j].balanceAmount = 0;
                        var balanceAmount = amount - paymentAmount;

                        var obj = {
                            transactionId: $scope.list.customerPayments[j].salesId,
                            adjustedAmount: balanceAmount < 0 ? amount : paymentAmount,
                            voucherId: $scope.list.voucherList[i].id,
                        }
                        settlementsList.push(obj);
                        amount = balanceAmount;
                        if (amount <= 0) {
                            $scope.list.customerPayments[j].balanceAmount = Math.abs(amount);
                            break;
                        }
                    }
                }
            }
        }
        return settlementsList;
    };

    $scope.CheckDueToAdjust = function (index) {
        if ($scope.list.voucherList[index].adjust && $scope.getTotalDue() == $scope.unHoldTotalDue) {
            $scope.list.voucherList[index].adjust = false;
            toastr.info("Total due already adjusted.");
            return;
        }
        else {
            $scope.adjustBalanceAmount();
            if ($scope.list.voucherList[index].adjust && ($scope.list.voucherList[index].adjustedAmount == undefined || $scope.list.voucherList[index].adjustedAmount == 0)) {
                $scope.list.voucherList[index].adjust = false;
                toastr.info("Total due already adjusted with another bill.");
                return;
            }
        }
    };

    $scope.returnTotalAmt = 0;
    $scope.balanceTotalDue = 0;
    $scope.adjustBalanceAmount = function () {
        var paymentManualAmt = 0;
        if ($scope.paymentData.amount != null && $scope.paymentData.amount != undefined && $scope.paymentData.amount != "")
            paymentManualAmt = parseFloat($scope.paymentData.amount);

        $scope.balanceTotalDue = $scope.unHoldTotalDue -paymentManualAmt;
        if ($scope.balanceTotalDue < 0)
            $scope.balanceTotalDue = 0;

        $scope.returnTotalAmt = 0;
        for (var i = 0; i < $scope.list.voucherList.length; i++) {
            if ($scope.list.voucherList[i].adjustedAmount != undefined && $scope.list.voucherList[i].adjustedAmount != null && $scope.list.voucherList[i].adjustedAmount != "") {
                $scope.list.voucherList[i].amount += $scope.list.voucherList[i].adjustedAmount;
                $scope.list.voucherList[i].adjustedAmount = 0;
        }

        if ($scope.list.voucherList[i].adjust && $scope.balanceTotalDue > 0 && $scope.list.voucherList[i].amount != 0) {
            if ($scope.list.voucherList[i].amount <= $scope.balanceTotalDue) {
                    $scope.list.voucherList[i].adjustedAmount = $scope.list.voucherList[i].amount;
                    $scope.balanceTotalDue = $scope.balanceTotalDue -$scope.list.voucherList[i].amount;
                    $scope.list.voucherList[i].amount = 0;
                    $scope.returnTotalAmt += $scope.list.voucherList[i].adjustedAmount;
            }
            else if ($scope.list.voucherList[i].amount > $scope.balanceTotalDue) {
                    $scope.list.voucherList[i].adjustedAmount = $scope.balanceTotalDue;
                    $scope.list.voucherList[i].amount = $scope.list.voucherList[i].amount -$scope.balanceTotalDue;
                    $scope.balanceTotalDue = 0;
                    $scope.returnTotalAmt += $scope.list.voucherList[i].adjustedAmount;
        }
        }
    }
        $scope.amountReceived(paymentManualAmt);
    };

    $scope.getTotalDue = function () {
        if ($scope.paymentData.amount != null && $scope.paymentData.amount != undefined && $scope.paymentData.amount != "")
            return parseFloat((parseFloat($scope.paymentData.amount) + $scope.returnTotalAmt).toFixed(2));
        else
            return parseFloat($scope.returnTotalAmt.toFixed(2));
    };

    $scope.getTotalBalance = function () {
        var balanceTotal = 0;
        if ($scope.list.voucherList == undefined)
            return balanceTotal;

        for (var i = 0; i < $scope.list.voucherList.length; i++) {
            balanceTotal = balanceTotal +$scope.list.voucherList[i].amount;
            if ($scope.list.voucherList[i].adjustedAmount != undefined)
                balanceTotal += $scope.list.voucherList[i].adjustedAmount;
    }
        return balanceTotal;
    };

    $scope.settleAdjustment = false;
    $scope.settleBalanceAmount = function () {
        $scope.settleAdjustment = false;
        for (var i = 0; i < $scope.list.voucherList.length; i++) {
            if ($scope.list.voucherList[i].settle)
                $scope.settleAdjustment = true;
            if ($scope.list.voucherList[i].settle && $scope.list.voucherList[i].amount != 0) {
                $scope.list.voucherList[i].returnAmount = $scope.list.voucherList[i].amount;
                $scope.list.voucherList[i].amount = 0;
            }
            else {
                    if (!$scope.list.voucherList[i].settle && $scope.list.voucherList[i].returnAmount != undefined && $scope.list.voucherList[i].returnAmount != 0) {
                    $scope.list.voucherList[i].amount = $scope.list.voucherList[i].returnAmount;
                    $scope.list.voucherList[i].returnAmount = 0;
            }
        }
    }
    };

    $("#cardDate").keyup(function (e) {
        if ($(this).val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $(this).val($(this).val() + "/");
    }
    });
    //$scope.getTotalPaid = function (customerPayment) {
    //    var amt = customerPayment.invoiceAmount.toFixed(2) - (customerPayment.credit - customerPayment.debit).toFixed(2);
    //    return amt;
    //};
});

app.filter('round', function () {
    return function (input) {
        return Math.round(input);
    };
});