app.factory('customerReceiptService', function ($http) {
    return {
        "list": function (name, mobile) {
            return $http.post('/PaymentData/listData?customername=' + name + '&mobile=' + mobile);
        },
        "save": function (data) {
            return $http.post('/CustomerReceiptData/saveCustomerPayment', data);
        },
        //To get customer receipt data by PatientId instead of patient mobile & name - Settu
        "customerDetails": function (patientId) {
            return $http.get('/PaymentData/CustomerListDetails?PatientId=' + patientId);
        },
        "getCustomerBalance": function (mobile, name) {
            return $http.get('/CustomerReceiptData/GetCustomerBalance?customerMobile=' + mobile + '&customerName=' + name);
        },
        "customerData": function () {
            return $http.get('/PaymentData/CustomerListData');
        },
        "searchCustomer": function (keyword, type) {
            return $http.get('/CustomerReceiptData/searchCustomer?keyword=' + keyword + "&type=" + type);
        },

        "GetCustomerCheque": function (PatientId) {
            return $http.get('/CustomerReceiptData/GetCustomerCheque?PatientId=' + PatientId);
        },
        "updateChequeStatus": function (item) {
            return $http.post("/CustomerReceiptData/updateChequeStatus", item);
        }
    }
});
