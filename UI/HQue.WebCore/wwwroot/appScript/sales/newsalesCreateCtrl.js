app.controller('newsalesCreateCtrl', function ($scope, $rootScope, $location, $window, toastr, newsalesEditHelper, newcustomerHelper,
    printingHelper, shortcutHelper, newsalesService, productStockService, doctorService, cacheService, productModel, productStockModel,
    ModalService, $filter, customerReceiptService, patientService, missedOrderModel, tempVendorPurchaseItemModel, vendorPurchaseService,
    salesModel, pagerServcie, leadsModel, salesReturnModel, salesPaymentModel, $anchorScroll, salesService) {
    var ele = "";
    var currentlyFocusedElement = "";
    shortcutHelper.setScope($scope);
    $scope.minDate = new Date();
    var d = new Date();
    $scope.Enablesalesbtn = false;
    var valqty = false;
    var valdisct = false;
    var selprice = false;
    $scope.TotalAmountGiven = 0;
    $scope.EnablePayBtn = false;
    $scope.valqty = false;
    $scope.valdisct = false;
    $scope.focusbtn = false;
    $scope.selprice = false;
    $scope.edititem = false;
    $scope.isProductGenericSearch = true;
    $scope.deleteAppPopup = false; // Added by arun for delete popup
    $scope.isPopupOpened = false; //added by Durga to check whether any popup opened or not
    var getdeletevalues = [];
    $scope.errLast4digit = false;
    $scope.productBatchDetails = [];
    $scope.open = function () {
        $scope.popup.opened = true;
    };
    $scope.popup = {
        "opened": false
    };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        "opened": false
    };
    //$scope.sale.reminderFrequency = 0;  //Added by arun for displaying the frequency type
    $scope.Iscredit = false;
    //Senthil variables
    $scope.batchList1 = [];
    $scope.totalQty = 0;
    $scope.showQty = false;
    $scope.showBatchGrid = false;
    $scope.selectedWatchId = 1;
    $scope.setIndex = 1;
    $scope.totalDiscount = 0;
    $scope.showPayment = false;
    $scope.focusONMainGD = true;
    $scope.focusedTextid = 1;
    $scope.focusSelectbox = false;
    var salesPrint = salesModel;
    var leads = leadsModel;
    $scope.search = salesPrint;
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //
    //Check offline status
    $scope.IsOffline = false;
    getOfflineStatus = function () {
        newsalesService.getOfflineStatus()
            .then(function (response) {
                if (response.data) {
                    $scope.IsOffline = true;
                }
            });
    };
    getOfflineStatus();
    $scope.selectProducthtml = function (id) {
        $scope.selectedWatchId = 1;
        $scope.focusONMainGD = true;
        $scope.showQty = false;
        $scope.showBatchGrid = false;
        $scope.batchList1.length = 0;
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.remiderOpen = function () {
        $scope.reminderPopup.opened = true;
    };
    $scope.reminderPopup = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };
    $scope.customerHelper = newcustomerHelper;
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.selectedBillDate = new Date();
    $scope.batchList = [];
    $scope.selectedProduct = null;
    $scope.selectedBatch = { "reminderFrequency": "0" };
    $scope.Math = window.Math;
    $scope.selectedDoctor = null;
    $scope.totalBalanceAmount = 0;
    $scope.doctor = {};
    $scope.customerHelper.data.isCustomerDiscount = 0;
    $scope.customerHelper.data.isReset = 0;
    $scope.enableBillPrint = true;
    $scope.printStatus = "Disable";
    $scope.zeroBatch = false;
    //Newly added by Mani on 28-Jan-17
    $scope.dispInvoiceNo = "";
    $scope.sales = getSalesObject();
    //Senthil start
    var productItem = {
        "sno": 1,
        "rowid": 1,
        "schedule": "",
        "id": "",
        "accountId": "",
        "name": "",
        "product": "",
        "vatInPrice": 0,
        "discount": 0,
        "age": 0,
        "totalStock": 0,
        "quantity": 0,
        "soldqty": 0,
        "productId": "",
        "vendorId": "",
        "batchNo": "",
        "expireDate": "",
        "vat": 0,
        "sellingPrice": 0,
        "sellingValue": 0,
        "stock": 0,
        "packageSize": 0,
        "packagePurchasePrice": 0,
        "purchasePrice": 0,
        "offlineStatus": false,
        "cst": 0,
        "reminderFrequency": 'None',
        "reminderDate": "",
        "isReminderAvail": false,
        "availableQty": 0,
        "totalQuantity": 0,
        "selectedBatch": [],
        "isLastRow": true,
        "isQuantityFocused": false,
        "isMrpFocused": false,
        "isDiscountFocused": false,
        "salesReturn": false
    };
    $scope.product = { "items": [] };
    // senthil end
    //   if ($scope.sales.discount > 0 || ($scope.sales.discount == 0 && $scope.sales.discountValue == 0))
    //$scope.sal.discType = "1";
    //else
    $scope.sale = {
        "reminderFrequency": "0",
        "discountType": "1"
    };
    $scope.chkPrintValue = false;
    $scope.getLastSalesPrintType = function () {
        newsalesService.getLastSalesPrintType()
            .then(function (response) {
                $scope.chkPrintValue = response.data;
            }, function () {
            });
    };
    $scope.changeTextBox = function (val, ind, selectedRow) {
        var serachrowID = selectedRow.rowid;
        if (val == 1) {
            if (selectedRow.quantity > 0) {
                if (selectedRow.quantity > selectedRow.totalQuantity) {
                    toastr.error("Exceeds available quantity " + selectedRow.totalQuantity + "");
                    focusedTextid = val;
                    $scope.focusedTextid = val;
                    return false;
                } else {
                    focusedTextid = val + 1;
                    $scope.focusedTextid = val + 1;
                }
            } else {
                return;
            }
        }
        if (val == 2) {
            if ($scope.isSellingPriceNotValid == true) {
                focusedTextid = val;
                $scope.focusedTextid = val;
                return false;
            } else {
                if (selectedRow.discount == 0) {
                    selectedRow.discount = "";
                }
                focusedTextid = val + 1;
                $scope.focusedTextid = val + 1;
            }
        }
        if (val == 3) {

            if (selectedRow.discount == "" || selectedRow.discount == undefined) {
                selectedRow.discount = 0;
            }
            focusSelectbox = false;
            $scope.focusSelectbox = false;
            focusedTextid = val + 1;
            $scope.focusedTextid = val + 1;
        }
        if (val == 4) {
            $scope.isSelectedItemMain++;
            $scope.isCurrentRow = parseInt($scope.isSelectedItemMain) - 1;
            $scope.isLastRow = false;
            focusedTextid = 1;
            $scope.focusedTextid = 1;
            if ((ind + 1) == parseInt($scope.product.items.length)) {
                $scope.isSelectedItemMain = 1;
                $scope.isCurrentRow = 0;
                $scope.isLastRow = true;
                $scope.focusSelectbox = true;
            }
        }
    };
    $scope.keyPressMainGDCtrls = function (e, val, selectedRow) {
        //right arrow
        if (e.keyCode == 39) {
            if (val == 1) {
                if (selectedRow.quantity > 0) {
                    if (selectedRow.quantity > selectedRow.totalQuantity) {
                        toastr.error("Exceeds available quantity " + selectedRow.totalQuantity + "");
                        $scope.isQtyNotValid = true;
                        return;
                    } else {
                        focusedTextid = val + 1;
                        $scope.focusedTextid = val + 1;
                    }
                } else {
                    return;
                }
            }
            if (val == 2) {
                if ($scope.isSellingPriceNotValid == true) {
                    focusedTextid = val;
                    $scope.focusedTextid = val;
                    return false;
                } else {
                    if (selectedRow.discount == 0) {
                        selectedRow.discount = "";
                    }
                    focusedTextid = val + 1;
                    $scope.focusedTextid = val + 1;
                }
            }
        }
        if (e.keyCode == 37) {
            if (val == 2) {
                if ($scope.isSellingPriceNotValid == true) {
                    focusedTextid = val;
                    $scope.focusedTextid = val;
                    return false;
                } else {
                    focusedTextid = val - 1;
                    $scope.focusedTextid = val - 1;
                }
            }
            if (val == 1) {
                $scope.focusedTextid = val;
            }
            if (val == 3) {
                focusedTextid = val - 1;
                $scope.focusedTextid = val - 1;
            }
            //if (val == 5) {
            //    $scope.focusedTextid = 2;
            //}
            //if (val == 6) {
            //    focusSelectbox = true;
            //    $scope.focusSelectbox = true;
            //}
        }
        if (e.keyCode == 40) {
            if (val == 2) {
                if ($scope.isSellingPriceNotValid == true) {
                    focusedTextid = val;
                    $scope.focusedTextid = val;
                    return;
                }
            }
            if (val == 3) {
                if (selectedRow.discount == "") {
                    selectedRow.discount = 0;
                }
            }
            if (selectedRow.quantity == 0 || selectedRow.quantity == undefined) {
                toastr.error("Enter Valid Quantity");
                $scope.isQtyNotValid = true;
                return;
            }
            $scope.isQtyNotValid = false;
            if (selectedRow.quantity > 0) {
                if (selectedRow.quantity > selectedRow.totalQuantity) {
                    toastr.error("Exceeds available quantity " + selectedRow.totalQuantity + "");
                    $scope.isQtyNotValid = true;
                    return;
                } else {
                }
            } else {
                return;
            }
        }
        if (e.keyCode == 38) {
            if (val == 2) {
                if ($scope.isSellingPriceNotValid == true) {

                    focusedTextid = val;
                    $scope.focusedTextid = val;
                    return false;
                }
            }
            if (val == 3) {
                if (selectedRow.discount == "") {
                    selectedRow.discount = 0;
                }
            }
            if (selectedRow.quantity == 0 || selectedRow.quantity == undefined) {
                toastr.error("Enter Valid Quantity");
                $scope.isQtyNotValid = true;
                return;
            }
            $scope.isQtyNotValid = false;
            if (selectedRow.quantity > 0) {
                if (selectedRow.quantity > selectedRow.totalQuantity) {
                    toastr.error("Exceeds available quantity " + selectedRow.totalQuantity + "");
                    $scope.isQtyNotValid = true;
                    return;
                } else {
                }
            } else {
                return;
            }
        }
        focusSelectbox = false;
        $scope.focusSelectbox = false;
    };
    $scope.getLastSalesPrintType();
    $scope.sales.billPrint = $scope.chkPrintValue;
    $rootScope.$on("doSelectPatient", function (data) {
        newcustomerHelper.doSelectPatient();
    });
    $scope.doctorSelected = function () {
        $scope.selectedDoctor;
    };
    $scope.$on("doctorSelected", function (event, args) {
        $scope.sales.doctorMobile = args.value.mobile;
    });
    $scope.$on("update_getValue", function (event, value) {
        $scope.doctor.name = value;
    });
    $scope.leadsProductArray = [];
    //Newly added by Mani on 28-Jan-17 begins
    $scope.trackInvoiceNo = function () {
        if ($scope.InvoiceSeriestype == 1 || $scope.InvoiceSeriestype == "") {
            $scope.dispInvoiceNo = $scope.sales.invoiceNo;
        }
        if ($scope.InvoiceSeriestype == 2) {
            if ($scope.sales.invoiceNo != "" && $scope.sales.invoiceNo != undefined) {
                $scope.dispInvoiceNo = $scope.selectedSeriesItem + "" + $scope.sales.invoiceNo;
            }
        }
        if ($scope.InvoiceSeriestype == 3) {
            $scope.dispInvoiceNo = $scope.manualSeries;
        }
    };
    //Newly added by Mani on 28-Jan-17 ends
    $scope.productnameCookie = "";
    $scope.getProducts = function (val, index) {
        if ($scope.sales.salesItem.length > 4) {
            //  $('ul.dropdown-menu').addClass('productup');
            //if ($('ul.dropdown-menu').hasClass('productup')) {
            //} else {
            //    $('ul.dropdown-menu').addClass('productup');
            //}
            // $('.dropdown-menu').css('top', '0px !important');
            //  $('.dropdown-menu').css('top', 'none');
            //$("#txtDrugName").find("ul").addClass('dropdown-menus');
            //  $('.dropdown-menu').css('bottom', '100%');
            //$location.hash('bottom');
            //$anchorScroll();
            //if ($("#divProdtest").hasClass('sell-product1')) {
            //} else {   
            //    $("#divProdtest").addClass('sell-product1')
            //}
            //  $("#divProdtest").addClass('sell-product1');
        }
        $scope.productnameCookie = val;
        if ($scope.isProductGenericSearch == true) {
            return productStockService.drugFilterData(val).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        } else {
            return productStockService.genericFilterListData(val).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        }
    };
    function setDropdownStyles() {
        if ($scope.product.items.length > 9) {
            $(".dropdown-menu").css({ "bottom": "100%", "top": "" });
            //   $(".sell-product .dropdown-menu").css({ "bottom": "100%", "top": "" });
        }
    }
    $scope.CheckName = function () {
        ele = document.getElementById("txtDrugName");
        if (ele.value == "") {
            $scope.selectedBatch = {
                "reminderFrequency": "0",
                "purchasePrice": 0
            };
        }
    };
    $scope.Namecookie = "";
    $scope.patientCollection = [];
    $scope.selectedCustomer = "";
    $scope.getPatient = function (val) {
        // $scope.customerHelper.data.patientSearchData.name = val;
        $scope.Namecookie = val;
        if ($scope.PatientNameSearch == "" || $scope.PatientNameSearch == undefined) {
            $scope.PatientNameSearch = 2;
        }
        if ($scope.PatientNameSearch == 1) {
            return patientService.GetPatientName(val, $scope.isDepartmentsave)
                .then(function (response) {
                    var origArr = response.data;
                    var newArr = [],
                    origLen = origArr.length,
                    found, x, y;
                    for (x = 0; x < origLen; x++) {
                        found = undefined;
                        for (y = 0; y < newArr.length; y++) {
                            if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            newArr.push(origArr[x]);
                        }
                    }
                    return newArr.map(function (item) {
                        return item;
                    });
                });
        } else {
            return patientService.Patientlist(val, $scope.isDepartmentsave)
                .then(function (response) {
                    var origArr = response.data;
                    var newArr = [],
                   origLen = origArr.length,
                   found, x, y;
                    for (x = 0; x < origLen; x++) {
                        found = undefined;
                        for (y = 0; y < newArr.length; y++) {
                            if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            newArr.push(origArr[x]);
                        }
                    }
                    return newArr.map(function (item) {
                        return item;
                    });
                });
        }
    };
    $scope.getPatientMobile = function (val) {
        $scope.Mobilecookie = val;
        return patientService.GetPatientMobile(val, $scope.isDepartmentsave)
            .then(function (response) {
                var origArr = response.data;
                var newArr = [],
                origLen = origArr.length,
                found, x, y;
                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }
                return newArr.map(function (item) {
                    return item;
                });
            });
    };
    $scope.ShortDoctorsList = function (val) {
        if ($scope.DoctorNameSearch == "" || $scope.DoctorNameSearch == undefined) {
            $scope.DoctorNameSearch = 2;
        }
        if ($scope.DoctorNameSearch == 1) {
            return doctorService.LocalShortlist(val)
                .then(function (response) {
                    //return response.data;
                    var origArr = response.data;
                    var newArr = [],
                    origLen = origArr.length,
                    found, x, y;
                    for (x = 0; x < origLen; x++) {
                        found = undefined;
                        for (y = 0; y < newArr.length; y++) {
                            if ($filter('uppercase')(origArr[x].shortName) === $filter('uppercase')(newArr[y].shortName) && origArr[x].mobile === newArr[y].mobile) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            newArr.push(origArr[x]);
                        }
                    }
                    return newArr.map(function (item) {
                        return item;
                    });
                }, function (error) {
                    console.log(error);
                });
        }
        //else {
        //    return doctorService.list(val).then(function (response) {
        //        var origArr = response.data;
        //        var newArr = [],
        //   origLen = origArr.length,
        //   found, x, y;
        //        for (x = 0; x < origLen; x++) {
        //            found = undefined;
        //            for (y = 0; y < newArr.length; y++) {
        //                if ($filter('uppercase')(origArr[x].shortName) === $filter('uppercase')(newArr[y].shortName) && origArr[x].mobile === newArr[y].mobile) {
        //                    found = true;
        //                    break;
        //                }
        //            }
        //            if (!found) {
        //                newArr.push(origArr[x]);
        //            }
        //        }
        //        return newArr.map(function (item) {
        //            return item;
        //        });
        //    }, function (error) {
        //        console.log(error);
        //    });
        //}
    };
    $scope.changeShortDoctorname = function (name) {
        $scope.shortDoctorname = name;
        if ($scope.sales.salesItem.length > 0) {
            if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
                if ($scope.shortDoctorname == "") {
                    $scope.isdoctornameMandatory = true;
                } else {
                    $scope.isdoctornameMandatory = false;
                }
            }
        }
        if ($scope.shortDoctorname == "") {
            $scope.sales.doctorMobile = "";
        }
    };
    $scope.ShortNamecookie = "";
    //$scope.getShortPatient = function (val) {
    //    $scope.Namecookie = "";
    //    $scope.ShortNamecookie = val;
    //    if ($scope.PatientNameSearch == "" || $scope.PatientNameSearch == undefined) {
    //        $scope.PatientNameSearch = 2;
    //    }
    //    if ($scope.PatientNameSearch == 1) {
    //        return patientService.GetShortPatientName(val)
    //            .then(function (response) {
    //                var origArr = response.data;
    //                var newArr = [], origLen = origArr.length, found, x, y;
    //                for (x = 0; x < origLen; x++) {
    //                    found = undefined;
    //                    for (y = 0; y < newArr.length; y++) {
    //                        if ($filter('uppercase')(origArr[x].shortName) === $filter('uppercase')(newArr[y].shortName) && origArr[x].mobile === newArr[y].mobile) {
    //                            found = true;
    //                            break;
    //                        }
    //                    }
    //                    if (!found) {
    //                        newArr.push(origArr[x]);
    //                    }
    //                }
    //                return newArr.map(function (item) {
    //                    return item;
    //                });
    //            });
    //    } else {
    //        return patientService.Patientlist(val).then(function (response) {
    //            var origArr = response.data;
    //            var newArr = [], origLen = origArr.length, found, x, y;
    //            for (x = 0; x < origLen; x++) {
    //                found = undefined;
    //                for (y = 0; y < newArr.length; y++) {
    //                    if ($filter('uppercase')(origArr[x].shortName) === $filter('uppercase')(newArr[y].shortName) && origArr[x].mobile === newArr[y].mobile) {
    //                        found = true;
    //                        break;
    //                    }
    //                }
    //                if (!found) {
    //                    newArr.push(origArr[x]);
    //                }
    //            }
    //            return newArr.map(function (item) {
    //                return item;
    //            });
    //        });
    //    }
    //};
    //$scope.blurShortCustname = function () {
    //    $scope.customerHelper.data.patientSearchData.shortName = $scope.ShortNamecookie;
    //};
    //$scope.changeShortCustomerName = function () {
    //    ele = document.getElementById("shortCustomerName").value;
    //    if ($scope.sales.salesItem.length > 0) {
    //        if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
    //            if (ele == "") {
    //                $scope.iscustomerNameMandatory = true;
    //            } else {
    //                $scope.iscustomerNameMandatory = false;
    //                $scope.flagCustomername = false;
    //            }
    //        }
    //    }
    //};
    var patientSelected = 0;
    //$scope.onPatientShortSelect = function (obj, event) {
    //    var TABKEY = 9;
    //    if (event.which == TABKEY) {
    //        event.preventDefault();
    //        $scope.customerHelper.data.patientSearchData.shortName = $scope.ShortNamecookie;
    //        $scope.ShortNamecookie = "";
    //        $scope.Namecookie = "";
    //        ele = document.getElementById("searchPatientMobile");
    //        ele.focus();
    //        return false;
    //    } else {
    //        patientSelected = 1;
    //        $scope.customerHelper.data.patientSearchData = obj;
    //        $scope.customerHelper.data.patientSearchData.patientSearchType = $scope.PatientNameSearch;
    //        patientService.list($scope.customerHelper.data.patientSearchData, $scope.isDepartmentsave)
    //            .then(function (response) {
    //                $scope.customerHelper.data.customerList = response.data;
    //                $scope.Namecookie = "";
    //                if ($scope.customerHelper.data.customerList.length > 0) {
    //                    for (var p = 0; p < $scope.customerHelper.data.customerList.length; p++) {
    //                        if ($scope.customerHelper.data.patientSearchData.shortName == $scope.customerHelper.data.customerList[p].shortName) {
    //                            $scope.customerHelper.data.selectedCustomer = $scope.customerHelper.data.customerList[p];
    //                            $scope.customerHelper.data.customerList[p].nameIndex = $scope.customerHelper.data.customerList[p].shortName.substring(0, 1);
    //                        }
    //                    }
    //                    $scope.customerHelper.data.isCustomerSelected = true;
    //                    var drugName = "";
    //                    if ($scope.DoctorNameMandatory == 1) {
    //                        var docname = document.getElementById("doctorName").value;
    //                        if (docname == "") {
    //                            docname = document.getElementById("doctorName");
    //                            docname.focus();
    //                        } else {
    //                            drugName = document.getElementById("txtDrugName");
    //                            drugName.focus();
    //                        }
    //                    } else {
    //                        drugName = document.getElementById("txtDrugName");
    //                        drugName.focus();
    //                    }
    //                    $scope.iscustomerNameMandatory = false;
    //                    $scope.chequeCustomerRequired = false;
    //                    if ($scope.iscashtypevalid == true) {
    //                        $scope.iscashtypevalid = false;
    //                    }
    //                    if ($scope.isdeliverytypevalid == true) {
    //                        $scope.isdeliverytypevalid = false;
    //                    }
    //                    customerReceiptService.getCustomerBalance($scope.customerHelper.data.selectedCustomer.mobile, $scope.customerHelper.data.selectedCustomer.name)
    //                        .then(function (resp) {
    //                            if (resp.data.length == 0) {
    //                                return;
    //                            }
    //                            if (resp.data.credit != undefined && resp.data.debit != undefined) {
    //                                $scope.customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
    //                            }
    //                        });
    //                    if ($scope.customerHelper.data.selectedCustomer != null && $scope.customerHelper.data.selectedCustomer.name != undefined) {
    //                        patientService.getCustomerDiscount($scope.customerHelper.data.selectedCustomer)
    //                            .then(function (response) {
    //                                $scope.customerHelper.data.selectedCustomer.discount = response.data;
    //                                if ($scope.maxDiscountFixed == "Yes") {
    //                                    if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue) {
    //                                        toastr.info("Selected Customer Having more discount than the Fixed Max discount ,So we changed discount percentage to Max discount");
    //                                        //  $scope.customerHelper.data.selectedCustomer.discount = $scope.maxDisountValue;
    //                                    }
    //                                }
    //                            });
    //                    }
    //                }
    //                // $scope.customerHelper.data.patientSearchData.name = $scope.customerHelper.data.selectedCustomer.name;
    //                $.LoadingOverlay("hide");
    //            }, function () {
    //                $.LoadingOverlay("hide");
    //            });
    //    }
    //};
    $scope.finaldelpress = function (e, previousid, nextid, currentid) {
        if (e.keyCode == 37) {
            ele = document.getElementById(previousid);
            ele.focus();
        }
        if (e.keyCode == 39) {
            ele = document.getElementById(nextid);
            ele.focus();
        }
    };
    //$scope.getCustomerBalance = function () {
    //    customerReceiptService.getCustomerBalance(customerHelper.data.selectedCustomer.mobile, customerHelper.data.selectedCustomer.name).then(function (resp) {
    //        if (resp.data.length == 0) {
    //            customerHelper.data.customerBalance = 0;
    //            return;
    //        }
    //        if (resp.data.credit != undefined)
    //            customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
    //    });
    //};
    $scope.onPatientSelect = function (obj, event) {
        var TABKEY = 9;
        if (event !== undefined) {
            if (event.which == TABKEY) {
                event.preventDefault();
                $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
                $scope.Namecookie = "";
                ele = document.getElementById("searchPatientMobile");
                ele.focus();
                return false;
            } else {
                patientSelected = 1;
                $scope.customerHelper.data.patientSearchData = obj;
                $scope.customerHelper.data.patientSearchData.patientSearchType = $scope.PatientNameSearch;
                patientService.list($scope.customerHelper.data.patientSearchData, $scope.isDepartmentsave)
                    .then(function (response) {


                        $scope.customerHelper.data.customerList = response.data;


                        $scope.Namecookie = "";
                        // addded by arun for department calculations
                        if ($scope.customerHelper.data.patientSearchData.patientType == 2) {
                            //document.getElementById('sellingPrice').disabled = true;
                            //document.getElementById('discount').disabled = true;
                            //document.getElementById('txtDiscount').disabled = true;
                        }
                        // ended by arun for department calculations
                        if ($scope.customerHelper.data.customerList.length > 0) {
                            for (var p = 0; p < $scope.customerHelper.data.customerList.length; p++) {
                                if ($scope.customerHelper.data.patientSearchData.name == $scope.customerHelper.data.customerList[p].name) {
                                    $scope.customerHelper.data.selectedCustomer = $scope.customerHelper.data.customerList[p];
                                    $scope.customerHelper.data.customerList[p].nameIndex = $scope.customerHelper.data.customerList[p].name.substring(0, 1);
                                    // $scope.customerHelper.data.patientSearchData.name = $scope.customerHelper.data.selectedCustomer.name;
                                }
                            }
                            $scope.customerHelper.data.isCustomerSelected = true;
                            var drugName = "";
                            if ($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) {
                                var docname = document.getElementById("doctorName").value;
                                if (docname == "") {
                                    docname = document.getElementById("doctorName");
                                    docname.focus();
                                } else {
                                    drugName = document.getElementById("doctorName");
                                    drugName.focus();
                                }
                            } else {
                                drugName = document.getElementById("doctorName");
                                drugName.focus();
                            }
                            $scope.iscustomerNameMandatory = false;
                            $scope.chequeCustomerRequired = false;
                            if ($scope.iscashtypevalid == true) {
                                $scope.iscashtypevalid = false;
                            }
                            if ($scope.isdeliverytypevalid == true) {
                                $scope.isdeliverytypevalid = false;
                            }
                            customerReceiptService.getCustomerBalance($scope.customerHelper.data.selectedCustomer.mobile, $scope.customerHelper.data.selectedCustomer.name)
                                .then(function (resp) {
                                    if (resp.data.length == 0)
                                        return;
                                    if (resp.data.credit != undefined && resp.data.debit != undefined)
                                        $scope.customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
                                });
                            if ($scope.customerHelper.data.selectedCustomer != null && $scope.customerHelper.data.selectedCustomer.name != undefined) {
                                //   $scope.customerHelper.data.patientSearchData.name = $scope.customerHelper.data.selectedCustomer.name;
                                patientService.getCustomerDiscount($scope.customerHelper.data.selectedCustomer)
                                    .then(function (response) {
                                        //   $scope.customerHelper.data.patientSearchData.name = $scope.customerHelper.data.selectedCustomer.name;
                                        $scope.customerHelper.data.selectedCustomer.discount = response.data;
                                        if ($scope.maxDiscountFixed == "Yes") {
                                            if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue) {
                                                toastr.info("Selected Customer Having more discount than the Fixed Max discount ,So we changed discount percentage to Max discount");
                                                //  $scope.customerHelper.data.selectedCustomer.discount = $scope.maxDisountValue;
                                            }
                                        }
                                    });
                            }
                        }
                        //else {
                        //    popupAddNew();
                        //}
                        $.LoadingOverlay("hide");
                    }, function () {
                        $.LoadingOverlay("hide");
                    });
            }
        }
    };
    $scope.onProductSelect = function (selectedProduct) {
        //$scope.productSearchType = "Product";
        $scope.selectedProduct = selectedProduct;
        $.LoadingOverlay("show");
        productStockService.productBatch($scope.selectedProduct.product.id)
            .then(function (responses) {
                var batchListCheck = responses.data;
                var totalQuantityCheck = 0;
                var tempBatchCheck = [];
                var editBatchCheck = null;
                var availableStockCheck = "";
                for (var i = 0; i < batchListCheck.length; i++) {
                    batchListCheck[i].productStockId = batchListCheck[i].id;
                    batchListCheck[i].id = null;
                    availableStockCheck = $scope.getAvailableStock(batchListCheck[i], editBatchCheck);
                    if (editBatchCheck != null && editBatchCheck.productStockId == batchListCheck[i].productStockId) {
                        editBatchCheck.availableQty = availableStockCheck;
                    }
                    totalQuantityCheck += availableStockCheck;
                    if (availableStockCheck == 0) {
                        continue;
                    }
                    batchListCheck[i].availableQty = availableStockCheck;
                    tempBatchCheck.push(batchListCheck[i]);
                }
                if (editBatchCheck != null && tempBatchCheck.length == 0) {
                    availableStockCheck = $scope.getAvailableStock(editBatchCheck, editBatchCheck);
                    editBatchCheck.availableQty = availableStockCheck;
                    totalQuantityCheck = availableStockCheck;
                    tempBatchCheck.push(editBatchCheck);
                }
                batchListCheck = tempBatchCheck;
                $.LoadingOverlay("hide");
                if (batchListCheck.length > 0) {
                    $scope.zeroBatch = false;
                    //if ($scope.batchListType != "") {
                    //if ($scope.batchListType == "Batch") {
                    $scope.ProductDetails();
                    //} else {
                    //    loadBatch(null);
                    //}
                    //}
                } else {
                    $scope.zeroBatch = true;
                }
                //$.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                toastr.error("Error Ocured", "Error");
            });
        ele = document.getElementById("productbatchList");
        if (ele !== null) {
            ele.focus();
        }
    };
    $scope.getBatchListDetail = function () {
        newsalesService.getBatchListDetail()
            .then(function (response1) {
                $scope.batchListType = response1.data;
                //if ($scope.batchListType == "Batch") {
                //    $scope.ProductDetails();
                //} else {
                //    loadBatch(null);
                //}
            }, function (error) {
                console.log(error);
                $scope.batchListType = "";
            });
    };
    $scope.getBatchListDetail();
    $rootScope.$on("tempStockAlertClose", function () {
        var keyPressed = cacheService.get('pressedKey');
        if (parseInt(keyPressed) == 27) {
            $scope.enableTempStockPopup = true;
        } else {
            $scope.enableTempStockPopup = false;
        }
        newsalesService.getBatchListDetail()
            .then(function (response) {
                $scope.batchListType = response.data;
                if ($scope.batchListType == "Batch") {
                    $scope.ProductDetails();
                } else {
                    loadBatch(null);
                }
            }, function () {
            });
    });
    $scope.getAvailableStock = function (productStock, editBatch) {
        var addedItem = getAddedItem(productStock.productStockId);
        var newAddedQty = getAddedItemQuantity(addedItem);
        if (editBatch == null) {
            return productStock.stock - newAddedQty;
        } else if (editBatch.id != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock + parseInt(editBatch.quantity) - newAddedQty;
        } else if (editBatch != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock - (newAddedQty - parseInt(editBatch.quantity));
        } else if (editBatch != null && productStock.productStockId != editBatch.productStockId) {
            return productStock.stock - newAddedQty;
        }
        return productStock.stock;
    };
    function getAddedItemQuantity(addedItems) {
        var qty = 0;
        for (var i = 0; i < addedItems.length; i++) {
            if (addedItems[i].id != null) {
                qty += parseInt(addedItems[i].quantity) - parseInt(addedItems[i].orginalQty);
            } else {
                qty += parseInt(addedItems[i].quantity);
            }
        }
        return qty;
    }
    function getAddedItem(id) {
        var addedItems = [];
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ($scope.sales.salesItem[i].productStockId == id) {
                addedItems.push($scope.sales.salesItem[i]);
            }
        }
        return addedItems;
    }
    function getAddedItems(id, sellingPrice, discount) {
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if (id != null && $scope.sales.salesItem[i].productStockId == id && $scope.sales.salesItem[i].sellingPrice == sellingPrice && $scope.sales.salesItem[i].discount == discount) {
                return $scope.sales.salesItem[i];
            }
        }
        return null;
    }
    $scope.editId = 1;
    $scope.validateDiscount = function (discount) {
        if (parseFloat(discount) > 100) {
            $scope.salesItems.$valid = true;
        } else {
            $scope.salesItems.$valid = false;
        }
    };
    $scope.Quantityexceeds = 0;
    $scope.validateQty = function () {
        if ($scope.selectedBatch == null) {
            return;
        }
        $scope.Quantityexceeds = 0;
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.totalQuantity && $scope.selectedBatch.editId == null) {
            $scope.Quantityexceeds = 1;
        }
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty && $scope.selectedBatch.editId != null) {
            $scope.Quantityexceeds = 1;
        }
        var qty = document.getElementById("quantity");
        if (qty.value == "" || qty.value == 0) {
            $scope.salesItems.$valid = false;
            $scope.Quantityexceeds = 0;
        } else {
            //if ((qty.value > $scope.selectedBatch.totalQuantity && $scope.selectedBatch.editId == null) ||
            //    (qty.value > $scope.selectedBatch.availableQty && $scope.selectedBatch.editId != null)) {
            //    $scope.IsFormInvalid = false;
            //    $scope.salesItems.$valid = true;
            //    if ($scope.Quantityexceeds == 1) {
            //        $scope.IsFormInvalid = false;
            //        $scope.salesItems.$valid = true;
            //    } else {
            //        $scope.salesItems.$valid = false;
            //        $scope.IsFormInvalid = true;
            //    }
            //}
            //else {
            //    if (typeof ($scope.selectedBatch.discount) == "string") {
            //        //if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || $scope.selectedBatch.discount == "" || $scope.selectedBatch.discount == undefined || parseFloat($scope.selectedBatch.discount) > 100) {
            //        if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || parseFloat($scope.selectedBatch.discount) > 100) {
            //            $scope.salesItems.$valid = false;
            //            //var sell = document.getElementById("sellingPrice");
            //            //sell.focus();
            //            $scope.IsFormInvalid = true;
            //        } else {
            //            $scope.IsFormInvalid = false;
            //            $scope.salesItems.$valid = true;
            //        }
            //    }
            //    else {
            //        if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || parseFloat($scope.selectedBatch.discount) > 100) {
            //            $scope.salesItems.$valid = false;
            //            //var sell = document.getElementById("sellingPrice");
            //            //sell.focus();
            //            $scope.IsFormInvalid = true;
            //        } else {
            //            $scope.IsFormInvalid = false;
            //            $scope.salesItems.$valid = true;
            //        }
            //    }
            //    $scope.Quantityexceeds = 0;
            var dis = "";
            if ($scope.Quantityexceeds == 1) {
                $scope.IsFormInvalid = false;
                $scope.salesItems.$valid = true;
                dis = document.getElementById("quantity");
                dis.focus();
                return false;
            } else {
                $scope.salesItems.$valid = false;
                $scope.IsFormInvalid = true;
                dis = document.getElementById("discount");
                dis.focus();
            }
            $scope.Quantityexceeds = 0;
            // }
        }
        //if (!$scope.IsFormInvalid && $scope.salesItems.$valid) {
        //    var dis = document.getElementById("discount");
        //    dis.focus();
        //}
    };
    //Newly Added by DURGA on 09-06-2017
    $scope.zeroQuantityError = false;
    $scope.changeQtyinpopUp = function (val) {
        $scope.zeroQuantityError = false;
        if (val == 0) {
            $scope.zeroQuantityError = true;
        }
    };
    $scope.EditPriceValue = function (selectedRow) {
        var serachrowID = selectedRow.rowid;
        var oldSellingprice = 0;
        var filterrow = $filter('filter')($scope.sales.salesItem, { rowid: serachrowID });
        var index = $scope.sales.salesItem.indexOf(filterrow, 1);
        if (index > -1) {
            oldSellingprice = $scope.sales.salesItem[index].sellingPrice;
        }
        if (selectedRow.quantity != undefined || selectedRow.quantity != 0) {
            if (($scope.batchList1.length > 0) && (selectedRow.sellingPrice == "." || selectedRow.purchasePrice > selectedRow.sellingPrice || selectedRow.sellingPrice == undefined)) {
                alert("Price must be greater than Purchase Price");
                selectedRow.sellingPrice = oldSellingprice;
                return;
            } else {
                if (selectedRow.quantity > 0) {
                    for (var k = 0; k < $scope.sales.salesItem.length; k++) {
                        var aa = $scope.sales.salesItem[k].rowid;
                        var bb, cc = "";
                        if (serachrowID > 1) {
                            cc = serachrowID;
                            bb = serachrowID - 1;
                            $scope.sales.salesItem[bb].sellingValue = selectedRow.sellingValue;
                        } else {
                            bb = serachrowID - 1;
                            cc = serachrowID;
                            $scope.sales.salesItem[bb].sellingValue = selectedRow.sellingValue;
                        }
                        if (cc == aa) {
                            $scope.BillCalculations();
                            getDiscountRules();
                        }
                    }
                }
            }
        } else {
            selectedRow.sellingPrice = oldSellingprice;
        }
    };
    $scope.editQuantity = function (selectedRow, ind) {
        var serachrowID = selectedRow.rowid;
        var filterrow = $filter('filter')($scope.sales.salesItem, { rowid: serachrowID });
        var oldQty = 0;
        var index = $scope.sales.salesItem.indexOf(filterrow);
        //if (selectedRow.quantity==undefined) {
        //    selectedRow.quantity = 0;
        //}
        if (selectedRow.quantity == 0 || selectedRow.quantity == undefined) {
            toastr.error("Enter Valid Quantity");
            //  return false;
        }
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            var aa = $scope.sales.salesItem[k].rowid;
            var cc, bb = "";
            if (serachrowID > 1) {
                cc = serachrowID;
                bb = serachrowID - 1;
                $scope.sales.salesItem[bb].quantity = selectedRow.quantity;
            } else {
                bb = serachrowID - 1;
                cc = serachrowID;
                $scope.sales.salesItem[bb].quantity = selectedRow.quantity;
            }
            if (cc == aa) {
                oldQty = $scope.sales.salesItem[k].quantity;
                if (selectedRow.quantity > selectedRow.totalQuantity) {
                    toastr.error("Exceeds available quantity " + selectedRow.totalQuantity + "");
                    return false;
                }
                $scope.BillCalculations();
                getDiscountRules();
            }
        }
    };
    $scope.editDiscount = function (selectedRow) {
        var serachrowID = selectedRow.rowid;
        var oldDiscount = 0;
        var bb = 0;
        var cc = 0;
        // var filterrow = $filter('filter')($scope.sales.salesItem, { rowid: serachrowID });

        oldDiscount = $scope.sales.salesItem[serachrowID - 1].discount;

        //var index = $scope.sales.salesItem.indexOf(filterrow, 1);
        //if (index > -1) {
        //    oldDiscount = $scope.sales.salesItem[index].discount;
        //}
        // if (($scope.batchList1.length > 0)) {
        if ((selectedRow.sellingValue == undefined || selectedRow.sellingValue == "" || (parseFloat(selectedRow.discount) > 100))) {

            toastr.error("Discount should not more than 100%");
            selectedRow.discount = oldDiscount;
            // return;
        }
        //  }
        var disclength = 0;
        //  if (selectedRow.discount > 0) {
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            var aa = $scope.sales.salesItem[k].rowid;
            if (serachrowID > 1) {
                cc = serachrowID;
                bb = serachrowID - 1;
                $scope.sales.salesItem[bb].discount = selectedRow.discount;
            } else {
                bb = serachrowID - 1;
                cc = serachrowID;
                $scope.sales.salesItem[bb].discount = selectedRow.discount;
            }
            if (cc == aa) {
                $scope.BillCalculations();
                getDiscountRules();
            }
            if ($scope.sales.salesItem[k].discount == 0 || $scope.sales.salesItem[k].discount == undefined) {
                disclength++;
            }
        }
        // }
        if (disclength == $scope.sales.salesItem.length) {
            $scope.discountInValid = false;
        } else {
            $scope.discountInValid = true;
        }
        if (index > -1) {
            $scope.sales.salesItem[index].discount = selectedRow.discount;
            $scope.BillCalculations();
            getDiscountRules();
        }
    };
    $scope.hasCustomerData = function () {
        var hasCustomer = false;
        if ($scope.customerHelper.data.selectedCustomer.name == null || $scope.customerHelper.data.selectedCustomer.mobile == null || $scope.customerHelper.data.selectedCustomer.id == null) {
            hasCustomer = true;
        } else {
            hasCustomer = false;
        }
        return hasCustomer;
    };
    $scope.MissedOrderOpened = false;
    $scope.MissedOrder = function () {
        $scope.MissedOrderOpened = true;
        var m = ModalService.showModal({
            "controller": "newmissedOrderCreateCtrl",
            "templateUrl": 'missedOrders',
            "inputs": {
                "patientId": $scope.customerHelper.data.selectedCustomer.id,
                "patientPhone": $scope.customerHelper.data.selectedCustomer.mobile,
                "patientName": $scope.customerHelper.data.selectedCustomer.name,
                "patientAge": $scope.customerHelper.data.selectedCustomer.age,
                "patientGender": $scope.customerHelper.data.selectedCustomer.gender
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.MissedOrderOpened = false;
                $scope.message = "You said " + result;
                if (result == 'No' || result == 'Cancel') {
                    $scope.FocusonProduct();
                }
            });
        });
    };
    //$scope.ProductDetails = function () {
    //    $scope.enablePopup = true;
    //    cacheService.set('selectedProduct1', $scope.selectedProduct)
    //    var m = ModalService.showModal({
    //        controller: "productDetailSearchCtrl",
    //        templateUrl: 'productDetails'
    //        , inputs: {
    //            productId: $scope.selectedProduct.product.id,
    //            productName: $scope.selectedProduct.name,
    //            items: $scope.sales.salesItem,
    //            enableWindow: $scope.enablePopup
    //        }
    //    }).then(function (modal) {
    //        modal.element.modal();
    //        modal.close.then(function (result) {
    //            $scope.message = "You said " + result;
    //        });
    //    });
    //    return false;
    //};
    $scope.ProductDetails = function () {
        $.LoadingOverlay("show");
        productStockService.productBatch($scope.selectedProduct.product.id)
            .then(function (response) {
                var batchList = response.data;
                var totalQuantity = 0;
                $scope.totalQty = 0;
                var rack = "";
                var tempBatch = [];
                $scope.changediscountsale("");
                var editBatch = null;
                var availableStock = "";
                for (var i = 0; i < batchList.length; i++) {
                    batchList[i].productStockId = batchList[i].id;
                    batchList[i].id = null;
                    availableStock = $scope.getAvailableStock(batchList[i], editBatch);
                    if (editBatch != null && editBatch.productStockId == batchList[i].productStockId) {
                        editBatch.availableQty = availableStock;
                    }
                    totalQuantity += availableStock;
                    if (availableStock == 0) {
                        continue;
                    }
                    batchList[i].availableQty = availableStock;
                    tempBatch.push(batchList[i]);
                }
                if (editBatch != null && tempBatch.length == 0) {
                    availableStock = $scope.getAvailableStock(editBatch, editBatch);
                    editBatch.availableQty = availableStock;
                    totalQuantity = availableStock;
                    tempBatch.push(editBatch);
                }
                $scope.totalQty = totalQuantity;
                batchList = tempBatch;
                if (batchList.length > 0) {
                    $scope.zeroBatch = false;
                    $scope.enablePopup = true;
                    $scope.batchList1 = batchList;
                    totalQuantity = 0;
                    rack = "";
                    tempBatch = [];
                    editBatch = null;
                    for (var i = 0; i < $scope.batchList1.length; i++) {
                        availableStock = $scope.getAvailableStock($scope.batchList1[i], editBatch);
                        if (editBatch != null && editBatch.productStockId == $scope.batchList1[i].productStockId) {
                            editBatch.availableQty = availableStock;
                        }
                        totalQuantity += availableStock;
                        if (availableStock == 0) {
                            continue;
                        }
                        $scope.batchList1[i].availableQty = availableStock;
                        if ($scope.batchList1[i].product.rackNo == "" || $scope.batchList1[i].product.rackNo == undefined) {
                            $scope.batchList1[i].product.rackNo = "-";
                        }
                        if ($scope.batchList1[i].product.boxNo == "" || $scope.batchList1[i].product.boxNo == undefined) {
                            $scope.batchList1[i].product.boxNo = "-";
                        }
                        $scope.batchList1[i].product.rackBox = $scope.batchList1[i].product.rackNo + " / " + $scope.batchList1[i].product.boxNo;
                        tempBatch.push($scope.batchList1[i]);
                    }
                    if (editBatch != null && tempBatch.length == 0) {
                        availableStock = $scope.getAvailableStock(editBatch, editBatch);
                        editBatch.availableQty = availableStock;
                        totalQuantity = availableStock;
                        tempBatch.push(editBatch);
                    }
                    $scope.batchList1 = tempBatch;
                    $scope.totalQty = totalQuantity;
                    $scope.showBatchGrid = true;
                    $scope.productBatchDetails = $scope.batchList1;
                    $scope.isSelectedItem = 1;
                    $scope.shouldBeOpen = true;
                    $('.batch-popup').animate({ scrollTop: (0) }, 'slow');
                    $scope.isPopupOpened = true;
                } else {
                    $scope.zeroBatch = true;
                }
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
            });
    };
    $scope.isSelectedItem = 1;
    $scope.selectRow = function (val) {
        // $scope.isSelectedItem = ($scope.isSelectedItem == val) ? null : val;
        $scope.isSelectedItem = ($scope.isSelectedItem == null) ? 1 : val;
        $scope.enterBatchdetails($scope.batchList1[$scope.isSelectedItem - 1]);
    };
    $scope.keyPressSection = function (e) {
        if (e.keyCode == 113) { //f2
            $scope.isLastRow = false;
            $scope.focusONMainGD = true;
            $scope.showQty = false;
            $scope.showBatchGrid = false;
            $scope.batchList1.length = 0;
            $scope.FocusonProduct();
        }
        if (e.keyCode == 115) { //f3
            $scope.FocusonProduct();
        }
    };
    $scope.keyPressGD1 = function (e) {
        //Batch Arrow Up
        if (e.keyCode == 38) {
            if ($scope.isSelectedItem == 1) {
                e.preventDefault();
                return;
            }
            $scope.isSelectedItem--;
            document.getElementById('productbatchList11').scrollTop -= 20;
        }
        //Batch Arrow Down
        if (e.keyCode == 40) {
            if ($scope.isSelectedItem == $scope.batchList1.length) {
                e.preventDefault();
                return;
            }
            $scope.isSelectedItem++;
            document.getElementById('productbatchList11').scrollTop += 20;
        }
        if (e.keyCode == 113) { //f2
            $scope.isLastRow = false;
            $scope.focusONMainGD = true;
            $scope.showQty = false;
            $scope.showBatchGrid = false;
            $scope.batchList1.length = 0;
            var index = parseInt($scope.product.items.length) - 1;
            var element = document.getElementById("divmain");
            var elementToFocus = angular.element(element.children[index]).find('input')[0];
            if (angular.isDefined(elementToFocus)) {
                elementToFocus.focus();
            }
        }
        if (e.keyCode == 115) { //f3
            $scope.FocusonProduct();
        }
        if (e.keyCode == 13) {
            $scope.enterBatchdetails($scope.batchList1[$scope.isSelectedItem - 1]);
        }
        //if (e.keyCode == 27) {
        //    $scope.showQty = false;
        //    $scope.showBatchGrid = false;
        //}
    };
    $scope.isQtyNotValid = false;
    $scope.keyPressGDmain = function (e) {
        var frmItem = angular.element(window.document.activeElement).attr('name');
        var prodname = "";
        if (e.keyCode == 8) {
            if (frmItem == 'drugName') {
                prodname = document.getElementById("txtDrugName").value;
                if (prodname.length == 1 || prodname == "") {
                    $scope.productnameCookie = "";
                }
            }
        }
        if ($scope.isQtyNotValid == false) {
            if ($scope.isSellingPriceNotValid == true) {


                toastr.error("Required");

            } else {
                //Up Arrow
                if (e.keyCode == 38) {
                    if (frmItem == 'drugName') {
                        prodname = document.getElementById("txtDrugName").value;
                        if ($scope.productnameCookie == "") {
                        } else {
                            return;
                        }
                    }
                    $scope.isLastRow = false;
                    if ($scope.isSelectedItemMain == 1) {
                        $scope.isSelectedItemMain = ($scope.product.items.length) + 1;
                    }
                    $scope.isSelectedItemMain--;
                    $scope.isCurrentRow = parseInt($scope.isSelectedItemMain) - 1;
                    if (parseInt($scope.isSelectedItemMain) == 1) {
                        $scope.isLastRow = true;
                    }
                    if (parseInt($scope.isSelectedItemMain) == parseInt($scope.product.items.length)) {
                        $scope.isLastRow = false;
                    }
                    //if ($scope.isSelectedItemMain == 1) {
                    //    $scope.isSelectedItemMain = ($scope.product.items.length) + 1;
                    //}
                    //$scope.isSelectedItemMain--;
                    //$scope.isCurrentRow = parseInt($scope.isSelectedItemMain) - 1;
                    //if (parseInt($scope.isSelectedItemMain) == parseInt($scope.product.items.length)) {
                    //    $scope.isLastRow = true;
                    //} else {
                    //    $scope.isLastRow = false;
                    //}
                    e.preventDefault();
                }
                //down arrow
                if (e.keyCode == 40) {
                    if (frmItem == 'drugName') {
                        if ($scope.productnameCookie == "") {
                        } else {
                            return;
                        }
                    }
                    $scope.isCurrentRow = $scope.isSelectedItemMain;
                    $scope.isLastRow = false;
                    if ($scope.isSelectedItemMain == $scope.product.items.length) {
                        $scope.isSelectedItemMain = 0;
                        $scope.isCurrentRow = 0;
                        $scope.isLastRow = true;
                    }
                    $scope.isSelectedItemMain++;
                    //if (parseInt($scope.isSelectedItemMain) == parseInt($scope.product.items.length)+1) {
                    //    $scope.isLastRow = true;
                    //}
                    //if ($scope.isSelectedItemMain == $scope.product.items.length) {
                    //    // return;
                    //    $scope.isSelectedItemMain = 0;
                    //}
                    //$scope.isSelectedItemMain++;
                    //$scope.isCurrentRow = parseInt($scope.isSelectedItemMain) - 1;
                    //if (parseInt($scope.isSelectedItemMain) == parseInt($scope.product.items.length)) {
                    //    $scope.isLastRow = true;
                    //} else {
                    //    $scope.isLastRow = false;
                    //}
                    e.preventDefault();
                }
            }
        }
        if (e.keyCode == 39) {
            event.preventDefault();
            if (frmItem == 'drugName') {
                var number = parseFloat($scope.isSelectedItemMain) + 1;
                ele = document.getElementById("txtQty" + number);
                ele.focus();
                return;
            }
        }
        //f2
        if (e.keyCode == 113) {
            $scope.isLastRow = false;
            $scope.focusONMainGD = true;
            $scope.showQty = false;
            $scope.showBatchGrid = false;
            $scope.batchList1.length = 0;
            $scope.FocusonProduct();
        }
        //f3
        if (e.keyCode == 115) {
            $scope.FocusonProduct();
        }
        //f4
        if (e.keyCode == 115) {
            $scope.FocusonProduct();
        }
        //delete
        if (e.keyCode == 46) {
            currentlyFocusedElement = angular.element(document.activeElement)[0];
            $scope.isLastRow = false;
            var i = parseInt($scope.isSelectedItemMain) - 1;
            if ($scope.product.items.length > 1) {
                getdeletevalues = $scope.product.items[i];
                if (getdeletevalues.name == "") {
                    $scope.deleteAppPopup = false;
                    toastr.error("You can't delete this default row");
                    return;
                } else {
                    var myEl = angular.element(document.querySelector("#btnrmvrow"));
                    myEl.addClass('active');
                    event.preventDefault();
                    $scope.deleteAppPopup = true;
                }
            } else {
                toastr.error("You can't delete this default row");
                return;
            }
        }
        // new F9 for add customer focus. // existing f9 for selling price changed by arun on11.05.2017
        if (e.keyCode == 120) {
            event.preventDefault();
            document.getElementById("btnAddcust").focus();  //sellingPrice
        }
        //if (e.keyCode == 65 && e.ctrlKey == true) {
        //    $scope.isLastRow = true;
        //    $scope.isCurrentRow = parseInt($scope.isSelectedItemMain) - 1
        //    event.preventDefault();
        //}
    };
    $scope.removeSalesByIcon = function (sale) {
        getdeletevalues = sale;
        ele = angular.element(document.querySelector("#btnrmvrow"));
        ele.addClass('active');
        $scope.deleteAppPopup = true;
    };
    $scope.FocusonProduct = function () {
        var index = parseInt($scope.product.items.length) - 1;
        var element = document.getElementById("divmain");
        var elementToFocus = angular.element(element.children[0]).find('input')[0];
        if (angular.isDefined(elementToFocus)) {
            elementToFocus.focus();
            $scope.isLastRow = true;
        }
    };
    $rootScope.$on("productDetail", function (data) {
        $scope.productSelection();
    });
    $rootScope.$on("productDetailCancel", function (data) {
        var prod = document.getElementById("txtDrugName");
        prod.focus();
    });
    $rootScope.$on("ResetCustomerdetails", function () {
        var custname = document.getElementById("customerName").value;
        patientSelected = 0;
        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                $scope.iscustomerNameMandatory = true;
            } else {
                $scope.iscustomerNameMandatory = false;
                $scope.flagCustomername = true;
            }
        }
        if ($scope.sales.credit == null || $scope.sales.credit == undefined) {
            $scope.sales.credit = "";
        }
        if ($scope.sales.cashType == 'Credit' && $scope.sales.credit == "") {
            $scope.iscashtypevalid = true;
        }
        if ($scope.sales.deliveryType == 'Home Delivery') {
            $scope.isdeliverytypevalid == true;
        }
        document.getElementById('sellingPrice').disabled = false;
        document.getElementById('discount').disabled = false;
        document.getElementById('txtDiscount').disabled = false;
        $scope.sales.pendingAmountChecked = false;
        $scope.sales.credit -= parseFloat($scope.customerHelper.data.customerBalance);
        $scope.customerHelper.data.customerBalance = 0;
    });
    $rootScope.$on("CheckCustomerdetails", function (event, args) {
        $scope.product.items = $scope.sales.salesItem;
        if ($scope.product.items.length > 0) {
            $scope.product.items.push(angular.copy(productItem));
        }
        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                if (args.message == "false") {
                    $scope.iscustomerNameMandatory = true;
                } else {
                    $scope.iscustomerNameMandatory = false;
                }
            }
        }
        if ($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) {
            var custname = document.getElementById("customerName").value;
            var docname = document.getElementById("doctorName").value;
            if (custname == "") {
                var customername = document.getElementById("customerName");
                customername.focus();
            } else if (docname == "") {
                docname = document.getElementById("doctorName");
                docname.focus();
            }
        } else {
            var drugName = document.getElementById("doctorName");
            if (drugName !== null && drugName !== undefined) {
                drugName.focus();
            }
        }
        if (newcustomerHelper.data.selectedCustomer != null) {
            if ($scope.chequeCustomerRequired) {
                $scope.chequeCustomerRequired = false;
            }
        }
        if ($scope.iscashtypevalid == true) {
            $scope.iscashtypevalid = false;
        }
        if ($scope.isdeliverytypevalid == true) {
            $scope.isdeliverytypevalid = false;
        }
        if (customerHelper.data.selectedCustomer.patientType == 2) {
            document.getElementById('sellingPrice').disabled = true;
            document.getElementById('discount').disabled = true;
            document.getElementById('txtDiscount').disabled = true;
        }
    });
    $scope.LeadProductIndex = "";
    $scope.LeadProductId = "";
    $scope.LeadproductSelection = function (bool, ind, id) {
        var product1 = cacheService.get('selectedLeadProduct');
        $scope.selectedProduct = product1;
        $scope.LeadsProductSelected = bool;
        $scope.LeadProductIndex = ind;
        $scope.LeadProductId = id;
        $scope.onProductSelect();
        //if (product1 != null) {
        //    $scope.selectedBatch = product1;
        //}
        //if (blist.length > 0) {
        //    $scope.batchList = blist;
        //}
        //dayDiff($scope.selectedBatch.expireDate);
        //$scope.selectedBatch.totalQuantity = product1.totalQuantity;
        //var qty = document.getElementById("quantity");
        //qty.focus();
    };
    $scope.enterBatchdetails = function (selectedProduct) {
        selectedProduct.totalQuantity = $scope.totalQty;
        $scope.selectedPrd = selectedProduct;
        $scope.batchLst = $scope.batchList1;
        $scope.productSelection();
        $scope.isSelectedItem = 1;
    };
    $scope.productSelection = function () {
        var product1 = $scope.selectedPrd;
        var blist = $scope.batchLst;
        //$scope.enablePopup = cacheService.get('enableWindow1');
        if (product1 != null) {
            $scope.selectedBatch = product1;
        }
        // added by arun for department calculations
        for (var i = 0; i < $scope.customerHelper.data.customerList.length; i++) {
            if ($scope.customerHelper.data.patientSearchData.mobile == $scope.customerHelper.data.customerList[i].mobile) {
                if ($scope.customerHelper.data.selectedCustomer.name == $scope.customerHelper.data.customerList[i].name) {
                    if ($scope.customerHelper.data.customerList[i].patientType == 2) {
                        //$scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.vat / 100))) * 1.1).toFixed(2);

                        $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.gstTotal / 100))) * 1.1).toFixed(2);

                    }
                }
            }
        }
        // ended by arun
        if (blist.length > 0) {
            $scope.batchList = blist;
        }
        dayDiff($scope.selectedBatch.expireDate);
        $scope.selectedBatch.name = $scope.selectedProduct.product.name;
        if ($scope.selectedBatch.discount == 0) {
            $scope.selectedBatch.discount = "";
        }
        $scope.selectedBatch.quantity = LeadsRequiredquantity;
        $scope.selectedBatch.totalQuantity = product1.totalQuantity;
        $scope.focusONMainGD = false;
        $scope.showQty = true;
        $scope.showBatchGrid = false;
        ele = document.getElementById("quantity");
        ele.focus();
    };
    //    $scope.addDetail($scope.selectedBatch);
    //};
    //$scope.addDetail = function (val) {
    //    $scope.submit($scope.batchList1[val - 1]);
    //};
    //$scope.submit = function (selectedProduct) {
    //  //  selectedProduct.totalQuantity = totalQuantity;
    //    $scope.close("Yes");
    //    $.LoadingOverlay("show");
    //    cacheService.set('selectedProduct1', selectedProduct);
    //    cacheService.set('batchList1', $scope.batchList1);
    //    $rootScope.$emit("productDetail", selectedProduct);
    //    $.LoadingOverlay("hide");
    //};
    $scope.productSearchType = "";
    $scope.PopupAlternatesOpened = false;
    $scope.OpenPopupAlternates = function (selectedProduct) {
        $scope.selectedProduct = selectedProduct;
        $scope.productSearchType = "Generic";
        $scope.PopupAlternates();
    };
    $scope.PopupAlternates = function () {
        $scope.IsFormInvalid = false;
        $scope.salesItems.$valid = true;
        cacheService.set('selectedAlternate1', $scope.selectedProduct);
        $scope.PopupAlternatesOpened = true;
        $scope.isPopupOpened = true;
        var m = ModalService.showModal({
            "controller": "newalternateProductSearchCtrl",
            "templateUrl": 'searchProduct',
            "inputs": {
                "selectedProduct": $scope.selectedProduct != null ? $scope.selectedProduct : null,
                "selectedProductId": ($scope.selectedProduct != null && $scope.selectedProduct.product != null) ? $scope.selectedProduct.product.id : null,
                "productSearchType": $scope.productSearchType
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
                $scope.PopupAlternatesOpened = false;
                if (result == 'No' || result == 'Cancel') {
                    $scope.isPopupOpened = false;
                    $scope.FocusonProduct();
                }
            });
        });
    };
    $scope.TempStockOpened = false;
    $scope.showTempStockScreen = function () {
        $scope.TempStockOpened = true;
        cacheService.set('tempStockSection', $scope.selectedProduct);
        var m = ModalService.showModal({
            "controller": "tempStockCtrl",
            "templateUrl": 'tempStock',
            inputs: {
                "screenType": "new",
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.TempStockOpened = false;
                $scope.message = "You said " + result;
                if ($scope.zeroBatch) {
                    $scope.zeroBatch = false;
                }
                if (result == 'No' || result == 'Cancel') {
                    $scope.FocusonProduct();
                }
            });
        });
    };
    $rootScope.$on("alternateSelection", function (data) {
        $scope.alternateSelection();
    });
    $rootScope.$on("tempStockSelection", function (data) {
        $scope.tempStockSelection();
    });
    $scope.tempStockSelection = function () {
        var tempStockValues = cacheService.get('tempStockValues');
        if ($scope.zeroBatch) {
            $scope.zeroBatch = false;
        }
        if (tempStockValues != null) {
            // $scope.selectedProduct = tempStockValues.productStock.product.name;
            $scope.selectedProduct = tempStockValues.productStock;
            $scope.selectedProduct.product.id = tempStockValues.productStock.productId;
            $scope.selectedProduct.product.name = tempStockValues.productStock.name;
            var editBatch = null;
            productStockService.productBatch(tempStockValues.productStock.productId)
                .then(function (responses) {
                    var batchListCheck = responses.data;
                    var totalQuantityCheck = 0;
                    var tempBatchCheck = [];
                    var editBatchCheck = null;
                    var availableStockCheck = "";
                    for (var i = 0; i < batchListCheck.length; i++) {
                        batchListCheck[i].productStockId = batchListCheck[i].id;
                        batchListCheck[i].id = null;
                        availableStockCheck = $scope.getAvailableStock(batchListCheck[i], editBatchCheck);
                        if (editBatchCheck != null && editBatchCheck.productStockId == batchListCheck[i].productStockId) {
                            editBatchCheck.availableQty = availableStockCheck;
                        }
                        totalQuantityCheck += availableStockCheck;
                        if (availableStockCheck == 0) {
                            continue;
                        }
                        batchListCheck[i].availableQty = availableStockCheck;
                        tempBatchCheck.push(batchListCheck[i]);
                    }
                    if (editBatchCheck != null && tempBatchCheck.length == 0) {
                        availableStockCheck = $scope.getAvailableStock(editBatchCheck, editBatchCheck);
                        editBatchCheck.availableQty = availableStockCheck;
                        totalQuantityCheck = availableStockCheck;
                        tempBatchCheck.push(editBatchCheck);
                    }
                    batchListCheck = tempBatchCheck;
                    if (batchListCheck.length > 0) {
                        $scope.zeroBatch = false;
                        //   loadBatch(null);
                        //productStockService.productBatch(tempStockValues.productStock.productId).then(function (response) {
                        //    $scope.batchList = response.data;
                        //    var qty = document.getElementById("quantity");
                        //    qty.focus();
                        //    var totalQuantity = 0;
                        //    var rack = "";
                        //    var tempBatch = [];
                        //    $scope.changediscountsale("");
                        //    for (var i = 0; i < $scope.batchList.length; i++) {
                        //        if ($scope.Editsale == false) {
                        //            $scope.batchList[i].productStockId = $scope.batchList[i].id;
                        //            $scope.batchList[i].id = null;
                        //            var availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                        //            if (editBatch != null && editBatch.productStockId == $scope.batchList[i].productStockId)
                        //                editBatch.availableQty = availableStock;
                        //            totalQuantity += availableStock;
                        //            if (availableStock == 0)
                        //                continue;
                        //            $scope.batchList[i].availableQty = availableStock;
                        //            if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                        //                rack = $scope.batchList[i].rackNo;
                        //            }
                        //            tempBatch.push($scope.batchList[i]);
                        //        } else {
                        //            if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                        //                rack = $scope.batchList[i].rackNo;
                        //            }
                        //        }
                        //    }
                        //    if (editBatch != null && tempBatch.length == 0) {
                        //        var availableStock = $scope.getAvailableStock(editBatch, editBatch);
                        //        editBatch.availableQty = availableStock;
                        //        totalQuantity = availableStock;
                        //        tempBatch.push(editBatch);
                        //    }
                        //    $scope.batchList = tempBatch;
                        //    if ($scope.batchList.length > 0) {
                        //        if (editBatch == null) {
                        //            $scope.selectedBatch = $scope.batchList[0];
                        //            $scope.selectedBatch.reminderFrequency = "0";
                        //            $scope.selectedBatch.discount = "";
                        //        }
                        //        else {
                        //            $scope.selectedBatch = editBatch;
                        //            $scope.selectedBatch.discount = editBatch.discount;
                        //            $scope.selectedBatch.previoussellingPrice = editBatch.sellingPrice;
                        //        }
                        //        $scope.selectedBatch.totalQuantity = totalQuantity;
                        //        $scope.selectedBatch.rackNo = rack;
                        //        dayDiff($scope.selectedBatch.expireDate);
                        //        //if (editBatch == null) {
                        //        //    $scope.selectedBatch.discount = "";
                        //        //}
                        //    }
                        //}, function () { });
                        //$scope.valPurchasePrice = "";
                        //var qty = document.getElementById("quantity");
                        //qty.focus();
                        productStockService.productBatch(tempStockValues.productStock.productId)
                            .then(function (response) {
                                var batchList = response.data;
                                var totalQuantity = 0;
                                $scope.totalQty = 0;
                                var rack = "";
                                var tempBatch = [];
                                var availableStock = "";
                                $scope.changediscountsale("");
                                var editBatch = null;
                                for (var i = 0; i < batchList.length; i++) {
                                    batchList[i].productStockId = batchList[i].id;
                                    batchList[i].id = null;
                                    availableStock = $scope.getAvailableStock(batchList[i], editBatch);
                                    if (editBatch != null && editBatch.productStockId == batchList[i].productStockId) {
                                        editBatch.availableQty = availableStock;
                                    }
                                    totalQuantity += availableStock;
                                    if (availableStock == 0) {
                                        continue;
                                    }
                                    batchList[i].availableQty = availableStock;
                                    if (batchList[i].rackNo != null || batchList[i].rackNo != undefined) {
                                        rack = batchList[i].rackNo;
                                    }
                                    tempBatch.push(batchList[i]);
                                }
                                if (editBatch != null && tempBatch.length == 0) {
                                    availableStock = $scope.getAvailableStock(editBatch, editBatch);
                                    editBatch.availableQty = availableStock;
                                    totalQuantity = availableStock;
                                    tempBatch.push(editBatch);
                                }
                                $scope.totalQty = totalQuantity;
                                batchList = tempBatch;
                                if (batchList.length > 0) {
                                    $scope.zeroBatch = false;
                                    $scope.enablePopup = true;
                                    /* cacheService.set('selectedProduct1', $scope.selectedProduct);
                                     var m = ModalService.showModal({
                                         "controller": "productDetailSearchCtrl",
                                         "templateUrl": 'productDetails',
                                         "inputs": {
                                             "productId": $scope.selectedProduct.product.id,
                                             "productName": $scope.selectedProduct.name,
                                             "items": $scope.sales.salesItem,
                                             "enableWindow": $scope.enablePopup
                                         }
                                     }).then(function (modal) {
                                         modal.element.modal();
                                         modal.close.then(function (result) {
                                             $scope.message = "You said " + result;
                                         });
                                     });
                                     return false;*/
                                    $scope.batchList1 = batchList;
                                    totalQuantity = 0;
                                    rack = "";
                                    tempBatch = [];
                                    editBatch = null;
                                    for (var i = 0; i < $scope.batchList1.length; i++) {
                                        //$scope.batchList1[i].productStockId = $scope.batchList1[i].id;
                                        //$scope.batchList1[i].id = null;
                                        availableStock = $scope.getAvailableStock($scope.batchList1[i], editBatch);
                                        if (editBatch != null && editBatch.productStockId == $scope.batchList1[i].productStockId) {
                                            editBatch.availableQty = availableStock;
                                        }
                                        totalQuantity += availableStock;
                                        if (availableStock == 0) {
                                            continue;
                                        }
                                        $scope.batchList1[i].availableQty = availableStock;
                                        if ($scope.batchList1[i].rackNo != null || $scope.batchList1[i].rackNo != undefined) {
                                            rack = $scope.batchList1[i].rackNo;
                                        }
                                        tempBatch.push($scope.batchList1[i]);
                                    }
                                    if (editBatch != null && tempBatch.length == 0) {
                                        availableStock = $scope.getAvailableStock(editBatch, editBatch);
                                        editBatch.availableQty = availableStock;
                                        totalQuantity = availableStock;
                                        tempBatch.push(editBatch);
                                    }
                                    $scope.batchList1 = tempBatch;
                                    $scope.totalQty = totalQuantity;
                                    $scope.showBatchGrid = true;
                                    $scope.productBatchDetails = $scope.batchList1;
                                    $scope.isSelectedItem = 1;
                                    $scope.shouldBeOpen = true;
                                } else {
                                    $scope.zeroBatch = true;
                                }
                            }, function () {
                            });
                    } else {
                        $scope.zeroBatch = true;
                    }
                }, function () {
                });
        }
    };
    $scope.alternateSelection = function () {
        var alter1 = cacheService.get('selectedAlternate1');
        if ($scope.zeroBatch) {
            $scope.zeroBatch = false;
        }
        if (alter1 != null) {
            $scope.batchList = alter1;
            $scope.selectedProduct = alter1[0];
            $scope.selectedProduct.product.id = alter1[0].productId;
            $scope.selectedProduct.product.name = alter1[0].name;
            //loadBatch(null);
            //  $scope.productSelection();
            productStockService.productBatch($scope.selectedProduct.product.id)
                .then(function (responses) {
                    var batchListCheck = responses.data;
                    var totalQuantityCheck = 0;
                    var tempBatchCheck = [];
                    var editBatchCheck = null;
                    var availableStockCheck = "";
                    for (var i = 0; i < batchListCheck.length; i++) {
                        batchListCheck[i].productStockId = batchListCheck[i].id;
                        batchListCheck[i].id = null;
                        availableStockCheck = $scope.getAvailableStock(batchListCheck[i], editBatchCheck);
                        if (editBatchCheck != null && editBatchCheck.productStockId == batchListCheck[i].productStockId) {
                            editBatchCheck.availableQty = availableStockCheck;
                        }
                        totalQuantityCheck += availableStockCheck;
                        if (availableStockCheck == 0) {
                            continue;
                        }
                        batchListCheck[i].availableQty = availableStockCheck;
                        tempBatchCheck.push(batchListCheck[i]);
                    }
                    if (editBatchCheck != null && tempBatchCheck.length == 0) {
                        availableStockCheck = $scope.getAvailableStock(editBatchCheck, editBatchCheck);
                        editBatchCheck.availableQty = availableStockCheck;
                        totalQuantityCheck = availableStockCheck;
                        tempBatchCheck.push(editBatchCheck);
                    }
                    batchListCheck = tempBatchCheck;
                    if (batchListCheck.length > 0) {
                        $scope.zeroBatch = false;
                        newsalesService.getBatchListDetail()
                            .then(function (response1) {
                                $scope.batchListType = response1.data;
                                if ($scope.batchListType == "Batch") {
                                    $scope.ProductDetails();
                                } else {
                                    loadBatch(null);
                                }
                            }, function () {
                            });
                    } else {
                        $scope.zeroBatch = true;
                    }
                }, function () {
                });
        }
        var qty = document.getElementById("quantity");
        qty.focus();
    };
    $scope.isdoctornameMandatory = false;
    $scope.isSalesTypeMandatory = false;
    $scope.iscustomerNameMandatory = false;
    $scope.maxDiscountExceeds = false;
    $scope.addNewRow = function () {
        $scope.product.items.length = 0;
        var emptyrow = false;
        for (var i = 0; i < $scope.product.items.length; i++) {
            if ($scope.product.items[i].name == "") {
                emptyrow = true;
            }
        }
        if (emptyrow) {
            $scope.product.items.push(angular.copy(productItem));
        }
    };
    $scope.addSales = function () {
        $scope.maxDiscountExceeds = false;
        if ($scope.maxDiscountFixed == 'Yes') {
            if ($scope.selectedBatch.discount > $scope.maxDisountValue) {
                $scope.maxDiscountExceeds = true;
                return false;
            }
        }
        $scope.enableTempStockPopup = false;
        if ($scope.Quantityexceeds == 1) {
            return;
        }
        if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == null || $scope.selectedBatch.sellingPrice == "") {
            return;
        }
        if (typeof ($scope.selectedBatch.discount) == "string") {
            if (parseFloat($scope.selectedBatch.discount) > 100 || $scope.selectedBatch.discount == '.') {
                return;
            } else
                if ($scope.selectedBatch.discount == undefined || $scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "")
                    $scope.selectedBatch.discount = 0;
        } else {
            if (parseFloat($scope.selectedBatch.discount) > 100) {
                return;
            } else {
                if ($scope.selectedBatch.discount == undefined || $scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "") {
                    $scope.selectedBatch.discount = 0;
                }
            }
        }
        if ($scope.selectedBatch.quantity <= 0 || $scope.selectedBatch.quantity == undefined) {
            return;
        }
        if ($scope.batchList.length == 0) {
            return;
        }
        var RequiredQuantity = angular.element("#spnquantity").attr("data-quantityId");
        var ChangedMrp = angular.element("#spnquantity").attr("data-price");
        var ChangedDicount = angular.element("#spnquantity").attr("data-discount") || 0;
        var addedItem = "";
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty) {
            for (var i = 0; i < $scope.batchList.length; i++) {
                if ($scope.batchList[i].editId == null) {
                    $scope.batchList[i].editId = $scope.editId++;
                    addedItem = getAddedItems($scope.batchList[i].productStockId, $scope.batchList[i].sellingPrice, ChangedDicount);
                    if (addedItem != null) {
                        if (RequiredQuantity > $scope.batchList[i].quantity) {
                            addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.batchList[i].quantity);
                        } else {
                            addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.batchList[i].availableQty);
                        }
                    } else {
                        if (RequiredQuantity != 0) {
                            if (RequiredQuantity > $scope.batchList[i].availableQty) {
                                $scope.batchList[i].quantity = $scope.batchList[i].availableQty;
                                RequiredQuantity = RequiredQuantity - $scope.batchList[i].availableQty;
                                $scope.batchList[i].availableQty = 0;
                            } else {
                                $scope.batchList[i].availableQty = $scope.batchList[i].availableQty - RequiredQuantity;
                                $scope.batchList[i].quantity = RequiredQuantity;
                                RequiredQuantity = 0;
                            }
                            $scope.batchList[i].discount = ChangedDicount;
                            $scope.batchLst[i].totalQuantity = $scope.totalQty;
                            $scope.sales.salesItem.push($scope.batchList[i]);
                        }
                    }
                } else {
                    for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                        if ($scope.sales.salesItem[i].editId == $scope.batchList[i].editId) {
                            $scope.sales.salesItem[i] = $scope.batchList[i];
                            $scope.sales.salesItem[i].Action = "U";
                            break;
                        }
                    }
                }
            }
            $scope.salesItems.$setPristine();
            $scope.selectedProduct = null;
            $scope.selectedBatch = { "reminderFrequency": "0", "purchasePrice": 0 };
            $scope.batchList = [];
            $scope.highlight = "";
            resetFocus();
            ChangedDicount = 0;
            ChangedMrp = 0;
            $scope.showBatchGrid = false;
        } else {
            if ($scope.selectedBatch.editId == null) {
                $scope.selectedBatch.editId = $scope.editId++;
                addedItem = getAddedItems($scope.selectedBatch.productStockId, $scope.selectedBatch.sellingPrice, $scope.selectedBatch.discount);
                if (addedItem != null) {
                    addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.selectedBatch.quantity);
                } else {
                    $scope.sales.salesItem.push($scope.selectedBatch);
                }
            } else {
                for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                    if ($scope.sales.salesItem[i].editId == $scope.selectedBatch.editId) {
                        $scope.sales.salesItem[i] = $scope.selectedBatch;
                        $scope.sales.salesItem[i].Action = "U";
                        break;
                    }
                }
            }
            $scope.salesItems.$setPristine();
            $scope.selectedProduct = null;
            $scope.selectedBatch = { "reminderFrequency": "0", "purchasePrice": 0 };
            $scope.batchList = [];
            $scope.highlight = "";
            resetFocus();
        }
        $scope.product.items.length = 0;
        for (var i = 0; i < $scope.sales.salesItem.length + 1; i++) {
        
            $scope.product.items.push(angular.copy(productItem));
        }
         
        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            $scope.product.items[j].sno = j + 1;
            $scope.product.items[j].rowid = j + 1;
            $scope.product.items[j].schedule = "";
            $scope.product.items[j].id = $scope.sales.salesItem[j].id;
            $scope.product.items[j].accountId = $scope.sales.salesItem[j].accountId;
            $scope.product.items[j].name = $scope.sales.salesItem[j].name;
            $scope.product.items[j].product = $scope.sales.salesItem[j].name;
            $scope.product.items[j].vatInPrice = $scope.sales.salesItem[j].vatInPrice;
            $scope.product.items[j].discount = $scope.sales.salesItem[j].discount;
            $scope.product.items[j].age = $scope.sales.salesItem[j].age;
            $scope.product.items[j].totalStock = $scope.sales.salesItem[j].totalStock;
            $scope.product.items[j].soldqty = $scope.sales.salesItem[j].soldqty;
            $scope.product.items[j].productId = $scope.sales.salesItem[j].productId;
            $scope.product.items[j].vendorId = $scope.sales.salesItem[j].vendorId;
            $scope.product.items[j].batchNo = $scope.sales.salesItem[j].batchNo;
            $scope.product.items[j].expireDate = $scope.sales.salesItem[j].expireDate;
            // $scope.product.items[j].vat = $scope.sales.salesItem[j].vat;
            $scope.product.items[j].rackNo = $scope.sales.salesItem[j].product.rackNo;
            $scope.product.items[j].boxNo = $scope.sales.salesItem[j].product.boxNo;
            $scope.product.items[j].rackBox = $scope.sales.salesItem[j].product.rackBox;
            $scope.product.items[j].sellingPrice = $scope.sales.salesItem[j].sellingPrice;
            $scope.product.items[j].stock = $scope.sales.salesItem[j].stock;
            $scope.product.items[j].packageSize = $scope.sales.salesItem[j].packageSize;
            $scope.product.items[j].packagePurchasePrice = $scope.sales.salesItem[j].packagePurchasePrice;
            $scope.product.items[j].purchasePrice = $scope.sales.salesItem[j].purchasePrice;
            $scope.product.items[j].offlineStatus = $scope.sales.salesItem[j].offlineStatus;
            $scope.product.items[j].cst = $scope.sales.salesItem[j].cst;
          
            $scope.product.items[j].totalQuantity = $scope.sales.salesItem[j].totalQuantity;
            $scope.product.items[j].availableQty = $scope.sales.salesItem[j].availableQty;

            $scope.product.items[j].igst = $scope.sales.salesItem[j].igst;
            $scope.product.items[j].cgst = $scope.sales.salesItem[j].cgst;
            $scope.product.items[j].sgst = $scope.sales.salesItem[j].sgst;
            $scope.product.items[j].gstTotal = $scope.sales.salesItem[j].gstTotal;

            $scope.product.items[j].reminderFrequency = 'None';
            $scope.product.items[j].reminderDate = "";
            $scope.product.items[j].isLastRow = false;
            $scope.product.items[j].isQuantityFocused = false;
            $scope.product.items[j].isMrpFocused = false;
            $scope.product.items[j].isDiscountFocused = false;
            $scope.sales.salesItem[j].rowid = j + 1;

            if ($scope.sales.salesItem[j].salesReturn != undefined) {
                $scope.product.items[j].salesReturn = $scope.sales.salesItem[j].salesReturn;
            }
            $scope.product.items[j].quantity = $scope.sales.salesItem[j].quantity;
            if ($scope.product.items[j].salesReturn == true) {
                $scope.product.items[j].quantity = $scope.product.items[j].quantity * (-1);
            }
          
        }
        //Discount Length Code
        var disclength = 0;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            if ($scope.sales.salesItem[k].discount == 0) {
                disclength++;
            }
        }
        if (disclength == $scope.sales.salesItem.length) {
            $scope.discountInValid = false;
        }
        if ($scope.sales.credit != 0 || $scope.sales.credit != undefined) {
            if ($scope.sales.credit > $scope.sales.total) {
                $scope.Iscredit = true;
            } else {
                $scope.Iscredit = false;
            }
        }
        //Doctor And Schedule Code
        $scope.isdoctornameMandatory = false;
        $scope.iscustomerNameMandatory = false;
        $scope.schedulecount = 0;
        if ($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) {
            var schedulecount = 0;
            for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                if ($scope.DoctorNameMandatory == 3) {
                    schedulecount = 1;
                } else {
                    if ($scope.sales.salesItem[i].product.schedule == "H" || $scope.sales.salesItem[i].product.schedule == "H1" || $scope.sales.salesItem[i].product.schedule == "X") {
                        schedulecount++;
                    }
                }
            }
            $scope.schedulecount = schedulecount;
            if ($scope.schedulecount > 0) {
                // $scope.isdoctornameMandatory = true;
                $scope.iscustomerNameMandatory = true;
                var custname = document.getElementById("customerName").value;
                if ($scope.customerHelper.data.selectedCustomer.name == undefined || $scope.customerHelper.data.selectedCustomer.name == null) {
                    $scope.customerHelper.data.selectedCustomer.name = "";
                }
                if (custname != "" || $scope.customerHelper.data.selectedCustomer.name != "") {
                    $scope.iscustomerNameMandatory = false;
                }
                var docname = document.getElementById("doctorName").value;
                if (docname != "") {
                    $scope.isdoctornameMandatory = false;
                }
            }
        }
        $scope.isSalesTypeMandatory = false;
        if ($scope.SalesTypeMandatory == 1) {
            $scope.isSalesTypeMandatory = true;
        }
        $scope.BillCalculations();
        getDiscountRules();
        $scope.isPopupOpened = false;
        $scope.showQty = false;
        $scope.focusONMainGD = true;
        //$scope.isSelectedItemMain = parseInt($scope.product.items.length);
        $scope.isSelectedItemMain = 1;
        $scope.isCurrentRow = parseInt($scope.isSelectedItemMain) - 1;
        $scope.productnameCookie = "";
        $scope.selectedProduct = null;
        //if ($scope.sales.salesItem.length > 5) {
        //    document.getElementById('tblProducts').scrollTop += 100;
        //}
        $scope.divmainHeight = $('#divmain').css("height");
        if ($scope.isProductGenericSearch == true) {
            $scope.focusProductSearch = true;
            $scope.focusGenericSearch = false;
        } else {
            $scope.focusProductSearch = false;
            $scope.focusGenericSearch = true;
        }
    };
    $scope.resetSales = function () {
        $location.path('#/pos');
    };
    $scope.closeResetPopup = function () {
        $scope.resetAppPopup = false;
    };
    $scope.schedulecount = 0;
    $scope.removeSales = function () {
        var selectedRow = getdeletevalues;
        if (selectedRow.name == "") {
            toastr.info('you cant delete this default row');
            return;
        }
        var serachrowID = selectedRow.rowid;
        var filterrow = $filter('filter')($scope.sales.salesItem, {
            rowid: serachrowID
        });
        var oldQty = 0;
        var item = selectedRow;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            $scope.sales.total += ($scope.sales.salesItem[k].sellingPrice * $scope.sales.salesItem[k].quantity);
        }
        $scope.sales.total -= (item.sellingPrice * item.quantity);
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            var aa = $scope.sales.salesItem[k].rowid;
            var cc, bb = "";
            if (serachrowID > 1) {
                cc = serachrowID;
                bb = serachrowID - 1;
                $scope.sales.salesItem[bb].quantity = selectedRow.quantity;
            } else {
                bb = serachrowID - 1;
                cc = serachrowID;
                $scope.sales.salesItem[bb].quantity = selectedRow.quantity;
            }
            if (cc == aa) {
                $scope.product.items.splice(k, 1);
                $scope.sales.salesItem.splice(k, 1);
                $scope.deleteAppPopup = false;
                setTimeout(resetFocus, 200);
                toastr.success('Deleted successfully');
            }
        }
        for (var i = 0; i < $scope.product.items.length; i++) {
            if ($scope.product.items[i].name !== "") {
                $scope.product.items[i].rowid = i + 1;
                $scope.product.items[i].sno = i + 1;
                $scope.sales.salesItem[i].rowid = i + 1;
            }
        }
        $scope.discountInValid = false;
        $scope.schedulecount = 0;
        var schedulecount = 0;
        if ($scope.sales.salesItem.length > 0) {
            for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                if (($scope.sales.salesItem[i].discount > 0) && ($scope.sales.salesItem[i].discount <= 100)) {
                    $scope.discountInValid = true;
                }
                if ($scope.DoctorNameMandatory == 1) {
                    if ($scope.sales.salesItem[i].product.schedule == "H" || $scope.sales.salesItem[i].product.schedule == "H1" || $scope.sales.salesItem[i].product.schedule == "X") {
                        schedulecount++;
                    }
                }
                if ($scope.DoctorNameMandatory == 3) {
                    schedulecount = 1;
                }
            }
            $scope.schedulecount = schedulecount;
            var custname = document.getElementById("customerName").value;
            var docname = document.getElementById("doctorName").value;
            if (custname == "" && $scope.schedulecount > 0) {
                $scope.iscustomerNameMandatory = true;
            } else {
                $scope.iscustomerNameMandatory = false;
            }
            if (docname == "" && $scope.schedulecount > 0) {
                $scope.isdoctornameMandatory = true;
            } else {
                $scope.isdoctornameMandatory = false;
            }
        } else {
            $scope.iscustomerNameMandatory = false;
            $scope.isdoctornameMandatory = false;
        }
        //  getDiscountRules();
        $scope.BillCalculations();
        $scope.sales.discount = "";
        $scope.sales.discountValue = "";
        $scope.selectedProduct = null;
        //$scope.isLastRow = true;
        //$scope.isSelectedItemMain = $scope.product.items.length;
        $scope.isSelectedItemMain = 1;
    };
    //Added by arun
    $scope.closeDeletePopup = function () {
        $scope.deleteAppPopup = false;
        var myEl4 = angular.element(document.querySelector("#btndelrow"));
        myEl4.removeClass('active');
        var myEl5 = angular.element(document.querySelector("#btnrmvrow"));
        myEl5.removeClass('active');
    };
    $scope.manualSeries = "";
    $scope.selectedSeriesItem = "";
    $scope.editSales = function (item) {
        if ($scope.zeroBatch) {
            $scope.zeroBatch = false;
        }
        var newObject = jQuery.extend(true, {}, item);
        $scope.selectedProduct = newObject;
        $scope.isProductGenericSearch = true;
        $scope.focusProductSearch = true;
        $scope.focusGenericSearch = false;
        loadBatch(newObject);
    };
    $scope.saveAfterConfirm = function () {
        // $.LoadingOverlay("show");
        $scope.isProcessing = true;
        $scope.discountInValid = false;
        //Add patient information
        if (newcustomerHelper.data.isCustomerSelected) {
            if ($scope.PatientNameSearch != 1) {
                $scope.sales.patientId = newcustomerHelper.data.selectedCustomer.id;
            } else {
                $scope.sales.patientId = "";
            }
            $scope.sales.name = newcustomerHelper.data.selectedCustomer.name;
            $scope.sales.mobile = newcustomerHelper.data.selectedCustomer.mobile;
            $scope.sales.email = newcustomerHelper.data.selectedCustomer.email;
            $scope.sales.dOB = newcustomerHelper.data.selectedCustomer.dOB;
            $scope.sales.age = newcustomerHelper.data.selectedCustomer.age;
            $scope.sales.gender = newcustomerHelper.data.selectedCustomer.gender;
            $scope.sales.address = newcustomerHelper.data.selectedCustomer.address;
            $scope.sales.isValidEmail = newcustomerHelper.data.selectedCustomer.isValidEmail;
        } else {
            $scope.sales.name = newcustomerHelper.data.patientSearchData.name;
            $scope.sales.mobile = newcustomerHelper.data.patientSearchData.mobile;
            $scope.sales.patientId = newcustomerHelper.data.patientSearchData.empID;
        }
        //if ($scope.selectedDoctor == null) {
        //    //$scope.$root.$broadcast('getValue', 'doctorName');
        //    //$scope.sales.doctorName = $scope.doctor.name;
        //    $scope.sales.doctorName = $scope.doctorname;            
        //}
        //else {
        //    $scope.sales.doctorName = $scope.selectedDoctor.originalObject.name;
        //}
        if ($scope.doctorname != null && $scope.doctorname != undefined) {
            if (typeof ($scope.doctorname) == "object") {
                $scope.sales.doctorName = $scope.doctorname.name;
            } else {
                $scope.sales.doctorName = $scope.doctorname;
            }
        }
        if ($scope.shortDoctorname != null && $scope.shortDoctorname != undefined) {
            if (typeof ($scope.shortDoctorname) == "object") {
                $scope.sales.doctorName = $scope.shortDoctorname.name;
            } else {
                $scope.sales.doctorName = $scope.shortDoctorname;
            }
        }
        if ($scope.sales.cashType == "Credit") {
            $scope.sales.paymentType = "Credit";
        }
        if ($scope.selectedBillDate == "" || $scope.selectedBillDate == undefined || $scope.selectedBillDate == null) {
            $scope.currentdate = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
            $scope.selectedBillDate = $scope.currentdate;
        }
        $scope.sales.autoSavecust = false;
        if ($scope.AutosaveCustomer == 1) {
            var custname = document.getElementById("customerName").value;
            if ($scope.customerHelper.data.selectedCustomer.name == undefined || $scope.customerHelper.data.selectedCustomer.name == null) {
                $scope.customerHelper.data.selectedCustomer.name = "";
            }
            if (custname != "" || $scope.customerHelper.data.selectedCustomer.name != "") {
                $scope.sales.autoSavecust = true;
            }
        }
        if ($scope.InvoiceSeriestype == "" || $scope.InvoiceSeriestype == undefined) {
            $scope.InvoiceSeriestype = 1;
        }
        if ($scope.InvoiceSeriestype == 1) {
            if ($scope.Editsale == false) {
                $scope.sales.InvoiceSeries = "";
            }
            Savesale();
        }
        if ($scope.InvoiceSeriestype == 2) {
            if ($scope.selectedSeriesItem == "" || $scope.selectedSeriesItem == undefined) {
                toastr.error("Select Custom Series");
                // $.LoadingOverlay("hide");
                return false;
            } else {
                $scope.sales.InvoiceSeries = $scope.selectedSeriesItem;
                $scope.IsSelectedInvoiceSeries = false;
                // $.LoadingOverlay("show");
                newsalesService.saveCustomseries($scope.selectedSeriesItem)
                    .then(function (response) {
                        // $scope.isProcessing = false;
                        //$.LoadingOverlay("hide");
                    }, function () {
                        // $.LoadingOverlay("hide");
                    });
            }
            Savesale();
        }
        if ($scope.InvoiceSeriestype == 3) {
            if ($scope.manualSeries == "") {
                toastr.error("Enter Manual Series");
                //$.LoadingOverlay("hide");
                var Manual = document.getElementById("ManualSeries");
                Manual.focus();
                return false;
            } else {
                $scope.sales.InvoiceSeries = "MAN";
                $scope.sales.InvoiceNo = $scope.manualSeries;
                var sdate = $filter("date")($scope.selectedBillDate, "dd/MM/yyyy");
                newsalesService.IsInvoiceManualSeriesAvail($scope.manualSeries, sdate)
                  .then(function (resp) {
                      bReturn = resp.data;
                      if (bReturn) {
                          toastr.error("" + $scope.manualSeries + " is already Exists");
                          var Manual = document.getElementById("ManualSeries");
                          Manual.focus();
                          $scope.IsSelectedInvoiceSeries = true;
                          // $.LoadingOverlay("hide");
                          return false;
                      } else {
                          $scope.IsSelectedInvoiceSeries = false;
                          Savesale();
                      }
                  }, function (response) {
                      $scope.responses = response;
                  });
            }
        }
    };
    $scope.save = function () {
        if ($scope.SaveProcessing == true) {
            return false;
        }
        if ($scope.iscustomerNameMandatory == true) {
            var custname = document.getElementById("customerName").value;
            if (custname == "") {
                document.getElementById("customerName").focus();
                return false;
            } else {
                $scope.iscustomerNameMandatory = false;
            }
            if ($scope.isShortCustomerName == true) {
                var shortCustname = document.getElementById("shortCustomerName").value;
                if (shortCustname == "") {
                    document.getElementById("shortCustomerName").focus();
                    return false;
                } else {
                    $scope.iscustomerNameMandatory = false;
                }
            }
        }
        if ($scope.isdoctornameMandatory == true) {
            var docname = document.getElementById("doctorName").value;
            if (docname == "") {
                document.getElementById("doctorName").focus();
                return false;
            } else {
                $scope.isdoctornameMandatory = false;
            }
            if ($scope.isShortDoctorName == true) {
                var shortDocname = document.getElementById("shortDoctorname").value;
                if (shortDocname == "") {
                    document.getElementById("shortDoctorname").focus();
                    return false;
                } else {
                    $scope.isdoctornameMandatory = false;
                }
            }
        }
        if ($scope.isSalesTypeMandatory == true) {
            if ($scope.sales.salesType == "") {
                document.getElementById("saleType").focus();
                return false;
            } else {
                $scope.isSalesTypeMandatory = false;
            }
        }
        if ($scope.isInvoicedatevalid == true) {
            document.getElementById("txtBillDate").focus();
            return false;
        }
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {

            //if ($scope.sales.salesItem[i].salesReturn) {
            //    $scope.sales.salesItem[i].quantity = $scope.sales.salesItem[i].quantity * (-1);
            //}
            if ($scope.sales.salesItem[i].isReminder !== undefined && $scope.sales.salesItem[i].isReminder == true) {
                $scope.sales.salesItem[i].reminderFrequency = $scope.sale.reminderFrequency;
                $scope.sales.salesItem[i].reminderDate = $scope.selectedBatch.reminderDate;
            }
        }
        //if ($scope.chkPrintValue) {
        //newsalesService.getPrintStatus().then(function (response) {
        //    if (response.data == "" || response.data == null || response.data == undefined) {
        //        $scope.printStatus = "Disable";
        //    } else {
        //        if (response.data.item1 != undefined) {
        //            $scope.printStatus = response.data.item1;
        //        } else {
        //            $scope.printStatus = "Disable";
        //        }
        //        $scope.printFooterNote = response.data.item2;
        //    }
        //    //if ($scope.printStatus == "Enable") {
        //    //    $scope.confirmPrintPopUp = true;
        //    //    $.confirm({
        //    //        title: 'Print Invoice Confirm',
        //    //        content: 'Do you want to Print Invoice ?',
        //    //        closeIcon: function () {
        //    //            $scope.enableBillPrint = false;
        //    //            if ($scope.confirmPrintPopUp) {
        //    //                $scope.confirmPrintPopUp = false;
        //    //                $scope.saveAfterConfirm();
        //    //            }
        //    //        },
        //    //        buttons: {
        //    //            yes: {
        //    //                text: 'yes [y]',
        //    //                btnClass: 'primary-button',
        //    //                keys: ['enter', 'y'],
        //    //                action: function () {
        //    //                    $scope.enableBillPrint = true;
        //    //                    if ($scope.confirmPrintPopUp) {
        //    //                        $scope.confirmPrintPopUp = false;
        //    //                        $scope.saveAfterConfirm();
        //    //                    }
        //    //                }
        //    //            },
        //    //            no: {
        //    //                text: 'no [n]',
        //    //                btnClass: 'secondary-button',
        //    //                keys: ['n'],
        //    //                action: function () {
        //    //                    $scope.enableBillPrint = false;
        //    //                    if ($scope.confirmPrintPopUp) {
        //    //                        $scope.confirmPrintPopUp = false;
        //    //                        $scope.saveAfterConfirm();
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //    });
        //    //}
        //    //else {
        //    //    $scope.enableBillPrint = false;
        //    //    $scope.saveAfterConfirm();
        //    //}
        //    $scope.saveAfterConfirm();
        //}, function (error) {
        //    console.log(error);
        //});
        ////}
        //else {
        //    $scope.enableBillPrint = false;
        //    $scope.saveAfterConfirm();
        //}
        $scope.saveAfterConfirm();
    };
    //$("#txtBillDate").keyup(function (e) {
    //    if ($(this).val().length == 2 || $(this).val().length == 5) {
    //        if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
    //            $(this).val($(this).val() + "/");
    //    }
    //    // validatedate($(this).val());
    //});
    //$("#txtBillDate").keydown(function (e) {
    //    validatedate($(this).val());
    //});
    $scope.validatedate = function () {
        var val = document.getElementById("txtBillDate").value;
        if (val.length == 10) {
            validatedate(val);
        }
    };
    $scope.isInvoicedatevalid1 = false;
    $scope.isInvoicedatevalid = false;
    function validatedate1(val) {
        var splits = val.split("/");
        var dt = new Date(splits[1] + "/" + splits[0] + "/" + splits[2]);
        if (dt.getDate() == splits[0] && dt.getMonth() + 1 == splits[1] && dt.getFullYear() == splits[2]) {
            $scope.isInvoicedatevalid1 = false;
        } else {
            $scope.isInvoicedatevalid1 = true;
            return;
        }
        FutureDateValidation(dt);
    }
    function validatedate(val) {
        var splits = val.split("/");
        var dt = new Date(splits[1] + "/" + splits[0] + "/" + splits[2]);
        if (dt.getDate() == splits[0] && dt.getMonth() + 1 == splits[1] && dt.getFullYear() == splits[2]) {
            $scope.isInvoicedatevalid = false;
        } else {
            $scope.isInvoicedatevalid = true;
            return;
        }
        FutureDateValidation(dt);
    }
    function FutureDateValidation(dt) {
        var dtToday = new Date();
        var pastDate = new Date(Date.parse(dtToday.getMonth() + "/" + dtToday.getDate() + "/" + parseInt(dtToday.getFullYear() - 100)));
        if (dt < pastDate || dt >= dtToday) {
            $scope.isInvoicedatevalid = true;
        } else {
            $scope.isInvoicedatevalid = false;
        }
    }
    //Durga Modified Code on 27th January 2017
    $scope.totalAmountwithoutVat = 0;
    $scope.vatAmount = 0;
    $scope.discountedTotalNetValue = 0;
    $scope.roundoffNetAmount = 0;
    $scope.totalNetValue = 0;
    $scope.FinalNetAmount = 0;
    $scope.BillCalculations = function () {
        var sumvaluewithoutVat = 0;
        var sumvaluewithVat = 0;
        var discountAmt = 0;
        var TotalDiscountedAmount = 0;
        var vatAmt = 0;
        var totalvatAmt = 0;
        $scope.totalAmountwithoutVat = 0;
        $scope.vatAmount = 0;
        $scope.discountedTotalNetValue = 0;
        $scope.roundoffNetAmount = 0;
        $scope.totalNetValue = 0;
        $scope.FinalNetAmount = 0;
        if ($scope.sales.discount == 100) {
            return 0;
        }
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ($scope.sales.salesItem[i].quantity == undefined) {
                $scope.sales.salesItem[i].quantity = 0;
            }



            if ($scope.sales.salesItem[i].discount == undefined) {
                $scope.sales.salesItem[i].discount = 0;
            }
            if ($scope.sales.salesItem[i].sellingPrice == undefined) {
                $scope.sales.salesItem[i].sellingPrice = 0;
            }
            if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100)) {

                discountAmt = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
                $scope.sales.salesItem[i].discountAmount = angular.copy(discountAmt);

            

                $scope.product.items[i].sellingValue = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - $scope.sales.salesItem[i].discountAmount);

             
                TotalDiscountedAmount += discountAmt;
                sumvaluewithVat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - discountAmt);
            
                vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + $scope.sales.salesItem[i].gstTotal)) * 100));


                $scope.sales.salesItem[i].vatAmount = angular.copy(vatAmt);
                totalvatAmt += vatAmt;
                sumvaluewithoutVat = (sumvaluewithVat - totalvatAmt);
            }
        }
        $scope.totalAmountwithoutVat = sumvaluewithoutVat;
        $scope.totalNetValue = sumvaluewithVat;
        if ($scope.totalNetValue > 1) {
            $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
            $scope.FinalNetAmount = Math.round($scope.totalNetValue);
            $scope.finalNetVAL = Math.round(sumvaluewithVat);
        } else {
            $scope.roundoffNetAmount = 0;
            $scope.FinalNetAmount = $scope.totalNetValue;
        }
        $scope.tempFinalAmount = $scope.FinalNetAmount;
        $scope.discountedTotalNetValue = TotalDiscountedAmount;
        $scope.vatAmount = totalvatAmt;
    };
    $scope.maxDiscountExceeded = false;
    $scope.changeDiscount = function (discount) {





        if ($scope.sale.discountType == "1") {
            if (discount > 100) {
                toastr.error("Discount should not be greater than 100%");
                $scope.sales.discount = 0;
                return false;
            }
        }

        if ($scope.sale.discountType == "2") {
            if (discount > $scope.finalNetVAL) {
                toastr.error("Discount must be less than the Net amount");
                $scope.sales.overallDiscount = 0;
                $scope.sales.discountValue = 0;
                return false;
            }
            $scope.sales.discountValue = discount;
            $scope.changeDiscountValue(discount);
            return;
        }
        $scope.sales.discountValue = "";
        if (discount == "") {
            discount = 0;
        }
        //$scope.sales.discount = parseInt(discount);
        $scope.maxDiscountExceeded = false;
        if ($scope.maxDiscountFixed == 'Yes') {
            if ($scope.sales.discount > $scope.maxDisountValue) {
                $scope.maxDiscountExceeded = true;
                toastr.error("Discount should not be greater than Max Discount " + $scope.maxDisountValue + "%");
                $scope.sales.discount = 0;
                return false;
            }
        }
        $scope.salesItems.$valid = true;
        discountchanged();
        if (discount == 0) {
            $scope.sales.discount = "";
        }

    };
    $scope.changeDiscountValue = function (discountValue) {
        $scope.sales.discount = "";
        if (discountValue == "") {
            discountValue = 0;
        }
        $scope.sales.discountValue = parseInt(discountValue);
        $scope.maxDiscountExceeded = false;
        //if($scope.maxDiscountFixed == 'Yes') {
        if ($scope.sales.discountValue >= $scope.finalNetVAL) {
            $scope.maxDiscountExceeded = true;
            return false;
        }
        //}
        $scope.salesItems.$valid = true;
        discountchanged();
        if (discountValue == 0) {
            $scope.sales.discountValue = "";
        }
        document.getElementById("txtDiscountValue").focus();
    };
    function discountchanged() {
        $scope.DiscountCalculations1();
        //if ($scope.discountType == "1") {
        //    $scope.DiscountCalculations1();
        //} else {
        //    $scope.DiscountCalculations2();
        //}
    }
    $scope.finalNetVAL = 0;
    $scope.DiscountCalculations1 = function () {
        var discountInPercent = null;
        if (($scope.sales.discount == 0 || $scope.sales.discount == "") && ($scope.sales.discountValue == 0 || $scope.sales.discountValue == 0)) {
            discountInPercent = null;
        } else if ($scope.sales.discount > 0) {
            discountInPercent = true;
        } else if ($scope.sales.discountValue > 0) {
            discountInPercent = false;
        }
        var afterDiscountFinalNetAmount = 0;
        var afterDisocutTotalAmountwithoutvat = 0;
        var afterDiscountVatAmount = 0;
        var sumvaluewithoutVat = 0;
        var sumvaluewithVat = 0;
        var discountAmt = 0;
        var TotalDiscountedAmount = 0;
        var vatAmt = 0;
        var totalvatAmt = 0;
        if (discountInPercent) {
            if ($scope.sales.discount > 100)
                return 0;
        }
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100)) {
                discountAmt = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
                $scope.sales.salesItem[i].discountAmount = angular.copy(discountAmt);
                TotalDiscountedAmount += discountAmt;
                sumvaluewithVat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - discountAmt);

                //vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + $scope.sales.salesItem[i].vat)) * 100));

                vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + $scope.sales.salesItem[i].gstTotal)) * 100));


                $scope.sales.salesItem[i].vatAmount = angular.copy(vatAmt);
                totalvatAmt += vatAmt;
                sumvaluewithoutVat = (sumvaluewithVat - totalvatAmt);
            }
        }
        if (discountInPercent) {
            $scope.totalNetValue = (sumvaluewithVat - (($scope.sales.discount / 100) * sumvaluewithVat));
            //  $scope.FinalNetAmount = Math.round($scope.totalNetValue);
            $scope.vatAmount = totalvatAmt - (($scope.sales.discount / 100) * totalvatAmt);
            $scope.discountedTotalNetValue = (($scope.sales.discount / 100) * sumvaluewithVat);
            $scope.totalAmountwithoutVat = (sumvaluewithoutVat - (($scope.sales.discount / 100) * sumvaluewithoutVat));
            ////  $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
        } else {
            $scope.totalNetValue = sumvaluewithVat - $scope.sales.discountValue;
            //  $scope.FinalNetAmount = Math.round($scope.totalNetValue);
            var tempDiscountPercent = ($scope.sales.discountValue / sumvaluewithVat) * 100;
            //$scope.vatAmount = totalvatAmt - $scope.sales.discountValue;
            $scope.vatAmount = totalvatAmt - ((tempDiscountPercent / 100) * totalvatAmt);
            $scope.discountedTotalNetValue = $scope.sales.discountValue;
            //$scope.totalAmountwithoutVat = sumvaluewithoutVat - $scope.sales.discountValue;
            $scope.totalAmountwithoutVat = sumvaluewithoutVat - ((tempDiscountPercent / 100) * sumvaluewithoutVat);
            ////  $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
        }
        if ($scope.totalNetValue > 1) {
            $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
            $scope.FinalNetAmount = Math.round($scope.totalNetValue);
        } else {
            $scope.roundoffNetAmount = 0;
            $scope.FinalNetAmount = $scope.totalNetValue;
        }
    };
    $scope.discountTypeChanged = function () {
        $scope.sales.discountValue = "";
        $scope.discountedTotalNetValue = "";
        if ($scope.sales.discountType == 1) {
            $scope.changeDiscount("");
        } else {
            $scope.changeDiscountValue("");
        }
    };
    //$scope.DiscountCalculations2 = function () {
    //    var afterDiscountFinalNetAmount = 0;
    //    var afterDisocutTotalAmountwithoutvat = 0;
    //    var afterDiscountVatAmount = 0;
    //    var sumvaluewithoutVat = 0;
    //    var sumvaluewithVat = 0;
    //    var discountAmt = 0;
    //    var TotalDiscountedAmount = 0;
    //    var vatAmt = 0;
    //    var totalvatAmt = 0;
    //    if ($scope.sales.discount > 100)
    //        return 0;
    //    for (var i = 0; i < $scope.sales.salesItem.length; i++) {
    //        if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100)) {
    //            discountAmt = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
    //            TotalDiscountedAmount += discountAmt;
    //            sumvaluewithVat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - discountAmt);
    //            vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + $scope.sales.salesItem[i].vat)) * 100));
    //            totalvatAmt += vatAmt;
    //            sumvaluewithoutVat = (sumvaluewithVat - totalvatAmt);
    //        }
    //    }
    //    $scope.totalAmountwithoutVat = (sumvaluewithoutVat - (($scope.sales.discount / 100) * sumvaluewithoutVat));
    //    $scope.totalNetValue = (sumvaluewithVat - (($scope.sales.discount / 100) * sumvaluewithoutVat));
    //    //   $scope.FinalNetAmount = Math.round($scope.totalNetValue);
    //    $scope.discountedTotalNetValue = (($scope.sales.discount / 100) * sumvaluewithoutVat);
    //    $scope.vatAmount = totalvatAmt;
    //    //  $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
    //    if ($scope.totalNetValue > 1) {
    //        $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
    //        $scope.FinalNetAmount = Math.round($scope.totalNetValue);
    //    } else {
    //        $scope.roundoffNetAmount = 0;
    //        $scope.FinalNetAmount = $scope.totalNetValue;
    //    }
    //};
    function Savesale() {
        if ($scope.Editsale == true) {
            $scope.sales.isNew = false;
        }
        if ($scope.LeadsProductSelected == true) {
            $scope.sales.leadsProductSelected = true;
        } else {
            $scope.sales.leadId = "";
        }
        $scope.sales.billPrint = $scope.chkPrintValue;
        if ($scope.SaveProcessing == false) {
            $scope.SaveProcessing = true;
            $scope.sales.roundoffNetAmount = $scope.roundoffNetAmount;
            $.LoadingOverlay("show");
            newsalesService.create($scope.sales, $scope.SelectedFileForUpload, $scope.selectedBillDate)
                .then(function (response) {
                    var sales = response.data;
                    $scope.getLastSalesPrintType();
                    $scope.sales.billPrint = $scope.chkPrintValue;
                    $scope.SaveProcessing = false;
                    toastr.success('Sale completed successfully');
                    if ($scope.enableBillPrint) {
                        printingHelper.printInvoice(sales.id);
                    }
                    //else if ($scope.printStatus != "Enable" && $scope.sales.billPrint) {
                    //    printingHelper.printInvoice(sales.id);
                    //}
                    $scope.salesItems.$setPristine();
                    $scope.sales = getSalesObject();
                    $scope.selectedProduct = null;
                    $scope.selectedBatch = {};
                    newcustomerHelper.ResetCustomer();
                    var salesEditId = document.getElementById('salesEditId').value;
                    if (salesEditId != "") {
                        window.location = window.location.origin + "/Sales/NewList";
                    }
                    $location.path('#/pos');
                    $scope.isProcessing = false;
                    $scope.Editsale = false;
                    $scope.confirmPrintPopUp = false;
                    $scope.printMessage = null;
                    $.LoadingOverlay("hide");
                }, function () {
                    $.LoadingOverlay("hide");
                    toastr.error('Error Occured', 'Error');
                    $scope.isProcessing = false;
                    $scope.SaveProcessing = false;
                    $scope.closePayment();
                });
        }
    }
    $scope.manualSeriesChange = function (series) {
        $scope.manualSeries = series;
        $scope.IsSelectedInvoiceSeries = false;
        $scope.isProcessing = false;
    };
    $scope.ValidateManualSeries = function (series) {
        $scope.manualSeries = series;
        if ($scope.manualSeries != "") {
            newsalesService.IsInvoiceManualSeriesAvail($scope.manualSeries)
      .then(function (resp) {
          bReturn = resp.data;
          if (bReturn) {
              toastr.error("" + $scope.manualSeries + " is already Exists");
              var Manual = document.getElementById("ManualSeries");
              Manual.focus();
              $scope.IsSelectedInvoiceSeries = true;
              $.LoadingOverlay("hide");
              return false;
          } else {
              $scope.IsSelectedInvoiceSeries = false;
              $scope.isProcessing = false;
              //$scope.IsFormInvalid = false;
              //$scope.salesItems.$valid = true;
              return true;
          }
      }, function (response) {
          $scope.responses = response;
      });
        } else {
            toastr.error("Enter Manual Series");
            var Manual = document.getElementById("ManualSeries");
            Manual.focus();
            $scope.IsSelectedInvoiceSeries = true;
        }
    };
    //Newly created by Manivannan on 30-Jan-2017 begins
    function getCustomInvoiceNo(customSeries) {
        newsalesService.getCustomInvoiceNo(customSeries)
            .then(function (response) {
                if ($scope.InvoiceSeriestype == 2) {
                    $scope.sales.invoiceNo = response.data;
                }
                $scope.trackInvoiceNo();
            }, function () {
            });
    }
    //Newly created by Manivannan on 30-Jan-2017 ends
    $scope.ValidateCustomSeries = function (series) {
        $scope.selectedSeriesItem = series;
        if ($scope.selectedSeriesItem == "" || $scope.selectedSeriesItem == undefined) {
            $scope.IsSelectedInvoiceSeries = true;
            return false;
        } else {
            $scope.IsSelectedInvoiceSeries = false;
            $scope.isProcessing = false;
            getCustomInvoiceNo(series); //Newly created for showing Invoice No
            //window.localStorage.setItem("CustomSeriesCookie", $scope.selectedSeriesItem);
            document.getElementById("btnSubmitwithoutPrint").focus();
            return true;
        }
    };
    //getPatientSearchType();
    $scope.PatientNameSearch = "1";
    //function getPatientSearchType() {
    //    newsalesService.getPatientSearchType()
    //        .then(function (response) {
    //            if (response.data == "" || response.data == null || response.data == undefined) {
    //                $scope.PatientNameSearch = "1";
    //            } else {
    //                if (response.data.patientSearchType != undefined) {
    //                    $scope.PatientNameSearch = response.data.patientSearchType;
    //                } else {
    //                    $scope.PatientNameSearch = "1";
    //                }
    //            }
    //        }, function () {
    //        });
    //}
    getDoctorSearchType();
    $scope.DoctorNameSearch = "1";
    function getDoctorSearchType() {
        newsalesService.getDoctorSearchType()
            .then(function (response) {
                if (response.data == "" || response.data == null || response.data == undefined) {
                    $scope.DoctorNameSearch = "1";
                } else {
                    if (response.data.doctorSearchType != undefined) {
                        $scope.DoctorNameSearch = response.data.doctorSearchType;
                    } else {
                        $scope.DoctorNameSearch = "1";
                    }
                }
            }, function () {
            });
    }
    getDoctorNameMandatoryType();
    $scope.DoctorNameMandatory = "2";
    function getDoctorNameMandatoryType() {
        newsalesService.getDoctorNameMandatoryType()
            .then(function (response) {
                if (response.data == "" || response.data == null || response.data == undefined) {
                    $scope.DoctorNameMandatory = "2";
                } else {
                    if (response.data.doctorNameMandatoryType != undefined) {
                        $scope.DoctorNameMandatory = response.data.doctorNameMandatoryType;
                    } else {
                        $scope.DoctorNameMandatory = "2";
                    }
                }
            }, function () {
            });
    }
    getSalestypeMandatory();
    $scope.SalesTypeMandatory = "2";
    function getSalestypeMandatory() {
        newsalesService.getSalestypeMandatory()
            .then(function (response) {
                if (response.data == "" || response.data == null || response.data == undefined) {
                    $scope.SalesTypeMandatory = "2";
                } else {
                    if (response.data.saleTypeMandatory != undefined) {
                        $scope.SalesTypeMandatory = response.data.saleTypeMandatory;
                    } else {
                        $scope.SalesTypeMandatory = "2";
                    }
                }
            }, function () {
            });
    }
    getautoSaveisMandatory();
    $scope.AutosaveCustomer = "2";
    function getautoSaveisMandatory() {
        newsalesService.getautoSaveisMandatory()
            .then(function (response) {
                if (response.data == "" || response.data == null || response.data == undefined) {
                    $scope.AutosaveCustomer = "2";
                } else {
                    if (response.data.autosaveCustomer != undefined) {
                        $scope.AutosaveCustomer = response.data.autosaveCustomer;
                    } else {
                        $scope.AutosaveCustomer = "2";
                    }
                }
            }, function () {
            });
    }
    $scope.saletypeDropdownchnage = function (model) {
        $scope.sales.salesType = model.id;
        $scope.isSalesTypeMandatory = false;
        if ($scope.InvoiceSeriesItems.length != 0) {
            document.getElementById("ddlCustomerSeries").focus();
        }
    };
    getPharmacyDiscountType();
    $scope.discountType = "1";
    function getPharmacyDiscountType() {
        newsalesService.getPharmacyDiscountType()
            .then(function (response) {
                if (response.data == "" || response.data == null || response.data == undefined) {
                    $scope.discountType = "1";
                } else {
                    if (response.data.discountType != undefined) {
                        $scope.discountType = response.data.discountType;
                    } else {
                        $scope.discountType = "1";
                    }
                }
            }, function () {
            });
    }
    function getCustomSeriesSelected() {
        newsalesService.getCustomSeriesSelected()
            .then(function (response) {
                if (response.data != "" && response.data != null) {
                    if (response.data.customSeriesInvoice != undefined || response.data.customSeriesInvoice != null || response.data.customSeriesInvoice != "") {
                        $scope.selectedSeriesItem = response.data.customSeriesInvoice;
                        $scope.IsSelectedInvoiceSeries = false;
                        //Newly added by Manivannan on 30-Jan-17 
                        getCustomInvoiceNo($scope.selectedSeriesItem);
                    }
                }
            }, function () {
            });
    }
    function GetIndexofItem(array, productstockid) {
        var index = -1;
        for (var i = 0; i < array.length; i++) {
            if (array[i].productStockId == productstockid) {
                index = i;
            }
        }
        return index;
    }
    $scope.batchSelected = function (batch) {
        $scope.valPurchasePrice = "";
        var totalQuantity = 0;
        $scope.selectedBatch = $scope.batchList[batch];
        for (var i = 0; i < $scope.batchList.length; i++) {
            totalQuantity += parseInt($scope.batchList[i].stock);
        }
        // added by arun for department calculations
        for (var i = 0; i < $scope.customerHelper.data.customerList.length; i++) {
            if ($scope.customerHelper.data.patientSearchData.mobile == $scope.customerHelper.data.customerList[i].mobile) {
                if ($scope.customerHelper.data.selectedCustomer.name == $scope.customerHelper.data.customerList[i].name) {
                    if ($scope.customerHelper.data.customerList[i].patientType == 2) {




                        //$scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.vat / 100))) * 1.1).toFixed(2);

                        $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.gstTotal / 100))) * 1.1).toFixed(2);



                    }
                }
            }
        }
        // ended by arun
        $scope.selectedBatch.totalQuantity = totalQuantity;
        $scope.selectedBatch.discount = "";
        dayDiff($scope.selectedBatch.expireDate);
    };
    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };
    //File Upload
    $scope.SelectedFileForUpload = null;
    //File Select event 
    $scope.selectFileforUpload = function (file) {
        $scope.SelectedFileForUpload = file[0];
    };
    //Amount Calculation
    $scope.receivedAmount1 = function (receivedAmount2) {
        if ($scope.sales.credit > $scope.FinalNetAmount || $scope.sales.credit == "" || $scope.sales.credit == 0) {
            $scope.Iscredit = true;
        } else {
            $scope.Iscredit = false;
        }
        if (parseFloat(receivedAmount2) > 0) {
            $scope.balanceAmount = parseFloat(receivedAmount2) - $scope.FinalNetAmount + ($scope.sales.credit == "" || $scope.sales.credit == null ? 0 : parseFloat($scope.sales.credit));
        } else {
            $scope.balanceAmount = "";
        }
    };
    $scope.iscashtypevalid = false;
    $scope.changeCashType = function () {
        if ($scope.sales.cashType == "Credit") {
            $scope.sales.paymentType = "Cash";
            $scope.sales.cardNo = "";
            $scope.sales.cardDate = "";
            $scope.sales.cardDigits = "";
            $scope.sales.cardName = "";
            var custname = document.getElementById("customerName").value;
            var custmobile = document.getElementById("searchPatientMobile").value;
            if (custname == "" && custmobile == "") {
                if (!newcustomerHelper.data.isCustomerSelected) {
                    $scope.iscashtypevalid = true;
                }
            } else {
                $scope.iscashtypevalid = false;
            }
            $scope.Iscredit = true;
        }
        if ($scope.sales.cashType == "Full") {
            $scope.sales.credit = "";
            $scope.Iscredit = false;
            $scope.iscashtypevalid = false;
        }
    };
    $scope.isdeliverytypevalid = false;
    $scope.changedeliveryType = function () {
        $scope.salesTypeFocused = true;
        $scope.ReciptModeFocused = false;
        if ($scope.sales.deliveryType == "Home Delivery") {
            var custname = document.getElementById("customerName").value;
            var custmobile = document.getElementById("searchPatientMobile").value;
            if (custname == "" && custmobile == "") {
                if (!newcustomerHelper.data.isCustomerSelected) {
                    $scope.isdeliverytypevalid = true;
                }
            } else {
                $scope.isdeliverytypevalid = false;
            }
        } else {
            $scope.isdeliverytypevalid = false;
        }
    };
    $scope.resetAppPopup = false;
    $scope.cancel = function () {
        $scope.resetAppPopup = true;
        //if (window.confirm('Are you sure, Do you want to Reset?')) {
        //    window.location = window.location.origin + window.location.pathname;
        //}
        $scope.discountInValid = false;
    };
    $scope.loadSalesDiscount = function () {
        if (window.localStorage.getItem("s_discount") == null) {
            return;
        }
        $scope.salesDiscount = JSON.parse(window.localStorage.getItem("s_discount"));
    };
    $scope.isFormValid = true;
    $scope.chequeCustomerRequired = false;
    var discountFlag = 0;
    $scope.selectedBatch.purchasePrice = 0;
    $scope.collectCardInfo = function () {
        if ($scope.sales.paymentType == "Cash") {
            $scope.sales.cardNo = "";
            $scope.sales.cardDate = "";
            $scope.sales.cardDigits = "";
            $scope.sales.cardName = "";
            $scope.chequeCustomerRequired = false;
        }
        if ($scope.sales.paymentType == "Card") {
            $scope.sales.cashType = "Full";
            $scope.sales.cardNo = "";
            $scope.sales.cardDate = "";
            $scope.sales.cardDigits = "";
            $scope.sales.cardName = "";
            $scope.sales.credit = "";
            $scope.Iscredit = false;
            $scope.chequeCustomerRequired = false;
        }
        if ($scope.sales.paymentType == "Cheque") {
            $scope.sales.cashType = "Full";
            $scope.sales.cardNo = "";
            $scope.sales.cardDate = "";
            $scope.sales.cardDigits = "";
            $scope.sales.cardName = "";
            $scope.sales.credit = "";
            $scope.Iscredit = false;
            var custname = document.getElementById("customerName").value;
            var custmobile = document.getElementById("searchPatientMobile").value;
            if (custname == "" && custmobile == "") {
                if (!newcustomerHelper.data.isCustomerSelected) {
                    $scope.chequeCustomerRequired = true;
                }
            } else {
                $scope.chequeCustomerRequired = false;
            }
        }
    };
    $scope.isSellingPriceNotValid = false;
    $scope.validateSellingPrice = function (selectedBatch) {
        $scope.isSellingPriceNotValid = false;
        if (selectedBatch.sellingPrice == "" || selectedBatch.sellingPrice == undefined) {
            $scope.isSellingPriceNotValid = true;
            toastr.error("Required");
            //return false;
        }
        if (selectedBatch.purchasePrice > selectedBatch.sellingPrice || selectedBatch.sellingPrice == 0) {
            toastr.error("Given Price is less than Purchase Price " + selectedBatch.purchasePrice + "");
            // return false;
        }
        var serachrowID = selectedBatch.rowid;
        if (selectedBatch.quantity > 0) {
            for (var k = 0; k < $scope.sales.salesItem.length; k++) {
                var aa = $scope.sales.salesItem[k].rowid;
                var bb, cc = "";
                if (serachrowID > 1) {
                    cc = serachrowID;
                    bb = serachrowID - 1;
                    $scope.sales.salesItem[bb].sellingPrice = selectedBatch.sellingPrice;
                } else {
                    bb = serachrowID - 1;
                    cc = serachrowID;
                    $scope.sales.salesItem[bb].sellingPrice = selectedBatch.sellingPrice;
                }
                if (cc == aa) {
                    $scope.BillCalculations();
                    getDiscountRules();
                }
            }
        }
    };
    $scope.keyEnterdiscount = function () {
        var sell = document.getElementById("sellingPrice");
        sell.focus();
    };
    $scope.discountInValid = false;
    $scope.changediscountsale = function (discount) {
        $scope.maxDiscountExceeds = false;
        if ($scope.maxDiscountFixed == 'Yes') {
            if ($scope.selectedBatch.discount > $scope.maxDisountValue) {
                $scope.maxDiscountExceeds = true;
                return false;
            }
        }
        //if (discount == "" || discount == undefined || parseFloat(discount) > 100) {
        if (parseFloat(discount) > 100 || discount == ".") {
            $scope.salesItems.$valid = false;
        } else {
            //$scope.salesItems.$valid = true;
            $scope.validateQty();
        }
        if ((discount > 0) && (discount <= 100)) {
            $scope.discountInValid = true;
        } else if ((discount == 0) && ($scope.sales.salesItem.length == 0)) {
            $scope.discountInValid = false;
        } else if ((discount == 0) && ($scope.sales.salesItem.length > 0)) {
            for (var j = 0; j < $scope.sales.salesItem.length; j++) {
                if ($scope.sales.salesItem[j].discount > 0) {
                    $scope.discountInValid = true;
                }
            }
        }
    };
    $scope.isShortCustomerName = false;
    $scope.isShortDoctorName = false;
    //$scope.keydown = shortcutHelper.salesShortcuts;
    $scope.focusDiscType = function (val) {
        //$scope.focusInput1 = false;
        //$scope.focusInput = false;
        //if (val == 1) {
        //    $scope.focusInput1 = true;
        //} else {
        //    $scope.focusInput = true;
        //}
        $scope.sales.discount = 0;
        $scope.sales.overallDiscount = 0;
        $scope.sales.discountValue = 0;
        discountchanged();
    };
    $scope.enableTempStockPopup = false;
    $scope.SaveProcessing = false;
    $scope.keydownMain = function (e) {
    };
    ////Adde by arun for Closing the payment details popup 
    //$scope.closePopup = function()
    //{
    //    $scope.showPayment = false;
    //}
    ////Adde by arun for showing the payment details popup
    //$scope.showPaymentDetails = function () {
    //    $scope.showPayment = true;
    //}
    $scope.keydown = function (e) {
        // F1
        if (e.keyCode == 112) {
            event.preventDefault();
            $scope.FocusonProduct();
        }
        var frmItem = angular.element(window.document.activeElement).attr('name');
        //Right Arrow
        if (e.keyCode == 39) {
            event.preventDefault();
            //if (frmItem == 'drugName') {
            //    ele = document.getElementById("txtQty" + $scope.isSelectedItemMain + "");
            //    ele.focus();
            //    return;
            //}
            if ($scope.salesTypeFocused) {
                if ($scope.sales.deliveryType == "Counter") {
                    $scope.sales.deliveryType = "Home Delivery";
                    document.getElementById("door-delivery").focus();
                }
            }
            if ($scope.ReciptModeFocused) {
                if ($scope.sales.sendEmail || $scope.MAILfocused) {
                    $scope.sales.sendSms = true;
                    document.getElementById("custom_checkboxsms").focus();
                    $scope.SMSFocued = true;
                    $scope.MAILfocused = false;
                }
            }
        }
        //Left Arrow
        if (e.keyCode == 37) {
            event.preventDefault();
            if ($scope.salesTypeFocused) {
                if ($scope.sales.deliveryType == "Home Delivery") {
                    $scope.sales.deliveryType = "Counter";
                    document.getElementById("counter").focus();
                }
            }
            if ($scope.ReciptModeFocused) {
                if ($scope.sales.sendSms || $scope.SMSFocued) {
                    $scope.sales.sendEmail = true;
                    document.getElementById("custom_checkboxemail").focus();
                    $scope.MAILfocused = true;
                    $scope.SMSFocued = false;
                }
            }
        }
        //F2
        if (e.keyCode == 113) {
            event.preventDefault();
            ele = document.getElementById("btnSubmitwithPrint");
            ele.focus();
        }
        //F3
        if (e.keyCode == 114) {
            event.preventDefault();
            document.getElementById("selldiscount").focus();
        }
        //F4 Reminder
        if (e.keyCode == 115) {
            event.preventDefault();
            document.getElementById("remFrequency").focus(); //receivedAmount
        }
        //F10 reset data customer
        if (e.keyCode == 121) {
            event.preventDefault();
            document.getElementById("btnreset").focus(); //receivedAmount
            newcustomerHelper.ResetCustomer();
        }
        //F5
        if (e.keyCode == 116) {
            event.preventDefault();
            $scope.sales.cashType = "Credit";
            $scope.focusCreditTextBox = true; productStockService.productBatch;
        }
        //F6
        if (e.keyCode == 117) {
            event.preventDefault();
            document.getElementById("txtBillDate").focus();
        }
        //F7
        if (e.keyCode == 118) {
            event.preventDefault();
            $scope.sales.deliveryType = "Counter";
            document.getElementById("counter").focus();
            $scope.salesTypeFocused = true;
            $scope.ReciptModeFocused = false;
            //$scope.saleType(true);
        }
        //F9
        if (e.keyCode == 120) {
            event.preventDefault();
            //document.getElementById("btnAddcust").focus();  //sellingPrice
            newcustomerHelper.PopupAddNew($scope.maxDiscountFixed, $scope.maxDisountValue, $scope.isDepartmentsave);
        }
        //F8
        if (e.keyCode == 119) {
            event.preventDefault();
            //document.getElementById("btneditcust").focus(); //discount
            newcustomerHelper.EditCustomer($scope.maxDiscountFixed, $scope.maxDisountValue, $scope.isDepartmentsave);
        }
        //F10
        if (e.keyCode == 121) {
            event.preventDefault();
            document.getElementById("reminder").focus();
        }
        //F11
        if (e.keyCode == 122) {
            event.preventDefault();
            document.getElementById("customerType").focus();
        }
        //ctrl+c for customer name
        if (e.keyCode == 67 && e.ctrlKey == true) {
            event.preventDefault();
            document.getElementById("searchPatientMobile").focus();
        }
        if (e.keyCode == 68 && e.ctrlKey == true) {
            event.preventDefault();
            document.getElementById("doctorName").focus();
        }
        //ctrl+S
        //if (e.keyCode == 83 && e.ctrlKey == true) {
        //    event.preventDefault();
        //    var myId = $("#shortNameCtrl").val();
        //    if (myId == "true") {
        //        var shortCustname = "";
        //        if ($scope.isShortCustomerName == true) {
        //            $scope.isShortCustomerName = false;
        //            shortCustname = document.getElementById("shortCustomerName").value;
        //            if (shortCustname == "") {
        //                document.getElementById("shortCustomerName").focus();
        //            }
        //        } else {
        //            $scope.isShortCustomerName = true;
        //            shortCustname = document.getElementById("customerName").value;
        //            if (shortCustname == "") {
        //                document.getElementById("customerName").focus();
        //            }
        //        }
        //    }
        //    myId = $("#shortDoctorNameCtrl").val();
        //    if (myId == "true") {
        //        if ($scope.isShortDoctorName == true) {
        //            $scope.isShortDoctorName = false;
        //        } else {
        //            $scope.isShortDoctorName = true;
        //        }
        //    }
        //    GetDefaultDoctor();
        //    document.getElementById("shortCustomerName").focus();
        //}
        var newWindow = "True"//document.getElementById("newWindow").value;
        var shortCutKeys = "False";
        if ($scope.isPopupOpened == false) {
            // alt + m for missed order Added by arun
            if (e.which == 77 && e.altKey) {
                event.preventDefault();
                $scope.MissedOrder();
            }
            // alt + t for Tempstock Added by arun
            if (e.which == 84 && e.altKey) {
                event.preventDefault();
                $scope.showTempStockScreen();
                //  document.getElementById("BtnTempstock").focus();
            }
            // alt + a for Alternates Added by arun
            if (e.which == 65 && e.altKey) {
                event.preventDefault();
                // document.getElementById("BtnAlternate").focus();
                $scope.PopupAlternates();
            }
            // alt + a for Alternates Added by durga
            if (e.which == 82 && e.altKey) {
                event.preventDefault();
                $scope.cancel();
            }
            //End
            if (e.keyCode == 35) {
                event.preventDefault();
                $scope.showkeyshortcutlist = false;
                $scope.completeSale();
            }
            //ctrl+r
            if (e.keyCode == 82 && e.ctrlKey == true) {
                event.preventDefault();
            }
            //ctrl + b
            if (e.keyCode == 66 && e.ctrlKey == true) {
                $scope.printBill();
            }
        }
        //shift + ? for opening shortcut list 
        if (e.which == 191 && e.shiftKey) {
            event.preventDefault();
            //document.getElementById("showkeyshortcutlist").focus();
            $scope.showkeyshortcutlist = true;
        }
        //Escape
        if (e.keyCode == 27) {
            if ($scope.showkeyshortcutlist == true) {
                $scope.showkeyshortcutlist = false;
                e.preventDefault();
                return false;
            }
            setTimeout(resetFocus, 200);
            $scope.isPopupOpened = false
            if ($scope.MissedOrderOpened) {
                $rootScope.$emit("MissedOrderPopUpEvent", {});
                $scope.MissedOrderOpened = false;
            }
            if ($scope.PopupAlternatesOpened) {
                $rootScope.$emit("PopupAlternatesEvent", {});
                $scope.PopupAlternatesOpened = false;
            }
            if ($scope.TempStockOpened) {
                $rootScope.$emit("TempStockEvent", {});
                $scope.TempStockOpened = false;
            }
            if (cacheService.get("isProductCreateOpened")) {
                $rootScope.$emit("ProductCreateEvent", {});
            }
            if ($scope.showPayment) {
                if (newcustomerHelper.data.AddNewCustomerOpened) {
                    $rootScope.$emit("AddNewCustomerEvent", {});
                } else {
                    $scope.showPayment = false;
                }
            }
        }
        //Y
        if (e.keyCode == 89) {
            if ($scope.deleteAppPopup) {
                $scope.removeSales();
            }
            if ($scope.resetAppPopup) {
                $scope.resetSales();
            }
        }
        //N
        if (e.keyCode == 78) {
            if ($scope.deleteAppPopup) {
                $scope.deleteAppPopup = false;
            }
            if ($scope.resetAppPopup) {
                $scope.closeResetPopup();
            }
            if (currentlyFocusedElement != "") {
                currentlyFocusedElement.focus();
            }
        }
        //ctrl+G  // need to copy data arun
        if (e.keyCode == 71 && e.ctrlKey == true) {
            event.preventDefault();
            if (document.getElementById('txtDrugName').disabled != true) {
                if ($scope.isProductGenericSearch == true) {
                    $scope.isProductGenericSearch = false;
                    if ($scope.isProductGenericSearch == false) {
                        if (document.getElementById("txtDrugName").value != "" || document.getElementById("genericName").value != "") {
                            document.getElementById("txtDrugName").value = "";
                            document.getElementById("genericName").value = "";
                            //$scope.CheckGenericName();
                        }
                        $scope.focusGenericSearch = true;
                        $scope.focusProductSearch = false;
                    }
                } else {
                    $scope.isProductGenericSearch = true;
                    if ($scope.isProductGenericSearch == true) {
                        if (document.getElementById("txtDrugName").value != "" || document.getElementById("genericName").value != "") {
                            document.getElementById("txtDrugName").value = "";
                            document.getElementById("genericName").value = "";
                            $scope.CheckName();
                        }
                        $scope.focusProductSearch = true;
                        $scope.focusGenericSearch = false;
                    }
                }
            }
        }
    };
    $scope.completeSale = function () {
        if ($scope.product.items.length == 1) {
        } else {
            if (!$scope.showPayment) {
                $scope.showPayment = true;
                $scope.enablePopup = true;
                $scope.calculatePaymentDetails();
            }
        }
    };
    $scope.billPrintOption = function (item) {
        if (item) {
            $scope.enableBillPrint = true;
        } else {
            $scope.enableBillPrint = false;
        }
        //$scope.showPayment = !$scope.showPayment;
        $scope.enablePopup = false;
        completeSaleKeyPress();
    };
    function completeSaleKeyPress() {
        var custname = "";
        if ($scope.iscustomerNameMandatory == true) {
            custname = document.getElementById("customerName").value;
            if (custname == "") {
                document.getElementById("customerName").focus();
                toastr.error("Customer Name Required");
                return false;
            } else {
                $scope.iscustomerNameMandatory = false;
            }
            if ($scope.isShortCustomerName == true) {
                var shortCustname = document.getElementById("shortCustomerName").value;
                if (shortCustname == "") {
                    document.getElementById("shortCustomerName").focus();
                    toastr.error("Customer Short Name Required");
                    return false;
                } else {
                    $scope.iscustomerNameMandatory = false;
                }
            }
        }
        if ($scope.isdoctornameMandatory == true) {
            var docname = document.getElementById("doctorName").value;
            if (docname == "") {
                document.getElementById("doctorName").focus();
                toastr.error("Doctor Name Required");
                return false;
            } else {
                $scope.isdoctornameMandatory = false;
            }
            if ($scope.isShortDoctorName == true) {
                var shortDocname = document.getElementById("shortDoctorname").value;
                if (shortDocname == "") {
                    document.getElementById("shortDoctorname").focus();
                    toastr.error("Doctor Short Name Required");
                    return false;
                } else {
                    $scope.isdoctornameMandatory = false;
                }
            }
        }
        if ($scope.isSalesTypeMandatory == true) {
            if ($scope.sales.salesType == "") {
                document.getElementById("customerType").focus();
                toastr.error("customerType Required");
                return false;
            }
        }
        var valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing && $scope.sales.discount <= 100);
        if (!valid) {
            return false;
        }
        if ($scope.sales.cashType == 'Credit' || $scope.sales.deliveryType == 'Home Delivery') {
            custname = document.getElementById("customerName").value;
            var custmobile = document.getElementById("searchPatientMobile").value;
            if (custname == "" && custmobile == "") {
                if (!newcustomerHelper.data.isCustomerSelected) {
                    return false;
                }
            }
        }
        if ($scope.Iscredit) {
            return false;
        }
        if ($scope.maxDiscountExceeded == true) {
            return false;
        }
        if ($scope.enableTempStockPopup == true) {
            $scope.enableTempStockPopup = false;
            return false;
        }
        if ($scope.enablePopup == true) {
            $scope.enablePopup = false;
            return false;
        }
        if ($scope.confirmPrintPopUp == true) {
            return false;
        }
        if ($scope.balanceAmount > 0) {
            if ($scope.sales.cashPayment.amount >= $scope.balanceAmount) {
                $scope.sales.cashPayment.amount = $scope.sales.cashPayment.amount - $scope.balanceAmount;
            } else {
                toastr.error("Payment amount exceeding the Net amount");
                return false;
            }
        }
        $scope.sales.netAmount = $scope.sales.cashPayment.amount;
        if ($scope.sales.cashPayment.amount > 0) {
            $scope.sales.cashPayment.paymentInd = 1;
            $scope.sales.salesPayments.push($scope.sales.cashPayment);
        }
        if ($scope.sales.cardPayment.amount > 0) {
            $scope.sales.cardPayment.paymentInd = 2;
            $scope.sales.salesPayments.push($scope.sales.cardPayment);
        }
        if ($scope.sales.chequePayment.amount > 0) {
            $scope.sales.chequePayment.paymentInd = 3;
            $scope.sales.salesPayments.push($scope.sales.chequePayment);
        }
        if ($scope.sales.walletPayment.amount > 0) {
            $scope.sales.walletPayment.paymentInd = 5;
            $scope.sales.salesPayments.push($scope.sales.walletPayment);
        }
        if ($scope.sales.credit > 0) {
            $scope.sales.creditPayment.paymentInd = 4;
            $scope.sales.creditPayment.amount = $scope.sales.credit;
            $scope.sales.salesPayments.push($scope.sales.creditPayment);
        }
        $scope.save();
    }
    $scope.isSaveValid = function () {
        var valid = "";
        if ($scope.InvoiceSeriestype != 1) {
            if ($scope.sales.salesItem.length == 0 || $scope.IsSelectedInvoiceSeries == false) {
                if ($scope.selectedBatch.name != null && $scope.selectedBatch.quantity != null) {
                    $scope.salesItems.$valid = true;
                }
                valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing && $scope.sales.discount <= 100);
                if (!valid) {
                    return false;
                }
            }
        } else {
            //   if ($scope.sales.salesItem.length == 0) {
            if ($scope.selectedBatch.name != null && $scope.selectedBatch.quantity != null) {
                $scope.salesItems.$valid = true;
            }
            valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing && $scope.sales.discount <= 100);
            if (!valid) {
                return false;
            }
            //  }
        }
        if ($scope.InvoiceSeriestype != 1) {
            if ($scope.IsSelectedInvoiceSeries == true) {
                return false;
            }
        }
        if ($scope.sales.cashType == 'Credit' || $scope.sales.deliveryType == 'Home Delivery') {
            var custname = document.getElementById("customerName").value;
            var custmobile = document.getElementById("searchPatientMobile").value;
            if (custname == "" && custmobile == "") {
                if (!newcustomerHelper.data.isCustomerSelected) {
                    return false;
                }
            }
        }
        if ($scope.customerHelper.data.isCustomerDiscount == 0)
            if ($scope.sales.salesItem.length > 0 && $scope.customerHelper.data.selectedCustomer.name != undefined) {
                //$scope.setTotal1();
                //$scope.vat1();
                //$scope.salesdiscount1();
                //$scope.netTotal();
                $scope.customerHelper.data.isCustomerDiscount = 1;
            }
        if ($scope.customerHelper.data.isReset == 1) {
            //$scope.setTotal1();
            //$scope.vat1();
            //$scope.salesdiscount1();
            //$scope.netTotal();
            $scope.customerHelper.data.isReset = 0;
        }
        return true;
    };
    $scope.closePayment = function () {
        $scope.showPayment = !$scope.showPayment;
        $scope.enablePopup = false;
        $scope.sales.cashPayment = angular.copy(salesPaymentModel),
        $scope.sales.cardPayment = angular.copy(salesPaymentModel),
        $scope.sales.chequePayment = angular.copy(salesPaymentModel),
        $scope.sales.walletPayment = angular.copy(salesPaymentModel),
        $scope.sales.creditPayment = angular.copy(salesPaymentModel),
        $scope.sales.salesPayments = [];
        $scope.sales.credit = "";
        $scope.showCard = false;
        $scope.showCheque = false;
        $scope.showWallet = false;
        $scope.balanceAmount = "";
        $scope.sales.cardDateError = false;
        $scope.sales.cardDateFormatError = false;
        $scope.sales.chequeDateError = false;
        $scope.sales.chequeDateFormatError = false;
        $scope.FocusonProduct();
    };
    newsalesEditHelper.setScope($scope);
    $scope.sales.salesItemDiscount = 0;
    $scope.GetSalesType = function () {
        newsalesService.getSalesType()
           .then(function (response) {
               $scope.sales.saleType = response.data;
               if ($scope.sales.saleType.salesTypeList.length > 0) {
                   $scope.customerType = $scope.sales.saleType.salesTypeList[0];
                   //if ($scope.Editsale == false) {
                   //    $scope.sales.salesType = "";
                   //}
               }
               GetDefaultDoctor();
           }, function () {
           });
    };
    $scope.BatchPopUpSettings = function () {
        newsalesService.getBatchPopUpSettings()
            .then(function (resp) {
                $scope.BatchPopUpSettings = resp.data;
            }, function (error) {
                console.log(error);
            });
    };
    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        newsalesService.getEnableSelling()
            .then(function (response) {
                $.LoadingOverlay("hide");
                $scope.enableSelling = response.data;
            }, function (error) {
                $.LoadingOverlay("hide");
                console.log(error);
                toastr.error('Error Occured', 'Error');
            });
    };
    $scope.getScanBarcodeOption = function () {
        newsalesService.getScanBarcodeOption()
            .then(function (resp) {
                $scope.scanBarcodeOption = resp.data;
            }, function (error) {
                console.log(error);
            });
    };
    $scope.init = function (GSTEnabled) {
        if (GSTEnabled == "True") {
            $scope.GSTEnabled = true;
        } else {
            $scope.GSTEnabled = false;
        }
        $scope.GetSalesType();
        $scope.BatchPopUpSettings();
        $scope.getEnableSelling();
        $scope.getScanBarcodeOption();
        //setProductsGridHeight();
    };
    function setProductsGridHeight() {
        var HeaderHeight = $(".sell-add-header").height();
        var wrapperHeight = $(".wrapper").height();
        var FooterHeight = $("#divFooter").height();
        var documentheight = $(document).height();
        var finalheight = documentheight - HeaderHeight - FooterHeight;
        // $(".wrapper").css("height", "" + finalheight + "px");
    }
    function GetDefaultDoctor() {
        newsalesService.getDefaultDoctor()
            .then(function (response) {
                if (response.data.name != null) {
                    if (($scope.doctorname == null || $scope.doctorname == undefined) && ($scope.sales.doctorMobile == null || $scope.sales.doctorMobile == undefined)) {
                        $scope.doctorname = response.data.name;
                        $scope.shortDoctorname = response.data.name;
                        $scope.sales.doctorMobile = response.data.mobile;
                    }
                }
            }, function (error) {
                console.log(error);
            });
    }
    function getDiscountRules() {
        $scope.sales.salesItemDiscount = 0;
        if ($scope.sales.discount > 0) {
            $scope.sales.discount = $scope.sales.discount;
        } else {
            $scope.sales.discount = "";
        }
        if (!($scope.sales.discountValue > 0)) {
            $scope.sales.discountValue = "";
        }
        discountFlag = 0;
        if (($scope.sales.salesItem != null) && ($scope.sales.salesItem.length > 0)) {
            var total1 = 0;
            for (var x = 0; x < $scope.sales.salesItem.length; x++) {
                total1 = total1 + ((parseFloat($scope.sales.salesItem[x].discount) / 100) * ($scope.sales.salesItem[x].sellingPrice * parseFloat($scope.sales.salesItem[x].quantity)));
            }
            $scope.sales.salesItemDiscount = total1;
        }
        var overalldisc = true;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            if (($scope.sales.salesItem[k].discount > 0) && ($scope.sales.salesItem[k].discount <= 100)) {
                overalldisc = false;
            }
        }
        // $scope.sales.total = $scope.setTotal();
        //   || $scope.customerHelper.data.patientSearchData.discount !=undefined
        if ($scope.customerHelper.data.patientSearchData.discount != null || $scope.customerHelper.data.patientSearchData.discount != undefined) {
            $scope.customerHelper.data.selectedCustomer.discount = $scope.customerHelper.data.patientSearchData.discount;
        }
        //else if ($scope.customerHelper.data.selectedCustomer != null && $scope.customerHelper.data.selectedCustomer.name != undefined) {
        //    patientService.getCustomerDiscount($scope.customerHelper.data.selectedCustomer).then(function (response) {
        //        $scope.customerHelper.data.selectedCustomer.discount = response.data;
        //        if ($scope.maxDiscountFixed == "Yes") {
        //            if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue) {
        //                toastr.info("Selected Customer Having more discount than the Fixed Max discount ,So we changed discount percentage to Max discount");
        //                $scope.customerHelper.data.selectedCustomer.discount = $scope.maxDisountValue;
        //            }
        //        }
        //    });
        //}
        if ($scope.customerHelper.data.selectedCustomer.name == undefined || $scope.customerHelper.data.selectedCustomer.name == null) {
            $scope.customerHelper.data.selectedCustomer.name = "";
        }
        if (overalldisc) {
            newsalesService.getDiscountDetail()
                .then(function (response) {
                    $scope.discountRules = response.data;
                    if (response.data == "" || response.data == null || response.data == undefined) {
                        $scope.sales.discount = 0;
                        $scope.sales.discountValue = 0;
                        $scope.totalDiscount = 0;
                        if ($scope.customerHelper.data.selectedCustomer.name != "") {
                            $scope.sales.discount = $scope.customerHelper.data.selectedCustomer.discount;
                            if ($scope.maxDiscountFixed == "Yes") {
                                if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue) {
                                    $scope.sales.discount = $scope.maxDisountValue;
                                }
                            }
                            discountFlag = 1;
                        }
                    } else {
                        if ($scope.discountRules.billAmountType != "slabDiscount" && $scope.discountRules.billAmountType != "customerWiseDiscount") {
                            if (!$scope.discountRules.amount != null) {
                                if ($scope.discountRules.amount <= $scope.sales.total) {
                                    $scope.sales.discount = $scope.discountRules.discount;
                                    discountFlag = 1;
                                } else {
                                    if (discountFlag == 1) {
                                        discountFlag = 0;
                                    }
                                }
                            }
                        } else if ($scope.discountRules.billAmountType == "slabDiscount") {
                            $scope.sales.discount = 0;
                            for (var z = 0; z < $scope.discountRules.discountItem.length; z++) {
                                if ((!$scope.discountRules.discountItem[z].amount != null) && (!$scope.discountRules.discountItem[z].toAmount != null)) {
                                    if (($scope.discountRules.discountItem[z].amount <= $scope.sales.total) && ($scope.discountRules.discountItem[z].toAmount >= $scope.sales.total)) {
                                        $scope.sales.discount = $scope.discountRules.discountItem[z].discount;
                                        discountFlag = 1;
                                    } else if ($scope.discountRules.discountItem[z].toAmount < $scope.sales.total) {
                                        $scope.sales.discount = $scope.discountRules.discountItem[z].discount;
                                        discountFlag = 1;
                                    }
                                }
                            }
                        } else {
                            $scope.sales.discount = 0;
                            if ($scope.customerHelper.data.selectedCustomer.name != "") {
                                $scope.sales.discount = $scope.customerHelper.data.selectedCustomer.discount;
                                if ($scope.maxDiscountFixed == "Yes") {
                                    if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue) {
                                        $scope.sales.discount = $scope.maxDisountValue;
                                    }
                                }
                                discountFlag = 1;
                            }
                        }
                    }
                    if ($scope.sales.discount == "" || $scope.sales.discount == undefined) {
                        $scope.sales.discount = 0;
                    }
                    if ($scope.sales.discountValue == "" || $scope.sales.discountValue == undefined) {
                        $scope.sales.discountValue = 0;
                    }
                    //Changed as parameter value 1 for discount percentage, 2 for discount value
                    discountchanged();
                });
            $scope.totalDiscount = $scope.sales.discount;
        } else {
            $scope.sales.discount = 0;
            $scope.sales.discountValue = 0;
        }
        if ($scope.sales.discount == 0) {
            $scope.sales.discount = "";
            $scope.totalDiscount = "";
        }
        if ($scope.sales.discountValue == 0) {
            $scope.sales.discountValue = "";
            $scope.totalDiscount = "";
        }
    }
    function dayDiff(expireDate) {
        $scope.highlight = "";
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date(formatString(expireDate));
        var date1 = new Date(formatString(today));
        //var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference < 30) {
            var dt = expireDate;
            $scope.highlight = "Expiry";
        } else {
            $scope.highlight = "";
        }
    }
    function formatString(format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    }
    function resetFocus() {
        //var vlength = $scope.product.items.length;
        //var qtdy = document.getElementsByClassName("drugaclassname");
        //qtdy.focus();
        //$scope.selectedWatchId = 1;
        //$scope.setIndex = 1;
        //$scope.totalDiscount = 0;
        //$scope.showPayment = false;
        //$scope.focusONMainGD = true;
        //$scope.focusedTextid = 1;
        //$scope.focusSelectbox = false;
        //var qty = document.getElementById("drugName");
        //qty.focus();
        $scope.FocusonProduct();
    }
    $scope.sales.discountTotal = 0;
    $scope.sales.discountVat = 0;
    $scope.keyEnter = function (event, e) {
        if ($scope.isShortCustomerName == false) {
            if (event.which === 8) {
                if ($scope.customerHelper.data.patientSearchData.name == "" || $scope.customerHelper.data.patientSearchData.name == undefined) {
                    $scope.Namecookie = "";
                }
            }
            ele = document.getElementById(e);
            if (event.which === 13) // Enter key
            {
                if (patientSelected == 0) {
                    ele.focus();
                    if (ele.nodeName != "BUTTON") {
                        ele.select();
                    }
                }
            }
            if (event.which === 9) {
                $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
            }
        } else {
            if (event.which === 8) {
                if ($scope.customerHelper.data.patientSearchData.shortName == "" || $scope.customerHelper.data.patientSearchData.shortName == undefined) {
                    $scope.ShortNamecookie = "";
                }
            }
            ele = document.getElementById(e);
            if (event.which === 13) // Enter key
            {
                if (patientSelected == 0) {
                    ele.focus();
                    if (ele.nodeName != "BUTTON") {
                        ele.select();
                    }
                }
            }
            if (event.which === 9) {
                $scope.customerHelper.data.patientSearchData.shortName = $scope.ShortNamecookie;
            }
        }
    };
    $scope.blurCustname = function () {
        $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
    };
    $scope.doctorNamekeyEnter = function (nextid) {
        if ($scope.doctorname != null) {
            if ($scope.doctorname.name == null || $scope.doctorname.name == undefined || $scope.doctorname.name == "") {
                $scope.showAddress = true;
            } else {
                $scope.showAddress = false;
            }
        }
        if (newcustomerHelper.data.customerBalance == 0) {
            ele = document.getElementById(nextid);
        } else {
            ele = document.getElementById("chkPendingAmount");
        }
        ele.focus();
    };
    $scope.doctorShortNamekeyEnter = function (nextid) {
        if ($scope.shortDoctorname != null) {
            if ($scope.shortDoctorname.name == null || $scope.shortDoctorname.name == undefined || $scope.shortDoctorname.name == "") {
                $scope.showAddress = true;
            } else {
                $scope.showAddress = false;
            }
        }
        ele = document.getElementById(nextid);
        ele.focus();
    };
    $scope.billDateEnter = function (nextid) {
        ele = document.getElementById(nextid);
        ele.focus();
    };
    $scope.discountEnter = function (nextid) {
        if ($scope.sales.discount == 0) {
            $scope.sales.discount = "";
        }
        ele = document.getElementById(nextid);
        ele.focus();
    };
    $scope.gotoComplete = function (nextid) {
        event.preventDefault();
        //if (nextid == "btncomplete")
        //{
        //    $scope.showPayment = true;
        //}
        //var ele = document.getElementById(nextid);
        //ele.focus();
        if (nextid == "btncomplete") {
            if ($scope.product.items.length > 1) {
                $scope.showPayment = true;
            }
        }
        ele = document.getElementById(nextid);
        ele.focus();
    };
    $scope.changedoctorname = function (name) {
        $scope.doctorname = name;
        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                if ($scope.doctorname == "") {
                    $scope.isdoctornameMandatory = true;
                } else {
                    $scope.isdoctornameMandatory = false;
                }
            }
        }
        //if ($scope.DoctorNameMandatory == 1 && $scope.isdoctornameMandatory == true) {
        //    if ($scope.doctorname == "") {
        //        $scope.isdoctornameMandatory = true;
        //    } else {
        //        $scope.isdoctornameMandatory = false;
        //        $scope.flagDoctorname = false;
        //    }
        //}
        //else if ($scope.DoctorNameMandatory == 1 && $scope.flagDoctorname == false) {
        //    if ($scope.doctorname == "") {
        //        $scope.isdoctornameMandatory = true;
        //    } else {
        //        $scope.isdoctornameMandatory = false;
        //        $scope.flagDoctorname = false;
        //    }
        //}
        if ($scope.doctorname == "") {
            $scope.sales.doctorMobile = "";
        }
    };
    $scope.changeCustomerName = function () {
        var custname = document.getElementById("customerName").value;
        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                if (custname == "") {
                    $scope.iscustomerNameMandatory = true;
                } else {
                    $scope.iscustomerNameMandatory = false;
                    $scope.flagCustomername = false;
                }
            }
        }
        //if ($scope.DoctorNameMandatory == 1 && schedulecount > 0) {
        //    if (custname == "") {
        //        $scope.iscustomerNameMandatory = true;
        //    } else {
        //        $scope.iscustomerNameMandatory = false;
        //        $scope.flagCustomername = false;
        //    }
        //}
        //else if ($scope.DoctorNameMandatory == 1 && $scope.flagCustomername == false) {
        //    if (custname == "") {
        //        $scope.iscustomerNameMandatory = true;
        //    } else {
        //        $scope.iscustomerNameMandatory = false;
        //        $scope.flagCustomername = false;
        //    }
        //}
    };
    //Added by arun for search using mobile no.
    $scope.changeCustomerMobile = function () {
        var custname = document.getElementById("searchPatientMobile").value;
        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                if (custname == "") {
                    $scope.iscustomerNameMandatory = true;
                } else {
                    $scope.iscustomerNameMandatory = false;
                    $scope.flagCustomername = false;
                }
            }
        }
    };
    $scope.focusRemDate = function (nextid) {
        ele = document.getElementById(nextid);
        ele.focus();
    };
    $scope.doctorMobilekeyEnter = function (nextid) {
        ele = document.getElementById(nextid);
        ele.focus();
        if ($scope.showAddress) {
            ele = document.getElementById("doctorAddress");
            ele.focus();
        }
    };
    $scope.doctorAddresskeyEnter = function (nextid) {
        ele = document.getElementById(nextid);
        ele.focus();
    };
    function getSalesObject() {
        return {
            "salesItem": [],
            "discount": "",
            "total": 0,
            "TotalQuantity": 0,
            "rackNo": "",
            "vat": 0,
            "cashType": "Full",
            "paymentType": "Cash",
            "deliveryType": "Counter",
            "billPrint": false,
            "sendEmail": false,
            "sendSms": false,
            "doctorMobile": null,
            "credit": null,
            "cardNo": null,
            "cardDate": null,
            "cardName": null,
            "cardDigits": null,
            "cardType": null,
            "isCreditEdit": false,
            "changeDiscount": "",
            "cashPayment": angular.copy(salesPaymentModel),
            "cardPayment": angular.copy(salesPaymentModel),
            "chequePayment": angular.copy(salesPaymentModel),
            "walletPayment": angular.copy(salesPaymentModel),
            "creditPayment": angular.copy(salesPaymentModel),
            "salesPayments": [],
            "overallDiscount": "",
            "pendingAmountChecked": false,
            "cardDateError": false,
            "cardDateFormatError": false,
            "chequeDateError": false,
            "chequeDateFormatError": false
        };
    }
    function loadBatch(editBatch) {
        productStockService.productBatch($scope.selectedProduct.product.id)
            .then(function (response) {
                $scope.batchList = response.data;
                var qty = document.getElementById("quantity");
                qty.focus();
                var totalQuantity = 0;
                var rack = "";
                var tempBatch = [];
                var availableStock = "";
                $scope.changediscountsale("");
                for (var i = 0; i < $scope.batchList.length; i++) {
                    if ($scope.Editsale == false) {
                        $scope.batchList[i].productStockId = $scope.batchList[i].id;
                        $scope.batchList[i].id = null;
                        availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                        if (editBatch != null && editBatch.productStockId == $scope.batchList[i].productStockId) {
                            editBatch.availableQty = availableStock;
                        }
                        totalQuantity += availableStock;
                        if (availableStock == 0) {
                            continue;
                        }
                        $scope.batchList[i].availableQty = availableStock;
                        if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                            rack = $scope.batchList[i].rackNo;
                        }
                        tempBatch.push($scope.batchList[i]);
                    } else {
                        if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                            rack = $scope.batchList[i].rackNo;
                        }
                    }
                }
                if (editBatch != null && tempBatch.length == 0) {
                    availableStock = $scope.getAvailableStock(editBatch, editBatch);
                    editBatch.availableQty = availableStock;
                    totalQuantity = availableStock;
                    tempBatch.push(editBatch);
                }
                $scope.batchList = tempBatch;
                if ($scope.batchList.length > 0) {
                    if (editBatch == null) {
                        $scope.selectedBatch = $scope.batchList[0];
                        $scope.selectedBatch.reminderFrequency = "0";
                        $scope.selectedBatch.discount = "";
                    } else {
                        $scope.selectedBatch = editBatch;
                        $scope.selectedBatch.discount = editBatch.discount;
                        $scope.selectedBatch.previoussellingPrice = editBatch.sellingPrice;
                    }
                    $scope.selectedBatch.totalQuantity = totalQuantity;
                    $scope.selectedBatch.rackNo = rack;
                    dayDiff($scope.selectedBatch.expireDate);
                    //added by arun 
                    for (var i = 0; i < $scope.customerHelper.data.customerList.length; i++) {
                        if ($scope.customerHelper.data.patientSearchData.mobile == $scope.customerHelper.data.customerList[i].mobile) {
                            if ($scope.customerHelper.data.selectedCustomer.name == $scope.customerHelper.data.customerList[i].name) {
                                if ($scope.customerHelper.data.customerList[i].patientType == 2) {


                                    //$scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.vat / 100))) * 1.1).toFixed(2);

                                    $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.gstTotal / 100))) * 1.1).toFixed(2);



                                }
                            }
                        }
                    }
                    //ended by arun
                }
            }, function () {
            });
        $scope.valPurchasePrice = "";
        ele = document.getElementById("quantity");
        ele.focus();
    }
    //Durga Code For Batch Series
    getInvoiceSeries();
    //Newly created by Manivannan on 30-Jan-2017 begins
    function getNumericInvoiceNo() {
        var salesEditId = document.getElementById('salesEditId').value;
        if (salesEditId == "") {
            if ($scope.InvoiceSeriestype == 1 || $scope.InvoiceSeriestype == "") {
                newsalesService.getNumericInvoiceNo()
                    .then(function (response) {
                        $scope.sales.invoiceNo = response.data;
                        $scope.trackInvoiceNo();
                    }, function () {
                    });
            }
        }
        $scope.trackInvoiceNo();
    }
    //Newly created by Manivannan on 30-Jan-2017 ends
    $scope.InvoiceSeriestype = "";
    function getInvoiceSeries() {
        newsalesService.getInvoiceSeries()
            .then(function (response) {
                if (response.data != "" && response.data != null) {
                    $scope.InvoiceSeriestype = response.data.invoiceseriesType;
                    if ($scope.InvoiceSeriestype == 2) {
                        getInvoiceSeriesItems();
                        $scope.IsSelectedInvoiceSeries = true;
                    }
                    if ($scope.InvoiceSeriestype == 1) {
                        //Newly added by Manivannan on 31-Jan-17
                        getNumericInvoiceNo();
                        $scope.IsSelectedInvoiceSeries = false;
                    }
                    if ($scope.InvoiceSeriestype == 3) {
                        $scope.IsSelectedInvoiceSeries = false;
                    }
                } else {
                    //$scope.IsSelectedInvoiceSeries = false;
                    getNumericInvoiceNo();
                    //Newly added by Manivannan on 31-Jan-17
                    $scope.IsSelectedInvoiceSeries = false;
                }
            }, function () {
            });
    }
    $scope.InvoiceSeriesItems = [];
    function getInvoiceSeriesItems() {
        newsalesService.getInvoiceSeriesItems()
            .then(function (response) {
                if (response.data != "" && response.data != null) {
                    $scope.InvoiceSeriesItems = response.data;
                    // $scope.selectedSeriesItem = $scope.InvoiceSeriesItems[0].seriesName;
                }
                getCustomSeriesSelected();
            }, function () {
            });
    }
    Checkleads();
    $scope.LeadId = "";
    $scope.SaleLeadsList = {};
    function Checkleads() {
        if (window.localStorage.getItem("ConvertLeadstoSaleObj") == null) {
        } else {
            $scope.SaleLeadsList = JSON.parse(window.localStorage.getItem("ConvertLeadstoSaleObj"));
            window.localStorage.removeItem('ConvertLeadstoSaleObj');
            $scope.leadsProductArray = $scope.SaleLeadsList.leadsProduct;
            $scope.sales.name = $scope.SaleLeadsList.name;
            $scope.sales.mobile = $scope.SaleLeadsList.mobile;
            $scope.sales.patientId = $scope.SaleLeadsList.patientId;
            $scope.sales.leadId = $scope.SaleLeadsList.id;
            if ($scope.SaleLeadsList.doctorName != null && $scope.SaleLeadsList.doctorName != undefined && $scope.SaleLeadsList.doctorName != "") {
                $scope.doctorname = $scope.SaleLeadsList.doctorName;
            }
            if ($scope.SaleLeadsList.doctorMobile != null && $scope.SaleLeadsList.doctorMobile != undefined && $scope.SaleLeadsList.doctorMobile != "") {
                $scope.sales.doctorMobile = $scope.SaleLeadsList.doctorMobile;
            }
            $scope.customerHelper.data.patientSearchData.mobile = $scope.sales.mobile;
            $scope.customerHelper.data.patientSearchData.name = $scope.sales.name;
            if ($scope.customerHelper.data.patientSearchData.mobile != undefined && $scope.customerHelper.data.patientSearchData.mobile != "") {
                $scope.customerHelper.search(false, 2, "No", 0);
            }
        }
    }
    var LeadsRequiredquantity = "";
    $scope.LeadsProductSelected = false;
    $scope.AddItemtoSave = function (obj, index) {
        $scope.LeadProductIndex = "";
        $scope.LeadProductId = "";
        $scope.LeadsProductSelected = false;
        var productname = obj.name;
        LeadsRequiredquantity = obj.Totaltablets;
        var val = productname.split(" ");
        productStockService.drugFilterData(val[0])
            .then(function (response) {
                $scope.tempproductlistArray = [];
                $scope.tempproductlistArray = response.data;
                if ($scope.tempproductlistArray.length == 0) {
                    var threecharVal = productname.slice(0, 3);
                    productStockService.drugFilterData(threecharVal)
                        .then(function (response) {
                            $scope.tempproductlistArray = [];
                            $scope.tempproductlistArray = response.data;
                            $scope.enableTempProductsPopup = true;
                            var m = ModalService.showModal({
                                "controller": "tempproductsCtrl",
                                "templateUrl": 'TempProductDetails',
                                "inputs": {
                                    "tempproductlistArray": $scope.tempproductlistArray,
                                    "productName": productname,
                                    "Requiredquantity": LeadsRequiredquantity
                                }
                            }).then(function (modal) {
                                modal.element.modal();
                                modal.close.then(function (result) {
                                    $scope.message = "You said " + result;
                                    if (result == "Yes") {
                                        $scope.LeadproductSelection(true, index, obj.id);
                                    }
                                });
                            });
                            return false;
                        }, function (error) {
                            console.log(error);
                        });
                } else {
                    $scope.enableTempProductsPopup = true;
                    var m = ModalService.showModal({
                        "controller": "tempproductsCtrl",
                        "templateUrl": 'TempProductDetails',
                        "inputs": {
                            "tempproductlistArray": $scope.tempproductlistArray,
                            "productName": productname,
                            "Requiredquantity": LeadsRequiredquantity
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            $scope.message = "You said " + result;
                            if (result == "Yes") {
                                $scope.LeadproductSelection(true, index, obj.id);
                            }
                        });
                    });
                    return false;
                }
                //productId: $scope.selectedProduct.product.id,
                //productName: $scope.selectedProduct.name,
                //items: $scope.sales.salesItem,
                //enableWindow: $scope.enablePopup
            }, function (error) {
                console.log(error);
            });
    };
    $scope.calculatePaymentDetails = function () {
        var cash1 = 0;
        var card1 = 0;
        var cheque1 = 0;
        var wallet1 = 0;
        var creditamt1 = 0;
        $scope.sales.pendingAmount = parseFloat($scope.customerHelper.data.customerBalance);
        if ($scope.sales.cashPayment.amount != null && $scope.sales.cashPayment.amount != undefined) {
            cash1 = $scope.sales.cashPayment.amount;
        }
        if ($scope.sales.cardPayment.amount != null && $scope.sales.cardPayment.amount != undefined && $scope.sales.cardPayment.amount > 0) {
            card1 = $scope.sales.cardPayment.amount;
            $scope.showCard = true;
        } else {
            $scope.showCard = false;
        }
        if ($scope.sales.chequePayment.amount != null && $scope.sales.chequePayment.amount != undefined && $scope.sales.chequePayment.amount > 0) {
            cheque1 = $scope.sales.chequePayment.amount;
            $scope.showCheque = true;
        } else {
            $scope.showCheque = false;
        }
        if ($scope.sales.walletPayment.amount != null && $scope.sales.walletPayment.amount != undefined && $scope.sales.walletPayment.amount > 0) {
            wallet1 = $scope.sales.walletPayment.amount;
            $scope.showWallet = true;
        } else {
            $scope.showWallet = false;
        }
        if ($scope.sales.creditamout != null && $scope.sales.creditamout != undefined && $scope.sales.creditamout > 0) {
            creditamt1 = $scope.sales.creditamout;
        } else {
        }
        if (cash1 == "") {
            cash1 = 0;
        }
        if (card1 == "") {
            card1 = 0;
        }
        if (cheque1 == "") {
            cheque1 = 0;
        }
        if (wallet1 == "") {
            wallet1 = 0;
        }
        if (creditamt1 == "") {
            creditamt1 = 0;
        }
        $scope.TotalAmountGiven = parseFloat(cash1) + parseFloat(card1) + parseFloat(cheque1) + parseFloat(wallet1) + parseFloat(creditamt1);
        if ($scope.sales.pendingAmountChecked && $scope.TotalAmountGiven != 0) {
            $scope.FinalNetAmount = $scope.sales.pendingAmount + $scope.tempFinalAmount;
        } else {
            $scope.FinalNetAmount = $scope.tempFinalAmount;
            if ($scope.sales.pendingAmountChecked && $scope.TotalAmountGiven == 0) {
                $scope.FinalNetAmount += $scope.sales.pendingAmount;
            }
        }
        if (parseFloat($scope.TotalAmountGiven) < parseFloat($scope.FinalNetAmount)) {
            $scope.sales.credit = parseFloat($scope.FinalNetAmount) - parseFloat($scope.TotalAmountGiven);
            $scope.balanceAmount = 0;
        } else {
            $scope.sales.credit = "";
            $scope.balanceAmount = parseFloat($scope.TotalAmountGiven) - parseFloat($scope.FinalNetAmount);
        }
        if ($scope.TotalAmountGiven > 0) {
            if ($scope.FinalNetAmount > $scope.TotalAmountGiven) {
                $scope.EnablePayBtn = false;
            } else {
                $scope.EnablePayBtn = true;
            }
        } else {
            $scope.EnablePayBtn = false;
        }
    };
    $("#expiryDate").keyup(function (e) {
        if ($(this).val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46)) {
                $(this).val($(this).val() + "/");
            }
        }
        var dt1 = value;
        var parts = toDateSplit(dt1);
        if (parts[0] - 1 > 12 || dt1.length != 5 || dt1[2] != '/') {
            $scope.sales.cardDateFormatError = true;
        } else {
            $scope.sales.cardDateFormatError = false;
        }
        if (dt1 == null || dt1 == "") {
            $scope.sales.cardDateFormatError = false;
            $scope.sales.cardDateError = false;
        }
    });
    $("#chequeDate").keyup(function (e) {
        if (($(this).val().length == 2) || ($(this).val().length == 5)) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $(this).val($(this).val() + "/");
        }
        var dt1 = $(this).val();
        var parts = toDateSplit(dt1);
        if (parts[1] - 1 > 12 || parts[0] > 31 || dt1.length != 10 || dt1[2] != '/' || dt1[5] != '/') {
            $scope.sales.chequeDateFormatError = true;
        } else {
            $scope.sales.chequeDateFormatError = false;
        }
        if (dt1 == null || dt1 == "") {
            $scope.sales.chequeDateFormatError = false;
            $scope.sales.chequeDateError = false;
        }
    });
    $('#reminderDate').keypress(function (event) {

        if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
            event.preventDefault(); //stop character from entering input
        }

    });
    $("#reminderDate").keyup(function (e) {



        if (($(this).val().length == 2) || ($(this).val().length == 5)) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $(this).val($(this).val() + "/");
        }
        var dt1 = $(this).val();
        var parts = toDateSplit(dt1);
        if (parts[1] - 1 > 12 || parts[0] > 31 || dt1.length != 10 || dt1[2] != '/' || dt1[5] != '/') {
            $scope.ReminderDateFormatError = true;
        } else {
            $scope.ReminderDateFormatError = false;
        }
        if (dt1 == null || dt1 == "") {
            $scope.ReminderDateFormatError = false;
            $scope.isInvoicedatevalid1 = false;
        }
    });
    //$("#txtBillDate").keyup(function (e) {
    //    if (($(this).val().length == 2) || ($(this).val().length == 5)) {
    //        if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
    //            $(this).val($(this).val() + "/");
    //    }
    //    var dt1 = $(this).val();
    //    var parts = toDateSplit(dt1);
    //    if (parts[1] - 1 > 12 || parts[0] > 31 || dt1.length != 10 || dt1[2] != '/' || dt1[5] != '/') {
    //        $scope.BillDateFormatError = true;
    //    } else {
    //        $scope.BillDateFormatError = false;
    //    }
    //    if (dt1 == null || dt1 == "") {
    //        $scope.BillDateFormatError = false;
    //        $scope.isInvoicedatevalid = false;
    //    }
    //});
    $('#txtBillDate').keypress(function (event) {

        if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
            event.preventDefault(); //stop character from entering input
        }

    });
    $("#txtBillDate").keyup(function (e) {
        if ($(this).val() == "") {
            $scope.isInvoicedatevalid = false;
        } else {
            if ($(this).val().length == 2 || $(this).val().length == 5) {
                if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                    $(this).val($(this).val() + "/");
            }
            var value = $(this).val();
            var splits = value.split("/");
            if (splits[1] > "12") {
                $scope.isInvoicedatevalid = true;
                toastr.error('Month Should Be Below 13', { timeOut: 1000 });
                return false;
            }
            else {
                $scope.isInvoicedatevalid = false;
            }
            if (splits[1] <= "12" && splits[0] > "31") {
                $scope.isInvoicedatevalid = true;
                toastr.error('Invalid Date', { timeOut: 1000 });
                return false;
            }
            if (splits[1].length < 4) {
                toastr.error('Invalid Date', { timeOut: 1000 });
                $scope.isInvoicedatevalid = true;
                return false;
            }
            else {
                $scope.isInvoicedatevalid = false;
            }
        }
    });
    function toDate(dateStr) {
        if (dateStr != null) {
            dateStr = $filter('date')(new Date(dateStr), "yyyy-MM-dd");
            var parts = dateStr.split("-");
            return new Date(parts[0], parts[1] - 1, parts[2]);
        }
    }
    function toDateSplit(dateStr) {
        if (dateStr != null) {
            var parts = dateStr.split("/");
            return parts;
        }
    }
    $scope.checkExpiryDate = function (cardDate) {
        var val1 = new Date(toDate(cardDate));
        var val2 = document.getElementById("expiryDate").value;
        if (val1.getFullYear() > new Date().getFullYear()) {
            $scope.sales.cardDateError = false;
        } else if (val1.getMonth() >= new Date().getMonth() && val1.getFullYear() == new Date().getFullYear()) {
            $scope.sales.cardDateError = false;
        } else {
            $scope.sales.cardDateError = true;
        }
        if (cardDate == null && val2.length == 0) {
            $scope.sales.cardDateError = false;
            $scope.sales.cardDateFormatError = false;
        }
        if (val2.length == 5 && val2[2] == '/') {
            $scope.sales.cardDateFormatError = false;
        }
    };
    $scope.checkChequeDate = function (dates) {
        var val1 = new Date(toDate(dates));
        var val2 = new Date();
        val2.setMonth(val2.getMonth() - 5);
        var val3 = document.getElementById("chequeDate").value;
        if (val1 >= val2) {
            $scope.sales.chequeDateError = false;
        } else {
            $scope.sales.chequeDateError = true;
        }
        if (dates == null && val3.length == 0) {
            $scope.sales.chequeDateError = false;
            $scope.sales.chequeDateFormatError = false;
        }
        if (val3.length == 8 && val3[2] == '/' && val3[5] == '/') {
            $scope.sales.chequeDateFormatError = false;
        }
    };
    //validateBilldate
    $scope.ReminderDate = function (dates) {
        console.log("change event");
        var val1 = new Date(toDate(dates));
        val1 = $filter('date')(new Date(val1), "yyyy-MM-dd");
        var val2 = new Date();
        val2 = $filter('date')(new Date(val2), "yyyy-MM-dd");
        var val3 = document.getElementById("reminderDate").value;
        if (val1 >= val2) {
            $scope.isInvoicedatevalid1 = false;
        } else {
            $scope.isInvoicedatevalid1 = true;
        }
        if (dates == null && val3.length == 0) {
            $scope.isInvoicedatevalid1 = false;
            $scope.ReminderDateFormatError = false;
        }
        if (val3.length == 10 && val3[2] == '/' && val3[5] == '/') {
            $scope.ReminderDateFormatError = false;
        }
    };
    $scope.validateBilldate = function (dates) {
        var val1 = new Date(toDate(dates));
        val1 = $filter('date')(new Date(val1), "yyyy-MM-dd");
        var val2 = new Date();
        val2 = $filter('date')(new Date(val2), "yyyy-MM-dd");
        var val3 = document.getElementById("txtBillDate").value;
        if (val1 > val2) {
            toastr.error('Future Date Not Allowed', { timeOut: 1000 })
            $scope.isInvoicedatevalid = true;
        }
        else {
            $scope.isInvoicedatevalid = false;
        }
        if (dates == null && val3.length == 0) {
            $scope.isInvoicedatevalid = false;
            $scope.BillDateFormatError = false;
        }
        if (val3.length == 10 && val3[2] == '/' && val3[5] == '/') {
            $scope.BillDateFormatError = false;
        }
    };
    $scope.DoctorsList = function (val) {
        if ($scope.DoctorNameSearch == "" || $scope.DoctorNameSearch == undefined) {
            $scope.DoctorNameSearch = 2;
        }
        if ($scope.DoctorNameSearch == 1) {
            return doctorService.Locallist(val)
                .then(function (response) {
                    //return response.data;
                    var origArr = response.data;
                    var newArr = [],
                    origLen = origArr.length,
                    found, x, y;
                    for (x = 0; x < origLen; x++) {
                        found = undefined;
                        for (y = 0; y < newArr.length; y++) {
                            if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            newArr.push(origArr[x]);
                        }
                    }
                    return newArr.map(function (item) {
                        return item;
                    });
                }, function (error) {
                    console.log(error);
                });
        } else {
            return doctorService.list(val)
                .then(function (response) {
                    var origArr = response.data;
                    var newArr = [],
               origLen = origArr.length,
               found, x, y;
                    for (x = 0; x < origLen; x++) {
                        found = undefined;
                        for (y = 0; y < newArr.length; y++) {
                            if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            newArr.push(origArr[x]);
                        }
                    }
                    return newArr.map(function (item) {
                        return item;
                    });
                }, function (error) {
                    console.log(error);
                });
        }
    };
    $scope.onDoctorSelect = function (obj, event) {
        $scope.sales.doctorMobile = obj.mobile;
        ele = document.getElementById("doctorMobile");
        ele.focus();
        $scope.isdoctornameMandatory = false;
    };
    getPrintType = function () {
        newsalesService.getPrintType()
            .then(function (response) {
                $scope.isDotMatrix = response.data;
                window.localStorage.setItem("IsDotMatrix", JSON.stringify($scope.isDotMatrix));
            }, function () {
            });
    };
    getPrintType();
    var seperator = "/";
    $scope.isInvoicedatevalid = false;
    $scope.isInvoicedatevalid1 = false;
    $scope.ReminderDateFormatError = false;
    $scope.validatedate = function () {
        var val = document.getElementById("txtBillDate").value;
        if (val.length == 10) {
            validatedate(val);
        }
    };
    $scope.validateRemdate = function () {
        var val = document.getElementById("reminderDate").value;
        if (val.length == 10) {
            validatedate(val);
        }
    };
    function validatedate(val) {
        var splits = val.split("/");
        var dt = new Date(splits[1] + "/" + splits[0] + "/" + splits[2]);
        if (dt == "Invalid Date") {
            $scope.isInvoicedatevalid = true;
            return false;
        } else {
            if (dt.getDate() == splits[0] && dt.getMonth() + 1 == splits[1] && dt.getFullYear() == splits[2]) {
                $scope.isInvoicedatevalid = false;
            } else {
                $scope.isInvoicedatevalid = true;
                return;
            }
            FutureDateValidation(dt);
        }
    }
    function FutureDateValidation(dt) {
        var dtToday = new Date();
        var pastDate = new Date(Date.parse(dtToday.getMonth() + "/" + dtToday.getDate() + "/" + parseInt(dtToday.getFullYear() - 100)));
        if (dt < pastDate || dt >= dtToday) {
            $scope.isInvoicedatevalid = true;
        } else {
            $scope.isInvoicedatevalid = false;
        }
    }
    getIsMaxDiscountAvail();
    $scope.maxDiscountFixed = "No";
    function getIsMaxDiscountAvail() {
        newsalesService.getIsMaxDiscountAvail()
            .then(function (response) {
                if (response.data == "" || response.data == null || response.data == undefined) {
                    $scope.maxDiscountFixed = "No";
                } else {
                    if (response.data.maxDiscountAvail != undefined) {
                        $scope.maxDiscountFixed = response.data.maxDiscountAvail;
                    } else {
                        $scope.maxDiscountFixed = "No";
                    }
                }
                if ($scope.maxDiscountFixed == "Yes") {
                    getMaxDiscountValue();
                }
            }, function () {
            });
    }
    $scope.maxDisountValue = "";
    function getMaxDiscountValue() {
        newsalesService.getMaxDiscountValue()
            .then(function (response) {
                if (response.data != "" && response.data != null) {
                    $scope.maxDisountValue = response.data.instanceMaxDiscount;
                }
            }, function () {
            });
    }
    $scope.billList = [];
    $scope.printBill = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        $scope.search.searchProductId = "";
        newsalesService.list($scope.search)
            .then(function (response) {
                if (response.data.list !== undefined) {
                    $scope.billList = response.data.list[0];
                }
                printingHelper.printInvoice($scope.billList.id);
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
    };
    $scope.chkPrint = function () {
        $scope.sales.billPrint = $scope.chkPrintValue;
    };
    function getIndexOf(arr, val, prop) {
        var l = arr.length,
          k = 0;
        for (k = 0; k < l; k = k + 1) {
            if (arr[k][prop] === val) {
                return k;
            }
        }
        return false;
    }
    $scope.isDepartmentsave = "0";
    getIsDepartmentadded();
    function getIsDepartmentadded() {
        newsalesService.getIsDepartmentadded()
            .then(function (response) {
                if (response.data == "" || response.data == null || response.data == undefined) {
                    $scope.isDepartmentsave = "0";
                } else {
                    if (response.data.patientTypeDept != undefined) {
                        $scope.isDepartmentsave = "0";
                        if (response.data.patientTypeDept == true) {
                            $scope.isDepartmentsave = "1";
                        }
                    } else {
                        $scope.isDepartmentsave = "0";
                    }
                }
            }, function () {
            });
    }
    $scope.checkbox = [];
    $scope.checked = function (index) {
        //for(i=0;i<$scope.sales.salesItem.length;i++)
        //{
        //    if (index == i && $scope.checkbox[index] == "YES")
        //    {
        //        //alert(JSON.stringify($scope.sales.salesItem[i]));
        //        $scope.sales.salesItem[i].isReminder = true;
        //    }
        //}
        if ($scope.sales.salesItem[index].isReminderAvail == false) {
            $scope.sales.salesItem[index].isReminderAvail = true;
        } else {
            $scope.sales.salesItem[index].isReminderAvail = false;
        }
    };
    //added by arun for open shortcut list popup
    $scope.showkeyshortcutlist = false;
    $scope.openshortcutlistPopup = function () {
        $scope.showkeyshortcutlist = true;
    };
    $scope.closeShortcutPopup = function () {
        $scope.showkeyshortcutlist = false;
        $scope.FocusonProduct();
    };
    $scope.paymentSection = function (nextid, currid) {
        if ($scope.showCard == true && currid == "card") {
            var dummyid = document.querySelector('.clsCardpayment').id;
            nextid = dummyid;
        }
        if ($scope.showCheque == true && currid == "cheque") {
            nextid = "chequeNumber";
        }
        if ($scope.showWallet == true && currid == "walletAmt") {
            nextid = "walletTransId";
        }
        if (nextid == "credit") {
            if ($scope.hasCustomerData() == true) {
                //nextid = "btnSubmitwithPrint";
                nextid = "counter";
                $scope.sales.deliveryType = "Counter";
                $scope.salesTypeFocused = true;
            } else {
                nextid = "credit";
            }
        }
        if (nextid != "") {
            ele = document.getElementById(nextid);
            ele.focus();
        }
    };
    $scope.paymentKeyPress = function (e, previousid, nextid, currentid) {
        //Down arrow
        if (e.keyCode == 40) {
            ele = document.getElementById(nextid);
            ele.focus();
        }
        //Up arrow
        if (e.keyCode == 38) {
            ele = document.getElementById(previousid);
            ele.focus();
        }
        //Enter Key
        if (e.keyCode == 13) {
            event.preventDefault();
            ele = document.getElementById("btnSubmitwithPrint");
            ele.focus();
        }
    };
    $scope.finalKeyPress = function (e, previousid, nextid, currentid) {
        //Left arrow
        if (e.keyCode == 37) {
            ele = document.getElementById(previousid);
            ele.focus();
        }
        //Rigth Arrow
        if (e.keyCode == 39) {
            ele = document.getElementById(nextid);
            ele.focus();
        }
    };
    $scope.btnClosePaymentpop = function () {
        $scope.showPayment = false;
        $scope.FocusonProduct();
    };
    $scope.ReciptModeFocused = false;
    $scope.focusReciptMode = function () {
        $scope.sales.sendEmail = true;
        document.getElementById("custom_checkboxemail").focus();
        $scope.ReciptModeFocused = true;
        $scope.salesTypeFocused = false;
    };
    $scope.focusCustomerType = function () {
        $scope.ReciptModeFocused = false;
        $scope.SMSFocued = false;
        $scope.MAILfocused = false;
        if ($scope.sales.saleType.salesTypeList.length != 0) {
            document.getElementById("customerType").focus();
        } else if ($scope.InvoiceSeriesItems.length != 0) {
            document.getElementById("ddlCustomerSeries").focus();
        } else {
            document.getElementById("btnSubmitwithoutPrint").focus();
        }
    };
    $scope.changeRecipetMode = function () {
        $scope.ReciptModeFocused = true;
    };
    $scope.closeBatchPopUp = function () {
        $scope.batchList1.length == 0;
        $scope.showBatchGrid = false;
        $scope.isPopupOpened = false;
        $scope.FocusonProduct();
    };
    $scope.focusCurentRow = function (index) {
        $scope.isSelectedItemMain = index;
    };
    $scope.btnCloseQtypop = function () {
        $scope.showQty = false;
        $scope.FocusonProduct();
    };
    $scope.focusNextField = function (nextid) {
        ele = document.getElementById(nextid);
        ele.focus();
    };
    $scope.checkPendingAmount = function () {
        if ($scope.sales.pendingAmountChecked) {
            $scope.sales.credit += parseFloat($scope.customerHelper.data.customerBalance);
        } else {
            $scope.sales.credit -= parseFloat($scope.customerHelper.data.customerBalance);
        }
    };
    $scope.getGSTIN = function () {
        salesService.getGSTINDetail()
            .then(function (response) {
                var m = ModalService.showModal({
                    "controller": "updateGSTINPopupCtrl",
                    "templateUrl": 'NewupdateGSTINPopup',
                    "inputs": {
                        "gstIn": response.data
                    }
                }).then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                        $scope.message = "You said " + result;
                    });
                });
                return false;
            }, function (error) {
                console.log(error);
            });
    };
    $scope.ReturnProduct = function () {
        //if ($scope.sales.salesItem.length > 0) {
        //    for (var i = 0; i < $scope.sales.salesItem.length; i++) {
        //        if ($scope.sales.salesItem[i].salesReturn) {
        //            $scope.sales.salesItem[i].quantity = $scope.sales.salesItem[i].quantity * (-1);
        //        }
        //    }
        //}
        var m = ModalService.showModal({
            "controller": "productReturnCtrl",
            "templateUrl": 'NewproductReturn',
            "inputs": {
                "salesItem": $scope.sales.salesItem,
                "gstEnable": $scope.GSTEnabled,
                "patientType": newcustomerHelper.data.selectedCustomer.patientType  //Patient Type Added by Poongodi on 30/05/2017
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.returnItems = result;
                if ($scope.returnItems != null) {
                    $scope.addReturnItems();
                }
            });
        });
    };
    $scope.addReturnItems = function () {
        if ($scope.returnItems.length > 0) {
            $scope.returnDiscountInValid = true;
            for (var x = 0; x < $scope.returnItems.length; x++) {
                if ($scope.returnItems[x].editId == null || $scope.returnItems[x].editId == undefined) {
                    $scope.returnItems[x].editId = $scope.editId++;
                    $scope.sales.salesItem.push($scope.returnItems[x]);
                }
            }

            $scope.product.items.length = 0;
            for (var i = 0; i < $scope.sales.salesItem.length + 1; i++) {
                $scope.product.items.push(angular.copy(productItem));
            }
            for (var j = 0; j < $scope.sales.salesItem.length; j++) {
                $scope.product.items[j].sno = j + 1;
                $scope.product.items[j].rowid = j + 1;
                $scope.product.items[j].schedule = "";
                $scope.product.items[j].id = $scope.sales.salesItem[j].id;
                $scope.product.items[j].accountId = $scope.sales.salesItem[j].accountId;
                $scope.product.items[j].name = $scope.sales.salesItem[j].name;
                $scope.product.items[j].product = $scope.sales.salesItem[j].name;
                $scope.product.items[j].vatInPrice = $scope.sales.salesItem[j].vatInPrice;
                $scope.product.items[j].discount = $scope.sales.salesItem[j].discount;
                $scope.product.items[j].age = $scope.sales.salesItem[j].age;
                $scope.product.items[j].totalStock = $scope.sales.salesItem[j].totalStock;
                $scope.product.items[j].soldqty = $scope.sales.salesItem[j].soldqty;
                $scope.product.items[j].productId = $scope.sales.salesItem[j].productId;
                $scope.product.items[j].vendorId = $scope.sales.salesItem[j].vendorId;
                $scope.product.items[j].batchNo = $scope.sales.salesItem[j].batchNo;
                $scope.product.items[j].expireDate = $scope.sales.salesItem[j].expireDate;
                $scope.product.items[j].rackNo = $scope.sales.salesItem[j].product.rackNo;
                $scope.product.items[j].boxNo = $scope.sales.salesItem[j].product.boxNo;
                $scope.product.items[j].rackBox = $scope.sales.salesItem[j].product.rackBox;
                $scope.product.items[j].sellingPrice = $scope.sales.salesItem[j].sellingPrice;
                $scope.product.items[j].stock = $scope.sales.salesItem[j].stock;
                $scope.product.items[j].packageSize = $scope.sales.salesItem[j].packageSize;
                $scope.product.items[j].packagePurchasePrice = $scope.sales.salesItem[j].packagePurchasePrice;
                $scope.product.items[j].purchasePrice = $scope.sales.salesItem[j].purchasePrice;
                $scope.product.items[j].offlineStatus = $scope.sales.salesItem[j].offlineStatus;
                $scope.product.items[j].cst = $scope.sales.salesItem[j].cst;


            
              

                $scope.product.items[j].totalQuantity = $scope.sales.salesItem[j].totalQuantity;
                $scope.product.items[j].availableQty = $scope.sales.salesItem[j].availableQty;
                $scope.product.items[j].igst = $scope.sales.salesItem[j].igst;
                $scope.product.items[j].cgst = $scope.sales.salesItem[j].cgst;
                $scope.product.items[j].sgst = $scope.sales.salesItem[j].sgst;
                $scope.product.items[j].gstTotal = $scope.sales.salesItem[j].gstTotal;
                $scope.product.items[j].reminderFrequency = 'None';
                $scope.product.items[j].reminderDate = "";
                $scope.product.items[j].isLastRow = false;
                $scope.product.items[j].isQuantityFocused = false;
                $scope.product.items[j].isMrpFocused = false;
                $scope.product.items[j].isDiscountFocused = false;

                if ($scope.sales.salesItem[j].salesReturn != undefined) {
                    $scope.product.items[j].salesReturn = $scope.sales.salesItem[j].salesReturn;
                }


                $scope.product.items[j].quantity = $scope.sales.salesItem[j].quantity;
                if ($scope.product.items[j].salesReturn == true) {
                    $scope.product.items[j].quantity = $scope.product.items[j].quantity * (-1);
                }
                $scope.sales.salesItem[j].rowid = j + 1;
            }
            $scope.BillCalculations();
            $scope.sales.discount = 0;
            if ($scope.sales.discountValue == undefined) {
                $scope.sales.discountValue = 0;
            }
            $scope.DiscountCalculations1();
        } else {
            $scope.returnDiscountInValid = false;
        }
        $scope.returnItems = null;
    };
});