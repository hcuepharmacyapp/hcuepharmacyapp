app.controller('salesCompleteMobileVerifyCtrl', function ($scope, $rootScope, close, toastr, cacheService, ModalService, salesService) {

    $scope.focustxtProduct = true;

    $scope.mobileNo = null;
    $scope.otp = null;
    $scope.showOtp = false;


    $scope.sendOTP = function () {
        salesService.sendOtp($scope.mobileNo).then(function (response) {
            $scope.showOtp = true;
            if (response.data == true) {                
                toastr.success("OTP successfully sent to mobile");
            } else {
                toastr.error("Error");
            }

        }, function (error) {
            console.log(error);
            toastr.error("Error");
        });
    }


    $scope.validateOTP = function () {
        salesService.validateOtp($scope.otp, $scope.mobileNo).then(function (response) {

            if (response.data == true) {
                toastr.success("OTP matches correctly");
                $scope.close("Yes");
            } else {
                toastr.error("Error");
                $scope.close("No");
            }

        }, function (error) {
            console.log(error);
            toastr.error("Error");
            $scope.close("No");
        });
    }


    $scope.close = function (result) {
        close(result, 500);
        if (result == "Yes") {            
            cacheService.set('mobileNo', $scope.mobileNo);
        } else {
            cacheService.set('mobileNo', '');
        }
        $(".modal-backdrop").hide();
    };


});