app.controller('salesPaymentCtrl', function ($scope, $filter, $rootScope, close, toastr, cacheService, ModalService, salesService, pendingAmt, customerBalanceAmt, salesPaymentModel, salesModel, salesPaymentDetails, domainValuesModel) {

    $scope.focusInput1 = true;
    
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.sales1 = salesModel;

    $scope.allowComplete = false;
    $scope.applyClass = "darkgray-button mr-10 mb-10";

    $scope.showPayment = true;
    $scope.sales1.cashPayment = angular.copy(salesPaymentModel);
    $scope.sales1.cardPayment = angular.copy(salesPaymentModel);
    $scope.sales1.chequePayment = angular.copy(salesPaymentModel);
    $scope.sales1.walletPayment = angular.copy(salesPaymentModel);
    $scope.sales1.creditPayment = angular.copy(salesPaymentModel);
    $scope.sales1.walletPayment.subPaymentInd = "";
    $scope.sales1.salesPayments = [];
    $scope.sales1.credit = "";
    $scope.showCard = false;
    $scope.showCheque = false;
    $scope.showWallet = false;
    $scope.balanceAmount = "";
    $scope.sales1.cardDateError = false;
    $scope.sales1.cardDateFormatError = false;
    $scope.sales1.chequeDateError = false;
    $scope.sales1.chequeDateFormatError = false;
    $scope.sales1.creditamout = null;
    $scope.sales1.printStatus = false;

    $scope.pendingAmount = pendingAmt;
    $scope.customerBalanceAmount = customerBalanceAmt;

    $scope.domainValueList = [];
    $scope.eWalletOptions = [];

    $scope.getPaymentTypes = function () {
        salesService.getPaymentDomainValues().then(function (response) {
            if (response.data.length > 0) {
                $scope.domainValueList = response.data;

                angular.forEach($scope.domainValueList, function (value, key) {
                    if (value.domainId.replace(/\s+/g, '') == "2") {
                        $scope.eWalletOptions.push(value);
                    }
                });
            }

        }, function(error){
            console.log(error);
        })
    }

    $scope.getPaymentTypes();

    $scope.savePayment = function () {

        if (($scope.sales1.walletPayment.amount > 0) && ($scope.sales1.walletPayment.subPaymentInd == null || $scope.sales1.walletPayment.subPaymentInd == undefined || $scope.sales1.walletPayment.subPaymentInd == "")) {
            toastr.info("Select eWallet option");
            return;
        }

        if ($scope.sales1.cashPayment.amount > 0) {            

            if ($scope.balanceAmount <= $scope.sales1.cashPayment.amount) {
                $scope.sales1.cashPayment.amount = $scope.sales1.cashPayment.amount - $scope.balanceAmount;
                if ($scope.sales1.cashPayment.amount > 0) {
                    $scope.sales1.cashPayment.paymentInd = 1;
                    $scope.sales1.salesPayments.push($scope.sales1.cashPayment);
                }                 
            }
        }
        if ($scope.sales1.cardPayment.amount > 0) {
            $scope.sales1.cardPayment.paymentInd = 2;
            $scope.sales1.salesPayments.push($scope.sales1.cardPayment);
        }
        if ($scope.sales1.chequePayment.amount > 0) {
            $scope.sales1.chequePayment.paymentInd = 3;
            $scope.sales1.salesPayments.push($scope.sales1.chequePayment);
        }
        if ($scope.sales1.walletPayment.amount > 0) {
            $scope.sales1.walletPayment.paymentInd = 5;
            $scope.sales1.salesPayments.push($scope.sales1.walletPayment);
        }
        if ($scope.sales1.creditamout > 0) {
            $scope.sales1.creditPayment.paymentInd = 4;
            $scope.sales1.creditPayment.amount = $scope.sales1.creditamout;
            $scope.sales1.salesPayments.push($scope.sales1.creditPayment);
        }

        $scope.close($scope.sales1);

    };

    $scope.savePaymentPrint = function () {

        if (($scope.sales1.walletPayment.amount > 0) && ($scope.sales1.walletPayment.subPaymentInd == null || $scope.sales1.walletPayment.subPaymentInd == undefined || $scope.sales1.walletPayment.subPaymentInd == "")) {
            toastr.info("Select eWallet option");
            return;
        }
        $scope.sales1.printStatus = true;
        $scope.savePayment();
    };

    angular.element(document).keydown(function (e) {
        if (e.keyCode == 27) { //ESC (escape)
            event.preventDefault();
            $scope.close('No');
        }
    });

    $scope.clear = function () {
        $scope.sales1.cashPayment = angular.copy(salesPaymentModel);
        $scope.sales1.cardPayment = angular.copy(salesPaymentModel);
        $scope.sales1.chequePayment = angular.copy(salesPaymentModel);
        $scope.sales1.walletPayment = angular.copy(salesPaymentModel);
        $scope.sales1.creditPayment = angular.copy(salesPaymentModel);
        $scope.sales1.walletPayment.subPaymentInd = "";
        $scope.sales1.salesPayments = [];
        $scope.sales1.credit = "";
        $scope.showCard = false;
        $scope.showCheque = false;
        $scope.showWallet = false;
        $scope.balanceAmount = "";
        $scope.sales1.cardDateError = false;
        $scope.sales1.cardDateFormatError = false;
        $scope.sales1.chequeDateError = false;
        $scope.sales1.chequeDateFormatError = false;
        $scope.sales1.creditamout = null;
        $scope.sales1.printStatus = false;
        $scope.allowComplete = false;        
        $scope.applyClass = "darkgray-button mr-10 mb-10";
        $scope.TotalAmountGiven = 0;

        document.getElementById("expiryDatepay").value = null;
        document.getElementById("chequeDatepay").value = null;

        ele = document.getElementById("cashpay");
        ele.focus();
    }
    
    $scope.checkExpiryDate = function (cardDate) {
        $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.expireDate = $filter('date')(cardDate, 'dd/MM/yyyy');
        var val1 = new Date($scope.formatString($scope.expireDate));        
        var val2 = ($scope.paymentDetails.expiryDatepay.$viewValue);

        if (val1.getFullYear() > new Date().getFullYear()) {
            $scope.sales1.cardDateError = false;
        } else if (val1.getMonth() >= new Date().getMonth() && val1.getFullYear() == new Date().getFullYear()) {
            $scope.sales1.cardDateError = false;
        } else {
            $scope.sales1.cardDateError = true;
        }
        if (cardDate == null && val2.length == 0) {
            $scope.sales1.cardDateError = false;
            $scope.sales1.cardDateFormatError = false;
        }
        if (val2.length == 5 && val2[2] == '/') {
            $scope.sales1.cardDateFormatError = false;
        }
        checkCompleteSale();
    };
       
    function toDate(dateStr) {
        if (dateStr != null) {
            var parts = dateStr.split("-");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
    }
           
    $scope.checkChequeDate = function (e) {

        if ($("#chequeDatepay").val().length == 2 || $("#chequeDatepay").val().length == 5) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $("#chequeDatepay").val($("#chequeDatepay").val() + "/");
        }

        var dt1 = $scope.paymentDetails.chequeDatepay.$viewValue;        

        var chequeDate1 = $filter('date')(dt1, 'dd/MM/yyyy');
        var val1 = new Date($scope.formatString1(chequeDate1));
        var val2 = new Date();
        val2.setMonth(val2.getMonth() - 5);
        var val3 = document.getElementById("chequeDatepay").value;
        
        var parts = val3.split("/");

        if (val1 >= val2) {
            $scope.sales1.chequeDateError = false;
        } else {
            $scope.sales1.chequeDateError = true;
        }
        
        if (val3.length == 8 && val3[2] == '/' && val3[5] == '/') {
            if (parts[1] - 1 >= 12 || parts[0] > 31) {
                $scope.sales1.chequeDateFormatError = true;
            }else{
                $scope.sales1.chequeDateFormatError = false;
            }            
        } else {
            $scope.sales1.chequeDateFormatError = true;
        }

        if (dt1 == null || dt1 == "" || dt1.length == 0) {
            $scope.sales1.chequeDateFormatError = false;
            $scope.sales1.chequeDateError = false;
        }

        checkCompleteSale();
    };
    
       
    $scope.keyUp = function (e) {
        var dt = $("#expiryDatepay").val();
        if ($("#expiryDatepay").val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $("#expiryDatepay").val($("#expiryDatepay").val() + "/");
        }
        var dt1 = $scope.paymentDetails.expiryDatepay.$viewValue;
        var parts = dt1.split("/");
        if (parts != undefined) {
            if (parts[0] - 1 >= 12 || dt1.length != 5 || dt1[2] != '/') {
                $scope.sales1.cardDateFormatError = true;
            } else {
                $scope.sales1.cardDateFormatError = false;
            }
        }
        
        if (dt1 == null || dt1 == "" || dt1.length == 0) {
            $scope.sales1.cardDateFormatError = false;
            $scope.sales1.cardDateError = false;
        }         
        checkCompleteSale();
    };   

    $scope.close = function (result) {
        if (result == "No") {
            result = null;
        }
        close(result, 100); // close, but give 100ms for bootstrap to animate
        
        $(".modal-backdrop").hide();
    };    

    $scope.calculatePaymentDetails = function () {
        var cash1 = 0;
        var card1 = 0;
        var cheque1 = 0;
        var wallet1 = 0;
        var creditamt1 = 0;
        $scope.sales1.pendingAmount = parseFloat(pendingAmt);
        if ($scope.sales1.cashPayment.amount != null && $scope.sales1.cashPayment.amount != undefined) {
            cash1 = $scope.sales1.cashPayment.amount;
        }
        if ($scope.sales1.cardPayment.amount != null && $scope.sales1.cardPayment.amount != undefined && $scope.sales1.cardPayment.amount > 0) {
            card1 = $scope.sales1.cardPayment.amount;
            $scope.showCard = true;
        } else {
            $scope.showCard = false;
        }
        if ($scope.sales1.chequePayment.amount != null && $scope.sales1.chequePayment.amount != undefined && $scope.sales1.chequePayment.amount > 0) {
            cheque1 = $scope.sales1.chequePayment.amount;
            $scope.showCheque = true;
        } else {
            $scope.showCheque = false;
        }
        if ($scope.sales1.walletPayment.amount != null && $scope.sales1.walletPayment.amount != undefined && $scope.sales1.walletPayment.amount > 0) {
            wallet1 = $scope.sales1.walletPayment.amount;
            $scope.showWallet = true;
        } else {
            $scope.showWallet = false;
        }
        if ($scope.sales1.creditamout != null && $scope.sales1.creditamout != undefined && $scope.sales1.creditamout > 0) {
            creditamt1 = $scope.sales1.creditamout;
        } else {
        }
        if (cash1 == "") {
            cash1 = 0;
        }
        if (card1 == "") {
            card1 = 0;
        }
        if (cheque1 == "") {
            cheque1 = 0;
        }
        if (wallet1 == "") {
            wallet1 = 0;
        }
        if (creditamt1 == "") {
            creditamt1 = 0;
        }
        $scope.TotalAmountGiven = parseFloat(cash1) + parseFloat(card1) + parseFloat(cheque1) + parseFloat(wallet1) + parseFloat(creditamt1);

        $scope.FinalNetAmount1 = parseFloat(pendingAmt).toFixed(2);
        if (parseFloat($scope.TotalAmountGiven) < parseFloat($scope.FinalNetAmount1)) {
            $scope.sales1.credit = parseFloat($scope.FinalNetAmount1) - parseFloat($scope.TotalAmountGiven);
            $scope.balanceAmount = 0;
        } else {
            $scope.sales1.credit = "";
            $scope.balanceAmount = parseFloat($scope.TotalAmountGiven) - parseFloat($scope.FinalNetAmount1);

            //if ($scope.sales1.cashPayment.amount > 0) {
            //    if ($scope.balanceAmount <= $scope.sales1.cashPayment.amount) {
            //        $scope.sales1.cashPayment.amount = $scope.sales1.cashPayment.amount - $scope.balanceAmount;
            //        $scope.balanceAmount = 0;
            //    }
            //}
        }        

        checkCompleteSale();
    };

    function checkCompleteSale() {
        if (($scope.sales1.credit > 0) || ($scope.balanceAmount > $scope.sales1.cashPayment.amount || $scope.sales1.cardDateError || $scope.sales1.cardDateFormatError || $scope.sales1.chequeDateError || $scope.sales1.chequeDateFormatError)) {
            $scope.allowComplete = false;
            $scope.applyClass = "darkgray-button mr-10 mb-10";
            if ($scope.balanceAmount > $scope.sales1.cashPayment.amount) {
                toastr.info("Given amount is greater than Net amount");
            }

        } else {
            $scope.allowComplete = true;
            $scope.applyClass = "green-button mr-10";
        }
    }

    $scope.paymentSection = function (nextid, currid) {
        if ($scope.showCard == true && currid == "card") {
            var dummyid = document.querySelector('.clsCardpayment').id;
            nextid = dummyid;
        }
        if ($scope.showCheque == true && currid == "cheque") {
            nextid = "chequeNumber";
        }
        if ($scope.showWallet == true && currid == "walletAmt") {
            nextid = "walletTransId";
        }
        if (nextid == "credit") {
            if ($scope.hasCustomerData() == true) {
                //nextid = "btnSubmitwithPrint";
                nextid = "counter";
                $scope.sales1.deliveryType = "Counter";
                $scope.salesTypeFocused = true;
            } else {
                nextid = "credit";
            }
        }
        if (nextid != "") {
            ele = document.getElementsByClassName("card");
            ele.focus();
        }
    };

    $scope.expDateFormat = function (e) {

        var TXT = document.getElementById("expDate");

        if (TXT.length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $scope.temp.productStock.expireDate = TXT + "/";
        }
    };
        
    $scope.getStrLength = function (text, e) {
        if (text.length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $scope.inputText = text + "/";
        }
    };
    $scope.formatString = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    };   
   
    $scope.formatString1 = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt("20" + format.substring(6, 8));
        var date = new Date(year, month - 1, day);
        return date;
    };

    if (salesPaymentDetails.length > 0) {

        for (var i = 0; i < salesPaymentDetails.length; i++) {

            if (salesPaymentDetails[i].paymentInd == 1 && salesPaymentDetails[i].amount > 0) {
                $scope.sales1.cashPayment.paymentInd = salesPaymentDetails[i].paymentInd;
                $scope.sales1.cashPayment.amount = parseFloat(salesPaymentDetails[i].amount).toFixed(2);
            }
            if (salesPaymentDetails[i].paymentInd == 2 && salesPaymentDetails[i].amount > 0) {
                $scope.sales1.cardPayment.paymentInd = salesPaymentDetails[i].paymentInd;
                $scope.sales1.cardPayment.amount = parseFloat(salesPaymentDetails[i].amount).toFixed(2);
                $scope.sales1.cardPayment.cardNo = salesPaymentDetails[i].cardNo != undefined ? salesPaymentDetails[i].cardNo : null;
                $scope.sales1.cardPayment.cardDate = salesPaymentDetails[i].cardDate != undefined ? salesPaymentDetails[i].cardDate : null;
                $scope.sales1.cardPayment.cardTransId = salesPaymentDetails[i].cardTransId != undefined ? salesPaymentDetails[i].cardTransId : null;                
            }
            if (salesPaymentDetails[i].paymentInd == 3 && salesPaymentDetails[i].amount > 0) {
                $scope.sales1.chequePayment.paymentInd = salesPaymentDetails[i].paymentInd;
                $scope.sales1.chequePayment.amount = parseFloat(salesPaymentDetails[i].amount).toFixed(2);
                $scope.sales1.chequePayment.chequeNo = salesPaymentDetails[i].chequeNo != undefined ? salesPaymentDetails[i].chequeNo : null;
                $scope.sales1.chequePayment.chequeDate = salesPaymentDetails[i].chequeDate != undefined ? salesPaymentDetails[i].chequeDate : null;               
            }
            if (salesPaymentDetails[i].paymentInd == 4 && salesPaymentDetails[i].amount > 0) {
                $scope.sales1.creditPayment.paymentInd = salesPaymentDetails[i].paymentInd;
                $scope.sales1.creditPayment.amount = parseFloat(salesPaymentDetails[i].amount).toFixed(2);
                $scope.sales1.creditamout = parseFloat(salesPaymentDetails[i].amount).toFixed(2);
            }
            if (salesPaymentDetails[i].paymentInd == 5 && salesPaymentDetails[i].amount > 0) {
                $scope.sales1.walletPayment.paymentInd = salesPaymentDetails[i].paymentInd;
                $scope.sales1.walletPayment.amount = parseFloat(salesPaymentDetails[i].amount).toFixed(2);
                $scope.sales1.walletPayment.walletTransId = salesPaymentDetails[i].walletTransId != undefined ? salesPaymentDetails[i].walletTransId : null;
                $scope.sales1.walletPayment.subPaymentInd = salesPaymentDetails[i].subPaymentInd != undefined ? salesPaymentDetails[i].subPaymentInd.toString() : null;
            }
        }
        $scope.calculatePaymentDetails();
        //$scope.sales1.cashPayment.amount = $scope.sales1.cashPayment.amount > 0 ? $scope.sales1.cashPayment.amount.toFixed(2) : null;
    }
   
   
    
});

app.directive('focusBox1', function () {
    return {
        "link": function (scope, element, attrs) {
            element.bind("keydown", function (event) {
                var valueLength = attrs.$$element[0].value.length;
                var value = parseFloat(attrs.$$element[0].value);
                var ele = "";
                //Enter 
                if (event.which === 13) {
                    
                    if (attrs.id === "cardpay") {
                        if (value > 0) {
                            ele = document.getElementById(attrs.nextid);
                            ele.focus();
                            return;
                        } else {
                            ele = document.getElementById("chequepay");
                            ele.focus();
                            return;
                        }                        
                    }

                    else if (attrs.id === "chequepay") {
                        if (value > 0) {
                            ele = document.getElementById(attrs.nextid);
                            ele.focus();
                            return;
                        } else {
                            ele = document.getElementById("walletpay");
                            ele.focus();
                            return;
                        }
                    }

                    else if (attrs.id === "walletAmtpay") {
                        if (value > 0) {
                            ele = document.getElementById(attrs.nextid);
                            ele.focus();
                            return;
                        } else {
                            ele = document.getElementById("creditpay");
                            ele.focus();
                            return;
                        }
                    }
                    else {
                        ele = document.getElementById(attrs.nextid);
                        ele.focus();
                    }                    
                }

            });
        }
    };
});