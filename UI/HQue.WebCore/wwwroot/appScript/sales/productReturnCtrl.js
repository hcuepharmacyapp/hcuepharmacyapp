app.controller('productReturnCtrl', function ($scope, $rootScope, $filter, close, toastr, cacheService, ModalService, salesService, productService, productStockService, productModel, salesItem, salesReturnModel, productStockModel, patientId, patientType, gstEnable, isComposite, salesPriceSettings,taxValuesList) {
    /*Patient Type added by Poongodi on 30/05/2017*/
    if (patientType != null && patientType != "") {
        $scope.patientType = patientType;
    }
    else {
        $scope.patientType = 1;
    }
    $scope.patientId = patientId;
    $scope.salesPriceSetting = salesPriceSettings;

    /*Patient Type added by Poongodi on 30/05/2017*/
    if (gstEnable != null && gstEnable != "") {
        $scope.gstEnable = gstEnable;
    }
    else {
        $scope.gstEnable = true;
    }
    var product = productModel;

    $scope.search = product;
    $scope.isComposite = isComposite;
    $scope.taxValuesList = taxValuesList;


    $scope.selectedProduct_1 = null;
    $scope.selectedBatch1 = { reminderFrequency: "0" };
    $scope.selectedBatch1.transactionType = "return";
    $scope.selectedBatch1.salesReturn = true;
    $scope.tempReturnItem = [];
    $scope.quantityValid = false;
    $scope.focustxtProduct1 = true;
    $scope.batchList1 = null;
    $scope.gstReturnMode = false;
    $scope.getReturnGstvalue = null;

    angular.forEach(salesItem, function (value, key) {
        if (value.salesReturn == null || value.salesReturn == undefined) {
            value.salesItem = false;
        }
    });

    $scope.validateQuantity = function (val1) {
        if (val1 > 0) {
            $scope.quantityValid = true;
        } else {
            $scope.quantityValid = false;
        }
    }


    $scope.clearReturn = function () {
        $scope.selectedProduct_1 = null;
        $scope.selectedBatch1 = { reminderFrequency: "0" };
        $scope.selectedBatch1.transactionType = "return";
        $scope.selectedBatch1.salesReturn = true;
        $scope.productReturn.$setPristine();
        $scope.batchList1 = null;
        $scope.quantityValid = false;
        document.getElementById("drugName1").focus();
    }


    $scope.getProducts11 = function (val) {

        return productStockService.getAllStockDataForReturn(val).then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }

            return output.map(function (item) {
                return item;
            });
        });

    };
    $scope.gstValidate = function () {
        if (selectedBatch1.gstTotal > 100 || selectedBatch1.gstTotal == null || selectedBatch1.gstTotal == '') {
            return false;
        }
        else {
            return true;
        }
    }
    $scope.onProductSelect1 = function () {

        $scope.selectedBatch1 = { reminderFrequency: "0", purchasePrice: 0 };
        $scope.selectedBatch1.salesReturn = true;
        $scope.selectedBatch1.transactionType = "return";

        productStockService.getBatchForReturn($scope.selectedProduct_1.product.id).then(function (response) {
            $scope.batchList1 = response.data;
            var qty = document.getElementById("ReturnQuantity");
            qty.focus();
            var totalQuantity = 0;
            var rack = "";
            var tempBatch = [];
            var editBatch = null;
            //$scope.changediscountsale("");
            for (var i = 0; i < $scope.batchList1.length; i++) {

                $scope.batchList1[i].productStockId = $scope.batchList1[i].id;
                $scope.batchList1[i].id = null;

                //Added by Sarubala on 07-11-17 to show whole sale price
                if ($scope.salesPriceSetting == 3) {
                    $scope.batchList1[i].sellingPrice = parseFloat(($scope.batchList1[i].returnPurchasePriceWithoutTax * (1 + ($scope.batchList1[i].gstTotal / 100))).toFixed(6));
                    $scope.batchList1[i].purchasePriceWithoutTax = $scope.batchList1[i].returnPurchasePriceWithoutTax;
                }

                if ($scope.patientType == 2 && !gstEnable) {
                    $scope.batchList1[i].sellingPrice = ((($scope.batchList1[i].packagePurchasePrice / $scope.batchList1[i].packageSize) + (($scope.batchList1[i].packagePurchasePrice / $scope.batchList1[i].packageSize) * ($scope.batchList1[i].vat / 100))) * 1.1).toFixed(2);
                }
                else if ($scope.patientType == 2 && gstEnable) {
                    if ($scope.patientId != "30fdae11-4a06-4f9d-a87d-3ead26a8d110") {
                        $scope.batchList1[i].sellingPrice = ((($scope.batchList1[i].packagePurchasePrice / $scope.batchList1[i].packageSize) + (($scope.batchList1[i].packagePurchasePrice / $scope.batchList1[i].packageSize) * ((!angular.isUndefinedOrNull($scope.batchList1[i].gstTotal) ? $scope.batchList1[i].gstTotal : 0) / 100))) * 1.1).toFixed(2);
                    }
                    else {
                        $scope.batchList1[i].sellingPrice = ((($scope.batchList1[i].packagePurchasePrice / $scope.batchList1[i].packageSize) + (($scope.batchList1[i].packagePurchasePrice / $scope.batchList1[i].packageSize) * ((!angular.isUndefinedOrNull($scope.batchList1[i].gstTotal) ? $scope.batchList1[i].gstTotal : 0) / 100)))).toFixed(2);
                    }
                }
                var availableStock = $scope.getAvailableStock($scope.batchList1[i], editBatch);
                if (editBatch != null && editBatch.productStockId == $scope.batchList1[i].productStockId)
                    editBatch.availableQty = availableStock;
                totalQuantity += availableStock;

                $scope.batchList1[i].availableQty = availableStock;
                if ($scope.batchList1[i].rackNo != null || $scope.batchList1[i].rackNo != undefined) {
                    rack = $scope.batchList1[i].rackNo;
                }
                tempBatch.push($scope.batchList1[i]);

            }

            $scope.batchList1 = tempBatch;

            if ($scope.batchList1.length > 0) {

                $scope.selectedBatch1 = $scope.batchList1[0];
                $scope.selectedBatch1.reminderFrequency = "0";
                $scope.selectedBatch1.discount = "";
                if ($scope.selectedBatch1.gstTotal == null || $scope.selectedBatch1.gstTotal == "undefined")
                { $scope.selectedBatch1.gstTotal = 0; }
                $scope.selectedBatch1.canEnableTaxEdit = ($scope.selectedBatch1.gstTotal == 0);
                //if (LeadsRequiredquantity != "") {
                //    $scope.selectedBatch.quantity = LeadsRequiredquantity;
                //}
                $scope.selectedBatch1.totalQuantity = totalQuantity;
                $scope.selectedBatch1.rackNo = rack;
                $scope.selectedBatch1.transactionType = "return";
                $scope.selectedBatch1.salesReturn = true;
                dayDiff($scope.selectedBatch1.expireDate);

                for (var k = 0; k < salesItem.length; k++) {

                    if ((salesItem[k].productStockId == $scope.selectedBatch1.productStockId) && (salesItem[k].productId == $scope.selectedBatch1.productId) && (salesItem[k].salesReturn != $scope.selectedBatch1.salesReturn)) {
                        var ele = document.getElementById("drugName1");
                        ele.focus();
                        toastr.info("Product already added in Sales");
                        return;
                    }
                }

            }

            if ($scope.taxValuesList.length > 0) {
                var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.selectedBatch1.gstTotal) }, true);
                if (taxList.length == 0) {
                    $scope.gstReturnMode = true;
                    return;
                }
                else
                {
                    $scope.gstReturnMode = false;
                }
            }
        }, function () { });
    };

    $scope.getAvailableStock = function (productStock, editBatch) {
        var addedItem = getAddedItem(productStock.productStockId);
        var newAddedQty = getAddedItemQuantity(addedItem);

        if (editBatch == null)
            return productStock.stock - newAddedQty;
        else if (editBatch.id != null && productStock.productStockId == editBatch.productStockId)
            return productStock.stock + parseInt(editBatch.quantity) - newAddedQty;
        else if (editBatch != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock - (newAddedQty - parseInt(editBatch.quantity));
        }
        else if (editBatch != null && productStock.productStockId != editBatch.productStockId) {
            return productStock.stock - newAddedQty;
        }
        return productStock.stock;
    };

    function getAddedItemQuantity(addedItems) {
        var qty = 0;
        for (var i = 0; i < addedItems.length; i++) {
            if (addedItems[i].id != null) {
                qty += parseInt(addedItems[i].quantity) - parseInt(addedItems[i].orginalQty);
            }
            else {
                qty += parseInt(addedItems[i].quantity);
            }
        }
        return qty;
    }

    function getAddedItem(id) {
        var addedItems = [];
        for (var i = 0; i < salesItem.length; i++) {
            if (salesItem[i].productStockId == id)
                addedItems.push(salesItem[i]);
        }
        return addedItems;
    }
    function getAddedItems(id, sellingPrice, discount) {

        for (var i = 0; i < $scope.tempReturnItem.length; i++) {

            if ($scope.tempReturnItem[i].productStockId == id && $scope.tempReturnItem[i].sellingPrice == sellingPrice && $scope.tempReturnItem[i].discount == discount) {
                return $scope.tempReturnItem[i];
            }

        }
        return null;
    }


    function dayDiff(expireDate) {
        $scope.highlight1 = "";
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');

        var date2 = new Date(formatString(expireDate));
        var date1 = new Date(formatString(today));
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference1 = Math.ceil(timeDiff / (1000 * 3600 * 24));

        if ($scope.dayDifference1 < 30) {
            var dt = expireDate;
            $scope.highlight1 = "Expiry";
        }
        else {
            $scope.highlight1 = "";
        }
    }

    function formatString(format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    }

    $scope.batchSelected1 = function (batch) {

        $scope.valPurchasePrice = "";
        var totalQuantity = 0;
        $scope.selectedBatch1 = $scope.batchList1[batch];
        $scope.selectedBatch1.transactionType = "return";
        $scope.selectedBatch1.salesReturn = true;

        if ($scope.selectedBatch1.quantity != undefined) {
            $scope.validateQuantity(parseInt($scope.selectedBatch1.quantity));
        }
        else {
            $scope.quantityValid = false;
        }


        for (var k = 0; k < salesItem.length; k++) {

            if ((salesItem[k].productStockId == $scope.selectedBatch1.productStockId) && (salesItem[k].productId == $scope.selectedBatch1.productId) && (salesItem[k].salesReturn != $scope.selectedBatch1.salesReturn)) {
                var ele = document.getElementById("drugName1");
                ele.focus();
                toastr.info("Product already added in Sales");
                return;
            }
        }

        for (var i = 0; i < $scope.batchList1.length; i++) {
            totalQuantity += parseInt($scope.batchList1[i].stock);
        }


        if ($scope.selectedBatch1 != undefined) {
            if ($scope.selectedBatch1.totalQuantity == undefined) {
                $scope.selectedBatch1.totalQuantity = "";
            }
            else {
                $scope.selectedBatch1.totalQuantity = totalQuantity;

            }
            if ($scope.selectedBatch1.availableQty != null && $scope.selectedBatch1.batchNo != null) {
                $scope.selectedBatch1.totalQuantity = totalQuantity;
            }

            if ($scope.selectedBatch1.discount == undefined) {
                $scope.selectedBatch1.discount = "";
            }
            else {
                $scope.selectedBatch1.discount = "";
            }

            dayDiff($scope.selectedBatch1.expireDate);
        }
        
        if ($scope.taxValuesList.length > 0) {
            var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.selectedBatch1.gstTotal) }, true);
            if (taxList.length == 0) {
                $scope.gstReturnMode = true;
                return;
            }
            else {
                $scope.gstReturnMode = false;
            }
        }
        var qty = document.getElementById("ReturnQuantity");
        qty.focus();

    };

    $scope.CheckName1 = function () {
        var drugName = document.getElementById("drugName1");

        if (drugName.value == "") {

            $scope.selectedBatch1 = { reminderFrequency: "0", purchasePrice: 0 };
            $scope.selectedBatch1.salesReturn = true;
            $scope.selectedBatch1.transactionType = "return";
        }
    };

    //Added by Sarubala on 08-11-17
    $scope.calculateMrp = function (item) {
        if ($scope.salesPriceSetting == 3) {
            if (parseFloat(item.purchasePriceWithoutTax) > 0) {
                var gst = (item.gstTotal != null && item.gstTotal != undefined && item.gstTotal != '') ? item.gstTotal : 0;
                item.sellingPrice = parseFloat((item.purchasePriceWithoutTax * (1 + (gst / 100))).toFixed(6));
                $scope.selectedBatch1.sellingPrice = item.sellingPrice;
            }
        }
    }

    $scope.calculatePrice = function (temp1) {
        if ($scope.salesPriceSetting == 3) {
            if (parseFloat(temp1.sellingPrice) > 0) {
                var gst = (temp1.gstTotal != null && temp1.gstTotal != undefined && temp1.gstTotal != '') ? temp1.gstTotal : 0;
                temp1.purchasePriceWithoutTax = parseFloat(((temp1.sellingPrice * 100) / (100 + gst)).toFixed(6));
                $scope.selectedBatch1.purchasePriceWithoutTax = temp1.purchasePriceWithoutTax;
            }
        }
    }

    $scope.addReturn = function () {


        for (var k = 0; k < salesItem.length; k++) {

            if ((salesItem[k].productStockId == $scope.selectedBatch1.productStockId) && (salesItem[k].productId == $scope.selectedBatch1.productId) && (salesItem[k].salesReturn != $scope.selectedBatch1.salesReturn)) {
                var ele = document.getElementById("drugName1");
                ele.focus();
                toastr.info("Product already added in Sales");
                return;
            }
        }

        if ($scope.selectedBatch1.quantity == '' || $scope.selectedBatch1.quantity == null || $scope.selectedBatch1.quantity == undefined) {
            return;
        }

        if ($scope.selectedBatch1.sellingPrice == '' || $scope.selectedBatch1.sellingPrice == null || $scope.selectedBatch1.sellingPrice == undefined ||
            ($scope.selectedBatch1.sellingPrice < $scope.selectedBatch1.purchasePrice && $scope.salesPriceSetting != 3) || (($scope.selectedBatch1.purchasePriceWithoutTax == '' || $scope.selectedBatch1.purchasePriceWithoutTax == null || $scope.selectedBatch1.purchasePriceWithoutTax == undefined) && $scope.salesPriceSetting == 3)) {
            return;
        }

        if ($scope.selectedBatch1.discount > 100) {
            return;
        }

        if ($scope.batchList1.length == 0)
            return;

        $scope.selectedBatch1.quantity = $scope.selectedBatch1.quantity * (-1);

        //var RequiredQuantity = angular.element("#spnquantity1").attr("data-quantityId");
        //var ChangedMrp = angular.element("#spnquantity1").attr("data-price");
        //var ChangedDicount = angular.element("#spnquantity1").attr("data-discount") || 0;

        //if (RequiredQuantity > 0)
        //    $scope.selectedBatch1.quantity = RequiredQuantity;

        //if (ChangedMrp >= $scope.selectedBatch1.purchasePrice) {
        //    $scope.selectedBatch1.sellingPrice = ChangedMrp;
        //}

        //if (ChangedDicount <= 100)
        //    $scope.selectedBatch1.discount = ChangedDicount;

        if ($scope.selectedBatch1.discount == '' || $scope.selectedBatch1.discount == null) {
            $scope.selectedBatch1.discount = 0;
        }

        var addedItem = getAddedItems($scope.selectedBatch1.productStockId, $scope.selectedBatch1.sellingPrice, $scope.selectedBatch1.discount);

        if (addedItem != null) {
            addedItem.quantity = (parseInt(addedItem.quantity)) + (parseInt($scope.selectedBatch1.quantity));
        }
        else {
            $scope.tempReturnItem.push($scope.selectedBatch1);
        }

        $scope.clearReturn();

    }
    $scope.assignfocus = function (currentId) {
        if ($scope.selectedBatch1.canEnableTaxEdit) {
            $scope.focusNext(currentId, 'gstTotal1');
        }
        else {   //Modified by Sarubala on 08-11-17
            if ($scope.salesPriceSetting == 3) {
                $scope.focusNext(currentId, 'PPrice1');
            }
            else {
                $scope.focusNext(currentId, 'sellingPrice1');
            }
        }

    }

    //Added by Sarubala on 08-11-17
    $scope.assignfocusNext = function (currentId) {

        if ($scope.salesPriceSetting == 3) {
            $scope.focusNext(currentId, 'PPrice1');
        }
        else {
            $scope.focusNext(currentId, 'sellingPrice1');
        }
    }

    $scope.focusNext = function (currentId, nextId) {

        var val1 = document.getElementById(currentId).value;
        if (currentId == "discount1" && (val1 == null || val1 == '')) {
            val1 = "0";
        }

        if (currentId == "gst" && (val1 == null || val1 == '')) {
            val1 = "0";
        }
        if ($scope.patientType != 2) {
            if (val1 != '' && val1 != null) {
                var ele = document.getElementById(nextId);
                ele.focus();
            }
            else {
                var ele = document.getElementById(currentId);
                ele.focus();
            }
        }
        else {
            if (val1 != '' && val1 != null) {
                if (currentId != "batch1") {
                    var ele = document.getElementById(nextId);
                    ele.focus();
                }
                else {
                    $scope.addReturn();
                }

            }
            else {
                var ele = document.getElementById(currentId);
                ele.focus();
            }
        }


    };


    $scope.toggleProductDetail1 = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper1' + row).slideToggle();
        $('#chip-wrapper1' + row).show();
        if ($('#chip-btn1' + row).text() === '+')
            $('#chip-btn1' + row).text('-');
        else {
            $('#chip-btn1' + row).text('+');
        }
    };

    $scope.addReturnToSales = function () {
        $scope.close($scope.tempReturnItem);
    }

    $rootScope.$on("returnPopupEvent", function () {
        $scope.close("No");
    });


    $scope.close = function (result) {
        if (result == 'No') {
            result = null;
        }
        close(result, 500); // close, but give 500ms for bootstrap to animate
        $(".modal-backdrop").hide();
    };



    for (var i = 0; i < salesItem.length; i++) {
        if (salesItem[i].salesReturn) {
            $scope.tempReturnItem.push(salesItem[i]);
        }
    }

    angular.isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null || val === ""
    }

});