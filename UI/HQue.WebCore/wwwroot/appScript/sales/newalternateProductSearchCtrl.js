app.controller('newalternateProductSearchCtrl', function ($scope, $rootScope, selectedProduct, close, toastr, cacheService, ModalService, productService, productStockService, productModel, shortcutHelper, productSearchType, selectedProductId) {
    $scope.focusFirstRow = false;
    shortcutHelper.setScope($scope);
    $scope.keydown = shortcutHelper.alternateProductSearchShortcuts;
    $scope.focusProduct = false;
    $scope.focusGenericName = false;
    var product = productModel;
    $scope.search = product;
    $scope.search.genericName = "";
    $scope.search.productStock = [];
    $scope.focustxtProduct = true;
    $scope.list = [];
    $scope.search.drugName = null;
    $scope.isFormInvalid = true;
    $scope.searchValid = true;
    $scope.focustxttype = false;
    $scope.searchInValid = false;
    $scope.getProducts = function (val) {
        return productStockService.drugFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    $scope.getGeneric = function (val) {
        return productService.genericFilterData(val)
            .then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
    };
    //end
    $scope.addProduct = function () {
        $scope.close('Yes');
        $.LoadingOverlay("show");
        productStockService.productBatch($scope.search.selectedAlternate)
            .then(function (response) {
                cacheService.set('selectedAlternate1', response.data);
                $rootScope.$emit("alternateSelection", response.data);
                var qty = document.getElementById("quantity");
                qty.focus();
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                toastr.error("Error Ocured", "Error");
                $.LoadingOverlay("hide");
            });
    };
    $scope.getProductDetails = function () {
        $.LoadingOverlay("show");
        productStockService.getProductDetails($scope.search.drugName.product.id)
            .then(function (response) {
                var name1 = $scope.search.drugName.name;
                $scope.search = response.data;
                $scope.search.drugName = name1;
                if ($scope.search.productStock.length != 0) {
                    $scope.focusFirstRow = true;
                    $scope.selectRow(0);
                }
                $scope.selectedGeneric = $scope.search.genericName;
                $.LoadingOverlay("hide");
            }, function (error) {
                toastr.error("Error Ocured", "Error");
                console.log(error);
                $.LoadingOverlay("hide");
            });
    };
    $scope.setFormValid = function (val) {
        if (val == 0) {
            $scope.isFormInvalid = true;
        } else {
            $scope.isFormInvalid = false;
        }
    };
    $rootScope.$on("PopupAlternatesEvent", function () {
        $scope.close("No");
    });
    $scope.close = function (result) {
        close(result, 500);
        if (result != "Yes") {
            $scope.search.drugName = null;
            if ($scope.products != undefined) {
                $scope.products.genericName = null;
            }
            $scope.search = null;
            $rootScope.$emit("alternateDetailCancel", null);
        }
        $(".modal-backdrop").hide();
    };
    $scope.GetProdutsByProductId = function (selectedProductId) {
        productStockService.getProductDetails(selectedProductId)
            .then(function (response) {
                $scope.search = response.data;
                $scope.search.drugName = selectedProduct.name;
                if ($scope.products != undefined) {
                    $scope.products.genericName = null;
                }
            }, function (error) {
                console.log(error);
                toastr.error("Error Ocured", "Error");
            });
    };
    $scope.GetProdutsByGenericName = function (genericName) {
        $.LoadingOverlay("show");
        productService.getGenericDetails(genericName)
             .then(function (response) {
                 $scope.search = response.data;
                 if ($scope.search.productStock.length != 0) {
                     $scope.focusFirstRow = true;
                     $scope.selectRow(0);
                 }
                 $.LoadingOverlay("hide");
             }, function (error) {
                 console.log(error);
                 toastr.error("Error Ocured", "Error");
                 $.LoadingOverlay("hide");
             });
    };
    $scope.searchGenericProducts = function (selectedGeneric) {
        $scope.GetProdutsByGenericName(selectedGeneric.genericName);
    };
    if (productSearchType != "") {
        if (productSearchType == "Product") {
            $scope.searchValid = false;
            $scope.searchInValid = true;
            if ($scope.searchInValid == true) {
                $scope.focustxttype = true;
            }
            $scope.GetProdutsByProductId(selectedProductId);
        }
        if (productSearchType == "Generic") {
            $scope.searchValid = false;
            $scope.searchInValid = true;
            $scope.selectedGeneric = selectedProduct != null ? selectedProduct.product.genericName : "";
            $scope.GetProdutsByGenericName($scope.selectedGeneric);
        }
    }
    //Code By Sabarish Start
    $scope.isSelectedItem = 0;
    $scope.ProductRowChange = function (e) {
        //Arrow Up
        if (e.keyCode == 38) {
            if ($scope.isSelectedItem == 0) {
                e.preventDefault();
                return;
            }
            $scope.isSelectedItem--;
            $scope.selectRow($scope.isSelectedItem);
            document.getElementById('historydata').scrollTop -= 20;
        }
        //Arrow Down
        if (e.keyCode == 40) {
            if ($scope.isSelectedItem == $scope.search.productStock.length - 1) {
                e.preventDefault();
                return;
            }
            $scope.isSelectedItem++;
            $scope.selectRow($scope.isSelectedItem);
            document.getElementById('historydata').scrollTop += 20;
        }
        //Enter
        if (e.keyCode == 13) {
            if (!$scope.isFormInvalid) {
                $scope.addProduct();
            }
        }
    };
    $scope.selectRow = function (index) {
        $scope.isSelectedItem = ($scope.isSelectedItem == null) ? 1 : index;
        $scope.setFormValid($scope.search.productStock[index].stock);
        $scope.search.selectedAlternate = $scope.search.productStock[index].product.id;
    };
    //Code By Sabarish End
});


