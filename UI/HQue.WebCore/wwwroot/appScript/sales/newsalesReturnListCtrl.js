app.controller('newsalesReturnListCtrl', function ($scope, newsalesService, salesReturnModel, pagerServcieforsales, patientService, $filter, printingHelper) {

    var salesReturn = salesReturnModel;

    $scope.search = salesReturn;
    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcieforsales.page;
    $scope.pages = pagerServcieforsales.pages;
    $scope.paginate = pagerServcieforsales.paginate;
    $scope.isSelectedItemMain = 1;
    $scope.amt = 0; // Added by arun for displaying the net value 
    //pagination

    function pageSearch() {
        $.LoadingOverlay("show");
        newsalesService.returnList($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $scope.buildselectedSales($scope.list[0], 0);
            for (var x = 0; x < $scope.list.length; x++) {
                var active = 0;
                for (var y = 0; y < $scope.list[x].salesReturnItem.length; y++) {
                    if ($scope.list[x].salesReturnItem[y].mrpSellingPrice > 0) {
                        var discountAmtPerQty = ($scope.list[x].salesReturnItem[y].mrpSellingPrice * $scope.list[x].salesReturnItem[y].discount) / 100;
                        $scope.list[x].salesReturnItem[y].discountAmtPerQty = discountAmtPerQty;
                    }
                    else {
                        var discountAmtPerQty = ($scope.list[x].salesReturnItem[y].total * $scope.list[x].salesReturnItem[y].discount) / 100;
                        $scope.list[x].salesReturnItem[y].discountAmtPerQty = discountAmtPerQty;
                    }

                    if ($scope.list[x].salesReturnItem[y].cancelType == 1) {
                        active++
                    }
                }
                if (active > 0) {
                    $scope.list[x].Status = true;
                } else {
                    $scope.list[x].Status = false;
                }
            }
            $.LoadingOverlay("hide");
            document.getElementById('listdata').scrollTop = 0;
            $scope.isSelectedItemMain = 1;
        }, function () {
            $.LoadingOverlay("hide");
        });
    }


    $scope.keyPressGDmain = function (e) {

        if ($scope.isSelectedItemMain == 1) {
           
            }
            else
            {
            if (e.keyCode == 38) {


                if ($scope.isSelectedItemMain == 1) {
                    $scope.isSelectedItemMain = ($scope.list.length) + 1;
                   // document.getElementById('listdata').scrollTop += 887;  // to focus bottom of the table. Added by arun
                }
                else {
                    document.getElementById('listdata').scrollTop -= 42;
                }

                $scope.isSelectedItemMain--;

                e.preventDefault();
            }

        }

        if (e.keyCode == 40) { //down arrow

            if ($scope.isSelectedItemMain == $scope.list.length) {
                document.getElementById('listdata').scrollTop = 0; // to focus on top of the table.
                $scope.isSelectedItemMain = 0;
            }
            else {
                document.getElementById('listdata').scrollTop += 42; //to focus next row of the table.
            }
            $scope.isSelectedItemMain++;
            e.preventDefault();


        }

        if (e.keyCode == 13) {

            $scope.buildselectedSales($scope.list[$scope.isSelectedItemMain - 1], $scope.isSelectedItemMain - 1);

        }

    }

    $scope.buildselectedSales = function (selectedSaleItem, index) {
        $scope.amt = 0;
        $scope.isSelectedItemMain = index + 1;
          $scope.selectedSale = selectedSaleItem;

        $scope.search.SalesID = selectedSaleItem.id;
   
        if ($scope.selectedSale.salesReturnItem !== undefined && $scope.selectedSale.salesItem.length > 0) {
                for (var i = 0; i < $scope.selectedSale.salesReturnItem.length; i++) {
                    $scope.amt += Math.round($scope.selectedSale.salesReturnItem[i].total);
                }
            }
            console.log($scope.amt);
   
        document.getElementById('productbatchList').focus();
        $scope.focusInputsalereturnlist = true;
    }
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };


    $scope.printBill = function (sales) {
        printingHelper.printInvoice(sales.id)
    }


    $scope.salesReturnSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        newsalesService.returnList($scope.search).then(function (response) {
            $scope.list = response.data.list;
            if ($scope.list.length > 0) {
                $scope.selectedSale = $scope.list[0];
                $scope.amt = 0;
                for (var i = 0; i < $scope.selectedSale.salesReturnItem.length; i++) {

                    $scope.amt += Math.round($scope.selectedSale.salesReturnItem[i].total);
                }
                $scope.isSelectedItemMain = 1;
                for (var x = 0; x < $scope.list.length; x++) {
                    var active = 0;
                    for (var y = 0; y < $scope.list[x].salesReturnItem.length; y++) {
                        if ($scope.list[x].salesReturnItem[y].mrpSellingPrice > 0) {
                            var discountAmtPerQty = ($scope.list[x].salesReturnItem[y].mrpSellingPrice * $scope.list[x].salesReturnItem[y].discount) / 100;
                            $scope.list[x].salesReturnItem[y].discountAmtPerQty = discountAmtPerQty;
                        }
                        else {
                            var discountAmtPerQty = ($scope.list[x].salesReturnItem[y].total * $scope.list[x].salesReturnItem[y].discount) / 100;
                            $scope.list[x].salesReturnItem[y].discountAmtPerQty = discountAmtPerQty;
                        }

                        if ($scope.list[x].salesReturnItem[y].cancelType == 1) {
                            active++
                        }
                    }
                    if (active > 0) {
                        $scope.list[x].Status = true;
                    } else {
                        $scope.list[x].Status = false;
                    }
                }

                pagerServcieforsales.init(response.data.noOfRows, pageSearch);
                $.LoadingOverlay("hide");

                $scope.buildselectedSales($scope.list[0], 0);
            }
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.salesReturnSearch();

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.clearSearch = function () {
        $scope.name = '';
        $scope.search.returnNo = "";
        if ($scope.search.sales != undefined) {
            $scope.search.sales.name = "";
            $scope.search.sales.mobile = "";
            $scope.search.sales.invoiceNo = "";
            $scope.search.sales.email = "";
            $scope.search.returnNo = "";
        }
        document.getElementById('listdata').scrollTop = 0;
        $scope.salesReturnSearch();
    }

    $scope.printReturnList = function (salesReturn) {
        window.open("/Invoice/GetReturnListById?salesid=" + salesReturn.sales.id + "&salesreturnid=" + salesReturn.id);
    };

    $scope.changeCustomerName = function () {
        var custname = document.getElementById("customerName").value;


        console.log(custname);

        //if ($scope.sales.salesItem.length > 0) {
        //    if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
        //        if (custname == "") {
        //            $scope.iscustomerNameMandatory = true;
        //        } else {
        //            $scope.iscustomerNameMandatory = false;
        //            $scope.flagCustomername = false;
        //        }
        //    }
        //}

    };

    $scope.Namecookie = "";
    $scope.getPatient = function (val) {
        // $scope.customerHelper.data.patientSearchData.name = val;
        $scope.Namecookie = val;

        if ($scope.PatientNameSearch == "" || $scope.PatientNameSearch == undefined) {
            $scope.PatientNameSearch = 2;
        }

        if ($scope.PatientNameSearch == 1) {
            return patientService.GetPatientName(val).then(function (response) {

                var origArr = response.data;
                var newArr = [],
           origLen = origArr.length,
           found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });
            });
        } else {
            return patientService.Patientlist(val).then(function (response) {

                var origArr = response.data;
                var newArr = [],
           origLen = origArr.length,
           found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });
            });
        }
    };

    //$scope.getCustomerBalance = function () {
    //    customerReceiptService.getCustomerBalance(customerHelper.data.selectedCustomer.mobile, customerHelper.data.selectedCustomer.name).then(function (resp) {
    //        if (resp.data.length == 0) {
    //            customerHelper.data.customerBalance = 0;
    //            return;
    //        }

    //        if (resp.data.credit != undefined)
    //            customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
    //    });
    //};
    var patientSelected = 0;
    $scope.onPatientSelect = function (obj, event) {

     
        if (obj !== undefined) {
            $scope.search.sales = { 'name': '' };
            $scope.search.sales.name = obj.name;
        }

  };

    getPatientSearchType();
    $scope.PatientNameSearch = "1";
    function getPatientSearchType() {
        newsalesService.getPatientSearchType().then(function (response) {

            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.PatientNameSearch = "1";
            } else {
                if (response.data.patientSearchType != undefined) {
                    $scope.PatientNameSearch = response.data.patientSearchType;
                } else {
                    $scope.PatientNameSearch = "1";
                }
            }
        }, function () {

        });
    }


    $scope.keyEnter = function (event, e) {



        console.log(e);
        if (event.which === 8) {
            if ($scope.customerHelper.data.patientSearchData.name == "" || $scope.customerHelper.data.patientSearchData.name == undefined) {
                $scope.Namecookie = "";
            }
        }
        var ele = document.getElementById(e);

        if (event.which === 13) // Enter key
        {
            ele.focus();

        }


        if (event.which === 9) {
            $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
        }



    };

    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        newsalesService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    }
    $scope.getEnableSelling();

});

app.filter('round', function () {
    return function (input) {
        return Math.round(input);
    };
});