app.factory('salesEditHelper', function (salesService, customerHelper) {
    var scope;
    return {
        setScope: function (parentScope) {
            scope = parentScope;
            loadEditData();
        }
    };

    function loadEditData() {
        $.LoadingOverlay("show");
        scope.Editsale = false;
        var salesEditId = document.getElementById('salesEditId').value;
        if (salesEditId == "") {
            $.LoadingOverlay("hide");
            return;
        }


        salesService.salesDetail(salesEditId).then(function (response) {
            scope.sales = response.data;

            scope.oldRedeemPoint = scope.sales.redeemPts >= 0 ? scope.sales.redeemPts : 0;

            //if (scope.sales.loyaltyId) {
            //    getSaleLoyaltyPointSetting(scope.sales.loyaltyId);
            //} else {
            //    scope.loyaltyPointSettings.maximumRedeemPoint = 0;
            //}

            if (scope.sales.loyaltySettings != null && scope.sales.loyaltySettings != undefined) {
                scope.loyaltyPointSettings = scope.sales.loyaltySettings;
                scope.loyaltyPointSettings.maximumRedeemPoint = scope.loyaltyPointSettings.maximumRedeemPoint >= 0 ? scope.loyaltyPointSettings.maximumRedeemPoint : 0;
            } else {
                scope.loyaltyPointSettings.maximumRedeemPoint = 0;
            }


            scope.doctorname = scope.sales.doctorName;

            if (scope.sales.discountType != null && scope.sales.discountType != undefined && scope.sales.discountType > 0) {
                scope.sales.discountType = scope.sales.discountType.toString();
            } else {
                scope.sales.discountType = '1';
            }
            // Added by Gavaskar Loyalty Points 12-12-2017 Start
            if (scope.sales.redeemPts > 0 && scope.sales.redeemAmt != "") {
                redeemEnable.checked = true;
                scope.isRedeemPts = true;
                scope.isRedeemValue = true;
            }
            // Added by Gavaskar Loyalty Points 12-12-2017 End
            scope.isGstEnabled = scope.sales.taxRefNo == 1 ? true : false;


            //if (scope.sales.discount > 0 || (scope.sales.discount == 0 && (scope.sales.discountValue == 0 || scope.sales.discountValue == undefined))) {
            //    scope.sales.discountType = 1;
            //    if (!(scope.sales.discount > 0))
            //        scope.changeDiscount(0);
            //}
            //else {
            //    scope.sales.discountType = 2;
            //    //scope.changeDiscount(scope.sales.discountValue);
            //}

            var salesItems = [];
            scope.schedulecount = 0;
            var schedulecount = 0;

            scope.salesReturnCount = 0;
            scope.salesCount = 0;

            for (var i = 0; i < scope.sales.salesItem.length; i++) {

                if (scope.sales.salesItem[i].salesReturn) {
                    scope.salesReturnCount++;
                } else {
                    scope.salesCount++;
                }

                var salesItem = scope.sales.salesItem[i].productStock;
                salesItem.saleProductName = scope.sales.salesItem[i].saleProductName; //Added by Sarubala on 08/09/17
                salesItem.productStockId = salesItem.id;
                salesItem.id = scope.sales.salesItem[i].id;
                salesItem.editId = i + 1;
                salesItem.Action = "I";
                salesItem.quantity = scope.sales.salesItem[i].quantity;
                if (salesItem.quantity == 0 && scope.sales.salesItem[i].salesReturnId != undefined && scope.sales.salesItem[i].salesReturnId != null)
                    salesItem.quantity = scope.sales.salesItem[i].returnedQty;
                salesItem.discount = scope.sales.salesItem[i].discount;
                salesItem.orginalQty = scope.sales.salesItem[i].quantity;
                salesItem.sellingPrice = scope.sales.salesItem[i].sellingPrice;
                salesItem.reminderFrequency = scope.sales.salesItem[i].reminderFrequency == undefined ? "0" : scope.sales.salesItem[i].reminderFrequency.toString();
                salesItem.reminderDate = scope.sales.salesItem[i].reminderDate;
                salesItem.salesReturn = scope.sales.salesItem[i].salesReturn;
                if (salesItem.salesReturn) {
                    scope.returnDiscountInValid = true;
                    scope.transactionType = "return";
                    scope.returnItemCount++;
                } else {
                    scope.transactionType = "sales";
                }
                //Added by Sarubala on 07-11-17
                if (scope.salesPriceSettings == 3) {
                    salesItem.purchasePriceWithoutTax = (salesItem.sellingPrice * 100) / (100 + salesItem.gstTotal);
                }

                if (scope.sales.salesItem[i].salesReturnId != null && scope.sales.salesItem[i].salesReturnId != undefined) {
                    salesItem.salesReturnId = scope.sales.salesItem[i].salesReturnId;
                } else {
                    scope.sales.salesItem[i].salesReturnId = null;
                }

                if (scope.sales.salesItem[i].productStock.product.schedule == "H" || scope.sales.salesItem[i].productStock.product.schedule == "H1" || scope.sales.salesItem[i].productStock.product.schedule == "X") {

                    schedulecount++;
                }
                // Added by Gavaskar 21-11-2017 Start
                salesItem.salesOrderProductSelected = scope.sales.salesItem[i].salesOrderEstimateType;
                if (scope.sales.salesItem[i].salesOrderEstimateId != null) {
                    salesItem.salesOrderEstimatePatientId = scope.sales.patientId;
                    salesItem.salesOrderEstimateId = scope.sales.salesItem[i].salesOrderEstimateId;
                }

                salesItems.push(salesItem);
            }






            //console.log(schedulecount);
            scope.schedulecount = schedulecount;
            scope.sales.salesItem = salesItems;
            scope.editId = salesItems.length + 1;
            scope.IsSelectedInvoiceSeries = false;
            scope.InvoiceSeriestype = 1;
            scope.Editsale = true;

            //Partial credit bill should be selected by Cash in edit - Settu
            if (scope.sales.paymentType == "Credit") {
                scope.sales.paymentType = "Cash";
            }

            // console.log(scope.Editsale);

            ////commented by durga to enable discount text box in sales edit mode

            // scope.discountInValid = true;

            for (var j = 0; j < scope.sales.salesItem.length; j++) {
                if (scope.sales.salesItem[j].discount > 0) {
                    scope.discountInValid = true;
                }
            }

            scope.isSalesTypeMandatory = false;

            scope.selectedBillDate = scope.sales.invoiceDate;
            loadCustomer(scope.sales);
            $.LoadingOverlay("hide");
            loadSalesType();
            scope.BillCalculations();
            scope.DiscountCalculations1();
            scope.loadEditSalesFocus();


        });

    }

    function loadCustomer(sales) {

        customerHelper.data.patientSearchData.mobile = sales.mobile;
        customerHelper.data.patientSearchData.name = sales.name;
        customerHelper.data.patientSearchData.id = sales.patientId;

        scope.Namecookie = sales.name;
        //if (sales.patient.empID != null && sales.patient.empID!= undefined)
        //    customerHelper.data.patientSearchData.empID = sales.patient.empID;

        if (customerHelper.data.patientSearchData.name != "" || customerHelper.data.patientSearchData.mobile != "") {

            customerHelper.search(true, scope.PatientNameSearch);
        }

    }

    function loadSalesType() {
        salesService.getSalesType().then(function (response) {
            //console.log(JSON.stringify(response.data));
            scope.sales.saleType = response.data;
        });
    }

    function getSaleLoyaltyPointSetting(loyaltyId) {
        salesService.getSaleLoyaltyPointSettings(loyaltyId).then(function (response) {
            scope.loyaltyPointSettings = response.data;
            scope.loyaltyPointSettings.maximumRedeemPoint = scope.loyaltyPointSettings.maximumRedeemPoint >= 0 ? scope.loyaltyPointSettings.maximumRedeemPoint : 0;
            $scope.loyaltyPointSettings.isMaximumRedeem = $scope.loyaltyPointSettings.isMaximumRedeem == true ? $scope.loyaltyPointSettings.isMaximumRedeem : false;
        })
    }



});