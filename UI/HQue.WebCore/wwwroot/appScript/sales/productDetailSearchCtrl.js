app.controller('productDetailSearchCtrl', function ($scope, $rootScope, $filter, salesService, productStockService, cacheService, productId, productName, items, close, salesPriceSettings) {

    $scope.BatchPopUpSettings = {};

    $scope.scanBarcodeOption = null;
    $scope.enableSelling = false;
    $scope.batchList1 = [];
    $scope.selectedProduct = null;
    $scope.selectedBatch = { "reminderFrequency": "0" };
    $scope.Math = window.Math;
    $scope.salesPriceSetting = salesPriceSettings;

    $scope.sales = getSalesObject();
    var totalQuantity = 0;

    var drugName = document.getElementById("drugName");
    drugName.focus;

    $scope.onProductSelect = function () {
        loadBatch(null);
    };
    $scope.getAvailableStock = function (productStock, editBatch) {
        var addedItem = getAddedItem(productStock.productStockId);
        var newAddedQty = getAddedItemQuantity(addedItem);

        if (editBatch == null) {
            return productStock.stock - newAddedQty;
        } else if (editBatch.id != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock + parseInt(editBatch.quantity) - newAddedQty;
        } else if (editBatch != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock - (newAddedQty - parseInt(editBatch.quantity));
        } else if (editBatch != null && productStock.productStockId != editBatch.productStockId) {
            return productStock.stock - newAddedQty;
        }
        return productStock.stock;
    };


    function getAddedItemQuantity(addedItems) {
        var qty = 0;
        for (var i = 0; i < addedItems.length; i++) {
            if (addedItems[i].id != null) {
                qty += parseInt(addedItems[i].quantity) - parseInt(addedItems[i].orginalQty);
            } else {
                qty += parseInt(addedItems[i].quantity);
            }
        }
        return qty;
    }

    function getAddedItem(id) {
        var addedItems = [];
        for (var i = 0; i < items.length; i++) {
            if (items[i].productStockId == id)
                addedItems.push(items[i]);
        }
        return addedItems;
    }
    function getAddedItems(id, sellingPrice, discount) {

        for (var i = 0; i < items.length; i++) {

            if (items[i].productStockId == id && items[i].sellingPrice == sellingPrice && items[i].discount == discount) {
                return items[i];
            }

        }
        return null;
    }
    $scope.editId = 1;


    $scope.batchSelected = function (batch) {
        $scope.valPurchasePrice = "";
        var totalQuantity = 0;
        $scope.selectedBatch = $scope.batchList[batch];
        for (var i = 0; i < $scope.batchList.length; i++) {
            totalQuantity += parseInt($scope.batchList[i].stock);
        }
        $scope.selectedBatch.totalQuantity = totalQuantity;
        $scope.selectedBatch.discount = 0;
        dayDiff($scope.selectedBatch.expireDate);
    };

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };


    $scope.cancel = function () {
        if (window.confirm('Are you sure, Do you want to cancel?')) {
            window.location = window.location.origin + window.location.pathname;
        }
        $scope.discountInValid = false;
    };

    $scope.isSelectedItem = 1;
    $scope.shouldBeOpen = true;
    $scope.selectRow = function (val) {
        $scope.isSelectedItem = ($scope.isSelectedItem == val) ? null : val;

    };


    $scope.keyPress = function (e) {

        if (e.keyCode == 38) {

            if ($scope.isSelectedItem == 1) {
                $scope.isSelectedItem = ($scope.batchList1.length) + 1;
            }
            $scope.isSelectedItem--;
            e.preventDefault();

        }
        if (e.keyCode == 40) {

            if ($scope.isSelectedItem == $scope.batchList1.length) {
                // return;
                $scope.isSelectedItem = 0;
            }
            $scope.isSelectedItem++;
            e.preventDefault();

        }
        if (e.keyCode == 13) {
            $scope.submit($scope.batchList1[$scope.isSelectedItem - 1]);
        }
        if (e.keyCode == 27) {
            $scope.close("No");
        }

    };


    $scope.addDetail = function (val) {
        $scope.submit($scope.batchList1[val - 1]);
    };

    $scope.submit = function (selectedProduct) {

        //Added by Sarubala on 06-11-17
        if ($scope.salesPriceSetting == 3) {
            selectedProduct.sellingPrice = parseFloat((selectedProduct.purchasePriceWithoutTax * (1 + (selectedProduct.gstTotal / 100))).toFixed(6));

            for (var i = 0 ; i < $scope.batchList1.length; i++) {
                $scope.batchList1[i].sellingPrice = parseFloat(($scope.batchList1[i].purchasePriceWithoutTax * (1 + ($scope.batchList1[i].gstTotal / 100))).toFixed(6));
            }
        }

        selectedProduct.totalQuantity = totalQuantity;
        $scope.close("Yes");
        $.LoadingOverlay("show");
        $scope.batchList1 = rearrangeBatchList($scope.batchList1, selectedProduct);
        cacheService.set('selectedProduct1', selectedProduct);
        cacheService.set('batchList1', $scope.batchList1);
        $rootScope.$emit("productDetail", selectedProduct);
        $.LoadingOverlay("hide");
    };

    $scope.close = function (result) {
        close(result, 100);
        if (result == "No") {
            $rootScope.$emit("productDetailCancel", null);
        }
        $(".modal-backdrop").hide();
    };

    function getSalesObject() {
        return {
            "salesItem": [],
            "discount": 0,
            "total": 0,
            "TotalQuantity": 0,
            "rackNo": "",
            "vat": 0,
            "cashType": "Full",
            "paymentType": "Cash",
            "deliveryType": "Counter",
            "billPrint": true,
            "sendEmail": false,
            "sendSms": false,
            "doctorMobile": null,
            "credit": null,
            "cardNo": null,
            "cardDate": null,
            "cardDigits": null,
            "cardName": null,
            "isCardSelected": false,
            "isCreditEdit": false,
            "changeDiscount": ""
        };
    }

    $scope.getScanBarcodeOption = function () {
        salesService.getScanBarcodeOption().then(function (resp) {
            $scope.scanBarcodeOption = resp.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getScanBarcodeOption();

    $scope.getEnableSelling = function () {
        salesService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getEnableSelling();

    if (productId != null) {
        $scope.selectedProductName = productName;
      //  if (!salesReturn) {
            productStockService.productBatch(productId).then(function (response) {
                $scope.batchList1 = response.data;
                $scope.batchList1 = $filter('orderBy')($scope.batchList1, 'expireDate');
                totalQuantity = 0;
                var rack = "";
                var tempBatch = [];
                var editBatch = null;
                for (var i = 0; i < $scope.batchList1.length; i++) {
                    $scope.batchList1[i].productStockId = $scope.batchList1[i].id;
                    $scope.batchList1[i].id = null;
                    var availableStock = $scope.getAvailableStock($scope.batchList1[i], editBatch);
                    if (editBatch != null && editBatch.productStockId == $scope.batchList1[i].productStockId) {
                        editBatch.availableQty = availableStock;
                    }

                    totalQuantity += availableStock;

                    if (availableStock == 0) {
                        continue;
                    }

                    if ($scope.batchList1[i].product.rackNo == null || $scope.batchList1[i].product.rackNo == '' || $scope.batchList1[i].product.rackNo == undefined) {
                        $scope.batchList1[i].product.rackNo = '-';
                    }
                    if ($scope.batchList1[i].product.boxNo == null || $scope.batchList1[i].product.boxNo == '' || $scope.batchList1[i].product.boxNo == undefined) {
                        $scope.batchList1[i].product.boxNo = '-';
                    }


                    $scope.batchList1[i].availableQty = availableStock;
                    if ($scope.batchList1[i].rackNo != null || $scope.batchList1[i].rackNo != undefined) {
                        rack = $scope.batchList1[i].rackNo;
                    }

                    tempBatch.push($scope.batchList1[i]);
                }


                if (editBatch != null && tempBatch.length == 0) {
                    var availableStock = $scope.getAvailableStock(editBatch, editBatch);
                    editBatch.availableQty = availableStock;
                    totalQuantity = availableStock;
                    tempBatch.push(editBatch);
                }

                $scope.batchList1 = tempBatch;

            }, function () {
            });
       // }
        //else {
        //    productStockService.getBatchForReturn(productId).then(function (response) {
        //        $scope.batchList1 = response.data;
        //        totalQuantity = 0;
        //        var rack = "";
        //        var tempBatch = [];
        //        var editBatch = null;
        //        for (var i = 0; i < $scope.batchList1.length; i++) {
        //            $scope.batchList1[i].productStockId = $scope.batchList1[i].id;
        //            $scope.batchList1[i].id = null;
        //            var availableStock = $scope.getAvailableStock($scope.batchList1[i], editBatch);
        //            if (editBatch != null && editBatch.productStockId == $scope.batchList1[i].productStockId) {
        //                editBatch.availableQty = availableStock;
        //            }

        //            totalQuantity += availableStock;

        //            //if (availableStock == 0) {
        //            //    continue;
        //            //}


        //            $scope.batchList1[i].availableQty = availableStock;
        //            if ($scope.batchList1[i].rackNo != null || $scope.batchList1[i].rackNo != undefined) {
        //                rack = $scope.batchList1[i].rackNo;
        //            }

        //            tempBatch.push($scope.batchList1[i]);
        //        }


        //        if (editBatch != null && tempBatch.length == 0) {
        //            var availableStock = $scope.getAvailableStock(editBatch, editBatch);
        //            editBatch.availableQty = availableStock;
        //            totalQuantity = availableStock;
        //            tempBatch.push(editBatch);
        //        }

        //        $scope.batchList1 = tempBatch;

        //    }, function () {
        //    });
        //}
        
    }
    $scope.BatchPopUpSettings = function () {
        salesService.getBatchPopUpSettings().then(function (resp) {

            $scope.BatchPopUpSettings = resp.data;

        }, function (error) {

            console.log(error);

        });
    };
    $scope.BatchPopUpSettings();

    function rearrangeBatchList(batchList1, batchList2) {
        var tmpList = [];
        tmpList.push(batchList2);
        for (var i = 0; i < batchList1.length; i++) {
            var exist = false;
            if (batchList1[i] == batchList2) {
                exist = true;
            }
            if (!exist) {
                tmpList.push(batchList1[i]);
            }
        }
        return tmpList;
    };

});






