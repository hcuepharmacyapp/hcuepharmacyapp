app.controller('salesOrderEstimateTemplateListCtrl', function ($scope, vendorOrderModel, vendorOrderService, vendorService, pagerServcie, $filter, printingHelper, toastr, patientService, salesOrderEstimateModel, salesOrderEstimateItemModel, salesTemplateModel, salesModel, smsSettingsModel, salesService,$rootScope) {

   
    var salesOrderEstimate = salesOrderEstimateModel;
    $scope.salesOrderEstimate = salesOrderEstimate;
    var salesOrderEstimateItem = salesOrderEstimateItemModel;

    var salesTemplate = salesTemplateModel;
    $scope.salesTemplate = salesTemplate;
    $scope.salesTemplate.salesTemplateItem = [];
    $scope.salesOrderEstimate.salesOrderEstimateItem = [];
    $scope.salesOrderEstimate.salesEstimateItem = [];

    var sales = salesModel;
    $scope.sales = sales;
    $scope.sales.salesItem = [];

    $scope.orderlist = [];
    $scope.templist = [];
    $scope.smsSettings = angular.copy(smsSettingsModel); //Added by Sarubala on 21-11-17

    
    //pagination
    $scope.salesOrderEstimate.page = pagerServcie.page;
    $scope.salesTemplate.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

   

    $scope.salesOrderclass = "tabSelected";
    $scope.salesEstimateclass = "tabNormal";
    $scope.salesTemplateClass = "tabNormal";
    $scope.isProcessing = true;
    $scope.BySalesOrderSection = true;
    $scope.BySalesEstimateSection = false;
    $scope.BySalesTemplateSection = false;
    $scope.selectedSalesOrder = null;
    $scope.selectedSalesEstimate = null;
    $scope.selectedSalesTemplate = null;
    $scope.message = "No records found";
    $scope.Filtermessage = "";
    $scope.IsOffline = false;
    $scope.showSalesOrder = function () {
        $scope.salesOrderclass = "tabSelected";
        $scope.salesEstimateclass = "tabNormal";
        $scope.salesTemplateClass = "tabNormal";
        $scope.BySalesOrderSection = true;
        $scope.BySalesEstimateSection = false;
        $scope.BySalesTemplateSection = false;
        $scope.orderlist = [];
        $scope.customOrderList = [];
        $scope.salesOrderSearch();
        
    };
    $scope.showSalesEstimate = function () {
        $scope.salesEstimateclass = "tabSelected";
        $scope.salesOrderclass = "tabNormal";
        $scope.salesTemplateClass = "tabNormal";
        $scope.BySalesEstimateSection = true;
        $scope.BySalesOrderSection = false;
        $scope.BySalesTemplateSection = false;
        $scope.orderlist = [];
        $scope.customOrderList = [];
        $scope.salesOrderSearch();
    };
    $scope.showSalesTemplate = function () {
        $scope.salesTemplateClass = "tabSelected";
        $scope.salesOrderclass = "tabNormal";
        $scope.salesEstimateclass = "tabNormal";
        $scope.BySalesTemplateSection = true;
        $scope.BySalesEstimateSection = false;
        $scope.BySalesOrderSection = false;
        $scope.templist = [];
        $scope.templateList = [];
        $scope.salesTemplateSearch();

    };

    function pagesalesOrderSearch() {
        $scope.customOrderList = [];
        $.LoadingOverlay("show");
        if ($scope.BySalesOrderSection == true) {
            $scope.salesOrderEstimate.salesOrderEstimateType = 1;
        }
        else if ($scope.BySalesEstimateSection == true) {
            $scope.salesOrderEstimate.salesOrderEstimateType = 2;
        }
        salesService.salesOrderList($scope.salesOrderEstimate).then(function (response) {
            $scope.customOrderList = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.customOrderList = [];
    $scope.salesOrderSearch = function () {
        $.LoadingOverlay("show");
        $scope.salesOrderEstimate.page.pageNo = 1;
        if ($scope.BySalesOrderSection == true) {
            $scope.salesOrderEstimate.salesOrderEstimateType = 1;
        }
        else if ($scope.BySalesEstimateSection == true) {
            $scope.salesOrderEstimate.salesOrderEstimateType = 2;
        }
        salesService.salesOrderList($scope.salesOrderEstimate).then(function (response) {

            $scope.customOrderList = response.data.list;
           
            pagerServcie.init(response.data.noOfRows, pagesalesOrderSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.salesOrderSearch();


    function pagesalesTemplateSearch() {
        $scope.templateList = [];
        $.LoadingOverlay("show");
        salesService.salesTemplateList($scope.salesTemplate).then(function (response) {
            $scope.templateList = response.data.list;
            $scope.options = { Active: 0, InActive: 1 };
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.templateList = [];
    $scope.salesTemplateSearch = function () {
        $.LoadingOverlay("show");
        $scope.salesTemplate.page.pageNo = 1;
        $scope.salesTemplate.isActive = null;
        salesService.salesTemplateList($scope.salesTemplate).then(function (response) {

            $scope.templateList = response.data.list;
            $scope.options = { Active: 0, InActive: 1 };
           
            pagerServcie.init(response.data.noOfRows, pagesalesTemplateSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
   
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.toggleProductDetail = function (obj) {        
            var row = obj.target.getAttribute("data");
            $('#chip-wrapper' + row).slideToggle();
            $('#chip-wrapper' + row).show();
            if ($('#chip-btn' + row).text() === '+')
                $('#chip-btn' + row).text('-');
            else {
                $('#chip-btn' + row).text('+');
            }
    };

    $scope.editSalesTemplateStatus = function (item) {
        item.isEdit = true;
        if (item.isActive == 0) {
            $scope.salesTemplate.isActive = "Active";
        }
        else {
            $scope.salesTemplate.isActive = "InActive";
        }
    }
    $scope.updateSalesTemplateStatus = function (item) {
        $.LoadingOverlay("show");
        salesService.updateSalesTemplateStatus(item).then(function (response) {
            item.isEdit = !response.data;
            toastr.success('Template status  updated Successfully');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    
    $scope.saleOrderEdit = function (salesOrderEstimate) {
        if (salesOrderEstimate.isEstimateActive == 1 && salesOrderEstimate.salesOrderEstimateType == 1) {
            toastr.info('Order edit is not allowed since it seems all items converted/deleted.');
            return;
        }
        if (salesOrderEstimate.isEstimateActive == 1 && salesOrderEstimate.salesOrderEstimateType == 2) {
            toastr.info('Estimate edit is not allowed since it seems all items converted/deleted.');
            return;
        }
       
        window.location.href = "/sales/UpdateSalesOrderEstimate?salesOrderEstimateId=" + salesOrderEstimate.id;
        //window.location.href = "/vendorOrder/SalesOrder?salesOrderEstimateId=" + salesOrderEstimate.id;
        ////window.localStorage.setItem("salesOrderEstimateEdit", JSON.stringify(salesOrderEstimate));
        ////window.location.assign('/vendorOrder/SalesOrder?salesOrderEstimateId=' + salesOrderEstimate.id);
    };

    $scope.selectedTemplateNameList = {};
    $scope.editId = 1;
    $scope.TemplateConvertTosale = function (ind, obj) {

        window.localStorage.setItem("ConvertTemplatetoSaleObj", JSON.stringify(obj));
        window.location.assign('/sales/index');

    }

    $scope.EstimateConvertTosale = function (ind, obj) {
       
        if ($scope.isDepartmentsave == "0" && obj.patient.patientType == "2")
        {
            toastr.info('Please enable department in Sales settings');
            return;
        }
        window.localStorage.setItem("ConvertEstimatetoSaleObj", JSON.stringify(obj));
        window.location.assign('/sales/index');
    }

    $scope.resendSms = function (salesOrderEstimate) {
            $.LoadingOverlay("show");
            var salesOrderSms = removeDeletedConvertedItems(salesOrderEstimate)
            if (salesOrderEstimate.salesOrderEstimateType == 1)
            {
                if (salesOrderSms.salesOrderEstimateItem.length == 0 || salesOrderEstimate.isEstimateActive == 1) {
                    toastr.info('SMS not allowed. It seems all items converted/deleted.');
                    $.LoadingOverlay("hide");
                    return;
                }
            }
            else if (salesOrderEstimate.salesOrderEstimateType == 2)
            {
                if (salesOrderSms.salesEstimateItem.length == 0 || salesOrderEstimate.isEstimateActive == 1) {
                    toastr.info('SMS not allowed. It seems all items converted/deleted.');
                    $.LoadingOverlay("hide");
                    return;
                }
            }
            
        salesService.resendSalesOrderEstimateSms(salesOrderSms)
            .then(function (resp) {
                $.LoadingOverlay("hide");
                toastr.success('Sms Sent Successfully');
            }, function (error) {
                $.LoadingOverlay("hide");
                console.log(error);
                toastr.error("", "Error");
            });
    };
    $scope.resendMail = function (salesOrderEstimate) {
        $.LoadingOverlay("show");
        var salesOrderEstimateEmail = removeDeletedConvertedItems(salesOrderEstimate)
        if (salesOrderEstimate.salesOrderEstimateType == 1) {
            if (salesOrderEstimateEmail.salesOrderEstimateItem.length == 0 || salesOrderEstimate.isEstimateActive == 1) {
                toastr.info('Email not allowed. It seems all items converted/deleted.');
                $.LoadingOverlay("hide");
                return;
            }
        }
        else if (salesOrderEstimate.salesOrderEstimateType == 2) {
            if (salesOrderEstimateEmail.salesEstimateItem.length == 0 || salesOrderEstimate.isEstimateActive == 1) {
                toastr.info('Email not allowed. It seems all items converted/deleted.');
                $.LoadingOverlay("hide");
                return;
            }
        }
        salesService.resendSalesOrderEstimateEmail(salesOrderEstimateEmail)
            .then(function (resp) {
                $.LoadingOverlay("hide");
                toastr.success('Mail Sent Successfully');
            }, function (error) {
                $.LoadingOverlay("hide");
                console.log(error);
                toastr.error("", "Error");
            });
    };

    function removeDeletedConvertedItems(salesOrderEstimate) {
        
        if (salesOrderEstimate.salesOrderEstimateType == 1)
        {
            var salesOrderSmsEmail = angular.copy(salesOrderEstimate);
            var salesOrderItem = [];
            for (var i = 0; i < salesOrderSmsEmail.salesOrderEstimateItem.length; i++) {
                if (salesOrderSmsEmail.salesOrderEstimateItem[i].isDeleted != 1) {
                    salesOrderItem.push(salesOrderSmsEmail.salesOrderEstimateItem[i]);
                }
            }
            salesOrderSmsEmail.salesOrderEstimateItem = salesOrderItem;
            return salesOrderSmsEmail;
        }
        else (salesOrderEstimate.salesOrderEstimateType == 2)
        {
            var salesEstimateSmsEmail = angular.copy(salesOrderEstimate);
            var salesEstimateItems = [];
            for (var i = 0; i < salesEstimateSmsEmail.salesOrderEstimateItem.length; i++) {
                if (salesEstimateSmsEmail.salesOrderEstimateItem[i].isDeleted != 1) {
                    salesEstimateItems.push(salesEstimateSmsEmail.salesOrderEstimateItem[i]);
                }
            }
            salesEstimateSmsEmail.salesEstimateItem = salesEstimateItems;
            return salesEstimateSmsEmail;
        }
       
    };

    $scope.printOrder = function (salesOrderEstimate) {
        var salesOrderEstimatePrints = removeDeletedConvertedItems(salesOrderEstimate)
        if (salesOrderEstimatePrints.salesOrderEstimateType == 1) {
            if (salesOrderEstimatePrints.salesOrderEstimateItem.length == 0 || salesOrderEstimate.isEstimateActive == 1) {
                toastr.info('Print not allowed. It seems all items converted/deleted.');
                $.LoadingOverlay("hide");
                return;
            }
        }
        else if (salesOrderEstimatePrints.salesOrderEstimateType == 2) {
            if (salesOrderEstimatePrints.salesEstimateItem.length == 0 || salesOrderEstimate.isEstimateActive == 1) {
                toastr.info('Print not allowed. It seems all items converted/deleted.');
                $.LoadingOverlay("hide");
                return;
            }
        }
        printingHelper.salesOrderEstimatePrint(salesOrderEstimate.id);
    };

    function getSalesSettings() {
        salesService.getAllSalesSettings().then(function (response) {
            if (response.data == "" || response.data == null || response.data == undefined) {
            } else {
                if (response.data.patientTypeDept != undefined) {
                    $scope.isDepartmentsave = "0";
                    if (response.data.patientTypeDept == true) {
                        $scope.isDepartmentsave = "1";
                    }
                } else {
                    $scope.isDepartmentsave = "0";
                }
            }

        }, function (error) {
            console.log(error);
        });
    }

    getSalesSettings();

    //Added by Sarubala on 21-11-17
    function getSmsSettings() {
        salesService.getSmsSettings().then(function (response) {
            $scope.smsSettings = response.data;
        }, function (error) {
            console.log(error);
        });
    };

    getSmsSettings();

    SalesOrderEstimateEditReturn();
    $scope.salesOrderEstimateTypes = "";
    function SalesOrderEstimateEditReturn() {
        if (window.localStorage.getItem("doSelectedOrderEstimate") == null) {
        } else {
            $scope.salesOrderEstimateTypes = JSON.parse(window.localStorage.getItem("doSelectedOrderEstimate"));
            window.localStorage.removeItem('doSelectedOrderEstimate');
            
            if ($scope.salesOrderEstimateTypes == 1) {
                $scope.salesOrderclass = "tabSelected";
                $scope.salesEstimateclass = "tabNormal";
                $scope.salesTemplateClass = "tabNormal";
                $scope.BySalesOrderSection = true;
                $scope.BySalesEstimateSection = false;
                $scope.BySalesTemplateSection = false;
                $scope.orderlist = [];
                $scope.customOrderList = [];
                $scope.salesOrderSearch();
            }
            else if ($scope.salesOrderEstimateTypes == 2) {
                $scope.salesEstimateclass = "tabSelected";
                $scope.salesOrderclass = "tabNormal";
                $scope.salesTemplateClass = "tabNormal";
                $scope.BySalesEstimateSection = true;
                $scope.BySalesOrderSection = false;
                $scope.BySalesTemplateSection = false;
                $scope.orderlist = [];
                $scope.customOrderList = [];
                $scope.salesOrderSearch();
            }
        }
    }
   
    
});
