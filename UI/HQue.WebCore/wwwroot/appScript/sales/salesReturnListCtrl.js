app.controller('salesReturnListCtrl', function ($scope, salesService, salesReturnModel, pagerServcie, patientService, printingHelper, $filter, productService) {

    var salesReturn = salesReturnModel;

    $scope.search = salesReturn;
    $scope.list = [];
    $scope.search.sales = "";
    $scope.isComposite = false;
    $scope.isEnableRoundOff = false;

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        $.LoadingOverlay("show");
        salesService.returnList($scope.search).then(function (response) {
            $scope.list = response.data.list;
            for (var x = 0; x < $scope.list.length; x++) {
                var active = 0;
                $scope.list[x].sumQuantity = 0;
                for (var y = 0; y < $scope.list[x].salesReturnItem.length; y++) {

                    $scope.list[x].sumQuantity = $scope.list[x].sumQuantity + $scope.list[x].salesReturnItem[y].quantity;

                    if ($scope.list[x].salesReturnItem[y].mrpSellingPrice > 0) {
                        var discountAmtPerQty = ($scope.list[x].salesReturnItem[y].mrpSellingPrice * $scope.list[x].salesReturnItem[y].discount) / 100;
                        $scope.list[x].salesReturnItem[y].discountAmtPerQty = discountAmtPerQty;
                    }
                    else {
                        var discountAmtPerQty = ($scope.list[x].salesReturnItem[y].total * $scope.list[x].salesReturnItem[y].discount) / 100;
                        $scope.list[x].salesReturnItem[y].discountAmtPerQty = discountAmtPerQty;
                    }

                    if ($scope.list[x].salesReturnItem[y].cancelType == 1) {
                        active++;
                    }
                }
                if (active > 0) {
                    $scope.list[x].Status = true;
                } else {
                    $scope.list[x].Status = false;
                }
            }
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.salesReturnSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        if ($scope.search.drugName)
            $scope.search.searchProductId = $scope.search.drugName.id;
        else {
            $scope.search.searchProductId = "";
        }

        salesService.returnList($scope.search).then(function (response) {
            $scope.list = response.data.list;
            if ($scope.list.length > 0) {
                $scope.isComposite = angular.isUndefinedOrNull($scope.list[0].instance.gstselect) ? false : $scope.list[0].instance.gstselect; // Added for hide gst/vat
            }
            for (var x = 0; x < $scope.list.length; x++) {
                var active = 0;
                $scope.list[x].sumQuantity = 0;
                for (var y = 0; y < $scope.list[x].salesReturnItem.length; y++) {

                    $scope.list[x].sumQuantity = $scope.list[x].sumQuantity + $scope.list[x].salesReturnItem[y].quantity;

                    if ($scope.list[x].salesReturnItem[y].mrpSellingPrice > 0) {
                        var discountAmtPerQty = ($scope.list[x].salesReturnItem[y].mrpSellingPrice * $scope.list[x].salesReturnItem[y].discount) / 100;
                        $scope.list[x].salesReturnItem[y].discountAmtPerQty = discountAmtPerQty;
                    }
                    else {
                        var discountAmtPerQty = ($scope.list[x].salesReturnItem[y].total * $scope.list[x].salesReturnItem[y].discount) / 100;
                        $scope.list[x].salesReturnItem[y].discountAmtPerQty = discountAmtPerQty;
                    }

                    if ($scope.list[x].salesReturnItem[y].cancelType == 1) {
                        active++
                    }
                }
                if (active > 0) {
                    $scope.list[x].Status = true;
                } else {
                    $scope.list[x].Status = false;
                }
            }

            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.salesReturnSearch();
    //added by nandhini 13.09.17
    $scope.getProducts = function (val) {

        return productService.SalesReturnProductdrugFilter(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.toggleProductDetail = function (obj, item) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');

            salesService.getReturnListAudit(item.id).then(function (response) {
                if (response.data.length > 0) {
                    item.salesReturnItemAudit = response.data;
                }

            }, function (error) {
                console.log(error);
            });
        }
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.clearSearch = function () {
        $scope.name = '';
        $scope.search.name = "";
        $scope.search.invoiceNo = "";
        $scope.search.FromReturnDate = "";
        $scope.search.ToReturnDate = "";
        $scope.search.toReturnNo = "";
        $scope.search.fromReturnNo = "";
        $scope.search.fromCancelNo = "";
        $scope.search.toCancelNo = "";
        $scope.search.select1 = "";
        $scope.search.drugName = "";
        $scope.dateCondition = false;
        $scope.CustomerMailCondition = false;
        $scope.productCondition = false;
        $scope.CustomerCondition = false;
        $scope.cancelnumberCondition = false;
        $scope.returnnumberCondition = false;
        $scope.invoicenumberCondition = false;
        $scope.search.select = "";
        if ($scope.search.sales != undefined) {
            $scope.search.sales.name = "";
            $scope.search.sales.patientId = "";
            $scope.search.sales.mobile = "";
            $scope.search.sales.invoiceNo = "";
            $scope.search.returnNo = "";
            $scope.search.sales.email = "";
            $scope.search.patientId = "";

        }
        $scope.salesReturnSearch();
    }

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.Enterkey = function (nextid) {
        var ele = document.getElementById(nextid);
        ele.focus();
    };

    //Added by Sarubala for return no filter - start
    $scope.InvoiceSeriesItems = [];
    function getInvoiceSeriesItems() {
        salesService.getInvoiceSeriesItems().then(function (response) {

            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }

        }, function () {

        });
    }
    //Added by Sarubala for return no filter - end

    $scope.changefilters = function () {


        if ($scope.search.select == 'returnDate') {
            $scope.dateCondition = true;
            $scope.search.FromReturnDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.ToReturnDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.CustomerCondition = false;
            $scope.invoicenumberCondition = false;
            $scope.CustomerMailCondition = false;
            $scope.returnnumberCondition = false;
            $scope.cancelnumberCondition = false;
            $scope.CustomerMobileCondition = false;
            $scope.productCondition = false;
            $scope.search.returnNo = "";
            $scope.search.sales.email = "";
            $scope.search.sales.invoiceNo = "";
            $scope.search.toReturnNo = "";
            $scope.search.fromReturnNo = "";
            $scope.search.fromCancelNo = "";
            $scope.search.toCancelNo = "";
            $scope.name = "";
            $scope.search.select1 = "";
            $scope.search.drugName = "";
            $scope.search.sales = "";
            $scope.search.sales.name = "";
            $scope.search.sales.patientId = "";
            $scope.search.sales.mobile = "";
            $scope.search.patientId = "";

        }
            // Added by arun for customer name on 22nd march by punithavel req.
        else if ($scope.search.select == 'customerName') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.CustomerCondition = true;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
            $scope.CustomerMailCondition = false;
            $scope.returnnumberCondition = false;
            $scope.cancelnumberCondition = false;
            $scope.CustomerMobileCondition = false;
            $scope.productCondition = false;
            $scope.search.FromReturnDate = "";
            $scope.search.ToReturnDate = "";
            $scope.search.returnNo = "";
            $scope.search.sales.email = "";
            $scope.search.sales.invoiceNo = "";
            $scope.search.toReturnNo = "";
            $scope.search.fromReturnNo = "";
            $scope.search.fromCancelNo = "";
            $scope.search.toCancelNo = "";
            $scope.search.select1 = "";
            $scope.search.drugName = "";
        }

        else if ($scope.search.select == 'invoiceNo') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.CustomerCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = true;
            $scope.CustomerMailCondition = false;
            $scope.returnnumberCondition = false;
            $scope.cancelnumberCondition = false;
            $scope.CustomerMobileCondition = false;
            $scope.productCondition = false;
            $scope.search.FromReturnDate = "";
            $scope.search.ToReturnDate = "";
            $scope.search.returnNo = "";
            $scope.search.sales.email = "";
            $scope.search.toReturnNo = "";
            $scope.search.fromReturnNo = "";
            $scope.search.fromCancelNo = "";
            $scope.search.toCancelNo = "";
            $scope.name = "";
            $scope.search.select1 = "";
            $scope.search.drugName = "";
            $scope.search.sales = "";
            $scope.search.sales.name = "";
            $scope.search.sales.patientId = "";
            $scope.search.sales.mobile = "";
            $scope.search.patientId = "";
        }
        else if ($scope.search.select == 'returnNo') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.CustomerCondition = false;
            $scope.EnterCondition = false;
            $scope.returnnumberCondition = true;
            $scope.cancelnumberCondition = false;
            $scope.invoicenumberCondition = false;
            $scope.CustomerMailCondition = false;
            $scope.CustomerMobileCondition = false;
            $scope.productCondition = false;
            $scope.search.FromReturnDate = "";
            $scope.search.ToReturnDate = "";
            $scope.search.sales.email = "";
            $scope.search.toReturnNo = "";
            $scope.search.fromReturnNo = "";
            $scope.search.fromCancelNo = "";
            $scope.search.toCancelNo = "";
            $scope.search.sales.invoiceNo = "";
            $scope.name = "";
            $scope.search.select1 = "Custom";
            getInvoiceSeriesItems();
            $scope.search.drugName = "";
            $scope.search.sales = "";
            $scope.search.sales.name = "";
            $scope.search.sales.patientId = "";
            $scope.search.sales.mobile = "";
            $scope.search.patientId = "";
        }
        else if ($scope.search.select == 'cancelNo') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.CustomerCondition = false;
            $scope.EnterCondition = false;
            $scope.returnnumberCondition = false;
            $scope.cancelnumberCondition = true;
            $scope.invoicenumberCondition = false;
            $scope.CustomerMailCondition = false;
            $scope.CustomerMobileCondition = false;
            $scope.productCondition = false;
            $scope.search.FromReturnDate = "";
            $scope.search.ToReturnDate = "";
            $scope.search.sales.email = "";
            $scope.search.toReturnNo = "";
            $scope.search.fromReturnNo = "";
            $scope.search.sales.invoiceNo = "";
            $scope.search.fromCancelNo = "";
            $scope.search.toCancelNo = "";
            $scope.name = "";
            $scope.search.drugName = "";
            $scope.search.sales = "";
            $scope.search.sales.name = "";
            $scope.search.sales.patientId = "";
            $scope.search.sales.mobile = "";
            $scope.search.patientId = "";
        }
        else if ($scope.search.select == 'customerEmail') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.CustomerCondition = false;
            $scope.CustomerMailCondition = true;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
            $scope.returnnumberCondition = false;
            $scope.cancelnumberCondition = false;
            $scope.CustomerMobileCondition = false;
            $scope.productCondition = false;
            $scope.search.FromReturnDate = "";
            $scope.search.ToReturnDate = "";
            $scope.search.sales.invoiceNo = "";
            $scope.search.toReturnNo = "";
            $scope.search.fromReturnNo = "";
            $scope.search.fromCancelNo = "";
            $scope.search.toCancelNo = "";
            $scope.name = "";
            $scope.search.select1 = "";
            $scope.search.drugName = "";
            $scope.search.sales = "";
            $scope.search.sales.name = "";
            $scope.search.sales.patientId = "";
            $scope.search.sales.mobile = "";
            $scope.search.patientId = "";

        } else if ($scope.search.select == "customerMobile") {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.CustomerCondition = false;
            $scope.CustomerMailCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
            $scope.returnnumberCondition = false;
            $scope.cancelnumberCondition = false;
            $scope.CustomerMobileCondition = true;
            $scope.productCondition = false;
            $scope.search.FromReturnDate = "";
            $scope.search.ToReturnDate = "";
            $scope.search.sales.invoiceNo = "";
            $scope.search.toReturnNo = "";
            $scope.search.fromReturnNo = "";
            $scope.search.fromCancelNo = "";
            $scope.search.toCancelNo = "";
            $scope.name = "";
            $scope.search.select1 = "";
            $scope.search.drugName = "";
            $scope.search.sales = "";
            $scope.search.sales.name = "";
            $scope.search.sales.patientId = "";
            $scope.search.sales.mobile = "";
            $scope.search.patientId = "";
        }
        else if ($scope.search.select == "product") {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.CustomerCondition = false;
            $scope.CustomerMailCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
            $scope.returnnumberCondition = false;
            $scope.cancelnumberCondition = false;
            $scope.productCondition = true;
            $scope.CustomerMobileCondition = false;
            $scope.search.FromReturnDate = "";
            $scope.search.ToReturnDate = "";
            $scope.search.sales.invoiceNo = "";
            $scope.search.toReturnNo = "";
            $scope.search.fromReturnNo = "";
            $scope.search.fromCancelNo = "";
            $scope.search.toCancelNo = "";
            $scope.name = "";
            $scope.search.select1 = "";
            $scope.search.drugName = "";
            $scope.search.sales = "";
            $scope.search.sales.name = "";
            $scope.search.sales.patientId = "";
            $scope.search.sales.mobile = "";
            $scope.search.patientId = "";
        } else {

            $scope.search.name = "";
            $scope.search.invoiceNo = "";
            $scope.search.FromReturnDate = "";
            $scope.search.ToReturnDate = "";
            $scope.search.returnNo = "";
            $scope.search.sales.email = "";
            $scope.search.sales.invoiceNo = "";
            $scope.search.toReturnNo = "";
            $scope.search.fromReturnNo = "";
            $scope.search.fromCancelNo = "";
            $scope.search.toCancelNo = "";
            $scope.search.select = "";
            $scope.dateCondition = false;
            $scope.CustomerMailCondition = false;
            $scope.CustomerCondition = false;
            $scope.returnnumberCondition = false;
            $scope.cancelnumberCondition = false;
            $scope.invoicenumberCondition = false;
            $scope.CustomerMobileCondition = false;
            $scope.productCondition = false;
            $scope.search.select1 = "";
            $scope.search.drugName = "";
            $scope.search.sales = "";
            $scope.search.sales.name = "";
            $scope.search.sales.patientId = "";
            $scope.search.sales.mobile = "";
            $scope.search.patientId = "";
        }
    };
    $scope.printReturnList = function (salesReturn) {
        //window.open("/Invoice/GetReturnListById?salesid=" + salesReturn.sales.id + "&salesreturnid=" + salesReturn.id);
        printingHelper.printReturnInvoice(salesReturn.id);

    };
    $scope.viewBill = function (salesReturn) {
        printingHelper.getReturnInvoiceView(salesReturn.id)
    }

    $scope.editReturn = function (salesReturn) {
        window.location.href = "/salesReturn/editReturnOnly?id=" + salesReturn.id;
    }

    $scope.changeCustomerName = function () {
        var custname = document.getElementById("customerName").value;


        console.log(custname);

        //if ($scope.sales.salesItem.length > 0) {
        //    if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
        //        if (custname == "") {
        //            $scope.iscustomerNameMandatory = true;
        //        } else {
        //            $scope.iscustomerNameMandatory = false;
        //            $scope.flagCustomername = false;
        //        }
        //    }
        //}

    };

    $scope.Namecookie = "";
    $scope.getPatient = function (val) {
        // $scope.customerHelper.data.patientSearchData.name = val;
        $scope.Namecookie = val;

        if ($scope.PatientNameSearch == "" || $scope.PatientNameSearch == undefined) {
            $scope.PatientNameSearch = 2;
        }

        if ($scope.PatientNameSearch == 1) {
            return patientService.GetPatientName(val, 1).then(function (response) {

                var origArr = response.data;
                var newArr = [],
           origLen = origArr.length,
           found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });
            });
        } else {
            return patientService.Patientlist(val, 1).then(function (response) {

                var origArr = response.data;
                var newArr = [],
           origLen = origArr.length,
           found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });
            });
        }
    };

    //$scope.getCustomerBalance = function () {
    //    customerReceiptService.getCustomerBalance(customerHelper.data.selectedCustomer.mobile, customerHelper.data.selectedCustomer.name).then(function (resp) {
    //        if (resp.data.length == 0) {
    //            customerHelper.data.customerBalance = 0;
    //            return;
    //        }

    //        if (resp.data.credit != undefined)
    //            customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
    //    });
    //};
    var patientSelected = 0;
    $scope.onPatientSelect = function (obj, event) {


        if (obj !== undefined) {
            $scope.search.sales = { 'name': '' };
            $scope.search.sales.name = obj.name;
            $scope.search.sales.patientId = obj.id;
            $scope.search.sales.mobile = obj.mobile;
            $scope.search.patientId = obj.id;
        }

    };

    getPatientSearchType();
    $scope.PatientNameSearch = "1";
    function getPatientSearchType() {
        salesService.getPatientSearchType().then(function (response) {

            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.PatientNameSearch = "1";
            } else {
                if (response.data.patientSearchType != undefined) {
                    $scope.PatientNameSearch = response.data.patientSearchType;
                } else {
                    $scope.PatientNameSearch = "1";
                }
            }
        }, function () {

        });
    }


    $scope.keyEnter = function (event, e) {



        console.log(e);
        if (event.which === 8) {
            if ($scope.customerHelper.data.patientSearchData.name == "" || $scope.customerHelper.data.patientSearchData.name == undefined) {
                $scope.Namecookie = "";
            }
        }
        var ele = document.getElementById(e);

        if (event.which === 13) // Enter key
        {
            ele.focus();

        }


        if (event.which === 9) {
            $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
        }



    };

    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        salesService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    }
    $scope.getEnableSelling();

    $scope.getTotal = function (salesReturn) {
        var total = 0;
        var aa = salesReturn.salesReturnItem;
        for (var i = 0; i < aa.length; i++) {

            if (angular.isUndefinedOrNull(aa[i].mrpSellingPrice))
                aa[i].mrpSellingPrice = aa[i].productStock.sellingPrice;


            total += (((aa[i].mrpSellingPrice - aa[i].discountAmtPerQty) * aa[i].quantity) -
             (aa[i].total * (angular.isUndefinedOrNull(salesReturn.discount) ? 0 : salesReturn.discount / 100)));

        }
        total -= !angular.isUndefinedOrNull(salesReturn.returnCharges) ? salesReturn.returnCharges : 0;
        return total;
    }

    angular.isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null || val === "" || val === 0
    }

    //By San - 15-11-2017
    $scope.getRoundOffSettings = function () {
        $.LoadingOverlay("show");
        salesService.getRoundOffSettings().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.isEnableRoundOff = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
        });
    }
    //$scope.getRoundOffSettings();


});
app.filter('round', function () {
    return function (input) {
        return Math.round(input);
    };
});