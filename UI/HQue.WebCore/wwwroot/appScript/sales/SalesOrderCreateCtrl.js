app.controller('salesOrderCreateCtrl', function ($scope, productService, vendorOrderService, vendorService, toastr, $rootScope, $filter, ModalService, salesService, vendorPurchaseService, patientService, salesOrderEstimateModel, salesOrderEstimateItemModel, salesTemplateModel, productStockService, cacheService) {

    var salesOrderEstimate = salesOrderEstimateModel;
    $scope.salesOrderEstimate = salesOrderEstimate;
    var salesOrderEstimateItem = salesOrderEstimateItemModel;

    var salesTemplate = salesTemplateModel;
    $scope.salesTemplate = salesTemplate;
    $scope.salesTemplate.salesTemplateItem = [];
    $scope.salesOrderEstimate.salesOrderEstimateItem = [];
    $scope.salesOrderEstimate.salesEstimateItem = [];
    $scope.salesOrderEstimate.patient = {};
    $scope.templateNameList = [];
    $scope.SelectedTemplateNameList = [];
    $scope.IsEdit = false;
    $scope.GSTEnabled = true;
    $scope.draftOrderCount = 0;
    $scope.draftOrderLoaded = false;
    $scope.salesOrderclass = "tabSelected";
    $scope.salesEstimateclass = "tabNormal";
    $scope.salesTemplateClass = "tabNormal";
    $scope.isProcessing = true;
    $scope.BySalesOrderSection = true;
    $scope.BySalesEstimateSection = false;
    $scope.BySalesTemplateSection = false;
    $scope.selectedSalesOrder = null;
    $scope.selectedSalesEstimate = null;
    $scope.selectedSalesTemplate = null;
    $scope.EditSalesOrderEstimate = false;
    $scope.message = "No records found";
    $scope.Filtermessage = "";
    $scope.IsOffline = false;
    getIsMaxDiscountAvail();
    resetOrder();
    resetEstimate();
  
    $scope.showSalesOrder = function () {
        $scope.salesOrderclass = "tabSelected";
        $scope.salesEstimateclass = "tabNormal";
        $scope.salesTemplateClass = "tabNormal";
        $scope.BySalesOrderSection = true;
        $scope.BySalesEstimateSection = false;
        $scope.BySalesTemplateSection = false;
        $scope.focusCustomerName = true;
        $scope.focusestimateCustomerName = false;
        $scope.focustemplateProductName = false;
       
        resetOrder();
        getIsMaxDiscountAvail();
        getSalesSettings();
       // CheckSalesTemplates();
    };
    $scope.showSalesEstimate = function () {
        $scope.salesEstimateclass = "tabSelected";
        $scope.salesOrderclass = "tabNormal";
        $scope.salesTemplateClass = "tabNormal";
        $scope.BySalesEstimateSection = true;
        $scope.BySalesOrderSection = false;
        $scope.BySalesTemplateSection = false;
        $scope.focusestimateCustomerName = true;
        $scope.focusCustomerName = false;
        $scope.focustemplateProductName = false;
        $scope.Enablesalesbtn = false;
        resetEstimate();
        getIsMaxDiscountAvail();
        getSalesSettings();
    };
    $scope.showSalesTemplate = function () {
        $scope.salesTemplateClass = "tabSelected";
        $scope.salesOrderclass = "tabNormal";
        $scope.salesEstimateclass = "tabNormal";
        $scope.BySalesTemplateSection = true;
        $scope.BySalesEstimateSection = false;
        $scope.BySalesOrderSection = false;
        $scope.templateNameList = [];
        $scope.salesTemplate.salesTemplateItem = [];
        $scope.SelectedTemplateNameList = [];
        salesTemplateNameList();
        $scope.focustemplateProductName = true;
        $scope.focusestimateCustomerName = false;
        $scope.focusCustomerName = false;
       
    };
    $scope.PopupAddNewProduct = function () {
        var m = ModalService.showModal({
            "controller": "productCreateCtrl",
            "templateUrl": "createProduct",
            "inputs": {
                "mode": "Create",
                "poproductname": '',
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();

        });
    };
    function resetOrder() 
    {
        $scope.salesOrderEstimate.salesOrderEstimateItem = [];
        $scope.salesOrderEstimate.salesEstimateItem = [];
        $scope.salesOrderEstimate.patient = {};
        $scope.salesOrderCustomerName = "";
        $scope.salesEstimateCustomerName = "";
        $scope.salesOrderpatientId = "";
        $scope.salesEstimatepatientId = "";
        $scope.salesOrderEstimate.patientId = "";
        $scope.selectedSalesOrder = "";
        $scope.selectedSalesOrder.quantity = "";
        $scope.selectedSalesOrder.discount = "";
        $scope.selectedSalesOrder.sellingPrice = "";
        $scope.selectedSalesOrder.isEditMrp = false;
        $scope.maxDiscountExceeded = false;
        $scope.maxEditDiscountExceeded = false;
        $scope.maxEditqtyExceeded = false;
        $scope.EditSalesOrderEstimate = false;
    }
   
    function resetEstimate() {
        $scope.salesOrderEstimate.salesOrderEstimateItem = [];
        $scope.salesOrderEstimate.salesEstimateItem = [];
        $scope.salesOrderEstimate.patient = {};
        $scope.salesOrderCustomerName = "";
        $scope.salesEstimateCustomerName = "";
        $scope.salesOrderpatientId = "";
        $scope.salesEstimatepatientId = "";
        $scope.salesOrderEstimate.patientId = "";
        $scope.selectedSalesEstimate = "";
        $scope.selectedBatch = "";
        $scope.selectedBatch = {};
        $scope.batchList = {};
        $scope.maxDiscountExceeded = false;
        $scope.maxEditDiscountExceeded = false;
        $scope.maxEditqtyExceeded = false;
        $scope.EditSalesOrderEstimate = false;
       
    }

    $scope.resetSalesOrder = function () {
        $scope.salesOrderCustomerName = "";
        $scope.salesEstimateCustomerName = "";
        $scope.salesOrderpatientId = "";
        $scope.salesEstimatepatientId = "";
        $scope.salesOrderEstimate.patientId = "";
        $scope.selectedSalesOrder = "";
        $scope.selectedSalesOrder.quantity = "";
        $scope.selectedSalesOrder.discount = "";
        $scope.selectedSalesOrder.sellingPrice = "";
        $scope.selectedSalesOrder.isEditMrp = false;
        $scope.focusCustomerName = true;
        $scope.focusestimateCustomerName = false;
        $scope.focustemplateProductName = false;
        document.getElementById("customerName").focus();
    };
    $scope.resetSalesEstimate = function () {
        $scope.salesOrderCustomerName = "";
        $scope.salesEstimateCustomerName = "";
        $scope.salesOrderpatientId = "";
        $scope.salesEstimatepatientId = "";
        $scope.salesOrderEstimate.patientId = "";
        $scope.selectedSalesEstimate = "";
        $scope.selectedBatch = "";
        $scope.selectedBatch = {};
        $scope.batchList = {};
        $scope.focusCustomerName = false;
        $scope.focusestimateCustomerName = true;
        $scope.focustemplateProductName = false;
        document.getElementById("estimateCustomerName").focus();
    };

    ////$scope.init = function () {

    ////    vendorOrderService.getInstanceData().then(function (response) {
    ////        $scope.instance = response.data;
    ////    }, function () {
    ////    });

    ////}

    function salesTemplateNameList() {
       
        salesService.getTemplateNameList().then(function (response) {
            $scope.templateNameList = response.data;
            SetTemplateNameDetails();
        }, function () {
        });
    }

    function SetTemplateNameDetails() {
        for (var i = 0; i < $scope.templateNameList.length; i++) {
           
            $scope.templateNameList[i].templateName = ($scope.templateNameList[i].templateName);
           
        } 
    };
   

    $scope.toggleProductDetail = function (row) {
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.getProducts = function (val) {
        return productService.nonHiddenProductList(val).then(function (response) {
            response.data=$filter('orderBy')(response.data,'-totalstock');
            var flags = [], output = [], l =  response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }

            return output.map(function (item) {
                return item;
            });
        });
    };

    $scope.getEstimateProducts = function (val) {
            return productStockService.getSalesProduct(val).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
        });
    };
    
    $scope.onProductSelect = function ($event, selectedProduct1) {
        $scope.quantityExceed = 0;
        if (selectedProduct1 != null) {
            vendorPurchaseService.getPurchaseValues(selectedProduct1.id, selectedProduct1.name).then(function (response) {
                $scope.vendorPurchaseItem = response.data.vendorPurchaseItem;

                if ($scope.BySalesOrderSection == true)
                {
                    if ($scope.vendorPurchaseItem.packageSellingPrice == "" || $scope.vendorPurchaseItem.packageSellingPrice == null ||  $scope.vendorPurchaseItem.packageSellingPrice == undefined) {
                        $scope.vendorPurchaseItem.packageSellingPrice = 0;
                    }
                    if ($scope.vendorPurchaseItem.packageMRP == "" || $scope.vendorPurchaseItem.packageMRP == null || $scope.vendorPurchaseItem.packageMRP == undefined) {
                        $scope.vendorPurchaseItem.packageSellingPrice = 0;
                    }
                    if ($scope.vendorPurchaseItem.packageSize == "" || $scope.vendorPurchaseItem.packageSize == null || $scope.vendorPurchaseItem.packageSize == undefined) {
                        $scope.vendorPurchaseItem.packageSize = 0;
                    }
                    if ($scope.vendorPurchaseItem.packageSellingPrice != 0 && $scope.vendorPurchaseItem.packageSize != 0)
                    {
                        $scope.selectedSalesOrder.sellingPrice = ($scope.vendorPurchaseItem.packageSellingPrice / $scope.vendorPurchaseItem.packageSize);
                        // $scope.selectedSalesOrder.sellingPrice = $scope.vendorPurchaseItem.packageSellingPrice;
                        $scope.selectedSalesOrder.isEditMrp = false;
                       
                    }
                    else
                    {
                        $scope.selectedSalesOrder.sellingPrice = 0;
                        $scope.selectedSalesOrder.isEditMrp = true;
                    }
                   
                    document.getElementById("OrderQuantity").focus();
                }
               
                else if ($scope.BySalesTemplateSection == true)
                {
                    document.getElementById("templateQty").focus();
                }
               
            }, function (response) {
                $scope.responses = response;
            });
        }
    };

    

    $scope.onProductSelectEstimate = function () {
       
        if ($scope.selectedSalesEstimate != null) {
            $scope.zeroQty = false;
            productStockService.productBatch($scope.selectedSalesEstimate.product.id).then(function (response) {
                var tempBatchList1 = [];
                $scope.batchList = response.data;
                var x = 0;
                for (var i = 0; i < $scope.batchList.length; i++) {
                    $scope.batchList[i].productStockId = $scope.batchList[i].id;
                    $scope.batchList[i].id = null;
                    var availableStock = $scope.getAvailableStock($scope.batchList[i], null);
                    if (availableStock == 0)
                        continue;
                    $scope.batchList[i].availableQty = availableStock;
                }
                for (var i = 0; i < $scope.batchList.length; i++) {
                    if ($scope.batchList[i].tempStock != null && $scope.batchList[i].tempStock != undefined && $scope.batchList[i].tempStock.id != null) {
                        tempBatchList1[x] = $scope.batchList[i];
                        x++;
                    }
                }
                
                $scope.batchList = {};
                if (tempBatchList1.length > 0) {
                    $scope.enableTempStockPopup = true;
                    var m = ModalService.showModal({
                        "controller": "tempStockAlertCtrl",
                        "templateUrl": 'tempStockAlert',
                        "inputs": {
                            "tempBatchList": tempBatchList1,
                            "productName": $scope.selectedSalesEstimate.name
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            $scope.message = "You said " + result;
                        });
                    });
                    return false;
                } else if (tempBatchList1.length <= 0) {
                    productStockService.productBatch($scope.selectedSalesEstimate.product.id).then(function (responses) {
                        var batchListCheck = responses.data;
                        var totalQuantityCheck = 0;
                        var tempBatchCheck = [];
                        var editBatchCheck = null;
                        var availableStockCheck = null;
                        for (var i = 0; i < batchListCheck.length; i++) {
                            batchListCheck[i].productStockId = batchListCheck[i].id;
                            batchListCheck[i].id = null;
                            availableStockCheck = $scope.getAvailableStock(batchListCheck[i], editBatchCheck);
                            if (editBatchCheck != null && editBatchCheck.productStockId == batchListCheck[i].productStockId)
                                editBatchCheck.availableQty = availableStockCheck;
                            totalQuantityCheck += availableStockCheck;
                            if (availableStockCheck == 0)
                                continue;
                            batchListCheck[i].availableQty = availableStockCheck;
                            tempBatchCheck.push(batchListCheck[i]);
                        }
                        if (editBatchCheck != null && tempBatchCheck.length == 0) {
                            availableStockCheck = $scope.getAvailableStock(editBatchCheck, editBatchCheck);
                            editBatchCheck.availableQty = availableStockCheck;
                            totalQuantityCheck = availableStockCheck;
                            tempBatchCheck.push(editBatchCheck);
                        }
                        batchListCheck = tempBatchCheck;
                        if (batchListCheck.length > 0) {
                            $scope.zeroBatch = false;
                            salesService.getBatchListDetail().then(function (response1) {
                                $scope.batchListType = response1.data;
                                
                                loadBatch(null);
                                
                            }, function () {
                            });
                        } else {
                            $scope.zeroBatch = true;
                        }
                        
                    }, function () { });
                }
            }, function (error) {
                console.log(error);
            });
        } 
        
    };

    function loadBatch(editBatch) {
        
        productStockService.productBatch($scope.selectedSalesEstimate.product.id).then(function (response) {
                $scope.batchList = response.data;
                var qty = document.getElementById("estimateQuantity");
                qty.focus();
                var totalQuantity = 0;
                var rack = "";
                var tempBatch = [];
                var availableStock = null;
                for (var i = 0; i < $scope.batchList.length; i++) {
                    if ($scope.Editsale == false || editBatch == null) {
                        $scope.batchList[i].productStockId = $scope.batchList[i].id;
                        $scope.batchList[i].id = null;
                        availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                        if (editBatch != null && editBatch.productStockId == $scope.batchList[i].productStockId)
                            editBatch.availableQty = availableStock;
                        totalQuantity += availableStock;
                        if (availableStock == 0)
                            continue;
                        $scope.batchList[i].availableQty = availableStock;
                        if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                            rack = $scope.batchList[i].rackNo;
                        }
                        tempBatch.push($scope.batchList[i]);
                    } else {
                        if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                            rack = $scope.batchList[i].rackNo;
                        }
                    }
                    if ($scope.batchList[i].rackNo == null || $scope.batchList[i].rackNo == '' || $scope.batchList[i].rackNo == undefined) {
                        $scope.batchList[i].rackNo = '-';
                        rack = $scope.batchList[i].rackNo;
                    }
                    if ($scope.batchList[i].product.boxNo == null || $scope.batchList[i].product.boxNo == '' || $scope.batchList[i].product.boxNo == undefined) {
                        $scope.batchList[i].product.boxNo = '-';
                    }
                }
                if (editBatch != null && tempBatch.length == 0) {
                    availableStock = $scope.getAvailableStock(editBatch, editBatch);
                    editBatch.availableQty = availableStock;
                    totalQuantity = availableStock;
                    tempBatch.push(editBatch);
                }
                $scope.batchList = tempBatch;
                if ($scope.batchList.length > 0) {
                    if (editBatch == null) {
                        $scope.selectedBatch = $scope.batchList[0];
                       
                    } else {
                        $scope.selectedBatch = editBatch;

                        $scope.selectedBatch.discount = editBatch.discount;
                        $scope.selectedBatch.previoussellingPrice = editBatch.sellingPrice;
                    }
                    $scope.selectedBatch.totalQuantity = totalQuantity;
                   
                    dayDiff($scope.selectedBatch.expireDate);
                   
                } else {
                    $scope.zeroBatch = true;
                }
            }, function () { });
       
       
        var qty = document.getElementById("estimateQuantity");
        qty.focus();
        
    }

    $scope.batchSelected = function (batch) {
        $scope.valPurchasePrice = "";
        var totalQuantity = 0;
        $scope.selectedBatch = $scope.batchList[batch];

        for (var i = 0; i < $scope.batchList.length; i++) {
            totalQuantity += parseInt($scope.batchList[i].stock);
        }
       
       
        if ($scope.selectedBatch != undefined) {
            if ($scope.selectedBatch.totalQuantity == undefined) {
                $scope.selectedBatch.totalQuantity = "";
            } else {
                $scope.selectedBatch.totalQuantity = totalQuantity;
            }
            if ($scope.selectedBatch.availableQty != null && $scope.selectedBatch.batchNo != null) {
                $scope.selectedBatch.totalQuantity = totalQuantity;
            }

           
            else
                $scope.selectedBatch.discount = "";

            dayDiff($scope.selectedBatch.expireDate);
        }
    };

    function dayDiff(expireDate) {
        $scope.highlight = "";
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date(formatString(expireDate));
        var date1 = new Date(formatString(today));
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference < 30) {
            var dt = expireDate;
            $scope.highlight = "Expiry";
        } else {
            $scope.highlight = "";
        }
    }
    function formatString(format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    }

    $scope.getAvailableStock = function (productStock, editBatch) {
        var addedItem = getAddedItem(productStock.productStockId);
        var newAddedQty = getAddedItemQuantity(addedItem);
        if (editBatch == null)
            return productStock.stock - newAddedQty;
        else if (editBatch.id != null && productStock.productStockId == editBatch.productStockId)
            return productStock.stock + parseInt(editBatch.quantity) - newAddedQty;
        else if (editBatch != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock - (newAddedQty - parseInt(editBatch.quantity));
        }
        else if (editBatch != null && productStock.productStockId != editBatch.productStockId) {
            return productStock.stock - newAddedQty;
        }
        return productStock.stock;
    };
    function getAddedItemQuantity(addedItems) {
        var qty = 0;
        for (var i = 0; i < addedItems.length; i++) {
            if (addedItems[i].id != null) {
                qty += parseInt(addedItems[i].quantity) - parseInt(addedItems[i].orginalQty);
            } else {
                qty += parseInt(addedItems[i].quantity);
            }
        }
        return qty;
    }
    function getAddedItem(id) {
        var addedItems = [];
        for (var i = 0; i < $scope.salesOrderEstimate.salesEstimateItem.length; i++) {
            if ($scope.salesOrderEstimate.salesEstimateItem[i].productStockId == id)
                addedItems.push($scope.salesOrderEstimate.salesEstimateItem[i]);
        }
        return addedItems;
    }
    function getAddedItems(id, sellingPrice, discount) {
        for (var i = 0; i < $scope.salesOrderEstimate.salesEstimateItem.length; i++) {
            if ($scope.salesOrderEstimate.salesEstimateItem[i].productStockId == id && $scope.salesOrderEstimate.salesEstimateItem[i].sellingPrice == sellingPrice && $scope.salesOrderEstimate.salesEstimateItem[i].discount == discount) {
                return $scope.salesOrderEstimate.salesEstimateItem[i];
            }
        }
        return null;
    }

    $scope.changeQuantity = function (val) {
        if ($scope.selectedSalesOrder.id != null) {
            if (val > 0) {
                $scope.quantityExceed = 1;
            }
            else {
                $scope.quantityExceed = 0;
            }
        }
    };

    $scope.Quantityexceeds = 0;
    $scope.validateQty = function (valqty) {
        if (valqty == true) {
            $scope.valqty = true;
            $scope.addbtnenable = false;
            $scope.enableSaleBtn();
        }
        if ($scope.selectedBatch == null)
            return;
        
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.totalQuantity) { //&& $scope.selectedBatch.editId == null
            $scope.Quantityexceeds = 1;
        }
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty) { // && $scope.selectedBatch.editId != null
            $scope.Quantityexceeds = 1;
        }
        var qty = document.getElementById("estimateQuantity");
        if (qty.value == "") {
            $scope.salesEstimateItems.$valid = false;
            $scope.Quantityexceeds = 0;
        } else {
            if ((qty.value > $scope.selectedBatch.totalQuantity && $scope.selectedBatch.editId == null) || (qty.value > $scope.selectedBatch.availableQty && $scope.selectedBatch.editId != null)) {
                $scope.salesEstimateItems.$valid = true;
                if ($scope.Quantityexceeds == 1) {
                    $scope.salesEstimateItems.$valid = true;
                } else {
                    $scope.salesEstimateItems.$valid = false;
                }
            }
        }
    };

    $scope.enableSaleBtn = function () {
        $scope.Enablesalesbtn = true;
    };

    $scope.getPatient = function (val) {
        return patientService.GetActivePatientName(val, $scope.isDepartmentsave, 1).then(function (response) {
        //return patientService.GetPatientName(val, $scope.isDepartmentsave).then(function (response) {
            var origArr = response.data;
            var newArr = [], origLen = origArr.length, found, x, y;
            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name)) {
                        if (origArr[x].mobile != undefined || newArr[y].mobile != undefined) {
                            if (origArr[x].mobile === newArr[y].mobile) {
                                found = true;
                                break;
                            }
                        }
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }
            return newArr.map(function (item) {
                return item;
            });
        });
    };

    $scope.onPatientSelect = function (obj, event) {
       
        //$scope.patientId = obj.id;
        // $scope.CustomerMobile = obj.mobile;
        //document.getElementById("drugName1").focus();
        if ($scope.BySalesOrderSection == true) {
            $scope.salesOrderCustomerName = obj.name;
            $scope.salesOrderpatientId = obj.id;
            $scope.salesOrderEstimate.patient.name = obj.name;
            $scope.salesOrderEstimate.patient.mobile = obj.mobile;
            $scope.salesOrderEstimate.patient.email = obj.email;
            document.getElementById("drugName1").focus();
        }
        else if ($scope.BySalesEstimateSection == true) {
            $scope.salesEstimateCustomerName = obj.name;
            $scope.salesEstimatepatientId = obj.id;
            $scope.salesOrderEstimate.patient.name = obj.name;
            $scope.salesOrderEstimate.patient.mobile = obj.mobile;
            $scope.salesOrderEstimate.patient.email = obj.email;
            document.getElementById("estimateDrugName").focus();
        }
        
    };

    $scope.keyEnter = function (event, e) {
        if ($scope.BySalesOrderSection == true)
        {
            var result = document.getElementById("OrderQuantity").value;
            if (result == "" || result == 0) {
               // $scope.salesOrderCustomerName = "";
            } else {
                var ele = document.getElementById(e);
                if (event.which === 13) { // Enter key
                    ele.focus();
                    if (ele.nodeName != "BUTTON") {
                        ele.select();
                    }
                }
            }
        }
        else if ($scope.BySalesEstimateSection == true)
        {
            var result = document.getElementById("estimateQuantity").value;
            if (result == "" || result == 0) {
            } else {
                var ele = document.getElementById(e);
                if (event.which === 13) { // Enter key
                    ele.focus();
                    if (ele.nodeName != "BUTTON") {
                        ele.select();
                    }
                }
            }
        }
        else
        {
            var result = document.getElementById("templateQty").value;
            if (result == "" || result == 0) {
            } else {
                var ele = document.getElementById(e);
                if (event.which === 13) { // Enter key
                    ele.focus();
                    if (ele.nodeName != "BUTTON") {
                        ele.select();
                    }
                }
            }
        }
       
    };

    $scope.addSalesOrderProduct = function () {

        var existItem = 0;

        if ($scope.selectedSalesOrder.sellingPrice == "" || $scope.selectedSalesOrder.sellingPrice == null || $scope.selectedSalesOrder.sellingPrice == undefined) {
            $scope.selectedSalesOrder.sellingPrice = 0;
        }

        if ($scope.selectedSalesOrder.sellingPrice > 0) {
            $scope.selectedSalesOrder.isEditMrp = false;
        } else {
            $scope.selectedSalesOrder.isEditMrp = true;
        }

        if ($scope.selectedSalesOrder.quantity > 0) {
            for (var i = 0; i < $scope.salesOrderEstimate.salesOrderEstimateItem.length; i++) {
                if ($scope.salesOrderEstimate.salesOrderEstimateItem[i].id == $scope.selectedSalesOrder.id) {
                    existItem = 1;
                }
            }
            for (var k = 0; k < $scope.salesOrderEstimate.salesOrderEstimateItem.length; k++) {
                if (($scope.salesOrderEstimate.salesOrderEstimateItem[k].productId == $scope.selectedSalesOrder.id)) {
                    toastr.info("Product already added");
                    document.getElementById("drugName1").focus();
                    return;
                }
            }
            if (existItem == 0) {
                if ($scope.selectedSalesOrder.accountId === null || $scope.selectedSalesOrder.accountId === undefined || $scope.selectedSalesOrder.accountId === "") {
                    $scope.selectedSalesOrder.productMasterID = $scope.selectedSalesOrder.id;
                    $scope.selectedSalesOrder.productId = $scope.selectedSalesOrder.id;
                    $scope.selectedSalesOrder.productMasterName = $scope.selectedSalesOrder.name;
                    $scope.selectedSalesOrder.manufacturer = $scope.selectedSalesOrder.manufacturer;
                    $scope.selectedSalesOrder.category = $scope.selectedSalesOrder.category;
                    $scope.selectedSalesOrder.schedule = $scope.selectedSalesOrder.schedule;
                    $scope.selectedSalesOrder.genericName = $scope.selectedSalesOrder.genericName;
                    $scope.selectedSalesOrder.packing = $scope.selectedSalesOrder.packing;

                }
                else {
                    $scope.selectedSalesOrder.productMasterID = null;
                    $scope.selectedSalesOrder.productId = $scope.selectedSalesOrder.id;
                }
                $scope.selectedSalesOrder.productName = $scope.selectedSalesOrder.name;
                if ($scope.selectedSalesOrder.discount == null || $scope.selectedSalesOrder.discount == "" || $scope.selectedSalesOrder.discount == undefined)
                {
                    $scope.selectedSalesOrder.discount = 0;
                }
                $scope.salesOrderEstimate.salesOrderEstimateItem.push($scope.selectedSalesOrder);
                $scope.selectedSalesOrder = null;
                $scope.quantityExceed = 0;
                document.getElementById("drugName1").focus();
            }
        }

        $scope.maxEditDiscountExceeded = false;

        //if (saleOrderItem.IsEdit==true)
        //{
        //    $scope.salesOrderList.$valid = false;
        //}

    };
    $scope.editId = 1;
    $scope.addSalesEstimateProduct = function () {

        //if ($scope.Quantityexceeds == 1) return;
       
        if ($scope.selprice == true || $scope.valdisct == true || $scope.valqty == true) {
            selprice = false;
            valdisct = false;
            valqty = false;
            $scope.valqty = false;
            $scope.valdisct = false;
            $scope.selprice = false;
            $scope.Enablesalesbtn = false;
           
            //if ($scope.Quantityexceeds == 1) return;
            if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == null || $scope.selectedBatch.purchasePrice > $scope.selectedBatch.sellingPrice) {
                return;
            }
            if (parseFloat($scope.selectedBatch.discount) > 100 || $scope.selectedBatch.discount == '.') {
                return;
            }
            else if ($scope.selectedBatch.discount == undefined || $scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "") {
                $scope.selectedBatch.discount = 0;
            }
            if ($scope.selectedBatch.quantity <= 0 || $scope.selectedBatch.quantity == undefined) {
                return;
            }
            if ($scope.batchList.length == 0) {
                return;
            }
            var RequiredQuantity = angular.element("#spnquantity").attr("data-quantityId");
            var ChangedMrp = angular.element("#spnquantity").attr("data-price");
            var ChangedDicount = angular.element("#spnquantity").attr("data-discount") || 0;
           
            if (($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty)) {
                var addedItem = "";
                for (var i = 0; i < $scope.batchList.length; i++) {
                    if ($scope.batchList[i].editId == null) {
                        $scope.batchList[i].editId = $scope.editId++;
                        addedItem = getAddedItems($scope.batchList[i].productStockId, $scope.batchList[i].sellingPrice, ChangedDicount);
                        if (addedItem != null) {
                            if (RequiredQuantity > $scope.batchList[i].quantity) {
                                addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.batchList[i].quantity);
                            } else {
                                addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.batchList[i].availableQty);
                            }
                        } else {
                            if (RequiredQuantity != 0) {
                                if (RequiredQuantity > $scope.batchList[i].availableQty) {
                                    $scope.batchList[i].quantity = $scope.batchList[i].availableQty;
                                    RequiredQuantity = RequiredQuantity - $scope.batchList[i].availableQty;
                                    $scope.batchList[i].availableQty = 0;
                                } else {
                                    $scope.batchList[i].availableQty = $scope.batchList[i].availableQty - RequiredQuantity;
                                    $scope.batchList[i].quantity = RequiredQuantity;
                                    RequiredQuantity = 0;
                                }

                                // $scope.batchList[i].saleProductName = $scope.batchList[i].product.name;
                                $scope.batchList[i].productName = $scope.batchList[i].product.name;

                                if ($scope.batchList[i].discount == null || $scope.batchList[i].discount == "" || $scope.batchList[i].discount == undefined) {
                                    $scope.batchList[i].discount = 0;
                                }

                                $scope.salesOrderEstimate.salesEstimateItem.push($scope.batchList[i]);
                            }
                        }
                    } 
                }
                //$scope.salesItems.$setPristine();
                $scope.selectedSalesEstimate = null;
                $scope.batchList = {};
                $scope.highlight = "";
                ChangedDicount = 0;
                ChangedMrp = 0;
            } else {
                if ($scope.selectedBatch.editId == null) {
                    $scope.selectedBatch.editId = $scope.editId++;
                    addedItem = getAddedItems($scope.selectedBatch.productStockId, $scope.selectedBatch.sellingPrice, $scope.selectedBatch.discount);
                    if (addedItem != null && (addedItem.transactionType == $scope.selectedBatch.transactionType)) {
                        addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.selectedBatch.quantity);
                    } else {
                        // $scope.selectedBatch.saleProductName = $scope.selectedBatch.product.name;
                        $scope.selectedBatch.productName = $scope.selectedBatch.product.name;
                        if ($scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "" || $scope.selectedBatch.discount == undefined) {
                            $scope.selectedBatch.discount = 0;
                        }
                        $scope.salesOrderEstimate.salesEstimateItem.push($scope.selectedBatch);
                    }
                } 
               // $scope.salesItems.$setPristine();
                $scope.selectedSalesEstimate = null;
                $scope.batchList = {};
                $scope.highlight = "";
            }
        }
        $scope.maxEditDiscountExceeded = false;
        $scope.maxEditqtyExceeded = false;
        document.getElementById("estimateDrugName").focus();

    };

   

    $scope.addSalesTemplateProduct = function () {

        for (var k = 0; k < $scope.salesTemplate.salesTemplateItem.length; k++) {
            if ($scope.salesTemplate.salesTemplateItem[k].isDeleted != true)
            {
                if (($scope.salesTemplate.salesTemplateItem[k].productId == $scope.selectedSalesTemplate.id)) {
                    toastr.info("Product already added");
                    document.getElementById("templateDrugName").focus();
                    return;
                }
            }
        }
        if ($scope.selectedSalesTemplate.accountId === null || $scope.selectedSalesTemplate.accountId === undefined || $scope.selectedSalesTemplate.accountId === "") {
            $scope.selectedSalesTemplate.productMasterID = $scope.selectedSalesTemplate.id;
            $scope.selectedSalesTemplate.productId = $scope.selectedSalesTemplate.id;
            $scope.selectedSalesTemplate.productMasterName = $scope.selectedSalesTemplate.name;
            $scope.selectedSalesTemplate.manufacturer = $scope.selectedSalesTemplate.manufacturer;
            $scope.selectedSalesTemplate.category = $scope.selectedSalesTemplate.category;
            $scope.selectedSalesTemplate.schedule = $scope.selectedSalesTemplate.schedule;
            $scope.selectedSalesTemplate.genericName = $scope.selectedSalesTemplate.genericName;
            $scope.selectedSalesTemplate.packing = $scope.selectedSalesTemplate.packing;

        }
        else {
            $scope.selectedSalesTemplate.productMasterID = null;
            $scope.selectedSalesTemplate.productId = $scope.selectedSalesTemplate.id;
        }
        $scope.selectedSalesTemplate.id = null;
        $scope.selectedSalesTemplate.productName = $scope.selectedSalesTemplate.name;
        $scope.salesTemplate.salesTemplateItem.push($scope.selectedSalesTemplate);
        // $scope.disableDeleted = false;
        $scope.addTemplateEnable = false;
        $scope.selectedSalesTemplate = null;
        $scope.quantityExceed = 0;
        $scope.disableDeleted = false;
        $scope.salesTemplateCount = 1;
        window.setTimeout(function () {
            for (var i = 0; i < $scope.salesTemplate.salesTemplateItem.length; i++) {
                $('#chip-wrapper' + i).show();
                $('#chip-btn' + i).text('-');
            }
        }, 0);
        document.getElementById("templateDrugName").focus();

    };

    $scope.tempQuantity = null;
    $scope.isAdd = false;
    $scope.templateQuantity = null;
    $scope.isTemplateAdd = false;

    $scope.editProduct = function (saleOrderItem) {
        saleOrderItem.IsEdit = true;
        $scope.isAdd = true;
        $scope.tempQuantity = saleOrderItem.quantity;
        $scope.salesOrderList.$valid = false;
        //document.getElementById("drugName1").focus();
    };

    $scope.saveProduct = function (saleOrderItem) {
        saleOrderItem.IsEdit = false;
        $scope.isAdd = false;
        var index1 = $scope.salesOrderEstimate.salesOrderEstimateItem.indexOf(saleOrderItem);
        if (parseFloat(saleOrderItem.quantity) > 0) {
            $scope.salesOrderEstimate.salesOrderEstimateItem[index1].quantity = saleOrderItem.quantity;
        }
        else {
            $scope.salesOrderEstimate.salesOrderEstimateItem[index1].quantity = $scope.tempQuantity;
        }
        if (parseFloat(saleOrderItem.sellingPrice) > 0) {
            $scope.salesOrderEstimate.salesOrderEstimateItem[index1].sellingPrice = saleOrderItem.sellingPrice;
            $scope.salesOrderEstimate.salesOrderEstimateItem[index1].isEditMrp = false;
        }
        else {
            $scope.salesOrderEstimate.salesOrderEstimateItem[index1].sellingPrice = 0;
            $scope.salesOrderEstimate.salesOrderEstimateItem[index1].isEditMrp = true;
        }
        if (parseFloat(saleOrderItem.discount) > 0) {
            $scope.salesOrderEstimate.salesOrderEstimateItem[index1].discount = saleOrderItem.discount;
        }
        else
        {
            $scope.salesOrderEstimate.salesOrderEstimateItem[index1].discount = 0;
        }
        $scope.salesOrderList.$valid = true;

    };

    $scope.removeProduct = function (item) {

        if (!confirm("Are you sure, Do you want to delete ? "))
            return;

        var index = $scope.salesOrderEstimate.salesOrderEstimateItem.indexOf(item);
        $scope.salesOrderEstimate.salesOrderEstimateItem.splice(index, 1);
       // $scope.salesOrderEstimate.salesOrderEstimateItem[index].isDeleted = true;
        document.getElementById("drugName1").focus();
    };

    // SalesEstimateItem Edit,delete and Template Selection Option Start

    $scope.editEstimateProduct = function (saleEstimate) {
        saleEstimate.IsEdit = true;
        $scope.isTemplateAdd = true;
        $scope.estimateQuantity = saleEstimate.quantity;
    };

    $scope.maxEditqtyExceeded = false;
    $scope.validateEditQty = function (saleEstimate) {
        var qty = document.getElementById("productEstimateQuantity");
        
            $scope.maxEditqtyExceeded = false;
            if (saleEstimate.quantity > saleEstimate.stock) {
                $scope.maxEditqtyExceeded = true;
                $scope.salesEstimateList.$valid = false;
                return false;
            }
    };

    $scope.saveEstimateProduct = function (saleEstimate) {
        saleEstimate.IsEdit = false;
        $scope.isTemplateAdd = false;
        var index1 = $scope.salesOrderEstimate.salesEstimateItem.indexOf(saleEstimate);
        if (parseFloat(saleEstimate.quantity) > 0) {
            $scope.salesOrderEstimate.salesEstimateItem[index1].quantity = saleEstimate.quantity;
        }
        else {
            $scope.salesOrderEstimate.salesEstimateItem[index1].quantity = $scope.estimateQuantity;
        }
        if (parseFloat(saleEstimate.discount) > 0) {
            $scope.salesOrderEstimate.salesEstimateItem[index1].discount = saleEstimate.discount;
        }
        else {
            $scope.salesOrderEstimate.salesEstimateItem[index1].discount = 0;
        }

    };

    $scope.removeEstimateProduct = function (item) {

        if (!confirm("Are you sure, Do you want to delete ? "))
            return;

        var index = $scope.salesOrderEstimate.salesEstimateItem.indexOf(item);
        $scope.salesOrderEstimate.salesEstimateItem.splice(index, 1);

        //$scope.salesOrderEstimate.salesEstimateItem[index].isDeleted = true;
        document.getElementById("estimateDrugName").focus();
        $scope.maxEditqtyExceeded = false;
    };

    // SalesEstimateItem Edit,delete and Template Selection Option End

    // SalesTemplateItem Edit,delete and Template Selection Option Start

    $scope.editTemplateProduct = function (saleTemp) {
        saleTemp.IsEdit = true;
        $scope.isTemplateAdd = true;
        $scope.addTemplateEnable = true;
        $scope.templateQuantity = saleTemp.quantity;
    };

    $scope.saveTemplateProduct = function (saleTemp) {
        saleTemp.IsEdit = false;
        $scope.isTemplateAdd = false;
        var index1 = $scope.salesTemplate.salesTemplateItem.indexOf(saleTemp);
        if (parseFloat(saleTemp.quantity) > 0) {
            $scope.salesTemplate.salesTemplateItem[index1].quantity = saleTemp.quantity;
        }
        else {
            $scope.salesTemplate.salesTemplateItem[index1].quantity = $scope.templateQuantity;
        }
        $scope.addTemplateEnable = false;
    };

    $scope.removeTemplateProduct = function (item) {

        if (!confirm("Are you sure, Do you want to delete ? "))
            return;

        var index = $scope.salesTemplate.salesTemplateItem.indexOf(item);
       // $scope.salesTemplate.salesTemplateItem.splice(index, 1);
       
        $scope.salesTemplate.salesTemplateItem[index].isDeleted = true;
        $scope.disableDeleted = false;
        for (var i = 0; i < $scope.salesTemplate.salesTemplateItem.length; i++) {
            if ($scope.salesTemplate.salesTemplateItem[i].isDeleted==true)
            {
                $scope.disableDeleted = false;
            }
            else
            {
                $scope.disableDeleted = false;
            }
        }
        $scope.addTemplateEnable = false;

        var totalcount = 0;
        $scope.salesTemplateCount = 0;
        for (var j = 0; j < $scope.salesTemplate.salesTemplateItem.length; j++) {
            if ($scope.salesTemplate.salesTemplateItem[j].isDeleted==true) {
                totalcount++;
            } else {
                $scope.salesTemplateCount++;
            }
        }

        document.getElementById("templateDrugName").focus();
    };

    $scope.selectedSalesTemplateName = function (selectedTemplate) {

        $scope.addTemplateEnable = true;
        $scope.salesTemplate.salesTemplateItem = [];
        $scope.SelectedTemplateNameList = [];

        if (selectedTemplate == undefined)
            return;
        salesService.getSelectedTemplateNameList(selectedTemplate.id).then(function (response) {
            $scope.SelectedTemplateNameList = response.data;
            for (var i = 0; i < $scope.SelectedTemplateNameList.length; i++)
            {
                $scope.salesTemplate.salesTemplateItem.push($scope.SelectedTemplateNameList[i]);
            }

            for (var i = 0; i < $scope.salesTemplate.salesTemplateItem.length; i++) {
                if ($scope.salesTemplate.salesTemplateItem[i].name == null || $scope.salesTemplate.salesTemplateItem[i].name == undefined)
                {
                    $scope.salesTemplate.salesTemplateItem[i].name = $scope.salesTemplate.salesTemplateItem[i].product.name;
                }
            }
            $scope.selectedSalesTemplate = "";
            $scope.selectedSalesTemplate.quantity = "";
            var totalcount = 0;
            $scope.salesTemplateCount = 0;
            for (var j = 0; j < $scope.salesTemplate.salesTemplateItem.length; j++) {
                if ($scope.salesTemplate.salesTemplateItem[j].isDeleted == true) {
                    totalcount++;
                } else {
                    $scope.salesTemplateCount++;
                }
            }
            document.getElementById("templateDrugName").focus();
        }, function () {
        });

    };

    // SalesTemplateItem Edit,delete and Template Selection Option End

    $scope.saveSalesOrder = function () {

        //$scope.close('Yes');
        $.LoadingOverlay("show");

        // $scope.salesOrderEstimate.salesOrderEstimateItem.patientId = $scope.patientId;
        if ($scope.BySalesOrderSection == true) {
            $scope.salesOrderEstimate.patientId = $scope.salesOrderpatientId;
            if ($scope.salesOrderEstimate.patientId == null || $scope.salesOrderEstimate.patientId == undefined || $scope.salesOrderEstimate.patientId == "" || $scope.salesOrderCustomerName == null || $scope.salesOrderCustomerName=="") {
                $scope.focusCustomerName = true;
                $scope.focusestimateCustomerName = false;
                $scope.focustemplateProductName = false;
                toastr.info('Please enter the customer name');
                document.getElementById("customerName").focus();
                $.LoadingOverlay("hide");
                return;
            }
            $scope.salesOrderEstimate.salesOrderEstimateType = 1;
        }
        else if ($scope.BySalesEstimateSection == true ) {
            $scope.salesOrderEstimate.patientId = $scope.salesEstimatepatientId;
            if ($scope.salesOrderEstimate.patientId == null || $scope.salesOrderEstimate.patientId == undefined || $scope.salesOrderEstimate.patientId == "" || $scope.salesEstimateCustomerName == null || $scope.salesEstimateCustomerName=="")
            {
                $scope.focusCustomerName = false;
                $scope.focusestimateCustomerName = true;
                $scope.focustemplateProductName = false;
                toastr.info('Please enter the customer name');
                document.getElementById("estimateCustomerName").focus();
                $.LoadingOverlay("hide");
                return;
            }
           
            $scope.salesOrderEstimate.salesOrderEstimateType = 2;
        }
       
        salesService.createSalesOrder($scope.salesOrderEstimate).then(function (respone) {
            var salesOrderItems = respone.data;
            if ($scope.salesOrderEstimate.salesOrderEstimateType == 1)
            {
                resetOrder();
                toastr.success('Sales order saved successfully');
                document.getElementById("customerName").focus();
            }
            else if ($scope.salesOrderEstimate.salesOrderEstimateType == 2)
            {
                resetEstimate();
                toastr.success('Sales estimate saved successfully');
                document.getElementById("estimateCustomerName").focus();
            }
           
            if ($scope.BySalesOrderSection == true) {
                
            }
            else if ($scope.BySalesEstimateSection == true) {
               
            }

            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };




    function saveSalesNewTemplate(salesTemplateName) {

        //$scope.close('Yes');
        //$.LoadingOverlay("show");

        $scope.salesTemplate.templateName = salesTemplateName;
        

        salesService.createSalesNewTemplate($scope.salesTemplate).then(function (response) {
            var salesOrderItems = response.data;
            $scope.templateNameList = [];
            $scope.salesTemplate.salesTemplateItem = [];
            $scope.SelectedTemplateNameList = [];
            salesTemplateNameList();
            toastr.success('New template saved successfully');
            document.getElementById("templateDrugName").focus();
            $scope.isProcessing = false;
            //$.LoadingOverlay("hide");
        }, function (response) {
            $scope.responses = response;
            var msg = response.data.errorDesc;
            var templatename = "Template Name Already Exist";
            if (msg.indexOf(templatename) != -1)
                toastr.info(response.data.errorDesc, 'Info..');
            else
                toastr.error(response.data.errorDesc, 'Error');
            $scope.isProcessing = false;
            document.getElementById("templateDrugName").focus();
        });
    }

    $scope.updateTemplate = function () {

        //$scope.close('Yes');
        //$.LoadingOverlay("show");

        // $scope.salesOrderEstimate.salesOrderEstimateItem.patientId = $scope.patientId;
        $scope.salesTemplate.id = $scope.selectedTemplateName.id;
        salesService.updateTemplate($scope.salesTemplate).then(function (respone) {
            var salesOrderItems = respone.data;
            $scope.templateNameList = [];
            $scope.salesTemplate.salesTemplateItem = [];
            $scope.SelectedTemplateNameList = [];
            salesTemplateNameList();
            toastr.success('Updated template successfully');
            document.getElementById("templateDrugName").focus();
            $scope.isProcessing = false;
            //$.LoadingOverlay("hide");
        }, function () {
            //$.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };

    $scope.saveNewTemopen = function () {
        $scope.showSalesTemplateScreen($scope.salesTemplate);
    }

    $scope.showSalesTemplateScreen = function (salesTemplate) {
        $scope.AnyPopupOpened = true;
        var m = ModalService.showModal({
            "controller": "salesTemplateCtrl",
            "templateUrl": "saveSalesTemplate",
            "inputs": {
                "salesTemplate": salesTemplate
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.AnyPopupOpened = false;
                if (result.status == "Yes") {
                    $scope.tempStockLoaded = true;
                    saveSalesNewTemplate(result.data);
                }
            });
        });

    };

    $rootScope.$on("returnPopupEvent", function () {
        $scope.AnyPopupOpened = false;
    });

    $rootScope.$on("tempStockAlertClose", function () {
        var keyPressed = cacheService.get('pressedKey');
        if (parseInt(keyPressed) == 27) {
            $scope.enableTempStockPopup = true;
        } else {
            $scope.enableTempStockPopup = false;
        }
        salesService.getBatchListDetail().then(function (response) {
            $scope.batchListType = response.data;
            //if ($scope.batchListType == "Batch") {
            //    $scope.ProductDetails();
            //} else {
                loadBatch(null);
            //}
        }, function () {
        });
    });

    $scope.maxDiscountFixed = "No";
    function getIsMaxDiscountAvail() {
        salesService.getIsMaxDiscountAvail().then(function (response) {
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.maxDiscountFixed = "No";
            } else {
                if (response.data.maxDiscountAvail != undefined) {
                    $scope.maxDiscountFixed = response.data.maxDiscountAvail;
                } else {
                    $scope.maxDiscountFixed = "No";
                }
            }
            if ($scope.maxDiscountFixed == "Yes") {
                getMaxDiscountValue();
            }
        }, function () {
        });
    }

    $scope.maxDisountValue = 0;
    function getMaxDiscountValue() {
        salesService.getMaxDiscountValue().then(function (response) {
            if (response.data != "" && response.data != null) {
                $scope.maxDisountValue = response.data.instanceMaxDiscount;
            }
        }, function () {
        });
    }

    $scope.maxDiscountExceeded = false;
    $scope.changeDiscount = function (discount) {

        if ($scope.BySalesOrderSection == true) {
            if (discount == "" || discount == ".") {
                discount = 0;
            }
            $scope.maxDiscountExceeded = false;
            if ($scope.maxDiscountFixed == 'Yes') {
                if ($scope.selectedSalesOrder.discount > $scope.maxDisountValue) {
                    $scope.maxDiscountExceeded = true;
                    return false;
                }
            }
            document.getElementById("estimateDiscount").focus();
        }
        else if ($scope.BySalesEstimateSection == true) {
            if (discount == "" || discount == ".") {
                discount = 0;
            }
            $scope.maxDiscountExceeded = false;
            if ($scope.maxDiscountFixed == 'Yes') {
                if ($scope.selectedBatch.discount > $scope.maxDisountValue) {
                    $scope.maxDiscountExceeded = true;
                    return false;
                }
            }
            document.getElementById("estimateDiscount").focus();
        }
       
    };
    $scope.maxEditDiscountExceeded = false;
    $scope.changeEstimateEditDiscount = function (discount, saleEstimate) {

        if ($scope.BySalesOrderSection == true) {
            if (discount == "" || discount == ".") {
                discount = 0;
            }
            $scope.maxEditDiscountExceeded = false;
            if ($scope.maxDiscountFixed == 'Yes') {
                if (saleEstimate.discount > $scope.maxDisountValue) {
                    $scope.maxEditDiscountExceeded = true;
                    return false;
                }
            }
            else {
                if (saleEstimate.discount > 100) {
                    $scope.salesOrderList.$valid = false;
                }
                $scope.maxEditDiscountExceeded = false;
            }
            //document.getElementById("productDiscount").focus();
        }
        else if ($scope.BySalesEstimateSection == true) {
            if (discount == "" || discount == ".") {
                discount = 0;
            }
            $scope.maxEditDiscountExceeded = false;
            if ($scope.maxDiscountFixed == 'Yes') {
               
                if (saleEstimate.discount > $scope.maxDisountValue) {
                    $scope.maxEditDiscountExceeded = true;
                    return false;
                }
            }
            else
            {
                if (saleEstimate.discount > 100) {
                    $scope.salesEstimateList.$valid = false;
                }
                $scope.maxEditDiscountExceeded = false;
            }
            //document.getElementById("productEstimateDisc").focus();
        }

    };

    //CheckSalesTemplates();
    function CheckSalesTemplates() {
        if (window.localStorage.getItem("salesOrderEstimateEdit") == null) {
        } else {
            $scope.salesOrderEstimate = JSON.parse(window.localStorage.getItem("salesOrderEstimateEdit"));
            //window.localStorage.removeItem('salesOrderEstimateEdit');

            if ($scope.salesOrderEstimate.salesOrderEstimateType == 1)
            {
                $scope.salesOrderclass = "tabSelected";
                $scope.salesEstimateclass = "tabNormal";
                $scope.salesTemplateClass = "tabNormal";
                $scope.BySalesOrderSection = true;
                $scope.BySalesEstimateSection = false;
                $scope.BySalesTemplateSection = false;
                $scope.focusCustomerName = true;
                $scope.focusestimateCustomerName = false;
                $scope.focustemplateProductName = false;
            }
            else if ($scope.salesOrderEstimate.salesOrderEstimateType == 2)
            {
                $scope.salesEstimateclass = "tabSelected";
                $scope.salesOrderclass = "tabNormal";
                $scope.salesTemplateClass = "tabNormal";
                $scope.BySalesEstimateSection = true;
                $scope.BySalesOrderSection = false;
                $scope.BySalesTemplateSection = false;
                $scope.focusestimateCustomerName = true;
                $scope.focusCustomerName = false;
                $scope.focustemplateProductName = false;
                $scope.Enablesalesbtn = false;
            }
           

            salesService.getSalesOrderById($scope.salesOrderEstimate.id)
        .then(function (resp) {
            $scope.salesOrderEstimate.salesOrderEstimateItem = resp.data;
            var salesOrderItem = [];
            for (var i = 0; i < resp.data.salesOrderEstimateItem.length; i++) {
                if (resp.data.salesOrderEstimateItem[i].isDeleted != true) {
                    salesOrderItem.push(resp.data.salesOrderEstimateItem[i]);
                }
            }
            $scope.EditSalesOrderEstimate = true;
            $scope.salesOrderEstimate.salesOrderEstimateItem = salesOrderItem;
           
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
        }
    }

    $scope.checkAllSalesOrderfields = function (ind) {
        if ($scope.BySalesOrderSection == true)
        {
            if (ind > 1) {
                if ($scope.selectedSalesOrder == undefined || $scope.selectedSalesOrder == null || $scope.selectedSalesOrder == "") {
                    var ele = document.getElementById("drugName1");
                    ele.focus();
                    return false;
                }
            }
            if (ind > 2) {
                if ($scope.selectedSalesOrder.quantity == undefined || $scope.selectedSalesOrder.quantity == null || $scope.selectedSalesOrder.quantity == "") {
                    var ele = document.getElementById("OrderQuantity");
                    ele.focus();
                    return false;
                }
            }
            if (ind > 3) {
                if ($scope.selectedSalesOrder.discount == undefined || $scope.selectedSalesOrder.discount == null || $scope.selectedSalesOrder.discount == "") {
                    var ele = document.getElementById("orderdiscount");
                    ele.focus();
                    return false;
                }
            }
        }
        else if ($scope.BySalesTemplateSection == true)
        {
            if (ind > 1) {
                if ($scope.selectedSalesTemplate == undefined || $scope.selectedSalesTemplate == null || $scope.selectedSalesTemplate == "") {
                    var ele = document.getElementById("templateDrugName");
                    ele.focus();
                    return false;
                }
            }
            if (ind > 2) {
                if ($scope.selectedSalesTemplate.quantity == undefined || $scope.selectedSalesTemplate.quantity == null || $scope.selectedSalesTemplate.quantity == "") {
                    var ele = document.getElementById("templateQty");
                    ele.focus();
                    return false;
                }
            }
        }
        
    };


    function getSalesSettings() {
        salesService.getAllSalesSettings().then(function (response) {
            if (response.data == "" || response.data == null || response.data == undefined) {
            } else {
                if (response.data.patientTypeDept != undefined) {
                    $scope.isDepartmentsave = "0";
                    if (response.data.patientTypeDept == true) {
                        $scope.isDepartmentsave = "1";
                    }
                } else {
                    $scope.isDepartmentsave = "0";
                }
            }

        }, function (error) {
            console.log(error);
        });
    }

    getSalesSettings();
    
});
app.directive('focusBox', function () {
    return {
        "link": function (scope, element, attrs) {
            element.bind("keydown", function (event) {
                var valueLength = attrs.$$element[0].value.length;
                var value = parseFloat(attrs.$$element[0].value);
                //Enter 
                if (event.which === 13) {

                    if (scope.BySalesOrderSection == true)
                    {
                        if (attrs.id === "OrderQuantity" || attrs.id === "orderdiscount") {
                            ele = document.getElementById(attrs.nextid);
                            ele.focus();
                        }
                        else
                        {
                            ele = document.getElementById("drugName1");
                            ele.focus();
                        }
                    }
                    else if (scope.BySalesTemplateSection == true)
                    {
                        if (attrs.id === "templateQty") {
                            ele = document.getElementById(attrs.nextid);
                            ele.focus();
                        }
                        else {
                            ele = document.getElementById("templateDrugName");
                            ele.focus();
                        }
                    }
                }
            });
        }
    };
});
