app.controller('otherSalesCreateCtrl', function ($scope, $rootScope, $location, $window, toastr, salesModel, salesService, cacheService, ModalService, $filter, pagerServcie, customerReceiptService, productStockModel, productModel, productStockService, salesService, printingHelper) {

    $scope.minDate = new Date();
    var d = new Date();
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.open3 = function () {
        $scope.popup3.opened = true;
    };
    $scope.popup3 = {
        opened: false
    };
    $scope.open4 = function () {
        $scope.popup4.opened = true;
    };
    $scope.popup4 = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    function resetSales() {
        $scope.batchList = [];
        $scope.selectedProduct = null;
        $scope.selectedBatch = { reminderFrequency: "0" };
        $scope.selectedBatch.transactionType = "sales";
        $scope.selectedBatch.salesReturn = false;
        $scope.selectedBatch.canEnableTaxEdit = false;
        $scope.salesAmount = 0;
        $scope.Math = window.Math;
        $scope.salesCount = 0;
        $scope.zeroBatch = false;
        $scope.zeroQty = false;
        $scope.dispInvoiceNo = "";
        $scope.sales = getSalesObject();
        $scope.eancodeAlert = "";
        $scope.Quantityexceeds = 0;
        $scope.Enablesalesbtn = false;
        var valqty = false;
        var valdisct = false;
        var selprice = false;
        $scope.valqty = false;
        $scope.valdisct = false;
        $scope.selprice = false;
        $scope.edititem = false;
        $scope.salesPriceSettings = 0;
        $scope.addbtnenable = false;
        $scope.FinalNetAmount = 0;
        $scope.selectedBillDate = new Date();
    }

    resetSales();

    $scope.getInvoiceNo = function () {
        salesService.getNumericInvoiceNo().then(function (response) {
            // $scope.trackInvoiceNo();
            if (response.data != "" && response.data != null && response.data != undefined) {
                //   $scope.sales.invoiceNo = response.data;
                var invoiceno = response.data;
                $scope.dispInvoiceNo = invoiceno;
                $scope.sales.invoiceNo = $scope.dispInvoiceNo;
                $scope.focusOnProduct();
            }
        }, function () {
        });
    }

    $scope.getIsGstEnabled = function () {
        var gstEnabled = document.getElementById("isGstEnabled").value;
        $scope.isGstEnabled = gstEnabled == "True" ? true : false;
    };

    $scope.focusOnProduct = function () {
        var qty = document.getElementById("drugName");
        qty.focus();
    };

    function getSalesObject() {
        return {
            "salesItem": [],
            "discount": "",
            "total": 0,
            "TotalQuantity": 0,
            "rackNo": "",
            "vat": 0,
            "cashType": "Full",
            "paymentType": "Cash",
            "deliveryType": "Counter",
            "billPrint": false,
            "sendEmail": false,
            "sendSms": false,
            "doctorMobile": null,
            "credit": null,
            "cardNo": null,
            "cardDate": null,
            "cardName": null,
            "cardDigits": null,
            "isCreditEdit": false,
            "changeDiscount": "",
            "salesPayments": []
        };
    }

    $scope.CheckName = function () {
        var drugName = document.getElementById("drugName");
        if (drugName.value == "") {
            $scope.selectedBatch = { reminderFrequency: "0", purchasePrice: 0 };
            $scope.selectedBatch.transactionType = "sales";
            $scope.selectedBatch.canEnableTaxEdit = false;
        }
    };

    $scope.getProducts = function (val) {

        $scope.eancodeAlert = "";

        return productStockService.getSalesProduct(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });

    };

    $scope.getProduct = function () {

        var eleId = document.getElementById("drugName");
        var scannedEancode = eleId.value
        if (($scope.selectedProduct === undefined || typeof ($scope.selectedProduct) !== "object" || $scope.selectedProduct === null) && eleId.value !== "") {
            productStockService.getEancodeExistData(scannedEancode).then(function (eanRes) {
                if (eanRes.data.length !== 0) {
                    productStockService.getSalesProduct(eanRes.data[0].id).then(function (response) {
                        if (response.data.length === 1) {
                            $scope.scanBarcodeVal = eleId.value;
                            $scope.selectedProduct = response.data[0];
                            $scope.onProductSelect();
                        } else if (response.data.length === 0) {
                            $scope.eancodeAlert = "Stock not available";
                        }
                    });
                } else {
                    $scope.eancodeAlert = "Product not found";
                }
            });
        }
    };

    $scope.getAvailableStock = function (productStock, editBatch) {
        var addedItem = getAddedItem(productStock.productStockId);
        var newAddedQty = getAddedItemQuantity(addedItem);
        if (editBatch == null)
            return productStock.stock - newAddedQty;
        else if (editBatch.id != null && productStock.productStockId == editBatch.productStockId)
            return productStock.stock + parseInt(editBatch.quantity) - newAddedQty;
        else if (editBatch != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock - (newAddedQty - parseInt(editBatch.quantity));
        }
        else if (editBatch != null && productStock.productStockId != editBatch.productStockId) {
            return productStock.stock - newAddedQty;
        }
        return productStock.stock;
    };

    function getAddedItemQuantity(addedItems) {
        var qty = 0;
        for (var i = 0; i < addedItems.length; i++) {
            if (addedItems[i].id != null) {
                qty += parseInt(addedItems[i].quantity) - parseInt(addedItems[i].orginalQty);
            } else {
                qty += parseInt(addedItems[i].quantity);
            }
        }
        return qty;
    }

    function getAddedItem(id) {
        var addedItems = [];
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ($scope.sales.salesItem[i].productStockId == id)
                addedItems.push($scope.sales.salesItem[i]);
        }
        return addedItems;
    }

    function dayDiff(expireDate) {
        $scope.highlight = "";
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date(formatString(expireDate));
        var date1 = new Date(formatString(today));
        //var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference < 30) {
            var dt = expireDate;
            $scope.highlight = "Expiry";
        } else {
            $scope.highlight = "";
        }
    }
    function formatString(format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    }

    function loadBatch(editBatch) {

        productStockService.productBatch($scope.selectedProduct.product.id).then(function (response) {
            $scope.batchList = response.data;
            $scope.batchList = $filter('orderBy')($scope.batchList, 'expireDate');
            var qty = document.getElementById("quantity");
            qty.focus();
            var totalQuantity = 0;
            var rack = "";
            var tempBatch = [];
            var tempBatchScan = [];
            var availableStock = null;

            for (var i = 0; i < $scope.batchList.length; i++) {
                if ($scope.Editsale == false || editBatch == null) {
                    $scope.batchList[i].productStockId = $scope.batchList[i].id;
                    $scope.batchList[i].id = null;

                    $scope.batchList[i].sellingPrice = parseFloat(($scope.batchList[i].purchasePriceWithoutTax * (1 + ($scope.batchList[i].gstTotal / 100))).toFixed(6));

                    availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                    if (editBatch != null && editBatch.productStockId == $scope.batchList[i].productStockId)
                        editBatch.availableQty = availableStock;
                    totalQuantity += availableStock;
                    if (availableStock == 0)
                        continue;
                    $scope.batchList[i].availableQty = availableStock;
                    if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                        rack = $scope.batchList[i].rackNo;
                    }
                    tempBatch.push($scope.batchList[i]);
                } else {
                    if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                        rack = $scope.batchList[i].rackNo;
                    }
                }
                if ($scope.batchList[i].rackNo == null || $scope.batchList[i].rackNo == '' || $scope.batchList[i].rackNo == undefined) {
                    $scope.batchList[i].rackNo = '-';
                    rack = $scope.batchList[i].rackNo;
                }
                if ($scope.batchList[i].product.boxNo == null || $scope.batchList[i].product.boxNo == '' || $scope.batchList[i].product.boxNo == undefined) {
                    $scope.batchList[i].product.boxNo = '-';
                }
                if ($scope.scanBarcodeVal !== undefined && $scope.scanBarcodeVal !== "" && $scope.scanBarcodeVal !== null) {
                    if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')($scope.batchList[i].purchaseBarcode)) {
                        tempBatchScan.push($scope.batchList[i]);
                    }
                    else if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')($scope.batchList[i].eancode)) {
                        tempBatchScan.push($scope.batchList[i]);
                    }
                }
            }
            if (editBatch != null && tempBatch.length == 0) {
                availableStock = $scope.getAvailableStock(editBatch, editBatch);
                editBatch.availableQty = availableStock;
                totalQuantity = availableStock;
                tempBatch.push(editBatch);
            }
            $scope.batchList = tempBatch;
            if (tempBatchScan.length == 1) {
                $scope.batchList = rearrangeBatchList($scope.batchList, tempBatchScan);
            }
            if ($scope.batchList.length > 0) {
                var sreturn = $scope.selectedBatch.salesReturn;
                var ttype = $scope.selectedBatch.transactionType;
                if (editBatch == null) {
                    $scope.selectedBatch = $scope.batchList[0];
                    $scope.selectedBatch.reminderFrequency = "0";

                    $scope.selectedBatch.discount = "";

                    $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                } else {
                    $scope.selectedBatch = editBatch;
                    $scope.selectedBatch.discount = editBatch.discount;
                    $scope.selectedBatch.previoussellingPrice = editBatch.sellingPrice;
                    $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                }

                $scope.selectedBatch.totalQuantity = totalQuantity;
                $scope.selectedBatch.rackNo = rack;
                $scope.selectedBatch.salesReturn = sreturn;
                $scope.selectedBatch.transactionType = ttype;
                dayDiff($scope.selectedBatch.expireDate);

                if ($scope.batchListType == "Product") {
                    if ($scope.taxValuesList.length > 0) {
                        $scope.getGstvalue = $scope.selectedBatch.gstTotal;
                        var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.getGstvalue) }, true);
                        if (taxList.length == 0) {
                            $scope.gstMode = true;
                            return;
                        }
                        else {
                            $scope.gstMode = false;
                        }
                    }
                }

                if (tempBatchScan.length > 0) {
                    loadAutoQty();
                }
            } else {
                $scope.zeroBatch = true;
            }
        }, function () { });

        $scope.valPurchasePrice = "";
        var qty = document.getElementById("quantity");
        qty.focus();

    }

    function rearrangeBatchList(batchList1, batchList2) {
        var tmpList = [];
        for (var j = 0; j < batchList2.length; j++) {
            tmpList.push(batchList2[j]);
        }
        for (var i = 0; i < batchList1.length; i++) {
            var exist = false;
            for (var j = 0; j < batchList2.length; j++) {
                if (batchList1[i] == batchList2[j]) {
                    exist = true;
                }
            }
            if (!exist) {
                tmpList.push(batchList1[i]);
            }
        }
        return tmpList;
    };

    $scope.onProductSelect = function () {
        $scope.previousdetails = []; //added by Annadurai
        $scope.selectedBatch = { reminderFrequency: "0" };
        $scope.selectedBatch.transactionType = "sales";
        $scope.selectedBatch.salesReturn = false;
        $scope.selectedBatch.canEnableTaxEdit = false;
        if ($scope.selectedProduct.stock > 0) {
            $scope.zeroQty = false;
            productStockService.productBatch($scope.selectedProduct.product.id).then(function (response) {
                var tempBatchList1 = [];
                $scope.batchList = response.data;
                var x = 0;
                for (var i = 0; i < $scope.batchList.length; i++) {
                    $scope.batchList[i].productStockId = $scope.batchList[i].id;
                    $scope.batchList[i].id = null;
                    var availableStock = $scope.getAvailableStock($scope.batchList[i], null);
                    if (availableStock == 0)
                        continue;
                    $scope.batchList[i].availableQty = availableStock;
                }

                $scope.batchList = {};
                productStockService.productBatch($scope.selectedProduct.product.id).then(function (responses) {
                    var batchListCheck = responses.data;
                    var totalQuantityCheck = 0;
                    var tempBatchCheck = [];
                    var editBatchCheck = null;
                    var availableStockCheck = null;
                    for (var i = 0; i < batchListCheck.length; i++) {
                        batchListCheck[i].productStockId = batchListCheck[i].id;
                        batchListCheck[i].id = null;
                        availableStockCheck = $scope.getAvailableStock(batchListCheck[i], editBatchCheck);
                        if (editBatchCheck != null && editBatchCheck.productStockId == batchListCheck[i].productStockId)
                            editBatchCheck.availableQty = availableStockCheck;
                        totalQuantityCheck += availableStockCheck;
                        if (availableStockCheck == 0)
                            continue;
                        batchListCheck[i].availableQty = availableStockCheck;
                        tempBatchCheck.push(batchListCheck[i]);
                    }
                    if (editBatchCheck != null && tempBatchCheck.length == 0) {
                        availableStockCheck = $scope.getAvailableStock(editBatchCheck, editBatchCheck);
                        editBatchCheck.availableQty = availableStockCheck;
                        totalQuantityCheck = availableStockCheck;
                        tempBatchCheck.push(editBatchCheck);
                    }
                    batchListCheck = tempBatchCheck;
                    if (batchListCheck.length > 0) {
                        $scope.zeroBatch = false;
                        salesService.getBatchListDetail().then(function (response1) {
                            $scope.batchListType = response1.data;
                            $scope.previousdetails = [];//Added for HistoryPopup on 07032017

                            loadBatch(null);

                        }, function () {
                        });
                    } else {
                        $scope.zeroBatch = true;
                    }
                }, function () { });
            }, function (error) {
                console.log(error);
            });
        } else {
            $.alert({
                title: "Stock not available!",
                content: 'Stock not available for the selected item (' + $scope.selectedProduct.product.name + ')',
                animation: 'center',
                closeAnimation: 'center',
                backgroundDismiss: true,
                buttons: {
                    okay: {
                        text: 'Ok [Esc]',
                        btnClass: 'primary-button custom-transform',
                        action: function () {
                            // do nothing
                        }
                    }
                }
            });
            $scope.selectedProduct = ""
        }
    };


    $scope.enableSaleBtn = function () {
        $scope.Enablesalesbtn = true;
    };


    $scope.validateQty = function (valqty) {
       
        if (valqty == true) {
            $scope.valqty = true;
            $scope.addbtnenable = false;
            $scope.enableSaleBtn();
        }
        if ($scope.selectedBatch == null)
            return;
       
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.totalQuantity && $scope.selectedBatch.editId == null) {
            $scope.Quantityexceeds = 1;
        }
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty && $scope.selectedBatch.editId != null) {
            $scope.Quantityexceeds = 1;
        }
        //Code added by Sarubala - For Return in Sales
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty) {
            $scope.Quantityexceeds = 1;
            toastr.info("Please enter quantity available in selected batch");
            document.getElementById("quantity").focus();
            return;
        }
        //End
        var qty = document.getElementById("quantity");
        if (qty.value == "") {
            $scope.salesItems.$valid = false;
            $scope.Quantityexceeds = 0;
        } else {
            if ((qty.value > $scope.selectedBatch.totalQuantity && $scope.selectedBatch.editId == null) || (qty.value > $scope.selectedBatch.availableQty && $scope.selectedBatch.editId != null)) {
                $scope.IsFormInvalid = false;
                $scope.salesItems.$valid = true;
                if ($scope.Quantityexceeds == 1) {
                    $scope.IsFormInvalid = false;
                    $scope.salesItems.$valid = true;
                } else {
                    $scope.salesItems.$valid = false;
                    $scope.IsFormInvalid = true;
                }
            } else {
                if (typeof ($scope.selectedBatch.discount) == "string") {
                   
                    if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || parseFloat($scope.selectedBatch.discount) > 100) {
                        $scope.salesItems.$valid = false;                        
                        $scope.IsFormInvalid = true;
                    } else {
                        $scope.IsFormInvalid = false;
                        $scope.salesItems.$valid = true;
                    }
                } else {
                    if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || parseFloat($scope.selectedBatch.discount) > 100) {
                        $scope.salesItems.$valid = false;                       
                        $scope.IsFormInvalid = true;
                    } else {
                        $scope.IsFormInvalid = false;
                        $scope.salesItems.$valid = true;
                    }
                }
                $scope.Quantityexceeds = 0;
            }
        }
    };

    $scope.addSales = function () {

        if ($scope.selectedBatch.quantity <= 0 || $scope.selectedBatch.quantity == undefined) {
            return;
        }
        if ($scope.batchList.length == 0) {
            return;
        }
        
        $scope.maxDiscountExceeds = false;
        
        $scope.enableTempStockPopup = false;
        if ($scope.Quantityexceeds == 1) return;
       
        if ($scope.selprice == true || $scope.valdisct == true || $scope.valqty == true || $scope.edititem == false) {
            selprice = false;
            valdisct = false;
            valqty = false;
            $scope.edititem = false;
            $scope.valqty = false;
            $scope.valdisct = false;
            $scope.selprice = false;
            $scope.Enablesalesbtn = false;
            $scope.maxDiscountExceeds = false;
            if ($scope.maxDiscountFixed == 'Yes') {
                if ($scope.selectedBatch.discount > $scope.maxDisountValue) {
                    $scope.maxDiscountExceeds = true;
                    return false;
                }
            }
            $scope.enableTempStockPopup = false;
            if ($scope.Quantityexceeds == 1) return;
            if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == null || ($scope.selectedBatch.purchasePrice > $scope.selectedBatch.sellingPrice && $scope.salesPriceSettings != 3) || (($scope.selectedBatch.purchasePriceWithoutTax == '' || $scope.selectedBatch.purchasePriceWithoutTax == null || $scope.selectedBatch.purchasePriceWithoutTax == undefined) && $scope.salesPriceSettings == 3)) { //Modified by Sarubala on 08-11-17
                return;
            }
            if (typeof ($scope.selectedBatch.discount) == "string") {
                if (parseFloat($scope.selectedBatch.discount) > 100 || $scope.selectedBatch.discount == '.') {
                    return;
                } else if ($scope.selectedBatch.discount == undefined || $scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "") {
                    $scope.selectedBatch.discount = 0;
                }
            } else {
                if (parseFloat($scope.selectedBatch.discount) > 100) {
                    return;
                } else if ($scope.selectedBatch.discount == undefined || $scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "") {
                    $scope.selectedBatch.discount = 0;
                }
            }

            var RequiredQuantity = angular.element("#spnquantity").attr("data-quantityId");
            var ChangedMrp = angular.element("#spnquantity").attr("data-price");
            var ChangedDicount = angular.element("#spnquantity").attr("data-discount") || 0;
            if ($scope.selectedBatch.salesReturn) {
                $scope.selectedBatch.quantity = $scope.selectedBatch.quantity * (-1);
            }
            if (($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty) && !$scope.selectedBatch.salesReturn) {
                var addedItem = "";
                for (var i = 0; i < $scope.batchList.length; i++) {
                    if ($scope.batchList[i].editId == null) {
                        $scope.batchList[i].editId = $scope.editId++;
                        addedItem = getAddedItems($scope.batchList[i].productStockId, $scope.batchList[i].sellingPrice, ChangedDicount);
                        if (addedItem != null) {                          

                            if (RequiredQuantity != 0) {
                                if (RequiredQuantity > $scope.batchList[i].availableQty) {
                                    addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.batchList[i].availableQty);
                                    RequiredQuantity = RequiredQuantity - $scope.batchList[i].availableQty;
                                    $scope.batchList[i].availableQty = 0;
                                } else {
                                    $scope.batchList[i].availableQty = $scope.batchList[i].availableQty - RequiredQuantity;
                                    addedItem.quantity = parseInt(addedItem.quantity) + parseInt(RequiredQuantity);
                                    RequiredQuantity = 0;
                                }
                            }
                        } else {
                            if (RequiredQuantity != 0) {
                                if (RequiredQuantity > $scope.batchList[i].availableQty) {
                                    $scope.batchList[i].quantity = $scope.batchList[i].availableQty;
                                    RequiredQuantity = RequiredQuantity - $scope.batchList[i].availableQty;
                                    $scope.batchList[i].availableQty = 0;
                                } else {
                                    $scope.batchList[i].availableQty = $scope.batchList[i].availableQty - RequiredQuantity;
                                    $scope.batchList[i].quantity = RequiredQuantity;
                                    RequiredQuantity = 0;
                                }
                                $scope.batchList[i].discount = ChangedDicount;
                                $scope.batchList[i].salesReturn = false;
                                
                                if ($scope.customerHelper.data.selectedCustomer.patientType == 2) {

                                    var vatPercent = 0;
                                    if ($scope.isGstEnabled == true) {
                                        if ($scope.batchList[i].gstTotal != null && $scope.batchList[i].gstTotal != undefined) {
                                            vatPercent = $scope.batchList[i].gstTotal;
                                        } else if ($scope.batchList[i].product.gstTotal != null && $scope.batchList[i].product.gstTotal != undefined) {
                                            vatPercent = $scope.batchList[i].product.gstTotal;
                                        }

                                    } else {
                                        vatPercent = $scope.batchList[i].vat;
                                    }

                                    $scope.batchList[i].sellingPrice = ((($scope.batchList[i].packagePurchasePrice / $scope.batchList[i].packageSize) + (($scope.batchList[i].packagePurchasePrice / $scope.batchList[i].packageSize) * (vatPercent / 100))) * 1.1).toFixed(2);
                                }

                                $scope.batchList[i].saleProductName = $scope.batchList[i].product.name; //Added by Sarubala on 07-09-17 to save product name
                                $scope.batchList[i].transactionType = $scope.selectedBatch.transactionType;
                                $scope.sales.salesItem.push($scope.batchList[i]);
                            }
                        }
                    } else {
                        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                            if ($scope.sales.salesItem[i].editId == $scope.batchList[i].editId) {
                                $scope.sales.salesItem[i] = $scope.batchList[i];
                                $scope.sales.salesItem[i].originalQty = $scope.batchList[i].orginalQty;
                                if (!angular.isUndefinedOrNull($scope.salesEditId))
                                    $scope.sales.salesItem[i].Action = "U";
                                break;
                            }
                        }
                    }
                }
                $scope.salesItems.$setPristine();
                $scope.selectedProduct = null;
                $scope.selectedBatch = { "reminderFrequency": "0", "purchasePrice": 0 };
                $scope.selectedBatch.transactionType = "sales";
                $scope.selectedBatch.salesReturn = false;
                $scope.selectedBatch.canEnableTaxEdit = false;
                $scope.batchList = {};
                $scope.highlight = "";
                ChangedDicount = 0;
                ChangedMrp = 0;
                
            } else {
                if ($scope.selectedBatch.editId == null) {
                    $scope.selectedBatch.editId = $scope.editId++;
                    addedItem = getAddedItems($scope.selectedBatch.productStockId, $scope.selectedBatch.sellingPrice, $scope.selectedBatch.discount);
                    if (addedItem != null && (addedItem.transactionType == $scope.selectedBatch.transactionType)) {
                        addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.selectedBatch.quantity);
                    } else {
                        $scope.selectedBatch.saleProductName = $scope.selectedBatch.product.name;//Added by Sarubala on 07-09-17 to save product name
                        $scope.sales.salesItem.push($scope.selectedBatch);
                    }
                } else {
                    for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                        if ($scope.sales.salesItem[i].editId == $scope.selectedBatch.editId) {
                            $scope.sales.salesItem[i] = $scope.selectedBatch;
                            $scope.sales.salesItem[i].originalQty = $scope.selectedBatch.orginalQty;
                            if (!angular.isUndefinedOrNull($scope.salesEditId))
                                $scope.sales.salesItem[i].Action = "U";
                            break;
                        }
                    }
                }
                $scope.salesItems.$setPristine();
                $scope.selectedProduct = null;
                $scope.selectedBatch = { "reminderFrequency": "0", "purchasePrice": 0 };
                $scope.selectedBatch.transactionType = "sales";
                $scope.selectedBatch.salesReturn = false;
                $scope.selectedBatch.canEnableTaxEdit = false;
                
                $scope.batchList = {};
                $scope.highlight = "";
                var drugName = document.getElementById('drugName');
                drugName.disabled = false;
            }
            //Discount Length Code
            var disclength = 0;
            for (var k = 0; k < $scope.sales.salesItem.length; k++) {
                if ($scope.sales.salesItem[k].discount == 0) {
                    disclength++;
                }
            }
            if (disclength == $scope.sales.salesItem.length) {
                $scope.discountInValid = false;
            }
           
            //Doctor And Schedule Code
            $scope.isdoctornameMandatory = false;
            $scope.iscustomerNameMandatory = false;
            $scope.schedulecount = 0;
            
            $scope.isSalesTypeMandatory = false;
            
            $scope.focusOnProduct();
            LeadsRequiredquantity = "";
            $scope.BillCalculations();            
                    
        }
        // need to copy data arun
        $scope.focusProductSearch = true;
        $scope.focusGenericSearch = false;
        var ele = document.getElementById("drugName");
        ele.focus();
    };

    function getAddedItems(id, sellingPrice, discount) {
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ($scope.sales.salesItem[i].productStockId == id && $scope.sales.salesItem[i].sellingPrice == sellingPrice && $scope.sales.salesItem[i].discount == discount) {
                return $scope.sales.salesItem[i];
            }
        }
        return null;
    }

    $scope.backButton = function () {
        window.location = window.location.origin + "/ProductStock/List";
    };

    $scope.cancel = function () {
        if (window.confirm('Are you sure, Do you want to cancel?')) {
            window.location = window.location.origin + window.location.pathname;
        }        
    };

    $scope.removeSales = function (item) {

        if ($scope.Editsale == true) {
            if (item.salesOrderEstimateId != null && item.salesOrderProductSelected == 1) {
                toastr.info("Cannot delete order to sales converted items from sales bill");
                return;
            }
        }

        if (!confirm("Are you sure, Do you want to delete ? "))
            return;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            $scope.sales.total += ($scope.sales.salesItem[k].sellingPrice * $scope.sales.salesItem[k].quantity);
        }
        $scope.sales.total -= (item.sellingPrice * item.quantity);
        var index = $scope.sales.salesItem.indexOf(item);
        $scope.sales.salesItem.splice(index, 1);
        $scope.discountInValid = false;
        $scope.schedulecount = 0;
        var schedulecount = 0;
        if ($scope.sales.salesItem.length > 0) {
            for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                if (($scope.sales.salesItem[i].discount > 0) && ($scope.sales.salesItem[i].discount <= 100)) {
                    $scope.discountInValid = true;
                }                
            }
            $scope.schedulecount = schedulecount;
           
            $scope.iscustomerNameMandatory = false;
            $scope.isdoctornameMandatory = false;
        } else {
            $scope.iscustomerNameMandatory = false;
            $scope.isdoctornameMandatory = false;
        }       
       
        $scope.BillCalculations();        
        $scope.sales.discount = "";
        $scope.sales.discountValue = "";
        $scope.selectedProduct = null;
        var drugName = document.getElementById('drugName'); //Added by Sarubala on 04-08-17
        drugName.disabled = false;
        $scope.selectedBatch = { "reminderFrequency": "0", "purchasePrice": 0 };
        $scope.selectedBatch.transactionType = "sales";
        $scope.selectedBatch.salesReturn = false;
        $scope.selectedBatch.canEnableTaxEdit = false;
        $scope.batchList = {};
        var rcount = 0;
        $scope.salesReturnCount = 0;
        $scope.salesCount = 0;
        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            if ($scope.sales.salesItem[j].salesReturn) {
                rcount++;
                $scope.salesReturnCount++;
            } else {
                $scope.salesCount++;
            }
        }
        if (rcount > 0) {
            $scope.returnDiscountInValid = true;
        } else {
            $scope.returnDiscountInValid = false;
        }
        //Added by Settu on 04/07/17 for auto assign credit amount        
        $scope.focusCreditTextBox = false;
        document.getElementById('drugName').focus();
    };

    $scope.BillCalculations = function () {
        var sumvaluewithoutVat = 0;
        var sumvaluewithVat = 0;
        var discountAmt = 0;
        var TotalDiscountedAmount = 0;
        var vatAmt = 0;
        var totalvatAmt = 0;
        $scope.totalAmountwithoutVat = 0;
        $scope.vatAmount = 0;
        $scope.discountedTotalNetValue = 0;
        $scope.roundoffNetAmount = 0;
        $scope.totalNetValue = 0;
        $scope.FinalNetAmount = 0;
        var salesAmt = 0;
        var returnAmt = 0;
        if ($scope.sales.discount == 100)
            return 0;
        
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100) && ($scope.sales.salesItem[i].salesReturn != true)) {
                var vatPercent1 = 0;
                if ($scope.isGstEnabled == true) {
                    if ($scope.sales.salesItem[i].gstTotal != null && $scope.sales.salesItem[i].gstTotal != undefined) {
                        vatPercent1 = $scope.sales.salesItem[i].gstTotal;
                    } else if ($scope.sales.salesItem[i].product.gstTotal != null && $scope.sales.salesItem[i].product.gstTotal != undefined) {
                        vatPercent1 = $scope.sales.salesItem[i].product.gstTotal;
                    }

                } else {
                    vatPercent1 = $scope.sales.salesItem[i].vat;
                }
                if (isNaN(vatPercent1) == false) {
                    vatPercent1 = parseFloat(vatPercent1);
                } else {
                    vatPercent1 = 0;
                }
                discountAmt = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
                TotalDiscountedAmount += discountAmt;
                sumvaluewithVat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - discountAmt);
                vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + parseFloat(vatPercent1))) * 100));
                totalvatAmt += vatAmt;
                if ($scope.isComposite == true) {
                    sumvaluewithoutVat = sumvaluewithVat;
                }
                else {
                    sumvaluewithoutVat = (sumvaluewithVat - totalvatAmt);
                }

            }
        }
        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            if ((parseFloat($scope.sales.salesItem[j].discount) >= 0) && (parseFloat($scope.sales.salesItem[j].discount) < 100)) {
                var discountAmt1 = $scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity * parseFloat($scope.sales.salesItem[j].discount) / 100;
                if ($scope.sales.salesItem[j].salesReturn) {
                    returnAmt += (($scope.sales.salesItem[j].sellingPrice * Math.abs($scope.sales.salesItem[j].quantity)) + discountAmt1);
                } else {
                    salesAmt += (($scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity) - discountAmt1);
                }
            }
        }
        $scope.returnAmount = returnAmt;
        $scope.salesAmount = salesAmt;
        $scope.totalAmountwithoutVat = sumvaluewithoutVat;       
        $scope.totalNetValue = sumvaluewithVat;
      
        if ($scope.returnDiscountInValid && $scope.returnAmount > 0) {          
            // Added by Gavaskar Loyalty Points 12-12-2017 Start
            var amt1 = 0;
            if ($scope.sales.redeemAmt > 0) {
                amt1 = parseFloat($scope.salesAmount) - parseFloat($scope.returnAmount) - parseFloat($scope.sales.redeemAmt);
            }
            else {
                amt1 = $scope.salesAmount - $scope.returnAmount;
            }
            // Added by Gavaskar Loyalty Points 12-12-2017 End
            var tempamt = 0;
            if (amt1 < 0) {
                tempamt = angular.copy(amt1);
                amt1 = amt1 * (-1);
            }
            if (tempamt == 0) {
                $scope.roundoffNetAmount = amt1 - amt1;
                $scope.FinalNetAmount = amt1;
                $scope.finalNetVAL = amt1;
            } else {
                $scope.roundoffNetAmount = (amt1 - amt1) * (-1);
                $scope.FinalNetAmount = (amt1) * (-1);
                $scope.finalNetVAL = (amt1) * (-1);
            }

        } else {
           
            $scope.roundoffNetAmount = $scope.totalNetValue - $scope.totalNetValue;
            $scope.FinalNetAmount = $scope.totalNetValue;
            $scope.finalNetVAL = sumvaluewithVat;
        }        

        $scope.discountedTotalNetValue = TotalDiscountedAmount;

        $scope.vatAmount = totalvatAmt;        

        setModelValues();
    };

    function setModelValues() {
        if ($scope.sales.salesReturn == null) {
            $scope.sales.salesReturn = {
            };
        }
        
        $scope.sales.salesItemAmount = 0;
        $scope.sales.discount = $scope.sales.discount || 0;
        $scope.sales.discountValue = $scope.sales.discountValue || 0;
        $scope.sales.gstAmount = 0;
        $scope.sales.isRoundOff == false
        $scope.isEnableRoundOff = false;

        $scope.sales.salesReturn.totalDiscountValue = 0;
        $scope.sales.salesReturn.netAmount = 0;
        $scope.sales.salesReturn.roundOffNetAmount = 0;
        $scope.sales.salesReturn.returnItemAmount = 0;
        $scope.sales.salesReturn.gstAmount = 0;
        $scope.sales.salesReturn.discount = $scope.sales.salesReturn.discount || 0;
        $scope.sales.salesReturn.discountValue = $scope.sales.salesReturn.discountValue || 0;

        for (var i = 0; i < $scope.sales.salesItem.length; i++) {

            var discount = 0;
            $scope.sales.salesItem[i].discount = $scope.sales.salesItem[i].discount || 0;
            discount = $scope.sales.salesItem[i].discount;

            if ($scope.sales.isRoundOff == false) {
                $scope.salesAmount = $scope.sales.salesAmount;
            }

            var totalItemAmount = Math.abs($scope.sales.salesItem[i].quantity) * $scope.sales.salesItem[i].sellingPrice;
            var totalDiscountAmt = totalItemAmount * discount / 100;
            $scope.sales.salesItem[i].totalAmount = totalItemAmount - totalDiscountAmt;
            $scope.sales.salesItem[i].discountAmount = totalDiscountAmt;

            $scope.sales.salesItemAmount += totalItemAmount;
            $scope.sales.salesItem[i].itemAmount = totalItemAmount;

            var taxPerc = 0;
            if ($scope.isGstEnabled == true) {
                if ($scope.sales.salesItem[i].gstTotal != null && $scope.sales.salesItem[i].gstTotal != undefined) {
                    taxPerc = $scope.sales.salesItem[i].gstTotal;
                } else if ($scope.sales.salesItem[i].product.gstTotal != null && $scope.sales.salesItem[i].product.gstTotal != undefined) {
                    taxPerc = $scope.sales.salesItem[i].product.gstTotal;
                }
            } else {
                taxPerc = $scope.sales.salesItem[i].vat;
            }
            if (isNaN(taxPerc) == false) {
                taxPerc = parseFloat(taxPerc);
            } else {
                taxPerc = 0;
            }
            var taxAmt = ($scope.sales.salesItem[i].totalAmount - (($scope.sales.salesItem[i].totalAmount / (100 + parseFloat(taxPerc))) * 100));
            if ($scope.isGstEnabled == true) {
                $scope.sales.salesItem[i].gstAmount = taxAmt;
                $scope.sales.salesItem[i].vatAmount = 0;
                $scope.sales.salesItem[i].vat = null;
                if ($scope.sales.salesItem[i].salesReturn) {
                    $scope.sales.salesReturn.gstAmount += taxAmt;
                }
                else {
                    $scope.sales.gstAmount += taxAmt;
                }
            }
            else {
                $scope.sales.salesItem[i].vatAmount = taxAmt;
                $scope.sales.salesItem[i].gstAmount = 0;
                $scope.sales.salesItem[i].gstTotal = null;
            }
        }

        $scope.sales.saleAmount = $scope.totalNetValue;
        $scope.sales.roundoffSaleAmount = 0;

        $scope.sales.salesReturn.roundOffNetAmount = 0;
    };

    $scope.batchSelected = function (batch) {
        $scope.valPurchasePrice = "";
        var totalQuantity = 0;
        $scope.selectedBatch = $scope.batchList[batch];
        $scope.selectedBatch.transactionType = "sales";
        $scope.selectedBatch.salesReturn = false;

        for (var i = 0; i < $scope.batchList.length; i++) {
            totalQuantity += parseInt($scope.batchList[i].stock);
        }
        
        $scope.batchList = rearrangeBatchList1($scope.batchList, $scope.selectedBatch);        
        
        if ($scope.selectedBatch != undefined) {
            if ($scope.selectedBatch.totalQuantity == undefined) {
                $scope.selectedBatch.totalQuantity = "";
            } else {
                $scope.selectedBatch.totalQuantity = totalQuantity;
            }
            if ($scope.selectedBatch.availableQty != null && $scope.selectedBatch.batchNo != null) {
                $scope.selectedBatch.totalQuantity = totalQuantity;
            }

            $scope.selectedBatch.discount = "";

            dayDiff($scope.selectedBatch.expireDate);
        }
    };

    function rearrangeBatchList1(batchList1, batchList2) {
        var tmpList = [];
        tmpList.push(batchList2);
        for (var i = 0; i < batchList1.length; i++) {
            var exist = false;
            if (batchList1[i] == batchList2) {
                exist = true;
            }
            if (!exist) {
                tmpList.push(batchList1[i]);
            }
        }
        return tmpList;
    };

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.isSaveValid = function () {
        var valid = null;
        if ($scope.InvoiceSeriestype != 1) {
            if ($scope.sales.salesItem.length == 0 || $scope.IsSelectedInvoiceSeries == false) {
                if ($scope.selectedBatch == undefined) {
                    $scope.salesItems.$valid = false;
                    valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing);
                    if ($scope.sales.discountType == "1" && $scope.sales.discount > 100) {
                        valid = false;
                    }
                    if (!valid)
                        return false;
                } else {
                    if ($scope.selectedBatch.name != null && $scope.selectedBatch.quantity != null) {
                        $scope.salesItems.$valid = true;
                    }
                    valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing);
                    if ($scope.sales.discountType == "1" && $scope.sales.discount > 100) {
                        valid = false;
                    }
                    if (!valid)
                        return false;
                }
            }
        } else {           
            if ($scope.selectedBatch == undefined) {
                $scope.salesItems.$valid = false;
                valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing);
                if ($scope.sales.discountType == "1" && $scope.sales.discount > 100) {
                    valid = false;
                }
                if (!valid)
                    return false;
            } else {
                if ($scope.selectedBatch.name != null && $scope.selectedBatch.quantity != null) {
                    $scope.salesItems.$valid = true;
                }
                valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing);
                if ($scope.sales.discountType == "1" && $scope.sales.discount > 100) {
                    valid = false;
                }
                if (!valid)
                    return false;               
            }
        }
        if ($scope.InvoiceSeriestype != 1) {
            if ($scope.IsSelectedInvoiceSeries == true) {
                return false;
            }
        }        
       
        return true;
    };


    $scope.save = function () {
        var custname = null;
        $scope.sales.doorDeliveryStatus = null;

        if (angular.element(document.getElementById("saleComplete"))[0].disabled == true)
            return;        

        if ($scope.SaveProcessing == true) {
            return false;
        }
        
        $scope.iscustomerNameMandatory = false;
        $scope.isdoctornameMandatory = false;       
        $scope.isSalesTypeMandatory = false;
        $scope.sales.mobile = "";
        
        $scope.saveAfterConfirm();

        //completeSale();

    };

    $scope.saveAfterConfirm = function () {

        var m = ModalService.showModal({
            "controller": "salesCompleteMobileVerifyCtrl",
            "templateUrl": 'salesCompleteVerify',
            "inputs": {}
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result == "Yes") {
                    $scope.sales.mobile = cacheService.get('mobileNo');
                    completeSale();
                }
            });
        });    
        
    };
   

    function completeSale() {
        $scope.sales.roundoffNetAmount = 0;
        $scope.sales.netAmount = $scope.FinalNetAmount;
        $scope.sales.vatAmount = parseFloat($scope.vatAmount.toFixed(2));

        if ($scope.sales.redeemAmt == undefined) {
            $scope.sales.redeemAmt = 0;

        }
        if ($scope.sales.loyaltyPts == undefined) {
            $scope.sales.loyaltyPts = 0;
        }
        if ($scope.sales.redeemPts == undefined) {
            $scope.sales.redeemPts = 0;
        }
        if ($scope.sales.redeemPercent == undefined) {
            $scope.sales.redeemPercent = 0;
        }
        if ($scope.sales.discount == '' || $scope.sales.discount == null) {
            $scope.sales.discount = 0;
        }
        if ($scope.sales.redeemAmt == '' || $scope.sales.redeemAmt == null) {
            $scope.sales.redeemAmt = 0;
        }

        $scope.sales.discountValue = (parseFloat($scope.sales.discount) * parseFloat($scope.sales.netAmount) / (100 - parseFloat($scope.sales.discount)));

        if ($scope.sales.discountValue == null && $scope.sales.discount == null) {
            $scope.sales.discountValue = 0;
            $scope.sales.discount = 0;
        }

        $scope.sales.discountValue = parseFloat($scope.sales.discountValue) + parseFloat($scope.sales.redeemAmt);
        $scope.sales.discount = parseFloat($scope.sales.discount) + parseFloat($scope.sales.redeemPercent);

        var index = -1;
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            $scope.sales.salesItem[i].salesItemSno = i;
        }

        if ($scope.InvoiceSeriestype == "" || $scope.InvoiceSeriestype == undefined) {
            $scope.InvoiceSeriestype = 1;
        }
        if ($scope.InvoiceSeriestype == 1) {
            if ($scope.Editsale == false) {
                $scope.sales.InvoiceSeries = "";
            }            
        }
        $scope.sales.isRoundOff = false;
        $scope.sales.discountType = 1;
        $scope.sales.totalDiscountValue = 0;


        $.LoadingOverlay("show");
        salesService.createOtherSales($scope.sales, $scope.minDate).then(function (response) {
            toastr.success('Sale completed successfully');

            $scope.salesItems.$setPristine();             
            $scope.selectedBatch = {};
            resetSales();
            $scope.getInvoiceNo();

            $.LoadingOverlay("hide");
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
            toastr.error("Error in Sales");
        });

    }

});
app.directive('numbersOnly', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.directive('noSpecialChar', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue === undefined)
                    return '';
                cleanInputValue = inputValue.replace("#", '');
                cleanInputValue = cleanInputValue.replace("^", '');
                cleanInputValue = cleanInputValue.replace("&", '');
                if (cleanInputValue != inputValue) {
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                }
                return cleanInputValue;
            });
        }
    };
});