app.controller('UpdateSalesOrderCreatectrl', function ($scope, vendorOrderService, vendorService, toastr, $filter, salesService, vendorPurchaseService, productService, $rootScope) {
    $scope.list = [];
    $scope.list.product = [];
    $scope.maxEditqtyExceeded = false;
    $scope.maxEditOrderQtySold = false;

    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.init = function (salesOrderEstimateId) {
        $.LoadingOverlay("show");
        getIsMaxDiscountAvail();
        salesService.getSalesOrderById(salesOrderEstimateId)
        .then(function (resp) {
            $scope.list = resp.data;
            var salesOrderItem = [];
            for (var i = 0; i < $scope.list.salesOrderEstimateItem.length; i++) {
                if ($scope.list.salesOrderEstimateItem[i].isDeleted != true && $scope.list.salesOrderEstimateItem[i].isOrder != 1) {
                    salesOrderItem.push($scope.list.salesOrderEstimateItem[i]);
                }
            }

            $scope.list.salesOrderEstimateItem = salesOrderItem;

            for (var i = 0; i < $scope.list.salesOrderEstimateItem.length; i++) {
                if ($scope.list.salesOrderEstimateItem[i].discount > $scope.maxDisountValue) {
                    $scope.maxDiscountExceeded = true;
                }

                if (parseFloat($scope.list.salesOrderEstimateItem[i].sellingPrice) > 0) {
                    $scope.list.salesOrderEstimateItem[i].isEditMrp = false;
                } else {
                    $scope.list.salesOrderEstimateItem[i].isEditMrp = true;
                }
            }

            if ($scope.list.salesOrderEstimateType == 1)
            {
                //var ele = document.getElementById("selectedSalesOrder");
                //ele.focus();
                var ele = document.getElementById("txtCustomQuantity0");
                if (ele != null) {
                    ele.focus();
                }
            }
            else
            {
                window.setTimeout(function () {
                    var ele = document.getElementById("txtCustomQuantity0");
                    if (ele != null) {
                        ele.focus();
                    }
                }, 0);
            }

           
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.focusCustomTxtpackageQty = function (id, index) {
        var ele = document.getElementById(id + index);
        ele.focus();
    };
    $scope.focustxtCustomsellingPrice = function (id, index) {
        var ele = document.getElementById(id + index);
        ele.focus();
    };
   
    $scope.cancelCustomSalesVendor = function () {
        if (confirm("Are you sure, Do you want to cancel ? ")) {
            window.location.href = "/sales/SalesOrderHistory";
        }
    };
    
    $scope.saveCustomSalesOrder = function () {

        for (var j = 0; j < $scope.list.salesOrderEstimateItem.length; j++) {
            //chekcing Quantity
            if ($scope.list.salesOrderEstimateItem[j].quantity == "") {
                document.getElementById("txtCustomQuantity" + j).focus();
                alert("Enter Quantity");
                return false;
                break;
            }
            if ($scope.list.salesOrderEstimateItem[j].quantity == 0) {
                document.getElementById("txtCustomQuantity" + j).focus();
                alert("0 Not Allowed");
                return false;
                break;
            }
        }

        //Added by Gavaskar 21-11-2017 Start
        if ($scope.maxDiscountFixed == "Yes")
        {
            for (var i = 0; i < $scope.list.salesOrderEstimateItem.length; i++) {
                if ($scope.list.salesOrderEstimateItem[i].isDeleted != true)
                {
                    if ($scope.list.salesOrderEstimateItem[i].discount > $scope.maxDisountValue) {
                        toastr.info("Discount should be less than Max discount");
                        return;
                    }
                }
            }
        }
       
        $scope.salesOrderEstimateType = $scope.list.salesOrderEstimateType;
       
        //Added by Gavaskar 21-11-2017 End

        $.LoadingOverlay("show");
        salesService.updateSalesOrderEstimate($scope.list).then(function (response) {
            toastr.success('Sales Order Saved Successfully');
            $.LoadingOverlay("hide");
            // window.location.href = "/sales/SalesOrderHistory";
            window.localStorage.setItem("doSelectedOrderEstimate", JSON.stringify($scope.salesOrderEstimateType));
            window.location.assign('/sales/SalesOrderHistory');
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    };
   
    $scope.deleteClick = function (item) {
        if (item.receivedQty > 0)
        {
            toastr.info("It seems this order item converted as sales");
            item.isDeleted = false;
            return;
        }
        else
        {
            if (item.isDeleted == true && !confirm("Are you sure to delete?")) {
                item.isDeleted = false;
                return;
            }
            else {
                $scope.maxEditDiscountExceeded = false;
                $scope.salesOrderEstimateList.$valid = true;
            }
        }
       
    }

   
    $scope.maxDiscountFixed = "No";
    function getIsMaxDiscountAvail() {
        salesService.getIsMaxDiscountAvail().then(function (response) {
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.maxDiscountFixed = "No";
            } else {
                if (response.data.maxDiscountAvail != undefined) {
                    $scope.maxDiscountFixed = response.data.maxDiscountAvail;
                } else {
                    $scope.maxDiscountFixed = "No";
                }
            }
            if ($scope.maxDiscountFixed == "Yes") {
                getMaxDiscountValue();
            }
        }, function () {
        });
    }

    $scope.maxDisountValue = "";
    function getMaxDiscountValue() {
        salesService.getMaxDiscountValue().then(function (response) {
            if (response.data != "" && response.data != null) {
                $scope.maxDisountValue = response.data.instanceMaxDiscount;
            }
        }, function () {
        });
    }

    $scope.maxDiscountExceeded = false;
    $scope.maxDiscount = false;
    $scope.changeDiscount = function (discount) {

            if (discount == "" || discount == ".") {
                discount = 0;
            }
            $scope.maxDiscountExceeded = false;
            if ($scope.maxDiscountFixed == 'Yes') {
                if (discount > $scope.maxDisountValue) {
                    $scope.maxDiscountExceeded = true;
                    return false;
                }
            }
            else
            {
                if (discount > 99) {
                    $scope.maxDiscount = true;
                    return false;
                }
                else
                {
                    $scope.maxDiscount = false;
                    return false;
                }
            }
    };

    $scope.maxEditDiscountExceeded = false;
    $scope.changeOrderEditDiscount = function (discount, saleEstimate) {

            if (discount == "" || discount == ".") {
                discount = 0;
            }
            $scope.maxEditDiscountExceeded = false;
            if ($scope.maxDiscountFixed == 'Yes') {
                if (saleEstimate.discount > $scope.maxDisountValue) {
                    $scope.maxEditDiscountExceeded = true;
                    return false;
                }
            }
            else {
                if (saleEstimate.discount > 100) {
                    $scope.salesOrderEstimateList.$valid = false;
                }
                $scope.maxEditDiscountExceeded = false;
            }
    };

    $scope.getProducts = function (val) {
        return productService.nonHiddenProductList(val).then(function (response) {
            response.data = $filter('orderBy')(response.data, '-totalstock');
            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }

            return output.map(function (item) {
                return item;
            });
        });
    };

    $scope.onProductSelect = function ($event, selectedProduct1) {
        $scope.quantityExceed = 0;
        if (selectedProduct1 != null) {
            vendorPurchaseService.getPurchaseValues(selectedProduct1.id, selectedProduct1.name).then(function (response) {
                $scope.vendorPurchaseItem = response.data.vendorPurchaseItem;

                    if ($scope.vendorPurchaseItem.packageSellingPrice == "" || $scope.vendorPurchaseItem.packageSellingPrice == null || $scope.vendorPurchaseItem.packageSellingPrice == undefined) {
                        $scope.vendorPurchaseItem.packageSellingPrice = 0;
                    }
                    if ($scope.vendorPurchaseItem.packageMRP == "" || $scope.vendorPurchaseItem.packageMRP == null || $scope.vendorPurchaseItem.packageMRP == undefined) {
                        $scope.vendorPurchaseItem.packageSellingPrice = 0;
                    }
                    if ($scope.vendorPurchaseItem.packageSize == "" || $scope.vendorPurchaseItem.packageSize == null || $scope.vendorPurchaseItem.packageSize == undefined) {
                        $scope.vendorPurchaseItem.packageSize = 0;
                    }
                    if ($scope.vendorPurchaseItem.packageSellingPrice != 0 && $scope.vendorPurchaseItem.packageSize != 0) {
                        $scope.selectedSalesOrder.sellingPrice = ($scope.vendorPurchaseItem.packageSellingPrice / $scope.vendorPurchaseItem.packageSize);
                        // $scope.selectedSalesOrder.sellingPrice = $scope.vendorPurchaseItem.packageSellingPrice;
                        $scope.selectedSalesOrder.isEditMrp = false;

                    }
                    else {
                        $scope.selectedSalesOrder.sellingPrice = 0;
                        $scope.selectedSalesOrder.isEditMrp = true;
                    }

                    document.getElementById("OrderQuantity").focus();
                   

            }, function (response) {
                $scope.responses = response;
            });
        }
    };

    $scope.addSalesOrderProduct = function () {

        var existItem = 0;

        if ($scope.selectedSalesOrder.sellingPrice == "" || $scope.selectedSalesOrder.sellingPrice == null || $scope.selectedSalesOrder.sellingPrice == undefined) {
            $scope.selectedSalesOrder.sellingPrice = 0;
        }

        if ($scope.selectedSalesOrder.sellingPrice > 0) {
            $scope.selectedSalesOrder.isEditMrp = false;
        } else {
            $scope.selectedSalesOrder.isEditMrp = true;
        }

        if ($scope.selectedSalesOrder.quantity > 0) {
            for (var i = 0; i < $scope.list.salesOrderEstimateItem.length; i++) {
                if ($scope.list.salesOrderEstimateItem[i].id == $scope.selectedSalesOrder.id) {
                    existItem = 1;
                }
            }
            for (var k = 0; k < $scope.list.salesOrderEstimateItem.length; k++) {
                if (($scope.list.salesOrderEstimateItem[k].productId == $scope.selectedSalesOrder.id)) {
                    toastr.info("Product already added");
                    document.getElementById("drugName1").focus();
                    return;
                }
            }
            if (existItem == 0) {
                if ($scope.selectedSalesOrder.accountId === null || $scope.selectedSalesOrder.accountId === undefined || $scope.selectedSalesOrder.accountId === "") {
                    $scope.selectedSalesOrder.productMasterID = $scope.selectedSalesOrder.id;
                    $scope.selectedSalesOrder.productId = $scope.selectedSalesOrder.id;
                    $scope.selectedSalesOrder.productMasterName = $scope.selectedSalesOrder.name;
                    $scope.selectedSalesOrder.manufacturer = $scope.selectedSalesOrder.manufacturer;
                    $scope.selectedSalesOrder.category = $scope.selectedSalesOrder.category;
                    $scope.selectedSalesOrder.schedule = $scope.selectedSalesOrder.schedule;
                    $scope.selectedSalesOrder.genericName = $scope.selectedSalesOrder.genericName;
                    $scope.selectedSalesOrder.packing = $scope.selectedSalesOrder.packing;

                }
                else {
                    $scope.selectedSalesOrder.productMasterID = null;
                    $scope.selectedSalesOrder.productId = $scope.selectedSalesOrder.id;
                }
                $scope.selectedSalesOrder.productName = $scope.selectedSalesOrder.name;
                if ($scope.selectedSalesOrder.discount == null || $scope.selectedSalesOrder.discount == "" || $scope.selectedSalesOrder.discount == undefined) {
                    $scope.selectedSalesOrder.discount = 0;
                }
                if ($scope.selectedSalesOrder.receivedQty == null || $scope.selectedSalesOrder.receivedQty == "" || $scope.selectedSalesOrder.receivedQty == undefined) {
                    $scope.selectedSalesOrder.receivedQty = 0;
                }
                $scope.list.salesOrderEstimateItem.push($scope.selectedSalesOrder);
                $scope.selectedSalesOrder = null;
                $scope.quantityExceed = 0;
                document.getElementById("drugName1").focus();
            }
        }

        $scope.maxEditDiscountExceeded = false;
        $scope.salesOrderEstimateList.$valid = true;

    };

    $scope.keyEnter = function (event, e) {
            var result = document.getElementById("OrderQuantity").value;
            if (result == "" || result == 0) {
            } else {
                var ele = document.getElementById(e);
                if (event.which === 13) { // Enter key
                    ele.focus();
                    if (ele.nodeName != "BUTTON") {
                        ele.select();
                    }
                }
            }
    };

    $scope.maxEditqtyExceeded = false;
    $scope.maxEditOrderQtySold = false;
    $scope.validateEditQty = function (orderItem, salesOrderEstimateType) {
        // var qty = document.getElementById("txtCustomQuantity0");
        if (salesOrderEstimateType == 1) {
            $scope.maxEditOrderQtySold = false;
            if (orderItem.receivedQty > orderItem.quantity) {
                $scope.maxEditOrderQtySold = true;
                return false;
            }
        }
        else if (salesOrderEstimateType == 2) {
            $scope.maxEditqtyExceeded = false;
            if (orderItem.quantity > orderItem.productStock.stock) {
                $scope.maxEditqtyExceeded = true;
                return false;
            }
        }
    };

    $scope.checkAllSalesOrderfields = function (ind) {
            if (ind > 1) {
                if ($scope.selectedSalesOrder == undefined || $scope.selectedSalesOrder == null || $scope.selectedSalesOrder == "") {
                    var ele = document.getElementById("drugName1");
                    ele.focus();
                    return false;
                }
            }
            if (ind > 2) {
                if ($scope.selectedSalesOrder.quantity == undefined || $scope.selectedSalesOrder.quantity == null || $scope.selectedSalesOrder.quantity == "") {
                    var ele = document.getElementById("OrderQuantity");
                    ele.focus();
                    return false;
                }
            }
            if (ind > 3) {
                if ($scope.selectedSalesOrder.discount == undefined || $scope.selectedSalesOrder.discount == null || $scope.selectedSalesOrder.discount == "") {
                    var ele = document.getElementById("orderdiscount");
                    ele.focus();
                    return false;
                }
            }
    };

});

app.directive('focusBox', function () {
    return {
        "link": function (scope, element, attrs) {
            element.bind("keydown", function (event) {
                var valueLength = attrs.$$element[0].value.length;
                var value = parseFloat(attrs.$$element[0].value);
                //Enter 
                if (event.which === 13) {
                   
                    if (attrs.id === "OrderQuantity" || attrs.id === "orderdiscount") {
                        ele = document.getElementById(attrs.nextid);
                        ele.focus();
                    }
                    else {
                        ele = document.getElementById("drugName1");
                        ele.focus();
                    }
                }
            });
        }
    };
});

