app.controller('tempStockAlertCtrl', function ($scope, $rootScope, $location, $window, toastr, salesService, productStockService, cacheService, productModel, ModalService, $filter, tempBatchList, productName, close) {

    $scope.batchList1 = [];

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };


    $scope.shouldBeOpen1 = true;


    $scope.keyPress = function (e) {

        if (e.keyCode == 13) {
            $scope.closeTempPopUp("No");
        } else if (e.keyCode == 27) {
            $scope.closeTempPopUp("Yes");
        }
    };

    $scope.closeTempPopUp = function (result) {
        if (result == "No") {
            cacheService.set('pressedKey', 13);
        } else {
            cacheService.set('pressedKey', 27);
        }
        close(result, 100);
        cacheService.set('pressedKey', 27);
        $rootScope.$emit("tempStockAlertClose");
        $(".modal-backdrop").hide();
    };
    $scope.close = function (result) {
        close(result, 100); // close, but give 100ms for bootstrap to animate
        if (result != "Yes") {
            if ($scope.temp != undefined) {
                $scope.temp.drugName = null;
            }
            // $scope.temp = null;
        }
        $(".modal-backdrop").hide();
    };

    if (tempBatchList != null) {
        $scope.batchList1 = tempBatchList;
    }

    if (productName != null) {
        $scope.productName = productName;
    }



});






