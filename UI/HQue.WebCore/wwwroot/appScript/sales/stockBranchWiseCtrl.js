app.controller('stockBranchWiseCtrl', function ($scope, $filter, $rootScope, close, toastr, cacheService, ModalService, productService, salesService, productModel, productStockService, productStockModel, tempVendorPurchaseItemModel, tempProduct) {

    $scope.focusInput = true;
    $scope.minDate = new Date();

    $scope.batchList = [];
    $scope.previouspurchases = [];

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1

    };
    var productStock = productStockModel;
    var product = productModel;
    productStock.product = product;

    $scope.backgroundTask = false; //Added by Sarubala on 02-05-2018
    $scope.tempProduct = null;
    $scope.searchType1 = null;
    $scope.getProductsItem = null;
    // added by arun for close function for stockbranch on 06 jun
    $rootScope.$on("stockbranch", function () {
        $scope.close("No");
    });

    $scope.getProducts = function (val) {
        return productService.accountWiseFilterData(val).then(function (response) {
            var flags = [], output = [], l = response.data.length, i;

            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }
            console.log(output);
            return output.map(function (item) {
                $scope.getProductsItem = item;
                return item;
            });
        });

    };

    $scope.getSumOfQty = function (val) {
        var nSum = 0;
        for (i = 0; i < $scope.batchList.length; i++) {
            if ($scope.batchList[i].instanceName == val) {
                nSum += $scope.batchList[i].stock
            }

        }
        return nSum;
    }

    $scope.close = function (result) {
        console.log("inside close");
        close(result, 100); // close, but give 100ms for bootstrap to animate
        if (result != "Yes") {
            //$scope.temp.drugName = null;
            //$scope.temp = null;
        }
        $rootScope.$emit("disableEscEvent");

        $(".modal-backdrop").hide();
    };

    $scope.clear = function () {
        $scope.tempProduct = null;
        $scope.batchList.length = 0;
        $scope.previouspurchases.length = 0;
    };

    $scope.toggleBranchWiseProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapperSW' + row).slideToggle();
        $('#chip-wrapperSW' + row).show();
        if ($('#chip-btnSW' + row).text() === '+')
            $('#chip-btnSW' + row).text('-');
        else {
            $('#chip-btnSW' + row).text('+');
        }
    }

    $scope.keyEnter = function (event, nextid, currentid, value) {
        var ele = document.getElementById(nextid);
        if (event.which === 13) {
            if (currentid == "vat1") {
                if (value == null || value < 0) {
                    ele = document.getElementById(currentid);
                } else {
                    ele = document.getElementById(nextid);
                }
                ele.focus();
            }
            if (currentid == "packageSize" || currentid == "packageQty") {
                if (value == 0 || value == null) {
                    ele = document.getElementById(currentid);
                } else {
                    ele = document.getElementById(nextid);
                }
                ele.focus();
            }
            if (value != "" || value != 0) {
                if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) {
                    ele.focus();
                    if (ele.nodeName != "BUTTON")
                        ele.select();
                }
            }

        }
    };

    $scope.onTempProductSelect = function () {
        //$scope.selectedProduct.product = $scope.tempProduct;
        $.LoadingOverlay("show");
        $scope.backgroundTask = true;
        productStockService.productBranchWise($scope.tempProduct).then(function (response) {
            var tempBatchList1 = [];
            if (response.data.productStockList == null && response.data.vendorPurchaseItemList == null) {
                toastr.info("Internet connection required to fetch brach wise stock");
            }
            else {
                $scope.batchList = response.data.productStockList;
                $scope.previouspurchases = response.data.vendorPurchaseItemList;

                if ($scope.Editsale != true) {
                    $scope.Editsale = false;
                }
            }
            $scope.backgroundTask = false;
            $.LoadingOverlay("hide");

        }, function (error) {
            console.log(error);
            $scope.backgroundTask = false;
            $.LoadingOverlay("hide");
        });

    };

    if (tempProduct != null) {
        $scope.tempProduct = tempProduct;
        $scope.onTempProductSelect();
    }

});

