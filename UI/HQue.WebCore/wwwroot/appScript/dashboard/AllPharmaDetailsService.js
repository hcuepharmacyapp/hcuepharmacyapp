app.factory("AllPharmaDetailsService", function ($http) {
    return {
        "list": function (type, data, registerType) {
            return $http.post("/Dashboard/getAllPharmaDetails?type=" + type + "&registerType=" + registerType, data);
        }
    };
});