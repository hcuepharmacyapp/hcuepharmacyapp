app.controller("AllPharmaController", function ($scope, AllPharmaDetailsService, pagerServcie, $filter) {

    var sno = 0;
    $scope.errormessage = "";
    $scope.registerType = "2";
    $scope.PharmaList = [];
    $scope.Total = {};
    $scope.Total.sales = 0;
    $scope.Total.purchase = 0;
    $scope.Total.salescount = 0;
    $scope.Total.purchasecount = 0;
    $scope.Total.saleslastmonth = 0;
    $scope.Total.purchaselastmonth = 0;
    $scope.Total.saleslastmonthcount = 0;
    $scope.Total.purchaselastmonthcount = 0;




    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');


    $scope.type = 'TODAY';

    $scope.page = {};
    var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.Datetype = function (type) {
        $scope.type = type;


        if ($scope.type === "TODAY") {
            $scope.from = currentdate;
            $scope.to = currentdate;
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = currentdate;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = currentdate;
        }

        $scope.getAllPharmaDetails();
    };
    //pagination
    $scope.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
  
 
    $scope.getAllPharmaDetails = function () {
      
        $.LoadingOverlay("show");
        var data = {
            "fromDate": $scope.from,
            "toDate": $scope.to
        };
        console.log(JSON.stringify(data));
        AllPharmaDetailsService.list($scope.type, data, $scope.registerType).then(function (response) {

            $scope.PharmaList = response.data;
            var pagelength = response.data.length;

            var grid = $("#grid").kendoGrid({
                excel: {
                    fileName: "AllPharmaDetails.xlsx",
                    allPages: true
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "90px", attributes: { ftype: "sno", class: "text-left field-report" } },
                  { field: "pharmaName", title: "Name Of Pharmacy", width: "70px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "pharmaArea", title: "Area", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "city", title: "City", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },



                  { field: "salesInvoiceCount", title: "No.of Sales Bill", width: "80px", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "Total: #=sum#" },
                  { field: "salesTotalValue", title: "Total Sales Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: #=kendo.toString(sum, '0.00')#" },
                  { field: "purchaseInvoiceCount", title: "No.of Purchase Bill", width: "80px", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "Total: #=sum#" },
                  { field: "purchaseTotalValue", title: "Total Purchases Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: #=kendo.toString(sum, '0.00')#" },
                  { field: "saleslastmonthcount", title: "Previous Month No.of Sales Bill", width: "80px", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "Total: #=sum#" },
                  { field: "saleslastmonthTotalValue", title: "Previous Month Total Sales Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-   left field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: #=kendo.toString(sum, '0.00')#" },
                  { field: "purchasetotalmonthCount", title: "Previous Month No.of Purchase Bill", width: "80px", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "Total: #=sum#" },
                  { field: "purchaseTotalmonthValue", title: "Previous Month Total Purchases Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: #=kendo.toString(sum, '0.00')#" },



                  { field: "relationshipManagerName", title: "Relationship manager name", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "whetherRMStillInRolesorNot", title: "Whether RM Still in Roles or not", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "currentRM", title: "Current RM", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "owner1Name", title: "Owner 1 Name", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "owner1number", title: "Owner 1 number", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "owner1Email", title: "Owner 1 Email", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "owner2Name", title: "Owner 2 Name", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "owner2number", title: "Owner 2 number", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "owner2Email", title: "Owner 2 Email", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "trialorPaidID", title: "Trial or Paid ID", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "invoiceValue", title: "Invoice Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "number", fformat: "0.00" } },
                  { field: "billAmount", title: "Bill Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "number", fformat: "0.00" } },
                  //{ field: "invoiceDate", title: "Invoice Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(invoiceDate), 'dd/MM/yyyy') #" },
                  { field: "invoiceDate", title: "invoice Date", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "fullAmountPaid", title: "Full Amount Paid", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "reasonforfullamountnotpaid", title: "Reason for full amount not paid", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  //{ field: "lastDateLoggedin", title: "Last Date Logged in", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(lastDateLoggedin), 'dd/MM/yyyy') #" },
                  { field: "lastDateLoggedin", title: "Last Date Logged in", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "numberofInvoicesfortheday", title: "Number of Invoices for the day", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } }
                ],
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                dataSource: {
                    data: response.data,
                    aggregate: [
                         { field: "salesTotalValue", aggregate: "sum" },
                         { field: "purchaseTotalValue", aggregate: "sum" },
                         { field: "saleslastmonthTotalValue", aggregate: "sum" },
                         { field: "purchaseTotalmonthValue", aggregate: "sum" },
                         { field: "salesInvoiceCount", aggregate: "sum" },
                         { field: "purchaseInvoiceCount", aggregate: "sum" },
                         { field: "saleslastmonthcount", aggregate: "sum" },
                         { field: "purchasetotalmonthCount", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "string" },
                                "pharmaName": { "type": "string" },
                                "pharmaArea": { "type": "string" },
                                "city": { "type": "string" },
                                "salesInvoiceCount": { "type": "number" },
                                "salesTotalValue": { "type": "number" },
                                "purchaseInvoiceCount": { "type": "number" },
                                "purchaseTotalValue": { "type": "number" },
                                "saleslastmonthcount": { "type": "number" },
                                "saleslastmonthTotalValue": { "type": "number" },
                                "purchasetotalmonthCount": { "type": "number" },
                                "purchaseTotalmonthValue": { "type": "number" },
                                "relationshipManagerName": { "type": "string" },
                                "whetherRMStillInRolesorNot": { "type": "string" },
                                "currentRM": { "type": "string" },
                                "owner1Name": { "type": "string" },
                                "owner1number": { "type": "string" },
                                "owner1Email": { "type": "string" },
                                "owner2Name": { "type": "string" },
                                "owner2number": { "type": "string" },
                                "owner2Email": { "type": "string" },
                                "trialorPaidID": { "type": "number" },
                                "invoiceValue": { "type": "number" },
                                "billAmount": { "type": "number" },
                                "invoiceDate": { "type": "string" },
                                "fullAmountPaid": { "type": "string" },
                                "reasonforfullamountnotpaid": { "type": "string" },
                                "lastDateLoggedin": { "type": "string" },
                                "numberofInvoicesfortheday": { "type": "number" }
                            }
                        }
                    },
                    pageSize: pagelength
                },
                excelExport: function (e) {
                    addHeader(e);
                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd/MM/yyyy');
                            }
                            //if (row.type == "group-footer" || row.type == "footer") {
                            //    if (cell.value) {
                            //        cell.value = $.trim($('<div>').html(cell.value).text());
                            //        cell.value = cell.value.replace('Total:', '');
                            //        cell.hAlign = "right";
                            //        cell.format = "#0.00";
                            //        cell.bold = true;
                            //    }
                            //}
                            //if (row.type == "data" && (this.columns[ci].title == "Cash" || this.columns[ci].title == "Card" || this.columns[ci].title == "Credit" || this.columns[ci].title == "Net Value")) {
                            //    cell.format = "#0.00";
                            //}
                        }
                    }
                }
            }).data("kendoGrid");
            grid.dataSource.originalFilter = grid.dataSource.filter;
            grid.dataSource.filter = function () {
                if (arguments.length > 0) {
                    this.trigger("filtering", arguments);
                }
                var result = grid.dataSource.originalFilter.apply(this, arguments);
                if (arguments.length > 0) {
                    this.trigger("filtering", result);
                }
                return result;
            }
            $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                var filters = dataSource.filter();
                var allData = dataSource.data();
                var query = new kendo.data.Query(allData);
                var data = query.filter(filters).data;
            });




            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;

        headerCell = { cells: [{ value: "From: " + $filter('date')($scope.from, "dd/MM/yyyy") + "      To:  " + $filter("date")($scope.to, "dd/MM/yyyy"), bold: true, vAlign: "center", textAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);


        headerCell = { cells: [{ value: "ALL PHARMA DETAILS", bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);

    }



    $scope.getAllPharmaDetails();
    $scope.getDataBasedOnData = function () {
        $scope.type = "";



        //from is empty
        if (($scope.from == null || $scope.from == undefined) && ($scope.to != null || $scope.to != undefined)) {
            $scope.errormessage = "Invalid Date";
            return false;
        } else {
            $scope.errormessage = "";
        }
        //to is empty
        if (($scope.from != null || $scope.from != undefined) && ($scope.to == null || $scope.to == undefined)) {
            $scope.errormessage = "Invalid Date";
            return false;
        } else {
            $scope.errormessage = "";
        }
        //from and to empty
        if (($scope.from == null || $scope.from == undefined) && ($scope.to == null || $scope.to == undefined)) {
            $scope.from = currentdate;
            $scope.to = currentdate;
            $scope.errormessage = "";
        }
        ////from and to not empty
        //if (($scope.from != null || $scope.from != undefined) && ($scope.to != null || $scope.to != undefined)) {
        //    var from = $filter("date")($scope.from, "dd-MM-yyyy").split("-");
        //    var to = $filter("date")($scope.to, "dd-MM-yyyy").split("-");
        //    console.log(from);
        //    console.log(to);
        //}


        $scope.getAllPharmaDetails();
    };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];










});