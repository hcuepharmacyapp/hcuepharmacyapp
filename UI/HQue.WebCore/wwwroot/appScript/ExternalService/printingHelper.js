app.factory('printingHelper', function ($http, $filter) {
    var printerType = JSON.parse(window.localStorage.getItem("IsDotMatrix"));

    var printLogo = null;
    var printCustomFields = null;
    var pdfModel = null;
    printSettings();

    return {
        "printInvoice": function (salesId) {
            if (printerType == 18 || printerType == 19 || printerType == 20) {
                printBillPDF(salesId);
            }
            else if (printerType == 5) {
                var tempateData = window.localStorage.getItem("TemplateData");
                if (tempateData != null)
                    window.open("/InvoicePrint/printTemplate?id=" + salesId + "&templateFile=" + tempateData.StationaryFile);
                else
                    window.open("/InvoicePrint/printTemplate?id=" + salesId + "&templateFile=null");
            } else {
                var w = window.open("/Invoice/salesInvoice?id=" + salesId);
                if (printerType == 1) {
                    w.window.print();
                }
            }
        },
        //Added for department a4 print by lawrence
        "printInvoiceNew": function (SalesId, SeriesType, SeriesTypeValue, InvNo, printerType) {            
            var w = window.open("/Invoice/salesInvoiceNew?SalesId=" + SalesId + "&SeriesType=" + SeriesType + "&SeriesTypeValue=" + SeriesTypeValue + "&InvNo=" + InvNo + "&printerType=" + printerType);
            if (printerType == 1) {
                w.window.print();
            }
        },
        //Added by Sarubala for only return in sage page - print
        "printReturnInvoice": function (salesReturnId) {
            if (printerType == 5) {
                var tempateData = window.localStorage.getItem("TemplateData");
                if (tempateData != null)
                    window.open("/InvoicePrint/printReturnTemplate?id=" + salesReturnId + "&templateFile=" + tempateData.StationaryFile);
                else
                    window.open("/InvoicePrint/printReturnTemplate?id=" + salesReturnId + "&templateFile=null");
            } else {
                var w = window.open("/Invoice/ReturnWithoutSalesInvoice?id=" + salesReturnId);
                if (printerType == 1) {
                    w.window.print();
                }
            }
        },
        //End by Sarubala for only return in sage page - print
        //GEt Invoice method added by Poongodi on 27/20/2017
        "getInvoice": function (salesId) {
            var w = window.open("/Invoice/SalesViewInvoice?id=" + salesId);
        },
        "printEstimate": function (estimateId) {
            if (printerType == 5) {
                var tempateData = window.localStorage.getItem("TemplateData");
                window.open("/InvoicePrint/printTemplate?id=" + estimateId + "&templateFile=" + tempateData.StationaryFile);
            } else {
                var w = window.open("/Invoice/estimate?id=" + estimateId);
                if (printerType == 1) {
                    w.window.print();
                }
            }
        },
        "orderPrint": function (vendororderid, customOrder) {
            window.open("/Invoice/orderPrint?vendororderid=" + vendororderid + "&customOrder=" + customOrder);
        },

        "getReturnInvoiceView": function (salesReturnId) {
            var w = window.open("/Invoice/ReturnSalesInvoiceView?id=" + salesReturnId);
        },
        
        "transferPrint": function (stockTransferId, transferSettings) {
            var w = window.open("/Invoice/TransferPrint?id=" + stockTransferId + "&transferSettings=" + transferSettings);
            if (printerType == 1) {
                w.window.print();
            }
        },
        "salesReturnOnlyByReturnId": function (SalesReturnId, SeriesType, SeriesTypeValue, InvNo, printerType) {
            var w = window.open("/Invoice/salesReturnOnlyByReturnId?SalesReturnId=" + SalesReturnId + "&SeriesType=" + SeriesType + "&SeriesTypeValue=" + SeriesTypeValue + "&InvNo=" + InvNo + "&printerType=" + printerType);
            if (printerType == 1) {
                w.window.print();
            }
        },
        // Added by Gavaskar 29-10-2017
        "salesOrderEstimatePrint": function (salesOrderEstimateId) {
            window.open("/Invoice/salesOrderEstimatePrint?salesOrderEstimateId=" + salesOrderEstimateId);
        },

    };

    function printBillPDF(id) {
        $.LoadingOverlay("show");
        $http({
            method: 'GET',
            url: '/Invoice/SalesInvoiceContent?id=' + id
        }).then(function (response) {
            $.LoadingOverlay("hide");
            var model = response.data;
            var data = model.sales;
            var pageType = "";

            if (data.patient.locationType == undefined || data.patient.locationType == null || data.patient.locationType == "") {
                data.patient.locationType = 1;
            }
            
			pageType = 'portrait';
            if (printerType == 18)
                pageType = 'portrait';
            else if (printerType == 19)
                pageType = 'landscape';

            pdfMake.tableLayouts = {
                leftTopRightBottomLayout: {
                    hLineWidth: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 1 : 0;
                    },
                    vLineWidth: function (i, node, j) {
                        return (i === 0 || i === node.table.widths.length) ? 1 : 0;
                    },
                    hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                    },
                    vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                    }
                },
                leftTopRightLayout: {
                    hLineWidth: function (i, node) {
                        return (i === 0) ? 1 : 0;
                    },
                    vLineWidth: function (i, node, j) {
                        return (i === 0 || i === node.table.widths.length) ? 1 : 0;
                    },
                    hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                    },
                    vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                    }
                },
                leftRightBottomLayout: {
                    hLineWidth: function (i, node) {
                        return (i === node.table.body.length) ? 1 : 0;
                    },
                    vLineWidth: function (i, node, j) {
                        return (i === 0 || i === node.table.widths.length) ? 1 : 0;
                    },
                    hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                    },
                    vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                    }
                },
                leftRightBottomWTThirdRowLayout: {
                    hLineWidth: function (i, node) {
                        return (i === 3 || i === node.table.body.length) ? 1 : 0;
                    },
                    vLineWidth: function (i, node, j) {
                        return (i === 0 || i === node.table.widths.length) ? 1 : 0;
                    },
                    hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                    },
                    vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                    }
                },
                leftRightLayout: {
                    hLineWidth: function (i, node) {
                        return 0;
                    },
                    vLineWidth: function (i, node, j) {
                        return (i === 0 || i === node.table.widths.length) ? 1 : 0;
                    },
                    hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                    },
                    vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                    }
                },
                leftTopRightBottomWTHeaderColSpanLayout: {
                    hLineWidth: function (i, node) {
                        return (i === 0 || i === 1 || i === node.table.body.length
                            || node.table.body[i][0].colSpan != undefined
                            || node.table.body[i - 1][0].colSpan != undefined) ? 1 : 0;
                    },
                    vLineWidth: function (i, node, j) {
                        return (i === 0 || i === node.table.widths.length) ? 1 : 1;
                    },
                    hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                    },
                    vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                    }
                },
                leftTopRightBottomWTVerticalLayout: {
                    hLineWidth: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 1 : 0;
                    },
                    vLineWidth: function (i, node, j) {
                        return 1;
                    },
                    hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                    },
                    vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                    }
                },
                tableVerticalLayout: {
                    hLineWidth: function (i, node) {
                        return 0;
                    },
                    vLineWidth: function (i, node, j) {
                        return 1;
                    },
                    hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                    },
                    vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                    }
                },
                topLayout: {
                    hLineWidth: function (i, node) {
                        return (i === 0) ? 1 : 0;
                    },
                    vLineWidth: function (i, node, j) {
                        return 0;
                    },
                    hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                    },
                    vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                    }
                }
            };

            var defaultFontSize = (printerType == 19 ? 10 : printerType == 18 ? 8 : 7);
            var tableFontSize = (printerType == 19 ? 9 : printerType == 18 ? 7 : 6.5);
            var pageSize = ((printerType == 18 || printerType == 19) ? "A4" : "A5");
            var headerFontSize = (printerType == 18 || printerType == 19) ? 12 : 7.5;

            var docDefinition = JSON.stringify(pdfModel.pdfDesign);

            docDefinition = docDefinition.replace("@defaultFontSize", defaultFontSize);
            docDefinition = docDefinition.replace("@tableFontSize", tableFontSize);
            docDefinition = docDefinition.replace("@headerFontSize", headerFontSize);
            docDefinition = docDefinition.replace("@pageSize", pageSize);
            docDefinition = docDefinition.replace("@pageOrientation", pageType);
            docDefinition = docDefinition.replace("@pharmacyName", data.instance.name.toUpperCase());
            docDefinition = docDefinition.replace("@printLogo", printLogo);

            var txt = "";
            txt += getText(data.instance.address).length > 0 ? data.instance.address + ", " : "";
            txt += getText(data.instance.area).length > 0 ? data.instance.area : "";
            docDefinition = docDefinition.replace("@pharmacyAddressArea", txt.toUpperCase());

            txt = "";
            txt += getText(data.instance.city).length > 0 ? data.instance.city + ", " : "";
            txt += getText(data.instance.state).length > 0 ? data.instance.state + ", " : "";
            txt += getText(data.instance.pincode).length > 0 ? data.instance.pincode : "";
            docDefinition = docDefinition.replace("@pharmacyCityStatePincode", txt.toUpperCase());


            txt = "";
            txt = getText(data.instance.address).length > 0 ? data.instance.address + ", " : "";
            docDefinition = docDefinition.replace("@pharmacyAddress", txt.toUpperCase());

            txt = "";
            txt += getText(data.instance.area).length > 0 ? data.instance.area + ", " : "";
            txt += getText(data.instance.city).length > 0 ? data.instance.city + ", " : "";
            docDefinition = docDefinition.replace("@pharmacyAreaCity", txt.toUpperCase());

            txt = "";
            txt += getText(data.instance.state).length > 0 ? data.instance.state + ", " : "";
            txt += getText(data.instance.pincode).length > 0 ? data.instance.pincode : "";
            docDefinition = docDefinition.replace("@pharmacyStatePincode", txt.toUpperCase());


            docDefinition = docDefinition.replace("@pharmacyPhone", data.instance.phone);
            docDefinition = docDefinition.replace("@pharmacyMobile", data.instance.mobile);
            docDefinition = docDefinition.replace("@pharmacyDLNo", data.instance.drugLicenseNo);
            if (data.taxRefNo) {
                docDefinition = docDefinition.replace("@pharmacyGSTinNoOrTinNo", "GSTIN No: " + data.instance.gsTinNo);
            }
            else {
                docDefinition = docDefinition.replace("@pharmacyGSTinNoOrTinNo", "TIN   : " + data.instance.tinNo);
            }
            docDefinition = docDefinition.replace("@contactEmail", data.instance.contactEmail);
            docDefinition = docDefinition.replace("@pharmacyPanno", "Pan No:");
            docDefinition = docDefinition.replace("@doctorName", getText(data.doctorName).toUpperCase());
            docDefinition = docDefinition.replace("@customerName", getText(data.name).toUpperCase());
            docDefinition = docDefinition.replace("@customerMobile", getText(data.mobile));
            docDefinition = docDefinition.replace("@customerGSTinNo", getText(data.patient.gsTin));
            docDefinition = docDefinition.replace("@customerDLNo", getText(data.patient.drugLicenseNo));
            docDefinition = docDefinition.replace("@customerEmpIDNo", getText(data.patient.empID));
            docDefinition = docDefinition.replace("@customerEmail", getText(data.email));//added by nandhini on 9.12.17
            docDefinition = docDefinition.replace("@customerPan", getText(data.patient.pan));//added by nandhini on 9.12.17
            txt = "";
            txt += getText(data.address).length > 0 ? data.address + ", " : "";
            txt += getText(data.patient.city).length > 0 ? data.patient.city + ", " : "";
            txt += getText(data.patient.pincode).length > 0 ? data.patient.pincode : "";
            docDefinition = docDefinition.replace("@customerAddress", txt);

            docDefinition = docDefinition.replace("@billDateTime", $filter('date')(data.salesCreatedAt, "dd/MM/yyyy hh:mm a"));
            docDefinition = docDefinition.replace("@billDate", $filter('date')(data.invoiceDate, "dd/MM/yyyy"));
            docDefinition = docDefinition.replace("@billTime", $filter('date')(data.salesCreatedAt, "hh:mm a"));
            docDefinition = docDefinition.replace("@billNo", data.invoiceSeries + data.invoiceNo);
            docDefinition = docDefinition.replace("@invoiceType", data.paymentType);            

            docDefinition = JSON.parse(docDefinition);
            
            var unionTerritory = "";
            txt = JSON.stringify(pdfModel.saleDetails);
            if (data.instance.isUnionTerritory == true) {
                unionTerritory = "UTGST";                             
            }
            else {
                unionTerritory = "SGST";
            }
            txt = txt.replace("@sgstOrUtgstFlagPerc", unionTerritory);
            txt = txt.replace("@sgstOrUtgstFlagAmt", unionTerritory);
            txt = JSON.parse(txt);
            var index = -1;
            if (data.patient.locationType == 1) {
                index = txt.table.body[0].indexOf($filter("filter")(txt.table.body[0], { text: "IGST%" }, true)[0]);
                txt.table = removeField(txt.table, index);
                index = txt.table.body[0].indexOf($filter("filter")(txt.table.body[0], { text: "IGST Amt" }, true)[0]);
                txt.table = removeField(txt.table, index);
            }
            else if (data.patient.locationType == 2) {                
                index = txt.table.body[0].indexOf($filter("filter")(txt.table.body[0], { text: "CGST%" }, true)[0]);
                txt.table = removeField(txt.table, index);
                index = txt.table.body[0].indexOf($filter("filter")(txt.table.body[0], { text: "CGST Amt" }, true)[0]);
                txt.table = removeField(txt.table, index);
                index = txt.table.body[0].indexOf($filter("filter")(txt.table.body[0], { text: unionTerritory + "%" }, true)[0]);
                txt.table = removeField(txt.table, index);
                index = txt.table.body[0].indexOf($filter("filter")(txt.table.body[0], { text: unionTerritory + " Amt" }, true)[0]);
                txt.table = removeField(txt.table, index);
            }

            var totalQty = 0;
            for (var i = 0; i < data.salesItem.length; i++) {
                var item = data.salesItem[i];
                if (item.quantity <= 0) {
                    break;
                }
                hdr = txt.table.body[1];
                hdr = JSON.stringify(hdr);

                item.gstTotalValue = Number(item.gstTotalValue) || 0;
                item.productStock.packageSize = getText(item.productStock.packageSize) == "" ? 1: item.productStock.packageSize;

                hdr = hdr.replace("@sno", i + 1);
                hdr = hdr.replace("@productName", item.productStock.product.name.toUpperCase());
                hdr = hdr.replace("@productPackageSize", item.productStock.product.packageSize);
                hdr = hdr.replace("@rackNo", getText(item.productStock.product.rackNo));

                hdr = hdr.replace("@productCgstPerc", (data.patient.locationType == 1 ? getText1(item.cgst, 0) : 0).toFixed(2));
                hdr = hdr.replace("@productCgstAmt", (data.patient.locationType == 1 ? getText1(item.cgstValue, 0) : 0).toFixed(2));
                hdr = hdr.replace("@productSgstPerc", (data.patient.locationType == 1 ? getText1(item.sgst, 0) : 0).toFixed(2));
                hdr = hdr.replace("@productSgstAmt", (data.patient.locationType == 1 ? getText1(item.sgstValue, 0) : 0).toFixed(2));
                hdr = hdr.replace("@productIgstPerc", (data.patient.locationType == 2 ? getText1(item.gstTotal, 0) : 0).toFixed(2));
                hdr = hdr.replace("@productIgstAmt", (data.patient.locationType == 2 ? getText1(item.gstTotalValue, 0) : 0).toFixed(2));
                hdr = hdr.replace("@gstPerc", (data.taxRefNo ? (getGst(item) > 0 ? getGst(item) : item.productStock.gstTotal) : item.productStock.vat).toFixed(2));
                if (item != null && item.discount > 0)
                {
                    hdr = hdr.replace("@productGstAmt", item.gstTotalValue.toFixed(2));
                }
                else
                {
                    hdr = hdr.replace("@productGstAmt", ((((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.quantity)) * ((data.taxRefNo ? (getGst(item) > 0 ? getGst(item) : item.productStock.gstTotal) : item.productStock.vat)) / 100).toFixed(2));
                }
               
                if (item != null && item.discount > 0) {
                    hdr = hdr.replace("@productDiscountAmount", (item.discountAmount));
                }
                else
                {
                    hdr = hdr.replace("@productDiscountAmount", "0.00");
                }
                hdr = hdr.replace("@productDiscount", getText1(item.discount, 0).toFixed(2));
                hdr = hdr.replace("@stripMRP", ((item.mrp > 0 ? item.mrp : item.productStock.mrp) * item.productStock.packageSize).toFixed(2));
                hdr = hdr.replace("@maxMRP", (item.mrp > 0 ? item.mrp : item.productStock.mrp).toFixed(2));
                hdr = hdr.replace("@packageSize", item.productStock.packageSize);

                var rp = ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6)).toFixed(2);
                //var decimal = Math.round((rp - parseInt(rp)) * 100);
                //hdr = hdr.replace("@MRPrupee", Math.floor(rp) + "        ");
                //hdr = hdr.replace("@MRPpaise",  decimal);
                hdr = hdr.replace("@MrpPrice", rp);


                hdr = hdr.replace("@hsnCode", item.productStock.product.hsnCode);
                hdr = hdr.replace("@manufacturer", item.productStock.product.manufacturer);
                hdr = hdr.replace("@manuf", (item.productStock.product.manufacturer).substring(0, 3));

                var sch = getText(item.productStock.product.schedule);
                if (sch.toUpperCase().indexOf("NON") != -1 && sch.toUpperCase().indexOf("SCHEDULE") != -1) {
                    sch = "NH";
                }
                hdr = hdr.replace("@schedule", sch);
                
                totalQty += (item.quantity);
                hdr = hdr.replace("@batchNo", item.productStock.batchNo);
                hdr = hdr.replace("@expiry", $filter('date')(item.productStock.expireDate, "MM/yy"));
                hdr = hdr.replace("@saleQty", item.quantity);
                hdr = hdr.replace("@totalPrice", ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.quantity).toFixed(2));
               // hdr = hdr.replace("@ratewottax", ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (item.gstTotalValue / item.quantity)).toFixed(2));
                hdr = hdr.replace("@ratewottax", ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6)).toFixed(2));
                hdr = hdr.replace("@ratewottaxforwholesale", ((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.quantity).toFixed(2));
                hdr = hdr.replace("@discountforwholesale", ((((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.quantity)) * ((item.discount) / 100)).toFixed(2));
                hdr = hdr.replace("@totamtforwholesale",(((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.quantity)-((((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.quantity))*((item.discount)/100))+(item.gstTotalValue)).toFixed(2));
                hdr = hdr.replace("@stripRate", ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.productStock.packageSize).toFixed(2));
                hdr = hdr.replace("@rate", (item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice).toFixed(2));
                if (item.discount > 0) {
                    hdr = hdr.replace("@maxRate", (item.sellingPriceAfterDiscount).toFixed(2));
                }
                else {
                    hdr = hdr.replace("@maxRate", (item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice).toFixed(2));
                }
                
                if (item.discount > 0) {
                    hdr = hdr.replace("@total", (item.sellingPriceAfterDiscount * item.quantity).toFixed(2));
                }
                else {
                    hdr = hdr.replace("@total", item.total.toFixed(2));
                }
                hdr = hdr.replace("@discountItemAmount", (item.quantity * ((item.mrp > 0 ? item.mrp : item.productStock.mrp) - (item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice)).toFixed(2)));
                hdr = hdr.replace("@amountTotal", ((item.quantity * ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)))) - (item.discountAmount )+ (item.gstTotalValue)).toFixed(2));
                hdr = hdr.replace("@amountwottax", ((item.total).toFixed(2) - (item.gstTotalValue).toFixed(2)).toFixed(2));
              
                  
                if (item.discount > 0) {       //start for bombay pharmacy           
                    hdr = hdr.replace("@discoutafternet", ((item.total) - (item.discountAmount)).toFixed(2));
                }
                else {
                    hdr = hdr.replace("@discoutafternet", (item.total).toFixed(2));
                }
                //end for bombay pharmacy  
                if (item.discount > 0) {
                    hdr = hdr.replace("@withouttaxdiscoutafternet", ((item.total) - (item.discountAmount) - (item.gstTotalValue)).toFixed(2));
                }
                else {
                    hdr = hdr.replace("@withouttaxdiscoutafternet", ((item.total) - (item.gstTotalValue)).toFixed(2));
                }
                var tmpMRP = item.quantity * ((item.mrp > 0 ? item.mrp : item.productStock.mrp) * item.productStock.packageSize);
                var tmpSellPrice = item.quantity * ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.productStock.packageSize);
                var tmpDiscount = 0;
                if (tmpMRP == 0)
                    tmpDiscount = 0;
                else
                    tmpDiscount = (((tmpMRP - tmpSellPrice) / tmpMRP) * 100);
                hdr = hdr.replace("@QMSDiscount", tmpDiscount.toFixed(2));

                hdr = JSON.parse(hdr);
                txt.table.body.push(hdr);
            };

            var rtnGSTTotalValue = 0;
            var returnAmt = 0;
            var totReturnAmount = 0;
            var totReturnDiscount = 0;
            var ReturnDiscount = 0;
            if (data.salesReturnItem.length > 0) {
                var retHdr = [
                            { colSpan: txt.table.body[1].length, text: "Returns", alignment: 'left', style: 'header', fillColor: '#eeeeee' },
                            {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
                ];
                retHdr.splice(txt.table.body[1].length);
                txt.table.body.push(retHdr);

                for (var i = 0; i < data.salesReturnItem.length; i++) {
                    var item = data.salesReturnItem[i];
                    if (item.returnedQuantity > 0) {
                        hdr = txt.table.body[1];
                        hdr = JSON.stringify(hdr);
                       
                        item.total = ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.returnedQuantity);
                        item.gstTotalValue = Number(item.gstTotalValue) || 0;
                        item.productStock.packageSize = getText(item.productStock.packageSize) == "" ? 1 : item.productStock.packageSize;

                        hdr = hdr.replace("@sno", i + 1);
                        hdr = hdr.replace("@productName", item.productStock.product.name.toUpperCase());
                        hdr = hdr.replace("@rackNo", getText(item.productStock.product.rackNo)); 
                        hdr = hdr.replace("@productPackageSize", item.productStock.product.packageSize);
                        hdr = hdr.replace("@productCgstPerc", (data.patient.locationType == 1 ? getText1(item.cgst, 0) : 0).toFixed(2));
                        hdr = hdr.replace("@productCgstAmt", (data.patient.locationType == 1 ? getText1(item.cgstValue, 0) : 0).toFixed(2));
                        hdr = hdr.replace("@productSgstPerc", (data.patient.locationType == 1 ? getText1(item.sgst, 0) : 0).toFixed(2));
                        hdr = hdr.replace("@productSgstAmt", (data.patient.locationType == 1 ? getText1(item.sgstValue, 0) : 0).toFixed(2));
                        hdr = hdr.replace("@productIgstPerc", (data.patient.locationType == 2 ? getText1(item.gstTotal, 0) : 0).toFixed(2));
                        hdr = hdr.replace("@productIgstAmt", (data.patient.locationType == 2 ? getText1(item.gstTotalValue, 0) : 0).toFixed(2));
                        hdr = hdr.replace("@gstPerc", (data.taxRefNo ? (item.gstTotal > 0 ? item.gstTotal : item.productStock.gstTotal) : item.productStock.vat).toFixed(2));
                        if (item.discount > 0) {
                            hdr = hdr.replace("@productGstAmt", (getText(item.gstTotalValue) != "" ? item.gstTotalValue : 0).toFixed(2));
                        }
                        else {
                            hdr = hdr.replace("@productGstAmt", ((((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.returnedQuantity)) * ((data.taxRefNo ? (getGst(item) > 0 ? getGst(item) : item.productStock.gstTotal) : item.productStock.vat)) / 100).toFixed(2));
                        }
                       
                        if (item.discount > 0) {
                            hdr = hdr.replace("@productDiscountAmount", (item.discountAmount));
                        }
                        else {
                            hdr = hdr.replace("@productDiscountAmount", "0.00");
                        }
                        hdr = hdr.replace("@productDiscount", getText1(item.discount, 0).toFixed(2));
                        hdr = hdr.replace("@stripMRP", ((item.mrp > 0 ? item.mrp : item.productStock.mrp) * item.productStock.packageSize).toFixed(2));
                        hdr = hdr.replace("@maxMRP", (item.mrp > 0 ? item.mrp : item.productStock.mrp).toFixed(2));

                        var rp = ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6)).toFixed(2);
                        //var decimal = rp - Math.floor(rp);
                        //hdr = hdr.replace("@MRPrupee", Math.floor(rp));
                        //hdr = hdr.replace("@MRPpaise", decimal.toFixed(2));
                        hdr = hdr.replace("@MrpPrice", rp);


                        hdr = hdr.replace("@packageSize", item.productStock.packageSize);

                        hdr = hdr.replace("@hsnCode", item.productStock.product.hsnCode);
                        hdr = hdr.replace("@manufacturer", item.productStock.product.manufacturer);
                        hdr = hdr.replace("@manuf", (item.productStock.product.manufacturer).substring(0, 3));

                        var sch = getText(item.productStock.product.schedule);
                        if (sch.toUpperCase().indexOf("NON") != -1 && sch.toUpperCase().indexOf("SCHEDULE") != -1) {
                            sch = "NH";
                        }
                        hdr = hdr.replace("@schedule", sch);

                        hdr = hdr.replace("@batchNo", item.productStock.batchNo);
                        hdr = hdr.replace("@expiry", $filter('date')(item.productStock.expireDate, "MM/yy"));
                        hdr = hdr.replace("@saleQty", item.returnedQuantity);
                        hdr = hdr.replace("@totalPrice", ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.returnedQuantity).toFixed(2));

                        // hdr = hdr.replace("@ratewottax", ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (item.gstTotalValue / item.returnedQuantity)).toFixed(2));
//Commented because rate calculating with tax for above calculation.commented by nandhini
                        hdr = hdr.replace("@ratewottax", ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6)).toFixed(2));
                        hdr = hdr.replace("@ratewottaxforwholesale", ((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.returnedQuantity).toFixed(2));                       
                        hdr = hdr.replace("@discountforwholesale", ((((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.returnedQuantity)) * ((item.discount) / 100)).toFixed(2));
                        hdr = hdr.replace("@totamtforwholesale", (((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.returnedQuantity) - ((((((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)).toFixed(6))) * item.returnedQuantity)) * ((item.discount) / 100)) + (item.gstTotalValue)).toFixed(2));
                        hdr = hdr.replace("@stripRate", ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.productStock.packageSize).toFixed(2));
                        hdr = hdr.replace("@rate", (item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice).toFixed(2));
                        if (item.discount > 0) {                            
                            hdr = hdr.replace("@maxRate", (item.sellingPriceAfterDiscount).toFixed(2));
                        }
                        else {
                            hdr = hdr.replace("@maxRate", (item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice).toFixed(2));
                        }

                        if (item.discount > 0) {
                            hdr = hdr.replace("@total", (item.sellingPriceAfterDiscount * item.quantity).toFixed(2));
                        }
                        else {
                            hdr = hdr.replace("@total", item.total.toFixed(2));
                        }
                        hdr = hdr.replace("@discountItemAmount", (item.quantity * ((item.mrp > 0 ? item.mrp : item.productStock.mrp) - (item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice)).toFixed(2)));
                        hdr = hdr.replace("@amountTotal", ((item.quantity * ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) - (((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.gstTotal) / (100 + item.gstTotal)))) - (item.discountAmount) + (item.gstTotalValue)).toFixed(2));
                        hdr = hdr.replace("@amountwottax", (item.total - item.gstTotalValue).toFixed(2));
                        if (item.discount > 0) {
                            hdr = hdr.replace("@discoutafternet", ((item.total) - (item.discountAmount)).toFixed(2));
                        }
                        else {
                            hdr = hdr.replace("@discoutafternet", (item.total).toFixed(2));
                        }
                        if (item.discount > 0) {
                            hdr = hdr.replace("@withouttaxdiscoutafternet", ((item.total) - (item.discountAmount) - (item.gstTotalValue)).toFixed(2));
                        }
                        else {
                            hdr = hdr.replace("@withouttaxdiscoutafternet", ((item.total) - (item.gstTotalValue)).toFixed(2));
                        }
                        var tmpMRP = item.returnedQuantity * ((item.mrp > 0 ? item.mrp: item.productStock.mrp) * item.productStock.packageSize);
                        var tmpSellPrice = item.returnedQuantity * ((item.sellingPrice > 0 ? item.sellingPrice: item.productStock.sellingPrice) * item.productStock.packageSize);
                        var tmpDiscount = 0;
                        if (tmpMRP == 0)
                            tmpDiscount = 0;
                        else
                            tmpDiscount = (((tmpMRP - tmpSellPrice) / tmpMRP) * 100);
                        hdr = hdr.replace("@QMSDiscount", tmpDiscount.toFixed(2));

                        hdr = JSON.parse(hdr);
                        txt.table.body.push(hdr);

                        var getGstPercent = (item.gstTotal > 0 ? item.gstTotal : item.productStock.gstTotal) + 100;
                        var SalesItemDiscount = ((item.discount / 100) * item.total);
                        var ActualValue = (item.total - SalesItemDiscount);
                        rtnGSTTotalValue += ActualValue - ((ActualValue * 100) / getGstPercent);
                        returnAmt += ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.returnedQuantity);
                        totReturnAmount = totReturnAmount + ((item.sellingPrice > 0 ? item.sellingPrice : item.productStock.sellingPrice) * item.returnedQuantity);
                        totReturnDiscount = ((item.discount / 100) * item.total);
                        totReturnAmount = totReturnAmount - totReturnDiscount;
                        ReturnDiscount += totReturnDiscount;
                    }
                };
            }
            txt.table.body.splice(1, 1);
            docDefinition.content.push(txt);

            //QMS change - start
            var youHaveSaved = data.mrpTotal - data.purchasedTotal;
            if (youHaveSaved > 0) {
                if (data.discountInValue > 0) {
                    youHaveSaved += data.discountInValue;
                }
            }
            else {
                youHaveSaved = data.discountInValue;
            }
            //QMS change - end

            if (pdfModel.saleFooterDetails != undefined && pdfModel.saleFooterDetailsWithReturn != undefined) {                
                if (data.salesReturnItem.length == 0) {
                    txt = JSON.stringify(pdfModel.saleFooterDetails);
                    if (data.discount > 0) {
                        txt = txt.replace("@taxableAmount", (data.purchasedTotal - (data.cgstTotalValue + data.sgstTotalValue)).toFixed(2));
                    }
                    else
                    {
                        txt = txt.replace("@taxableAmount", (data.purchasedTotal - (data.cgstTotalValue + data.sgstTotalValue) - ((data.discountInValue).toFixed(2))).toFixed(2));
                    }
                    txt = txt.replace("@comptaxableAmount", data.purchasedTotal.toFixed(2));
                    txt = txt.replace("@rupeesInWords", ConvertDecimalNumbertoWords(data.netAmount));
                    txt = txt.replace("@totalGstAmt", (data.cgstTotalValue + data.sgstTotalValue).toFixed(2));
                    //txt = txt.replace("@totalDiscountAmt", data.discountInValue.toFixed(2));
                    if (data.discount > 0) {
                        txt = txt.replace("@itemDiscountAmt", "0.00");
                    }
                    else
                    {
                        txt = txt.replace("@itemDiscountAmt", data.discountInValue.toFixed(2));
                    }
                    txt = txt.replace("@discount", data.discount);
                    if (data.discountValue != undefined)
                    {
                        txt = txt.replace("@totalDiscountAmt", (data.discountValue != "" ? data.discountValue : 0).toFixed(2));
                        txt = txt.replace("@savedDiscountAmt", (data.discountValue != "" ? data.discountValue : 0).toFixed(2));
                    }
                    else
                    {
                        txt = txt.replace("@totalDiscountAmt", "0.00");
                    }
                    txt = txt.replace("@roundoffAmt", data.roundoffNetAmount.toFixed(2));
                    txt = txt.replace("@netAmtPayable", data.netAmount.toFixed(2));
                    txt = txt.replace("@printFooterNote", getText(data.printFooterNote));
                    txt = txt.replace("@rupees", ConvertDecimalNumbertoWords(data.netAmount));
                    txt = txt.replace("@totalSaleItem", (data.salesItem.length) - (data.salesReturnItem.length));//added by nandhini
                    txt = txt.replace("@amtAfterDisc", (data.purchasedTotal.toFixed(2)) - (data.discountInValue.toFixed(2)));
                    txt = txt.replace("@amtTotal", data.purchasedTotal.toFixed(2));
                    txt = txt.replace("@totalQty", totalQty);
                    txt = txt.replace("@custCreditBalanceAmt", (data.patient.creditBalanceAmount).toFixed(2))

                    var gstVal = "";
                    var cgstStr = "";
                    var sgstStr = "";
                    var igstStr = "";
                    if (data.patient.locationType == 1) {
                        gstVal = "CGST: " + data.cgstTotalValue.toFixed(2);
                        cgstStr = gstVal;
                        if (data.instance.isUnionTerritory == true) {
                            gstVal = gstVal + " + " + "UTGST: " + data.sgstTotalValue.toFixed(2);
                            sgstStr = "UTGST: " + data.sgstTotalValue.toFixed(2);
                        }
                        else {
                            gstVal = gstVal + " + " + "SGST: " + data.sgstTotalValue.toFixed(2);
                            sgstStr = "SGST: " + data.sgstTotalValue.toFixed(2);
                        }
                    }
                    else if (data.patient.locationType == 2) {
                        gstVal = gstVal + "IGST: " + (data.cgstTotalValue + data.sgstTotalValue).toFixed(2);
                        igstStr = gstVal;
                    }
                    if (gstVal != "") {
                        gstVal = "(" + gstVal + ")";
                    }
                    txt = txt.replace("(@totalCgstSgstIgstAmt)", gstVal);
                    if (data.discount > 0) {
                        txt = txt.replace("@totalSaleAmt", data.purchasedTotal.toFixed(2));                        
                    }
                    else {
                        txt = txt.replace("@totalSaleAmt", (data.purchasedTotal - data.discountInValue).toFixed(2));
                    }
                   
                    //start for bombay pharmacy  3/7/18
                    var finalAmnt = 0;
                    if (item != undefined || item != null || item != "") {
                        if (item.discount > 0) {
                            finalAmnt += ((data.purchasedTotal) - (data.discountInValue));
                        }
                        else {
                            finalAmnt += (data.purchasedTotal);
                        }
                    }
                   
                       txt = txt.replace("@finalAmnt", finalAmnt.toFixed(2));
                    //end for bombay pharmacy  

                       var finalamntWithoutTax = 0;
                       if (item.discount > 0) {
                           finalamntWithoutTax += ((data.purchasedTotal) - (data.discountInValue) - (data.cgstTotalValue + data.sgstTotalValue).toFixed(2));
                       }
                       else {
                           finalamntWithoutTax += (data.purchasedTotal) - (data.cgstTotalValue + data.sgstTotalValue).toFixed(2);
                       }

                       txt = txt.replace("@finalamntWithoutTax", finalamntWithoutTax.toFixed(2));
                    txt = txt.replace("@totalCgstAmt", cgstStr);
                    txt = txt.replace("@totalSgstAmt", sgstStr);
                    txt = txt.replace("@totalIgstAmt", igstStr);
                    txt = txt.replace("@QMSTotalSaleAmt", (youHaveSaved + data.purchasedTotal).toFixed(2));
                    txt = txt.replace("@QMSTotalDiscount", youHaveSaved.toFixed(2));
                }
                else {
                    var RtntotgstValue = rtnGSTTotalValue / 2;
                    var RoffAmount = parseFloat(data.roundoffNetAmount.toFixed(2)); //RoundOffCalculation(data.net, totReturnAmount);
                    txt = JSON.stringify(pdfModel.saleFooterDetailsWithReturn);
                    
                    if (data.discount > 0) {
                        txt = txt.replace("@taxableAmount", Math.abs(((data.purchasedTotal - returnAmt) - ((data.cgstTotalValue - RtntotgstValue) + (data.sgstTotalValue - RtntotgstValue)))).toFixed(2));
                    }
                    else {
                        txt = txt.replace("@taxableAmount", Math.abs((((data.purchasedTotal - returnAmt) - ((data.cgstTotalValue - RtntotgstValue) + (data.sgstTotalValue - RtntotgstValue)))) - ((data.discountInValue))).toFixed(2));
                    }
                    txt = txt.replace("@comptaxableAmount", data.purchasedTotal.toFixed(2));
                    txt = txt.replace("@rupeesInWords", ConvertDecimalNumbertoWords(data.netAmount));
                    txt = txt.replace("@totalGstAmt", ((data.cgstTotalValue - RtntotgstValue) + (data.sgstTotalValue - RtntotgstValue)).toFixed(2));
                  
                    if (data.discount > 0) {
                        txt = txt.replace("@itemDiscountAmt", data.discountInValue.toFixed(2));
                    }
                    else {
                        txt = txt.replace("@itemDiscountAmt", "0.00");
                    }

                    if (data.discount > 0) {
                        txt = txt.replace("@discount", data.discount.toFixed(2));
                    }
                    
                    if (data.discountValue != undefined && data.discountValue > 0) {
                        txt = txt.replace("@totalDiscountAmt", (data.discountInValue - ReturnDiscount).toFixed(2));
                    }
                    else {
                        txt = txt.replace("@totalDiscountAmt", "0.00");
                    }
                                        
                    txt = txt.replace("@roundoffAmt", RoffAmount.toFixed(2));
                    txt = txt.replace("@netAmtPayable", (data.netAmount).toFixed(2));
                    txt = txt.replace("@netReturnAmt", totReturnAmount.toFixed(2));
                    txt = txt.replace("@printFooterNote", getText(data.printFooterNote));
                    txt = txt.replace("@rupees", ConvertDecimalNumbertoWords(data.netAmount));
                    txt = txt.replace("@totalSaleItem", (data.salesItem.length) - (data.salesReturnItem.length));//added by nandhini
                    txt = txt.replace("@amtAfterDisc", (data.purchasedTotal.toFixed(2)) - (data.discountInValue.toFixed(2)));
                    txt = txt.replace("@amtTotal", data.purchasedTotal.toFixed(2));
                    txt = txt.replace("@totalQty", totalQty);
                    var gstVal = "";
                    var cgstStr = "";
                    var sgstStr = "";
                    var igstStr = "";
                    if (data.patient.locationType == 1) {
                        gstVal = "CGST: " + (data.cgstTotalValue - RtntotgstValue).toFixed(2);
                        cgstStr = gstVal;
                        if (data.instance.isUnionTerritory == true) {
                            gstVal = gstVal + "+" + "UTGST: " + (data.sgstTotalValue - RtntotgstValue).toFixed(2);
                            sgstStr = "UTGST: " + (data.sgstTotalValue - RtntotgstValue).toFixed(2);
                        }
                        else {
                            gstVal = gstVal + "+" + "SGST: " + (data.sgstTotalValue - RtntotgstValue).toFixed(2);
                            sgstStr = "SGST: " + (data.sgstTotalValue - RtntotgstValue).toFixed(2);
                        }
                    }
                    else if (data.patient.locationType == 2) {
                        gstVal = gstVal + "+" + "IGST: " + ((data.cgstTotalValue - RtntotgstValue) + (data.sgstTotalValue - RtntotgstValue)).toFixed(2);
                        igstStr = gstVal;
                    }
                    if (gstVal != "") {
                        gstVal = "(" + gstVal + ")";
                    }
                  txt = txt.replace("(@totalCgstSgstIgstAmt)", gstVal);

                  if (data.discount > 0) {
                      txt = txt.replace("@totalSaleAmt", data.purchasedTotal.toFixed(2));                      
                  }
                  else {
                      txt = txt.replace("@totalSaleAmt", (data.purchasedTotal - data.discountInValue).toFixed(2));
                  }
                  
                    //final amnt with tax
                  var finalAmnt = 0;
                  if (item.discount > 0) {
                      finalAmnt += ((data.purchasedTotal) - (data.discountInValue));
                  }
                  else {
                      finalAmnt += (data.purchasedTotal);
                  }

                  txt = txt.replace("@finalAmnt", finalAmnt.toFixed(2));
                    //end
                    //final amnt without tax for hingoli customer 10.7.18
                  var finalamntWithoutTax = 0;
                  if (item.discount > 0) {
                      finalamntWithoutTax += ((data.purchasedTotal) - (data.discountInValue) - (data.cgstTotalValue + data.sgstTotalValue).toFixed(2));
                  }
                  else {
                      finalamntWithoutTax += (data.purchasedTotal) - (data.cgstTotalValue + data.sgstTotalValue).toFixed(2);
                  }
                  txt = txt.replace("@finalamntWithoutTax", finalamntWithoutTax.toFixed(2));
                    //end
                    txt = txt.replace("@totalCgstAmt", cgstStr);
                    txt = txt.replace("@totalSgstAmt", sgstStr);
                    txt = txt.replace("@totalIgstAmt", igstStr);
                    txt = txt.replace("@QMSTotalSaleAmt", (youHaveSaved + data.purchasedTotal).toFixed(2));
                    txt = txt.replace("@QMSTotalDiscount", youHaveSaved.toFixed(2));
                }
                txt = JSON.parse(txt);
                docDefinition.content.push(txt);
            }
            else {
                if (data.salesReturnItem.length == 0) {
                    if (data.taxRefNo) {
                        txt = {
                            table: {
                                style: 'tableStyle',
                                widths: ['35%', '35%', '3%', '15%', '2%', '10%'],
                                body: [
                                    [{}, {}, {}, {}, {}, {}],
                                    [
                                        { text: (getPrintField("totcgstamt") ? (data.patient.locationType == 1 ? "CGST: " + data.cgstTotalValue.toFixed(2) : "") : "") },
                                        { text: (data.patient.locationType == 1 ? (getPrintField("totutgst/sgstamt") ? (data.instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + data.sgstTotalValue.toFixed(2) : "") : (getPrintField("totigstamt") ? "IGST: " + (data.cgstTotalValue + data.sgstTotalValue).toFixed(2) : "")) },
                                        {},
                                        { text: "TOTAL" },
                                        { text: ":" },
                                        { text: data.purchasedTotal.toFixed(2), alignment: 'right' }
                                    ],
                                ]
                            },
                            layout: 'leftRightBottomLayout',
                        };
                        txt.table.body.push([
                            { colSpan: 2, text: "Rs: " + ConvertDecimalNumbertoWords(data.netAmount) },
                            {},
                            {},
                            { text: "Discount" },
                            { text: ":" },
                            { text: data.discountInValue.toFixed(2), alignment: 'right' }
                        ]);
                        if (getPrintField("totgstamt") || getPrintField("pharmacyOfficialNote")) {
                            txt.table.body.push([
                                { colSpan: 2, text: (getPrintField("pharmacyOfficialNote") ? "FOR " + data.instance.name.toUpperCase() : ""), alignment: 'right', margin: [0, 0, 5, 0] },
                                {},
                                {},
                                { text: (getPrintField("totgstamt") ? "GST Amount" : "") },
                                { text: (getPrintField("totgstamt") ? ":" : "") },
                                { text: (getPrintField("totgstamt") ? (data.cgstTotalValue + data.sgstTotalValue).toFixed(2) : ""), alignment: 'right' }
                            ]);
                        }
                        txt.table.body.push([
                            {},
                            {},
                            {},
                            { text: "Round Off" },
                            { text: ":" },
                            { text: data.roundoffNetAmount.toFixed(2), alignment: 'right' }
                        ]);
                        if (getPrintField("courierfees")) {
                            txt.table.body.push([
                                {},
                                {},
                                {},
                                { text: "Courier Fees" },
                                { text: ":" },
                                {}
                            ]);
                        }
                        txt.table.body.push([
                            { text: (getPrintField("pharmacyOfficialNote") ? "Executive" : "") },
                            { text: (getPrintField("pharmacyOfficialNote") ? "Authorised Signature" : ""), alignment: 'right' },
                            {},
                            { text: "Net Amount", style: 'header' },
                            { text: ":", style: 'header' },
                            { text: data.netAmount.toFixed(2), alignment: 'right', style: 'header' }
                        ]);
                        if (data.instance.gstselect) {
                            txt.table.body.push([
                            {
                                colSpan: 6,
                                margin: [5, 0, 0, 0],
                                stack: [
                                    "Note :",
                                    {
                                        ul: [
                                            "Composite tax payer not eligible to collect taxes."
                                        ],
                                        margin: [10, 0, 0, 0]
                                    }
                                ]
                            },
                            {},
                            {},
                            {},
                            {},
                            {}
                            ]);
                        }
                        txt.table.body.push([
                            { colSpan: 6, text: getText(data.printFooterNote), alignment: 'center' },
                            {},
                            {},
                            {},
                            {},
                            {}
                        ]);
                        docDefinition.content.push(txt);
                    }
                }
                else {
                    if (data.taxRefNo) {
                        var RtntotgstValue = rtnGSTTotalValue / 2;
                        txt = {
                            table: {
                                style: 'tableStyle',
                                widths: ['35%', '32%', '3%', '18%', '2%', '10%'],
                                body: [
                                    [
                                        { text: (getPrintField("totcgstamt") ? (data.patient.locationType == 1 ? "CGST: " + (data.cgstTotalValue - RtntotgstValue).toFixed(2) : "") : "") },
                                        { text: (data.patient.locationType == 1 ? (getPrintField("totutgst/sgstamt") ? (data.instance.isUnionTerritory == true ? "UTGST: " : "SGST: ") + (data.sgstTotalValue - RtntotgstValue).toFixed(2) : "") : (getPrintField("totigstamt") ? "IGST: " + ((data.cgstTotalValue - RtntotgstValue) + (data.sgstTotalValue - RtntotgstValue)).toFixed(2) : "")) },
                                        {},
                                        { text: "TOTAL" },
                                        { text: ":" },
                                        { text: (data.purchasedTotal - returnAmt).toFixed(2), alignment: 'right' }
                                    ],
                                ]
                            },
                            layout: 'leftRightBottomLayout',
                        };

                        var RoffAmount = data.roundoffNetAmount; //RoundOffCalculation(data.net, totReturnAmount);
                        txt.table.body.push([
                            { colSpan: 2, text: "Rs: " + ConvertDecimalNumbertoWords(data.netAmount) },
                            {},
                            {},
                            { text: "Discount" },
                            { text: ":" },
                            { text: (data.discountInValue - ReturnDiscount).toFixed(2), alignment: 'right' }
                        ]);
                        if (getPrintField("totgstamt")) {
                            txt.table.body.push([
                                {},
                                {},
                                {},
                                { text: "GST Amount" },
                                { text: ":" },
                                { text: ((data.cgstTotalValue - RtntotgstValue) + (data.sgstTotalValue - RtntotgstValue)).toFixed(2), alignment: 'right' }
                            ]); 
                        }
                        txt.table.body.push([
                            { colSpan: 2, text: (getPrintField("pharmacyOfficialNote") ? "FOR " + data.instance.name.toUpperCase() : ""), alignment: 'right', margin: [0, 0, 5, 0] },
                            {},
                            {},
                            { text: "Round Off" },
                            { text: ":" },
                            { text: RoffAmount.toFixed(2), alignment: 'right' }
                        ]);
                        if (getPrintField("courierfees")) {
                            txt.table.body.push([
                                {},
                                {},
                                {},
                                { text: "Courier Fees" },
                                { text: ":" },
                                {}
                            ]);
                        }
                        txt.table.body.push([
                            {},
                            {},
                            {},
                            { text: "Return Amount", style: 'header' },
                            { text: ":", style: 'header' },
                            { text: totReturnAmount.toFixed(2), alignment: 'right', style: 'header' }
                        ]);
                        txt.table.body.push([
                            { text: (getPrintField("pharmacyOfficialNote") ? "Executive" : "") },
                            { text: (getPrintField("pharmacyOfficialNote") ? "Authorised Signature" : ""), alignment: 'right' },
                            {},
                            { text: "Payable Amount", style: 'header' },
                            { text: ":", style: 'header' },
                            { text: data.netAmount.toFixed(2), alignment: 'right', style: 'header' }
                        ]);
                        if (data.instance.gstselect) {
                            txt.table.body.push([
                            {
                                colSpan: 6,
                                margin: [5, 0, 0, 0],
                                stack: [
                                    "Note :",
                                    {
                                        ul: [
                                            "Composite tax payer not eligible to collect taxes."
                                        ],
                                        margin: [10, 0, 0, 0]
                                    }
                                ]
                            },
                            {},
                            {},
                            {},
                            {},
                            {}
                            ]);
                        }
                        txt.table.body.push([
                            { colSpan: 6, text: getText(data.printFooterNote), alignment: 'center' },
                            {},
                            {},
                            {},
                            {},
                            {}
                        ]);
                        docDefinition.content.push(txt);
                    }
                }
            }

            if (pdfModel.paymentDescription != undefined) {
                txt = JSON.stringify(pdfModel.paymentDescription);
                txt = txt.replace("@custBalanceAmt", getText(data.patient.balanceAmount).toFixed(2));
                txt = txt.replace("@custCreditBalanceAmt", getText(data.patient.creditBalanceAmount).toFixed(2))
                txt = JSON.parse(txt);
                docDefinition.content.push(txt);
            }
            if (pdfModel.termsConditions != undefined) {
               var date = new Date();
                txt = JSON.stringify(pdfModel.termsConditions);                
                txt = txt.replace("@youHaveSavedAmt", youHaveSaved.toFixed(2));
                txt = txt.replace("@pharmacyCity", getText(data.instance.city).toUpperCase());
                txt = txt.replace("@pharmacyName", data.instance.name.toUpperCase());
                txt = txt.replace("@pharmacyArea", getText(data.instance.area));
                txt = txt.replace("@billingUser", getText(data.hQueUser.name).toUpperCase());
                txt = txt.replace("@printFooterNote", getText(data.printFooterNote));
                //added by nandhini on 25.12.17
                txt = txt.replace("@printDateTime", $filter('date')(date, "dd/MM/yyyy hh:mm a"));
                txt = JSON.parse(txt);
                docDefinition.content.push(txt);
            }

            docDefinition.footer = function (currentPage, pageCount, pageSize) {    
                if (currentPage == pageCount) {
                    return { text: currentPage, alignment: "right", margin: [0, 0, 15, 0], style: "header" };
                }
                else {
                    return { text: currentPage + "      Continued...", alignment: "right", margin: [0, 0, 8, 0], style: "header" };
                }
            };
            
            pdfMake.createPdf(docDefinition).download("hq_pdf_" + data.invoiceNo,
                function () {
                    var editEle = document.getElementById('salesEditId');
                    if (editEle != null) {
                        if (editEle.value) {
                            window.location = window.location.origin + "/Sales/List";
                        } else {
                            window.location.reload();
                        }
                    }
                });

        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    };

    function removeField(table, index) {
        if (index != -1) {
            table.widths.splice(index, 1);
            for (var i = 0; i < table.body.length; i++) {
                table.body[i].splice(index, 1);
            }
        }
        return table;
    };

    function getGst(item) {
        var gst = (item.igst == 0 || item.igst == null) ? (item.cgst + item.sgst) : item.igst;
        return gst;
    }

    function getText(input) {
        if (input == null || input == undefined) {
            return "";
        }
        else {
            return input;
        }
    }

    function getText1(input, val) {
        if (input == null || input == undefined) {
            return val;
        }
        else {
            return input;
        }
    }

    //function RoundOffCalculation(Net, totReturnAmount) {
    //    var Retval = Math.round(Net - totReturnAmount);
    //    var RetroundOff = Net - totReturnAmount;

    //    return (Retval - RetroundOff);

    //}

    function printSettings() {
        if (printerType == 18 || printerType == 19 || printerType == 20) {
            $http({
                method: 'GET',
                url: '/Sales/getAutoTempstockAdd',
            }).then(function (response) {
                printLogo = response.data.printLogo;
                pdfModel = JSON.parse(response.data.printCustomFields);
                printCustomFields = pdfModel.printCustomFields;
            },
            function (error) {
                console.log(error);
            });
        }
    }

    function getPrintField(field) {
        var fld = null;
        if (printCustomFields != null && printCustomFields != undefined && printCustomFields != "") {
            for (var i = 0; i < printCustomFields.length; i++) {
                if (printCustomFields[i].field == field) {
                    fld = printCustomFields[i];
                    break;
                }
            }
        }
        if (fld != null && fld != undefined) {
            return fld.enable;
        }
        else {
            return 0;
        }
    }

    function getFieldWidth(field) {
        var fld = null;
        if (printCustomFields != null && printCustomFields != undefined && printCustomFields != "") {
            for (var i = 0; i < printCustomFields.length; i++) {
                if (printCustomFields[i].field == field) {
                    fld = printCustomFields[i];
                    break;
                }
            }
        }
        if (fld != null && fld != undefined) {
            return fld.width;
        }
        else {
            return "auto";
        }
    }

    function getFieldTitle(field) {
        var fld = null;
        if (printCustomFields != null && printCustomFields != undefined && printCustomFields != "") {
            for (var i = 0; i < printCustomFields.length; i++) {
                if (printCustomFields[i].field == field) {
                    fld = printCustomFields[i];
                    break;
                }
            }
        }
        if (fld != null && fld != undefined) {
            return fld.title;
        }
        else {
            return "";
        }
    }

    function ConvertDecimalNumbertoWords(decimalNumber) {
        var value = decimalNumber.toFixed(2);
        var numbers = value.toString().split('.');
        var number = numbers[0];
        var number2 = numbers[1];
        var words = ConvertNumbertoWords(number);
        if (number2 > 0) {
            words = words.replace("And ", "");
            words = words + " And ";
            words = words + ConvertNumbertoWords(number2) + " Paisa";
        }
        return words;
    }

    function ConvertNumbertoWords(number) {
        if (number == 0) return "Zero";
        if (number < 0) return "Minus " + ConvertNumbertoWords(Math.abs(number));
        var words = "";
        if (parseInt(number / 100000) > 0) {
            if (parseInt(number / 100000) == 1)
                words += ConvertNumbertoWords(number / 100000) + " Lakh ";
            else
                words += ConvertNumbertoWords(number / 100000) + " Lakhs ";
            number %= 100000;
        }
        if (parseInt(number / 1000) > 0) {
            words += ConvertNumbertoWords(number / 1000) + " Thousand ";
            number %= 1000;
        }
        if (parseInt(number / 100) > 0) {
            words += ConvertNumbertoWords(number / 100) + " Hundred ";
            number %= 100;
        }

        if (parseInt(number) <= 0)
            return words;

        if (words != "") words += "And ";
        var unitsMap = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"];
        var tensMap = ["Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];
        if (parseInt(number) < 20)
            words += unitsMap[parseInt(number)];
        else {
            words += tensMap[parseInt(number / 10)];
            if (parseInt(number % 10) > 0) words += " " + unitsMap[parseInt(number % 10)];
        }
        return words;
    };

});
