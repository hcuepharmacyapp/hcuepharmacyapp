app.controller('branchListCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'branchServcie', 'productStockModel', 'productModel', '$filter', function ($scope, $rootScope, $http, $interval, $q, branchServcie, productStockModel, productModel, $filter) {

    $scope.data = [];
    $scope.instanceList = [];
    $scope.instanceModel = [];
    $scope.firstTimeLoad = true;
    
    $scope.$on('LoginBranch', function (event, obj, multiSelect) {
        $scope.branchid = obj.id;
        $scope.multiSelect = multiSelect;
    });

    $scope.$on('LoadMultiBranch', function (event, data) {
        $scope.instanceList = data;
        SetBranchAreaDetails();
    });

    $scope.$on('ResetMultiBranch', function (event) {
        $scope.instanceList = JSON.parse(JSON.stringify($scope.instanceList));
        $scope.instanceModel = [];
        $scope.instanceModel = $filter("filter")($scope.instanceList, { id: $scope.branchid }, true);
        $rootScope.$broadcast('instanceList', $scope.instanceModel);
    });
 
    $scope.selectedInstance = function () {
        $scope.selectedInstance;
    }
    $scope.instance = function () {
        branchServcie.selectInstance().then(function (response) {
            $scope.instanceList = response.data;
            SetBranchAreaDetails();

        }, function () {

        });
    }
    $scope.ddlchange = function (obj) {
        if (obj != undefined)
            $rootScope.$broadcast('branchname', obj.id, obj);
        else
            $rootScope.$broadcast('branchname', null, null);
    }
    $scope.ddlmultichange = function (obj) {
        //console.log(obj);
        $rootScope.$broadcast('instanceList', obj);
    }
    $scope.instance();

    $scope.applyInstanceList = function () {
        if ($scope.instanceList.length == 0) {
            $scope.instance();
        }
    };

    function SetBranchAreaDetails() {
        //$scope.instanceModel = $scope.instanceList;
        for (var i = 0; i < $scope.instanceList.length; i++) {
            if ($scope.instanceList[i].area != undefined && $scope.instanceList[i].area.length > 30) {
                $scope.instanceList[i].name = ($scope.instanceList[i].name + ", "+(($scope.instanceList[i].area == undefined) ? "" : $scope.instanceList[i].area)).substring(0, 60);
            }
            else if ($scope.instanceList[i].area == undefined) {
                $scope.instanceList[i].name = $scope.instanceList[i].name;
            }
            else {
                $scope.instanceList[i].name = ($scope.instanceList[i].name + ", " + $scope.instanceList[i].area).substring(0, 30);
            }
            if ($scope.firstTimeLoad && $scope.instanceList[i].id == $scope.branchid) {
                $scope.instanceList[i].checked = true;
                $scope.firstTimeLoad = false;
                $scope.instanceModel = [];
                $scope.instanceModel.push($scope.instanceList[i]);
                $rootScope.$broadcast('instanceList', $scope.instanceModel);
            }
        } // Added by Lawrence for InstanceName + Area
        //$scope.selectedInstance.id = $scope.instanceList[0].id;
        if ($scope.branchid == undefined) {
            $scope.instance();
        }
        $scope.selectedInstance.id = $scope.branchid;        

        //$rootScope.$broadcast('branchname', $scope.selectedInstance.id);
        $rootScope.$broadcast('branchname', $scope.selectedInstance.id, $scope.selectedInstance);
    };
   
}]);