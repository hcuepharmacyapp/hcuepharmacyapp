var app = angular.module("showConfirmMsgPopupModule", ['commonApp', 'angularModalService']);

angular.element(document).ready(function () {
    angular.bootstrap(document.getElementById("divShowConfirmMsg"), ["showConfirmMsgPopupModule"]);
});

app.controller("showConfirmMsgPopCtrl", function (ModalService) {
    
    function ShowConfirmMsgWindow() {
        var data = {
            msgTitle: "Backup your Data!",
            msg: "Data backup is pending",
            msg1: "Please connect internet and backup your data to avoid data loss.",
            showOk: true,
            showOkButtonText: "OK, I will connect",
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: "",
            showExclamation: true,
            popupWidth: "popup-width-500"
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                return result;
            });
        });
    }

    $("#divShowConfirmMsg").click(function () {
        ShowConfirmMsgWindow();
    });

});