app.factory('cacheService', function () {

    var dataStore = {};

    function set(key, value) {
        dataStore[key] = value;
    };

    function get(key) {
        return dataStore[key];
    };

    function remove(key) {
        delete dataStore[key];
    }

    return {
        set: set,
        get: get,
        remove : remove
    }
});