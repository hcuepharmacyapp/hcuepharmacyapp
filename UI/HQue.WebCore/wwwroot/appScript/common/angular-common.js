var appCommon = angular.module('commonApp', []);
appCommon.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
              
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});
appCommon.directive('myDate', function (dateFilter, $parse) {
    return {
        "restrict": 'EAC',
        "require": '?ngModel',
        "link": function (scope, element, attrs, ngModel, ctrl) {
            ngModel.$parsers.push(function (viewValue) {
                return dateFilter(viewValue, 'yyyy-MM-dd');
            });
        }
    };
});
appCommon.directive('validNumber', function () {
    return {
        "require": '?ngModel',
        "link": function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                var clean = "";
                if (val == ".") {
                    clean = val.replace(/./g, '');
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                } else {
                    clean = val.replace(/[^0-9.]+/g, '');
                    var negativeCheck = clean.split('-');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                }
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
appCommon.directive('validDecimalNumber', function () {
    return {
        "require": '?ngModel',
        "link": function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                var clean = "";
                if (val == ".") {
                    clean = val.replace(/./g, '');
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                } else {
                    clean = val.replace(/[^0-9.]+/g, '');
                    var negativeCheck = clean.split('-');
                    var decimalCheck = clean.split('.');
                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 5);
                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                    }
                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                }
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
appCommon.directive('convertToNumber', function () {
    return {
        "require": 'ngModel',
        "link": function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function (val) {
                return '' + val;
            });
        }
    };
});
appCommon.directive("formatDate", function () {
    return {
        "require": 'ngModel',
        "link": function (scope, elem, attr, modelCtrl) {
            modelCtrl.$formatters.push(function (modelValue) {
                return new Date(modelValue);
            });
        }
    };
});
appCommon.directive('numbersOnly', function () {
    return {
        "require": '?ngModel',
        "link": function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
appCommon.directive('noSpecialChar', function () {
    return {
        "require": 'ngModel',
        "restrict": 'A',
        "link": function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined)
                    return '';
                cleanInputValue = inputValue.replace(/[^\w\s\.,\'\"\%\(\)]/gi, '');
                if (cleanInputValue != inputValue) {
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                }
                return cleanInputValue;
            });
        }
    };
});
appCommon.directive('drugSpecialChar', function () {
    return {
        "require": 'ngModel',
        "restrict": 'A',
        "link": function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined)
                    return '';
                cleanInputValue = inputValue.replace(/[^\w\s\,\/\(\)\.]/gi, '');
                if (cleanInputValue != inputValue) {
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                }
                return cleanInputValue;
            });
        }
    };
});
appCommon.directive('drugnameSpecialChar', function () {
    return {
        "require": 'ngModel',
        "restrict": 'A',
        "link": function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined)
                    return '';
                cleanInputValue = inputValue.replace(/[^\w\s\,\/\!\@\#\$\%\^\&\*\?\>\<\[\]\(\)\.\-]/gi, '');
                if (cleanInputValue != inputValue) {
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                }
                return cleanInputValue;
            });
        }
    };
});
appCommon.directive('shortcut', function () {
    return {
        "restrict": 'E',
        "replace": true,
        "scope": true,
        "link": function postLink(scope, iElement, iAttrs) {
            jQuery(document).on('keydown', function (e) {
                scope.$apply(scope.keydown(e));
            });
        }
    };
});
appCommon.directive('focusMe', function ($timeout, $parse) {
    return {
        "link": function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            element.bind('blur', function () {
                scope.$apply(model.assign(scope, false));
            });
        }
    };
});
// added by arun on 21.03.2017
appCommon.directive('awLimitLength', function () {
    return {
        "restrict": "A",
        "require": 'ngModel',
        "link": function (scope, element, attrs, ngModel) {
            attrs.$set("ngTrim", "false");
            var limitLength = parseInt(attrs.awLimitLength, 10);// console.log(attrs);
            scope.$watch(attrs.ngModel, function (newValue) {
                if (ngModel.$viewValue) {
                    if (ngModel.$viewValue.length > limitLength) {
                        ngModel.$setViewValue(ngModel.$viewValue.substring(0, limitLength));
                        ngModel.$render();
                    }
                }
            });
        }
    };
});
appCommon.directive('missedorderFocus', function ($timeout) {
    return function (scope, element, attrs) {
        scope.$watch(attrs.missedorderFocus, function (value) {
            if (value) {
                $timeout(function () {
                    element.focus();
                }, 700);
            }
        });
    };
});
appCommon.directive('numbersOnly', function () {
    return {
        "require": 'ngModel',
        "link": function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});
appCommon.directive('popupFocus', function ($timeout) {
    return function (scope, element, attrs) {
        scope.$watch(attrs.popupFocus, function (value) {
            if (value) {
                $timeout(function () {
                    element.focus();
                }, 700);
            }
        });
    };
});
// Ended by arun on 21.03.2017 for numbers only,limit length directive
//Added by arun on 23.03 for popup focus by punithavel
appCommon.directive('productFocus', function ($timeout) {
    return function (scope, element, attrs) {
        scope.$watch(attrs.productFocus, function (value) {
            if (value) {
                $timeout(function () {
                    element.focus();
                }, 700);
            }
        });
    };
});
appCommon.filter('INR', function () {
    return function (input) {
        if (!isNaN(input)) {
            var result = input.toString().split('.');
            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '') {
                lastThree = ',' + lastThree;
            }
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            var res = output.replace("-,", "-");
            output = res;
            if (result.length > 1) {
                output += "." + result[1].substr(0, 2);
            } else {
                output += ".00";
            }
            return output;
        }
    }
});
appCommon.filter('round', function () {
    return function (input) {
        return Math.round(input);
    };
});
appCommon.directive('numberfilter', ['$filter', function ($filter) {
    return {
        "require": 'ngModel',
        "link": function (elem, $scope, attrs, ngModel) {
            ngModel.$formatters.push(function (val) {
                if (val == "" || val == null) {
                    return "";
                } else {
                    var number = parseFloat(val);
                    return number.toFixed(2);
                }
            });
            ngModel.$parsers.push(function (val) {
                return val.replace(/[\$,]/, '')
            });
        }
    }
}]);
appCommon.filter('strLimit', ['$filter', function ($filter) {
    return function (input, limit) {
        if (!input) return;
        if (input.length <= limit) {
            return input;
        }

        return $filter('limitTo')(input, limit) + '...';
    };
}]);
appCommon.filter('float', function () {
    return function (input) {
        return parseFloat(input);
    };

});
appCommon.directive('alphaNumeric', function () {
    return {
        "require": "ngModel",
        "link": function (scope, element, attrs, modelCtrl) {
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) {
                    return '';
                }
                var transformedInput = inputValue.replace(/[^0-9a-zA-Z]/g, "");
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }

    };
});

appCommon.directive("checkPercent", [function () {
    return {
        restrict: "A",
        link: function (scope, elem, attrs) {
            elem.bind('keydown', function (e) {
                var value = elem[0].value;
                var char = e.key;
                if (parseFloat(value + char) > 100 && e.keyCode != 8) {
                    e.preventDefault();
                    return false;
                }
                return true;
            });
        }
    }
}]);