app.config(function ($routeProvider) {
    $routeProvider.when('/patientsearch', {
        "controller": "patientSearchCtrl",
        "templateUrl": '/patient/search'
    });
    $routeProvider.when('/patientcreate', {
        "controller": "patientCreateCtrl",
        "templateUrl": '/patient/create'
    });
    $routeProvider.when('/pos', {
        "controller": "salesCreateCtrl",
        "templateUrl": '/sales/sale'
    });
    $routeProvider.when('/salesreturn', {
        "controller": "salesReturnCtrl",
        "templateUrl": '/salesReturn/index'
    });
    $routeProvider.when('/saleslist', {
        "controller": "salesSearchCtrl",
        "templateUrl": '/sales/listData'
    });

  
    $routeProvider.otherwise({ redirectTo: '/pos' });


    //$locationProvider.html5Mode({
    //    enabled: true,
    //    requireBase: false
    //});
});