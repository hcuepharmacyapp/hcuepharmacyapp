app.controller('stockTransferIndentCtrl', function ($scope, toastr, stockTransferService, $filter) {
    
    $scope.list = [];
    $scope.orderId = "";
    $scope.type = 1;
    $scope.search = {};
    $scope.toBranchs = [];
  

    $scope.minDate = new Date();
    var d = new Date();

    $scope.popup1 = {
        opened: false
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.search.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.search.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');

    //Date validation added start
    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
    };

    $scope.checkFromDate = function () {
        var dt = $("#orderFromDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if ($scope.search.fromDate != undefined && $scope.search.toDate != null) {
                    $scope.checkToDate1($scope.search.fromDate, $scope.search.toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var dt = $("#orderToDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1($scope.search.fromDate, $scope.search.toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }


    $scope.focusNextField = function (nextid) {
        if (nextid == "transferNo") {
            ele = document.getElementById("transferNo");
        }
        else if (nextid == "branch") {
            ele = document.getElementById("branch");
        }
        else if (nextid == "searchTransfer") {
            ele = document.getElementById("searchTransfer");
        }
        ele.focus();
    };
    //Date validation added End

    /* Get ToBranch*/

    //$scope.getToInstance = function () {
    //    stockTransferService.GetInstancesByAccountId().then(function (response) {
            
    //        $scope.toBranchs = response.data;
    //        $scope.search.toInstance = $scope.toBranchs[0];

    //    });
    //};
    $scope.loadActiveIndentList = function () {
        $.LoadingOverlay("show");
       
        //$scope.getToInstance();
        //if ($scope.search.toInstance.id != "" && $scope.search.toInstance.id != undefined)
        //{
        //    $scope.search.toInstance.id = $scope.search.toInstance.id;
        //}
        //else {
        //    $scope.search.toInstance.id = null;
        //}
        stockTransferService.loadActiveIndentList($scope.orderId, $scope.type, $scope.search.fromDate, $scope.search.toDate).then(function (response) {
            $scope.list = response.data;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.loadActiveIndentList();

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.getTransferredCount = function (order) {
        var count = 0;
        order.partialCount = 0;
        order.pendingCount = 0;
        order.balanceStock = 0;
        for (var i = 0; i < order.vendorOrderItem.length; i++) {
            if (order.vendorOrderItem[i].quantity == order.vendorOrderItem[i].receivedQty)
                count += 1;
            else if (order.vendorOrderItem[i].receivedQty == 0)
                order.pendingCount += 1;
            else
                order.partialCount += 1;
            if (order.vendorOrderItem[i].quantity != order.vendorOrderItem[i].receivedQty)
                order.balanceStock += order.vendorOrderItem[i].productStock.stock;

            if (((order.vendorOrderItem[i].quantity - (order.vendorOrderItem[i].receivedQty != undefined ? order.vendorOrderItem[i].receivedQty : 0))) > order.vendorOrderItem[i].productStock.stock)
                order.showMsg = true;
        }
        order.transferredCount = count;
        return count;
    };

    $scope.btnTransfer = function (order) {
        if (order.balanceStock > 0) {
            if (!confirm("Only stock available items will be transferred. Do you wish to continue?"))
                return;
            $.LoadingOverlay("show");
            
            stockTransferService.loadActiveIndentList(order.id, $scope.type, $scope.search.fromDate, $scope.search.toDate).then(function (response) {
                
                window.localStorage.setItem("IndentStockTransfer", JSON.stringify(response.data[0]));
                window.location = "/StockTransfer/Index";

                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        else {
            toastr.info("It seems stock not available for all indent items.");
        }
    };

    $scope.clearSearch = function () {
      
        $scope.search.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.search.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.loadActiveIndentList();
    }



 

});