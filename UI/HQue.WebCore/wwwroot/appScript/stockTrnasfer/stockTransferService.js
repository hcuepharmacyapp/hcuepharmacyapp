app.factory('stockTransferService', function ($http) {

    var transferInstanceUrl = "/StockTransferData/TransferInstance";
    var GetInstancesByAccountId = "/StockTransferData/GetInstancesByAccountId";
    var stockProductListUrl = '/ProductStockData/InStockProductListForInstance?productName=';
    var batchListUrl = '/ProductStockData/InStockProduct/batch?productId=';
    var transferStockUrl = '/StockTransferData/Index';
    var acceptListStockUrl = '/StockTransferData/AcceptList?forAccept=';
    var getTransfereNoUrl = '/StockTransferData/GetTransfereNo';
    var accetpRejectkUrl = '/StockTransferData/AccetpReject?id=';
    var filterInstanceUrl = '/StockTransferData/GetFilterInstance?forAccept=';
    var acceptedTransferListUrl = '/StockTransferData/getAcceptedTransferList?TransferNo=';

    return {
        getTransferBranch: function () {
            return $http.get(transferInstanceUrl);
        },
        "GetInstancesByAccountId": function () {
            return $http.get(GetInstancesByAccountId);
        },
        getProductList: function (filter, instanceId,showExpDateQty) {
            return $http.get(stockProductListUrl + filter + '&instanceId=' + instanceId + '&showExpDateQty=' + showExpDateQty);
        },
        getBatchList: function (productId) {
            return $http.get(batchListUrl + productId);
        },
        stockTransfer: function (data) {
            return $http.post(transferStockUrl, data);
        },
        getTransfereNo: function () {
            return $http.get(getTransfereNoUrl);
        },
        getStockTransfer: function (data, forAccept) {
            return $http.post(acceptListStockUrl + forAccept, data);
        },
        getFilterInstance: function (forAccept) {
            return $http.get(filterInstanceUrl + forAccept);
        },
        accetpReject: function (transferId, status, stocktransferItem) {
            return $http.post(accetpRejectkUrl + transferId + "&status=" + status, stocktransferItem);
        },
        getSettings: function () {
            return $http.get('/StockTransfer/getTransferSetting');
        },
        updateSettings: function (data) {
            return $http.post('/StockTransfer/UpdateTransferSetting', data);
        },
        //MRP Implementation by Settu
        getEnableSelling: function () {
            return $http.get('/vendorPurchase/getEnableSelling');
        },
        //Added by Settu for indent transfer with stock reducing
        //loadActiveIndentList: function (orderId, type) {
        //    return $http.get('/StockTransferData/GetActiveIndentList?OrderId=' + orderId + '&Type=' + type);
        //},

        loadActiveIndentList: function (orderId, type, fromDate, toDate) {
            return $http.get('/StockTransferData/GetActiveIndentList?OrderId=' + orderId + '&Type=' + type + '&fromDate=' + fromDate + '&toDate=' + toDate);
        },
        getAcceptedTransferList: function (transferNo) {
            return $http.get('/StockTransferData/getAcceptedTransferList?TransferNo=' + transferNo);
        },
    }
});