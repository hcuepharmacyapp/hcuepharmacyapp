app.controller('stockTransferSearchCtrl', function ($scope, stockTransferService, pagerServcie,printingHelper, $filter, toastr) {

    $scope.init = function (value) {
        $scope.forAccept = value;
        getFilterInstance();
        $scope.transferSearch();
    }
  
    $scope.filterBranchList = [];
    $scope.search = {};
    $scope.list = [];
    $scope.IsEdit = false;
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination
    $scope.urlFrom = window.location;
    
    $scope.chkDisableBatch = function(value)
    {
        if($scope.urlFrom.pathname == '/StockTransfer/Accept')
        {
            if(value == '')
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        return true;
    }

    $scope.minDate = new Date();
    var d = new Date();

    $scope.popup1 = {
        opened: false
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.search.transferDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.search.transferToDate = $filter('date')(new Date(), 'yyyy-MM-dd');

    //Date validation added start
    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
    };

    $scope.checkFromDate = function () {
        var dt = $("#transferDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if ($scope.search.transferDate != undefined && $scope.search.transferToDate != null) {
                    $scope.checkToDate1($scope.search.transferDate, $scope.search.transferToDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var dt = $("#transferToDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1($scope.search.transferDate, $scope.search.transferToDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }
    //Date validation added End

    $scope.focusNextField = function (nextid) {
        if (nextid == "transferNo")
        {
            ele = document.getElementById("transferNo");
        }
        else if (nextid == "branch")
        {
            ele = document.getElementById("branch");
        }
        else if (nextid == "searchTransfer") {
            ele = document.getElementById("searchTransfer");
        }
        ele.focus();
    };

    var ProductStock = (function () {
        function ProductStock(purchasePrice, vat, gstTotal, quantity, packageQty) {
            this.purchasePrice = purchasePrice;
            this.vat = vat;
            this.gstTotal = gstTotal;
            this.quantity = quantity;
            this.packageQty = packageQty;
        }
        ProductStock.prototype.taxValue = function () {

            var GSTEnabled = angular.element(document.getElementById("GSTEnabled")).val();
            for (var i = 0; i < $scope.list.length; i++)
            {
                if (GSTEnabled == "True" && $scope.list[i].taxRefNo == 1) {
                    if (this.purchasePrice === undefined || this.gstTotal === undefined)
                        return 0;
                    return this.purchasePrice * (this.gstTotal / (100 + this.gstTotal));
                }
                else {
                    if (this.purchasePrice === undefined || this.vat === undefined)
                        return 0;
                    return this.purchasePrice * (this.vat / (100 + this.vat));
                }
            }
           

           
        };
        ProductStock.prototype.grossValue = function () {
            if (this.purchasePrice === undefined || this.quantity === undefined)
                return 0;
            //return this.quantity * this.purchasePrice;
            if ($scope.transferSettings)
                {
                    return this.quantity * this.purchasePrice;
                }
                else
                {
                    return this.packageQty * this.purchasePrice;
                }
        };
        ProductStock.prototype.totalVatValue = function () {
            if (this.quantity === undefined)
                return 0;
            //return this.quantity * this.taxValue();
           return ($scope.transferSettings == true ? this.quantity : this.packageQty) * this.taxValue();
        };
        ProductStock.prototype.netValue = function () {
            return this.grossValue();
        };
        return ProductStock;
    }());


    $scope.CalculateQty = (function (stockTransferItem)
    {
        stockTransferItem.quantity1 = stockTransferItem.packageSize * stockTransferItem.noofStrip;
    });

    $scope.getStockTransferNetValue = function (obj) {
        var netValue = 0;
        obj.stockTransferItems.forEach(function (value) {
            value.packageQty = (value.quantity / value.productStock.packageSize);
            value.productStockObj = $scope.getProductStock(value);
            if ($scope.transferSettings) {
                if (value.productStockId != "") {
                    netValue += value.productStockObj.netValue();
                }
                else {
                    netValue += (value.productStockObj.netValue() / value.productStock.packageSize);
                }
            }
            else {
                if (value.productStockId != "")
                {
                    netValue += value.productStockObj.netValue() * value.packageQty;
                }
                else
                {
                    netValue += (value.productStockObj.netValue() / value.productStock.packageSize) * value.packageQty;
                }
                                
            }
           
        });
        return netValue;
    };

    $scope.getProductStock = function (obj) {
        return new ProductStock(obj.productStock.purchasePrice, obj.productStock.vat, obj.productStock.gstTotal, obj.quantity, obj.productStock.packageSize);
    };

    function pageSearch() {
        $.LoadingOverlay("show");
        stockTransferService.getStockTransfer($scope.search, $scope.forAccept).then(function (response) {
            $scope.list = response.data.list;
          
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
   
    $scope.transferSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;  
        $scope.search.toInstanceId = null;
        if ($scope.selectedBranch != undefined) {
            $scope.search.toInstanceId = $scope.selectedBranch.id;
        }
        stockTransferService.getStockTransfer($scope.search, $scope.forAccept).then(function (response) {
            $scope.list = response.data.list;            
            console.log($scope.list);
            pagerServcie.init(response.data.noOfRows, pageSearch);
            ele = document.getElementById("transferDate");
            ele.focus();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
   
    function getFilterInstance() {
        stockTransferService.getFilterInstance($scope.forAccept).then(function (response) {
            $scope.filterBranchList = response.data;
            for (var i = 0; i < $scope.filterBranchList.length; i++) {
                if ($scope.filterBranchList[i].area != undefined && $scope.filterBranchList[i].area.length > 30) {
                    $scope.filterBranchList[i].name = ($scope.filterBranchList[i].name + ($scope.filterBranchList[i].area == undefined) ? "" : $scope.filterBranchList[i].area).substring(0, 30);
                }
                else if ($scope.filterBranchList[i].area == undefined) {
                    $scope.filterBranchList[i].name = $scope.filterBranchList[i].name;
                }
                else {
                    $scope.filterBranchList[i].name = ($scope.filterBranchList[i].name + ", " + $scope.filterBranchList[i].area).substring(0, 30);
                }
            } //Added by Lawrence for InstanceName + Area
        }, function () {
        });
    }

    $scope.clearSearch = function () {
        $scope.search.name = "";
        $scope.search.mobile = "";
        $scope.search.select = "";
        $scope.search.invoiceNo = "";
        $scope.search.transferNo = "";
        $scope.search.transferDate = "";
        $scope.search.transferToDate = "";
        $scope.selectedBranch = "";
        $scope.search.values = "";
        $scope.transferSearch();
    }

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.selectedItemCount = function (stockTransferItem) {
        $scope.emptyCount = 0;
        angular.forEach(stockTransferItem, function (data) {
            if (data.productStock.batchNo == '') {
                $scope.emptyCount = 1;
            }
            else if (data.packageSize == '' || data.packageSize <=0) {
                $scope.emptyCount = 2;
            }
        });
        return $scope.emptyCount;
    }

    $scope.chkITItem = function (stockTransferItem) {
        var blnITitem = false;

        for (var i = 0; i < stockTransferItem.length; i++) {
            if (stockTransferItem[i].productStockId == "") {
                blnITitem = true;
            }
        }
        return blnITitem;
    }
    $scope.disableAccept = false;
    $scope.disableRejct = false;
    //toInstanceId Added for offline transfer issue , bala
    $scope.accept = function (transferId, stockTransferItem, toInstanceId) {
        $scope.disableAccept = true;
        stockTransferItem[0].toInstanceId = toInstanceId;
        var ncnt = $scope.selectedItemCount(stockTransferItem);
        if (ncnt == 0)
        {
            $.LoadingOverlay("show");
            stockTransferService.accetpReject(transferId, 3, stockTransferItem)
                        .success(function (accept) {
                            if (accept[0].saveFailed)
                            {
                                $scope.disableAccept = true;
                                toastr.info(accept[0].errorLog.errorMessage);
                            }
                            else
                            {
                                // alert('Accept successful...!');
                                toastr.success('Accept Saved Successfully');
                                $scope.transferSearch();
                            }
                            //if (accept[0].isAccept == true)
                            //{
                            //    angular.element(document.getElementById('Reject')).disabled = true;
                            //}
                        })
                        .finally(function() {
                            $scope.disableAccept = false;
                            $.LoadingOverlay("hide");
                        });
                        
        }
        else if (ncnt==2)
        {
            $scope.disableAccept = false;
            toastr.info("Please enter valid Units / Strip");
            $.LoadingOverlay("hide");
        }
        else  {
            $scope.disableAccept = false;
            toastr.info("Please enter valid Batch No");
            $.LoadingOverlay("hide");
        }
    };

    //ToInstanceId Parameter added for Offline Sync by Poongodi on 23/05/2017
    $scope.reject = function (transferId, stockTransferItem, toInstanceId) {
        $scope.disableRejct = true;
        stockTransferItem[0].toInstanceId = toInstanceId;
        if ($scope.chkITItem(stockTransferItem) == false)
        {
            if (confirm("Sure to reject?"))
            {
                $.LoadingOverlay("show");
                stockTransferService.accetpReject(transferId, 2, stockTransferItem)
                    .then(function (accept) {
                        if (accept.data[0].saveFailed)
                        {
                            $scope.disableRejct = true;
                            toastr.info(accept.data[0].errorLog.errorMessage);                            
                        }
                        else
                        {
                            alert('Reject successful...!');
                            $scope.transferSearch();
                        }
                        //if (accept.data[0].isAccept == false) {
                        //    angular.element(document.getElementById('Accept')).disabled = true;
                        //}
                    })
                    .finally(function () {
                        $scope.disableRejct = false;
                        $.LoadingOverlay("hide");
                    });
            }
            else
            {
                $scope.disableRejct = false;
                $.LoadingOverlay("hide");
            }
                
        }
        else {
            $scope.disableRejct = false;
            toastr.info("You can't reject indent items");
        }
            
    };
    $scope.transferSettings = true;
    $scope.getSettings = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        stockTransferService.getSettings().then(function (response) {
            $scope.transferSettings = response.data.transferQtyType;

            $scope.isProcessing = false;
            $.LoadingOverlay("hide");

        }, function (error) {
            console.log(error);
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        })
    };

    $scope.getSettings();

    //MRP Implementation by Settu
    $scope.getEnableSelling = function () {
        stockTransferService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.getEnableSelling();

    // Added by San 16-06-2017
    $scope.printBill = function (stockTransfer, transferSettings) {
        printingHelper.transferPrint(stockTransfer.id, transferSettings)
    }
});