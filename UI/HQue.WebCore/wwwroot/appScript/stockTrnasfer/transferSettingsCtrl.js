app.controller('transferSettingsCtrl', function ($scope, $location, $window, toastr, stockTransferService, transferSettingsModel, $filter) {

    var transferSettings = transferSettingsModel;
    $scope.transferSettings = transferSettings;
    

    if ($scope.transferSettings.transferQtyType == true || $scope.transferSettings.transferQtyType == null)
    {
        $scope.transferQtyType = "1";
    }
    else
    {
        $scope.transferQtyType = "0";
    }
        
   

    $scope.saveSettings = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        $scope.transferSettings.ShowExpiredQty = $scope.ShowExpDateQty;
       
     

        $scope.transferSettings.transferQtyType = false;
        if ($scope.transferQtyType == "1") {
            $scope.transferSettings.transferQtyType = true;
        }


        stockTransferService.updateSettings($scope.transferSettings).then(function (response) {

            toastr.success('Settings saved successfully');
            $scope.getSettings();
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });

        
    };

    $scope.getSettings = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;

        stockTransferService.getSettings().then(function (response) {
            if (response.data.productBatchDisplayType == undefined)
            {
                $scope.transferSettings.productBatchDisplayType = "2";
            }
            else
            {
                $scope.transferSettings.productBatchDisplayType = response.data.productBatchDisplayType;
            }
           

            if (response.data.transferQtyType == true || response.data.transferQtyType == undefined)
                $scope.transferQtyType = "1";
            else
                $scope.transferQtyType = "0";

            if (response.data.showExpiredQty == null || response.data.showExpiredQty == 0)
                $scope.ShowExpDateQty = "0";
            else
                $scope.ShowExpDateQty = "1";

            $scope.isProcessing = false;
            $.LoadingOverlay("hide");

        }, function (error) {
            console.log(error);
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        })
    };

  
});


//app.directive('numbersOnly', function () {
//    return {
//        require: '?ngModel',
//        link: function (scope, element, attrs, ngModelCtrl) {
//            if (!ngModelCtrl) {
//                return;
//            }

//            ngModelCtrl.$parsers.push(function (val) {
//                if (angular.isUndefined(val)) {
//                    var val = '';
//                }
//                var clean = val.replace(/[^0-9]+/g, '');
//                if (val !== clean) {
//                    ngModelCtrl.$setViewValue(clean);
//                    ngModelCtrl.$render();
//                }
//                return clean;
//            });

//            element.bind('keypress', function (event) {
//                if (event.keyCode === 32) {
//                    event.preventDefault();
//                }
//            });
//        }
//    };
//});

//app.directive('noSpecialChar', function () {
//    return {
//        require: 'ngModel',
//        restrict: 'A',
//        link: function (scope, element, attrs, modelCtrl) {
//            modelCtrl.$parsers.push(function (inputValue) {
//                if (inputValue === undefined)
//                    return '';
//                cleanInputValue = inputValue.replace("#", '');
//                cleanInputValue = cleanInputValue.replace("^", '');
//                cleanInputValue = cleanInputValue.replace("&", '');
//                if (cleanInputValue != inputValue) {
//                    modelCtrl.$setViewValue(cleanInputValue);
//                    modelCtrl.$render();
//                }
//                return cleanInputValue;
//            });
//        }
//    };
//});