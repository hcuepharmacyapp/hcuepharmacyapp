app.controller('multiInstanceReportsSearchCtrl', function ($scope, toastr, multiInstanceReportsService, registrationModel) {

    $scope.registration = registrationModel;

    $scope.fromDate = '';
    $scope.toDate = '';
    $scope.type = "Day";
    $scope.allInstances = true;
    $scope.instanceIds = [];

    $scope.reportSearchRequest = {};

    $scope.list = [];

    //pagination
    //$scope.search.page = pagerServcie.page;
    //$scope.pages = pagerServcie.pages;
    //$scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        if ($scope.reportSearchRequest.fromDate == "")
        {
            $scope.reportSearchRequest.fromDate = new Date();
        }
        if ($scope.reportSearchRequest.toDate == "") {
            $scope.reportSearchRequest.toDate = new Date();
        }
        multiInstanceReportsService.list("day", true, "", $scope.reportSearchRequest.fromDate, $scope.reportSearchRequest.toDate).then(function (response) {
            $scope.list = response.data.list;
            
        }, function () { });
    }

    //$scope.bdoLoad = function () {
    //    multiInstanceReportsService.bdoData().then(function (response) {
    //        $scope.bdoList = response.data;
    //    }, function () { toastr.error('Error Occured', 'Error'); });
    //}

    //if (registration.search=="")
    //$scope.bdoLoad();

    //$scope.assignBDO = function (index) {
    //    //console.log(index);
    //    var instance = $scope.list[index].user.instance;
    //    console.log(instance);
    //    multiInstanceReportsService.assignBDO(instance.id, instance.bdoId).then(function (response) {
    //        $scope.list[index].user.instance.bdoId = response.data;
    //        toastr.success("BDO assigned successfully.");
    //    }, function () { toastr.error('Error Occured', 'Error'); });
    //}

    //$scope.registertypevalue = function (item, bool) {
    //    console.log(bool);
    //    item.isCreditEdit = bool;
    //    //$scope.sales.credit = null;
    //    if (bool == false)
    //    {
    //        $scope.registration.search = false;
    //    }
    //    else
    //    {
    //        $scope.registration.search = true;
    //    }
    //}



    $scope.SearchList = function () {
        //$scope.search.page.pageNo = 1;
        
        if ($scope.reportSearchRequest.fromDate == "" || $scope.reportSearchRequest.fromDate == undefined) {
            var date = new Date();
            $scope.reportSearchRequest.fromDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
        }
        if ($scope.reportSearchRequest.toDate == "" || $scope.reportSearchRequest.toDate == undefined) {
            $scope.reportSearchRequest.toDate = $scope.reportSearchRequest.fromDate;
        }

        $scope.reportSearchRequest.rptType = "day";
        $scope.reportSearchRequest.allInstances = true;
        $scope.reportSearchRequest.instanceIds = [];

        //multiInstanceReportsService.list("day", true, "", $scope.reportSearchRequest.fromDate, $scope.reportSearchRequest.toDate).then(function (response) {
        multiInstanceReportsService.list($scope.reportSearchRequest).then(function (response) {
            $scope.list = response.data;
            //var bdo = {"id"=null, "name"=null};

            //for (var i = 0; i < $scope.list.length; i++){
            //    $scope.list[i].user.instance.bdo = bdo;
            //    $scope.list[i].user.instance.bdo.id = $scope.list[i].user.instance.bdoId;
            //}
            // pagerServcie.init(response.data.noOfRows, pageSearch);
        }, function () { });
    }

    $scope.init = function (searchType) {
        //debugger;
        //console.log("inside init");
        //console.log(searchType);
        if (searchType != "0") {
            $scope.registration.search = searchType;
        }
        else
            $scope.registration.search = "2";
        $scope.SearchList();

    }


});