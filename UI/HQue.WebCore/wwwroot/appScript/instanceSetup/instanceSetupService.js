app.factory('instanceSetupService', function ($http) {
    return {
        list: function (searchData) {
            return $http.post('/instanceSetup/listData', searchData);
        },

        create: function (instance) {
            return $http.post('/instanceSetup/index', instance);
        },

        getbyid: function (id) {
            return $http.post('/instanceSetup/GetById', id);
        },

        editInstance: function (id) {
            return $http.post('/instanceSetup/GetById?instanceId=' + id);
        },

        updateInstance: function (instance) {
            return $http.post('/instanceSetup/UpdateInstance', instance);
        }
    }
});