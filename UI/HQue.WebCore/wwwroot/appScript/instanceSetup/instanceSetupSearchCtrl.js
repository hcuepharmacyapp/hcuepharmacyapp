app.controller('instanceSetupSearchCtrl', function ($scope, instanceSetupService, instanceModel, pagerServcie) {

    var instance = instanceModel;

    $scope.search = instance;
    //$scope.search.pager = pagerService.page;

    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        $.LoadingOverlay("show");
        instanceSetupService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.pharmacySearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        instanceSetupService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () { 
            $.LoadingOverlay("hide");
        });
    }

    $scope.pharmacySearch();

    $scope.getTotalLicense = function (getLicense) {
        var total = 0;
        total = getLicense;
        return total;
    }

    //$scope.getTotalLicense();

});