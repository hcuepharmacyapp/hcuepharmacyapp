app.factory('purchaseReportService', function ($http) {
    return {
        list: function (type, data, filter, instanceid) {
            return $http.post('/vatReport/listData?type=' + type + '&filter=' + filter + '&sInstanceId=' + instanceid, data);
        },
        returnList: function (type, data, instanceid) {
            return $http.post('/vatReport/returnListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        salesList: function (type, data, instanceid) {
            return $http.post('/vatReport/salesListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },

        salesReturnList: function (type, data, instanceid) {
            return $http.post('/vatReport/salesReturnListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        listCst: function (type, data, instanceid) {
            return $http.post('/vatReport/listDataCst?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        returnListCst: function (type, data, instanceid) {
            return $http.post('/vatReport/returnListDataCst?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        localSalesList: function (type, data, instanceid) {
            return $http.post('/vatReport/LocalSalesListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        salesConsolidatedVATList: function (type, data, instanceid) {
            return $http.post('/vatReport/SalesConsolidatedVATListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        
        stockInventoryListData: function () {
            return $http.post('/vatReport/StockInventoryListData');
           
        },
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },
    }
});
