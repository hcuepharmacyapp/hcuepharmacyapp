app.controller('purchaseReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'purchaseReportService', 'vendorPurchaseModel', 'vendorPurchaseItemModel', '$filter', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, purchaseReportService, vendorPurchaseModel, vendorPurchaseItemModel, $filter) {
    var sno = 0;
    var vendorPurchaseItem = vendorPurchaseItemModel;
    var vendorPurchase = vendorPurchaseModel;
    $scope.search = vendorPurchaseItem;
    $scope.search.vendorPurchase = vendorPurchase;
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');


    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {

        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    
    $scope.type = '';
   

    // chng-2

    $scope.init = function (getfilter) {
        $scope.reportFilter = getfilter;
        $.LoadingOverlay("show");        

        purchaseReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.buyReport($scope.reportFilter);

        }, function () {
            $.LoadingOverlay("hide");
        });
        purchaseReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.buyReport = function (filter) {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        purchaseReportService.list($scope.type, data, filter, $scope.branchid).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                purchaseReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                    $scope.pdfHeader.tinNo = "";
                else
                    $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "Annx-01-1st_Schedule_Goods_Purchased_from_Registered_Dealer_v1.0.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Annx-01-1st_Schedule_Goods_Purchased_from_Registered_Dealer_v1.0.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                height:350,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                 //{ field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "90px", attributes: { ftype: "sno", class: " text-left field-report" } },
                 { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "90px", attributes: { ftype: "sno", class: " text-left field-report" } },
                     { field: "vendor.name", title: "Name of Seller", width: "120px", attributes: { class: "text-left field-report" } },
                          { field: "vendor.tinNo", title: "Seller Taxpayer Identification Number", width: "110px", attributes: { class: "text-left field-report" } },
                          { field: "productStock.product.commodityCode", title: "Commodity Code", width: "100px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },
                  { field: "buyPurchase.invoiceNo", title: "Invoice No", width: "80px", attributes: { class: "text-left field-report" } },
                  { field: "buyPurchase.invoiceDate", title: "Invoice Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" },footerTemplate: "Total", template: "#= kendo.toString(kendo.parseDate(buyPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  
                  { field: "poValue", title: "Purchase Value(Rs.)", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "productStock.vat", title: "Rate of Tax(VAT %)", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                   //{ field: "productStock.cst", title: "Rate of Tax(CST %)", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                  { field: "vatValue", title: "Tax Paid(Rs.)", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                     { field: "category", title: "Category", width: "90px", attributes: { class: "text-left field-report" } },
                         
                  //{ field: "reportTotal", title: "Total Value(Rs.)", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  
              ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                       
                        { field: "poValue", aggregate: "sum" },
                        { field: "vatValue", aggregate: "sum" },
                        { field: "reportTotal", aggregate: "sum" }

                    ],
                    schema: {
                        model: {
                            fields: {
                                 "sNo": { type: "number" },
                                "buyPurchase.vendor.tinNo": { type: "string" },
                                "buyPurchase.invoiceDate": { type: "date" },
                                "buyPurchase.invoiceNo": { type: "number" },
                                "buyPurchase.vendor.name": { type: "string" },
                                "productStock.product.commodityCode": { type: "string" },
                                "poValue": { type: "number" },
                                "productStock.vat": { type: "number" },
                                "productStock.cst": { type: "number" },
                                "vatValue": { type: "number" }
                               //, "reportTotal": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd/MM/yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                    e.preventDefault();
                    promises[0].resolve(e.workbook);
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.buyReport($scope.reportFilter);
    }


    // chng-3
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: "Annexture-7", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "TIN No: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }
    function addSearch(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Tax Period To*:  " + $scope.to, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: "Tax Period From*: " + $scope.from, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "Dealer TIN No*: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);


    }
    //

    //
    var promises = [
    $.Deferred(),
    $.Deferred()
    ];

    $("#btnXLSExport").click(function (e) {

        $("#basicDetails").data("kendoGrid").saveAsExcel();
        $("#grid").data("kendoGrid").saveAsExcel();
        // wait for both exports to finish
        $.when.apply(null, promises)
         .then(function (gridWorkbook, ordersWorkbook) {

             // create a new workbook using the sheets of the products and orders workbooks
             var sheets = [
                gridWorkbook.sheets[0],
               ordersWorkbook.sheets[0]

             ];

             sheets[0].title = "Local Purchase";
             sheets[1].title = "Basic Details";

             var workbook = new kendo.ooxml.Workbook({
                 sheets: sheets
             });

             // save the new workbook,b
             kendo.saveAs({
                 dataURI: workbook.toDataURL(),
                 fileName: "Local Purchase.xlsx"
             })
         });
    });

    $("#basicDetails").kendoGrid({
        dataSource: {

            data: $scope.instance,
            pageSize: 20,
            serverPaging: true
        },
        height: 550,
        pageable: true,
        columns: [
            { field: "LocalPurchase" }
        ],
        excelExport: function (e) {
            e.preventDefault();
            addSearch(e);
            promises[1].resolve(e.workbook);
        }
    });
}]);
