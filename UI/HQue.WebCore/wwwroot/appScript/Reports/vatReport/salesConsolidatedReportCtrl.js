app.controller('salesConsolidatedVATCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'purchaseReportService', 'salesModel', 'salesItemModel', '$filter', 'salesReportService', function ($scope, $rootScope, $http, $interval, $q, purchaseReportService, salesModel, salesItemModel, $filter, salesReportService) {

    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    $scope.list = [];
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.type = "";
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };


    $scope.type = '';



    $scope.init = function () {
        $.LoadingOverlay("show");
        purchaseReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            
            //$scope.salesReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        purchaseReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }


    $scope.salesReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined //$scope.instance.id;
        }
     
        if ($scope.search.select1 == undefined || $scope.search.select1 == "") {
            $scope.search.select1 = "";
        }

        if ($scope.search.cashType == undefined || $scope.search.cashType == "") {
            $scope.search.cashType = "";
        }
        
        salesReportService.consolidatedsalesReportList($scope.type, data, $scope.branchid, $scope.search.select1, $scope.search.cashType)
            .then(function (response) {
                $scope.data = response.data;
                if ($scope.from != undefined || $scope.from != null) {
                    $scope.dayDiff($scope.from);
                }
                $scope.type = "";
                $scope.list = response.data;
                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                } else {
                    purchaseReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "") {
                        $scope.pdfHeader.drugLicenseNo = "";
                    } else {
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");
                    }
                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "") {
                        $scope.pdfHeader.gsTinNo = "";
                    } else {
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");
                    } if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "") {
                        $scope.pdfHeader.fullAddress = "";
                    } else {
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");
                    }
                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>" + "Consolidated Sales Report - Tax Period From " + $filter("date")($scope.from, "dd/MM/yyyy") + " To " + $filter("date")($scope.to, "dd/MM/yyyy");
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Consolidated Sales Report";
                }
                $("#grid").kendoGrid({
                    excel: {
                        fileName: "ConsolidatedSalesReport.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000],
                        landscape: true,
                        allPages: true,
                        fileName: "ConsolidatedSalesReport.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    //height: 350,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                           { field: "instance.name", title: "Branch", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "invoiceNo", title: "Bill No.", width: "70px", type: "string", attributes: { class: "text-left field-report" } },

                      { field: "invoiceDate", title: "Bill Date", width: "90px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #"},



                      { field: "name", title: "Customer", width: "200px", type: "string", attributes: { class: "text-left field-highlight" }, footerTemplate: "Total" },


                      { field: "vatOther_Gross", title: "Gross", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                      { field: "vatOther", title: "GST Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                      
                      { field: "consolidatedRoundOff", title: "RoundOff", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },


                      { field: "finalValue", title: "Invoice Amount", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                            { field: "finalValue", aggregate: "sum" },
                           
                             { field: "vatOther_Gross", aggregate: "sum" },
                              { field: "vatOther", aggregate: "sum" },
                            { field: "consolidatedRoundOff", aggregate: "sum" },
                            { field: "surCharge", aggregate: "sum" }
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "instance.name": { type: "string" },
                                    "invoiceNo": { type: "string" },
                                    "invoiceDate": { type: "date" },                                  
                                    "consolidatedRoundOff": { type: "number" },                                    
                                    "finalValue": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {
                        addHeader(e);
                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];
                                cell.fontFamily = "verdana";
                                cell.width = "200px";
                                cell.vAlign = "center";
                                if (row.type == "header") {
                                    cell.fontSize = "10";
                                    cell.width = "200";
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd/MM/yyyy');

                                }
                                if (row.type == "data")
                                { cell.fontSize = "8"; }

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.fontSize = "8"
                                        cell.bold = true;
                                        cell.format = "#0.00";

                                    }

                                }
                            }
                        }

                        //e.preventDefault();
                        //promises[0].resolve(e.workbook);
                    },
                });
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
            });
    }
    //$scope.filter = function (type) {
    //    $scope.type = type;
    //    $scope.salesReport();
    //}

    //add by nandhini on 22.6.17

    $scope.filter = function (type) {
        $scope.type = type;

        if ($scope.type === "TODAY") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        $scope.salesReport();
    }
    //end

    $scope.header = "Elixir Softlab Solutions";
    function addHeader(e) {


        var clen = e.workbook.sheets[0].rows[0].cells.length;
        var headerCell = "";
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);

            headerCell = { cells: [{ value: "Tax Period From " + $filter("date")($scope.from, "dd/MM/yyyy") + " To " + $filter("date")($scope.to, "dd/MM/yyyy"), format: "dd/MM/yyyy", type: "date", bold: true, vAlign: "center", textAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);

            headerCell = { cells: [{ value: "Consolidated Sales Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 15, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Consolidated Sales Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }




    }


    function addSearch(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Tax Period To*:  " + $scope.to, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: "Tax Period From*: " + $scope.from, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "Dealer GSTIN No:*: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);


    }
    //

    //
    var promises = [
    $.Deferred(),
    $.Deferred()
    ];

    //$("#btnXLSExport").click(function (e) {

    //    $("#basicDetails").data("kendoGrid").saveAsExcel();
    //    $("#grid").data("kendoGrid").saveAsExcel();

    //    $.when.apply(null, promises)
    //     .then(function (gridWorkbook, ordersWorkbook) {


    //         var sheets = [
    //            gridWorkbook.sheets[0],


    //         ];

    //         sheets[0].title = "Sales Consolidated VAT";


    //         var workbook = new kendo.ooxml.Workbook({
    //             sheets: sheets
    //         });


    //         kendo.saveAs({
    //             dataURI: workbook.toDataURL(),
    //             fileName: "Sales Consolidated VAT.xlsx"
    //         })
    //     });
    //});

    $("#basicDetails").kendoGrid({
        dataSource: {

            data: $scope.instance,
            pageSize: 20,
            serverPaging: true
        },
        height: 550,
        pageable: true,
        columns: [
            { field: "ExcemptedSales" }
        ],
        //excelExport: function (e) {
        //    e.preventDefault();
        //    addSearch(e);
        //    promises[1].resolve(e.workbook);
        //}
    });
    $scope.btnTXTExport = function () {
        var data = {
            "fromDate": $scope.from,
            "toDate": $scope.to
        };
       
        $scope.getSettings();
        window.open('/SalesReport/consolidatedsalesReportPrint?from=' + $scope.from + '&to=' + $scope.to + '&printType=' + $scope.pageBreakSettings + '&invoiceSeries=' + $scope.search.select1 + '&cashType=' + $scope.search.cashType + '&sInstanceId=' + $scope.branchid);
    };  
    $scope.getSettings = function () {
        $scope.pageBreakSettings = JSON.parse(window.localStorage.getItem("PageBreakSettings"));
        if ($scope.pageBreakSettings == null) {
            $scope.pageBreakSettings = "1";
        }
    };

    //$scope.ChangeInvoiceSeriesDropdown = function (seriestype) {
       
    //    $scope.InvoiceSeriesType = seriestype;
    //    $scope.search.select1 = "";

    //    if ($scope.InvoiceSeriesType == '1') {
    //        $scope.search.select1 = "";
    //        $scope.InvoiceSeriesItems = [];
    //    }
    //    if ($scope.InvoiceSeriesType == '2') {
    //        getInvoiceSeriesItems();
    //    }
    //    if ($scope.InvoiceSeriesType == '3') {
    //        $scope.search.select1 = "MAN";
    //        $scope.InvoiceSeriesItems = [];
    //    }
    //    if ($scope.InvoiceSeriesType == '4') {
    //        $scope.search.select1 = "MAN";
    //    }
    //}

    $scope.InvoiceSeriesItems = [];
   
    function getInvoiceSeriesItems() {

        salesReportService.getInvoiceSeriesItems(2).then(function (response) {

            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }

        }, function () {

        });
    }
    getInvoiceSeriesItems();

    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date') (frmDate, 'MM/dd/yyyy');
        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);