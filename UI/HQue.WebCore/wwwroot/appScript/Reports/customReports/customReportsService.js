app.factory('customReportsService', function ($http) {
    return {
        getFilters: function (type) {
            return $http.post('/customReports/getFilters?type=' + type);
        },
        saveCustomReport: function (data) {
            return $http.post('/customReports/saveCustomReport', data);
        },
        getReportList: function (data) {
            return $http.get('/customReports/getReportList', data);
        },
        getReportListNew: function (data) {
            return $http.get('/customReports/getReportListNew', data);
        },
        runReport: function (data) {
            return $http.post('/customReports/runReport', data);
        },
        getDevReportList: function (data) {
            return $http.get('/customReports/getDevReportList', data);
         },

        saveDevCustomReport: function (data) {
            return $http.post('/customReports/SaveDevCustomReport', data);
        },
        runDevReport: function (data) {
            return $http.post('/customReports/runDevReport', data);
        },
        runDevFilter: function (data) {
            return $http.post('/customReports/runDevFilter', data);
        },
        getSupplierName: function (name, mobile) {
            return $http.get('/supplierReport/getSupplierName?supplier=' + name + '&mobile=' + mobile);
        },
        allStockProductList: function (filter, instanceid) {
            return $http.get('/ProductStockData/AllStockProductList?productName=' + filter + '&instanceid=' + instanceid);
        },
        editQryData: function (id) {
            return $http.post('/customReports/editReportData?id=' + id);
        },
        updateDevCustomReport: function (data) {
            return $http.post('/customReports/updateDevCustomReport', data);
        },
    }
});
    