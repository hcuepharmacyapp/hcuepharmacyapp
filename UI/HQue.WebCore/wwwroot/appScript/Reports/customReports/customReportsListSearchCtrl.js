app.controller('customReportsListSearchCtrl', ['$scope', 'customReportsService', 'toastr', function ($scope, customReportsService, toastr) {

    $scope.init = function () {

        $scope.loadReports();
    }

    $scope.customReportsNew = [];
  

    $scope.loadReports = function(objs, context) {
        
        $.LoadingOverlay("show");
        
        customReportsService.getDevReportList(0).then(function (response) {
            $scope.customReportsNew = response.data;
            $.LoadingOverlay("hide");
        }, function (response) {
            $.LoadingOverlay("hide");
            toastr.error(response.data, 'error');
        });

    }

    $scope.checkReportTypeExists = function (type) {

        for (var i = 0; i < $scope.customReportsNew.length; i++) {
            if ($scope.customReportsNew[i].reportType == type)
                return false;
        }

        return true;
    }


    $scope.loadReportType = function (type) {
        var reports = [];
        for (var i = 0; i < $scope.customReportsNew.length; i++) {
            if ($scope.customReportsNew[i].reportType == type)
                reports.push($scope.customReportsNew[i]);
        }

        return reports;
    }

}]);
