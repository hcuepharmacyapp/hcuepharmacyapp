app.controller('customReportsSearchCtrl', ['$scope', 'customReportsService', 'toastr', function ($scope, customReportsService, toastr) {

    $scope.init = function () {

        $scope.loadReports();
    }

    $scope.customReports = [];
  

    $scope.loadReports = function(objs, context) {
        
        $.LoadingOverlay("show");
        
        customReportsService.getReportList(0).then(function (response) {
            $scope.customReports = response.data;
            $.LoadingOverlay("hide");
        }, function (response) {
            $.LoadingOverlay("hide");
            toastr.error(response.data, 'error');
        });

    }


    $scope.checkReportTypeExists = function (type) {

        for (var i = 0; i < $scope.customReports.length; i++) {
            if ($scope.customReports[i].reportType == type)
                return false;
        }

        return true;
    }


    $scope.loadReportType = function (type) {
        var reports = [];
        for (var i = 0; i < $scope.customReports.length; i++) {
            if ($scope.customReports[i].reportType == type)
                reports.push($scope.customReports[i]);
        }

        return reports;
    }

}]);
