app.controller('renderDevReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$location', 'customReportsService', 'salesReportService', '$filter', function ($scope, $rootScope, uiGridConstants, $http, $interval, $location, customReportsService, salesReportService, $filter) {

    $scope.id = "";
    $scope.reportName = "Render Report";
    $scope.dataLen = null;
    $scope.IsInvoice = false;

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.init = function (id) {

        $scope.id = id;
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.renderFilter();
            $scope.renderReport();
        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }


    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.checkFromDate = function () {
        var frmDate = $scope.getFilter.from;
        var toDate = $scope.getFilter.to;

        var dt = $("#fromDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if (toDate != undefined && toDate != null) {
                    $scope.checkToDate1(frmDate, toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var frmDate = $scope.getFilter.from;
        var toDate = $scope.getFilter.to;

        var dt = $("#toDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1(frmDate, toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    function reportRequest() {
        this.id = "";
    }

    $scope.renderFilter = function () {

        $scope.type = 'Month';
        var data = new reportRequest();
        data.id = $scope.id;
    
        customReportsService.runDevFilter(data).then(function (response) {
            var resp = response.data;
            $scope.reportName = resp.reportName;

            if ($scope.reportName == "Purchase Free and Discount")
                $scope.IsInvoice = true;

            $scope.getFilter = JSON.parse(resp.data);
            $scope.getFilter.from = $scope.from;
            $scope.getFilter.to = $scope.to;
            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.popup1 = {
                opened: false
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.popup2 = {
                opened: false
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
            $scope.format = $scope.formats[2];
            $scope.altInputFormats = ['M!/d!/yyyy'];
            $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

            $scope.data = [];
            $scope.type = "TODAY";
            $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                if (col.filters[0].term) {
                    return 'header-filtered';
                } else {
                    return '';
                }
            };



        }, function () {
        });
    }
    //$scope.$apply(function () {

    //});

    $scope.getFilter = [];
    $scope.renderReport = function () {
        $.LoadingOverlay("show");

        $scope.type = 'Month';
        var data = new reportRequest();
        data.id = $scope.id;
        data.productId = null;
        data.vendor = null;

        data.fromDate = $scope.from;
        data.toDate = $scope.to;

        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);

        //setFileName();
        customReportsService.runDevReport(data).then(function (response) {

            var resp = response.data;
            $scope.dataLen = response.data;
            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {  //$scope.currentInstance == typeof (Object)
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>" + $scope.reportName;
            }

            for (var i = 0; i < resp.columns.length > 0; i++) {

                if (resp.columns[i].title.match(/ Date/g) == ' Date' || resp.columns[i].title.match(/ date/g) == ' date' || resp.columns[i].title.match(/Expiry/g) == 'Expiry') {
                    //resp.columns[i]["template"] = "#= kendo.toString(kendo.parseDate(" + resp.columns[i].field + "), 'dd/MM/yyyy') #";

                    resp.columns[i]["template"] = "#=" + resp.columns[i].field + " == null ? '-' : kendo.toString(kendo.parseDate(" + resp.columns[i].field + "), 'dd/MM/yyyy') #";

                    resp.columns[i].type = "date";
                    resp.columns[i]['ftype'] = "date";
                }

                //if (resp.columns[i].title.match(/ Time/g) == ' Time' || resp.columns[i].title.match(/ time/g) == ' time') {
                //    resp.columns[i]["template"] = "#= kendo.toString(kendo.parseDate(" + resp.columns[i].field + ", 'yyyy-MM-dd'), 't') #";
                //    resp.columns[i].type = "date";
                //    resp.columns[i]['ftype'] = "date";
                //}
                if (resp.columns[i].title.match(/COST/g) == 'COST' || resp.columns[i].title.match(/MRP/g) == 'MRP' || resp.columns[i].title.match(/Total/g) == 'Total' || resp.columns[i].title.match(/Amount/g) == 'Amount' || resp.columns[i].title.match(/Profit/g) == 'Profit' || resp.columns[i].title.match(/ Price/g) == ' Price' || resp.columns[i].title.match(/VAT/g) == 'VAT' || resp.columns[i].title.match(/Difference/g) == 'Difference') {
                    resp.columns[i].type = "number";
                    resp.columns[i]['format'] = "{0:n}";
                    resp.columns[i]['attributes'] = { class: "text-right field-report", ftype: "number", fformat: "0.00" };
                    //resp.columns[i]['ftype'] = "number";
                }

            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: $scope.reportName + ".xlsx",
                    allPages: true
                },
                pdf: {
                    allPages: true,
                    paperSize: [1850, 1000],
                    //paperSize: "A4",
                    fileName: $scope.reportName + ".pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: resp.columns,
                dataSource : {
                    data: resp.data,
                    pageSize: 20
                },
                excelExport: function (e) {

                    addHeader(e, $scope.reportName);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes) {
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = new Date(cell.value);
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }
                            }
                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
   

    $scope.searchReport = function (filters) {
        $.LoadingOverlay("show");

        $scope.type = 'Month';
        var data = new reportRequest();
        data.id = $scope.id;
        data.productId = null;
        data.vendor = null;
        data.fromDate = $filter('date')(filters.from, 'yyyy-MM-dd');
        data.toDate = $filter('date')(filters.to, 'yyyy-MM-dd');
        $scope.cc = data.fromDate;
        if (filters.supplier)
            data.vendor = filters.supplier.id;

        if (filters.product)
            data.productId = filters.product.product.id;

        if ($scope.branchid == undefined || $scope.branchid == "") {
            data.instanceId = $scope.instance.id;
        }
        else
        {
            data.instanceId = $scope.branchid;
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.cc;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
       //setFileName();
        customReportsService.runDevReport(data).then(function (response) {

            var resp = response.data;
            $scope.dataLen = response.data;

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {  //$scope.currentInstance == typeof (Object)
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>" + $scope.reportName;
            }

            for (var i = 0; i < resp.columns.length > 0; i++) {

                if (resp.columns[i].title.match(/ Date/g) == ' Date' || resp.columns[i].title.match(/ date/g) == ' date' || resp.columns[i].title.match(/Expiry/g) == 'Expiry') {
                    resp.columns[i]["template"] = "#=" + resp.columns[i].field + " == null ? '-' : kendo.toString(kendo.parseDate(" + resp.columns[i].field + "), 'dd/MM/yyyy') #";
                    resp.columns[i].type = "date";
                    resp.columns[i]['ftype'] = "date";
                }
                //if (resp.columns[i].title.match(/ Time/g) == ' Time' || resp.columns[i].title.match(/ time/g) == ' time') {
                //    resp.columns[i]["template"] = "#= kendo.toString(kendo.parseDate(" + resp.columns[i].field + ", 'yyyy-MM-dd'), 't') #";
                //    resp.columns[i].type = "date";
                //    resp.columns[i]['ftype'] = "date";
                //    // resp.columns[i].template="#= kendo.toString(kendo.parseDate(createdAt), 'dd/MM/yyyy') #";
                //}
                if (resp.columns[i].title.match(/COST/g) == 'COST' || resp.columns[i].title.match(/MRP/g) == 'MRP' || resp.columns[i].title.match(/Total/g) == 'Total' || resp.columns[i].title.match(/Amount/g) == 'Amount' || resp.columns[i].title.match(/Profit/g) == 'Profit' || resp.columns[i].title.match(/ Price/g) == ' Price' || resp.columns[i].title.match(/VAT/g) == 'VAT' || resp.columns[i].title.match(/Difference/g) == 'Difference') {
                    resp.columns[i].type = "number";
                    resp.columns[i]['format'] = "{0:n}";
                    resp.columns[i]['attributes'] = { class: "text-right field-report", ftype: "number", fformat: "0.00" };
                    // resp.columns[i]['ftype'] = "number";
                }
            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: $scope.reportName+".xlsx",
                    allPages: true
                },
                pdf: {
                    //paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    allPages: true,
                    paperSize: [1850, 1000],
                    //paperSize: "A4",
                    fileName: $scope.reportName + ".pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: resp.columns,
                dataSource: {
                    data: resp.data,
                    pageSize: 20
                },
                excelExport: function (e) {

                    addHeader(e,$scope.reportName);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes) {
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = new Date(cell.value);
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }
                            }
                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    //$scope.order = ['ProductName', 'BatchNoOld', 'BatchNoNew', 'ExpireDateOld1', 'ExpireDateNew1', 'VATOld', 'VATNew', 'GSTOld', 'GSTNew', 'PackageSizeOld', 'PackageSizeNew', 'PackagePurchasePriceOld', 'PackagePurchasePriceNew', 'SellingPriceOld', 'SellingPriceNew'];
    $scope.clearSearch = function () {
        $scope.init($scope.id);
    }

    $scope.selectedProduct = {
        "name": ""
    };

    $scope.productId = "";
    $scope.onProductSelect = function (obj) {
        $scope.productId = obj.product.id;
    }

    $scope.getProducts = function (val) {
        var instanceid = $scope.branchid;
        if (instanceid != undefined && instanceid != null) {
            return customReportsService.allStockProductList(val, instanceid).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        }
    };

    $scope.onSupplierSelect = function (obj) {

        //data.supplier = obj.name;
        //data.mobile = obj.mobile;
        $scope.supplier = obj.id;
    }

    $scope.getSupplierName = function (val) {
        return customReportsService.getSupplierName(val, '').then(function (response) {
            return response.data.map(function (item) {
                return item;
            });

        });
    };



    function addHeader(e,gTitle) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: gTitle, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
//setFileName = function () {
//        $scope.fileNameNew = "Inventory Data Update Report_" + $scope.instance.name;
//    };

}]);

app.factory('Page', function () {
    var title = '';
    return {
        title: function () { return title; },
        setTitle: function (newTitle) { title = newTitle }
    };
});