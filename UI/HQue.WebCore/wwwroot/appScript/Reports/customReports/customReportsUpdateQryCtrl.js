app.controller('customReportsUpdateQryCtrl', ['$scope', 'customReportsService', 'toastr', function ($scope, customReportsService, toastr) {


    $scope.reportType = 1;
    $scope.reportName = "";
    $scope.selectedFields = [];
    $scope.reportFields = [];


    $scope.result = "";

    $scope.init = function (id) {      
        if (id != "") {           
            customReportsService.editQryData(id).then(function (response) {
                $scope.getResult = response.data;
                $scope.reportName = $scope.getResult.reportName;
                $scope.reportQry = $scope.getResult.query;
            }, function (error) {
              
            });
        }
    };
    $scope.resultFilter = "";
    
    var type = "";
    var lable = "";
    var searchOption = "";
    $scope.save = function () {
        $scope.splitObject = [];
        var test = $scope.reportQry;
       
        var split1 = test.split("[");       
        angular.forEach(split1, function (value, i) {

            var column = value.match("(.*)]");
            if (column != null && column[0].match("#(.*)")==null) {
                
                column[1] = { 'Id': column[1], 'Label': column[1], 'Type': type, "Size": 15, "IsGrouped": false, "Aggregate": null, "IsShowTotal": false }
                $scope.selectedFields.push(column[1]);
            }
            var matchValues = value.match("#(.*)#");
            if (matchValues != null && matchValues[1] != "") {
                if (matchValues[1].match("(.*)@")[1] == 'select') {
                    type = "select";
                    lable = "Select";
                    searchOption = matchValues[1].match("@(.*)")[1];
                }
                else if (matchValues[1].match("(.*)@")[1] == 'dt') {
                    type = "date";
                    lable = "date";
                    searchOption = matchValues[1].match("@(.*)")[1];
                }
                else if (matchValues[1].match("(.*)@")[1] == 'int') {
                    type = "int";
                    lable = " ";
                    searchOption = matchValues[1].match("@(.*)")[1];
                }
                else {
                    type = "string";
                    lable = "";
                    searchOption = matchValues[1].match("@(.*)")[1];
                }

                matchValues[0] = { 'FID': matchValues[0], 'type': type, 'label': lable, 'search': searchOption }
                $scope.splitObject.push(matchValues[0]);
            }
        });


        
        $scope.resultFilter = JSON.stringify($scope.splitObject);

        var data = {};

        data.selectedFields = $scope.selectedFields;
        data.query = $scope.reportQry;
        data.filterJSON = $scope.resultFilter;
        data.configJSON = JSON.stringify($scope.selectedFields);
        data.reportName = $scope.reportName;
        data.reportType = 1;
        data.id = $scope.getResult.id;
        if (data.id != null && data.id != '' && data.id != undefined)
        {
            customReportsService.updateDevCustomReport(data).then(function (response) {
                $scope.result = response.data;
                $.LoadingOverlay("hide");
                toastr.success("Successfully Update the Report");
                window.location = "/CustomReports/customReportsList";
            }, function (response) {
                $.LoadingOverlay("hide");
                toastr.error(response.data, 'error');
            });
        }
    };


}]);


app.filter('splitString', function () {
        return function (input, splitChar) {
            return input.split(splitChar);
        }
    });