app.controller('customReportsQryCtrl', ['$scope', 'customReportsService', 'toastr', function ($scope, customReportsService, toastr) {

    $scope.init = function () {

        $('#wizard').smartWizard({
            onLeaveStep: leaveAStepCallback,
            onFinish: onFinishCallback,
            onShowStep: onShowStep
        });
    }

    $scope.reportType = 1;
    $scope.reportName = "";
    $scope.selectedFields = [];
    $scope.reportFields = [];


    $scope.setReportType = function(selectedType){
        $scope.reportType = selectedType;
    }

    $scope.onItemSelected = function (index) {
        var foundIndex = $scope.selectedFields.indexOf($scope.reportFields[index]);
        if ($scope.selectedFields.indexOf($scope.reportFields[index]) != -1) {
            $scope.reportFields[index].selected = false;
            $scope.selectedFields.splice(foundIndex, 1);
        }
        else {
            $scope.reportFields[index].selected = true;
            $scope.selectedFields.push($scope.reportFields[index]);
        }
    };
   
    $scope.IsItemSelected = function (index) {
        if ($scope.selectedFields.length <= 0)
            return false;

        if ($scope.selectedFields.indexOf($scope.reportFields[index]) != -1)
            return true;
        return false;
    };

    $scope.hideme = function (e) {
        //e.preventDefault();
        e.stopPropagation();
    };
    //something
    function onShowStep(obj, context) {

        switch (context.toStep) {
            case 2:

                $.LoadingOverlay("show");

                customReportsService.getFilters($scope.reportType).then(function (response) {
                    $scope.reportFields = response.data;
                    $.LoadingOverlay("hide");
                }, function () {
                    $.LoadingOverlay("hide");
                    toastr.error('Something went wrong', 'error');
                });
                break;
            case 3:
                initializeQB($scope.reportFields);
                break;
        }
    }

    function leaveAStepCallback(obj, context) {
        return true;
    }

    $scope.result = "";

    function onFinishCallback(objs, context) {
        
        $.LoadingOverlay("show");
        var data = {};

        var res = $('#builder').queryBuilder('getSQL', $(this).data('stmt'), false);
        data.selectedFields = $scope.selectedFields;
        data.filter = res.sql;
        data.reportName = $scope.reportName;
        data.reportType = $scope.reportType;
        customReportsService.saveCustomReport(data).then(function (response) {
            $scope.result = response.data;
            $.LoadingOverlay("hide");
            toastr.success("Successfully save custom report");
            window.location = "list";
        }, function (response) {
            $.LoadingOverlay("hide");
            toastr.error(response.data, 'error');
        });

    }

    function initializeQB(objectFilters) {


        var $b = $('#builder');

        var options = {
            allow_empty: true,

            sort_filters: true,

            optgroups: {
                core: {
                    en: 'Core',
                    fr: 'Coeur'
                }
            },

            plugins: {
                'bt-tooltip-errors': { delay: 100 },
                'sortable': null,
                'filter-description': { mode: 'bootbox' },
                'bt-selectpicker': null,
                'unique-filter': null,
                'bt-checkbox': { color: 'primary' },
                'invert': null
            },

            // standard operators in custom optgroups
            operators: [
              { type: 'equal', optgroup: 'basic' },
              { type: 'not_equal', optgroup: 'basic' },
              { type: 'in', optgroup: 'basic' },
              { type: 'not_in', optgroup: 'basic' },
              { type: 'less', optgroup: 'numbers' },
              { type: 'less_or_equal', optgroup: 'numbers' },
              { type: 'greater', optgroup: 'numbers' },
              { type: 'greater_or_equal', optgroup: 'numbers' },
              { type: 'between', optgroup: 'numbers' },
              { type: 'not_between', optgroup: 'numbers' },
              { type: 'begins_with', optgroup: 'strings' },
              { type: 'not_begins_with', optgroup: 'strings' },
              { type: 'contains', optgroup: 'strings' },
              { type: 'not_contains', optgroup: 'strings' },
              { type: 'ends_with', optgroup: 'strings' },
              { type: 'not_ends_with', optgroup: 'strings' },
              { type: 'is_empty' },
              { type: 'is_not_empty' },
              { type: 'is_null' },
              { type: 'is_not_null' }
            ],
            filters: objectFilters

        };

        // init
        $('#builder').queryBuilder(options);

        $('#builder').on('afterCreateRuleInput.queryBuilder', function (e, rule) {
            if (rule.filter.plugin == 'selectize') {
                rule.$el.find('.rule-value-container').css('min-width', '200px')
                  .find('.selectize-control').removeClass('form-control');
            }
        });

        // reset builder
        $('.reset').on('click', function () {
            $('#builder').queryBuilder('reset');
            $('#result').addClass('hide').find('pre').empty();
        });


        $('.parse-sql').on('click', function () {
            var res = $('#builder').queryBuilder('getSQL', $(this).data('stmt'), false);
            $('#result').removeClass('hide')
              .find('pre').html(
                res.sql + (res.params ? '\n\n' + JSON.stringify(res.params, undefined, 2) : '')
              );
        });

    }



    $scope.resultFilter = "";
    
    var type = "";
    var lable = "";
    var searchOption = "";
    $scope.save = function () {
        $scope.splitObject = [];
        var test = $scope.reportQry;
        //var split = test.split(" ");
        var split1 = test.split("[");       
        angular.forEach(split1, function (value, i) {

            var column = value.match("(.*)]");
            if (column != null && column[0].match("#(.*)")==null) {

                //var rpl = $scope.reportQry;
                //$scope.reportQry = rpl.replace(column[0], column[1]);
                column[1] = { 'Id': column[1], 'Label': column[1], 'Type': type, "Size": 15, "IsGrouped": false, "Aggregate": null, "IsShowTotal": false }
                $scope.selectedFields.push(column[1]);

            }

            var matchValues = value.match("#(.*)#");
            if (matchValues != null && matchValues[1] != "") {
                if (matchValues[1].match("(.*)@")[1] == 'select') {
                    type = "select";
                    lable = "Select";
                    searchOption = matchValues[1].match("@(.*)")[1];
                }
                else if (matchValues[1].match("(.*)@")[1] == 'dt') {
                    type = "date";
                    lable = "date";
                    searchOption = matchValues[1].match("@(.*)")[1];
                }
                else if (matchValues[1].match("(.*)@")[1] == 'int') {
                    type = "int";
                    lable = " ";
                    searchOption = matchValues[1].match("@(.*)")[1];
                }
                else {
                    type = "string";
                    lable = "";
                    searchOption = matchValues[1].match("@(.*)")[1];
                }

                matchValues[0] = { 'FID': matchValues[0], 'type': type, 'label': lable, 'search': searchOption }
                $scope.splitObject.push(matchValues[0]);
            }
        });


        //angular.forEach(split, function (value, i) {
            
        //    var column = value.match("@(.*)@");
        //    if (column != null) {

        //        var rpl = $scope.reportQry;
        //        //rpl.replace(column[0], column[1]);
        //        $scope.reportQry = rpl.replace(column[0], column[1]);
        //        column[1] = { 'Id': column[1], 'Label': lable, 'Type': type, "Size": 15, "IsGrouped": false, "Aggregate": null, "IsShowTotal": false }
        //        $scope.selectedFields.push(column[1]);
               
        //    }

        //    var matchValues = value.match("#(.*)#");
        //    if (matchValues != null && matchValues[1]!="") {
        //        if (matchValues[1].match("(.*)@")[1] == 'select')
        //        {
        //            type="select";
        //            lable = "Select";
        //            searchOption = matchValues[1].match("@(.*)")[1];
        //        }
        //        else if(matchValues[1].match("(.*)@")[1]=='dt')
        //        {
        //            type="date";
        //            lable = "date";
        //            searchOption = matchValues[1].match("@(.*)")[1];
        //        }
        //        else if (matchValues[1].match("(.*)@")[1] == 'int')
        //        {
        //            type="int";
        //            lable = " ";
        //            searchOption = matchValues[1].match("@(.*)")[1];
        //        }
        //        else{
        //            type="string";
        //            lable = "";
        //            searchOption = matchValues[1].match("@(.*)")[1];
        //        }
                
        //        matchValues[0] = { 'FID': matchValues[0], 'type': type, 'label': lable, 'search': searchOption }
        //        $scope.splitObject.push(matchValues[0]);
        //    }
        //});

        
        $scope.resultFilter = JSON.stringify($scope.splitObject);

        //$scope.resultFilter1 = $scope.selectedFields;

        var data = {};

        data.selectedFields = $scope.selectedFields;
        data.query = $scope.reportQry;
        data.filterJSON = $scope.resultFilter;
        data.configJSON = JSON.stringify($scope.selectedFields);
        data.reportName = $scope.reportName;
        data.reportType = 1;
        customReportsService.saveDevCustomReport(data).then(function (response) {
            $scope.result = response.data;
            $.LoadingOverlay("hide");
            toastr.success("Successfully save custom Qry report");
            window.location = "customReportsList";
        }, function (response) {
            $.LoadingOverlay("hide");
            toastr.error(response.data, 'error');
        });
    };


}]);


app.filter('splitString', function () {
        return function (input, splitChar) {
            return input.split(splitChar);
        }
    });