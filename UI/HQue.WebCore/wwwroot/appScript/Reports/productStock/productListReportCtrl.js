app.controller('productListReportCtrl', ['$scope', '$rootScope', 'stockReportService', '$filter', function ($scope, $rootScope, stockReportService, $filter) {

    $scope.data = [];
    var sno = 0;
   

    $scope.InstanceName = document.getElementById("hdnInstancename").value;

    $scope.InstanceWiseType = "Instance";

    $scope.ChangeInstancewiseTypeDropdown = function () {
        $scope.init();
    };

    $scope.init = function () {

        $.LoadingOverlay("show");

        
     
        stockReportService.productList($scope.InstanceWiseType)
            .then(function (response) {
                $scope.data = response.data;
                var pdfHeader = "Product List";
                if ($scope.instance != undefined) {
                    $scope.pdfHeader = $scope.instance;
                } else {
                    stockReportService.getInstanceData()
                        .then(function (pdfResponse) {
                            $scope.pdfHeader = pdfResponse.data;
                        }, function () { });
                }

                if ($scope.pdfHeader) {

                    pdfHeader = "Product List";


                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Product List.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1600, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Product List.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    //height: 350,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                      { field: "sno", template: "#= ++record #", type: "string", title: "S.No", width: "30px", attributes: { ftype: "sno", class: " text-left field-report" } },
                      { field: "name", title: "Product Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "manufacturer", title: "Manufacturer", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                       { field: "genericName", title: "GenericName", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                        { field: "category", title: "Category", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                          { field: "packageSize", title: "PackageSize", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } }



                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [


                        ],
                        schema: {
                            model: {
                                fields: {
                                    "sno": { type: "number" },
                                    "name": { type: "string" },
                                    "manufacturer": { type: "string" },
                                    "genericName": { type: "string" },
                                    "category": { type: "string" },
                                    "packageSize": { type: "string" }



                                }
                            }
                        },
                        pageSize: 20
                    },

                    dataBinding: function () {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    },
                    excelExport: function (e) {

                        addHeader(e);
                        sno = 0;
                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];
                                //added by nandhini for excel s.no
                                var cell = row.cells[ci];
                                if (this.columns[ci].attributes.ftype == "sno" && i == 2) {
                                    sno = 0;
                                }

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                    cell.hAlign = "left";
                                    sno = sno + 1;
                                    cell.value = sno;
                                }
                                //end

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                       
                    },
                });

                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
    }

    //
    $scope.order = ['name', 'manufacturer', 'genericName', 'category', 'packageSize'];

    //$scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.buyReport();
    }
    
    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        //headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        //e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Product List", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);


    }

    
}]);

