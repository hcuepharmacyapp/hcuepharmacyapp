app.controller('stockValueReportCtrl', ['$scope', '$http', '$interval', '$q', 'salesReportService', 'stockReportService', 'productModel', 'stockValueReportModel', 'productService', 'toastr', 'ModalService', '$filter', function ($scope, $http, $interval, $q, salesReportService, stockReportService, productModel, stockValueReportModel, productService, toastr, ModalService, $filter) {
    
    var stockValue = stockValueReportModel;
    $scope.search = stockValue;
    $scope.stockValueList = [];
    $scope.list = [];
    var sno = 0;
    $scope.listItems = [];

    $scope.enableSave = false;
    //$scope.open1 = function () {
    //    $scope.popup1.opened = true;
    //};

    //$scope.popup1 = {
    //    opened: false
    //};

    //$scope.open2 = function () {
    //    $scope.popup2.opened = true;
    //};

    //$scope.popup2 = {
    //    opened: false
    //};

    //$scope.dateOptions = {
    //    formatYear: 'yy',
    //    startingDay: 1
    //};

    //$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    //$scope.format = $scope.formats[2];
    //$scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    //$scope.data = [];
    //$scope.type = "Today";
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;

            $scope.stockValueReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.save = function () {
        $.LoadingOverlay("show");

        stockReportService.saveReport($scope.data).then(function (response) {

            toastr.success("Saved Successfully");
            $scope.getSavedReport();
            $.LoadingOverlay("hide");

        }, function (error) {
            toastr.error("Error Occurred");
            $.LoadingOverlay("hide");
        });


    };

    $scope.toggleProductDetail = function (obj, val1) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+'){
            stockReportService.getUniqueSavedReport(val1).then(function (response) {
                $scope.listItems = response.data;
                $scope.overallPurchaseTotal = 0;
                $scope.overallMrpTotal = 0;
                for(var i=0; i < $scope.listItems.length; i++){
                    $scope.overallPurchaseTotal = $scope.overallPurchaseTotal + $scope.listItems[i].totalPurchasePrice;
                    $scope.overallMrpTotal = $scope.overallMrpTotal + $scope.listItems[i].totalMrp;
                }
                $('#chip-btn' + row).text('-');
            }, function (error) {
                $('#chip-btn' + row).text('-');
            });
        }            
        else {
            $scope.listItems = [];
            $scope.overallPurchaseTotal = 0;
            $scope.overallMrpTotal = 0;
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.getSavedReport = function () {
        $.LoadingOverlay("show");
        stockReportService.getSavedReport().then(function (response) {
            $scope.list = response.data;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    
    $scope.getSavedReport();

    $scope.stockValueReport = function () {
        $.LoadingOverlay("show");
        //var data = {
        //    fromDate: $scope.from,
        //    toDate: $scope.to
        //}

        //$scope.search.dates = data;
       
        stockReportService.getStockValueData().then(function (response) {
            $scope.data = response.data;
            if (response.data.length > 0) {
                $scope.enableSave = true;
            }
            else {
                $scope.enableSave = false;
            }

            $scope.stockValueList = $scope.data;
            
            var pdfHeader = "";
            if ($scope.instance != undefined) {
                $scope.pdfHeader = $scope.instance;
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                    $scope.pdfHeader.tinNo = "";
                else
                    $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "TIN No:" + $scope.pdfHeader.tinNo + "<br/> Stock Value Details";
            }


            $("#grid").kendoGrid({
                    excel: {
                        fileName: "Stock Value Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Stock Value Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height:400,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                   { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "90px", attributes: { ftype: "sno", class: " text-left field-report" } },
                   { field: "category", title: "Category", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },                   
                   { field: "productCount", title: "No.of Products", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total:" },
                   { field: "vat0PurchasePrice", title: "Vat 0 % Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                   { field: "vat5PurchasePrice", title: "Vat 5 % Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                   { field: "vat145PurchasePrice", title: "Vat 14.5 % Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                   { field: "vatOtherPurchasePrice", title: "Vat(Other %) Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                   { field: "gst0PurchasePrice", title: "Gst 0 % Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                   { field: "gst5PurchasePrice", title: "Gst 5 % Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "gst12PurchasePrice", title: "Gst 12 % Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                     { field: "gst18PurchasePrice", title: "Gst 18 % Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                      { field: "gst28PurchasePrice", title: "Gst 28 % Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                     { field: "gstOtherPurchasePrice", title: "Gst(Other %) Cost Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "totalPurchasePrice", title: "Total Cost Price(GST)", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div style='float:right;margin-left:3px;' class='report-footer'>#= kendo.toString(sum, 'n0') #.00</div>" },
                    { field: "totalMrp", title: "Total MRP", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div style='float:right;margin-left:3px;' class='report-footer'>#= kendo.toString(sum, 'n0') #.00</div>" },
                    { field: "profit", title: "Profit %", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "totalPurchasePrice", aggregate: "sum" },
                          { field: "totalMrp", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "number" },
                                "category": { type: "string" },
                                "productCount": { type: "number" },
                                "vat0PurchasePrice": { type: "number" },
                                "vat5PurchasePrice": { type: "number" },
                                "vat145PurchasePrice": { type: "number" },
                                "vatOtherPurchasePrice": { type: "number" },
                                "totalPurchasePrice": { type: "number" },
                                "totalMrp": { type: "number" },
                                "profit": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //
    $scope.order = ['category', 'productCount', 'vat0PurchasePrice', 'vat5PurchasePrice', 'vat145PurchasePrice', 'vatOtherPurchasePrice', 'gst0PurchasePrice', 'gst5PurchasePrice', 'gst12PurchasePrice', 'gst18PurchasePrice', 'gst28PurchasePrice', 'gstOtherPurchasePrice', 'totalPurchasePrice', 'totalMrp', 'profit'];
 
    //$scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.stockValueReport();
    }


    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Stock Value Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "TIN No: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
}]);
