app.controller('stockMovementReportCtrl', ['$scope', '$rootScope', 'stockReportService', 'tempVendorPurchaseItemModel', 'productModel', '$filter', 'toastr', function ($scope, $rootScope, stockReportService, tempVendorPurchaseItemModel, productModel, $filter, toastr) {



    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.type = "";
    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.selectedProduct = {
        "name": ""
    };

    $scope.productId = "";
    $scope.onProductSelect = function (obj) {
        $scope.productId = obj.id;
    }

    $scope.getProducts = function (val) {
        var instanceid = $scope.branchid;
        if (instanceid != undefined && instanceid != null) {
            return stockReportService.tempStockProductList(val, instanceid).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        stockReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.GetstockMovementList();

        }, function () {
            $.LoadingOverlay("hide");
        });
        stockReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.GetstockMovementList = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        $scope.type = "";
        $scope.setFileName();
        stockReportService.stockMovementList($scope.type, data, $scope.branchid).then(function (response) {
            $scope.data = response.data;
            //$scope.type = "";
           
            if ($scope.data.length ==1)
            {
                if ($scope.data[0].stockExecuteStatus !=0)
                {
                    toastr.info($scope.data[0].closingStockMessge);
                    $scope.data = [];
                    response.data = [];
                    //$.LoadingOverlay("hide");
                    //return;
                }
            }
            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                stockReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "Stock Movement Register.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [2800, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Stock_Movement_Register.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [


                  { field: "productName", title: "Product Name", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "openingQty", title: "Opening Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                         { field: "createdOpeningStockQty", title: "Created Opening Stock", width: "120px", format: "{0}", type: "number", attributes: { class: "text-center field-report" } },
                   
                    { field: "importQty", title: "Import Qty", width: "80px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "purchaseQty", title: "Purchase Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                     { field: "salesReturnQty", title: "Sales Ret Qty", width: "80px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "adjustmentInQty", title: "Adj. In Qty", width: "80px", format: "{0}", type: "number", attributes: { class: "text-right field-  report" } },
                    { field: "transferInQty", title: "Transfer In Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                     { field: "dcQty", title: "DC Qty", width: "80px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "tempStockQty", title: "Temp Stock Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                         { field: "totalInQty", title: "Total In Qty", width: "80px", format: "{0}", type: "number", attributes: { class: "text-right report-highlights field-report"  } },

                    { field: "salesQty", title: "Sales Qty", width: "80px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "purchaseReturnQty", title: "Purchase Ret Qty", width: "120px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "adjustmentOutQty", title: "Adj. Out Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-right field-  report" } },
                       { field: "selfConsumption", title: "Self Consumption Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-right field-  report" } },
                    { field: "transferOutQty", title: "Transfer Out Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                         { field: "totalOutQty", title: "Total Out Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-right report-highlights field-report" } },
                              { field: "closingQty", title: "Closing Qty", width: "80px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                                  { field: "currentStock", title: "Current Stock", width: "80px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [


                    ],
                    schema: {
                        model: {
                            fields: {
                                "productId": { type: "string" },
                                "openingQty": { type: "number" },
                                "importQty": { type: "number" },
                                "purchaseQty": { type: "number" },
                                "purchaseReturnQty": { type: "number" },
                                "salesQty": { type: "number" },
                                "salesReturnQty": { type: "number" },
                                "adjustmentQty": { type: "number" },
                                "transferInQty": { type: "number" },
                                "transferOutQty": { type: "number" },
                                "dCQty": { type: "number" },
                                "tempStockQty": { type: "number" },
  

                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });

            $.LoadingOverlay("hide");
        }, function () {
          
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['productName', 'openingQty', 'createdOpeningStockQty', 'importQty', 'purchaseQty', 'salesReturnQty', 'adjustmentInQty', 'transferInQty', 'dcQty', 'tempStockQty', 'totalInQty', 'salesQty', 'purchaseReturnQty', 'adjustmentOutQty', 'selfConsumption', 'transferOutQty', 'totalOutQty', 'closingQty', 'currentStock'];
    //
    $scope.setFileName = function () {
        $scope.fileNameNew = "STOCK MOVEMENT REPORT_" + $scope.instance.name;
    };

    //$scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.GetstockMovementList();
    }

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: "Stock Movement Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
}]);

