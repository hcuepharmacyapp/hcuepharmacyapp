app.controller('productAgeAnalyseListCtrl', ['$scope', '$rootScope', 'stockReportService', 'tempVendorPurchaseItemModel', 'productModel', '$filter', function ($scope, $rootScope, stockReportService, tempVendorPurchaseItemModel, productModel, $filter) {

    var tempVendorPurchaseItem = tempVendorPurchaseItemModel;
   
    $scope.search = tempVendorPurchaseItem;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }

    }

    $scope.checkFromDate = function () {
        var dt = $("#fromDate").val();
     
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validFromDate = true;

                    if ($scope.to != undefined && $scope.to != null) {
                        $scope.checkToDate1($scope.from, $scope.to);
                    }
                }
                else {
                    $scope.validFromDate = false;
                }
            }
            else {
                $scope.validFromDate = false;
            }
    }

    $scope.checkToDate = function () {
        var dt = $("#toDate").val();

            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validToDate = true;
                    $scope.checkToDate1($scope.from, $scope.to);
                }
                else {
                    $scope.validToDate = false;
                }
            }
            else {
                $scope.validToDate = false;
            }
    }

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.selectedProduct = {
        "name": ""
    };

    $scope.productId = "";
    $scope.onProductSelect = function (obj) {
        $scope.productId = obj.product.id;
    }

    $scope.getProducts = function (val) {
        var instanceid = $scope.branchid;
        if (instanceid != undefined && instanceid != null) {
            return stockReportService.allStockProductList(val, instanceid).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        stockReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.buyReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        stockReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.buyReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        stockReportService.productAgeAnalyseListData($scope.type, data, $scope.branchid, $scope.productId).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                stockReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "Product Age Analysis.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1600, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Product_Age_Analysis.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                  
                  { field: "productName", title: "Product Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "batchNo", title: "BatchNo", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "expireDate", title: "Expire Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(expireDate), 'MM/yy') #" },
                  { field: "lessthan30days", title: "Stock <30 days", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "lessthan60days", title: "Stock <60", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "lessthan120days", title: "Stock<120", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "lessthan180days", title: "Stock <180", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "lessthan360days", title: "Stock <360", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "age", title: "Age", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                 { field: "lastSalesDate", title: "Last Sale Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yy" }, template: "#=lastSalesDate == null ? '-' :kendo.toString(kendo.parseDate(lastSalesDate), 'dd/MM/yy') #" },

                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                       
                    ],
                    schema: {
                        model: {
                            fields: {
                                "productName": { type: "string" },
                                "batchNo": { type: "string" },
                                "expireDate": { type: "date" },                                
                                "age": { type: "number" },
                                "lastSalesDate": { type: "date" },
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['productName', 'batchNo', 'reportExpireDate', 'lessthan30days', 'lessthan60days', 'lessthan120days', 'lessthan180days', 'lessthan360days', 'age', 'reportInvoiceDate'];
    //


    //$scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.buyReport();
    }

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Product Age Analysis", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
    $scope.cancel = function () {
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.selectedProduct.name = null;       
        $scope.productId = "";
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;
        $scope.init();
    }
}]);

