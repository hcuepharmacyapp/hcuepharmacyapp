app.factory('stockReportService', function ($http) {
    return {
        selectInstance: function () {
            return $http.post('/stockReport/SelectInstance')
        },
        //     //Stock Type parameter added by Poongodi on 13/03/2017
        stockList: function (category, manufacturer, sRackNo, sBoxNo, instanceid, stocktype, productWise, Alphabet1, Alphabet2, data) {
            return $http.post('/stockReport/StockProductList?category=' + escape(category) + '&manufacturer=' + manufacturer + '&sRackNo=' + sRackNo + '&sBoxNo=' + sBoxNo + '&id=' + instanceid + '&sStockType=' + stocktype + '&isProductWise=' + productWise + '&alphabet1=' + Alphabet1 + '&alphabet2=' + Alphabet2, data);
        },
        allBranchConsolidatedList: function () {
            return $http.post('/stockReport/getAllBranchConsolidatedList');
        },
        getInstanceData: function () {
            return $http.post('/expireProductReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },
        //add by nandhini for product list report
        productList: function (type) {
            return $http.post('/stockReport/productListData?type=' + type);
        },
        //productList: function (instanceid) {
        //    return $http.post('/stockReport/productListData?InstanceId=' + instanceid);
        //},
        //end
        getStockLedger: function (data) {
            return $http.post('/stockReport/getStockLedger', data);
        },
        getStockAdjustmentData: function (type, data) {
            return $http.post('/stockReport/getStockAdjustmentReport?type=' + type, data);
        },
        getStockValueData: function () {
            return $http.get('/stockReport/GetStockValueReport');
        },
        saveReport: function (data) {
            return $http.post('/stockReport/saveReport', data);
        },
        getSavedReport: function () {
            return $http.get('/stockReport/getSavedReport')
        },
        getUniqueSavedReport: function (reportNo) {
            return $http.post('/stockReport/getUniqueSavedReport?reportNo=' + reportNo);
        },
        tempStockList: function (type, data, instanceid,id) {
            return $http.post('/stockReport/tempStockListData?type=' + type + '&sInstanceId=' + instanceid + '&productId=' + id, data);
        },
        tempStockProductList: function (filter, instanceid) {
            return $http.get('/ProductStockData/tempStockProductList?productName=' + escape(filter) + '&instanceid=' + instanceid);
        },
        productAgeAnalyseListData: function (type, data, instanceid, id) {
            return $http.post('/stockReport/productAgeAnalyseListData?type=' + type + '&sInstanceId=' + instanceid + '&productId=' + id, data);
        },

        
        selfConsumptionListData: function (type, data, instanceid, id,invno) {
            return $http.post('/stockReport/selfConsumptionListData?type=' + type + '&sInstanceId=' + instanceid + '&productId=' + id + '&invoiceNo=' + invno, data);
            },
        allStockProductList: function (filter, instanceid) {
            return $http.get('/ProductStockData/AllStockProductList?productName=' + escape(filter) + '&instanceid=' + instanceid);
        },
        getCategoryList: function (instanceid, stocktype, category) {
            return $http.post('/stockReport/StockProductListByCategory?id=' + instanceid + '&sStockType=' + stocktype + '&category=' + category);
        },
        getManufacturerList: function (instanceid, stocktype, manufacturer) {
            return $http.post('/stockReport/StockProductListByManufacturer?id=' + instanceid + '&sStockType=' + stocktype + '&manufacturer=' + manufacturer);
        },
        getRackNoList: function (instanceid, stocktype, searchtext, searchType) {
            return $http.post('/stockReport/StockProductListByLocation?id=' + instanceid + '&sStockType=' + stocktype + '&sSearchText=' + searchtext + '&sSearchType=' + searchType);
        },
        stockTransferList: function (type, data, instanceid) {
            return $http.post('/stockReport/stockTransferList?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        stockMovementList: function (type, data, instanceid) {
            return $http.post('/stockReport/StockMovementList?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        //,
        //getTypeList: function (instanceid, stocktype, type) {
        //    return $http.post('/stockReport/StockProductList?id=' + instanceid + '&sStockType=' + stocktype + '&type=' + type);
        //}
        physicalStockHistoryList: function (data, type, instanceId, productId, userName, userId) {
            return $http.post('/stockReport/physicalStockHistoryList?type=' + type + '&InstanceId=' + instanceId + '&ProductId=' + productId + '&UserName=' + userName + '&UserId=' + userId, data);
        },
        stockTransferDetail: function (type, data, instanceid,reportType) {
            return $http.post('/stockReport/StockTransferDetailList?type=' + type + '&sInstanceId=' + instanceid + '&sReportType=' + reportType, data);
        },
        GetUserName: function (filter) {
            return $http.get('/salesReport/GetUserName?userName=' + filter);
        },
        GetUserId: function (filter) {
            return $http.get('/salesReport/GetUserId?userId=' + filter);
        },
    }
});
