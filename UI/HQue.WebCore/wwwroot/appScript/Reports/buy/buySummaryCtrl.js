app.controller('buySummaryCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'buyReportService', 'vendorPurchaseModel', 'vendorPurchaseItemModel', '$filter', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, buyReportService, vendorPurchaseModel, vendorPurchaseItemModel, $filter) {
    
    var vendorPurchaseItem = vendorPurchaseItemModel;
    var vendorPurchase = vendorPurchaseModel;
    $scope.search = vendorPurchaseItem;
    $scope.search.vendorPurchase = vendorPurchase;
    $scope.allBranch = true;
    $scope.IsInvoiceDate = "false";

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };
        

    $scope.init = function () {
        $.LoadingOverlay("show");
        buyReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.buyReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        buyReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }


    $scope.buyReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        if ($scope.branchid == undefined || $scope.branchid == "" || $scope.branchid == null) {
            $scope.branchid = undefined;//$scope.instance.id;
        }
        $scope.setFileName();
        buyReportService.buySummarylist($scope.type, data, $scope.branchid, $scope.IsInvoiceDate).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            angular.forEach($scope.data, function (data1, key) {
                data1.buyPurchase.goodsRcvNo = data1.buyPurchase.billSeries + data1.buyPurchase.goodsRcvNo;
            });

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                buyReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Purchase Invoice Wise";
            }
            $scope.fileName = "Purchase Details.";
            $("#grid").kendoGrid({
                excel: {
                    fileName: $scope.fileName,
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Purchase Invoice Wise.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "buyPurchase.invoiceDate", title: "Invoice Date", width: "110px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(buyPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  { field: "buyPurchase.invoiceNo", title: "Invoice No", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "buyPurchase.vendor.name", title: "Supplier Name", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "buyPurchase.instance.name", title: "Branch", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "buyPurchase.goodsRcvNo", title: "GR No.", width: "100px", format: "{0}", type: "string", attributes: { class: "text-left field-report" }, footerTemplate: "Total" },
                    { field: "reportTotal", title: "Purchase Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                   { field: "buyPurchase.credit", title: "Credit Note Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                         { field: "buyPurchase.actualPaymentTotal", title: "Debit Note Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "buyPurchase.totalPurchaseValue", title: "Final Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        
                        { field: "reportTotal", aggregate: "sum" },
                         { field: "buyPurchase.credit", aggregate: "sum" },
                    { field: "buyPurchase.totalPurchaseValue", aggregate: "sum" },
                            { field: "buyPurchase.actualPaymentTotal", aggregate: "sum" }
      

                    ],
                    schema: {
                        model: {
                            fields: {
                                "buyPurchase.invoiceDate": { type: "date" },
                                "buyPurchase.invoiceNo": { type: "string" },
                                "buyPurchase.vendor.name": { type: "string" },
                                "buyPurchase.instance.name": { type: "string" },
                                "buyPurchase.goodsRcvNo": { type: "string" },
                                "reportTotal": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['reportInvoiceDate', 'buyPurchase.invoiceNo', 'buyPurchase.vendor.name', 'buyPurchase.instance.name', 'buyPurchase.goodsRcvNo', 'reportTotal', 'credit', 'actualPaymentTotal', 'totalPurchaseValue'];
    //
 

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.buyReport();
    }

    // chng-3

    $scope.header = "Hcue Technologies";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Purchase Invoice Wise", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Purchase Invoice Wise", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }
    $("#btnPdfExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").saveAsPDF();
        }
    });


    $("#btnXLSExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "xlsx";
            $("#instanceName").text("Qbitz Tech");
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });

    $("#btnCSVExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "csv";
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });
    $scope.setFileName = function () {
        $scope.fileNameNew = "PurchaseInvoiceWise_" + $scope.instance.name;
    }
    //
}]);
