app.controller('buyProductwiseReportCtrl', ['$scope', '$rootScope',  '$http', '$interval', '$q', 'buyReportService', 'vendorPurchaseModel', 'vendorPurchaseItemModel', '$filter', 'vendorService', 'productStockService', function ($scope, $rootScope, $http, $interval, $q, buyReportService, vendorPurchaseModel, vendorPurchaseItemModel, $filter, vendorService, productStockService) {
    
    var vendorPurchaseItem = vendorPurchaseItemModel;
    var vendorPurchase = vendorPurchaseModel;
    $scope.search = vendorPurchaseItem;
    $scope.search.vendorPurchase = vendorPurchase;
    $scope.allBranch = true;
    $scope.IsInvoiceDate = "false";
    $scope.search.select = "Productname";

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');


    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };


    $scope.vendor = function () {
        vendorService.vendorData().then(function (response) {
            $scope.vendorList = response.data;
        }, function () { toastr.error('Error Occured', 'Error'); });
    }

    $scope.getProducts = function (val) {
        return productStockService.drugFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

   
    $scope.vendor();


    $scope.nameCondition = true;
    $scope.changefilters = function () {
        console.log($scope.search.select);
        if ($scope.search.select == 'Productname') {
            $scope.nameCondition = true;
            $("#grid").empty();
        }
        else {
            $scope.nameCondition = false;
              $("#grid").empty();
        }

    };

    $scope.ProductId = "";
    $scope.VendorId = "";
    $scope.onProductSelect = function (obj) {
        console.log(JSON.stringify(obj));

        $scope.ProductId = obj.product.id;
       
    }

    $scope.onVendorSelect = function (obj) {
        console.log(JSON.stringify(obj));
        $scope.VendorId = obj.id;


    }
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.type = '';//"Today";
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        buyReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.buyReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        buyReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.buyReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);



        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }
        console.log($scope.search.select);
        console.log($scope.ProductId);
        console.log($scope.VendorId);


        if ($scope.search.select == "") {
            alert("Select Required");
        }
        if ($scope.ProductId == "" && $scope.VendorId == "") {
            alert("Select a Product or Vendor")
        }
      

        if ($scope.from == "" || $scope.to == "") {
            alert("select from date and to date");
        }
        var pid ="";
        var vid = "";
        var tt = "false";

        if ($scope.search.select == "Productname") {
            pid = $scope.ProductId;
       
            $scope.setFileNameProd();
            buyReportService.list($scope.type, data, $scope.branchid, vid, pid, $scope.IsInvoiceDate).then(function (response) {
                $scope.data = response.data;
                $scope.type = "";


                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Purchase Details";
                }

                $scope.fileName = "Purchase Details.";
               var grid= $("#grid").kendoGrid({
                    excel: {
                        fileName: $scope.fileName,
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Purchase Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    height:350,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                        { field: "instanceName", title: "Branch", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                        { field: "productStock.product.name", title: "Product", hidden: false, width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },                    
                      { field: "buyPurchase.invoiceDate", title: "Invoice Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(buyPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                      { field: "buyPurchase.invoiceNo", title: "Invoice No", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                      { field: "vendor.name", title: "Supplier", width: "160px", attributes: { class: "text-left field-report" }, footerTemplate: "Total", filterable: { cell: { showOperators: false } } },
                      { field: "actualQty", title: "Qty", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "freeQty", title: "Free Qty", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                      { field: "packagePurchasePrice", title: "Purchase Rate (excluding vat)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "packageSellingPrice", title: "Selling Price(MRP)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "discount", title: "P.Discount %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                         { field: "schemeDiscountPerc", title: "Sch.Disc %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                     { field: "markupPerc", title: "Markup %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                      { field: "productStock.vat", title: "VAT/GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                       { field: "productStock.cst", title: "CST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } }, //CST/IGST %
                        { field: "freeQtyTaxValue", title: "Free Qty Tax Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                      { field: "vatValue", title: "Total VAT/GST Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },  //VAT/CST Amount

                      { field: "reportTotal", title: "Final Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },


                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                            { field: "actualQty", aggregate: "sum" },
                            { field: "freeQty", aggregate: "sum" },
                              { field: "discount", aggregate: "sum" },
                            { field: "packagePurchasePrice", aggregate: "sum" },
                            { field: "productStock.vat", aggregate: "sum" },
                            { field: "vatValue", aggregate: "sum" },
                            { field: "packageSellingPrice", aggregate: "sum" },
                            { field: "reportTotal", aggregate: "sum" }
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "productStock.product.name": { type: "string" },
                                    "buyPurchase.invoiceDate": { type: "date" },
                                    "buyPurchase.invoiceNo": { type: "number" },
                                    "buyPurchase.vendor.address": { type: "string" },
                                    "buyPurchase.vendor.gsTinNo": { type: "number" },
                                  
                                    "actualQty": { type: "number" },
                                    "freeQty": { type: "number" },
                                    "discount": { type: "number" },
                                    "schemeDiscountPerc": { type: "number" },
                                    "markupPerc": { type: "number" },
                                    "packagePurchasePrice": { type: "number" },
                                    "productStock.vat": { type: "number" },
                                    "productStock.cst": { type: "number" },
                                    "freeQtyTaxValue": { type: "number" },
                                    "vatValue": { type: "number" },
                                    "packageSellingPrice": { type: "number" },
                                    "reportTotal": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });
               
                
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
            $scope.order = ['instanceName', 'productStock.product.name', 'reportInvoiceDate', 'category', 'reason', 'actualQty', 'freeQty', 'packagePurchasePrice', 'packageSellingPrice', 'discount', 'schemeDiscountPerc', 'markupPerc', 'productStock.vat', 'productStock.cst', 'freeQtyTaxValue', 'vatValue', 'reportTotal'];
           
        }
        else if ($scope.search.select == "Vendorname") {
            vid = $scope.VendorId;
            $scope.setFileNameVendor();
            buyReportService.list($scope.type, data, $scope.branchid, vid, pid, $scope.IsInvoiceDate).then(function (response) {
                $scope.data = response.data;
                $scope.type = "";


                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Purchase Details";
                }

                $scope.fileName = "Purchase Details.";
                var grid = $("#grid").kendoGrid({
                    excel: {
                        fileName: $scope.fileName,
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Purchase Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    height: 350,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                    { field: "instanceName", title: "Branch", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                    { field: "vendor.name", title: "Supplier", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                    { field: "productStock.product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },
                      { field: "buyPurchase.invoiceDate", title: "Invoice Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(buyPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                      { field: "buyPurchase.invoiceNo", title: "Invoice No", width: "100px", attributes: { class: "text-left field-report" },footerTemplate: "Total", filterable: { cell: { showOperators: false } } },
                       { field: "actualQty", title: "Qty", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "freeQty", title: "Free Qty", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                      { field: "packagePurchasePrice", title: "Purchase Rate (excluding vat/gst)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "packageSellingPrice", title: "Selling Price(MRP)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "discount", title: "P.Discount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },

                      { field: "productStock.vat", title: "VAT %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                       { field: "productStock.cst", title: "CST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                      { field: "vatValue", title: "VAT/GST Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" }, //VAT/CST Amount

                      { field: "reportTotal", title: "Final Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },


                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                            { field: "actualQty", aggregate: "sum" },
                            { field: "freeQty", aggregate: "sum" },
                              { field: "discount", aggregate: "sum" },
                            { field: "packagePurchasePrice", aggregate: "sum" },
                            { field: "productStock.vat", aggregate: "sum" },
                            { field: "vatValue", aggregate: "sum" },
                            { field: "packageSellingPrice", aggregate: "sum" },
                            { field: "reportTotal", aggregate: "sum" }
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "productStock.product.name": { type: "string" },
                                    "buyPurchase.invoiceDate": { type: "date" },
                                    "buyPurchase.invoiceNo": { type: "number" },
                                    "buyPurchase.vendor.address": { type: "string" },
                                    "buyPurchase.vendor.gsTinNo": { type: "number" },

                                    "actualQty": { type: "number" },
                                    "freeQty": { type: "number" },
                                    "discount": { type: "number" },
                                    "packagePurchasePrice": { type: "number" },
                                    "productStock.vat": { type: "number" },
                                    "productStock.cst": { type: "number" },

                                    "vatValue": { type: "number" },
                                    "packageSellingPrice": { type: "number" },
                                    "reportTotal": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
            $scope.order = ['instanceName', 'reason', 'productStock.product.name', 'reportInvoiceDate', 'buyPurchase.invoiceNo', 'actualQty', 'freeQty', 'packagePurchasePrice', 'packageSellingPrice', 'discount', 'productStock.vat', 'productStock.cst', 'vatValue', 'reportTotal'];
        }
        }
    /*    if ($scope.search.select == "Productname") {
            buyReportService.Productwiselist($scope.ProductId, data).then(function (response) {
                $scope.data = response.data;
                $scope.type = "";


                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    $scope.pdfHeader = $scope.instance;
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                        $scope.pdfHeader.tinNo = "";
                    else
                        $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Purchase Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Purchase Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                         { field: "productStock.product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },
                      { field: "buyPurchase.invoiceDate", title: "Invoice Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(buyPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                      { field: "buyPurchase.invoiceNo", title: "Invoice No", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                      { field: "buyPurchase.vendor.name", title: "Supplier", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                      { field: "actualQty", title: "Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                       { field: "freeQty", title: "Free Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                      { field: "packagePurchasePrice", title: "Purchase Rate (excluding vat)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "packageSellingPrice", title: "Selling Price(MRP)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "discount", title: "P.Discount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },

                      { field: "productStock.vat", title: "VAT %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                       { field: "productStock.cst", title: "CST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                      { field: "vatPrice", title: "VAT/CST Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },

                      { field: "reportTotal", title: "Final Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },


                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                            { field: "actualQty", aggregate: "sum" },
                            { field: "freeQty", aggregate: "sum" },
                              { field: "discount", aggregate: "sum" },
                            { field: "packagePurchasePrice", aggregate: "sum" },
                            { field: "productStock.vat", aggregate: "sum" },
                            { field: "vatPrice", aggregate: "sum" },
                            { field: "packageSellingPrice", aggregate: "sum" },
                            { field: "reportTotal", aggregate: "sum" }
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "productStock.product.name": { type: "string" },
                                    "buyPurchase.invoiceDate": { type: "date" },
                                    "buyPurchase.invoiceNo": { type: "number" },
                                    "buyPurchase.vendor.name": { type: "string" },
                                    "buyPurchase.vendor.address": { type: "string" },
                                    "buyPurchase.vendor.tinNo": { type: "number" },
                                  
                                    "actualQty": { type: "number" },
                                    "freeQty": { type: "number" },
                                    "discount": { type: "number" },
                                    "packagePurchasePrice": { type: "number" },
                                    "productStock.vat": { type: "number" },
                                    "productStock.cst": { type: "number" },

                                    "vatPrice": { type: "number" },
                                    "packageSellingPrice": { type: "number" },
                                    "reportTotal": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }*/

      /*  if ($scope.search.select == "Vendorname") {
            buyReportService.Vendorwiselist($scope.VendorId, data).then(function (response) {
                $scope.data = response.data;
                $scope.type = "";


                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    $scope.pdfHeader = $scope.instance;
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                        $scope.pdfHeader.tinNo = "";
                    else
                        $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "ProductWise Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "ProductWise_Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                         { field: "buyPurchase.vendor.name", title: "Supplier", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                      { field: "productStock.product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },

                      { field: "buyPurchase.invoiceDate", title: "Invoice Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(buyPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                      { field: "buyPurchase.invoiceNo", title: "Invoice No", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                      { field: "actualQty", title: "Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                       { field: "freeQty", title: "Free Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                      { field: "packagePurchasePrice", title: "Purchase Rate (excluding vat)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "packageSellingPrice", title: "Selling Price(MRP)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "discount", title: "P.Discount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },

                      { field: "productStock.vat", title: "VAT %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                       { field: "productStock.cst", title: "CST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                      { field: "vatPrice", title: "VAT/CST Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },

                      { field: "reportTotal", title: "Final Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },


                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                            { field: "actualQty", aggregate: "sum" },
                            { field: "freeQty", aggregate: "sum" },
                              { field: "discount", aggregate: "sum" },
                            { field: "packagePurchasePrice", aggregate: "sum" },
                            { field: "productStock.vat", aggregate: "sum" },
                            { field: "vatPrice", aggregate: "sum" },
                            { field: "packageSellingPrice", aggregate: "sum" },
                            { field: "reportTotal", aggregate: "sum" }
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "buyPurchase.vendor.name": { type: "string" },
                                    "buyPurchase.vendor.address": { type: "string" },
                                    "buyPurchase.vendor.tinNo": { type: "number" },
                                    "productStock.product.name": { type: "string" },
                                    "buyPurchase.invoiceDate": { type: "date" },
                                    "buyPurchase.invoiceNo": { type: "number" },
                                   
                                    "actualQty": { type: "number" },
                                    "freeQty": { type: "number" },
                                    "discount": { type: "number" },
                                    "packagePurchasePrice": { type: "number" },
                                    "productStock.vat": { type: "number" },
                                    "productStock.cst": { type: "number" },

                                    "vatPrice": { type: "number" },
                                    "packageSellingPrice": { type: "number" },
                                    "reportTotal": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });

        } */



     
   
  
    $scope.Option = {
        "Product": "",
        "Vendor": ""
    }

    $scope.clearSearch = function () {
        $scope.ProductId = "";
        $scope.VendorId = "";
        $scope.search.select = "";
        $scope.Option = {
            "Product": "",
            "Vendor": ""
        };
        $scope.from = "";
        $scope.to = "";
        $("#grid").empty();
        $scope.data = [];
        $scope.IsInvoiceDate = "false";
    }

    //$scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.buyReport();
    }


    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "Purchase Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Purchase Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $("#btnPdfExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").saveAsPDF();
        }
    });


    $("#btnXLSExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "xlsx";
            $("#instanceName").text("Qbitz Tech");
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });

    //$("#btnCSVExport").kendoButton(
    //{
    //    click: function () {
    //        $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "csv";
    //        $("#grid").data("kendoGrid").saveAsExcel();
    //    }
    //});
  
    //

    $scope.setFileNameProd = function () {
        $scope.fileNameNewProd = "PurchaseProductWiseDetails_" + $scope.instance.name;
    }

    $scope.setFileNameVendor = function () {
        $scope.fileNameNewVendor = "PurchaseVendorWiseDetails_" + $scope.instance.name;
    }
}]);
