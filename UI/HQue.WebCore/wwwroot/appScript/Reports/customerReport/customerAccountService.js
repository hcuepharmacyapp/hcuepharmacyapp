app.factory('customerReportService', function ($http) {
    return {
        list: function (type,data) {
            return $http.post('/customerReport/list?type=' + type, data);
        },
        customerWiseBalanceList: function (instanceid) {
            return $http.post('/customerReport/customerWiseBalanceList?instanceId=' + instanceid);            
        },
        customerDetailsList: function (data,instanceid) {
            return $http.post('/customerReport/customerDetailsList?instanceId=' + instanceid, data);
        },
        getCustomerName: function (branchid,name, mobile) {
            return $http.get('/customerReport/getCustomerName?customer=' + name + '&mobile=' + mobile + '&instanceid=' + branchid);
        },
       
        getCustomerMobile: function (branchid, name, mobile) {
            return $http.get('/customerReport/getCustomerName?customer=' + name + '&mobile=' + mobile + '&instanceid=' + branchid);
        },
        
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },

        customerWisePaymentReceiptCancellation: function (data, instanceid) {
            return $http.post('/customerReport/customerWisePaymentReceiptCancellationList?instanceId=' + instanceid, data);
        },
        customerPaymentDetails: function (data,instanceid) {
            return $http.post('/customerReport/customerPaymentDetails?instanceId=' + instanceid, data);
        },

        GetPettyCashDates: function (selectedDate) {
            return $http.post('/customerReport/GetPettyCashDates?selectedDate=' + selectedDate);
        },

        GetPettycashDetails: function (pettyId) {
            return $http.post('/customerReport/GetPettycashDetails?pettyId=' + pettyId);
        },
    }
});
