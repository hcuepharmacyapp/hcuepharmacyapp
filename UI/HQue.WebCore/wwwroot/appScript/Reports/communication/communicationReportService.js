app.factory('communicationReportService', function ($http) {
    return {
        smsUserlist: function () {
            return $http.post('/smsReport/smsListData');
        },
        smsBranchList: function (type, data) {
            return $http.post('/smsReport/smsBranchListData?type=' + type, data);
        },
        getInstanceData: function () {
            return $http.post('/expireProductReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },
    }
});
