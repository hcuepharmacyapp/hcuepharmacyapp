app.controller('branchReportCtrl', ['$scope', '$http', '$interval', '$q', 'branchReportService', 'instanceModel', function ($scope, $http, $interval, $q, branchReportService, instanceModel) {
    
    var branch = instanceModel;
    $scope.search = branch;

    $scope.data = [];

    //var fakeI18n = function (title) {
    //    var deferred = $q.defer();
    //    $interval(function () {
    //        deferred.resolve('col: ' + title);
    //    }, 1000, 1);
    //    return deferred.promise;
    //};

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.gridOptions = {
        exporterMenuCsv: true,
        enableGridMenu: true,
        //enableFiltering: true,
        enableColumnResizing: true,
        //gridMenuTitleFilter: fakeI18n,
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        data: 'data',
        columnDefs: [
          { field: 'name', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Name', width: '15%' },
          { field: 'registrationDate', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Registration Date', width: '15%' },
          { field: 'drugLicenseNo', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Drug License No', width: '15%' },
          { field: 'tinNo', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Tin No', width: '15%' },
          { field: 'contactName', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Contact Name', width: '15%' },
          { field: 'address', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Address', width: '15%' },
          { field: 'phone', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Phone', width: '15%' },
          //     name: 'productStockId',
          //     name: 'quantity'
          // },
          //{ field: 'branchItem[5].quantity'}
        ],

        gridMenuCustomItems: [
            {
                title: 'Rotate Grid',
                action: function ($event) {
                    this.grid.element.toggleClass('rotated');
                },
                order: 210
            }
        ],

        onRegisterApi: function( gridApi ){
        $scope.gridApi = gridApi;
 
        //// interval of zero just to allow the directive to have initialized
        //$interval( function() {
        //    gridApi.core.addToGridMenu( gridApi.grid, [{ title: 'Dynamic item', order: 100}]);
        //}, 0, 1);
 
        gridApi.core.on.columnVisibilityChanged( $scope, function( changedColumn ){
            $scope.columnChanged = { name: changedColumn.colDef.name, visible: changedColumn.colDef.visible };
        });
    }
  };

    $scope.branchReport = function () {
        branchReportService.list($scope.search).then(function (response) {
            $scope.data = response.data;
        }, function () { });
    }

    $scope.branchReport();
    //$http.post('/branchReport/listData', $scope.search)
    //.success(function (data) {
    //    $scope.gridOptions.data = data;
    //});
}]);