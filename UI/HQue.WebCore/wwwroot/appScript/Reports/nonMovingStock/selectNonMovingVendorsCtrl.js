app.controller('selectNonMovingVendorsCtrl', function ($scope, $rootScope, toastr, close, cacheService, ModalService, productStockModel, vendorPurchaseItemModel, vendorPurchaseModel, nonMovingStockReportService, vendorModel) {

    var productStocks = [];
    var vendorPurchase = vendorPurchaseModel;
    var vendorPurchaseItem = vendorPurchaseItemModel;
    var vendor = vendorModel;
    var productStock = productStockModel;
    
    $scope.vendor = function () {
        nonMovingStockReportService.vendorData().then(function (response) {
            $scope.vendorList = response.data;
        }, function () { toastr.error('Error Occured', 'Error'); });
    }
    $scope.vendor();

    var items= cacheService.get('selectedItems');

    $scope.loadStocks = function (items) {

        $scope.productStocks = items;
    }

    $scope.loadStocks(items);

    
    $scope.vendorUpdate = function () {

        nonMovingStockReportService.vendorUpdate($scope.productStocks).then(function (response) {

            window.location.href = '/VendorPurchaseReturn/ReturnIndex';

        }, function () { toastr.error('Error Occured', 'Error'); });
    }

    $scope.close = function () {

        $rootScope.$emit("close");
        close('No', 500); // close, but give 500ms for bootstrap to animate
    }

});