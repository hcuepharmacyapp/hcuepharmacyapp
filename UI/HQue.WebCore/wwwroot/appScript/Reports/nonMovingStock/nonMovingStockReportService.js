app.factory('nonMovingStockReportService', function ($http) {
    return {
        selectInstances: function () {
            return $http.post('/nonMovingStockReport/SelectInstances')
        },
        stockList: function (instanceid, data) {
            //return $http.post('/nonMovingStockReport/StockProductList?stockmonth=' + stockMonth + '&sInstanceId=' + instanceid);
            return $http.post('/nonMovingStockReport/StockProductList?&sInstanceId=' + instanceid, data);
        },
        movingStock: function (productStocks) {
            return $http.post('/nonMovingStockReport/UpdateIsMovingStock', productStocks);
        },
        getInstanceData: function () {
            return $http.post('/expireProductReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },

        // Newly Added Gavaskar 04-11-2016
        vendorData: function () {
            return $http.get('/VendorData/VendorList');
        },
        vendorUpdate: function (productStock) {
            return $http.post('/nonMovingStockReport/vendorUpdate', productStock);
        },
    }
});
