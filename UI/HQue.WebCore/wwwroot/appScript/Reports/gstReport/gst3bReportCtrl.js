app.controller('gst3bReportCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'gstreportservice', 'userAccessModel', '$filter', 'ModalService', function ($scope, $rootScope, $http, $interval, $q, gstreportservice, userAccessModel, $filter, ModalService) {

 
    var sno = 0;
    $scope.currentInstance = null;
    $scope.gstinList = [];
    $scope.searchType = "gstin";
    $scope.isInvoiceDate = "0";
    //$scope.branchid = "";
    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event,id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
    //$scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    //$scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
     var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
     
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.pdfHeader = "";

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.type = 'TODAY';
    $scope.getGstin = function () {
        gstreportservice.getGstin().then(function (response) {
            $scope.gstinList = response.data;  
        }, function () {

        });
    }

    $scope.validateGSTR3BMsg = "";
    $scope.validateGSTR3B = function () {
        $.LoadingOverlay("show");
        gstreportservice.validateGSTR3B().then(function (response) {
            $scope.validateGSTR3BMsg = response.data[0];
            $scope.redirectUrl = response.data[1];
            $.LoadingOverlay("hide");
            if ($scope.validateGSTR3BMsg != "Ok") {
                ShowConfirmMsgWindow();
            }
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.init = function () {
        $.LoadingOverlay("show");
        $scope.validateGSTR3B();
        $scope.getGstin();
        gstreportservice.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;                
            }
            $scope.selectedGstin = {
                gsTinNumber: $scope.instance.gsTinNo,
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.gstr3b();

        }, function () {
            $.LoadingOverlay("hide");
        });
        gstreportservice.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }
     
    $scope.clearSearch = function () {
        
        $scope.search.select1 = "";
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.gstr3b();

    }
    $scope.changeFilter = function () {

    }
  
    $scope.gstr3b = function () {
        if ($scope.validateGSTR3BMsg != "Ok") {
            ShowConfirmMsgWindow();
        }
        else {
            filter = 1;
            $.LoadingOverlay("show");
            var data = {
                fromDate: $scope.from,
                toDate: $scope.to
            }
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            // Added by Gavaskar 17-08-2017  Branch Select issue Start
            if ($scope.selectedGstin == undefined) {
                $scope.gsTinNo = "";
            }
            else {
                $scope.gsTinNo = $scope.selectedGstin.gsTinNumber;
            }
            // Added by Gavaskar 17-08-2017  Branch Select issue End
            gstreportservice.gst3b($scope.type, data, filter, $scope.branchid, $scope.searchType, $scope.gsTinNo, $scope.isInvoiceDate).then(function (response) {
                $scope.data = response.data;
                $scope.type = "";

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    gstreportservice.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                        $scope.pdfHeader.tinNo = "";
                    else
                        $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "GSTR_3B.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "GSTR_3B.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    height: 350,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [

                     { field: "col0", title: "1", width: "100px", attributes: { class: "text-left field-report" } },
                     { field: "col1", title: "1", width: "200px", attributes: { class: "text-left field-report" } },
                     { field: "col2", title: "2", width: "100px", attributes: { class: "text-left field-report" } },

                     { field: "col3", title: "3", width: "80px", attributes: { class: "text-left field-report" } },

                     { field: "col4", title: "4", width: "100px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },
                     { field: "col5", title: "5", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                     { field: "col6", title: "6", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                     { field: "isHeader", title: "header", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },

                    ],
                    dataSource: {
                        data: response.data,

                        schema: {
                            model: {
                                fields: {
                                    "col1": { type: "string" },
                                    "col2": { type: "string" },
                                    "col3": { type: "string" },
                                    "col4": { type: "string" },
                                    "col5": { type: "string" }
                                }
                            }
                        },
                        pageSize: 100
                    },
                    dataBinding: function () {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    },
                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                var cell = row.cells[ci];
                                cell.wrap = true;
                                cell.height = "15px";
                                cell.border = 1;
                                cell.borderColor = "#ddd9c4"
                                if (row.type == "header" && i == 2) {
                                    cell.value = "";
                                    cell.background = "#ffffff";
                                }
                                if (row.type == "data" && sheet.rows[i].cells[7].value == "1" && this.columns[ci].field != "col0") {
                                    cell.background = "#8db4e2";
                                    cell.fontSize = "11";
                                    cell.bold = true;
                                    cell.width = 1000;

                                }
                                else if (row.type == "data" && sheet.rows[i].cells[7].value == "2" && this.columns[ci].field != "col0") {
                                    cell.background = "#ddd9c4";
                                    cell.fontSize = "11";
                                    cell.bold = true;
                                    cell.hAlign = "center";
                                }
                                else if (row.type == "data" && sheet.rows[i].cells[7].value == "0" && this.columns[ci].field != "col0") {
                                    cell.fontSize = "11";
                                    cell.wrap = true;
                                }
                                if (row.type == "data" && sheet.rows[i].cells[7].value == "3") {
                                    cell.colSpan = 8;
                                    cell.bold = true;
                                }

                                if (this.columns[ci].field == "isHeader") {
                                    cell.value = "";
                                }


                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    }

    //$scope.salesReport();

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "TODAY") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        $scope.gstr3b();
    };

    // chng-3

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        var headerCell = { cells: [{ value: "Month & Year: " +$filter('date')($scope.to, 'MMM-yyyy') , bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        //headerCell = { cells: [{ value: " [ See Rule 61(5)]", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        //e.workbook.sheets[0].rows.splice(0, 0, headerCell);
      
        headerCell = { cells: [{ value: "Form GSTR-3B", bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    function ShowConfirmMsgWindow() {
        var data = {
            msgTitle: "",
            msg: $scope.validateGSTR3BMsg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: $scope.redirectUrl,
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                return false;
            });
        });
    }

}]);

