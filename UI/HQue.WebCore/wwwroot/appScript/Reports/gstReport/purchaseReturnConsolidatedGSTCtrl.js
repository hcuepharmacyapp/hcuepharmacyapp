app.controller('purchaseReturnConsolidatedGSTCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'gstreportservice', 'vendorReturnModel', 'vendorReturnItemModel', '$filter', function ($scope, $rootScope, $http, $interval, $q, gstreportservice, vendorReturnModel, vendorReturnItemModel, $filter) {
    var vendorReturnItem = vendorReturnItemModel;
    var vendorReturn = vendorReturnModel;
    $scope.search = vendorReturnItem;
    $scope.search.vendorReturn = vendorReturn;
    $scope.list = []; //---
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.type = "";
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };


    $scope.type = '';



    $scope.init = function () {
        $.LoadingOverlay("show");
        gstreportservice.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.salesReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        gstreportservice.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }


    $scope.salesReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined //$scope.instance.id;
        }

        gstreportservice.purchaseReturnConsolidatedGSTList($scope.type, data, $scope.branchid).then(function (response) {
            $scope.data = response.data;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);
            }
            $scope.type = "";
            $scope.list = response.data;
            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                gstreportservice.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>" + "Sales Consolidated Report - Tax Period From " + $filter("date")($scope.from, "dd/MM/yyyy") + " To " + $filter("date")($scope.to, "dd/MM/yyyy");
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/>  Sales Consolidated GST";
            }

            
            $("#grid").kendoGrid({
                excel: {
                    fileName: "PurchaseReturnConsolidatedGST.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "PurchaseReturnConsolidatedGST.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "returnNo", title: "Bill No.", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "string" } },
                  { field: "returnDate", title: "Bill Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(returnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", footerTemplate: "Total" },
                  { field: "gst0_PurchaseReturnValue", title: "0% Gross", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "gst0_Value", title: "0% GST", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "gst0_TotalPurchaseReturnValue", title: "0% Total", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "gst5_PurchaseReturnValue", title: "5% Gross", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "gst5_Value", title: "5% GST", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "gst5_TotalPurchaseReturnValue", title: "5% Total", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "gst12_PurchaseReturnValue", title: "12% Gross", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                      { field: "gst12_Value", title: "12% gst", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "gst12_TotalPurchaseReturnValue", title: "12% Total", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },

                        { field: "gst18_PurchaseReturnValue", title: "18% Gross", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "gst18_Value", title: "18% gst", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "gst18_TotalPurchaseReturnValue", title: "18% Total", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },

                       { field: "gst28_PurchaseReturnValue", title: "28% Gross", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "gst28_Value", title: "28% gst", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "gst28_TotalPurchaseReturnValue", title: "28% Total", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },

                       { field: "gstOther_PurchaseReturnValue", title: "Other Gross", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "gstOther_Value", title: "Other gst", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "gstOther_TotalPurchaseReturnValue", title: "Other Total", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                      { field: "finalValue", title: "Net Amount", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },

                ],
                dataSource: {
                    data: $scope.list,//response.data,
                    aggregate: [

                      { field: "gst0_PurchaseReturnValue", aggregate: "sum" },
                      { field: "gst0_Value", aggregate: "sum" },
                      { field: "gst0_TotalPurchaseReturnValue", aggregate: "sum" },

                     { field: "gst5_PurchaseReturnValue", aggregate: "sum" },
                      { field: "gst5_Value", aggregate: "sum" },
                      { field: "gst5_TotalPurchaseReturnValue", aggregate: "sum" },
                       { field: "finalValue", aggregate: "sum" },

                     { field: "gst28_Value", aggregate: "sum" },
                      { field: "gst28_PurchaseReturnValue", aggregate: "sum" },
                      { field: "gst28_TotalPurchaseReturnValue", aggregate: "sum" },

                      { field: "gst18_Value", aggregate: "sum" },
                      { field: "gst18_PurchaseReturnValue", aggregate: "sum" },
                      { field: "gst18_TotalPurchaseReturnValue", aggregate: "sum" },

                      { field: "gst12_Value", aggregate: "sum" },
                      { field: "gst12_PurchaseReturnValue", aggregate: "sum" },
                      { field: "gst12_TotalPurchaseReturnValue", aggregate: "sum" },

                     { field: "gstOther_PurchaseReturnValue", aggregate: "sum" },
                      { field: "gstOther_Value", aggregate: "sum" },
                      { field: "gstOther_TotalPurchaseReturnValue", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                                "returnNo": { type: "string" },
                                "returnDate": { type: "date" },
                                "gst0_PurchaseReturnValue": { type: "number" },
                                "gst0_Value": { type: "number" },
                                "gst0_TotalPurchaseReturnValue": { type: "number" },
                                "gst5_PurchaseReturnValue": { type: "number" },
                                "gst5_Value": { type: "number" },
                                "gst5_TotalPurchaseReturnValue": { type: "number" },
                                "gst12_PurchaseReturnValue": { type: "number" },
                                "gst12_Value": { type: "number" },
                                "gst12_TotalPurchaseReturnValue": { type: "number" },
                                "gst18_PurchaseReturnValue": { type: "number" },
                                "gst18_Value": { type: "number" },
                                "gst18_TotalPurchaseReturnValue": { type: "number" },
                                "gst28_PurchaseReturnValue": { type: "number" },
                                "gst28_Value": { type: "number" },
                                "gst28_TotalPurchaseReturnValue": { type: "number" },
                                "gstOther_PurchaseReturnValue": { type: "number" },
                                "gstOther_Value": { type: "number" },
                                "gstOther_TotalPurchaseReturnValue": { type: "number" },
                                "finalValue": { type: "number" }

                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            cell.fontFamily = "verdana";
                            cell.width = "200px";
                            cell.vAlign = "center";
                            if (row.type == "header") {

                                cell.fontSize = "10";
                                cell.width = "200";
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd/MM/yyyy');

                            }
                            if (row.type == "data")
                            { cell.fontSize = "8"; }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.fontSize = "8"
                                    cell.bold = true;
                                    cell.format = "#0.00";

                                }

                            }
                        }
                    }

                    //e.preventDefault();
                    promises[0].resolve(e.workbook);
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //

    //$scope.salesReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.salesReport();
    }

    // chng-3

    $scope.header = "Elixir Softlab Solutions";
    function addHeader(e) {


        var clen = e.workbook.sheets[0].rows[0].cells.length;
        var headerCell = null;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);

            headerCell = { cells: [{ value: "Tax Period From " + $filter("date")($scope.from, "dd/MM/yyyy") + " To " + $filter("date")($scope.to, "dd/MM/yyyy"), format: "dd/MM/yyyy", type: "date", bold: true, vAlign: "center", textAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            //headerCell = { cells: [{ value: "Tax Period From*: " + $scope.from, format: "dd/MM/yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            //e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "Sales Consolidated GST", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 15, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Sales Consolidated GST", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }

    }


    function addSearch(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Tax Period To*:  " + $scope.to, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: "Tax Period From*: " + $scope.from, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "Dealer GSTIN No:*: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);


    }
    //

    //
    var promises = [
    $.Deferred(),
    $.Deferred()
    ];

    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');
        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);