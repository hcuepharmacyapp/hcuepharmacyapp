app.controller('gstr1SalesReturnCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'gstreportservice', 'userAccessModel', '$filter', 'ModalService', function ($scope, $rootScope, $http, $interval, $q, gstreportservice, userAccessModel, $filter, ModalService) {

 
    var sno = 0;
    $scope.currentInstance = null;
    $scope.gstinList = [];
    $scope.searchType = "gstin";
    //$scope.branchid = "";
    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event,id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
    //$scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    //$scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
     var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
     
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.pdfHeader = "";

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.type = 'TODAY';
    $scope.getGstin = function () {
        gstreportservice.getGstin().then(function (response) {
            $scope.gstinList = response.data;  
        }, function () {

        });
    }

    $scope.validateGSTR3BMsg = "Ok";
    $scope.validateGSTR3B = function () {
        $.LoadingOverlay("show");
        gstreportservice.validateGSTR3B().then(function (response) {
            $scope.validateGSTR3BMsg = response.data[0];
            $scope.redirectUrl = response.data[1];
            $.LoadingOverlay("hide");
            if ($scope.validateGSTR3BMsg != "Ok") {
                ShowConfirmMsgWindow();
            }
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.init = function (val) {
        $scope.exportHeader = val;
        $.LoadingOverlay("show");
        //$scope.validateGSTR3B();
        $scope.getGstin();
        gstreportservice.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;                
            }
            $scope.selectedGstin = {
                gsTinNumber: $scope.instance.gsTinNo,
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.gstr3b();

        }, function () {
            $.LoadingOverlay("hide");
        });
        gstreportservice.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }
     
    $scope.clearSearch = function () {
        
        $scope.search.select1 = "";
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.gstr3b();

    }
    $scope.changeFilter = function () {

    }
  
    $scope.gstr3b = function () {
        if ($scope.validateGSTR3BMsg != "Ok") {
            ShowConfirmMsgWindow();
        }
        else {
            filter = 1;
            $.LoadingOverlay("show");
            var data = {
                fromDate: $scope.from,
                toDate: $scope.to
            }
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            
            if ($scope.selectedGstin == undefined) {
                $scope.gsTinNo = "";
            }
            else {
                $scope.gsTinNo = $scope.selectedGstin.gsTinNumber;
            }
            setFileName();
            gstreportservice.gstr1salesreturn($scope.type, data, filter, $scope.branchid, $scope.searchType, $scope.gsTinNo).then(function (response) {
                $scope.data = response.data;
                $scope.type = "";

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    gstreportservice.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                        $scope.pdfHeader.tinNo = "";
                    else
                        $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: ($scope.exportHeader == "True") ? "GSTR4_SalesReturn.xlsx" : "GSTR1_SalesReturn.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "GSTR1_SalesReturn.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    height: 450,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                     { field: "invoiceDate", title: "Return Date", width: "100px", type: "date", attributes: { class: "text-left field-highlight", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                     { field: "invoiceNumber", title: "Return Number", width: "120px", attributes: { class: "text-left field-report" } },

                    // { field: "recipientName", title: "Name of Recipient", width: "200px", attributes: { class: "text-left field-report" } },
                     { field: "recipientGSTin", title: "GSTIN/UIN of Recipient", width: "100px", attributes: { class: "text-left field-report" }, footerTemplate: "Grand Total" },
                    { field: "valueWithoutTax", title: "Taxable Value", width: "120px", format: "{0:n3}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n3')#</div>" },
                    { field: "placeOfSupply", title: "Place Of Supply", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                    { field: "gstTotal", title: "GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "igstAmount", title: "IGST Amount", width: "70px", format: "{0:n}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n0')#<span>.00</span></div>" },
                    { field: "cgstAmount", title: "CGST Amount", width: "70px", format: "{0:n3}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n3')#</div>" },
                    { field: "sgstAmount", title: "SGST/UTGST Amount", width: "70px", format: "{0:n3}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n3')#</div>" },
                    { field: "cess", title: "CESS Amount", width: "100px", format: "{0:n}", attributes: { class: "text-right field-report" } },

                       //{ field: "quantity", title: "Quantity", width: "100px", format: "{0:n0}", type: "number", attributes: { class: "text-right field-report" } },
                        { field: "returnCharges", title: "Return Charges", width: "70px", format: "{0:n3}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n3')#</div>" },
                      { field: "invoiceValue", title: "Return Value", width: "100px", format: "{0:n2}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n2')#</div>" },
                   
                // { field: "productName", title: "Product", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                //{ field: "batchNo", title: "Batch Number", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                //{ field: "exipryDate", title: "Expiry Date", width: "100px", type: "date", attributes: { class: "text-left field-highlight", ftype: "date", fformat: "MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(exipryDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                  { field: "branchName", title: "Branch Name", width: "150px", attributes: { class: "text-left field-report" } },
                      { field: "branchGSTin", title: "Branch GSTIN", width: "100px", attributes: { class: "text-left field-report" } },

                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [

                      { field: "valueWithoutTax", aggregate: "sum" },
                      { field: "igstAmount", aggregate: "sum" },
                      { field: "cgstAmount", aggregate: "sum" },
                      { field: "sgstAmount", aggregate: "sum" },
                      { field: "taxAmount", aggregate: "sum" },
                      { field: "cess", aggregate: "sum" },
                     { field: "invoiceValue", aggregate: "sum" },
                      { field: "returnCharges", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "recipientName": { type: "string" },
                                    "recipientGSTin": { type: "string" },
 
                                    "invoiceNumber": { type: "string" },
                                    "valueWithoutTax": { type: "number" },
                                    "invoiceValue": { type: "number" },
                                    "igstAmount": { type: "number" },
                                    "cgstAmount": { type: "number" },
                                    "sgstAmount": { type: "number" },
                                    "taxAmount": { type: "number" },
                                    "returnCharges": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },
                    dataBinding: function () {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    },
                    excelExport1: function(e) {
                        // Prevent the default behavior which will prompt the user to save the generated file.
                        e.preventDefault();
                        // Get the Excel file as a data URL.
                        var dataURL = new kendo.ooxml.Workbook(e.workbook).toDataURL();
                        // Strip the data URL prologue.
                        var base64 = dataURL.split(";base64,")[1];
                        // Post the base64 encoded content to the server which can save it.
                        $.post("/server/save", {
                            base64: base64,
                            fileName: "ExcelExport.xlsx"
                        });
                    },
                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];
                                //added by nandhini for excel s.no
                                var cell = row.cells[ci];
                            
                               
                                //end
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    }
    $scope.order = ['invoiceDate1', 'invoiceNumber', 'recipientGSTin', 'valueWithoutTax', 'placeOfSupply', 'gstTotal', 'igstAmount', 'cgstAmount', 'sgstAmount', 'cess', 'invoiceValue',
'branchName', 'branchGSTin'];
    //$scope.salesReport();

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "TODAY") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        $scope.gstr3b();
    };

    // chng-3

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        var headerCell = { cells: [{ value: "Month & Year: " +$filter('date')($scope.to, 'MMM-yyyy') , bold: true, vAlign: "center", textAlign: "left", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        //headerCell = { cells: [{ value: " [ See Rule 61(5)]", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        //e.workbook.sheets[0].rows.splice(0, 0, headerCell);
      
        headerCell = { cells: [{ value: ($scope.exportHeader == "True") ? "GSTR 4- Sales Return" : "GSTR 1- Sales Return", bold: true, fontSize: 20, vAlign: "center", hAlign: "center", textAlign: "left", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    function ShowConfirmMsgWindow() {
        var data = {
            msgTitle: "",
            msg: $scope.validateGSTR3BMsg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: $scope.redirectUrl,
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                return false;
            });
        });
    }
    setFileName = function () {
        $scope.fileName = ($scope.exportHeader == "True") ? "GSTR4_SalesReturn_" : "GSTR1_SalesReturn_" + $scope.instance.name;
    }
}]);

