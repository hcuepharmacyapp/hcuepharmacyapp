app.controller('hierarchyReportCtrl', ["$scope", "hierarchyService", "$filter", function ($scope, hierarchyService, $filter) {

    $scope.data = [];
    $scope.instanceList = [];

    $scope.OrderProductList = [];

    document.getElementById("btnOrder").disabled = true;

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.selectedInstance = function () {
        $scope.selectedInstance;
    };

    $scope.instance = function () {
        hierarchyService.selectInstance().then(function (response) {
            $scope.instanceList = response.data;
            $scope.selectedInstance.id = $scope.instanceList[0].id;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.instance();
    $scope.init = function () {
        $.LoadingOverlay("show");
        hierarchyService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $scope.lowProductStock();
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });
        hierarchyService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function (error) { console.log(error); });
    };
    $scope.changeInstance = function () {
        $scope.OrderProductList = [];
    };
    $scope.lowProductStock = function () {
        $.LoadingOverlay("show");
        var data = {
            "fromDate": $scope.from,
            "toDate": $scope.to
        };
        hierarchyService.list($scope.instance.id).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                $scope.pdfHeader = $scope.instance;
            }
            else {
                hierarchyService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function (error) { console.log(error); });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: "Low Product Stock.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Low Product Stock.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()

                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                      { field: "isOrderRequired", template: "<input type='checkbox' name='isOrderRequired' value='true' ng-model='isOrderRequired' class='isOrderRequired' />", title: "Order Rquired", width: "110px", attributes: { class: "text-left field-report" } },

                  { field: "product.name", title: "Product", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-highlight" } },


                   { field: "batchNo", title: "Batch No", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-highlight" } },


                  { field: "stock", title: "Stock", width: "120px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" }
                ],

                dataBound: function () {
                    $(".isOrderRequired").bind("change", function (e) {
                        $(e.target).closest("tr").toggleClass("k-state-selected");
                        var ind = $(e.target).closest("tr").index();
                        var rowIndex = $(this).parent().index();
                        var cellIndex = $(this).parent().parent().index();
                        grid = $("#grid").data("kendoGrid");

                        var productid = grid._data[ind].product.id;

                        var OrderProduct = $filter("filter")($scope.data, { "product": { "id": productid } })[0];




                        if (jQuery(e.target).is(":checked") == true) {

                            grid._data[cellIndex].isOrderRequired = 1;

                            $scope.OrderProductList.push(OrderProduct);

                        } else {
                            grid._data[cellIndex].isOrderRequired = null;

                            var OrderProductIndex = $scope.OrderProductList.indexOf(OrderProduct);

                            if (OrderProductIndex > -1) {

                                $scope.OrderProductList.splice(OrderProductIndex, 1);
                            }

                        }


                        if ($scope.OrderProductList.length == 0) {
                            document.getElementById("btnOrder").disabled = true;
                        } else {
                            document.getElementById("btnOrder").disabled = false;
                        }

                    });
                },
                dataSource: {
                    data: response.data,
                    aggregate: [
                        //{ field: "product.name", aggregate: "sum" },
                        { field: "stock", aggregate: "sum" }

                    ],
                    schema: {
                        model: {
                            fields: {
                                "isOrderRequired": { type: "number" },
                                "product.name": { type: "string" },
                                "batchNo": { type: "string" },
                                "stock": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];


                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                }
            });


            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });
    };

    $scope.order = ['product.name', 'batchNo', 'stock'];
   
    
    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Low Product Stock", bold: true, vAlign: "center", hAlign: "center", colSpan: clen }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }
    $scope.Order = function () {
        var object = { "instanceId": $scope.selectedInstance.id, "OrderList": $scope.OrderProductList };
        localStorage.setItem("OrderList", JSON.stringify(object));
        window.location.assign("/VendorOrder/Index");
    };
}]);