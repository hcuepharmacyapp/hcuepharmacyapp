app.controller('expireProductReportCtrl', ['$scope', 'uiGridConstants', '$http', '$interval', '$q', 'expireProductReportService', 'productStockModel', 'productModel', 'toastr', 'cacheService', '$filter', 'ModalService', '$rootScope', function ($scope, uiGridConstants, $http, $interval, $q, expireProductReportService, productStockModel, productModel, toastr, cacheService, $filter, ModalService, $rootScope) {

    $scope.data = [];
    $scope.instanceList = [];

    //$scope.tempids = [];

    $scope.isExpiry = false;
    $scope.gridLength = 0;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };



    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
    };

    $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');


    $scope.gridOptions = {
        exporterMenuCsv: true,
        enableGridMenu: true,
        enableFiltering: true,
        enableColumnResizing: true,
        //gridMenuTitleFilter: fakeI18n,
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        data: 'data',
        columnDefs: [

             {
                 name: 'Moving Stock',
                 field: 'isMovingStockExpire',
                 filter: {
                     type: uiGridConstants.filter.SELECT,
                     selectOptions: [
                       { value: false, label: 'Unselect' },
                       { value: true, label: 'Selected' }
                     ]
                 }, width: '7%',
                 cellTemplate: '<input type="checkbox" name="select_item" value="true" ng-model="row.entity.isMovingStockExpire"/>'
             },

          { field: 'product.name', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Product Name', width: '15%' },
          { field: 'vendor.name', displayName: 'Vendor Name', width: '15%' },
          { field: 'stock', displayName: 'Stock', width: '10%' },
          { field: 'expireDate', displayName: 'Expire Date', cellFilter: 'date:"dd-MM-yyyy"', width: '10%', type: 'date' },
        ],

        gridMenuCustomItems: [
            {
                title: 'Rotate Grid',
                action: function ($event) {
                    this.grid.element.toggleClass('rotated');
                },
                order: 210
            }
        ],

        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;

            gridApi.core.on.columnVisibilityChanged($scope, function (changedColumn) {
                $scope.columnChanged = { name: changedColumn.colDef.name, visible: changedColumn.colDef.visible };
            });
        }
    };

    $scope.selectedInstance = function () {
        $scope.selectedInstance;
    }

    $scope.instance = function () {
        $.LoadingOverlay("show");
        expireProductReportService.selectInstance().then(function (response) {
            $scope.instanceList = response.data;
            $scope.selectedInstance.id = $scope.instanceList[0].id;
            $scope.expireProductReport();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    //$scope.instance();


    $rootScope.$on("close", function () {
        //$scope.expireProductReport();
    });

    //$scope.expireProductReport = function () {
    //    $.LoadingOverlay("show");
    //    expireProductReportService.list($scope.selectedInstance.id).then(function (response) {
    //        $scope.data = response.data;
    //        $.LoadingOverlay("hide");
    //    }, function () {
    //        $.LoadingOverlay("hide");
    //    });
    //}

    // chng-2

    $scope.init = function () {
        $.LoadingOverlay("show");
        expireProductReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;

            $scope.expireProductReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        expireProductReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.searchName = function (name) {
        $("#grid").data("kendoGrid").dataSource.filter({
            filters: [
                {
                    field: "product.name",
                    operator: "contains",
                    value: name
                }
            ]
        });
        $scope.gridLength = $("#grid").data("kendoGrid").dataSource.view();
        
        //$scope.usingservice = $filter('findobj')(grid._data, grid.tbody[0].rows); // No Where used this object
    };
    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }

    }

    $scope.checkFromDate = function () {
        var dt = $("#fromDate1").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if ($scope.toDate != undefined && $scope.toDate != null) {
                    $scope.checkToDate1($scope.fromDate, $scope.toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var dt = $("#toDate1").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1($scope.fromDate, $scope.toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    var grid = "";
    $scope.expireProductReport = function () {
        $.LoadingOverlay("show");

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        $scope.isExpiry = false;
        if ($scope.branchid == $scope.instance.id)
        {
            $scope.isExpiry = true;
        }
        var data = {
            fromDate: $scope.fromDate,
            toDate: $scope.toDate
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.fromDate;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        $scope.setFileName();
        expireProductReportService.list($scope.branchid,data).then(function (response) {
            $scope.data = response.data;
            $scope.gridLength = response.data;
            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                expireProductReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: "Expire Product.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Expire_Product.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                //pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                    { field: "isMovingStockExpire", template: "<input type='checkbox' name='isMovingStockExpire' value='true' ng-model='isMovingStockExpire' class='isMovingStock' />", title: "Expire-Moving Stock", width: "110px", attributes: { class: "text-left field-report" } },

                  { field: "product.name", title: "Product", width: "140px", type: "string", attributes: { class: "text-left field-highlight" } },
                  { field: "vendor.name", title: "Vendor", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "expireDate", title: "Expiry", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                { field: "stock", title: "Stock", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" }
                ],

                dataBound: function () {
                    $(".isMovingStock").bind("change", function (e) {
                        $(e.target).closest("tr").toggleClass("k-state-selected");
                        var ind = $(e.target).closest("tr").index();

                        var rowIndex = $(this).parent().index();
                        var cellIndex = $(this).parent().parent().index();
                        grid = $("#grid").data("kendoGrid");

                        if (jQuery(e.target).is(":checked") == true) {
                            // response.data[ind].IsMovingStockExpire = 1;
                            grid._data[cellIndex].isMovingStockExpire = 1;
                           // $scope.saveMoving = true;
                            ///$scope.tempids.push(grid._data[cellIndex])
                        }

                        else
                        {
                            // response.data[ind].IsMovingStockExpire = null;
                            grid._data[cellIndex].isMovingStockExpire = null;
                           // $scope.saveMoving = false;
                            //$scope.expireProductReport();
                        }
                            

                    });
                },

                dataSource: {
                    data: response.data,
                    aggregate: [
                        
                        { field: "stock", aggregate: "sum" }

                    ],
                    schema: {
                        model: {
                            fields: {
                                "isMovingStockExpire": { type: "number" },
                                "product.name": { type: "string" },
                                "vendor.name": { type: "string" },
                                "stock": { type: "number" },
                                "expireDate": { type: "date" }
                            }
                        }
                    },
                    //pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    //End Chng 2

    //$scope.expireProductReport();
   // $scope.saveMoving = false;

    $scope.SaveMovingStock = function () {
        $.LoadingOverlay("show");

        if (grid._data == undefined) {
            toastr.error('Please select the Expired Moving Stock');
            $scope.expireProductReport();
            $.LoadingOverlay("hide");
            return false;
        }
        $scope.searchName('');
        var updateGrid = [];
        for (var item = 0; item < grid._data.length; item++) {
            if (grid._data[item].isMovingStockExpire == 1) {
                updateGrid.push(grid._data[item]);
            }
        }
        expireProductReportService.movingStock(updateGrid.valueOf()).then(function (response) {
            //$scope.data = response.data;
            
            //if ($scope.saveMoving == true)
            //{
                $scope.PopupVendors();
                //window.location.href = '/VendorPurchaseReturn/ExpireReturnIndex';
                //toastr.success('Expire Moving Stock updated successfully');
                $.LoadingOverlay("hide");
            //}
            //else
            //{
            //    toastr.success('Please select the Expire Moving Stock');
            //    $scope.expireProductReport();
            //    // $scope.init();
            //    $.LoadingOverlay("hide");
            //}

           
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['product.name', 'vendor.name', 'reportInvoiceDate', 'stock'];
    $scope.PopupVendors = function () {
        var allData = grid._data;
        var popupData = [];
        for (var i = 0; i < allData.length; i++)
        {
            if (allData[i].isMovingStockExpire == true && (allData[i].vendor.name == null || allData[i].vendor.name == ''))
            {
                allData[i].vendorid = "";
                popupData.push(allData[i]);

            }
        }
        if (popupData.length == 0)
        {
            window.location.href = '/VendorPurchaseReturn/ExpireReturnIndex';
            return false;
        }
        else
        {
            cacheService.set('selectedItems', popupData)
            var m = ModalService.showModal({
                controller: "selectVendorsCtrl",
                templateUrl: "selectVendors"
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.message = "You said " + result;
                });
            });
            return false;
        }
        

    };

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Expire Product", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }
    $scope.setFileName = function () {
        $scope.fileNameNew = "EXPIRY PRODUCTS_" + $scope.instance.name;
    }
    //
}]);

app.filter('findobj', function () {

    return function (list, rows) {

        return list.filter(function (element) {
            if (element.isMovingStockExpire == 1) {
                if (rows[list.indexOf(element)].dataset.uid == element.uid) {
                    $("[data-uid='" + rows[list.indexOf(element)].dataset.uid + "']").addClass("k-state-selected");;
                    $("[data-uid='" + rows[list.indexOf(element)].dataset.uid + "'] .isMovingStock").prop('checked', true);
                    return true;
                }
            }
        });
    };
})