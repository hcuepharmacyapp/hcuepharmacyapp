app.factory('expireProductReportService', function ($http) {
    return {
        selectInstance: function () {
            return $http.post('/expireProductReport/SelectInstance')
        },
//date parameter added by nandhini 13-9-17
        list: function (id, data) {
            return $http.post('/expireProductReport/expireProductList?id=' + id, data);
        },
        movingStock: function (productStock) {
            return $http.post('/expireProductReport/UpdateIsMovingStockExpire', productStock);
        },
        getInstanceData: function () {
            return $http.post('/expireProductReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },
        // Newly Added Gavaskar 01-11-2016
        vendorData: function () {
            return $http.get('/VendorData/VendorList');
        },
        vendorUpdate: function (productStock) {
            return $http.post('/expireProductReport/vendorUpdate', productStock);
        },
    }
});
