app.factory('supplierReportService', function ($http) {
    return {
        list: function (type,data) {
            return $http.post('/supplierReport/list?type=' + type, data);
        },
        supplierWiseBalanceList: function (instanceid) {
            return $http.post('/supplierReport/supplierWiseBalanceList?instanceId=' + instanceid);
        },
        //supplierDetailsList: function (data, instanceid) {
        //    return $http.post('/supplierReport/supplierDetailsList?instanceId=' + instanceid, data);
        //},
        supplierDetailsList: function (data,instanceid) {
            return $http.post('/supplierReport/supplierDetailsList?instanceId=' + instanceid, data);
        },
        supplierLedgerDetails: function (data, instanceid) {
            return $http.post('/supplierReport/supplierLedgerDetails?instanceId=' + instanceid, data);
        },
        getSupplierName: function (name, mobile) {
            return $http.get('/supplierReport/getSupplierName?supplier=' + name + '&mobile=' + mobile);
        },
        //getSupplierMobile: function (name, mobile) {
        //    return $http.get('/supplierReport/getCustomerName?supplier=' + name + '&mobile=' + mobile);
        //},
        supplierWisePurchaseReturnDetail: function (data, IsInvoiceDate) {
            return $http.post('/supplierReport/supplierWisePurchaseReturnDetail?IsInvoiceDate=' + IsInvoiceDate, data);
        },
        supplierPaymentDetails: function (data, instanceid) {
            return $http.post('/supplierReport/supplierPaymentDetails?instanceId=' + instanceid, data);
        },
        supplierWiseExpiryReturnDetail: function (data) {
            return $http.post('/supplierReport/supplierWiseExpiryReturnDetail', data);
        },
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },

        supplierWisePaymentReceiptCancellation: function (data, instanceid) {
            return $http.post('/supplierReport/supplierWisePaymentReceiptCancellationList?instanceId=' + instanceid, data);
        },
        
    }
});
