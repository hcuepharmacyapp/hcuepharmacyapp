app.controller('supplierWiseExpiryReturnCtrl', function ($scope, $rootScope, supplierReportService, paymentModel, toastr, $filter) {

    $scope.minDate = new Date();
    
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.fromDate = null;
    $scope.toDate = null;

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
    };

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.type = '';
    $scope.data = [];

    var payment = paymentModel;
    
    $scope.search = payment;
    $scope.list = [];
    $scope.vendorList = [];

    $scope.supplierList = true;
    //$scope.supplierWiseSearch = function () {
    //    $scope.totalDue = 0;
    //    $.LoadingOverlay("show");

    //    supplierReportService.supplierWiseBalanceList().then(function (response) {
    //        $scope.list = response.data;            
    //        $.LoadingOverlay("hide");
    //    }, function () {
    //        $.LoadingOverlay("hide");
    //    });
    //}
    //$scope.supplierWiseSearch();
    
    $scope.init = function () {
        $.LoadingOverlay("show");
        supplierReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
        }, function () {
            $.LoadingOverlay("hide");
        });
        supplierReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }

    }

    $scope.checkFromDate = function () {
        var dt = $("#fromDate").val();
        if (dt)
        {
        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if ($scope.toDate != undefined && $scope.toDate != null) {
                    $scope.checkToDate1($scope.fromDate, $scope.toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
        }
        else
            $scope.validFromDate = true;
    }

    $scope.checkToDate = function () {
        var dt = $("#toDate").val();

        if (dt)
        {
        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1($scope.fromDate, $scope.toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
        }
        else
            $scope.validToDate = true;
    }

    
    $scope.changefilters = function () {       
        $scope.search.values = "";

        $scope.search.supplier = undefined;
        if ($scope.search.select == 'supplier') {
            $scope.selectMobile = false;
            $scope.selectSupplier = true;
            $scope.chkDate = true;
            
        } else if ($scope.search.select == 'mobile') {
            $scope.selectMobile = true;
            $scope.selectSupplier = false;
            $scope.chkDate = true;
        }       
        else {
            $scope.selectMobile = false;
            $scope.selectSupplier = false;
            $scope.chkDate = false;
            $scope.cancel();
        }
    };

    function isEmpty(value) {
        return (typeof value !== undefined || value !== null || value!=="");
    }
    $scope.supplierDetailsReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.fromDate,
            toDate: $scope.toDate
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        else {
            $scope.search.instanceId = $scope.branchid;
        }
        $scope.search.fromDate = $scope.fromDate;
        $scope.search.toDate = $scope.toDate;
        $scope.search.supplier = $scope.search.supplier;
        $scope.search.mobile = $scope.search.mobile;

        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.fromDate;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);

        if ($scope.search.supplier != undefined || $scope.search.mobile != undefined) {

            supplierReportService.supplierWiseExpiryReturnDetail($scope.search).then(function (response) {
                $scope.data = response.data;
                $scope.supplierList = false;



                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    supplierReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                }


                var grid = $("#grid").kendoGrid({
                    excel: {
                        fileName: "Expiry Return.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1600, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Expiry_Return.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                    { field: "vendorpurchaseReturnReport.productName", title: "Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                     { field: "vendorpurchaseReturnReport.batchNo", title: "Batch No", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                     //{ field: "returnNo", title: "Return No", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                     //{ field: "returnDate", title: "Return Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(returnDate), 'dd/MM/yyyy') #" },
                    

                      { field: "vendorpurchaseReturnReport.stock", title: "Qty", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "vendorpurchaseReturnReport.costPrice", title: "Cost Price", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                       { field: "vendorpurchaseReturnReport.sellingPrice", title: "Selling Price", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "vendorpurchaseReturnReport.vat", title: "VAT", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "vendorpurchaseReturnReport.vatAmount", title: "Vat Amount", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "<span style='float:left;'>Total:</span><div style='float:left;margin-left:3px;' class='report-footer'>#= kendo.toString(sum, 'n0') #.00</div>" },
                     { field: "reason", title: "Reason", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                     { field: "vendorpurchaseReturnReport.total", title: "Final Value", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "<span style='float:left;'>Total:</span><div style='float:left;margin-left:3px;' class='report-footer'>#= kendo.toString(sum, 'n0') #.00</div>" },

                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [                            
                             { field: "vendorpurchaseReturnReport.vatAmount", aggregate: "sum" },
                             { field: "vendorpurchaseReturnReport.total", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    //"returnNo": { type: "string" },
                                    //"returnDate": { type: "date" },                                   
                                    "vendorpurchaseReturnReport.batchNo": { type: "string" },
                                    "vendorpurchaseReturnReport.productName": { type: "string" },
                                    "vendorpurchaseReturnReport.stock": { type: "number" },
                                    "vendorpurchaseReturnReport.costPrice": { type: "number" },
                                    "vendorpurchaseReturnReport.sellingPrice": { type: "number" },
                                    "vendorpurchaseReturnReport.vat": { type: "number" },
                                    "vendorpurchaseReturnReport.vatAmount": { type: "number" },
                                    "reason": { type: "string" },
                                    "vendorpurchaseReturnReport.total": { type: "number" },
                                    
                                }
                            }
                        },
                        pageSize: 20
                    },
                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },

                }).data("kendoGrid");


                grid.dataSource.originalFilter = grid.dataSource.filter;
                grid.dataSource.filter = function () {
                    if (arguments.length > 0) {
                        this.trigger("filtering", arguments);
                    }

                    var result = grid.dataSource.originalFilter.apply(this, arguments);
                    if (arguments.length > 0) {
                        this.trigger("filtering", result);
                    }

                    return result;
                }

                $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    var filters = dataSource.filter();
                    var allData = dataSource.data();
                    var query = new kendo.data.Query(allData);
                    var data = query.filter(filters).data;

                });

                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        else {
            $.LoadingOverlay("hide");
        }
        

    }
   
    $scope.filter = function (type) {
        $scope.type = type;
        $scope.supplierDetailsReport();
    };
    
    $scope.cancel = function () {
        $scope.fromDate = null;
        $scope.toDate = null;
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;
        $scope.search.select = null;
        $scope.search.supplier = null;
        $scope.selectSupplier = false;
        $scope.selectMobile = false;
        $scope.supplierList = true;
        $scope.chkDate = false;
        $scope.search.mobile = null;
        $scope.search.supplier = null;
        //$scope.supplierDetailsReport();

    }
    
    $scope.onSupplierSelect = function (obj) {       
        $scope.search.supplier = obj.name;
        $scope.search.mobile = obj.mobile;
        $scope.search.vendorId = obj.id;        
    }

    $scope.getSupplierName = function (val) {
        return supplierReportService.getSupplierName(val, '').then(function (response) {

            return response.data.map(function (item) {
                return item;
            });
            //var flags = [], output = [], l = response.data.length, i;
            //for (i = 0; i < l; i++) {
            //    if (flags[$filter('uppercase')(response.data[i].name) && (response.data[i].mobile)]) continue;
            //    flags[$filter('uppercase')(response.data[i].name) && (response.data[i].mobile)] = true;
            //    output.push(response.data[i]);
            //}
            //return output.map(function (item) {
            //    return item;
            //});
        });
    };

    //$scope.getSupplierName = function () {
    //    vendorService.vendorData().then(function (response) {
    //        $scope.vendorList = response.data;
    //    }, function () { toastr.error('Error Occured', 'Error'); });
    //}

    $scope.getSupplierMobile = function (val) {
        return supplierReportService.getSupplierName('', val).then(function (response) {

            return response.data.map(function (item) {
                return item;
            });
        });
    };

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Expiry Return", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }
    
});

app.filter('sumFilter', function () {
    return function (groups) {
        var totalDue = 0;
        for (i = 0; i < groups.length; i++) {
            totalDue = totalDue + groups[i].balance;
        };
        return totalDue;
    };
});
