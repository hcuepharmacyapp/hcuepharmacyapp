app.controller('journalReportTallyCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'gstreportservice', 'tallyreportservice', 'salesModel', 'salesItemModel', '$filter', function ($scope, $rootScope, $http, $interval, $q, gstreportservice, tallyreportservice, salesModel, salesItemModel, $filter) {
    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    $scope.list = []; //---
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.type = "";
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months
    $scope.data = [];
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };


    $scope.type = '';



    $scope.init = function () {
        $.LoadingOverlay("show");
        gstreportservice.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.typeFilter = "CustomerReceipts";
            //$scope.salesReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        gstreportservice.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.ChangeFilter = function (typeFilter) {

        if ($scope.typeFilter == 'CustomerReceipts') {
            $scope.typeFilter = "CustomerReceipts";
            $scope.journalReport();
        }
        if ($scope.typeFilter == 'VendorPayment') {
            $scope.typeFilter = "VendorPayment";
            $scope.journalReport();
        }
    }

    $scope.journalReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined //$scope.instance.id;
        }

        if ($scope.typeFilter == "CustomerReceipts" || $scope.typeFilter == "" || $scope.typeFilter == undefined) {

            tallyreportservice.journalList($scope.typeFilter, $scope.type, data, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                if ($scope.from != undefined || $scope.from != null) {
                    $scope.dayDiff($scope.from);
                }
                $scope.type = "";
                $scope.list = response.data;
                console.log('data', $scope.list)
                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    gstreportservice.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>" + "Sales Consolidated Report - Tax Period From " + $filter("date")($scope.from, "dd/MM/yyyy") + " To " + $filter("date")($scope.to, "dd/MM/yyyy");
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/>  Sales Tally Report";
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "JournalTallyReport.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "JournalTallyReport.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    height: 350,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                      { field: "voucherdate", title: "Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(voucherdate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                      { field: "voucherno", title: "Bill No.", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "string" } },
                      { field: "crledger", title: "Cr Ledger", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" } },
                      { field: "drledger", title: "Dr Ledger", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" } },
                      { field: "billType", title: "Bill Type", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" } },
                      { field: "billName", title: "Bill Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" }, footerTemplate: "Total" },
                      { field: "amount", title: "Amount", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                      { field: "paymentType", title: "Transaction Type", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report", ftype: "string" } },
                      { field: "cheque", title: "Cheque No", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report", ftype: "string" } },
                      { field: "chequeDate", title: "ChequeDate Date", width: "120px", format: "{0:n}", type: "string", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" } },
                     { field: "cardNo", title: "Card No", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report", ftype: "string" } },
                      { field: "cardDate", title: "Card Date", width: "120px", format: "{0:n}", type: "string", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" } },
                      { field: "remarks", title: "Remarks", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" } },
                    ],
                    dataSource: {
                        data: $scope.list,//response.data,
                        aggregate: [
                           { field: "amount", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "voucherno": { type: "string" },
                                    "voucherdate": { type: "date" },
                                    "crledger": { type: "string" },
                                    "drledger": { type: "string" },
                                    "billType": { type: "string" },
                                    "billName": { type: "string" },
                                    "amount": { type: "number" },
                                    "paymentType": { type: "string" },
                                    "Cheque": { type: "string" },
                                    "ChequeDate": { type: "date" },
                                    "CardNo": { type: "string" },
                                    "CardDate": { type: "date" },
                                    "Remarks": { type: "string" },
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];
                                cell.fontFamily = "verdana";
                                cell.width = "200px";
                                cell.vAlign = "center";
                                if (row.type == "header") {

                                    cell.fontSize = "10";
                                    cell.width = "200";
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd/MM/yyyy');

                                }
                                if (row.type == "data")
                                { cell.fontSize = "8"; }

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.fontSize = "8"
                                        cell.bold = true;
                                        cell.format = "#0.00";

                                    }

                                }
                            }
                        }

                        //e.preventDefault();
                        promises[0].resolve(e.workbook);
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });

        }

        if ($scope.typeFilter == "VendorPayment" || $scope.typeFilter == "" || $scope.typeFilter == undefined) {

            tallyreportservice.journalList($scope.typeFilter, $scope.type, data, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                if ($scope.from != undefined || $scope.from != null) {
                    $scope.dayDiff($scope.from);
                }
                $scope.type = "";
                $scope.list = response.data;
                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    gstreportservice.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>" + "Sales Consolidated Report - Tax Period From " + $filter("date")($scope.from, "dd/MM/yyyy") + " To " + $filter("date")($scope.to, "dd/MM/yyyy");
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/>  Sales Tally Report";
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "JournalTallyReport.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "JournalTallyReport.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    height: 350,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                      { field: "voucherdate", title: "Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(voucherdate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                      { field: "voucherno", title: "Bill No.", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "string" } },
                      { field: "crledger", title: "Cr Ledger", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" } },
                      { field: "drledger", title: "Dr Ledger", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" } },
                      { field: "billType", title: "Bill Type", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" } },
                      { field: "billName", title: "Bill Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" }, footerTemplate: "Total" },
                      { field: "amount", title: "Amount", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                      { field: "paymentType", title: "Transaction Type", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report", ftype: "string" } },
                      { field: "cheque", title: "Card No", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report", ftype: "string" } },
                      { field: "chequeDate", title: "Card Date", width: "120px", format: "{0:n}", type: "string", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" } },
                      { field: "Remarks", title: "Remarks", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report", ftype: "string" } },
                    ],
                    dataSource: {
                        data: $scope.list,//response.data,
                        aggregate: [
                           { field: "amount", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "voucherno": { type: "string" },
                                    "voucherdate": { type: "date" },
                                    "crledger": { type: "string" },
                                    "drledger": { type: "string" },
                                    "billType": { type: "string" },
                                    "billName": { type: "string" },
                                    "amount": { type: "number" },
                                    "paymentType": { type: "string" },
                                    "Cheque": { type: "string" },
                                    "ChequeDate": { type: "date" },
                                    "Remarks": { type: "string" },
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];
                                cell.fontFamily = "verdana";
                                cell.width = "200px";
                                cell.vAlign = "center";
                                if (row.type == "header") {

                                    cell.fontSize = "10";
                                    cell.width = "200";
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd/MM/yyyy');

                                }
                                if (row.type == "data")
                                { cell.fontSize = "8"; }

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.fontSize = "8"
                                        cell.bold = true;
                                        cell.format = "#0.00";

                                    }

                                }
                            }
                        }

                        //e.preventDefault();
                        promises[0].resolve(e.workbook);
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });

        }
    }

    //

    //$scope.salesReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.salesReport();
    }

    // chng-3

    $scope.header = "Elixir Softlab Solutions";
    function addHeader(e) {


        var clen = e.workbook.sheets[0].rows[0].cells.length;
        var headerCell = null;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);

            headerCell = { cells: [{ value: "Tax Period From " + $filter("date")($scope.from, "dd/MM/yyyy") + " To " + $filter("date")($scope.to, "dd/MM/yyyy"), format: "dd/MM/yyyy", type: "date", bold: true, vAlign: "center", textAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            //headerCell = { cells: [{ value: "Tax Period From*: " + $scope.from, format: "dd/MM/yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            //e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "Sales Consolidated GST", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 15, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Sales Consolidated GST", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }

    }


    function addSearch(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Tax Period To*:  " + $scope.to, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: "Tax Period From*: " + $scope.from, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "Dealer GSTIN No:*: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);


    }
    //

    //
    var promises = [
    $.Deferred(),
    $.Deferred()
    ];

    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');
        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);