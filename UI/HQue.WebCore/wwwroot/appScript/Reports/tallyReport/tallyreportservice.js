app.factory('tallyreportservice', function ($http) {
    return {
       
        salesList: function (type, data, instanceid) {
            return $http.post('/tallyReport/SalesReportTallyListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        salesreturnList: function (type, data, instanceid) {
            return $http.post('/tallyReport/SalesReturnReportTallyListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        purchaseList: function (type, data, instanceid) {
            return $http.post('/tallyReport/PurchaseReportTallyListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        purchaseReturnList: function (type, data, instanceid) {
            return $http.post('/tallyReport/PurchaseReturnReportTallyListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        journalList: function (filterType, type, data, instanceid) {
            return $http.post('/tallyReport/JournalListData?type=' + type + '&sInstanceId=' + instanceid + '&filterType=' + filterType, data);
        },
    }
});
