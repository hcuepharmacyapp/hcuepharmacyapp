app.controller('patientInformationCtrl', function ($scope, patientService, salesModel, pagerServcie) {

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    var sales = salesModel;
    var customer = salesModel;

    $scope.sales = sales;
    $scope.customer = customer;
    $scope.list = [];

    //pagination
    $scope.sales.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    $scope.init = function (id, name, mobile) {
        $scope.sales.PatientId = id;
        $scope.sales.Name = name;
        $scope.sales.Mobile = mobile;
        

        $scope.customerData = function () {
            patientService.customerInformation($scope.sales).then(function (response) {
                $scope.customer = response.data;
            }, function () { });
        }
        //$scope.salesSearch();
        $scope.customerData();
    }

        function pageSearch() {
            patientService.patientSaleInformation($scope.sales).then(function (response) {
                $scope.list = response.data.list;
            }, function () { });
        }

        $scope.salesSearch = function () {
            $scope.sales.page.pageNo = 1;
            patientService.patientSaleInformation($scope.sales).then(function (response) {
                $scope.list = response.data.list;
                pagerServcie.init(response.data.noOfRows, pageSearch);
            }, function () { });
        }

        $scope.salesSearch();

    $scope.togglePageLoad = function () {
        $(".pat-info-btn").text('+');
        $(".alter-row-collapse").hide();
    }

    $scope.togglePageLoad();

    $scope.togglePatientDetail = function () {
        $(".alter-row-collapse").slideToggle();
        $(".alter-row-collapse").show();
        if ($(".pat-info-btn").text() === '+')
            $(".pat-info-btn").text('-');
        else {
            $(".pat-info-btn").text('+');
        }
    }

});
