app.controller('patientListCtrl', function ($scope, patientService, salesModel, pagerServcie, ModalService, toastr, cacheService, salesService) {
    var sales = salesModel;
    $scope.search = sales;
    $scope.list = [];
    $scope.patientSearchData = {};
    $scope.search.patient = {
        status: 1
    }; //$scope.status = "1";
    
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination
    function pageSearch() {
        patientService.customerList($scope.search)
            .then(function (response) {
                $scope.list = response.data.list;
            }, function () { });
    }
    $scope.salesSearch = function () {
        $scope.search.page.pageNo = 1;
        if ($scope.status != '')
            $scope.search.patient.status = $scope.status;
        $.LoadingOverlay("show");
        patientService.customerList($scope.search)
            .then(function (response) {
                $scope.list = response.data.list;
                $.LoadingOverlay("hide");
                pagerServcie.init(response.data.noOfRows, pageSearch);
            }, function (error) {
                $.LoadingOverlay("hide");
                console.log(error);
                toastr.error("Error");
            });
    };

    $scope.clearSearch = function () {
        $scope.status = '';
        $scope.search.name = '';
        $scope.search.mobile = '';
        $scope.search.email = '';
    }

    $scope.isDepartmentsave = "0";
    getIsDepartmentadded();
    function getIsDepartmentadded() {
        salesService.getIsDepartmentadded()
            .then(function (response) {
                if (response.data == "" || response.data == null || response.data == undefined) {
                    $scope.isDepartmentsave = "0";
                } else {
                    if (response.data.patientTypeDept != undefined) {
                        $scope.isDepartmentsave = "0";
                        if (response.data.patientTypeDept == true) {
                            $scope.isDepartmentsave = "1";
                        }
                    } else {
                        $scope.isDepartmentsave = "0";
                    }
                }
            }, function (error) {
                console.log(error);
                toastr.error("Error");
            });
    }
    $scope.popupAddNew = function () {
        $scope.patientSearchData.gender = "M";
        cacheService.set('selectedPatient', $scope.patientSearchData);
        var m = ModalService.showModal({
            controller: "patientCreateCtrl",
            templateUrl: 'createPatient',
            inputs: {
                param: {
                    "mode": "Create",
                    "from": "customer",
                    "patData": "",
                    "isCustomerOrDept": $scope.isDepartmentsave
                }
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.salesSearch();
            });
        });
    };
    $scope.editCustomer = function (patData) {
        $scope.patientSearchData.gender = "M";
        cacheService.set('selectedPatient', $scope.patientSearchData);
        var m = ModalService.showModal({
            controller: "patientCreateCtrl",
            templateUrl: 'createPatient',
            inputs: {
                param: {
                    "mode": "Edit",
                    "patData": patData,
                    "isCustomerOrDept": $scope.isDepartmentsave
                }
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.salesSearch();
            });
        });
    };
    $scope.salesSearch();
});








