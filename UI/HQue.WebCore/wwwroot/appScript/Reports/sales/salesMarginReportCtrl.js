app.controller('salesMarginReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'salesItemModel', '$filter', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, salesReportService, salesModel, salesItemModel, $filter) {
    
    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months
    var isComposite = false;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    
    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false   
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.type = "TODAY";
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            isComposite = $scope.instance.gstselect;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.salesRdFilter = "dateWise";
            //$scope.buyReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.ChangeFilter = function (salesRdFilter) {
       
        if ($scope.salesRdFilter == 'dateWise') {
            $scope.salesRdFilter = "dateWise";
            $scope.buyReport();
        }
        if ($scope.salesRdFilter == 'productWise') {
            $scope.salesRdFilter = "productWise";
            $scope.buyReport();
        }

        if ($scope.salesRdFilter == 'invoiceWise') {
            $scope.salesRdFilter = "invoiceWise";
            $scope.buyReport();
        }
    }

    
    $scope.buyReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }
        $("#grid").empty();
        if ($scope.salesRdFilter == "dateWise" || $scope.salesRdFilter == "" || $scope.salesRdFilter == undefined) {
            salesReportService.salesMarginProductWiseList($scope.salesRdFilter,data, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                if ($scope.from != undefined || $scope.from != null) {
                    $scope.dayDiff($scope.from);
                }
                $scope.type = "";

                $scope.TotalMRP = 0;
                $scope.TotalCostPrice = 0;
                $scope.TotalMargin = 0;
                for (var i = 0; i < $scope.data.length; i++) {
                    $scope.TotalMRP += $scope.data[i].sales.finalValue;
                    $scope.TotalCostPrice += $scope.data[i].productStock.totalCostPrice;
                }

                $scope.TotalMargin = (($scope.TotalMRP - $scope.TotalCostPrice) / $scope.TotalMRP) * 100;

                var TotalMarginFooter = "<div id='totalMargin'>" + $filter('number')($scope.TotalMargin, 2) + " <div>";


                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales Margin DateWise Detail";
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Sales Margin DateWise Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Sales Margin DateWise Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()                       
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height: 350,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                  { field: "productStock.instanceName", title: "Branch", width: "100px", format: "{0}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "sales.invoiceDate", title: "Bill Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" },footerTemplate: "Total", template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },

                  { field: "productStock.totalCostPrice", title: "Cost Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                  { field: "sales.finalValue", title: "Sale Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                  { field: "reportTotal", title: "Gross Margin %", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: TotalMarginFooter },
                  { field: "salesProfit", title: "Profit Amount", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" }

                  //{ field: "saleValue", title: "ROI %", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number",fformat: "0.00"  } },

                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                              { field: "productStock.totalCostPrice", aggregate: "sum" },
                               { field: "sales.finalValue", aggregate: "sum" },
                                { field: "reportTotal", aggregate: "sum" },
                                 { field: "salesProfit", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "productStock.instanceName": { type: "string" },
                                    "sales.invoiceDate": { type: "date" },                                   
                                    "sales.finalValue": { type: "number" },
                                    "productStock.totalCostPrice": { type: "number" },
                                    "reportTotal": { type: "number" },
                                    "salesProfit": { type: "number" },                                    
                                    "saleValue": { type: "number" },
                                }
                            }
                        },
                        pageSize: 20
                    },
                    excelExport: function (e) {

                        addHeader(e, 'Sales Margin DateWise Details');

                        var sheet = e.workbook.sheets[0];
                        //sheet(sheet);
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        if ($scope.salesRdFilter == "productWise") {
            salesReportService.salesMarginProductWiseList($scope.salesRdFilter, data, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                $scope.type = "";

                $scope.TotalMRP = 0;
                $scope.TotalCostPrice = 0;
                $scope.TotalMargin = 0;
                for (var i = 0; i < $scope.data.length; i++) {
                    $scope.TotalMRP += $scope.data[i].sales.finalValue;
                    $scope.TotalCostPrice += $scope.data[i].productStock.totalCostPrice;
                }

                $scope.TotalMargin = (($scope.TotalMRP - $scope.TotalCostPrice) / $scope.TotalMRP) * 100;

                var TotalMarginFooter = "<div id='totalMargin'>" + $filter('number')($scope.TotalMargin, 2) + " <div>";


                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.tinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales Margin ProductWise Details";
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Sales Margin ProductWise Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Sales Margin ProductWise Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height: 350,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                   { field: "productStock.instanceName", title: "Branch", width: "100px", format: "{0}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "productStock.product.name", title: "Product", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-highlight" } },
                   { field: "sales.actualInvoice", title: "Bill No", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "sales.invoiceDate", title: "Bill Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },

                   { field: "sales.name", title: "Patient Name", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                    { field: "productStock.batchNo", title: "Batch", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.expireDate", title: "Expiry", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                  { field: "productStock.product.schedule", title: "Schedule", width: "130px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.vat", title: "VAT/GST %", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, hidden: isComposite },

                  { field: "productStock.totalCostPrice", title: "Cost Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                  { field: "sales.finalValue", title: "Sale Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                  { field: "reportTotal", title: "Gross Margin %", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                  { field: "salesProfit", title: "Profit Amount", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },

                  //{ field: "saleValue", title: "ROI %", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },

                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                              { field: "productStock.totalCostPrice", aggregate: "sum" },
                               { field: "sales.finalValue", aggregate: "sum" },
                                { field: "reportTotal", aggregate: "sum" },
                                 { field: "salesProfit", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "productStock.instanceName": { type: "string" },
                                    "sales.actualInvoice": { type: "string" },
                                    "sales.invoiceDate": { type: "date" },
                                    "productStock.product.name": { type: "string" },
                                    "productStock.vat": { type: "number" },
                                    "sales.finalValue": { type: "number" },
                                    "productStock.totalCostPrice": { type: "number" },
                                    "reportTotal": { type: "number" },
                                    "salesProfit": { type: "number" },
                                    "productStock.batchNo": { type: "number" },
                                    "productStock.expireDate": { type: "date" },
                                    "sales.name": { type: "string" },
                                    "sales.credit": { type: "number" },
                                    "productStock.product.schedule": { type: "string" },
                                    "saleValue": { type: "number" },
                                }
                            }
                        },
                        pageSize: 20
                    },
                    excelExport: function (e) {

                        addHeader(e, 'Sales Margin ProductWise Details');

                        var sheet = e.workbook.sheets[0];
                        // sheet(sheet);
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        if ($scope.salesRdFilter == "invoiceWise") {
            salesReportService.salesMarginProductWiseList($scope.salesRdFilter, data, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                $scope.type = "";
                $scope.TotalMRP= 0;
                $scope.TotalCostPrice = 0;
                $scope.TotalMargin = 0;
                for (var i = 0; i < $scope.data.length; i++) {
                    $scope.TotalMRP += $scope.data[i].sales.finalValue;
                    $scope.TotalCostPrice += $scope.data[i].productStock.totalCostPrice;
                }
       
                $scope.TotalMargin = (($scope.TotalMRP - $scope.TotalCostPrice)/$scope.TotalMRP)*100;

                var TotalMarginFooter = "<div id='totalMargin'>" + $filter('number')($scope.TotalMargin, 2) + " <div>";
 

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales Margin InvoiceWise Details";
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Sales Margin InvoiceWise Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000],
                        landscape: true,
                        allPages: true,
                        fileName: "Sales Margin InvoiceWise Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()                      
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height: 350,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                   { field: "productStock.instanceName", title: "Branch", width: "100px", format: "{0}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "sales.actualInvoice", title: "Bill No", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "sales.invoiceDate", title: "Bill Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },

                   { field: "sales.name", title: "Patient Name", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "Total", },
                  { field: "productStock.totalCostPrice", title: "Cost Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                  { field: "sales.finalValue", title: "Sale Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                  { field: "reportTotal", title: "Gross Margin %", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: TotalMarginFooter },
                  { field: "salesProfit", title: "Profit Amount", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                  //{ field: "saleValue", title: "ROI %", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },

                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                             { field: "productStock.totalCostPrice", aggregate: "sum" },
                               { field: "sales.finalValue", aggregate: "sum" },
                                { field: "reportTotal", aggregate: "sum" },
                                 { field: "salesProfit", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "productStock.instanceName": { type: "string" },
                                    "sales.actualInvoice": { type: "string" },
                                    "sales.invoiceDate": { type: "date" },                                    
                                    "sales.finalValue": { type: "number" },
                                    "productStock.totalCostPrice": { type: "number" },
                                    "reportTotal": { type: "number" },
                                    "salesProfit": { type: "number" },                                   
                                    "sales.name": { type: "string" },
                                    "saleValue": { type: "number" },
                                }
                            }
                        },
                        pageSize: 20
                    },
                    excelExport: function (e) {

                        addHeader(e, 'Sales Margin InvoiceWise Details');

                        var sheet = e.workbook.sheets[0];
                        // sheet(sheet);
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    }

    function sheet(sheet) {
       for (var i = 0; i < sheet.rows.length; i++) {
           var row = sheet.rows[i];
           for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
               var cell = row.cells[ci];

               if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                   cell.hAlign = "left";
                   cell.format = this.columns[ci].attributes.fformat;
                   cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
               }
               if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                   cell.hAlign = "right";
                   cell.format = "#" + this.columns[ci].attributes.fformat;
               }

               if (row.type == "group-footer" || row.type == "footer") {
                   if (cell.value) {
                       cell.value = $.trim($('<div>').html(cell.value).text());
                       cell.value = cell.value.replace('Total:', '');
                       cell.hAlign = "right";
                       cell.format = "#0.00";
                       cell.bold = true;
                   }
               }
           }
       }
   }

    function addHeader(e,gTitle) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: gTitle, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: gTitle, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    //
    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);
