app.factory('salesReportService', function ($http) {
    return {
        list: function (type, data, instanceid, kind) {
            return $http.post('/salesReport/listData?type=' + type + '&sInstanceId=' + instanceid + '&kind=' + kind, data);
        },
        CustomerWiseDetailList: function (Name, Mobile, Instanceid, data) {
            return $http.post('/salesReport/CustomerDetailListData?Name=' + Name + '&Mobile=' + Mobile + '&Instanceid=' + Instanceid, data);
        },

        ProductWiseDetailList: function (Id,Name,Mobile, data) {
            return $http.post('/salesReport/ProductwiseListData?Id=' + Id + '&Name=' + Name + '&Mobile=' + Mobile, data);
        },
        ProductWiseDetailListInSales: function ( Id, Name, Mobile,Instanceid, data) {
            return $http.post('/salesReport/ProductWiseDetailListInSales?Id=' + Id + '&Name=' + Name + '&Mobile=' + Mobile + '&Instanceid=' + Instanceid, data);
        },
        NonSaleCustomers: function (Instanceid, data) {
            return $http.post('/salesReport/NonSaleCustomersList?Instanceid=' + Instanceid, data);
        },
        returnList: function (type, data, instanceid, nIssummary, fromvalues, tovalues, SearchType,kind) {
            return $http.post('/salesReport/returnListData?&type=' + type + '&sInstanceId=' + instanceid + '&nIssummary=' + nIssummary + '&fromvalues=' + fromvalues + '&tovalues=' + tovalues + '&SearchType=' + SearchType + '&kind=' + kind, data);
        },
        homeDeliveryList: function (type, data, instanceid, invoiceType, invoiceSearchType, fromInvoice, toInvoice) {
            return $http.post('/salesReport/homeDeliveryListData?type=' + type + '&sInstanceId=' + instanceid + '&InvoiceType=' + invoiceType + '&InvoiceSearchType=' + invoiceSearchType + '&FromInvoice=' + fromInvoice + '&ToInvoice=' + toInvoice, data);
        },
        doorDeliveryList: function (type, data, instanceid) {
            return $http.post('/salesReport/doorDeliveryListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        drugWiseList: function (data, type, instanceid, invoiceSeries, fromInvoice, toInvoice, productVal) {
            return $http.post('/salesReport/drugWiseListData?type=' + type + '&instanceid=' + instanceid + '&InvoiceSeries=' + invoiceSeries + '&FromInvoice=' + fromInvoice + '&ToInvoice=' + toInvoice + '&productVal=' + productVal, data);
        },
        saleslist: function (type, data, instanceid, invoiceType, invoiceSearchType, fromInvoice,toInvoice) {
            return $http.post('/salesReport/SalesListData?type=' + type + '&sInstanceId=' + instanceid + '&InvoiceType=' + invoiceType + '&InvoiceSearchType=' + invoiceSearchType + '&FromInvoice=' + fromInvoice + '&ToInvoice=' + toInvoice, data);
        },
        salesCustomerwiselist: function (Name, Mobile, Instanceid,  data) {
            return $http.post('/salesReport/SalesCustomerwiseListData?Name=' + Name + '&Mobile=' + Mobile + '&Instanceid=' + Instanceid, data);
        },
       
        salesSummarylist: function (type, data, instanceid, invoiceSeries, fromInvoice, toInvoice, paymentType) {
            return $http.post('/salesReport/salesSummarylistData?type=' + type + '&sInstanceId=' + instanceid + '&InvoiceSeries=' + invoiceSeries + '&FromInvoice=' + fromInvoice + '&ToInvoice=' + toInvoice + '&paymentType=' + paymentType, data);
        },
        salesSummaryCustomerwiselist: function (Name, Mobile, Instanceid, data) {
            return $http.post('/salesReport/salesSummaryCustomerwiselistData?Name=' + Name + '&Mobile=' + Mobile + '&Instanceid=' + Instanceid, data);
        },
        salesSummaryReturnlist: function (type, data) {
            return $http.post('/salesReport/SalesSummaryReturnListData?type=' + type, data);
        },
        salesUserlist: function (data, instanceid) {
            return $http.post('/salesReport/salesUserListData?sInstanceId=' + instanceid, data);
        },
        exportData: function (data) {
            return $http.post('/salesReport/exportData', data);
        },
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        getCurrentInstanceData: function (branchId) {
            return $http.get('/salesReport/getCurrentInstanceData?branchid=' + branchId);
        },
        scheduleList: function (type, schedule, data, search, frombillno, tobillno, instanceid) {
            return $http.post('/scheduleReport/listData?type=' + type + '&schedule=' + schedule + '&search=' + search + '&frombillno=' + frombillno + '&tobillno=' + tobillno + '&instanceid=' + instanceid, data);
        },

        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },
        GetReturnsByDate: function (type, data, instanceid, invoiceType, invoiceSearchType, fromInvoice, toInvoice) {
            return $http.post('/salesReport/GetReturnsByDate?type=' + type + '&sInstanceId=' + instanceid + '&InvoiceType=' + invoiceType + '&InvoiceSearchType=' + invoiceSearchType + '&FromInvoice=' + fromInvoice + '&ToInvoice=' + toInvoice, data);
        },
        getSalesDetailReturnsByDate: function (type, data) {
            return $http.post('/salesReport/GetSalesDetailReturnsByDate?type=' + type, data);
        },
        billCancelList: function (type, data) {
            return $http.post('/salesReport/BillCancelListData?type=' + type, data);
        },
        salesMarginProductWiseList: function (filter, data, instanceid) {
            return $http.post('/salesReport/salesMarginProductWiseList?filter=' + filter + '&instanceId=' + instanceid, data);
        },

        // Added Gavaskar 07-03-2017
        getInvoiceSeriesItems: function (filter, branchid) {
            return $http.post('/salesReport/getInvoiceSeriesItems?filter=' + filter + '&branchid=' + branchid);
            },

            GetReturnsByDateSalesList: function (type, data, instanceid, invoiceType, invoiceSearchType) {
                return $http.post('/salesReport/GetReturnsByDateSalesList?type=' + type + '&sInstanceId=' + instanceid + '&InvoiceType=' + invoiceType + '&InvoiceSearchType=' + invoiceSearchType, data);
            },
            GetUserName: function (filter) {
                return $http.get('/salesReport/GetUserName?userName=' + filter);
            },
            GetUserId: function (filter) {
                return $http.get('/salesReport/GetUserId?userId=' + filter);
            },
       //consolidatedsalesReportList: function (type, data, instanceid, invoiceType, invoiceSearchType) {
       //    return $http.post('/SalesReport/consolidatedsalesReportList?type=' + type + '&sInstanceId=' + instanceid + '&InvoiceType=' + invoiceType, data);
        //}, 
            consolidatedsalesReportList: function (type, data, instanceid, invoiceType, cashType, invoiceSearchType) {
                return $http.post('/SalesReport/consolidatedsalesReportList?type=' + type + '&sInstanceId=' + instanceid + '&InvoiceType=' + invoiceType + '&cashType=' + cashType, data);
            },
        //add by nandhini for consolidated sales return report 9/6/17
            consolidatedsalesReturnReportList: function (type, data, instanceid, invoiceType, cashType, invoiceSearchType) {
                return $http.post('/SalesReport/consolidatedsalesReturnReportList?type=' + type + '&sInstanceId=' + instanceid + '&InvoiceType=' + invoiceType + '&cashType=' + cashType, data);
            },
            DateWiseSalesReportData: function (type, data, instanceid, level) {
                return $http.post('/SalesReport/DateWiseSalesReportData?type=' + type + '&sInstanceId=' + instanceid + '&level=' + level, data);
            },
            ItemWiseSalesReportData: function (type, data, instanceid, level, productId) {
                return $http.post('/SalesReport/ItemWiseSalesReportData?type=' + type + '&sInstanceId=' + instanceid + '&level=' + level + '&productId=' + productId, data);
            },
            TaxWiseSalesReportData: function (type, data, instanceid, level, gstTotal) {
                return $http.post('/SalesReport/BranchWiseSalesGroupReportData?type=' + type + '&sInstanceId=' + instanceid + '&level=' + level + '&gstTotal=' + gstTotal, data);
            },
        //added by Bikas for Branch wise Sales report 14/06/2018
            BranchWiseSalesReportData: function (type, data, instanceid, level) {
                return $http.post('/SalesReport/BranchWiseSalesReportData?type=' + type + '&sInstanceId=' + instanceid + '&level=' + level, data);
            },
        //addded by Sarubala on 29-08-17
            DoctorwiseList: function (type, data, instanceid, filterType, doctorid, productid) {
                return $http.post('/salesReport/DoctorwiseReportList?type=' + type + '&sInstanceId=' + instanceid + '&filterType=' + filterType + '&doctorId=' + doctorid + '&productId=' + productid , data);
            },
    }
});
