app.controller('reportSettingsCtrl', function ($scope, $window, toastr) {

    $scope.saveSettings = function () {
        window.localStorage.setItem("PageBreakSettings", JSON.stringify($scope.pageBreakSettings));
        toastr.success('Settings saved successfully');
    };
    $scope.getSettings = function () {
        $scope.pageBreakSettings = JSON.parse(window.localStorage.getItem("PageBreakSettings"));
        if($scope.pageBreakSettings == null){
            $scope.pageBreakSettings = "1";
        }
    };

});

