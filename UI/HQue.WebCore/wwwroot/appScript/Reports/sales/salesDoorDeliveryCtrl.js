app.controller('salesDoorDeliveryCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'userAccessModel', '$filter', function ($scope, $rootScope, $http, $interval, $q, salesReportService, salesModel, userAccessModel, $filter) {

    var sales = salesModel;
    $scope.search = sales;
    $scope.totalSalesAmount = 0;
    var sno = 0;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months

    //$scope.branchid = "";
    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.pdfHeader = "";

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.type = 'TODAY';

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.salesReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.salesReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined //$scope.instance.id;
        }

        //console.log($scope.branchid);
        salesReportService.doorDeliveryList($scope.type, data, $scope.branchid).then(function (response) {
            $scope.data = response.data;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);
            }
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            } else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/> Door Delivery Report";
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Door Delivery Report";
            }

            var grid = $("#grid").kendoGrid({
                excel: {
                    fileName: "Door Delivery Report.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Door Delivery Report.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                // height:450,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                      { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "60px", attributes: { ftype: "sno", class: " text-left field-report" } },
                      { field: "instance.name", title: "Branch", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                     { field: "actualInvoice", title: "Bill No.", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                       { field: "invoiceDate", title: "B Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, footerTemplate: "Grand Total", template: "#= kendo.toString(kendo.parseDate(invoiceDate), 'dd/MM/yyyy') #" },
                        { field: "cash", title: "Cash", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                      { field: "card", title: "Card", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                       { field: "credit", title: "Credit", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                       { field: "cheque", title: "Cheque", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                       { field: "ewallet", title: "eWallet", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                       { field: "subPayment", title: "e-Provider", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                       { field: "roundoffSaleAmount", title: "RoundOff Value", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                       { field: "finalValue", title: "Net Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span id='totalSalesAmount'>#= kendo.toString(sum, 'n2') #</span></div>" },
                  { field: "name", title: "Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "mobile", title: "Mobile", width: "75px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "email", title: "Email", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "doorDeliveryStatus", title: "Status", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left text-bold" } },

                { field: "createdBy", title: "Transaction By", width: "110px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                         { field: "cash", aggregate: "sum" },
                         { field: "card", aggregate: "sum" },
                         { field: "credit", aggregate: "sum" },
                         { field: "cheque", aggregate: "sum" },
                         { field: "ewallet", aggregate: "sum" },
                         { field: "finalValue", aggregate: "sum" },
                         { field: "roundoffSaleAmount", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "string" },
                                "instance.name": { type: "string" },
                                "invoiceNo": { type: "string" },
                                "invoiceDate": { type: "date" },
                                "cash": { type: "number" },
                                "card": { type: "number" },
                                "credit": { type: "credit" },
                                "cheque": { type: "number" },
                                "ewallet": { type: "number" },
                                "subPayment": { type: "string" },
                                "roundoffSaleAmount": { type: "number" },
                                "finalValue": { type: "number" },
                                "name": { type: "string" },
                                "mobile": { type: "string" },
                                "email": { type: "string" },
                                // "deliveryType": { type: "string" },
                                "doorDeliveryStatus": { type: "string" },
                                "createdBy": { type: "string" }
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }

                            if (row.type == "data" && (this.columns[ci].title == "Cash" || this.columns[ci].title == "Card" || this.columns[ci].title == "Credit" || this.columns[ci].title == "Net Value")) {
                                cell.format = "#0.00";
                            }
                        }
                    }
                },
            }).data("kendoGrid");

            // Save the reference to the original filter function.
            grid.dataSource.originalFilter = grid.dataSource.filter;

            // Replace the original filter function.
            grid.dataSource.filter = function () {
                // If a column is about to be filtered, then raise a new "filtering" event.
                if (arguments.length > 0) {
                    this.trigger("filtering", arguments);
                }
                // Call the original filter function.
                var result = grid.dataSource.originalFilter.apply(this, arguments);

                // If a column is about to be filtered, then raise a new "filtering" event.
                if (arguments.length > 0) {
                    this.trigger("filtering", result);
                }

                return result;
            }

            // Bind to the dataSource filtering event.
            $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                var filters = dataSource.filter();
                var allData = dataSource.data();
                var query = new kendo.data.Query(allData);
                var data = query.filter(filters).data;


            });

            $scope.totalSalesAmount = parseInt($("#totalSalesAmount").text());

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });


    }
    //End Chng 2

    //$scope.salesReport();

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "TODAY") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.salesReport();
    };

    // chng-3

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Door Delivery Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Non Sale Customers Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);

