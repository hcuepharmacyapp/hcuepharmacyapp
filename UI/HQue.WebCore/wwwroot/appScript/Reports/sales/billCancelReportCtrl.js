app.controller('billCancelReportCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'salesReturnModel', 'salesReturnItemModel', 'productModel', 'productStockModel', 'productService', 'salesService', '$filter', function ($scope, $rootScope, $http, $interval, $q, salesReportService, salesModel, salesReturnModel, salesReturnItemModel, productModel, productStock, productService, salesService, $filter) {
    
    var salesReturnItem = salesReturnItemModel;
    var salesReturn = salesReturnModel;
    $scope.search = salesReturnItem;
    $scope.search.salesReturn = salesReturn;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months
    
    $scope.search.select = null;
    $scope.search.drugName = null;
    $scope.search.selectedValue = null;
    $scope.productCondition = false;
    $scope.search.select1 = 0;
    $scope.totalCondition = false;

    var sno = 0;

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    //$scope.type = "Today";
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.billCancelReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.getProducts = function (val) {
        var instanceid = '';
        if ($scope.branchid == undefined || $scope.branchid == '')
            instanceid = $scope.instance.id;
        else
            instanceid = $scope.branchid;
        if (instanceid != undefined && instanceid != null) {
            return productService.InstanceWisedrugFilter(instanceid, val).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        }
    };

    $scope.getProductId = function () {

        $scope.search.selectedValue = $scope.search.drugName.id;
        $scope.billCancelReport();
    };

    $scope.getPatientName = function (val) {

        return salesService.GetPatientName(val).then(function (response) {

            var origArr = response.data;
            var newArr = [],
       origLen = origArr.length,
       found, x, y;
            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if (origArr[x].mobile === newArr[y].mobile && origArr[x].name === newArr[y].name) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    if (origArr[x].mobile == "") {

                    } else {
                        newArr.push(origArr[x]);
                    }

                }
            }
            return newArr.map(function (item) {
                return item;
            });
        });
    };

    $scope.onPatientSelect = function (obj) {
        //console.log(JSON.stringify(obj));
        $scope.search.selectedValue = obj.name;
    }

    $scope.changefilters = function () {
        $scope.search.selectedValue = null;
        $scope.search.drugName = null;
        if ($scope.search.select == "Product") {
            $scope.productCondition = true;
            $scope.totalCondition = false;
        }
        else if($scope.search.select == "Amount") {
            $scope.totalCondition = true;
            $scope.productCondition = false;
        }
        else {
            $scope.productCondition = false;
            $scope.totalCondition = false;
        }
    }

    $scope.clearSearch = function () {
        $scope.search.select = null;
        $scope.search.drugName = null;
        $scope.search.selectedValue = null;
        $scope.productCondition = false;
        $scope.search.select1 = 0;
        $scope.totalCondition = false;
    }

    $scope.billCancelReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
            $scope.search.instanceId = undefined; //$scope.branchid;
        }
        else
            $scope.search.instanceId = $scope.branchid;

        $scope.search.dates = data;
        if ($scope.type == undefined)
        {
            $scope.type = "";
        }

        salesReportService.billCancelList($scope.type, $scope.search).then(function (response) {
            $scope.data = response.data;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);
            }
            $scope.type = "";
            
            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {  //$scope.currentInstance == typeof (Object)
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/> Bill Cancel Details";
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Bill Cancel Details";
            }


            $("#grid").kendoGrid({
                    excel: {
                        fileName: "Bill Cancel Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Bill Cancel Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height:400,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                   { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "90px", attributes: { ftype: "sno", class: " text-left field-report" } },
                   { field: "productStock.instanceName", title: "Branch", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "cancelledDateTime", title: "Cancelled Date", width: "120px", type: "string", attributes: { class: "field-primary", ftype: "string" }, filterable: { cell: { showOperators: false } } },
                  { field: "salesReturn.returnNo", title: "Cancel No", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "salesReturn.sales.invoiceDate", title: "Invoice Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(salesReturn.sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                  { field: "salesReturn.sales.invoiceNo", title: "Invoice No", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "salesReturn.sales.name", title: "Customer", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },
                  { field: "salesReturn.sales.mobile", title: "Mobile No", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "productStock.product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },
                  { field: "productStock.batchNo", title: "Batch", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                   { field: "productStock.expireDate", title: "Expire Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } }, footerTemplate: "Total:" },
                   { field: "quantity", title: "Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                   { field: "totalAmount", title: "Amount", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div style='float:right;margin-left:3px;' class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                   { field: "cancelledBy", title: "CancelledBy", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } }
                
                  //{ field: "totalAmount", title: "Final Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<span style='float:left;'>Total:</span><div style='float:left;margin-left:3px;' class='report-footer'>#= kendo.toString(sum, 'n0') #.00</div>" }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "quantity", aggregate: "sum" },
                          { field: "totalAmount", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "number" },
                                "productStock.instanceName": { type: "string" },
                                "cancelledDateTime": { type: "string" },
                                "salesReturn.returnNo": { type: "number" },
                                "salesReturn.sales.invoiceDate": { type: "date" },
                                "salesReturn.sales.invoiceNo": { type: "number" },
                                "salesReturn.sales.name": { type: "string" },
                                "salesReturn.sales.mobile": { type: "string" },
                                "productStock.product.name": { type: "string" },
                                "productStock.batchNo": { type: "number" },
                                "productStock.expireDate": { type: "date" },
                                "quantity": { type: "number" },
                                "totalAmount": { type: "number" },
                                "cancelledBy": { type: "string" }
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }

 	           if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //


    //$scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.clearSearch();

        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }

        $scope.billCancelReport();
    }


    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Bill Cancel Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Bill Cancel Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    //
    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);
