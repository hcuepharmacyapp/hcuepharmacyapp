app.controller('salesUserListReportCtrl', ['$scope', '$rootScope','salesService','patientService', 'uiGridConstants', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'salesItemModel', '$filter', function ($scope,$rootScope,salesService,patientService, uiGridConstants, $http, $interval, $q, salesReportService, salesModel, salesItemModel, $filter) {

    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.rptHeader = "";
    $scope.dateRangeExceeds = false; // hide excel export more than two months
    var isComposite = false;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;


    $scope.checkFromDate = function () {
        var dt = $("#fromDate").val();
        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validFromDate = true;

                    if ($scope.to != undefined && $scope.to != null) {
                        $scope.checkToDate1($scope.from, $scope.to);
                    }
                }
                else {
                    $scope.validFromDate = false;
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else
            $scope.validFromDate = true;
    }

    $scope.checkToDate = function () {
        var dt = $("#toDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validToDate = true;
                    $scope.checkToDate1($scope.from, $scope.to);
                }
                else {
                    $scope.validToDate = false;
                }
            }
            else {
                $scope.validToDate = false;
            }
        }
        else
            $scope.validToDate = true;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.nameCondition = true;
    $scope.changefilters = function () {
       
        if ($scope.search.select == 'userName') {
            $scope.nameCondition = true;
        }
        else {
            $scope.nameCondition = false;
        }

    };
    
    $scope.getUserName = function (val) {

        return salesReportService.GetUserName(val).then(function (response) {

            var origArr = response.data;
            var newArr = [],
       origLen = origArr.length,
       found, x, y;
            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if (origArr[x].mobile === newArr[y].mobile && origArr[x].name === newArr[y].name) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    //if (origArr[x].mobile == "") {

                    //} else {
                    //    newArr.push(origArr[x]);
                    //}
                    newArr.push(origArr[x]);
                }
            }
            return newArr.map(function (item) {
                return item;
            });
        });
    };

    $scope.getUserId = function (val) {
        return salesReportService.GetUserId(val).then(function (response) {
            console.log(response.data);
            var origArr = response.data;
            var newArr = [],
       origLen = origArr.length,
       found, x, y;
            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if (origArr[x].userId === newArr[y].userId && origArr[x].mobile === newArr[y].mobile) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }
            return newArr.map(function (item) {
                return item;
            });
        });
    };

    $scope.CustomerName = "";
    $scope.CustomerMobile = "";
    $scope.onCustomerSelect = function (obj) {
        console.log(JSON.stringify(obj));

        $scope.CustomerName = obj.name;
        $scope.CustomerMobile = obj.mobile;
    }

    $scope.CustomerUserId = "";
    $scope.onCustomerSelectUserId = function (obj) {
        console.log(JSON.stringify(obj));

        $scope.CustomerUserId = obj.userId;
        $scope.CustomerMobile = obj.mobile;
    }

    $scope.Checked = "Summary";
    $scope.CheckSummary = true;
    $scope.CheckInvoice = false;
    $scope.CheckDetail = false;
   // $scope.Checked = "";
    $scope.CheckWhichReport = function (index, val) {

        console.log(index + "__" + val);
        if (index == 1) {


            if (val == false) {
                $scope.CheckSummary = false;
                $scope.Checked = "";
            } else {
                $scope.CheckSummary = true;
                $scope.CheckInvoice = false;
                $scope.CheckDetail = false;
                $scope.Checked = "Summary";
                $scope.salesReport();
                $("#grid").empty();
            }

        } else if (index == 2) {


            if (val == false) {
                $scope.CheckInvoice = false;
                $scope.Checked = "";

            } else {
                $scope.CheckInvoice = true;
                $scope.CheckSummary = false;
                $scope.CheckDetail = false;
                $scope.Checked = "Invoice";
                $scope.salesReport();
                $("#grid").empty();
            }
        }
        else {

            if (val == false) {
                $scope.CheckDetail = false;
                $scope.Checked = "";
            } else {
                $scope.CheckDetail = true;
                $scope.CheckInvoice = false;
                $scope.CheckSummary = false;
                $scope.Checked = "Detail";
                $scope.salesReport();
                $("#grid").empty();
            }
        }
    }

   

    $scope.init = function () {
        $.LoadingOverlay("show");

        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            isComposite = $scope.instance.gstselect;
           //$scope.salesReport();
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;             
            }
        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }


    $scope.salesReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }

        if ($scope.search.select == 'userName')
        {
            $scope.search.sales.name = $scope.CustomerName;
        }
        else
        {
            $scope.search.sales.name = $scope.CustomerUserId;
        }
        

        $scope.search.sales.mobile = $scope.CustomerMobile;
        $scope.search.sales.select = $scope.from;
        $scope.search.sales.select1 = $scope.to;
        $scope.search.sales.selectValue = $scope.search.select;
        $scope.search.sales.values = $scope.Checked;

        if ($scope.Checked == "Summary") {
            salesReportService.salesUserlist($scope.search, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                if ($scope.from != undefined || $scope.from != null) {
                    $scope.dayDiff($scope.from);
                }
                $scope.type = "";

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales User Wise Summary";
                }

                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Sales_User_Wise_Summary.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Sales_User_Wise_Summary.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                        { field: "sales.instance.name", title: "Branch", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                        { field: "sales.invoiceDate", title: "Sales Date", width: "70px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                        { field: "sales.hQueUser.name", title: "User Name", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "Total :" },
                      { field: "sales.totalSales", title: "Total Sales", width: "160px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                      { field: "sales.invoiceAmount", title: "Total Purchase Price(Including Tax)", width: "160px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n0') #</span><span>.00</span></div>" },
                      { field: "sales.finalValue", title: "Total MRP(Including Tax)", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n0') #</span><span>.00</span></div>" }
                    //{ field: "sales.profit", title: "Profit", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-right text-bold" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" }
                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [

                            { field: "sales.totalSales", aggregate: "sum" },
                            { field: "sales.invoiceAmount", aggregate: "sum" },
                            { field: "sales.finalValue", aggregate: "sum" },
                            //{ field: "sales.profit", aggregate: "sum" }

                        ],
                        schema: {
                            model: {
                                fields: {
                                    "sales.invoiceDate": { type: "date" },
                                    "sales.hQueUser.name": { type: "string" },
                                    "sales.instance.name": { type: "string" },
                                    "sales.totalSales": { type: "number" },
                                    "sales.invoiceAmount": { type: "number" },
                                    "sales.finalValue": { type: "number" },
                                    //"sales.profit": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {
                        $scope.rptHeader = " Sales User Wise Summary";
                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }

        if ($scope.Checked == "Invoice") {
            salesReportService.salesUserlist($scope.search, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                if ($scope.from != undefined || $scope.from != null) {
                    $scope.dayDiff($scope.from);
                }
                $scope.TotalSales = 0;
                for (var i = 0; i < $scope.data.length; i++) {
                    $scope.TotalSales += $scope.data[i].sales.finalValue;
                }


                $scope.type = "";

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales User Wise Invoice";
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Sales_User_Wise_Invoice.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Sales_User_Wise_Invoice.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                        { field: "sales.instance.name", title: "Branch", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                        { field: "sales.hQueUser.name", title: "User Name", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                        { field: "sales.name", title: "Patient Name", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                        { field: "sales.invoiceDate", title: "Invoice Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                      { field: "sales.actualInvoice", title: "Invoice No", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" }, footerTemplate: "Total :" },
                      { field: "sales.invoiceAmount", title: "Invoice Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                      { field: "sales.discount", title: "Discount Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                      { field: "sales.roundoffSaleAmount", title: "RoundOff Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                      { field: "sales.finalValue", title: "Net Value", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" }
                     
                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                            { field: "sales.invoiceAmount", aggregate: "sum" },
                            { field: "sales.discount", aggregate: "sum" },
                            { field: "sales.finalValue", aggregate: "sum" },
                            { field: "sales.roundoffSaleAmount", aggregate: "sum" },

                        ],
                        schema: {
                            model: {
                                fields: {
                                    "sales.invoiceDate": { type: "date" },
                                    "sales.actualInvoice": { type: "string" },
                                    "sales.invoiceAmount": { type: "number" },
                                    "sales.hQueUser.name": { type: "string" },
                                    "sales.name": { type: "string" },
                                    "sales.discount": { type: "number" },
                                    "sales.finalValue": { type: "number" },
                                    "sales.instance.name": { type: "string" },
                                    "sales.roundoffSaleAmount": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },
                    excelExport: function (e) {
                        $scope.rptHeader = " Sales User Wise Invoice";
                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }

        if ($scope.Checked == "Detail") {
            salesReportService.salesUserlist($scope.search, $scope.branchid).then(function (response) {
                if ($scope.from != undefined || $scope.from != null) {
                    $scope.dayDiff($scope.from);
                }
                console.log(JSON.stringify(response.data));
                $scope.data = response.data;
                //$scope.type = "";
                var totalCash = 0;
                for (var i = 0; i < $scope.data.length; i++) {
                    totalCash += $scope.data[i].reportTotal;
                }

                var total = parseInt(totalCash);
                $scope.total = total;

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales User Wise Detail";
                }

                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Sales_User_Wise_Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [2500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Sales_User_Wise_Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                        { field: "sales.instance.name", title: "Branch", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                        { field: "sales.hQueUser.name", title: "User Name", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                           { field: "sales.name", title: "Patient Name", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                             
                      { field: "sales.actualInvoice", title: "Bill No", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "sales.invoiceDate", title: "Bill Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },

                      { field: "productStock.product.name", title: "Product", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-highlight" } },
                      { field: "quantity", title: "Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" } },
                      { field: "sellingPrice", title: "Selling Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                        { field: "productStock.purchasePrice", title: "Cost Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                      { field: "productStock.vat", title: "VAT/GST %", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, hidden: isComposite },

                      { field: "gstAmount", title: "VAT/GST Amount", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite },
                       { field: "itemAmount", title: "Amount without Tax", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite },
                      { field: "discountAmount", title: "Discount Value", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                       { field: "productStock.totalCostPrice", title: "Total Cost Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                      { field: "reportTotal", title: "Final Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                      //{ field: "salesProfit", title: "Profit %", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number" } },
                      { field: "productStock.batchNo", title: "Batch", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "productStock.expireDate", title: "Expiry", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },

                      { field: "sales.doctorName", title: "Doctor Name", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "sales.address", title: "Patient Address", width: "150px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "sales.credit", title: "Is Credit", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "productStock.product.manufacturer", title: "Manufacturer", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "productStock.product.schedule", title: "Schedule", width: "130px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "productStock.product.type", title: "Type", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } }
                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                            //{ field: "quantity", aggregate: "sum" },
                            //{ field: "sellingPrice", aggregate: "sum" },
                            //{ field: "productStock.purchasePrice", aggregate: "sum" },
                            //{ field: "productStock.vat", aggregate: "sum" },
                            //{ field: "reportActualAmount", aggregate: "sum" },
                            //{ field: "reportVatAmount", aggregate: "sum" },
                            //{ field: "salesDiscountValue", aggregate: "sum" },
                            // { field: "productStock.totalCostPrice", aggregate: "sum" },
                            //{ field: "reportTotal", aggregate: "sum" }

                        ],
                        schema: {
                            model: {
                                fields: {
                                   
                                    "sales.hQueUser.name": { type: "string" },
                                    "sales.name": { type: "string" },
                                    "sales.actualInvoice": { type: "string" },
                                    "sales.invoiceDate": { type: "date" },
                                    "productStock.product.name": { type: "string" },
                                    "quantity": { type: "number" },
                                    "sellingPrice": { type: "number" },
                                    "productStock.purchasePrice": { type: "number" },
                                    "productStock.vat": { type: "number" },
                                    "gstAmount": { type: "number" },
                                    "itemAmount": { type: "number" },
                                    "discountAmount": { type: "number" },
                                    "productStock.totalCostPrice": { type: "number" },
                                    "reportTotal": { type: "number" },
                                    //"salesProfit": { type: "number" },
                                    "productStock.batchNo": { type: "number" },
                                    "productStock.expireDate": { type: "date" },
                                    "sales.doctorName": { type: "string" },
                                    "sales.address": { type: "string" },
                                    "sales.credit": { type: "number" },
                                    "productStock.product.manufacturer": { type: "string" },
                                    "productStock.product.schedule": { type: "string" },
                                    "productStock.product.type": { type: "string" },
                                    "sales.instance.name": { type: "string" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {
                        $scope.rptHeader = " Sales User Wise Detail";
                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    }
    //End Chng 2

    $scope.clearSearch = function () {
        $("#grid").empty();
        $scope.CheckSummary = true;
        $scope.CheckInvoice = false;
        $scope.CheckDetail = false;
        $scope.Checked = "Summary";
        $scope.CustomerMobile = "";
        $scope.CustomerName = "";
        $scope.CustomerUserId = "";
        $scope.search.select = "";
        $scope.Customer = {
            "userName": "",
            "userId": ""
        };
        $scope.Customer.Name = "";
        $scope.Customer.Id = "";
        $scope.search.sales.name = "";
        $scope.nameCondition = false;
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.data = [];
    }


   // $scope.salesReport();

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            if ($scope.Checked == "Summary") {
                headerCell = { cells: [{ value: $scope.rptHeader, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            }
            if ($scope.Checked == "Invoice") {
                headerCell = { cells: [{ value: $scope.rptHeader, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            }
            if ($scope.Checked == "Detail") {
                headerCell = { cells: [{ value: $scope.rptHeader, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            }

            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: $scope.rptHeader, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    //
    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');
        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);


