app.controller('salesInvoiceWiseReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'salesItemModel', '$filter', function ($scope,$rootScope, uiGridConstants, $http, $interval, $q, salesReportService, salesModel, salesItemModel, $filter) {
    var loadedSales = true;
    var loadedSalesReturn = true;
    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    $scope.netDisplay = false;//added by nandhini
    $scope.allBranch = true; // Enabled all branch in branch ddl
    // Added Gavaskar 04-03-2017 
    $scope.invoiceWiseList = [];
    $scope.dateRangeExceeds = false;

    //$scope.branchid = "";
    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;        
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };


    $scope.type = ''; //'TODAY';

    // chng-2
    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.salesReport();
        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    };

     $scope.ChangeInvoiceSeriesDropdown = function (seriestype) {
        //console.log(seriestype);

        $scope.InvoiceSeriesType = seriestype;
        $scope.search.select1 = "";

        if ($scope.InvoiceSeriesType == '1') {
            $scope.search.select1 = "";
            $scope.InvoiceSeriesItems = [];
        }
        if ($scope.InvoiceSeriesType == '2') {
            getInvoiceSeriesItems();
        }
        if ($scope.InvoiceSeriesType == '3') {
            $scope.search.select1 = "MAN";
            $scope.InvoiceSeriesItems = [];
        }
        if ($scope.InvoiceSeriesType == '4') {
            $scope.search.select1 = "MAN";
        }
     }

     $scope.InvoiceSeriesItems = [];
     function getInvoiceSeriesItems() {

         salesReportService.getInvoiceSeriesItems($scope.InvoiceSeriesType).then(function (response) {

             if (response.data != "" && response.data != null) {
                 $scope.InvoiceSeriesItems = response.data;
             }

         }, function () {

         });
     }



    $scope.salesReport = function () {
        $.LoadingOverlay("show");
        $scope.netDisplay = true;//added by nandhini
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }
        if ($scope.InvoiceSeriesType == undefined || $scope.InvoiceSeriesType == "") {
            $scope.InvoiceSeriesType = "";
        }

        if ($scope.search.select1 == undefined || $scope.search.select1 == "") {
            $scope.search.select1 = "";
        }

        if ($scope.fromInvoice == "") {
            $scope.fromInvoice = "";
        }
        if ($scope.toInvoice == "") {
            $scope.toInvoice = "";
        }
        loadedSales = false;
        loadedSalesReturn = false;

        salesReportService.saleslist($scope.type, data, $scope.branchid, $scope.InvoiceSeriesType, $scope.search.select1, $scope.fromInvoice, $scope.toInvoice).then(function (response) {
            $scope.data = response.data;
            //console.log(JSON.stringify(response.data));
            $scope.TotalSales = 0;
            for (var i = 0; i < $scope.data.length; i++) {
                $scope.TotalSales += $scope.data[i].sales.finalValue;
            }

            $scope.dayDiff($scope.from);

            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales Invoice Wise";
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "Sales Invoice Wise.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Sales Invoice Wise.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 270,
                sortable: true,
                filterable: {
                    mode: "column"
                },//Discount and Bill amount rounded off removed by Poongodi on 19/05/2017 (based on Sundar & Thulasi Reddy Input)
                columns: [
                     { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "40px", attributes: { ftype: "sno", class: " text-left field-report" } },
                   { field: "sales.instance.name", title: "Branch", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "sales.actualInvoice", title: "Bill No", width: "60px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "sales.invoiceDate", title: "B Date", width: "60px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  { field: "sales.name", title: "Customer", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" }, footerTemplate: "Grand Total" },

                  { field: "sales.invoiceAmount", title: "Bill Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                  { field: "sales.discount", title: "Discount (Rs.)", width: "70px", format: "{0:n}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n2')#</div>" },
                         { field: "sales.roundoffNetAmount", title: "Round off (Rs.)", width: "70px", format: "{0:n}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n2')#</div>" },
                  { field: "sales.finalValue", title: "Net Value  (Rs.)", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                  { field: "sales.salesType", title: "Sales Type", width: "80px", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "sales.isRoundOff", title: "RoundOff Enabled", width: "60px", type: "string", attributes: { class: "text-left field-report" }, template: "#= sales.isRoundOff==true ? 'Y' : 'N' #" }
                  //{ field: "sales.profit", title: "Profit", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-right text-bold" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "sales.invoiceAmount", aggregate: "sum" },
                         { field: "sales.discount", aggregate: "sum" },
                        { field: "sales.finalValue", aggregate: "sum" },
                    { field: "sales.roundoffNetAmount", aggregate: "sum" }
                        //{ field: "sales.profit", aggregate: "sum" }

                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "string" },
                                "sales.actualInvoice": { type: "string" },
                                "sales.invoiceDate": { type: "date" },                                
                                "sales.invoiceAmount": { type: "number" },
                                "sales.discount": { type: "number" },
                                "sales.finalValue": { type: "number" },
                                "sales.salesType": { type: "string" },
                                "sales.isRoundOff": { type: "string" },
                                //"sales.profit": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }
                                cell.width = "300";
                             
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });
            if (loadedSalesReturn == true) {
                $.LoadingOverlay("hide");
            }
            loadedSales = true;
        }, function () {
            if (loadedSalesReturn == true) {
                $.LoadingOverlay("hide");
            }
        });


        if ($scope.InvoiceSeriesType == undefined || $scope.InvoiceSeriesType == "") {
            $scope.InvoiceSeriesType = "";
        }

        if ($scope.search.select1 == undefined || $scope.search.select1 == "") {
            $scope.search.select1 = "";
        }

        salesReportService.GetReturnsByDateSalesList($scope.type, data, $scope.branchid, $scope.InvoiceSeriesType, $scope.search.select1).then(function (resp) {
            //console.log(JSON.stringify(resp.data));
            $scope.data = resp.data;
            $scope.type = "";

            $scope.amountBeforeDiscountTotal = 0;
            $scope.amountAfterDiscountTotal = 0;
            for (var i = 0; i < $scope.data.length; i++) {
                $scope.amountBeforeDiscountTotal += $scope.data[i].amountBeforeDiscount;
                $scope.amountAfterDiscountTotal += $scope.data[i].amountAfterDiscount;
                $scope.amountAfterDiscountTotal -= $scope.data[i].returnCharges; //Added by Sarubala on 14-10-17
            }
            //console.log($scope.TotalSales);
            //console.log($scope.amountAfterDiscountTotal);
            $scope.netamount = $scope.TotalSales - $scope.amountAfterDiscountTotal;

            var ReturnedAmountFooter = "<div id='totalBeforediscount'>" + $filter('number')($scope.amountBeforeDiscountTotal, 2) + " <div>";
            var NetValueFooter = "<div id='totalAfterDiscount'>" + $filter('number')($scope.amountAfterDiscountTotal, 2) + " <div>";



            var Returnsgrid = $("#Returnsgrid").kendoGrid({
                columnMenu: true,
                pageable: true,
                resizable: true,
                height: 220,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [

                  { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "40px", attributes: { ftype: "sno", class: " text-left field-report" } },
                    { field: "instance.name", title: "Branch", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "returnNo", title: "Return No", width: "60px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" }, footerTemplate: "Grand Total" },
                  
                   { field: "returnDate", title: "Return Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(returnDate), 'dd/MM/yyyy') #" },
                   
                  
                   { field: "amountBeforeDiscount", title: "Returned Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                    { field: "sales.actualInvoice", title: "Bill No", width: "70px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                   { field: "discountedAmount", title: "Discount (Rs.)", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },

                   { field: "returnCharges", title: "Return Charges", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },

                   { field: "roundOffNetAmount", title: "Round off (Rs.)", width: "60px", format: "{0:n}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n2')#</div>" },

                    { field: "returnedPrice", title: "Net Value (Rs.)", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                    { field: "isRoundOff", title: "RoundOff Enabled", width: "60px", type: "string", attributes: { class: "text-left field-report" }, template: "#= isRoundOff=='true' ? 'Y' : 'N' #" }

                ],
                dataSource: {
                    data: resp.data,
                    aggregate: [
                         { field: "returnedPrice", aggregate: "sum" },
                           { field: "discountedAmount", aggregate: "sum" },
                            { field: "amountBeforeDiscount", aggregate: "sum" },

                             { field: "amountAfterDiscount", aggregate: "sum" },
                             { field: "returnCharges", aggregate: "sum" },
                             { field: "roundOffNetAmount", aggregate: "sum" }

                         
                    ], 
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "string" },
                                "returnNo": { type: "string" },                               
                                "returnDate": { type: "date" },                               
                                "amountBeforeDiscount": { type: "number" },
                                "sales.actualInvoice": { type: "string" },
                                "returnCharges":{ type: "number" },
                                "discountedAmount": { type: "number" },
                                "amountAfterDiscount": { type: "number" },
                                "roundOffNetAmount": { type: "number" },
                                "isRoundOff": { type: "string" },
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
            }).data("kendoGrid");


            Returnsgrid.dataSource.originalFilter = Returnsgrid.dataSource.filter;


            Returnsgrid.dataSource.filter = function () {

                if (arguments.length > 0) {
                    this.trigger("filtering", arguments);
                }

                var result = Returnsgrid.dataSource.originalFilter.apply(this, arguments);


                if (arguments.length > 0) {
                    this.trigger("filtering", result);
                }
                return result;
            }
            

            $("#Returnsgrid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                var dataSource = $("#Returnsgrid").data("kendoGrid").dataSource;
                var filters = dataSource.filter();
                var allData = dataSource.data();
                var query = new kendo.data.Query(allData);
                var data = query.filter(filters).data;
                // $scope.Rreturntotal = 0;
                cash = 0;
                credit = 0;
                for (var i = 0; i < $scope.data.length; i++) {

                    if ($scope.data[i].sales.paymentType == "cash") {
                        cash += $scope.data[i].sales.cash;
                    }
                    if ($scope.data[i].sales.paymentType == "credit") {
                        credit += $scope.data[i].sales.credit;
                    }

                }               
                $scope.Rreturntotal = parseInt(cash) + parseInt(credit);

                cashfooter1 = "Total: " + $filter("number")(cash, 2);
                $("#totalCashTemplate1").html(cashfooter1);

                creditfooter1 = "Total: " + $filter("number")(credit, 2);
                $("#totalCreditTemplate1").html(creditfooter1);

                netfooter1 = "Total: " + $filter("number")($scope.Rreturntotal, 2);
                $("#totalFooterTemplate1").html(netfooter1);

            });
          
            if (loadedSales == true) {
                $.LoadingOverlay("hide");
            }
            loadedSalesReturn = true;
        }, function () {
            if (loadedSales == true) {
                $.LoadingOverlay("hide");
            }
        });

    }

    $scope.clearSearch = function () {
        $scope.fromInvoice = "";
        $scope.toInvoice = "";
        $scope.InvoiceSeriesType = "";
        $scope.search.select1 = "";
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.salesReport();

    }
    //

    //$scope.salesReport();

     $scope.filter = function (type) {
         $scope.type = type;
         if ($scope.type === "TODAY") {

             $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
             $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
         }
         if ($scope.type === "Week") {
             var curr = new Date;
             var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
             $scope.from = firstday;
             $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
         }
         if ($scope.type === "Month") {
             var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
             var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
             $scope.from = firstDay;
             $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
         }
        $scope.salesReport();
     }

    // chng-3

     $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
     function addHeader(e) {
         var clen = e.workbook.sheets[0].rows[0].cells.length;
         if ($scope.branchid != undefined) {
             headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
             e.workbook.sheets[0].rows.splice(0, 0, headerCell);
             headerCell = { cells: [{ value: " Sales Invoice Wise", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
             e.workbook.sheets[0].rows.splice(0, 0, headerCell);
             var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
             e.workbook.sheets[0].rows.splice(0, 0, headerCell);
             headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
             e.workbook.sheets[0].rows.splice(0, 0, headerCell);
             //var headerCell = { cells: [], type: "title" };
             //e.workbook.sheets[0].rows.splice(0, 0, headerCell);
             //headerCell = { cells: [], type: "title" };
             //e.workbook.sheets[0].rows.splice(0, 0, headerCell);
             headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
             e.workbook.sheets[0].rows.splice(0, 0, headerCell);
         }
         else {
             headerCell = { cells: [{ value: " Sales Invoice Wise", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
             e.workbook.sheets[0].rows.splice(0, 0, headerCell);
             headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
             e.workbook.sheets[0].rows.splice(0, 0, headerCell);
         }
     }

    //
     $scope.dayDiff = function (frmDate) {

         $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
         $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');
         var day = 24 * 60 * 60 * 1000;

         var fromDate = new Date($scope.dtFrom);
         var today = new Date($scope.today);
         $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

         if ($scope.dayDifference > 60) {
             $scope.dateRangeExceeds = true;
         }
         else {
             $scope.dateRangeExceeds = false;
         }
     };

}]);

