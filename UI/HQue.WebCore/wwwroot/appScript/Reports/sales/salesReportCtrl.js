app.controller('salesReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'salesItemModel', '$filter', 'salesService', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, salesReportService, salesModel, salesItemModel, $filter, salesService) {
    var loadedSales = true;
    var loadedSalesReturn = true;
    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false;
    
    var isComposite = false;

    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.type = ''; //'TODAY';
    $scope.kindNameOptions = [];

    $scope.getPaymentTypes = function () {
        salesService.getPaymentDomainValues().then(function (response) {
            if (response.data.length > 0) {
                $scope.domainValueList = response.data;

                angular.forEach($scope.domainValueList, function (value, key) {
                    if (value.domainId.replace(/\s+/g, '') == "5") {
                        $scope.kindNameOptions.push(value);
                    }
                });
            }

        }, function (error) {
            console.log(error);
        })
    }

    $scope.getPaymentTypes();
    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            isComposite = $scope.instance.gstselect;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);

            //$scope.salesReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.salesReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }      
        var kind = $scope.kind;
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }

        else
            $scope.search.instanceId = $scope.branchid;
        loadedSales = false;
        loadedSalesReturn = false;

        salesReportService.list($scope.type, data, $scope.branchid, kind).then(function (response) {
            $scope.data = response.data;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);
            }

            var totalCash = 0;
            for (var i = 0; i < $scope.data.length; i++) {                
                totalCash += $scope.data[i].reportTotal;
            }

            var total = parseInt(totalCash);
            $scope.total = total;

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales Details";
            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: "Sales Details.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [2500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Sales_Details.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                         { field: "sales.instance.name", title: "Branch", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                  { field: "sales.actualInvoice", title: "Bill No", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "sales.invoiceDate", title: "Bill Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                 
                  { field: "productStock.product.name", title: "Product", width: "140px", attributes: { class: "text-left field-highlight" } },
                  { field: "productStock.product.hsnCode", title: "HsnCode", width: "100px", attributes: { class: "text-left field-report" } },
                   { field: "productStock.product.kindName", title: "Kind", width: "140px", attributes: { class: "text-left field-report" } },
                  { field: "quantity", title: "Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" } },
                  { field: "sellingPrice", title: "Selling Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                    { field: "productStock.purchasePrice", title: "Cost Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }},
                   { field: "productStock.vat", title: "VAT/GST %", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, hidden: isComposite },                 
                  { field: "gstAmount", title: "VAT/GST Amount", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite },
                   { field: "itemAmount", title: "Amount without Tax", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite },
                  { field: "discountAmount", title: "Discount Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                   { field: "productStock.totalCostPrice", title: "Total Cost Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                  { field: "totalAmount", title: "Final Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                  { field: "salesProfit", title: "Profit %", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number"} },
                  { field: "productStock.batchNo", title: "Batch", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.expireDate", title: "Expiry", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                   { field: "sales.name", title: "Patient Name", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "sales.doctorName", title: "Doctor Name", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "sales.address", title: "Patient Address", width: "150px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "sales.credit", title: "Is Credit", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.manufacturer", title: "Manufacturer", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.schedule", title: "Schedule", width: "130px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.type", title: "Product Type", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },                  
                  { field: "productStock.createdAt", title: "Stock Inward Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(productStock.createdAt, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" }

                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        //{ field: "quantity", aggregate: "sum" },
                        //{ field: "sellingPrice", aggregate: "sum" },
                        //{ field: "productStock.purchasePrice", aggregate: "sum" },
                        //{ field: "productStock.vat", aggregate: "sum" },
                        //{ field: "reportActualAmount", aggregate: "sum" },
                        //{ field: "reportVatAmount", aggregate: "sum" },
                        //{ field: "salesDiscountValue", aggregate: "sum" },
                        // { field: "productStock.totalCostPrice", aggregate: "sum" },
                        //{ field: "reportTotal", aggregate: "sum" }
                       
                    ],
                    schema: {
                        model: {
                            fields: {                              
                                "sales.actualInvoice": { type: "string" },
                                "sales.invoiceDate": { type: "date" },                              
                                "productStock.product.name": { type: "string" },
                                "quantity": { type: "number" },
                                "sellingPrice": { type: "number" },
                                "productStock.purchasePrice": { type: "number" },
                                "productStock.vat": { type: "number" },
                                "itemAmount": { type: "number" },
                                "gstAmount": { type: "number" },
                                //"salesDiscountValue": { type: "number" },
                                 "productStock.totalCostPrice": { type: "number" },
                                 "totalAmount": { type: "number" },
                                 "salesProfit": { type: "number" },
                                "productStock.batchNo": { type: "number" },
                                "productStock.expireDate": { type: "date" },
                                "sales.name": { type: "string" },
                                "sales.doctorName": { type: "string" },
                                "sales.address": { type: "string" },
                                "sales.credit": { type: "number" },
                                "productStock.product.manufacturer": { type: "string" },
                                "productStock.product.schedule": { type: "string" },
                                "productStock.product.type": { type: "string" },
                                "productStock.product.hsnCode": { type: "string" }
                            }
                        }
                    },
                    pageSize: 20
        },

            excelExport: function (e) {

                addHeader(e);

                var sheet = e.workbook.sheets[0];
                for (var i = 0; i < sheet.rows.length; i++) {
                    var row = sheet.rows[i];
                    for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                        var cell = row.cells[ci];

                        if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                            cell.hAlign = "left";
                            cell.format = this.columns[ci].attributes.fformat;
                            cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                        }
                        if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                            cell.hAlign = "right";
                            cell.format = "#" + this.columns[ci].attributes.fformat;
                        }

                        if (row.type == "group-footer" || row.type == "footer") {
                            if (cell.value) {
                                cell.value = $.trim($('<div>').html(cell.value).text());
                                cell.value = cell.value.replace('Total:', '');
                                cell.hAlign = "right";
                                cell.format = "#0.00";
                                cell.bold = true;
                            }
                        }
                    }
                }
            },
            });
            if (loadedSalesReturn == true) {
                $.LoadingOverlay("hide");
            }
            loadedSales = true;
        }, function () {
            if (loadedSalesReturn == true) {
                $.LoadingOverlay("hide");
            }
        });

        var kind = $scope.kind;
      
        //Footer section removed on 22/02/2017 by Poongodi
        salesReportService.returnList($scope.type, data, $scope.branchid, 0,null,null,null, kind).then(function (resp) {

            $scope.data = resp.data;
            $scope.type = "";
            var cash = 0;
           
            for (var i = 0; i < $scope.data.length; i++) {
                    cash += $scope.data[i].salesRoundOff;
            }
            $scope.Rreturntotal = parseInt(cash) ;
            //var cashfooter1 = "<div id='totalCashTemplate1'>Total : " + $filter('number')(cash, 2) + " <div>";
            //var netfooter1 = "<div id='totalFooterTemplate1'> Total : " + $filter('number')($scope.Rreturntotal, 2) + " <div>";

            var SalesReturnGrid = $("#SalesReturnGrid").kendoGrid({
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                height:350,
                filterable: {
                    mode: "column"
                },
                columns: [
                 { field: "salesReturn.instance.name", title: "Branch", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                 { field: "salesReturn.returnNo", title: "Return Bill No", width: "120px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                 { field: "salesReturn.returnDate", title: "Return Bill Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(salesReturn.returnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                     { field: "salesReturn.sales.name", title: "Customer Name", width: "130px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.name", title: "Product", width: "130px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.hsnCode", title: "HsnCode", width: "100px", attributes: { class: "text-left field-report" } },
                    { field: "productStock.product.kindName", title: "Kind", width: "140px", attributes: { class: "text-left field-report" } },
                 { field: "quantity", title: "Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }},
                 { field: "productStock.sellingPrice", title: "Selling Rate(MRP)", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }  },
                 { field: "productStock.vat", title: "VAT/GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, hidden: isComposite },
               { field: "vatValue", title: "VAT/GST Amount", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite },
                 { field: "totalWOvat", title: "Amount without Tax", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite },
               { field: "discountAmount", title: "Discount Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
               { field: "salesReturn.returnCharges", title: "Return Charges", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
               { field: "salesRoundOff", title: "Final Value", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }  },
                 { field: "productStock.batchNo", title: "Batch", width: "130px", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "productStock.expireDate", title: "Expiry", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
             
                ],
                dataSource: {
                    data: resp.data,
                    //aggregate: [
                    //    { field: "quantity", aggregate: "sum" },
                    //    { field: "productStock.sellingPrice", aggregate: "sum" },
                    //    { field: "productStock.vat", aggregate: "sum" },
                    //    { field: "vatAmount", aggregate: "sum" },
                    //     { field: "reportVatAmount", aggregate: "sum" },
                    //     { field: "actualAmount", aggregate: "sum" },
                    //     { field: "salesDiscountValue", aggregate: "sum" },
                    //    { field: "salesRoundOff", aggregate: "sum" }

                    //],
                    schema: {
                        model: {
                            fields: {
                                "salesReturn.returnNo": { type: "number" },
                                "salesReturn.returnDate": { type: "date" },                             
                                "productStock.product.name": { type: "string" },
                                "productStock.product.hsnCode": { type: "string" },
                                "productStock.product.category": { type: "string" },
                                "quantity": { type: "number" },
                                "productStock.sellingPrice": { type: "number" },
                                "productStock.vat": { type: "number" },
                                "vatAmount": { type: "number" },
                                "reportVatAmount": { type: "number" },
                                //"actualAmount": { type: "number" },
                                //"salesDiscountValue": { type: "number" },
                                "salesReturn.returnCharges": { type: "number" },
                                "salesRoundOff": { type: "number" },
                                "productStock.batchNo": { type: "string" },
                                "productStock.expireDate": { type: "date" },
                                "salesReturn.sales.name": { type: "string" },
                            }
                        }
                    },
                    pageSize: 20
                },
            }).data("kendoGrid");
            if (loadedSales == true) {
                $.LoadingOverlay("hide");
            }
            loadedSalesReturn = true;
        }, function () {
            if (loadedSales == true) {
                $.LoadingOverlay("hide");
            }
        });
    }


    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "TODAY") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.salesReport();
    }

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Sales Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Sales Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };

}]);