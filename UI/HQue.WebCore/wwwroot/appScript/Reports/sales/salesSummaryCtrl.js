app.controller('salesSummaryReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'salesItemModel', '$filter', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, salesReportService, salesModel, salesItemModel, $filter) {

    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    var sno = 0;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false;

    //$scope.branchid = "";
    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };


    $scope.type = ''; //'TODAY';

    //2
    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            //$scope.salesReport();
            //$scope.salesReturnReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.ChangeInvoiceSeriesDropdown = function (seriestype) {
        //console.log(seriestype);

        $scope.InvoiceSeriesType = seriestype;
        $scope.search.select1 = "";

        if ($scope.InvoiceSeriesType == '1') {
            $scope.search.select1 = "";
            $scope.InvoiceSeriesItems = [];
        }
        if ($scope.InvoiceSeriesType == '2') {
            getInvoiceSeriesItems();
        }
        if ($scope.InvoiceSeriesType == '3') {
            $scope.search.select1 = "MAN";
            $scope.InvoiceSeriesItems = [];
        }
        if ($scope.InvoiceSeriesType == '4') {
            $scope.search.select1 = "MAN";
        }
    }

    function getInvoiceSeriesItems() {

        salesReportService.getInvoiceSeriesItems($scope.InvoiceSeriesType).then(function (response) {
            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }
        }, function () {

        });
    }

    $scope.clearSearch = function () {
        $scope.paymentType = '';
        $scope.fromInvoice = null;
        $scope.toInvoice = null;
        $("#grid").empty();
        $scope.dateRangeExceeds = false;
    }

    $scope.salesReport = function () {
        $.LoadingOverlay("show");
        $("#grid").empty();
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }        

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }
        //Default value handled while opening report
        if ($scope.InvoiceSeriesType == undefined || $scope.InvoiceSeriesType == null) {
            $scope.InvoiceSeriesType = "";
        }
        if ($scope.search.select1 == undefined || $scope.search.select1 == null) {
            $scope.search.select1 = "";
        }

        if ($scope.fromInvoice == undefined || $scope.fromInvoice == null) {
            $scope.fromInvoice = "";
        }
        if ($scope.toInvoice == undefined || $scope.toInvoice == null) {
            $scope.toInvoice = "";
        }

        var InvoiceSeries = "";
        if ($scope.InvoiceSeriesType == "") {
            InvoiceSeries = "ALL"
        }
        else if ($scope.InvoiceSeriesType == 2 && $scope.search.select1 == "") {
            for (var i = 0; i < $scope.InvoiceSeriesItems.length; i++) {
                if (InvoiceSeries != "")
                    InvoiceSeries = InvoiceSeries + ",";
                InvoiceSeries = InvoiceSeries + $scope.InvoiceSeriesItems[i].seriesName;
            }
        }
        else {
            InvoiceSeries = $scope.search.select1;
        }
        //if ($scope.fromInvoice != '') {
        //    $scope.from = null;
        //    $scope.to = null;
        //    data.fromDate = null;
        //    data.toDate = null;
        //}
        //if ($scope.type == "TODAY" || $scope.type == "Week" || $scope.type == "Month") {
        //    $scope.fromInvoice = '';
        //    $scope.toInvoice = '';
        //    $scope.InvoiceSeriesType = "";
        //}
        setfileName();
        salesReportService.salesSummarylist($scope.type, data, $scope.branchid, InvoiceSeries, $scope.fromInvoice, $scope.toInvoice, $scope.paymentType)
            .then(function (response) {
            $scope.data = response.data;
            //var Totalsalesprofit = 0;
            //for (var i = 0; i < $scope.data.length; i++) {
            //    //Totalsalesprofit += $scope.data[i].sales.profit;
            //    $scope.data[i].invoiceAmount = $scope.data[i].invoiceAmount - $scope.data[i].discount;

            //}
            //$scope.SalesProfit = Totalsalesprofit;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);
            }

            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>" + "Sales Summary";
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales Summary";
            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: "Sales Summary.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Sales_Summary.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                filterable: true,
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,

                serverFiltering: true,
                //height:100vh,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                      { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "60px", attributes: { ftype: "sno", class: " text-left field-report" } },
                      { field: "instance.name", title: "Branch", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "invoiceDate", title: "Date", width: "60px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, footerTemplate: "Grand Total", template: "#= kendo.toString(kendo.parseDate(invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  { field: "totalSales", title: "No. of Bills", width: "70px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },

                   { field: "cash", title: "Cash", width: "60px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                    { field: "card", title: "Card", width: "60px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                     { field: "cheque", title: "Cheque", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                     { field: "credit", title: "Outstanding", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                     { field: "ewallet", title: "eWallet", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                     ////new field total
                     // { field: "Total", title: "Total", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                     // //new field Return amt
                     // { field: "Return Amount", title: "returnamount", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },

                     

                  { field: "finalValue", title: "Cost Price", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                  { field: "invoiceAmount", title: "Sale Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                     { field: "returnedPurchasePrice", title: "Cost Price (Return)", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                  { field: "returnValue", title: "Return Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                  { field: "netValue", title: "Net Value", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                  { field: "profit", title: "Sale Profit", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right text-bold", ftype: "number", fformat: "0.n" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                      { field: "amount", title: "Sale Profit %", width: "60px", format: "{0:n}", type: "number", attributes: { class: "text-right text-bold", ftype: "number", fformat: "0.n" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(average, 'n2') #</div>" }

                ],
                dataSource: {
                    data: response.data,
                    aggregate: [

                        { field: "totalSales", aggregate: "sum" },
                        { field: "cash", aggregate: "sum" },
                        { field: "card", aggregate: "sum" },
                        { field: "credit", aggregate: "sum" },
                        { field: "cheque", aggregate: "sum" },
                        { field: "ewallet", aggregate: "sum"},
                        { field: "invoiceAmount", aggregate: "sum" },
                        { field: "finalValue", aggregate: "sum" },
                          { field: "netValue", aggregate: "sum" },
                        { field: "returnValue", aggregate: "sum" },
                        { field: "returnedPurchasePrice", aggregate: "sum" },
                        { field: "profit", aggregate: "sum" },
                    { field: "amount", aggregate: "average" }

                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "number" },
                                "instance.name": { type: "string" },
                                "invoiceDate": { type: "date" },
                                "totalSales": { type: "number" },
                                "cash": { type: "number" },
                                "card": { type: "number" },
                                "cheque": { type: "number" },
                                "credit": { type: "number" },
                                "ewallet": { type: "number" },
                                //"total": { type: "number" },
                                //"returnamount": { type: "number" },
                                "finalValue": { type: "number" },
                                "invoiceAmount": { type: "number" },
                                "returnValue": { type: "number" },
                                "netValue": { type: "number" },
                                "profit": { type: "number" },
                                "amount": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            //added by nandhini for excel s.no
                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }
                            //end
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.SalesProfit = 0;
    $scope.SalesReturnProfit = 0;
    $scope.SummarysalesReport = function () {
        $scope.salesReport();
        //$scope.salesReturnReport();

    };

    $scope.salesReturnReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        salesReportService.salesSummaryReturnlist($scope.type, data).then(function (response) {
            console.log(JSON.stringify(response.data));
            $scope.data = response.data;

            var TotalsalesReturnprofit = 0;
            for (var i = 0; i < $scope.data.length; i++) {
                TotalsalesReturnprofit += $scope.data[i].returnProfit;
            }
            $scope.SalesReturnProfit = TotalsalesReturnprofit;

            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales Return Summary";
            }


            $("#gridR").kendoGrid({
                excel: {
                    fileName: "Sales Summary.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Sales_Summary.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },

                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "salesReturn.returnDate", title: "Return Invoice Date", width: "70px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(salesReturn.returnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  { field: "totalSalesReturn", title: "Total Sales Return", width: "70px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                  { field: "totalPurchasePrice", title: "Total Purchases Return", width: "70px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                  { field: "totalMrp", title: "Total MRP Return", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "returnProfit", title: "Profit Return", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right text-bold", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "totalSalesReturn", aggregate: "sum" },
                        { field: "totalPurchasePrice", aggregate: "sum" },
                        { field: "totalMrp", aggregate: "sum" },
                        { field: "returnProfit", aggregate: "sum" }

                    ],
                    schema: {
                        model: {
                            fields: {
                                "salesReturn.returnDate": { type: "date" },
                                "totalSalesReturn": { type: "number" },
                                "totalPurchasePrice": { type: "number" },
                                "totalMrp": { type: "number" },
                                "returnProfit": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = new Date(cell.value);
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }


    //$scope.salesReport();
    $scope.order = ['instance.name', 'reportInvoiceDate', 'totalSales', 'cash', 'card', 'cheque', 'credit','ewallet', 'finalValue', 'invoiceAmount', 'returnedPurchasePrice', 'returnValue', 'netValue', 'profit', 'amount'];
    $scope.filter = function (type) {
        $scope.type = type;
        $scope.fromInvoice = null;
        $scope.toInvoice = null;
        $scope.paymentType = null;
        if ($scope.type === "TODAY") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.salesReport();

        // $scope.salesReturnReport();
    }

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Sales Summary", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Sales Summary", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }
    function setfileName() {
        $scope.fileName = "SalesSummary_" + $scope.instance.name;
    }

    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');
        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);

