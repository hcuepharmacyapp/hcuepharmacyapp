app.controller('drugWiseReportCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'salesReportService', 'productService', 'salesModel', 'salesItemModel', '$filter', function ($scope, $rootScope, $http, $interval, $q, salesReportService, productService, salesModel, salesItemModel, $filter) {

    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.checkFromDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#fromDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validFromDate = true;

                    if (toDate != undefined && toDate != null) {
                        $scope.checkToDate1(frmDate, toDate);
                    }
                }
                else {
                    $scope.validFromDate = false;
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else
            $scope.validFromDate = true;
    }

    $scope.checkToDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#toDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validToDate = true;
                    $scope.checkToDate1(frmDate, toDate);
                }
                else {
                    $scope.validToDate = false;
                }
            }
            else {
                $scope.validToDate = false;
            }
        }
        else
            $scope.validToDate = true;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };


    //$scope.type = 'TODAY';

    // chng-2

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.drugWiseReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    //$scope.init = function () {
    //    $.LoadingOverlay("show");
    //    salesReportService.getInstanceData().then(function (response) {
    //        $.LoadingOverlay("hide");
    //        $scope.instance = response.data;
    //    }, function () {
    //        $.LoadingOverlay("hide");
    //    });
    //}

    //FromInvoice, ToInvoice & InvoiceSeries filter added by settu on 27-05-2017
    $scope.ChangeInvoiceSeriesDropdown = function (seriestype) {

        $scope.InvoiceSeriesType = seriestype;
        $scope.search.select1 = "";

        if ($scope.InvoiceSeriesType == '1') {
            $scope.search.select1 = "";
            $scope.InvoiceSeriesItems = [];
        }
        if ($scope.InvoiceSeriesType == '2') {
            getInvoiceSeriesItems();
        }
        if ($scope.InvoiceSeriesType == '3') {
            $scope.search.select1 = "MAN";
            $scope.InvoiceSeriesItems = [];
        }
    }

    $scope.InvoiceSeriesItems = [];
    function getInvoiceSeriesItems() {

        salesReportService.getInvoiceSeriesItems($scope.InvoiceSeriesType).then(function (response) {

            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }

        }, function () {

        });
    }

    $scope.getProducts = function (val) {
        var instanceid = '';
        if ($scope.branchid == undefined || $scope.branchid == '')
            instanceid = $scope.instance.id;
        else
            instanceid = $scope.branchid;

        if (instanceid != undefined && instanceid != null) {
            return productService.InstanceWisedrugFilter(instanceid, val).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        }
    };

    $scope.product = null;
    $scope.selectedProduct = "";
    $scope.onProductSelect = function (obj) {
        $scope.product = obj;
    };

    $scope.clearSearch = function () {
        $scope.type = 'TODAY';
        $scope.product = null;
        $scope.selectedProduct = "";
        $scope.InvoiceSeriesItems = [];
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.fromInvoice = "";
        $scope.toInvoice = "";
        $scope.InvoiceSeriesType = "";
        $scope.search.select1 = "";
        $scope.data = [];
        $("#grid").empty();
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;
    }

    $scope.focusNextId = function (id) {
        var ele = document.getElementById(id);
        if (ele.disabled == true)
            document.getElementById("fromInvoice").focus();
        else
            ele.focus();
    };
    //Endhere
    $scope.drugWiseReport = function () {
        $.LoadingOverlay("show");

        //Default value handled while opening report
        if ($scope.InvoiceSeriesType == undefined || $scope.InvoiceSeriesType == null) {
            $scope.InvoiceSeriesType = "";
        }
        if ($scope.search.select1 == undefined || $scope.search.select1 == null) {
            $scope.search.select1 = "";
        }

        if ($scope.fromInvoice == undefined || $scope.fromInvoice == null) {
            $scope.fromInvoice = "";
        }
        if ($scope.toInvoice == undefined || $scope.toInvoice == null) {
            $scope.toInvoice = "";
        }

        var InvoiceSeries = "";
        if ($scope.InvoiceSeriesType == "") {
            InvoiceSeries = "ALL"
        }
        else if ($scope.InvoiceSeriesType == 2 && $scope.search.select1 == "") {
            for (var i = 0; i < $scope.InvoiceSeriesItems.length; i++) {
                if (InvoiceSeries != "")
                    InvoiceSeries = InvoiceSeries + ",";
                InvoiceSeries = InvoiceSeries + $scope.InvoiceSeriesItems[i].seriesName;
            }
        }
        else {
            InvoiceSeries = $scope.search.select1;
        }
        var productVal = "";
        if(typeof($scope.selectedProduct) == "string")
            productVal = $scope.selectedProduct;
        else
            productVal = $scope.selectedProduct.id;

        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        //var instanceid = $scope.branchid;
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }
        if ($scope.type == undefined)
        {
            $scope.type ="";
        }
        salesReportService.drugWiseList(data, $scope.type, $scope.branchid, InvoiceSeries, $scope.fromInvoice, $scope.toInvoice, productVal).then(function (response) {
            $scope.data = response.data;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);
            }
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Drug Wise Sale";
            }

            $("#grid").kendoGrid({
                //toolbar: ["excel", "pdf"],
                excel: {
                    fileName: "Drug Wise Sale.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Drug Wise Sale.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "productStock.instanceName", title: "Branch", width: "100px", format: "{0}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.name", title: "Product", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-left field-highlight" } },
                  { field: "quantity", title: "Sold Quantity", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total Sold : <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                  { field: "productStock.stock", title: "Available Quantity", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total Stock : <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                  { field: "productStock.product.rackNo", title: "Rack No", width: "90px", format: "{0}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.boxNo", title: "Box No", width: "90px", format: "{0}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "productStock.vendor.name", title: "Last Purchase Vendor", width: "120px", format: "{0}", type: "string", attributes: { class: "text-left field-report" } },
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                       
                        { field: "quantity", aggregate: "sum" },
                        { field: "productStock.stock", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                                "productStock.instanceName": { type: "string" },
                                "productStock.product.name": { type: "string" },
                                "quantity": { type: "number" },
                                "productStock.stock": { type: "number" },
                                "productStock.product.rackNo": { type: "string" },
                                "productStock.product.boxNo": { type: "string" },
                                "productStock.vendor.name": { type: "string" },
                            }
                        }
                    },
                    pageSize: 20
                },
                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    //End Chng 2

    //$scope.drugWiseReport();

    //$scope.filter = function (type) {
    //    $scope.type = type;
    //    $scope.drugWiseReport();
    //}
    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.drugWiseReport();
    };

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Drug Wise Sale", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            //var headerCell = { cells: [], type: "title" };
            //e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            //headerCell = { cells: [], type: "title" };
            //e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Drug Wise Sale", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    //
    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);
