app.controller('doctorSearchCtrl', function ($scope, $rootScope, doctorService, doctorModel, pagerServcie, salesService, toastr, $filter) {
    var doctormodel = doctorModel;
    $scope.search = doctormodel;
    $scope.temp = angular.copy(doctorModel);

    $scope.list = [];
    $scope.showAddDoctor = false;
    $scope.enableAddDoctor = true;
    $scope.editDoctor1 = false;
    $scope.doctorname = null;
    $scope.shouldBeOpen = false;
    $scope.search.defaultDoctorName = null;
    $scope.defaultDoctor = null;

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        $.LoadingOverlay("show");
        doctorService.getDoctorList($scope.search)
            .then(function (response) {
                $scope.list = response.data.list;
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
    }

    $scope.changefilters = function () {
        $scope.search.values = "";
        if ($scope.search.select == 'name') {
            $scope.nameCondition = true;
        } else {
            $scope.nameCondition = false;
        }
    };


    $scope.getDefaultDoctor = function () {
        doctorService.getDefaultDoctor()
            .then(function (response) {
            $scope.defaultDoctor = response.data;

        }, function () { });
    };

    $scope.doctorSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        doctorService.getDoctorList($scope.search)
            .then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $scope.getDefaultDoctor();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.doctorSearch();

    $scope.addDoctor = function () {
        $scope.showAddDoctor = true;
        $scope.enableAddDoctor = false;
        $scope.shouldBeOpen = true;
    };

    $scope.doctorCreate = function () {

        $.LoadingOverlay("show");
        doctorService.create($scope.search).then(function (response) {
            if (response.data != null && response.data != '') {
                toastr.success("Doctor added successfully");
                $scope.clear();
                $scope.search.page = pagerServcie.page;
                $scope.showAddDoctor = false;
                $scope.enableAddDoctor = true;
                $scope.shouldBeOpen = false;
                $scope.isEdit = false;
                $scope.doctorSearch();
                $.LoadingOverlay("hide");
            } else {
                toastr.error("Short name already exists");
                $.LoadingOverlay("hide");
            }

        }, function (error) {
            toastr.error("Error Occurred");
            $.LoadingOverlay("hide");
        })
    };

    $scope.clear = function () {
        //$scope.doctorForm.$setPristine();
        //$scope.search = angular.copy($scope.temp);
        $scope.search.name = "";
        $scope.search.shortName = "";
        $scope.search.mobile = "";
        $scope.search.commission = "";
        $scope.search.area = "";
        $scope.search.city = "";
        $scope.search.state = "";
        $scope.search.pincode = "";
        $scope.search.gsTin = "";

        if ($scope.search.shortName != null && $scope.search.shortName != undefined && $scope.search.shortName != '') {
            $scope.isEdit = true;
        } else {
            $scope.isEdit = false;
        }

        angular.element("#txt_name").focus();

    };

    $scope.clearSearch = function () {
        $scope.search.select = "";
        $scope.search.values = "";
        $scope.search.doctorName = null;
        $scope.search.defaultDoctorName = null;

    };


    $scope.editDoctor = function (id) {
        doctorService.getDoctorById(id).then(function (response) {
            $scope.search = response.data;
            $scope.showAddDoctor = true;
            $scope.enableAddDoctor = false;
            $scope.editDoctor1 = true;
            $scope.shouldBeOpen = true;

            if ($scope.search.shortName != null && $scope.search.shortName != undefined && $scope.search.shortName != '') {
                $scope.isEdit = true;
            } else {
                $scope.isEdit = false;
            }

        }, function (error) {
            console.log(error);
            toastr.error("Error Occurred");
        });

    };

    $scope.updateDoctor = function () {
        $.LoadingOverlay("show");
        doctorService.update($scope.search).then(function (response) {
            if (response.data != null && response.data != '') {
                toastr.success("Updated successfully");
                $scope.clear();
                $scope.search.page = pagerServcie.page;
                $scope.showAddDoctor = false;
                $scope.enableAddDoctor = true;
                $scope.editDoctor1 = false;
                $scope.shouldBeOpen = false;

                if ($scope.search.shortName != null && $scope.search.shortName != undefined && $scope.search.shortName != '') {
                    $scope.isEdit = true;
                } else {
                    $scope.isEdit = false;
                }

                $scope.doctorSearch();
                $.LoadingOverlay("hide");
            } else {
                toastr.error("Short name already exists");
                $.LoadingOverlay("hide");
            }


        }, function (error) {
            toastr.error("Error Occurred");
            $.LoadingOverlay("hide");
        })
    };

    $scope.saveDefaultDoctor = function (doctor) {
        $.LoadingOverlay("show");
        doctorService.saveDefaultDoctor(doctor).then(function (response) {
            toastr.success("Default Doctor Saved");
            $scope.doctorSearch();
            $.LoadingOverlay("hide");
        }, function (error) {
            toastr.error("Error Occurred");
            $scope.doctorSearch();
            $.LoadingOverlay("hide");
        })
    };

    $scope.resetDefaultDoctor = function () {
        $.LoadingOverlay("show");
        if ($scope.defaultDoctor != null && $scope.defaultDoctor != '') {
            doctorService.resetDefaultDoctor($scope.defaultDoctor).then(function (response) {
                toastr.success("Default Doctor is Reset");
                $scope.doctorSearch();
                $.LoadingOverlay("hide");
            }, function (error) {
                toastr.error("Error Occurred");
                $scope.doctorSearch();
                $.LoadingOverlay("hide");
            })
        } else {
            toastr.error("Default Doctor is not selected");
            $scope.doctorSearch();
            $.LoadingOverlay("hide");
        }
    };

    $scope.doctorNameSearch = function (obj, event) {
        $scope.search.values = obj.id;
        $scope.search.doctorName = obj.name;
        $scope.doctorSearch();
    };

    $scope.defaultDoctorNameSearch = function (val1) {
        if (val1 != null && val1.id != null) {
            $scope.saveDefaultDoctor(val1);
        } else {
            toastr.error("Doctor not selected");
        }

    };

    $scope.DoctorNameSearch = "1";
    function getDoctorSearchType() {
        salesService.getDoctorSearchType().then(function (response) {
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.DoctorNameSearch = "1";
            } else {
                if (response.data.doctorSearchType != undefined) {
                    $scope.DoctorNameSearch = response.data.doctorSearchType;
                } else {
                    $scope.DoctorNameSearch = "1";
                }
            }
        }, function () {

        });
    }

    getDoctorSearchType();


    $scope.DoctorsList = function (val) {


        if ($scope.DoctorNameSearch == "" || $scope.DoctorNameSearch == undefined) {
            $scope.DoctorNameSearch = 2;
        }


        if ($scope.DoctorNameSearch == 1) {
            return doctorService.Locallist(val).then(function (response) {

                var origArr = response.data;
                var newArr = [],
                origLen = origArr.length,
                found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });


            }, function (error) {
                console.log(error);
            });
        } else {
            return doctorService.list(val).then(function (response) {

                var origArr = response.data;
                var newArr = [],
           origLen = origArr.length,
           found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });


            }, function (error) {
                console.log(error);
            });

        }





    };


});

