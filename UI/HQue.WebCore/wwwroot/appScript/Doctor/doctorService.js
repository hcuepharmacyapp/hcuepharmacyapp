app.factory('doctorService', function ($http) {
    return {
        list: function (searchData) {
            return $http.post("/doctor/DoctorList?doctorName=" + searchData);
        },


        Locallist: function (searchData) {
            return $http.post("/doctor/LocalDoctorList?doctorName=" + searchData);
        },

        getDoctorList: function (data) {
            return $http.post("/doctor/getDoctorList", data);
        },

        create: function (data) {
            return $http.post("/doctor/createDoctor", data);
        },

        getDoctorById: function (id) {
            return $http.get("/doctor/getDoctorById?id=" + id);
        },

        update: function (data) {
            return $http.post("/doctor/UpdateDoctor", data);
        },

        getDoctorListProfile: function (searchData) {
            return $http.post("/doctor/DoctorListProfile?doctorName=" + searchData);
        },

        getDoctorSales: function (searchData) {
            return $http.post("/doctor/GetDoctorSales?doctorId=" + searchData);
        },

        saveDefaultDoctor: function (data) {
            return $http.post("/doctor/saveDefaultDoctor", data);
        },

        getDefaultDoctor: function () {
            return $http.get("/doctor/getDefaultDoctor");
        },

        resetDefaultDoctor: function (doctor) {
            return $http.post("/doctor/resetDefaultDoctor", doctor);
        },

        LocalShortlist: function (searchData) {
            return $http.post("/doctor/LocalShortDoctorList?shortDoctorname=" + searchData);
        },

        //test update
    }
});
