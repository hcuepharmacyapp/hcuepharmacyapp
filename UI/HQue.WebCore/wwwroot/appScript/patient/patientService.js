app.factory('patientService', function ($http) {
    return {
        list: function (patient, department) {
            //return $http.post('/patient/list', patient);
            return $http.post("/patient/list?department=" + department, patient);
        },
        localList: function (patient, department) {
            //  return $http.post('/patient/LocalList', patient);
            return $http.post("/patient/LocalList?department=" + department, patient);
        },

        //Modified by Violet on 06-06-2017
        create: function (patient) {
            return $http.post('/patient/PatientCreate', patient);
        },

        update: function (data) {
            return $http.post('/Patient/PatientUpdate', data);
        },
        customerList: function (sale) {
            return $http.post('/patientHistory/listData', sale);
        },

        customerListBulkSms: function (sale) {
            return $http.post('/patientHistory/listDataBulkSms', sale);
        },

        checkPaymentDone: function (customerId) {
            return $http.post('/patientHistory/CheckOBPaymentPaid?customerId=' + customerId);
        },

        customerInformation: function (sale) {
            return $http.post('/patientHistory/customerInformation', sale);
        },

        patientSaleInformation: function (sale) {
            return $http.post('/patientHistory/patientSaleList', sale);
        },

        Patientlist: function (filter, department) {
            return $http.get('/patient/Patientlist?patientName=' + filter + '&department=' + department);
        },
        GetPatientName: function (filter, department) {
            return $http.get('/patient/GetPatientName?patientName=' + filter + '&department=' + department);
        },
        GetActivePatientName: function (filter, department, isActive) {
            return $http.get('/patient/GetPatientName?patientName=' + filter + '&department=' + department + '&isActive=' + isActive);
        },
        // Newly Added by violet on 09/06/2017

        GetSalesPatientName: function (filter, department) {
            return $http.get('/patient/GetSalesPatientName?patientName=' + filter + '&department=' + department);
        },
        //Newly created by Mani on 16-May-2017
        GetPatientMobile: function (filter, department) {
            return $http.get('/patient/GetPatientMobile?patientMobile=' + filter + '&department=' + department);
        },
        GetPatientId: function (filter) {
            return $http.get('/patient/GetPatientId?patientId=' + filter);
        },
        getInstanceDetails: function () {
            return $http.get('/sales/getInstanceValues');
        },
        //GetPatientId: function (filter, instanceid) {
        //    return $http.get('/patient/GetPatientId?patientId=' + filter + '&instanceid=' + instanceid);
        //},

        getCustomerDiscount: function (customer) {
            return $http.post('/patient/GetCustomerDiscount', customer);
        },
        sendSms: function (CustomerSms) {
            return $http.post('/patientHistory/sendSms', CustomerSms);
        },

        GetShortPatientName: function (filter) {
            return $http.get('/patient/GetShortPatientName?patientName=' + filter);
        },
        PatientShortlist: function (filter) {
            return $http.get('/patient/PatientShortlist?patientName=' + filter);
        },
        custPaymentStatus: function (CustName, CustMobile) {
            return $http.get('/patient/custPaymentStatus?CustName=' + CustName + '&CustMobile=' + CustMobile);
        },
        //Added by Sarubala on 20-11-17
        getCustomerBulkSmsSetting: function () {
            return $http.get('/patientHistory/GetCustomerBulkSmsSetting');
        },
    }
});