app.controller('vendorSearchCtrl', function ($scope, vendorService, vendorModel, pagerServcie) {

    var vendor = vendorModel;

    $scope.search = vendor;

    $scope.list = [];

    $scope.vendorStatus = 2;

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        $.LoadingOverlay("show");
        vendorService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.vendorSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        vendorService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
             $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.vendorSearch();

    $scope.vendor = function () {
        //vendorService.vendorAllData().then(function (response) {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) {
            $scope.vendorList = response.data;
        }, function () { });
    }
    $scope.vendor();

});