app.controller('vendorEditCtrl', function ($scope, vendorModel, vendorService, toastr) {

    var vendor = vendorModel;

    $scope.vendor = vendor;

    // Newly Gavaskar
    $scope.vendor.mobile1Checked = false;
    $scope.vendor.mobile2Checked = false;

    var accountId = null;
    $scope.vendor.tempDrugLicenseNo = null;

    $scope.init = function (id) {
        $.LoadingOverlay("show");
        $scope.vendor.id = id;
        vendorService.vendorDetail($scope.vendor)
            .then(function (response) {
                $scope.vendor = response.data;
                //accountId = $scope.vendor.accountId; 
                $scope.vendor.tempDrugLicenseNo = ($scope.vendor.drugLicenseNo == undefined) ? '' : $scope.vendor.drugLicenseNo.toUpperCase();
                $scope.vendor.drugLicenseNo = ($scope.vendor.drugLicenseNo == undefined) ? '' : $scope.vendor.drugLicenseNo.toUpperCase();
                if ($scope.vendor.stateRef != undefined) {
                    $scope.stateCode = $scope.vendor.stateRef.stateCode;
                    $scope.selectedState = $scope.vendor.stateRef.stateName;
                }
                
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                toastr.error("", "Error");
                $.LoadingOverlay("hide");
            });
    };
   
    $scope.vendorUpdate = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if ($scope.selectedState == "") {
            $scope.stateCode = "";
            $scope.vendor.stateId = null;
        }
        vendorService.update($scope.vendor)
            .then(function (response) {
                if (response.data != "") {
                    $scope.vendorForm.$setPristine();
                    window.location.assign("/Vendor/list");
                    toastr.success('Updated vendor successfully');
                }
                else
                    toastr.error("Drug License No already exists", "Error");
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                toastr.error("", "Error");
                $.LoadingOverlay("hide");
            });

        $scope.isProcessing = false;
    };

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            window.location.assign("/Vendor/list");
        }
    };

    // Newly Added Gavaskar 22-10-2016 Start

    $scope.checkMobile1 = function () {
        if ($scope.vendor.mobile1Checked == true) {
            if ($scope.vendor.mobile1 == null || $scope.vendor.mobile1 == "") {
                $scope.ErrorMsgMobile1 = true;
            } else {
                $scope.ErrorMsgMobile1 = false;
            }
        } else {
            $scope.ErrorMsgMobile1 = false;
        }
    };
 
    $scope.checkAllPurchasefields = function (ind) {

        if (ind > 1) {
            if ($scope.vendor.name == undefined || Object.keys($scope.vendor.name).length == 0) {
                var ele = document.getElementById("vendorName");
                ele.focus();
                return false;
            }
        }


        if (ind > 6) {
            if ($scope.vendor.drugLicenseNo == undefined || $scope.vendor.drugLicenseNo == null) {
                var ele = document.getElementById("DrugLicenseNo");
                ele.focus();
                return false;
            }
        }
        //added by nandhini
        if (ind > 8) {
            if ($scope.vendor.gsTinNo == undefined || $scope.vendor.gsTinNo == null) {
                var ele = document.getElementById("gsTinNo");
                ele.focus();
                return false;
            }
        }

        if (ind > 19) {

            if ($scope.vendor.locationType == undefined || $scope.vendor.locationType == null) {
                var ele = document.getElementById("locationType");
                ele.focus();
                return false;
            }
        }
        if (ind > 23) {

            if ($scope.vendor.vendorType == undefined || $scope.vendor.vendorType == null) {
                var ele = document.getElementById("vendorType");
                ele.focus();
                return false;
            }
        }

//end

    };


    $scope.moveNext = function (currentId, nextId) {

        if (currentId == "vendorName" || currentId == "DrugLicenseNo" || currentId == "gsTinNo" || currentId == "locationType" || currentId == "vendorType") {
            var value1 = document.getElementById(currentId).value;
            if (value1 != null && value1 != "") {
                document.getElementById(nextId).focus();
            } else {
                document.getElementById(currentId).focus();
            }
        }
        else if (currentId == "paymentType") {
            if ($scope.vendor.paymentType == "Credit") {
                //document.getElementById(nextId).focus();
                document.getElementById("creditDays").focus();
            }
            else if ($scope.vendor.paymentType == "Cash") {
                document.getElementById("state").focus();
            }
            else {
                document.getElementById("state").focus();
            }

        } else {
            if ($scope.selectedState == "") {
                $scope.stateCode = "";
                $scope.vendor.stateId = null;
            }
            document.getElementById(nextId).focus();
        }

    };

    $scope.submit = function () {
        if (!$scope.isProcessing && $scope.vendorForm.$valid) {
            $scope.vendorUpdate();
        }

    };


    $scope.checkMobile2 = function () {
        if ($scope.vendor.mobile2Checked == true) {
            if ($scope.vendor.mobile2 == null || $scope.vendor.mobile2 == "") {
                $scope.ErrorMsgMobile2 = true;
            } else {
                $scope.ErrorMsgMobile2 = false;
            }
        } else {
            $scope.ErrorMsgMobile2 = false;
        }
    };

    // Newly Added Gavaskar 22-10-2016 End
    $scope.getStates = function (val) {
        return vendorService.vendorState(val).then(function (response) {            
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.onStateSelect = function (val) {
        $scope.stateCode = val.stateCode;
        $scope.selectedState = val.stateName;
        $scope.vendor.stateId = val.id;
        $scope.vendor.stateRef.id = val.id;
        document.getElementById("state").focus();
    };
});
