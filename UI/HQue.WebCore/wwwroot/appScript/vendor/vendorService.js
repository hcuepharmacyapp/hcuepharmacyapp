app.factory('vendorService', function ($http) {
    return {
        "list": function (searchData) {
            return $http.post('/VendorData/listData', searchData);
        },
        "create": function (vendor) {
            return $http.post('/VendorData/index', vendor);
        },
        "update": function (vendor) {
            return $http.put('/VendorData/Update', vendor);
        },
        "vendorData": function () {
            return $http.get('/VendorData/VendorList');
        },
        "vendorAllData": function () {
            return $http.get('/VendorData/vendorAllData');
        },
        "vendorDataList": function (vendorStatus) {
            return $http.get('/VendorData/VendorDataList?vendorStatus=' + vendorStatus);
        },
        "vendorDetail": function (vendor) {
            return $http.post('/VendorData/GetById', vendor);
        },
        "checkPaymentDone": function (customerId) {
            return $http.post('/VendorData/CheckOBPaymentPaid?customerId=' + customerId);
        },
        "vendorState": function (stateName) {
            return $http.get('/VendorData/GetState?stateName='+ stateName);
        },
        // Added by Gavaskar Vendor bulk update 14-08-2017
        bulkUpdate: function (data) {
            return $http.post('/VendorData/bulkUpdate', data);
        },
        savePurchaseFormula: function (data) {
            return $http.post('/VendorData/SavePurchaseFormula', data);
        },
        getPurchaseFormulaList: function (data) {
            return $http.get('/VendorData/GetPurchaseFormulaList?formulaName=' + data);
        },
        paymentStatus: function (vendorId) {
            return $http.get('/VendorData/PaymentStatus?vendorId=' + vendorId);
        },
        purchaseSettings: function () {
            return $http.get('/vendorPurchase/getBuyInvoiceDateEditSetting');
        },
        //Added by Sarubala on 17-11-17
        GetVendorCreateSmsSetting: function () {
            return $http.get('/VendorData/GetVendorCreateSmsSetting');
        },
        checkExisitingProductList: function (vendorId) {
            return $http.get('/VendorData/CheckExisitingProductList?vendorId=' + vendorId);
        },
    };
});