app.controller('updateAccountSetupCtrl', function ($scope, accountSetupService, accountModel, $filter, toastr) {
    var account = accountModel;
    $scope.account = account;
    $scope.list = [];
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        "opened": false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.open3 = function () {
        $scope.popup3.opened = true;
    };
    $scope.popup3 = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.data = [];
    $scope.selectedRegisterType
    $scope.selectedType = [
        { "name": "Live", "value" : "2" },
        { "name": "Test", "value": "3" },
        { "name": "Trial", "value": "4" }
    ];
    
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.selectedInstanceType = null;
    $scope.getInstanceTypes = function () {

        accountSetupService.getInstanceType().then(function (response) {
            $scope.selectedInstanceType = response.data;
            if (response.data == '') {
                $scope.selectedInstanceType = [
                { "displayText": "Only Offline", "domainValue": "1" },
                { "displayText": "Both Online and Offline", "domainValue": "2" },
                { "displayText": "Only Online", "domainValue": "3" }
                ];
            }
        }, function (error) {
            $scope.selectedInstanceType = [
                { "displayText": "Only Offline", "domainValue": "1" },
                { "displayText": "Both Online and Offline", "domainValue": "2" },
                { "displayText": "Only Online", "domainValue": "3" }
            ];
        });
    };
    $scope.getInstanceTypes();

    $scope.setInstanceCount = function (instanceType) {
        if (instanceType == "1") {
            $scope.account.instanceCount = 1;
        } else {
            $scope.account.instanceCount = null;
        }
    }

    $scope.editGroup = function (val) {
        $scope.account.id = val;
        $scope.account.instanceTypeId = parseInt($scope.account.instanceTypeId);
        accountSetupService.editGroup($scope.account.id).then(function (response) {
            $scope.account = response.data;
            $scope.account.instanceTypeId = $scope.account.instanceTypeId.toString();
            for (var x = 0; x < $scope.selectedType.length; x++) {
                if ($scope.account.registerType == parseInt($scope.selectedType[x].value)) {
                    $scope.selectedRegisterType = $scope.selectedType[x];
                }
            }
        }, function () {
        });
    };
    $scope.saveGroup = function (val) {
        $scope.isProcessing = true;
        $scope.account.instanceTypeId = parseInt($scope.account.instanceTypeId);
        accountSetupService.UpdateGroupDetails(val).then(function (response) {
            //$scope.accountSetup.$setPristine();
            toastr.success('Account Edited Successfully');
            $scope.isProcessing = false;
            window.location.assign('/accountSetup/list');
        }, function () {
            toastr.error('Record already exists, Check Group Name / Email / Phone Number', 'error');
            $scope.isProcessing = false;
        });
    };
    $scope.changeRegisterType = function (val) {
        if (val != null) {
            $scope.account.registerType = val.value;
        }
        if ($scope.account.registerType != 4) {
            $scope.account.licenseStartDate = "";
            $scope.account.licenseEndDate = "";
            $scope.expiryInvalid = false;
        }
    };
    $scope.checkLicenseDateValid = function () {
        if (account.registerType == 4) {
            if ($scope.account.licenseStartDate > $scope.account.licenseEndDate)
                $scope.expiryInvalid = true;
            else
                $scope.expiryInvalid = false;
        }
    };
    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to cancel the edit?');
        if (cancel) {
            window.location.assign('/accountSetup/list');
        }
    };
});
