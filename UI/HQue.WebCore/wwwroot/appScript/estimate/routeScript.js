app.config(function ($routeProvider) {
    $routeProvider.when('/estimate', {
        "controller": "estimateCreateCtrl",
        "templateUrl": '/estimate/create'
    });
    $routeProvider.when('/estimateList', {
        "controller": "estimateListCtrl",
        "templateUrl": '/estimate/list'
    });
    
    $routeProvider.otherwise({ redirectTo: '/estimate' });
});