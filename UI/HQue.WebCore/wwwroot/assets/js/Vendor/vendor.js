﻿var app = angular.module('vendorApp', ['toastr']);
app.controller('VendorCtrl', function ($scope, $http, toastr) {
    function setupScope() {
        $scope.vendor = {
            Id: "",
            Code: "",
            Name: "",
            Mobile: null,
            Phone: null,
            Email: null,
            Area: null,
            City: null,
            State: null,
            Pincode: null
        }
    }
    
    setupScope();

    $scope.createVendor = function () {
        console.log($scope.data);
        $http.post('/vendor/index', $scope.vendor).then(function (response) {
            toastr.success('Vendor Created Successfully');
        }, function () { });
    }

});

