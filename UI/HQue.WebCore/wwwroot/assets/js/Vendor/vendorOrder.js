﻿var app = angular.module('vendororderApp', ['commonApp', 'toastr']);
app.controller('VendorOrderCtrl', function ($scope, $http, toastr) {

    $scope.save = {
        vendorId : "",
        productId: "",
        quantity: "",
        selectedVendor: null,
        selectedProduct: null,
    }
    $scope.vendorOrder = [];
    $scope.vendorList = [];
    $scope.productList = [];

    function vendorData() {
        $http.get('/vendor/VendorList').then(function (response) { $scope.vendorList = response.data; }, function () { });
    }

    vendorData();

    function drugData() {
        $http.get('/product/ProductList').then(function (response) { $scope.productList = response.data; }, function () { });
    }

    drugData();

    $scope.Math = window.Math;

    $scope.vendorSelected = function () {
        $scope.save.selectedVendor;
    }

    $scope.productSelected = function () {
        $scope.save.selectedProduct;
    }

    $scope.list = [];

    $scope.addOrder = function () {
        $scope.vendorOrder.IsEdit = false;
        $scope.save.vendorId = $scope.save.selectedVendor.id;
        $scope.save.productId = $scope.save.selectedProduct.id;

        $scope.vendorOrder.push($scope.save);

        $scope.save = null;
        $scope.vendorOrderItems.$setPristine();

    }

    $scope.removeOrder = function (item) {
        var index = $scope.vendorOrder.indexOf(item);
        $scope.vendorOrder.splice(index, 1);
    }

    $scope.editOrder = function (item) {
        item.isEdit = true;
    }

    $scope.doneOrder = function (item) {
        item.isEdit = false;
    }

    $scope.saveOrder = function () {
        console.log($scope.vendorOrder);
        $http.post('/vendorOrder/index', $scope.vendorOrder).then(function (response) { $scope.list = response.data; toastr.success('Data Saved Successfully'); }, function () { toastr.error('Error Occured', 'Error'); });
    }

});


