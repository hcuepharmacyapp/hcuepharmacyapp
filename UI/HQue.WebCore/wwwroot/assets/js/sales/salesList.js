﻿var app = angular.module('salesListApp', []);
app.controller('salesSearchCtrl', function ($scope, $http) {
    $scope.search = {
        invoiceNo: "",
        patient : {
            name: "",
            mobile: "",
            code: "",
            email: ""
        }
    };

    $scope.list = [];

    $scope.invoiceSearch = function () {
        $http.post('/sales/listData', $scope.search).then(function (response) { $scope.list = response.data; console.log($scope.list); }, function () { });
    }

    $scope.invoiceSearch();

});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});