﻿var app = angular.module('salesApp', ['ngRoute', 'commonApp', 'toastr'])
.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/patientsearch', {
        "templateUrl": '/patient/search'
    });
    $routeProvider.when('/patientcreate', {
        "templateUrl": '/patient/create'
    });
    $routeProvider.when('/pos', {
        "templateUrl": '/sales/sale'
    });
    $routeProvider.otherwise({ redirectTo: '/patientsearch' });
    //$locationProvider.html5Mode({
    //    enabled: true,
    //    requireBase: false
    //});
});
app.controller('PatientSearchCtrl', function ($scope, $http, $location, toastr) {

    function setTotal() {
        $scope.sales.total = 0;
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            $scope.sales.total += $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity;
        }
    }

    function setupScope() {
        $scope.patientSearchData = {
            name: "",
            code: "",
            mobile: "",
            email: ""
        };
        $scope.savePatient = {
            mobile: "",
            name: "",
            age: "",
            gender: "",
            email: "",
            address : ""
        };
        $scope.list = [];
        $scope.selectedPatient = null;
        $scope.sales = {
            productList: [],
            invoiceDate: new Date(),
            selectedProduct: null,
            selectedBatch: null,
            batchList: [],
            salesItem: [],
            discount: 0,
            total: 0,
            paymnetType: "Cash",
            deliveryType: "Counter",
            billPrint: true,
            sendEmail: false,
            sendSms: false
        };
    }

    setupScope();

    $scope.Math = window.Math;

    $scope.patientSearch = function () {
        console.log("seaching");
        $http.post('/patient/list', $scope.patientSearchData).then(function (response) {
            $scope.list = response.data;
            if ($scope.list.length === 1) {
                $scope.patientSelect($scope.list[0]);
            }
            if ($scope.list.length === 0) {
                $scope.savePatient.mobile = $scope.patientSearchData.mobile;
                $location.path('/patientcreate');
            }

        }, function () { });
    }


    $scope.createPatient = function() {
        $http.post('/patient/create', $scope.savePatient).then(function (response) {
            $scope.selectedPatient = response.data;
            $scope.sales.patient = response.data;
            toastr.success('Patient Created Successfully');
            $location.path('/pos');
            $http.get('/productStock/InStockProduct').then(function (response) {
                $scope.sales.productList = response.data;
            }, function () { });
        }, function () { });
    }

    $scope.patientSelect = function (patient) {
        $scope.selectedPatient = patient;
        $scope.sales.patient = patient;
        $location.path('/pos');
        $http.get('/productStock/InStockProduct').then(function (response) {
            $scope.sales.productList = response.data;
        }, function () { });
    }

    $scope.batchSelected = function (batch) {
        if (batch == null) {
            return;
        }

        batch.quantity = 1;
    }

    $scope.productSelected = function () {
        $http.get('/productStock/InStockProduct/batch?productId=' + $scope.sales.selectedProduct.product.id).then(function (response) {
            $scope.sales.batchList = response.data;
            $scope.sales.selectedBatch = $scope.sales.batchList[0];
            $scope.sales.selectedBatch.quantity = 1;
        }, function () { });
    }

    $scope.addSales = function () {
        var isUpdated = false;
        $scope.sales.selectedBatch.IsEdit = false;
        $scope.sales.selectedBatch.productStockId = $scope.sales.selectedBatch.id;

        angular.forEach($scope.sales.salesItem, function (value, key) {
            if (value.id === $scope.sales.selectedBatch.id) {
                value.quantity = parseInt(value.quantity) + parseInt($scope.sales.selectedBatch.quantity);
                isUpdated = true;
                return;
            }
        });

        if (!isUpdated)
            $scope.sales.salesItem.push($scope.sales.selectedBatch);

        $scope.sales.total += ($scope.sales.selectedBatch.sellingPrice * $scope.sales.selectedBatch.quantity);
        $scope.salesItems.$setPristine();
        $scope.sales.selectedProduct = null;
        $scope.sales.selectedBatch = null;
    }

    $scope.removeSales = function (item) {
        $scope.sales.total -= (item.sellingPrice * item.quantity);
        var index = $scope.sales.salesItem.indexOf(item);
        $scope.sales.salesItem.splice(index, 1);
    }

    $scope.editSales = function (item) {
        item.isEdit = true;
    }

    $scope.doneEditSales = function (item) {
        item.isEdit = false;
        setTotal();
    }

    $scope.save = function () {
        salesService.create().then(function (response) {
            $scope.list = response.data;
            setupScope();
            toastr.success('Data Saved Successfully');
            $location.path('/patientSearch');
        }, function () {
            toastr.error('Error Occured', 'Error');
        });
    }
});