﻿var app = angular.module('vendorPurchaseApp', ['commonApp', 'toastr', 'angucomplete', 'localytics.directives']);
app.controller('VendorPurchaseCtrl', function ($scope, $http, toastr, productModel, productService) {

    function setTotal() {
        $scope.vendorPurchase.total = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            $scope.vendorPurchase.total += $scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty;
        }
    }

    function setupScope() {
        $scope.selectedVendor = null;
        $scope.selectedProduct = null;
        $scope.vendorPurchaseItem = {
            productStock: {
                productName: "",
                productId: "",
                batchNo: "",
                expireDate: "",
                vat: "",
                sellingPrice: "",
                purchaseBarcode: "",
            },
            quantity: "",
            purchasePrice: "",
            packageQty: "",
            packagePurchasePrice: "",
            packageSellingPrice: "",
        };

        $scope.vendorPurchase = {
            vendorId: "",
            invoiceDate: new Date(),
            invoiceNo: "",
            vendorPurchaseItem: [],
            discount: 0,
            total: 0,
            credit: 0
        };

        vendorList = [];
        productList = [];

        $scope.list = [];

        $scope.selectedVendor = "";
        $scope.selectedProduct = "";

        $scope.product = productModel;
    }

    setupScope();

    $scope.$on("update_getValue", function (event, value) {
        $scope.product.name = value;
    });

    $scope.Math = window.Math;

    $scope.vendorSelected = function () {
        $scope.selectedVendor;
    }

    $scope.productSelected = function () {
        $scope.selectedProduct;
    }

    $scope.init = function (id) {
        $scope.vendorPurchase.verndorOrderId = id;

        salesService.salesReturnDetail($scope.vendorPurchase.verndorOrderId).then(function (response) {
            $scope.salesReturn = response.data;

        }, function () { });
    }

    $scope.addStock = function () {

        $scope.vendorPurchaseItem.IsEdit = false;
        $scope.vendorPurchase.vendorId = $scope.selectedVendor;

        //Need to check after angucomplete refactor
        if ($scope.selectedProduct == null || $scope.selectedProduct == "") {
            //Create product name from augucomplete
            $scope.$root.$broadcast('getValue', 'drugName');

            if ($scope.product.name !== "") {
                //Create product
                productService.create($scope.product).then(function (response) {
                    $scope.vendorPurchaseItem.productStock.productId = response.data.id;
                    $scope.vendorPurchaseItem.productStock.productName = response.data.name;
                    $scope.vendorPurchase.vendorPurchaseItem.push($scope.vendorPurchaseItem);
                    $scope.vendorPurchase.total += ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty);
                    $scope.vendorPurchaseItems.$setPristine();
                    $scope.selectedProduct = "";
                    $scope.vendorPurchaseItem = {};
                    $scope.product.name = "";
                    clearAutoCompleteFields('drugName');
                });
            }
        }
        else {
            $scope.vendorPurchaseItem.productStock.productId = $scope.selectedProduct.originalObject.id;
            $scope.vendorPurchaseItem.productStock.productName = $scope.selectedProduct.originalObject.name;
            $scope.vendorPurchase.vendorPurchaseItem.push($scope.vendorPurchaseItem);
            $scope.vendorPurchase.total += ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty);
            $scope.vendorPurchaseItems.$setPristine();
            $scope.product.name = "";
            $scope.selectedProduct = "";
            $scope.vendorPurchaseItem = {};
            clearAutoCompleteFields('drugName');
        }
    }

    $scope.removeStock = function (item) {
        $scope.vendorPurchase.total -= (item.packagePurchasePrice * item.packageQty);
        var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
        $scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
    }

    $scope.editPurchase = function (item) {
        item.isEdit = true;
    }

    $scope.doneEditPurchase = function (item) {
        item.isEdit = false;
        setTotal();
    }

    $scope.save = function () {
        $scope.isProcessing = true;
        $http.post('/vendorPurchase/index', $scope.vendorPurchase).then(function (response) {
            $scope.list = response.data;
            $scope.vendorPurchaseItems.$setPristine();
            toastr.success('Data Saved Successfully');
            clearAutoCompleteFields('vendorName');
            setupScope();
            $scope.isProcessing = false;
        }, function () {
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    }

    function clearAutoCompleteFields(elementId) {
        $scope.$root.$broadcast('clearInput', elementId);
    }

    $scope.vendor = function () {
        $http.post('/VendorData/VendorList', null).then(function (response) {
            $scope.vendorList = response.data;
        }, function () { toastr.error('Error Occured', 'Error'); });
    }
    $scope.vendor();
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('myDate', function (dateFilter, $parse) {
    return {
        restrict: 'EAC',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel, ctrl) {
            ngModel.$parsers.push(function (viewValue) {
                return dateFilter(viewValue, 'yyyy-MM-dd');
            });
        }
    }
});

