﻿var app = angular.module('vendorPurchaseListApp', []);
app.controller('vendorPurchaseSearchCtrl', function ($scope, $http) {
    $scope.search = {
        invoiceNo: "",
        vendor: {
            name: "",
            mobile: "",
            email: ""
        }
    };

    $scope.list = [];

    $scope.purchaseSearch = function () {
        $http.post('/vendorPurchase/listData', $scope.search).then(function (response) { $scope.list = response.data; console.log($scope.list); }, function () { });
    }

    $scope.purchaseSearch();
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});