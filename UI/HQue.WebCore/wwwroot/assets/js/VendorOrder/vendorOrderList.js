﻿var app = angular.module('vendorOrderListApp', ['commonApp']);
app.controller('vendorOrderSearchCtrl', function ($scope, $http) {
    $scope.search = {
        orderId: "",
        orderDate: "",
        vendor: {
            name: "",
            mobile: "",
            email: ""
        }
    };

    $scope.list = [];

    $scope.orderSearch = function () {
        $http.post('/vendorOrder/listData', $scope.search).then(function (response) { $scope.list = response.data; console.log($scope.list); }, function () { });
    }

    $scope.orderSearch();
});
