﻿var app = angular.module('pharmacyListApp', []);
app.controller('pharmacySearchCtrl', function ($scope, $http) {
    $scope.search = {
        name: "",
        code: "1234",
        phone: ""
    };

    $scope.list = [];

    $scope.pharmacySearch = function () {
        $http.post('/instanceSetup/listData', $scope.search).then(function (response) { $scope.list = response.data; console.log($scope.list); }, function () { });
    }
   
    $scope.pharmacySearch();

});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});