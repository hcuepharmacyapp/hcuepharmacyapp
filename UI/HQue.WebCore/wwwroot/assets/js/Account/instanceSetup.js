﻿var app = angular.module('instanceSetupApp', ['toastr']);
app.controller('instanceSetupCtrl', function ($scope, $http, toastr) {
    function setupScope() {
        $scope.data ={ 
            name: "",
            code: "1234",
            registrationDate: "",
            phone: "",
            address: "",
            //user: {
            //    name: "",
            //    userId: "",
            //    mobile: ""
            //},
            userList: []
        };
    }

    $scope.user = {
        name: "",
        userId: "",
        mobile: ""
    };

    setupScope();

    $scope.list = [];

    $scope.addUser = function () {
        
        var isUpdated = false;

        if (!isUpdated)
            $scope.data.userList.push($scope.user);

        $scope.user = null;
        //$scope.data = null;
        $scope.instanceUser.$setPristine();

        //$scope.sales.total += ($scope.sales.selectedBatch.sellingPrice * $scope.sales.selectedBatch.quantity);
        //$scope.data.$setPristine();
        //$scope.sales.selectedProduct = null;
        //$scope.sales.selectedBatch = null;
    }

    $scope.removeUser = function (item) {
        //$scope.sales.total -= (item.sellingPrice * item.quantity);
        var index = $scope.data.userList.indexOf(item);
        $scope.data.userList.splice(index, 1);
    }

    $scope.instanceCreate = function () {
        console.log($scope.data);
        $http.post('/instanceSetup/index', $scope.data).then(function (response)
        {
            setupScope();
            $scope.instanceUser.$setPristine();
            toastr.success('Pharmacy Created Successfully')
        }, function () {
            alert("There was a problem saving the data");
        });
    }

});

app.directive('myDate', function (dateFilter, $parse) {
    return {
        restrict: 'EAC',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel, ctrl) {
            ngModel.$parsers.push(function (viewValue) {
                return dateFilter(viewValue, 'yyyy-MM-dd');
            });
        }
    }
});