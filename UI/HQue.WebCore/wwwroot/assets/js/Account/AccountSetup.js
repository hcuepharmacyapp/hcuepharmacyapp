﻿var app = angular.module('accountSetupApp', ['toastr']);
app.controller('AccountSetupCtrl', function ($scope, $http, toastr) {
    function setupScope() {
        $scope.data = {
            name: "",
            code: "",
            instanceCount: "",
            registrationDate: "",
            phone: "",
            address: "",
            user: {
                name: "",
                userId: "",
                mobile: ""
            }
        };
    }

    setupScope();

    $scope.list = [];

    $scope.accountCreate = function () {
        console.log($scope.data);
        $http.post('/accountSetup/index', $scope.data).then(function (response) {
            setupScope();
            $scope.accountSetup.$setPristine();
            toastr.success('Account Created Successfully');
        }, function () { });
    }

});

app.directive('myDate', function (dateFilter, $parse) {
    return {
        restrict: 'EAC',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel, ctrl) {
            ngModel.$parsers.push(function (viewValue) {
                return dateFilter(viewValue, 'yyyy-MM-dd');
            });
        }
    }
});