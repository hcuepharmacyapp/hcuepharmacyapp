﻿var app = angular.module('accountListApp', []);
app.controller('accountSearchCtrl', function ($scope, $http) {
    $scope.search = {
        name: "",
        code: "",
        phone: "",

    };

    $scope.list = [];

    $scope.accountSearch = function () {
        $http.post('/accountSetup/listData', $scope.search).then(function (response) { $scope.list = response.data; console.log($scope.list); }, function () { });
    }

    $scope.accountSearch();

});