var selectedField = "";

$(document).ready(function () {
    //alert("ready!");

    init2();

   

    $('.tree').on('dragstart', '.dragitem', function (e) {

        selectedField = e.currentTarget.innerText;
    });
    
    $('#textProp').blur(function (e) {

        if (shapeSelected == null)
            return;

        shapeSelected.text = this.value;
        window.reDraw();
    });

    $('.list-group').on('dragstart', '.list-group-item', function (e) {

        selectedField = e.currentTarget.innerText;

    });

    $("#section").on('change', function () {

        shapeSelected.section = this.value;

    });

    $("#dataitem-align").on('change', function () {

        if (shapeSelected == null)
            return;

        shapeSelected.alignment = this.value;
        window.reDraw();

    });

    $("#dataitem-itemfontsize").on('change', function () {

        if (shapeSelected == null)
            return;

        shapeSelected.itemfontsize = this.value;
        window.reDraw();

    });

    $("#dataitem-fontweight").on('change', function () {

        if (shapeSelected == null)
            return;

        shapeSelected.fontWeight = this.value;
        window.reDraw();

    });

    $('#panel_dataitems').on('click', function () {

        if ($(this).hasClass('fa fa-caret-up pull-right')) {

            $('#dataPallet').css('position', 'absolute');
            $(this).removeClass('fa fa-caret-up pull-right')
            $(this).addClass('fa fa-caret-down pull-right')
        }
        else {

            $('#dataPallet').css('position', 'relative');
            $(this).removeClass('fa fa-caret-down pull-right')
            $(this).addClass('fa fa-caret-up pull-right')
        }
    });



    //$('.single-line').text
    

});
