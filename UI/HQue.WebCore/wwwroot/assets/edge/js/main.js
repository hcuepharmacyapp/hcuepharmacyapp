
// error message
window.onload = function() {
        $('.one').click(
            function() {
                $.miniNoty({

                    message: 'Click me to go other website!',
                })
                return false;
            }
        )
    }
    // material design form
$(window, document, undefined).ready(function() {

    $('input').blur(function() {
        var $this = $(this);
        if ($this.val())
            $this.addClass('used');
        else
            $this.removeClass('used');
    });

    var $ripples = $('.ripples');

    $ripples.on('click.Ripples', function(e) {

        var $this = $(this);
        var $offset = $this.parent().offset();
        var $circle = $this.find('.ripplesCircle');

        var x = e.pageX - $offset.left;
        var y = e.pageY - $offset.top;

        $circle.css({
            top: y + 'px',
            left: x + 'px'
        });

        $this.addClass('is-active');

    });

    $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
        $(this).removeClass('is-active');
    });

});

$(document).ready(function() {

    $('form:first *:input[type!=hidden]:first').focus();

    // login steps
    $(".login-click").click(function() {
        $(".login-form").css("display", "none");
        $(".choose-section").css("display", "block");
    });
    // forget password steps
    $(".forgot-click").click(function() {
        $(".login-form").css("display", "none");
        $(".forget-form").css("display", "block");
    });

    $('.progress-in').stop().animate({ width: $('.progress-val').text() }, {
        duration: 2000,
        step: function(now) {
            $('.progress-val').text(Math.ceil(now) + '%');
        }
    });

    // image uploadFile preview
    //$("#uploadFile1").on("change", function() {
    //    var files = !!this.files ? this.files : [];
    //    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

    //    if (/^image/.test(files[0].type)) { // only image file
    //        var reader = new FileReader(); // instance of the FileReader
    //        reader.readAsDataURL(files[0]); // read the local file

    //        reader.onloadend = function() { // set image data as background of div

    //            $(".imagePreview1").css("background-image", "url(" + this.result + ")");

    //            $(".imagePreview1").addClass('current');

    //        }
    //    }
    //});

    $('.imagePreview1').click(function() {
        $('#uploadFile1').trigger('click');
    });

    //$("#uploadFile2").on("change", function() {
    //    var files = !!this.files ? this.files : [];
    //    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

    //    if (/^image/.test(files[0].type)) { // only image file
    //        var reader = new FileReader(); // instance of the FileReader
    //        reader.readAsDataURL(files[0]); // read the local file

    //        reader.onloadend = function() { // set image data as background of div
    //            $(".imagePreview2").css("background-image", "url(" + this.result + ")");
    //            $(".imagePreview2").addClass('current');
    //        }
    //    }
    //});
    $('.imagePreview2').click(function() {
        $('#uploadFile2').trigger('click');
    });

    //$("#uploadFile3").on("change", function() {
    //    var files = !!this.files ? this.files : [];
    //    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

    //    if (/^image/.test(files[0].type)) { // only image file
    //        var reader = new FileReader(); // instance of the FileReader
    //        reader.readAsDataURL(files[0]); // read the local file

    //        reader.onloadend = function() { // set image data as background of div
    //            $(".imagePreview3").css("background-image", "url(" + this.result + ")");
    //            $(".imagePreview3").addClass('current');
    //        }
    //    }
    //});
    $('.imagePreview3').click(function() {
        $('#uploadFile3').trigger('click');
    });

    //$("#uploadFile4").on("change", function() {
    //    var files = !!this.files ? this.files : [];
    //    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

    //    if (/^image/.test(files[0].type)) { // only image file
    //        var reader = new FileReader(); // instance of the FileReader
    //        reader.readAsDataURL(files[0]); // read the local file

    //        reader.onloadend = function() { // set image data as background of div
    //            $(".imagePreview4").css("background-image", "url(" + this.result + ")");
    //            $(".imagePreview4").addClass('current');
    //        }
    //    }
    //});
    $('.imagePreview4').click(function() {
        $('#uploadFile4').trigger('click');
    });

    //$("#uploadFile5").on("change", function() {
    //    var files = !!this.files ? this.files : [];
    //    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

    //    if (/^image/.test(files[0].type)) { // only image file
    //        var reader = new FileReader(); // instance of the FileReader
    //        reader.readAsDataURL(files[0]); // read the local file

    //        reader.onloadend = function() { // set image data as background of div
    //            $(".imagePreview5").css("background-image", "url(" + this.result + ")");
    //            $(".imagePreview5").addClass('current');
    //        }
    //    }
    //});
    $('.imagePreview5').click(function() {
        $('#uploadFile5').trigger('click');
    });

    // header dropdown
    $(".header-menu").click(function() {
        $(".menu-dropdown").slideToggle(500);
    });
    // left menu 
    $("#sidebar .menu-action").click(function() {
        $(".main").toggleClass("hide-menu");
    });
    // export dropdown
    $(".export-files").click(function() {
        $(".export-dropdown").slideToggle(500);
    });

    // grid change
    $(".grid-change").click(function() {
        $(this).find('img').toggle();
        $(".row").toggleClass("grid-with");
        $('.dashboard-section').matchHeight();
    });

    // date picker


    // matchHeight
    $('.equal-height').matchHeight();
    $('.dashboard-section').matchHeight();
    $('.purchase-import-page p').matchHeight();

    // tabs
    $('.common-tab li').click(function() {
        var tab_id = $(this).attr('data-tab');

        $('.common-tab li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    });


    // Add vender 
    $(".add-vender").click(function() {
        $(".vender-list").css("display", "none");
        $(".add-vender-page").css("display", "block");
    });
    $(".save-vender").click(function() {
        $(".add-vender-page").css("display", "none");
        $(".vender-list").css("display", "block");
    });

    // Add branch 
    $(".add-branch").click(function() {
        $(".branch-list").css("display", "none");
        $(".add-branch-page").css("display", "block");
    });
    $(".save-branch").click(function() {
        $(".add-branch-page").css("display", "none");
        $(".branch-list").css("display", "block");
    });

    // accordion
    $('.accordiontitle').append('<span class="arrowaccordion"></span>')

    $(".accordioncontent").hide();
    $(".accordioncontent:eq(0)").fadeIn();
    $(".accordiontitle").click(function() {
        $(".accordioncontent").slideUp();
        $(".accordiontitle").removeClass("active");
        $(this).addClass("active");
        ele_id = '.' + ($(this).attr('id'));
        $(ele_id).slideDown();
    });


    //$('.otp-popup').on('click', function() {
    //    var post_id = $(this).attr('data-post-id');
    //    $('#form_post_id').val(post_id);
    //    $('.otp-section').fadeIn();
    //    return false;
    //});

    //$('.overlayClick').on('click', function() {
    //    var post_id = $(this).attr('data-post-id');
    //    $('#form_post_id').val(post_id);
    //    $('.form-popup').fadeIn();
    //    return false;
    //});
    //$('.alternats').on('click', function() {
    //    var post_id = $(this).attr('data-post-id');
    //    $('#form_post_id').val(post_id);
    //    $('.alternats-tablets').fadeIn();
    //    return false;
    //});

    //$('.popup-close').on('click', function() {
    //    $(this).closest('.popup-overlay').fadeOut();
    //    return false;
    //});

    // ripple effect 
    var addMulitListener = function(el, events, callback) {
        // Split all events to array
        var e = events.split(' ');

        // Loop trough all elements
        Array.prototype.forEach.call(el, function(element, i) {
            // Loop trought all events and add event listeners to each
            Array.prototype.forEach.call(e, function(event, i) {
                element.addEventListener(event, callback, false);
            });
        });
    };

    addMulitListener(document.querySelectorAll('[material]'), 'click touchstart', function(e) {
        var ripple = this.querySelector('.ripple');
        var eventType = e.type;
        if (ripple == null) {
            ripple = document.createElement('span');
            ripple.classList.add('ripple');
            this.insertBefore(ripple, this.firstChild);
            if (!ripple.offsetHeight && !ripple.offsetWidth) {
                var size = Math.max(e.target.offsetWidth, e.target.offsetHeight);
                ripple.style.width = size + 'px';
                ripple.style.height = size + 'px';
            }
        }
        // Remove animation effect
        ripple.classList.remove('animate');

        // get click coordinates by event type
        if (eventType == 'click') {
            var x = e.pageX;
            var y = e.pageY;
        } else if (eventType == 'touchstart') {
            var x = e.changedTouches[0].pageX;
            var y = e.changedTouches[0].pageY;
        }
        x = x - this.offsetLeft - ripple.offsetWidth / 2;
        y = y - this.offsetTop - ripple.offsetHeight / 2;

        // set new ripple position by click or touch position
        ripple.style.top = y + 'px';
        ripple.style.left = x + 'px';
        ripple.classList.add('animate');
    });

    $("#overall-discount").on('click', function() {
        if ($(this).is(':checked')) {
            $(".overall-discount-section").css("display", "block");
            $(".billbased-discount-section").css("display", "none");
            $(".slab-discount-section").css("display", "none");
        } else {
            $(".overall-discount-section").css("display", "none");
        }
    });
    $("#bill-based-discount").on('click', function() {
        if ($(this).is(':checked')) {
            $(".billbased-discount-section").css("display", "block");
            $(".slab-discount-section").css("display", "none");
            $(".overall-discount-section").css("display", "none");
        } else {
            $(".billbased-discount-section").css("display", "none");
        }
    });
    $("#slab-discount").on('click', function() {
        if ($(this).is(':checked')) {
            $(".overall-discount-section").css("display", "none");
            $(".billbased-discount-section").css("display", "none");
            $(".slab-discount-section").css("display", "block");
        } else {
            $(".slab-discount-section").css("display", "none");
        }
    });
    $("#Credit").on('click', function() {
        if ($(this).is(':checked')) {
            $(".amount-details").css("display", "block");
        } else {
            $(".amount-details").css("display", "none");
        }
    });
    $("#Full").on('click', function() {
        if ($(this).is(':checked')) {
            $(".amount-details").css("display", "none");
        } else {
            $(".amount-details").css("display", "block");
        }
    });
    $("#Card").on('click', function() {
        if ($(this).is(':checked')) {
            $(".card-details").css("display", "block");
        } else {
            $(".card-details").css("display", "none");
        }
    });
    $("#Cash").on('click', function() {
        if ($(this).is(':checked')) {
            $(".card-details").css("display", "none");
        } else {
            $(".card-details").css("display", "block");
        }
    });
    $("#Delivery").on('click', function() {
        if ($(this).is(':checked')) {
            $(".door-delivery").css("display", "block");
        } else {
            $(".door-delivery").css("display", "none");
        }
    });
    $("#Counter").on('click', function() {
        if ($(this).is(':checked')) {
            $(".door-delivery").css("display", "none");
        } else {
            $(".door-delivery").css("display", "block");
        }
    });

    //starttime.addEventListener('keyup', function (e) {
    //    console.log(e.keyCode);
    //    if (e.keyCode !== 8) {
    //        if (this.value.length === 2) {
    //            this.value = this.value += ' : ';
    //        }
    //    }
    //    this.value.length === 6
    //});
    //endtime.addEventListener('keyup', function (e) {

    //    if (e.keyCode !== 8) {


    //        if (this.value.length === 2) {
    //            this.value = this.value += ' : ';
    //        }


    //    }
    //    this.value.length === 6
    //});


    // error message
    $('#demo-success').on('click', function() {
        notify({
            type: "success", //alert | success | error | warning | info
            title: "Success",
            position: {
                x: "right", //right | left | center
                y: "top" //top | bottom | center
            },
            icon: '<img src="images/paper_plane.png" />',
            // message: "jQuery Notify.js Demo. Super simple Notify plugin."
        });
    });

    $('#demo-warning').on('click', function() {
        notify({
            type: "warning", //alert | success | error | warning | info
            title: "Warning",
            theme: "dark-theme",
            position: {
                x: "right", //right | left | center
                y: "top" //top | bottom | center
            },
            icon: '<img src="images/paper_plane.png" />',
            // message: "jQuery Notify.js Demo. Super simple Notify plugin."
        });
    });
    $('#demo-error').on('click', function() {
        notify({
            type: "error", //alert | success | error | warning | info
            title: "Error",
            theme: "dark-theme",
            position: {
                x: "right", //right | left | center
                y: "top" //top | bottom | center
            },
            icon: '<img src="images/paper_plane.png" />',
            // message: "jQuery Notify.js Demo. Super simple Notify plugin."
        });
    });
    $('#demo-info').on('click', function() {
        notify({
            type: "info", //alert | success | error | warning | info
            title: "Info",
            theme: "dark-theme",
            position: {
                x: "right", //right | left | center
                y: "top" //top | bottom | center
            },
            icon: '<img src="images/paper_plane.png" />',
            // message: "jQuery Notify.js Demo. Super simple Notify plugin."
        });
    });

});
