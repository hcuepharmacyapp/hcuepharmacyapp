(function() {

    'use strict';

    /**
     * Test code for ng-datepicker demo
     */
    angular
        .module('hcue1', ['ngFlatDatepicker'])
        .controller('registrationCreateCtrl1', ['$scope', mainController]);

    function mainController ($scope) {

        // $scope.minDate = moment.utc('2015-09-04');
        // $scope.maxDate = moment.utc('2015-09-22');
    }

})();
