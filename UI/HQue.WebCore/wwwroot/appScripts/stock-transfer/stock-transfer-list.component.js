"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var stock_transfer_accept_component_1 = require("./stock-transfer-accept.component");
var stock_transfer_service_1 = require("./stock-transfer.service");
var StockTransferList = (function (_super) {
    __extends(StockTransferList, _super);
    function StockTransferList(stockTransferService) {
        _super.call(this, stockTransferService);
        this.forAccept = false;
    }
    StockTransferList = __decorate([
        core_1.Component({
            selector: 'stock-transfer-list',
            templateUrl: '/StockTransfer/AcceptList',
            providers: [stock_transfer_service_1.StockTransferService, http_1.HTTP_PROVIDERS],
        }), 
        __metadata('design:paramtypes', [stock_transfer_service_1.StockTransferService])
    ], StockTransferList);
    return StockTransferList;
}(stock_transfer_accept_component_1.StockTransferAccept));
exports.StockTransferList = StockTransferList;
