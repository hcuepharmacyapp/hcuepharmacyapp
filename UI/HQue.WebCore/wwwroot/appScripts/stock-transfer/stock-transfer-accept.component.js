"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var stock_transfer_service_1 = require("./stock-transfer.service");
var stockTransfer_1 = require("../model/stockTransfer");
var productStock_1 = require("../model/productStock");
var StockTransferAccept = (function () {
    function StockTransferAccept(stockTransferService) {
        this.stockTransferService = stockTransferService;
        this.forAccept = true;
        this.searchModel = new stockTransfer_1.StockTransfer();
        this.stockTransferList = [];
    }
    StockTransferAccept.prototype.ngOnInit = function () {
        this.loadData();
        this.getFilterInstance();
    };
    StockTransferAccept.prototype.loadData = function () {
        var _this = this;
        this.stockTransferService.getStockTransfer(this.searchModel, this.forAccept)
            .subscribe(function (stockTransfers) { _this.stockTransferList = stockTransfers; });
    };
    StockTransferAccept.prototype.search = function () {
        this.loadData();
    };
    StockTransferAccept.prototype.accept = function (transferId) {
        var _this = this;
        this.stockTransferService.accetpReject(transferId, 3)
            .subscribe(function (accept) { alert('Accept successful...!'); _this.loadData(); });
    };
    StockTransferAccept.prototype.reject = function (transferId) {
        var _this = this;
        if (confirm("Sure to reject?"))
            this.stockTransferService.accetpReject(transferId, 2)
                .subscribe(function (accept) { alert('Reject successful...!'); _this.loadData(); });
    };
    StockTransferAccept.prototype.getProductStock = function (obj) {
        return new productStock_1.ProductStock(obj.productStock.purchasePrice, obj.productStock.vat, obj.quantity);
    };
    StockTransferAccept.prototype.getStockTransferNetValue = function (obj) {
        var _this = this;
        var netValue = 0;
        obj.stockTransferItems.forEach(function (value) {
            value.productStockObj = _this.getProductStock(value);
            netValue += value.productStockObj.netValue();
        });
        return netValue;
    };
    StockTransferAccept.prototype.getFilterInstance = function () {
        var _this = this;
        this.stockTransferService.getFilterInstance(this.forAccept)
            .subscribe(function (filterBranchList) { _this.filterBranchList = filterBranchList; });
    };
    StockTransferAccept = __decorate([
        core_1.Component({
            selector: 'stock-transfer-accept',
            templateUrl: '/StockTransfer/AcceptList',
            providers: [stock_transfer_service_1.StockTransferService, http_1.HTTP_PROVIDERS],
        }), 
        __metadata('design:paramtypes', [stock_transfer_service_1.StockTransferService])
    ], StockTransferAccept);
    return StockTransferAccept;
}());
exports.StockTransferAccept = StockTransferAccept;
