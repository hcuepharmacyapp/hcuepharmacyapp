"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var common_1 = require('@angular/common');
var http_1 = require("@angular/http");
var forms_1 = require('@angular/forms');
var product_component_1 = require("./product.component");
var stock_transfer_service_1 = require("./stock-transfer.service");
var stockTransfer_1 = require("../model/stockTransfer");
var stockTransferItem_1 = require("../model/stockTransferItem");
var StockTransferComponent = (function () {
    function StockTransferComponent(stockTransferService, formBuilder) {
        this.stockTransferService = stockTransferService;
        this.formBuilder = formBuilder;
        this.createForm();
        this.model = new stockTransfer_1.StockTransfer();
    }
    StockTransferComponent.prototype.createForm = function () {
        this.stockTransfer = this.formBuilder.group({
            "fromInstanceId": ["", forms_1.Validators.required],
            "toInstanceId": ["", forms_1.Validators.required],
        });
        this.fromBranchCtl = this.stockTransfer.controls["fromInstanceId"];
        this.toBranchCtl = this.stockTransfer.controls["toInstanceId"];
    };
    StockTransferComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.stockTransferService.getTransferBranch('')
            .subscribe(function (branchs) {
            _this.fromBranchs = branchs;
            if (_this.fromBranchs.length == 1) {
                _this.model.fromInstanceId = _this.fromBranchs[0].id;
                _this.getToInstance(_this.model.fromInstanceId);
            }
        });
        this.stockTransferService.getTransfereNo()
            .subscribe(function (x) { _this.model.transferNo = x; });
    };
    StockTransferComponent.prototype.reset = function () {
        this.createForm();
        this.model = new stockTransfer_1.StockTransfer();
        this.addedProducts = [];
        this.vc.componentReset();
        document.getElementById('fromBranch').focus();
    };
    StockTransferComponent.prototype.productUpdated = function (products) {
        this.addedProducts = products;
    };
    StockTransferComponent.prototype.isFormValid = function () {
        if (this.addedProducts === undefined)
            return false;
        if (this.addedProducts.length === 0)
            return false;
        return this.stockTransfer.valid;
    };
    StockTransferComponent.prototype.transfer = function () {
        var _this = this;
        this.setTransferItem();
        this.stockTransferService.stockTransfer(this.model).subscribe(function (x) {
            _this.transferSuccess(x);
        });
    };
    StockTransferComponent.prototype.transferSuccess = function (transferNo) {
        alert('Transfer successful..!');
        this.reset();
        this.model.transferNo = transferNo;
    };
    StockTransferComponent.prototype.setTransferItem = function () {
        for (var i = 0; i < this.addedProducts.length; i++) {
            var item = new stockTransferItem_1.StockTransferItem();
            item.productStockId = this.addedProducts[i].id;
            item.quantity = this.addedProducts[i].quantity;
            this.model.stockTransferItems.push(item);
        }
    };
    StockTransferComponent.prototype.getToInstance = function (value) {
        var _this = this;
        this.stockTransferService.getTransferBranch(value)
            .subscribe(function (branchs) {
            _this.toBranchs = branchs;
            if (_this.toBranchs.length == 1) {
                _this.model.toInstanceId = _this.toBranchs[0].id;
                document.getElementById('productCtl').focus();
            }
        });
    };
    StockTransferComponent.prototype.getNetValue = function () {
        if (this.addedProducts === undefined)
            return 0;
        var total = 0;
        this.addedProducts.forEach(function (value) { total += value.netValue(); });
        return total;
    };
    __decorate([
        core_1.ViewChild(product_component_1.ProductComponent), 
        __metadata('design:type', product_component_1.ProductComponent)
    ], StockTransferComponent.prototype, "vc", void 0);
    StockTransferComponent = __decorate([
        core_1.Component({
            selector: 'stock-transfer',
            templateUrl: '/StockTransfer/StockTransfer',
            providers: [stock_transfer_service_1.StockTransferService, http_1.HTTP_PROVIDERS],
            directives: [common_1.CORE_DIRECTIVES, forms_1.FORM_DIRECTIVES, forms_1.REACTIVE_FORM_DIRECTIVES, product_component_1.ProductComponent],
        }), 
        __metadata('design:paramtypes', [stock_transfer_service_1.StockTransferService, common_1.FormBuilder])
    ], StockTransferComponent);
    return StockTransferComponent;
}());
exports.StockTransferComponent = StockTransferComponent;
