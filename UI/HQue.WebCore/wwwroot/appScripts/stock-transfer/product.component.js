"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var common_1 = require('@angular/common');
var http_1 = require("@angular/http");
var forms_1 = require('@angular/forms');
var stock_transfer_service_1 = require("./stock-transfer.service");
var ProductStock_1 = require("../model/ProductStock");
var ng2_bootstrap_1 = require('ng2-bootstrap');
var product_list_item_component_1 = require("./product-list-item.component");
var Observable_1 = require('rxjs/Observable');
var ProductComponent = (function () {
    function ProductComponent(stockTransferService, formBuilder) {
        var _this = this;
        this.stockTransferService = stockTransferService;
        this.formBuilder = formBuilder;
        this.typeaheadOptionField = "name";
        this.asyncSelected = '';
        this.typeaheadLoading = false;
        this.typeaheadNoResults = false;
        this.productsUpdated = new core_1.EventEmitter();
        this.dataSource = Observable_1.Observable.create(function (observer) {
            observer.next(_this.asyncSelected);
        }).mergeMap(function (token) { return _this.stockTransferService.getProductList(token, _this.fromInstanceId); });
        this.createForm();
        this.model = new ProductStock_1.ProductStock();
        this.selectedBatch = new ProductStock_1.ProductStock();
        this.addedTransfer = [];
    }
    ProductComponent.prototype.createForm = function () {
        this.checkQuantity = this.checkQuantity.bind(this);
        this.transferProduct = this.formBuilder.group({
            "selectedProduct": ["", forms_1.Validators.required],
            "quantity": ["", forms_1.Validators.required, this.checkQuantity],
            "batchNo": ["", forms_1.Validators.required],
        });
        this.selectedProductCtl = this.transferProduct.controls["selectedProduct"];
        this.quantityCtl = this.transferProduct.controls["quantity"];
        this.batchNoCtl = this.transferProduct.controls["batchNo"];
    };
    ProductComponent.prototype.changeTypeaheadLoading = function (e) {
        this.typeaheadLoading = e;
    };
    ProductComponent.prototype.changeTypeaheadNoResults = function (e) {
        this.typeaheadNoResults = e;
    };
    ProductComponent.prototype.typeaheadOnSelect = function (e) {
        var _this = this;
        this.stockTransferService.getBatchList(e.item.product.id).subscribe(function (batchs) {
            _this.batchs = batchs;
            if (_this.batchs.length == 1) {
                _this.model.id = _this.batchs[0].id;
                _this.batchSelected(_this.model.id);
            }
        });
        this.model.product = e.item.product;
        document.getElementById('quantity').focus();
    };
    ProductComponent.prototype.batchSelected = function (value) {
        this.selectedBatch = this.batchs.filter(function (item) { return item.id === value; })[0];
        this.model.expireDate = this.selectedBatch.expireDate;
        this.model.vat = this.selectedBatch.vat;
        this.model.batchNo = this.selectedBatch.batchNo;
        this.model.purchasePrice = this.selectedBatch.purchasePrice;
        this.model.sellingPrice = this.selectedBatch.sellingPrice;
        this.quantityCtl.updateValueAndValidity();
    };
    ProductComponent.prototype.addProduct = function () {
        var _this = this;
        if (!this.transferProduct.valid)
            return;
        var existing = this.addedTransfer.filter(function (item) { return item.id === _this.model.id; });
        if (existing.length == 0) {
            this.addedTransfer.push(this.model);
        }
        else {
            existing[0].quantity = parseInt(existing[0].quantity.toString()) + parseInt(this.model.quantity.toString());
        }
        this.reset();
        this.productsUpdated.emit(this.addedTransfer);
    };
    ProductComponent.prototype.componentReset = function () {
        this.reset();
        this.addedTransfer = [];
    };
    ProductComponent.prototype.reset = function () {
        this.createForm();
        this.model = new ProductStock_1.ProductStock();
        this.asyncSelected = '';
        document.getElementById('productCtl').focus();
    };
    ProductComponent.prototype.qtyKeyDown = function (keyCode) {
        if (keyCode == 13 && this.quantityCtl.valid) {
            document.getElementById('batchNo').focus();
        }
    };
    ProductComponent.prototype.batchNoKeyDown = function (keyCode) {
        if (keyCode == 13 && this.batchNoCtl.valid) {
            this.addProduct();
        }
    };
    ProductComponent.prototype.checkQuantity = function (control) {
        if (this.model.quantity > this.selectedBatch.stock) {
            this.quantityCtl.setErrors({ "qtyError": "Quantity cannot be more than" + this.selectedBatch.stock });
            return false;
        }
        var addedStock = this.getAddedStock(this.model.id);
        if ((parseInt(this.model.quantity.toString()) + parseInt(addedStock.toString())) > this.selectedBatch.stock) {
            this.quantityCtl.setErrors({ "qtyError": "Quantity cannot be more than " + this.selectedBatch.stock + " already added stock is " + addedStock });
            return false;
        }
        if (this.model.quantity == 0) {
            this.quantityCtl.setErrors({ "qtyError": "Quantity cannot be zero" });
            return false;
        }
        this.quantityCtl.setErrors(null);
        return true;
    };
    ProductComponent.prototype.getAvailableStock = function (productStock) {
        return productStock.stock - this.getAddedStock(productStock.id);
    };
    ProductComponent.prototype.getAddedStock = function (id) {
        var existing = this.addedTransfer.filter(function (item) { return item.id === id; });
        if (existing.length == 0) {
            return 0;
        }
        return existing[0].quantity;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], ProductComponent.prototype, "fromInstanceId", void 0);
    ProductComponent = __decorate([
        core_1.Component({
            selector: 'product',
            templateUrl: '/StockTransfer/StockTransferProduct',
            providers: [stock_transfer_service_1.StockTransferService, http_1.HTTP_PROVIDERS],
            directives: [ng2_bootstrap_1.TYPEAHEAD_DIRECTIVES, common_1.CORE_DIRECTIVES, forms_1.FORM_DIRECTIVES, forms_1.REACTIVE_FORM_DIRECTIVES, product_list_item_component_1.ProductListItemComponent],
            outputs: ['productsUpdated']
        }), 
        __metadata('design:paramtypes', [stock_transfer_service_1.StockTransferService, common_1.FormBuilder])
    ], ProductComponent);
    return ProductComponent;
}());
exports.ProductComponent = ProductComponent;
