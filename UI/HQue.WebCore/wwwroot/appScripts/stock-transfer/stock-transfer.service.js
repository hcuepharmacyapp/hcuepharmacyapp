"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/Rx");
require("rxjs/add/operator/map");
var StockTransferService = (function () {
    function StockTransferService(_http) {
        this._http = _http;
        this.transferInstanceUrl = "/StockTransferData/TransferInstance?instanceId=";
        this.stockProductListUrl = '/ProductStockData/InStockProductListForInstance?productName=';
        this.batchListUrl = '/ProductStockData/InStockProduct/batch?productId=';
        this.transferStockUrl = '/StockTransferData/Index';
        this.acceptListStockUrl = '/StockTransferData/AcceptList?forAccept=';
        this.getTransfereNoUrl = '/StockTransferData/GetTransfereNo';
        this.accetpRejectkUrl = '/StockTransferData/AccetpReject?id=';
        this.filterInstanceUrl = '/StockTransferData/GetFilterInstance?forAccept=';
    }
    StockTransferService.prototype.getTransferBranch = function (instanceId) {
        return this._http.get(this.transferInstanceUrl + instanceId)
            .map(function (res) { return res.json(); });
    };
    StockTransferService.prototype.getProductList = function (filter, instanceId) {
        return this._http.get(this.stockProductListUrl + filter + '&instanceId=' + instanceId)
            .map(function (res) { return res.json(); });
    };
    StockTransferService.prototype.getBatchList = function (productId) {
        return this._http.get(this.batchListUrl + productId)
            .map(function (res) { return res.json(); });
    };
    StockTransferService.prototype.stockTransfer = function (data) {
        return this._http.post(this.transferStockUrl, data)
            .map(function (res) { return res.json(); });
    };
    StockTransferService.prototype.getTransfereNo = function () {
        return this._http.get(this.getTransfereNoUrl)
            .map(function (res) { return res.json(); });
    };
    StockTransferService.prototype.getStockTransfer = function (data, forAccept) {
        return this._http.post(this.acceptListStockUrl + forAccept, data)
            .map(function (res) { return res.json(); });
    };
    StockTransferService.prototype.getFilterInstance = function (forAccept) {
        return this._http.get(this.filterInstanceUrl + forAccept)
            .map(function (res) { return res.json(); });
    };
    StockTransferService.prototype.accetpReject = function (transferId, status) {
        return this._http.post(this.accetpRejectkUrl + transferId + "&status=" + status, {})
            .map(function (res) { return res.json(); });
    };
    StockTransferService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], StockTransferService);
    return StockTransferService;
}());
exports.StockTransferService = StockTransferService;
