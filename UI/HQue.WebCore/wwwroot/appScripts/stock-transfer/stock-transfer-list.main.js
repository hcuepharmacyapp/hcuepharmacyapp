"use strict";
///<reference path="../../typings/globals/core-js/index.d.ts"/>
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var stock_transfer_list_component_1 = require('./stock-transfer-list.component');
platform_browser_dynamic_1.bootstrap(stock_transfer_list_component_1.StockTransferList);
