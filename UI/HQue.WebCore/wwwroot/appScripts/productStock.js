"use strict";
var ProductStock = (function () {
    function ProductStock(purchasePrice, vat, quantity) {
        this.purchasePrice = purchasePrice;
        this.vat = vat;
        this.quantity = quantity;
    }
    //public purchasePrice: number;
    ProductStock.prototype.taxValue = function () {
        if (this.purchasePrice === undefined || this.vat === undefined)
            return 0;
        return this.purchasePrice * (this.vat / 100);
    };
    ProductStock.prototype.grossValue = function () {
        if (this.purchasePrice === undefined || this.quantity === undefined)
            return 0;
        return this.quantity * this.purchasePrice;
    };
    ProductStock.prototype.totalVatValue = function () {
        if (this.quantity === undefined)
            return 0;
        return this.quantity * this.taxValue();
    };
    ProductStock.prototype.netValue = function () {
        return this.grossValue() + this.totalVatValue();
    };
    return ProductStock;
}());
exports.ProductStock = ProductStock;
