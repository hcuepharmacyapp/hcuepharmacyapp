﻿app.controller('acceptedLeadsSearchCtrl', function ($scope, $location, $window, leadsService, leadsModel, pagerServcie) {

    var leads = leadsModel;

    $scope.search = leads;

    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        $.LoadingOverlay("show");
        leadsService.list($scope.search).then(function (response) {

          
            $scope.list = response.data.list;
           
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.leadsSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        $scope.search.leadStatus = 'ACPH';

       
        leadsService.list($scope.search).then(function (response) {


            
            $scope.List = response.data.list;
             
             
            for (var i = 0; i < $scope.List.length; i++) {

                if ($scope.List[i].localLeadStatus==undefined) {
                    $scope.List[i].localLeadStatus = "";
                }
              
                for (var j = 0; j < $scope.List[i].leadsProduct.length; j++) {



                    if ($scope.List[i].leadsProduct[j].type == "Capsules" || $scope.List[i].leadsProduct[j].type == "TAB" || $scope.List[i].leadsProduct[j].type == "CAP" || $scope.List[i].leadsProduct[j].type == null || $scope.List[i].leadsProduct[j].type == "INJ" || $scope.List[i].leadsProduct[j].type == "TABLET" || $scope.List[i].leadsProduct[j].type == "TABLETS" || $scope.List[i].leadsProduct[j].type == "Tablet") {


                        var Dosagestring = $scope.List[i].leadsProduct[j].dosage;
                        var Dosagedays = $scope.List[i].leadsProduct[j].numberofDays;

                        var Dosage = Dosagestring.split('-');
                        var M = 0, A = 0, N = 0;
                        var M = 0, A = 0, N = 0;
                        if (Dosage[0] != "") {
                            M = parseFloat(Dosage[0]);
                        }
                        if (Dosage[1] != "") {
                            A = parseFloat(Dosage[1]);
                        }
                        if (Dosage[2] != "") {
                            N = parseFloat(Dosage[2]);
                        }

                        var Totaldosage = +(M + A + N).toFixed(12)
                        var TotalTablets = Totaldosage * parseInt(Dosagedays);

                        TotalTablets = Math.round(TotalTablets);
                        $scope.List[i].leadsProduct[j].Totaltablets = TotalTablets;

                    } else {
                        $scope.List[i].leadsProduct[j].Totaltablets = 1;

                    }
                }
            }

            $scope.list = $scope.List;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.leadsSearch();

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }
$scope.ConvertTosale = function (ind, obj) {

        console.log(ind);
        console.log(JSON.stringify(obj));
        window.localStorage.setItem("ConvertLeadstoSaleObj", JSON.stringify(obj));
     //   $rootScope.$emit('ConvertLeadstoSaleObj', JSON.stringify(obj));
        window.location.assign('/sales/index');
      


    }
});