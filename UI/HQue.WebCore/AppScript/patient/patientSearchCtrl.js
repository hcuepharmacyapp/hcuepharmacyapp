﻿app.controller('patientSearchCtrl', function($scope, $location, patientService, patientModel, cacheService) {

    function navigate(list) {
        if (list.length === 0) {
            cacheService.set('selectedPatient', $scope.patientSearchData);
            $location.path('/patientcreate');
        }
    };

    $scope.patientSearchData = patientModel;
    $scope.patientSearchData.mobile = "";

    $scope.search = function () {
        $.LoadingOverlay("show");
        patientService.list($scope.patientSearchData,1).then(function(response) {
            $scope.list = response.data;
            navigate($scope.list);
            $.LoadingOverlay("hide");
        }, function() {
            $.LoadingOverlay("hide");
        });
    };

    $scope.patientSelect = function (patient) {
        cacheService.set('selectedPatient', patient);
        $location.path('/pos');
    }
});
