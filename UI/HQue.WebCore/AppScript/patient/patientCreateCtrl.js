﻿app.controller("patientCreateCtrl", function ($scope, $rootScope, $location, toastr, patientService, cacheService, salesService, close, param, $timeout, $filter) {
    $scope.focusInput = true;
    var ele = "";
    $scope.disableDiscount = false;
    $scope.disableDOB = false;
    $scope.disableFemale = false;
    $scope.disableMale = false;
    $scope.disableAddress = false;
    $scope.disableCity = false;
    $scope.disablePincode = false;
    $scope.disableCash = false;
    $scope.disableCredit = false;
    $scope.disableShortName = false;
    $scope.disableMobile = false;
    $scope.disableIdNumber = false;
    $scope.disableEmail = false;
    $scope.enableSavebtn = false;
    $scope.DobError = "";
    $scope.OldBalanceAmount = 0;
    $scope.isComposite = false;
    $scope.isdisableID = false;
    //$scope.isenableSavebtn();

    $scope.savePatient = cacheService.get("selectedPatient");


    cacheService.remove("selectedPatient");

    if ($scope.savePatient.customerPaymentType == undefined || $scope.savePatient.customerPaymentType == null || $scope.savePatient.customerPaymentType == "")
        $scope.savePatient.customerPaymentType = "Cash";

    $scope.isPaymentDone = false;

    $scope.checkPaymentDone = function (customerId) {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        patientService.checkPaymentDone(customerId).then(function (response) {
            $scope.OldBalanceAmount = response.data;

            $.LoadingOverlay("hide");


        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error', 'error');
        });
        $scope.isProcessing = false;
    };

    //if ($scope.savePatient.customerPaymentType === undefined) {
    //    $scope.savePatient.customerPaymentType = "Cash";
    //}
    if ($scope.savePatient.patientType === undefined) {
        $scope.savePatient.patientType = "1";
    }
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.maxDate = new Date();
    $scope.popup1 = {
        "opened": false
    };
    $scope.result1 = '';
    $scope.options1 = null;
    $scope.details1 = '';
    $scope.dateOptions = {
        "formatYear": "yy",
        "startingDay": 1
    };
    $scope.mode = param.mode;
    $scope.from = param.from;
    $scope.depMode = param.mode;
    $scope.LocationTypeList = ["Local", "Interstate", "Export"];
    if ($scope.mode == "Create") {
        if ($scope.from == "customer") {
            ResetPatientFileds("1");
        }
        $scope.savePatient.locationTypeName = $scope.LocationTypeList[0];
        $scope.savePatient.status = "1";
        //ResetPatientFileds("1");
    }

    function ResetPatientFileds(patientType) {
        $scope.savePatient = {
            "id": null,
            "name": null,
            "shortName": null,
            "dob": null,
            "gender": "M",
            "mobile": null,
            "empID": null,
            "address": null,
            "city": null,
            "pincode": null,
            "email": null,
            "customerPaymentType": "Cash",
            "discount": null,
            "patientType": patientType,
            "gsCustomerType": ""
        };
    }

    getInstanceValues = function () {
        patientService.getInstanceDetails().then(function (response) {
            console.log(JSON.stringify(response.data))

            if (response.data.gstselect) {
                $scope.isComposite = response.data.gstselect;
                if ($scope.isComposite == true) {
                    $scope.savePatient.locationTypeName = $scope.LocationTypeList[0];
                }
            }


        }, function (error) {
            console.log(error);
        });
    };
    getInstanceValues();

    $scope.GetPatientData = function () {
        console.log(JSON.stringify($scope.patData));
        $scope.savePatient.name = $scope.patData.name;

        $scope.savePatient.patientType = $scope.patData.patient == undefined ? $scope.patData.patientType.toString() : $scope.patData.patient.patientType.toString();
        $scope.savePatient.shortName = $scope.patData.patient == undefined ? $scope.patData.shortName : $scope.patData.patient.shortName;
        $scope.savePatient.locationTypeName = $scope.patData.patient == undefined ? $scope.patData.locationTypeName : $scope.patData.patient.locationTypeName;
        $scope.savePatient.status = ($scope.patData.status == undefined) ? "1" : $scope.patData.status.toString();

        $scope.savePatient.balanceAmount = 0;
        if ($scope.patData != undefined) {
            if ($scope.patData.balanceAmount != undefined) {
                $scope.savePatient.balanceAmount = $scope.patData.balanceAmount
            } else if ($scope.patData.patient != undefined) {
                if ($scope.patData.patient.balanceAmount != undefined) {
                    $scope.savePatient.balanceAmount = $scope.patData.patient.balanceAmount;
                }
            }
            $scope.savePatient.status = ($scope.patData.patient == undefined) ? "1" : ($scope.patData.patient.status == undefined) ? "1" : $scope.patData.patient.status.toString();
        }
        // $scope.OldBalanceAmount = $scope.savePatient.balanceAmount;
        // $scope.savePatient.balanceAmount = $scope.patData == undefined ? 0 : $scope.patData.balanceAmount;
        if ($scope.savePatient.locationTypeName == undefined) {
            $scope.savePatient.locationTypeName = "Local";
        }


        $scope.savePatient.gsTin = $scope.patData.patient == undefined ? $scope.patData.gsTin : $scope.patData.patient.gsTin;

        $scope.savePatient.pan = $scope.patData.patient == undefined ? $scope.patData.pan : $scope.patData.patient.pan;

        $scope.savePatient.drugLicenseNo = $scope.patData.patient == undefined ? $scope.patData.drugLicenseNo : $scope.patData.patient.drugLicenseNo;



        if ($scope.savePatient.shortName != null && $scope.savePatient.shortName != undefined && $scope.savePatient.shortName != '') {
            $scope.disableShortName = true;
        } else {
            $scope.disableShortName = false;
        }


        $scope.savePatient.dob = ($scope.patData.dob == "0001-01-01T00:00:00" ? '' : $scope.patData.dob);
        $scope.savePatient.gender = $scope.patData.gender;


        //if ($scope.patData.mobile == "NA")
        //{
        //    $scope.savePatient.mobile = "";
        //}
        //else {
        $scope.savePatient.mobile = $scope.patData.mobile;
        //}
        //Added by Annadurai Starts here
        if ($scope.patData.gender == "") {
            $scope.savePatient.gender = 'M';
        }
        //End
       
        salesService.getCustomerIDSeriesItem().then(function (response) {
            if (response.data != "" && response.data != null) {
                if (response.data.isEnableCustomerSeries == false || response.data.isEnableCustomerSeries == null || response.data.isEnableCustomerSeries == undefined) {
                    $scope.isdisableID = false;
                }
                else {
                    $scope.isdisableID = true;
                }
          }
            }, function () { 
        });



        $scope.savePatient.empID = $scope.patData.patient == undefined ? $scope.patData.empID : $scope.patData.patient.empID;
        $scope.savePatient.address = $scope.patData.address;
        $scope.savePatient.city = $scope.patData.patient == undefined ? $scope.patData.city : $scope.patData.patient.city;
        $scope.savePatient.pincode = $scope.patData.pincode;
        $scope.savePatient.email = $scope.patData.email;
        $scope.savePatient.discount = $scope.patData.discount;
        $scope.savePatient.id = $scope.patData.patient == undefined ? $scope.patData.id : $scope.patData.patient.id;
        if ($scope.patData.patient != undefined) {
            if ($scope.patData.patient.customerPaymentType == undefined) {
                $scope.savePatient.customerPaymentType = "Cash";
            } else {
                if ($scope.patData.patient.customerPaymentType == "Credit") {
                    $scope.savePatient.customerPaymentType = $scope.patData.patient.customerPaymentType;
                } else {
                    $scope.savePatient.customerPaymentType = "Cash";
                }
            }
        } else {
            if ($scope.patData.customerPaymentType == "Credit") {
                $scope.savePatient.customerPaymentType = $scope.patData.customerPaymentType;
            } else {
                $scope.savePatient.customerPaymentType = "Cash";
            }
        }
        if ($scope.savePatient.patientType == "2") {
            $scope.disableDiscount = false;
            $scope.disableDOB = true;
            $scope.disableFemale = true;
            $scope.disableMale = true;
            $scope.disableAddress = true;
            $scope.disableCity = true;
            $scope.disablePincode = true;
            // $scope.disableCash = true;
            //$scope.disableCredit = true;
            $scope.disableShortName = true;
            $scope.disableMobile = true;
            $scope.disableIdNumber = true;
            $scope.disableEmail = true;
            if ($scope.savePatient.customerPaymentType != "Cash") {
                $scope.savePatient.customerPaymentType = "Credit";
            }
        } else {
            $scope.disableDiscount = false;
            $scope.disableDOB = false;
            $scope.disableFemale = false;
            $scope.disableMale = false;
            $scope.disableAddress = false;
            $scope.disableCity = false;
            $scope.disablePincode = false;
            $scope.disableCash = false;
            $scope.disableCredit = false;
            $scope.disableShortName = false;
            $scope.disableMobile = false;
            $scope.disableIdNumber = false;
            $scope.disableEmail = false;
            if ($scope.savePatient.mobile != null && $scope.savePatient.mobile != undefined && $scope.savePatient.mobile != "") {
                $scope.disableMobile = true;
            } else {
                $scope.disableMobile = false;
            }
            if ($scope.savePatient.shortName != null && $scope.savePatient.shortName != undefined && $scope.savePatient.shortName != "") {
                $scope.disableShortName = true;
            } else {
                $scope.disableShortName = false;
            }
        }
    };
    if ($scope.mode == "Edit") {
        $scope.patData = angular.copy(param.patData);
        $scope.GetPatientData();
        $scope.checkPaymentDone($scope.savePatient.id);
    }
    $scope.isCustomerOrDept = param.isCustomerOrDept;
    $scope.maxDiscountFixed = param.isAvaildiscount;
    $scope.maxDisountValue = param.discountValue;

    $rootScope.$on("AddNewCustomerEvent", function () {
        $scope.close("No");
    });
    $scope.close = function (result) {
        close(result, 500);
        ResetPatientFileds("1");
        $(".modal-backdrop").hide();
    };
    $scope.formats = ["dd-MMMM-yyyy", "yyyy/MM/dd", "dd/MM/yyyy", "shortDate"];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ["M!/d!/yyyy"];

    $scope.patientTypeChange = function () {
        if ($scope.savePatient.patientType == 2) {
            $scope.disableDiscount = false;
            $scope.disableDOB = true;
            $scope.disableFemale = true;
            $scope.disableMale = true;
            $scope.disableAddress = true;
            $scope.disableCity = true;
            $scope.disablePincode = true;
            // $scope.disableCash = true;
            // $scope.disableCredit = true;
            $scope.disableShortName = true;
            $scope.disableIdNumber = true;
            $scope.disableMobile = true;
            $scope.disableEmail = true;
            // ResetPatientFileds("2");
            $scope.savePatient.customerPaymentType = "Credit";
            ele = document.getElementById("patientName");
            ele.focus();
        } else {
            $scope.disableDiscount = false;
            $scope.disableDOB = false;
            $scope.disableFemale = false;
            $scope.disableMale = false;
            $scope.disableAddress = false;
            $scope.disableCity = false;
            $scope.disablePincode = false;
            $scope.disableCash = false;
            $scope.disableCredit = false;
            $scope.disableShortName = false;
            $scope.disableMobile = false;
            $scope.disableIdNumber = false;
            $scope.disableEmail = false;
            ele = document.getElementById("patientName");
            ele.focus();
        }
    };
    $scope.idNumberChange = function () {
        $scope.IDNumberError = false;
        var empID = document.getElementById("idNumer").value;
        if (empID != undefined && empID != "") {
            $scope.savePatient.isAutoSave = false;

        }

    };
    $scope.maxDiscountExceeds = false;
    $scope.customerDiscount = function (val) {
        $scope.savePatient.discount = val;
        $scope.maxDiscountExceeds = false;
        if ($scope.maxDiscountFixed == "Yes") {
            if ($scope.savePatient.discount > $scope.maxDisountValue) {
                $scope.maxDiscountExceeds = true;
                return false;
            }
        }
    };
    $scope.GenderFocused = false;
    $scope.focusNextField = function (nextid) {
        if (nextid == "custtype") {
            if ($scope.disableDiscount) {
                ele = document.getElementById("saveCustomer");
            } else {
                ele = document.getElementById(nextid);
            }
        } else if (nextid == "shortName") {
            if ($scope.disableDOB) {
                ele = document.getElementById("address");
            } else {
                ele = document.getElementById(nextid);
            }
        } else if (nextid == "txtPan") {
            ele = document.getElementById("txtPan");
        } else if (nextid == "drugNo") {
            ele = document.getElementById("drugNo");
        } else if (nextid == "ddlLocationType") {
            if ($scope.isComposite && $scope.mode == "Create")
                ele = document.getElementById("ddlStatus1");
            else if ($scope.isComposite && $scope.mode == "Edit")
                ele = document.getElementById("ddlStatus2");
            else
                ele = document.getElementById("ddlLocationType");
        }
        else if (nextid == "ddlStatus1" && $scope.mode == "Edit") {
            ele = document.getElementById("ddlStatus2");
        }
        else {
            ele = document.getElementById(nextid);
            if ($scope.disableEmail && nextid == "txtemail") {
                //ele = document.getElementById("Discount");
                ele = document.getElementById("txtGstin");
            }
            if ($scope.disableIdNumber && nextid == "idNumer") {
                ele = document.getElementById("address");
            }
            if ($scope.disableMobile && nextid == "phone") {
                ele = document.getElementById("address");
            }
        }
        ele.focus();
    };
    $scope.checkAllPurchasefields = function (ind) {
        if (ind > 1) {
            if ($scope.savePatient.patientType == "2" && ($scope.savePatient.name == undefined || Object.keys($scope.savePatient.name).length == 0)) {
                ele = document.getElementById("patientName");
                ele.focus();
                return false;
            }
        }
        if (ind > 2) {
            if ($scope.savePatient.mobile == undefined || $scope.savePatient.mobile == null) {
                ele = document.getElementById("phone");
                ele.focus();
                return false;
            }
        }
    };
    //Added by bikas for customerSeries ID
    $scope.CustomerIDSeriesItem = [];
    if ($scope.mode != "Edit") {
        getCustomerIDSeriesItem();
    }
    function getCustomerIDSeriesItem() {
        salesService.getCustomerIDSeriesItem().then(function (response) {
            if (response.data != "" && response.data != null) {
                var customerid = response.data.customerSeriesId;

                var prefix = response.data.prefix;
                if (response.data.isEnableCustomerSeries == false || response.data.isEnableCustomerSeries == null || response.data.isEnableCustomerSeries == undefined) {
                    $scope.savePatient.empID = "";
                }
                else {
                    $scope.savePatient.empID = prefix + "" + customerid;
                    $scope.savePatient.customerSeriesId = response.data.customerSeriesId;
                    $scope.savePatient.Prefix = response.data.prefix;
                    $scope.isdisableID = true;
                }
               
            }
        }, function () {
        });
    }
    if ($scope.mode != "Edit") {
        getCustomerSeries();
    }
    function getCustomerSeries() {
        salesService.getCustomerIdSeriesSetting().then(function (response) {
            if (response.data != "" && response.data != null) {
                var tempValue = (response.data.isEnableCustomerSeries != null && response.data.isEnableCustomerSeries != undefined) ? response.data.isEnableCustomerSeries : false;
                if (tempValue == false) {
                    $scope.savePatient.empID = "";
                }
            } 
        });
    }
    
    // date validation 
    var DobErrorMessege = "* Invalid Date";
    var customerDobvalue = "";
    $scope.checkDateformate = function (e) {
        customerDobvalue = document.getElementById("customerDob").value;
        if (customerDobvalue.length == 2 || customerDobvalue.length == 5) {
            customerDobvalue = customerDobvalue + "/";
            $scope.savePatient.dob = customerDobvalue;
        }

        if (customerDobvalue.length == 10) {

            var currentDate = $filter('date')(new Date(), "dd/MM/yyyy").split("/");
            var SelectedDate = customerDobvalue.split("/");
            currentDate = currentDate[2] + currentDate[1] + currentDate[0];
            SelectedDate = SelectedDate[2] + SelectedDate[1] + SelectedDate[0];

            if (currentDate < SelectedDate) {
                $scope.DobError = " * Future Date Not Allowed";
                return;
            } else {
                $scope.DobError = "";
            }

        }


        $scope.validatedate();


    };
    $scope.validatedate = function () {
        customerDobvalue = document.getElementById("customerDob").value;
        if (customerDobvalue.length == 10) {
            $scope.DobError = "";
            validatedate(customerDobvalue);
        } else {
            $scope.DobError = "";
            if (customerDobvalue.length != 0) {
                $scope.DobError = DobErrorMessege;
            }
        }
    };



    function validatedate(customerDobvalue) {

        var date = customerDobvalue.substring(0, 2);
        var month = customerDobvalue.substring(3, 5);
        var year = customerDobvalue.substring(6, 10);

        if ((isNaN(date)) || isNaN(month) || isNaN(year)) {
            $scope.DobError = DobErrorMessege;
            return false;
        }
        //var date1 = parseFloat(SelectedDateSplits[2] + SelectedDateSplits[1] + SelectedDateSplits[0]);
        // var date2 = parseFloat(currentDateSplits[2] + currentDateSplits[1] + currentDateSplits[0]);

        var myDate = new Date(year, month - 1, date);
        var today = new Date();

        if (date < 1 || month < 1 || year < 1) {
            $scope.DobError = DobErrorMessege;
            return false;
        }
        // Create list of days of a month [assume there is no leap year by default]
        var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (month == 1 || month > 2) {
            if (date > ListofDays[month - 1]) {

                $scope.DobError = DobErrorMessege;
                return false;
            }
        }

        if (month == 2) {
            var lyear = false;
            if ((!(year % 4) && year % 100) || !(year % 400)) {
                lyear = true;
            }
            if ((lyear == false) && (date >= 29)) {

                $scope.DobError = DobErrorMessege;
                return false;
            }
            if ((lyear == true) && (date > 29)) {

                $scope.DobError = DobErrorMessege;
                return false;
            }
        }

        //Added date check by Annadurai
        if (myDate > today || month > 12) {
            $scope.DobError = DobErrorMessege;
        } else {
            $scope.DobError = "";
        }


    }



    $scope.isenableSavebtn = function () {

        if ($scope.savePatient.name != null && $scope.savePatient.name.length > 0 && $scope.savePatient.name != undefined) {
            $scope.enableSavebtn = true;
        } else if ($scope.savePatient.mobile != null && $scope.savePatient.mobile.length > 0 && $scope.savePatient.mobile != undefined) {
            $scope.enableSavebtn = true;
        } else {
            $scope.enableSavebtn = false;
        }
    };
    $scope.isenableSavebtn();

    $scope.create = function () {

        if (parseFloat($scope.savePatient.balanceAmount) > 0) {
            if ($scope.savePatient.mobile == null || $scope.savePatient.mobile == undefined) {

                toastr.info("Please Enter Mobile Number");
                return;
            } else if ($scope.savePatient.mobile != undefined && $scope.savePatient.mobile.length == 0) {
                toastr.info("Please Enter Mobile Number");
                return;
            }
        }
        if ((parseInt($scope.savePatient.balanceAmount) > 0) && parseInt($scope.savePatient.status) == 2) {
            $scope.savePatient.status = '1';
            toastr.info("Opening balance only applicable for Active customer..", "");
            return;
        }

        if ($scope.savePatient.status == undefined) {
            $scope.savePatient.status = 1;
        }

        $.LoadingOverlay("show");
        if ($scope.mode == "Create") {
            patientService.create($scope.savePatient)
                .then(function (response) {
                    $scope.savePatient.customerSeriesId = response.data.customerSeriesId;
                    $scope.savePatient.Prefix = response.data.prefix;
                    if (response.data.patientType != 2) {
                        if (response.data.isexists == true) {
                            $scope.IDNumberError = true;
                            ele = document.getElementById("idNumer");
                            ele.focus();
                            $.LoadingOverlay("hide");
                        } else {
                            if (response.data.shortName != "Already ShortName Exists") {
                                if (response.data.name != "Already Name Exists" && response.data.mobile != "Already Mobile Exists") {
                                    $scope.IDNumberError = false;
                                    if (response.data.status == 2 || response.data.status == 3) {
                                        response.data.name = response.data.mobile = response.data.empID = ""; response.data.shortName = "";
                                    }
                                    cacheService.set("selectedPatient", response.data);
                                    toastr.success("Customer created successfully");
                                    $rootScope.$emit("doSelectPatient", response.data, response.data.customerPaymentType);
                                    $scope.close("NO");
                                    $.LoadingOverlay("hide");
                                } else {
                                    toastr.error("Name and mobile already exists");
                                    ele = document.getElementById("patientName");
                                    ele.focus();
                                    $.LoadingOverlay("hide");
                                }
                            } else {
                                toastr.error("Short name already exists");
                                ele = document.getElementById("shortName");
                                ele.focus();
                                $.LoadingOverlay("hide");
                            }
                        }
                    } else {

                        if (response.data.name != "Already DepartmentName Exists") {
                            $scope.IDNumberError = false;
                            if (response.data.status == 2 || response.data.status == 3) {
                                response.data.name = response.data.mobile = response.data.empID = ""; response.data.shortName = "";
                            }
                            cacheService.set("selectedPatient", response.data);
                            toastr.success("Customer created successfully");
                            $rootScope.$emit("doSelectPatient", response.data, response.data.customerPaymentType);
                            $scope.close("NO");
                            $.LoadingOverlay("hide");

                        } else {
                            toastr.error("Department name already exists");
                            ele = document.getElementById("patientName");
                            ele.focus();
                            $.LoadingOverlay("hide");
                        }

                    }

                }, function () {
                    $.LoadingOverlay("hide");
                    toastr.error("Error creating customer");
                });
        } else {
            var tempEmpId = $scope.savePatient.empID;
            //if ($scope.disableIdNumber) {
            //    $scope.savepatient.empid = null;
            //}
            if ($scope.savePatient.dob == "DD/MM/YYYY") {
                $scope.savePatient.dob = null;
            }

            //if ($scope.savePatient.locationTypeName == "" || $scope.savePatient.locationTypeName == undefined) {
            //    $scope.savePatient.locationTypeName = "Local";
            //}
            if (parseFloat($scope.OldBalanceAmount) > 0) {
                if (($scope.savePatient.balanceAmount) == '') {
                    toastr.info("Opening Balance Cannot be less than: " + $scope.OldBalanceAmount.toString());
                    $scope.focusNextField('BalAmt');
                    $.LoadingOverlay("hide");
                    return;
                }
            }

            if (parseFloat($scope.savePatient.balanceAmount) < parseFloat($scope.OldBalanceAmount)) {
                toastr.info("Opening Balance Cannot be less than: " + $scope.OldBalanceAmount.toString());
                $scope.focusNextField('BalAmt');
                $.LoadingOverlay("hide");
                return;
            }

            console.log(JSON.stringify($scope.savePatient));
            patientService.update($scope.savePatient)
                .then(function (response) {

                    if (response.data.patientType != 2) {
                        if (response.data.isexists == true) {
                            $scope.IDNumberError = true;
                            ele = document.getElementById("idNumer");
                            ele.focus();
                            $.LoadingOverlay("hide");
                        } else {
                            if ($scope.disableIdNumber) {
                                response.data.empID = tempEmpId;
                            }
                            if (response.data.shortName != "Already ShortName Exists") {
                                if (response.data.status == 2 || response.data.status == 3) {
                                    response.data.name = response.data.mobile = response.data.empID = ""; response.data.shortName = "";
                                }

                                cacheService.set("selectedPatient", response.data);
                                toastr.success("Patient updated successfully");
                                $rootScope.$emit("doSelectPatient", response.data, response.data.customerPaymentType);
                                $scope.close("NO");
                                $.LoadingOverlay("hide");
                            } else {
                                toastr.error("Short name already exists");
                                ele = document.getElementById("shortName");
                                ele.focus();
                                $.LoadingOverlay("hide");
                            }
                        }
                    } else {

                        if ($scope.disableIdNumber) {
                            response.data.empID = tempEmpId;
                        }
                        if (response.data.name != "Already DepartmentName Exists") {
                            if (response.data.status == 2 || response.data.status == 3) {
                                response.data.name = response.data.mobile = response.data.empID = ""; response.data.shortName = "";
                            }

                            cacheService.set("selectedPatient", response.data);
                            toastr.success("Patient updated successfully");
                            $rootScope.$emit("doSelectPatient", response.data, response.data.customerPaymentType);
                            $scope.close("NO");
                            $.LoadingOverlay("hide");
                        } else {
                            toastr.error("Department name already exists");
                            ele = document.getElementById("shortName");
                            ele.focus();
                            $.LoadingOverlay("hide");
                        }

                    }

                }, function (error) {
                    if ($scope.savePatient.dob == null) {
                        $scope.savePatient.dob == "DD/MM/YYYY";
                    }
                    console.log(error);
                    $.LoadingOverlay("hide");
                    toastr.error("Error updating customer");
                });
        }
    };


    //$("#customerdob").keyup(function (e) {
    //    if ($(this).val().length == 2 || $(this).val().length == 5) {
    //        if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
    //            $(this).val($(this).val() + "/");
    //    }
    //});

    $scope.keydown = function (e) {
        //Rigth Arrow
        if (e.keyCode == 39) {
            if ($scope.GenderFocused) {
                if ($scope.savePatient.gender == "F") {
                    $scope.savePatient.gender = "M";
                    document.getElementById("male").focus();
                }
            }
        }
        //Left Arrow
        if (e.keyCode == 37) {
            if ($scope.GenderFocused) {
                if ($scope.savePatient.gender == "M") {
                    $scope.savePatient.gender = "F";
                    document.getElementById("female").focus();
                }
            }

        }
    };

    $scope.CustPaymentStatus = function (val) {

        if (parseInt($scope.savePatient.status) == 1) {
            $scope.savePatient.status = "1";
            return;
        }

        if ((parseInt($scope.savePatient.balanceAmount) > 0) && parseInt($scope.savePatient.status) != 3) {
            $scope.savePatient.status = '1';
            toastr.info("Payment is pending.... Please use Hide instead of InActive", "");
        }
        else {
            //patientService.custPaymentStatus($scope.savePatient.id)
            patientService.custPaymentStatus($scope.savePatient.name, $scope.savePatient.mobile)
                .then(function (response) {
                    var flag = response.data;

                    if (flag) {
                        $scope.savePatient.status = val;
                    }
                    else if ((!flag) && (parseInt(val) != 2)) {

                    }
                    else {
                        $scope.savePatient.status = '1';
                        toastr.info("Payment is pending.... Please use Hide instead of InActive", "");
                    }

                }, function (error) {
                    toastr.error("", "Error");
                    $.LoadingOverlay("hide");
                });
        }
    }

    $scope.CheckOpeningBal = function () {
        if (parseInt($scope.savePatient.balanceAmount) > 0) {
            $scope.savePatient.status = '1';
            toastr.info("Opening balance only applicable for Active customer..", "");
        }
        else {

        }
    }

    $scope.changePaymentType = function (status) {
        if (status) {
            $scope.savePatient.customerPaymentType = "Credit";
        } else {
            $scope.savePatient.customerPaymentType = "Cash";
        }
    };
});
