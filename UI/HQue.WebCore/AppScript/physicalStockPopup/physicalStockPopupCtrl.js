﻿var app = angular.module("physicalStockPopupModule", ['commonApp', 'toastr', 'angularModalService', 'ui.grid', 'ui.grid.edit', 'ui.grid.cellNav']);

angular.element(document).ready(function () {
    angular.bootstrap(document.getElementById("divPhysicalStockPopup"), ["physicalStockPopupModule"]);
});

app.controller("physicalStockPopupCtrl", function ($scope, $interval, $filter, ModalService, physicalStockPopupService) {
    
    var startDate = null;
    var endDate = null;
    var today = null;
    var lastPopupTime = null;
    var popupInterval = 0;
    var openedPopup = 0;
    var modalOpened = false;
    $scope.inventorySetting = { popupEnabled: false };

    $scope.StartTimer = function () {

        $scope.Timer = $interval(function () {
            if ($scope.inventorySetting.popupEnabled == true && modalOpened == false && !document.hidden) {

                today = new Date();                

                //We can add this lines when user reload the page at least per day once 
                //if (today.getTime() > endDate.getTime()) {
                //    $scope.StopTimer();
                //    return;
                //}

                var nextPopupDiff = null;
                if (lastPopupTime != undefined && lastPopupTime != null)
                    nextPopupDiff = ((today.getTime() - lastPopupTime.getTime()) / 1000) / 60;

                today = $filter('date')(today, 'yyyy-MM-dd HH:mm');
                today = new Date(today);

                var endTime = $scope.inventorySetting.popupEndTime.split(':');
                var todayEndTime = $filter('date')(new Date(), 'yyyy-MM-dd');
                todayEndTime = todayEndTime.split('-');
                todayEndTime = new Date(todayEndTime[0], todayEndTime[1] - 1, todayEndTime[2], endTime[0], endTime[1], 0, 0);

                var startTime = $scope.inventorySetting.popupStartTime.split(':');
                var todayStartTime = $filter('date')(new Date(), 'yyyy-MM-dd');
                todayStartTime = todayStartTime.split('-');
                todayStartTime = new Date(todayStartTime[0], todayStartTime[1] - 1, todayStartTime[2], startTime[0], startTime[1], 0, 0);

                if (openedPopup == 1 || ((lastPopupTime == null || nextPopupDiff >= popupInterval && popupInterval != 0) && startDate <= today && today <= endDate && todayStartTime <= today && today <= todayEndTime)) {

                    if (openedPopup != 1) {
                        lastPopupTime = new Date();
                        $scope.inventorySetting.lastPopupAt = $filter('date')(lastPopupTime, 'yyyy-MM-dd HH:mm:ss');
                        $scope.inventorySetting.openedPopup = 1;

                        physicalStockPopupService.updatePhysicalStockPopupSettings($scope.inventorySetting).then(function (response) {
                            $scope.inventorySetting = response.data;
                        });
                    }

                    physicalStockPopupService.getPhysicalStockProducts().then(function (response) {
                        if (response.data.length != 0)
                            showPhysicalStockPopup(response.data);
                    });
                }
            }
        }, 5000);

    };

    $scope.StopTimer = function () {

        //Cancel the Timer.
        if (angular.isDefined($scope.Timer)) {
            $interval.cancel($scope.Timer);
        }
    };

    function showPhysicalStockPopup(data) {
        if (modalOpened == false) {
            var m = ModalService.showModal({
                "controller": "physicalStockHistoryCtrl",
                "templateUrl": 'physicalStockPopupModal',
                "inputs": { "params": [{ "list": data }] },
            }).then(function (modal) {
                modalOpened = true;
                modal.element.modal();
                modal.close.then(function (result) {
                    modalOpened = false;
                    openedPopup = 0;
                });
            });
        }
    }

    function getInventorySettings() {
        physicalStockPopupService.getInventorySettings().then(function (response) {
            $scope.inventorySetting = response.data;
            setPopupSettings();
        });
    };
    getInventorySettings();

    function setPopupSettings() {

        if ($scope.inventorySetting.popupEnabled == true) {
            startDate = $filter('date')($scope.inventorySetting.popupStartDate, 'yyyy-MM-dd');
            endDate = $filter('date')($scope.inventorySetting.popupEndDate, 'yyyy-MM-dd');
            popupInterval = $scope.inventorySetting.popupInterval;

            var bits = startDate.split('-');
            var time = $scope.inventorySetting.popupStartTime.split(':');
            startDate = new Date(bits[0], bits[1] - 1, bits[2], time[0], time[1], 0, 0);

            bits = endDate.split('-');
            time = $scope.inventorySetting.popupEndTime.split(':');
            endDate = new Date(bits[0], bits[1] - 1, bits[2], time[0], time[1], 0, 0);

            if ($scope.inventorySetting.lastPopupAt != undefined)
                lastPopupTime = new Date($scope.inventorySetting.lastPopupAt);

            if ($scope.inventorySetting.openedPopup != undefined)
                openedPopup = $scope.inventorySetting.openedPopup;

            $scope.StartTimer();
        }
        else {
            $scope.StopTimer();
        }
    }

});