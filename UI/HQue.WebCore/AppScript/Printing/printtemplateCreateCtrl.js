﻿app.controller('printtemplateCreateCtrl', function ($scope, printingService, toastr) {

    $scope.templateData = {};
    $scope.templateData.templateName = "Custom Template";
    $scope.templateData.paperType = "4INCH";

    $scope.templateData.height;
    $scope.templateData.width;

    $scope.selectedField = {};
    window.onFieldSelected = function (field) {
        $scope.selectedField = field;
    };

    //$scope.btnSave = false;

    $scope.loadTemplate = function () {

        
        printingService.getTemplate('sales').then(function (response) {

            if (response.data == "") {
               // $scope.btnSave = false;
                $.LoadingOverlay("hide");
                return;
            }
            //$scope.btnSave = true;
            $scope.templateData = {};
            $scope.templateData = response.data;
            window.localStorage.setItem("TemplateData", JSON.stringify(response.data));
            window.localStorage.setItem("TemplateStationaryPath", $scope.templateData.stationaryFile);
        
            window.loadTemplate();
            $scope.loadStationary();
            $.LoadingOverlay("hide");

        }, function () {
            $.LoadingOverlay("hide");
        });

    }

    $scope.init = function () {

        window.init2();
        setDPI();

       // $scope.loadTemplate();
    }

    $scope.clear = function () {

        window.localStorage.removeItem("TemplateData");
        window.localStorage.removeItem("TemplateStationaryPath");
        boxes2 = [];
        //window.init2();
        window.reDraw();
    }

    $scope.save = function () {

        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        setDPI();
        //$scope.templateData.height = Math.round(HEIGHT);
        //$scope.templateData.width = Math.round(WIDTH);
        $scope.templateData.fontSize = FONT_SIZE;
        $scope.templateData.topMargin = TOP_MARGIN;
        $scope.templateData.leftMargin = LEFT_MARGIN;
        $scope.templateData.rightMargin = RIGHT_MARGIN;
        //$scope.templateData.columns = Math.round(Number(WIDTH) / DPI * CPI);
        $scope.templateData.columns = Math.round((Number(WIDTH) - (Number(RIGHT_MARGIN) * Number(FONT_WIDTH))) / ((Number(FONT_WIDTH) * 1.23)));
        
        $scope.templateData.stationaryFile = window.localStorage.getItem("TemplateStationaryPath");
        //$scope.templateData.dataItems = generateLayoutData();
        generateLayoutData();

        if ($scope.templateData.dataItems.length>0) {
            printingService.save($scope.templateData).then(function (response) {
                toastr.success('Print Template saved successfully');
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        else {
            $.LoadingOverlay("hide");
        }

        $scope.isProcessing = false;
    }

    function layoutData() {
        this.fieldName = "";
        this.lineNo = 0;
        this.width = 0;
        this.offset = 0;
        this.fieldType = 0;
        this.text = "";
        this.textAlign = 0;
        this.section = 0;
        this.x = 0;
        this.y = 0;
        this.h = 0;
        this.w = 0;
        this.fill = "";
        this.fontWeight = 0; 
        this.itemfontsize = 11;
    }

    function generateLayoutData() {

        $scope.templateData.dataItems =[]
        for (var i = 0; i < boxes2.length; i++) {

            var dataItem = new layoutData();
            dataItem.fieldName = boxes2[i].fieldName;
            dataItem.lineNo = Math.round((boxes2[i].y - (TOP_MARGIN * LINE_HEIGHT)) / (LINE_HEIGHT + LINE_SPACING));
            //dataItem.width = Math.round(boxes2[i].w / DPI * CPI);
            //dataItem.offset = Math.round(boxes2[i].x / DPI * (CPI + SPACING));
            dataItem.width = Math.round(boxes2[i].w / (FONT_WIDTH * 1.23));
            dataItem.offset = Math.round(boxes2[i].x / (FONT_WIDTH * 1.23));
            dataItem.fieldType = boxes2[i].fieldType;
            dataItem.text = boxes2[i].text;
            dataItem.textAlign = boxes2[i].alignment;
            dataItem.section = boxes2[i].section;
            dataItem.x = Math.round(boxes2[i].x);
            dataItem.y = Math.round(boxes2[i].y);
            dataItem.h = Math.round(boxes2[i].h);
            dataItem.w = Math.round(boxes2[i].w);
            dataItem.fill = boxes2[i].fill;
            dataItem.fontSize = boxes2[i].fontSize;
            dataItem.fontWeight = boxes2[i].fontWeight;
            dataItem.itemfontsize = boxes2[i].itemfontsize;
            $scope.templateData.dataItems.push(dataItem);
        }
        
    }

    $scope.uploadFile = function () {
        $.LoadingOverlay("show");
        printingService.upload($scope.SelectedFileForUpload).then(function (response) {

            window.localStorage.setItem("TemplateStationaryPath", response.data);

            $scope.loadStationary();

            $.LoadingOverlay("hide");

            setDPI();

        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    //File Upload
    $scope.SelectedFileForUpload = null;
    $scope.fileSelected = false;
    //File Select event 
    $scope.selectFileforUpload = function (file) {
        $scope.SelectedFileForUpload = file[0];
        $scope.fileSelected = true;
    }

    //Load stationary file 
    $scope.loadStationary = function () {

        var fileName = window.localStorage.getItem("TemplateStationaryPath");
        if (fileName != "undefined") {
            window.loadImage(fileName);
            setDPI();
        }
    }

    function setDPI() {

        $scope.templateData.height = Math.round((HEIGHT / DPI) * 2.54);
        $scope.templateData.width = Math.round((WIDTH / DPI) * 2.54);
        //window.setResolution($scope.templateData.height, $scope.templateData.width);
    }


    // Ruler



});