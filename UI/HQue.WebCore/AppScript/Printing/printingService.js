﻿app.factory('printingService', function ($http) {
    return {
        save: function (templateData) {
            return $http.post('/invoiceprint/saveTemplate', templateData);
        },
        getTemplate: function (t) {
            return $http.get('/invoiceprint/getTemplate', t);
        },
        upload: function (file) {
            var formData = new FormData();
            formData.append('file', file);

            return $http.post('/invoiceprint/uploadStationary', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });
            return $http.post('/invoiceprint/uploadStationary', file);
        },
    }
});
