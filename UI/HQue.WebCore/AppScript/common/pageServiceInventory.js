﻿app.factory('pagerServcieInventory', function () {

    var pages = [];

    var noOfLinks = 5;
    var pageSize = 20;
    var withFirstLast = true;
    var linksToAdd = Math.floor(noOfLinks / 2);
    var noOfPages = 0;
    var _cb = null;
    var page = { "pageNo": 1, "pageSize": pageSize };

    function pushPages(pageNo) {
        var active = pageNo.pageNo === page.pageNo;
        var p = {
            page: pageNo,
            isActive: active
        };

        pages.push(p);
    }

    function getLinks() {
        if (noOfLinks === 2) {
            if (page.pageNo > 1) {
                var link = { "name": "Previous", pageNo: page.pageNo - 1 };
                pushPages(link);
            }
            if (page.pageNo < noOfPages) {
                var link = { "name": "Next", pageNo: page.pageNo + 1 };
                pushPages(link);
            }
        }
        if (noOfLinks > 2) {
            if (noOfPages < noOfLinks) {
                if (noOfPages === 1)
                    return;

                for (var i = 1; i <= noOfPages; i++) {
                    var link = { "name": i, pageNo: i };
                    pushPages(link);
                }

            } else {
                var max = page.pageNo + linksToAdd;
                var min = page.pageNo - linksToAdd;

                if (max > noOfPages) {
                    min = (noOfPages - noOfLinks) + 1;
                    max = noOfPages;
                }

                if (min < 1) {
                    min = 1;
                    max = noOfLinks;
                }

                if (withFirstLast && page.pageNo > 1) {
                    var link = { "name": "First", pageNo: 1 };
                    pushPages(link);
                }

                for (var i = min; i <= max; i++) {
                    var link = { "name": i, pageNo: i };
                    pushPages(link);
                }

                if (withFirstLast && page.pageNo < noOfPages) {
                    var link = { "name": "Last", pageNo: noOfPages };
                    pushPages(link);
                }
            }
        }

        return pages;
    }

    

    function init(totalRows, cb) {
        page.pageNo = 1;
        pages.length = 0;
        noOfPages = Math.ceil(totalRows / pageSize);
        _cb = cb;
        getLinks();
    }

    function paginate(pageNo) {
        page.pageNo = pageNo;
        pages.length = 0;
        getLinks();
        _cb();
    }

    return {
        page: page,
        pages: pages,
        init: init,
        paginate: paginate
    };
});