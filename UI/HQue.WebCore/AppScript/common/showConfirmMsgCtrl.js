﻿app.controller('showConfirmMsgCtrl', function ($scope, close, ModalService, params) {

    $scope.params = params;
    if ($scope.params[0].data.msgTitle == "" || $scope.params[0].data.msgTitle == null || $scope.params[0].data.msgTitle == undefined) {
        $scope.params[0].data.msgTitle = "hCue - Alert";
    }

    $scope.close = function (result) {
        close(result, 100);
        $(".modal-backdrop").hide();
    };

    if ($scope.params[0].data.showOk) {
        $scope.focusOnOk = true;
    }
    else if ($scope.params[0].data.focusOnNo) {
        $scope.focusOnNo = true;
    }
    else if ($scope.params[0].data.showYes) {
        $scope.focusOnYes = true;
    }
    
});