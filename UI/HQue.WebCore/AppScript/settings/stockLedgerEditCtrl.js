﻿app.controller('stockLedgerEditCtrl', function ($scope, toastr, toolService, $filter, ModalService, productStockService, pagerServcie, stockLedgerUpdateHistoryModel, toolModel, productModel) {

    var stockLedgerUpdateHistory = stockLedgerUpdateHistoryModel;
    $scope.search = stockLedgerUpdateHistory;

    var tools = toolModel;
    $scope.tool = tools;
    var product = productModel;

    $scope.list = [];
    $scope.stockLedgerList = [];
    $scope.dataCorrectionArray = [];
    $scope.stockLedgerList.stockLedgerUpdateHistoryItem = [];
    $scope.selectedBranch = {
        "id": "",
        "name": "",
        "phone": "",
        "accountId": ""
    };
    $scope.selectedAccount = {
        "id": "",
        "name": ""
    };
    $scope.accountList = [];
    $scope.branchList = [];
    $scope.branchListTemp = [];
    $scope.offline = "";
    $scope.online = "";

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    

    $scope.getBranchList = function () {
        $scope.offline = "";
        $scope.online = "";
        $.LoadingOverlay("show");
        if ($scope.selectedBranch == null)
        {
            $scope.selectedBranch = {
                "id": "",
                "name": "",
                "phone": "",
                "accountId": ""
            };
        }
        toolService.getBranchList($scope.selectedAccount.id,$scope.selectedBranch.id).then(function (response) {
            $.LoadingOverlay("hide");
            $scope.list = response.data;
            $scope.getNagativeStockList();
            //$scope.getReverseSyncOfflineToOnlineCompare();
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getBranchList();

    $scope.getBranchListByAccount = function () {
        $scope.branchList = [];
        $scope.stockLedgerList = [];
        for (var i = 0; i < $scope.branchListTemp.length; i++) {
            if ($scope.branchListTemp[i].accountId == $scope.selectedAccount.id)
                $scope.branchList.push($scope.branchListTemp[i]);
        }
        $scope.getBranchList();
    }


    $scope.productId = "";
    $scope.onProductSelect = function (obj) {
        $scope.productId = obj.id;
    }

    $scope.getProducts = function (val) {
        return toolService.InstancedrugFilter(val, $scope.selectedBranch.id).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getAccountList = function () {
        $.LoadingOverlay("show");
        toolService.getAccountList().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.accountList = response.data;
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getAccountList();

    $scope.getBranchListData = function () {
        $.LoadingOverlay("show");
        toolService.getBranchListData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.branchList = response.data;
            $scope.branchListTemp = $scope.branchList;
           
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getBranchListData();

    $scope.stockLedgerEditList = function () {
        $scope.getReverseSyncOfflineToOnlineCompare();
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        $scope.search.page.pageNo = 1;
        $scope.productname = "";
        $scope.dataCorrectionArray = [];
        $scope.isAllSelected = false;
        if ($scope.drugName == null || $scope.drugName == "" || $scope.drugName == undefined)
        {
            $scope.productname = "";
        }
        else
        {
            $scope.productname = $scope.drugName.name;
        }
        if ($scope.selectedAccount.id == "" || $scope.selectedAccount.id == undefined || $scope.selectedAccount.id == null) {
            toastr.info('Please select the Account');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
            return;
        }
        if ($scope.selectedBranch.id == "" || $scope.selectedBranch.id == undefined || $scope.selectedBranch.id == null) {
            toastr.info('Please select the Branch');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
            return;
        }
        toolService.stockLedgerEditList($scope.selectedAccount.id, $scope.selectedBranch.id, $scope.productname).then(function (response) {
            $scope.stockLedgerList = response.data.list;
            if ($scope.stockLedgerList.length > 0)
            {
                var change = 0;
                var count = 0;

                // Gavaskar Comment 26-04-2018 Start
                /*
                for (var i = 0; i < $scope.stockLedgerList.length; i++) {
                    count = 0;
                    for (var j = 0; j < $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem.length; j++) {
                        $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].transactionType = "OpeningStock";
                        if ($scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock != 0) {
                            if ($scope.stockLedgerList[i].stockid == $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].stockid)
                            {
                                if (count == 0) {
                                    if ($scope.stockLedgerList[i].diffQty <= 0) {
                                        $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock = $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock + $scope.stockLedgerList[i].diffQty;
                                        change = $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock - $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].currentStock;
                                        if (change = $scope.stockLedgerList[i].diffQty) {
                                            count = 1;
                                        }
                                    }
                                    else {
                                        $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock = $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock + $scope.stockLedgerList[i].diffQty;
                                        change = $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock + $scope.stockLedgerList[i].diffQty;
                                        if (change = $scope.stockLedgerList[i].diffQty) {
                                            count = 2;
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            if (count == 0) {
                                if ($scope.stockLedgerList[i].stockid == $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].stockid)
                                {
                                    $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock = $scope.stockLedgerList[i].diffQty;
                                    change = $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].diffQty;
                                    if (change = $scope.stockLedgerList[i].diffQty) {
                                        count = 3;
                                    }
                                }
                            }
                        }
                    }
                } */
                // Gavaskar Comment 26-04-2018 End

                for (var i = 0; i < $scope.stockLedgerList.length; i++) {
                    for (var j = 0; j < $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem.length; j++) {
                        for (var k = 0; k < $scope.stockLedgerList[i].stockLedgerUpdateHistoryDiffQtyItem.length; k++) {
                            $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].transactionType = "OpeningStock";
                            if ($scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock != 0) {

                                if ($scope.stockLedgerList[i].stockLedgerUpdateHistoryDiffQtyItem[k].stockid == $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].stockid) {
                                    if ($scope.stockLedgerList[i].diffQty <= 0) {
                                        $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock = $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock + $scope.stockLedgerList[i].stockLedgerUpdateHistoryDiffQtyItem[k].diffQty;

                                    }
                                    else {
                                        $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock = $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock + $scope.stockLedgerList[i].stockLedgerUpdateHistoryDiffQtyItem[k].diffQty;

                                    }
                                }

                            }
                            else {
                                if ($scope.stockLedgerList[i].stockLedgerUpdateHistoryDiffQtyItem[k].stockid == $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].stockid) {
                                    $scope.stockLedgerList[i].stockLedgerUpdateHistoryItem[j].actualStock = $scope.stockLedgerList[i].stockLedgerUpdateHistoryDiffQtyItem[k].diffQty;
                                }
                            }
                        }
                    }
                }
                $scope.isDisabled = false;
                $.LoadingOverlay("hide");
                $scope.selectedIndex = -1;
            }
            else
            {
                toastr.info('Record not found');
                $.LoadingOverlay("hide");
            }

        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.toggleProductDetail = function (obj, data) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        }
        else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.validateEditClosingStock = function (stockLedger) {
        var closingstock = 0;
        closingstock = stockLedger.currentStock - stockLedger.diffQty;
        if (closingstock > stockLedger.stock) {
            // toastr.info("Current stock diff");
            return false;
        }
    };

    $scope.updateAllProduct = function (stockLedgerList) {
        $scope.getReverseSyncOfflineToOnlineCompare();
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if ($scope.negativeStockList != 0)
        {
            toastr.info('Please update negative stock');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
            return;
        }
        if ($scope.reverseSyncId != 0 || $scope.reverseSyncId == 'Internet Not Available' || $scope.reverseSyncId == 'Online') {
            if ($scope.reverseSyncId == 'Internet Not Available') {
                toastr.info('Internet not available. Please connect the Internet');
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
                return;
            }
            else if ($scope.reverseSyncId == 'Online' || $scope.reverseSyncId == null || $scope.reverseSyncId == undefined) {

            }
            else {
                toastr.info('Online data is pending for sync. Please wait and try later');
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
                return;
            }
        }
        $scope.isDisabled = true;
        toolService.bulkStockLedgerUpdate(stockLedgerList, $scope.selectedAccount.id, $scope.selectedBranch.id).then(function (response) {
            $scope.isUpdate = false;
            // $scope.productSearch($scope.search.name);
            ///$scope.searchProduct();
            toastr.success('Stock ledger updated successfully');
            $scope.selectedIndex = -1;
            $scope.stockLedgerList = [];
            // $scope.productsItem = {};
            $scope.isProcessing = false;
            $scope.isDisabled = false;
            $scope.cancel();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            //toastr.error('Product already exist', 'Error');
            $scope.isProcessing = false;

        });
    }

    $scope.cancel = function () {
        //$.LoadingOverlay("show");
        $scope.selectedBranch = {
            "id": "",
            "name": "",
            "phone": "",
            "accountId": ""
        };
        $scope.selectedAccount = {
            "id": "",
            "name": ""
        };
        $scope.getBranchList();
        $scope.stockLedgerList = [];
        $scope.drugName = "";
        //document.getElementById("selectedAccount").focus();
        // $.LoadingOverlay("hide");
    };

    $scope.addPatientId = function (status, index, stockLedger) {

        var count = 0;
        var listcount = $scope.stockLedgerList.length;
        angular.forEach($scope.stockLedgerList, function (itm, ind) {
            if (itm.isChecked)
                count++; //$scope.dataCorrectionArray.push(itm);                            
        });

        if (status)
            $scope.dataCorrectionArray.push(JSON.parse(JSON.stringify(stockLedger))); //angular.copy(saleItem, dataCorrectionArray);
        else {
            var idx = $scope.dataCorrectionArray.indexOf(index);
            //$scope.dataCorrectionArray[index].isChecked = false; //.splice(idx, 1);
            $scope.dataCorrectionArray.splice(idx, 1);
        }

        if (count == listcount) {
            $scope.isAllSelected = true;
        }
    }

    $scope.selectAll = function () {
        var toggleStatus = $scope.isAllSelected;

        angular.forEach($scope.stockLedgerList, function (itm) {
            itm.isChecked = toggleStatus;
        });
        //Changed for Pagination Issue and bulk Data Correction issue
        if (toggleStatus) {
            var tempList = JSON.parse(JSON.stringify($scope.stockLedgerList));  //angular.copy(tempList, $scope.dataCorrectionArray);
            if ($scope.dataCorrectionArray.length == 0)
                $scope.dataCorrectionArray = tempList;
            else {
                angular.forEach(tempList, function (itm) {
                    $scope.dataCorrectionArray.push(itm);
                });
            }
        }
        else {
            var poptempList = JSON.parse(JSON.stringify($scope.stockLedgerList));
            angular.forEach(poptempList, function (index) {
                var idx = index;
                $scope.dataCorrectionArray.splice(index, 1);    //$scope.dataCorrectionArray.pop(poptempList);
            });
        }
    }

    $scope.updateProductStockOfflineToOnline = function () {
        $scope.getReverseSyncOfflineToOnlineCompare();
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if ($scope.selectedAccount.id == "" || $scope.selectedAccount.id == undefined || $scope.selectedAccount.id == null) {
            toastr.info('Please select the Account');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
            return;
        }
        if ($scope.selectedBranch.id == "" || $scope.selectedBranch.id == undefined || $scope.selectedBranch.id == null) {
            toastr.info('Please select the Branch');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
            return;
        }
        if ($scope.reverseSyncId != 0 || $scope.reverseSyncId == 'Internet Not Available' || $scope.reverseSyncId == 'Online') {
            if ($scope.reverseSyncId == 'Internet Not Available') {
                toastr.info('Internet not available. Please connect the Internet');
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
                return;
            }
            else if ($scope.reverseSyncId == 'Online' || $scope.reverseSyncId == null || $scope.reverseSyncId == undefined) {

            }
            else {
                toastr.info('Online data is pending for sync. Please wait and try later');
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
                return;
            }
        }
        toolService.bulkUpdateProductStockOfflineToOnline($scope.selectedAccount.id, $scope.selectedBranch.id).then(function (response) {
            if (response.data == false)
            {
                toastr.info('Internet not available. Please connect the Internet');
                $scope.isProcessing = false;
                $scope.cancel();
                $.LoadingOverlay("hide");
            }
            else
            {
                toastr.success('Offline to online productstock updated successfully');
                $scope.isProcessing = false;
                $scope.cancel();
                //document.getElementById("selectedAccount").focus();
                $.LoadingOverlay("hide");
            }

        }, function () {
            $.LoadingOverlay("hide");
            $scope.isProcessing = false;

        });
    }
    $scope.getNagativeStockList = function () {
        if ($scope.selectedBranch.id == "" || $scope.selectedBranch.id == undefined || $scope.selectedBranch.id == null) {
            return;
        }
        toolService.getNagativeStockList($scope.selectedAccount.id, $scope.selectedBranch.id).then(function (response) {
            $scope.negativeStockList = response.data.length;

        }, function () {

        });
    }
    $scope.updateNegativeStockOfflineToOnline = function () {
        $scope.getReverseSyncOfflineToOnlineCompare();
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if ($scope.selectedAccount.id == "" || $scope.selectedAccount.id == undefined || $scope.selectedAccount.id == null)
        {
            toastr.info('Please select the Account');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
            return;
        }
        if ($scope.selectedBranch.id == "" || $scope.selectedBranch.id == undefined || $scope.selectedBranch.id == null)
        {
            toastr.info('Please select the Branch');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
            return;
        }
        if ($scope.reverseSyncId != 0 || $scope.reverseSyncId == 'Internet Not Available' || $scope.reverseSyncId == 'Online')
        {
            if ($scope.reverseSyncId == 'Internet Not Available') {
                toastr.info('Internet not available. Please connect the Internet');
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
                return;
            }
            else if ($scope.reverseSyncId == 'Online' || $scope.reverseSyncId == null || $scope.reverseSyncId == undefined) 
            {

            }
            else
            {
                toastr.info('Online data is pending for sync. Please wait and try later');
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
                return;
            }
        }
        
        toolService.bulkUpdateNegativeStockOfflineToOnline($scope.selectedAccount.id, $scope.selectedBranch.id).then(function (response) {
            if (response.data.length > 0)
            {
                if (response.data[0].statusText == 'Internet Not Available')
                    {
                        toastr.info('Internet not available. Please connect the Internet');
                        $scope.isProcessing = false;
                        $scope.cancel();
                        $.LoadingOverlay("hide");
                    }
                    else
                    {
                        toastr.success('Offline to online negative stock updated successfully');
                        $scope.isProcessing = false;
                        $scope.cancel();
                        $scope.stockLedgerList = [];
                        //document.getElementById("selectedAccount").focus();
                        $.LoadingOverlay("hide");
                    }
            }
            else
            {
                toastr.info('Record not found nagative stock');
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
            }
           
        }, function () {
            $.LoadingOverlay("hide");
            $scope.isProcessing = false;

        });
    }

    $scope.getReverseSyncOfflineToOnlineCompare = function () {
        if ($scope.selectedBranch.id == "" || $scope.selectedBranch.id == undefined || $scope.selectedBranch.id == null) {
            return;
        }
        toolService.getReverseSyncOfflineToOnlineCompare($scope.selectedAccount.id, $scope.selectedBranch.id).then(function (response) {
            $scope.reverseSyncId = response.data;
        }, function () {

        });
    }

});

app.directive("contenteditable", function () {
    return {
        restrict: "A",
        require: "ngModel",

        link: function (scope, element, attrs, ngModel) {

            function read() {
                ngModel.$setViewValue(element.html());
            }
            ngModel.$render = function () {
                element.html(ngModel.$viewValue || "");
            };
            element.bind("blur keyup change", function () {
                scope.$apply(read);
            });

            element.css('outline', 'none');
            element.bind("blur", function (event) {
                element[0].blur();
                if (element[0].innerText != '' && element[0].innerText != null) {

                    if (attrs.ngModel != "stockLedger.stockLedgerUpdateHistoryItem.actualStock") {
                        if ((parseFloat(element[0].innerText) > 100 || ((element[0].innerText).length) >= 6) && event.which != 9 && event.which != 8 && event.which != 13 && event.which != 110 && event.which != 190 || (event.which < 48 && event.which > 57) || (event.which < 37 && event.which > 40)) {
                            event.preventDefault();
                            return false;
                        }
                    }
                    else if (attrs.ngModel == "stockLedger.stockLedgerUpdateHistoryItem.actualStock") {
                        if (element[0].innerText.length >= 7 && event.which != 8 && event.which != 13 && event.which != 9)
                            event.preventDefault();
                        return false;
                    }
                    scope.stockLedger.isEdit = true;
                }

                event.preventDefault();
            });

            element.bind("keydown", function (event) {

                if (element[0].innerText != '' && element[0].innerText != null && element[0].innerText != undefined) {
                    if (attrs.ngModel != "stockLedger.stockLedgerUpdateHistoryItem.actualStock") {
                        if ((parseFloat(element[0].innerText + event.charCode) > 100 || ((element[0].innerText + event.charCode).length) >= 6) && event.which != 9 && event.which != 8 && event.which != 13 && event.which != 110 && event.which != 190 || (event.which < 48 && event.which > 57) || (event.which < 37 && event.which > 40)) {
                            event.preventDefault();
                            return false;
                        }
                    }
                    else if (attrs.ngModel == "stockLedger.stockLedgerUpdateHistoryItem.actualStock") {
                        if (element[0].innerText.length >= 7 && event.which != 8 && event.which != 9 && event.which != 13) {
                            event.preventDefault();
                            return false;
                        }
                    }
                    scope.stockLedger.isEdit = true;
                }
                if (event.which === 13) {
                    event.preventDefault();
                    element.next().focus();
                }
            });
        }
    };
});

app.directive('numberOnlyInput', function () {
    return {
        restrict: 'EA',
        //template: '<input type="text" name="{{inputName}}" ng-model="inputValue" />',
        scope: {
            inputValue: '=',
            inputName: '=',
            min: '@',
            max: '@',
            step: '@'
        },
        link: function (scope) {
            scope.$watch('inputValue', function (newValue, oldValue) {
                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.')) return;
                if (arr.length === 2 && newValue === '-.') return;
                if (isNaN(newValue)) {
                    scope.inputValue = oldValue;
                    return;
                }
                if (!isNaN(newValue)) {
                    if (newValue < parseInt(scope.min) || newValue > parseInt(scope.max)) {
                        scope.inputValue = oldValue;
                        return;
                    }
                }
            });
        }
    };
});