﻿app.controller('branchListCtrl', function ($scope, toastr, toolService, $filter, ModalService) {
    $scope.list = [];
    $scope.listTemp = [];
    $scope.Isupdatenable = false;
    $scope.selectedIndex = -1;
    $scope.selectedBranch = {
        "id": "",
        "name": "",
        "phone": "",
        "accountId": "",
    };
    $scope.selectedAccount = {
        "id": "",
        "name": ""
    };
    //$scope.selectedUserId = {
    //    "id": "",
    //    "userId": ""
    //};
    $scope.accountList = [];
    $scope.branchList = [];
    $scope.branchListTemp = [];
    $scope.featureList = null;
    $scope.selectAll = false;

    $scope.updatenable = function (branch,index) {
        $scope.selectedIndex = index;
        $scope.getCustomSettings(branch);
    }



    $scope.offline = "";
    $scope.online = "";
    $scope.getBranchList = function () {
        $scope.offline = "";
        $scope.online = "";
        $.LoadingOverlay("show");
        console.log('selecttedData', $scope.selectedAccount.name)
        if ($scope.selectedBranch.id == null || $scope.selectedBranch.id == undefined) {
             $scope.selectedBranch.id = "";
        }
        toolService.getBranchList($scope.selectedAccount.id, $scope.selectedBranch.id, $scope.selectedAccount.name).then(function (response) {
            $.LoadingOverlay("hide");
            $scope.list = response.data;
            $scope.listTemp = $scope.list;
        }, function () {
            $.LoadingOverlay("hide");
        });
    }


    $scope.getBranchList();

    $scope.getBranchUserIdList = function () {
        $scope.offline = "";
        $scope.online = "";
        $.LoadingOverlay("show");
        console.log('selecttedData', $scope.selectedAccount.name)
        if ($scope.selectedBranch.id == null || $scope.selectedBranch.id == undefined) {
            $scope.selectedBranch.id = $scope.selectedBranch.instanceId;
        }
        else {
            $scope.selectedBranch = "";
        }
        toolService.getBranchList($scope.selectedAccount.id, $scope.selectedBranch.id, $scope.selectedAccount.name).then(function (response) {
            $.LoadingOverlay("hide");
            $scope.list = response.data;
            $scope.listTemp = $scope.list;
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.getBranchListByAccount = function () {
        $scope.branchList = [];
        for (var i = 0; i < $scope.branchListTemp.length; i++) {
            if ($scope.branchListTemp[i].accountId == $scope.selectedAccount.id)
                $scope.branchList.push($scope.branchListTemp[i]);
        }
        $scope.selectedBranch.id = "";
        if ($scope.selectedBranch != null) {
            $scope.getBranchList($scope.selectedBranch.id);
        }
        else {
            $scope.selectedBranch = "";
        }
    }   

    $scope.getAccountList = function () {
        $.LoadingOverlay("show");
        toolService.getAccountList().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.accountList = response.data;
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getAccountList();
       
    $scope.changeOptions = function () {
        $.LoadingOverlay("show");
        $scope.list = $filter("filter")($scope.listTemp, {
            "isOfflineInstalled": $scope.offline,
            "offlineStatus": $scope.online
        });
        $.LoadingOverlay("hide");
    }

    $scope.getBranchListData = function () {
        $.LoadingOverlay("show");
        toolService.getBranchListData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.branchList = response.data;
            $scope.branchListTemp = $scope.branchList;
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getBranchListData();


    //$scope.showSync = false;
    //$scope.showLoyal = false;
    //$scope.updateSync = function (branch, index,status) {
    //    if ($scope.selectedAccount.id == null || $scope.selectedAccount.id == "") {
    //        toastr.info("Please select account and try again.");
    //        return;
    //    }
    //    if ($scope.selectedBranch.id == null || $scope.selectedBranch.id == "") {
    //        toastr.info("Please select branch and try again.");
    //        return;
    //    }
    //    $.LoadingOverlay("show");
    //    $scope.list[index].isActive = status;
    //    $scope.list[index].customSettingsGroupId = 5;
    //    if ($scope.CustomId == null || $scope.CustomId == undefined)
    //    {
    //        $scope.list[index].id = "";
    //    }
    //    else {
    //        $scope.list[index].id = $scope.CustomId;
    //    }
    //    toolService.saveCustomSettings($scope.list).then(function (response) {
    //        $.LoadingOverlay("hide");
    //        toastr.success("Settings saved successfully.");
    //        $scope.getBranchList();
    //        $scope.getCustomSettings(branch);
    //       // $scope.list = response.data;
    //    }, function () {
    //        $.LoadingOverlay("hide");
    //    });
    //};
    //$scope.disSync = true;
    //$scope.disLoyalty = true;

    $scope.checkAll = function (selectVal) {
        console.log($scope.selectAll);
        if (selectVal) {
            angular.forEach($scope.featureList, function (value, key) {
                value.isActive = true;
            });

        } else {
            angular.forEach($scope.featureList, function (value, key) {
                value.isActive = false;
            });
        }
    };

    $scope.checkSelectAll = function (val1) {        

        if (val1 == false) {
            document.getElementById('selectAll').checked = false;
        } else {
            var count1 = 0;

            angular.forEach($scope.featureList, function (value, key) {
                if (value.isActive == true) {
                    count1++;
                }
            });
            if (count1 == $scope.featureList.length) {
                document.getElementById('selectAll').checked = true;
            } else {
                document.getElementById('selectAll').checked = false;
            }
        }

    }

    $scope.getCustomSettings = function (branch) {
        if ($scope.selectedAccount == null || $scope.selectedAccount.id == null) {
            return;
        }
        if ($scope.selectedBranch == null || $scope.selectedBranch.id == null) {
            return;
        }
        $.LoadingOverlay("show");
        toolService.getCustomSettings(branch.accountId,branch.instanceId).then(function (response) {
            $.LoadingOverlay("hide");
            $scope.featureList = response.data;

            $scope.checkSelectAll(true);

            //for (var x = 0; x < $scope.featureList.length; x++) {

            //    if ($scope.featureList[x].customSettingsGroupId == 5) {
            //        $scope.showSync = $scope.featureList[x].isActive;
            //        $scope.CustomId = $scope.featureList[x].id;
            //        if ($scope.featureList[x].isActive == true) {
            //            $scope.disSync = false;
            //        }
            //        else {
            //            $scope.disSync = true;
            //        }
            //    }
            //    if ($scope.featureList[x].customSettingsGroupId == 4) {
            //        $scope.showLoyal = $scope.featureList[x].isActive;
            //        $scope.CustomId = $scope.featureList[x].id;
            //        if ($scope.featureList[x].isActive == true) {
            //            $scope.disLoyalty = false;
            //        }
            //        else {
            //            $scope.disLoyalty = true;
            //        }
            //    }
            //}
            console.log('status', $scope.showSync)
        }, function () {
            $.LoadingOverlay("hide");
        });
    };


    $scope.saveSettings = function () {
        if ($scope.selectedAccount == null || $scope.selectedAccount.id == null) {
            return;
        }
        if ($scope.selectedBranch == null || $scope.selectedBranch.id == null) {
            return;
        }
        $.LoadingOverlay("show");
        toolService.saveCustomSettings($scope.featureList).then(function (response) {
            $.LoadingOverlay("hide");
            toastr.success("Settings saved successfully.");
            $scope.featureList = response.data;
        }, function () {
            $.LoadingOverlay("hide");
        });
    };


    //$scope.updateLoyality = function (branch, index, status) {
    //    if ($scope.selectedAccount.id == null || $scope.selectedAccount.id == "") {
    //        toastr.info("Please select account and try again.");
    //        return;
    //    }
    //    if ($scope.selectedBranch.id == null || $scope.selectedBranch.id == "") {
    //        toastr.info("Please select branch and try again.");
    //        return;
    //    }
    //    $.LoadingOverlay("show");
    //    $scope.list[index].isActive = status;
    //    $scope.list[index].customSettingsGroupId = 4;

    //    toolService.saveCustomSettings($scope.list).then(function (response) {
    //        $.LoadingOverlay("hide");
    //        toastr.success("Settings saved successfully.");
    //        $scope.getBranchList();
    //        $scope.getCustomSettings(branch);
    //        // $scope.list = response.data;
    //    }, function () {
    //        $.LoadingOverlay("hide");
    //    });
    //};

    $scope.updateOfflineStatus = function (branch, status) {

        var str = "";
        if (status == true)
            str = "Are you sure to block online for '" + branch.instanceName + "'?";
        else
            str = "Are you sure to unblock online for '" + branch.instanceName + "'?";
        var r = confirm(str);

        if (r == true) {
            $.LoadingOverlay("show");
            toolService.updateOfflineStatus(branch, status).then(function (response) {
                $.LoadingOverlay("hide");
                toastr.success("Status has been successfully updated.");
                for (var i = 0; i < $scope.list.length; i++) {
                    if ($scope.list[i] == branch) {
                        $scope.list[i] = response.data;
                        break;
                    }
                }
                //for (var i = 0; i < $scope.listTemp.length; i++) {
                //    if ($scope.listTemp[i] == branch) {
                //        $scope.listTemp[i] = response.data;
                //        break;
                //    }
                //}
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    }
    $scope.updateOfflineVersionNo = function (branch, status) {

        var str = "";
        if (status == true)
            str = "Are you sure to upgrade Offline for '" + branch.instanceName + "'?";
        
        var r = confirm(str);

        if (r == true) {
            $.LoadingOverlay("show");
            toolService.updateOfflineVersionNo(branch).then(function (response) {
                $.LoadingOverlay("hide");
                toastr.success("Upgrade successfully..");
                for (var i = 0; i < $scope.list.length; i++) {
                    if ($scope.list[i].instanceId == branch.instanceId) {
                        $scope.list[i] = response.data;
                        break;
                    }
                }
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    }

    //Added by Sarubala on 25-11-17
    $scope.showSmsPopup = function (data) {
        var m = ModalService.showModal({
            "controller": "smsConfigurationCtrl",
            "templateUrl": 'smsConfigurationAlert', 
            "inputs": {
                "branchDetail": data,
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
    };


    $scope.toggleProductDetail = function (obj, data) {
        var row = obj.target.getAttribute("data");
        //$('#chip-wrapper' + row).show();

        if ($('#chip-btn' + row).text() === '+') {

            $('#chip-wrapper' + row).delay(30).fadeToggle();

            $('#chip-btn' + row).text('-');
        }
        else {
            $('#chip-wrapper' + row).slideToggle();
            $('#chip-btn' + row).text('+');
        }

    }; 


   
});