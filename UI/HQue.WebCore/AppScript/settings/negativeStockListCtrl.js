﻿app.controller('negativeStockListCtrl', ['$scope', 'uiGridConstants', '$http', '$interval', '$q', 'toolService', 'toolModel', '$filter', function ($scope, uiGridConstants, $http, $interval, $q, toolService, toolModel, $filter) {

    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
 
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        toolService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;

            $scope.negativeStockReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        toolService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.negativeStockReport = function () {
        $.LoadingOverlay("show");

        toolService.negativeStockList().then(function (response) {
            $scope.data = response.data;    

            var pdfHeader = "";
            if ($scope.instance !== undefined) {
                $scope.pdfHeader = $scope.instance;
            }
            else {
                toolService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo === undefined || $scope.pdfHeader.drugLicenseNo === "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.tinNo === undefined || $scope.pdfHeader.tinNo === "")
                    $scope.pdfHeader.tinNo = "";
                else
                    $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress === undefined || $scope.pdfHeader.fullAddress === "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: "Negative Stock Products.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Negative_Stock_Products.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                    { field: "accountName", title: "Account", width: "100px", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "instanceName", title: "Instance", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "productName", title: "Product", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "stock", title: "Stock", width: "50px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } }
                ],
                dataSource: {
                    data: response.data,
                    schema: {
                        model: {
                            fields: {
                                "accountName": { type: "string" },
                                "instanceName": { type: "string" },
                                "productName": { type: "string" },
                                "stock": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type === "data" && this.columns[ci].attributes.ftype === "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type === "data" && this.columns[ci].attributes.ftype === "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type === "group-footer" || row.type === "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    //End Chng 2

    $scope.negativeStockReport();

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Negative Stock Products", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "TIN No: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
}]);
