﻿app.controller('updateInventoryCtrl', function ($scope, $rootScope, productStockService, productModel, productStockModel, inventorySettingsModel, toastr, $filter, productService, pagerServcie, cacheService, ModalService) {

    var product = productModel;
    $scope.search = product;
    $scope.list = [];
    $scope.newOpenedList = [];
    $scope.productStockList = [];
    $scope.productStockListFull = [];
    var productStock = productStockModel;
    $scope.addProduct = productStock;
    $scope.addProduct.product = null;
    $scope.addProduct.vat = 0;
    $scope.addProduct.gstTotal = 0;
    $scope.addProduct.packageSize = 1;
    $scope.selectedProduct1 = null;
    $scope.enablePrice = false;
    $scope.purchasePricePerUnit = 0;
    $scope.sellPricePerUnit = 0;
    $scope.GSTEnabled = true;
    $scope.isAvailable = false;
    $scope.isAvailableqty = false;
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.minDate = new Date();

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,

    };

    $scope.PopupAddNewProduct = function () {
        var m = ModalService.showModal({
            "controller": "productCreateCtrl",
            "templateUrl": "createProduct",
            "inputs": {
                "mode": "Create",
                "poproductname": '',
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();

        });
    };
    $scope.dayDiff = function (expireDate) {

        if ($scope.openStockTab == true)
        {
            var val = (toDate($scope.openStock.expDate.$viewValue) > $scope.minDate);
            $scope.openStock.expDate.$setValidity("InValidexpDate", val);
            $scope.isFormValid = val;

            $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
            $scope.expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');

            var date2 = new Date($scope.formatString($scope.expireDate));
            var date1 = new Date($scope.formatString($scope.today));

            var timeDiff = date2.getTime() - date1.getTime();
            $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));

            if ($scope.dayDifference < 30) {
                $scope.highlight = "highlight";
                $scope.openStock.expDate.$setValidity("InValidexpDate", false);
                $scope.isFormValid = false;
            }
            else {
                $scope.openStock.expDate.$setValidity("InValidexpDate", true);
                $scope.isFormValid = true;
                $scope.highlight = "";
            }
        }
        else
        {
            var val = (toDate($scope.inventoryDataUpdates.expDateData.$viewValue) > $scope.minDate);
            $scope.inventoryDataUpdates.expDateData.$setValidity("InValidexpDate", val);
            $scope.isFormValid = val;

            $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
            $scope.expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');

            var date2 = new Date($scope.formatString($scope.expireDate));
            var date1 = new Date($scope.formatString($scope.today));

            var timeDiff = date2.getTime() - date1.getTime();
            $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));

            if ($scope.dayDifference < 30) {
                $scope.highlight = "highlight";
                $scope.inventoryDataUpdates.expDateData.$setValidity("InValidexpDate", false);
                $scope.isFormValid = false;
            }
            else {
                $scope.inventoryDataUpdates.expDateData.$setValidity("InValidexpDate", true);
                $scope.isFormValid = true;
                $scope.highlight = "";
            }
        }


       
    };

    function toDate(dateStr) {
        if (dateStr != null) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
    };

    $scope.gstTotalChange = function (contextObj) {
        //if ($scope.addProduct.gstTotal > 0 && $scope.addProduct.gstTotal < 100)
        //{
        //    $scope.addProduct.igst = $scope.addProduct.gstTotal;
        //    $scope.addProduct.cgst = $scope.addProduct.gstTotal / 2;
        //    $scope.addProduct.sgst = $scope.addProduct.gstTotal / 2;
        //}
        //else
        //{
        //    $scope.addProduct.igst = 0;
        //    $scope.addProduct.cgst = 0;
        //    $scope.addProduct.sgst = 0;
        //}
        if (contextObj.gstTotal > 0 && contextObj.gstTotal < 100) {
            contextObj.igst = contextObj.gstTotal;
            contextObj.cgst = contextObj.gstTotal / 2;
            contextObj.sgst = contextObj.gstTotal / 2;
        }
        else {
            contextObj.igst = 0;
            contextObj.cgst = 0;
            contextObj.sgst = 0;
        }
        //added by nandhini
        if (contextObj.gstTotal != null && contextObj.gstTotal != "" && contextObj.gstTotal != undefined) {
               

            if (parseFloat(contextObj.packageSellingPrice) < (parseFloat(contextObj.packagePurchasePrice) + parseFloat(contextObj.packagePurchasePrice * contextObj.gstTotal / 100)))
                    $scope.inventoryDataUpdates.sellingPrice.$setValidity("checkMrpError", false);
                else
                    $scope.inventoryDataUpdates.sellingPrice.$setValidity("checkMrpError", true);

            $scope.checkSellingMrpFull(contextObj.packageSellingPrice, contextObj.packageMRP);
            
        }
    };

    $scope.keydown1 = function (e) {

        if ($scope.openStockTab == true)
        {
            if ($("#expDate").val().length == 2) {
                if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                    $("#expDate").val($("#expDate").val() + "/");
            }
            if ($("#expDate").val().length <= 5) {
                var dt = $("#expDate").val();
                if (($("#expDate").val().length == 5) && (dt.charAt(2) == '/')) {
                    $scope.dayDiff($scope.addProduct.expireDate);
                }
                else {
                    $scope.highlight = "highlight";
                    $scope.openStock.expDate.$setValidity("InValidexpDate", false);
                    $scope.isFormValid = false;
                }

            }
            else {
                $scope.highlight = "highlight";
                $scope.openStock.expDate.$setValidity("InValidexpDate", false);
                $scope.isFormValid = false;
            }
        }
        else
        {
            if ($("#expDateData").val().length == 2) {
                if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                    $("#expDateData").val($("#expDateData").val() + "/");
            }
            if ($("#expDateData").val().length <= 5) {
                var dt = $("#expDateData").val();
                if (($("#expDateData").val().length == 5) && (dt.charAt(2) == '/')) {
                    $scope.dayDiff($scope.addProductFull.expireDate);
                }
                else {
                    $scope.highlight = "highlight";
                    $scope.inventoryDataUpdates.expDateData.$setValidity("InValidexpDate", false);
                    $scope.isFormValid = false;
                }

            }
            else {
                $scope.highlight = "highlight";
                $scope.inventoryDataUpdates.expDateData.$setValidity("InValidexpDate", false);
                $scope.isFormValid = false;
            }
        }
        
    };


    $scope.keydown2 = function (e, pos) {
        if ($("#expDate1" + pos).val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $("#expDate1" + pos).val($("#expDate1" + pos).val() + "/");
        }
        if ($("#expDate1" + pos).val().length <= 5) {
            var dt = $("#expDate1" + pos).val();
            if (($("#expDate1" + pos).val().length == 5) && (dt.charAt(2) == '/')) {

                var val = (toDate($("#expDate1" + pos).val()) > $scope.minDate);
                $scope.productStockList[pos].errorDate = val;
                $scope.isFormValid = !val;

                $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
                $scope.expireDate = $filter('date')($scope.productStockList[pos].expireDate, 'dd/MM/yyyy');

                var date2 = new Date($scope.formatString($scope.expireDate));
                var date1 = new Date($scope.formatString($scope.today));

                var timeDiff = date2.getTime() - date1.getTime();
                $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));

                if ($scope.dayDifference < 30) {
                    $scope.productStockList[pos].highlight = "highlight";
                    $scope.productStockList[pos].errorDate = true;
                    $scope.isFormValid = false;
                }
                else {
                    $scope.productStockList[pos].errorDate = false;
                    $scope.isFormValid = true;
                    $scope.productStockList[pos].highlight = "";
                }
            }
            else {
                $scope.productStockList[pos].highlight = "highlight";
                $scope.productStockList[pos].errorDate = true;
                $scope.isFormValid = false;
            }

        }
        else {
            $scope.productStockListFull[pos].highlight = "highlight";
            $scope.productStockListFull[pos].errorDate = true;
            $scope.isFormValid = false;
        }
    };

    $scope.checkMrp = function (purchase, sell, vat) {

        if (vat != null && vat != undefined) {
            if (vat % 1 != 0) {
                var vatError = $scope.checkDots(vat);
                $scope.openStock.gstTotal.$setValidity("vatError", !vatError);
            }
            else {
                $scope.openStock.gstTotal.$setValidity("vatError", true);
            }

            $scope.purchasePriceCalculate(purchase);
        }

        if (sell != null && sell != undefined) {

            if ($scope.enablePrice) {
                var amt = parseFloat(sell) * 20 / 100;
                $scope.addProduct.packagePurchasePrice = (parseFloat(sell) - parseFloat(amt)).toFixed(2);
                $scope.openStock.purchasePrice.valid = true;
                $scope.purchasePriceCalculate($scope.addProduct.packagePurchasePrice);
                purchase = $scope.addProduct.packagePurchasePrice;
                $scope.purchasePriceCalculate(purchase);
            }

            if (sell % 1 != 0) {
                var sellError = $scope.checkDots(sell);
                $scope.openStock.sellingPrice.$setValidity("sellPriceError", !sellError);
            }
            else {
                $scope.openStock.sellingPrice.$setValidity("sellPriceError", true);
            }
            $scope.sellingPriceCalculate(sell);            

        }
        else {
            $scope.sellPricePerUnit = 0;
        }

        if (purchase != null && purchase != undefined) {
            if (purchase % 1 != 0) {
                var purchaseError = $scope.checkDots(purchase);
                $scope.openStock.purchasePrice.$setValidity("purchasePriceError", !purchaseError);
            }
            else {
                $scope.openStock.purchasePrice.$setValidity("purchasePriceError", true);
            }
            $scope.purchasePriceCalculate(purchase);
        }


        if (parseFloat(sell) < (parseFloat(purchase) + parseFloat(purchase * vat / 100)))
            $scope.openStock.sellingPrice.$setValidity("checkMrpError", false);
        else
            $scope.openStock.sellingPrice.$setValidity("checkMrpError", true);

        //MRP Implementation by Settu
        if (!$scope.enableSelling) {
            $scope.addProduct.packageMRP = $scope.addProduct.packageSellingPrice;
        }
        else {
            if ($scope.addProduct.packageMRP > 0) {
                $scope.checkSellingMrp($scope.addProduct.packageSellingPrice, $scope.addProduct.packageMRP);
            }
        }
    };

    $scope.checkSellingMrp = function (selling, mrp) {

        if ($scope.enableSelling != true)
            return;
        selling = selling != undefined ? parseFloat(selling) : 0;
        mrp = mrp != undefined ? parseFloat(mrp) : 0;
        if (mrp < selling || (mrp == 0)) {
            $scope.openStock.mrp.$setValidity("checkMrpError", false);
        } else {
            $scope.openStock.mrp.$setValidity("checkMrpError", true);
        }
    };

    $scope.checkDots = function (value1) {
        if (value1 != null) {
            if (1 < value1.match(/\./g).length) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    $scope.priceCalculation = function () {
        if (parseFloat($scope.addProduct.packageSize) <= 0) {
            $scope.addProduct.packageSize = 1;
        }
        $scope.sellingPriceCalculate($scope.addProduct.packageSellingPrice);
        $scope.purchasePriceCalculate($scope.addProduct.packagePurchasePrice);
    }

    $scope.sellingPriceCalculate = function (sell) {
        if (parseFloat($scope.addProduct.packageSize) > 0 && parseFloat(sell) > 0) {
            $scope.sellPricePerUnit = (parseFloat(sell) / parseFloat($scope.addProduct.packageSize)).toFixed(2);  //Added by Sarubala on 13-09-17
        }
        else {
            $scope.sellPricePerUnit = 0;
        }
    }


    $scope.purchasePriceCalculate = function (purchase) {
        if (parseFloat($scope.addProduct.packageSize) > 0 && parseFloat(purchase) > 0 && parseFloat(($scope.GSTEnabled ? $scope.addProduct.gstTotal : $scope.addProduct.vat)) >= 0) {
            var tax1 = parseFloat(purchase) * parseFloat(($scope.GSTEnabled ? $scope.addProduct.gstTotal : $scope.addProduct.vat)) / 100;
            var total1 = tax1 + parseFloat(purchase);
            $scope.purchasePricePerUnit = (total1 / parseFloat($scope.addProduct.packageSize)).toFixed(2); //Added by Sarubala on 13-09-17
        }       
        else {
            $scope.purchasePricePerUnit = 0;
        }

    }
  //  $scope.isAvailablequantity = false;
    //$scope.checkEditQuantity = function (quantity) {
    //    if (quantity == 0 || quantity == null || quantity == undefined) {
    //        $scope.isAvailablequantity = true;
    //    }
    //}

    $scope.checkEditMrp = function (purchase, sell, vat, pos) {

        if (vat != null && vat != undefined) {
            if (vat % 1 != 0) {
                var vatError = $scope.checkDots(vat);
                $scope.productStockList[pos].vatError = !vatError;
            }
            else {
                $scope.productStockList[pos].vatError = true;
            }

        }

        if (sell != null && sell != undefined) {
             if ($scope.enablePrice) {
                var amt = parseFloat(sell) * 20 / 100;
                $scope.productStockList[pos].packagePurchasePrice = (parseFloat(sell) - parseFloat(amt)).toFixed(2);
                $scope.openStock.purchasePrice.valid = true;
                $scope.purchasePriceCalculate($scope.productStockList[pos].packagePurchasePrice);
                purchase = $scope.productStockList[pos].packagePurchasePrice;
                $scope.purchasePriceCalculate(purchase);
            }
            if (sell % 1 != 0) {
                var sellError = $scope.checkDots(sell);
                $scope.productStockList[pos].sellPriceError = !sellError;
            }
            else {
                $scope.productStockList[pos].sellPriceError = true;
            }

        }
        
        if (purchase == 0 || purchase == null || purchase == undefined) {
            $scope.isAvailable = true;
        }
        else {
            $scope.isAvailable = false;
        }
       // console.log('data', $scope.isAvailable);

        if (purchase != null && purchase != undefined) {
            if (purchase % 1 != 0) {
                var purchaseError = $scope.checkDots(purchase);
                $scope.productStockList[pos].purchasePriceError = !purchaseError;
            }
            else {
                $scope.productStockList[pos].purchasePriceError = true;
            }
            $scope.purchasePriceCalculate(purchase);
        }

        if (parseFloat(sell) < (parseFloat(purchase) + parseFloat(purchase * vat / 100)))
            $scope.productStockList[pos].mrpError = true;
        else
            $scope.productStockList[pos].mrpError = false;

        //MRP Implementation by Settu
        if (!$scope.enableSelling) {
            $scope.productStockList[pos].packageMRP = $scope.productStockList[pos].packageSellingPrice;
        }
        else {
            if ($scope.productStockList[pos].packageMRP > 0) {
                $scope.checkEditSellingMrp($scope.productStockList[pos].packageSellingPrice, $scope.productStockList[pos].packageMRP, pos);
            }
        }
    };

    $scope.checkEditQty = function (qty, pos) {
        if (qty == 0 || qty == null || qty == undefined) {
            $scope.isAvailableqty = true;
        }
        else {
            $scope.isAvailableqty = false;
        }
    }

    $scope.checkEditSellingMrp = function (sell, mrp, pos) {

        if (parseFloat(mrp) < parseFloat(sell))
            $scope.productStockList[pos].sellingMrpError = true;
        else
            $scope.productStockList[pos].sellingMrpError = false;
    };

    $scope.checkAllfields = function (ind) {

        if (ind > 1) {
            if ($scope.addProduct.product == undefined || $scope.addProduct.product == null || $scope.addProduct.product.id == undefined || $scope.addProduct.product.id == null) {
                var ele = document.getElementById("productName1");
                ele.focus();
                return false;
            }
        }

        if (ind > 2) {
            if ($scope.addProduct.batchNo == undefined || $scope.addProduct.batchNo == null) {
                var ele = document.getElementById("batchNo");
                ele.focus();
                return false;
            }
        }

        if (ind > 3) {
            if ($scope.addProduct.expireDate == undefined || $scope.addProduct.expireDate == null) {
                var ele = document.getElementById("expDate");
                ele.focus();
                return false;
            }
        }

        //if (ind > 4) {
        //    if ($scope.addProduct.vat == undefined || $scope.addProduct.vat == null) {
        //        var ele = document.getElementById("vat");
        //        ele.focus();
        //        return false;
        //    }
        //}

        //if (ind > 5) {
        //    if ($scope.addProduct.hsnCode == undefined || $scope.addProduct.hsnCode == null) {
        //        var ele = document.getElementById("hsnCode");
        //        ele.focus();
        //        return false;
        //    }
        //}

        if (ind > 6) {
            if ($scope.addProduct.gstTotal == undefined || $scope.addProduct.gstTotal == null || $scope.addProduct.gstTotal > 100 || $scope.addProduct.gstTotal == "") {
                var ele = document.getElementById("gstTotal");
                ele.focus();
                return false;
            }
        }

        if (ind > 7) {
            if ($scope.addProduct.packageSize == undefined || $scope.addProduct.packageSize == null) {
                var ele = document.getElementById("packSize");
                ele.focus();
                return false;
            }
        }

        if (ind > 8) {
            if (!$scope.enablePrice) {
                if ($scope.addProduct.packagePurchasePrice == undefined || $scope.addProduct.packagePurchasePrice == null || $scope.addProduct.packagePurchasePrice == '0') {
                    var ele = document.getElementById("purchasePrice");
                    ele.focus();
                    return false;
                }
            }
        }


        if (ind > 9) {
            if ($scope.addProduct.packageSellingPrice == undefined || $scope.addProduct.packageSellingPrice == null || $scope.addProduct.packageSellingPrice == '0') {
                var ele = document.getElementById("sellingPrice");
                ele.focus();
                return false;
            }
        }
        //MRP Implementation by Settu
        if (ind > 10) {
            if ($scope.addProduct.packageMRP == undefined || $scope.addProduct.packageMRP == null || $scope.addProduct.packageMRP == '0') {
                var ele = document.getElementById("mrp");
                ele.focus();
                return false;
            }
        }
        if (ind > 11) {
            if ($scope.addProduct.packageQty == undefined || $scope.addProduct.packageQty == null || $scope.addProduct.packageQty == '0') {
                var ele = document.getElementById("qtyinUnits");
                ele.focus();
                return false;
            }
        }
    };


    $scope.formatString = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    }

    $scope.submit = function () {
        if ($scope.isFormValid && $scope.openStock.$valid && $scope.addProduct.product.id != null && $scope.addProduct.product.id != undefined) {
            $scope.addProducts();
        }
    };

    $scope.onProductSelect1 = function () {

        var tempProduct = $scope.addProduct.product;
        $scope.addProduct = {};
        $scope.openStock.$setPristine();
        //$scope.addProduct.vat = 5;
        $scope.addProduct.vat = 0;
        $scope.addProduct.gstTotal = 0;
        $scope.addProduct.packageSize = 1;        
        $scope.purchasePricePerUnit = 0;
        $scope.sellPricePerUnit = 0;
        $scope.addProduct.product = tempProduct;

        $scope.currentStock = 0;
        if (parseFloat($scope.addProduct.product.totalstock) > 0) {
            $scope.currentStock = $scope.addProduct.product.totalstock;
        }
        else {
            $scope.currentStock = 0
        }

        productStockService.productAllBatch($scope.addProduct.product.id).then(function (response) {
            $scope.batchList1 = response.data;
            if ($scope.batchList1.length > 0) {
                if ($scope.batchList1.length == 1 && ($scope.batchList1[0].id == undefined || $scope.batchList1[0].id == null))
                {
                    cacheService.set('selectedProduct1', $scope.batchList1[0]);
                    $scope.productSelection();
                }
                else {
                    $scope.ProductDetails();
                }
            }

        }, function () {
        });        

        var ele = document.getElementById("batchNo");
        ele.focus();
    };


    $scope.ProductDetails = function () {        
        cacheService.set('selectedProduct1', $scope.addProduct.product)
        var m = ModalService.showModal({
            controller: "updateBatchDetailsCtrl",
            templateUrl: 'updateProductDetails'
            , inputs: {
                productId: $scope.addProduct.product.id,
                productName: $scope.addProduct.product.name                
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
        return false;
    };

    $rootScope.$on("productDetail", function (data) {
        if ($scope.openStockTab == true)
        {
            $scope.productSelection();
        }
        else
        {
            $scope.productSelectionFull();
        }
       
    });

    $rootScope.$on("productDetailCancel", function (data) {

        if ($scope.openStockTab == true) {
           
            var ele = document.getElementById("batchNo");
            ele.focus();
        }
        else {

            var ele = document.getElementById("batchNoFull");
            ele.focus();
        }
        
    });

    $scope.productSelection = function () {
        var product1 = cacheService.get('selectedProduct1');
               
        if (product1 != null) {
            product1.barcodeProfileId = null;
            product1.purchaseBarcode = null;
            if (product1.id != null && product1.id != undefined) {
                $scope.addProduct = product1;
                var ngst = $filter("filter")($scope.taxValuesList, { "tax": product1.gstTotal }, true);

                if (ngst.length == 0)
                {
                    $scope.addProduct.gstTotal = null;
                    $scope.gstTotalChange($scope.addProduct);
                }
                if ($scope.addProduct.gstTotal == 0) {
                    $scope.addProduct.gstTotal = 0 + "";
                }
                $scope.addProduct.packageSize = parseFloat($scope.addProduct.packageSize || 1);
                $scope.addProduct.packageSellingPrice = (parseFloat($scope.addProduct.sellingPrice) * parseFloat($scope.addProduct.packageSize)).toFixed(2); //Added by Sarubala on 13-09-17
                //MRP Implementation by Settu
                $scope.addProduct.packageMRP = (parseFloat($scope.addProduct.mrp) * parseFloat($scope.addProduct.packageSize)).toFixed(2); //Added by Sarubala on 13-09-17
                $scope.dayDiff($scope.addProduct.expireDate);

                $scope.priceCalculation();

                if (parseFloat($scope.addProduct.packageSellingPrice) < (parseFloat($scope.addProduct.packagePurchasePrice) + parseFloat($scope.addProduct.packagePurchasePrice * ($scope.GSTEnabled ?  $scope.addProduct.gstTotal : $scope.addProduct.vat) / 100)))
                    $scope.openStock.sellingPrice.$setValidity("checkMrpError", false);
                else
                    $scope.openStock.sellingPrice.$setValidity("checkMrpError", true);

                //MRP Implementation by Settu
                $scope.checkSellingMrp($scope.addProduct.packageSellingPrice, $scope.addProduct.packageMRP);
            }
            else {
                $scope.addProduct.hsnCode = product1.product.hsnCode;
                $scope.addProduct.packageSize = product1.product.packageSize;
                var ngst = $filter("filter")($scope.taxValuesList, { "tax": product1.gstTotal }, true);

                if (ngst.length == 0) {
                    $scope.addProduct.gstTotal = null;
                    $scope.addProduct.igst = null;
                    $scope.addProduct.cgst = null;
                    $scope.addProduct.sgst = null;
                }
                else
                    {
                        $scope.addProduct.gstTotal = product1.gstTotal;
                        $scope.addProduct.igst = product1.igst;
                        $scope.addProduct.cgst = product1.cgst;
                        $scope.addProduct.sgst = product1.sgst;
                }
            }
        }       
        
        var ele = document.getElementById("batchNo");
        ele.focus();
    };

    $scope.productSelected = function () {
        $scope.selectedProduct;
    }

    $scope.$on("update_getValue", function (event, value) {
        $scope.search.name = value;
    });
    $scope.productStockSearch = function (productName, obj) {
        $.LoadingOverlay("show");

        if (productName != undefined && productName != null) {
            $scope.search.name = productName;
            if ($scope.selectedProduct) {
                $scope.search.id = $scope.selectedProduct.originalObject.product.id;
            }
        }
        else if ($scope.selectedProduct != null) {
            $scope.search.name = $scope.selectedProduct.title;
            $scope.search.id = $scope.selectedProduct.originalObject.product.id;
        }
        else {
            $scope.search.name = null;
        }
        productStockService.getInventory($scope.search).then(function (response) {
            $scope.list = response.data;

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.updateStock = function (item) {
        if (!confirm("Are you sure, you want to update stock? "))
            return;
        $.LoadingOverlay("show");
        productStockService.updateInventory(item).then(function (response) {
            toastr.success('Stock updated successfully');
            $scope.productStockSearch(null);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }


    $scope.getInventorySettings = function () {
        var inventorySettings = inventorySettingsModel;
        $scope.inventorySetting = inventorySettings;
        editInventorySettings();
    };


    $scope.inventorySettingUpdate = function () {

        $.LoadingOverlay("show");
        productStockService.inventorySetting($scope.inventorySetting).then(function (response) {
            toastr.success('No of Days updated successfully');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    function editInventorySettings() {
        productStockService.editInventorySettings().then(function (response) {
            $scope.inventorySetting = response.data;

        });
    };

    $scope.openStockTab = true;
    $scope.updateStockTab = false;
    $scope.openStockListTab = false;
    $scope.updateDataTab = false; // Added Gavaskar 08-02-2017


    $scope.openStockList = function () {
        $scope.openStockTab = false;
        $scope.updateStockTab = false;
        $scope.openStockListTab = true;
        $scope.updateDataTab = false; // Added Gavaskar 08-02-2017
        $scope.selectedProduct1 = null;
        document.getElementById("productName0_value").value = "";
        $scope.openingHistory();

    }

    $scope.openingHistory = function () {
        $.LoadingOverlay("show")
        var search = null;
        if ($scope.selectedProduct1 != null && $scope.selectedProduct1.originalObject.product.id != null) {
            search = $scope.selectedProduct1.originalObject.product.id;
        }
        else {
            search = null;
        }
        $scope.openingStockTotal = 0;
        productStockService.getNewOpenedInventory(search).then(function (response) {
            $scope.newOpenedList = response.data;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            for (var x = 0; x < $scope.newOpenedList.length; x++) {
                if (parseFloat($scope.newOpenedList[x].purchasePrice) > 0) {
                    var tmp = parseFloat($scope.newOpenedList[x].newStockQty) * parseFloat($scope.newOpenedList[x].purchasePrice);
                    $scope.openingStockTotal += parseFloat(tmp);
                }
            }
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    function pageSearch() {
        $.LoadingOverlay("show");
        var search = null;
        if ($scope.selectedProduct1 != null && $scope.selectedProduct1.originalObject.product.id != null) {
            search = $scope.selectedProduct1.originalObject.product.id;
        }
        else {
            search = null;
        }
        productStockService.getNewOpenedInventory(search).then(function (response) {
            $scope.newOpenedList = response.data;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.updateType = function () {
        $scope.openStockTab = false;
        $scope.updateStockTab = true;
        $scope.openStockListTab = false;
        $scope.updateDataTab = false; // Added Gavaskar 08-02-2017
        $scope.selectedProduct = null;
    }

    $scope.openStock1 = function () {
        $scope.openStockTab = true;
        $scope.updateStockTab = false;
        $scope.openStockListTab = false;
        $scope.updateDataTab = false; // Added Gavaskar 08-02-2017
        $scope.selectedProduct = null;
        $scope.searchStr = null;
        $scope.productStockList = [];
        $scope.addProduct = {};
        //$scope.addProduct.vat = 5;
        $scope.addProduct.vat = 0;
        $scope.addProduct.gstTotal = 0;
        $scope.addProduct.packageSize = 1;
        $scope.isEdit = 0;
        $scope.enablePrice = false;
        $scope.addProduct.product = null;
        $scope.openStock.$setPristine();
        $scope.currentStock = 0;
        $scope.purchasePricePerUnit = 0;
        $scope.sellPricePerUnit = 0;
        var ele = document.getElementById("productName1");
        ele.focus();

    }

    // Added Gavaskar 08-02-2017 Start 

    $scope.inventoryData = function () {
        $scope.openStockTab = false;
        $scope.updateStockTab = false;
        $scope.openStockListTab = false;
        $scope.updateDataTab = true;
        $scope.selectedProduct = null;
        $scope.productStockListFull = [];
        $scope.addProductFull = {};
        $scope.isEdit = 0;
        $scope.addProductFull.product = null;
        $scope.inventoryDataUpdates.$setPristine();
        var ele = document.getElementById("productNamefull");
        ele.focus();
    }

    // Added Gavaskar 08-02-2017 End 

    $scope.isEdit = 0;
    $scope.editProduct = function (item) {
        $scope.isAvailable == false;
        $scope.isAvailableqty == false;
        item.gstTotal = "" + item.gstTotal;
        item.isEdit = true;
        $scope.isEdit += 1;
    }

    //$scope.availablechange = function (item) {
    //    if (item.packagePurchasePrice == null || stock.packagePurchasePrice == 0) {
    //        $scope.isAvailable = true;
    //    }
    //}
    $scope.saveProduct = function (item, pos1, valid1) {
       
      
        //MRP Implementation by Settu
        if (item.isEdit && valid1 && !item.mrpError && !item.sellingMrpError && !item.errorDate && item.vatError && item.sellPriceError && item.purchasePriceError) {
            item.isEdit = false;

            item.stock = parseFloat(item.packageQty);
            item.newStockQty = parseFloat(item.packageQty);
            var tax1 = parseFloat(item.packagePurchasePrice) * parseFloat(($scope.GSTEnabled ? item.gstTotal : item.vat)) / 100;
            var total1 = tax1 + parseFloat(item.packagePurchasePrice);
            item.purchasePrice = total1 / parseFloat(item.packageSize);
            item.sellingPrice = parseFloat(item.packageSellingPrice) / parseFloat(item.packageSize);
            item.mrp = parseFloat(item.packageMRP) / parseFloat(item.packageSize);

            $scope.isEdit -= 1;
            //$scope.addProduct.vat = 5;
            $scope.addProduct.packageSize = 1;
            var ele = document.getElementById("productName1");
            ele.focus();
        }
    }

    $scope.removeProduct = function (item, pos1) {

        if ($scope.openStockTab == true)
        {
            if (!confirm("Are you sure, Do you want to delete ? ")) {
                //$scope.addProduct.vat = 5;
                $scope.addProduct.vat = 0;
                $scope.addProduct.gstTotal = 0;
                $scope.addProduct.packageSize = 1;
                var ele = document.getElementById("productName1");
                ele.focus();
                return;
            }
            if (item.isEdit == true) {
                $scope.isEdit -= 1;
            }
            $scope.productStockList.splice(pos1, 1);
            //$scope.addProduct.vat = 5;
            $scope.addProduct.vat = 0;
            $scope.addProduct.gstTotal = 0;
            $scope.addProduct.packageSize = 1;
            var ele = document.getElementById("productName1");
            ele.focus();
        }
        else
        {
            if (!confirm("Are you sure, Do you want to delete ? ")) {
                var ele = document.getElementById("productNamefull");
                ele.focus();
                return;
            }
            if (item.isEdit == true) {
                $scope.isEdit -= 1;
            }
            $scope.productStockListFull.splice(pos1, 1);
            var ele = document.getElementById("productNamefull");
            ele.focus();
        }
        
    }

    $scope.checkSize = function (val1, pos1) {
        if (parseFloat(val1) <= 0) {
            $scope.productStockList[pos1].packageSize = 1;
        }
    }

    $scope.checkId = function () {
        if ($scope.addProduct.product.id == undefined || $scope.addProduct.product.id == null) {
            $scope.openStock.productName1.$setValidity("productName", false);            
        }
        else {
            $scope.openStock.productName1.$setValidity("productName", true);
        }        
    }

    $scope.addProducts = function () {
        $scope.addProduct.stock = parseFloat($scope.addProduct.packageQty);
        $scope.addProduct.newStockQty = parseFloat($scope.addProduct.packageQty);
        if ($scope.addProduct.vat == '' || $scope.addProduct.vat == null || $scope.addProduct.vat == undefined)
            $scope.addProduct.vat = 0;
        if ($scope.addProduct.gstTotal == '' || $scope.addProduct.gstTotal == null || $scope.addProduct.gstTotal == undefined)
            $scope.addProduct.gstTotal = 0;
        var tax1 = parseFloat($scope.addProduct.packagePurchasePrice) * parseFloat(($scope.GSTEnabled ? $scope.addProduct.gstTotal : $scope.addProduct.vat)) / 100;
        var total1 = tax1 + parseFloat($scope.addProduct.packagePurchasePrice);
        $scope.addProduct.purchasePrice = total1 / parseFloat($scope.addProduct.packageSize);
        $scope.addProduct.sellingPrice = parseFloat($scope.addProduct.packageSellingPrice) / parseFloat($scope.addProduct.packageSize);
        //MRP Implementation by Settu
        $scope.addProduct.mrp = parseFloat($scope.addProduct.packageMRP) / parseFloat($scope.addProduct.packageSize);
        $scope.addProduct.isEdit = false;
        $scope.addProduct.mrpError = false;
        $scope.addProduct.errorDate = false;
        $scope.addProduct.vatError = true;
        $scope.addProduct.sellPriceError = true;
        $scope.addProduct.purchasePriceError = true;

        
        if ($scope.addProduct.product.accountId === null || $scope.addProduct.product.accountId === undefined || $scope.addProduct.product.accountId === "") {
            $scope.addProduct.product.productMasterID = $scope.addProduct.product.id;
            $scope.addProduct.product.manufacturer = $scope.addProduct.product.manufacturer;
            $scope.addProduct.product.schedule = $scope.addProduct.product.schedule;
            $scope.addProduct.product.category = $scope.addProduct.product.category;
            $scope.addProduct.product.genericName = $scope.addProduct.product.genericName;
            $scope.addProduct.product.packing = $scope.addProduct.product.packing;
        }
        else {
            $scope.addProduct.product.productMasterID = null;
        }

        var item1 = $scope.addProduct;

        $scope.productStockList.push(angular.copy(item1));
        $scope.addProduct = {};
        $scope.openStock.$setPristine();
        //$scope.addProduct.vat = 5;
        $scope.addProduct.vat = 0;
        $scope.addProduct.gstTotal = 0;
        $scope.addProduct.packageSize = 1;
        $scope.currentStock = 0;
        $scope.purchasePricePerUnit = 0;
        $scope.sellPricePerUnit = 0;
        var ele = document.getElementById("productName1");
        ele.focus();
    };

    $scope.save = function () {
        $scope.temp = [];
        $.LoadingOverlay("show");

        for (var x = 0; x < $scope.productStockList.length; x++) {
            $scope.temp.push($scope.productStockList[x]);
        }
        productStockService.saveNewStock($scope.temp).then(function (response) {
            $scope.productStockList = response.data;
            $scope.addProduct = {};
            $scope.addProduct.product = null;
            $scope.productStockList = [];
            $scope.openStock.$setPristine();
            $scope.openStockTab = true;
            toastr.success('Stock updated successfully');
            //$scope.addProduct.vat = 5;
            $scope.addProduct.packageSize = 1;
            $scope.isEdit = 0;
            $scope.enablePrice = false;
            $scope.purchasePricePerUnit = 0;
            $scope.sellPricePerUnit = 0;
            $scope.currentStock = 0;
            var ele = document.getElementById("productName1");
            ele.focus();
            $.LoadingOverlay("hide");

        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    }
    //Added by Bikas
    //$scope.getProductszero = function (val)
    //{
    //    return productService.getProductszero(val).then(function (response) {
    //        var flags = [], output = [], l = response.data.length, i;
    //        for (i = 0; i < l; i++) {
    //            if (flags[$filter('uppercase')(response.data[i].name)]) continue;
    //            flags[$filter('uppercase')(response.data[i].name)] = true;
    //            output.push(response.data[i]);
    //        }
    //        return output.map(function (item) {
    //            return item;
    //        });
    //    });
    //}
    $scope.getProducts = function (val) {

        return productService.drugFilterData(val).then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }

            return output.map(function (item) {
                return item;
            });
        });
    }

    // Added Gavaskar 09-02-2017 Inventory Full Data Update Start

    $scope.getProductsFull = function (val) {

        return productService.drugFilterDataInventoryUpdate(val).then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }

            return output.map(function (item) {
                return item;
            });
        });
    }

    $scope.onProductSelectFull = function () {

        var tempProduct = $scope.addProductFull.product;
        $scope.addProductFull = {};
        $scope.inventoryDataUpdates.$setPristine();
        $scope.addProductFull.product = tempProduct;

        productStockService.getActiveBatch($scope.addProductFull.product.id).then(function (response) {
            $scope.batchList1 = response.data;
            if ($scope.batchList1.length > 0) {
                $scope.ProductDetailsFull();
            }

        }, function () {
        });

        var ele = document.getElementById("batchNoFull");
        ele.focus();
    };

    $scope.checkIdFull = function () {
        if ($scope.addProductFull.product.id == undefined || $scope.addProductFull.product.id == null) {
            $scope.inventoryDataUpdates.productName1.$setValidity("productName", false);
        }
        else {
            $scope.inventoryDataUpdates.productName1.$setValidity("productName", true);
        }
    }

    ///

    $scope.ProductDetailsFull = function () {
        cacheService.set('selectedProduct1', $scope.addProductFull.product)
        var m = ModalService.showModal({
            controller: "updateBatchDetailsCtrl",
            templateUrl: 'updateProductDetails'
            , inputs: {
                productId: $scope.addProductFull.product.id,
                productName: $scope.addProductFull.product.name
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
        return false;
    };


    $scope.productSelectionFull = function () {
        var product1 = cacheService.get('selectedProduct1');

        if (product1 != null) {
            $scope.addProductFull = product1;
            var ngst = $filter("filter")($scope.taxValuesList, { "tax": product1.gstTotal }, true);

            if (ngst.length == 0) {
                $scope.addProductFull.gstTotal = null;
                $scope.gstTotalChange($scope.addProductFull);
            }
            if ($scope.addProductFull.gstTotal == 0) {
                $scope.addProductFull.gstTotal = 0 + "";
            }
            $scope.addProductFull.packageSize = parseFloat($scope.addProductFull.packageSize || 1);
            $scope.addProductFull.packageSellingPrice = (parseFloat($scope.addProductFull.sellingPrice) * parseFloat($scope.addProductFull.packageSize)).toFixed(2); //Added by Sarubala on 13-09-17
            //MRP Implementation by Settu
            $scope.addProductFull.packageMRP = (parseFloat($scope.addProductFull.mrp) * parseFloat($scope.addProductFull.packageSize)).toFixed(2); //Added by Sarubala on 13-09-17
            $scope.dayDiff($scope.addProductFull.expireDate);
            $scope.priceCalculationFull();

            if (parseFloat($scope.addProductFull.packageSellingPrice) < (parseFloat($scope.addProductFull.packagePurchasePrice) + parseFloat($scope.addProductFull.packagePurchasePrice * ($scope.GSTEnabled ? $scope.addProductFull.gstTotal : $scope.addProductFull.vat) / 100)))
                $scope.inventoryDataUpdates.sellingPrice.$setValidity("checkMrpError", false);
            else
                $scope.inventoryDataUpdates.sellingPrice.$setValidity("checkMrpError", true);

            $scope.checkSellingMrpFull($scope.addProductFull.packageSellingPrice, $scope.addProductFull.packageMRP);
        }

        var ele = document.getElementById("batchNoFull");
        ele.focus();
    };

    $scope.priceCalculationFull = function () {
        if (parseFloat($scope.addProductFull.packageSize) <= 0) {
            $scope.addProductFull.packageSize = 1;
        }
    }

    $scope.checkAllfieldsFull = function (ind) {

        if (ind > 1) {
            if ($scope.addProductFull.product == undefined || $scope.addProductFull.product == null || $scope.addProductFull.product.id == undefined || $scope.addProductFull.product.id == null) {
                var ele = document.getElementById("productNamefull");
                ele.focus();
                return false;
            }
        }

        if (ind > 2) {
            if ($scope.addProductFull.batchNo == undefined || $scope.addProductFull.batchNo == null) {
                var ele = document.getElementById("batchNoFull");
                ele.focus();
                return false;
            }
        }

        if (ind > 3) {
            if ($scope.addProductFull.expireDate == undefined || $scope.addProductFull.expireDate == null) {
                var ele = document.getElementById("expDateData");
                ele.focus();
                return false;
            }
        }

        //if (ind > 4) {
        //    if ($scope.addProductFull.vat == undefined || $scope.addProductFull.vat == null) {
        //        var ele = document.getElementById("Idvat");
        //        ele.focus();
        //        return false;
        //    }
        //}
        //GST related fields starts
        //if (ind > 5) {
        //    if ($scope.addProductFull.hsnCode == undefined || $scope.addProductFull.hsnCode == null || $scope.addProductFull.hsnCode == "") {
        //        var ele = document.getElementById("IdhsnCode");
        //        ele.focus();
        //        return false;
        //    }
        //}
        if (ind > 6) {
            if ($scope.addProductFull.gstTotal == undefined || $scope.addProductFull.gstTotal == null || $scope.addProductFull.gstTotal == "") {
                var ele = document.getElementById("IdgstTotal");
                ele.focus();
                return false;
            }
        }
        
//GST related fields ends
        if (ind > 7) {
            if ($scope.addProductFull.packageSize == undefined || $scope.addProductFull.packageSize == null) {
                var ele = document.getElementById("IdpackSize");
                ele.focus();
                return false;
            }
        }

        if (ind > 8) {
            if (!$scope.enablePrice) {
                if ($scope.addProductFull.packagePurchasePrice == undefined || $scope.addProductFull.packagePurchasePrice == null) {
                    var ele = document.getElementById("IdpurchasePrice");
                    ele.focus();
                    return false;
                }
            }
        }


        if (ind > 9) {
            if ($scope.addProductFull.packageSellingPrice == undefined || $scope.addProductFull.packageSellingPrice == null) {
                var ele = document.getElementById("IdsellingPrice");
                ele.focus();
                return false;
            }
        }

        //MRP Implementation by Settu
        if (ind > 10) {
            if ($scope.addProductFull.packageSellingPrice == undefined || $scope.addProductFull.packageSellingPrice == null) {
                var ele = document.getElementById("IdMrp");
                ele.focus();
                return false;
            }
        }

    };

    $scope.checkMrpFull = function (purchase, sell, vat) {

        if (vat != null && vat != undefined) {
            if (vat % 1 != 0) {
                var vatError = $scope.checkDots(vat);
                $scope.inventoryDataUpdates.gstTotal.$setValidity("vatError", !vatError);
              
            }
            else {
                $scope.inventoryDataUpdates.gstTotal.$setValidity("vatError", true);
            }
          
        }

        if (sell != null && sell != undefined) {

            if ($scope.enablePrice) {
                var amt = parseFloat(sell) * 20 / 100;
                $scope.addProductFull.packagePurchasePrice = (parseFloat(sell) - parseFloat(amt)).toFixed(2);
                $scope.inventoryDataUpdates.purchasePrice.valid = true;
                purchase = $scope.addProductFull.packagePurchasePrice;
            }

            if (sell % 1 != 0) {
                var sellError = $scope.checkDots(sell);
                $scope.inventoryDataUpdates.sellingPrice.$setValidity("sellPriceError", !sellError);
            }
            else {
                $scope.inventoryDataUpdates.sellingPrice.$setValidity("sellPriceError", true);
            }

        }

        if (purchase != null && purchase != undefined) {
            if (purchase % 1 != 0) {
                var purchaseError = $scope.checkDots(purchase);
                $scope.inventoryDataUpdates.purchasePrice.$setValidity("purchasePriceError", !purchaseError);
            }
            else {
                $scope.inventoryDataUpdates.purchasePrice.$setValidity("purchasePriceError", true);
            }
        }


        if (parseFloat(sell) < (parseFloat(purchase) + parseFloat(purchase * vat / 100)))
            $scope.inventoryDataUpdates.sellingPrice.$setValidity("checkMrpError", false);
        else
            $scope.inventoryDataUpdates.sellingPrice.$setValidity("checkMrpError", true);

        //MRP Implementation by Settu
        if (!$scope.enableSelling) {
            $scope.addProductFull.packageMRP = $scope.addProductFull.packageSellingPrice;
        }
        else {
            if ($scope.addProductFull.packageMRP > 0) {
                $scope.checkSellingMrpFull($scope.addProductFull.packageSellingPrice, $scope.addProductFull.packageMRP);
            }
        }
    };

    $scope.checkSellingMrpFull = function (selling, mrp) {
        if ($scope.enableSelling != true)
            return;
        selling = selling != undefined ? parseFloat(selling) : 0;
        mrp = mrp != undefined ? parseFloat(mrp) : 0;
        if (mrp < selling || (mrp == 0)) {
            $scope.inventoryDataUpdates.mrp.$setValidity("checkMrpError", false);
        } else {
            $scope.inventoryDataUpdates.mrp.$setValidity("checkMrpError", true);
        }        
    };

    $scope.checkEditMrpFull = function (purchase, sell, vat, pos) {

        if (vat != null && vat != undefined) {
            if (vat % 1 != 0) {
                var vatError = $scope.checkDots(vat);
                $scope.productStockListFull[pos].vatError = !vatError;
            }
            else {
                $scope.productStockListFull[pos].vatError = true;
            }

        }

        if (sell != null && sell != undefined) {
            if (sell % 1 != 0) {
                var sellError = $scope.checkDots(sell);
                $scope.productStockListFull[pos].sellPriceError = !sellError;
            }
            else {
                $scope.productStockListFull[pos].sellPriceError = true;
            }

        }

        if (purchase != null && purchase != undefined) {
            if (purchase % 1 != 0) {
                var purchaseError = $scope.checkDots(purchase);
                $scope.productStockListFull[pos].purchasePriceError = !purchaseError;
            }
            else {
                $scope.productStockListFull[pos].purchasePriceError = true;
            }

        }

        if (parseFloat(sell) < (parseFloat(purchase) + parseFloat(purchase * vat / 100)))
            $scope.productStockListFull[pos].mrpError = true;
        else
            $scope.productStockListFull[pos].mrpError = false;

        //MRP Implementation by Settu
        if (!$scope.enableSelling) {
            $scope.productStockListFull[pos].packageMRP = $scope.productStockListFull[pos].packageSellingPrice;
        }
        else {
            if ($scope.productStockListFull[pos].packageMRP > 0) {
                $scope.checkEditSellingMrpFull($scope.productStockListFull[pos].packageSellingPrice, $scope.productStockListFull[pos].packageMRP, pos);
            }
        }
    };

    $scope.checkEditSellingMrpFull = function (sell, mrp, pos) {
        if (parseFloat(mrp) < parseFloat(sell))
            $scope.productStockListFull[pos].sellingMrpError = true;
        else
            $scope.productStockListFull[pos].sellingMrpError = false;
    };

    $scope.addProductsFull = function () {
       
        var tax1 = parseFloat($scope.addProductFull.packagePurchasePrice) * parseFloat(($scope.GSTEnabled ? $scope.addProductFull.gstTotal : $scope.addProductFull.vat)) / 100;
        var total1 = tax1 + parseFloat($scope.addProductFull.packagePurchasePrice);
        $scope.addProductFull.purchasePrice = total1 / parseFloat($scope.addProductFull.packageSize);
        $scope.addProductFull.sellingPrice = parseFloat($scope.addProductFull.packageSellingPrice) / parseFloat($scope.addProductFull.packageSize);
        $scope.addProductFull.mrp = parseFloat($scope.addProductFull.packageMRP) / parseFloat($scope.addProductFull.packageSize);
        $scope.addProductFull.isEdit = false;
        $scope.addProductFull.mrpError = false;
        $scope.addProductFull.errorDate = false;
        $scope.addProductFull.vatError = true;

        $scope.addProductFull.igstError = true;
        $scope.addProductFull.cgstError = true;
        $scope.addProductFull.sgstError = true;
        $scope.addProductFull.gstTotalError = true;

        $scope.addProductFull.sellPriceError = true;
        $scope.addProductFull.purchasePriceError = true;

        if ($scope.addProductFull.product.totalstock === -1) {
            $scope.addProductFull.product.productMasterID = $scope.addProductFull.product.id;
        }
        else {
            $scope.addProductFull.product.productMasterID = null;
        }

        var item1 = $scope.addProductFull;

        $scope.productStockListFull.push(angular.copy(item1));
        $scope.addProductFull = {};
        $scope.inventoryDataUpdates.$setPristine();
        var ele = document.getElementById("productNamefull");
        ele.focus();
    };

    $scope.inventoryDataUpdate = function () {
        $scope.temp = [];
        $.LoadingOverlay("show");

        for (var x = 0; x < $scope.productStockListFull.length; x++) {
            $scope.temp.push($scope.productStockListFull[x]);
        }
        productStockService.inventoryDataUpdate($scope.temp).then(function (response) {
            $scope.productStockListFull = response.data;
            $scope.addProductFull = {};
            $scope.addProductFull.product = null;
            $scope.productStockListFull = [];
            $scope.inventoryDataUpdates.$setPristine();
            $scope.openStockTab = false;
            $scope.updateStockTab = false;
            $scope.openStockListTab = false;
            $scope.updateDataTab = true;
            $scope.selectedProduct = null;
            toastr.success('Inventory data updated successfully');
            $scope.isEdit = 0;
            var ele = document.getElementById("productNamefull");
            ele.focus();

            $.LoadingOverlay("hide");

        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    }

    $scope.submitFull = function () {
        if ($scope.inventoryDataUpdates.$valid && $scope.addProductFull.product.id != null && $scope.addProductFull.product.id != undefined && ($scope.addProductFull.gstTotal > 0 && $scope.addProductFull.gstTotal <= 100)) {
            $scope.addProductsFull();

            var ele = document.getElementById("productNamefull");
            ele.focus();
        }
    };

    // Added Gavaskar 09-02-2017 Inventory Full Data Update End

    //MRP Implementation by Settu
    $scope.getEnableSelling = function () {
        productStockService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.getEnableSelling();

    $scope.init = function (isAdjustment) {

        if (isAdjustment == "False") {
           // $scope.getTaxValues();
            $scope.openStockTab = false;
            $scope.updateStockTab = false;
            $scope.openStockListTab = false;
            $scope.updateDataTab = true;
            $scope.selectedProduct = null;
            $scope.productStockListFull = [];
            $scope.addProductFull = {};
            $scope.isEdit = 0;
            $scope.addProductFull.product = null;
            var ele = document.getElementById("productNamefull");
            ele.focus();
            $("#inventoryData").attr('checked', true);
        }
        $scope.getTaxValues();

    };
    $scope.getTaxValues = function () {
        productStockService.getTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
});


app.directive('focusBox', function () {
    return {
        "link": function (scope, element, attrs) {

            element.bind("keydown", function (event) {
                var valueLength = attrs.$$element[0].value.length;
                //Enter 
                if (attrs.id != "qtyinUnits") {
                    if (event.which === 13) {
                        //if (valueLength !== 0) {

                        if (attrs.nextid != undefined)
                            ele = document.getElementById(attrs.nextid);

                            if (scope.enablePrice && attrs.id == "packSize") {
                                ele = document.getElementById("sellingPrice");
                            }
                            ele.focus();
                            event.preventDefault();
                        //}
                    }
                }
                //BackSpace
                if (event.which === 8) {
                    if (valueLength === 0) {
                        ele = document.getElementById(attrs.previousid);

                        if (scope.enablePrice && attrs.id == "sellingPrice") {
                            ele = document.getElementById("packSize");
                        }
                        ele.focus();
                        event.preventDefault();
                    }
                }
            });
        }
    };
});

app.directive('numbersOnly', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});


