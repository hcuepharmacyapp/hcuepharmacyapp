﻿app.controller('productListCtrl', function ($scope, productStockService, productModel, pagerServcieInventory, $filter) {

    $scope.minDate = new Date();
    var d = new Date();


    var product = productModel;
    $scope.search = product;
    $scope.list = [];
    $scope.count = null;
    $scope.availableProductsCount = null;
    $scope.search.select = null;
    $scope.nGetcount = 1;
    //pagination
    $scope.search.page = pagerServcieInventory.page;
    $scope.pages = pagerServcieInventory.pages;
    $scope.paginate = pagerServcieInventory.paginate;
    $scope.productCondition = false;
    //pagination


    $scope.currentpage = 1;
    function toDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 0, parts[0]);
    };
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.rowParent = [];
    $scope.InActiveCount = 0;
    $scope.selectedProduct = null;
    $scope.selectedGeneric = null;
    $scope.showPanel = false;
    $scope.Myclass = 'productR';

    function pageSearch() {
        console.log($scope.search);

        $scope.currentpage = $scope.search.page.pageNo;


       

        // $.LoadingOverlay("show");
        productStockService.list($scope.search, $scope.nGetcount).then(function (response) {
            $scope.list = response.data.list;

            $scope.options = { Active: 1, InActive: 2 };

            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].product.status == 2) {
                    $scope.list[i].product.status = 2;
                    $scope.InActiveCount += 1;
                }
                else {
                    $scope.list[i].product.status = 1;
                }
                for (var j = 0; j < $scope.list[i].product.productStock.length; j++) {
                    if ($scope.list[i].product.productStock[j].status == 2) {
                        //$scope.list[i].status = 'Active';                   
                        $scope.list[i].product.productStock[j].status = 2;
                    }
                    else {
                        $scope.list[i].product.productStock[j].status = 1;
                    }
                    var expDate = $filter('date')($scope.list[i].product.productStock[j].expireDate, 'dd/MM/yyyy');

                    var val = (toDate(expDate) < $scope.minDate);
                    if (val == true) {
                        $scope.list[i].product.productStock[j].rowHighlight = "rowHighlight";
                        $scope.rowParent[i] = "rowHighlight";
                    }
                    else {
                        $scope.list[i].product.productStock[j].rowHighlight = "";
                        $scope.rowParent[i] = "rowHighlight";
                    }
                }
            }
        //    $.LoadingOverlay("hide");
        }, function () {
         //   $.LoadingOverlay("hide");
        });
    }

    $scope.getProducts = function (val) {
        return productStockService.InstancedrugFilter(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }
    $scope.getGeneric = function (val) {
        return productStockService.genericFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }
    $scope.changeoptions = function () {
        $scope.search.genName = undefined;
        $scope.search.drugName = undefined;

        if ($scope.search.select == 'product') {
            $scope.productCondition = true;
            $scope.genericCondition = false;
        }
        else if ($scope.search.select == 'generic') {
            $scope.genericCondition = true;
            $scope.productCondition = false;
        }
        else {
            $scope.productCondition = false;
            $scope.genericCondition = false;
        }

    };

    $scope.productSelected = function () {
        $scope.selectedProduct;
    }


    $scope.$on("update_getValue", function (event, value) {
        $scope.search.name = value;
    });
    $scope.search.getFilter = 'Available';
    $scope.activeMenu = 'Available';
    $scope.ReorderScope = 0;
    $scope.allItemScope = 0;

    $scope.stockSorting = function (stockSelected) {
        if (stockSelected == 'Zero') {
            $scope.ReorderScope = 2;
        }else{
            $scope.ReorderScope = 0;
        }
        $scope.allItemScope = 0;

        if (stockSelected == 'All') {
            $scope.allItemScope = 1 ;
        }
        $scope.currentpage = 1;
        $scope.search.getFilter = stockSelected;
        $scope.activeMenu = stockSelected;
        $scope.nGetcount = 0;
        $scope.productStockSearch();
    }

    $scope.reorderSorting = function (stockSelected) {
        $scope.ReorderScope = 1;
        $scope.allItemScope = 1;
        $scope.currentpage = 1;
        $scope.search.getFilter = stockSelected;
        $scope.activeMenu = stockSelected;
        $scope.nGetcount = 0;
        $scope.productStockSearch();
    }
  
    $scope.productStockSearch = function (productName, obj) {
        $.LoadingOverlay("show");

        if (($scope.search.drugName) && $scope.search.drugName != undefined && $scope.search.drugName != null) {
            $scope.search.genName = undefined;
            $scope.search.genericName = "";
            $scope.search.name = $scope.search.drugName.name;
            $scope.search.id = $scope.search.drugName.id;
            $scope.nGetcount = 1;
        }
        else if (($scope.search.genName) && $scope.search.genName != undefined && $scope.search.genName != null) {
            $scope.search.drugName = undefined;
            $scope.search.genericName = $scope.search.genName.genericName;
            $scope.search.id = "";
            $scope.search.name = "";
            $scope.nGetcount =1;
        }
        else if (productName != "" && productName != null)
        {
            $scope.search.name = productName;
            $scope.search.id = "";
            $scope.search.genericName = "";
            $scope.nGetcount = 1;
        }
        else {
            $scope.search.name = null;
        }
        console.log($scope.currentpage)
        $scope.search.page.pageNo = $scope.currentpage;
       
        productStockService.list($scope.search, $scope.nGetcount).then(function (response) {
            $scope.list = response.data.list;

            $scope.options = { Active: 1, InActive: 2 };

            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].product.status == 2) {
                    $scope.list[i].product.status = 2;
                    $scope.InActiveCount += 1;
                }
                else {
                    $scope.list[i].product.status = 1;
                }
                for (var j = 0; j < $scope.list[i].product.productStock.length; j++) {
                    if ($scope.list[i].product.productStock[j].status == 2) {
                        //$scope.list[i].status = 'Active';                   
                        $scope.list[i].product.productStock[j].status = 2;

                    }
                    else {
                        $scope.list[i].product.productStock[j].status = 1;
                    }
                    var expDate = $filter('date')($scope.list[i].product.productStock[j].expireDate, 'dd/MM/yyyy');

                    var val = (toDate(expDate) < $scope.minDate);
                    if (val == true) {
                        $scope.list[i].product.productStock[j].rowHighlight = "rowHighlight";
                        $scope.rowParent[i] = "rowHighlight";
                    }
                    else {
                        $scope.list[i].product.productStock[j].rowHighlight = "";
                        $scope.rowParent[i] = "rowHighlight";
                    }

                    }
                }

            if ($scope.nGetcount == 1)
                {
                $scope.count = response.data.noOfRows;
                $scope.available = response.data.available;
                $scope.zeroQty = response.data.zeroQty;
                $scope.expiry = response.data.expiry;
                $scope.reOrder = response.data.reorder;
            }
                //if ($scope.search.getFilter == "All")
                //    $scope.responseFilter = response.data.noOfRows;
                //else if ($scope.search.getFilter == "Zero")
                //    $scope.responseFilter = response.data.zeroQty;
                //else if ($scope.search.getFilter == "Expiry")
                //    $scope.responseFilter = response.data.expiry;
                //else if()
                //    $scope.responseFilter = response.data.available;
            if ($scope.nGetcount ==0) {
                if ($scope.search.getFilter == "All") {
                    $scope.responseFilter = $scope.count;
                }

 
                if ($scope.search.getFilter == "Zero") {
                    $scope.responseFilter = $scope.zeroQty;
                }

                if ($scope.search.getFilter == "Expiry") {
                    $scope.responseFilter = $scope.expiry;
                }

                if ($scope.search.getFilter == "Available") {
                    $scope.responseFilter = $scope.available;
                }

                if ($scope.search.getFilter == "Reorder") {
                    $scope.responseFilter = $scope.reOrder;
                    
                }
            }
           else {
                if ($scope.search.getFilter == "All") {
                    $scope.responseFilter = response.data.noOfRows;
                }
                 
               else if ($scope.search.getFilter == "Zero") {
                    $scope.responseFilter = response.data.zeroQty;
                }

               else if ($scope.search.getFilter == "Expiry") {
                    $scope.responseFilter = response.data.expiry;
                }

               else if ($scope.search.getFilter == "Available") {
                    $scope.responseFilter = response.data.available;
                }

               else if ($scope.search.getFilter == "Reorder") {
                    $scope.responseFilter = response.data.reorder;

                }
            }
                    pagerServcieInventory.init($scope.responseFilter, pageSearch);
                    if ($scope.count > 0) {
                        $scope.availableProducts();
                    } else {
                        $scope.availableProductsCount = 0;
                    }
 
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
       
    }

    $scope.productStockSearch();

    $scope.availableProducts = function () {
        var total = 0;
        for (count = 0; count < $scope.list.length; count++) {
            if ($scope.list[count].stock > 0) {
                total += 1;
            }
        }
        $scope.availableProductsCount = total;
    };

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.updateProduct = function (item) {
        $.LoadingOverlay("show");
        productStockService.updateProduct(item, $scope.search.getFilter).then(function (response) {
            $scope.list = response.data.list;
            $scope.paginate($scope.currentpage);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.updateSingleProductStock = function (item) {
        $.LoadingOverlay("show");
        productStockService.updateSingleProductStock(item).then(function (response) {
            $scope.list = response.data.list;
            $scope.nGetcount = 0;
            $scope.productStockSearch();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
});