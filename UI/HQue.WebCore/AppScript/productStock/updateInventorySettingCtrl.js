﻿app.controller('updateInventorySettingCtrl', function ($scope, $filter, productStockService, productModel, inventorySettingsModel, toastr) {

    var product = productModel;
    $scope.search = product;
    $scope.list = [];

    //Physical stock popup settings - Start
    $scope.physicalStockPopup = {
        popupEnabled: 0,
        startHour: { time: "12" },
        startMinute: {time: "00"},
        startMeridian: "AM",
        endHour: { time: "12" },
        endMinute: { time: "00" },
        endMeridian: "AM",
        popupInterval: 0
    }
    
    $scope.open1 = function () {
        if ($("#startDate").val().length < 10)
            $scope.startDate = "";
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        if ($("#endDate").val().length < 10)
            $scope.endDate = "";
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.startDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.endDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.minDate = new Date();

    $scope.hourList = [];
    function setTimeList() {
        for (var i = 1; i <= 12; i++) {
            var obj = {
                time: i < 10 ? "0" + i : i
            }
            $scope.hourList.push(obj);
        }
    }
    setTimeList();
    $scope.minutesList = [];
    function setMinutesList() {
        for (var i = 0; i <= 59; i++) {
            var obj = {
                time: i < 10 ? "0" + i : i
            }
            $scope.minutesList.push(obj);
        }
    }
    setMinutesList();
    $scope.meridianList = [];
    function setMeridianList() {
        var obj = {
            time:"AM"
        }
        $scope.meridianList.push(obj);
        obj = {
            time: "PM"
        }
        $scope.meridianList.push(obj);
    }
    setMeridianList();
    
    $scope.btnPhysicalStockPopup = function () {
        if (parseInt($scope.physicalStockPopup.popupEnabled) == 1 || $scope.physicalStockPopup.popupEnabled == true)
            $scope.inventorySetting.popupEnabled = true;
        else
            $scope.inventorySetting.popupEnabled = false;

        if ($scope.inventorySetting.popupEnabled == true) {
            
            var fromDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
            var toDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');
            var bits = fromDate.split('-');
            fromDate = new Date(bits[0], bits[1] - 1, bits[2]);
            bits = toDate.split('-');
            toDate = new Date(bits[0], bits[1] - 1, bits[2]);
            if (toDate < fromDate) {
                toastr.info("Popup End Date must be greater than or equal to Popup Start Date.");
                return;
            }

            if ($scope.physicalStockPopup.startHour.time + $scope.physicalStockPopup.startMinute.time + $scope.physicalStockPopup.startMeridian == $scope.physicalStockPopup.endHour.time + $scope.physicalStockPopup.endMinute.time + $scope.physicalStockPopup.endMeridian) {
                toastr.info("Popup Start Time & Popup End Time should not be the same.");
                return;
            }
            else if ($scope.physicalStockPopup.endHour.time == "12" && $scope.physicalStockPopup.endMeridian == "AM" && parseInt($scope.physicalStockPopup.endMinute.time) > 0) {
                toastr.info("Popup End Time must be in the same day.");
                return;
            }

            var startTime =
            (parseInt($scope.physicalStockPopup.startHour.time) == 12 && $scope.physicalStockPopup.startMeridian == "AM"
            ? "00" : ($scope.physicalStockPopup.startMeridian == "PM" && parseInt($scope.physicalStockPopup.startHour.time) == 12
            ? $scope.physicalStockPopup.startHour.time : ($scope.physicalStockPopup.startMeridian == "PM"
            ? parseInt($scope.physicalStockPopup.startHour.time) + 12 : $scope.physicalStockPopup.startHour.time)))
            + ":" + $scope.physicalStockPopup.startMinute.time;

            var endTime =
            (parseInt($scope.physicalStockPopup.endHour.time) == 12 && $scope.physicalStockPopup.endMeridian == "AM"
            ? "00" : ($scope.physicalStockPopup.endMeridian == "PM" && parseInt($scope.physicalStockPopup.endHour.time) == 12
            ? $scope.physicalStockPopup.endHour.time : ($scope.physicalStockPopup.endMeridian == "PM"
            ? parseInt($scope.physicalStockPopup.endHour.time) + 12 : $scope.physicalStockPopup.endHour.time)))
            + ":" + $scope.physicalStockPopup.endMinute.time;

            //console.log(startTime + endTime);   
            //fromDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
            //bits = fromDate.split('-');
            //fromDate = new Date(bits[0], bits[1] - 1, bits[2], startTime.substring(0, 2), startTime.substring(3, 5));
            //if (($scope.inventorySetting.popupStartTime != startTime || $filter('date')($scope.inventorySetting.popupStartDate, 'yyyy-MM-dd') != $filter('date')($scope.startDate, 'yyyy-MM-dd')) && $filter('date')($scope.startDate, 'yyyy-MM-dd') == $filter('date')(new Date(), 'yyyy-MM-dd') && fromDate.getTime() < (new Date()).getTime()) {
            //    toastr.info("Popup Start Time must be greater current time.");
            //    return;
            //}

            $scope.inventorySetting.popupStartDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
            $scope.inventorySetting.popupEndDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');
            $scope.inventorySetting.popupInterval = $scope.physicalStockPopup.popupInterval;
            
            $scope.inventorySetting.popupStartTime = startTime;
            if (endTime == "00:00")
                endTime = "23:59";
            $scope.inventorySetting.popupEndTime = endTime;

            var maxPopupInterval = checkPopupInterval(false);
            if (maxPopupInterval < 0) {
                toastr.info("Popup End Time must be greater than Popup Start Time.");
                return;
            }
            else if (parseInt($scope.inventorySetting.popupInterval) >= maxPopupInterval) {
                toastr.info("'Next Popup Interval (in Minutes)' must be less than '" + maxPopupInterval + "' minutes.");
                return;
            }
            else if (parseInt($scope.inventorySetting.popupInterval) < 15) {
                toastr.info("Minimum 'Next Popup Interval (in Minutes)' should be greater than or equal to 15 minutes.");
                return;
            }
        }
        $scope.inventorySettingUpdate();
    }

    function checkPopupInterval(forcount) {
        if ($scope.inventorySetting.popupEnabled == true || forcount == true) {
            var fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            var toDate = $filter('date')(new Date(), 'yyyy-MM-dd');            

            var bits = fromDate.split('-');
            var time = $scope.inventorySetting.popupStartTime.split(':');
            fromDate = new Date(bits[0], bits[1] - 1, bits[2], time[0], time[1], 0, 0);

            bits = toDate.split('-');
            time = $scope.inventorySetting.popupEndTime.split(':');
            toDate = new Date(bits[0], bits[1] - 1, bits[2], time[0], time[1], 0, 0);

            var maxPopupInterval = ((toDate.getTime() - fromDate.getTime()) / 1000) / 60;

            return maxPopupInterval;
        }
    }

    $scope.NoofPopupCount = function () {

        var fromDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
        var toDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');
        var bits = fromDate.split('-');
        fromDate = new Date(bits[0], bits[1] - 1, bits[2]);
        bits = toDate.split('-');
        toDate = new Date(bits[0], bits[1] - 1, bits[2]);
        if (toDate < fromDate) {            
            return 1;
        }

        if ($scope.physicalStockPopup.startHour.time + $scope.physicalStockPopup.startMinute.time + $scope.physicalStockPopup.startMeridian == $scope.physicalStockPopup.endHour.time + $scope.physicalStockPopup.endMinute.time + $scope.physicalStockPopup.endMeridian) {
            return 1;
        }
        else if ($scope.physicalStockPopup.endHour.time == "12" && $scope.physicalStockPopup.endMeridian == "AM" && parseInt($scope.physicalStockPopup.endMinute.time) > 0) {
            return 1;
        }

        var startTime =
        (parseInt($scope.physicalStockPopup.startHour.time) == 12 && $scope.physicalStockPopup.startMeridian == "AM"
        ? "00" : ($scope.physicalStockPopup.startMeridian == "PM" && parseInt($scope.physicalStockPopup.startHour.time) == 12
        ? $scope.physicalStockPopup.startHour.time : ($scope.physicalStockPopup.startMeridian == "PM"
        ? parseInt($scope.physicalStockPopup.startHour.time) + 12 : $scope.physicalStockPopup.startHour.time)))
        + ":" + $scope.physicalStockPopup.startMinute.time;

        var endTime =
        (parseInt($scope.physicalStockPopup.endHour.time) == 12 && $scope.physicalStockPopup.endMeridian == "AM"
        ? "00" : ($scope.physicalStockPopup.endMeridian == "PM" && parseInt($scope.physicalStockPopup.endHour.time) == 12
        ? $scope.physicalStockPopup.endHour.time : ($scope.physicalStockPopup.endMeridian == "PM"
        ? parseInt($scope.physicalStockPopup.endHour.time) + 12 : $scope.physicalStockPopup.endHour.time)))
        + ":" + $scope.physicalStockPopup.endMinute.time;

        $scope.inventorySetting.popupStartDate = $filter('date')($scope.startDate, 'yyyy-MM-dd');
        $scope.inventorySetting.popupEndDate = $filter('date')($scope.endDate, 'yyyy-MM-dd');
        $scope.inventorySetting.popupInterval = $scope.physicalStockPopup.popupInterval;

        $scope.inventorySetting.popupStartTime = startTime;
        if (endTime == "00:00")
            endTime = "23:59";
        $scope.inventorySetting.popupEndTime = endTime;

        var interval = $scope.inventorySetting.popupInterval;
        if (interval == undefined || interval == null || interval == "" || parseInt(interval) == 0)
            return 1;

        var maxPopupInterval = checkPopupInterval(true);
        var count = maxPopupInterval / parseInt(interval);
        if (count < 0)
            count = 1;
        return parseInt(count);
    }

    //End - Physical stock popup settings

    var inventorySettings = inventorySettingsModel;
    $scope.inventorySetting = inventorySettings;
    editInventorySettings();
   
    $scope.inventorySettingUpdate = function () {
        
        $.LoadingOverlay("show");
        productStockService.inventorySetting($scope.inventorySetting).then(function (response) {
            var temp = response.data;
            $scope.inventorySetting = temp;
            toastr.success('Settings updated successfully');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    function editInventorySettings() {
        productStockService.editInventorySettings().then(function (response) {
            var temp = response.data;
            if (temp != "")
                $scope.inventorySetting = temp;

            if ($scope.inventorySetting.popupStartTime != undefined) {
                $scope.physicalStockPopup.startHour.time = $scope.inventorySetting.popupStartTime.toString().substr(0, 2);
                $scope.physicalStockPopup.startMinute.time = $scope.inventorySetting.popupStartTime.toString().substr(3, 2);
                if (parseInt($scope.physicalStockPopup.startHour.time) > 12) {
                    $scope.physicalStockPopup.startHour.time = (parseInt($scope.physicalStockPopup.startHour.time) - 12).toString();
                    $scope.physicalStockPopup.startMeridian = "PM";
                }
                else if (parseInt($scope.physicalStockPopup.startHour.time) == 0)
                    $scope.physicalStockPopup.startHour.time = "12";
                else if (parseInt($scope.physicalStockPopup.startHour.time) == 12)
                    $scope.physicalStockPopup.startMeridian = "PM";
                if ($scope.physicalStockPopup.startHour.time.length == 1)
                    $scope.physicalStockPopup.startHour.time = "0" + $scope.physicalStockPopup.startHour.time;
            }
            else
                $scope.inventorySetting.popupStartTime = null;

            if ($scope.inventorySetting.popupEndTime != undefined) {
                $scope.physicalStockPopup.endHour.time = $scope.inventorySetting.popupEndTime.toString().substr(0, 2);
                $scope.physicalStockPopup.endMinute.time = $scope.inventorySetting.popupEndTime.toString().substr(3, 2);
                if (parseInt($scope.physicalStockPopup.endHour.time) > 12) {
                    $scope.physicalStockPopup.endHour.time = (parseInt($scope.physicalStockPopup.endHour.time) - 12).toString();
                    $scope.physicalStockPopup.endMeridian = "PM";
                }
                else if (parseInt($scope.physicalStockPopup.endHour.time) == 0)
                    $scope.physicalStockPopup.endHour.time = "12";
                else if (parseInt($scope.physicalStockPopup.endHour.time) == 12)
                    $scope.physicalStockPopup.endMeridian = "PM";
                if ($scope.physicalStockPopup.endHour.time.length == 1)
                    $scope.physicalStockPopup.endHour.time = "0" + $scope.physicalStockPopup.endHour.time;
            }
            else
                $scope.inventorySetting.popupEndTime = null;

            if ($scope.inventorySetting.popupEnabled != undefined)
                $scope.physicalStockPopup.popupEnabled = $scope.inventorySetting.popupEnabled;
            else
                $scope.inventorySetting.popupEnabled = null;

            if ($scope.inventorySetting.popupInterval != undefined)
                $scope.physicalStockPopup.popupInterval = $scope.inventorySetting.popupInterval;
            else
                $scope.inventorySetting.popupInterval = null;

            if ($scope.inventorySetting.popupStartDate != undefined)
                $scope.startDate = $filter('date')($scope.inventorySetting.popupStartDate, 'yyyy-MM-dd');
            else
                $scope.inventorySetting.popupStartDate = null;

            if ($scope.inventorySetting.popupEndDate != undefined)
                $scope.endDate = $filter('date')($scope.inventorySetting.popupEndDate, 'yyyy-MM-dd');
            else
                $scope.inventorySetting.popupEndDate = null;

        });
    };

});