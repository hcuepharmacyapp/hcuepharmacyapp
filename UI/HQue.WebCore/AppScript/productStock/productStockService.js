﻿app.factory('productStockService', function ($http) {
    return {
        "inStockProduct": function () { return $http.get('/ProductStockData/InStockProduct') },
        "productBatch": function (productId, blnQtyType) {
            return $http.get('/ProductStockData/InStockProduct/batch?productId=' + productId + '&blnQtyType=' + blnQtyType );
        },
        "productBatchTransfer": function (productId, showExpDateQty, blnQtyType) {
            return $http.get('/ProductStockData/InStockProductTransfer/productBatchTransfer?productId=' + productId + '&showExpDateQty=' + showExpDateQty + '&blnQtyType=' + blnQtyType);
        },
        "productBranchWise": function (product) {
            return $http.get('/ProductStockData/InBranchWiseStock?productid=' + product.id + "&productname=" + escape(product.name));
        },
        "loadBatch": function (productId) {
            return $http.get('/ProductStockData/getBathcDetails/batch?productId=' + productId);
        },
        "productAllBatch": function (productId) {
            return $http.get('/ProductStockData/GetProductAllBatch?productId=' + productId);
        },

        getActiveBatch: function (productId) {
            return $http.get('/ProductStockData/GetActiveProductBatch?productId=' + productId);
        },
        "list": function (productName) { //, ngetcount
            return $http.post('/ProductStockData/ListData', productName); //?nGetcount='+ngetcount
        },
        "lowProductStockList": function () {
            return $http.post('/ProductStockData/lowProductStockList');
        },
        "drugFilterData": function (filter) {
            return $http.get('/ProductStockData/InStockProductList?productName=' + escape(filter));
        },
           "getSalesProduct": function (filter) {
               return $http.get('/ProductStockData/InStockProductForSales?productName=' + escape(filter));
           },
        getAllStockData: function (filter) {
            return $http.get('/ProductStockData/GetAllStockProductList?productName=' + escape(filter));
        },

        //added for return in sales page

        getAllStockDataForReturn: function (filter) {
            return $http.get('/ProductStockData/GetInAllStockItemListForReturn?productName=' + escape(filter));
        },
        getBatchForReturn: function(productId){
            return $http.get('/ProductStockData/GetProductBatchForReturn?productId=' + productId);
        },

        "productStockData": function (filter) {
            return $http.get('/ProductStockData/AllStockProductList?productName=' + escape(filter));
        },
        "getInventory": function (filter) {
            return $http.post('/ProductStockData/getInventoryData', filter);
        },
        "updateInventory": function (item) {
            return $http.post('/ProductStockData/updateProductStock', item);
        },
        "saveNewStock": function (stock) {
            return $http.post('/ProductStockData/saveNewStock', stock);
        },
        "saveStockConsumption": function (stock) {
            return $http.post('/ProductStockData/saveStockConsumption', stock);
        },
        "getNewOpenedInventory": function (id, invoiceNo) {
            return $http.get('/ProductStockData/getNewOpenedInventory?prodId=' + id + "&InvoiceNo=" + invoiceNo);
        },
        "productList": function (productName) {
            return $http.post('/ProductStockData/ProductListData', productName);
        },
        "inventorySetting": function (data) {
            return $http.post('/ProductStockData/InventorySetting', data);
        },
        "editInventorySettings": function (data) {
            return $http.post('/ProductStockData/EditInventorySetting', data);
        },
        "updateProduct": function (item, filtertype) {
            return $http.post('/ProductStockData/updateProductInventory?filtertype=' + filtertype, item);
        },
        "updateTransferProduct": function (item) {
            return $http.post('/ProductStockData/updateTransferProductInventory', item);
        },
        "updateTransferProductStock": function (item) {
            return $http.post('/ProductStockData/updateTransferProductStockInventory', item);
        },
        "updateSingleProductStock": function (item) {
            return $http.post('/ProductStockData/updateSingleProductStock', item);
        },
        "getInstanceData": function () {
            return $http.post('/expireProductReport/getInstanceData');
        },
        "drugList": function (filter) {
            return $http.get('/product/ProductList?productName=' + escape(filter));
        },
        "genericFilterData": function (filter) {
            return $http.get('/product/GenericList?genericName=' + filter);
        },
        "genericFilterListData": function (filter) {
            return $http.get('/product/GenericSearchList?genericName=' + filter);
        },
        "getProductDetails": function (item) {
            return $http.get('/productStockData/GetProductDetails?productId=' + item);
        },
        "InstancedrugFilter": function (filter) {
            return $http.get('/product/InstanceProduct?productName=' + escape(filter));
        },
        // Added Gavaskar 16-02-2017 Start 
        inventoryDataUpdate: function (stock) {
            return $http.post('/ProductStockData/inventoryDataUpdate', stock);
        },
        // Added Gavaskar 16-02-2017 End 
        //Added Sarubala  15-03-17 Start
        getAllDrugFilterData: function (filter) {
            return $http.get('/ProductStockData/GetAllProductsinProductStock?productName=' + escape(filter));
        },
        //Added Sarubala  15-03-17 End

        //Added by Lawrence on 22Mar2017
        //getAllDrugFilterData: function (instanceid, filter) {
        //    return $http.get('/ProductStockData/GetAllProductsinProductStockList?instanceid=' + instanceid + '&productName=' + filter);
        //}
        //End
        "getEancodeExistData": function (data) {
            return $http.get('/ProductStockData/getEancodeExistData?eancode=' + data);
        },
        //MRP Implementation by Settu
        getEnableSelling: function () {
            return $http.get('/vendorPurchase/getEnableSelling');
        },
        getTaxValues: function () {
            return $http.get('/vendorPurchase/getTaxValues');
        },
        updateBarcodeProfile: function (data) {
            return $http.post('/ProductStockData/updateBarcodeProfile', data);
        },
        getBarcodeProfileList: function (data) {
            return $http.post('/ProductStockData/getBarcodeProfileList?name=' + data);
        },
        updateBarcodePrnDesign: function (data) {
            return $http.post('/ProductStockData/updateBarcodePrnDesign', data);
        },
        getBarcodePrnDesignList: function (data) {
            return $http.post('/ProductStockData/getBarcodePrnDesignList?name=' + data);
        },
        generateBarcode: function (data, profileId) {
            return $http.post('/ProductStockData/generateBarcode?BarcodeProfileId=' + profileId, data);
        },
        getProductCodes: function (data) {
            return $http.post('/Product/GetOnlineProductCodes', data);
        },
    };
});