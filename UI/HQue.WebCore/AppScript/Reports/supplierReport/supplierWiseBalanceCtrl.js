﻿app.controller('supplierWiseBalanceCtrl', function ($scope, $rootScope, supplierReportService, paymentModel, salesModel, toastr, $filter) {

    $scope.minDate = new Date();
    $scope.dateRangeExceeds = false; // hide excel export more than two months

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
        //Re-load the grid when change the branch name
        if ($scope.branchid != undefined && $scope.branchid != null) {
            $scope.supplierWiseSearch();
        }
    });
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.fromDate = null;
    $scope.toDate = null;

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
    };

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.type = '';
    $scope.data = [];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    var payment = paymentModel;
    
    $scope.search = payment;
    $scope.list = [];
    $scope.vendorList = [];

    $scope.supplierList = true;

    $scope.init = function () {
        $.LoadingOverlay("show");
        supplierReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $scope.supplierWiseSearch();
        }, function () {
            $.LoadingOverlay("hide");
        });
        supplierReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.supplierWiseSearch = function () {
        $scope.totalDue = 0;
        $.LoadingOverlay("show");

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }

        supplierReportService.supplierWiseBalanceList($scope.branchid).then(function (response) {
            $scope.list = response.data;            
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    //$scope.supplierWiseSearch();
    
    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }

    }

    $scope.checkFromDate = function () {
        var dt = $("#fromDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validFromDate = true;

                    if ($scope.toDate != undefined && $scope.toDate != null) {
                        $scope.checkToDate1($scope.fromDate, $scope.toDate);
                    }
                }
                else {
                    $scope.validFromDate = false;
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else
            $scope.validFromDate = true;
    }

    $scope.checkToDate = function () {
        var dt = $("#toDate").val();
        if (dt)
        {
        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1($scope.fromDate, $scope.toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
        }
        else
            $scope.validToDate = true;
    }

    
    $scope.changefilters = function () {       
        $scope.search.values = "";

        $scope.search.supplier = undefined;
        if ($scope.search.select == 'supplier') {
            $scope.selectMobile = false;
            $scope.selectSupplier = true;
            $scope.chkDate = true;
            $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.onChange();
            
        } else if ($scope.search.select == 'mobile') {
            $scope.selectMobile = true;
            $scope.selectSupplier = false;
            $scope.chkDate = true;
            $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.onChange();
        }       
        else {
            $scope.selectMobile = false;
            $scope.selectSupplier = false;
            $scope.chkDate = false;
            $scope.cancel();
        }
    };

    function isEmpty(value) {
        return (typeof value !== undefined || value !== null || value!=="");
    }

    $scope.supplierDetailsReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.fromDate,
            toDate: $scope.toDate
        }
        $scope.search.fromDate = $scope.fromDate;
        $scope.search.toDate = $scope.toDate;
        $scope.search.supplier = $scope.search.supplier;
        $scope.search.mobile = $scope.search.mobile;

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }

        //if ($scope.search.supplier != undefined || $scope.search.mobile != undefined) {

             supplierReportService.supplierDetailsList($scope.search,$scope.branchid).then(function (response) {
            //supplierReportService.supplierDetailsList($scope.search).then(function (response) {
                $scope.data = response.data;
                if ($scope.search.fromDate != undefined || $scope.search.fromDate != null)
                    $scope.dayDiff($scope.search.fromDate);
                angular.forEach($scope.data, function (data1, key) {
                    data1.vendorPurchase.goodsRcvNo = data1.vendorPurchase.billSeries + data1.vendorPurchase.goodsRcvNo;
                });

                $scope.supplierList = false;

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    supplierReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                }

                var grid = $("#grid").kendoGrid({
                    excel: {
                        fileName: "Supplier Wise Balance Details Report.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1600, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Supplier Wise Balance Details Report.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                      { field: "vendor.name", title: "Supplier Name", width: "110px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "vendorPurchase.invoiceNo", title: "Invoice No", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                       { field: "vendorPurchase.goodsRcvNo", title: "GRN", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "vendorPurchase.invoiceDate", title: "Invoice Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(vendorPurchase.invoiceDate), 'dd/MM/yyyy') #" },
                       { field: "creditDate", title: "Credit Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=creditDate == null ? '-' : kendo.toString(kendo.parseDate(creditDate), 'dd/MM/yyyy')#" },
                      { field: "payabledays", title: "Payable Days", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "age", title: "Age", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "Total " },
                     
                    { field: "debit", title: "Paid Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0') #<span>.00</span></div>" },

                    { field: "balance", title: "Pending Amount", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0') #<span>.00</span></div>" },
                    

                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [                            
                             { field: "debit", aggregate: "sum" },
                             { field: "balance", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "vendor.name": { type: "string" },
                                    "invoiceNo": { type: "string" },
                                    "goodsRcvNo": { type: "string" },
                                    "invoiceDate": { type: "date" },
                                    "creditDate": { type: "date" },
                                    "payableDays": { type: "number" },
                                    "age": { type: "number" },
                                    "debit": { type: "number" },
                                    "balance": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },
                    excelExport: function (e) {
                        addHeader(e);
                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }
                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },

                }).data("kendoGrid");


                grid.dataSource.originalFilter = grid.dataSource.filter;
                grid.dataSource.filter = function () {
                    if (arguments.length > 0) {
                        this.trigger("filtering", arguments);
                    }

                    var result = grid.dataSource.originalFilter.apply(this, arguments);
                    if (arguments.length > 0) {
                        this.trigger("filtering", result);
                    }

                    return result;
                }

                $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    var filters = dataSource.filter();
                    var allData = dataSource.data();
                    var query = new kendo.data.Query(allData);
                    var data = query.filter(filters).data;

                });

                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        //}
        //else {
        //    $.LoadingOverlay("hide");
        //}     
    }

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Supplier Wise Balance Details Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }
   
    $scope.filter = function (type) {
        $scope.type = type;
        $scope.supplierDetailsReport();
    };
    
    $scope.cancel = function () {
        $scope.fromDate = null;
        $scope.toDate = null;
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;
        $scope.search.select = null;
        $scope.search.supplier = null;
        $scope.selectSupplier = false;
        $scope.selectMobile = false;
        $scope.supplierList = true;
        $scope.chkDate = false;
        $scope.search.mobile = null;
        $scope.search.supplier = null;
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');

        //$scope.supplierDetailsReport();

    }
    
    $scope.onSupplierSelect = function (obj) {       
        $scope.search.supplier = obj.name;
        $scope.search.mobile = obj.mobile;
        $scope.search.vendorId = obj.id;        
    }

    $scope.getSupplierName = function (val) {
        return supplierReportService.getSupplierName(val, '').then(function (response) {

            return response.data.map(function (item) {
                return item;
            });
        });
    };

    //$scope.getSupplierName = function () {
    //    vendorService.vendorData().then(function (response) {
    //        $scope.vendorList = response.data;
    //    }, function () { toastr.error('Error Occured', 'Error'); });
    //}

    $scope.getSupplierMobile = function (val) {
        return supplierReportService.getSupplierName('', val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    
    $scope.onChange = function () {
        $scope.search.vendorId = "";
    };

    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;
        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);

        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}); 

app.filter('sumFilter', function () {
    return function (groups) {
        var totalDue = 0;
        for (i = 0; i < groups.length; i++) {
            totalDue = totalDue + groups[i].balance;
        };
        return totalDue;
    };
});
