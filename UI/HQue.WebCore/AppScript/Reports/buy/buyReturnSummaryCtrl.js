﻿app.controller('buyReturnSummaryCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'buyReportService', 'vendorPurchaseModel', 'vendorPurchaseItemModel', '$filter', function ($scope, $rootScope, $http, $interval, $q, buyReportService, vendorPurchaseModel, vendorPurchaseItemModel, $filter) {
    
    var vendorPurchaseItem = vendorPurchaseItemModel;
    var vendorPurchase = vendorPurchaseModel;
    $scope.search = vendorPurchaseItem;
    $scope.search.vendorPurchase = vendorPurchase;
    $scope.IsReturnDate = "true";
    $scope.allBranch = true;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };
        

    $scope.init = function () {
        $.LoadingOverlay("show");
        buyReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.buyReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        buyReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }


    $scope.buyReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);

        if ($scope.branchid == undefined || $scope.branchid == "" || $scope.branchid == null) {
            $scope.branchid = undefined;//$scope.instance.id;
        }
        $scope.setFileName();
        buyReportService.buyReturnSummary($scope.type, data, $scope.branchid, $scope.IsReturnDate).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                buyReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Purchase Return Summary";
            }

            $scope.fileName = "Purchase Return Summary.";

            $("#grid").kendoGrid({
                excel: {
                    fileName: $scope.fileName,
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Purchase_Return_Summary.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                     { field: "returnNo", title: "Return No", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "returnDate", title: "Return Date", width: "110px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(returnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                 
                  { field: "invoiceNo", title: "Invoice No", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                   //{ field: "invoiceDate", title: "Invoice Date", width: "110px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                   { field: "invoiceDate", title: "Invoice Date", width: "110px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=invoiceDate == null ? ' ' : kendo.toString(kendo.parseDate(invoiceDate), 'dd/MM/yyyy')#" },
                   { field: "instanceName", title: "Branch", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "vendorName", title: "Supplier Name", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "vendorpurchaseReturnReport.noOfItem", title: "No Of Item", width: "100px", type: "string", attributes: { class: "text-right field-report" }},
               
                  { field: "vendorpurchaseReturnReport.total", title: "Total Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'><span>Total : #= kendo.toString(sum, '0') #</span><span>.00</span></div>" }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        
                       { field: "vendorpurchaseReturnReport.total", aggregate: "sum" }

                    ],
                    schema: {
                        model: {
                            fields: {
                                "returnNo": { type: "string" },
                                "returnDate": { type: "date" },
                                "invoiceNo": { type: "string" },
                                "invoiceDate": { type: "date" },
                                "vendorName": { type: "string" },
                                "instanceName": { type: "string" },
                                "vendorpurchaseReturnReport.noOfItem": { type: "number" },                              
                                "vendorpurchaseReturnReport.total": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['returnNo', 'reportReturnDate', 'invoiceNo', 'reportInvoiceDate', 'instanceName', 'vendorName', 'vendorpurchaseReturnReport.noOfItem', 'vendorpurchaseReturnReport.total'];
    //
 

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.buyReport();
    }

    $scope.supplierList = [];

    $scope.selectedVendor = function () {
        $scope.selectedVendor;
    }
    $scope.getSupplierByInstance = function () {

        $scope.selectedVendor.instanceId;
        return buyReportService.getSupplierByInstance($scope.selectedVendor.instanceId).then(function (response) {

            $scope.supplierList = response.data;
            //var flags = [], output = [], l = response.data.length, i;
            //for (i = 0; i < l; i++) {
            //    if (flags[$filter('uppercase')(response.data[i].name) && (response.data[i].mobile)]) continue;
            //    flags[$filter('uppercase')(response.data[i].name) && (response.data[i].mobile)] = true;
            //    output.push(response.data[i]);
            //}
            //return output.map(function (item) {
            //    return item;
            //});
        });
    };

    //$scope.getSupplierByInstance();

    $scope.ddlchange = function (obj) {
       $rootScope.$broadcast('suppliername', obj.id);
    }

    $scope.header = "Hcue Technologies";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Purchase Return Summary", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Purchase Order Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $("#btnPdfExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").saveAsPDF();
        }
    });

    $("#btnXLSExport").kendoButton(
    {
        click: function () {
            $("#instanceName").text("Qbitz Tech");
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "xlsx";
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });

    //$("#btnCSVExport").kendoButton(
    //{
    //    click: function () {
    //        $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "csv";
    //        $("#grid").data("kendoGrid").saveAsExcel();
    //    }
    //});
    //
    $scope.setFileName = function () {
        $scope.fileNameNew = "PurchaseReturnSummaryList_" + $scope.instance.name;
    }
}]);
