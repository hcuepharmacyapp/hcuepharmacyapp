﻿app.controller('buyReportDCCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'buyReportService', 'vendorPurchaseModel', 'vendorPurchaseItemModel', '$filter', function ($scope, $rootScope, $http, $interval, $q, buyReportService, vendorPurchaseModel, vendorPurchaseItemModel, $filter) {


    var vendorPurchaseItem = vendorPurchaseItemModel;
    var vendorPurchase = vendorPurchaseModel;
    $scope.search = vendorPurchaseItem;
    $scope.search.vendorPurchase = vendorPurchase;
    $scope.allBranch = true;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.type = '';//"TODAY";
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        buyReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.buyReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        buyReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.buyReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }
        $scope.setFileName();
        buyReportService.dcList($scope.type, data, $scope.branchid, "", "").then(function (response) {
            $scope.data = response.data;
            $scope.type = "";


            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Purchase DC Details";
            }
            $scope.fileName = "Purchase DC Details.";
            //Package price and mrp  total removed on 22/02/2017 by poongodi
            $("#grid").kendoGrid({
                excel: {
                    fileName: $scope.fileName,
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Purchase DC Details.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                height: 350,
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "dcDate", title: "Dc Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(dcDate, 'yyyy-mm-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                  { field: "dcNo", title: "DcNo", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "vendor.name", title: "Supplier", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "instanceName", title: "Branch", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "productStock.product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" }, footerTemplate: "Total", filterable: { extra: false, cell: { operator: "startswith" } } },
                  { field: "packageQty", title: "No. of Strips", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                   { field: "freeQty", title: "Free Strips", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "packagePurchasePrice", title: "Purchase Rate (excluding vat/gst)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                    { field: "packageSellingPrice", title: "Selling Price(MRP)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                    { field: "discount", title: "P.Discount %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },

                  { field: "productStock.vat", title: "VAT/GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                   { field: "productStock.cst", title: "CST/IGST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                  { field: "vatValue", title: "VAT/GST Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },

                  { field: "reportTotal", title: "Final Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<span style='float:right;'></span><div style='float:right;margin-left:3px;' class='report-footer'>#= kendo.toString(sum,  '0.00')#</div>" },
                   { field: "status", title: "Status", width: "90px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "packageQty", aggregate: "sum" },
                        { field: "freeQty", aggregate: "sum" },
                          { field: "discount", aggregate: "sum" },
                        { field: "packagePurchasePrice", aggregate: "sum" },
                        { field: "productStock.vat", aggregate: "sum" },
                        { field: "vatValue", aggregate: "sum" },
                        { field: "packageSellingPrice", aggregate: "sum" },
                        { field: "reportTotal", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "dcDate": { type: "date" },
                                "dcNo": { type: "string" },
                                "vendor.name": { type: "string" },
                                "instanceName": { type: "string" },
                                "vendor.address": { type: "string" },
                                "vendor.gsTinNo": { type: "number" },
                                "productStock.product.name": { type: "string" },
                                "packageQty": { type: "number" },
                                "freeQty": { type: "number" },
                                "discount": { type: "number" },
                                "packagePurchasePrice": { type: "number" },
                                "productStock.vat": { type: "number" },
                                "productStock.cst": { type: "number" },

                                "vatValue": { type: "number" },
                                "packageSellingPrice": { type: "number" },
                                "reportTotal": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['reportDCDate', 'dcNo', 'vendor.name', 'instanceName', 'productStock.product.name', 'packageQty', 'freeQty', 'packagePurchasePrice', 'packageSellingPrice', 'discount', 'productStock.vat', 'productStock.cst', 'vatValue', 'reportTotal', 'status'];
    //


    //$scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.buyReport();
    }


    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Purchase DC Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Purchase DC Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $("#btnPdfExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").saveAsPDF();
        }
    });

    $("#btnXLSExport").kendoButton(
    {
        click: function () {
            $("#instanceName").text("Qbitz Tech");
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "xlsx";
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });

    $("#btnCSVExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "csv";
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });
    $scope.setFileName = function () {
        $scope.fileNameNew = "PurchaseDCDetails_" + $scope.instance.name;
    }
    //
}]);
