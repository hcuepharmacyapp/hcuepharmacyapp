﻿app.controller('buyOrderReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'buyReportService', 'vendorOrderModel', 'vendorOrderItemModel', '$filter', function ($scope,$rootScope, uiGridConstants, $http, $interval, $q, buyReportService, vendorOrderModel, vendorOrderItemModel, $filter) {
    
    var vendorOrderItem = vendorOrderItemModel;
    var vendorOrder = vendorOrderModel;
    $scope.search = vendorOrderItem;
    $scope.search.vendorOrder = vendorOrder;
    $scope.search.vendorOrder.orderId = null;
    $scope.allBranch = true;
    $scope.selectedInstance = {};

    var sno = 0;
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
    //Task rolledback for 37 release 
    //$scope.$on('instanceList', function (event, data) {
    //    console.log(data);
    //    data.splice(0, 0, { name: "All", id: "" });
    //});

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');


    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.type = "Today";
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };
  

    $scope.init = function () {
        $.LoadingOverlay("show");
        buyReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.buyOrderReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        buyReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.buyOrderReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);

        $scope.search.vendorOrder.dates = data;

        if ($scope.search.vendorOrder.orderId != null) {
            $scope.type = "OrderNo"
        }
        //if ($scope.branchid == undefined || $scope.branchid == "") {
        if ($scope.branchid == undefined) {
            $scope.branchid = undefined; //$scope.instance.id;
        }
        // $scope.search.vendorOrder.InstanceId = $scope.branchid;
        $scope.setFileName();
        buyReportService.buyOrderList($scope.type, $scope.search.vendorOrder, $scope.branchid).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";
            $scope.search.vendorOrder.orderId = null;

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/> Purchase Order Details";
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Purchase Order Details";
              
            }

            $scope.fileName = "Purchase Order Details.";

            $("#grid").kendoGrid({
                    excel: {
                        fileName: $scope.fileName,
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Purchase Order Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height:350,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                   { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "90px", attributes: { ftype: "sno", class: " text-left field-report" } },
                   { field: "vendorOrder.instance.name", title: "Branch", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "vendorOrder.orderDate", title: "Order Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(vendorOrder.orderDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                  { field: "vendorOrder.orderId", title: "Order No", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "vendorOrder.vendor.name", title: "Supplier", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra:false, cell: {operator: "startswith"}}},
                  { field: "quantity", title: "Qty", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "productStock.packageSize", title: "Package Size", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report" } },
                   { field: "freeQty", title: "Free Qty", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "productStock.packagePurchasePrice", title: "Purchase Rate (excluding vat/gst)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                    { field: "packageSellingPrice", title: "Selling Price(MRP)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "discount", title: "P.Discount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    
                  { field: "productStock.vat", title: "VAT/GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                   { field: "productStock.cst", title: "CST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },    //CST/IGST              
                
                  { field: "totalAmount", title: "Final Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<span style='float:left;'>Total:</span><div style='float:left;margin-left:3px;' class='report-footer'>#= kendo.toString(sum, 'n0') #.00</div>" }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "quantity", aggregate: "sum" },
                        { field: "freeQty", aggregate: "sum" },                          
                          { field: "totalAmount", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "number" },
                                "vendorOrder.instance.name": { type: "string" },
                                "vendorOrder.orderDate": { type: "date" },
                                "vendorOrder.orderId": { type: "number" },
                                "vendorOrder.vendor.name": { type: "string" },
                                "vendorOrder.vendor.address": { type: "string" },
                                "vendorOrder.vendor.gsTinNo": { type: "number" },
                                "product.name": { type: "string" },
                                "quantity": { type: "number" },
                                "productStock.packageSize": { type: "number" },
                                "freeQty": { type: "number" },
                                "discount": { type: "number" },
                                "productStock.packagePurchasePrice": { type: "number" },
                                "productStock.vat": { type: "number" },
                                "productStock.cst": { type: "number" },                               
                                
                                "packageSellingPrice": { type: "number" },
                                "totalAmount": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }
 	          if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['vendorOrder.instance.name', 'reportInvoiceDate', 'orderId', 'vendorOrder.vendor.name', 'product.name', 'quantity', 'productStock.packageSize', 'freeQty', 'productStock.packagePurchasePrice', 'packageSellingPrice', 'discount', 'productStock.vat', 'productStock.cst', 'totalAmount'];
    //


    //$scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;

        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }

        $scope.buyOrderReport();
    }


    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Purchase Order Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Purchase Order Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $("#btnPdfExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").saveAsPDF();
        }
    });


    $("#btnXLSExport").kendoButton(
    {
        click: function () {
            $("#instanceName").text("Qbitz Tech");
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "xlsx";
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });

    //$("#btnCSVExport").kendoButton(
    // {
    //     click: function () {
    //         $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "csv";
    //         $("#grid").data("kendoGrid").saveAsExcel();
    //     }
    // });
    //

    $scope.setFileName = function () {
        $scope.fileNameNew = "PurchaseOrderDetails_" + $scope.instance.name;
    }
}]);
