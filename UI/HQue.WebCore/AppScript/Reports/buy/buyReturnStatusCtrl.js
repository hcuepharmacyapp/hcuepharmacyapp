﻿app.controller('buyReturnStatusCtrl', function ($scope, $rootScope, buyReportService, vendorPurchaseService, vendorService, vendorReturnModel, pagerServcie, productService, $filter) {

    var vendorPurchaseReturn = vendorReturnModel;
    $scope.search = vendorPurchaseReturn;
    $scope.IsReturnDate = "false";
    $scope.isFromReport = true;
    $scope.allBranch = true;

    $scope.list = {};
    $scope.vendordata1 = {};

    var sno = 0;
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
   
    $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    function pageSearch() {
        $.LoadingOverlay("show");
        vendorPurchaseService.returnList($scope.search, $scope.IsReturnDate, $scope.isFromReport).then(function (response) {
            $scope.list = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.init = function () {
        $.LoadingOverlay("show");
        $scope.search.paymentStatus = "All";        
        buyReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.vendorPurchaseReturnSearch();

        }, function () {
            $.LoadingOverlay("hide");
        });
        buyReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.vendorPurchaseReturnSearch = function () {
        $.LoadingOverlay("show");
       
        $scope.search.searchProductId = "";
        $scope.search.searchProductName = "";
        //if ($scope.search.paymentStatus == "" || $scope.search.paymentStatus == null) {
        //    $scope.search.paymentStatus = "None";
        //}
        if ($scope.search.drugName) {
            $scope.search.searchProductId = $scope.search.drugName.id;
            $scope.search.searchProductName = $scope.search.drugName.name;
        }
       
        if ($scope.search.vendorPurchase)
        {
            $scope.search.returnNo = $scope.search.vendorPurchase.returnNo;                       
        }
        $scope.search.fromDate = $scope.fromDate;
        $scope.search.toDate = $scope.toDate;

        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.fromDate;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);

        if ($scope.branchid == undefined || $scope.branchid == "") {
            //$scope.search.id = undefined; //$scope.branchid;
            $scope.search.instanceId = "undefined";
        }
     
        //var grid = $("#grid").data("kendoGrid");       
        //$("#grid").data("kendoGrid").destroy();
        $("#grid").empty();
       //setFileName();
        vendorPurchaseService.returnList($scope.search, $scope.IsReturnDate, $scope.isFromReport).then(function (response) {
            $scope.list = response.data.list;
            $scope.vendordata1 = response.data;            
            var pdfHeader = "";            
            $scope.temp = [];
            if ($scope.search.searchProductName != "") { //To do the fine search based on the selected product
                for (var i = 0; i < $scope.list.length; ++i) {
                    for (var j = 0; j < $scope.list[i].vendorPurchaseReturnItem.length; ++j) {
                        $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase = $scope.list[i].vendorPurchase;
                        $scope.list[i].vendorPurchaseReturnItem[j].returnNo = $scope.list[i].returnNo;
                        $scope.list[i].vendorPurchaseReturnItem[j].paymentStatus = $scope.list[i].paymentStatus;
                        $scope.list[i].vendorPurchaseReturnItem[j].paymentRemarks = $scope.list[i].paymentRemarks;
                        $scope.list[i].vendorPurchaseReturnItem[j].returnDate = $scope.list[i].returnDate;
                        if ($scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice!=undefined &&
                            $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice !=null)
                        {
                            $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice = ($scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice * $scope.list[i].vendorPurchaseReturnItem[j].quantity && $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice > 0);
                        }
                        else
                        {
                            // $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice = ($scope.list[i].vendorPurchaseReturnItem[j].productStock.purchasePrice * $scope.list[i].vendorPurchaseReturnItem[j].quantity); // Commented by Gavaskar 17-09-2017
                            $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice = ($scope.list[i].vendorPurchaseReturnItem[j].returnPurchasePrice * $scope.list[i].vendorPurchaseReturnItem[j].quantity); // Modified by Gavaskar 17-09-2017
                            
                        }
                        if ($scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo != null && $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo != undefined && $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo != '') {
                            $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo = $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.billSeries + $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo;
                        }
                        if ($scope.search.searchProductName != "" & ($scope.search.searchProductName == $scope.list[i].vendorPurchaseReturnItem[j].productStock.product.name)) {
                            $scope.temp.push($scope.list[i].vendorPurchaseReturnItem[j]);
                        }
                    }
                }
            }            
            else {
                for (var i = 0; i < $scope.list.length; ++i) {
                    for (var j = 0; j < $scope.list[i].vendorPurchaseReturnItem.length; ++j) {
                        $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase = $scope.list[i].vendorPurchase;
                        $scope.list[i].vendorPurchaseReturnItem[j].returnNo = $scope.list[i].returnNo;
                        $scope.list[i].vendorPurchaseReturnItem[j].paymentStatus = $scope.list[i].paymentStatus;
                        $scope.list[i].vendorPurchaseReturnItem[j].paymentRemarks = $scope.list[i].paymentRemarks;
                        $scope.list[i].vendorPurchaseReturnItem[j].returnDate = $scope.list[i].returnDate;
                        if ($scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice != undefined &&
                                 $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice != null && $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice> 0) {
                            $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice = ($scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice * $scope.list[i].vendorPurchaseReturnItem[j].quantity);
                        }
                        else {
                            //$scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice = ($scope.list[i].vendorPurchaseReturnItem[j].productStock.purchasePrice * $scope.list[i].vendorPurchaseReturnItem[j].quantity);  // Commented by Gavaskar 17-09-2017
                            $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchaseItem.purchasePrice = ($scope.list[i].vendorPurchaseReturnItem[j].returnPurchasePrice * $scope.list[i].vendorPurchaseReturnItem[j].quantity); // Modified by Gavaskar 17-09-2017
                        }
                        if ($scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo != null && $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo != undefined && $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo != '')
                        {
                            $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo = $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.billSeries + $scope.list[i].vendorPurchaseReturnItem[j].vendorPurchase.goodsRcvNo;
                        }                        
                        $scope.temp.push($scope.list[i].vendorPurchaseReturnItem[j]);
                    }
                }
            }

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            //else {
            //    salesReportService.getInstanceData().then(function (pdfResponse) {
            //        $scope.pdfHeader = pdfResponse.data;
            //    }
            //    , function () { });
            //}

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/> PURCHASE AND EXPIRY RETURN STATUS";
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> PURCHASE AND EXPIRY RETURN STATUS";

            }           
            $scope.fileName = "PURCHASE AND EXPIRY RETURN STATUS.";
            $("#grid").kendoGrid({
                excel: {
                    fileName: $scope.fileName,
                    allPages: true
                },
                pdf: {
                    paperSize: [1000, 2300], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "PURCHASE AND EXPIRY RETURN STATUS.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                height: 350,
                filterable: {
                    mode: "column"
                },
                columns: [             
               { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "90px", attributes: { ftype: "sno", class: " text-left field-report" } },
               { field: "vendorPurchase.instance.name", title: "Branch", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
               { field: "vendorPurchase.invoiceNo", title: "InvoiceNo", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
               { field: "paymentStatus", title: "Payment Status", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
               { field: "paymentRemarks", title: "Payment Remarks", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
               { field: "vendorPurchase.goodsRcvNo", title: "GR.No", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
               //{ field: "vendorPurchase.goodsRcvNo", title: "GR.No", width: "100px", type: "string", attributes: { class: "text-right field-report" } },               
               { field: "vendorPurchase.invoiceDate", title: "InvoiceDate", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=vendorPurchase.invoiceDate == null ? '' : kendo.toString(kendo.parseDate(vendorPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
               { field: "vendorPurchase.vendor.name", title: "Supplier Name", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
               { field: "vendorPurchase.vendor.mobile", title: "Mobile", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
               { field: "returnNo", title: "RetNo", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report" } },
               { field: "returnDate", title: "RetDate", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(returnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                { field: "productStock.product.name", title: "Product", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },                
                { field: "quantity", title: "Qty", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                 //{ field: "credit", title: "Credit", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n0') #<span>.00</span></div>" },
                //{ field: "quantity", title: "Quantity", width: "100px", attributes: { class: "text-left right-report" }, filterable: { cell: { showOperators: false } } },
                { field: "vendorPurchaseItem.discount", title: "Discount", width: "160px", attributes: { class: "text-left right-report" }, filterable: { cell: { showOperators: false } } },
                { field: "productStock.batchNo", title: "Batch", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                { field: "productStock.expireDate", title: "Expiry", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM/yy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'MM/yy') #", filterable: { cell: { showOperators: false } } },
                //{ field: "vendorPurchaseItem.purchasePrice * quantity", title: "Batch", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                //{ field: "vendorPurchaseItem.purchasePrice", title: "Return Price", width: "160px", format: "{0:00}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, template: "#= kendo.toString((vendorPurchaseItem.purchasePrice * quantity),'0.00')#"},
                { field: "vendorPurchaseItem.purchasePrice", title: "Return Price", width: "160px", format: "{0:00}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, template: "#= kendo.toString((vendorPurchaseItem.purchasePrice),'0.00')#" },
                 //{ field: "vendorPurchaseReturnItem.isDeleted", title: "Status", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                ],
                dataSource: {
                    data: $scope.temp,
                    aggregate: [
                        { field: "quantity", aggregate: "sum" },
                        //{ field: "vendorPurchaseItem.purchasePrice", aggregate: "sum" },                        
                    ],
                    schema: {
                        model: {
                            fields: {                                    
                                "sNo": { type: "number" },                                
                                "vendorPurchase.invoiceNo": { type: "string" },
                                "paymentStatus": { type: "string" },
                                "paymentRemarks": { type: "string" },
                                "vendorPurchase.goodsRcvNo": { type: "string" },
                                "vendorPurchase.invoiceDate": { type: "string" },
                                "vendorPurchase.vendor.name": { type: "string" },
                                "vendorPurchase.vendor.mobile": { type: "string" },
                                "returnNo": { type: "string" },
                                "returnDate": { type: "string" },
                                "productStock.product.name": { type: "string" },                               
                                "quantity": { type: "quantity" },
                                "vendorPurchaseItem.discount": { type: "string" },
                                "productStock.batchNo": { type: "string" },
                                "productStock.expireDate": { type: "string" },
                                "vendorPurchaseItem.purchasePrice": { type: "string" },
                                //"vendorPurchaseReturnItem.isDeleted": { type: "boolean" },
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    //$scope.order = ['name', 'invoiceNo', 'paymentStatus', 'paymentRemarks', 'goodsRcvNo', 'reportInvoiceDate', 'vendorName', 'mobile', 'returnNo', 'reportReturnDate', 'productName', 'quantity', 'discount', 'productStock.batchNo', 'reportExpireDate', 'vendorPurchaseItem.purchasePrice'];
    ////$scope.vendorPurchaseReturnSearch();

    $scope.vendor = function () {
        vendorService.vendorData().then(function (response) {
            $scope.vendorList = response.data;
        }, function () { });
    }
    $scope.vendor();

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.getProducts = function (val) {

        return productService.ReturndrugFilter(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    //by San
    $scope.minDate = new Date();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.openfromdate = function () {
        $scope.popupfromdate.opened = true;
    };

    $scope.popupfromdate = {
        opened: false
    };

    $scope.opentoDate = function () {
        $scope.popuptoDate.opened = true;
    };

    $scope.popuptoDate = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.clearSearch = function () {
        $scope.search.drugName = "";
        $scope.search.searchProductId = "";            
        $scope.search.invoiceNo = "";
        if ($scope.search.vendorPurchase != undefined) {
            $scope.search.vendorPurchase.GoodsRcvNo = "";
            $scope.search.vendorPurchase.vendor = { vendor: {} };
            $scope.search.vendorPurchase.returnNo = "";
        }
        $scope.search.batchNo = "";
        $scope.search.invoiceDate = "";
        //$scope.search.vendorPurchase.vendor.Mobile = "";
        $scope.search.paymentStatus = "All";
        
        $("#grid").empty();
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.IsReturnDate = "true";
        $scope.vendorPurchaseReturnSearch();
           
    }

    $scope.getEnableSelling = function () {
        vendorPurchaseService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getEnableSelling();

  
    $scope.editVendorPurchaseReturn = function (item) {
        item.isEdit = true;
    }
    $scope.updateVendorPurchaseReturn = function (item) {
        $.LoadingOverlay("show");
        vendorPurchaseService.updateVendorPurchaseReturn(item).then(function (response) {
            item.isEdit = !response.data;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "PURCHASE AND EXPIRY RETURN STATUS", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " PURCHASE AND EXPIRY RETURN STATUS", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $("#btnPdfExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").saveAsPDF();
        }
    });


    $("#btnXLSExport").kendoButton(
    {
        click: function () {
            $("#instanceName").text("Qbitz Tech");
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "xlsx";
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });

    $("#btnCSVExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "csv";
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });
   //setFileName = function () {
   //     $scope.fileNameNew = "PURCHASE RETURN AND EXPIRY STATUS_" + $scope.instance.name;
   // }
});


