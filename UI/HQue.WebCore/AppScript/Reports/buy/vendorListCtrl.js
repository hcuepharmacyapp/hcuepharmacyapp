﻿app.controller('vendorListCtrl', ['$scope',  '$http', '$interval', '$q', 'buyReportService', 'vendorPurchaseModel', 'vendorPurchaseItemModel',  function ($scope, $http, $interval, $q, buyReportService, vendorPurchaseModel, vendorPurchaseItemModel) {
    
    var vendorPurchaseItem = vendorPurchaseItemModel;
    var vendorPurchase = vendorPurchaseModel;
    $scope.search = vendorPurchaseItem;
    $scope.search.vendorPurchase = vendorPurchase;

 

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };


    $scope.init = function (vendorId) {
        $scope.vendorId = vendorId;
        $scope.buyReport(vendorId);
    }

    $scope.buyReport = function (vendorId) {
        $.LoadingOverlay("show");
        
        buyReportService.vendorListView(vendorId).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";


            $("#grid").kendoGrid({
                    excel: {
                        fileName: "Purchase Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        allPages: true,
                        paperSize: "A4",
                        margin: { top: "3cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                height:550,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                columns: [
                  { field: "buyPurchase.invoiceDate", title: "Invoice Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype:"date",fformat:"dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(buyPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                  { field: "buyPurchase.invoiceNo", title: "Invoice No", width: "100px", attributes: { class: "text-left field-report" }, filterable: {cell: {showOperators: false}} },
                  { field: "buyPurchase.vendor.name", title: "Supplier", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "productStock.product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra:false, cell: {operator: "startswith"}},footerTemplate: "Total"},
                  { field: "packageQty", title: "Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                   { field: "freeQty", title: "Free Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                  { field: "packagePurchasePrice", title: "Purchase Rate (excluding vat/gst)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                    { field: "packageSellingPrice", title: "Selling Price(MRP)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                    { field: "markupPerc", title: "Markup %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "discount", title: "P.Discount %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "schemeDiscountPerc", title: "Scheme Discount %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }},
                    
                  { field: "productStock.vat", title: "VAT/GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                   { field: "productStock.cst", title: "CST/IGST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                  { field: "vatPrice", title: "VAT/CST Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                
                  { field: "reportTotal", title: "Final Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  
                  
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "packageQty", aggregate: "sum" },
                        { field: "freeQty", aggregate: "sum" },
                          { field: "discount", aggregate: "sum" },
                        { field: "packagePurchasePrice", aggregate: "sum" },
                        { field: "productStock.vat", aggregate: "sum" },
                        { field: "vatPrice", aggregate: "sum" },
                        { field: "packageSellingPrice", aggregate: "sum" },
                        { field: "reportTotal", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "buyPurchase.invoiceDate": { type: "date" },
                                "buyPurchase.invoiceNo": { type: "number" },
                                "buyPurchase.vendor.name": { type: "string" },
                                "buyPurchase.vendor.address": { type: "string" },
                                "buyPurchase.vendor.tinNo": { type: "number" },
                                "productStock.product.name": { type: "string" },
                                "packageQty": { type: "number" },
                                "freeQty": { type: "number" },
                                "markupPerc": { type: "number" },
                                "discount": { type: "number" },
                                "schemeDiscountPerc": { type: "number" },
                                "packagePurchasePrice": { type: "number" },
                                "productStock.vat": { type: "number" },
                                "productStock.cst": { type: "number" },
                               
                                "vatPrice": { type: "number" },
                                "packageSellingPrice": { type: "number" },
                                "reportTotal": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = new Date(cell.value);
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //


    $scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.buyReport();
    }


    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Purchase Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "TIN No: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
}]);
