﻿app.factory('buyReportService', function ($http) {
    return {
        list: function (type, data, instanceid, vendorid, productid, IsInvoiceDate) {
            return $http.post('/buyReport/listData?type=' + type + '&sInstanceId=' + instanceid + '&sProductId=' + productid + '&sVendorId=' + vendorid + '&IsInvoiceDate=' + IsInvoiceDate, data);
        },
        dcList: function (type, data, instanceid, vendorid, productid) {
            return $http.post('/buyReport/dcListData?type=' + type + '&sInstanceId=' + instanceid + '&sProductId=' + productid + '&sVendorId=' + vendorid, data);
        },
        Productwiselist: function (id, data) {
            return $http.post('/buyReport/ProductWiselistData?Id=' + id, data);
        },
        Vendorwiselist: function (id, data) {
            return $http.post('/buyReport/VendorWiselistData?Id=' + id, data);
        },
        returnList: function (type, data, instanceid, IsInvoiceDate) {
            return $http.post('/buyReport/returnListData?type=' + type + '&sInstanceId=' + instanceid + '&IsInvoiceDate=' + IsInvoiceDate, data);
        },

        buySummarylist: function (type, data, instanceid, IsInvoiceDate) {
            return $http.post('/buyReport/purchaseSummaryListData?type=' + type + '&sInstanceId=' + instanceid + '&IsInvoiceDate=' + IsInvoiceDate, data);
        },

        buySaleslist: function (type, data, instanceid) {
            return $http.post('/buyReport/purchaseSalesListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },

        buyOrderList: function (type, order1, instanceid) {
            return $http.post('/buyReport/PurchaseOrderData?type=' + type + '&instanceid=' + instanceid, order1);
        },

        buyReturnSummary: function (type, data, instanceid, IsInvoiceDate) {
            return $http.post('/buyReport/returnSummaryListData?type=' + type + '&sInstanceId=' + instanceid + '&IsInvoiceDate=' + IsInvoiceDate, data);
        },
        vendorListView: function (id) {
            return $http.post('/buyReport/vendorListViewData?id=' + id);
        },
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },
        buyMarginlist: function (type, data, instanceid, xy, yz, IsInvoiceDate) {
            return $http.post('/buyReport/PurchaseMarginData?type=' + type + '&sInstanceId=' + instanceid + '&IsInvoiceDate=' + IsInvoiceDate, data);
        },

        buyModifiedlist: function (data,instanceid) {
            return $http.post('/buyReport/PurchaseModifiedList?sInstanceId=' + instanceid, data);
        },
        BranchWisePurchaseReportData: function (type, data, instanceid, level) {
            return $http.post('/buyReport/BranchWisePurchaseReportData?type=' + type + '&sInstanceId=' + instanceid + '&level=' + level, data);
        },
    }
});
