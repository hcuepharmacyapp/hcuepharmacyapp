﻿app.controller('buyModifiedReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'buyReportService', 'vendorPurchaseModel', 'vendorPurchaseItemModel', 'productService', 'productModel', 'salesReportService', '$filter', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, buyReportService, vendorPurchaseModel, vendorPurchaseItemModel, productService, productModel, salesReportService, $filter) {
    var vendorPurchaseItem = vendorPurchaseItemModel;
    var vendorPurchase = vendorPurchaseModel;
    //$scope.search.vendorPurchaseItem = vendorPurchaseItem;
    $scope.search = vendorPurchase;
    $scope.product = productModel;
    $scope.allBranch = true;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
    //$scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    //$scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        "opened": false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.data = [];
    $scope.type = "TODAY";
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };
    $scope.init = function () {
        $.LoadingOverlay("show");
        $scope.search.select = 'modifiedDate'
        $scope.search.select1 = "";
        $scope.salesCondition = false;
        $scope.dateCondition = true;
        $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.productCondition = false;
        buyReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.buyModifiedReport();
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });
        buyReportService.getUserData().then(function (response) {
            $scope.userData = response.data;
        }, function (error) { console.log(error); });
    };
    $scope.getProducts = function (val) {
        return productService.InstancedrugFilter(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    $scope.cancel = function () {
        $.LoadingOverlay("show");
        $scope.search.select = 'modifiedDate'
        $scope.search.select1 = "";
        $scope.search.drugName = "";
        $scope.salesCondition = false;
        $scope.dateCondition = true;
        $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.productCondition = false;
        $scope.buyModifiedReport();
        $.LoadingOverlay("hide");
    };
    $scope.changefilters = function () {
        $scope.search.select1 = "";
        $scope.search.values = "";
        $scope.search.invoiceDate = "";
        $scope.search.drugName = undefined;
        if ($scope.search.select == 'quantity' || $scope.search.select == 'mrp' || $scope.search.select == 'discount' || $scope.search.select == 'vat') {
            $scope.salesCondition = true;
            $scope.dateCondition = false;
            $scope.productCondition = false;
        } else if ($scope.search.select == 'createddate' || $scope.search.select == 'billDate' || $scope.search.select == 'expiry' || $scope.search.select == 'modifiedDate') {
            $scope.salesCondition = false;
            $scope.dateCondition = true;
            $scope.productCondition = false;
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');
        } else if ($scope.search.select == 'product') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = true;
        } else {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
        }
    };
    $scope.buyModifiedReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.search.values,
            toDate: $scope.search.selectValue
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.search.values;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        if ($scope.search.drugName)
            $scope.search.values = $scope.search.drugName.id;
        $scope.setFileName();
        buyReportService.buyModifiedlist($scope.search, $scope.branchid).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";
            angular.forEach($scope.data, function (data1, key) {
                data1.grnNo = data1.billSeries + data1.grnNo;
            });
            //var pdfHeader = "";
            //if ($scope.instance != undefined) {
            //    $scope.pdfHeader = $scope.instance;
            //}
            //else {
            //    buyReportService.getInstanceData().then(function (pdfResponse) {
            //        $scope.pdfHeader = pdfResponse.data;
            //    }, function () { });
            //}
            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            } else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function (error) {
                    console.log(error);
                });
            }
            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "") {
                    $scope.pdfHeader.drugLicenseNo = "";
                } else {
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");
                }
                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "") {
                    $scope.pdfHeader.gsTinNo = "";
                } else {
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");
                }
                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "") {
                    $scope.pdfHeader.fullAddress = "";
                } else {
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");
                }
                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>" + "Purchase Modified/Deleted Report" + "<br/>";
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Purchase Modified/Deleted Report";
            }
            $scope.fileName = "Purchase Modified/Deleted Report.";
            $("#grid").kendoGrid({
                excel: {
                    fileName: $scope.fileName,
                    allPages: true
                },
                pdf: {
                    paperSize: [2500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    allPages: true,
                    fileName: "Purchase Modified/Deleted Report.pdf",
                    margin: { top: "5cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "instanceName", title: "Branch", width: "100px", format: "{0}", type: "string", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-left field-report" } },
                  { field: "createdAt", title: "Date", width: "120px", type: "date", headerAttributes: { style: "white-space: normal" }, attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(createdAt, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },

                  //{ field: "createdAt", title: "Modified Time", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(createdAt, 'yyyy-MM-dd'), 't') #" },
                  { field: "modifiedTime", title: "Time", width: "120px", headerAttributes: { style: "white-space: normal" }, attributes: { class: "field-primary" } },

                  { field: "updatedByUser", title: "By", width: "120px", headerAttributes: { style: "white-space: normal" }, attributes: { class: "field-report" } },

                  { field: "productStock.statusText", title: "Status", width: "110px", format: "{0}", type: "string", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-left field-report" } },

                  { field: "grnDate", title: "GRN Date", width: "120px", type: "date", headerAttributes: { style: "white-space: normal" }, attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(grnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },

                  { field: "invoiceDate", title: "Invoice Date", width: "120px", type: "date", headerAttributes: { style: "white-space: normal" }, attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },

                  { field: "invoiceNo", title: "Invoice No", width: "100px", format: "{0}", type: "string", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-left field-report" } },
                  { field: "grnNo", title: "GRN No", width: "100px", format: "{0}", type: "string", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-left field-report" } },
                  { field: "vendorName", title: "Vendor Name", width: "100px", format: "{0}", type: "string", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.name", title: "Product Name", width: "110px", format: "{0}", type: "string", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-left field-report" } },
                  { field: "productStock.batchNo", title: "Batch No", width: "100px", format: "{0}", type: "string", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-left field-report" } },
                  { field: "productStock.vat", title: "VAT/GST %", width: "100px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report" } },
                  { field: "productStock.cst", title: "CST %", width: "100px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report" } }, //CST/IGST
                  { field: "productStock.expireDate", title: "Expiry Date", width: "120px", type: "date", headerAttributes: { style: "white-space: normal" }, attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                   { field: "packageSize", title: "Units/Strip", width: "110px", format: "{0}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report" } },
                   { field: "packageQty", title: "No.Strips", width: "100px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report" } },
                   { field: "freeQty", title: "Free Strips", width: "110px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report" } },
                   { field: "packagePurchasePrice", title: "Price/Strip", width: "110px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report" } },
                   { field: "packageSellingPrice", title: "MRP/Strip", width: "110px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                   { field: "quantity", title: "Total Units", width: "110px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report" } },
                   { field: "purchasePrice", title: "Price/Unit", width: "110px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report" } },
                   { field: "productStock.sellingPrice", title: "MRP/Unit", width: "120px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                   { field: "markupPerc", title: "Markup Perc", width: "120px", format: "{0:n}", type: "number", headerAttributes: { style: "white-space: normal" }, attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        //{ field: "reportTotal", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "createdAt": { type: "date" },
                                "modifiedTime": { type: "string" },
                                "updatedByUser": { type: "string" },
                                "grnDate": { type: "date" },
                                "invoiceDate": { type: "date" },
                                "invoiceNo": { type: "string" },
                                "grnNo": { type: "string" },
                                "vendorName": { type: "string" },
                                "productStock.product.name": { type: "string" },
                                "productStock.batchNo": { type: "string" },
                                "productStock.vat": { type: "number" },
                                "productStock.cst": { type: "number" },
                                "productStock.expireDate": { type: "date" },
                                "packageSize": { type: "number" },
                                "packageQty": { type: "number" },
                                "freeQty": { type: "number" },
                                "packagePurchasePrice": { type: "number" },
                                "packageSellingPrice": { type: "number" },
                                "quantity": { type: "number" },
                                "purchasePrice": { type: "number" },
                                "productStock.sellingPrice": { type: "number" },
                                "markupPerc": { type: "number" },
                                "productStock.statusText": { type: "string" },
                                //"reportTotal": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },
                excelExport: function (e) {
                    addHeader(e);
                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }
                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });
    };
    $scope.order = ['instanceName', 'reportCreatedAt', 'modifiedTime', 'updatedByUser', 'productStock.statusText', 'reportGRNDate', 'reportInvoiceDate', 'invoiceNo', 'grnNo', 'vendorName', 'productStock.product.name', 'productStock.batchNo', 'productStock.vat', 'productStock.cst', 'reportExpireDate', 'packageSize', 'packageQty', 'freeQty', 'packagePurchasePrice', 'packageSellingPrice', 'quantity', 'purchasePrice', 'productStock.sellingPrice'];
    //
    //$scope.buyReport();
    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {
            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.buyReport();
    };
    // chng-3
    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "Purchase Modified/Deleted Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Purchase Modified/Deleted Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $("#btnPdfExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").saveAsPDF();
        }
    });


    $("#btnXLSExport").kendoButton(
    {
        click: function () {
            $("#instanceName").text("Qbitz Tech");
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "xlsx";
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });

    $("#btnCSVExport").kendoButton(
    {
        click: function () {
            $("#grid").data("kendoGrid").options.excel.fileName = $scope.fileName + "csv";
            $("#grid").data("kendoGrid").saveAsExcel();
        }
    });
    //
    $scope.setFileName = function () {
        $scope.fileNameNew = "PURCHASE MODIFIED/DELETED REPORT_" + $scope.instance.name;
    }
}]);
