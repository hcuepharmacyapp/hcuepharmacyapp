﻿app.factory('LeadsReportService', function ($http) {
    return {
        list: function (type, data) {
            return $http.post('/LeadsReport/ListData?type=' + type, data);
        },
        getInstanceData: function () {
            return $http.post('/expireProductReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },
    }
});
