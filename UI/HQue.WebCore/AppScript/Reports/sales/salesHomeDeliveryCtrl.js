﻿app.controller('salesHomeDeliveryCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'userAccessModel', '$filter', function ($scope,$rootScope, $http, $interval, $q, salesReportService, salesModel, userAccessModel, $filter) {

    var loadedSales = true;
    var loadedSalesReturn = true;
    var sales = salesModel;
    $scope.search = sales;
    $scope.totalSalesAmount = 0;
    $scope.totalReturnAmount = 0;
    $scope.netDisplay = false; // Added by Sarubala on 13-09-17
    var sno = 0;
    $scope.currentInstance = null;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false;

    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event,id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
        //if ($scope.branchid == null)
        $scope.InvoiceSeriesType = "";
    });
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.pdfHeader = "";

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.type = ''; //'TODAY';

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;                
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.salesReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    // added invoice series filter by Violet on 19/06/2017
    $scope.ChangeInvoiceSeriesDropdown = function (seriestype) {
        //console.log(seriestype);

        $scope.InvoiceSeriesType = seriestype;
        $scope.search.select1 = "";

        if ($scope.InvoiceSeriesType == '1') {
            $scope.search.select1 = "";
            $scope.InvoiceSeriesItems = [];
        }
        if ($scope.InvoiceSeriesType == '2') {
            getInvoiceSeriesItems();
        }
        if ($scope.InvoiceSeriesType == '3') {
            $scope.search.select1 = "MAN";
            $scope.InvoiceSeriesItems = [];
        }
        if ($scope.InvoiceSeriesType == '4') {
            $scope.search.select1 = "MAN";
        }
    }

    $scope.InvoiceSeriesItems = [];
    function getInvoiceSeriesItems() {
        if ($scope.branchid == undefined)
            $scope.branchid = "undefined";
        salesReportService.getInvoiceSeriesItems($scope.InvoiceSeriesType, $scope.branchid).then(function (response) {

            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }

        }, function () {

        });
    }

    $scope.clearSearch = function () {
        $scope.fromInvoice = "";
        $scope.toInvoice = "";
        $scope.InvoiceSeriesType = "";
        $scope.search.select1 = "";
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.salesReport();

    }
    $scope.salesReport = function () {
        $.LoadingOverlay("show");
        $scope.netDisplay = true; // Added by Sarubala on 13-09-17
        var data = {
            fromDate: $scope.from,  
            toDate: $scope.to 
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined;//$scope.instance.id;
        }
        if ($scope.InvoiceSeriesType == undefined || $scope.InvoiceSeriesType == "") {
            $scope.InvoiceSeriesType = "";
        }

        if ($scope.search.select1 == undefined || $scope.search.select1 == "") {
            $scope.search.select1 = "";
        }

        if ($scope.fromInvoice == "") {
            $scope.fromInvoice = "";
        }
        if ($scope.toInvoice == "") {
            $scope.toInvoice = "";
        }
        //$scope.branchid = $scope.instance.id;
        loadedSales = false;
        loadedSalesReturn = false;
        //console.log($scope.branchid);
        $scope.setFileName();
        salesReportService.homeDeliveryList($scope.type, data, $scope.branchid,$scope.InvoiceSeriesType, $scope.search.select1, $scope.fromInvoice, $scope.toInvoice).then(function (response) {
            $scope.data = response.data;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);                
            }
            //add by nandhini for net amount 8/6/17            
            $scope.totalSalesAmount = 0;
            for (var i = 0; i < $scope.data.length; i++) {
                $scope.totalSalesAmount += $scope.data[i].finalValue;
            }
            $scope.netamount = $scope.totalSalesAmount - $scope.returnFinalAmt;
           // $scope.getcalc();
           //end
             var pdfHeader = "";
             if ($scope.instance != undefined) {
                 if (angular.isObject($scope.currentInstance)) {
                     $scope.pdfHeader = $scope.currentInstance;
                     $scope.instance = $scope.currentInstance;
                 }
                 else {
                     $scope.pdfHeader = $scope.instance;
                 }
             } else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>Sales Transaction Wise";
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales Transaction Wise";
            }

            var grid = $("#grid").kendoGrid({
                excel: {
                    fileName: "Sales.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Sales.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                height:280,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                      { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "60px", attributes: { ftype: "sno", class: " text-left field-report" } },
                     { field: "instance.name", title: "Branch", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                     { field: "actualInvoice", title: "Bill No.", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                       { field: "invoiceDate", title: "Bill Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, footerTemplate: "Grand Total", template: "#= kendo.toString(kendo.parseDate(invoiceDate), 'dd/MM/yyyy') #" },
                        { field: "cash", title: "Cash", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                      { field: "card", title: "Card", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                       { field: "credit", title: "Credit", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                       { field: "cheque", title: "Cheque", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                       { field: "ewallet", title: "eWallet", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                       { field: "subPayment", title: "e-Provider", width: "85px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                       { field: "roundoffSaleAmount", title: "RoundOff Value", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                       { field: "finalValue", title: "Net Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span id='totalSalesAmount' name='totalSalesAmount'>#= kendo.toString(sum, 'n2') #</div>" },
                  { field: "name", title: "Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "mobile", title: "Mobile", width: "75px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "email", title: "Email", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                 
               
                  { field: "deliveryType", title: "Delivery Type", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left text-bold" } },

                 

                { field: "createdBy", title: "Transaction By", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } }

                



                

                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                         { field: "cash", aggregate: "sum" },
                         { field: "card", aggregate: "sum" },
                         { field: "credit", aggregate: "sum" },
                         { field: "cheque", aggregate: "sum" },
                         { field: "ewallet", aggregate: "sum" },
                         { field: "finalValue", aggregate: "sum" },
                         { field: "roundoffSaleAmount", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "string" },
                                "invoiceNo": { type: "string" },
                                "invoiceDate": { type: "date" },
                                 "cash": { type: "number" },
                                "card": { type: "number" },
                                "credit": { type: "credit" },
                                "cheque": { type: "number" },
                                "ewallet": { type: "number" },
                                "subPayment": { type: "string" },
                                "finalValue": { type: "number" },
                                "name": { type: "string" },
                                "mobile": { type: "string" },
                                "email": { type: "string" },                               
                                "deliveryType": { type: "string" },                               
                                "createdBy": { type: "string" },
                                "roundoffSaleAmount": { type: "number" },
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            //added by nandhini for excel s.no
                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }
                            //end
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                          
                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                            
                            if (row.type == "data" && (this.columns[ci].title == "Cash" || this.columns[ci].title == "Card" || this.columns[ci].title == "Credit" || this.columns[ci].title == "Net Value")) {
                                cell.format = "#0.00";
                            }
                        }
                    }
                },
            }).data("kendoGrid");

            // Save the reference to the original filter function.
            grid.dataSource.originalFilter = grid.dataSource.filter;

            // Replace the original filter function.
            grid.dataSource.filter = function () {
                // If a column is about to be filtered, then raise a new "filtering" event.
                if (arguments.length > 0) {
                    this.trigger("filtering", arguments);
                }
                // Call the original filter function.
                var result = grid.dataSource.originalFilter.apply(this, arguments);

                // If a column is about to be filtered, then raise a new "filtering" event.
                if (arguments.length > 0) {
                    this.trigger("filtering", result);
                }

                return result;
            }

            // Bind to the dataSource filtering event.
            $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                var filters = dataSource.filter();
                var allData = dataSource.data();
                var query = new kendo.data.Query(allData);
                var data = query.filter(filters).data;
               

            });

            //$scope.totalSalesAmount = parseInt($("#totalSalesAmount").text());
            if (loadedSalesReturn == true) {
                $.LoadingOverlay("hide");
            }
            loadedSales = true;
        }, function () {
            if (loadedSalesReturn == true) {
                $.LoadingOverlay("hide");
            }
        });


        $scope.order = ['select1', 'actualInvoice', 'reportInvoiceDate', 'cash', 'card', 'credit', 'cheque', 'ewallet', 'subPayment', 'finalValue', 'name', 'mobile',
    'email', 'deliveryType', 'createdBy'];

        salesReportService.GetReturnsByDate($scope.type, data, $scope.branchid, $scope.InvoiceSeriesType, $scope.search.select1, $scope.fromInvoice, $scope.toInvoice).then(function (resp) {

            $scope.data = resp.data;
            $scope.type = "";
           
          
            //add by nandhini for net amount 8/6/17
            $scope.amountBeforeDiscountTotal = 0;
            $scope.amountAfterDiscountTotal = 0;
            $scope.returnFinalAmt = 0;
            for (var i = 0; i < $scope.data.length; i++) {
                $scope.amountBeforeDiscountTotal += $scope.data[i].amountBeforeDiscount;
                $scope.amountAfterDiscountTotal += $scope.data[i].amountAfterDiscount;
                $scope.returnFinalAmt += $scope.data[i].returnedPrice;
            }
          
            $scope.netamount = $scope.totalSalesAmount - $scope.returnFinalAmt;

          //end


            var SalesReturnGrid = $("#SalesReturnGrid").kendoGrid({

                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height:220,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                       { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "60px", attributes: { ftype: "sno", class: " text-left field-report" } },
                   { field: "instance.name", title: "Branch", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                        { field: "returnNo", title: "Return No", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  
                        { field: "returnDate", title: "Return Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, footerTemplate: "Grand Total", template: "#= kendo.toString(kendo.parseDate(returnDate), 'dd/MM/yyyy') #" },
                          
                    
                    { field: "sales.cash", title: "Cash", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</div>" },

                       { field: "sales.credit", title: "Credit", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</div>" },

                       { field: "returnCharges", title: "Return Charges", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</div>" },
                    { field: "roundOffNetAmount", title: "RoundOff Value", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</div>" },
                    { field: "returnedPrice", title: "Net Return Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span id='totalReturnAmount' name='totalReturnAmount'>#= kendo.toString(sum, 'n2') #</div>" },
                         { field: "sales.actualInvoice", title: "Bill No.", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                  { field: "sales.name", title: "Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                  { field: "sales.mobile", title: "Mobile", width: "70px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                      { field: "sales.createdBy", title: "Transaction By", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } }

                ],
                dataSource: {
                    data: resp.data,
                    aggregate: [
                         { field: "returnedPrice", aggregate: "sum" },
                         { field: "sales.cash", aggregate: "sum" },
                         { field: "sales.credit", aggregate: "sum" },
                        { field: "returnCharges", aggregate: "sum" },
                        { field: "roundOffNetAmount", aggregate: "sum" },
                 
                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "string" },
                                "returnNo": { type: "string" },                              
                                "returnDate": { type: "date" },                                                              
                                "cash": { type: "number" },
                                "credit": { type: "number" },
                                "returnCharges": {type: "number"},
                                "returnedPrice": { type: "number" },
                                "invoiceNo": { type: "string" },
                                "name": { type: "string" },
                                "mobile": { type: "string" },                                
                                "createdBy": { type: "string" },
                                "roundOffNetAmount": { type: "number" },
                              
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
            }).data("kendoGrid");


            SalesReturnGrid.dataSource.originalFilter = SalesReturnGrid.dataSource.filter;


            SalesReturnGrid.dataSource.filter = function () {

                if (arguments.length > 0) {
                    this.trigger("filtering", arguments);
                }

                var result = SalesReturnGrid.dataSource.originalFilter.apply(this, arguments);


                if (arguments.length > 0) {
                    this.trigger("filtering", result);
                }
                return result;
            }

            
            $("#SalesReturnGrid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                var dataSource = $("#SalesReturnGrid").data("kendoGrid").dataSource;
                var filters = dataSource.filter();
                var allData = dataSource.data();
                var query = new kendo.data.Query(allData);
                var data = query.filter(filters).data;
            });

            if (loadedSales == true) {
                $.LoadingOverlay("hide");
            }
            loadedSalesReturn = true;

            $scope.totalSalesAmount = document.getElementById('totalSalesAmount').innerHTML;
            $scope.totalReturnAmount = document.getElementById('totalReturnAmount').innerHTML; // parseInt($("#totalReturnAmount").text());
            $scope.totalSalesAmount1 = parseInt($scope.totalSalesAmount.replace(/,/g, ''));
            $scope.netamount = parseFloat($scope.totalSalesAmount.replace(/,/g, '')) - parseFloat($scope.totalReturnAmount.replace(/,/g, ''));          
            
        }, function () {
            if (loadedSales == true) {
                $.LoadingOverlay("hide");
            }
        });
    }
    //End Chng 2
   


    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "TODAY") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.salesReport(); 
    };

    // chng-3

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Sales Transaction Wise", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Sales Transaction Wise", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }
    $scope.setFileName = function () {       
        $scope.fileName = "SalesTransactionWise";
    }
    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');
        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));
        
        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);

