﻿
app.controller('salesProductWiseReportCtrl', ['$scope', 'uiGridConstants', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'salesItemModel', '$filter', 'productStockService', 'salesService', function ($scope, uiGridConstants, $http, $interval, $q, salesReportService, salesModel, salesItemModel, $filter, productStockService, salesService) {

    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months
    var isComposite = false;

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.checkFromDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#fromDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if (toDate != undefined && toDate != null) {
                    $scope.checkToDate1(frmDate, toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt1 = $("#toDate1").val();

        if (dt1.length == 10 && dt1.charAt(2) == '/' && dt1.charAt(5) == '/') {
            if (isValidDate(dt1)) {
                $scope.validToDate = true;
                $scope.checkToDate1(frmDate, toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.Customer = {
        "Name": "",
        "Id": ""
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.DisableButton = true;
    
    
    $scope.getProducts = function (val) {
      
            return productStockService.getAllDrugFilterData(val).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
      
    };
   
    $scope.ProductId = "";
    $scope.onProductSelect = function (obj) {
        $scope.ProductId = obj.product.id;
    }

    
    $scope.OnPatientNameblur = function () {
        console.log($scope.Customer.Name);

        if ($scope.Customer.Name == null || $scope.Customer.Name == "") {
            $scope.CustomerName = "";
            $scope.CustomerMobile = "";
        } 
      
    }

   
    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            isComposite = $scope.instance.gstselect;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    };

    $scope.salesReport = function () {

        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }
        
        salesReportService.ProductWiseDetailListInSales( $scope.ProductId, $scope.CustomerName, $scope.CustomerMobile, $scope.branchid, data).then(function (response) { 
                if ($scope.from != undefined || $scope.from != null) {
                    $scope.dayDiff($scope.from);
                }
                console.log(JSON.stringify(response.data));
                $scope.data = response.data;
                //$scope.type = "";
                var totalCash = 0;
                for (var i = 0; i < $scope.data.length; i++) {
                    totalCash += $scope.data[i].reportTotal;
                }

                var total = parseInt(totalCash);
                $scope.total = total;

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    salesReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                    // export file header for all branch
                    if ($scope.branchid == undefined)
                        pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Product Wise Details";
                }

                $("#grid").kendoGrid({
                    excel: {
                        fileName: "ProductWise Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [2500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "ProductWise_Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                        { field: "sales.instance.name", title: "Branch", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-left field-highlight" } },
                        { field: "productStock.product.name", title: "Product", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-highlight" } },
                              { field: "productStock.product.manufacturer", title: "Manufacturer", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "productStock.product.schedule", title: "Schedule", width: "130px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "Grand Total" },
                      { field: "productStock.product.type", title: "Type", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                    { field: "sales.actualInvoice", title: "Bill No", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "sales.invoiceDate", title: "Bill Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                      { field: "quantity", title: "Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                      { field: "sellingPrice", title: "Selling Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                        { field: "productStock.purchasePrice", title: "Cost Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                      { field: "productStock.vat", title: "VAT/GST %", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },

                      { field: "gstAmount", title: "VAT/GST Amount", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                       { field: "itemAmount", title: "Amount without Tax", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                      { field: "discountAmount", title: "Discount Value", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                       { field: "productStock.totalCostPrice", title: "Total Cost Price", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                      { field: "reportTotal", title: "Final Value", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                      { field: "salesProfit", title: "Profit %", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'><span>#= kendo.toString(sum, 'n2') #</span></div>" },
                      { field: "productStock.batchNo", title: "Batch", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "productStock.expireDate", title: "Expiry", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                     { field: "sales.name", title: "Patient Name", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "sales.doctorName", title: "Doctor Name", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "sales.address", title: "Patient Address", width: "150px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "sales.credit", title: "Is Credit", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } }
                      ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                           { field: "quantity", aggregate: "sum" },
                           { field: "sellingPrice", aggregate: "sum" },
                           { field: "productStock.purchasePrice", aggregate: "sum" },
                            { field: "productStock.vat", aggregate: "sum" },
                           { field: "gstAmount", aggregate: "sum" },
                           { field: "itemAmount", aggregate: "sum" },
                          { field: "discountAmount", aggregate: "sum" },
                             { field: "productStock.totalCostPrice", aggregate: "sum" },
                       { field: "reportTotal", aggregate: "sum" },
                       
                        { field: "salesProfit", aggregate: "sum" },

                        ],
                        schema: {
                            model: {
                                fields: {
                                    "productStock.product.name": { type: "string" },
                                    "sales.instance.name": { type: "string" },
                                    "productStock.product.manufacturer": { type: "string" },
                                    "productStock.product.schedule": { type: "string" },
                                    "productStock.product.type": { type: "string" },
                                    "sales.actualInvoice": { type: "number" },
                                    "sales.invoiceDate": { type: "date" },
                                    "quantity": { type: "number" },
                                    "sellingPrice": { type: "number" },
                                    "productStock.purchasePrice": { type: "number" },
                                    "productStock.vat": { type: "number" },
                                    "itemAmount": { type: "number" },
                                    "gstAmount": { type: "number" },
                                    "discountAmount": { type: "number" },
                                    "productStock.totalCostPrice": { type: "number" },
                                    "reportTotal": { type: "number" },
                                    "salesProfit": { type: "number" },
                                    "productStock.batchNo": { type: "number" },
                                    "productStock.expireDate": { type: "date" },
                                    "sales.name": { type: "string" },
                                    "sales.doctorName": { type: "string" },
                                    "sales.address": { type: "string" },
                                    "sales.credit": { type: "number" }
                                  
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
     
     
       
    }

    $scope.getPatientName = function (val) {

        return salesService.GetPatientName(val).then(function (response) {


            var origArr = response.data;

            var newArr = [],
       origLen = origArr.length,
       found, x, y;
            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if (origArr[x].mobile === newArr[y].mobile && origArr[x].name === newArr[y].name) {
                        found = true;
                        break;
                    }

                }
                if (!found) {
                    if (origArr[x].mobile == "") {

                    } else {
                        newArr.push(origArr[x]);
                    }

                }
            }


            return newArr.map(function (item) {
                return item;
            });
        });
    };
       $scope.CustomerName = "";
    $scope.CustomerMobile = "";
    $scope.onPatientSelect = function (obj) {
        console.log(JSON.stringify(obj));

        $scope.CustomerName = obj.name;
        $scope.CustomerMobile = obj.mobile;
    }
  
  
    $scope.filter = function (type) {
        $scope.type = type;
        $scope.salesReport();
    }
    $scope.PatientName = "";
    $scope.PatientId = "";
       $scope.selectedProduct = {
           "name":""
        };
    $scope.clearSearch = function () {
        $("#grid").empty();
        $scope.search.select = "";
        $scope.selectedProduct = {
           "name":""
        };
        $scope.from = "";
        $scope.to = "";
        $scope.CustomerName = "";
        $scope.CustomerMobile = "";


        $scope.Customer = {
            "Name": "",
            "Id": ""
        };
        $scope.DisableButton = true;
        $scope.data = [];
    }





    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "ProductWise Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Sales Customer Wise Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    //
    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);

