﻿app.controller('DateWiseSalesReportCtrl', function ($scope, $rootScope, $filter, salesReportService, patientService, branchServcie) {

    $scope.myclass = "margin-top-25";
    $scope.gridLevelName = "grid";
    $scope.instanceListData = [];
    $scope.search = {
        select: "",
        select2: "equal",
        select1: "",
        values: "",
    };
    $scope.InvoiceSeriesType = "";
    var instanceIds = "";
    var sales = "";
    $scope.level = null;
    var isComposite = false;

    $scope.$on('branchname', function (event, id) {
        $scope.branchid = id;
    });

    $scope.$on('instanceList', function (event, instanceList) {
        $scope.instanceListData = instanceList;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        var dateVal = $("#fromDate").val();
        if (isValidDate(dateVal) == false || dateVal.length < 10)
            $scope.from = "";
        $scope.popup1.opened = true;
    };

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;
    $scope.inValidMinFromDate = false;
    var today = new Date();

    $scope.checkDateChar = function (val) {
        var actualKey = val.key;
        val.key = val.key.replace(/[^0-9/]+/g, '');
        if (actualKey != val.key)
            event.preventDefault();
    }

    $scope.checkFromDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#fromDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                var bits = dt.split('/');
                var userDate = new Date(bits[2], bits[1] - 1, bits[0]);
                if (isValidDate(dt) && userDate <= today) {
                    $scope.validFromDate = true;

                    if (toDate != undefined && toDate != null) {
                        $scope.checkToDate1(frmDate, toDate);
                    }
                }
                else {
                    $scope.validFromDate = false;
                }
                if (userDate < $scope.minDate) {
                    $scope.inValidMinFromDate = true;
                }
                else {
                    $scope.inValidMinFromDate = false;
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else
            $scope.validFromDate = true;
    }

    $scope.checkToDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#toDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                var bits = dt.split('/');
                var userDate = new Date(bits[2], bits[1] - 1, bits[0]);
                if (isValidDate(dt) && userDate <= today) {
                    $scope.validToDate = true;
                    $scope.checkToDate1(frmDate, toDate);
                }
                else {
                    $scope.validToDate = false;
                }
            }
            else {
                $scope.validToDate = false;
            }
        }
        else
            $scope.validToDate = true;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        var dateVal = $("#toDate").val();
        if (isValidDate(dateVal) == false || dateVal.length < 10)
            $scope.to = "";
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.data = [];
    $scope.type = 'TODAY';
    $scope.maxDate = new Date();
    $scope.minDate = new Date("2017", "06", "01");

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $scope.instance = response.data;
            isComposite = $scope.instance.gstselect;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance, true);
            if ($scope.instanceListData.length == 0) {
                $scope.instanceListData.push($scope.instance);
            }
            getInvoiceSeriesItems();
            //if ($scope.instanceListData.length == 0) {
            //    branchServcie.selectInstance().then(function (response) {
            //        $rootScope.$broadcast('LoadMultiBranch', response.data);
            //        $scope.salesBookReport();
            //    });
            //} else {
            //    $scope.salesBookReport();
            //}            
            fillGridData();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });

        $.LoadingOverlay("show");
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.salesBookReport = function () {
        if ($scope.instanceListData.length == 0) {
            alert("Please select branch and try again.");
            return;
        }
        $.LoadingOverlay("show");
        SetDefaultValues($scope.from, $scope.to);

        salesReportService.DateWiseSalesReportData($scope.type, sales, instanceIds, 1).then(function (response) {
            $scope.data = response.data.item1;
            fillGridData();

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    function fillGridData() {
        $scope.type = "";
        $scope.level = 1;

        $scope.fileTitle = "Date Wise Sales Report";
        $("#grid").empty();
        $("#grid").kendoGrid({
            excel: {
                fileName: $scope.fileTitle + ".xlsx",
                allPages: true
            },
            pdf: {
                paperSize: [1500, 1000],
                landscape: true,
                allPages: true,
                fileName: $scope.fileTitle + ".pdf",
                margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                landscape: true,
                multiPage: true,
                template: "<div class='pdfHeader' style='top:10px;position:absolute;'>" + GetPdfHeader() + $scope.fileTitle + "</div>" + $("#page-template").html()
            },
            columnMenu: true,
            pageable: false,
            resizable: true,
            reorderable: true,
            sortable: true,
            filterable: {
                mode: "column"
            },
            columns: [
                    { field: "invoiceDate", title: "Date", width: "60px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", footerTemplate: "Grand Total" },
                    { field: "invoiceAmountNoTaxNoDiscount", title: "Sale Amt WOT GST", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                    { field: "taxAmount", title: "GST Amt", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                    //{ field: "discountValue", title: "Discount Amt", width: "70px", format: "{0:n}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                    { field: "roundoffNetAmount", title: "Round off", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') == null ? 0 : kendo.toString(sum, 'n2') #</div>" },
                    { field: "invoiceAmount", title: "Sale Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                    { field: "purchasePrice", title: "Net Cost Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                    //{ field: "profitOnCost", title: "Profit % on Cost", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= (data.invoiceAmount.sum > 0 ? (kendo.toString(((data.invoiceAmount.sum - data.purchasePrice.sum)/data.purchasePrice.sum) * 100, 'n2')) : '0.00') #</div>", aggregates: ["sum"] },
                    { field: "profitOnMrp", title: "Margin %", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= (data.invoiceAmount.sum != 0 ? (kendo.toString(((data.invoiceAmount.sum - data.purchasePrice.sum)/data.invoiceAmount.sum) * 100, 'n2')) : '0.00') #</div>", aggregates: ["sum"] },
            ],
            dataSource: {
                data: $scope.data,
                aggregate: [
                    { field: "invoiceAmountNoTaxNoDiscount", aggregate: "sum" },
                    { field: "taxAmount", aggregate: "sum" },
                    { field: "discountValue", aggregate: "sum" },
                    { field: "roundoffNetAmount", aggregate: "sum" },
                    { field: "invoiceAmount", aggregate: "sum" },
                    { field: "purchasePrice", aggregate: "sum" },
                    { field: "profitOnCost", aggregate: "sum" },
                    { field: "profitOnMrp", aggregate: "sum" },
                ],
                schema: {
                    model: {
                        fields: {
                            "invoiceDate": { type: "Date" },
                            "invoiceAmountNoTaxNoDiscount": { type: "number" },
                            "taxAmount": { type: "number" },
                            "discountValue": { type: "number" },
                            "roundoffNetAmount": { type: "number" },
                            "invoiceAmount": { type: "number" },
                            "purchasePrice": { type: "number" },
                            "profitOnCost": { type: "number" },
                            "profitOnMrp": { type: "number" },
                        }
                    }
                },
            },
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            detailInit: function (e) {
            },
            dataBound: onDataBound,
            excelExport: function (e) {
                $scope.fileTitle = "Date Wise Sales Report";
                addHeader(e);
                addFooter(e);
            },
        });
    };

    $scope.clearSearch = function () {
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.data = [];
        $("#grid").data('kendoGrid').dataSource.data([]);
        $rootScope.$broadcast('ResetMultiBranch');
        $scope.resetFilters();
        $scope.search.select = "";
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;
        $scope.inValidMinFromDate = false;
    };

    $scope.filter = function (type) {
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;
        $scope.inValidMinFromDate = false;
        $scope.type = type;
        if ($scope.type === "TODAY") {
            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.salesBookReport();
    };

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);

        if ($scope.level == 3) {
            headerCell = { cells: [{ value: e.data[0].instance.name, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }

        headerCell = { cells: [{ value: $scope.fileTitle, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    };

    function addFooter(e) {
        var grid = $('#' + $scope.gridLevelName).data('kendoGrid');
        var sheet = e.workbook.sheets[0];
        for (var i = 0; i < sheet.rows.length; i++) {
            var row = sheet.rows[i];
            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                var cell = row.cells[ci];
                if (grid.columns[ci].attributes.ftype == "sno" && i == 3) {
                    sno = 0;
                }
                if (row.type == "data" && grid.columns[ci].attributes.ftype == "sno") {
                    cell.hAlign = "left";
                    sno = sno + 1;
                    cell.value = sno;
                }
                cell.width = "300";

                if (row.type == "data" && grid.columns[ci].attributes.ftype == "date") {
                    cell.hAlign = "left";
                    cell.format = grid.columns[ci].attributes.fformat;
                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                }
                if (row.type == "data" && grid.columns[ci].attributes.ftype == "number") {
                    cell.hAlign = "right";
                    cell.format = "#" + grid.columns[ci].attributes.fformat;
                }

                if (row.type == "group-footer" || row.type == "footer") {
                    if (cell.value) {
                        cell.value = $.trim($('<div>').html(cell.value).text());
                        cell.value = cell.value.replace('Total:', '');
                        cell.hAlign = "right";
                        cell.format = "#0.00";
                        cell.bold = true;
                    }
                }
            }
        }
    };

    function GetPdfHeader() {
        var pdfHeaderData = "";
        //if ($scope.instance != undefined) {
        //    $scope.pdfHeader = $scope.instance;
        //}
        if ($scope.instanceListData != undefined && $scope.instanceListData != null && $scope.instanceListData.length != 0) {
            if ($scope.instanceListData.length == 1) {
                $scope.pdfHeader = $scope.instanceListData[0];
                $scope.instance = $scope.instanceListData[0];
            }
            else {
                $scope.pdfHeader = {};
                $scope.instance = {};
                $scope.instance.drugLicenseNo = "";
                $scope.instance.gsTinNo = "";
                $scope.instance.fullAddress = "";

                $scope.pdfHeader.name = "Multi Level Sales Report";
                $scope.instance.name = "Multi Level Sales Report";
            }

        }
        else {
            salesReportService.getInstanceData().then(function (pdfResponse) {
                $scope.pdfHeader = pdfResponse.data;
            }, function () { });
        }

        if ($scope.pdfHeader) {
            if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                $scope.pdfHeader.drugLicenseNo = "";
            else
                $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

            if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                $scope.pdfHeader.gsTinNo = "";
            else
                $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

            if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                $scope.pdfHeader.fullAddress = "";
            else
                $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

            pdfHeaderData = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>";
            return pdfHeaderData;
        }
    };

    function detailInit(e) {
        $.LoadingOverlay("show");
        SetDefaultValues($filter('date')(e.data.invoiceDate, 'yyyy-MM-dd'), $filter('date')(e.data.invoiceDate, 'yyyy-MM-dd'));

        salesReportService.DateWiseSalesReportData($scope.type, sales, instanceIds, 2).then(function (response) {
            $scope.data1 = response.data.item1;
            $scope.gridLevelName = "grid1";
            $scope.myclass = "";
            $scope.level = 2;

            $scope.treeViewHeader1 = $filter('date')(e.data.invoiceDate, 'dd/MM/yyyy') + " >> ";
            $scope.treeViewHeader = $scope.treeViewHeader1;
            $scope.fileTitle = "Branch Wise Sales Report";
            //$("<div/>").appendTo(e.detailCell).kendoGrid({
            //    toolbar: ["excel", "pdf"],
            $("#grid1").empty();
            $("#grid1").kendoGrid({
                excel: {
                    fileName: $scope.fileTitle + ".xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000],
                    landscape: true,
                    allPages: true,
                    fileName: $scope.fileTitle + ".pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute;'>" + GetPdfHeader() + $scope.fileTitle + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: false,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                        { field: "instance.name", title: "Branch", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" }, footerTemplate: "Grand Total" },
                        { field: "invoiceAmountNoTaxNoDiscount", title: "Sale Amt WOT GST", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        { field: "taxAmount", title: "GST Amt", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        //{ field: "discountValue", title: "Discount Amt", width: "70px", format: "{0:n}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        { field: "roundoffNetAmount", title: "Round off", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') == null ? 0 : kendo.toString(sum, 'n2') #</div>" },
                        { field: "invoiceAmount", title: "Sale Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        { field: "purchasePrice", title: "Net Cost Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        //{ field: "profitOnCost", title: "Profit % on Cost", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= (data.invoiceAmount.sum > 0 ? (kendo.toString(((data.invoiceAmount.sum - data.purchasePrice.sum)/data.purchasePrice.sum) * 100, 'n2')) : '0.00') #</div>", aggregates: ["sum"] },
                        { field: "profitOnMrp", title: "Margin %", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= (data.invoiceAmount.sum != 0 ? (kendo.toString(((data.invoiceAmount.sum - data.purchasePrice.sum)/data.invoiceAmount.sum) * 100, 'n2')) : '0.00') #</div>", aggregates: ["sum"] },
                ],
                dataSource: {
                    data: $scope.data1,
                    aggregate: [
                        { field: "invoiceAmountNoTaxNoDiscount", aggregate: "sum" },
                        { field: "taxAmount", aggregate: "sum" },
                        { field: "discountValue", aggregate: "sum" },
                        { field: "roundoffNetAmount", aggregate: "sum" },
                        { field: "invoiceAmount", aggregate: "sum" },
                        { field: "purchasePrice", aggregate: "sum" },
                        { field: "profitOnCost", aggregate: "sum" },
                        { field: "profitOnMrp", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                                "instance.name": { type: "string" },
                                "invoiceAmountNoTaxNoDiscount": { type: "number" },
                                "taxAmount": { type: "number" },
                                "discountValue": { type: "number" },
                                "roundoffNetAmount": { type: "number" },
                                "invoiceAmount": { type: "number" },
                                "purchasePrice": { type: "number" },
                                "profitOnCost": { type: "number" },
                                "profitOnMrp": { type: "number" },
                            }
                        }
                    },
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                detailInit: function (e) {
                },
                dataBound: onDataBound,
                excelExport: function (e) {
                    $scope.fileTitle = "Branch Wise Sales Report";
                    addHeader(e);
                    addFooter(e);
                },
            });

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    function detailInit1(e) {
        $.LoadingOverlay("show");
        SetDefaultValues($filter('date')(e.data.invoiceDate, 'yyyy-MM-dd'), $filter('date')(e.data.invoiceDate, 'yyyy-MM-dd'));

        salesReportService.DateWiseSalesReportData($scope.type, sales, e.data.instance.id, 3).then(function (response) {
            $scope.data2 = response.data.item1;
            $scope.gridLevelName = "grid2";
            $scope.myclass = "";
            $scope.level = 3;

            $scope.treeViewHeader2 = $filter('date')(e.data.invoiceDate, 'dd/MM/yyyy') + " >> " + e.data.instance.name + " >> ";
            $scope.treeViewHeader = $scope.treeViewHeader2;
            $scope.fileTitle = "Invoice Wise Sales Report";
            //$("<div/>").appendTo(e.detailCell).kendoGrid({
            //        toolbar: ["excel", "pdf"],
            $("#grid2").empty();
            $("#grid2").kendoGrid({
                excel: {
                    fileName: $scope.fileTitle + ".xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000],
                    landscape: true,
                    allPages: true,
                    fileName: $scope.fileTitle + ".pdf",
                    margin: { top: "5cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute;'>" + GetPdfHeader() + $scope.fileTitle + "<br/>" + e.data.instance.name + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: false,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                        { field: "invoiceDate", title: "Date", width: "60px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                        { field: "actualInvoice", title: "Bill No", width: "60px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                        { field: "name", title: "Customer", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" }, footerTemplate: "Grand Total" },
                        { field: "invoiceAmountNoTaxNoDiscount", title: "Sale Amt WOT GST", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        { field: "taxAmount", title: "GST Amt", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        //{ field: "discountValue", title: "Discount Amt", width: "70px", format: "{0:n}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        { field: "roundoffNetAmount", title: "Round off", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') == null ? 0 : kendo.toString(sum, 'n2') #</div>" },
                        { field: "invoiceAmount", title: "Sale Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        { field: "purchasePrice", title: "Net Cost Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        //{ field: "profitOnCost", title: "Profit % on Cost", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= (data.invoiceAmount.sum > 0 ? (kendo.toString(((data.invoiceAmount.sum - data.purchasePrice.sum)/data.purchasePrice.sum) * 100, 'n2')) : '0.00') #</div>", aggregates: ["sum"] },
                        { field: "profitOnMrp", title: "Margin %", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= (data.invoiceAmount.sum != 0 ? (kendo.toString(((data.invoiceAmount.sum - data.purchasePrice.sum)/data.invoiceAmount.sum) * 100, 'n2')) : '0.00') #</div>", aggregates: ["sum"] },
                ],
                dataSource: {
                    data: $scope.data2,
                    aggregate: [
                        { field: "invoiceAmountNoTaxNoDiscount", aggregate: "sum" },
                        { field: "taxAmount", aggregate: "sum" },
                        { field: "discountValue", aggregate: "sum" },
                        { field: "roundoffNetAmount", aggregate: "sum" },
                        { field: "invoiceAmount", aggregate: "sum" },
                        { field: "purchasePrice", aggregate: "sum" },
                        { field: "profitOnCost", aggregate: "sum" },
                        { field: "profitOnMrp", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                                "invoiceDate": { type: "string" },
                                "actualInvoice": { type: "string" },
                                "name": { type: "string" },
                                "invoiceAmountNoTaxNoDiscount": { type: "number" },
                                "taxAmount": { type: "number" },
                                "discountValue": { type: "number" },
                                "roundoffNetAmount": { type: "number" },
                                "invoiceAmount": { type: "number" },
                                "purchasePrice": { type: "number" },
                                "profitOnCost": { type: "number" },
                                "profitOnMrp": { type: "number" },
                            }
                        }
                    },
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                excelExport: function (e) {
                    $scope.fileTitle = "Invoice Wise Sales Report";
                    addHeader(e);
                    addFooter(e);
                },
            });

            //For returns
            $scope.data3 = response.data.item2;
            $("#grid3").empty();
            $("#grid3").kendoGrid({
                excel: {
                    fileName: $scope.fileTitle + ".xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000],
                    landscape: true,
                    allPages: true,
                    fileName: $scope.fileTitle + ".pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute;'>" + GetPdfHeader() + $scope.fileTitle + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: false,
                resizable: true,
                reorderable: true,
                sortable: true,
                scrollable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                        { field: "invoiceDate", title: "Date", width: "60px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                        { field: "actualInvoice", title: "Return No", width: "60px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                        { field: "name", title: "Customer", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" }, footerTemplate: "Grand Total" },
                        { field: "invoiceAmountNoTaxNoDiscount", title: "Return Amt WOT GST", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        { field: "taxAmount", title: "GST Amt", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        //{ field: "discountValue", title: "Discount Amt", width: "70px", format: "{0:n}", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        { field: "roundoffNetAmount", title: "Round off", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') == null ? 0 : kendo.toString(sum, 'n2') #</div>" },
                        { field: "invoiceAmount", title: "Return Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        { field: "purchasePrice", title: "Net Cost Amount", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                        //{ field: "profitOnCost", title: "Profit % on Cost", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= (data.invoiceAmount.sum > 0 ? (kendo.toString(((data.invoiceAmount.sum - data.purchasePrice.sum)/data.purchasePrice.sum) * 100, 'n2')) : '0.00') #</div>", aggregates: ["sum"] },
                        { field: "profitOnMrp", title: "Margin %", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= (data.invoiceAmount.sum != 0 ? (kendo.toString(((data.invoiceAmount.sum - data.purchasePrice.sum)/data.invoiceAmount.sum) * 100, 'n2')) : '0.00') #</div>", aggregates: ["sum"] },
                ],
                dataSource: {
                    data: $scope.data3,
                    aggregate: [
                        { field: "invoiceAmountNoTaxNoDiscount", aggregate: "sum" },
                        { field: "taxAmount", aggregate: "sum" },
                        { field: "discountValue", aggregate: "sum" },
                        { field: "roundoffNetAmount", aggregate: "sum" },
                        { field: "invoiceAmount", aggregate: "sum" },
                        { field: "purchasePrice", aggregate: "sum" },
                        { field: "profitOnCost", aggregate: "sum" },
                        { field: "profitOnMrp", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                                "invoiceDate": { type: "string" },
                                "actualInvoice": { type: "string" },
                                "name": { type: "string" },
                                "invoiceAmountNoTaxNoDiscount": { type: "number" },
                                "taxAmount": { type: "number" },
                                "discountValue": { type: "number" },
                                "roundoffNetAmount": { type: "number" },
                                "invoiceAmount": { type: "number" },
                                "purchasePrice": { type: "number" },
                                "profitOnCost": { type: "number" },
                                "profitOnMrp": { type: "number" },
                            }
                        }
                    },
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                excelExport: function (e) {
                    $scope.fileTitle = "Invoice Wise Sales Report";
                    addHeader(e);
                    addFooter(e);
                },
            });

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    function SetDefaultValues(fromDate, toDate) {
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        instanceIds = "";
        if ($scope.instanceListData.length == 0) {
            instanceIds = $scope.branchid;
        }
        else {
            for (var i = 0; i < $scope.instanceListData.length; i++) {
                if (instanceIds != "")
                    instanceIds = instanceIds + ",";
                if ($scope.instanceListData[i].id != undefined)
                    instanceIds = instanceIds + $scope.instanceListData[i].id;
                else
                    instanceIds = instanceIds + $scope.instanceListData[i];
            }
        }
        sales = {
            invoiceDate: fromDate,
            salesCreatedAt: toDate,
            select: $scope.search.select,
            select1: $scope.search.select1,
            values: $scope.search.values,
        };
        if (typeof ($scope.search.values) == "object") {
            if ($scope.search.select == "customerName") {
                sales.values = $scope.search.values.id;
            }
        }
        if ($scope.search.select == "gstTotal" || $scope.search.select == 'profitOnCost' || $scope.search.select == 'profitOnMrp') {
            sales.select1 = $scope.search.select2;
        }
        if ($scope.search.select == "billNo") {
            var InvoiceSeries = "";
            if ($scope.InvoiceSeriesType == "") {
                InvoiceSeries = "ALL"
            }
            else if ($scope.InvoiceSeriesType == "Custom") {
                if ($scope.search.select1 == "") {
                    for (var i = 0; i < $scope.InvoiceSeriesItems.length; i++) {
                        if (InvoiceSeries != "")
                            InvoiceSeries = InvoiceSeries + ",";
                        InvoiceSeries = InvoiceSeries + $scope.InvoiceSeriesItems[i].seriesName;
                    }
                }
                else {
                    InvoiceSeries = $scope.search.select1;
                }
            }
            else if ($scope.InvoiceSeriesType == "Default") {
                InvoiceSeries = "";
            }
            else if ($scope.InvoiceSeriesType == "Manual") {
                InvoiceSeries = "MAN";
            }
            sales.select1 = InvoiceSeries;
        }
    };

    $scope.getPatient = function (val) {
        return patientService.GetSalesPatientName(val, 1).then(function (response) {

            var origArr = response.data;
            var newArr = [],
            origLen = origArr.length,
            found, x, y;

            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if ($filter('uppercase')(origArr[x].id) === $filter('uppercase')(newArr[y].id)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }

            return newArr.map(function (item) {
                return item;
            });
        });
    };

    $scope.resetFilters = function () {
        $scope.search.select2 = "equal";
        $scope.search.select1 = "";
        $scope.search.values = "";
        $scope.InvoiceSeriesType = "";
        instanceIds = "";
        sales = "";
    };

    $scope.InvoiceSeriesItems = [];
    function getInvoiceSeriesItems() {
        salesReportService.getInvoiceSeriesItems(2).then(function (response) {

            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }

        }, function () {

        });
    };

    $("#btnPdfExport").kendoButton(
    {
        click: function () {
            var grid = $("#" + $scope.gridLevelName).data('kendoGrid');
            //var rows = grid.tbody.find('>tr.k-master-row');
            //for (var i = 0; i < rows.length; i++) {
            //    grid.collapseRow(rows.eq(i));
            //}
            //grid.refresh();
            grid.saveAsPDF();
        }
    });
    $("#btnXLSExport").kendoButton(
    {
        click: function () {
            $("#instanceName").text("Qbitz Tech");
            $("#" + $scope.gridLevelName).data("kendoGrid").saveAsExcel();
        }
    });

    var expandedRows = 0;
    $scope.closeDetailReport = function () {
        if ($scope.gridLevelName == 'grid2') {
            $scope.gridLevelName = 'grid1';
            $scope.fileTitle = "Branch Wise Sales Report";
            $scope.treeViewHeader = $scope.treeViewHeader1;
            $scope.myclass = "";
        }
        else if ($scope.gridLevelName == 'grid1') {
            $scope.gridLevelName = 'grid';
            $scope.fileTitle = "Invoice Wise Sales Report";
            $scope.myclass = "margin-top-25";
            $scope.treeViewHeader = "";
        }

        var grid = $("#" + $scope.gridLevelName).data('kendoGrid');
        var rows = grid.tbody.find('>tr.k-master-row');
        for (var i = 0; i < rows.length; i++) {
            grid.collapseRow(rows.eq(i));
        }
        expandedRows--;
    };

    var onDataBound = function () {
        var grid = $("#" + $scope.gridLevelName).data("kendoGrid");

        $(grid.tbody).on("click", "td", function (e) {
            var row = $(this).closest("tr");
            var rowIdx = $("tr", grid.tbody).index(row);
            var colIdx = $("td", row).index(this);

            if (colIdx == 0 && (expandedRows == 0 && $scope.gridLevelName == 'grid' || expandedRows == 1 && $scope.gridLevelName == 'grid1')) {
                expandedRows++;
                var item = grid.dataItem(row);
                e.data = item;
                //console.log(e);
                if ($scope.gridLevelName == 'grid')
                    detailInit(e);
                else if ($scope.gridLevelName == 'grid1')
                    detailInit1(e);
                e.preventDefault();
            }
        });
    };

});