﻿app.controller('NonSaleCustomersCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'salesReportService', 'patientModel', '$filter'
    , function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, salesReportService, patientModel, $filter) {

    //var salesItem = salesItemModel;
    var patient = patientModel;
    //$scope.search = salesItem;
    $scope.search = patient;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.checkFromDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#fromDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if (toDate != undefined && toDate != null) {
                    $scope.checkToDate1(frmDate, toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt1 = $("#toDate1").val();

        if (dt1.length == 10 && dt1.charAt(2) == '/' && dt1.charAt(5) == '/') {
            if (isValidDate(dt1)) {
                $scope.validToDate = true;
                $scope.checkToDate1(frmDate, toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.Customer = {
        "Name": "",
        "Id": ""
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('branchname', $scope.instance.id);

            //$scope.NonsalesCustomerReport();
        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    };

    $scope.NonsalesCustomerReport = function () { 

        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined //$scope.instance.id;
        }

        salesReportService.NonSaleCustomers($scope.branchid, data).then(function (response) {

            //console.log(JSON.stringify(response.data));
            $scope.data = response.data;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);
            }
            var totalCash = 0;
            for (var i = 0; i < $scope.data.length; i++) {
                totalCash += $scope.data[i].reportTotal;
            }

            var total = parseInt(totalCash);
            $scope.total = total;

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Non Sale Customers Details";
            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: "Non Sale Customers Details.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [2500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "NonSaleCustomers_Details.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [                        
                    { field: "instance.name", title: "Branch", width: "90px", format: "{0}", type: "string", attributes: { class: "text-left field-report", ftype: "string", fformat: "0" } },
                    { field: "patient.name", title: "Name", width: "90px", format: "{0}", type: "string", attributes: { class: "text-left field-report", ftype: "string", fformat: "0" } },
                  { field: "patient.mobile", title: "Mobile Number", width: "120px", format: "{0}", type: "string", attributes: { class: "text-left field-report", ftype: "string", fformat: "0" } },
                  { field: "patient.email", title: "Email", width: "90px", format: "{0}", type: "string", attributes: { class: "text-left field-report" }, ftype: "string", fformat: "0" },
                  { field: "patient.city", title: "City", width: "90px", format: "{0}", type: "string", attributes: { class: "text-left field-report", ftype: "string", fformat: "0" } },
                  { field: "patient.state", title: "State", width: "90px", format: "{0}", type: "string", attributes: { class: "text-left field-report", ftype: "string", fformat: "0" } },

                    { field: "invoiceNo", title: "Invoice No", width: "100px", format: "{0}", type: "string", attributes: { class: "text-left field-report", ftype: "string", fformat: "0" } },
                    { field: "invoiceDate", title: "Last Sale Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=invoiceDate == null ? '' : kendo.toString(kendo.parseDate(invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                    { field: "netAmount", title: "Amount", width: "90px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.n" } },
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [],
                    schema: {
                        model: {
                            fields: {                                
                                "instance.name": { type: "string" },
                                "name": { type: "string" },
                                "mobile": { type: "string" },
                                "email": { type: "string" },
                                "city": { type: "string" },
                                "state": { type: "string" },

                                "invoiceNo": { type: "string" },
                                "invoiceDate": { type: "date" },
                                "netAmount": { type: "number" },
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.clearSearch = function () {
        $("#grid").empty();
        //data = null;
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.data = [];
    }


    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: "Non Sale Customers Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Non Sale Customers Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);