﻿app.controller('scheduleReportCtrl', ['$scope', 'uiGridConstants', '$rootScope', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'salesItemModel', 'productService', 'salesService', '$filter', function ($scope, uiGridConstants, $rootScope, $http, $interval, $q, salesReportService, salesModel, salesItemModel, productService, salesService, $filter) {

    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.checkFromDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#fromDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if (toDate != undefined && toDate != null) {
                    $scope.checkToDate1(frmDate, toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#toDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1(frmDate, toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.type = 'Month';


    // chng-2

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            //added by nandhini 22.4.17
            //$scope.scheduleReport();
            $rootScope.$broadcast('LoginBranch', $scope.instance);

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }
    $scope.init();
    //add by nandhini
    //$scope.InvoiceSeriesType = "pss";

    $scope.ChangeInvoiceSeriesDropdown = function (seriestype) {


        $scope.InvoiceSeriesType = seriestype;

        $scope.search.select1 = "";

        if ($scope.InvoiceSeriesType == 'Default') {
            $scope.search.select1 = "Default";
            $scope.fromBillNo = "";
            $scope.toBillNo = "";
        }
        if ($scope.InvoiceSeriesType == 'Custom') {
            $scope.search.select1 = "Custom";
            getInvoiceSeriesItems();
            $scope.fromBillNo = "";
            $scope.toBillNo = "";
        }
        if ($scope.InvoiceSeriesType == 'Manual') {
            $scope.search.select1 = "MAN";
            $scope.fromBillNo = "";
            $scope.toBillNo = "";
        }
        if ($scope.InvoiceSeriesType == 'Total') {
            $scope.search.select1 = "Total";
            $scope.fromBillNo = "";
            $scope.toBillNo = "";
        }

    }
    getInvoiceSeriesItems();
    $scope.InvoiceSeriesItems = [];
    function getInvoiceSeriesItems() {
        salesService.getInvoiceSeriesItems().then(function (response) {

            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }

        }, function () {

        });
    }
    var sno = 0;

    var Schedule = "";
    var Dobj = {};
    $scope.scheduleReport = function () {
        /*document.getElementById("type1").style.visibility = "visible";
        document.getElementById("type2").style.visibility = "visible";
        document.getElementById("btnTXTExport").style.visibility = "visible";
        document.getElementById("btnTXTExport1").style.visibility = "visible";
        document.getElementById("btnXLSExport").style.visibility = "visible";
        document.getElementById("btnPdfExport").style.visibility = "visible"; */


        $.LoadingOverlay("show");

        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        if ($scope.schedule != null && $scope.schedule.schedule !== undefined)
            $scope.schedule = $scope.schedule.schedule;

        if ($scope.schedule == "H & H1") {
            Schedule = "H2";
        } else {
            Schedule = $scope.schedule
        }
        $scope.schedule = Schedule;

        Dobj.schedule = Schedule;

        if (data.fromDate != null && data.fromDate !== undefined)
            Dobj.type = $scope.type;
        else
            Dobj.type = $scope.type = '';

        if ($scope.fromBillNo != null && $scope.fromBillNo !== undefined && $scope.fromBillNo !== '')
            Dobj.type = $scope.type;
        else
            Dobj.type = $scope.type = '';

        Dobj.fromBillNo = $scope.fromBillNo;
        Dobj.toBillNo = $scope.toBillNo;
        var instanceid = $scope.branchid;
        var search = $scope.search.select1;
        var Schedule = $scope.schedule;

        $scope.setFileName();
        salesReportService.scheduleList($scope.type, Schedule, data, search, $scope.fromBillNo, $scope.toBillNo, instanceid).then(function (response) {
            $scope.data = response.data;
            var invoiceNo = null;

            for (i = 0; i < $scope.data.length; i++) {
                if ($scope.data[i].sales.invoiceNo == invoiceNo) {
                    $scope.data[i].sales.invoiceNo = "";
                    $scope.data[i].sales.name = "";
                }
                if ($scope.data[i].sales.invoiceNo)
                    invoiceNo = $scope.data[i].sales.invoiceNo;
                //added by nandhini 22.4
                $scope.data[i].sales.invoiceNo = $scope.data[i].sales.invoiceSeries + invoiceNo;
            }

            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: "Schedule.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1800, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Schedule.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                txt: {
                    fileName: "Schedule.txt",
                    allPages: true
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                //sortable: {
                //    field: "sales.invoiceNo",
                //    dir: "asc"
                //},
                sortable: true,
                //orderby:as
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "90px", attributes: { ftype: "sno", class: "text-left field-report" } },
                  { field: "sales.invoiceNo", title: "Bill No", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "sales.invoiceDate", title: "Bill Date", width: "140px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  { field: "sales.nameAddress", title: "Patient Name", width: "160px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "sales.doctorName", title: "Doctor", width: "160px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.name", title: "Product", width: "160px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.product.manufacturer", title: "Manufacturer", width: "220px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.batchNo", title: "Batch", width: "130px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "productStock.expireDate", title: "Expiry", width: "130px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                  //{ field: "quantity", title: "Qty", width: "140px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } }
                  { field: "quantity", title: "Qty", width: "140px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" } },
                  { field: "sign", title: "Signature", width: "140px", type: "string", attributes: { class: "text-right field-report" } }
                ],
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                dataSource: {
                    data: response.data,
                    //sort: { field: "sales.invoiceNo", dir: "asc" },
                    aggregate: [
                        //{ field: "quantity", aggregate: "sum" }
                        //{ field: "sales.invoiceNo", dir: "asc" }

                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "string" },
                                "sales.invoiceNo": { type: "number" },
                                "sales.invoiceDate": { type: "date" },
                                "sales.nameAddress": { type: "string" },
                                "sales.doctorName": { type: "string" },
                                "productStock.product.name": { type: "string" },
                                "productStock.product.manufacturer": { type: "string" },
                                "productStock.batchNo": { type: "string" },
                                "productStock.expireDate": { type: "date", format: "MM/yy" },
                                "quantity": { type: "number" },
                                "sign": { type: "string" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (this.columns[ci].attributes.ftype == "sno" && i == 6) {
                                sno = 0;
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                //cell.value = new Date(cell.value);
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['packing', 'reportCreatedAt', 'sales.nameAddress', 'sales.doctorName', 'productStock.product.name', 'productStock.product.manufacturer', 'productStock.batchNo', 'reportExpireDate', 'quantity', 'sign'];
    //
    $scope.setFileName = function () {
        $scope.fileNameNew = "ScheduleReport_" + $scope.instance.name;
    };
    $scope.clearSearch = function () {
        $scope.fromBillNo = null;
        $scope.toBillNo = "";
        $scope.from = "";
        $scope.to = null;
        $scope.search.select1 = "";
        $scope.InvoiceSeriesType = "";
        $("#grid").empty();        
        $("#grid").removeAttr("style");
        $("#grid").removeAttr("class");
        $scope.data = [];
        /*document.getElementById("type1").style.visibility = "hidden";
        document.getElementById("type2").style.visibility = "hidden";
        document.getElementById("btnTXTExport").style.visibility = "hidden";
        document.getElementById("btnTXTExport1").style.visibility = "hidden";
        document.getElementById("btnXLSExport").style.visibility = "hidden";
        document.getElementById("btnPdfExport").style.visibility = "hidden";
        */
    }
   
    //$scope.scheduleReport();

    $scope.scheduleList = [
            { schedule: 'H & H1', label: 'H & H1' },
            { schedule: 'H', label: 'H' },
            { schedule: 'H1', label: 'H1' },
            { schedule: 'NS', label: 'NON-SCHEDULED (NS) ' },
            { schedule: 'X', label: 'X' }];

    $scope.schedule = $scope.scheduleList[0].label;

    //added by nandhini

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {
            $scope.search.select1 = "All";
            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            $scope.search.select1 = "All";
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Month") {
            $scope.search.select1 = "All";
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        $scope.scheduleReport();
    };

    $scope.getSchedule = function (val) {

        return productService.scheduleFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    //$scope.vendor = function () {
    //    var val = "s";
    //    productService.scheduleFilterData(val).then(function (response) {
    //        $scope.vendorList = response.data;
    //    }, function () { toastr.error('Error Occured', 'Error'); });
    //}
    //$scope.vendor();

    $scope.keyEnter = function (event, e) {
        var ele = document.getElementById(e);
        if (event.which === 13) // Enter key
        {
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
            {
                ele.focus();
                if (ele.nodeName != "BUTTON")
                    ele.select();
            }
        }
    }

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Schedule", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //

    $scope.getSettings = function () {
        $scope.pageBreakSettings = JSON.parse(window.localStorage.getItem("PageBreakSettings"));
        if ($scope.pageBreakSettings == null) {
            $scope.pageBreakSettings = "1";
        }
    };

    $scope.btnTXTExport = function () {


        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }

        $scope.getSettings();
        //added by nandhini
        var tempSearch = $scope.search.select1;

        window.open('/scheduleReport/ListDataForPrint?type=' + Dobj.type + '&schedule=' + Dobj.schedule + '&frombillno=' + Dobj.fromBillNo + '&tobillno=' + Dobj.toBillNo + '&from=' + $scope.from + '&search=' + tempSearch + '&to=' + $scope.to + '&printType=' + $scope.pageBreakSettings + '&sInstanceId=' + $scope.branchid);

    }


    $scope.btnTXTExport1 = function () {
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        $scope.getSettings();
        //added by nandhini
        var tempSearch = $scope.search.select1;

        window.open('/scheduleReport/ListDataForPrint2?type=' + Dobj.type + '&schedule=' + Dobj.schedule + '&frombillno=' + Dobj.fromBillNo + '&tobillno=' + Dobj.toBillNo + '&from=' + $scope.from + '&search=' + tempSearch + '&to=' + $scope.to + '&printType=' + $scope.pageBreakSettings + '&sInstanceId=' + $scope.branchid);
    }

    $scope.btnTXTExport2 = function () {
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        $scope.getSettings();        
        var tempSearch = $scope.search.select1;

        window.open('/scheduleReport/ListDataForPrint3?type=' + Dobj.type + '&schedule=' + Dobj.schedule + '&frombillno=' + Dobj.fromBillNo + '&tobillno=' + Dobj.toBillNo + '&from=' + $scope.from + '&search=' + tempSearch + '&to=' + $scope.to + '&printType=' + $scope.pageBreakSettings + '&sInstanceId=' + $scope.branchid);
    }
    $scope.btnTXTExport3 = function () {
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        $scope.getSettings();
        var tempSearch = $scope.search.select1;

        window.open('/scheduleReport/ListDataForPrint4?type=' + Dobj.type + '&schedule=' + Dobj.schedule + '&frombillno=' + Dobj.fromBillNo + '&tobillno=' + Dobj.toBillNo + '&from=' + $scope.from + '&search=' + tempSearch + '&to=' + $scope.to + '&printType=' + $scope.pageBreakSettings + '&sInstanceId=' + $scope.branchid);
    }

}]);