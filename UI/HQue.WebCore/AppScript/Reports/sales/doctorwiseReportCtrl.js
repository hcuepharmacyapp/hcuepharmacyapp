﻿app.controller('doctorwiseReportCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'salesReportService', 'salesService', 'doctorService', 'productStockService', 'doctorModel', 'userAccessModel', '$filter', function ($scope, $rootScope, $http, $interval, $q, salesReportService, salesService, doctorService, productStockService, doctorModel, userAccessModel, $filter) {
        
    var doctor = angular.copy(doctorModel);
    $scope.search = doctor;
    $scope.selectedProduct = {
        "name": ""
    };
    $scope.productId = null;
    var sno = 0;
    $scope.currentInstance = null;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false; // hide excel export more than two months

    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event,id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.pdfHeader = "";

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.doctorNameSearch = function (obj, event) {
        $scope.search.values = obj.id;
        $scope.search.doctorName = obj.name;
        document.getElementById("drugName").focus();
    };

    $scope.DoctorNameSearch = "1";
    function getDoctorSearchType() {
        salesService.getDoctorSearchType().then(function (response) {
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.DoctorNameSearch = "1";
            } else {
                if (response.data.doctorSearchType != undefined) {
                    $scope.DoctorNameSearch = response.data.doctorSearchType;
                } else {
                    $scope.DoctorNameSearch = "1";
                }
            }
        }, function () {

        });
    }

    getDoctorSearchType();


    $scope.DoctorsList = function (val) {

        if ($scope.DoctorNameSearch == "" || $scope.DoctorNameSearch == undefined) {
            $scope.DoctorNameSearch = 2;
        }

        if ($scope.DoctorNameSearch == 1) {
            return doctorService.Locallist(val).then(function (response) {

                var origArr = response.data;
                var newArr = [],
                origLen = origArr.length,
                found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });


            }, function (error) {
                console.log(error);
            });
        } else {
            return doctorService.list(val).then(function (response) {

                var origArr = response.data;
                var newArr = [],
           origLen = origArr.length,
           found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });


            }, function (error) {
                console.log(error);
            });

        }

    };

    $scope.getProducts = function (val) {

        return productStockService.getAllDrugFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });

    };

    $scope.onProductSelect = function (obj) {
        $scope.productId = obj.product.id;
        document.getElementById("searchBtn").focus();
    }

    $scope.changeFocus = function (val) {
        document.getElementById(val).focus();
    }

    //$scope.type = 'TODAY';

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;                
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.doctorReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.emptyGrid = function () {
        $("#grid").empty();
        $scope.selectedProduct.name = "";
        $scope.search = angular.copy(doctorModel);
        $scope.search.doctorName = "";
        $scope.doctorReport();
    }
        
    $scope.clearSearch = function () {        
        $scope.filterType = "";       
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.selectedProduct = {
            "name": ""
        };
        $scope.productId = null;
        $scope.search = angular.copy(doctorModel);
        $scope.search.doctorName = "";

        getDoctorSearchType();

        $scope.doctorReport();

    }

    $scope.doctorReport = function () {
        $.LoadingOverlay("show");
        $("#grid").empty();
        var data = {
            fromDate: $scope.from,  
            toDate: $scope.to 
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined //$scope.instance.id;
        }
        if ($scope.filterType == undefined || $scope.filterType == "") {
            $scope.filterType = "1";
        }
        if ($scope.search.values == undefined || $scope.search.values == null || $scope.search.doctorName == undefined || $scope.search.doctorName == null || $scope.search.doctorName == "") {
            $scope.search.values = "";
        }
        if (($scope.search.values == undefined || $scope.search.values == null || $scope.search.values == "") && ($scope.search.doctorName != undefined && $scope.search.doctorName != null && $scope.search.doctorName != "")) {
            $scope.search.values = $scope.search.doctorName;
        }
        if ($scope.productId == undefined || $scope.productId == null || $scope.selectedProduct.name == undefined || $scope.selectedProduct.name == null || $scope.selectedProduct.name == "") {
            $scope.productId = "";
        }
        if (($scope.productId == undefined || $scope.productId == null ||  $scope.productId == "") && ($scope.selectedProduct.name != undefined && $scope.selectedProduct.name != null && $scope.selectedProduct.name != "")) {
            $scope.productId = $scope.selectedProduct.name;
        }
        if ($scope.type == undefined) {
            $scope.type = "";
        }
                
        salesReportService.DoctorwiseList($scope.type, data, $scope.branchid, $scope.filterType, $scope.search.values, $scope.productId).then(function (response) {
            $scope.data = response.data;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);
            }
            $scope.type = "";
            $.LoadingOverlay("hide");
             var pdfHeader = "";
             if ($scope.instance != undefined) {
                 if (angular.isObject($scope.currentInstance)) {
                     $scope.pdfHeader = $scope.currentInstance;
                     $scope.instance = $scope.currentInstance;
                 }
                 else {
                     $scope.pdfHeader = $scope.instance;
                 }
             } else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>Doctor Wise Report";
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/>Doctor Wise Report";
            }

            if ($scope.filterType == "1") {
                var grid = $("#grid").kendoGrid({
                    excel: {
                        fileName: "DoctorWiseReport.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "DoctorWiseReport.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    height: 380,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                          { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "25px", attributes: { ftype: "sno", class: " text-left field-report" } },
                          { field: "branchName", title: "Branch", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                          { field: "name", title: "Doctor Name", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                          { field: "product.name", title: "Product Name", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                          { field: "salesQty", title: "Sales Quantity", width: "35px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'><span id='totalSalesQty'>#= kendo.toString(sum, '0') #</span></div>" },
                          { field: "returnQty", title: "Return Quantity", width: "35px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'><span id='totalSalesQty'>#= kendo.toString(sum, '0') #</span></div>" },
                          { field: "netQuantity", title: "Net Quantity", width: "35px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'><span id='totalSalesQty'>#= kendo.toString(sum, '0') #</span></div>" },
                          { field: "profitPerc", title: "Gross Margin %", width: "45px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'><span id='totalSalesQty'>#= kendo.toString(sum, '0') #</span></div>" },
                          { field: "netValue", title: "Net Value WOT Round off (Sale-Return)", width: "60px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:n" }, footerTemplate: "<div class='report-footer'><span id='totalSalesQty'>#= kendo.toString(sum, 'n2') #</span></div>" }

                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                             { field: "salesQty", aggregate: "sum" },
                             { field: "returnQty", aggregate: "sum" },
                             { field: "netQuantity", aggregate: "sum" },
                             { field: "netValue", aggregate: "sum" },
                        { field: "profitPerc", aggregate: "sum" }
                                                
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "sNo": { type: "string" },
                                    "branchName": { type: "string" },
                                    "name": { type: "string" },
                                    "product.name": { type: "string" },
                                    "salesQty": { type: "number" },
                                    "returnQty": { type: "number" },
                                    "netQuantity": { type: "number" },
                                    "profitPerc": { type: "number" },
                                    "netValue": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },
                    dataBinding: function () {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    },
                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];
                                //added by nandhini for excel s.no
                                var cell = row.cells[ci];
                                if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                    sno = 0;
                                }

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                    cell.hAlign = "left";
                                    sno = sno + 1;
                                    cell.value = sno;
                                }
                                //end
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0";
                                        cell.bold = true;
                                    }
                                }
                                
                            }
                        }
                    },
                }).data("kendoGrid");
            }
            else if ($scope.filterType == "2") {
                var grid = $("#grid").kendoGrid({
                    excel: {
                        fileName: "DoctorWiseReport.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "DoctorWiseReport.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    height: 380,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                          { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "25px", attributes: { ftype: "sno", class: " text-left field-report" } },
                          { field: "branchName", title: "Branch", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                          { field: "product.name", title: "Product Name", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                          { field: "name", title: "Doctor Name", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                          
                          { field: "salesQty", title: "Sales Quantity", width: "35px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'><span id='totalSalesQty'>#= kendo.toString(sum, '0') #</span></div>" },
                          { field: "returnQty", title: "Return Quantity", width: "35px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'><span id='totalSalesQty'>#= kendo.toString(sum, '0') #</span></div>" },
                          { field: "netQuantity", title: "Net Quantity", width: "35px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'><span id='totalSalesQty'>#= kendo.toString(sum, '0') #</span></div>" },
                          { field: "netValue", title: "Net Value", width: "35px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:n" }, footerTemplate: "<div class='report-footer'><span id='totalSalesQty'>#= kendo.toString(sum, 'n2') #</span></div>" }

                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                             { field: "salesQty", aggregate: "sum" },
                             { field: "returnQty", aggregate: "sum" },
                             { field: "netQuantity", aggregate: "sum" },
                             { field: "netValue", aggregate: "sum" }
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "sNo": { type: "string" },
                                    "product.name": { type: "string" },
                                    "name": { type: "string" },
                                    "salesQty": { type: "number" },
                                    "returnQty": { type: "number" },
                                    "netQuantity": { type: "number" },
                                    "netValue": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },
                    dataBinding: function () {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    },
                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];
                                //added by nandhini for excel s.no
                                var cell = row.cells[ci];
                                if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                    sno = 0;
                                }

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                    cell.hAlign = "left";
                                    sno = sno + 1;
                                    cell.value = sno;
                                }
                                //end
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0";
                                        cell.bold = true;
                                    }
                                }
                               
                            }
                        }
                    },
                }).data("kendoGrid");
            }
            

            // Save the reference to the original filter function.
            grid.dataSource.originalFilter = grid.dataSource.filter;

            // Replace the original filter function.
            grid.dataSource.filter = function () {
                // If a column is about to be filtered, then raise a new "filtering" event.
                if (arguments.length > 0) {
                    this.trigger("filtering", arguments);
                }
                // Call the original filter function.
                var result = grid.dataSource.originalFilter.apply(this, arguments);

                // If a column is about to be filtered, then raise a new "filtering" event.
                if (arguments.length > 0) {
                    this.trigger("filtering", result);
                }

                return result;
            }

            // Bind to the dataSource filtering event.
            $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                var filters = dataSource.filter();
                var allData = dataSource.data();
                var query = new kendo.data.Query(allData);
                var data = query.filter(filters).data;
               

            });
                        

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
        
        
    }
    //End Chng 2

    
    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "TODAY") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.doctorReport();
    };

    // chng-3

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Doctor Wise Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Doctor Wise Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }

    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');
        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);

