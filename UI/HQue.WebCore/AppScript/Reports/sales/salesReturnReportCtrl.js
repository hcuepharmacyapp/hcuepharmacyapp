﻿app.controller('salesReturnReportCtrl', ['$scope', '$rootScope',  '$http', '$interval', '$q', 'salesReportService', 'salesReturnModel', 'salesReturnItemModel', '$filter', function ($scope,$rootScope,  $http, $interval, $q, salesReportService, salesReturnModel, salesReturnItemModel, $filter) {
    
    var salesReturnItem = salesReturnItemModel;
    var salesReturn = salesReturnModel;
    $scope.search = salesReturnItem;
    $scope.search.salesReturn = salesReturn;
    $scope.allBranch = true; // Enable all branch in branch ddl
    $scope.dateRangeExceeds = false;
    var isComposite = false;

    //$scope.branchid = "";
    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.fromvalues = "";
    $scope.tovalues = "";
    $scope.SearchType = "";
    $scope.search.SearchType = "billDate";
    $scope.dateCondition = true;
    $scope.returnNoValues = false;

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };
    $scope.type = ''; //'TODAY';
    
    // chng-2
    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            isComposite = $scope.instance.gstselect;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.salesReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }
    // Added by Violet for Sales Return Change filter 15-06-2017
    $scope.changefilters = function () {
      
        if ($scope.search.SearchType == 'returnNo') {
            $scope.returnNoValues = true;
            $scope.dateCondition = false;
            $scope.ReturnNoCondition = true;
        } else if ($scope.search.SearchType == 'billDate') {
            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.dateCondition = true;
            $scope.returnNoValues = false;
            $scope.ReturnNoCondition = false;
            $scope.fromvalues = "";
            $scope.tovalues = "";
        }
        else {
            $scope.returnNoCondition = false;
            $scope.dateCondition = false;
            $scope.returnNoValues = false;
            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.SearchType = "";
            $scope.fromvalues = "";
            $scope.tovalues = "";
           }
    };
    $scope.keyEnter = function (nextid) {
        var ele = document.getElementById(nextid);
        ele.focus();
    };
    $scope.salesReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to,
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = undefined; //$scope.instance.id;
        }

        if ($scope.search.SearchType == 'returnNo')
        {
            $scope.SearchType = 1;
        } else if ($scope.search.SearchType == 'billDate')
        {
            $scope.SearchType = 2;
        } else {
            $scope.SearchType = 0;
        }
        //Footer section removed on 22/02/2017 by Poongodi
        salesReportService.returnList($scope.type, data, $scope.branchid, 1, $scope.fromvalues, $scope.tovalues, $scope.SearchType).then(function (response) {
            $scope.data = response.data;
            if ($scope.from != undefined || $scope.from != null) {
                $scope.dayDiff($scope.from);
            }
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }
            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                // export file header for all branch
                if ($scope.branchid == undefined)
                    pdfHeader = $scope.instance.bdoName + " - " + " All Branch" + "<br/> Sales Return";
            }

            $("#grid").kendoGrid({
                //toolbar: ["excel", "pdf"],
                excel: {
                    fileName: "Sales Return.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Sales_Return.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                     { field: "salesReturn.instance.name", title: "Branch", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                  { field: "salesReturn.returnDate", title: "Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(salesReturn.returnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  { field: "salesReturn.returnNo", title: "Return Bill No", width: "120px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "salesReturn.returnDate", title: "Return Bill Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(salesReturn.returnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  { field: "salesReturn.sales.name", title: "Customer Name", width: "130px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" }},
                 { field: "productStock.product.name", title: "Product", width: "130px", type: "string", attributes: { class: "text-left field-report" }  },
                  { field: "quantity", title: "Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }    },
                  { field: "productStock.sellingPrice", title: "Selling Rate(MRP)", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                  { field: "productStock.vat", title: "VAT/GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, hidden: isComposite },
                { field: "vatValue", title: "VAT/GST Amount", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isComposite },
                //{ field: "salesRoundOff", title: "Final Value", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" }
                 { field: "salesRoundOff", title: "Final Value", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }  }
                ],
                dataSource: {
                    data: response.data,
                    //aggregate: [
                    //    { field: "quantity", aggregate: "sum" },
                    //    { field: "productStock.sellingPrice", aggregate: "sum" },
                    //    { field: "productStock.vat", aggregate: "sum" },
                    //    { field: "vatAmount", aggregate: "sum" },
                    //    { field: "salesRoundOff", aggregate: "sum" }

                    //],
                    schema: {
                        model: {
                            fields: {
                                "salesReturn.returnDate": { type: "date" },
                                "salesReturn.returnNo": { type: "number" },
                                "salesReturn.returnDate": { type: "date" },
                                "salesReturn.sales.name": { type: "string" },
                                "quantity": { type: "number" },
                                "productStock.sellingPrice": { type: "number" },
                                "productStock.vat": { type: "number" },
                                "vatAmount": { type: "number" },
                                "salesRoundOff": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    //End Chng 2

    //$scope.salesReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.search.SearchType = "billDate";
        $scope.dateCondition = true;
        $scope.returnNoValues = false;
        $scope.fromvalues = "";
        $scope.tovalues = "";
        if ($scope.type === "TODAY") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.salesReport();
    }

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        if ($scope.branchid != undefined) {
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Sales Return", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
        else {
            headerCell = { cells: [{ value: " Sales Return", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.bdoName + " - " + " All Branch", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        }
    }
    //
    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');
        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);
