﻿app.factory('hierarchyService', function ($http) {
    return {
        selectInstance:function(){
            return $http.post('/HierarchyReport/SelectInstance')
        },

        list: function (id) {
            return $http.post('/HierarchyReport/LowProductStockList?id='+id);
        },
        getInstanceData: function () {
            return $http.post('/HierarchyReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },
    }
});
