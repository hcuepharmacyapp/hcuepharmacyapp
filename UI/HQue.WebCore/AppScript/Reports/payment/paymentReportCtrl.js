﻿app.controller('paymentReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'paymentService', 'paymentModel', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, paymentService, paymentModel) {
    
    var payment = paymentModel;
    //var instance = "";
    $scope.search = payment;

    $scope.data = [];

    $scope.$on('branchname', function (event, id) {
        $scope.branchid = id;
    });

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        paymentService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.paymentReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        
    }
    
    $scope.gridOptions = {
        showColumnFooter: true,
        exporterMenuCsv: true,
        enableGridMenu: true,
        enableFiltering: true,
        enableColumnResizing: true,
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        data: 'data',
        columnDefs: [
          { field: 'vendor.name', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Name', width: '50%' },
           { field: 'balance', aggregationType: uiGridConstants.aggregationTypes.sum, displayName: 'Balance', width: '30%',  format: "{0:n}" ,footerTemplate: {format: "{0}"}},
           
        ],

        gridMenuCustomItems: [
            {
                title: 'Rotate Grid',
                action: function ($event) {
                    this.grid.element.toggleClass('rotated');
                },
                order: 210
            }
        ],

        onRegisterApi: function( gridApi ){
        $scope.gridApi = gridApi;
 
        gridApi.core.on.columnVisibilityChanged( $scope, function( changedColumn ){
            $scope.columnChanged = { name: changedColumn.colDef.name, visible: changedColumn.colDef.visible };
        });
    }
 };
    $scope.paymentReport = function () {
        $.LoadingOverlay("show");
 
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
         
            paymentService.paymentReport("",$scope.branchid).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                $scope.pdfHeader = $scope.instance;
            }
            else {
                purchaseReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                    $scope.pdfHeader.tinNo = "";
                else
                    $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "PaymentReport.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1700, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "PaymentReport.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                         { field: 'vendor.name', title: "Name", width: "150px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
              { field: "balance", title: "Balance", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report" } , footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" }
                    

                ],
                dataSource: {
                    data: response.data,
                    aggregate: [

                        { field: "balance", aggregate: "sum" } 
                     

                    ],
                    schema: {
                        model: {
                            fields: {
                                "vendor.name": { type: "string" },
                            
                                "balance": { type: "number" } 
                             
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            cell.fontFamily = "verdana";
                            cell.wrapText = true;
                            if (row.type == "header") {
                                row.cells[ci].background = "#039394";
                                cell.fontSize = "9";
                                cell.width = "200";
                            }
                            if (row.type == "title") {
                                row.cells[ci].background = "#0c0c0c";
                                cell.fontSize = "9";
                                cell.width = "200";
                              
                            }
                           
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }
                          
                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                 
                                    cell.fontFamily = "verdana";
                                    cell.fontSize = "8"
                                     
                                }
                              
                            }
                        }
                    }
                    e.preventDefault();
                    promises[0].resolve(e.workbook);
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //
    $scope.paymentReport1 = function () {
        $.LoadingOverlay("show");

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        paymentService.paymentReport("",$scope.branchid).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";
             $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

   // $scope.paymentReport();
}]);

