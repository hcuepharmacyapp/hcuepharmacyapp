﻿app.factory('LowProductStockReportService', function ($http) {
    return {
        list: function () {
            return $http.post('/LowProductStockReport/LowProductStockList');
        },
    }
});
