﻿app.controller('lowProductStockReportCtrl', ['$scope', '$http', '$interval', '$q', 'LowProductStockReportService', 'productStockModel', 'productModel', function ($scope, $http, $interval, $q, LowProductStockReportService, productStockModel, productModel) {

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.gridOptions = {
        exporterMenuCsv: true,
        enableGridMenu: true,
        enableFiltering: true,
        enableColumnResizing: true,
        //gridMenuTitleFilter: fakeI18n,
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        data: 'data',
        columnDefs: [
          { field: 'product.name', headerCellClass: $scope.highlightFilteredHeader, displayName:'Product Name', width: '15%' },
          { field: 'stock', displayName: 'Stock', width: '10%' }
        ],

        gridMenuCustomItems: [
            {
                title: 'Rotate Grid',
                action: function ($event) {
                    this.grid.element.toggleClass('rotated');
                },
                order: 210
            }
        ],

        onRegisterApi: function( gridApi ){
        $scope.gridApi = gridApi;
 
        gridApi.core.on.columnVisibilityChanged( $scope, function( changedColumn ){
            $scope.columnChanged = { name: changedColumn.colDef.name, visible: changedColumn.colDef.visible };
        });
    }
  };

    $scope.lowProductStockReport = function () {
        $.LoadingOverlay("show");
        LowProductStockReportService.list().then(function (response) {
            $scope.data =  response.data;
            $.LoadingOverlay("hide");
        }, function () { 
            $.LoadingOverlay("hide");
        });
    }

    $scope.lowProductStockReport();
}]);