﻿app.controller('GSTR4TaxRateWiseTurnOverCtrl', function ($scope, $rootScope, $filter, $http, gstreportservice) {

    
    $scope.searchType = "gstin";
    $scope.gstinList = [];
    $scope.dataList = [];

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });


    $scope.init = function () {
        $.LoadingOverlay("show");        
        $scope.getGstin();

        gstreportservice.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }

            $scope.selectedGstin = {
                gsTinNumber: $scope.instance.gsTinNo,
            }

            $rootScope.$broadcast('LoginBranch', $scope.instance);

        }, function () {
            $.LoadingOverlay("hide");
        });

        gstreportservice.getUserData().then(function (response) {
            $scope.userData = response.data;
        }, function () { });
    }

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
    var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
    $scope.from = firstDay;
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.data = [];
    $scope.pdfHeader = "";

    $scope.type = 'TODAY';

    $scope.getGstin = function () {
        gstreportservice.getGstin().then(function (response) {
            $scope.gstinList = response.data;
            $scope.selectedgstin = $scope.gstinList[0];
        }, function () {

        });
    }

    $scope.gsTinNumberChange = function () {
        var gstin = document.getElementById("gstin").value;

        $scope.selectedgstin = {
            gsTinNumber: gstin
        }
    }

    $scope.GSTR4TaxRateWiseTurnOver = function () {
        //if (false) { //$scope.validateGSTR3BMsg != "Ok"
        //    ShowConfirmMsgWindow();
        //}
        //else {
            $.LoadingOverlay("show");
            var data = {
                fromDate: $scope.from,
                toDate: $scope.to
            }
            if ($scope.selectedGstin == undefined) {
                $scope.gsTinNo = "";
            }
            else if ($scope.branchid == undefined) {
                $scope.gsTinNo = $('#gstin').find(":selected").text();
            }
            else {
                $scope.gsTinNo = $scope.selectedGstin.gsTinNumber;
            }

            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;                
            }
            
            setFileName();
            gstreportservice.gSTR4TaxRateWiseTurnOver(data, $scope.branchid, $scope.searchType, $scope.gsTinNo).then(function (response) {
                $scope.dataList = response.data;
                $scope.dataList = $scope.dataList.splice(0, 1);

                $scope.invAmountNoTaxNoDiscount = $scope.dataList[0].invAmountNoTaxNoDiscount;
                $scope.finYearVal = $scope.dataList[0].invoiceAmountNoTaxNoDiscount;

                $scope.data = response.data.splice(0, response.data.length);

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    gstreportservice.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader != undefined) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                        $scope.pdfHeader.tinNo = "";
                    else
                        $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "GSTR4.xlsx",
                        allPages: true
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    height: 450,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [                      
                      { field: "gstTotal", title: "GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "invoiceAmountNoTaxNoDiscount", title: "Invoice Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n3')#</div>" },
                      { field: "cgstTaxAmt", title: "CGST TaxAmount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n3')#</div>" },
                      { field: "sgstTaxAmt", title: "SGST TaxAmount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" }, footerTemplate: "<div class='report-footer'>#=  kendo.toString(sum, 'n3')#</div>" }
                    ],
                    dataSource: {
                        data: $scope.data, //response.data,
                        aggregate: [

                      { field: "invoiceAmountNoTaxNoDiscount", aggregate: "sum" },
                      { field: "cgstTaxAmt", aggregate: "sum" },
                      { field: "sgstTaxAmt", aggregate: "sum" }

                        ],
                        schema: {
                            model: {
                                fields: {
                                    "gstTotal": { type: "number" },
                                    "invoiceAmountNoTaxNoDiscount": { type: "number" },
                                    "cgstTaxAmt": { type: "number" },
                                    "sgstTaxAmt": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },
                    dataBinding: function () {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    },
                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];
                                var cell = row.cells[ci];
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        //}
    }

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        var headerCell = { cells: [{ value: "Aggregate Turnover - April to June, 2017 : " + $scope.invAmountNoTaxNoDiscount, bold: true, vAlign: "center", textAlign: "left", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);

        headerCell = { cells: [{ value: "Aggregate Turnover in the preceding Financial Year : " + $scope.finYearVal, bold: true, vAlign: "center", textAlign: "left", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);

        headerCell = { cells: [{ value: "GSTR 4 - TaxRateWise TurnOver", bold: true, fontSize: 20, vAlign: "center", hAlign: "center", textAlign: "left", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    $scope.order = ['gstTotal', 'invoiceAmountNoTaxNoDiscount', 'cgstTaxAmt', 'sgstTaxAmt'];

    setFileName = function () {
        $scope.fileName = "GSTR4 - TaxRateWise TurnOver" + $scope.instance.name;
    }
});