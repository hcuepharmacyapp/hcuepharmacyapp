﻿app.controller('hsnmasterReportCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'gstreportservice', 'userAccessModel', '$filter', 'ModalService', function ($scope, $rootScope, $http, $interval, $q, gstreportservice, userAccessModel, $filter, ModalService) {

 

 
    $scope.pdfHeader = "";
    $scope.init = function () {
        $.LoadingOverlay("show");
       

       
        gstreportservice.hsnreport().then(function (response) {
            $scope.data = response.data;

            var pdfHeader = "";
            pdfHeader = "HSN Master Report";   
            $("#grid").kendoGrid({
                excel: {
                    fileName: "HSN Master.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "HSN Master.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [

                 { field: "code", title: "Internal Item Code", width: "100px", attributes: { class: "text-left field-report" } },
                   { field: "productName", title: "Product Name", width: "100px", attributes: { class: "text-left field-report" } },
                 { field: "hsn", title: "HSN / SAC", width: "200px", attributes: { class: "text-left field-report" } },
                 { field: "hsnDesc", title: "HSN / SAC Description", width: "100px", attributes: { class: "text-left field-report" } },
                  { field: "uom", title: "Unit Of Measurement", width: "100px", attributes: { class: "text-left field-report" } },

                 { field: "gstTotal", title: "GST Rate", width: "100px", attributes: { class: "text-left field-report" } },

                ],
                dataSource: {
                    data: response.data,

                    schema: {
                        model: {
                            fields: {
                                "Code": { type: "string" },
                                "HSN": { type: "string" },
                                "HSNDesc": { type: "string" },
                                "GSTTotal": { type: "string" },
                                "uom": { type: "string" },

                            }
                        }
                    },
                    pageSize: 100
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            var cell = row.cells[ci];
                            cell.wrap = true;
                            cell.height = "15px";
                            cell.border = 1;
                            cell.borderColor = "#ddd9c4"
                            if (row.type == "header" && i == 2) {
                                cell.value = "";
                                cell.background = "#ffffff";
                            }



                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });    

    }
 
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
       
        headerCell = { cells: [{ value: "HSN Master", bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    function ShowConfirmMsgWindow() {
        var data = {
            msgTitle: "",
            msg: $scope.validateGSTR3BMsg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: $scope.redirectUrl,
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                return false;
            });
        });
    }

}]);

