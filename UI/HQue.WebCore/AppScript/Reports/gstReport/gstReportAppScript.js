﻿var app = angular.module('hcue', ['ngTouch', 'ui.grid', 'ngSanitize','ngCsv','ui.grid.exporter', 'ui.grid.selection', 'ui.grid.pagination', 'ui.grid.resizeColumns',
    "ui.bootstrap", 'commonApp', 'angularModalService']);

app.filter('statusFilter', function () {
    return function (input) {
        if (input === null) {
            return input;
        }
        return input === 'ACPH' ? 'Approved' : 'Rejected';
    };
});

