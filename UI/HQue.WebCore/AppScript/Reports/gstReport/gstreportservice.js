﻿app.factory('gstreportservice', function ($http) {
    return {
        gst3b: function (type, data, filter, instanceid, searchtype, gsttinNo, isInvoicedt) {
            return $http.post('/gstReport/GSTR3BListData?type=' + type + '&filter=' + filter + '&sInstanceId=' + instanceid + '&sSearchType=' + searchtype + '&sGsttinNo=' + gsttinNo + '&nInvoiceDt=' + isInvoicedt, data);
        },
       // added for GST REPORTS by Nandhini on 29/08/2017
        hsnreport: function () {
            return $http.post('/gstReport/hsnReportListData');
        },
        controlReport: function (data, filter, instanceid) {
            return $http.post('/gstReport/ControlListData?filter=' + filter + '&sInstanceId=' + instanceid, data);
        },
        gstr1purchase: function (type, data, filter, instanceid, searchtype, gsttinNo, isregister,isInvoicedt) {
            return $http.post('/gstReport/GSTR1PurchaseListData?type=' + type + '&filter=' + filter + '&sInstanceId=' + instanceid + '&sSearchType=' + searchtype + '&sGsttinNo=' + gsttinNo + '&isRegister=' + isregister + '&nInvoiceDt=' + isInvoicedt, data);
        },
        gstr2purchasesummary: function (type, data, filter, instanceid, searchtype, gsttinNo, isregister, isInvoicedt) {
            return $http.post('/gstReport/GSTR2PurchaseSummaryData?type=' + type + '&filter=' + filter + '&sInstanceId=' + instanceid + '&sSearchType=' + searchtype + '&sGsttinNo=' + gsttinNo + '&isRegister=' + isregister + '&nInvoiceDt=' + isInvoicedt, data);
        },
        gstr2purchasereturn: function (type, data, filter, instanceid, searchtype, gsttinNo, isRegister, isInvoicedt) {
            return $http.post('/gstReport/GSTR2PurchaseReturnData?type=' + type + '&filter=' + filter + '&sInstanceId=' + instanceid + '&sSearchType=' + searchtype + '&sGsttinNo=' + gsttinNo + '&isRegister=' + isRegister + '&nInvoiceDt=' + isInvoicedt, data);
        },
        gstr1sales: function (type, data, filter, instanceid, searchtype, gsttinNo) {
            return $http.post('/gstReport/GSTR1SalesListData?type=' + type + '&filter=' + filter + '&sInstanceId=' + instanceid + '&sSearchType=' + searchtype + '&sGsttinNo=' + gsttinNo, data);
        },
        gstr1salesreturn: function (type, data, filter, instanceid, searchtype, gsttinNo) {
            return $http.post('/gstReport/GSTR1SalesReturnData?type=' + type + '&filter=' + filter + '&sInstanceId=' + instanceid + '&sSearchType=' + searchtype + '&sGsttinNo=' + gsttinNo, data);
        },
        salesConsolidatedGSTList: function (type, data, instanceid) {
            return $http.post('/gstReport/SalesConsolidatedGSTListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        salesConsolidatedGSTReturnList: function (type, data, instanceid) {
            return $http.post('/gstReport/SalesConsolidatedGSTReturnListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        purchaseConsolidatedGSTList: function (type, data, instanceid) {
            return $http.post('/gstReport/PurchaseConsolidatedGSTListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        purchaseReturnConsolidatedGSTList: function (type, data, instanceid) {
            return $http.post('/gstReport/PurchaseReturnConsolidatedGSTListData?type=' + type + '&sInstanceId=' + instanceid, data);
        },
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        getUserData: function () {
            return $http.post('/salesReport/GetUserData');
        },
        getGstin: function () {
            return $http.post('/gstReport/getGstinData');
        },
        validateGSTR3B: function () {
            return $http.get('/gstReport/ValidateGSTR3B');
        },

        //Added by Sarubala on 04-09-17 - start

        gstrB2CSsales: function (type, data, filter, instanceid, searchtype, gsttinNo) {
            return $http.post('/gstReport/GSTR1B2CSListData?type=' + type + '&filter=' + filter + '&sInstanceId=' + instanceid + '&sSearchType=' + searchtype + '&sGsttinNo=' + gsttinNo, data);
        },
        //Added by Sarubala on 04-09-17 - end
       
        gSTR4TaxRateWiseTurnOver: function (data, instanceid, searchtype, gsttinNo) {
            return $http.post('/gstReport/gSTR4TaxRateWiseTurnOver?sInstanceId='+ instanceid + '&sSearchType=' + searchtype + '&sGsttinNo=' + gsttinNo, data);
        },
    }
});
