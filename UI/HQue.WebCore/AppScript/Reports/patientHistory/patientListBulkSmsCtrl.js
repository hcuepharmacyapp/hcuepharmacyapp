﻿app.controller('patientListBulkSmsCtrl', function ($scope, toastr, patientService, salesModel, pagerServcie) {

    var sales = salesModel;

    $scope.search = sales;
    $scope.list = [];
    $scope.PatientArray = [];
    $scope.CustomerSms = {};

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;

    $scope.showSms = false; //Added by Sarubala on 20-11-17
    //pagination

    function pageSearch() {

        $scope.isAllSelected = false;
        $scope.SelectedCount = 0;

        patientService.customerListBulkSms($scope.search).then(function (response) {
            $scope.list = response.data.list;
            var count = 0;
            //$scope.PatientArray = $scope.PatientArray;
            //Added for Pagination Issue 
            for (var i = 0; (i < $scope.PatientArray.length) && ($scope.PatientArray.length > 0) ; i++) {
                for (var j = 0; j < $scope.list.length; j++) {                    
                    if ($scope.PatientArray[i] != undefined) {
                        if ($scope.list[j].id == $scope.PatientArray[i].id) {
                            $scope.list[j] = $scope.PatientArray[i];
                            $scope.SelectedCount++;
                            break;
                        }
                    }
                }
            }
            
            if (parseInt($scope.SelectedCount) >= 30)
                $scope.isAllSelected = true;

        }, function () { });
    }

    $scope.salesSearch = function () {
        $scope.search.page.pageNo = 1;
        $scope.search.patientStatus = 1;
        patientService.customerListBulkSms($scope.search).then(function (response) {
            $scope.list = response.data.list;
            //console.log($scope.list);
            //Added for Pagination Issue 
            for (var i = 0; (i < $scope.PatientArray.length) && ($scope.PatientArray.length > 0) ; i++) {
                for (var j = 0; j < $scope.list.length; j++) {
                    if ($scope.PatientArray[i] != undefined) {
                         if ($scope.list[j].id == $scope.PatientArray[i].id) {
                            $scope.list[j] = $scope.PatientArray[i];
                            break;
                         }
                    }
                }
            }
            pagerServcie.init(response.data.noOfRows, pageSearch);
        }, function () { });
    };

    $scope.salesSearch();

    //Added by Sarubala on 20-11-17
    function getBulkSmsSetting() {
        patientService.getCustomerBulkSmsSetting().then(function (response) {
            $scope.showSms = response.data;
        }, function (error) {
            console.log(error);
        });
    };

    getBulkSmsSetting();

    $scope.sendSms = function () {

        $scope.CustomerSms.Customers = $scope.PatientArray;
        $scope.CustomerSms.smsContent = $scope.sales.smsContent;

        patientService.sendSms($scope.CustomerSms).then(function (response) {
            if ($scope.CustomerSms.Customers.length == 0) {
                toastr.error('Please select atleast one customer');
                $scope.salesSearch();
            }
            else {
                $scope.salesSearch();
                $scope.sales.smsContent = '';
                $scope.search.name = '';
                $scope.isAllSelected = false;
                $scope.PatientArray = [];
                toastr.success('SMS sent successfully');
            }

        }, function (error) {
            console.log(error);
            toastr.error("Message Not Sent");
        });
    }

    $scope.addPatientId = function (status, index, saleItem) {

        var count = 0;
        var listcount = $scope.list.length;
        angular.forEach($scope.list, function (itm, ind) {
            if (itm.isChecked)
                count++; //$scope.PatientArray.push(itm);                            
        });

        if (status)
            $scope.PatientArray.push(JSON.parse(JSON.stringify(saleItem))); //angular.copy(saleItem, PatientArray);
        else {
            var idx = $scope.PatientArray.indexOf(index);
            //$scope.PatientArray[index].isChecked = false; //.splice(idx, 1);
            $scope.PatientArray.splice(idx, 1);
        }

        if (count == listcount) {
            $scope.isAllSelected = true;
        }
        /* else {
            $scope.isAllSelected = false;
        }*/
    }

    $scope.selectAll = function () {
        var toggleStatus = $scope.isAllSelected;

        angular.forEach($scope.list, function (itm) {
            itm.isChecked = toggleStatus;
        });
        //Changed for Pagination Issue and bulk SMS issue
        if (toggleStatus) {
            var tempList = JSON.parse(JSON.stringify($scope.list));  //angular.copy(tempList, $scope.PatientArray);
            if ($scope.PatientArray.length == 0)
                $scope.PatientArray = tempList;
            else {
                angular.forEach(tempList, function (itm) {
                    $scope.PatientArray.push(itm);
                });
            }
        }
        else {
            var poptempList = JSON.parse(JSON.stringify($scope.list));
            angular.forEach(poptempList, function (index) {
                var idx = index;
                $scope.PatientArray.splice(index, 1);    //$scope.PatientArray.pop(poptempList);
            });
        }
    }
   
});


app.directive('noSpecialChar', function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function(scope, element, attrs, modelCtrl) {
        modelCtrl.$parsers.push(function(inputValue) {
          if (inputValue === undefined)
            return ''
          cleanInputValue = inputValue.replace("#",'');
          cleanInputValue = cleanInputValue.replace("^", '');
          cleanInputValue = cleanInputValue.replace("&", '');
          if (cleanInputValue != inputValue) {
            modelCtrl.$setViewValue(cleanInputValue);
            modelCtrl.$render();
          }
          return cleanInputValue;
        });
      }
    }
  });
