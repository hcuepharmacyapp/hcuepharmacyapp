﻿var app = angular.module('hcue', ['ngTouch', "ui.bootstrap", 'commonApp']);

app.filter('statusFilter', function () {
    return function(input) {
        if (input === null) {
            return input;
        }
        return input === 'ACPH' ? 'Approved' : 'Rejected';
    };
});

