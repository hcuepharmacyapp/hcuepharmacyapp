﻿app.controller('purchaseReturnReportCstCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'purchaseReportService', 'vendorReturnModel', 'vendorReturnItemModel', '$filter', function ($scope, $rootScope, $http, $interval, $q, purchaseReportService, vendorReturnModel, vendorReturnItemModel, $filter) {

    var vendorReturnItem = vendorReturnItemModel;
    var vendorPurchaseReturn = vendorReturnModel;
    $scope.search = vendorReturnItem;
    $scope.search.vendorPurchaseReturn = vendorPurchaseReturn;


    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.type = '';

    $scope.init = function () {
        $.LoadingOverlay("show");
        purchaseReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.buyReturnReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        purchaseReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.buyReturnReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }

        purchaseReportService.returnListCst($scope.type, data, $scope.branchid).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                purchaseReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

           if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                    $scope.pdfHeader.tinNo = "";
                else
                    $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "Annx-02-Purchase_Return_of_1st_Schedule_Goods_purchased_from_Registered_Dealer_v1.0.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1800, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Annx-02-Purchase_Return_of_1st_Schedule_Goods_purchased_from_Registered_Dealer_v1.0.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                height:350,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [

                    { field: "vendorPurchaseReturn.vendorPurchase.vendor.tinNo", title: "Seller Taxpayer Identification Number", width: "100px", attributes: { class: "text-left field-report" } },
                  { field: "vendorPurchaseReturn.vendorPurchase.vendor.name", title: "Name of Seller", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                     { field: "vendorPurchaseReturn.vendorPurchase.vendor.address", title: "Seller Address", width: "120px", attributes: { class: "text-left field-report" } },
                    { field: "vendorPurchaseReturn.vendorPurchase.vendor.state", title: "Seller State", width: "120px", attributes: { class: "text-left field-report" } },

                  { field: "vendorPurchaseReturn.vendorPurchase.invoiceNo", title: "Invoice No", type: "string", width: "100px", attributes: { class: "text-left field-report" } },
                  { field: "vendorPurchaseReturn.vendorPurchase.invoiceDate", title: "Invoice Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=vendorPurchaseReturn.vendorPurchase.invoiceDate=='0001-01-01T00:00:00'?'-': kendo.toString(kendo.parseDate(vendorPurchaseReturn.vendorPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  { field: "productStock.product.commodityCode", title: "Commodity Code", width: "120px", type: "string", attributes: { class: "text-left field-report" },footerTemplate: "Total" },
                  //{ field: "productStock.sellingPrice", title: "Selling Rate(MRP)", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  { field: "vendorPurchaseItem.poValue", title: "Purchase Value(Rs.)", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                    { field: "vendorPurchaseReturn.returnDate", title: "Date of Purchase Return", width: "110px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(vendorPurchaseReturn.returnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                    { field: "vendorPurchaseReturn.subTotal", title: "Value of Goods Returned(Rs.)", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "vendorPurchaseItem.vatValue", title: "Central Sales Tax(Rs.)", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                        { field: "deditNo", title: "Debit/Credit Note No", width: "120px", attributes: { class: "text-left field-report" } },
                  { field: "vendorPurchaseReturn.returnDate", title: "Debit/Credit Note Date", width: "120px", type: "string", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(vendorPurchaseReturn.returnDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },

                  { field: "vendorPurchaseReturn.vendorPurchase.formType", title: "Type of Form", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report" } },
                   { field: "vendorPurchaseReturn.vendorPurchase.comments", title: "Online Form No. If Generated", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report" } },
           
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "vendorPurchaseItem.poValue", aggregate: "sum" },
                        { field: "vendorPurchaseReturn.subTotal", aggregate: "sum" },
                        { field: "vendorPurchaseItem.vatValue", aggregate: "sum" },
                      
                        { field: "total", aggregate: "sum" }

                    ],
                    schema: {
                        model: {
                            fields: {
                                "vendorPurchaseReturn.returnDate": { type: "date" },
                                "vendorPurchaseReturn.vendorPurchase.invoiceNo": { type: "string" },
                                "vendorPurchaseReturn.vendorPurchase.invoiceDate": { type: "date" },
                                "vendorPurchaseReturn.vendorPurchase.vendor.name": { type: "string" },
                                "vendorPurchaseReturn.vendorPurchase.vendor.tinNo": { type: "string" },
                                "productStock.product.commodityCode": { type: "string" },
                                "vendorPurchaseItem.poValue": { type: "number" },
                                "vendorPurchaseReturn.subTotal": { type: "number" },
                                "vendorPurchaseItem.vatValue": { type: "number" },
                              
                                "deditNo": { type: "string" },
                                "vendorPurchaseReturn.returnDate": { type: "date" } 
                            
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            cell.fontFamily = "verdana";
                            cell.wrapText = true;
                            if (row.type == "header") {
                                row.cells[ci].background = "#039394";
                                cell.fontSize = "9";
                                cell.width = "200";
                            }
                            if (row.type == "title") {
                                row.cells[ci].background = "#0c0c0c";
                                cell.fontSize = "9";
                                cell.width = "200";
                                cell.color = "#ffffff";
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd/MM/yyyy');
                                cell.background = "#ffff99";
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "data") {
                                cell.fontSize = "8";
                                if (this.columns[ci].field == "vendorPurchaseReturn.vendorPurchase.vendor.tinNo"
                                    || this.columns[ci].field == "productStock.product.commodityCode"
                                   
                                    ) {
                                    cell.background = "#ccffff";
                                }
                                else if (this.columns[ci].field == "vendorPurchaseReturn.vendorPurchase.vendor.name"
                                    || this.columns[ci].field == "vendorPurchaseReturn.vendorPurchase.vendor.address"
                                    || this.columns[ci].field == "vendorPurchaseReturn.vendorPurchase.invoiceNo"
                                    || this.columns[ci].field == "deditNo"
                                    || this.columns[ci].field == "vendorPurchaseReturn.vendorPurchase.comments")
                                {
                                    cell.background = "#ffcc99";
                                }
                                else if (this.columns[ci].field == "vendorPurchaseReturn.vendorPurchase.invoiceDate"
                                  || this.columns[ci].field == "vendorPurchaseReturn.returnDate") {
                                    cell.background = "#ffff99";
                                }
                                else if (this.columns[ci].field == "vendorPurchaseItem.vatValue"
                                    || this.columns[ci].field == "vendorPurchaseItem.poValue"
                                     || this.columns[ci].field == "vendorPurchaseReturn.subTotal"
                                    
                                ) {
                                    cell.background = "#ccffcc";
                                }
                                else {
                                    cell.background = "#ff99cc";
                                }
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.fontSize = "8"
                                    cell.bold = true;
                                    if (this.columns[ci].field == "vendorPurchaseItem.poValue") {
                                        cell.background = "#ccffcc";
                                        row.cells[ci - 1].value = "Total Value of Purchase Return Goods which Purchased in the Course of Inter-State (Rs.)";
                                        row.cells[ci - 1].bold = true;
                                        row.cells[ci - 1].fontSize = "8";
                                        row.cells[ci - 1].fontFamily = "verdana";
                                        row.cells[ci - 1].hAlign = "right";
                                        row.cells[ci - 1].background = "#ffffff";
                                    }
                                    else if (this.columns[ci].field == "vendorPurchaseItem.poValue"
                                        || this.columns[ci].field == "vendorPurchaseReturn.subTotal"
                                        || this.columns[ci].field == "vendorPurchaseItem.vatValue") {
                                        cell.background = "#ccffcc";
                                    }
                                   
                                }
                                else {
                                    cell.background = "#ffffff";
                                }
                            }
                        }
                    }
                    e.preventDefault();
                    promises[0].resolve(e.workbook);
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //


    //$scope.buyReturnReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.buyReturnReport();
    }

    // chng-3

    $scope.header = "Elixir Technologies";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
      /*  headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: "Annuxure-2", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "TIN No: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);*/
        headerCell = { cells: [{ value:"Annexure 9: List of Purchase Return of Goods Purchased in the Course of Inter-State trade", bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }
    function addSearch(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Tax Period To*:  " + $scope.to, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: "Tax Period From*: " + $scope.from, format: "dd-MM-yyyy", type: "date", bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "Dealer TIN No*: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);


    }
    //

    //
    var promises = [
    $.Deferred(),
    $.Deferred()
    ];

    $("#btnXLSExport").click(function (e) {

        $("#basicDetails").data("kendoGrid").saveAsExcel();
        $("#grid").data("kendoGrid").saveAsExcel();
        // wait for both exports to finish
        $.when.apply(null, promises)
         .then(function (gridWorkbook, ordersWorkbook) {

             // create a new workbook using the sheets of the products and orders workbooks
             var sheets = [
                gridWorkbook.sheets[0],
               ordersWorkbook.sheets[0]

             ];

             sheets[0].title = "Purchase Return CST";
             sheets[1].title = "Basic Details";

             var workbook = new kendo.ooxml.Workbook({
                 sheets: sheets
             });

             // save the new workbook,b
             kendo.saveAs({
                 dataURI: workbook.toDataURL(),
                 fileName: "Purchase Return CST.xlsx"
             })
         });
    });

    $("#basicDetails").kendoGrid({
        dataSource: {

            data: $scope.instance,
            pageSize: 20,
            serverPaging: true
        },
        height: 550,
        pageable: true,
        columns: [
            { field: "PurchaseReturnCST" }
        ],
        excelExport: function (e) {
            e.preventDefault();
            addSearch(e);
            promises[1].resolve(e.workbook);
        }
    });
}]);

