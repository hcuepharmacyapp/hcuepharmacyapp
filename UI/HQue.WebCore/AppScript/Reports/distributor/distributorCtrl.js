﻿app.controller('distributorCtrl', function ($scope, distributorService, instanceModel, pagerServcie) {

    var instance = instanceModel;

    $scope.search = instance;
    //$scope.search.pager = pagerService.page;

    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        distributorService.Branchlist($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    }

    $scope.pharmacySearch = function () {
        $scope.search.page.pageNo = 1;
        distributorService.Branchlist($scope.search).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
        }, function () { });
    }

    $scope.pharmacySearch();

    $scope.pharmacySelect = function (pharmacy) {
        window.location.assign('/DistributorReport/DistributorLowStockList?id=' + pharmacy.id);
    }
});