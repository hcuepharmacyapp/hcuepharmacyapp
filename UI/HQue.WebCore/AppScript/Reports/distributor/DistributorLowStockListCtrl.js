﻿app.controller('lowProductStockReportCtrl', ['$scope', 'distributorService', function ($scope, distributorService) {

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.gridOptions = {
        exporterMenuCsv: true,
        enableGridMenu: true,
        enableFiltering: true,
        enableColumnResizing: true,
        //gridMenuTitleFilter: fakeI18n,
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        data: 'data',
        columnDefs: [
          { field: 'product.name', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Product Name', width: '15%' },
          { field: 'stock', displayName: 'Stock', width: '10%' }
        ],

        gridMenuCustomItems: [
            {
                title: 'Rotate Grid',
                action: function ($event) {
                    this.grid.element.toggleClass('rotated');
                },
                order: 210
            }
        ],

        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;

            //// interval of zero just to allow the directive to have initialized
            //$interval( function() {
            //    gridApi.core.addToGridMenu( gridApi.grid, [{ title: 'Dynamic item', order: 100}]);
            //}, 0, 1);

            gridApi.core.on.columnVisibilityChanged($scope, function (changedColumn) {
                $scope.columnChanged = { name: changedColumn.colDef.name, visible: changedColumn.colDef.visible };
            });
        }
    };

    $scope.init = function (id) {
        distributorService.LowStocklist(id).then(function (response) {
            $scope.data = response.data;
        }, function () { });
    }
}]);