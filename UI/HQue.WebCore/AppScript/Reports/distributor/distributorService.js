﻿app.factory('distributorService', function ($http) {
    return {
        Branchlist: function (searchData) {
            return $http.post('/DistributorReport/DistributorBranchListData', searchData);
        },
        LowStocklist: function (id) {
            return $http.post('/DistributorReport/DistributorLowStockListData?id='+ id);
        },
    }
});