﻿app.controller('renderDevReportCtrl', ['$scope', 'uiGridConstants', '$http', '$interval', '$location', 'customReportsService', 'salesReportService', '$filter', 'getFilterService', function ($scope, uiGridConstants, $http, $interval, $location, customReportsService, salesReportService, $filter, getFilterService) {

    $scope.id = "";
    $scope.reportName = "Render Report";

    $scope.init = function () {

        var qs = $location.search();
        $scope.id = qs['id'];

        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $scope.renderReport();
        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    function reportRequest() {
        this.id = "";
    }
    $scope.getFilter = [];
    $scope.renderReport = function () {
        $.LoadingOverlay("show");

        $scope.type = 'Month';
        var data = new reportRequest();
        data.id = $scope.id;
        
        customReportsService.runDevReport(data).then(function (response) {

            var resp = response.data;
            $scope.reportName = resp.reportName;
            $scope.getFilter = JSON.parse(resp.data);






            //angular.forEach($scope.getFilter, function (value, i) {
            //    var strInput = value.search("/#dt/");
            //    if (strInput == "#dt") {
            //        $scope.getFilter[i] = "input";
            //    }
                
            //});
            //$("#grid").kendoGrid({
            //    excel: {
            //        fileName: "custom report.xlsx",
            //        allPages: true
            //    },
            //    pdf: {
            //        allPages: true,
            //        paperSize: "A4",
            //        margin: { top: "3cm", right: "1cm", bottom: "1cm", left: "1cm" },
            //        landscape: true,
            //        template: $("#page-template").html()
            //    },
            //    columnMenu: true,
            //    pageable: true,
            //    resizable: true,
            //    reorderable: true,
            //    sortable: true,
            //    filterable: {
            //        mode: "column"
            //    },
            //    columns: resp.columns,
            //    dataSource : {
            //        data: resp.data,
            //        paperSize:20
            //    },
            //    excelExport: function (e) {

            //        addHeader(e);

            //        var sheet = e.workbook.sheets[0];
            //        for (var i = 0; i < sheet.rows.length; i++) {
            //            var row = sheet.rows[i];
            //            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
            //                var cell = row.cells[ci];
            //                if (this.columns[ci].attributes) {
            //                    if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
            //                        cell.hAlign = "left";
            //                        cell.format = this.columns[ci].attributes.fformat;
            //                        cell.value = new Date(cell.value);
            //                    }
            //                    if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
            //                        cell.hAlign = "right";
            //                        cell.format = "#" + this.columns[ci].attributes.fformat;
            //                    }
            //                }
            //                if (row.type == "group-footer" || row.type == "footer") {
            //                    if (cell.value) {
            //                        cell.value = $.trim($('<div>').html(cell.value).text());
            //                        cell.value = cell.value.replace('Total:', '');
            //                        cell.hAlign = "right";
            //                        cell.format = "#0.00";
            //                        cell.bold = true;
            //                    }
            //                }
            //            }
            //        }
            //    },
            //});

     
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Sales Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "TIN No: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //

}]);