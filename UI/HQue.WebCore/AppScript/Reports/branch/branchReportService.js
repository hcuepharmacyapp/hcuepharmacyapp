﻿app.factory('branchReportService', function ($http) {
    return {
        list: function (filterData) {
            return $http.post('/BranchReport/listData', filterData);
        },
    }
});
