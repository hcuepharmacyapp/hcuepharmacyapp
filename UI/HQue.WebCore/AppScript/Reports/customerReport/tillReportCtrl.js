﻿app.controller('tillReportCtrl', function ($scope, $rootScope, customerReportService,salesReportService,
      toastr, $filter) {

    $scope.minDate = new Date();
    $scope.searchBtnActive = false;
    $scope.DateAvailables = [{ id: "", transactionDate: "" }]
    $scope.selectedOption = { id: "", transactionDate: "" }
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');


    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,

    };

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.type = '';
    $scope.data = [];

    $scope.search ="";
    $scope.list = [];
    $scope.customerList = true;



    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            if ($scope.branchid == undefined || $scope.branchid == "") {
                $scope.branchid = $scope.instance.id;
            }
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.salesReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

   

    $scope.getTillDates = function () {
      
        $("#grid").empty();
        $scope.customerList = true;
        if ($scope.fromDate == "" || $scope.fromDate === undefined) {
            toastr.info('Please select Valid Date');
            return;
        }
        $.LoadingOverlay("show");
        customerReportService.GetPettyCashDates($scope.fromDate).then(function (response) {
            console.log(JSON.stringify(response.data));
            if (response.data.length > 0) {
                $scope.DateAvailables = response.data;
                console.log(JSON.stringify($scope.DateAvailables));
                $scope.selectedOption = $scope.DateAvailables[0]
                console.log(JSON.stringify($scope.selectedOption));
                $scope.searchBtnActive = false;
            } else {
                //toastr.info('No data found');
                $.LoadingOverlay("hide");
                $scope.searchBtnActive = true;
                $scope.cancel();
                return;
            }
            $.LoadingOverlay("hide");

        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.getTillDates();

    $scope.cashOnHand = 0;
    $scope.diffTotal = 0;
    $scope.status = '';
    $scope.UserName = '';

    $scope.cancel = function () {
        $scope.searchBtnActive = true;
        $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.DateAvailables = [{ id: "", transactionDate: "" }]
        $scope.selectedOption = { id: "", transactionDate: "" };
        $scope.customerList = true;
    }
    $scope.pettyCashDetailsReport = function () {
        var selectedId = $scope.selectedOption.id;
        if (selectedId == "" || selectedId === undefined) {
            toastr.info('Please select the Available Till Date');
            return;
        }
        $.LoadingOverlay("show");
        customerReportService.GetPettycashDetails(selectedId).then(function (response) {
            $scope.data = response.data;

            if (response.data !== undefined && response.data.length > 0) {
                $scope.cashOnHand = response.data[0].total;
                $scope.status = response.data[0].status;
                $scope.diffTotal = response.data[0].difftotal;
                $scope.UserName = response.data[0].username;
            } else {
                toastr.info('No data found');
                $scope.searchBtnActive = true;
                return;
            }

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            } else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/>Till Report <br/>TILL SAVED DATE & TIME:" + $scope.selectedOption.transactionDate + "<br/>";
            }

            console.log(JSON.stringify(response.data));
            $scope.customerList = false;
            var grid = $("#grid").kendoGrid({
                excel: {
                    fileName: "TillReport.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "TillReport.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "slNo", title: "Sl No", width: "40px",  type: "number", attributes: { class: "text-right field-report", ftype: "number" }, footerTemplate: " <div class='report-footer'></div>" },
                  { field: "details", title: "Details", width: "100px", type: "string", attributes: { class: "text-left field-report" } },
                 { field: "inward", title: "Inward", width: "60px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                  { field: "outward", title: "Outward", width: "60px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                { field: "denamination", title: "Denamination", width: "60px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                { field: "amount", title: "Amount", width: "60px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },


                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                         { field: "inward", aggregate: "sum" },
                         { field: "outward", aggregate: "sum" },
                         { field: "amount", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                                "slNo": { type: "number" },
                                "details": { type: "string" },
                                "inward": { type: "number" },
                                "outward": { type: "number" },
                                "denamination": { type: "string" },
                                "amount": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            //added by nandhini for excel s.no
                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }
                            //end
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }

                            if (row.type == "data" && (this.columns[ci].title == "Cash" || this.columns[ci].title == "Card" || this.columns[ci].title == "Credit" || this.columns[ci].title == "Net Value")) {
                                cell.format = "#0.00";
                            }
                        }
                    }
                },
            }).data("kendoGrid");


            grid.dataSource.originalFilter = grid.dataSource.filter;
            grid.dataSource.filter = function () {
                if (arguments.length > 0) {
                    this.trigger("filtering", arguments);
                }

                var result = grid.dataSource.originalFilter.apply(this, arguments);
                if (arguments.length > 0) {
                    this.trigger("filtering", result);
                }

                return result;
            }

            $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                var filters = dataSource.filter();
                var allData = dataSource.data();
                var query = new kendo.data.Query(allData);
                var data = query.filter(filters).data;

            });

            $.LoadingOverlay("hide");

        }, function () {
            $.LoadingOverlay("hide");
        });
    }


    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
            headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: " Till Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
            headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
            e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        
    }

    $scope.btnTXTExport = function () {

        var selectedId = $scope.selectedOption.id;

        window.open('/CustomerReport/ListDataForPrint?pettyId=' + selectedId + '&tillDateTime=' + $scope.selectedOption.transactionDate + '&userName=' + $scope.UserName);

    }

});