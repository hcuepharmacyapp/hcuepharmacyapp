﻿app.controller('customerWiseBalanceCtrl', function ($scope, $rootScope, customerReportService, customerPaymentModel, salesModel, toastr, $filter) {

    $scope.minDate = new Date();


    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
        //Re-load the grid when change the branch name
        if ($scope.branchid != undefined && $scope.branchid != null) {
            $scope.customerReceiptSearch();
        }
    });
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.fromDate = null;
    $scope.toDate = null;

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,

    };

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.type = '';
    $scope.data = [];

    var payment = customerPaymentModel;
    payment.sales = salesModel;
       
    $scope.search = payment;
    $scope.list = [];
    $scope.customerList = true;


    $scope.init = function () {
        $.LoadingOverlay("show");
        customerReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $scope.customerReceiptSearch();
        }, function () {
            $.LoadingOverlay("hide");
        });
        customerReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.customerReceiptSearch = function () {
        $scope.totalDue = 0;
        $.LoadingOverlay("show");

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }

        customerReportService.customerWiseBalanceList($scope.branchid).then(function (response) {
            $scope.list = response.data;
            //var totalDue = $filter('sumFilter')($scope.list);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

   // $scope.customerReceiptSearch();

   
    
    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }

    }

    $scope.checkFromDate = function () {
        var dt = $("#fromDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if ($scope.toDate != undefined && $scope.toDate != null) {
                    $scope.checkToDate1($scope.fromDate, $scope.toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var dt = $("#toDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1($scope.fromDate, $scope.toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    
    $scope.changefilters = function () {       
        $scope.search.values = "";

        $scope.search.customer = undefined;
        if ($scope.search.select == 'customer') {
            $scope.selectMobile = false;
            $scope.selectCustomer = true;
            $scope.chkDate = true;
            
        } else if ($scope.search.select == 'mobile') {
            $scope.selectMobile = true;
            $scope.selectCustomer = false;
            $scope.chkDate = true;
        }       
        else {  
            $scope.selectMobile = false;
            $scope.selectCustomer = false;
            $scope.chkDate = false;
            $scope.cancel();
        }
    };

    function isEmpty(value) {
        return (typeof value !== undefined || value !== null || value!=="");
    }
    $scope.customerDetailsReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.fromDate,
            toDate: $scope.toDate
        }

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }

        $scope.search.fromDate = $scope.fromDate;
        $scope.search.toDate = $scope.toDate;
        $scope.search.customerName = $scope.search.customer;
        $scope.search.customerMobile = $scope.search.mobile;

      //  if ($scope.search.customerName!=undefined || $scope.search.customerMobile!=undefined) {

        customerReportService.customerDetailsList($scope.search, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                $scope.customerList = false;
                var grid = $("#grid").kendoGrid({
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                      { field: "invoiceNo", title: "Bill No", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "invoiceDate", title: "Bill Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(invoiceDate), 'dd/MM/yyyy') #" },
                     { field: "invoiceAmount", title: "Bill Amount", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "Total: <div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                      { field: "age", title: "Age", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number" }, footerTemplate: " <div class='report-footer'></div>" },
                    { field: "debit", title: "Paid Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },

                    { field: "credit", title: "Pending Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },


                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                             { field: "invoiceAmount", aggregate: "sum" },
                             { field: "debit", aggregate: "sum" },
                             { field: "credit", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "invoiceNo": { type: "string" },
                                    "invoiceDate": { type: "date" },
                                    "invoiceAmount": { type: "number" },
                                    "age": { type: "number" },
                                    "debit": { type: "number" },
                                    "credit": { type: "number" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                }).data("kendoGrid");


                grid.dataSource.originalFilter = grid.dataSource.filter;
                grid.dataSource.filter = function () {
                    if (arguments.length > 0) {
                        this.trigger("filtering", arguments);
                    }

                    var result = grid.dataSource.originalFilter.apply(this, arguments);
                    if (arguments.length > 0) {
                        this.trigger("filtering", result);
                    }

                    return result;
                }

                $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    var filters = dataSource.filter();
                    var allData = dataSource.data();
                    var query = new kendo.data.Query(allData);
                    var data = query.filter(filters).data;

                });

                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        //else {
        //    $.LoadingOverlay("hide");
        //}
        

    //}
   
    $scope.filter = function (type) {
        $scope.type = type;
        $scope.customerDetailsReport();
    };
    
    $scope.cancel = function () {
        $scope.fromDate = null;
        $scope.toDate = null;
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;
        $scope.search.select = null;
        $scope.search.customer = null;
        $scope.selectCustomer = false;
        $scope.selectMobile = false;
        $scope.customerList = true;
        $scope.chkDate = false;
        $scope.search.mobile = null;
        $scope.search.customer = null;
        //$scope.customerDetailsReport();

    }
    $scope.onCustomerSelect = function (obj) {       
        $scope.search.customer = obj.customerName;
        $scope.search.mobile = obj.customerMobile;
    }

    $scope.getCustomerName = function (val) {
        return customerReportService.getCustomerName($scope.branchid,val,'').then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)]) continue;
                flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                return item;
            });
        });
    };

    $scope.getCustomerMobile = function (val) {
        return customerReportService.getCustomerMobile($scope.branchid,'', val).then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)]) continue;
                flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                return item;
            });
        });
    };
    
});

app.filter('sumFilter', function () {
    return function (groups) {
        var totalDue = 0;
        for (i = 0; i < groups.length; i++) {
            totalDue = totalDue + groups[i].customerDebit;
        };
        return totalDue;
    };
});
