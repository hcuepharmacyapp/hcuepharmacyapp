﻿app.controller('paymentReceiptCancelReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'salesReportService', 'salesModel', 'salesItemModel', 'paymentModel', 'customerPaymentModel', 'supplierReportService', 'customerReportService', '$filter', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, salesReportService, salesModel, salesItemModel, paymentModel, customerPaymentModel, supplierReportService, customerReportService, $filter) {
    
    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;
    $scope.dateRangeExceeds = false; // hide excel export more than two months

    var customerPayment = customerPaymentModel;
    $scope.searchCustomerPayment = customerPayment;

    var supplierPayment = paymentModel;
    $scope.searchSupplierPayment = supplierPayment;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false   
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.type = "TODAY";
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.onSupplierSelect = function (obj) {
        $scope.searchSupplierPayment.supplier = obj.name;
        $scope.searchSupplierPayment.mobile = obj.mobile;
        $scope.searchSupplierPayment.vendorId = obj.id;
    }
        
    $scope.getSupplierName = function (val) {
        return supplierReportService.getSupplierName(val, '').then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.onCustomerSelect = function (obj) {
        $scope.searchCustomerPayment.customer = obj.customerName;
        $scope.searchCustomerPayment.mobile = obj.customerMobile;
    }

    $scope.getCustomerName = function (val) {
        return customerReportService.getCustomerName(val, '').then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)]) continue;
                flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                return item;
            });
        });
    };
    

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.paymentList = true;
            $scope.buyReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.cancel = function () {
        $("#grid").empty();
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.selectCustomer = true;
        $scope.selectSupplier = true;
      //  $scope.searchCustomerPayment.customer = "";
        $scope.searchSupplierPayment.supplier = "";
        $scope.search.select = "";
        $scope.paymentList = true;
        $scope.data = [];
        $scope.buyReport();

    }


    $scope.selectCustomer = true;
    $scope.selectSupplier = true;
  

    $scope.changefilters = function () {
        $scope.search.values = "";

        $scope.search.supplier = undefined;
        $scope.search.customer = undefined;
        if ($scope.search.select == 'supplier') {
            $scope.selectCustomer = true;
            $scope.selectSupplier = false;
            //$scope.searchCustomerPayment.customer = "";
            $scope.searchSupplierPayment.supplier = "";
            //$scope.chkDate = true;

        } else if ($scope.search.select == 'customer') {
            $scope.selectCustomer = false;
            $scope.selectSupplier = true;
           // $scope.searchCustomerPayment.customer = "";
            $scope.searchSupplierPayment.supplier = "";
            $scope.chkDate = true;
        }
        else {
            $scope.selectCustomer = true;
            $scope.selectSupplier = true;
           // $scope.searchCustomerPayment.customer = "";
            $scope.searchSupplierPayment.supplier = "";
            //$scope.chkDate = false;
            //$scope.cancel();
        }
    };

   
    $scope.buyReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        $("#grid").empty();


        if ($scope.search.select == 'supplier' || $scope.search.select == "" || $scope.search.select == undefined) {

            $scope.searchSupplierPayment.fromDate = $scope.from;
            $scope.searchSupplierPayment.toDate = $scope.to;
            $scope.searchSupplierPayment.select1 = $scope.search.select;

            supplierReportService.supplierWisePaymentReceiptCancellation($scope.searchSupplierPayment, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                if ($scope.searchSupplierPayment.fromDate != undefined || $scope.searchSupplierPayment.fromDate != null) {
                    $scope.dayDiff($scope.searchSupplierPayment.fromDate);
                }
                $scope.type = "";

                angular.forEach($scope.data, function (data1, key) {
                    data1.vendorPurchase.goodsRcvNo = data1.vendorPurchase.billSeries + data1.vendorPurchase.goodsRcvNo;
                });

                $scope.paymentList = false;

                var pdfHeader = "";
                if ($scope.instance != undefined) {
                    if (angular.isObject($scope.currentInstance)) {
                        $scope.pdfHeader = $scope.currentInstance;
                        $scope.instance = $scope.currentInstance;
                    }
                    else {
                        $scope.pdfHeader = $scope.instance;
                    }
                }
                else {
                    supplierReportService.getInstanceData().then(function (pdfResponse) {
                        $scope.pdfHeader = pdfResponse.data;
                    }, function () { });
                }

                if ($scope.pdfHeader) {
                    if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                        $scope.pdfHeader.drugLicenseNo = "";
                    else
                        $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                    if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                        $scope.pdfHeader.gsTinNo = "";
                    else
                        $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                    if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                        $scope.pdfHeader.fullAddress = "";
                    else
                        $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                    pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
                }


                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Payment Modification & Cancellation.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Payment Modification & Cancellation.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()                       
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height: 350,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                  
                   { field: "transactionDate", title: "Payment Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=transactionDate == null ? '-' : kendo.toString(kendo.parseDate(transactionDate), 'dd/MM/yyyy') #" },
                   { field: "vendorPurchase.goodsRcvNo", title: "GRN No", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "vendorPurchase.invoiceNo", title: "Invoice No", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "vendorPurchase.invoiceDate", title: "Invoice Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=vendorPurchase.invoiceDate == null ? '-' : kendo.toString(kendo.parseDate(vendorPurchase.invoiceDate), 'dd/MM/yyyy') #" },
                   { field: "credit", title: "Invoice Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                   { field: "debit", title: "Paid Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                   { field: "paymentMode", title: "Payment Mode", width: "110px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "bankName", title: "Bank Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "bankBranchName", title: "Branch Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "ifscCode", title: "IFSC Code", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "chequeNo", title: "cheque No", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                   { field: "chequeDate", title: "Cheque Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=chequeDate == null ? '-' : kendo.toString(kendo.parseDate(chequeDate), 'dd/MM/yyyy') #" },
                   { field: "paymentType", title: "Modified & Cancelled", width: "120px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [

                        ],
                        schema: {
                            model: {
                                fields: {
                                   
                                    "transactionDate": { type: "date" },
                                    "vendorPurchase.goodsRcvNo": { type: "string" },
                                    "vendorPurchase.invoiceNo": { type: "string" },
                                    "vendorPurchase.invoiceDate": { type: "date" },
                                    "credit": { type: "number" },
                                    "debit": { type: "number" },
                                    "paymentMode": { type: "string" },
                                    "bankName": { type: "string" },
                                    "bankBranchName": { type: "string" },
                                    "ifscCode": { type: "string" },
                                    "chequeNo": { type: "string" },
                                    "chequeDate": { type: "date" },
                                    "paymentType": { type: "string" },
                                   
                                    
                                }
                            }
                        },
                        pageSize: 20
                    },
                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }

        if ($scope.search.select == 'customer') {

            $scope.searchCustomerPayment.fromDate = $scope.from;
            $scope.searchCustomerPayment.toDate = $scope.to;
            $scope.searchCustomerPayment.customerName = $scope.searchCustomerPayment.customer;
            //$scope.searchCustomerPayment.select1 = $scope.search.select;
           // $scope.search.customerName = $scope.search.customer;
          

            customerReportService.customerWisePaymentReceiptCancellation($scope.searchCustomerPayment, $scope.branchid).then(function (response) {
                $scope.data = response.data;
                $scope.paymentList = false;
               // $scope.customerList = false;
                var grid = $("#grid").kendoGrid({
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                      { field: "paymentDate", title: "Payment Date", width: "110px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=paymentDate == null ? '-' : kendo.toString(kendo.parseDate(paymentDate), 'dd/MM/yyyy') #" },
                      { field: "invoiceNo", title: "Bill No", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "invoiceDate", title: "Bill Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" } ,footerTemplate: "Total :",template: "#=invoiceDate == null ? '-' : kendo.toString(kendo.parseDate(invoiceDate), 'dd/MM/yyyy') #" },
                      { field: "invoiceAmount", title: "Bill Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #<span>.00</span></div>" },
                      { field: "paymentAmount", title: "Paid Amount", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0') #<span>.00</span></div>" },
                      { field: "paymentType", title: "Payment Mode", width: "110px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "chequeNo", title: "Cheque No", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "chequeDate", title: "Cheque Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=chequeDate == null ? '-' : kendo.toString(kendo.parseDate(chequeDate), 'dd/MM/yyyy') #" },
                      { field: "cardNo", title: "Card No", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "cardDate", title: "Cheque Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=cardDate == null ? '-' : kendo.toString(kendo.parseDate(cardDate), 'dd/MM/yyyy') #" },


                    ],
                    dataSource: {
                        data: response.data,
                        aggregate: [
                             { field: "invoiceAmount", aggregate: "sum" },
                             { field: "paymentAmount", aggregate: "sum" },
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "paymentDate": { type: "date" },
                                    "invoiceNo": { type: "string" },
                                    "invoiceDate": { type: "date" },
                                    "invoiceAmount": { type: "number" },
                                    "paymentAmount": { type: "number" },
                                    "paymentType": { type: "string" },
                                    "chequeNo": { type: "string" },
                                    "chequeDate": { type: "date" },
                                    "cardNo": { type: "string" },
                                    "cardDate": { type: "date" },
                                }
                            }
                        },
                        pageSize: 20
                    },

                }).data("kendoGrid");


                grid.dataSource.originalFilter = grid.dataSource.filter;
                grid.dataSource.filter = function () {
                    if (arguments.length > 0) {
                        this.trigger("filtering", arguments);
                    }

                    var result = grid.dataSource.originalFilter.apply(this, arguments);
                    if (arguments.length > 0) {
                        this.trigger("filtering", result);
                    }

                    return result;
                }

                $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    var filters = dataSource.filter();
                    var allData = dataSource.data();
                    var query = new kendo.data.Query(allData);
                    var data = query.filter(filters).data;

                });

                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        else {
            $.LoadingOverlay("hide");
        }
        
    }

    function sheet(sheet) {
       for (var i = 0; i < sheet.rows.length; i++) {
           var row = sheet.rows[i];
           for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
               var cell = row.cells[ci];

               if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                   cell.hAlign = "left";
                   cell.format = this.columns[ci].attributes.fformat;
                   cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
               }
               if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                   cell.hAlign = "right";
                   cell.format = "#" + this.columns[ci].attributes.fformat;
               }

               if (row.type == "group-footer" || row.type == "footer") {
                   if (cell.value) {
                       cell.value = $.trim($('<div>').html(cell.value).text());
                       cell.value = cell.value.replace('Total:', '');
                       cell.hAlign = "right";
                       cell.format = "#0.00";
                       cell.bold = true;
                   }
               }
           }
       }
   }

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Payment Modification & Cancellation", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
    $scope.dayDiff = function (frmDate) {

        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.searchSupplierPayment.fromDate = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;

        var fromDate = new Date($scope.searchSupplierPayment.fromDate);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));
        $scope.searchSupplierPayment.fromDate = $filter('date')($scope.searchSupplierPayment.fromDate, 'dd/MM/yyyy');

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
}]);
