﻿app.controller('customerPaymentCtrl', function ($scope, $rootScope, customerReportService,
    customerPaymentModel, salesReportService, salesModel, toastr, $filter) {




    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
        //Re-load the grid when change the branch name
        if ($scope.branchid != undefined && $scope.branchid != null) {
            $scope.customerPaymentReport();
        }
    });

    var payment = customerPaymentModel;

    $scope.search = payment;
    $scope.dateRangeExceeds = false; // hide excel export more than two months

    $scope.minDate = new Date();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.fromDate = null;
    $scope.toDate = null;

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
    };

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.type = '';
    $scope.data = [];

   
    //$scope.list = [];
    //$scope.vendorList = [];

   

   

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }

    }

    $scope.checkFromDate = function () {
        var dt = $("#fromDate").val();
        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validFromDate = true;

                    if ($scope.toDate != undefined && $scope.toDate != null) {
                        $scope.checkToDate1($scope.fromDate, $scope.toDate);
                    }
                }
                else {
                    $scope.validFromDate = false;
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else
            $scope.validFromDate = true;
    }

    $scope.checkToDate = function () {
        var dt = $("#toDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validToDate = true;
                    $scope.checkToDate1($scope.fromDate, $scope.toDate);
                }
                else {
                    $scope.validToDate = false;
                }
            }
            else {
                $scope.validToDate = false;
            }
        }
        else
            $scope.validToDate = true;
    }


    $scope.changefilters = function () {
        $scope.search.values = "";

        $scope.search.customer = undefined;
        if ($scope.search.select == 'customer') {
            $scope.selectMobile = false;
            $scope.selectCustomer = true;
            $scope.chkDate = true;

        } else if ($scope.search.select == 'mobile') {
            $scope.selectMobile = true;
            $scope.selectCustomer = false;
            $scope.chkDate = true;
        }
        else {
            $scope.selectMobile = false;
            $scope.selectCustomer = false;
            $scope.chkDate = false;
            $scope.cancel();
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $scope.customerPaymentReport();
        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    function isEmpty(value) {
        return (typeof value !== undefined || value !== null || value !== "");
    }
    $scope.customerPaymentReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.fromDate,
            toDate: $scope.toDate
        }

        

        $scope.search.fromDate = $scope.fromDate;
        $scope.search.toDate = $scope.toDate;
       

        if ($scope.search.select == 'customer')
        {
            $scope.search.customerName = $scope.search.customer;
            $scope.search.customerMobile = "";
        }
        else
        {
            $scope.search.customerName = "";
            $scope.search.customerMobile = $scope.search.mobile;
        }

        if ($scope.search.customer == undefined)
            $scope.search.customerName = "";
        if ($scope.search.mobile == undefined)
            $scope.search.customerMobile = "";

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }


        //if ($scope.search.customer != undefined || $scope.search.mobile != undefined) {
        // , $scope.branchid
        customerReportService.customerPaymentDetails($scope.search,$scope.branchid).then(function (response) {
            $scope.data = response.data;
            //$scope.supplierList = false;
            if ($scope.search.fromDate != undefined || $scope.search.fromDate != null)
                if ($scope.search.fromDate != undefined || $scope.search.fromDate != null) {
                    $scope.dayDiff($scope.search.fromDate);
                }

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                $scope.pdfHeader = $scope.instance;
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }
           

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                    $scope.pdfHeader.tinNo = "";
                else
                    $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
            }


            var grid = $("#grid").kendoGrid({
                excel: {
                    fileName: "Customer Receipt Report.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1600, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Customer Receipt Report.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [

                      { field: "paymentDate", title: "Payment Date", width: "110px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=paymentDate == null ? '-' : kendo.toString(kendo.parseDate(paymentDate), 'dd/MM/yyyy') #" },
                       { field: "customerName", title: "Customer Name", width: "110px", format: "{0}", type: "string", attributes: { class: "text-left field-report" } },
                       { field: "customerMobile", title: "Mobile No", width: "110px", format: "{0}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "invoiceNo", title: "Invoice No", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "invoiceDate", title: "Invoice Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, footerTemplate: "Total :", template: "#=invoiceDate == null ? '-' : kendo.toString(kendo.parseDate(invoiceDate), 'dd/MM/yyyy') #" },
                      { field: "invoiceAmount", title: "Invoice Amount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                      { field: "paymentAmount", title: "Paid Amount", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                       //{ field: "customerDebit", title: "Balance Amount", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: " <div class='report-footer'>#= kendo.toString(sum, '0') #<span>.00</span></div>" },
                      { field: "paymentType", title: "Payment Mode", width: "110px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "chequeNo", title: "Cheque No", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "chequeDate", title: "Cheque Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=chequeDate == null ? '-' : kendo.toString(kendo.parseDate(chequeDate), 'dd/MM/yyyy') #" },
                      { field: "cardNo", title: "Card No", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                      { field: "cardDate", title: "Card Date", width: "100px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=cardDate == null ? '-' : kendo.toString(kendo.parseDate(cardDate), 'dd/MM/yyyy') #" },

                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                          { field: "invoiceAmount", aggregate: "sum" },
                          { field: "paymentAmount", aggregate: "sum" },
                          //{ field: "customerDebit", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                               
                                "paymentDate": { type: "date" },
                                "customerName": { type: "string" },
                                "customerMobile": { type: "string" },
                                "invoiceNo": { type: "string" },
                                "invoiceDate": { type: "date" },
                                "invoiceAmount": { type: "number" },
                                "paymentAmount": { type: "number" },
                                "customerDebit": { type: "number" },
                                "paymentType": { type: "string" },
                                "chequeNo": { type: "string" },
                                "chequeDate": { type: "date" },
                                "cardNo": { type: "string" },
                                "cardDate": { type: "date" },

                            }
                        }
                    },
                    pageSize: 20
                },
                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },

            }).data("kendoGrid");


            grid.dataSource.originalFilter = grid.dataSource.filter;
            grid.dataSource.filter = function () {
                if (arguments.length > 0) {
                    this.trigger("filtering", arguments);
                }

                var result = grid.dataSource.originalFilter.apply(this, arguments);
                if (arguments.length > 0) {
                    this.trigger("filtering", result);
                }

                return result;
            }

            $("#grid").data("kendoGrid").dataSource.bind("filtering", function (arguments) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                var filters = dataSource.filter();
                var allData = dataSource.data();
                var query = new kendo.data.Query(allData);
                var data = query.filter(filters).data;

            });

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
        //}
        //else {
        //    $.LoadingOverlay("hide");
        //}
    }


   // $scope.customerPaymentReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.customerPaymentReport();
    };

    $scope.cancel = function () {
        $scope.fromDate = null;
        $scope.toDate = null;
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;
        $scope.search.select = null;
        $scope.search.customer = null;
        $scope.selectCustomer = false;
        $scope.selectMobile = false;
        // $scope.supplierList = true;
        $scope.chkDate = false;
        $scope.search.mobile = null;
        $scope.search.customer = null;
        $scope.search.vendorId = null;
        $scope.customerPaymentReport();
    }

    $scope.onCustomerSelect = function (obj) {
        $scope.search.customer = obj.customerName;
        $scope.search.mobile = obj.customerMobile;
    }

    $scope.getCustomerName = function (val) {
        return customerReportService.getCustomerName($scope.branchid,val, '').then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)]) continue;
                flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                return item;
            });
        });
    };

    $scope.getCustomerMobile = function (val) {
        return customerReportService.getCustomerMobile($scope.branchid,'', val).then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)]) continue;
                flags[$filter('uppercase')(response.data[i].customerName) && (response.data[i].customerMobile)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                return item;
            });
        });
    };

    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Customer Receipt Report", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "TIN No: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    $scope.dayDiff = function (frmDate) {
        $scope.today = $filter('date')(new Date(), 'MM/dd/yyyy');
        $scope.dtFrom = $filter('date')(frmDate, 'MM/dd/yyyy');

        var day = 24 * 60 * 60 * 1000;
        var fromDate = new Date($scope.dtFrom);
        var today = new Date($scope.today);
        $scope.dayDifference = Math.round(Math.abs((fromDate.getTime() - today.getTime()) / (day)));

        if ($scope.dayDifference > 60) {
            $scope.dateRangeExceeds = true;
        }
        else {
            $scope.dateRangeExceeds = false;
        }
    };
});

app.filter('sumFilter', function () {
    return function (groups) {
        var totalDue = 0;
        for (i = 0; i < groups.length; i++) {
            totalDue = totalDue + groups[i].balance;
        };
        return totalDue;
    };
});
