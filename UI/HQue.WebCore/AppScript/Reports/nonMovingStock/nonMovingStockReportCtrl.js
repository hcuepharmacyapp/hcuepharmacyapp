﻿app.controller('nonMovingStockReportCtrl', ['$scope', '$rootScope', '$http', '$interval', '$q', 'nonMovingStockReportService', 'productStockModel', 'productModel', 'toastr', 'cacheService', '$filter', 'ModalService', '$rootScope', function ($scope, $rootScope,$http, $interval, $q, nonMovingStockReportService, productStockModel, productModel, toastr, cacheService, $filter, ModalService, $rootScope) {

    $scope.data = [];
    $scope.instanceList = [];
    $scope.checkData = [];
    $scope.isreturn = false;
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });
   
    $scope.from = null;//$filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = null;//$filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.data = [];

    $scope.selectedInstance = function () {
        $scope.selectedInstance;
    }

    $scope.instance = function () {
        $.LoadingOverlay("show");
        nonMovingStockReportService.selectInstances().then(function (response) {
            $scope.instanceList = response.data;
            $scope.selectedInstance.id = $scope.instanceList[0].id;
        
            $scope.stockReport();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.instance();

    // chng-2
    $scope.init = function () {
        $.LoadingOverlay("show");
        nonMovingStockReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
        }, function () {
            $.LoadingOverlay("hide");
        });
        nonMovingStockReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    /*$scope.searchName = function (name) {
        $("#grid").data("kendoGrid").dataSource.filter({
            filters: [
                {
                    field: "product.name",
                    operator: "contains",
                    value: name
                }
            ]
        });

        $scope.usingservice = $filter('findobj')(grid._data, grid.tbody[0].rows);

        //$('.isMovingStock').prop('checked', true);
    };
    */
    $rootScope.$on("close", function () {
       // $scope.stockReport();
    });

    var grid = "";

    $scope.stockReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        /*var stock = 0;
        if ($scope.stockMonth == undefined) {
            stock = 0;
        } else {
            stock = $scope.stockMonth;
        } */
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        $scope.isreturn = false;
        if ($scope.branchid == $scope.instance.id)
        {
            $scope.isreturn = true;
        }
        $scope.setFileName();
        //nonMovingStockReportService.stockList($scope.stockMonth, $scope.branchid).then(function (response) {
        nonMovingStockReportService.stockList($scope.branchid, data).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                nonMovingStockReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "Non Moving Stock.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1710, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Non Moving Stock.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                //pageable: true,
                resizable: true,
                reorderable: true,
                height:350,
                sortable: true,
                filterable: {
                    mode: "column"
                    
                },
                columns: [

                    { field: "isMovingStock", template: "<input type='checkbox' name='isMovingStock' value='true' ng-model='isMovingStock' class='isMovingStock' />", title: "Non-Moving Stock", width: "110px", attributes: { class: "text-left field-report" } },

                    
                    { field: "product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" } },
                    { field: "batchNo", title: "Batch", width: "110px", attributes: { class: "text-left field-report" }},
                    { field: "expireDate", title: "Expiry", width: "90px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                     { field: "lastSaleDate", title: "Last Sale Date", width: "120px", type: "string", attributes: { class: "text-left field-primary", style: "color:red" } },
                   
                   { field: "vendorPurchase.invoiceDate", title: "Purchase Invoice Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(vendorPurchase.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                     //{ field: "createdAt", title: "Date of Last Sale", width: "160px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(createdAt, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                    { field: "age", title: "Age", width: "80px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" } },
                    { field: "vendor.name", title: "Vendor", width: "120px", attributes: { class: "text-left field-report" } },
                    { field: "product.category", title: "Category", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "product.schedule", title: "Schedule", width: "120px", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "product.type", title: "Type", width: "120px", type: "string", attributes: { class: "text-left field-report" } ,footerTemplate: "Total"},
                    { field: "stock", title: "Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                    { field: "vat", title: "VAT/GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" } },
                    { field: "costPrice", title: "Cost Price", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                    { field: "mrp", title: "MRP", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  
                  
                ],
                dataBound: function () {
                    $(".isMovingStock").bind("change", function (e)     {
                        $(e.target).closest("tr").toggleClass("k-state-selected");
                        var ind = $(e.target).closest("tr").index();

                        var rowIndex = $(this).parent().index();
                        var cellIndex = $(this).parent().parent().index();
                        grid = $("#grid").data("kendoGrid");


                        if (jQuery(e.target).is(":checked") == true)
                        {
                            //response.data[ind].isMovingStock = 0;
                            grid._data[cellIndex].isMovingStock = 1;
                            //$scope.saveMoving = true;

                            //$scope.tempids.push(grid._data[cellIndex])
                        }
                        else
                        {
                            //response.data[ind].isMovingStock = null;
                            //response.data[ind].isMovingStock = 0;
                            grid._data[cellIndex].isMovingStock = null;
                            //$scope.tempids.push(grid._data[cellIndex])
                           // $scope.saveMoving = false;
                           // $scope.stockReport();
                        }
                       
                    });
                },
                
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "stock", aggregate: "sum" },
                        { field: "costPrice", aggregate: "sum" },
                        { field: "mrp", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "isMovingStock": { type: "number" },
                                "product.name": { type: "string" },
                                "batchNo": { type: "string" },
                                "stock": { type: "number" },
                                "expireDate": { type: "date" },
                                //"createdAt": { type: "date" },
                                "lastSaleDate": { type: "string" },
                                 "vendorPurchase.invoiceDate": { type: "date" },
                                "age": { type: "number" },
                                "vendor.name": { type: "string" },
                                "vat": { type: "number" },
                                "costPrice": { type: "number" },
                                "mrp": { type: "number" },
                                "product.category": { type: "string" },
                                "product.schedule": { type: "string" },
                                "product.type": { type: "string" }
                            }
                        }
                    },
                    //pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });
            
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //$scope.stockReport();

    var productStock = productStockModel;
    $scope.productStock = productStock;
   // $scope.saveMoving = false;

    $scope.SaveMovingStock = function()
    {

        $.LoadingOverlay("show");

        if (grid._data == undefined)
        {
            toastr.error('Please select the Non Moving Stock');
            $scope.stockReport();
            $.LoadingOverlay("hide");
            return false;
        }
        console.log(grid._data);
        //$scope.searchName('');
        nonMovingStockReportService.movingStock(grid._data).then(function (response) {
            //$scope.checkData = grid._data;
           //console.log(JSON.stringify(response.data));
           //if (grid._data != undefined)
           //{
               //window.location.href = '/VendorPurchaseReturn/ReturnIndex';
               //toastr.success('Non Moving Stock updated successfully');
               $scope.PopupVendors();
               $.LoadingOverlay("hide");
           //}
           //else
           //{
           //    toastr.success('Please select the Non Moving Stock');
           //    $scope.stockReport();
           //    $.LoadingOverlay("hide");
           //}

               
          // }
            
        }, function () {
            $.LoadingOverlay("hide");
        }); 
    }

    $scope.PopupVendors = function () {
        var allData = grid._data; //grid._data;
        var popupData = [];
        console.log("PopupVendors");
        //console.log($scope.checkData);
        console.log(grid._data);
        for (var i = 0; i < allData.length; i++) {
            if (allData[i].isMovingStock == true && (allData[i].vendor.name == null || allData[i].vendor.name == '')) {
                allData[i].vendorid = "";
                popupData.push(allData[i]);
            }
        }
        if (popupData.length == 0)
        {
            window.location.href = '/VendorPurchaseReturn/ReturnIndex';
            return false;
        }
        else
        {
            cacheService.set('selectedItems', popupData)
            var m = ModalService.showModal({
                controller: "selectNonMovingVendorsCtrl",
                templateUrl: "selectVendors"
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.message = "You said " + result;
                });
            });
            return false;
        }
       

    };
    $scope.order = ['product.name', 'batchNo', 'reportExpireDate', 'lastSaleDate', 'reportInvoiceDate', 'age', 'vendor.name', 'product.category', 'product.schedule', 'product.type', 'stock', 'vat', 'costPrice', 'mrp'];
    $scope.setFileName = function () {
        $scope.fileNameNew = "NON MOVING STOCK" + $scope.instance.name;
    }
    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Non Moving Stock", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
    
}]);

app.filter('findobj', function () {

    return function (list,rows) {

        return list.filter(function (element) {

            if (element.isMovingStock == 1) {
                if (rows[list.indexOf(element)].dataset.uid == element.uid)
                {
                    $("[data-uid='" + rows[list.indexOf(element)].dataset.uid + "']").addClass("k-state-selected");;
                    $("[data-uid='" + rows[list.indexOf(element)].dataset.uid + "'] .isMovingStock").prop('checked', true);
                    return true;
                }
            }          
        });
    };
})