﻿app.controller('stockReportCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'stockReportService', 'productStockModel', 'productModel', '$filter', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, stockReportService, productStockModel, productModel, $filter) {

    $scope.data = [];
    $scope.instanceList = [];
    
    $scope.stockType = "nonzero";

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.flagSearch = false;
    $scope.fromDate = null;
    $scope.toDate = null;
    $scope.categoryName = null;
    $scope.manufacturerName = null;
    $scope.rackNoname = null;
    $scope.boxNoname = null;
    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.Pcategory = false;
    $scope.DateCondition = false;
    $scope.Manufacturer = false;
    $scope.rackNo = false;
    $scope.boxNo = false;
    $scope.productWise = false;
    $scope.isAlphabet = false;
    
    $scope.changefilters = function () {
        var _categorytype = $scope.categorytype;

        $scope.Alphabet1 = '';
        $scope.Alphabet2 = '';

        if (_categorytype == "Category") {
            $scope.flagSearch = true;
            $scope.Pcategory = true;
            $scope.DateCondition = false;
            $scope.Manufacturer = false;
            $scope.rackNo = false;
            $scope.boxNo = false;
            $scope.rackNoname = null;
            $scope.boxNoname = null;
            $scope.fromDate = null;
            $scope.toDate = null;
            $scope.manufacturerName = null;
            $scope.categoryName = null;
            $scope.isAlphabet = false;
        }
        else if (_categorytype == "rackno") {
            $scope.flagSearch = true;
            $scope.Pcategory = false;
            $scope.DateCondition = false;
            $scope.Manufacturer = false;
            $scope.rackNo = true;
            $scope.boxNo = false;
            $scope.rackNoname = null;
            $scope.boxNoname = null;
            $scope.fromDate = null;
            $scope.toDate = null;
            $scope.manufacturerName = null;
            $scope.categoryName = null;
            $scope.isAlphabet = false;
       }
       else if (_categorytype == "boxno") {
           $scope.flagSearch = true;
           $scope.Pcategory = false;
           $scope.DateCondition = false;
           $scope.Manufacturer = false;
           $scope.rackNo = false;
           $scope.boxNo = true;
           $scope.rackNoname = null;
           $scope.boxNoname = null;
           $scope.fromDate = null;
           $scope.toDate = null;
           $scope.manufacturerName = null;
           $scope.categoryName = null;
           $scope.isAlphabet = false;
       }
        else if (_categorytype == "Date") {
            $scope.flagSearch = false;
            $scope.fromDate = null;
            $scope.toDate = null;
            $scope.DateCondition = true;
            $scope.Pcategory = false;
            $scope.Manufacturer = false;
            $scope.manufacturerName = null;
            $scope.rackNo = false;
            $scope.boxNo = false;
            $scope.rackNoname = null;
            $scope.boxNoname = null;
            $scope.categoryName = null;
            $scope.isAlphabet = false;
        }
        else if (_categorytype == "Alphabet") {
            $scope.flagSearch = false;
            $scope.Pcategory = false;
            $scope.DateCondition = false;
            $scope.rackNo = false;
            $scope.boxNo = false;
            $scope.rackNoname = null;
            $scope.boxNoname = null;
            $scope.Manufacturer = false;
            $scope.manufacturerName = null;
            $scope.categoryName = null;
            $scope.fromDate = null;
            $scope.toDate = null;
            $scope.isAlphabet = true;
        }
        else {
            $scope.flagSearch = true;
            $scope.Pcategory = false;
            $scope.DateCondition = false;
            $scope.rackNo = false;
            $scope.boxNo = false;
            $scope.rackNoname = null;
            $scope.boxNoname = null;
            $scope.Manufacturer = true;
            $scope.manufacturerName = null;
            $scope.categoryName = null;
            $scope.fromDate = null;
            $scope.toDate = null;
            $scope.isAlphabet = false;
        }
    }

    $scope.checkFromDate = function () {
        var frmDate = $scope.fromDate;
        var toDate = $scope.toDate;

        var dt = $("#fromDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if (toDate != undefined && toDate != null) {
                    $scope.checkToDate1(frmDate, toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }


    $scope.checkToDate = function () {
        var frmDate = $scope.fromDate;
        var toDate = $scope.toDate;

        var dt = $("#toDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1(frmDate, toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'//newly added
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.getCategory = function (val) {

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        return stockReportService.getCategoryList($scope.branchid, $scope.stockType, val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.getManufacturer = function (val) {

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        return stockReportService.getManufacturerList($scope.branchid, $scope.stockType, val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.getrackno = function (val) {

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        return stockReportService.getRackNoList($scope.branchid, $scope.stockType, val, 'rackno').then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    $scope.getboxno = function (val) {

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        return stockReportService.getRackNoList($scope.branchid, $scope.stockType, val, 'boxno').then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    $scope.onCategorySelect = function (obj) {
        $scope.categoryName = obj.product.category;
        $scope.flagSearch = false;
    }

    $scope.onManufacturerSelect = function (obj) {
        $scope.manufacturerName = obj.product.manufacturer;
        $scope.flagSearch = false;
    }
    $scope.onRackNoSelect = function (obj) {
        $scope.rackNoname = obj.product.rackNo;
        $scope.flagSearch = false;
    }
    $scope.onBoxNoSelect = function (obj) {
        $scope.boxNoname = obj.product.boxNo;
        $scope.flagSearch = false;
    }

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };
    

    $scope.gridOptions = {
        showColumnFooter: true,
        exporterMenuCsv: true,
        enableGridMenu: true,
        enableFiltering: true,
        enableColumnResizing: true,
        //gridMenuTitleFilter: fakeI18n,
        paginationPageSizes: [25, 50, 75],
        paginationPageSize: 25,
        data: 'data',
        columnDefs: [
          { field: 'product.name', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Product Name', width: '10%' },
          { field: 'batchNo', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Batch No', width: '10%' },
          { field: 'stock', headerCellClass: $scope.highlightFilteredHeader, displayName: 'Quantity', width: '10%' },
          { field: 'expireDate', displayName: 'Expire Date', cellFilter: 'date:"dd-MM-yyyy"', width: '10%', type: 'date' },
          { field: 'vendor.name', displayName: 'Vendor Name', width: '10%' },
          { field: 'vat', headerCellClass: $scope.highlightFilteredHeader, displayName: 'VAT', width: '5%' },
          { field: 'gstTotal', headerCellClass: $scope.highlightFilteredHeader, displayName: 'GST Price', width: '5%' },
          { field: 'costPrice', displayName: 'Total Cost Price Value(VAT)', width: '10%', cellFilter: 'number: 2', aggregationType: uiGridConstants.aggregationTypes.sum, footerCellTemplate: '<div class="ng-binding">Total: {{col.getAggregationValue() | number:2 }}</div>' },
          { field: 'costPriceGst', displayName: 'Total Cost Price Value(GST)', width: '10%', cellFilter: 'number: 2', aggregationType: uiGridConstants.aggregationTypes.sum, footerCellTemplate: '<div class="ng-binding">Total: {{col.getAggregationValue() | number:2 }}</div>' },
          { field: 'mrp', displayName: 'Total MRP Value', width: '10%', aggregationType: uiGridConstants.aggregationTypes.sum, footerCellTemplate: '<div class="ng-binding">Total: {{col.getAggregationValue() | number:2 }}</div>' },
          { field: 'product.category', displayName: 'Category', width: '10%' },
          { field: 'product.manufacturer', displayName: 'Manufacturer', width: '10%' },
          { field: 'product.schedule', displayName: 'Schedule', width: '10%' },
          { field: 'product.type', displayName: 'Type', width: '10%' },
           { field: 'product.genericName', displayName: 'Generic Name', width: '10%' },
        ],

        gridMenuCustomItems: [
            {
                title: 'Rotate Grid',
                action: function ($event) {
                    this.grid.element.toggleClass('rotated');
                },
                order: 210
            }
        ],

        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;

            gridApi.core.on.columnVisibilityChanged($scope, function (changedColumn) {
                $scope.columnChanged = { name: changedColumn.colDef.name, visible: changedColumn.colDef.visible };
            });
        }
    };

    $scope.selectedInstance = function () {
        $scope.selectedInstance;
    }

    $scope.instance = function () {
        $.LoadingOverlay("show");
        stockReportService.selectInstance().then(function (response) {
            $scope.instanceList = response.data;
            //$scope.selectedInstance.id = $scope.instanceList[0].id;
            // $scope.stockReport();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    // $scope.instance();

    // chng-2
    var isHideValue = false;
    $scope.init = function () {
        $.LoadingOverlay("show");
        stockReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            isHide = $scope.instance.accountId;
            if (isHide == ("c503ca6e-f418-4807-a847-b6886378cf0b") || isHide == ("67067991-0e68-4779-825e-8e1d724cd68b") || isHide == ("63ED272D-1122-4E04-AFE8-C5A0F13B4F0E") || isHide == ("b322f4ad-c054-441a-8a43-c950dd205e50")) {
                isHideValue = $scope.instance.accountId;
            };

            $rootScope.$broadcast('LoginBranch', $scope.instance);
            //$scope.stockReport();  by San 28-09-2017

        }, function () {
            $.LoadingOverlay("hide");
        });
        stockReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    
    $scope.stockReport = function () {
        if ($scope.flagSearch) {
            return; //|| !$scope.validFromDate || !$scope.validToDate || !$scope.validDate
        }
        if ($scope.categorytype == "Category" && $scope.categoryName == undefined) {
            return;
        }
        if ($scope.categorytype == "Manufacturer" && $scope.manufacturerName == undefined) {
            return;
        }
        if ($scope.categorytype == "rackno" && $scope.rackNoname == undefined) {
            return;
        }
        if ($scope.categorytype == "boxno" && $scope.boxNoname == undefined) {
            return;
        }
        if ($scope.isAlphabet) {
            if (($scope.Alphabet1 == undefined || $scope.Alphabet1 == '') || ($scope.Alphabet2 == undefined || $scope.Alphabet2 == '')) {
                return;
            }
            if ($scope.Alphabet1.charCodeAt(0) > $scope.Alphabet2.charCodeAt(0)) {
                return;
            }
        }
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.fromDate,
            toDate: $scope.toDate
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        setFileName();
        if ($scope.fromDate!=null){
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.fromDate;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        };
        //stockReportService.stockList($scope.branchid, $scope.stockType, $scope.categoryName, data).then(function (response) {
        stockReportService.stockList($scope.categoryName, $scope.manufacturerName, $scope.rackNoname, $scope.boxNoname, $scope.branchid, $scope.stockType, $scope.productWise, $scope.Alphabet1, $scope.Alphabet2, data).then(function (response1) {
            $("#grid").empty();
            $scope.data = response1.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                stockReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }

            if ($scope.productWise) {
                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Stock.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1700, 1000],
                        landscape: true,
                        allPages: true,
                        fileName: "Stock.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height: 350,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                        { field: "product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" } },
                      { field: "vendor.name", title: "Vendor", width: "200px", attributes: { class: "text-left field-report", }, hidden: isHideValue },
                      { field: "product.category", title: "Category", width: "120px", type: "string", attributes: { class: "text-left text-bold" }, hidden: isHideValue },
                      { field: "product.manufacturer", title: "Manufacturer", width: "120px", type: "string", attributes: { class: "text-left text-bold" }, hidden: isHideValue },
                      { field: "product.schedule", title: "Schedule", width: "100px", type: "string", attributes: { class: "text-left text-bold" }, hidden: isHideValue },
                      { field: "product.type", title: "Type", width: "100px", type: "string", attributes: { class: "text-left text-bold" }, footerTemplate: "Total", hidden: isHideValue },
                      { field: "stock", title: "Stock", width: "120px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                      { field: "mrp", title: "MRP", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, hidden: isHideValue, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                      { field: "product.genericName", title: "Generic Name", width: "100px", type: "string", attributes: { class: "text-left text-bold" }, hidden: isHideValue },
                      { field: "product.rackNo", title: "Rack No", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },
                      { field: "product.boxNo", title: "Box No", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },

                    ],
                    dataSource: {
                        data: response1.data,
                        aggregate: [
                            { field: "stock", aggregate: "sum" },
                            { field: "mrp", aggregate: "sum" }
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "product.name": { type: "string" },
                                    "stock": { type: "number" },
                                    "vendor.name": { type: "string" },
                                    "mrp": { type: "number" },
                                    "product.category": { type: "string" },
                                    "product.manufacturer": { type: "string" },
                                    "product.schedule": { type: "string" },
                                    "product.type": { type: "string" },
                                    "product.genericName": { type: "string" },
                                    "product.rackNo": { type: "string" },
                                    "product.boxNo": { type: "string" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });

            }
            else {
                $("#grid").kendoGrid({
                    excel: {
                        fileName: "Stock.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1700, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Stock.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height: 350,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                        { field: "product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" } },
                      { field: "batchNo", title: "Batch", width: "100px", attributes: { class: "text-left field-report" } },
                      { field: "expireDate", title: "Expiry", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                      { field: "vendor.name", title: "Vendor", width: "200px", attributes: { class: "text-left field-report" } },
                      { field: "product.category", title: "Category", width: "120px", type: "string", attributes: { class: "text-left text-bold" } },
                      { field: "product.manufacturer", title: "Manufacturer", width: "120px", type: "string", attributes: { class: "text-left text-bold" } },
                      { field: "product.schedule", title: "Schedule", width: "100px", type: "string", attributes: { class: "text-left text-bold" } },
                      { field: "product.type", title: "Type", width: "100px", type: "string", attributes: { class: "text-left text-bold" }, footerTemplate: "Total" },
                      { field: "stock", title: "Stock", width: "120px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                      { field: "product.rackNo", title: "Rack No", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },
                        { field: "product.boxNo", title: "Box No", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },
                      { field: "vat", title: "VAT %", width: "60px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },
                          { field: "gstTotal", title: "GST %", width: "70px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },
                      { field: "costPrice", title: "Total Cost Price Value(VAT)", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                      { field: "costPriceGst", title: "Total Cost Price Value(GST)", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                      { field: "mrp", title: "Total MRP Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                       { field: "product.genericName", title: "Generic Name", width: "100px", type: "string", attributes: { class: "text-left text-bold" } },
                        { field: "product.displayStatus", title: "Status", width: "100px", type: "string", attributes: { class: "text-left field-report" } },
                    ],
                    dataSource: {
                        data: response1.data,
                        aggregate: [
                            { field: "stock", aggregate: "sum" },
                            { field: "costPrice", aggregate: "sum" },
                            { field: "costPriceGst", aggregate: "sum" },
                            { field: "mrp", aggregate: "sum" }
                        ],
                        schema: {
                            model: {
                                fields: {
                                    "product.name": { type: "string" },
                                    "batchNo": { type: "string" },
                                    "stock": { type: "number" },
                                    "expireDate": { type: "date" },
                                    "vendor.name": { type: "string" },
                                    "vat": { type: "number" },
                                    "costPrice": { type: "number" },
                                    "costPriceGst": { type: "number" },
                                    "mrp": { type: "number" },
                                    "product.category": { type: "string" },
                                    "product.manufacturer": { type: "string" },
                                    "product.schedule": { type: "string" },
                                    "product.type": { type: "string" },
                                    "product.genericName": { type: "string" }
                                }
                            }
                        },
                        pageSize: 20
                    },

                    excelExport: function (e) {

                        addHeader(e);

                        var sheet = e.workbook.sheets[0];
                        for (var i = 0; i < sheet.rows.length; i++) {
                            var row = sheet.rows[i];
                            for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                                var cell = row.cells[ci];

                                if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                    cell.hAlign = "left";
                                    cell.format = this.columns[ci].attributes.fformat;
                                    cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                                }
                                if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                    cell.hAlign = "right";
                                    cell.format = "#" + this.columns[ci].attributes.fformat;
                                }

                                if (row.type == "group-footer" || row.type == "footer") {
                                    if (cell.value) {
                                        cell.value = $.trim($('<div>').html(cell.value).text());
                                        cell.value = cell.value.replace('Total:', '');
                                        cell.hAlign = "right";
                                        cell.format = "#0.00";
                                        cell.bold = true;
                                    }
                                }
                            }
                        }
                    },
                });

            }


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });



    }
    $scope.order = ['product.name', 'vendor.name', 'product.category', 'product.manufacturer', 'product.schedule', 'product.type', 'stock', 'mrp', 'product.genericName', 'product.rackNo', 'product.boxNo'];
    $scope.orderNew = ['product.name', 'batchNo', 'reportExpireDate', 'vendor.name', 'product.category', 'product.manufacturer', 'product.schedule', 'product.type', 'stock', 'product.rackNo', 'product.boxNo', 'vat', 'gstTotal', 'costPrice', 'costPriceGst', 'mrp', 'product.genericName', 'statusText'];

    $scope.clearSearch = function () {
        $scope.toDate = null;
        $scope.fromDate = null;
        $scope.categoryName = null;
        $scope.manufacturerName = null;
        $scope.categorytype = null;
        $scope.Pcategory = false;
        $scope.DateCondition = false;
        $scope.Manufacturer = false;
        $scope.flagSearch = false;
        $scope.stockType = "nonzero";
        $scope.productWise = false;
        $scope.rackNoname = null;
        $scope.boxNoname = null;
        $scope.rackNo = false;
        $scope.boxNo = false;        
        $("#grid").empty();
        $scope.data = [];
        $scope.Alphabet1 = '';
        $scope.Alphabet2 = '';
        $scope.isAlphabet = false;
        $scope.init();

    }

    // By San 28-09-2017


    angular.isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null || val === ""
    }

    //$scope.stateChanged = function (qId) {
    //    if ($scope.productWise) { //If it is checked
    //        
    //    }
    //}

    $scope.header = "Qbitz Technologies ? Anna Nagar ? 90384923843";
    function addHeader(e) {
        $scope.stitle = "";
        if ($scope.stockType == "nonzero") {
            $scope.stitile = "Available";
        }
        else if ($scope.stockType == "expiry") {
            $scope.stitile = "Expiry";
        }
        else if ($scope.stockType == "zero") {
            $scope.stitile = "Zero";
        }
        else if ($scope.stockType == "all") {
            $scope.stitile = "All";
        }
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.stitile + " Stock", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
   setFileName = function () {
        $scope.fileNameNew = "Stock List_" + $scope.instance.name;
    }
    $scope.checkAlphabet = function (val1, val2) {
        var x = val1, y = val2;

        if (x != '' && x != undefined) {
            if ((x.charCodeAt(0) >= 97 && x.charCodeAt(0) <= 122) || (x.charCodeAt(0) >= 65 && x.charCodeAt(0) <= 90)) {
                $scope.Alphabet1 = x.toUpperCase();
            }
            else {
                $scope.Alphabet1 = "";
            }
        }

        if (y != '' && y != undefined) {
            if ((y.charCodeAt(0) >= 97 && y.charCodeAt(0) <= 122) || (y.charCodeAt(0) >= 65 && y.charCodeAt(0) <= 90)) {
                $scope.Alphabet2 = y.toUpperCase();
            }
            else {
                $scope.Alphabet2 = "";
            }
        }
    }
        
    $scope.btnTXTExport = function () {

        if ($scope.flagSearch) {
            return;
        }
        if ($scope.categorytype == "Category" && $scope.categoryName == undefined) {
            return;
        }
        if ($scope.categorytype == "Manufacturer" && $scope.manufacturerName == undefined) {
            return;
        }
        if ($scope.categorytype == "rackno" && $scope.rackNoname == undefined) {
            return;
        }
        if ($scope.categorytype == "boxno" && $scope.boxNoname == undefined) {
            return;
        }
        //$.LoadingOverlay("show");
        if ($scope.isAlphabet) {
            if (($scope.Alphabet1 == undefined || $scope.Alphabet1 == '') || ($scope.Alphabet2 == undefined || $scope.Alphabet2 == '')) {
                return;
            }
            if ($scope.Alphabet1.charCodeAt(0) > $scope.Alphabet2.charCodeAt(0)) {
                return;
            }
        }
        var data = {
            fromDate: $scope.fromDate,
            toDate: $scope.toDate
        }

        var category = (escape($scope.categoryName) == "undefined" || escape($scope.categoryName) == "null") ? '' : escape($scope.categoryName);

        window.open('/stockReport/StockListDataForPrint?category=' + category + '&manufacturer=' + $scope.manufacturerName + '&sRackNo=' + $scope.rackNoname + '&sBoxNo=' + $scope.boxNoname + '&id=' + $scope.branchid + '&sStockType=' + $scope.stockType + '&isProductWise=' + $scope.productWise + '&alphabet1=' + $scope.Alphabet1 + '&alphabet2=' + $scope.Alphabet2 + '&fromDate=' + $scope.fromDate + '&toDate=' + $scope.toDate);

    }


}]);