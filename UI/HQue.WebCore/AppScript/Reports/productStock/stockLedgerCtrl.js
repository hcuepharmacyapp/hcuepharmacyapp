﻿app.controller('stockLedgerCtrl', function ($scope, productStockService, stockReportService, productModel, productStockModel, $filter, productService) {

    $scope.minDate = new Date();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.totalQuantityIn = 0;
    $scope.totalQuantityOut = 0;
    $scope.totalstock = 0;

    //$scope.$on('branchname', function (event, id) {
    //    $scope.branchid = id;
    //});

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    //$scope.fromDate = null;
    //$scope.toDate = null;

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
    };

    $scope.fromDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.selectedProduct = null;

    $scope.getProducts = function (val) {


        return productService.stockdrugFilterData(val).then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }
            

            var origArr = output;
            var newArr = [],
       origLen = origArr.length,
       found, x, y;

            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {

                    if (origArr[x].name === newArr[y].name) {

                        found = true;
                        break;
                    }
                }

                if (!found) {
                    newArr.push(origArr[x]);
                }
            }


            return newArr.map(function (item) {
                return item;
            });
        });
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }

    }

    $scope.checkFromDate = function () {
        var dt = $("#fromDate1").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if ($scope.toDate != undefined && $scope.toDate != null) {
                    $scope.checkToDate1($scope.fromDate, $scope.toDate);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var dt = $("#toDate1").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1($scope.fromDate, $scope.toDate);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }


    $scope.search = function () {     
        if ($scope.selectedProduct.id != null && $scope.validDate == true) {
            $.LoadingOverlay("show");
            $scope.totalstock = $scope.selectedProduct.totalstock;
            var stockLedgerData = {
                fromDate: $scope.fromDate,
                toDate: $scope.toDate,
                selectedProduct: $scope.selectedProduct
            };
           
            return stockReportService.getStockLedger(stockLedgerData)
                .then(function (response) {

                    $scope.stockLedger = response.data;
                   
                    $scope.totalQuantityIn = 0;
                    $scope.totalQuantityOut = 0;
           
                    $scope.transferMiddle = [];
                    for (var x = 0; x < $scope.stockLedger.length; x++) {
                        if ($scope.stockLedger[x].transactionType == 14) {
                            $scope.transferMiddle.push($scope.stockLedger[x]);
                        }
                        //Stock In Quantity
                        if ($scope.stockLedger[x].stockType == 2) {
                            $scope.totalQuantityIn += $scope.stockLedger[x].quantity;
                        }
                        //Stock Out Quantity
                        if ($scope.stockLedger[x].stockType == 1) {
                            $scope.totalQuantityOut += $scope.stockLedger[x].quantity;
                        }
                        $scope.stockLedger.OpeningStock1 = $scope.stockLedger[x].openingStock1;
                    }
                                    
                    $.LoadingOverlay("hide");

                }, function (error) {
                    console.log(error);
                    $.LoadingOverlay("hide");
                });
        }
    }


    $scope.cancel = function () {
        $scope.fromDate = null;
        $scope.toDate = null;
        $scope.selectedProduct = null;
        $scope.stockLedger = null;
        $scope.transferMiddle = null;
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;

        var ele = document.getElementById("fromDate");
        ele.focus();

    }


});

app.directive('focusBox', function () {
    return {
        "link": function (scope, element, attrs) {

            element.bind("keydown", function (event) {


                var valueLength = attrs.$$element[0].value.length;

                //Enter 
                if (event.which === 13) {
                    if (valueLength !== 0) {
                        ele = document.getElementById(attrs.nextid);
                        ele.focus();
                    }
                }
                //BackSpace
                if (event.which === 8) {
                    if (valueLength === 0) {
                        ele = document.getElementById(attrs.previousid);
                        ele.focus();
                        event.preventDefault();
                    }
                }
            });
        }
    };
});