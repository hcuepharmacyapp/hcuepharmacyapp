﻿app.controller('physicalStockHistoryReportCtrl', ['$scope', '$rootScope', '$filter', 'stockReportService', 'productService', function ($scope, $rootScope, $filter, stockReportService, productService) {

    $scope.maxDate = new Date();

    $scope.userSearch = "search";
    $scope.husername = "";
    $scope.huserid = "";
    $scope.getUserName = function (val) {

        return stockReportService.GetUserName(val).then(function (response) {

            var origArr = response.data;
            var newArr = [],
            origLen = origArr.length,
            found, x, y;
            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if (origArr[x].mobile === newArr[y].mobile && origArr[x].name === newArr[y].name) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    if (origArr[x].mobile == "") {

                    } else {
                        newArr.push(origArr[x]);
                    }

                }
            }
            return newArr.map(function (item) {
                return item;
            });
        });
    };

    $scope.getUserId = function (val) {
        return stockReportService.GetUserId(val).then(function (response) {
            console.log(response.data);
            var origArr = response.data;
            var newArr = [],
            origLen = origArr.length,
            found, x, y;
            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if (origArr[x].userId === newArr[y].userId && origArr[x].mobile === newArr[y].mobile) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }
            return newArr.map(function (item) {
                return item;
            });
        });
    };

    $scope.onUserSelect = function (obj) {
        if ($scope.userSearch == 'userName') {
            $scope.husername = obj;
        }
        else if ($scope.userSearch == 'userId') {
            $scope.huserid = obj;
        }
    };
    $scope.onUserChange = function () {
        if (typeof ($scope.husername) == "object") {
            $scope.husername = document.getElementById('userName').value;;
        }
        if (typeof ($scope.selectedProduct) == "object") {
            $scope.huserid = document.getElementById('userId').value;;
        }
    }

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    $scope.checkFromDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#fromDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validFromDate = true;

                    if (toDate != undefined && toDate != null) {
                        $scope.checkToDate1(frmDate, toDate);
                    }
                }
                else {
                    $scope.validFromDate = false;
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else
            $scope.validFromDate = true;
    }

    $scope.checkToDate = function () {
        var frmDate = $scope.from;
        var toDate = $scope.to;

        var dt = $("#toDate").val();

        if (dt) {
            if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
                if (isValidDate(dt)) {
                    $scope.validToDate = true;
                    $scope.checkToDate1(frmDate, toDate);
                }
                else {
                    $scope.validToDate = false;
                }
            }
            else {
                $scope.validToDate = false;
            }
        }
        else
            $scope.validToDate = true;
    }

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.type = 'TODAY';

    $scope.init = function () {
        $.LoadingOverlay("show");
        stockReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.physicalStockHistoryReport();
        }, function () {
            $.LoadingOverlay("hide");
        });        
    }    

    $scope.getProducts = function (val) {
        var instanceid = $scope.branchid;
        if (instanceid != undefined && instanceid != null) {
            return productService.InstanceWisedrugFilter(instanceid, val).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        }
    };

    $scope.selectedProduct = "";
    $scope.onProductSelect = function (obj) {
        $scope.selectedProduct = obj;
    };

    $scope.clearSearch = function () {
        $scope.type = 'TODAY';
        $scope.selectedProduct = "";        
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');       
        $scope.data = [];
        $("#grid").empty();
        $scope.validDate = true;
        $scope.validFromDate = true;
        $scope.validToDate = true;
        $scope.userSearch = "search";
        $scope.husername = "";
        $scope.huserid = "";
    }

    $scope.focusNextId = function (id) {
        var ele = document.getElementById(id);
        ele.focus();
    };

    $scope.physicalStockHistoryReport = function () {
        $.LoadingOverlay("show");

        var productVal = "";
        if ($scope.selectedProduct == undefined || $scope.selectedProduct == null)
            $scope.selectedProduct = "";
        if(typeof($scope.selectedProduct) == "string")
            productVal = $scope.selectedProduct;
        else
            productVal = $scope.selectedProduct.id;

        var userName = "";
        var userId = "";
        if ($scope.userSearch == 'userName') {
            if ($scope.husername.name != undefined)
                userName = $scope.husername.mobile;
            else
                userName = document.getElementById('userName').value;
        }
        else if ($scope.userSearch == 'userId') {
            if ($scope.huserid.userId != undefined)
                userId = $scope.huserid.userId;
            else
                userId = document.getElementById('userId').value;
        }

        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }

        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        $scope.setFileName();
        stockReportService.physicalStockHistoryList(data, $scope.type, $scope.branchid, productVal, userName, userId).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                stockReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }

            $("#grid").kendoGrid({
                excel: {
                    fileName: "Physical Stock Verification History Report.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1500, 1000],
                    landscape: true,
                    allPages: true,
                    fileName: "Physical Stock Verification History Report.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
                  { field: "product.name", title: "Product", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-left field-highlight" } },
                  { field: "physicalStock", title: "Physical Stock", width: "110px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total Physical Stock : <div class='report-footer'>#= kendo.toString(sum, '0') == null ? 0 : kendo.toString(sum, '0') #</div>" },
                  { field: "currentStock", title: "System Stock", width: "110px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total System Stock : <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                  { field: "stockDifference", title: "Difference", width: "110px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "Total Difference Stock : <div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                  { field: "createdAt", title: "Date", width: "90px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=createdAt == null ? '' : kendo.toString(kendo.parseDate(createdAt, 'yyyy-MM-dd'), 'dd/MM/yyyy') #" },
                  { field: "openedAt", title: "Opened At", width: "110px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd/MM/yyyy h:mm tt" }, template: "#=openedAt == null ? '' : kendo.toString(kendo.parseDate(openedAt, 'yyyy-MM-dd h:mm'), 'dd/MM/yyyy h:mm tt') #" },
                  { field: "closedAt", title: "Closed At", width: "110px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd/MM/yyyy h:mm tt" }, template: "#=closedAt == null ? '' : kendo.toString(kendo.parseDate(closedAt, 'yyyy-MM-dd h:mm'), 'dd/MM/yyyy h:mm tt') #" },
                  { field: "user.name", title: "User", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-left field-highlight" } },
                  //{ field: "user.userId", title: "User Id", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-left field-highlight" } },
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [                       
                        { field: "physicalStock", aggregate: "sum" },
                        { field: "currentStock", aggregate: "sum" },
                        { field: "stockDifference", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                                "product.name": { type: "string" },                                
                                "physicalStock": { type: "number" },
                                "currentStock": { type: "number" },
                                "stockDifference": { type: "number" },
                                "createdAt": { type: "date" },
                                "openedAt": { type: "date" },
                                "closedAt": { type: "date" },
                                "user.name": { type: "string" },
                                //"user.userId": { type: "string" },
                            }
                        }
                    },
                    pageSize: 20
                },
                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];
                            
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['product.name', 'physicalStock', 'currentStock', 'stockDifference', 'reportCreateDate', 'reportOpenDate', 'reportCloseDate', 'user.name'];
    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.physicalStockHistoryReport();
    };
    $scope.setFileName = function () {
        $scope.fileNameNew = "PHYSICAL STOCK VERIFICATION HISTORY" + $scope.instance.name;
    };
    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Physical Stock Verification History", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

}]);
