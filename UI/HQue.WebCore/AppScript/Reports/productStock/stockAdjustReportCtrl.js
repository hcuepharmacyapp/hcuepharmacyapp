﻿app.controller('stockAdjustReportCtrl', ['$scope', '$http', '$interval', '$q', 'salesReportService', 'stockReportService', 'stockAdjustmentModel', 'productModel', 'productStockModel', 'productService', '$filter', function ($scope, $http, $interval, $q, salesReportService, stockReportService, stockAdjustmentModel, productModel, productStock, productService, $filter) {
    
    var stockAdjust = stockAdjustmentModel;    
    $scope.search = stockAdjust;
    
    $scope.search.select = null;
    $scope.search.drugName = null;
    $scope.search.selectedValue = null;
    $scope.productCondition = false;
    $scope.search.select1 = null;
    $scope.totalCondition = false;

    var sno = 0;

    //To get value from branch controller and assign to the local variable
    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.validDate = true;
    $scope.validFromDate = true;
    $scope.validToDate = true;

    function isValidDate(s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    }

    $scope.checkToDate1 = function (date01, date02) {
        var date1 = new Date(date01);
        var date2 = new Date(date02);
        $scope.validDate = true;
        if (date1 > date2) {
            $scope.validDate = false;
        }
        else {
            $scope.validDate = true;
        }
    }

    $scope.checkFromDate = function () {
        var dt = $("#fromDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validFromDate = true;

                if ($scope.to != undefined && $scope.to != null) {
                    $scope.checkToDate1($scope.from, $scope.to);
                }
            }
            else {
                $scope.validFromDate = false;
            }
        }
        else {
            $scope.validFromDate = false;
        }
    }

    $scope.checkToDate = function () {
        var dt = $("#toDate").val();

        if (dt.length == 10 && dt.charAt(2) == '/' && dt.charAt(5) == '/') {
            if (isValidDate(dt)) {
                $scope.validToDate = true;
                $scope.checkToDate1($scope.from, $scope.to);
            }
            else {
                $scope.validToDate = false;
            }
        }
        else {
            $scope.validToDate = false;
        }
    }

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];
    $scope.type = "Today";
    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        salesReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;

            $scope.stockAdjustReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        salesReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.getProducts = function (val) {
     
            return productService.InstancedrugFilter(val).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
       
    };

    $scope.getProductId = function () {

        $scope.search.selectedValue = $scope.search.drugName.id;
        $scope.stockAdjustReport();
    };

    $scope.changefilters = function () {
        $scope.search.drugName = null;
        $scope.search.selectedValue = null;
        if ($scope.search.select == "Product") {
            $scope.productCondition = true;
            $scope.totalCondition = false;
        }        
        else if ($scope.search.select == "Batch") {
            $scope.productCondition = false;
            $scope.totalCondition = false;
        }
        else{
            $scope.totalCondition = true;
            $scope.productCondition = false;
        }
    }

    $scope.clearSearch = function () {
        $scope.search.select = null;
        $scope.search.drugName = null;
        $scope.search.selectedValue = null;
        $scope.productCondition = false;
        $scope.search.select1 = null;
        $scope.totalCondition = false;
    }

    $scope.stockAdjustReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        $("#grid").empty();
        $scope.search.instanceId = $scope.branchid;
        $scope.search.dates = data;
        $scope.setFileName();
        stockReportService.getStockAdjustmentData($scope.type, $scope.search).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";
            
            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                salesReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + " / " + "GSTIN No:" + $scope.pdfHeader.gsTinNo + "<br/> Stock Adjustment Details";
            }


            $("#grid").kendoGrid({
                    excel: {
                        fileName: "Stock Adjustment Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        paperSize: [1500, 1000], // Scaling in pt - 8.5"x11" page ratio
                        landscape: true,
                        allPages: true,
                        fileName: "Stock Adjustment Details.pdf",
                        margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        multiPage: true,
                        template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                        //template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    height:400,
                    filterable: {
                        mode: "column"
                    },
                    columns: [
                   { field: "sNo", template: "#= ++record #", type: "string", title: "S.No", width: "90px", attributes: { ftype: "sno", class: " text-left field-report" } },
                   { field: "productStock.productName", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },
                   { field: "productStock.batchNo", title: "Batch", width: "110px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                   { field: "productStock.expireDate", title: "Expire Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                   { field: "productStock.vat", title: "VAT/GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total: " },
                   { field: "previousStockValue", title: "Stock", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                    { field: "adjustedStock", title: "Change", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }},
                    { field: "productStock.stock", title: "Updated Stock", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                    { field: "total", title: "Price", width: "100px", width: "120px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div style='float:right;margin-left:3px;' class='report-footer'>#= kendo.toString(sum, 'n0') #.00</div>" },
                    { field: "createdAt", title: "Adjusted Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(createdAt, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                    { field: "adjustedPerson", title: "Adjusted By", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } }
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "previousStockValue", aggregate: "sum" },
                          { field: "productStock.stock", aggregate: "sum" },
                          { field: "total", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "sNo": { type: "number" },
                                "productStock.productName": { type: "string" },
                                "productStock.batchNo": { type: "number" },
                                "productStock.expireDate": { type: "date" },
                                "productStock.vat": { type: "number" },
                                "previousStockValue": { type: "number" },
                                "adjustedStock": { type: "number" },
                                "productStock.stock": { type: "number" },
                                "total": { type: "number" },
                                "createdAt": { type: "date" },
                                "adjustedPerson": { type: "string" }
                            }
                        }
                    },
                    pageSize: 20
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            var cell = row.cells[ci];
                            if (this.columns[ci].attributes.ftype == "sno" && i == 3) {
                                sno = 0;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "sno") {
                                cell.hAlign = "left";
                                sno = sno + 1;
                                cell.value = sno;
                            }

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //
    $scope.setFileName = function () {
        $scope.fileNameNew = "STOCK ADJUSTMENT DETAILS_" + $scope.instance.name;
    };
    $scope.order = ['productStock.productName', 'select', 'reportExpireDate', 'productStock.vat', 'previousStockValue', 'adjustedStock', 'productStock.stock', 'total', 'reportCreatedAt', 'adjustedPerson'];
    //$scope.buyReport();

    $scope.filter = function (type) {
        $scope.type = type;
        $scope.clearSearch();

        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }

        $scope.stockAdjustReport();
    }


    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Stock Adjustment Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
}]);
