﻿app.controller('transferDetailReportCtrl', ['$scope', '$rootScope', 'stockReportService', 'tempVendorPurchaseItemModel', 'productModel', '$filter', function ($scope, $rootScope, stockReportService, tempVendorPurchaseItemModel, productModel, $filter) {

    var tempVendorPurchaseItem = tempVendorPurchaseItemModel;
   
    $scope.search = tempVendorPurchaseItem;

    $scope.$on('branchname', function (event, id, obj) {
        $scope.branchid = id;
        $scope.currentInstance = obj;
    });

    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.selectedProduct = {
        "name": ""
    };

    $scope.productId = "";
    $scope.select1 = "invoice";

    $scope.onProductSelect = function (obj) {
        $scope.productId = obj.id;
    }

    $scope.getProducts = function (val) {
        var instanceid = $scope.branchid;
        if (instanceid != undefined && instanceid != null) {
            return stockReportService.tempStockProductList(val, instanceid).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        }
    };

    $scope.init = function () {
        $.LoadingOverlay("show");
        stockReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
            $rootScope.$broadcast('LoginBranch', $scope.instance);
            $scope.TransferReport();

        }, function () {
            $.LoadingOverlay("hide");
        });
        stockReportService.getUserData().then(function (response) {
            $scope.userData = response.data;

        }, function () { });
    }

    $scope.TransferReport = function () {
        $("#grid").empty();
        if ($scope.select1 == "summary") {
            $scope.TransferSummaryeport();
        }
        else if ($scope.select1 == "invoice") {
            $scope.TransferInvoiceReport();
        }
        else if ($scope.select1 == "detail") {
            $scope.TransferDetailReport();
        }
    };

   
    $scope.TransferSummaryeport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        $("#grid").empty();
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        $scope.setFileName();
        stockReportService.stockTransferDetail($scope.type, data, $scope.branchid, $scope.select1).then(function (response) {
            $scope.data = response.data;            
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                stockReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "Stock Transfer.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1600, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Stock_Transfer.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [
         
                  { field: "stockTransfer.transferDate", title: "Transfer Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=stockTransfer.transferDate == null ? '-' : kendo.toString(kendo.parseDate(stockTransfer.transferDate), 'dd/MM/yyyy') #", footerTemplate: "Total:" },
                    { field: "stockTransfer.transferAmount", title: "Transfered Value (Rs.)", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                    { field: "stockTransfer.acceptedAmount", title: "Accepted Value", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                     { field: "stockTransfer.pendingAmount", title: "Accepted Pending Value", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
                    { field: "stockTransfer.rejectedAmount", title: "Rejected Value", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
              ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                       
                          { field: "stockTransfer.acceptedAmount", aggregate: "sum" },
                            { field: "stockTransfer.pendingAmount", aggregate: "sum" },
                              { field: "stockTransfer.rejectedAmount", aggregate: "sum" },
                                { field: "stockTransfer.transferAmount", aggregate: "sum" },
                    ],
                    schema: {
                        model: {
                            fields: {
                                "stockTransfer.transferAmount": { type: "number" },
                                "stockTransfer.acceptedAmount": { type: "number" },
                                "stockTransfer.pendingAmount": { type: "number" },
                                "stockTransfer.rejectedAmount": { type: "number" },
                         
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['reportTransferDate', 'stockTransfer.transferAmount', 'stockTransfer.acceptedAmount', 'stockTransfer.pendingAmount', 'stockTransfer.rejectedAmount'];
    setFileName = function () {
        $scope.fileNameNew = "Transfer Detail_" + $scope.instance.name;
    };

    $scope.TransferInvoiceReport = function () {
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        $("#grid").empty();
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        setFileName();
        stockReportService.stockTransferDetail($scope.type, data, $scope.branchid, $scope.select1).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                stockReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "Stock Transfer.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1600, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Stock_Transfer.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [

                  { field: "stockTransfer.transferDate", title: "Transfer Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=stockTransfer.transferDate == null ? '-' : kendo.toString(kendo.parseDate(stockTransfer.transferDate), 'dd/MM/yyyy') #" },
                           { field: "stockTransfer.transferNo", title: "Transfer No", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                      { field: "stockTransfer.instance.name", title: "Transfer From", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "stockTransfer.toInstance.name", title: "Transfer To", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                          { field: "stockTransfer.transferBy", title: "Transfer By", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" },footerTemplate: "Total: "  },
                                 { field: "stockTransfer.transferAmount", title: "Amount (Rs.)", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, 'n2') #</div>" },
       
                  { field: "stockTransfer.transferedStatus", title: "Transfer Status", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "stockTransfer.transferAmount", aggregate: "sum" },

                    ],
                    schema: {
                        model: {
                            fields: {
                                "stockTransfer.transferNo": { type: "string" },
                                "stockTransfer.transferBy": { type: "string" },
                                "stockTransfer.instance.name": { type: "string" },
                                "stockTransfer.transferDate": { type: "date" },
                              
                                "stockTransfer.transferedStatus": { type: "string" },

                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.orderInvoice = ['reportTransferDate', 'stockTransfer.transferNo', 'stockTransfer.instance.name', 'toProductStockId', 'stockTransfer.transferBy', 'sellingPrice', 'stockTransfer.transferedStatus'];
    $scope.TransferDetailReport = function () {
        $("#grid").empty();
        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
     
        if ($scope.branchid == undefined || $scope.branchid == "") {
            $scope.branchid = $scope.instance.id;
        }
        var currentdate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var currentdate1 = new Date(currentdate);
        var fromdate = $scope.from;
        var fromdate1 = new Date(fromdate);
        $scope.check = (currentdate1 - fromdate1) / (1000 * 60 * 60 * 24);
        setFileName();
        stockReportService.stockTransferDetail($scope.type, data, $scope.branchid, $scope.select1).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                if (angular.isObject($scope.currentInstance)) {
                    $scope.pdfHeader = $scope.currentInstance;
                    $scope.instance = $scope.currentInstance;
                }
                else {
                    $scope.pdfHeader = $scope.instance;
                }
            }
            else {
                stockReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.gsTinNo == undefined || $scope.pdfHeader.gsTinNo == "")
                    $scope.pdfHeader.gsTinNo = "";
                else
                    $scope.pdfHeader.gsTinNo = $scope.pdfHeader.gsTinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "GSTIN No:" + $scope.pdfHeader.gsTinNo;
            }


            $("#grid").kendoGrid({
                excel: {
                    fileName: "Stock Transfer.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1600, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Stock_Transfer.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                height: 350,
                sortable: true,
                filterable: {
                    mode: "column"
                },
                columns: [

                  { field: "stockTransfer.transferDate", title: "Transfer Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=stockTransfer.transferDate == null ? '-' : kendo.toString(kendo.parseDate(stockTransfer.transferDate), 'dd/MM/yyyy') #" },
                           { field: "stockTransfer.transferNo", title: "Transfer No", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                      { field: "stockTransfer.instance.name", title: "Transfer From", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "stockTransfer.toInstance.name", title: "Transfer To", width: "100px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                          { field: "stockTransfer.transferBy", title: "Transfer By", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                             
                   { field: "productStock.product.name", title: "Product Name", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                 
                   { field: "quantity", title: "Transfer Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                     { field: "acceptedUnits", title: "Accepted Qty", width: "90px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                  ////{ field: "productStock.stock", title: "Current Stock", width: "80px", format: "{0}", type: "number", attributes: { class: "text-left field-report" } },
                 { field: "batchNo", title: "Batch No", width: "80px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },
                    { field: "expireDate", title: "Expiry Date", width: "80px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#=expireDate == null ? '-' : kendo.toString(kendo.parseDate(expireDate), 'dd/MM/yyyy') #" },
                    { field: "purchasePrice", title: "Price", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                      { field: "mrp", title: "MRP", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                        { field: "total", title: "Item Value", width: "80px", format: "{0:n}", type: "number", attributes: { class: "text-left field-report" } },
                  { field: "stockTransfer.transferedStatus", title: "Transfer Status", width: "90px", format: "{0:n}", type: "string", attributes: { class: "text-left field-report" } },

                ],
                dataSource: {
                    data: response.data,
                    aggregate: [


                    ],
                    schema: {
                        model: {
                            fields: {
                                "stockTransfer.transferNo": { type: "string" },
                                "stockTransfer.transferBy": { type: "string" },
                                "stockTransfer.instance.name": { type: "string" },
                                "stockTransfer.transferDate": { type: "date" },
                                 "productStock.product.name": { type: "string" },
                                //   //"quantity1": { type: "number" },
                                  "quantity": { type: "number" },
                                //   //"productStock.stock": { type: "number" },
                                   "batchNo": { type: "string" },
                                   "expireDate": { type: "date" },
                                   "purchasePrice": { type: "number" },
                                   "mrp": { type: "number" },
                                //   "productStock.product.manufacturer": { type: "string" },
                                //   "productStock.vendor.name": { type: "string" },
                                "stockTransfer.transferedStatus": { type: "string" },

                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.orderDetail = ['reportTransferDate', 'stockTransfer.transferNo', 'stockTransfer.instance.name', 'toProductStockId', 'stockTransfer.transferBy', 'productStock.product.name', 'quantity', 'acceptedUnits', 'batchNo', 'reportExpiryDate', 'purchasePrice', 'mrp', 'total', 'stockTransfer.transferedStatus'];
    $scope.gridclear = function () {
        $("#grid").empty();
    }

    $scope.filter = function (type) {
        $scope.type = type;
        if ($scope.type === "Today") {

            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.TransferReport();
    }
    $scope.setFileName = function () {
        $scope.fileNameNew = "Transfer Detail_" + $scope.instance.name;
    };
    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: "Stock Transfer", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "GSTIN No: " + $scope.instance.gsTinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
}]);

