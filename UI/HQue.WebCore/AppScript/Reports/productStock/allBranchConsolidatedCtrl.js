﻿app.controller('allBranchConsolidatedCtrl', ['$scope', '$rootScope', 'uiGridConstants', '$http', '$interval', '$q', 'stockReportService', 'productStockModel', 'productModel', 'instanceModel', '$filter', function ($scope, $rootScope, uiGridConstants, $http, $interval, $q, stockReportService, productStockModel, productModel, instanceModel, $filter) {

    $scope.data = [];
    $scope.instanceList = [];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    //$scope.gridOptions = {
    //    showColumnFooter: true,
    //    exporterMenuCsv: true,
    //    enableGridMenu: true,
    //    enableFiltering: true,
    //    enableColumnResizing: true,
    //    //gridMenuTitleFilter: fakeI18n,
    //    paginationPageSizes: [25, 50, 75],
    //    paginationPageSize: 25,
    //    data: 'data',
    //    columnDefs: [
    //      { field: 'name', displayName: 'Branch Name', width: '10%' },
    //      { field: 'stockCount', displayName: 'No. of stocks', width: '10%', cellFilter: 'number: 2', aggregationType: uiGridConstants.aggregationTypes.sum, footerCellTemplate: '<div class="ng-binding">Total: {{col.getAggregationValue() | number:2 }}</div>' },
    //      { field: 'stockValue', displayName: 'Stocks value', width: '10%', aggregationType: uiGridConstants.aggregationTypes.sum, footerCellTemplate: '<div class="ng-binding">Total: {{col.getAggregationValue() | number:2 }}</div>' },
    //    ],

    //    gridMenuCustomItems: [
    //        {
    //            title: 'Rotate Grid',
    //            action: function ($event) {
    //                this.grid.element.toggleClass('rotated');
    //            },
    //            order: 210
    //        }
    //    ],

    //    onRegisterApi: function (gridApi) {
    //        $scope.gridApi = gridApi;

    //        gridApi.core.on.columnVisibilityChanged($scope, function (changedColumn) {
    //            $scope.columnChanged = { name: changedColumn.colDef.name, visible: changedColumn.colDef.visible };
    //        });
    //    }
    //};

    

    // chng-2

    $scope.init = function () {
        $.LoadingOverlay("show");        
        stockReportService.getInstanceData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.instance = response.data;
        }, function () {
            $.LoadingOverlay("hide");
        });
        stockReportService.getUserData().then(function (response) {
            $scope.userData = response.data;
        }, function () { });
        $scope.stockReport();
    }

    $scope.stockReport = function () {
        $.LoadingOverlay("show");
        stockReportService.allBranchConsolidatedList().then(function (response1) {
            $scope.setFileName();
            $("#grid").empty();
            $scope.data = response1.data;
            $scope.type = "";

            var pdfHeader = "";
            if ($scope.instance != undefined) {
                $scope.pdfHeader = $scope.instance;
            }
            else {
                stockReportService.getInstanceData().then(function (pdfResponse) {
                    $scope.pdfHeader = pdfResponse.data;
                }, function () { });
            }

            if ($scope.pdfHeader) {
                if ($scope.pdfHeader.drugLicenseNo == undefined || $scope.pdfHeader.drugLicenseNo == "")
                    $scope.pdfHeader.drugLicenseNo = "";
                else
                    $scope.pdfHeader.drugLicenseNo = $scope.pdfHeader.drugLicenseNo.replace("#", "");

                if ($scope.pdfHeader.tinNo == undefined || $scope.pdfHeader.tinNo == "")
                    $scope.pdfHeader.tinNo = "";
                else
                    $scope.pdfHeader.tinNo = $scope.pdfHeader.tinNo.replace("#", "");

                if ($scope.pdfHeader.fullAddress == undefined || $scope.pdfHeader.fullAddress == "")
                    $scope.pdfHeader.fullAddress = "";
                else
                    $scope.pdfHeader.fullAddress = $scope.pdfHeader.fullAddress.replace("#", "");

                pdfHeader = $scope.pdfHeader.name + "<br/>" + $scope.pdfHeader.fullAddress + "<br/>" + "DL.No:" + $scope.pdfHeader.drugLicenseNo + "<br/>" + "TIN No:" + $scope.pdfHeader.tinNo;
            } 

            $("#grid").kendoGrid({
                excel: {
                    fileName: "All Stock.xlsx",
                    allPages: true
                },
                pdf: {
                    paperSize: [1700, 1000], // Scaling in pt - 8.5"x11" page ratio
                    landscape: true,
                    allPages: true,
                    fileName: "Consolidated Stock.pdf",
                    margin: { top: "4cm", right: "1cm", bottom: "1cm", left: "1cm" },
                    landscape: true,
                    multiPage: true,
                    template: "<div class='pdfHeader' style='top:10px;position:absolute'>" + pdfHeader + "</div>" + $("#page-template").html()
                    //template: $("#page-template").html()
                },
                columnMenu: true,
                pageable: true,
                resizable: true,
                reorderable: true,
                sortable: true,
                height: 350,
                filterable: {
                    mode: "column"
                },
                columns: [
                    { field: 'name', title: 'Branch Name', width: '160px', attributes: { class: "text-left field-highlight" }  },
          { field: 'stockCount', title: 'No. of stocks', width: '160px', format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
          { field: 'stockValue', title: 'Stocks value (VAT)', width: '160px', format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
          { field: 'stockValueGst', title: 'Stocks value (GST)', width: '160px', format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },

                  //  { field: "product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" } },
                  //{ field: "batchNo", title: "Batch", width: "100px", attributes: { class: "text-left field-report" }},
                  //{ field: "expireDate", title: "Expiry", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "MM-yy" }, template: "#= kendo.toString(kendo.parseDate(expireDate, 'yyyy-MM-dd'), 'MM/yy') #" },
                  //{ field: "vendor.name", title: "Vendor", width: "200px", attributes: { class: "text-left field-report" } },
                  //{ field: "product.category", title: "Category", width: "120px", type: "string", attributes: { class: "text-left text-bold" } },
                  //{ field: "product.manufacturer", title: "Manufacturer", width: "120px", type: "string", attributes: { class: "text-left text-bold" } },
                  //{ field: "product.schedule", title: "Schedule", width: "100px", type: "string", attributes: { class: "text-left text-bold" }},
                  //{ field: "product.type", title: "Type", width: "100px", type: "string", attributes: { class: "text-left text-bold" }, footerTemplate: "Total" },
                  //{ field: "stock", title: "Stock", width: "120px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                  //{ field: "product.rackNo", title: "Rack No", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },
                  //  { field: "product.boxNo", title: "Box No", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },
                  //{ field: "vat", title: "VAT %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },
                  //    { field: "gstTotal", title: "GST %", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0:00" } },
                  //{ field: "costPrice", title: "Cost Price", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  //{ field: "mrp", title: "MRP", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0.00" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  // { field: "product.genericName", title: "Generic Name", width: "100px", type: "string", attributes: { class: "text-left text-bold" } },
                  //  { field: "product.displayStatus", title: "Status", width: "100px", type: "string", attributes: { class: "text-left field-report" } },
                ],
                dataSource: {
                    data: response1.data,
                    aggregate: [
                        { field: "stockCount", aggregate: "sum" },
                        { field: "stockValue", aggregate: "sum" },
                        { field: "stockValueGst", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "name": { type: "string" },
                                "stockCount": { type: "number" },
                                "stockValue": { type: "number" },
                                "stockValueGst": { type: "number" },
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = $filter('date')(cell.value, 'dd-MM-yyyy');
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });
 
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    $scope.order = ['name', 'stockCount', 'stockValue', 'stockValueGst'];
    $scope.setFileName = function () {
        $scope.fileNameNew = "All Branch Stock Consolidated Report";
    }
    //

    //$scope.stockReport();

    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        $scope.stitle = "All branch consolidated report";
        
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.stitle + " Stock", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "TIN No: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }
    
    //
}]);