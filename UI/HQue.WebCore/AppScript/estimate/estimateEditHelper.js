﻿app.factory('estimateEditHelper', function (estimateService, cacheService, customerHelper) {
    var scope;
    return {
        setScope: function (parentScope) {
            scope = parentScope;
            loadEditData();
        }
    }

    function loadEditData() {
        var salesEditId = cacheService.get("estimateEditId");
        if (salesEditId == null)
            return;

        cacheService.remove("estimateEditId")

        estimateService.estimateDeatils(salesEditId).then(function (response) {
            scope.sales = response.data;

            var salesItems = [];

            for (var i = 0; i < scope.sales.estimateItem.length; i++) {
                var estimateItem = scope.sales.estimateItem[i].productStock;
                estimateItem.productStockId = estimateItem.id;
                estimateItem.id = scope.sales.estimateItem[i].id;
                estimateItem.editId = i + 1;
                estimateItem.quantity = scope.sales.estimateItem[i].quantity;
                estimateItem.discount = scope.sales.estimateItem[i].discount;
                estimateItem.orginalQty = scope.sales.estimateItem[i].quantity;
                estimateItem.reminderFrequency = scope.sales.estimateItem[i].reminderFrequency.toString();
                estimateItem.reminderDate = scope.sales.estimateItem[i].reminderDate;
                salesItems.push(estimateItem);
            }

            scope.sales.salesItem = salesItems;
            scope.editId = salesItems.length + 1;

            loadCustomer(scope.sales);
        });

    }

    function loadCustomer(sales) {

        customerHelper.data.patientSearchData.mobile = sales.mobile;
        customerHelper.data.patientSearchData.name = sales.name;
        customerHelper.data.patientSearchData.empID = sales.patientId;

        if (customerHelper.data.patientSearchData.mobile != undefined && customerHelper.data.patientSearchData.mobile != "") {
            customerHelper.search(true);
        }

    }
});