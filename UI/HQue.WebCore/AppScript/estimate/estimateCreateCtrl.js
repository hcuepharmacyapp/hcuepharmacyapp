﻿app.controller('estimateCreateCtrl', function ($scope, $rootScope, $location, $window, toastr, estimateEditHelper, customerHelper, printingHelper, shortcutHelper, salesService, productStockService, doctorService, cacheService, productModel, ModalService, $filter, customerReceiptService, patientService, missedOrderModel, estimateService) {

    shortcutHelper.setScope($scope);
    $scope.minDate = new Date();
    var d = new Date();

    $scope.open = function () {
        $scope.popup.opened = true;
    };

    $scope.popup = {
        opened: false
    };


    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.remiderOpen = function () {
        $scope.reminderPopup.opened = true;
    };

    $scope.reminderPopup = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.customerHelper = customerHelper;

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.batchList = [];
    $scope.selectedProduct = null;
    $scope.selectedBatch = { reminderFrequency: "0" };
    $scope.Math = window.Math;
    $scope.selectedDoctor = null;
    $scope.totalBalanceAmount = 0;
    $scope.doctor = {};
    $scope.customerHelper.data.isCustomerDiscount = 0;
    $scope.customerHelper.data.isReset = 0;


    $scope.sales = getSalesObject();

    $rootScope.$on("doSelectPatient", function (data) {
        customerHelper.doSelectPatient();
    });

    $scope.doctorSelected = function () {
        $scope.selectedDoctor;
    }

    $scope.$on("doctorSelected", function (event, args) {
        $scope.sales.doctorMobile = args.value.mobile;
    });

    $scope.$on("update_getValue", function (event, value) {
        $scope.doctor.name = value;
    });

    $scope.getProducts = function (val) {
        return productStockService.drugFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    $scope.CheckName = function () {
        var drugName = document.getElementById("drugName");
        console.log(drugName.value);
        if (drugName.value == "") {
            $scope.selectedBatch = { reminderFrequency: "0", purchasePrice: 0 };
        }
    };

    $scope.Namecookie = "";
    $scope.getPatient = function (val) {
        $scope.customerHelper.data.patientSearchData.name = val;
        $scope.Namecookie = val;


        return patientService.Patientlist(val,1).then(function (response) {

            var origArr = response.data;
            var newArr = [],
       origLen = origArr.length,
       found, x, y;

            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if (origArr[x].name === newArr[y].name && origArr[x].mobile === newArr[y].mobile) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }

            return newArr.map(function (item) {
                return item;
            });
        });
    };

    $scope.getCustomerBalance = function () {
        customerReceiptService.getCustomerBalance(customerHelper.data.selectedCustomer.mobile, customerHelper.data.selectedCustomer.name).then(function (resp) {
            if (resp.data.length == 0) {
                customerHelper.data.customerBalance = 0;
                return;
            }

            if (resp.data[0].credit != undefined)
                customerHelper.data.customerBalance = resp.data[0].credit;
        });
    }

    $scope.onPatientSelect = function (obj, event) {


        var TABKEY = 9;

        if (event.which == TABKEY) {
            console.log("tab pressed");
            event.preventDefault();
            customerHelper.data.patientSearchData.name = $scope.Namecookie;
            var qty = document.getElementById("searchPatientMobile");
            qty.focus();
            return false;
        } else {
            customerHelper.data.patientSearchData = obj;
            patientService.list(customerHelper.data.patientSearchData,1).then(function (response) {
                customerHelper.data.customerList = response.data;
                if (customerHelper.data.customerList.length > 0) {
                    for (p in customerHelper.data.customerList) {
                        customerHelper.data.customerList[p].nameIndex = customerHelper.data.customerList[p].name.substring(0, 1);
                    }

                    customerHelper.data.selectedCustomer = customerHelper.data.customerList[0];
                    customerHelper.data.isCustomerSelected = true;
                    var drugName = document.getElementById("drugName");
                    drugName.focus();
                    customerReceiptService.getCustomerBalance(customerHelper.data.selectedCustomer.mobile, customerHelper.data.selectedCustomer.name).then(function (resp) {
                        if (resp.data.length == 0)
                            return;
                        if (resp.data[0].credit != undefined)
                            customerHelper.data.customerBalance = resp.data[0].credit;
                    });

                    if (customerHelper.data.selectedCustomer != null && customerHelper.data.selectedCustomer.name != undefined) {
                        patientService.getCustomerDiscount(customerHelper.data.selectedCustomer).then(function (response) {
                            customerHelper.data.selectedCustomer.discount = response.data;
                        });
                    }
                }
                //else {

                //    popupAddNew();
                //}

                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }




    };
    var drugName = document.getElementById("drugName");
    drugName.focus;
    $scope.onProductSelect = function () {

        salesService.getBatchListDetail().then(function (response) {
            $scope.batchListType = response.data;
            if ($scope.batchListType == "Batch") {
                $scope.ProductDetails();
            }
            else {
                loadBatch(null);
            }
        }, function () {

        });

    };
    $scope.getAvailableStock = function (productStock, editBatch) {
        var addedItem = getAddedItem(productStock.productStockId);
        var newAddedQty = getAddedItemQuantity(addedItem);

        if (editBatch == null)
            return productStock.stock - newAddedQty;
        else if (editBatch.id != null && productStock.productStockId == editBatch.productStockId)
            return productStock.stock + parseInt(editBatch.quantity) - newAddedQty;
        else if (editBatch != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock - (newAddedQty - parseInt(editBatch.quantity));
        }
        else if (editBatch != null && productStock.productStockId != editBatch.productStockId) {
            return productStock.stock - newAddedQty;
        }
        return productStock.stock;
    };

    function getAddedItemQuantity(addedItems) {
        var qty = 0;
        for (var i = 0; i < addedItems.length; i++) {
            if (addedItems[i].id != null) {
                qty += parseInt(addedItems[i].quantity) - parseInt(addedItems[i].orginalQty)
            }
            else {
                qty += parseInt(addedItems[i].quantity)
            }
        }
        return qty;
    }

    function getAddedItem(id) {
        var addedItems = [];
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ($scope.sales.salesItem[i].productStockId == id)
                addedItems.push($scope.sales.salesItem[i]);
        }
        return addedItems;
    };
    function getAddedItems(id, sellingPrice, discount) {

        for (var i = 0; i < $scope.sales.salesItem.length; i++) {

            if ($scope.sales.salesItem[i].productStockId == id && $scope.sales.salesItem[i].sellingPrice == sellingPrice && $scope.sales.salesItem[i].discount == discount) {
                return $scope.sales.salesItem[i];
            }

        }
        return null;
    };
    $scope.editId = 1;

    $scope.validateDiscount = function (discount) {
        //console.log($scope.selectedBatch);
        if (parseFloat(discount) > 100) {
            $scope.salesItems.$valid = true;
        }
        else {
            $scope.salesItems.$valid = false;
        }
    }

    $scope.Quantityexceeds = 0;
    $scope.validateQty = function () {
        if ($scope.selectedBatch == null)
            return;

        if ($scope.selectedBatch.quantity > $scope.selectedBatch.totalQuantity && $scope.selectedBatch.editId == null) {
            $scope.Quantityexceeds = 1;
        }

        if ($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty && $scope.selectedBatch.editId != null) {
            $scope.Quantityexceeds = 1;
        }

        var qty = document.getElementById("quantity");
        if (qty.value == "") {
            $scope.salesItems.$valid = false;
            $scope.Quantityexceeds = 0;
        }
        else {
            if ((qty.value > $scope.selectedBatch.totalQuantity && $scope.selectedBatch.editId == null) || (qty.value > $scope.selectedBatch.availableQty && $scope.selectedBatch.editId != null)) {
                $scope.IsFormInvalid = false;
                $scope.salesItems.$valid = true;


                if ($scope.Quantityexceeds == 1) {
                    $scope.IsFormInvalid = false;
                    $scope.salesItems.$valid = true;
                } else {
                    $scope.salesItems.$valid = false;
                    $scope.IsFormInvalid = true;
                }
            }
            else {
                console.log($scope.selectedBatch.sellingPrice);

                if (typeof ($scope.selectedBatch.discount) == "string") {
                    //if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || $scope.selectedBatch.discount == "" || $scope.selectedBatch.discount == undefined || parseFloat($scope.selectedBatch.discount) > 100) {
                    if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || parseFloat($scope.selectedBatch.discount) > 100) {
                        $scope.salesItems.$valid = false;
                        //var sell = document.getElementById("sellingPrice");
                        //sell.focus();
                        $scope.IsFormInvalid = true;
                    } else {
                        $scope.IsFormInvalid = false;
                        $scope.salesItems.$valid = true;
                    }
                }
                else {
                    if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || parseFloat($scope.selectedBatch.discount) > 100) {
                        $scope.salesItems.$valid = false;
                        //var sell = document.getElementById("sellingPrice");
                        //sell.focus();
                        $scope.IsFormInvalid = true;
                    } else {
                        $scope.IsFormInvalid = false;
                        $scope.salesItems.$valid = true;
                    }
                }



                $scope.Quantityexceeds = 0;

            }
        }
    }


    $scope.hasCustomerData = function () {
        var hasCustomer = false;
        if ($scope.customerHelper.data.selectedCustomer.name == null || $scope.customerHelper.data.selectedCustomer.mobile == null || $scope.customerHelper.data.selectedCustomer.id == null) {
            hasCustomer = true;
        }
        else {
            hasCustomer = false;
        }
        return hasCustomer;
    };

    $scope.MissedOrder = function () {
        var m = ModalService.showModal({
            controller: "missedOrderCreateCtrl",
            templateUrl: 'missedOrders'
            , inputs: {
                patientId: $scope.customerHelper.data.selectedCustomer.id,
                patientPhone: $scope.customerHelper.data.selectedCustomer.mobile
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
        return false;
    };


    $scope.ProductDetails = function () {
        $scope.enablePopup = true;
        cacheService.set('selectedProduct1', $scope.selectedProduct)
        var m = ModalService.showModal({
            controller: "productDetailSearchCtrl",
            templateUrl: 'productDetails'
            , inputs: {
                productId: $scope.selectedProduct.product.id,
                productName: $scope.selectedProduct.name,
                items: $scope.sales.salesItem,
                enableWindow: $scope.enablePopup
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
        return false;
    };

    $rootScope.$on("productDetail", function (data) {
        $scope.productSelection();
    });

    $rootScope.$on("productDetailCancel", function (data) {
        var prod = document.getElementById("drugName");
        prod.focus();
    });

    $scope.productSelection = function () {
        var product1 = cacheService.get('selectedProduct1');
        var blist = cacheService.get('batchList1');
        $scope.enablePopup = cacheService.get('enableWindow1');
        if (product1 != null) {
            $scope.selectedBatch = product1;
        }
        if (blist.length > 0) {
            $scope.batchList = blist;
        }
        dayDiff($scope.selectedBatch.expireDate);
        $scope.selectedBatch.totalQuantity = product1.availableQty;
        var qty = document.getElementById("quantity");
        qty.focus();
    }


    $scope.PopupAlternates = function () {

        $scope.IsFormInvalid = false;
        $scope.salesItems.$valid = true;
        cacheService.set('selectedAlternate1', $scope.selectedProduct)
        var m = ModalService.showModal({
            controller: "alternateProductSearchCtrl",
            templateUrl: 'searchProduct'
            , inputs: {
                selectedProduct: ($scope.selectedProduct != null) ? $scope.selectedProduct : null,
                selectedProductId: ($scope.selectedProduct != null && $scope.selectedProduct.product != null) ? $scope.selectedProduct.product.id : null
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
        return false;
    };
    $scope.showTempStockScreen = function () {

        cacheService.set('tempStockSection', $scope.selectedProduct)
        var m = ModalService.showModal({
            controller: "tempStockCtrl",
            templateUrl: 'tempStock'
            //,inputs: {
            //    reminderType: 1
            //}
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
        return false;
    };

    $rootScope.$on("alternateSelection", function (data) {
        $scope.alternateSelection();
    });

    $rootScope.$on("tempStockSelection", function (data) {
        $scope.tempStockSelection();
    }
    );
    $scope.tempStockSelection = function () {
        console.log("inside tempStockSelection");
        var tempStockValues = cacheService.get('tempStockValues');
        if (tempStockValues != null) {
            //console.log(tempStockValues);
            $scope.selectedProduct = tempStockValues.productStock.product.name;
            var editBatch = null;
            productStockService.productBatch(tempStockValues.productStock.productId).then(function (response) {
                $scope.batchList = response.data;
                console.log(tempStockValues.productStock.productId);
                console.log(response.data);
                var totalQuantity = 0;
                var rack = "";
                var tempBatch = [];

                for (var x = 0; x < $scope.sales.salesItem.length; x++) {
                    for (var y = 0; y < $scope.batchList.length; y++) {
                        if ($scope.sales.salesItem[x].productStockId == $scope.batchList[y].id) {
                            $scope.batchList[y].stock = $scope.batchList[y].stock - parseInt($scope.sales.salesItem[x].quantity);
                        }
                    }
                }

                for (var i = 0; i < $scope.batchList.length; i++) {
                    $scope.batchList[i].productStockId = $scope.batchList[i].id;
                    $scope.batchList[i].id = null;
                    var availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                    totalQuantity += availableStock;

                    if (availableStock == 0)
                        continue;

                    $scope.batchList[i].availableQty = availableStock;
                    if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                        rack = $scope.batchList[i].rackNo;
                    }

                    tempBatch.push($scope.batchList[i]);
                }

                if (editBatch != null && tempBatch.length == 0) {
                    tempBatch.push(editBatch);
                }

                $scope.batchList = tempBatch;

                if ($scope.batchList.length > 0) {

                    if (editBatch == null) {
                        $scope.selectedBatch = $scope.batchList[0];
                        $scope.selectedBatch.reminderFrequency = "0";
                    }
                    else {
                        $scope.selectedBatch = editBatch;
                        $scope.selectedBatch.availableQty = availableStock;
                    }

                    $scope.selectedBatch.totalQuantity = totalQuantity;
                    $scope.selectedBatch.rackNo = rack;
                    $scope.selectedBatch.discount = 0.0;
                    dayDiff($scope.selectedBatch.expireDate);
                }
                //$scope.selectedBatch.batchNo = tempStockValues.productStock.batchNo;
                //$scope.selectedBatch.expireDate = tempStockValues.productStock.expireDate;
                //$scope.selectedBatch.vat = tempStockValues.productStock.vAT;
            }, function () { });
        }
        var qty = document.getElementById("quantity");
        qty.disabled = false;
        qty.focus();
        $scope.selectedBatch = tempStockValues.productStock.batchNo;
    }
    $scope.alternateSelection = function () {
        var alter1 = cacheService.get('selectedAlternate1');
        if (alter1 != null) {
            $scope.batchList = alter1;
            $scope.selectedProduct = alter1[0];
            $scope.selectedProduct.product.id = alter1[0].productId;
            $scope.selectedProduct.product.name = alter1[0].name;
            loadBatch(null);
        }
        var qty = document.getElementById("quantity");
        qty.focus();
    }

    $scope.addSales = function () {

        if ($scope.Quantityexceeds == 1) return;

        if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == null || $scope.selectedBatch.purchasePrice > $scope.selectedBatch.sellingPrice) {
            return;
        }
        if (typeof ($scope.selectedBatch.discount) == "string") {
            if (parseFloat($scope.selectedBatch.discount) > 100 || $scope.selectedBatch.discount == '.') {
                return;
            }
            else
                if ($scope.selectedBatch.discount == undefined || $scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "")
                    $scope.selectedBatch.discount = 0;
        }
        else {
            if (parseFloat($scope.selectedBatch.discount) > 100) {
                return;
            }
            else
                if ($scope.selectedBatch.discount == undefined || $scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "")
                    $scope.selectedBatch.discount = 0;
        }


        if ($scope.selectedBatch.quantity <= 0 || $scope.selectedBatch.quantity == undefined)
            return;

        if ($scope.batchList.length == 0)
            return;

        var ChangedDicount = angular.element("#spnquantity").attr("data-discount");

        if ($scope.selectedBatch.editId == null) {
            $scope.selectedBatch.editId = $scope.editId++;
            var addedItem = getAddedItems($scope.selectedBatch.productStockId, $scope.selectedBatch.sellingPrice, ChangedDicount);
            if (addedItem != null) {
                addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.selectedBatch.quantity);
            } else {
                $scope.sales.salesItem.push($scope.selectedBatch);
            }
        } else {
            for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                if ($scope.sales.salesItem[i].editId == $scope.selectedBatch.editId) {
                    $scope.sales.salesItem[i] = $scope.selectedBatch;
                    break;
                }
            }
        }

        $scope.salesItems.$setPristine();
        $scope.selectedProduct = null;
        $scope.selectedBatch = { reminderFrequency: "0", purchasePrice: 0 };
        $scope.highlight = "";
        getDiscountRules();
        resetFocus();
        ChangedDicount = 0;
        ChangedMrp = 0;

        var disclength = 0;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            if ($scope.sales.salesItem[k].discount == 0) {
                disclength++;
            }
        }
        if (disclength == $scope.sales.salesItem.length) {
            $scope.discountInValid = false;
        }

    }

    $scope.removeSales = function (item) {
        if (!confirm("Are you sure, Do you want to delete ? "))
            return;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            $scope.sales.total += ($scope.sales.salesItem[k].sellingPrice * $scope.sales.salesItem[k].quantity);
        }

        $scope.sales.total -= (item.sellingPrice * item.quantity);
        var index = $scope.sales.salesItem.indexOf(item);

        $scope.sales.salesItem.splice(index, 1);

        $scope.discountInValid = false;
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if (($scope.sales.salesItem[i].discount > 0) && ($scope.sales.salesItem[i].discount <= 100)) {
                $scope.discountInValid = true;
            }
        }
        getDiscountRules();
    }
    $scope.manualSeries = "";
    $scope.editSales = function (item) {
        var drugName = document.getElementById('drugName');
        drugName.disabled = "disabled";
        var newObject = jQuery.extend(true, {}, item);
        $scope.selectedProduct = newObject;
        loadBatch(newObject);
    }
    $scope.selectedBillDate = "";
    $scope.save = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        $scope.discountInValid = false;

        //Add patient information
        if (customerHelper.data.isCustomerSelected) {
            $scope.sales.patientId = customerHelper.data.selectedCustomer.id;
            $scope.sales.name = customerHelper.data.selectedCustomer.name;
            $scope.sales.mobile = customerHelper.data.selectedCustomer.mobile;
            $scope.sales.email = customerHelper.data.selectedCustomer.email;
            $scope.sales.dOB = customerHelper.data.selectedCustomer.dOB;
            $scope.sales.age = customerHelper.data.selectedCustomer.age;
            $scope.sales.gender = customerHelper.data.selectedCustomer.gender;
            $scope.sales.address = customerHelper.data.selectedCustomer.address;
            $scope.sales.isValidEmail = customerHelper.data.selectedCustomer.isValidEmail;
        }
        else {
            $scope.sales.name = customerHelper.data.patientSearchData.name;
            $scope.sales.mobile = customerHelper.data.patientSearchData.mobile;
            $scope.sales.patientId = customerHelper.data.patientSearchData.empID;
        }

        if ($scope.selectedDoctor == null) {
            $scope.$root.$broadcast('getValue', 'doctorName');
            $scope.sales.doctorName = $scope.doctor.name;
        }
        else {
            $scope.sales.doctorName = $scope.selectedDoctor.originalObject.name;
        }
        $scope.sales.estimateItem = $scope.sales.salesItem;
        estimateService.create($scope.sales).then(function (response) {
            var sales = response.data;

            toastr.success('Estimate created successfully');

            if ($scope.sales.billPrint) {
                printingHelper.printEstimate(sales.id);
            }

            $scope.salesItems.$setPristine();
            $scope.sales = getSalesObject();
            $scope.selectedProduct = null;
            $scope.selectedBatch = {};
            customerHelper.ResetCustomer();
            var salesEditId = document.getElementById('salesEditId').value;
            if (salesEditId != "") {
                window.location = window.location.origin + "/Sales/List";
            }
            $location.path('#/pos');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");

        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    }

    $scope.ValidateManualSeries = function (series) {

        console.log($scope.manualSeries);
        salesService.IsInvoiceManualSeriesAvail($scope.manualSeries)
        .then(function (resp) {
            console.log(JSON.stringify(resp));
            bReturn = resp.data;
            if (bReturn) {
                toastr.error("" + $scope.manualSeries + " is already Exists");
                var Manual = document.getElementById("ManualSeries");
                Manual.focus();
                $scope.IsSelectedInvoiceSeries = true;
            } else {
                $scope.IsSelectedInvoiceSeries = false;
            }

        }, function (response) {
            $scope.responses = response;

        });

    }


    $scope.ValidateCustomSeries = function (series) {
        console.log(series);
        console.log($scope.selectedSeriesItem);


        if ($scope.selectedSeriesItem == "" || $scope.selectedSeriesItem == undefined) {
            $scope.IsSelectedInvoiceSeries = true;
        } else {
            $scope.IsSelectedInvoiceSeries = false;
        }
    }
    function GetIndexofItem(array, productstockid) {
        var index = -1;
        for (var i = 0; i < array.length; i++) {
            if (array[i].productStockId == productstockid) {
                index = i;
            }
        }
        return index;
    }
    $scope.batchSelected = function (batch) {
        $scope.valPurchasePrice = "";
        var totalQuantity = 0;
        $scope.selectedBatch = $scope.batchList[batch];
        for (var i = 0; i < $scope.batchList.length; i++) {
            totalQuantity += parseInt($scope.batchList[i].stock);
        }
        $scope.selectedBatch.totalQuantity = totalQuantity;
        $scope.selectedBatch.discount = 0;
        dayDiff($scope.selectedBatch.expireDate);
    }

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    //File Upload
    $scope.SelectedFileForUpload = null;

    //File Select event 
    $scope.selectFileforUpload = function (file) {
        $scope.SelectedFileForUpload = file[0];
    }

    //Amount Calculation
    $scope.receivedAmount1 = function (receivedAmount2) {
        if (parseFloat(receivedAmount2) > 0)
            $scope.balanceAmount = parseFloat(receivedAmount2) - Math.round($scope.netTotal());
        else
            $scope.balanceAmount = "";
    };

    $scope.cancel = function () {
        if (window.confirm('Are you sure, Do you want to cancel?')) {
            window.location = window.location.origin + window.location.pathname;
        }
        $scope.discountInValid = false;
    }

    $scope.loadSalesDiscount = function () {
        if (window.localStorage.getItem("s_discount") == null)
            return;
        $scope.salesDiscount = JSON.parse(window.localStorage.getItem("s_discount"));
    };
    $scope.isFormValid = true;

    var discountFlag = 0;
    $scope.selectedBatch.purchasePrice = 0;
    $scope.collectCardInfo = function (cardSelected) {
        $scope.sales.isCardSelected = cardSelected;
        if (!$scope.sales.isCardSelected) {
            $scope.sales.cardNo = null;
            $scope.sales.cardDate = null;
            $scope.sales.cardDigits = null;
            $scope.sales.cardName = null;
        }
    }

    $scope.validateSellingPrice = function (selectedBatch) {
        console.log(selectedBatch.purchasePrice + "_" + selectedBatch.sellingPrice)

        if (selectedBatch.quantity != undefined || selectedBatch.quantity != 0) {
            if (selectedBatch.purchasePrice == "." || selectedBatch.purchasePrice == 0 || selectedBatch.purchasePrice > selectedBatch.sellingPrice || selectedBatch.sellingPrice == undefined) {

                $scope.valPurchasePrice = "Price must be greater than Purchase Price";
                $scope.salesItems.$valid = false;
            }
            else {
                $scope.valPurchasePrice = "";
                $scope.salesItems.$valid = true;
            }
        }

    }

    $scope.discountInValid = false;

    $scope.changediscountsale = function (discount) {

        //if (discount == "" || discount == undefined || parseFloat(discount) > 100) {
        if (parseFloat(discount) > 100 || discount == ".") {
            $scope.salesItems.$valid = false;
        }
        else {
            //$scope.salesItems.$valid = true;
            $scope.validateQty();
        }

        if ((discount > 0) && (discount <= 100)) {
            $scope.discountInValid = true;
        }
        else if ((discount == 0) && ($scope.sales.salesItem.length == 0)) {
            //console.log("inside discount 0 and salesLength 0");
            $scope.discountInValid = false;
        }
        else if ((discount == 0) && ($scope.sales.salesItem.length > 0)) {
            for (var j = 0; j < $scope.sales.salesItem.length; j++) {
                if ($scope.sales.salesItem[j].discount > 0) {
                    $scope.discountInValid = true;
                }
            }
        }
    }


    //$scope.keydown = shortcutHelper.salesShortcuts;


    $scope.keydown = function (e) {
        // F1
        if (e.keyCode == 112) {
            event.preventDefault();
            document.getElementById("customerName").focus();
        }
        //F2
        if (e.keyCode == 113) {
            event.preventDefault();
            document.getElementById("doctorName_value").focus();
        }
        //F3
        if (e.keyCode == 114) {
            event.preventDefault();
            document.getElementById("txtDiscount").focus();
        }
        //F4
        if (e.keyCode == 115) {
            event.preventDefault();
            document.getElementById("receivedAmount").focus();
        }
        //F5
        if (e.keyCode == 116) {
            event.preventDefault();
            $scope.sales.cashType = "Credit";
            $scope.focusCreditTextBox = true;
        }
        //F6
        if (e.keyCode == 117) {
            event.preventDefault();
            $scope.sales.paymentType = "Card";
            $scope.focusCardTextbox = true;
            $scope.editCheque($scope.sales, true);
        }
        //F7
        if (e.keyCode == 118) {
            event.preventDefault();
            $scope.sales.deliveryType = "Home Delivery";
            $scope.saleType(true);
        }
        //F8
        if (e.keyCode == 119) {
            event.preventDefault();
            document.getElementById("discount").focus();
        }
        //F9
        if (e.keyCode == 120) {
            event.preventDefault();
            document.getElementById("sellingPrice").focus();
        }
        //F10
        if (e.keyCode == 121) {
            event.preventDefault();
            document.getElementById("reminder").focus();
        }
        //F11
        if (e.keyCode == 122) {
            event.preventDefault();
            document.getElementById("saleType").focus();
        }
        //Escape
        if (e.keyCode == 27) {
            event.preventDefault();

            var valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing && $scope.sales.discount <= 100)
            if (!valid)
                return false;

            if ($scope.sales.cashType == 'Credit' || $scope.sales.deliveryType == 'Home Delivery')
                return customerHelper.data.isCustomerSelected;

            if ($scope.enablePopup == true) {
                $scope.enablePopup = false;
                return false;
            }

            $scope.save();
        }

        //ctrl+r
        if (e.keyCode == 82 && e.ctrlKey == true) {
            event.preventDefault();
        }
        //ctrl+a
        if (e.keyCode == 65 && e.ctrlKey == true) {
            event.preventDefault();
            document.getElementById("drugName").focus();
        }
    }

    $scope.changeDiscount = function (discount) {
        if (discount == "") {
            discount = 0;
        }
        $scope.sales.discount = parseInt(discount);
        $scope.salesItems.$valid = true;
    };

    $scope.isSaveValid = function () {

        if ($scope.selectedBatch.name != null && $scope.selectedBatch.quantity != null) {
            $scope.salesItems.$valid = true;
        }
        var valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing && $scope.sales.discount <= 100)
        if (!valid)
            return false;

        if ($scope.sales.cashType == 'Credit' || $scope.sales.deliveryType == 'Home Delivery')
            return customerHelper.data.isCustomerSelected;

        if ($scope.customerHelper.data.isCustomerDiscount == 0)
            if ($scope.sales.salesItem.length > 0 && $scope.customerHelper.data.selectedCustomer.name != undefined) {
                getDiscountRules();
                $scope.setTotal1();
                $scope.vat1();
                $scope.salesdiscount1();
                $scope.netTotal();
                $scope.customerHelper.data.isCustomerDiscount = 1;
            }

        if ($scope.customerHelper.data.isReset == 1) {
            getDiscountRules();
            $scope.setTotal1();
            $scope.vat1();
            $scope.salesdiscount1();
            $scope.netTotal();
            $scope.customerHelper.data.isReset = 0;
        }


        return true;
    }

    estimateEditHelper.setScope($scope);

    $scope.sales.salesItemDiscount = 0;

    $scope.init = function () {
        salesService.getSalesType().then(function (response) {
            $scope.sales.saleType = response.data;
            if ($scope.sales.saleType.salesTypeList.length > 0) {
                $scope.sales.salesType = $scope.sales.saleType.salesTypeList[0].id;
            }
        }, function () {

        });
    }

    function getDiscountRules() {
        $scope.sales.salesItemDiscount = 0;
        if ($scope.sales.discount > 0)
            $scope.sales.discount = $scope.sales.discount;
        else
            $scope.sales.discount = 0;
        discountFlag = 0;

        if (($scope.sales.salesItem != null) && ($scope.sales.salesItem.length > 0)) {
            var total1 = 0;
            for (var x = 0; x < $scope.sales.salesItem.length; x++) {
                total1 = total1 + ((parseFloat($scope.sales.salesItem[x].discount) / 100) * ($scope.sales.salesItem[x].sellingPrice * parseFloat($scope.sales.salesItem[x].quantity)));
            }
            $scope.sales.salesItemDiscount = total1;
        }

        var overalldisc = true;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            if (($scope.sales.salesItem[k].discount > 0) && ($scope.sales.salesItem[k].discount <= 100)) {
                overalldisc = false;
            }
        }

        $scope.sales.total = $scope.setTotal();

        if ($scope.customerHelper.data.patientSearchData.discount != null) {
            $scope.customerHelper.data.selectedCustomer.discount = $scope.customerHelper.data.patientSearchData.discount;
        }

        else if ($scope.customerHelper.data.selectedCustomer != null && $scope.customerHelper.data.selectedCustomer.name != undefined) {
            patientService.getCustomerDiscount($scope.customerHelper.data.selectedCustomer).then(function (response) {
                $scope.customerHelper.data.selectedCustomer.discount = response.data;
            });
        }

        if (overalldisc) {
            salesService.getDiscountDetail().then(function (response) {

                $scope.discountRules = response.data;
                if ($scope.discountRules.billAmountType != "slabDiscount" && $scope.discountRules.billAmountType != "customerWiseDiscount") {
                    if (!$scope.discountRules.amount != null) {
                        if ($scope.discountRules.amount <= $scope.sales.total) {
                            $scope.sales.discount = $scope.discountRules.discount;
                            discountFlag = 1;
                        }
                        else {
                            if (discountFlag == 1) {
                                discountFlag = 0;
                            }
                        }
                    }
                }
                else if ($scope.discountRules.billAmountType == "slabDiscount") {
                    $scope.sales.discount = 0;
                    for (var z = 0; z < $scope.discountRules.discountItem.length; z++) {
                        if ((!$scope.discountRules.discountItem[z].amount != null) && (!$scope.discountRules.discountItem[z].toAmount != null)) {
                            if (($scope.discountRules.discountItem[z].amount <= $scope.sales.total) && ($scope.discountRules.discountItem[z].toAmount >= $scope.sales.total)) {
                                $scope.sales.discount = $scope.discountRules.discountItem[z].discount;
                                discountFlag = 1;
                            }
                            else if ($scope.discountRules.discountItem[z].toAmount < $scope.sales.total) {
                                $scope.sales.discount = $scope.discountRules.discountItem[z].discount;
                                discountFlag = 1;
                            }
                        }
                    }
                }
                else {
                    $scope.sales.discount = 0;
                    if ($scope.customerHelper.data.selectedCustomer.name != undefined) {
                        $scope.sales.discount = $scope.customerHelper.data.selectedCustomer.discount;
                        discountFlag = 1;
                    }
                }

            });
        }
        else {
            $scope.sales.discount = 0;
        }
    }

    function dayDiff(expireDate) {
        $scope.highlight = "";
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        var expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');

        var date2 = new Date(formatString(expireDate));
        var date1 = new Date(formatString(today));
        //var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));

        if ($scope.dayDifference < 30) {
            var dt = expireDate;
            $scope.highlight = "Expiry";
        }
        else {
            $scope.highlight = "";
        }
    }

    function formatString(format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    }

    function resetFocus() {
        var qty = document.getElementById("drugName");
        qty.focus();
    }

    $scope.sales.discountTotal = 0;
    $scope.setTotal = function () {
        $scope.sales.total = 0;
        $scope.sales.discountTotal = 0;
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if (parseFloat($scope.sales.salesItem[i].discount) > 0) {
                $scope.sales.discountTotal += $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
            }
            $scope.sales.total += $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity;
        }
        return $scope.sales.total;
    }

    $scope.tot = 0;
    $scope.setTotal1 = function () {
        var tmp = 0;
        var sum2 = 0;
        var tmp4 = 0;
        $scope.sales.discountTotal = 0;
        var disc = 0;

        if ($scope.sales.discount == 100)
            return 0;

        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100)) {
                $scope.sales.discountTotal += $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
                disc = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
                sum2 += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - disc);
            }
        }

        $scope.tot = sum2;

        tmp4 = $scope.vat() - (($scope.sales.discount / 100) * $scope.vat()) - $scope.sales.discountVat;
        if (tmp4 < 0)
            tmp4 = 0;

        tmp = (sum2 - (($scope.sales.discount / 100) * sum2) - tmp4);
        //tmp = ((sum2 - (($scope.sales.discount / 100) * sum2) - $scope.sales.discountTotal) - tmp4);
        //tmp = (($scope.setTotal() - (($scope.sales.discount / 100) * $scope.setTotal()) - $scope.sales.discountTotal) - ($scope.vat() - (($scope.sales.discount / 100) * $scope.vat()) - $scope.sales.discountVat));
        if (tmp > 0) {
            return tmp;
        }
        else {
            return 0;
        }
    }

    $scope.vat1 = function () {
        var tmp1 = 0;
        if ($scope.setTotal1() > 0) {
            tmp1 = $scope.vat() - (($scope.sales.discount / 100) * $scope.vat()) - $scope.sales.discountVat;
            if (tmp1 > 0)
                return tmp1;
            else
                return 0;
        }
        else {
            return 0;
        }
    }

    $scope.salesdiscount1 = function () {
        var tmp2 = 0;
        var sum1 = 0;
        var temp1 = 0;
        var disc = 0;

        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            sum1 += $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity;
        }

        if ($scope.sales.discount == 100) {
            return sum1;
        }

        for (var x = 0; x < $scope.sales.salesItem.length; x++) {
            if (parseFloat($scope.sales.salesItem[x].discount) < 100) {
                disc = $scope.sales.salesItem[x].sellingPrice * $scope.sales.salesItem[x].quantity * parseFloat($scope.sales.salesItem[x].discount) / 100;
                temp1 += (($scope.sales.salesItem[x].sellingPrice * $scope.sales.salesItem[x].quantity) - disc);
            }
        }

        tmp2 = ((($scope.sales.discount / 100) * temp1) + $scope.sales.salesItemDiscount);

        if (tmp2 > sum1) {
            tmp2 = sum1;
            return tmp2;
        }
        else
            return tmp2;
    }

    $scope.netTotal = function () {
        var net1 = 0;

        if ($scope.sales.discount == 100) {
            return 0;
        }
        net1 = ($scope.tot - (($scope.sales.discount / 100) * $scope.tot));
        if (net1 >= 0) {
            return net1;
        }
        else {
            return 0;
        }
    }

    $scope.sales.discountVat = 0;

    $scope.vat = function () {
        $scope.sales.vat = 0;
        $scope.sales.discountVat = 0;
        var discount1 = 0;
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100)) {
                discount1 = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
            }
            else if (parseFloat($scope.sales.salesItem[i].discount) >= 100) {
                discount1 = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * 100 / 100;
            }
            else {
                discount1 = 0;
            }
            $scope.sales.discountVat += (discount1) - ((discount1 / (100 + $scope.sales.salesItem[i].vat)) * 100);
            //$scope.sales.vat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) * 100) / ($scope.sales.salesItem[i].vat + 100)
            $scope.sales.vat += ($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) / (100 + $scope.sales.salesItem[i].vat)) * 100)
        }
        return $scope.sales.vat;
    }

    $scope.keyEnter = function (event, e) {
        var ele = document.getElementById(e);
        if (event.which === 13) // Enter key
        {
            ele.focus();
            if (ele.nodeName != "BUTTON")
                ele.select();
        }
        if (event.which === 9) {
            customerHelper.data.patientSearchData.name = $scope.Namecookie;
        }
    }

    function getSalesObject() {
        return {
            salesItem: [],
            discount: 0,
            total: 0,
            TotalQuantity: 0,
            rackNo: "",
            vat: 0,
            cashType: "Full",
            paymentType: "Cash",
            deliveryType: "Counter",
            billPrint: true,
            sendEmail: false,
            sendSms: false,
            doctorMobile: null,
            credit: null,
            cardNo: null,
            cardDate: null,
            cardDigits: null,
            cardName: null,
            isCardSelected: false,
            isCreditEdit: false,
            changeDiscount: "",
        }
    }

    function loadBatch(editBatch) {

        productStockService.productBatch($scope.selectedProduct.product.id).then(function (response) {
            $scope.batchList = response.data;
            var qty = document.getElementById("quantity");
            qty.focus();
            var totalQuantity = 0;
            var rack = "";
            var tempBatch = [];
            $scope.changediscountsale("");
            for (var i = 0; i < $scope.batchList.length; i++) {
                $scope.batchList[i].productStockId = $scope.batchList[i].id;
                $scope.batchList[i].id = null;
                var availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                if (editBatch != null && editBatch.productStockId == $scope.batchList[i].productStockId)
                    editBatch.availableQty = availableStock;
                totalQuantity += availableStock;

                if (availableStock == 0)
                    continue;

                $scope.batchList[i].availableQty = availableStock;
                if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                    rack = $scope.batchList[i].rackNo;
                }

                tempBatch.push($scope.batchList[i]);
            }

            console.log(JSON.stringify(editBatch))
            if (editBatch != null && tempBatch.length == 0) {
                var availableStock = $scope.getAvailableStock(editBatch, editBatch);
                editBatch.availableQty = availableStock;
                totalQuantity = availableStock;
                tempBatch.push(editBatch);
            }

            $scope.batchList = tempBatch;

            if ($scope.batchList.length > 0) {

                if (editBatch == null) {
                    $scope.selectedBatch = $scope.batchList[0];
                    $scope.selectedBatch.reminderFrequency = "0";
                    $scope.selectedBatch.discount = 0;
                }
                else {
                    $scope.selectedBatch = editBatch;
                    $scope.selectedBatch.discount = editBatch.discount;
                    $scope.selectedBatch.previoussellingPrice = editBatch.sellingPrice;
                }
                $scope.selectedBatch.totalQuantity = totalQuantity;
                $scope.selectedBatch.rackNo = rack;
                dayDiff($scope.selectedBatch.expireDate);
                if (editBatch == null) {
                    $scope.selectedBatch.discount = 0;
                }

            }
        }, function () { });
        $scope.valPurchasePrice = "";
        var qty = document.getElementById("quantity");
        qty.focus();
    }

    $scope.PopupAddNewProduct = function () {

        var m = ModalService.showModal({
            controller: "productCreateCtrl",
            templateUrl: 'createProduct',
            inputs: {
                mode: "Create"
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
    };

    $scope.showTempStockScreen = function () {

        cacheService.set('tempStockSection', $scope.selectedProduct)
        var m = ModalService.showModal({
            controller: "tempStockCtrl",
            templateUrl: 'tempStock'
            //,inputs: {
            //    reminderType: 1
            //}
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
            });
        });
        return false;
    };
});
