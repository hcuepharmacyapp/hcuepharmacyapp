﻿app.factory('estimateService', function ($http) {
    return {
        create: function (data) {
            return $http.post('/estimateData/create', data);
        },
        list: function (data) {
            return $http.post('/estimateData/list', data);
        },
        estimateDeatils: function (id) {
            return $http.get('/estimateData/Details?id=' + id);
        }
    }
});