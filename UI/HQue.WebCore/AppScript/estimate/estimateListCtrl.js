﻿app.controller('estimateListCtrl', function ($scope, $location, pagerServcie, printingHelper, estimateService, cacheService) {

    $scope.search = {};

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        $.LoadingOverlay("show");
        estimateService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.estimateSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;

        estimateService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.estimateSearch();

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.printBill = function (estimate) {
        printingHelper.printEstimate(estimate.id)
    }

    $scope.editBill = function (estimate) {
        cacheService.set("estimateEditId", estimate.id)
        $location.path('#/estimate');
    }
});
