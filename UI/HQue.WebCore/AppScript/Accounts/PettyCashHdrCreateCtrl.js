﻿app.controller('pettyCashHdrCreateCtrl', function ($scope, $q, $filter, $window,pettyCashHdrModel, pettyCashService, toastr, pagerServcie) {

    var pettyCash = pettyCashHdrModel;
    $scope.pettyCash = pettyCash;
    $scope.statusDiffFlag = "shortage-flag-color";

    var x = new Date();
    var timeString = x.getHours() + ":" + x.getMinutes() + ":" + x.getSeconds();
    document.getElementById('flamt1').focus();
    function setDefaultvalue() {
       
        $scope.salesAmount = 0;
        $scope.floatingAmount = 0;
        $scope.cashOnHand = 0;
        $scope.expenseAmount = 0;
        $scope.vendorPayment = 0;
        $scope.dinaminationAmount = 0;
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.pettyCash.transactionDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.clBalance = 0;
        $scope.clBal = 0;

        $scope.mode = 'SAVE';
    }
    setDefaultvalue();

    $scope.pettyCashHdrSave = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        pettyCashService.saveHdr($scope.pettyCash).then(function (response) {
            $scope.pettyCashForm.$setPristine();
            toastr.success('Created Petty Cash Details successfully');
            $scope.from = ($filter('date')($scope.pettyCash.transactionDate, 'yyyy-MM-dd HH:mm:ss'));
            $scope.pettyCashHdrSearch(2);
            $scope.statusDiff = "";
            //$scope.pettyCash = {};

            $.LoadingOverlay("hide");
           
            //setDefaultvalue();
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error', 'error');
        });
        $scope.isProcessing = false;
    };

    $scope.clearAll = function () {
        pettyCash = pettyCashHdrModel;
        $scope.pettyCash = pettyCash;
        setDefaultvalue();
    }

    $scope.pettyCashHdrUpdate = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        pettyCashService.updateHdr($scope.pettyCash).then(function (response) {
            $scope.pettyCashForm.$setPristine();
            toastr.success('Created Petty Cash Details successfully');
            $scope.pettyCash = {};
            $.LoadingOverlay("hide");
            setDefaultvalue();
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error', 'error');
        });
        $scope.isProcessing = false;
    };
    

     $scope.getLatestUpdatedDate = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.from,
            toDate: $scope.from
        }
        pettyCashService.getLatestDateHdr($scope.type, data).then(function (response) {
            $scope.updateddate = response.data;
           
            var udate = new Date($scope.updateddate.substr(0, 10));
            
            if ($filter('date')(udate, 'yyyy-MM-dd') < $filter('date')(new Date(), 'yyyy-MM-dd')){
                $scope.min1Date = $filter('date')(addDays(new Date($scope.updateddate.substr(0, 10)),1), 'yyyy-MM-dd');
            }else {
                $scope.min1Date = new Date();
            }
            $scope.testDate = $filter('date')(new Date($scope.updateddate), 'dd-MM-yyyy HH:mm:ss');
            // $scope.from = $filter('date')(new Date($scope.updateddate.substr(0, 10)), 'yyyy-MM-dd');
            $scope.goSearch();

            $.LoadingOverlay("hide");
            
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

     $scope.getLatestUpdatedDateforExpense = function () {
         $.LoadingOverlay("show");
         $scope.search.page.pageNo = 1;
         var data = {
             fromDate: $scope.from,
             toDate: $scope.from
         }
         pettyCashService.getLatestDate($scope.type, data).then(function (response) {
              $scope.expenseDate = response.data;
 
             $scope.viewExpenseDate = $filter('date')(new Date($scope.expenseDate), 'dd-MM-yyyy HH:mm:ss');
             // $scope.from = $filter('date')(new Date($scope.updateddate.substr(0, 10)), 'yyyy-MM-dd');
             $.LoadingOverlay("hide");

         }, function () {
             $.LoadingOverlay("hide");
         });
     };

   

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            $scope.pettyCash = {};
        }
    };


    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
       
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    function addDays(theDate, days) {
        return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
    }

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
   
    $scope.maxDate = addDays(new Date(), 5);
    $scope.minDate = new Date();

    $scope.max1Date = new Date();
   

    $scope.search = pettyCash;
    $scope.type = null;
    $scope.preBallist = [];
    $scope.ExpensedtlList = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        pettyCashService.list($scope.search).then(function (response) {
            $scope.preBallist = response.data.list;
        }, function () { });
    }

    $scope.getLatestUpdatedDateforExpense();
    $scope.getLatestUpdatedDate();
  

    $scope.pettyCashHdrSearch = function (status) {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.from,
            toDate: $scope.from
        }
        pettyCashService.listHdr($scope.type, data).then(function (response) {
            $scope.preBallist = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $scope.type = null;
            console.log(JSON.stringify($scope.preBallist));
            if ($scope.preBallist.length > 0 && status == 1) {
                $scope.clBalance = $scope.preBallist[0].clbalance;
                $scope.clBal = $scope.clBalance;
                $scope.pettyCash.clbalance = $scope.clBalance;
                $scope.getTotalExpense();
            } else if ($scope.preBallist.length > 0 && status == 2) {
                $scope.createdHdrId = $scope.preBallist[0].id;
                var counter = $scope.ExpensedtlList.length;
                if (counter == 0) {
                    $window.location.reload();
                    return;
                }
                var sequence = $q.defer();
                sequence.resolve();
                sequence = sequence.promise;
                angular.forEach($scope.ExpensedtlList, function (value, index) {
                    sequence = sequence.then(function () {
                    value.pettyHdrId = $scope.createdHdrId;
                    $scope.pettyCashDtlUpdate(value);
                    $window.location.reload();
                    counter -= 1;
                    return defaultAfterSave(counter);
                    });

                });
                
            } else if ($scope.preBallist.length == 0 && status == 1) {
                $scope.clBalance =0;
                $scope.clBal = 0;
                $scope.pettyCash.clbalance = 0;
                $scope.getTotalExpense();
            }
           
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    function defaultAfterSave(counter) {
        if (counter === 0) {
            $scope.pettyCash = {};
           
            setDefaultvalue();
        }
    }

     $scope.getSalesValue = function () {
         $.LoadingOverlay("show");
         var from =  ($filter('date')($scope.updateddate, 'yyyy-MM-dd HH:mm:ss'));
         var to = ($filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'));
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: from,
            toDate: to
        }
        pettyCashService.getSalesValue(data).then(function (response) {
            var salesData = response.data;
            pagerServcie.init(response.data.noOfRows, pageSearch);
                       
            if (salesData !== undefined) {
                console.log(JSON.stringify(salesData));
                $scope.salesAmount = salesData;
            }
            $scope.getVendorValue();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
   
     $scope.getVendorValue = function () {
         $.LoadingOverlay("show");
         var from = ($filter('date')($scope.updateddate, 'yyyy-MM-dd HH:mm:ss'));
         var to = ($filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'));
         $scope.search.page.pageNo = 1;
         var data = {
             fromDate: from,
             toDate: to
         }
         pettyCashService.getVendorValue(data).then(function (response) {
             var salesData = response.data;
             pagerServcie.init(response.data.noOfRows, pageSearch);

             if (salesData !== undefined) {
                 console.log(JSON.stringify(salesData));
                 $scope.vendorPayment = salesData;
             }
             $scope.getCustomerValue();
             $.LoadingOverlay("hide");
         }, function () {
             $.LoadingOverlay("hide");
         });
     };


     $scope.getCustomerValue = function () {
         $.LoadingOverlay("show");
         var from = ($filter('date')($scope.updateddate, 'yyyy-MM-dd HH:mm:ss'));
         var to = ($filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'));
         $scope.search.page.pageNo = 1;
         var data = {
             fromDate: from,
             toDate: to
         }
         pettyCashService.getCustomerValue(data).then(function (response) {
             var salesData = response.data;
             pagerServcie.init(response.data.noOfRows, pageSearch);

             if (salesData !== undefined) {
                 console.log(JSON.stringify(salesData));
                 $scope.pettyCash.customerPayment = salesData;
             }
             $scope.CalculateTotalAmt();
             $scope.Dinominate();
             $.LoadingOverlay("hide");
         }, function () {
             $.LoadingOverlay("hide");
         });
     };

    $scope.goSearch = function () {
        //if ($scope.from === undefined || $scope.from === "") {
        //    alert("Date Cannot be empty");
        //    return;
        //}
        $scope.from = ($filter('date')($scope.updateddate, 'yyyy-MM-dd HH:mm:ss')); //$scope.updateddate;
        $scope.pettyCashHdrSearch(1);
    }
   

    $scope.Datetype = function (type) {
        $scope.type = type;
        $scope.pettyCashHdrSearch(1);
    };


    $scope.pettyCashDtlSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.expenseDate,
            toDate:  $scope.expenseDate
        }
        pettyCashService.listDtl($scope.type, data).then(function (response) {
            $scope.ExpensedtlList = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $scope.type = null;
            if ($scope.ExpensedtlList.length > 0) {
                $scope.expenseAmount = 0;
                angular.forEach($scope.ExpensedtlList, function (value) {
                    $scope.expenseAmount = parseFloat($scope.expenseAmount) + parseFloat(value.amount);
                });
                
            }
            $scope.getSalesValue();
            //$scope.pettyCashHdrDetailsforCurrentDate();
            $.LoadingOverlay("hide");
            
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.pettyCashHdrDetailsforCurrentDate = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.pettyCash.transactionDate,
            toDate: $scope.pettyCash.transactionDate
        }
        pettyCashService.listHdr($scope.type, data).then(function (response) {
            $scope.preBallist = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $scope.type = null;
            console.log(JSON.stringify($scope.preBallist));
            if ($scope.preBallist.length > 0) {
                $scope.clBalance = $scope.preBallist[0].clbalance;
                $scope.clBal = $scope.preBallist[0].clbalance;
                $scope.salesAmount = $scope.preBallist[0].totalSales;
                $scope.floatingAmount = $scope.preBallist[0].floatingAmount;
                $scope.expenseAmount = $scope.preBallist[0].totalExpense;
                $scope.vendorPayment = $scope.preBallist[0].vendorPayment;
                $scope.pettyCash.customerPayment = $scope.preBallist[0].customerPayment;
                $scope.pettyCash.noof2000 = $scope.preBallist[0].noof2000;
                $scope.pettyCash.noof1000 = $scope.preBallist[0].noof1000;
                $scope.pettyCash.noof500 = $scope.preBallist[0].noof500;
                $scope.pettyCash.noof200 = $scope.preBallist[0].noof200;
                $scope.pettyCash.noof100 = $scope.preBallist[0].noof100;
                $scope.pettyCash.noof50 = $scope.preBallist[0].noof50;
                $scope.pettyCash.noof20 = $scope.preBallist[0].noof20;
                $scope.pettyCash.noof10 = $scope.preBallist[0].noof10;
                $scope.pettyCash.coins = $scope.preBallist[0].coins;
                $scope.pettyCash.id = $scope.preBallist[0].id;
                $scope.mode = "EDIT";
            } else {
                $scope.getSalesValue();
            }
            $scope.CalculateTotalAmt();
            $scope.Dinominate();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.acceptClBalance = function () {
        $scope.clBal = $scope.clBalance;
        $scope.pettyCash.clbalance = $scope.clBalance;
       
    }

    $scope.getTotalExpense = function () {
      
        $scope.expenseAmount = 0;
        //if ($scope.pettyCash.transactionDate === undefined || $scope.pettyCash.transactionDate === "" || $scope.pettyCash.transactionDate === null) {
        //    alert("Transaction Date Cannot be empty");
        //    return;
        //}
        $scope.pettyCash.transactionDate = ($filter('date')($scope.updateddate, 'yyyy-MM-dd HH:mm:ss'));
        $scope.pettyCashDtlSearch();
      
        $scope.CalculateTotalAmt();
    }
    $scope.pettyCashSaveHdr = function () {
        var r = confirm("Are you sure want to save the TILL ?");
        if (r == true) {
            $scope.pettyCash.totalSales = $scope.salesAmount;
            $scope.pettyCash.floatingAmount = $scope.floatingAmount;
            $scope.pettyCash.totalExpense = $scope.expenseAmount;
            $scope.pettyCash.vendorPayment = $scope.vendorPayment;
            $scope.pettyCash.transactionDate = $filter('date')(new Date(), 'yyyy-MM-dd') + " " + timeString; //$scope.pettyCash.transactionDate + " " + timeString;
            $scope.pettyCash.clbalance = ValueforNull(parseFloat($scope.pettyCash.clbalance));//ValueforNull($scope.cashOnHand);
            $scope.pettyCash.description = "Default";
            if ($scope.mode == 'SAVE') {
                $scope.pettyCashHdrSave();
            } else {
                $scope.pettyCashHdrUpdate();
            }
        }
       
        
    }

    $scope.CalculateTotalAmt = function () {
        var totalVal = ValueforNull((parseFloat($scope.clBal)) + ValueforNull(parseFloat($scope.salesAmount)) + ValueforNull(parseFloat($scope.floatingAmount)) + ValueforNull(parseFloat($scope.pettyCash.customerPayment))) -
            (ValueforNull(parseFloat($scope.expenseAmount)) +ValueforNull(parseFloat($scope.vendorPayment)));
        $scope.pettyCash.clbalance = totalVal;
        $scope.cashOnHand = ValueforNull($scope.dinaminationAmount) -  ValueforNull(parseFloat($scope.pettyCash.clbalance))
    }
    $scope.statusDiff = "";
    $scope.Dinominate = function () {
        $scope.statusDiff = "";
        var dinaTotal = ((ValueforNull(parseFloat($scope.pettyCash.noof2000)) * 2000) + (ValueforNull(parseFloat($scope.pettyCash.noof1000)) * 1000) + (ValueforNull(parseFloat($scope.pettyCash.noof500)) * 500)
                         + (ValueforNull(parseFloat($scope.pettyCash.noof200)) * 200) + (ValueforNull(parseFloat($scope.pettyCash.noof100)) * 100) + (ValueforNull (parseFloat($scope.pettyCash.noof50)) * 50)
                         + (ValueforNull(parseFloat($scope.pettyCash.noof20)) * 20) + (ValueforNull (parseFloat($scope.pettyCash.noof10)) * 10) + ValueforNull(parseFloat($scope.pettyCash.others))
                         + ValueforNull (parseFloat($scope.pettyCash.coins)))
        $scope.dinaminationAmount = dinaTotal;
        if (ValueforNull(parseFloat($scope.pettyCash.clbalance)) < 0) {
            $scope.cashOnHand = ValueforNull(parseFloat($scope.dinaminationAmount)) + ValueforNull(parseFloat($scope.pettyCash.clbalance))
        } else {
            $scope.cashOnHand = ValueforNull(parseFloat($scope.dinaminationAmount)) - ValueforNull(parseFloat($scope.pettyCash.clbalance))
        }
        
      
        if (ValueforNull(parseFloat($scope.dinaminationAmount.toFixed(2))) > ValueforNull(parseFloat($scope.pettyCash.clbalance.toFixed(2)))) {
            $scope.statusDiff = "Excess";
        } else if (ValueforNull(parseFloat($scope.dinaminationAmount.toFixed(2))) < ValueforNull(parseFloat($scope.pettyCash.clbalance.toFixed(2)))) {
            $scope.statusDiff = "Shortage";
        } else if (ValueforNull(parseFloat($scope.dinaminationAmount.toFixed(2))) == ValueforNull(parseFloat($scope.pettyCash.clbalance.toFixed(2)))) {
            $scope.statusDiff = "Tallied";
        }
        if (ValueforNull(parseFloat($scope.cashOnHand)) < 0) {
            $scope.cashOnHand = ValueforNull(parseFloat($scope.cashOnHand).toFixed(2)) * -1;
        }
        

        if ($scope.statusDiff != 'Tallied')
            $scope.statusDiffFlag = "shortage-flag-color";
        else
            $scope.statusDiffFlag = "tallied-flag-color";
        $scope.apply;
    }

    $scope.CalculateTotalAmtandDinominate = function () {
        $scope.CalculateTotalAmt();
        $scope.Dinominate();
    }

    function ValueforNull(val){
        if(isNaN(val)){
            val = 0;
        }
        return val;
    }


    //$scope.pettyCashSearch();
    $scope.pettyCashDtlUpdate = function (pettyCashDtls) {
       // $.LoadingOverlay("show");
       
        pettyCashService.updateDtl(pettyCashDtls).then(function (response) {
           
            $.LoadingOverlay("hide");
        }, function () {

            $.LoadingOverlay("hide");
            toastr.error('Error', 'error');
        });
        
    };

    $scope.moveNext = function (currentId, nextId) {
        var value1 = document.getElementById(currentId).value;
        if (value1 != null && value1 != "") {
            document.getElementById(nextId).focus();
        } else {
            document.getElementById(currentId).focus();
        }

        if(currentId == "rs2000" || currentId == "rs1000" || currentId == "rs500" || 
            currentId == "rs200" || currentId == "rs100" || currentId == "rs50" || currentId == "rs20" 
            || currentId == "rs10" || currentId == "coins") {
            $scope.Dinominate();
        }
    }
    
});