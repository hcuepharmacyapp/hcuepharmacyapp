﻿app.controller('pettyCashDtlCreateCtrl', function ($scope, $filter, $q,$timeout,pettyCashDtlModel, pettyCashService, toastr, pagerServcie) {

    var pettyCash = pettyCashDtlModel;
    $scope.pettyCash = pettyCash;
   // $scope.disableDesc = false;
    var x = new Date();
    var timeString = x.getHours() + ":" + x.getMinutes() + ":" + x.getSeconds();

    function setDefault() {
        $scope.mode = 'NEW';
        $scope.showSaveBtn = false;
        $scope.insertList = [];
        $scope.totalAmount = 0;
        $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    }
    setDefault();
    function addEmptyRow() {
        $scope.insertValue = { pettyItemId: "", description: "", amount: "", transactionDate: "", disableDesc : false};
        $scope.insertList.push(angular.copy($scope.insertValue));
        $scope.insertList.transactionDate = $scope.from;
    }
    addEmptyRow();
    
    $scope.pettyCashDtlSave = function (pettyCashDtls, chkval) {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if (pettyCashDtls.description == "" || pettyCashDtls.description === undefined) {
            toastr.info("Description cannot be empty");
            $.LoadingOverlay("hide");
            return;
        }
        pettyCashService.saveDtl(pettyCashDtls).then(function (response) {
            $scope.pettyCashForm.$setPristine();
            //var pettyCash1 = pettyCashDtlModel;
           // $scope.pettyCash = pettyCash1;
            if (chkval == 1) {
                $scope.populateData();
            } else {
                $.LoadingOverlay("hide");
               // setDefault();
            }
           // $.LoadingOverlay("hide");
        }, function () {
          
            $.LoadingOverlay("hide");
            toastr.error('Error', 'error');
        });
        $scope.isProcessing = false;
    };

    
    $scope.pettyCashDtlUpdate = function (pettyCashDtls, status) {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if (pettyCashDtls.description == "" || pettyCashDtls.description === undefined) {
            toastr.info("Description cannot be empty");
            return;
        }
        pettyCashService.updateDtl(pettyCashDtls).then(function (response) {
            $scope.pettyCashForm.$setPristine();
            var pettyCash2 = pettyCashDtlModel;
            $scope.pettyCash = pettyCash2;
            $scope.populateData();
            if (status == 1) {
                toastr.success('Expense Details Updated successfully');
            } else if (status == 3) {
                toastr.success('Expense Details Deleted successfully');
            }
           
            $.LoadingOverlay("hide");
        }, function () {

            $.LoadingOverlay("hide");
            toastr.error('Error', 'error');
        });
        $scope.isProcessing = false;
    };

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            $scope.pettyCash = {};
        }
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.maxDate = new Date();
    
    function addDays(theDate, days) {
        return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
    }

    function subtractDays(theDate, days) {
        return new Date(theDate.getTime() - days * 24 * 60 * 60 * 1000);
    }

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
   

    $scope.search = pettyCash;
    $scope.type = null;
    $scope.list = [];
    $scope.descriptionlist = [];
    $scope.vdate = { "viewDate": new Date() };
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        pettyCashService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    }

    $scope.getLatestUpdatedDate = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.from,
            toDate: $scope.from
        }
        pettyCashService.getLatestDate($scope.type, data).then(function (response) {
            $scope.updateddate = response.data;
           
            var udate = new Date($scope.updateddate.substr(0, 10));
            $scope.minDate = $filter('date')(subtractDays(new Date($scope.updateddate.substr(0, 10)), 7), 'yyyy-MM-dd');
            //if ($filter('date')(udate, 'yyyy-MM-dd') < $filter('date')(new Date(), 'yyyy-MM-dd')){
            //    $scope.minDate = $filter('date')(addDays(new Date($scope.updateddate.substr(0, 10)),1), 'yyyy-MM-dd');
            //}else {
            //    $scope.minDate = $filter('date')(subtractDays(new Date($scope.updateddate.substr(0, 10)), 2), 'yyyy-MM-dd');
            //}
            $scope.vdate.viewDate = $filter('date')(new Date($scope.updateddate), 'dd-MM-yyyy HH:mm:ss');
            // $scope.from = $filter('date')(new Date($scope.updateddate.substr(0, 10)), 'yyyy-MM-dd');
            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $.LoadingOverlay("hide");
            
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.getLatestUpdatedDate();

    $scope.pettyCashDtlSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.from,
            toDate: $scope.from
        }
        pettyCashService.listDtl($scope.type, data).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $scope.type = "";
            $.LoadingOverlay("hide");
            if ($scope.list.length == 0) {
                toastr.info('No records found');
            }
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

   // $scope.pettyCashDtlSearch();

    $scope.Datetype = function (type) {
        $scope.type = type;
        $scope.pettyCashDtlSearch();
    };


    $scope.pettyCashSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.from1,
            toDate: $scope.to1
        }
        pettyCashService.list($scope.type, data).then(function (response) {
            $scope.descriptionlist = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $scope.type = "";
            $.LoadingOverlay("hide");
            
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.pettyCashSearch();

    $scope.onDescriptionSelect = function (rec,item) {
        
        rec.transactionDate = $scope.from;
        rec.amount = item.amount;
        rec.pettyItemId = item.id;
        rec.description = item.description;
        calculateTotal();
        rec.disableDesc = true;
    }

    $scope.OnTextChanges = function (rec, item) {
        index = $scope.insertList.indexOf(item);
      
        if (index > -1) {
            if (rec.description !== item.description) {
                console.log(JSON.stringify(rec));
                return;
            }
        }
           
    }

    $scope.startsWith = function (item1, viewValue) {
        return item1.substr(0, viewValue.length).toLowerCase() == viewValue.toLowerCase();
    }
    
    $scope.getDescription = function (val) {
       var rtnval =  $scope.descriptionlist.map(function (temp) {
           
            if( temp.description.includes(val)){
                if (temp !== undefined) {
                    return temp;
                } 
              
            }
           // return temp;
       });

    
       return rtnval;
        
    }
    
    function calculateTotal() {
       
        var val = 0;
        $scope.totalAmount = 0;
        angular.forEach($scope.insertList, function (value) {
            val = ValueforNull(parseFloat(val)) + ValueforNull(parseFloat(value.amount));
            
        });

        $scope.totalAmount = val;
    }
    function ValueforNull(val) {
        if (isNaN(val)) {
            val = 0;
        }
        return val;
    }

    $scope.clearRow = function (item, rowid) {
        index = $scope.insertList.indexOf(item);
      
        if (index > -1) {
            $scope.insertList.splice(index, 1);
           
            if ($scope.insertList.length == 0) {
                addEmptyRow();
            }
            $scope.totalAmount = 0;
            calculateTotal();
            if (rowid >= 0) {
                var rowval = parseInt(rowid) - 1;
                if (rowval == -1) return;
                var val = document.getElementById('drugName_' + rowval.toString()).value;
                if (val != null && val != "") {
                    document.getElementById('drugName_' + rowval.toString()).focus();
                }
            }
           
           
        }
            
        
    }
    $scope.showSaveBtn = false;

    $scope.saveWhileUpdate = function (value) {
      

        if (value.amount > 0 && (value.pettyItemId !== undefined && value.pettyItemId !== "")
                        && (value.description !== undefined && value.description !== "")) {
            $scope.pettyCash.description = value.description;
            $scope.pettyCash.amount = value.amount;
            $scope.pettyCash.transactionDate = value.transactionDate + " " + timeString;;
            $scope.pettyCash.pettyItemId = value.pettyItemId;
            var dtlsobj = angular.copy($scope.pettyCash);
            $scope.pettyCashDtlSave(dtlsobj, 1);
            dtlsobj = undefined;
            toastr.success('Created Expense Details successfully');
            $.LoadingOverlay("hide");
        } else {
            toastr.info("Values cannot be empty")
            return;
        }
    }

    $scope.addNewRow = function (item, rowid) {
        if (item.description == "" || item.description === undefined) {
            toastr.info("Values cannot be empty")
            return;
        }
        addEmptyRow();
        calculateTotal();
        $scope.insertList.transactionDate = $scope.from;
        var rowval = parseInt(rowid) + 1;
        if ($scope.mode == 'EDIT') {
            $scope.showSaveBtn = true;
            document.getElementById('amount_' + rowval.toString()).focus();
        } else {
            var elem = document.getElementById('drugName_' + rowval.toString());
            if (elem != undefined) {
                elem.focus();
                $scope.$apply();
            }
        }
        
    }

    

    $scope.checkEnteredDesc = function (item,index) {
      
        if (item.pettyItemId == "") {
            toastr.success('Please select the item from the list');
            item.description = "";
            item.amount = "";
        } else {
            value1 = document.getElementById('amount_' + index).value;
            item.disableDesc = true;
            //var element = document.getElementById('descItems');
            //var elementToFocus = element.children().eq(index).find('amount')[0];
           // $scope.onDescriptionSelect(item, item);
            if (value1 != null && value1 != "") {
                document.getElementById('amount_' + index).focus();
            }
            //if (angular.isDefined(elementToFocus))
            //    elementToFocus.focus();
        }
       
    }

    $scope.SaveRows = function () {
        var counter = $scope.insertList.length;
        //if ($scope.insertList.length > 0) {
        //    $scope.pettyCashAllDtlSave($scope.insertList);
        //}
        
        var sequence = $q.defer();
        sequence.resolve();
        sequence = sequence.promise;

        angular.forEach($scope.insertList, function (value, key) {
            sequence = sequence.then(function () {
                if (value.amount > 0 && (value.pettyItemId !== undefined && value.pettyItemId !== "")
                                             && (value.description !== undefined && value.description !== "")) {
                    $scope.pettyCash.description = value.description;
                    $scope.pettyCash.amount = value.amount;
                    $scope.pettyCash.transactionDate = value.transactionDate + " " + timeString;
                    $scope.pettyCash.pettyItemId = value.pettyItemId;
                    $scope.pettyCash.deletedStatus = false;
                    var dtlsobj = angular.copy($scope.pettyCash);
                    $scope.pettyCashDtlSave(dtlsobj, 0);
                }
                counter -= 1;
                return defaultAfterSave(counter);
            });
        });
       
    }

    function defaultAfterSave(counter) {
        if (counter === 0) {
            toastr.success('Created Expense Details successfully');
            $scope.mode = 'NEW';
            $scope.showSaveBtn = false;
            setDefault();
            $scope.vdate.viewDate = $scope.pettyCash.transactionDate;
            addEmptyRow();
            var pettyCash3 = pettyCashDtlModel;
            $scope.pettyCash = pettyCash3;
            //$scope.getLatestUpdatedDate();
        }
    }

    $scope.populateData = function () {
        $scope.insertList.length = 0;
        $scope.list.length = 0;
        $.LoadingOverlay("show");
        $scope.showSaveBtn = false;
        $scope.totalAmount = 0;
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.from,//$scope.updateddate,
            toDate:  $scope.from //$scope.updateddate
        }
        pettyCashService.listDtl($scope.type, data).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $scope.type = "";
            $.LoadingOverlay("hide");
            if ($scope.list.length == 0) {
                toastr.info('No records found');
                addEmptyRow();
                $scope.mode = 'NEW';
                $scope.showSaveBtn = false;
            } else {
                angular.forEach($scope.list, function (value) {
                    var insertValue = { id: "", pettyItemId: "", description: "", amount: "", transactionDate: "" , disableDesc : true};
                    insertValue.id = value.id;
                    insertValue.pettyItemId = value.pettyItemId;
                    insertValue.description = value.description.toString().split("/").join("").split("\n").join("").trim();
                    insertValue.amount = value.amount;
                 
                    var tDate = new Date(value.transactionDate);
                    timeString = tDate.getHours() + ":" + tDate.getMinutes() + ":" + tDate.getSeconds();
                    var udate = new Date(value.transactionDate.substr(0, 10));
                    insertValue.transactionDate = $filter('date')(udate, 'yyyy-MM-dd');
                    
                    $scope.from = insertValue.transactionDate;
                    $scope.insertList.push(insertValue);
                   
                    calculateTotal();
                });
                var pettyCash = pettyCashDtlModel;
                $scope.pettyCash = pettyCash;
                $scope.mode = 'EDIT';
                console.log(JSON.stringify($scope.insertList))
            }
        }, function () {
            $.LoadingOverlay("hide");
        });
        
      
       
       
    }
    $scope.clearData = function () {
        $scope.insertList.length = 0;
        $scope.totalAmount = 0;
        $scope.mode = 'NEW';
        $scope.showSaveBtn = false;
        addEmptyRow();
        document.getElementById('drugName_0').focus();
        var pettyCash4 = pettyCashDtlModel;
        $scope.pettyCash = pettyCash4;
    }

    $scope.update = function (value) {
        if (value.amount <= 0) {
            return;
        }
        
            $scope.pettyCash.id = value.id;
            $scope.pettyCash.pettyItemId = value.pettyItemId;
            $scope.pettyCash.description = value.description;
            $scope.pettyCash.amount = value.amount;
            $scope.pettyCash.transactionDate = value.transactionDate + " " + timeString;;
            $scope.pettyCash.pettyItemId = value.pettyItemId;
            $scope.pettyCash.deletedStatus = false;
        
            if (value.id !== undefined) {
                $scope.pettyCashDtlUpdate($scope.pettyCash,1);
            } else {
                $scope.pettyCash.id = null;
                $scope.pettyCashDtlSave($scope.pettyCash,1);
            }
            $scope.showSaveBtn = false;
    }

    $scope.delete = function (value) {
        if (value.id === undefined) {
            $scope.clearRow(value,-1);
        } else {
            var del = window.confirm('Are you sure, Do you want to Delete?');
            if (del) {
                $scope.pettyCash.id = value.id;
                $scope.pettyCash.pettyItemId = value.pettyItemId;
                $scope.pettyCash.description = value.description;
                $scope.pettyCash.amount = value.amount;
                $scope.pettyCash.transactionDate = value.transactionDate + " " + timeString;;
                $scope.pettyCash.pettyItemId = value.pettyItemId;
                $scope.pettyCash.deletedStatus = true;
                $scope.pettyCashDtlUpdate($scope.pettyCash, 3);
            } 
        }
    }

    $scope.moveNext = function (currentId, nextId) {
        var value1 = document.getElementById(currentId).value;
        if (value1 != null && value1 != "") {
            document.getElementById(nextId).focus();
        } else {
            document.getElementById(currentId).focus();
        }
       // if (currentId == "amount") {
            calculateTotal();
       // }
    }

    $scope.changeAmount = function () {
        calculateTotal();
    }
});