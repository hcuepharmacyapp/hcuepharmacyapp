﻿app.factory('paymentService', function ($http) {
    return {
        list: function (payment) {
            return $http.post('/PaymentData/Index' , payment);
        },
        //history added by nandhini 20.9.17
        listHistory: function (history) {
            return $http.post('/PaymentData/History?', history);
        },

        save: function (data) {
            return $http.post('/PaymentData/Save', data);
        },
        saveFinancialYear: function (data) {
            return $http.post('/PaymentData/saveFinancialYear/', data);
        },
        
        saveFinyearStatus: function (data) {
                return $http.post('/PaymentData/saveFinyearStatus/', data);
            },



        paymentReport: function (type, instanceid) {
            return $http.post('/PaymentData/reportListData?type=' + type + '&sInstanceId=' + instanceid);
        },
        update: function (data) {
            return $http.post('/PaymentData/Update', data);
        },
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        deleteHistory: function (data) {
            return $http.post('/PaymentData/Delete', data);
        },


        "getFinyearAvailable": function (InvoiceSeries) {
            return $http.get('/PaymentData/getFinyearAvailable');
        }
    }
});
