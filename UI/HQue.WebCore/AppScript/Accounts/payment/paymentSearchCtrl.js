﻿app.controller('paymentSearchCtrl', function ($scope, paymentService, paymentModel, vendorService, vendorPurchaseModel, toastr, vendorModel) {

    $scope.minDate = new Date();
    $scope.maxDate = $scope.minDate.setMonth($scope.minDate.getMonth() + 6);
    $scope.checkDate = new Date().toISOString();
        

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        "opened": false
    };


    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };

    $scope.open4 = function () {
        $scope.popup4.opened = true;
    };

    $scope.popup4 = {
        "opened": false
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.toDate = function (dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    };

    var payment = paymentModel;
    payment.vendorPurchase = vendorPurchaseModel;

    $scope.search = payment;
    $scope.search.vendor = vendorModel;
    $scope.list = [];
    $scope.vendorList = [];
    $scope.payment = payment;
    $scope.showCheque = false;
    $scope.mobileInput = true;

    //$scope.selectedVendor = function () {
    //    $scope.selectedVendor;
    //};

   
    $scope.init = function (val) {
        $scope.isHistory = val;
        $scope.vendor();
    }
        

    $scope.vendor = function () {
        if ($scope.isHistory) {
            $scope.vendorStatus = 2;
            vendorService.vendorDataList($scope.vendorStatus).then(function (response) { //vendorService.vendorAllData().then(function (response) { //vendorData
                $scope.vendorList = response.data;
            }, function () { toastr.error('Error Occured', 'Error'); });
        }
        else {
            $scope.vendorStatus = 3;
            vendorService.vendorDataList($scope.vendorStatus).then(function (response) {
                $scope.vendorList = response.data;
            }, function () { toastr.error('Error Occured', 'Error'); });
        }
    };    

    $scope.vendorSearch = function () {
        $.LoadingOverlay("show");
        $scope.creditAmount = 0;
        $scope.debitAmount = 0;
        $scope.search.vendor.id = (($scope.selectedVendor != null && $scope.selectedVendor != undefined) ? $scope.selectedVendor.id : null);

        if (($scope.search.select == "mobile") && ($scope.search.mobile == null || $scope.search.mobile == '' || $scope.search.mobile == undefined)) {
            toastr.error("Mobile no not entered");
            $.LoadingOverlay("hide");
            return 0;
        }

        paymentService.list($scope.search).then(function (response) {
            $scope.list = response.data;
            $scope.selectall = true;
            for (var i = 0; i < $scope.list.length; i++) {
                $scope.list[i].hold = true;                
                $scope.list[i].payment = {
                    "vendorPurchaseId": $scope.list[i].vendorPurchaseId,
                    "vendorId": (($scope.selectedVendor != null && $scope.selectedVendor != undefined) ? $scope.selectedVendor.id : $scope.list[i].vendorId),
                    "paymentType": "Cash"
                };                
                $scope.creditAmount += $scope.list[i].credit;
                $scope.debitAmount += $scope.list[i].debit;

            }
            if ($scope.selectedVendor != null && $scope.selectedVendor != undefined) {
                $scope.selectedVendorName = $scope.selectedVendor.name;
                $scope.selectedVendorMobile = $scope.selectedVendor.mobile;
            }
            else if ($scope.list.length > 0) {
                if ($scope.search.select == "mobile") {
                    $scope.selectedVendorName = $scope.list[0].vendor.name;
                    $scope.selectedVendorMobile = $scope.search.mobile;
                    $scope.selectedVendorId = $scope.list[0].vendor.id;
                }
                else {
                    $scope.selectedVendorName = null;
                    $scope.selectedVendorMobile = null;
                    $scope.selectedVendorId = null;
                }
                
            }
            else {
                $scope.selectedVendorName = null;
                $scope.selectedVendorMobile = null;
                $scope.selectedVendorId = null;
            }
            $scope.paymentData = null;
            $scope.showCheque = false;
            $.LoadingOverlay("hide");

            angular.element("#enterAmount").focus();
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.editPayment = function (item, bool) {
        item.isEdit = bool;
    };

    $scope.savePayment = function (payment) {
        $.LoadingOverlay("show");
        paymentService.save(payment).then(function (response) {
            toastr.success('Payment made successfully');
            $scope.vendorSearch();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.changeStatus = function (status) {
        for (var i = 0; i < $scope.list.length; i++) {
            $scope.list[i].hold = status;
        }

        if ($scope.paymentData.amount == undefined) {
            $scope.amountReceived(0);
        } else {
            $scope.amountReceived($scope.paymentData.amount);
        }

    };

    $scope.setOptions = function () {
        if ($scope.search.select == "mobile") {
            $scope.mobileInput = true;
        }
        else if ($scope.search.select == "credit") {
            $scope.mobileInput = false;
        }
    };

    $scope.clearSearch = function () {
        /* 
        $scope.search.select = null;
        $scope.search.select1 = null;
        $scope.search.mobile = null;
        $scope.search.creditDate = null;
        $scope.selectedVendor = null;
        $scope.selectedVendorName = null;
        $scope.selectedVendorMobile = null;
        $scope.selectedVendorId = null;
        $scope.mobileInput = true;
        $scope.listHistory = null;
        */
        window.location.reload(true);
    };

    $scope.clearOptions = function () {
        $scope.search.select = null;
        $scope.search.select1 = null;
        $scope.search.mobile = null;
        $scope.search.creditDate = null;
        $scope.selectedVendorName = null;
        $scope.selectedVendorMobile = null;
        $scope.selectedVendorId = null;
        $scope.vendorSearch();
    };

    $scope.focusNextField = function (nextid) {
        var ele = document.getElementById(nextid);
        ele.focus();
    };

    $scope.checkAllPaymentfields = function (ind) {

        if (ind > 1) {
            if ($scope.paymentData.amount == undefined || Object.keys($scope.paymentData.amount).length == 0) {
                var ele = document.getElementById("enterAmount");
                ele.focus();
                return false;
            }
        }
        //if (ind > 4) {
        //    if ($scope.paymentData.chequeNo == undefined || Object.keys($scope.paymentData.chequeNo).length == 0) {
        //        var ele = document.getElementById("chequeNo");
        //        ele.focus();
        //        return false;
        //    }
        //}

        //if (ind > 5) {
        //    if ($scope.paymentData.chequeDate == undefined || Object.keys($scope.paymentData.chequeNo).length == 0) {
        //        var ele = document.getElementById("chequeDate");
        //        ele.focus();
        //        return false;
        //    }
        //}

    };

    $scope.totalAlert = '';
    $scope.paymentAlert = false;
    $scope.amountReceived = function (payment) {
        var checked = 0;
        if (payment > ($scope.creditAmount - $scope.debitAmount)) {
            $scope.totalAlert = 'Amount Exceeded';
        } else {
            $scope.totalAlert = '';
        }
        for (var i = 0; i < $scope.list.length; i++) {
            if (!$scope.list[i].hold) {
                if (payment > 0) {
                    $scope.list[i].paymentAmount = $scope.list[i].credit - $scope.list[i].debit;
                    if (payment >= $scope.list[i].paymentAmount) {
                        payment = payment - ($scope.list[i].credit - $scope.list[i].debit);
                        $scope.list[i].paymentAmount = $scope.list[i].paymentAmount;
                    } else {
                        $scope.list[i].paymentAmount = payment;
                        payment = 0;
                    }
                }
                else {
                    $scope.list[i].paymentAmount = 0;
                }
            } else {
                checked++;
                $scope.list[i].paymentAmount = 0;
                continue;
            }
        }
        if ($scope.list.length == checked) {
            $scope.selectall = true;
        } else {
            $scope.selectall = false;
        }
    };

    $scope.savePayments = function (payment, remarks, paymentType, data) {

        if ($scope.selectedVendor.status == 2) {
            toastr.info("", "Please Change Vendor status..");
            return;
        }

        $.LoadingOverlay("show");
        $scope.payment.remarks = remarks;
        $scope.payment.paymentType = paymentType;
        $scope.payment.paymentList = payment;

        if ($scope.selectedVendor != null && $scope.selectedVendor != undefined) {
            $scope.payment.vendorId = $scope.selectedVendor.id;
        }
        else if ($scope.selectedVendorId != null) {
            $scope.payment.vendorId = $scope.selectedVendorId;
        }
        else {
            toastr.error("Vendor Not Selected");
            $.LoadingOverlay("hide");
            return 0;
        }
        
        $scope.payment.bankName = data.bankName;
        $scope.payment.bankBranchName = data.bankBranchName;
        $scope.payment.ifscCode = data.ifscCode;
        $scope.payment.chequeNo = data.chequeNo;
        $scope.payment.chequeDate = data.chequeDate;

        if (parseInt($scope.selectedVendor.status) == 3) {
            var credit = $scope.payment.paymentList[0].credit;
            var pay = parseFloat($scope.paymentData.amount) + parseFloat($scope.payment.paymentList[0].debit);

            if (pay == parseInt(credit)) {
                $scope.selectedVendor.status = 4;
                $scope.payment.vendor = $scope.selectedVendor;
            }
        }

        paymentService.save($scope.payment).then(function (response) {
            toastr.success('Payment made successfully');
            $scope.creditAmount = 0;
            $scope.debitAmount = 0;
            var paymentDataAmount = parseInt($scope.paymentData.amount);
            var paymentAmount = 0;
            $scope.paymentData = null;
            if ($scope.selectedVendor.status == 4)
                window.location.reload(true);
            $scope.vendorSearch();
     
            var len = $scope.list.length;
            if (len > 0) {
                for (var i = 0; i < len; i++) {
                    paymentAmount += parseInt($scope.list[i].credit);
                }
                if (paymentDataAmount == paymentAmount)
                    window.location.reload(true);
            }

            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };


    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.editSales = function (paymentList) {
        paymentList.IsEdit = true;
    };

    $scope.vendorSearchHistory = function () {
        $.LoadingOverlay("show");
        $scope.creditAmount = 0;
        $scope.debitAmount = 0;
        $scope.search.vendor.id = (($scope.selectedVendor != null && $scope.selectedVendor != undefined) ? $scope.selectedVendor.id : null);

        if (($scope.search.select == "mobile") && ($scope.search.mobile == null || $scope.search.mobile == '' || $scope.search.mobile == undefined)) {
            toastr.error("Mobile no not entered");
            $.LoadingOverlay("hide");
            return 0;
        }
        paymentService.listHistory($scope.search).then(function (response) {
            $scope.listHistory = response.data;

            for (var i = 0; i < $scope.listHistory.length; i++) {
                $scope.listHistory[i].payment = {
                    vendorPurchaseId: $scope.listHistory[i].vendorPurchaseId,                   
                    vendorId: (($scope.selectedVendor != null && $scope.selectedVendor != undefined) ? $scope.selectedVendor.id : $scope.listHistory[i].vendor.id),
                    paymentType: "Cash"
                };
                $scope.creditAmount += $scope.listHistory[i].credit;
                $scope.debitAmount += $scope.listHistory[i].debit;

            }

            if ($scope.selectedVendor != null && $scope.selectedVendor != undefined) {
                $scope.selectedVendorName = $scope.selectedVendor.name;
                $scope.selectedVendorMobile = $scope.selectedVendor.mobile;
            }
            else if ($scope.listHistory.length > 0) {
                if ($scope.search.select == "mobile") {
                    $scope.selectedVendorName = $scope.listHistory[0].vendor.name;
                    $scope.selectedVendorMobile = $scope.search.mobile;
                    $scope.selectedVendorId = $scope.listHistory[0].vendor.id;
                }
                else {
                    $scope.selectedVendorName = null;
                    $scope.selectedVendorMobile = null;
                    $scope.selectedVendorId = null;
                }

            }
            else {
                $scope.selectedVendorName = null;
                $scope.selectedVendorMobile = null;
                $scope.selectedVendorId = null;
            }
            
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.savePaymentHistory = function (payment) {
        $.LoadingOverlay("show");
        paymentService.update(payment).then(function (response) {
            toastr.success('Payment updated successfully');
            $scope.creditAmount = 0;
            $scope.debitAmount = 0;
            $scope.vendorSearch();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.deletePaymentHistory = function (payment, parindex, index) {
        $.LoadingOverlay("show");
        payment.vendor = $scope.selectedVendor;
        paymentService.deleteHistory(payment).then(function (response) {
            toastr.success('Payment updated successfully');

            var data = response;
            if (data != undefined || data != null) {
                $scope.listHistory[parindex].paymentList.splice(index, 1);
                if ($scope.listHistory[parindex].paymentList.length == 0)
                    $scope.listHistory.splice(parindex, 1);
            }

            if ($scope.listHistory.length == 0)
                window.location.reload(true);

            $scope.creditAmount = 0;
            $scope.debitAmount = 0;
            $scope.vendorSearch();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.getChequeDetails = function (val) {
        if (val == "cheque") {
            $scope.paymentData.bankName = "";
            $scope.paymentData.bankBranchName = "";
            $scope.paymentData.ifscCode = "";
            $scope.paymentData.chequeNo = "";
            $scope.paymentData.chequeDate = "";
            $scope.showCheque = true;
        } else {
            $scope.paymentData.bankName = "";
            $scope.paymentData.bankBranchName = "";
            $scope.paymentData.ifscCode = "";
            $scope.paymentData.chequeNo = "";
            $scope.paymentData.chequeDate = "";
            $scope.showCheque = false;
        }
        $scope.paymentAlert = false;
    };

    $scope.payInValid = null;

    $scope.isPayValid = function () {
        var dat1 = new Date();
        dat1.setMonth(dat1.getMonth() + 6);
        var d = dat1.toISOString().slice(0, 10);
        if ($scope.paymentData != null) {
            if ($scope.paymentData.amount > 0 && $scope.paymentData.paymentType != null) {
                if ($scope.paymentData.paymentType == "cash") {
                    return false;
                } else {
                    if ($scope.paymentData.chequeDate != null && $scope.paymentData.chequeDate <= d) {
                        $scope.payInValid = false;
                    } else {
                        $scope.payInValid = true;
                    }
                    if ($scope.paymentData.chequeDate != null && $scope.paymentData.chequeDate <= d && $scope.paymentData.chequeNo != null) {
                        return false;
                    }
                }
            } else if ($scope.paymentData.paymentType != null && $scope.paymentData.paymentType == "cheque") {
                if ($scope.paymentData.chequeDate != null && $scope.paymentData.chequeDate <= d) {
                    $scope.payInValid = false;
                } else {
                    $scope.payInValid = true;
                }
            }
        }
        return true;
    };

    $scope.checkNetAmount = function (editAmountList, netAmount) {

        $scope.getTotal = 0;
        for (var i = 0; i < editAmountList.length; i++) {
            $scope.getTotal = parseFloat($scope.getTotal) + parseFloat(editAmountList[i].debit);
        }
        return $scope.getTotal;
    };

    $scope.isTallyPaymentAmount = function () {
        var totalPaymentAmount = 0;
        for (var i = 0; i < $scope.list.length; i++) {
            if ($scope.list[i].hold == false) {
                totalPaymentAmount = parseFloat(totalPaymentAmount) + parseFloat($scope.list[i].paymentAmount);
                if (totalPaymentAmount == $scope.paymentData.amount) {
                    return false;
                }
            }
        }
        return true;
    };

    $scope.keyUp = function (e, nextid, currentid) {        
        //Enter
        if (e.keyCode == 13) {
            var text = document.getElementById(currentid).value;
            if (text != "") {
                var ele = document.getElementById(nextid);
                ele.focus();
            }
        }
        if (currentid == "hold") {
            if ($scope.paymentData.paymentType == undefined) {
                $scope.paymentAlert = true;
            }
            else {
                $scope.paymentAlert = false;
            }
        }
    };

});