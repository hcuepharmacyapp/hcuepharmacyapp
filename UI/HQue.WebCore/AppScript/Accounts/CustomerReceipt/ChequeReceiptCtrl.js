﻿app.controller("ChequeReceiptCtrl", ["$scope", "customerReceiptService", "toastr", function ($scope, customerReceiptService, toastr) {

    $scope.patientSearchData = { "mobile": "" };
    $scope.disableCustomerMobile = false;
    $scope.list = [];
    $scope.totalAmount = 0;
    $scope.getCustomers = function (val, type) {
        return customerReceiptService.searchCustomer(val, type).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    $scope.onCustomerSelect = function () {
        $scope.patientSearchData.mobile = parseInt($scope.selectedCustomer.mobile);
    };
    $scope.customerChequeSearch = function () {
        if ($scope.selectedCustomer != null) {
            $scope.GetCustomerCheque($scope.selectedCustomer.patientId);
        }
    };
    $scope.GetCustomerCheque = function (customerId) {
        customerReceiptService.GetCustomerCheque(customerId)
        .then(function (resp) {
            $scope.name = $scope.selectedCustomer.name;
            $scope.mobile = $scope.selectedCustomer.mobile;
            $scope.PatientId = $scope.selectedCustomer.patientId;
            $scope.list = resp.data;
            GetTotal();
        }, function (error) {
            console.log(error);
        });
    };

    function GetTotal() {
        $scope.totalAmount = 0;
        for (var i = 0; i < $scope.list.length; i++) {
            $scope.list[i].isEdit = false;
            $scope.totalAmount += $scope.list[i].amount;
        }
    }
    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };
    $scope.EditItem = function (index) {
        if ($scope.list[index].isEdit) {
            $scope.list[index].isEdit = false;
        } else {
            $scope.list[index].isEdit = true;
        }
    };
    $scope.SaveItem = function (item, index) {
        customerReceiptService.updateChequeStatus(item)
        .then(function (resp) {
            toastr.success('Payment made successfully');
            if ($scope.list[index].bankDeposited && $scope.list[index].amountCredited) {
                $scope.list.splice(index, 1);
                if ($scope.list.length == 0) {
                    $scope.totalAmount = 0;
                    $scope.name = "";
                    $scope.mobile = "";
                    $scope.selectedCustomer = null;
                } else {
                    GetTotal();
                }
            } else {
                $scope.list[index].isEdit = false;
                //GetTotal();
            }
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
}]);

