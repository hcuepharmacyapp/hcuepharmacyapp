﻿app.controller('customerReceiptSearchCtrl', function ($scope, customerReceiptService, customerPaymentModel, salesModel, toastr, $filter) {

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.disableCustomerMobile = false;


    $scope.popup1 = {
        "opened": false
    };

    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    var payment = customerPaymentModel;
    payment.sales = salesModel;
    $scope.patientSearchData = { "mobile": "" };


    $scope.search = payment;
    $scope.list = [];
    //$scope.customerList = [];
    $scope.payment = payment;

    $scope.selectedCustomer = function () {
        $scope.selectedCustomer;
    };

    //Commented by Settu to avoid unwanted serive hit and done respective dependency changes
    //$scope.customer = function () {
    //    customerReceiptService.customerData().then(function (response) {
    //        $scope.customerList = response.data;
    //    }, function () { toastr.error('Error Occured', 'Error'); });
    //};
    //$scope.customer();

    $scope.customerReceiptSearch = function () {
        $scope.totalDue = 0;
        $.LoadingOverlay("show");
        var name = "";
        var mobile = "";
        if ($scope.selectedCustomer.customerName !== undefined) {
            name = $scope.selectedCustomer.customerName;
        }
        if ($scope.patientSearchData !== undefined) {
            mobile = $scope.patientSearchData.mobile;
        }
        customerReceiptService.list(name, mobile).then(function (response) {
            $scope.list = response.data;
            $scope.Filterlist = $scope.list;

            $scope.Filterlist = $filter("filter")($scope.Filterlist, { "customerStatus": 1 });
            $scope.Filterlist1 = $filter("filter")($scope.list, { "customerStatus": 3 });

            for (var i = 0; i < $scope.Filterlist1.length; i++) {
                $scope.Filterlist.push($scope.Filterlist1[i]);
            }

            for (var i = 0; i < $scope.list.length; i++) {
                $scope.totalDue += $scope.list[i].customerCredit - $scope.list[i].customerDebit;
            }
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.customerReceiptSearch();
    $scope.changeCustomerName = function (customer) {
        $scope.patientSearchData.mobile = customer.customerMobile;
        $scope.disableCustomerMobile = true;
        $scope.showCustomerWarn = "";
        //Customer receipt filtered by PatientId instead of patient mobile & name - Settu
        $scope.list = $filter("filter")($scope.Filterlist, { sales: { patientId: $scope.selectedCustomer.sales.patientId } }, true);
        $scope.totalDue = 0;
        getTotalDue();
        //$scope.customerReceiptSearch();
    };

    function getTotalDue() {
        for (var i = 0; i < $scope.list.length; i++) {
            $scope.totalDue += $scope.list[i].customerCredit - $scope.list[i].customerDebit;
        }
    }

    $scope.changeMobileNumber = function (customerMobile) {


        if (customerMobile == "") {
            $scope.showCustomerWarn = "";
        } else if (customerMobile.length == 0 || customerMobile.length < 10) {
            $scope.showCustomerWarn = "Enter 10 Digit Mobile Number";
        } else {
            $scope.showCustomerWarn = "";
            var customerObject = $filter("filter")($scope.Filterlist, { "customerMobile": customerMobile }, true);
            if (customerObject.length != 0) {
                $scope.showCustomerWarn = "";
                $scope.selectedCustomer = customerObject[0];
                $scope.list = customerObject;
                $scope.totalDue = 0;
                getTotalDue();
                //$scope.customerReceiptSearch();

            } else {
                $scope.selectedCustomer = "";
                $scope.showCustomerWarn = "No Customer Found";
            }
        }
    };

    $scope.Filterdata = function (customerMobile) {
        if ($scope.disableCustomerMobile) {
            var customerObject = $filter("filter")($scope.Filterlist, { sales: { patientId: $scope.selectedCustomer.sales.patientId } }, true);
        }
        else {
            var customerObject = $filter("filter")($scope.Filterlist, { "customerMobile": customerMobile }, true);
        }
        
        if (customerObject.length != 0) {
            $scope.selectedCustomer = customerObject[0];
            $scope.list = customerObject;
            $scope.totalDue = 0;
            getTotalDue();
        }
        else {
            $scope.selectedCustomer = "";
            $scope.list = [];
            $scope.totalDue = 0;
        }
    }

    $scope.searchCustomer = function (customerMobile) {
        if (customerMobile.length == 0 || customerMobile.length < 10 || $scope.selectedCustomer == null) {
        } else {
            $scope.customerReceiptSearch();
        }
    };
    $scope.savePayment = function (payment) {
        $.LoadingOverlay("show");
        customerReceiptService.save(payment).then(function (response) {
            toastr.success('Payment made successfully');
            $scope.customerReceiptSearch();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };

});
