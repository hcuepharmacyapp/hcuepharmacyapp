﻿app.controller('pettyCashExpenseListCtrl', function ($scope,$filter, pettyCashService, pettyCashDtlModel, pagerServcie) {

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    var pettyCash = pettyCashDtlModel;

    $scope.search = pettyCash;
    $scope.type = 'ALL';
    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination$filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
    function pageSearch() {
        pettyCashService.listDtl($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    }

    $scope.pettyCashSearch = function (type) {
        $.LoadingOverlay("show");
        $scope.type = type;
        $scope.search.page.pageNo = 1;
        var data = {
            fromDate: $scope.from,
            toDate: $scope.to
        }
        pettyCashService.listDtl($scope.type, data).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $scope.type = "";
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.pettyCashSearch('ALL');

    $scope.Datetype = function (type) {
        $scope.type = type;
        if ($scope.type === "TODAY") {
            $scope.from = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');
        }
        if ($scope.type === "Week") {
            var curr = new Date;
            var firstday = $filter('date')(new Date(curr.setDate(curr.getDate() - curr.getDay())), 'yyyy-MM-dd');
            $scope.from = firstday;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        if ($scope.type === "Month") {
            var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth();
            var firstDay = $filter('date')(new Date(y, m, 1), 'yyyy-MM-dd');
            $scope.from = firstDay;
            $scope.to = $filter('date')(new Date(), 'yyyy-MM-dd');;
        }
        $scope.pettyCashSearch(type);
    };

});