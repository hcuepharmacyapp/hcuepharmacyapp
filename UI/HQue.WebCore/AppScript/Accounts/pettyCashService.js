﻿app.factory('pettyCashService', function ($http) {
    return {
        list: function (type, data) {
            return $http.post('/PettyCashData/listData?type='+type,data);
        },

        save: function (data) {
            
            return $http.post('/PettyCashData/index', data);
        },

        listSettings: function (type, data) {
            return $http.post('/PettyCashSettingsData/listData?type=' + type, data);
        },

        saveSettings: function (data) {
            return $http.post('/PettyCashSettingsData/index', data);
        },

        saveHdr: function (data) {
            return $http.post('/PettyCashHdrData/Index', data);
        },

        getSalesValue: function (data) {
            return $http.post('/PettyCashHdrData/getSalesValue', data);
        },
         
        getVendorValue: function (data) {
            return $http.post('/PettyCashHdrData/getVendorValue', data);
        },

        getCustomerValue: function (data) {
            return $http.post('/PettyCashHdrData/getCustomerValue', data);
        },

        updateHdr: function (data) {
            return $http.post('/PettyCashHdrData/Update', data);
        },

        getLatestDateHdr: function (data) {
            return $http.post('/PettyCashHdrData/GetLatestupdatedDate', data);
        },

        listHdr: function (type, data) {
            return $http.post('/PettyCashHdrData/listData?type=' + type, data);
        },
        saveDtl: function (data) {
            return $http.post('/PettyCashDtlData/Save', data);
        },

        saveAll: function (data) {
            return $http.post('/PettyCashDtlData/SaveAll', data);
        },

        getLatestDate: function (data) {
            return $http.post('/PettyCashDtlData/GetLatestupdatedDate', data);
        },
        updateDtl: function (data) {
            return $http.post('/PettyCashDtlData/Update', data);
        },

        listDtl: function (type, data) {
            return $http.post('/PettyCashDtlData/listData?type=' + type, data);
        }
    }
});

app.directive('focusItems', function () {
    return {
        link: function (scope, element, attrs) {
            scope.$watch(attrs.focusItem, function () {
                
                element[0].focus();
            });
        }
    };
});