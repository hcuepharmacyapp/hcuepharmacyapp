﻿app.controller('pettyCashCreateCtrl', function ($scope, $window, pettyCashModel, pettyCashService, toastr) {

    var pettyCash = pettyCashModel;
    $scope.pettyCash = pettyCash;

    $scope.pettyCashSave = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        pettyCashService.save($scope.pettyCash).then(function (response) {
            $scope.pettyCashForm.$setPristine();
            toastr.success('Created Petty Cash successfully');
            $window.location.reload();
            $scope.pettyCash = {};
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error', 'error');
        });
        $scope.isProcessing = false;
    };

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            $scope.pettyCash = {};
        }
    };
    
});