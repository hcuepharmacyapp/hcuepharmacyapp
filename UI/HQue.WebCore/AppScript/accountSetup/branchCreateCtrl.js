﻿app.controller('registrationCreateCtrl', function ($scope, $filter, $location, $window, toastr, registrationService, accountSetupService, instanceModel, registrationModel, pharmacyTimingModel, pharmacyUploadsModel, $timeout) {
    $scope.errorMessage = {
        "PharmaciesName": false
    };
    $scope.chkEdit = false;
    $scope.tempGSTval = "";
    $scope.minDate = new Date();
    $scope.enableDoneButton = false; //Added by Sarubala on 16-10-17
    $scope.initTiming = function () {
        var timings = [];
        for (var i = 0; i < 7; i++) {
            var object = {
                "id": null,
                "accountId": null,
                "instanceId": null,
                "dayOfWeek": 0,
                "timing": "",
                "morning": { "startTime": "", "endTime": "" },
                "noon": { "startTime": "", "endTime": "" }
            };
            object.dayOfWeek = i + 1;
            timings.push(object);
        }
        return timings;
    };
    $scope.resetTiming = function () {
        $scope.timingsUI = {
            "startcheck": true,
            "endcheck": false,
            "starttime": "",
            "endtime": "",
            "openhours": 1,
            "sun": false,
            "mon": false,
            "tue": false,
            "wed": false,
            "thu": false,
            "fri": false,
            "sat": false
        };
        $scope.disablefields.timings = false;
    };
    $scope.secondaryContactFlag = false;
    $scope.initialize = function () {
        var registration = registrationModel;
        registration.instance = instanceModel;
        var timings = $scope.initTiming();
        //console.log(timings);
        registration.instance.timings = timings;
        //console.log(registration.instance.timings);
        $scope.timingsUI = { "startcheck": true, "endcheck": false };
        registration.instance.images = [];
        for (var i = 0; i < 5; i++)
            registration.instance.images[i] = pharmacyUploadsModel;
        $(".imagePreview1").css("background-image", "");
        $(".imagePreview2").css("background-image", "");
        $(".imagePreview3").css("background-image", "");
        $(".imagePreview4").css("background-image", "");
        $(".imagePreview5").css("background-image", "");
        $scope.registration = registration;
        //$scope.registration.account.instanceCount = 0;
        //console.log($scope.registration);
        $scope.access = { PharmacyInfo: 15, CreateLicense: 16, Payment: 17, PharmacyListing: 18, PharmacyEdit: 19, DrugListing: 20, DrugEdit: 21 };
        $scope.permissions = null;
        $scope.disablefields = { instance: true, timings: false };
        $scope.registerTypeStyle = true;
        $scope.drugLicenseNoStyle = true;
        $scope.drugLicenseExpDateStyle = true;
        $scope.registration.instance.isDoorDelivery = false;
        $scope.resetTiming();
        if ($scope.registration.instance.gstpr == "1") {
            getGstSelectedCustomer();
            getGstSelectedVendor();
        }
        else
        {
            $scope.gstSelectedVendorCount = 0;
            $scope.gstSelectedCustomerCount = 0;
        }
    };
    $scope.initialize();
    //$scope.getPermissions = function () {
    //    registrationService.getPermissions($scope.registration).then(function (response) {
    //        $scope.permissions = response.data;
    //        $scope.disableRegistration();
    //    });
    //};
    $scope.disableRegistration = function () {
        //if (($scope.permissions.indexOf($scope.access.PharmacyInfo) != -1) || ($scope.permissions.indexOf($scope.access.PharmacyEdit) != -1)) {
            $scope.disablefields.instance = false;
        //}
        //else {
        //    $scope.disablefields.instance = true;
        //}
    };
    $scope.disableRegistration();
    //$scope.getPermissions();
    $scope.init = function (instanceId, userName) {

        $scope.getInstanceTypes();

        if (instanceId != "") {
            $scope.chkEdit = true;
            //$.LoadingOverlay("show");
            registrationService.editBranch(instanceId).then(function (response) {
                $scope.registration = response.data;
                $scope.tempGSTval = response.data.instance.gsTinNo;
                if ($scope.registration.instance.isDoorDelivery == true)
                    $scope.registration.instance.DeliveryType = "1";
                else
                    $scope.registration.instance.DeliveryType = "0";
                if ($scope.registration.instance.isHeadOffice == true)
                    $scope.registration.instance.HeadOffice = "1";
                else
                    $scope.registration.instance.HeadOffice = "0";
                if ($scope.registration.instance.isUnionTerritory == true)
                    $scope.registration.instance.isUnionTerritory = "1";
                else
                    $scope.registration.instance.isUnionTerritory = "0";

                if ($scope.registration.instance.instanceTypeId != '') {
                    $scope.registration.instance.instanceTypeId = $scope.registration.instance.instanceTypeId.toString();
                }
                
                if ($scope.registration.instance.gstselect === undefined || $scope.registration.instance.gstselect == false)
                    $scope.registration.instance.gstpr = "0";
                else
                    $scope.registration.instance.gstpr = "1";
                //console.log("images");
                //console.log($scope.registration.instance.images);
                var images = $scope.registration.instance.images;
                if (images[0].fileName != '' && images[0].fileName != undefined) {
                    $(".imagePreview1").css("background-image", "url(" + images[0].fileName + ")");
                    $(".imagePreview1").addClass('current');
                }
                if (images[1].fileName != '' && images[1].fileName != undefined) {
                    $(".imagePreview2").css("background-image", "url(" + images[1].fileName + ")");
                    $(".imagePreview2").addClass('current');
                }
                if (images[2].fileName != '' && images[2].fileName != undefined) {
                    $(".imagePreview3").css("background-image", "url(" + images[2].fileName + ")");
                    $(".imagePreview3").addClass('current');
                }
                if (images[3].fileName != '' && images[3].fileName != undefined) {
                    $(".imagePreview4").css("background-image", "url(" + images[3].fileName + ")");
                    $(".imagePreview4").addClass('current');
                }
                if (images[4].fileName != '' && images[4].fileName != undefined) {
                    $(".imagePreview5").css("background-image", "url(" + $scope.registration.instance.images[4].fileName + ")");
                    $(".imagePreview5").addClass('current');
                }
                $scope.mapInitialize();
                //console.log($scope.registration.instance);
                if (!$scope.registration.instance.isDoorDelivery)
                    $scope.registration.instance.isDoorDelivery = false;
                if ($scope.registration.instance.bdoName == "" || $scope.registration.instance.bdoName == undefined || $scope.registration.instance.bdoName==null)
                    $scope.registration.instance.bdoName = userName;
                if ($scope.registration.instance.secondaryName)
                    $scope.secondaryContactFlag = true;
               
            });
        }
        else {
            $scope.registration.instance.bdoName = userName;
            $scope.registration.instance.DeliveryType = "1";
            $scope.registration.instance.HeadOffice = "0"
            $scope.registration.instance.gstpr = "0";
            if ($scope.registration.instance.isUnionTerritory == null || $scope.registration.instance.isUnionTerritory =="" || $scope.registration.instance.isUnionTerritory == undefined)
            {
                $scope.registration.instance.isUnionTerritory = "0";
            }
           
            initGeolocation();
            getAccountDetails();
        }
    };

    function getAccountDetails() {
        registrationService.getAccountDetail().then(function (response) {
            if (response.data != "") {
                $scope.registration.instance.instanceTypeId = response.data.instanceTypeId.toString();
            }
           
        }, function (error) {
            console.log(error);
        });
    };

    $scope.getInstanceTypes = function () {

        accountSetupService.getInstanceType().then(function (response) {
            $scope.selectedInstanceType = response.data;
            if (response.data == '') {
                $scope.selectedInstanceType = [
                { "displayText": "Only Offline", "domainValue": "1" },
                { "displayText": "Both Online and Offline", "domainValue": "2" },
                { "displayText": "Only Online", "domainValue": "3" }
                ];
            }
        }, function (error) {
            $scope.selectedInstanceType = [
                { "displayText": "Only Offline", "domainValue": "1" },
                { "displayText": "Both Online and Offline", "domainValue": "2" },
                { "displayText": "Only Online", "domainValue": "3" }
            ];
        });
    };

    $scope.branchCreate = function () {
        //console.log(JSON.stringify($scope.registration));
        $scope.enableDoneButton = true; //Added by Sarubala on 16-10-17
        $scope.registration.instance.status = true;
        if ($scope.registration.instance.DeliveryType == "1")
            $scope.registration.instance.isDoorDelivery = true;
        else
            $scope.registration.instance.isDoorDelivery = false;
        if ($scope.registration.instance.HeadOffice == "1")
            $scope.registration.instance.isHeadOffice = true;
        else
            $scope.registration.instance.isHeadOffice = false;
        if ($scope.registration.instance.isUnionTerritory == "1")
            $scope.registration.instance.isUnionTerritory = true;
        else
            $scope.registration.instance.isUnionTerritory = false;

        if ($scope.registration.instance.gstpr == "1")
            $scope.registration.instance.gstSelect = true;
        else
            $scope.registration.instance.gstSelect = false;
        if (!$scope.secondaryContactFlag) {
            $scope.registration.instance.secondaryName = "";
            $scope.registration.instance.secondaryMobile = "";
            $scope.registration.instance.secondaryEmail = "";
        }
        if ($scope.registration.instance.contactMobile == null || $scope.registration.instance.contactMobile == "" || $scope.registration.instance.contactMobile == undefined)
        {
            $scope.registration.instance.contactMobile = 0;
        }

        if (($scope.tempGSTval != $scope.registration.instance.gsTinNo) && ($scope.registration.instance.isEmail == true))
            $scope.registration.instance.isEmail = true;
        else
            $scope.registration.instance.isEmail = false;

        if ($scope.registration.instance.gstSelect == true)
        {
            if ($scope.gstSelectedVendorCount != 0) {
                toastr.info("Only Registered Local Vendors are allowed under Composite scheme!!! Please Change Vendor Location Type/Vendor Type in Profile and then enable composite scheme");
                $scope.enableDoneButton = false;
                return;
            }
            if ($scope.gstSelectedCustomerCount != 0) {
                toastr.info("Only Local Customers are allowed under Composite scheme!!! Please Change Customer Location Type in Profile and then enable composite scheme");
                return;
            }

        }
        $scope.registration.instance.instanceTypeId = parseInt($scope.registration.instance.instanceTypeId);

        registrationService.saveBranch($scope.registration).then(function (response) {
            //console.log(response);
            //console.log(JSON.stringify($scope.SelectedFileForUpload));
            //console.log("success");
            $scope.registration.instance.isEmail = false;
            var model = response.data;
            if ($scope.SelectedFileForUpload.length > 0) {
                $scope.uploadFile(model.instance.accountId, model.instance.id, model.instance.createdBy, model.instance.updatedBy);
            }

            toastr.success("Branch saved successfully. Branch Code is [" + model.instance.code + "]");
            setTimeout(function () {
                window.opener.location.reload();
                window.close();
            },1000);
            //$scope.initialize();
            //window.location.href = "List?type=" + $scope.registration.account.registerType;
        }, function (error) {
            //console.log(JSON.stringify(error));
            toastr.error("Failed");
            $scope.enableDoneButton = false;
            //console.log("fail");
            //$scope.initialize();
        });
    };

    // Added by Gavaskar 01-12-2017 Start

    function getGstSelectedCustomer() {
        registrationService.getGstSelectedCustomer().then(function (response) {
            $scope.gstSelectedCustomerCount = response.data;
        }, function () {
        });
    }
    function getGstSelectedVendor() {
        registrationService.getGstSelectedVendor().then(function (response) {
            $scope.gstSelectedVendorCount = response.data;
        }, function () {
        });
    }

    $scope.selectGstComposite = function () {
        if ($scope.registration.instance.gstpr == "1") {
            $scope.registration.instance.gstSelect = true;
             getGstSelectedCustomer();
            getGstSelectedVendor();
        } else {
            $scope.registration.instance.gstSelect = false;
        }
    };

    // Added by Gavaskar 01-12-2017 End

    $scope.showSecondaryContact = function () {
        $scope.secondaryContactFlag = !$scope.secondaryContactFlag;
    };
    $scope.uploadFile = function (accountid, instanceid, createdBy, updatedBy) {
        //console.log("fileindexes");
        //console.log($scope.SelectedFileNumber);
        registrationService.uploadFile(accountid, instanceid, createdBy, updatedBy, $scope.SelectedFileNumber, $scope.SelectedFileForUpload).then(function (response) {
        }, function () {
            //console.log("fail");
        });
    };
    $scope.ErrorName = false;
    $scope.mobileNotExists = true;
    $scope.SelectAllDays = function () {
        if ($scope.timingsUI.selectAll == true) {
            $scope.timingsUI.sun = true;
            $scope.timingsUI.mon = true;
            $scope.timingsUI.tue = true;
            $scope.timingsUI.wed = true;
            $scope.timingsUI.thu = true;
            $scope.timingsUI.fri = true;
            $scope.timingsUI.sat = true;
        }
        else {
            $scope.timingsUI.sun = false;
            $scope.timingsUI.mon = false;
            $scope.timingsUI.tue = false;
            $scope.timingsUI.wed = false;
            $scope.timingsUI.thu = false;
            $scope.timingsUI.fri = false;
            $scope.timingsUI.sat = false;
        }
    };
    $scope.TimingSubmit = function () {
        var startTime = "";
        var endTime = "";
        var tempTiming = "";
        var timingItem = {};
        if ($scope.timingsUI.openhours == "1") {
            if ($scope.timingsUI.starttime === "" || $scope.timingsUI.endtime === "") {
                $scope.timeErrorMessage = "Enter Timings";
                return false;
            } else {
                $scope.timeErrorMessage = "";
            }
            if ($scope.timingsUI.sun || $scope.timingsUI.mon || $scope.timingsUI.tue || $scope.timingsUI.wed || $scope.timingsUI.thu || $scope.timingsUI.fri || $scope.timingsUI.sat) {
                $scope.timeErrorMessage = "";
            } else {
                $scope.timeErrorMessage = "Select Day";
                return false;
            }
            timingItem = $scope.registration.instance.timings[0];
            //console.log(timingItem);
            if (timingItem.timing === "24/7") {
                //$scope.clearTimings();
                $scope.registration.instance.timings = $scope.initTiming();
            }
            startTime = "";
            endTime = "";
            var startAMPM = "";
            var endAMPM = "";
            var startTemp = "";
            var endTemp = "";
            tempTiming = "";
            startTime = $scope.timingsUI.starttime;
            endTime = $scope.timingsUI.endtime;
            if (startTime != "" && startTime != undefined && endTime != "" && endTime != undefined) {
                //True == AM
                //False == PM
                $scope.timingsUI.startcheck == true ? startAMPM = " AM" : startAMPM = " PM";
                $scope.timingsUI.endcheck == true ? endAMPM = " AM" : endAMPM = " PM";
                startTime += startAMPM;
                endTime += endAMPM;
                tempTiming = startTime + " To " + endTime;
                //console.log(tempTiming);
                if ($scope.timingsUI.sun == true) {
                    $scope.populateTiming(0, tempTiming, startTime, endTime, "sunday");
                }
                if ($scope.timingsUI.mon == true) {
                    $scope.populateTiming(1, tempTiming, startTime, endTime, "monday");
                }
                if ($scope.timingsUI.tue == true) {
                    $scope.populateTiming(2, tempTiming, startTime, endTime, "tuesday");
                }
                if ($scope.timingsUI.wed == true) {
                    $scope.populateTiming(3, tempTiming, startTime, endTime, "wednesday");
                }
                if ($scope.timingsUI.thu == true) {
                    $scope.populateTiming(4, tempTiming, startTime, endTime, "thursday");
                }
                if ($scope.timingsUI.fri == true) {
                    $scope.populateTiming(5, tempTiming, startTime, endTime, "friday");
                }
                if ($scope.timingsUI.sat == true) {
                    $scope.populateTiming(6, tempTiming, startTime, endTime, "saturday");
                }
                $scope.timingsUI.starttime = "";
                $scope.timingsUI.endtime = "";
                $scope.timingsUI.startcheck = true;
                $scope.timingsUI.endcheck = false;
                $scope.timingsUI.selectAll = false;
                $scope.timingsUI.sun = false;
                $scope.timingsUI.mon = false;
                $scope.timingsUI.tue = false;
                $scope.timingsUI.wed = false;
                $scope.timingsUI.thu = false;
                $scope.timingsUI.fri = false;
                $scope.timingsUI.sat = false;
            }
        } else {
            tempTiming = "24/7";
            for (var i = 0; i < 7; i++) {
                $scope.registration.instance.timings[i].timing = tempTiming;
            }
        }
    };
    function convertTo24Hours(string) {
        var hours = Number(string.match(/^(\d+)/)[1]);
        var minutes = Number(string.match(/:(\d+)/)[1]);
        var AMPM = string.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        return sHours + ":" + sMinutes;
    }
    $scope.clearTimings = function () {
        for (var i = 0; i < 7; i++)
            $scope.clearTiming(i);
    };
    $scope.clearTiming = function (timeIndex) {
        $scope.registration.instance.timings[timeIndex].timing = "";
    };
    $scope.populateTiming = function (timeIndex, tempTiming, startTime, endTime, day) {
        //console.log($scope.registration.instance.timings[timeIndex]);
        if ($scope.registration.instance.timings[timeIndex].timing.length < 40) {
            $scope.registration.instance.timings[timeIndex].day = day;
            var timing = $scope.registration.instance.timings[timeIndex].timing;
            //Morning
            if (timing === "") {
                $scope.registration.instance.timings[timeIndex].morning.startTime = convertTo24Hours(startTime);
                $scope.registration.instance.timings[timeIndex].morning.endTime = convertTo24Hours(endTime);
                if (!compareTime($scope.registration.instance.timings[timeIndex].morning)) {
                    $scope.timeErrorMessage = "Inavlid Time For " + day + " ";
                } else {
                    $scope.registration.instance.timings[timeIndex].timing = tempTiming;
                    $scope.timeErrorMessage = "";
                }
            }
                //Noon
            else {
                $scope.registration.instance.timings[timeIndex].noon.startTime = convertTo24Hours(startTime);
                $scope.registration.instance.timings[timeIndex].noon.endTime = convertTo24Hours(endTime);
                var res1 = compareTime($scope.registration.instance.timings[timeIndex].noon);
                var res2 = compareTotalTimes($scope.registration.instance.timings[timeIndex]);
                if (res1 && res2) {
                    $scope.registration.instance.timings[timeIndex].timing = timing + " AND " + tempTiming;
                    $scope.timeErrorMessage = "";
                } else {
                    $scope.timeErrorMessage = "Inavlid Time For " + day + " ";
                }
            }
            //console.log($scope.registration.instance.timings[timeIndex]);
        }
    };
    function compareTime(TimeObject) {
        var result = true;
        var stratTimeHH = TimeObject.startTime.split(":")[0];
        var startTimeMM = TimeObject.startTime.split(":")[1];
        var endTimeHH = TimeObject.endTime.split(":")[0];
        var endTimeMM = TimeObject.endTime.split(":")[1];
        if (stratTimeHH > endTimeHH) {
            result = false;
        }
        if (stratTimeHH == endTimeHH) {
            if (startTimeMM > endTimeMM) {
                result = false;
            }
            if (startTimeMM == endTimeMM) {
                result = false;
            }
        }
        return result;
    }
    function compareTotalTimes(timingsObject) {
        var result = true;
        var morningEndTimeHH = timingsObject.morning.endTime.split(":")[0];
        var morningEndTimeMM = timingsObject.morning.endTime.split(":")[1];
        var noonStartTimeHH = timingsObject.noon.startTime.split(":")[0];
        var noonStartTimeMM = timingsObject.noon.startTime.split(":")[1];
        if (morningEndTimeHH > noonStartTimeHH) {
            result = false;
        }
        if (morningEndTimeHH == noonStartTimeHH) {
            if (morningEndTimeMM > noonStartTimeMM) {
                result = false;
            }
            if (morningEndTimeMM == noonStartTimeMM) {
                result = false;
            }
        }
        return result;
    }
    $scope.select24By7 = function () {
        if ($scope.timingsUI.openhours == "1") {
            $scope.disablefields.timings = false;
        } else {
            $scope.disablefields.timings = true;
            $scope.timingsUI.starttime = "";
            $scope.timingsUI.endtime = "";
            $scope.timingsUI.startcheck = true;
            $scope.timingsUI.endcheck = false;
            $scope.timingsUI.sun = false;
            $scope.timingsUI.mon = false;
            $scope.timingsUI.tue = false;
            $scope.timingsUI.wed = false;
            $scope.timingsUI.thu = false;
            $scope.timingsUI.fri = false;
            $scope.timingsUI.sat = false;
            $scope.timingsUI.selectAll = false;
            $scope.timeErrorMessage = "";
        }
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.dayDiff = function (expireDate) {
        var val = (toDate($scope.registration.instance.drugLicenseExpDate.$viewValue) > $scope.minDate);
        $scope.registration.instance.drugLicenseExpDate.$setValidity("InValidexpDate", val);
        $scope.isFormValid = val;
        $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date($scope.formatString($scope.expireDate));
        var date1 = new Date($scope.formatString($scope.today));
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference >= 30) {
            $scope.registration.instance.drugLicenseExpDate.$setValidity("InValidexpDate", true);
            $scope.highlight = "";
        }
        else {
            $scope.highlight = "highlight";
            $scope.registration.instance.drugLicenseExpDate.$setValidity("InValidexpDate", false);
        }
    };
    // Newly Added Gavaskar 12-10-2016 Start
    $scope.SelectedFileForUpload = ['', '', '', '', ''];
    $scope.SelectedFileNumber = [false, false, false, false, false];
    $scope.uploadImages = function () {
        $("#uploadFile1").on("change", function () {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
                reader.onloadend = function () { // set image data as background of div
                    $(".imagePreview1").css("background-image", "url(" + this.result + ")");
                    $(".imagePreview1").addClass('current');
                };
            }
            $scope.SelectedFileForUpload[0] = files[0];
            $scope.SelectedFileNumber[0] = true;
        });
        $("#uploadFile2").on("change", function () {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
                reader.onloadend = function () { // set image data as background of div
                    $(".imagePreview2").css("background-image", "url(" + this.result + ")");
                    $(".imagePreview2").addClass('current');
                };
            }
            $scope.SelectedFileForUpload[1] = files[0];
            $scope.SelectedFileNumber[1] = true;
        });
        $("#uploadFile3").on("change", function () {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
                reader.onloadend = function () { // set image data as background of div
                    $(".imagePreview3").css("background-image", "url(" + this.result + ")");
                    $(".imagePreview3").addClass('current');
                };
            }
            $scope.SelectedFileForUpload[2] = files[0];
            $scope.SelectedFileNumber[2] = true;
        });
        $("#uploadFile4").on("change", function () {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
                reader.onloadend = function () { // set image data as background of div
                    $(".imagePreview4").css("background-image", "url(" + this.result + ")");
                    $(".imagePreview4").addClass('current');
                };
            }
            $scope.SelectedFileForUpload[3] = files[0];
            $scope.SelectedFileNumber[3] = true;
        });
        $("#uploadFile5").on("change", function () {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
                reader.onloadend = function () { // set image data as background of div
                    $(".imagePreview5").css("background-image", "url(" + this.result + ")");
                    $(".imagePreview5").addClass('current');
                };
            }
            $scope.SelectedFileForUpload[4] = files[0];
            $scope.SelectedFileNumber[4] = true;
        });
    };
    $scope.options = {
        types: ['(cities)'],
        componentRestrictions: { country: 'IN' }
    };
    $scope.address = {
        name: '',
        place: '',
        components: {
            placeId: '',
            streetNumber: '',
            street: '',
            city: '',
            state: '',
            countryCode: '',
            country: '',
            postCode: '',
            district: '',
            location: {
                lat: '',
                long: ''
            }
        }
    };
    function initGeolocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success, fail);
        } else {
            alert("Sorry, your browser does not support geolocation services.");
        }
    }
    function success(position) {
        $scope.registration.instance.lat = position.coords.latitude;
        $scope.registration.instance.lon = position.coords.longitude;
        //console.log(position.coords.latitude);
        //console.log(position.coords.longitude);
        //console.log($scope.registration.instance.lat);
        //console.log($scope.registration.instance.lon);
        $scope.mapInitialize();
    }
    function fail() {
    }
    function initMap() {
        var map = new google.maps.Map(document.getElementById('myMap'), {
            center: {
                lat: -34.397,
                lng: 150.644
            },
            zoom: 6
        });
        var infoWindow = new google.maps.InfoWindow({
            map: map
        });
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                infoWindow.setPosition(pos);
                infoWindow.setContent('Location found.');
                map.setCenter(pos);
            }, function () {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            handleLocationError(false, infoWindow, map.getCenter());
        }
    }
    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
    }
    var map;
    $scope.mapInitialize = function () {
        var marker;
        var myLatlng = new google.maps.LatLng($scope.registration.instance.lat, $scope.registration.instance.lon);
        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();
        var mapOptions = {
            zoom: 18,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("myMap"), mapOptions);
        marker = new google.maps.Marker({
            map: map,
            position: myLatlng,
            draggable: true
        });
        geocoder.geocode({
            'latLng': myLatlng
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#address').val(results[0].formatted_address);
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });
        google.maps.event.addListener(marker, 'dragend', function (e) {
            geocoder.geocode({
                'latLng': marker.getPosition()
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        $scope.registration.instance.lat = marker.getPosition().lat();
                        $scope.registration.instance.lon = marker.getPosition().lng();
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });
    };
    // google.maps.event.addDomListener(window, 'load', initGeolocation);
    $scope.ctrlFn = function () {
        $scope.mapInitialize();
    };
    $scope.changeEndTime = function (text, e) {
        var array = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        if (e.keyCode !== 8) {
            if (text !== "") {
                var finalLetter = text.slice("-1");
                var finalLetterIndex = array.indexOf(finalLetter);
                if (finalLetterIndex < 0) {
                    $scope.timingsUI.endtime = text.replace(finalLetter, "");
                }
                if (text.length === 2) {
                    $scope.timingsUI.endtime += ":";
                }
                if (text.length === 5) {
                    var timeHH = text.split(":")[0];
                    var timeMM = text.split(":")[1];
                    if (timeHH > 12 || timeMM > 59) {
                        $scope.endTimeError = true;
                    } else {
                        $scope.endTimeError = false;
                    }
                }
            }
        } else {
            $scope.endTimeError = false;
        }
    };
    $scope.changeStartTime = function (text, e) {
        var array = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        if (e.keyCode !== 8) {
            if (text !== "") {
                var finalLetter = text.slice("-1");
                var finalLetterIndex = array.indexOf(finalLetter);
                if (finalLetterIndex < 0) {
                    $scope.timingsUI.starttime = text.replace(finalLetter, "");
                }
                if (text.length === 2) {
                    $scope.timingsUI.starttime += ":";
                }
                if (text.length === 5) {
                    var timeHH = text.split(":")[0];
                    var timeMM = text.split(":")[1];
                    if (timeHH > 12 || timeMM > 59) {
                        $scope.startTimeError = true;
                    } else {
                        $scope.startTimeError = false;
                    }
                }
            }
        } else {
            $scope.startTimeError = false;
        }
    };
});
app.directive('numbersOnly', function () {
    return {
        "require": '?ngModel',
        "link": function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
