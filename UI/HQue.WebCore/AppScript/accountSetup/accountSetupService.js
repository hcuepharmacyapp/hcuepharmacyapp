﻿app.factory('accountSetupService', function ($http) {
    return {
        list: function(searchData) {
            return $http.post('/accountSetup/listData', searchData);
        },

        create : function (account) {
            return $http.post('/accountSetup/index', account);
        },

        UpdateGroupDetails: function (group1) {
            return $http.post('/accountSetup/UpdateGroup', group1);
        },

        editGroup: function (id) {
            return $http.get('/accountSetup/EditGroup1?id=' + id);
        },

        getInstanceType: function () {
            return $http.get('/accountSetup/GetInstanceType');
        }
    }
});