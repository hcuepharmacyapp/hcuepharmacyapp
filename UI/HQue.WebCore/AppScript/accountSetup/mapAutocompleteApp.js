﻿var app = angular.module('hcue', ['mapAutocompleteSearchCtrl']);

app.controller('MainCtrl', function ($scope) {

    $scope.options = {
        types: ['(cities)'],
        componentRestrictions: { country: 'FR' }
    };

    $scope.address = {
        name: '',
        place: '',
        components: {
            placeId: '',
            streetNumber: '',
            street: '',
            city: '',
            state: '',
            countryCode: '',
            country: '',
            postCode: '',
            district: '',
            location: {
                lat: '',
                long: ''
            }
        }
    };

});
