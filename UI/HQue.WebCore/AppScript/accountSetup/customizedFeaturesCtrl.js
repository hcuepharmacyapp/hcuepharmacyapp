﻿app.controller('customizedFeaturesCtrl', function ($scope, $filter, toastr, toolService) {

    $scope.selectedBranch = {
        "id": "",
        "name": "",
        "phone": "",
        "accountId": ""
    };
    $scope.selectedAccount = {
        "id": "",
        "name": ""
    };
    $scope.accountList = [];
    $scope.branchList = [];
    $scope.branchListTemp = [];
    $scope.featureList = [];

    $scope.getBranchListByAccount = function () {
        $scope.branchList = $filter("filter")($scope.branchListTemp, {
            accountId: $scope.selectedAccount.id
        }, true);
        $scope.selectedBranch = {
            "id": "",
            "name": "",
            "phone": "",
            "accountId": ""
        };
        $scope.getCustomSettings();
    };

    $scope.getAccountList = function () {
        $.LoadingOverlay("show");
        toolService.getAccountList().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.accountList = response.data;
        }, function () {
            $.LoadingOverlay("hide");
        });
    };    
    
    $scope.getBranchListData = function () {
        $.LoadingOverlay("show");
        toolService.getBranchListData().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.branchList = response.data;
            $scope.branchListTemp = JSON.parse(JSON.stringify($scope.branchList));
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.getCustomSettings = function () {
        if ($scope.selectedAccount == null || $scope.selectedAccount.id == null) {
            return;
        }
        if ($scope.selectedBranch == null || $scope.selectedBranch.id == null) {
            return;
        }
        $.LoadingOverlay("show");
        toolService.getCustomSettings($scope.selectedAccount.id, $scope.selectedBranch.id).then(function (response) {
            $.LoadingOverlay("hide");
            $scope.featureList = response.data;
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.saveSettings = function () {
        if ($scope.selectedAccount.id == null || $scope.selectedAccount.id == "") {
            toastr.info("Please select account and try again.");
            return;
        }
        if ($scope.selectedBranch.id == null || $scope.selectedBranch.id == "") {
            toastr.info("Please select branch and try again.");
            return;
        }
        $.LoadingOverlay("show");
        toolService.saveCustomSettings($scope.featureList).then(function (response) {
            $.LoadingOverlay("hide");
            toastr.success("Settings saved successfully.");
            $scope.featureList = response.data;
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.getAccountList();
    $scope.getBranchListData();
    $scope.getCustomSettings();
});