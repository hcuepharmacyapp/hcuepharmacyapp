﻿var app = angular.module("finYearPopupCtrlModule", ['commonApp', 'toastr', 'angularModalService', 'ui.bootstrap']);

angular.element(document).ready(function () {
    angular.bootstrap(document.getElementById("divFinYearPopup"), ["finYearPopupCtrlModule"]);
});

app.controller("finYearPopupCtrl", function ($scope, ModalService, $filter, toastr, finYearPopupService) {

    function getFinyearAvailable() {
        finYearPopupService.getFinyearAvailable().then(function (response) {

            if (response.data == undefined || response.data == null) {

            } else {
                var finYearEndDate = new Date(response.data.finYearEndDate);

                var today = new Date($filter('date')(new Date(), "yyyy-MM-dd") + " 00:00:000");
                var previous = new Date($filter('date')(new Date(), "yyyy-MM-dd") + " 00:00:000");
                previous.setDate(previous.getDate() + 3);

                if (today > finYearEndDate) {

                    var startDate = $filter('date')(new Date(response.data.finYearStartDate), "yyyy-MM-dd");
                    startDate = startDate.split("-");
                    startDate = new Date(parseInt(startDate[0]) + 1 + "-" + startDate[1] + "-" + startDate[2]);
                    var endDate = $filter('date')(new Date(response.data.finYearEndDate), "yyyy-MM-dd");
                    endDate = endDate.split("-");
                    endDate = new Date(parseInt(endDate[0]) + 1 + "-" + endDate[1] + "-" + endDate[2]);

                    var m = ModalService.showModal({
                        "controller": "myFinYearModalController",
                        "templateUrl": 'myFinYearContent.html',
                        "inputs": {
                            "StartingDate": startDate,
                            "EndDate": endDate,
                            "saveMandatory": (today > finYearEndDate),
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                        });
                    });
                }
            }
        }, function () {
        });
    }

    //getFinyearAvailable();

});

app.controller('myFinYearModalController', function ($scope, close, toastr, finYearPopupService, StartingDate, EndDate, saveMandatory) {

    $scope.saveMandatory = saveMandatory;
    if (StartingDate == "" || StartingDate == undefined || StartingDate == null) {
        $scope.finYearfrom = "";
    } else {
        $scope.finYearfrom = StartingDate;
    }
    if (EndDate == "" || EndDate == undefined || EndDate == null) {
        $scope.finYearto = "";
    } else {
        $scope.finYearto = EndDate;
    }

    $scope.isBillNumberReset = "";
    $scope.EmailId = "";
    $scope.password = "";

    $scope.close = function (result) {
        if (($scope.saveMandatory && result == "Yes") || !$scope.saveMandatory) {
            close(result, 500); // close, but give 500ms for bootstrap to animate
            $(".modal-backdrop").hide();
            $scope.saveMandatory = false;
        }
        else if ($scope.saveMandatory) {
            toastr.info('You must select & save "Bill Series" generation option for this financial year.');
        }
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.saveFinalcialyearStatus = function () {

        $scope.resultObj = {
            "FinYearStartDate": $scope.finYearfrom,
            "FinYearEndDate": $scope.finYearto,
            "IsBillNumberReset": parseInt($scope.isBillNumberReset),
            "EmailId": $scope.EmailId,
            "password": $scope.password
        };

        finYearPopupService.saveFinyearStatus($scope.resultObj).then(function (response) {
            if (response.data.saveFailed) {
                if (response.data.errorMessage != null) {
                    toastr.info(response.data.errorMessage);
                }
                else {
                    toastr.error("Unable to process your request.", "Error");
                }
            }
            else {
                toastr.success("Financial Year Reseted Successfully.");
                $scope.close("Yes");
                window.location.replace('/User/Logout');
            }
        }, function () {
        });
    }

    $(document).on('hide.bs.modal', function (e) {
        if ($scope.saveMandatory) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    });

});

app.factory('finYearPopupService', function ($http) {
    return {
        saveFinyearStatus: function (data) {
            return $http.post('/PaymentData/saveFinyearStatus/', data);
        },
        getFinyearAvailable: function (InvoiceSeries) {
            return $http.get('/PaymentData/getFinyearAvailable');
        }
    };
});