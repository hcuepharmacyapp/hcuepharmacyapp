﻿app.controller('accountSetupSearchCtrl', function ($scope, accountSetupService, accountModel, pagerServcie) {
   
    var account = accountModel;

    $scope.search = account;

    $scope.list = [];
    
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        accountSetupService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    }

    $scope.accountSearch = function () {
        $scope.search.page.pageNo = 1;
        accountSetupService.list($scope.search).then(function (response)
        {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
        }, function () { });
    }

    $scope.accountSearch();

});