﻿app.controller('accountSetupCreateCtrl', function ($scope, accountModel, hQueUserModel, accountSetupService, toastr) {

    var account = accountModel;
    account.user = hQueUserModel;

    $scope.account = account;
    $scope.account.registrationDate = new Date();

    $scope.expiryInvalid = false;

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.open3 = function () {
        $scope.popup3.opened = true;
    };

    $scope.popup3 = {
        opened: false
    };

    $scope.checkLicenseDateValid = function () {
        if (account.registerType == 4) {
            if ($scope.account.licenseStartDate > $scope.account.licenseEndDate)
                $scope.expiryInvalid = true;
            else
                $scope.expiryInvalid = false;
        }
    }

    $scope.selectedInstanceType = null;
    $scope.getInstanceTypes = function () {

        accountSetupService.getInstanceType().then(function (response) {
            $scope.selectedInstanceType = response.data;
            if (response.data == '') {
                $scope.selectedInstanceType = [
                { "displayText": "Only Offline", "domainValue": "1" },
                { "displayText": "Both Online and Offline", "domainValue": "2" },
                { "displayText": "Only Online", "domainValue": "3" }
                ];
            }
        }, function (error) {
            $scope.selectedInstanceType = [
                { "displayText": "Only Offline", "domainValue": "1" },
                { "displayText": "Both Online and Offline", "domainValue": "2" },
                { "displayText": "Only Online", "domainValue": "3" }
            ];
        });
    };
    $scope.getInstanceTypes();

    $scope.changeRegisterType = function () {
        if (account.registerType != 4) {
            $scope.account.licenseStartDate = "";
            $scope.account.licenseEndDate = "";
            $scope.expiryInvalid = false;
        }
    }

    $scope.setInstanceCount = function (instanceType) {
        if (instanceType == "1") {
            $scope.account.instanceCount = 1;
        } else {
            $scope.account.instanceCount = null;
        }
    }

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];


    $scope.isBillNumberReset = "";
    $scope.accountCreate = function () {
        if ($scope.account.name == undefined || $scope.account.name == null) {
            ele = document.getElementById("groupName");
            ele.focus();
            toastr.info('Provide Group Name', 'Info');
            return false;
        }
        if ($scope.account.instanceTypeId == undefined || $scope.account.instanceTypeId == null || $scope.account.instanceTypeId == '') {
            ele = document.getElementById("instanceType");
            ele.focus();
            toastr.info('Provide Instance Type', 'Info');
            return false;
        }
        if ($scope.account.registerType == undefined || $scope.account.registerType == null || $scope.account.registerType == '') {
            ele = document.getElementById("registerType");
            ele.focus();
            toastr.info('Provide Subscription Type', 'Info');
            return false;
        }
        if ($scope.account.instanceCount == undefined || $scope.account.instanceCount == null) {
            ele = document.getElementById("noInstance");
            ele.focus();
            toastr.info('Provide No.of Branches', 'Info');
            return false;
        }
        if ($scope.account.phone == undefined || $scope.account.phone == null) {
            ele = document.getElementById("phone");
            toastr.info('Provide Mobile No.', 'Info');
            ele.focus();
            return false;
        }
        if ($scope.account.user.name == undefined || $scope.account.user.name == null) {
            ele = document.getElementById("name");
            toastr.info('Provide Admin Name', 'Info');
            ele.focus();
            return false;
        }
        if ($scope.account.user.userId == undefined || $scope.account.user.userId == null) {
            ele = document.getElementById("email");
            toastr.info('Provide E-mail', 'Info');
            ele.focus();
            return false;
        }
        $scope.isProcessing = true;
        console.log($scope.isBillNumberReset);
        if ($scope.isBillNumberReset == "" || $scope.isBillNumberReset == undefined || $scope.isBillNumberReset == null) {
        } else {
            $scope.account.isBillNumberReset = $scope.isBillNumberReset;
        }

        var thisyear = new Date().getFullYear();
        var nextyear = new Date().getFullYear() + 1;

        var fromDate = "" + thisyear + "-04-01";
        var toDate = "" + nextyear + "-03-31";

        $scope.account.fromDate = fromDate;
        $scope.account.toDate = toDate;
        $scope.account.instanceTypeId = parseInt($scope.account.instanceTypeId);
        $.LoadingOverlay("show");
        accountSetupService.create($scope.account).then(function (response) {            
            toastr.success('Account Created Successfully');
            $scope.accountSetup.$setPristine();
            $scope.reset();
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            toastr.error('Record already exists, Email / Phone Number', 'error');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        });
    }

    $scope.reset = function () {
        $scope.account.name = "";
        $scope.account.instanceCount = "";
        $scope.account.registrationDate = new Date();
        $scope.account.phone = "";
        $scope.account.address = "";
        $scope.isBillNumberReset = "";
        $scope.account.isBillNumberReset = "";
        $scope.account.fromDate = "";
        $scope.account.toDate = "";

        //changed to include Account Type and Trial Expiry on 11/03/2016
        $scope.account.registerType = "";
        $scope.account.licenseStartDate = "";
        $scope.account.licenseEndDate = "";
        $scope.account.instanceTypeId = "";

        $scope.account.user.name = "";
        $scope.account.user.mobile = "";
        $scope.account.user.userId = "";


    }
});
