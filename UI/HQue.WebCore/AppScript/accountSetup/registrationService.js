﻿app.factory('registrationService', function ($http) {
    return {
        create: function (registrationData) {
            return $http.post('/Registration/Create', registrationData/*, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            }*/
                );
        },
        saveBranch: function (branchData) {
            return $http.post('/Registration/SaveBranch', branchData);
        },
        // Newly added Gavaskar 17-10-2016 Start
        uploadFile: function (accountid, instanceid, createdBy, updatedBy, fileNumber, file) {
            var formData = new FormData();
            formData.append('accountid', accountid);
            formData.append('instanceid', instanceid);
            formData.append('createdBy', createdBy);
            formData.append('updatedBy', updatedBy);
            formData.append('file0Index', fileNumber[0]);
            formData.append('file1Index', fileNumber[1]);
            formData.append('file2Index', fileNumber[2]);
            formData.append('file3Index', fileNumber[3]);
            formData.append('file4Index', fileNumber[4]);
            for (var i = 0; i < file.length; i++) {
                formData.append('file', file[i]);
            }
            return $http.post('/Registration/uploadFile', formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }
           );
        },
        getPermissions: function () {
            return $http.get('/Registration/GetPermissions');
        },
        //Newly Added Gavaskar 04-10-2016 Start
        send: function (mobileno) {
            return $http.get('/Registration/SendOTP?mobileno=' + mobileno);
        },
        confirmOTP: function (OTP) {
            return $http.get('/Registration/ConfirmOTP?OTP=' + OTP);
        },
        editRegisterData: function (accountId, instanceId) {
            //console.log("inside editRegisterData: " + id);
            return $http.post('/Registration/EditRegistration?accountId=' + accountId + '&instanceId=' + instanceId);
        },
        editBranch: function (instanceId) {
            //console.log("inside editRegisterData: " + id);
            return $http.post('/Registration/EditBranch?instanceId=' + instanceId);
        },
        bdoData: function () {
            return $http.get('/Registration/GetBDOs');
        },
        assignBDO: function (instanceId, bdoId) {
            return $http.put('/Registration/AssignBDO?instanceId=' + instanceId + '&bdoId=' + bdoId);
        },
        createLicense: function (registrationData) {
            return $http.post('/Registration/createLicense', registrationData);
        },
        addLicense: function (accountId, addnLicense) {
            return $http.post('/Registration/addLicense?accountId=' + accountId + '&addnLicense=' + addnLicense);
        },
        //Newly Added Gavaskar 07-10-2016 Start
        list: function (searchData) {
            //console.log("gg");
            //console.log(searchData);
            return $http.get('/Registration/AccountSearchList?registrationType=' + searchData);
        },
        uploadFile1: function (files) {
            return $http.get('/Registration/uploadFile?files=' + files);
        },
        // Added by Gavaskar 01-12-2017 Start
        getGstSelectedCustomer: function () {
            return $http.get('/Registration/GetGstSelectedCustomer');
        },
        getGstSelectedVendor: function () {
            return $http.get('/Registration/GetGstSelectedVendor');
        },
        getAccountDetail: function () {
            return $http.get('/Registration/GetAccountDetails');
        }

        // Added by Gavaskar 01-12-2017 End
        /*
        create: function (registration, file) {
            var formData = new FormData();
            formData.append('file', file);
            angular.forEach(registration, function (value, key) {
                if (key === "salesItem") {
                    for (var i = 0; i < sales.salesItem.length; i++) {
                        angular.forEach(sales.salesItem[i], function (value, key) {
                            if (value === "" || value === null || value === undefined)
                                return;
                            formData.append("salesItem[" + i + "]." + key, value);
                        });
                    }
                } else {
                    if (value === "" || value === null || value === undefined)
                        return;
                    formData.append(key, value);
                }
            });
            return $http.post('/sales/index', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });
        },
        list: function (searchData) {
            return $http.post('/sales/listData', searchData);
        },
        salesDetail: function (salesId) {
            return $http.get('/sales/SalesDetail?id=' + salesId);
        },
        createSalesReturn: function (salesReturn) {
            return $http.post('/salesReturn/index', salesReturn);
        },
        salesReturnDetail: function (salesId) {
            return $http.get('/salesReturn/SalesReturnDetail?id=' + salesId);
        },
        returnList: function (searchData) {
            return $http.post('/salesReturn/listData', searchData);
        },
        UpdateCustomer: function (data) {
            return $http.post('/Patient/Update', data);
        },
        UpdateSaleDetails: function (data) {
            return $http.post('/sales/update', data);
        },
        saveDiscountRules: function (data) {
            return $http.post('/sales/SaveDiscountRules', data);
        },
        getDiscountDetail: function () {
            return $http.get('/sales/SalesDiscountDetail');
        },
        editDiscountDetail: function () {
            return $http.get('/sales/editDiscountDetail');
        },
        purchasePrice: function (data) {
            return $http.get('/vendorpurchase/getPurchasePrice?id=' +data);
        },
        */
    }
});
app.service('vsGooglePlaceUtility', function () {
    function isGooglePlace(place) {
        if (!place)
            return false;
        return !!place.place_id;
    }
    function isContainTypes(place, types) {
        var placeTypes,
              placeType,
              type;
        if (!isGooglePlace(place))
            return false;
        placeTypes = place.types;
        for (var i = 0; i < types.length; i++) {
            type = types[i];
            for (var j = 0; j < placeTypes.length; j++) {
                placeType = placeTypes[j];
                if (placeType === type) {
                    return true;
                }
            }
        }
        return false;
    }
    function getAddrComponent(place, componentTemplate) {
        var result;
        if (!isGooglePlace(place))
            return;
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentTemplate[addressType]) {
                result = place.address_components[i][componentTemplate[addressType]];
                return result;
            }
        }
        return;
    }
    function getPlaceId(place) {
        if (!isGooglePlace(place))
            return;
        return place.place_id;
    }
    function getStreetNumber(place) {
        var COMPONENT_TEMPLATE = { street_number: 'long_name' },
            streetNumber = getAddrComponent(place, COMPONENT_TEMPLATE);
        return streetNumber;
    }
    function getStreet(place) {
        var COMPONENT_TEMPLATE = { route: 'long_name' },
              street = getAddrComponent(place, COMPONENT_TEMPLATE);
        return street;
    }
    function getCity(place) {
        var COMPONENT_TEMPLATE = { locality: 'long_name' },
              city = getAddrComponent(place, COMPONENT_TEMPLATE);
        return city;
    }
    function getState(place) {
        var COMPONENT_TEMPLATE = { administrative_area_level_1: 'long_name' },
              state = getAddrComponent(place, COMPONENT_TEMPLATE);
        return state;
    }
    function getDistrict(place) {
        var COMPONENT_TEMPLATE = { administrative_area_level_2: 'short_name' },
            state = getAddrComponent(place, COMPONENT_TEMPLATE);
        return state;
    }
    function getCountryShort(place) {
        var COMPONENT_TEMPLATE = { country: 'short_name' },
              countryShort = getAddrComponent(place, COMPONENT_TEMPLATE);
        return countryShort;
    }
    function getCountry(place) {
        var COMPONENT_TEMPLATE = { country: 'long_name' },
            country = getAddrComponent(place, COMPONENT_TEMPLATE);
        return country;
    }
    function getPostCode(place) {
        var COMPONENT_TEMPLATE = { postal_code: 'long_name' },
            postCode = getAddrComponent(place, COMPONENT_TEMPLATE);
        return postCode;
    }
    function isGeometryExist(place) {
        return angular.isObject(place) && angular.isObject(place.geometry);
    }
    function getLatitude(place) {
        if (!isGeometryExist(place)) return;
        return place.geometry.location.lat();
    }
    function getLongitude(place) {
        if (!isGeometryExist(place)) return;
        return place.geometry.location.lng();
    }
    return {
        isGooglePlace: isGooglePlace,
        isContainTypes: isContainTypes,
        getPlaceId: getPlaceId,
        getStreetNumber: getStreetNumber,
        getStreet: getStreet,
        getCity: getCity,
        getState: getState,
        getCountryShort: getCountryShort,
        getCountry: getCountry,
        getLatitude: getLatitude,
        getLongitude: getLongitude,
        getPostCode: getPostCode,
        getDistrict: getDistrict
    };
});
app.directive('vsGoogleAutocomplete', ['vsGooglePlaceUtility', '$timeout', "$rootScope", function (vsGooglePlaceUtility, $timeout, $rootScope) {
    return {
        restrict: 'A',
        require: ['vsGoogleAutocomplete', 'ngModel'],
        scope: {
            vsGoogleAutocomplete: '=',
            vsPlace: '=?',
            vsPlaceId: '=?',
            vsStreetNumber: '=?',
            vsStreet: '=?',
            vsCity: '=?',
            vsState: '=?',
            vsCountryShort: '=?',
            vsCountry: '=?',
            vsPostCode: '=?',
            vsLatitude: '=?',
            vsLongitude: '=?',
            vsDistrict: '=?',
            someCtrlFn: '&callbackFn'
        },
        controller: ['$scope', '$attrs', function ($scope, $attrs) {
            this.isolatedScope = $scope;
            /**
      * Updates address components associated with scope model.
      * @param {google.maps.places.PlaceResult} place PlaceResult object
      */
            this.updatePlaceComponents = function (place) {
                $scope.vsPlaceId = !!$attrs.vsPlaceId && place ? vsGooglePlaceUtility.getPlaceId(place) : undefined;
                $scope.vsStreetNumber = !!$attrs.vsStreetNumber && place ? vsGooglePlaceUtility.getStreetNumber(place) : undefined;
                $scope.vsStreet = !!$attrs.vsStreet && place ? vsGooglePlaceUtility.getStreet(place) : undefined;
                $scope.vsCity = !!$attrs.vsCity && place ? vsGooglePlaceUtility.getCity(place) : undefined;
                $scope.vsPostCode = !!$attrs.vsPostCode && place ? vsGooglePlaceUtility.getPostCode(place) : undefined;
                $scope.vsState = !!$attrs.vsState && place ? vsGooglePlaceUtility.getState(place) : undefined;
                $scope.vsCountryShort = !!$attrs.vsCountryShort && place ? vsGooglePlaceUtility.getCountryShort(place) : undefined;
                $scope.vsCountry = !!$attrs.vsCountry && place ? vsGooglePlaceUtility.getCountry(place) : undefined;
                $scope.vsLatitude = !!$attrs.vsLatitude && place ? vsGooglePlaceUtility.getLatitude(place) : undefined;
                $scope.vsLongitude = !!$attrs.vsLongitude && place ? vsGooglePlaceUtility.getLongitude(place) : undefined;
                $scope.vsDistrict = !!$attrs.vsDistrict && place ? vsGooglePlaceUtility.getDistrict(place) : undefined;
            };
        }],
        link: function (scope, element, attrs, ctrls) {
            // controllers
            var autocompleteCtrl = ctrls[0],
                modelCtrl = ctrls[1];
            // google.maps.places.Autocomplete instance (support google.maps.places.AutocompleteOptions)
            var autocompleteOptions = scope.vsGoogleAutocomplete || {},
                autocomplete = new google.maps.places.Autocomplete(element[0], autocompleteOptions);
            // google place object
            var place;
            // value for updating view
            var viewValue;
            // updates view value and address components on place_changed google api event
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                place = autocomplete.getPlace();
                viewValue = place.formatted_address || modelCtrl.$viewValue;
                scope.$apply(function () {
                    scope.vsPlace = place;
                    autocompleteCtrl.updatePlaceComponents(place);
                    modelCtrl.$setViewValue(viewValue);
                    modelCtrl.$render();
                });
                scope.someCtrlFn();
            });
            // updates view value on focusout
            element.on('blur', function (event) {
                viewValue = (place && place.formatted_address) ? viewValue : modelCtrl.$viewValue;
                $timeout(function () {
                    scope.$apply(function () {
                        modelCtrl.$setViewValue(viewValue);
                        modelCtrl.$render();
                    });
                });
            });
            google.maps.event.addDomListener(register.address, 'keydown', function (e) {
                if (e.keyCode == 13) {
                    scope.mapInitialize();
                    e.preventDefault();
                }
            });
        }
    };
}]);