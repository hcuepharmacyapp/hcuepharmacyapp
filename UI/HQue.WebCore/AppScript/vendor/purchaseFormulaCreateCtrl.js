﻿app.controller('purchaseFormulaCreateCtrl', function ($scope, $filter, ModalService, vendorService, close, params) {

    $scope.purchaseSettings = {};
    $scope.formulaList = [
        { name: "Discount %", tag: "discount" },
        { name: "Markup %", tag: "markup" },
        { name: "Scheme Discount %", tag: "schemediscount" },
        { name: "Tax %", tag: "tax" }
    ];

    function reset() {
        if (!$scope.purchaseSettings.enableMarkup) {
            $scope.model = {
                formulaName: "",
                formulaJSON: [
                { formulaTag: "markup", operation: "+", order: 1 },
                { formulaTag: "", operation: "", order: 2 },
                { formulaTag: "", operation: "", order: 3 },
                { formulaTag: "", operation: "", order: 4 },
                ]
            };
        }
        else {
            $scope.model = {
                formulaName: "",
                formulaJSON: [
                { formulaTag: "", operation: "", order: 1 },
                { formulaTag: "", operation: "", order: 2 },
                { formulaTag: "", operation: "", order: 3 },
                { formulaTag: "", operation: "", order: 4 },
                ]
            };
        }
    };
    reset();
    
    function validFormula() {
        if ($scope.model.formulaName == "" || $scope.model.formulaName == null || $scope.model.formulaName == undefined) {
            ShowConfirmMsgWindow("Please enter Formula Name and try again.", "formulaName");
            return false;
        }
        for (var i = 0; i < $scope.model.formulaJSON.length; i++) {
            //if ($scope.model.formulaJSON[i].operation == "" || $scope.model.formulaJSON[i].operation == null || $scope.model.formulaJSON[i].operation == undefined) {
            //    ShowConfirmMsgWindow("Please fill formula add/sub type and try again.", "operation" + i);
            //    return false;
            //}
            if ($scope.model.formulaJSON[i].formulaTag == "" || $scope.model.formulaJSON[i].formulaTag == null || $scope.model.formulaJSON[i].formulaTag == undefined) {
                ShowConfirmMsgWindow("Please fill formula fields and try again.", "formulaTag" + i);
                return false;
            }
        }
        if ($scope.model.formulaJSON[0].formulaTag == $scope.model.formulaJSON[1].formulaTag
            || $scope.model.formulaJSON[0].formulaTag == $scope.model.formulaJSON[2].formulaTag
            || $scope.model.formulaJSON[0].formulaTag == $scope.model.formulaJSON[3].formulaTag
            || $scope.model.formulaJSON[1].formulaTag == $scope.model.formulaJSON[2].formulaTag
            || $scope.model.formulaJSON[1].formulaTag == $scope.model.formulaJSON[3].formulaTag
            || $scope.model.formulaJSON[2].formulaTag == $scope.model.formulaJSON[3].formulaTag) {
            ShowConfirmMsgWindow("Formula fields should not be duplicated.");
            return false;
        }
        return true;
    };

    $scope.saveFormula = function () {
        if (typeof ($scope.model.formulaName) == "object") {
            ShowConfirmMsgWindow("Formula edit is not allowed. Please create new one if required with different Formula Name.");
            return;
        }
              
        if (!validFormula())
            return;

        $.LoadingOverlay("show");
        var model = {
            formulaName: $scope.model.formulaName,
            formulaJSON: JSON.stringify($scope.model.formulaJSON)
        };

        vendorService.getPurchaseFormulaList("").then(function (response) {
            $.LoadingOverlay("hide");
            $scope.purchaseFormulaList = response.data;
            var formulaExist = $filter("filter")($scope.purchaseFormulaList, { formulaJSON: JSON.stringify($scope.model.formulaJSON) }, true);
            if (formulaExist != null && formulaExist.length != 0) {
                ShowConfirmMsgWindow("Same formula already exists with [" + formulaExist[0].formulaName + "].");
                return;
            }

            var nameExist = $filter("filter")($scope.purchaseFormulaList, { formulaName: $scope.model.formulaName }, true);
            if (nameExist != null && nameExist.length != 0) {
                ShowConfirmMsgWindow("Formula Name already exists. Please provide different one.");
                return;
            }

            var data = {
                msgTitle: "",
                msg: "Are you sure to save [" + $scope.model.formulaName + "]?",
                showOk: false,
                showYes: true,
                showNo: true,
                showCancel: false,
                focusOnNo: false
            };
            var m = ModalService.showModal({
                "controller": "showConfirmMsgCtrl",
                "templateUrl": 'showConfirmMsgModal',
                "inputs": { "params": [{ "data": data }] },
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result == "Yes") {
                        $.LoadingOverlay("show");
                        vendorService.savePurchaseFormula(model).then(function (response) {
                            $.LoadingOverlay("hide");
                            ShowConfirmMsgWindow('Purchase formula [' + $scope.model.formulaName + '] has been created successfully.', "formulaName", false, "", true);
                        }, function (error) {
                            console.log(error);
                            $.LoadingOverlay("hide");
                            ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
                        });
                    };
                });
            });

        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
        });

    };

    $scope.setFocus = function (tag) {
        var ele = document.getElementById(tag);
        if (ele != null) {
            ele.focus();
        }
    };

    function ShowConfirmMsgWindow(msg, focusTag, showExclamation, width, isReset) {
        var data = {
            msgTitle: "",
            msg: msg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: "",
            showExclamation: showExclamation,
            popupWidth: width
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.setFocus(focusTag);
                if (isReset) {
                    reset();
                }
            });
        });
    };

    function getPurchaseFormulaList() {
        $.LoadingOverlay("show");
        vendorService.getPurchaseFormulaList("").then(function (response) {
            $scope.purchaseFormulaList = response.data;
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
        });
    };
    getPurchaseFormulaList();

    $scope.getFormulaList = function (val) {
        return vendorService.getPurchaseFormulaList(val).then(function (response) {            
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.onSelect = function (obj) {
        if (typeof ($scope.model.formulaName) == "object") {
            $scope.model.formulaJSON = JSON.parse(obj.formulaJSON);
        }
    };

    $scope.onChange = function (vendorFormula) {
        switch (vendorFormula.formulaTag) {
            case "markup": {
                vendorFormula.operation = "+";
                break;
            }
            case "discount": {
                vendorFormula.operation = "-";
                break;
            }
            case "schemediscount": {
                vendorFormula.operation = "-";
                break;
            }
            case "tax": {
                vendorFormula.operation = "+";
                break;
            }
            default: {
                vendorFormula.operation = "";
                break;
            }
        }
    };

    $scope.showExample = false;
    $scope.exampleFormula = {
        basicAmount: 100,
        discount: 10,
        markup: 0,
        tax: 18,
        schemediscount: 5
    };
    $scope.showHideFormula = function () {
        if (!$scope.showExample){
            if (!validFormula()) {
                return;
            }
        }
        $scope.showExample = !$scope.showExample;
    };

    $scope.getPerc = function (formulaTag) {
        switch (formulaTag) {
            case "markup":
                return $scope.getText($scope.exampleFormula.markup, 0);
            case "discount":
                return $scope.getText($scope.exampleFormula.discount, 0);
            case "schemediscount":
                return $scope.getText($scope.exampleFormula.schemediscount, 0);
            case "tax":
                return $scope.getText($scope.exampleFormula.tax, 0);
            default:
                return 0;
        }
    };

    $scope.getText = function (input, retVal) {
        if (input == null || input == undefined || input == "") {
            return retVal;
        }
        else {
            return input;
        }
    };

    $scope.getNetValue = function (amount, formulaObj) {
        amount = parseFloat(amount);
        for (var i = 0; i < $scope.model.formulaJSON.length; i++) {
            if ($scope.model.formulaJSON[i].operation == "+") {
                amount = amount + amount * ($scope.getPerc($scope.model.formulaJSON[i].formulaTag) / 100);
            }
            else if ($scope.model.formulaJSON[i].operation == "-") {
                amount = amount - amount * ($scope.getPerc($scope.model.formulaJSON[i].formulaTag) / 100);
            }
            if (formulaObj.formulaTag == $scope.model.formulaJSON[i].formulaTag)
                return parseFloat(amount).toFixed(2);
        }
        return amount;
    };

    $scope.close = function () {        
        close('No', 100);
        $(".modal-backdrop").hide();
        var ele = document.getElementById("purchaseFormulas");
        if (ele != null) {
            ele.remove();
        }
    };
    
    if (params[0].data != null && params[0].data != undefined && params[0].data != "") {
        $scope.model.formulaName = params[0].data;
        $scope.onSelect(params[0].data);
    };
    if (params[0].formulaValues != null && params[0].formulaValues != undefined && params[0].formulaValues != "") {
        $scope.exampleFormula = params[0].formulaValues;
        $scope.showHideFormula();
    };

    function purchaseSettings() {
        vendorService.purchaseSettings().then(function (response) {
            $scope.purchaseSettings = response.data;
            if (typeof ($scope.model.formulaName) != "object") {
                reset();
            }            
        });
    };
    purchaseSettings();

});