﻿app.controller('vendorCreateCtrl', function ($scope, $filter, vendorModel, vendorService, toastr, ModalService, salesService) {

    var vendor = vendorModel;
    $scope.vendor = vendor;
    $scope.vendor.email = "";
    $scope.vendor.mobile = "";
    //$scope.isState = false; // mandatory removed 
    // Newly Gavaskar
    $scope.vendor.mobile1checked = false;
    $scope.vendor.mobile2checked = false;
    $scope.showEmailRequired = false;
    $scope.showMobileRequired = false;
    $scope.vendor.taxType = ""; //Added by Poongodi on 29/03/2017
    $scope.showSms = true; //Added by Sarubala on 17/11/2017
    $scope.isComposite = false;

    getInstanceValues = function () {
        salesService.getInstanceDetails().then(function (response) {
            console.log(JSON.stringify(response.data))

            if (response.data.gstselect) {
                $scope.isComposite = response.data.gstselect;
                if ($scope.isComposite == true) {
                    $scope.vendor.locationType = 1;
                    $scope.vendor.vendorType = "1";
                    $scope.vendor.paymentType = "Credit";
                }
            }


        }, function (error) {
            console.log(error);
        });
    };
    getInstanceValues();


    $scope.vendorCreate = function () {
        $.LoadingOverlay("show");

        $scope.isProcessing = true;

        if ($scope.vendor.email == null || $scope.vendor.email == undefined) {
            $scope.vendor.email = "";
        }
        if ($scope.vendor.mobile == null || $scope.vendor.mobile == undefined) {
            $scope.vendor.mobile = "";
        }
        $scope.vendor.status = $scope.vendor.status; //($scope.vendor.status == "1") ? 1 : 2;


        if ($scope.vendor.email == "" || $scope.vendor.mobile == "") {
            $.confirm({
                title: 'CAUTION',
                content: "If you provide Vendor Email ID and Mobile Number, Vendor will be able to receive Email/SMS notifications. Do you wish to add?",
                closeIcon: function () {
                    $.LoadingOverlay("hide");
                    $scope.isProcessing = false;
                },
                buttons: {
                    yes: {
                        text: 'yes [y]',
                        btnClass: 'primary-button',
                        keys: ['y'],
                        action: function () {


                            $.LoadingOverlay("hide");
                            if ($scope.vendor.email == "") {
                                document.getElementById("email").focus();

                                $scope.showEmailRequired = true;

                            } else if ($scope.vendor.mobile == "") {
                                document.getElementById("mobile").focus();

                                $scope.showMobileRequired = true;

                            }

                            $scope.isProcessing = false;
                        }
                    },
                    no: {
                        text: 'no [n]',
                        btnClass: 'secondary-button',
                        keys: ['n'],
                        action: function () {



                            createVendor();

                            $scope.isProcessing = false;

                        }
                    }
                }
            });

        } else {


            createVendor();

            $scope.isProcessing = false;
        }



    };

    $scope.isPaymentDone = false;
    $scope.oldVendorBalance = 0;
    $scope.checkPaymentDone = function (customerId) {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        vendorService.checkPaymentDone(customerId).then(function (response) {
            $scope.oldVendorBalance = response.data;

            $.LoadingOverlay("hide");


        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error', 'error');
        });
        $scope.isProcessing = false;
    };

    function createVendor() {
        if ($scope.vendor.drugLicenseNo != null) {
            $scope.vendor.drugLicenseNo = $scope.vendor.drugLicenseNo.toUpperCase();
        }
        if ($scope.selectedState == "") {
            $scope.stateCode = "";
            $scope.vendor.stateId = null;
        }

        vendorService.create($scope.vendor).then(function (response) {
            if (response.data == "") {
                $scope.vendorForm.$setPristine();
                toastr.success('Created vendor successfully');
                $scope.vendor = {};
                $.LoadingOverlay("hide");
                $scope.selectedState = "";
                $scope.stateCode = "";
                $scope.csterror = "";
                $scope.vendor.vendorType = "1";
                $scope.vendor.status = "1";
                $scope.vendor.paymentType = "Credit";
                //Added by nandhini on 10/04/2017
                $scope.vendor.mobile1checked = false;
                $scope.vendor.mobile2checked = false;
                $scope.vendor.vendorPurchaseFormula = $scope.defaultFormula;
                document.getElementById("vendorName").focus(); //Added by Poongodi on 29/03/2017
            } else {
                $.LoadingOverlay("hide");
                toastr.error(response.data, 'error');
            }
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            //toastr.error('Vendor already exists', 'error');
            toastr.error('Something went wrong please try again', 'error');
        });

    };

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            if ($scope.editMode) {
                window.location.assign("/Vendor/list");
            }
            $scope.vendorForm.$setPristine();
            $scope.vendor = {};
            $scope.vendor.vendorPurchaseFormula = $scope.defaultFormula;
            $scope.selectedState = "";
            $scope.stateCode = "";
            $scope.vendor.stateId = null;
            //added by nandhini 29.12.17
            $scope.vendor.vendorType = "1";
            $scope.vendor.status = "1";
            $scope.vendor.paymentType = "Credit";
        }
    };

    // Newly Added Gavaskar 22-10-2016 Start

    $scope.checkMobile1 = function () {
        if ($scope.vendor.mobile1checked == true) {
            if ($scope.vendor.mobile1 == null || $scope.vendor.mobile1 == "") {
                $scope.ErrorMsgMobile1 = true;
            } else {
                $scope.ErrorMsgMobile1 = false;
            }
        } else {

            $scope.ErrorMsgMobile1 = false;
        }
    };
    $scope.vendor.vendorType = "1";
    $scope.vendor.paymentType = "Credit";
    //vendor customer new
    $scope.checkAllPurchasefields = function (ind) {
        if (ind > 1) {
            var companyName = $scope.vendor.name;
            if (companyName != null)
                $scope.vendor.name = companyName.replace(/^\s+|\s+$/gm, '');
            if ($scope.vendor.name == undefined || Object.keys($scope.vendor.name).length == 0) {
                var ele = document.getElementById("vendorName");
                ele.focus();
                return false;
            }
        }
        if (ind > 6) {
            var drugLicenseNotrim = $scope.vendor.drugLicenseNo;
            if (drugLicenseNotrim != null)
                $scope.vendor.drugLicenseNo = drugLicenseNotrim.replace(/^\s+|\s+$/gm, '');

            if ($scope.vendor.drugLicenseNo == undefined || $scope.vendor.drugLicenseNo == null) {
                var ele = document.getElementById("DrugLicenseNo");
                ele.focus();
                return false;
            }
        }
        //added by nandhini
        if (ind > 8) {
            if ($scope.vendor.gsTinNo == undefined || $scope.vendor.gsTinNo == null) {
                var ele = document.getElementById("gsTinNo");
                ele.focus();
                return false;
            }
        }

        if (ind > 19) {

            if ($scope.vendor.locationType == undefined || $scope.vendor.locationType == null) {
                var ele = document.getElementById("locationType");
                ele.focus();
                return false;
            }
        }
        if (ind > 23) {

            if ($scope.vendor.vendorType == undefined || $scope.vendor.vendorType == null) {
                var ele = document.getElementById("vendorType");
                ele.focus();
                return false;
            }
        }
       
        //end
    };

    //end customer new

    $scope.moveNext = function (currentId, nextId) {
        if (currentId == "vendorName" || currentId == "DrugLicenseNo" || currentId == "gsTinNo" || currentId == "locationType" || currentId == "vendorType") {
            var value1 = document.getElementById(currentId).value;
            if (value1 != null && value1 != "") {
                document.getElementById(nextId).focus();
            } else {
                document.getElementById(currentId).focus();
            }
        } else if (currentId == "paymentType") {
            if ($scope.vendor.paymentType == "Credit") {
                //document.getElementById(nextId).focus();
                document.getElementById("creditDays").focus();
            }
            else if ($scope.vendor.paymentType == "Cash") {
                document.getElementById("state").focus();
            }
            else {
                document.getElementById("state").focus();  //addVendor
            }
        } else {
            if ($scope.selectedState == "") {
                $scope.stateCode = "";
                $scope.vendor.stateId = null;
            }
            if ($scope.vendor.status == undefined || $scope.vendor.status == "") {
                //$scope.vendor.status = true;
                document.getElementById("status").focus();
            }
            if (nextId == "locationType" && $scope.isComposite)
                document.getElementById("paymentType").focus();
            else if (nextId == "vendorType" && $scope.isComposite)
                document.getElementById("balamt").focus();
            else if (nextId != undefined)
                document.getElementById(nextId).focus();
            else {
            }
        }
    };

    $scope.submit = function () {
        //if ($scope.selectedState == "")
        //    return;

        if ($scope.vendor.vendorType == null || $scope.vendor.vendorType == undefined || $scope.vendor.vendorType == "") {
            ShowConfirmMsgWindow("Please fill Vendor Type and try again.");
            return;
        }
        if ($scope.vendor.paymentType == null || $scope.vendor.paymentType == undefined || $scope.vendor.paymentType == "") {
            ShowConfirmMsgWindow("Please fill Payment Type and try again.");
            return;
        }

        else if (typeof ($scope.vendor.vendorPurchaseFormula) != "object" || $scope.vendor.vendorPurchaseFormula == null || $scope.vendor.vendorPurchaseFormula == undefined || $scope.vendor.vendorPurchaseFormula == "") {
            ShowConfirmMsgWindow("Please fill Vendor Purchase Formula and try again.");
            return;
        }
        else {
            $scope.vendor.vendorPurchaseFormulaId = $scope.vendor.vendorPurchaseFormula.id;
        }

        if (!$scope.isProcessing && $scope.vendorForm.$valid) {
            if (!$scope.editMode) {
                $scope.vendorCreate();
            }
            else {
                if ($scope.oldVendorBalance != undefined && $scope.oldVendorBalance != null) {
                    if ($scope.oldVendorBalance < parseInt($scope.vendor.balanceAmount) && $scope.vendor.status == 2) {
                        toastr.info("Opening balance changed.. can't change vendor status as Inactive", "");
                        return;
                    }
                }

                $scope.vendorUpdate();
            }
        }
    };

    $scope.checkMobile2 = function () {
        if ($scope.vendor.mobile2Checked == true) {
            if ($scope.vendor.mobile2 == null || $scope.vendor.mobile2 == "") {
                $scope.ErrorMsgMobile2 = true;
            } else {
                $scope.ErrorMsgMobile2 = false;
            }
        } else {
            $scope.ErrorMsgMobile2 = false;
        }
    };
    // Newly Added Gavaskar 22-10-2016 End

    $scope.getStates = function (val) {
        return vendorService.vendorState(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.onStateSelect = function (val) {
        $scope.stateCode = val.stateCode;
        $scope.selectedState = val.stateName;
        $scope.vendor.stateId = val.id;
        document.getElementById("state").focus();
        //$scope.isState = false;
    };

    /*$scope.blurState = function (val) {
        if (val.length == 1) {
            $scope.selectedState = "";
            $scope.match = null;
            //$scope.isState = true;
            document.getElementById("state").focus();
        }
        else
        {
            $scope.isState = false;
        }
    }*/

    $scope.openPurchaseFormula = function (data) {
        var m = ModalService.showModal({
            "controller": "purchaseFormulaCreateCtrl",
            "templateUrl": 'purchaseFormulas',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
            });
        });
    };

    $scope.getFormulaList = function (val) {
        return vendorService.getPurchaseFormulaList(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    function loadDefaultFormula() {
        var name = "Default Formula";
        vendorService.getPurchaseFormulaList(name).then(function (response) {
            $scope.defaultFormula = $filter("filter")(response.data, { formulaName: name }, true)[0];
            if ($scope.defaultFormula == null) {
                var model = {
                    id: null,
                    formulaName: name,
                    formulaJSON: '[{"formulaTag":"markup","operation":"+","order":1},{"formulaTag":"discount","operation":"-","order":2},{"formulaTag":"schemediscount","operation":"-","order":3},{"formulaTag":"tax","operation":"+","order":4}]'
                };
                vendorService.savePurchaseFormula(model).then(function (response) {
                    $scope.defaultFormula = response.data;
                    $scope.vendor.vendorPurchaseFormula = $scope.defaultFormula;
                    $scope.moveNext("vendorName", "vendorName");
                }, function (error) {
                    console.log(error);
                });

            }
            if ($scope.defaultFormula != null && !$scope.editMode) {
                $scope.vendor.vendorPurchaseFormula = $scope.defaultFormula;
                $scope.moveNext("vendorName", "vendorName");
            }
        });
    };

    function ShowConfirmMsgWindow(msg, focusTag) {
        var data = {
            msgTitle: "",
            msg: msg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: "",
            showExclamation: false,
            popupWidth: ""
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.moveNext(focusTag, focusTag);
            });
        });
    };

    $scope.init = function (id) {
        loadDefaultFormula();
        $scope.vendor.status = "1";
        if (id == null || id == undefined || id == "") {
            return;
            $scope.editMode = false;
        }
        else {
            $scope.editMode = true;
        }

        $scope.vendor.id = id;
        $scope.checkPaymentDone($scope.vendor.id);
        $.LoadingOverlay("show");
        $scope.vendor.tempDrugLicenseNo = null;
        vendorService.vendorDetail($scope.vendor)
            .then(function (response) {
                $scope.vendor = response.data;
                $scope.vendor.status = $scope.vendor.status.toString(); //"" + $scope.vendor.status + "";
                //accountId = $scope.vendor.accountId; 
                $scope.vendor.tempDrugLicenseNo = ($scope.vendor.drugLicenseNo == undefined) ? '' : $scope.vendor.drugLicenseNo.toUpperCase();
                $scope.vendor.drugLicenseNo = ($scope.vendor.drugLicenseNo == undefined) ? '' : $scope.vendor.drugLicenseNo.toUpperCase();
                if ($scope.vendor.stateRef != undefined) {
                    $scope.stateCode = $scope.vendor.stateRef.stateCode;
                    $scope.selectedState = $scope.vendor.stateRef.stateName;
                }
                $scope.vendor.vendorType = ($scope.vendor.vendorType == undefined ? null : ($scope.vendor.vendorType + "")); //Value not binding with model since if it is int.
                // $scope.oldVendorBalance = $scope.vendor.balanceAmount;
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                toastr.error("", "Error");
                $.LoadingOverlay("hide");
            });
    };

    $scope.vendorUpdate = function () {
        if (parseFloat($scope.oldVendorBalance) > 0) {
            if (($scope.vendor.balanceAmount) == '') {
                toastr.info("Opening Balance Cannot be less than: " + $scope.oldVendorBalance.toString());
                $scope.moveNext('vendorType', 'balamt');
                $.LoadingOverlay("hide");
                return;
            }
        }

        if (parseFloat($scope.vendor.balanceAmount) < parseFloat($scope.oldVendorBalance)) {
            toastr.info("Opening Balance Cannot be less than: " + $scope.oldVendorBalance.toString());
            $scope.moveNext('vendorType', 'balamt');
            $.LoadingOverlay("hide");
            return;
        }


        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if ($scope.selectedState == "") {
            $scope.stateCode = "";
            $scope.vendor.stateId = null;
        }
        vendorService.update($scope.vendor)
            .then(function (response) {
                if (response.data != "") {
                    $scope.vendorForm.$setPristine();
                    window.location.assign("/Vendor/list");
                    toastr.success('Updated vendor successfully');
                }
                else
                    toastr.error("Drug License No already exists", "Error");
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                toastr.error("", "Error");
                $.LoadingOverlay("hide");
            });

        $scope.isProcessing = false;
    };

    //Added by Sarubala on 17-11-17 - start
    function getVendorCreateSmsSettings() {
        vendorService.GetVendorCreateSmsSetting().then(function (response) {
            $scope.showSms = response.data;

        }, function (error) {
            console.log(error);
        });
    };

    getVendorCreateSmsSettings();

    //Added by Sarubala on 17-11-17 - end

    $scope.PaymentStatus = function (val) {

        vendorService.paymentStatus($scope.vendor.id)
            .then(function (response) {
                var flag = response.data;

                if (flag) {
                    $scope.vendor.status = val;
                }
                else if ((!flag) && (parseInt(val) != 2)) {

                }
                else {
                    $scope.vendor.status = '1';
                    toastr.info("Payment is pending.... Please use Hide instead of InActive", ""); //toastr.warning("Payment is pending....", "Warning");  
                }

            }, function (error) {
                toastr.error("", "Error");
                $.LoadingOverlay("hide");
            });

    }
});