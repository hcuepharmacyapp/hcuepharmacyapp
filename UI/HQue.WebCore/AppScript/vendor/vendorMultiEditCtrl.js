﻿app.controller('vendorMultiEditCtrl', function ($scope, toastr,vendorService,vendorModel, pagerServcie, $filter) {

    var vendor = vendorModel;

    $scope.search = vendor;

    $scope.list = [];

    $scope.vendorStatus = 2;
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination
    $scope.isUpdate = false;
    function pageSearch() {
        $.LoadingOverlay("show");
        if ($scope.isUpdate)
            $scope.updateAllVendor($scope.list);

        vendorService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $scope.selectedIndex = -1;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.vendorSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        if ($scope.isUpdate)
            $scope.updateAllVendor($scope.list);

        vendorService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.vendorSearch();

    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) { //vendorService.vendorData().then(function (response) {
            $scope.vendorList = response.data;
        }, function () { });
    }
    $scope.vendor();

    // Bulk Update
    $scope.updateAllVendor = function (list) {

        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        vendorService.bulkUpdate(list).then(function (response) {
            $scope.vendorSearch();
            toastr.success('Vendor updated successfully');
            $scope.selectedIndex = -1;
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            //toastr.error('Vendor already exist', 'Error');
            $scope.isProcessing = false;
        });
    }

    $scope.PaymentStatus = function (val, vendor) {

        vendorService.paymentStatus(vendor.id)
            .then(function (response) {
                var flag = response.data;

                if (flag) {
                    vendor.status = val;
                }
                else if ((!flag) && (parseInt(val) != 2)) {

                }
                else {
                    vendor.status = '1';
                    toastr.info("Payment is pending.... Please use Hide instead of InActive", ""); //toastr.warning("Payment is pending....", "Warning");  
                }

            }, function (error) {
                toastr.error("", "Error");
                $.LoadingOverlay("hide");
            });

    }

    $scope.reset = function () {
        window.location.assign("/Vendor/VendorMultiEdit");
    };


});




app.directive("contenteditable", function () {
    return {
        restrict: "A",
        require: "ngModel",

        link: function (scope, element, attrs, ngModel) {

            function read() {
                ngModel.$setViewValue(element.html());
            }
            ngModel.$render = function () {
                element.html(ngModel.$viewValue || "");
            };
            element.bind("blur keyup change", function () {
                scope.$apply(read);
                //alert(scope);
            });

            element.css('outline', 'none');
            element.bind("blur", function (event) {
                element[0].blur();
              
                //if (element[0].innerText != '' && element[0].innerText != null) {

                    if (attrs.ngModel == "vendor.drugLicenseNo") {
                        if (element[0].innerText.length >= 50 || event.which == 38 || event.which == 60 || event.which == 62) {
                            event.preventDefault();
                            return false;

                        }
                    }
                    else if (attrs.ngModel == "vendor.gsTinNo") {
                        if (element[0].innerText.length >= 15 || event.which == 38 || event.which == 60 || event.which == 62) {
                        //if (event.which == 38) {
                            event.preventDefault();
                            return false;

                        }
                    }
                //}
               
                event.preventDefault();
            });

            element.bind("keypress", function (event) {
                console.log(event.which);

                //if (element[0].innerText != '' && element[0].innerText != null) {

                    if (attrs.ngModel == "vendor.drugLicenseNo") {
                        if (element[0].innerText.length >= 50 || event.which == 38 || event.which == 60 || event.which == 62) {
                            event.preventDefault();
                            return false;

                        }
                    }
                    else if (attrs.ngModel == "vendor.gsTinNo") {
                        if (element[0].innerText.length >= 15 || event.which == 38 || event.which == 60 || event.which == 62) {
                        //if (event.which == 38) {
                            event.preventDefault();
                            return false;

                        }
                    }
                //}
               
                if (event.which === 13) {
                    event.preventDefault();
                    element.next().focus();
                }
               
            });
        }
    };
});
