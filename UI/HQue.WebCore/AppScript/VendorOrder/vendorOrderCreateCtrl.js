﻿app.controller('VendorOrderCreateCtrl', function ($scope,vendorOrderModel, productService, vendorOrderService, vendorService, toastr, $rootScope, $filter, pagerServcie, ModalService, salesService) {
    //By Vendor Section
    $scope.draftOrderCount = 0;
    $scope.draftOrderLoaded = false;
    $scope.vendorclass = "tabSelected";
    $scope.salesclass = "tabNormal";
    // Added by Settu on 06-06-2017 for minmax reorder
    $scope.minMaxReorderClass = "tabNormal";
    $scope.isProcessing = true;
    $scope.ByVendorSection = true;
    $scope.BySalesSection = false;
    $scope.ByMinMaxReorderSection = false;
    $scope.message = "No records found";
    $scope.Filtermessage = "";
    $scope.IsOffline = false;
    $scope.isOnlineEnabled = false;
    $scope.GSTEnabled = true;
    $scope.headOfficeVendorId = null;
    var vendorOrder = vendorOrderModel;
    $scope.search = vendorOrder;
    $scope.freeQty = 0;
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    function pageSearch() {
        $scope.SalesVendorOrderTemp = [];
        for (var i = ((pagerServcie.page.pageNo - 1) * pagerServcie.page.pageSize) ; i < (pagerServcie.page.pageNo * pagerServcie.page.pageSize) && i < $scope.SalesVendorOrder.length; i++) {
            $scope.SalesVendorOrderTemp.push($scope.SalesVendorOrder[i]);
        }
    }
    //pagination
    $scope.loadAllProducts = false;

    // Vendor name, Show zero stock & Pending po qty filter added by Settu 
    var customOrder = document.getElementById("hdnCustomOrder").value;
    $scope.showByVendor = function () {
        $scope.vendorclass = "tabSelected";
        $scope.salesclass = "tabNormal";
        $scope.minMaxReorderClass = "tabNormal";
        $scope.ByVendorSection = true;
        $scope.BySalesSection = false;
        $scope.ByMinMaxReorderSection = false;
        resetFilterData();
    };
    $scope.PopupAddNewProduct = function () {
        var m = ModalService.showModal({
            "controller": "productCreateCtrl",
            "templateUrl": "createProduct",
            "inputs": {
                "mode": "Create",
                "poproductname": '',
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();

        });
    };

    $scope.getHeadOfficeVendorId = function () {
        vendorOrderService.getHeadVendorId().then(function (response) {
            $scope.headOfficeVendorId = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getHeadOfficeVendorId();

    $scope.showBySales = function () {
        $scope.salesclass = "tabSelected";
        $scope.vendorclass = "tabNormal";
        $scope.minMaxReorderClass = "tabNormal";
        $scope.BySalesSection = true;
        $scope.ByVendorSection = false;
        $scope.ByMinMaxReorderSection = false;
        $scope.SalesVendorOrderItems.$setUntouched();
        $scope.SalesVendorOrder = [];
        $scope.customSalesVendorOrder = [];
        $scope.search.reOrderFactor = null;
        $scope.fromDate = $filter('date')(new Date($scope.fromDateTemp), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');   
        resetFilterData();
        $scope.message = "No records found";
        $scope.Filtermessage = "Try using a different criteria to view better results";
    };
    $scope.showByMinMaxReorder = function () {
        $scope.minMaxReorderClass = "tabSelected";
        $scope.vendorclass = "tabNormal";
        $scope.salesclass = "tabNormal";
        $scope.ByMinMaxReorderSection = true;
        $scope.BySalesSection = false;
        $scope.ByVendorSection = false;
        $scope.SalesVendorOrderItems.$setUntouched();
        $scope.SalesVendorOrder = [];
        $scope.customSalesVendorOrder = [];
        $scope.search.reOrderFactor = null;
        $scope.fromDate = $filter('date')(new Date($scope.fromDateTemp), 'yyyy-MM-dd');
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');   
        resetFilterData();
        $scope.message = "No records found";
        $scope.Filtermessage = "Please update Min & Max Reorder in product master to view the reorder details";
        $.LoadingOverlay("show");
        $scope.isLoading = true;
        vendorOrderService.ReorderByMinMax().then(function (resp) {
            $scope.isLoading = false;
            loadSalesVendorOrder(resp.data);
            $.LoadingOverlay("hide");
        },
        function (error) {
            $scope.isLoading = false;
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.save = {
        "vendorId": "",
        "quantity": "",
        "selectedVendor": null,
        "selectedProduct": null,
        "purchasePrice": null,
        "vat": null,
        "igst": null,
        "cgst": null,
        "sgst": null,
        "gstTotal":null,
        "packageSize": null,
        "packageQty": null,
        "freeQty":null
    };
    $scope.vendorOrder = [];
    $scope.vendorList = [];
    $scope.productList = [];
    //Order generation done by Transfer - Settu
    $scope.salesType = true;
    $scope.transferType = false;
    $scope.byMissedOrder = false;
    $scope.byIndentOrder = false;  //Order generation by indent qty from head office  
    var orderType = 0;
    $scope.vendorStatus = 1;

    //The below changes to update ToInstanceId in order for stock transfer offline sync by Settu
    $scope.headOfficeVendor = null;
    $scope.list = [];
    $scope.searchType = null;
    $scope.nonExpiredQuantity = 0;
    $scope.ExpiredQuantity = 0;
    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) { //vendorService.vendorData().then(function (response) {
            response.data.name = response.data.name;
            $scope.vendorList = response.data;
            if (localStorage.getItem("OrderList") !== null) {
                var DataFromOrderReport = JSON.parse(localStorage.getItem("OrderList"));
                pagerServcie.init(response.data.noOfRows, pageSearch);

                LoadDataDataFromOrderReport(DataFromOrderReport.OrderList);
            }
            if (window.localStorage.getItem("OrderBasedSales") != null) {
                $scope.orderBasedSales = JSON.parse(window.localStorage.getItem("OrderBasedSales"));
                pagerServcie.init(response.data.noOfRows, pageSearch);

                loadDataFromOrderBasedSales($scope.orderBasedSales);
            }
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };

    $scope.Math = window.Math;
    
    $scope.init = function () {

        $scope.vendor();
        $scope.getDateSettings();
        $scope.fromDate = $scope.filter.fromdate;
        $scope.toDate = $scope.filter.todate;
        resetFilterData();
        vendorOrderService.getInstanceData().then(function (response) {
            $scope.instance = response.data;
        }, function () {
        });
    }
 
    getOfflineStatus = function () {
        salesService.getOfflineStatus().then(function (response) {
            if (response.data) {
                $scope.IsOffline = true;
            }
        });
    };
    getOfflineStatus();

    //Added by Sarubala on 18-05-18
    getOnlineEnabledStatus = function () {
        salesService.getOnlineEnableStatus().then(function (response) {
            if (response.data) {
                $scope.isOnlineEnabled = true;
            }
        }, function (error) {
            console.log(error);
        });
    };

    getOnlineEnabledStatus();

    function applyGST(vendor, vendorOrderItem, igst, cgst, sgst) {
        if (vendor.locationType == 1 || vendor.locationType == undefined || vendor.locationType == null) {
            vendorOrderItem.cgst = cgst;
            vendorOrderItem.sgst = sgst;
            vendorOrderItem.igst = 0;
        }
        else if (vendor.locationType == 2) {
            vendorOrderItem.igst = igst;
            vendorOrderItem.cgst = 0;
            vendorOrderItem.sgst = 0;
        }
        else {
            vendorOrderItem.igst = 0;
            vendorOrderItem.cgst = 0;
            vendorOrderItem.sgst = 0;
        }
    }

    $scope.addOrder = function () {
        $scope.vendorOrder.IsEdit = false;
        $scope.save.vendorId = $scope.save.selectedVendor.id;
        var selecteditem = {
            "VendorId": $scope.save.selectedVendor.id,
            "selectedVendor": $scope.save.selectedVendor,
            "VendorOrderItem": [],
            "isContainVendor": true
        };
        if ($scope.save.selectedProduct.accountId === null || $scope.save.selectedProduct.accountId === undefined || $scope.save.selectedProduct.accountId === "") {
            $scope.save.selectedProduct.productMasterID = $scope.save.selectedProduct.id;
            $scope.save.selectedProduct.productName = $scope.save.selectedProduct.name;
            $scope.save.selectedProduct.manufacturer = $scope.save.selectedProduct.manufacturer;
            $scope.save.selectedProduct.category = $scope.save.selectedProduct.category;
            $scope.save.selectedProduct.schedule = $scope.save.selectedProduct.schedule;
            $scope.save.selectedProduct.genericName = $scope.save.selectedProduct.genericName;
            $scope.save.selectedProduct.packing = $scope.save.selectedProduct.packing;
        } else {
            $scope.save.selectedProduct.productMasterID = null;
        }


        $scope.save.selectedProduct.disableDone = false;

        selecteditem.VendorOrderItem.push($scope.save.selectedProduct);
        selecteditem.VendorOrderItem[0].quantity = $scope.save.packageSize * $scope.save.packageQty;
        selecteditem.VendorOrderItem[0].productId = $scope.save.selectedProduct.id;

        selecteditem.VendorOrderItem[0].freeQty = $scope.freeQty;

        selecteditem.VendorOrderItem[0].purchasePrice = $scope.save.purchasePrice;

        selecteditem.VendorOrderItem[0].vat = $scope.save.vat;

        applyGST($scope.save.selectedVendor, selecteditem.VendorOrderItem[0], $scope.save.igst, $scope.save.cgst, $scope.save.sgst);

        selecteditem.VendorOrderItem[0].gstTotal = $scope.save.gstTotal;
        selecteditem.VendorOrderItem[0].packageSize = $scope.save.packageSize;
        selecteditem.VendorOrderItem[0].packageQty = $scope.save.packageQty;
        selecteditem.VendorOrderItem[0].isEdit = false;
        selecteditem = $scope.checkTinNoForStockTransfer(selecteditem);
        if ($scope.vendorOrder.length === 0) {
            $scope.vendorOrder.push(selecteditem);
        } else {
            var vendorcount = $filter("filter")($scope.vendorOrder, { "VendorId": selecteditem.VendorId }).length;
            if (vendorcount === 0) {
                $scope.vendorOrder.push(selecteditem);
            } else {
                var vendorindex = GetIndexofItem($scope.vendorOrder, selecteditem.VendorId);
                if (vendorindex === -1) {
                    $scope.vendorOrder.push(selecteditem);
                } else {
                    var productcount = $filter("filter")($scope.vendorOrder[vendorindex].VendorOrderItem, { "id": selecteditem.VendorOrderItem[0].id }).length;
                    if (productcount === 0) {
                        $scope.vendorOrder[vendorindex].VendorOrderItem.push(selecteditem.VendorOrderItem[0]);
                    } else {
                        alert("order already added");
                    }
                }
            }
        }

        var vendor = $scope.save.selectedVendor;
        $scope.save = {
            "vendorId": "",
            "quantity": "",
            "selectedVendor": vendor,
            "selectedProduct": null,
            "purchasePrice": null,
            "vat": null,
            "igst": null,
            "cgst": null,
            "sgst": null,
            "gstTotal":null,
            "packageSize": null,
            "packageQty": null,
        };
        $scope.vendorOrderItems.$setUntouched();
        $scope.nonExpiredQuantity = 0;
        $scope.ExpiredQuantity = 0;
        $scope.isProcessing = false;
        $scope.freeQty = 0;
        window.setTimeout(function () {
            for (var i = 0; i < $scope.vendorOrder.length; i++) {
                $('#chip-wrapper' + i).show();
                $('#chip-btn' + i).text('-');
            }
        }, 0);
        var qty = document.getElementById("drugName");
        qty.focus();
    };
    function GetIndexofItem(vendorarray, vendorid) {
        for (var i = 0; i < vendorarray.length; i++) {
            if (vendorarray[i].VendorId === vendorid) {
                return i;
            }
        }
        return -1;
    }
    $scope.toggleProductDetail = function (row) {
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    };
    $scope.removeOrder = function (parentindex, childindex) {
        if (!confirm("Are you sure, Do you want to delete ? "))
            return;
        $scope.vendorOrder[parentindex].VendorOrderItem.splice(childindex, 1);

        if ($scope.vendorOrder[parentindex].VendorOrderItem.length === 0) {
            $scope.vendorOrder.splice(parentindex, 1);
        }
        checkEditforAllProducts();
    };
    $scope.editOrder = function (parentindex, childindex) {
        $scope.vendorOrder[parentindex].VendorOrderItem[childindex].isEdit = true;
        checkEditforAllProducts();
    };
    $scope.checkItemQuantity = function (parentIndex, Index, data) {
        CompleteEdit(parentIndex, Index, data, 'change');
    };
    function CompleteEdit(parentIndex, Index, data, event) {
        if (data == "") {
            $scope.vendorOrder[parentIndex].VendorOrderItem[Index].disableDone = true;
        } else {
            if (parseInt(data) == 0) {
                $scope.vendorOrder[parentIndex].VendorOrderItem[Index].disableDone = true;
            } else {
                $scope.vendorOrder[parentIndex].VendorOrderItem[Index].disableDone = false;

                if (event == "click") {
                    $scope.vendorOrder[parentIndex].VendorOrderItem[Index].isEdit = false;
                }
            }
        }
    }
    $scope.enableDone = function (event, parentIndex, Index, data) {
        var key = event.keyCode || event.which;
        if (key === 13) {
            CompleteEdit(parentIndex, Index, data, 'click');
        }
    };
    $scope.doneOrder = function (parentindex, childindex) {
        $scope.vendorOrder[parentindex].VendorOrderItem[childindex].isEdit = false;
        checkEditforAllProducts();
    };
    $scope.removeVendor = function (index) {
        if (!confirm("Are you sure, Do you want to delete ? "))
            return;
        $scope.vendorOrder.splice(index, 1);
    };
    $scope.saveOrder = function () {
        var status = checkQuantity();
        if (status) {
            alert("0 Quantity Not Allowed");
            return false;
        } else {
            $.LoadingOverlay("show");
            var index = -1;
          
            for (var i = 0; i < $scope.vendorOrder.length; i++) {
                for (var j = 0; j < $scope.vendorOrder[i].VendorOrderItem.length; j++) {
               
                    $scope.vendorOrder[i].VendorOrderItem[j].OrderItemSno = j;
                }
            }
            vendorOrderService.create($scope.vendorOrder).then(function (response) {
                $scope.list = response.data;
                toastr.success('Order Saved Successfully');
                $scope.save = {
                    "vendorId": "",
                    "quantity": "",
                    "selectedVendor": null,
                    "selectedProduct": null,
                    "purchasePrice": null,
                    "vat": null,
                    "igst": null,
                    "cgst": null,
                    "sgst": null,
                    "gstTotal": null,
                    "packageSize": null,
                    "packageQty": null,
                    "freeQty":0
                };
                $scope.vendorOrder = [];
                $scope.isProcessing = true;
                $scope.vendorOrderItems.$setUntouched();
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
            });
        }
    };
    function checkQuantity() {
        var status = false;
        for (var i = 0; i < $scope.vendorOrder.length; i++) {
            status = false;
            for (var j = 0; j < $scope.vendorOrder[i].VendorOrderItem.length; j++) {
                if (parseInt($scope.vendorOrder[i].VendorOrderItem[j].quantity) == 0) {
                    status = true;
                    break;
                }
            }
            if (status) {
                break;
            }
        }
        return status;
    }
    $scope.getProducts = function (val) {

        return productService.nonHiddenProductList(val).then(function (response) {
           
            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }

            return output.map(function (item) {
                return item;
            });
        });

    };

   
    $scope.PreviousOrders = [];
    $scope.onProductSelect = function (productname, productId,manufacturer) {

        vendorOrderService.getQuantityForProduct(productId)
    .then(function (response) {
        $scope.nonExpiredQuantity = response.data.stock;
        $scope.ExpiredQuantity = response.data.expiredstock;
        $scope.save.purchasePrice = $filter("number")(response.data.purchasePrice, 2);
        $scope.save.vat = response.data.vat;
        $scope.save.igst = response.data.igst;
        $scope.save.cgst = response.data.cgst;
        $scope.save.sgst = response.data.sgst;
        $scope.save.gstTotal = response.data.gstTotal;
        $scope.save.packageSize = response.data.packageSize;
        if ($scope.save.packageSize == null || $scope.save.packageSize == undefined || $scope.save.packageSize == "") {
            var qty = document.getElementById("txtpackageSize");
            qty.focus();
        }
        else {
            var qty = document.getElementById("txtPackageQty");
            qty.focus();
        }
        if ($scope.save.selectedVendor == null) {
            $scope.ProductDetailsPopup(productId);
        }

    });



    };
    $scope.ProductDetailsPopup = function (productId) {
        vendorOrderService.getPreviousOrders(productId).then(function (response) {
            $scope.PreviousOrders = response.data;
            if ($scope.PreviousOrders.length !== 0) {
                var m = ModalService.showModal({
                    "controller": "productDetailPopUpCtrl",
                    "templateUrl": "productDetailsPopUp",
                    "inputs": {
                        "PreviousOrders": $scope.PreviousOrders
                    }
                }).then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                        $scope.message = "You said " + result;
                    });
                });
            }
        }, function (error) {
            console.log(error);
        });

    };
    $rootScope.$on('vendorid', function (event, data) {
        if (data !== null) {
            $scope.vendormodel = $filter("filter")($scope.vendorList, { "id": data });
            $scope.save.selectedVendor = $scope.vendormodel[0];
            var qty = document.getElementById("txtpackageSize");
            qty.focus();
        }
    });
    $scope.cancel = function () {

        if (!confirm("Are you sure, Do you want to cancel ? "))
            return;

        $scope.save = {
            "vendorId": "",
            "quantity": "",
            "selectedVendor": null,
            "selectedProduct": null,
            "purchasePrice": null,
            "vat": null,
            "igst": null,
            "cgst": null,
            "sgst": null,
            "gstTotal":null,
            "packageSize": null,
            "packageQty": null,
            "freeQty":null
        };
        $scope.nonExpiredQuantity = 0;
        $scope.ExpiredQuantity = 0;
        $scope.vendorOrder = [];
        $scope.vendorOrderItems.$setUntouched();
        resetFilterData();
    };
    $scope.keyEnter = function (event, e, quantity) {
        if (quantity == undefined) {
            return false;
        } else {
            if (parseInt(quantity) == 0) {
                return false;
            }
        }
        var ele = document.getElementById(e);
        var key = event.keyCode || event.which;
        if (key === 13) // Enter key
        {

            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) {
                $scope.addOrder();

            }

        }
    };
    function LoadDataDataFromOrderReport(DataFromOrderReport) {
        for (var i = 0; i < DataFromOrderReport.length; i++) {
            var selectedProduct = {
                "productId": DataFromOrderReport[i].product.id,
                "manufacturer": DataFromOrderReport[i].product.manufacturer,
                "quantity": DataFromOrderReport[i].reOrderQty,
                "name": DataFromOrderReport[i].product.name,
                "purchasePrice": DataFromOrderReport[i].purchasePrice,
                "vat": DataFromOrderReport[i].vat,
                "packageSize": 0,
                "packageQty": 0,
                "freeQty": 0,
                "isEdit": true,
                "disableDone": true

            };
            var selecteditem = {
                "VendorId": DataFromOrderReport[i].vendor.id,
                "selectedVendor": $filter("filter")($scope.vendorList, { "id": DataFromOrderReport[i].vendor.id })[0],
                "VendorOrderItem": [],
                "isContainVendor": true
            };
            if (selecteditem.selectedVendor === undefined) {
                selecteditem.isContainVendor = false;
            }
            selecteditem.VendorOrderItem.push(selectedProduct);
            selecteditem = $scope.checkTinNoForStockTransfer(selecteditem);
            $scope.vendorOrder.push(selecteditem);
        }
        window.setTimeout(function () {
            for (var i = 0; i < $scope.vendorOrder.length; i++) {
                $scope.toggleProductDetail(i);
            }
        }, 0);
        localStorage.removeItem("OrderList");
    }
    function loadDataFromOrderBasedSales(orderBasedSales) {
        for (var i = 0; i < orderBasedSales.length; i++) {
            var selectedProduct = { "productId": orderBasedSales[i].product.id, "quantity": orderBasedSales[i].orderedQty, "name": orderBasedSales[i].product.name, "disableDone": false };
            var selecteditem = {
                "VendorId": orderBasedSales[i].vendor.id,
                "selectedVendor": $filter("filter")($scope.vendorList, { "id": orderBasedSales[i].vendor.id })[0],
                "VendorOrderItem": [],
                "isContainVendor": true
            };
            selecteditem.VendorOrderItem.push(selectedProduct);
            selecteditem = $scope.checkTinNoForStockTransfer(selecteditem);
            $scope.vendorOrder.push(selecteditem);
        }
        localStorage.removeItem("OrderBasedSales");
    }
    $scope.changeVendorName = function (data) {
        window.setTimeout(function () {
            var ele = document.getElementById("drugName");
            ele.focus();
        }, 0);

    };
    $scope.focusNextField = function (packageSize, nextid) {
        if (packageSize == undefined || packageSize == "") {
            return false;
        } else {
            if (parseInt(packageSize) == 0) {
                return false;
            } else {
                if ($scope.save.selectedVendor.isHeadOfficeVendor == true) {

                    var ele = document.getElementById("btnAddOrder");
                    ele.focus();
                }
                var ele = document.getElementById(nextid);
                ele.focus();
            }
        }
    };

    //add by nandhini25.4
    $scope.getDateSettings = function () {

        $.LoadingOverlay("show");

        vendorOrderService.getOrderSettings().then(function (response) {
            $scope.orderSettings = response.data;

            if ($scope.orderSettings.noOfDaysFromDate == undefined) {
                $scope.orderSettings.noOfDaysFromDate = 0;
            }
            if ($scope.orderSettings.noOfDaysFromDate > 0) {

                var d = new Date();
                d.setDate(d.getDate() - $scope.orderSettings.noOfDaysFromDate);
                $scope.fromDateTemp = $filter('date')(d, 'yyyy-MM-dd');
                $scope.fromDate = $scope.fromDateTemp;
            }
            else {
                $scope.fromDateTemp = $filter('date')(new Date(), 'yyyy-MM-dd');
                $scope.fromDate = $scope.fromDateTemp;
            }

            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });
    }
    //end
  


    $scope.changeQuantity = function (parentIndex, childIndex, packageSize, packageQty) {

        if ((packageSize == "" && packageQty == "") || (packageSize == "" && packageQty != "") || (packageSize != "" && packageQty == "")) {
            $scope.vendorOrder[parentIndex].VendorOrderItem[childIndex].disableDone = true;
            return false;
        } else {
            if (parseInt(packageSize) == 0 || parseInt(packageQty) == 0) {
                $scope.vendorOrder[parentIndex].VendorOrderItem[childIndex].disableDone = true;
                return false;
            } else {
                $scope.vendorOrder[parentIndex].VendorOrderItem[childIndex].quantity = packageSize * packageQty;
                $scope.vendorOrder[parentIndex].VendorOrderItem[childIndex].disableDone = false;
            }
        }
    };
    function checkEditforAllProducts() {
        var result = false;
        for (var i = 0; i < $scope.vendorOrder.length; i++) {
            result = false;
            for (var j = 0; j < $scope.vendorOrder[i].VendorOrderItem.length; j++) {
                if ($scope.vendorOrder[i].VendorOrderItem[j].isEdit) {
                    result = true;
                    break;
                }
            }
            if (result) {
                break;
            }
        }
        if (result) {
            $scope.isProcessing = true;
        } else {
            $scope.isProcessing = false;
        }
    }
    


    //By Sales Section
    $scope.SalesVendorOrder = [];
    $scope.factroErrorMessage = "";
    $scope.search = { "reOrderFactor": null };
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        "opened": false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.SalesVendorOrder = [];
    //added by nandhini 18.4.17
    var myVariable =new Date();
    var fromdate = new Date(myVariable);
    

    $scope.filter = {
        // "fromdate": $filter('date')(new Date(), 'yyyy-MM-dd'),
        //added by nandhini 18.4.17
      
        //"fromdate": $filter('date')(new Date(fromdate.setMonth(fromdate.getMonth() - 2)), 'yyyy-MM-dd'),
        "fromdate": $scope.fromDateTemp,
        "todate": $filter('date')(new Date(), 'yyyy-MM-dd'),
        "reorderfactor": ""

        
    };


    $scope.searchSales = function () {

        pagerServcie.page.pageNo = 1;

        validateOrderType();
        if (orderType == 0) {
            alert("Please select atleast one transaction type to generate order.");
            return;
        }

        $.LoadingOverlay("show");
        var data = {
            fromDate: $scope.fromDate,
            toDate: $scope.toDate
        };
        $scope.SalesVendorOrder = [];
        salesService.searchSales(data, $scope.search.reOrderFactor, orderType, $scope.loadAllProducts)
        .then(function (resp) {

            var value = document.getElementById("hdnCustomOrder").value;

            if (value == 1) {
                loadcustomSalesVendorOrder(resp.data);                
            } else {
                loadSalesVendorOrder(resp.data);
            }
            pagerServcie.init($scope.SalesVendorOrder.length, pageSearch);
            pageSearch();
            $.LoadingOverlay("hide");
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
        
    function validateOrderType() {
        orderType = 0;
        if ($scope.salesType == true && $scope.transferType == true && $scope.byMissedOrder == true && $scope.byIndentOrder == true) {
            orderType = 9;
        }
        else if ($scope.salesType == true && $scope.transferType == true && $scope.byIndentOrder == true) {
            orderType = 10;
        }        
        else if ($scope.salesType == true && $scope.transferType == true && $scope.byMissedOrder == true) {
            orderType = 7;
        }
        else if ($scope.salesType == true && $scope.byMissedOrder == true && $scope.byIndentOrder == true) {
            orderType = 11;
        }
        else if ($scope.transferType == true && $scope.byMissedOrder == true && $scope.byIndentOrder == true) {
            orderType = 12;
        }
        else if ($scope.salesType == true && $scope.transferType == true) {
            orderType = 4;
        }
        else if ($scope.salesType == true && $scope.byMissedOrder == true) {
            orderType = 5;
        }
        else if ($scope.byMissedOrder == true && $scope.transferType == true) {
            orderType = 6;
        }
        else if ($scope.salesType == true && $scope.byIndentOrder == true) {
            orderType = 13;
        }
        else if ($scope.byIndentOrder == true && $scope.transferType == true) {
            orderType = 14;
        }
        else if ($scope.byIndentOrder == true && $scope.byMissedOrder == true) {
            orderType = 15;
        }
        else if ($scope.salesType == true) {
            orderType = 1;
        }
        else if ($scope.transferType == true) {
            orderType = 2;
        }
        else if ($scope.byMissedOrder == true) {
            orderType = 3;
        }
        else if ($scope.byIndentOrder == true) {
            orderType = 8;
        }
    }

    function loadSalesVendorOrder(data) {
        for (var i = 0; i < data.length; i++) {
            data[i].packageQty = data[i].packageQty < 0 ? 0 : data[i].packageQty;
            var object = {
                "VendorId": data[i].vendorId,
                "selectedVendor": $filter("filter")($scope.vendorList, { "id": data[i].vendorId })[0],
                "VendorOrderItem": []
            };
            if (object.VendorId == undefined || object.selectedVendor == undefined) {
                object.VendorId = null;
                object.selectedVendor = null;
            }
            var OrderItem = {
                "productId": data[i].product.id,
                "productMasterID": null,
                "productName": data[i].product.name,
                "manufacturer": data[i].product.manufacturer,
                "vat": data[i].vat,
                "igst": data[i].igst,
                "cgst": data[i].cgst,
                "sgst": data[i].sgst,
                "gstTotal": data[i].gstTotal,
                "packageSize": data[i].packageSize,
                "packageQty": data[i].packageQty,
                "freeQty": data[i].freeQty,
                "purchasePrice": data[i].purchasePrice,
                "soldqty": data[i].soldqty,
                "requiredQty": data[i].requiredQty,
                "stock": data[i].stock,
                "suggQuantity": data[i].orderQty,
                "quantity": data[i].orderQty,
                "sellingPrice": data[i].sellingPrice,
                "pendingPOQty": data[i].pendingPOQty,
            };
            object.VendorOrderItem.push(OrderItem);
            object = $scope.checkTinNoForStockTransfer(object);
            $scope.SalesVendorOrder.push(object);
        }
        resetFilterData();
        setRequiredFilterData($scope.SalesVendorOrder);
        $scope.salesVendorOrderFilter($scope.filteredVendor);
        $scope.filter = {
            "fromdate": $scope.fromDate,
            "todate": $scope.toDate,
            "reorderfactor": $scope.search.reOrderFactor
        };
        $scope.search.reOrderFactor = $scope.search.reOrderFactor;
        $scope.SalesVendorOrderItems.$setUntouched();
        window.setTimeout(function () {
            var ele = document.getElementById("txtSalesVendor0");
            if (ele != null) {
                ele.focus();
            }
        }, 0);
    }
    $scope.changeSalesVendorQuantity = function (packageSize, packageQty, parentindex, childindex) {
        if (packageQty == undefined || packageSize == undefined || packageQty == "" || packageSize == "" || packageSize == 0 || packageQty == 0) {
            $scope.SalesVendorOrder[parentindex].VendorOrderItem[childindex].quantity = 0;
            return false;
        } else {
            $scope.SalesVendorOrder[parentindex].VendorOrderItem[childindex].quantity = packageSize * packageQty;
        }
    };

    $scope.changeCustomVendorQuantity = function (packageSize, packageQty, parentindex, childindex) {
        if (packageQty == undefined || packageSize == undefined || packageQty == "" || packageSize == "" || packageSize == 0 || packageQty == 0) {
            $scope.customSalesVendorOrder[parentindex].VendorOrderItem[childindex].quantity = 0;
            return false;
        } else {
            $scope.customSalesVendorOrder[parentindex].VendorOrderItem[childindex].quantity = packageSize * packageQty;
        }
        $scope.saveDraft();
    };

    $scope.getVendors = function (vendorname) {
        return vendorOrderService.getVendors(vendorname).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    $scope.onSalesVendorSelect = function (selectedvendor, index, value) {
        if (selectedvendor != undefined && typeof(selectedvendor) == "object") {
            $scope.SalesVendorOrder[index].VendorId = selectedvendor.id;
            $scope.SalesVendorOrder[index].selectedVendor = selectedvendor;
            $scope.SalesVendorOrder[index] = $scope.checkTinNoForStockTransfer($scope.SalesVendorOrder[index]);            
        }
        else
        {
            $scope.SalesVendorOrder[index].VendorId = null;
        }
        setRequiredFilterData($scope.SalesVendorOrderFilter);
    };

    // Vendor name, Show zero stock & Pending po qty filter added by Settu 
    function resetFilterData() {
        $scope.vendorListFilter = [];
        $scope.SalesVendorOrderFilter = [];
        $scope.filteredVendor = "";
        $scope.showZeroRows = false;
        $scope.showPOItems = false;
    };


    function setRequiredFilterData(orderList) {        
        $scope.SalesVendorOrderFilter = orderList;
        $scope.vendorListFilter = [];
        var emptyVendor = {
            id: "SelectVendorName",
            name: "All",
        }
        $scope.vendorListFilter.push(emptyVendor);
        emptyVendor = {
            id: "None",
            name: "Blank Vendor Name",
        }
        $scope.vendorListFilter.push(emptyVendor);

        var filteredVendor = $filter('groupBy')($scope.SalesVendorOrderFilter, 'VendorId');
        for (var vendor in filteredVendor) {
            $scope.vendorListFilter.push(filteredVendor[vendor][0].selectedVendor);
        }
        if ($scope.filteredVendor == "")
        $scope.filteredVendor = $scope.vendorListFilter[0];
    };
    
    $scope.salesVendorOrderFilter = function (selectedvendor) {

        if (selectedvendor == undefined)
            return;
        $scope.filteredVendor = selectedvendor;
        if (customOrder != 1 || $scope.ByMinMaxReorderSection) {
            $scope.SalesVendorOrder = $scope.SalesVendorOrderFilter;
            if (!$scope.showPOItems)
                $scope.SalesVendorOrder = $filter("filter")($scope.SalesVendorOrder, { VendorOrderItem: { pendingPOQty: 0 } }, true);
            if ($scope.showZeroRows)
                $scope.SalesVendorOrder = $filter("filter")($scope.SalesVendorOrder, { VendorOrderItem: { stock: 0 } }, true);
            if ($scope.filteredVendor.id != undefined && $scope.filteredVendor.id != "None" && $scope.filteredVendor.id != "SelectVendorName")
                $scope.SalesVendorOrder = $filter("filter")($scope.SalesVendorOrder, { VendorId: $scope.filteredVendor.id }, true);
            else if ($scope.filteredVendor.id == "None")
                $scope.SalesVendorOrder = $filter("filter")($scope.SalesVendorOrder, { VendorId: null }, true);
        }
        else
            $scope.customSalesVendorOrderFilter($scope.filteredVendor);
        $scope.message = "No records found";
        $scope.Filtermessage = "Try using a different criteria to view better results";
        pagerServcie.init($scope.SalesVendorOrder.length, pageSearch);
        pageSearch();
    };

    $scope.customSalesVendorOrderFilter = function () {

        $scope.customSalesVendorOrder = $scope.SalesVendorOrderFilter;
        if (!$scope.showPOItems)
            $scope.customSalesVendorOrder = $filter("filter")($scope.customSalesVendorOrder, { VendorOrderItem: { pendingPOQty: 0 } }, true);
        if ($scope.showZeroRows)
            $scope.customSalesVendorOrder = $filter("filter")($scope.customSalesVendorOrder, { VendorOrderItem: { stock: 0 } }, true);
        if ($scope.filteredVendor.id != undefined && $scope.filteredVendor.id != "None" && $scope.filteredVendor.id != "SelectVendorName")
            $scope.customSalesVendorOrder = $filter("filter")($scope.customSalesVendorOrder, { VendorId: $scope.filteredVendor.id }, true);
        else if($scope.filteredVendor.id == "None")
            $scope.customSalesVendorOrder = $filter("filter")($scope.customSalesVendorOrder, { VendorId: null }, true);

        $scope.saveDraft();
    };
    
    $scope.clearSavedOrder = function (SalesVendorOrderFilter, savedOrder) {
        var SalesVendorOrderFilterTemp = [];
        var found = false;
        for (var i = 0; i < SalesVendorOrderFilter.length; i++) {
            found = false;
            for (var j = 0; j < savedOrder.length; j++) {
                if (SalesVendorOrderFilter[i] == savedOrder[j]) {
                    found = true;
                    break;
                }                    
            }
            if (!found)
                SalesVendorOrderFilterTemp.push(SalesVendorOrderFilter[i]);
        }
        return SalesVendorOrderFilterTemp;
    };

    function removeDeletedVendor(order) {
        for (var i = 0; i < $scope.SalesVendorOrderFilter.length; i++) {
            if ($scope.SalesVendorOrderFilter[i] == order) {
                $scope.SalesVendorOrderFilter.splice(i, 1);
                break;
            }
        }
        setRequiredFilterData($scope.SalesVendorOrderFilter);
    }

    $scope.focusTxtpackageQty = function (value, index, childindex) {
        var ele = document.getElementById(value + index + "_" + childindex);
        ele.focus();
    };
    $scope.focusNextVendor = function (id, index) {
        index++;
        var ele = document.getElementById(id + index);
        if (ele != null) {
            ele.focus();
        } else {
            document.getElementById("btnSaveSalesOrder").focus();
        }
    };
    $scope.deleteSaleVendor = function (index) {
        if (!confirm("Are you sure, Do you want to delete ? "))
            return;
        var temp = $scope.SalesVendorOrder[index];
        $scope.SalesVendorOrder.splice(index, 1);
        removeDeletedVendor(temp);
    };
    $scope.cancelSalesVendor = function () {
        if (!confirm("Are you sure, Do you want to cancel ? "))
            return;
        $scope.search.reOrderFactor = null;
     
        //add by nandhini28.4
        var d = new Date();
        d.setDate(d.getDate() - $scope.orderSettings.noOfDaysFromDate);
        $scope.fromDateTemp = $filter('date')(d, 'yyyy-MM-dd');
        $scope.fromDate = $scope.fromDateTemp;
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.SalesVendorOrderItems.$setUntouched();
        $scope.SalesVendorOrder = [];
        resetFilterData();
    };
    $scope.focusBtnSearch = function () {
        if ($scope.search.reOrderFactor == null || $scope.fromDate == null || $scope.toDate == null || $scope.search.reOrderFactor < 0 || $scope.search.reOrderFactor == 0 || $scope.search.reOrderFactor > 99) {
            return false;
        }
        $scope.searchSales();
    };
    $scope.saveSalesOrder = function () {
        for (var i = 0; i < $scope.SalesVendorOrder.length; i++) {
            if ($scope.SalesVendorOrder[i].selectedVendor == "" || $scope.SalesVendorOrder[i].selectedVendor == undefined) {
                document.getElementById("txtSalesVendor" + i).focus();
                alert("Enter Vendor Name");
                return false;
                break;
            }
            if ($scope.SalesVendorOrder[i].selectedVendor.id == undefined) {
                document.getElementById("txtSalesVendor" + i).focus();
                alert("Enter Proper Vendor Name");
                return false;
                break;
            }
            for (var j = 0; j < $scope.SalesVendorOrder[i].VendorOrderItem.length; j++) {
                //chekcing packagesize
                if ($scope.SalesVendorOrder[i].VendorOrderItem[j].packageSize == "") {
                    document.getElementById("txtpackageSize" + i + "_" + j).focus();
                    alert("Enter Units/Strip");
                    return false;
                    break;
                }
                if ($scope.SalesVendorOrder[i].VendorOrderItem[j].packageSize == 0) {
                    document.getElementById("txtpackageSize" + i + "_" + j).focus();
                    alert("0 Not Allowed");
                    return false;
                    break;
                }
                //checking packageQty
                if (document.getElementById("txtpackageQty" + i + "_" + j).value.length > 5 && $scope.SalesVendorOrder[i].VendorOrderItem[j].packageQty == undefined) {
                    document.getElementById("txtpackageQty" + i + "_" + j).focus();
                    alert("No Of Strips should be less than or equal to 5 digits");
                    return false;
                    break;
                }
                if ($scope.SalesVendorOrder[i].VendorOrderItem[j].packageQty == "" || $scope.SalesVendorOrder[i].VendorOrderItem[j].packageQty == undefined) {
                    document.getElementById("txtpackageQty" + i + "_" + j).focus();
                    alert("Enter No Of Strips");
                    return false;
                    break;
                }                
                if ($scope.SalesVendorOrder[i].VendorOrderItem[j].packageQty == 0) {
                    document.getElementById("txtpackageQty" + i + "_" + j).focus();
                    alert("0 not Allowed");
                    return false;
                    break;
                }
                var cVendorOrder = $scope.SalesVendorOrder[i];
                var cVendorOrderItem = $scope.SalesVendorOrder[i].VendorOrderItem[j];


                applyGST(cVendorOrder.selectedVendor, cVendorOrderItem, cVendorOrderItem.igst, cVendorOrderItem.cgst, cVendorOrderItem.sgst);

            }
        }


        var grouppedStudents = $filter('groupBy')($scope.SalesVendorOrder, 'VendorId');
        $scope.tempSalesVendorOrder = [];
        for (var propt in grouppedStudents) {
            var obj = { "VendorId": propt, "VendorOrderItem": [], "selectedVendor": grouppedStudents[propt][0].selectedVendor };
            if ($filter("filter")($scope.tempSalesVendorOrder, { "VendorId": propt }).length == 0) {
                for (var i = 0; i < grouppedStudents[propt].length; i++) {
                    for (var item in grouppedStudents[propt][i]) {
                        if (item == "VendorOrderItem") {
                            obj.VendorOrderItem.push(grouppedStudents[propt][i][item][0]);
                        }
                    }
                }
                $scope.tempSalesVendorOrder.push(obj);
            }
        }

        $.LoadingOverlay("show");
        vendorOrderService.create($scope.tempSalesVendorOrder).then(function (response) {
            $scope.message = "Order Saved Successfully";
            toastr.success('Order Saved Successfully');
            $scope.search.reOrderFactor = null;
            
            //add by nandhini28.4
            var d = new Date();
            d.setDate(d.getDate() - $scope.orderSettings.noOfDaysFromDate);
            $scope.fromDateTemp = $filter('date')(d, 'yyyy-MM-dd');
            $scope.fromDate = $scope.fromDateTemp;
            $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');            
            var tempdata = $scope.clearSavedOrder($scope.SalesVendorOrderFilter, $scope.SalesVendorOrder);
            resetFilterData();
            setRequiredFilterData(tempdata);
            $scope.filteredVendor = "";
            $scope.SalesVendorOrderItems.$setUntouched();
            $scope.SalesVendorOrder = [];

            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    };
    //$("#txtFromdate").keyup(function (e) {
    //    if ($(this).val() == "") {
    //        $scope.fromDateError = false;
    //    } else {
    //        if ($(this).val().length == 2 || $(this).val().length == 5) {
    //            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
    //                $(this).val($(this).val() + "/");
    //        }
    //        var value = $(this).val();
    //        var splits = value.split("/");
    //        if (splits[1] > "12") {
    //            $scope.fromDateError = true;
    //            return false;
    //        } else {
    //            $scope.fromDateError = false;
    //        }
    //        if (splits[1] <= "12" && splits[0] > "31") {
    //            $scope.fromDateError = true;
    //            return false;
    //        } else {
    //            $scope.fromDateError = false;
    //        }
    //    }
    //});
    //$("#txtTodate").keyup(function (e) {
    //    if ($(this).val() == "") {
    //        $scope.todateError = false;
    //    } else {
    //        if ($(this).val().length == 2 || $(this).val().length == 5) {
    //            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
    //                $(this).val($(this).val() + "/");
    //        }
    //        var value = $(this).val();
    //        var splits = value.split("/");
    //        if (splits[1] > "12") {
    //            $scope.todateError = true;
    //            return false;
    //        } else {
    //            $scope.todateError = false;
    //        }
    //        if (splits[1] <= "12" && splits[0] > "31") {
    //            $scope.todateError = true;
    //            return false;
    //        } else {
    //            $scope.todateError = false;
    //        }
    //    }
    //});
    //$scope.fromDateError = false;
    //$scope.todateError = false;
    //$scope.validatedate = function (value, type) {
    //    if (value.length == 10) {
    //        validatedate(value, type);
    //    }
    //};
    //function validatedate(value, type) {
    //    var splits = value.split("/");
    //    var dt = new Date(splits[1] + "/" + splits[0] + "/" + splits[2]);
    //    if (dt.getDate() == splits[0] && dt.getMonth() + 1 == splits[1] && dt.getFullYear() == splits[2]) {
    //        if (type == "fromdate") {
    //            $scope.fromDateError = false;
    //        }
    //        if (type == "todate") {
    //            $scope.todateError = false;
    //        }
    //    } else {
    //        if (type == "fromdate") {
    //            $scope.fromDateError = true;
    //        }
    //        if (type == "todate") {
    //            $scope.todateError = true;
    //        }
    //        return;
    //    }
    //    FutureDateValidation(dt, type);
    //}
    //function FutureDateValidation(dt, type) {
    //    var dtToday = new Date();
    //    var pastDate = new Date(Date.parse(dtToday.getMonth() + "/" + dtToday.getDate() + "/" + parseInt(dtToday.getFullYear() - 100)));
    //    if (dt < pastDate || dt >= dtToday) {
    //        if (type == "fromdate") {
    //            $scope.fromDateError = true;
    //        }
    //        if (type == "todate") {
    //            $scope.todateError = true;
    //        }
    //    } else {
    //        if (type == "fromdate") {
    //            $scope.fromDateError = false;
    //        }
    //        if (type == "todate") {
    //            $scope.todateError = false;
    //        }
    //    }
    //}








    //By Sales NagerCoil

    $scope.customSalesVendorOrder = [];

    function loadcustomSalesVendorOrder(data) {
        $scope.draftAll = false;
        $scope.customSalesVendorOrder = [];
        for (var i = 0; i < data.length; i++) {
            var object = {
                "VendorId": data[i].vendorId,
                "selectedVendor": $filter("filter")($scope.vendorList, { "id": data[i].vendorId })[0],
                "VendorOrderItem": [],
                "macthedPrevious": 0
            };
            if (object.VendorId == undefined || object.selectedVendor == undefined) {
                object.VendorId = null;
                object.selectedVendor = null;
            }
            var OrderItem = {
                "productId": data[i].product.id,
                "productMasterID": null,
                "productName": data[i].product.name,
                "manufacturer": data[i].product.manufacturer,
                "vat": data[i].vat,
                "igst": data[i].igst,
                "cgst": data[i].cgst,
                "sgst": data[i].sgst,
                "gstTotal": data[i].gstTotal,
                "packageSize": data[i].packageSize,
                "packageQty": "",
                "freeQty":data[i].freeQty,
                "purchasePrice": data[i].purchasePrice,
                "soldqty": data[i].soldqty,
                "requiredQty": data[i].requiredQty,
                "stock": data[i].stock,


                "quantity": "",
                "sellingPrice": "",
                "expireDate": "",
                "pendingPOQty": data[i].pendingPOQty,
                "suggQuantity": data[i].packageQty,

            };
            object.VendorOrderItem.push(OrderItem);
            object = $scope.checkTinNoForStockTransfer(object);
            $scope.customSalesVendorOrder.push(object);
        }
        $scope.filter = {
            "fromdate": $scope.fromDate,
            "todate": $scope.toDate,
            "reorderfactor": $scope.search.reOrderFactor
        };
        $scope.search.reOrderFactor = $scope.search.reOrderFactor;
        $scope.SalesVendorOrderItems.$setUntouched();
        window.setTimeout(function () {
            var ele = document.getElementById("txtCustomSalesVendor0");
            if (ele != null) {
                ele.focus();
            }
        }, 0);
        $scope.attachPreviousDraft();
        $scope.saveDraft();

        resetFilterData();
        setRequiredFilterData($scope.customSalesVendorOrder);
        $scope.customSalesVendorOrderFilter($scope.filteredVendor);
    }

    $scope.attachPreviousDraft = function () {
        if (window.localStorage.getItem("order_data") == null)
            return;        
        var tempOrderItem = JSON.parse(window.localStorage.getItem("order_data"));
        var prdsId = "";
            for (var j = 0; j < $scope.customSalesVendorOrder.length; j++) {
                var macthedPrevious = false;
                for (var i = 0; i < tempOrderItem.customSalesVendorOrder.length; i++) {
                    if ($scope.customSalesVendorOrder[j].VendorOrderItem[0].productId == tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].productId) {
                        tempOrderItem.customSalesVendorOrder[i].isAdded = true;
                        if ($scope.customSalesVendorOrder[j].VendorOrderItem[0].stock != tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].stock ||
                            $scope.customSalesVendorOrder[j].VendorOrderItem[0].soldqty != tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].soldqty) {
                            tempOrderItem.customSalesVendorOrder[i].macthedPrevious = 1;
                        }
                        tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].freeQty = ($scope.customSalesVendorOrder[j].VendorOrderItem[0].freeQty == null || $scope.customSalesVendorOrder[j].VendorOrderItem[0].freeQty == undefined) ? "" : $scope.customSalesVendorOrder[j].VendorOrderItem[0].freeQty;
                        tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].soldqty = $scope.customSalesVendorOrder[j].VendorOrderItem[0].soldqty;
                        tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].requiredQty = $scope.customSalesVendorOrder[j].VendorOrderItem[0].requiredQty;
                        tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].stock = $scope.customSalesVendorOrder[j].VendorOrderItem[0].stock;
                        tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].packageSize = $scope.customSalesVendorOrder[j].VendorOrderItem[0].packageSize;
                        tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].suggQuantity = $scope.customSalesVendorOrder[j].VendorOrderItem[0].suggQuantity;
                        tempOrderItem.customSalesVendorOrder[i].VendorOrderItem[0].pendingPOQty = $scope.customSalesVendorOrder[j].VendorOrderItem[0].pendingPOQty;

                        macthedPrevious = true;
                    }
                }
                if (macthedPrevious == false) {
                    $scope.customSalesVendorOrder[j].macthedPrevious = 2;
                    tempOrderItem.customSalesVendorOrder.push($scope.customSalesVendorOrder[j]);
                }
            }

            $scope.customSalesVendorOrder = $filter('orderBy')(tempOrderItem.customSalesVendorOrder, 'macthedPrevious');

            for (var j = 0; j < $scope.customSalesVendorOrder.length; j++) {
                if ($scope.customSalesVendorOrder[j].isAdded == undefined) {
                    if (tempOrderItem.filter.reorderfactor != $scope.filter.reorderfactor) {
                        $scope.customSalesVendorOrder[j].VendorOrderItem[0].requiredQty = Math.round($scope.customSalesVendorOrder[j].VendorOrderItem[0].soldqty * $scope.filter.reorderfactor);
                        $scope.customSalesVendorOrder[j].VendorOrderItem[0].suggQuantity = Math.round(($scope.customSalesVendorOrder[j].VendorOrderItem[0].requiredQty - $scope.customSalesVendorOrder[j].VendorOrderItem[0].stock) / $scope.customSalesVendorOrder[j].VendorOrderItem[0].packageSize);
                    }
                    if (prdsId != "")
                        prdsId = prdsId + ",";
                    prdsId = prdsId + "'" + $scope.customSalesVendorOrder[j].VendorOrderItem[0].productId + "'";
                }
            }
        $scope.getProductDetails(prdsId);
    };

    $scope.getProductDetails = function (prdsId) {        
        if (prdsId != "") {
            var prodsId = [];
            prodsId[0] = prdsId;
            validateOrderType();
            $.LoadingOverlay("show");
            vendorOrderService.getProductDetails(prodsId, $scope.filter.fromdate, $scope.filter.todate, orderType).then(function (response) {
                $.LoadingOverlay("hide");
                for (var i = 0; i < response.data.length; i++) {
                    for (var j = 0; j < $scope.customSalesVendorOrder.length; j++) {
                        if ($scope.customSalesVendorOrder[j].VendorOrderItem[0].productId == response.data[i].productId
                            && ($scope.customSalesVendorOrder[j].VendorOrderItem[0].soldqty != response.data[i].soldqty
                            || $scope.customSalesVendorOrder[j].VendorOrderItem[0].stock != response.data[i].stock)) {
                            $scope.customSalesVendorOrder[j].VendorOrderItem[0].soldqty = response.data[i].soldqty;
                            $scope.customSalesVendorOrder[j].VendorOrderItem[0].stock = response.data[i].stock;
                            $scope.customSalesVendorOrder[j].macthedPrevious = 1;
                            $scope.customSalesVendorOrder[j].VendorOrderItem[0].requiredQty = Math.round($scope.customSalesVendorOrder[j].VendorOrderItem[0].soldqty * $scope.filter.reorderfactor);
                            $scope.customSalesVendorOrder[j].VendorOrderItem[0].suggQuantity = Math.round(($scope.customSalesVendorOrder[j].VendorOrderItem[0].requiredQty - $scope.customSalesVendorOrder[j].VendorOrderItem[0].stock) / $scope.customSalesVendorOrder[j].VendorOrderItem[0].packageSize);
                        }
                    }
                }
                $scope.customSalesVendorOrder = $filter('orderBy')($scope.customSalesVendorOrder, 'macthedPrevious');
            });
        }
    }

    getHeadOfficeVendor = function () {
        vendorOrderService.getHeadOfficeVendor().then(function (response) {
            $scope.headOfficeVendor = response.data;
        });
    }
    getHeadOfficeVendor();
   
    $scope.customOnSalesVendorSelect = function (selectedvendor, index, value) {
        if (selectedvendor != undefined && typeof (selectedvendor) == "object") {
            $scope.customSalesVendorOrder[index].VendorId = selectedvendor.id;
            $scope.customSalesVendorOrder[index].selectedVendor = selectedvendor;
            $scope.customSalesVendorOrder[index] = $scope.checkTinNoForStockTransfer($scope.customSalesVendorOrder[index]);
        }
        else {
            $scope.customSalesVendorOrder[index].VendorId = null;
        }
        setRequiredFilterData($scope.SalesVendorOrderFilter);
    };
    $scope.focusCustomTxtpackageQty = function (id, parentindex, childindex) {
        var ele = document.getElementById(id + parentindex + "_" + childindex);
        ele.focus();
    };

    $scope.focustxtCustomsellingPrice = function (id, parentindex, childindex) {
        var ele = document.getElementById(id + parentindex + "_" + childindex);
        ele.focus();
    };

    $scope.focustxtCustomExpireDate = function (id, parentindex, childindex) {
        var ele = document.getElementById(id + parentindex + "_" + childindex);
        ele.focus();
    };

    $scope.OnExpDateSelect = function (expdate, parentindex, childindex) {
        parentindex++;
        var ele = document.getElementById("txtCustomSalesVendor" + parentindex);
        if (ele != null) {
            ele.focus();
        } else {
            document.getElementById("btnCustomSaveSalesOrder").focus();
        }
    };


    $scope.deleteCustomSaleVendor = function (index) {
        if (!confirm("Are you sure, Do you want to delete ? "))
            return;
        var temp = $scope.customSalesVendorOrder[index];
        $scope.customSalesVendorOrder.splice(index, 1);
        removeDeletedVendor(temp);
        $scope.saveDraft();

    };

    if (window.localStorage.getItem("order_data") !== null) {
        $scope.draftOrderCount = 1;
    }

    $scope.addCustomSaleVendor = function (index) {
        var customSalesVendorOrderTemp = [];
        for (var i = 0; i < $scope.customSalesVendorOrder.length; i++) {
            customSalesVendorOrderTemp.push($scope.customSalesVendorOrder[i]);
            if (i === index) {
                customSalesVendorOrderTemp.push($scope.customSalesVendorOrder[index]);
                var tempFilter = [];
                for (var j = 0; j < $scope.SalesVendorOrderFilter.length; j++) {
                    tempFilter.push($scope.SalesVendorOrderFilter[j]);
                    if ($scope.SalesVendorOrderFilter[j] == $scope.customSalesVendorOrder[index]) {
                        tempFilter.push($scope.customSalesVendorOrder[index]);                        
                    }
                }
                $scope.SalesVendorOrderFilter = tempFilter;
                window.localStorage.setItem("orderFilterData", JSON.stringify($scope.SalesVendorOrderFilter));
                $scope.SalesVendorOrderFilter = JSON.parse(window.localStorage.getItem("orderFilterData"));
                window.localStorage.removeItem('orderFilterData');
            }
        }
        $scope.SalesVendorOrderItems.$setUntouched();
        $scope.customSalesVendorOrder = [];
        $scope.customSalesVendorOrder = customSalesVendorOrderTemp;
        $scope.saveDraft();
        var tempOrderItem = JSON.parse(window.localStorage.getItem("order_data"));
        $scope.customSalesVendorOrder = tempOrderItem.customSalesVendorOrder;
        $scope.filter = tempOrderItem.filter;
        $scope.fromDate = tempOrderItem.filter.fromdate;
        $scope.toDate = tempOrderItem.filter.todate;
    };

    $scope.saveDraft = function () {
        if ($scope.customSalesVendorOrder.length > 0) {
            var tempOrderItem = {
                "customSalesVendorOrder": $scope.customSalesVendorOrder,
                "filter": $scope.filter,
                "type": {
                    salesType: $scope.salesType,
                    transferType: $scope.transferType,
                    byMissedOrder: $scope.byMissedOrder,
                    byIndentOrder: $scope.byIndentOrder,
                },
            };
            window.localStorage.setItem("order_data", JSON.stringify(tempOrderItem));
            $scope.draftOrderCount = 1;
        }
        else {
            window.localStorage.removeItem('order_data');
            $scope.draftOrderCount = 0;
        }
    }

    $scope.loadDraft = function (fromSave) {
        if (window.localStorage.getItem("order_data") == null)
            return;
        $.LoadingOverlay("show");
        $scope.draftOrderLoaded = true;
        var tempOrderItem = JSON.parse(window.localStorage.getItem("order_data"));
        $scope.customSalesVendorOrder = tempOrderItem.customSalesVendorOrder;
        $scope.filter = tempOrderItem.filter;
        $scope.fromDate = tempOrderItem.filter.fromdate;
        $scope.toDate = tempOrderItem.filter.todate;
        $scope.salesType = (tempOrderItem.type.salesType == undefined ? true : tempOrderItem.type.salesType);
        $scope.transferType = tempOrderItem.type.transferType;
        $scope.byMissedOrder = tempOrderItem.type.byMissedOrder;
        $scope.byIndentOrder = tempOrderItem.type.byIndentOrder;
        var prdsId = "";
        for (var i = 0; i < $scope.customSalesVendorOrder.length; i++) {
            if (prdsId != "")
                prdsId = prdsId + ","
            prdsId = prdsId + "'" + $scope.customSalesVendorOrder[i].VendorOrderItem[0].productId + "'"
        }
        $.LoadingOverlay("hide");
        $scope.getProductDetails(prdsId);
        if (!fromSave) {
            resetFilterData();
            setRequiredFilterData($scope.customSalesVendorOrder);
        }
    };

    $scope.cancelCustomSalesVendor = function () {
        if (!confirm("Are you sure, Do you want to cancel ? "))
            return;
        if ($scope.draftOrderLoaded) {
            var alert = window.confirm("Do you want to delete draft also?.");
            if (alert) {
                window.localStorage.removeItem('order_data');
                $scope.draftOrderCount = 0;
            }
        }
        $scope.search.reOrderFactor = null;
        //added by nandhini 28.4.17
        var d = new Date();
        d.setDate(d.getDate() - $scope.orderSettings.noOfDaysFromDate);
        $scope.fromDateTemp = $filter('date')(d, 'yyyy-MM-dd');
        $scope.fromDate = $scope.fromDateTemp;
        $scope.toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.SalesVendorOrderItems.$setUntouched();
        $scope.customSalesVendorOrder = [];
        $scope.draftOrderLoaded = false;
        resetFilterData();
    };
    $scope.formateDate = function (parentindex, childindex, event, id) {

        var expireDate = document.getElementById(id + parentindex + "_" + childindex).value;
        var key = expireDate.substring(expireDate.length - 1, expireDate.length);

        if (isNaN(key)) {
            if (!(expireDate.length === 3 && key === "/")) {
                $scope.customSalesVendorOrder[parentindex].VendorOrderItem[childindex].expireDate = "";
                return;
            }
        }

        if (expireDate.length === 2) {
            if (parseInt(expireDate.substring(0, 2)) > 12) {
                $scope.customSalesVendorOrder[parentindex].VendorOrderItem[childindex].expireDate = "";
                return;
            }
        }

        if (expireDate.length === 3) {
            if (expireDate.substring(2, 3) !== "/") {
                $scope.customSalesVendorOrder[parentindex].VendorOrderItem[childindex].expireDate = expireDate.substring(0, expireDate.length - 1) + "/";
                return;
            }
        }

        if (expireDate.length === 5) {
            var today = $filter('date')(new Date(), 'MM/yy');
            today = parseInt(today.substring(3, 5));
            if (parseInt(expireDate.substring(3, 5)) < today) {
                $scope.customSalesVendorOrder[parentindex].VendorOrderItem[childindex].expireDate = expireDate.substring(0, expireDate.length - 1) + "";
                return;
            }
        }

        if (expireDate.length > 5) {
            $scope.customSalesVendorOrder[parentindex].VendorOrderItem[childindex].expireDate = expireDate.substring(0, expireDate.length - 1) + "";
        }

        var value = document.getElementById(id + parentindex + "_" + childindex).value;
        if (value.length == 2) {
            if (!event.ctrlKey && !event.metaKey && (event.keyCode == 32 || event.keyCode > 46))
                $scope.customSalesVendorOrder[parentindex].VendorOrderItem[childindex].expireDate = value + "/";
        }        
        return true;
    };

    $scope.dayDiff = function (expireDate) {
        expireDate = expireDate.VendorOrderItem[0].expireDate;
        if (expireDate === undefined || expireDate === null || expireDate.length < 5) {
            return true;
        }
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');

        var date2 = new Date($scope.formatString(expireDate));
        var date1 = new Date($scope.formatString(today));

        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));

        if ($scope.dayDifference < 30) {
            return true;
        }
        return false;
    }
    $scope.formatString = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    };

    $scope.draftAll = false;
    $scope.saveAlltoDraft = function (draftAll) {
        for (var i = 0; i < $scope.customSalesVendorOrder.length; i++) {
            $scope.customSalesVendorOrder[i].draft = draftAll;
        }
        $scope.saveDraft();
    }    

    $scope.calculatePurchasePrice = function (index) {
        for (var j = 0; j < $scope.customSalesVendorOrder[index].VendorOrderItem.length; j++) {
            if ($scope.customSalesVendorOrder[index].VendorOrderItem[j].sellingPrice > 0) 
                $scope.customSalesVendorOrder[index].VendorOrderItem[j].purchasePrice = parseFloat($scope.currency($scope.customSalesVendorOrder[index].VendorOrderItem[j].sellingPrice - ($scope.customSalesVendorOrder[index].VendorOrderItem[j].sellingPrice * 0.05)));
            else
                $scope.customSalesVendorOrder[index].VendorOrderItem[j].purchasePrice = $scope.customSalesVendorOrder[index].VendorOrderItem[j].sellingPrice;
        }
        $scope.saveDraft();
    }

    $scope.currency = function (N) {
        N = parseFloat(N);
        if (!isNaN(N)) {
            N = N.toFixed(2);
        } else {
            N = '0.00';
        }
        return N;
    };

    $scope.saveCustomSalesOrder = function () {        
        for (var i = 0; i < $scope.customSalesVendorOrder.length; i++) {
            if ($scope.customSalesVendorOrder[i].draft != true) {
                if ($scope.customSalesVendorOrder[i].selectedVendor == "" || $scope.customSalesVendorOrder[i].selectedVendor == undefined) {
                    document.getElementById("txtCustomSalesVendor" + i).focus();
                    alert("Enter Vendor Name");
                    return false;
                    break;
                }
                if ($scope.customSalesVendorOrder[i].selectedVendor.id == undefined) {
                    document.getElementById("txtCustomSalesVendor" + i).focus();
                    alert("Enter Proper Vendor Name");
                    return false;
                    break;
                }
                for (var j = 0; j < $scope.customSalesVendorOrder[i].VendorOrderItem.length; j++) {
                    //chekcing Units/Strip
                    if ($scope.customSalesVendorOrder[i].VendorOrderItem[j].packageSize == "" || $scope.customSalesVendorOrder[i].VendorOrderItem[j].packageSize == 0 || $scope.customSalesVendorOrder[i].VendorOrderItem[j].packageSize == null) {
                        document.getElementById("txtCustomPackageSize" + i + "_" + j).focus();
                        alert("Enter valid units/strip");
                        return false;
                        break;
                    }
                    //chekcing Quantity
                    if ($scope.customSalesVendorOrder[i].VendorOrderItem[j].suggQuantity == "") {
                        document.getElementById("txtCustomQuantity" + i + "_" + j).focus();
                        alert("Enter Quantity");
                        return false;
                        break;
                    }
                    if ($scope.customSalesVendorOrder[i].VendorOrderItem[j].suggQuantity == 0) {
                        document.getElementById("txtCustomQuantity" + i + "_" + j).focus();
                        alert("0 Not Allowed");
                        return false;
                        break;
                    }
                    //checking SellingPrice
                    if ($scope.customSalesVendorOrder[i].VendorOrderItem[j].sellingPrice == "") {
                        document.getElementById("txtCustomSellingPrice" + i + "_" + j).focus();
                        alert("Enter Strip Mrp");
                        return false;
                        break;
                    }
                    if ($scope.customSalesVendorOrder[i].VendorOrderItem[j].sellingPrice == 0) {
                        document.getElementById("txtCustomSellingPrice" + i + "_" + j).focus();
                        alert("0 not Allowed");
                        return false;
                        break;
                    }
                    ////checking ExpDate
                    //if ($scope.customSalesVendorOrder[i].VendorOrderItem[j].expireDate == "" || $scope.customSalesVendorOrder[i].VendorOrderItem[j].expireDate == undefined || $scope.customSalesVendorOrder[i].VendorOrderItem[j].expireDate == null) {
                    //    document.getElementById("txtExpDate" + i + "_" + j).focus();
                    //    alert("Enter Expiry Date");
                    //    return false;
                    //    break;
                    //}
                    //if ($scope.dayDiff($scope.customSalesVendorOrder[i]) || document.getElementById("txtExpDate" + i + "_" + j).value.length != 5) {
                    //    document.getElementById("txtExpDate" + i + "_" + j).focus();
                    //    alert("Expire date must be more than 30 days.");
                    //    return;
                    //    break;
                    //}

                    var cVendorOrder = $scope.customSalesVendorOrder[i];
                    var cVendorOrderItem = $scope.customSalesVendorOrder[i].VendorOrderItem[j];


                    applyGST(cVendorOrder.selectedVendor, cVendorOrderItem, cVendorOrderItem.igst, cVendorOrderItem.cgst, cVendorOrderItem.sgst);
                }
            }
        }

        $scope.customSalesVendorOrderForSave = [];
        $scope.customSalesVendorOrderForDraft = [];
        for (var i = 0; i < $scope.customSalesVendorOrder.length; i++) {
            if ($scope.customSalesVendorOrder[i].draft == true)
                $scope.customSalesVendorOrderForDraft.push($scope.customSalesVendorOrder[i]);
            else
                $scope.customSalesVendorOrderForSave.push($scope.customSalesVendorOrder[i]);
        }

        if ($scope.customSalesVendorOrderForSave.length == 0) {
            alert("Please unselect save draft option to process order.");
            return;
        }

        var grouppedStudents = $filter('groupBy')($scope.customSalesVendorOrderForSave, 'VendorId');
        $scope.tempCustomSalesVendorOrder = [];
        for (var propt in grouppedStudents) {
            var obj = { "VendorId": propt, "VendorOrderItem": [], "selectedVendor": grouppedStudents[propt][0].selectedVendor };
            if ($filter("filter")($scope.tempCustomSalesVendorOrder, { "VendorId": propt }).length == 0) {
                for (var i = 0; i < grouppedStudents[propt].length; i++) {
                    for (var item in grouppedStudents[propt][i]) {
                        if (item == "VendorOrderItem") {
                            obj.VendorOrderItem.push(grouppedStudents[propt][i][item][0]);
                        }
                    }
                }
                $scope.tempCustomSalesVendorOrder.push(obj);
            }
        }
        $.LoadingOverlay("show");
        vendorOrderService.create($scope.tempCustomSalesVendorOrder).then(function (response) {
            var tempdata = $scope.clearSavedOrder($scope.SalesVendorOrderFilter, $scope.customSalesVendorOrderForSave);
            resetFilterData();
            setRequiredFilterData(tempdata);
            $scope.filteredVendor = "";
            $scope.customSalesVendorOrder = $scope.customSalesVendorOrderForDraft;
            $scope.saveDraft();
            toastr.success('Order Saved Successfully');
            $scope.message = "Order Saved Successfully";
            $scope.search.reOrderFactor = null;
            //add by nandhini 18.4,17
            $scope.fromDate = $filter('date')(new Date($scope.fromDateTemp), 'yyyy-MM-dd');
            $scope.toDate = $filter('date') (new Date(), 'yyyy-MM-dd');
            $scope.SalesVendorOrderItems.$setUntouched();            
            $scope.customSalesVendorOrder = [];
            $scope.loadDraft(true);
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    };
    
    //To check and assign head office Id for stock transfer offline sync by Settu
    $scope.checkTinNoForStockTransfer = function (data) {

        if (data.selectedVendor == undefined)
            return data;
        for (var i = 0; i < data.VendorOrderItem.length; i++) {
            data.VendorOrderItem[i].toInstanceId = "";
        }
        data.selectedVendor.toInstanceId = "";

        if ((data.selectedVendor.tinNo != undefined && data.selectedVendor.tinNo != null && data.selectedVendor.tinNo != "") || (data.selectedVendor.gsTinNo != undefined && data.selectedVendor.gsTinNo != null && data.selectedVendor.gsTinNo != "")) {
            if ($scope.headOfficeVendor != null && $scope.headOfficeVendor != "") {
                if (($scope.headOfficeVendor.tinNo != undefined && $scope.headOfficeVendor.tinNo != null && $scope.headOfficeVendor.tinNo != "") || ($scope.headOfficeVendor.gsTinNo != undefined && $scope.headOfficeVendor.gsTinNo != null && $scope.headOfficeVendor.gsTinNo != "")) {
                    if ($scope.headOfficeVendor.isHeadOffice == true && ($scope.headOfficeVendor.tinNo == data.selectedVendor.tinNo || $scope.headOfficeVendor.gsTinNo == data.selectedVendor.gsTinNo)) {
                        for (var i = 0; i < data.VendorOrderItem.length; i++) {
                            data.VendorOrderItem[i].toInstanceId = $scope.headOfficeVendor.id;
                        }
                        data.selectedVendor.toInstanceId = $scope.headOfficeVendor.id;
                    }
                }
            }
        }
        return data;
    }

    $scope.exportDataToCsv = function () {
        if ($scope.customSalesVendorOrder.length > 0) {
            var data = "Sr.no.,Vendor Name,Product Name,Sold Qty,Req Qty,Stock,Units/strip,Free Quantity, Suggested Strip, Ordered Strip,Mrp/Strip, ExpDate,\n";
            for (i = 0; i < $scope.customSalesVendorOrder.length; i++) {
                data = data + (($scope.customSalesVendorOrder[i] == null || $scope.customSalesVendorOrder[i] == undefined) ? "" : i+1) + ",";
                data = data + (($scope.customSalesVendorOrder[i].selectedVendor == null || $scope.customSalesVendorOrder[i].selectedVendor == undefined) ? "" : $scope.customSalesVendorOrder[i].selectedVendor.name) + ",";
                data = data + (($scope.customSalesVendorOrder[i].VendorOrderItem[0] == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.customSalesVendorOrder[i].VendorOrderItem[0].productName) + ",";
                data = data + (($scope.customSalesVendorOrder[i].VendorOrderItem[0] == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.customSalesVendorOrder[i].VendorOrderItem[0].soldqty) + ",";
                data = data + (($scope.customSalesVendorOrder[i].VendorOrderItem[0] == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.customSalesVendorOrder[i].VendorOrderItem[0].requiredQty) + ",";
                data = data + (($scope.customSalesVendorOrder[i].VendorOrderItem[0] == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.customSalesVendorOrder[i].VendorOrderItem[0].stock) + ",";
                data = data + (($scope.customSalesVendorOrder[i].VendorOrderItem[0] == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.customSalesVendorOrder[i].VendorOrderItem[0].packageSize) + ",";
                data = data + (($scope.customSalesVendorOrder[i].VendorOrderItem[0] == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : (($scope.customSalesVendorOrder[i].VendorOrderItem[0].freeQty == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0].freeQty == undefined) ? "" : $scope.customSalesVendorOrder[i].VendorOrderItem[0].freeQty)) + ",";
                data = data + (($scope.customSalesVendorOrder[i].VendorOrderItem[0] == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.customSalesVendorOrder[i].VendorOrderItem[0].suggQuantity) + ",";
                data = data + (($scope.customSalesVendorOrder[i].VendorOrderItem[0] == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.customSalesVendorOrder[i].VendorOrderItem[0].packageQty) + ",";
                data = data + (($scope.customSalesVendorOrder[i].VendorOrderItem[0] == null || $scope.customSalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.customSalesVendorOrder[i].VendorOrderItem[0].sellingPrice) + ",";

                if ($scope.customSalesVendorOrder[i].VendorOrderItem[0].expireDate == "" ||
                        $scope.customSalesVendorOrder[i].VendorOrderItem[0].expireDate == null
                        || $scope.customSalesVendorOrder[i].VendorOrderItem[0].expireDate == undefined) {
                    data = data + "" + ",";
                }
                else {
                    data = data + ($filter('date')($scope.customSalesVendorOrder[i].VendorOrderItem[0].expireDate, "MM/yy")).toString() + ",";
                }
                data = data + ",\n";
            }
            data = data + ",\n";

            var frmday = $scope.filter.fromdate.split('-');
            var frmdayValue = frmday[2] + frmday[1];
            var today = $scope.filter.todate.split('-');
            var todayValue = today[2] + today[1];
            var filename = 'OrderbySales_' + frmdayValue + 'to' + todayValue + '_ROF_' + $scope.filter.reorderfactor + '.csv';

            var a = $('<a/>', {
                style: 'display:none',
                href: 'data:application/octet-stream;base64,' + btoa(data),
                download: filename
            }).appendTo('body')
            a[0].click()
            a.remove();
        }

        if ($scope.SalesVendorOrder.length > 0) {
            var data = "Vendor Name,Product Name,Sold Qty,Req Qty,Stock, Suggested Qty, Units/strip, No of Strips, Purchase Price/Strip, Mrp/Strip, GST%\n";
            for (i = 0; i < $scope.SalesVendorOrder.length; i++) {
                data = data + (($scope.SalesVendorOrder[i].selectedVendor == null || $scope.SalesVendorOrder[i].selectedVendor == undefined) ? "" : $scope.SalesVendorOrder[i].selectedVendor.name) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].productName) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].soldqty) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].requiredQty) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].stock) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].suggQuantity) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].packageSize) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].packageQty) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].purchasePrice) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].sellingPrice) + ",";
                data = data + (($scope.SalesVendorOrder[i].VendorOrderItem[0] == null || $scope.SalesVendorOrder[i].VendorOrderItem[0] == undefined) ? "" : $scope.SalesVendorOrder[i].VendorOrderItem[0].gstTotal) + ",";
               
                data = data + ",\n";
            }
            data = data + ",\n";

            var frmday = $scope.filter.fromdate.split('-');
            var frmdayValue = frmday[2] + frmday[1];
            var today = $scope.filter.todate.split('-');
            var todayValue = today[2] + today[1];
            var filename = 'OrderbySales_' + frmdayValue + 'to' + todayValue + '_ROF_' + $scope.filter.reorderfactor + '.csv';

            var a = $('<a/>', {
                style: 'display:none',
                href: 'data:application/octet-stream;base64,' + btoa(data),
                download: filename
            }).appendTo('body')
            a[0].click()
            a.remove();
        }

    };

    $scope.showStockBranchWise = function (productId, productName) {
        var m = ModalService.showModal({
            "controller": "stockBranchWiseCtrl",
            "templateUrl": 'stockBranchWise',
            "inputs": { "tempProduct": { "id": productId, "name": productName } }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {

            });
        });
    };

});
//Added by Bikas on 15.10.2018 for First letter caps.
app.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
app.filter('groupBy', function ($timeout) {
    return function (data, key) {
        if (!key) return data;
        var outputPropertyName = '__groupBy__' + key;
        if (!data[outputPropertyName]) {
            var result = {};
            for (var i = 0; i < data.length; i++) {
                if (!result[data[i][key]])
                    result[data[i][key]] = [];
                result[data[i][key]].push(data[i]);
            }
            Object.defineProperty(data, outputPropertyName, { enumerable: false, configurable: true, writable: false, value: result });
            $timeout(function () { delete data[outputPropertyName]; }, 0, false);
        }
        return data[outputPropertyName];
    };
});