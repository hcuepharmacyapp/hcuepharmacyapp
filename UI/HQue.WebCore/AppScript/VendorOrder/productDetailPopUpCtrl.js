﻿app.controller('productDetailPopUpCtrl', function ($scope, $rootScope, close, PreviousOrders) {
    $scope.PreviousOrders = PreviousOrders;
    $scope.shouldBeOpen = true;
    $scope.isSelectedItem = 1;
    $scope.cancel = function () {
        if (window.confirm('Are you sure, Do you want to cancel?')) {
            window.location = window.location.origin + window.location.pathname;
        }
        $scope.discountInValid = false;
    };
    $scope.selectRow = function (val) {
        $scope.isSelectedItem = ($scope.isSelectedItem == val) ? null : val;
    };
    $scope.keyPress = function (e) {
        if (e.keyCode == 38) {
            if ($scope.isSelectedItem == 1) {
                $scope.isSelectedItem = ($scope.PreviousOrders.length) + 1;
            }
            $scope.isSelectedItem--;
            e.preventDefault();
        }
        if (e.keyCode == 40) {
            if ($scope.isSelectedItem == $scope.PreviousOrders.length) {
                $scope.isSelectedItem = 0;
            }
            $scope.isSelectedItem++;
            e.preventDefault();
        }
        if (e.keyCode == 13) {
            enableWindow = false;
            $scope.submit($scope.PreviousOrders[$scope.isSelectedItem - 1]);
        }
        if (e.keyCode == 27) {
            $scope.close('No');
        }
    };

    $scope.addDetail = function (val) {
        $scope.submit($scope.PreviousOrders[val - 1]);
    };
    $scope.submit = function (selectedProduct) {
        $rootScope.$emit('vendorid', selectedProduct.vendorOrder.vendor.id);
        $scope.close('Yes');
        $.LoadingOverlay("show");
        $.LoadingOverlay("hide");
    };

    $scope.close = function (result) {
        close(result, 100);
        if (result == "No") {
            $rootScope.$emit('vendorid', null);
        }
        $(".modal-backdrop").hide();
    };
});
app.directive('focusMe', function ($timeout, $parse) {
    return {
        "link": function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            element.bind('blur', function () {
                scope.$apply(model.assign(scope, false));
            })
        }
    };
});







