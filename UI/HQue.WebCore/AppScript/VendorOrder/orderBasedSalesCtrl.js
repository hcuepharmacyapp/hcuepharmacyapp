﻿app.controller('orderBasedSalesCtrl', function ($scope, vendorOrderService, pagerServcie, $filter) {
   
    //var missedOrder = missedOrderModel;

    $scope.search = null;
    $scope.list = [];
    $scope.titles = [];
    $scope.titleCount = null;
    $scope.showQtyError = false;    

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.orderSales = function () {
        $.LoadingOverlay("show");
        vendorOrderService.orderBasedSales($scope.search).then(function (response) {
            $scope.list = response.data;

            angular.forEach($scope.list, function (item) { item.orderedQty = null; item.idLength = $.trim(item.vendor.id).length; });
            $scope.titles = [];
            $scope.titleCount = null;
            $scope.showQtyError = false;
            $scope.showVendorError = false;
            $scope.errorCount = 0;
            getTitles($scope.search);
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });
    }

    function getTitles(val) {
        if (val <= 7) {
            $scope.titleCount = val;
            for (var i = 0; i < $scope.titleCount; i++) {
                var d = new Date();
                d.setDate(d.getDate() - i);
                $scope.titles.push(new Date(d));
            }
            $scope.titles.reverse();            
        }
        else if (val <= 30) {
            $scope.titleCount = Math.ceil(val / 7);
            for (var i = 0; i < $scope.titleCount; i++) {
                var title = "Week " + (i + 1).toString();
                $scope.titles.push(title);
            }
        }
        else {
            $scope.titleCount = Math.ceil(val / 30);
            for (var i = 0; i < $scope.titleCount; i++) {
                var title = "Month " + (i + 1).toString();
                $scope.titles.push(title);
            }
        }
    }

    $scope.clearSearch = function () {
        $scope.search = null;
        $scope.list = [];
        $scope.titles = [];
    }

    $scope.validateQty = function (val) {
        if (val > 0 && $scope.showQtyError) {
            $scope.showQtyError = false;
        }        
    }
    
    $scope.convertOrderToSales = function () {
        var temp = [], count1 = 0;        
        for (var i = 0; i < $scope.list.length; i++) {
            if ($scope.list[i].orderedQty > 0) {
                    temp.push($scope.list[i]);
                    count1++;
            }
        }
        if (count1 <= 0) {
            $scope.showQtyError = true;
            return 0;
        }        
        window.localStorage.setItem("OrderBasedSales", JSON.stringify(temp));
        window.location.assign('/vendorOrder/index');
    }
        

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper'+ row).slideToggle();
        $('#chip-wrapper'+ row).show();
        if ($('#chip-btn'+ row).text() === '+')
            $('#chip-btn'+ row).text('-');
        else {
            $('#chip-btn'+ row).text('+');
        }
     }

     $scope.currency = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.00'; return N; }
     $scope.currency3 = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.000'; return N; }
   

});


