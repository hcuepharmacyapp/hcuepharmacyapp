﻿app.controller('pendingAuthorisationListCtrl', function ($scope, vendorOrderModel, vendorOrderService, pagerServcie, toastr) {

    var vendorOrder = vendorOrderModel;

    $scope.search = vendorOrder;

    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        $.LoadingOverlay("show");
        vendorOrderService.pendingAuthorisationList($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.orderSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        vendorOrderService.pendingAuthorisationList($scope.search).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.orderSearch();

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.authoriseOrders = function (item) {
        if (!confirm("Are you sure, you want to approve this order? "))
            return;
            $.LoadingOverlay("show");
            vendorOrderService.sendAuthoriseData(item).then(function (response) {
            $.LoadingOverlay("show");
            toastr.success('Order approved successfully');
            window.location.assign('/vendorOrder/pendingAuthorisationList');
            $.LoadingOverlay("hide");
        }, function () {
             $.LoadingOverlay("hide");
        });
    }
   
});
