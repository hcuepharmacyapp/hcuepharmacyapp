﻿app.factory('dashboardService', function ($http) {
    return {
        lowStock: function (instanceId) {
            return $http.post('/dashboardData/lowStock?InstanceId=' + instanceId);
        },
        trendingSales: function (instanceId) {
            return $http.post('/dashboardData/trendingSales?InstanceId=' + instanceId);
        }, 
        getPendingLeads: function (instanceId) {
            return $http.post('/dashboardData/getPendingLeads?InstanceId=' + instanceId);
        },
        getAboutToExpireProduct: function (instanceId) {
            return $http.post('/dashboardData/AboutToExpireProduct?InstanceId=' + instanceId);
        },
        // Newly Added Gavaskar 27-10-2016 Start
        getSales: function (instanceId) {
            return $http.post('/dashboardData/Sales?InstanceId='+ instanceId);
        },
        getPurchase: function (instanceId) {
            return $http.post('/dashboardData/Purchase?InstanceId=' + instanceId);
        },
        // Newly Added Gavaskar 27-10-2016 End

        // Newly Added Gavaskar 15-11-2016 Start
        getCurrentStock: function (instanceId) {
            return $http.post('/dashboardData/CurrentStock?InstanceId=' + instanceId);
        },
        // Newly Added Gavaskar 15-11-2016 End
        //New service by Manivannan
        loadInstances: function () {
            return $http.post('/dashboardData/LoadInstances');
        }
    }
});