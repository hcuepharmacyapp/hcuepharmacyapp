﻿app.controller("dashboardCtrl", function ($scope, dashboardService, paymentService, ModalService, $filter, toastr) {






    $scope.instance = '';

    $scope.loadDashBoardData = function () {       
        $scope.getSales();
        $scope.getPurchase();
        $scope.lowStock();
        $scope.trendingSales();
        $scope.getPendingLeads();
        $scope.getCurrentStock();
        $scope.getAboutToExpireProduct();

    }

    $scope.init = function (instanceId) {
        //  $.LoadingOverlay("show");

       // getModelPopup(); // For Release Notes
        //finYearManagement();//It has been moved to layout page, don't enable here.
        $scope.instance = instanceId;
        $scope.loadInstances();
        $scope.loadDashBoardData();
    }

    var EndYear = "";
    function finYearManagement() {
        getFinyearAvailable();
    }

    function getFinyearAvailable() {
        paymentService.getFinyearAvailable().then(function (response) {
           
            if (response.data == undefined || response.data == null ) {

            } else {


                // EndYear = response.data.finYearEndDate;

                // var nextDay = new Date(EndYear);

                //var next= nextDay.setDate(nextDay.getDate() + 1);


                //var NextofEndDate = $filter('date')(new Date(next), "yyyy-MM-dd");

                // var currdate = $filter('date')(new Date(), "yyyy-MM-dd");
                // var newEndDate = EndDate.split("-");
                // var currdate = currdate.split("-");




                //    if (newEndDate[1] == currdate[1] && newEndDate[2] == currdate[2]) {



                //  }

                var finYearEndDate = new Date(response.data.finYearEndDate);

                var today = new Date();
                var previous = new Date($filter('date')(new Date(), "yyyy-MM-dd") + " 00:00:000");
                previous.setDate(previous.getDate() + 3);

                if (today > finYearEndDate || previous > finYearEndDate) {

                    var startDate = $filter('date')(new Date(response.data.finYearStartDate), "yyyy-MM-dd");
                    startDate = startDate.split("-");
                    startDate = new Date(parseInt(startDate[0]) + 1 + "-" + startDate[1] + "-" + startDate[2]);
                    var endDate = $filter('date')(new Date(response.data.finYearEndDate), "yyyy-MM-dd");
                    endDate = endDate.split("-");
                    endDate = new Date(parseInt(endDate[0]) + 1 + "-" + endDate[1] + "-" + endDate[2]);

                    var m = ModalService.showModal({
                        "controller": "ModalController",
                        "templateUrl": 'myFinYearContent.html',
                        "inputs": {
                            "StartingDate": startDate,
                            "EndDate": endDate
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {



                            $scope.saveFinyearStatus(result);

                        });
                    });
                }
                $scope.finYearfrom = $filter('date')(new Date(response.data.finYearStartDate), "yyyy-MM-dd");
            }

        }, function () {

        });
    }  
    $scope.loadInstances = function () {
        //var target = $('#branchDiv');
        //target.LoadingOverlay("show");
        dashboardService.loadInstances().then(function (response) {
            $scope.instanceData = response.data;
            for (var i = 0; i < $scope.instanceData.length; i++) {
                if ($scope.instanceData[i].area != undefined && $scope.instanceData[i].area.length > 30) {
                    $scope.instanceData[i].name = ($scope.instanceData[i].name + (($scope.instanceData[i].area == undefined) ?
                        "" : ", " + $scope.instanceData[i].area)).substring(0, 35);
                }
                else if ($scope.instanceData[i].area == undefined) {
                    $scope.instanceData[i].name = $scope.instanceData[i].name;
                }
                else {
                    $scope.instanceData[i].name = ($scope.instanceData[i].name + ", " + $scope.instanceData[i].area).substring(0, 30);
                }
            } // Added by Lawrence for InstanceName + Area
        }, function () {
            //target.LoadingOverlay("hide");
        });
    }


    $scope.getSales = function () {
        //var target = $('#salesDiv');
        //target.LoadingOverlay("show");

        $.LoadingOverlay("show");
        dashboardService.getSales($scope.instance).then(function (response) {
            $scope.salesData = response.data;
            $.LoadingOverlay("hide");
            //target.LoadingOverlay("hide");
        }, function () {
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        });
    }


    $scope.getPurchase = function () {
        //var target = $('#purchaseDiv');
        //target.LoadingOverlay("show");
        $.LoadingOverlay("show");
        dashboardService.getPurchase($scope.instance).then(function (response) {
            $scope.purchaseData = response.data;
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function () {
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        });
    }


    $scope.lowStock = function () {
        //var target = $('#lowStockDiv');
        //target.LoadingOverlay("show");
        $.LoadingOverlay("show");
        dashboardService.lowStock($scope.instance).then(function (response) {
            $scope.lowStockData = response.data;
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function () {
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        });
    }


    $scope.trendingSales = function () {
        //var target = $('#trendingSalesDiv');
        //target.LoadingOverlay("show");
        $.LoadingOverlay("show");
        dashboardService.trendingSales($scope.instance).then(function (response) {
            $scope.trendingSalesData = response.data;
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function () {
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        });
    }


    $scope.getPendingLeads = function () {
        $scope.isData = true;
        //var target = $('#leadsDiv');
        //target.LoadingOverlay("show");
        $.LoadingOverlay("show");
        dashboardService.getPendingLeads($scope.instance).then(function (response) {
            console.log(JSON.stringify(response.data));
            if (response.data.data.length > 0) {
                $scope.labels = response.data.labels;
                $scope.series = ['Series A'];
                $scope.data = [];
                $scope.data.push(response.data.data);
            }
            else {
                $scope.isData = false;
            }
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function () {
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        });
    }


    $scope.getCurrentStock = function () {
        //var target = $('#currentStockDiv');
        //target.LoadingOverlay("show");
        $.LoadingOverlay("show");
        dashboardService.getCurrentStock($scope.instance).then(function (response) {
            $scope.currentStockData = response.data;
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function () {
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        });
    }


    $scope.getAboutToExpireProduct = function () {
        //var target = $('#aboutToExpireDiv');
        //target.LoadingOverlay("show");
        $.LoadingOverlay("show");
        dashboardService.getAboutToExpireProduct($scope.instance).then(function (response) {
            $scope.aboutToExpireData = response.data;
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        }, function () {
            //target.LoadingOverlay("hide");
            $.LoadingOverlay("hide");
        });
    }


    $scope.finyearobject = {};

    $scope.saveFinyearStatus = function (result) {
        //var resetval = 0;//continue resultval=0
        //if (result.resetStatus == 'Reset') {
        //    resetval = 1; //1 is to Reset 
        //}

        $scope.finyearobject = {
            'FinYearStartDate': result.finYearfrom,
            'FinYearEndDate': result.finYearto,
            //"FinYearStatus": resetvalpassWord
            "IsBillNumberReset": result.isBillNumberReset,
            "EmailId": result.EmailId,
             "passWord": result.passWord
        }


        paymentService.saveFinyearStatus($scope.finyearobject).then(function (response) {
            if (response.data.saveFailed) {
                toastr.error(response.data.errorMessage, 'Error');
              
            }
            else
            {
                toastr.success("Financial Year Reseted Successfully");
            }
           
            // $.LoadingOverlay("hide");
        }, function () {
            //   $.LoadingOverlay("hide");
        });
    }

    function getModelPopup() {
            
        if (window.localStorage.getItem("offer") == 38)
            return;
        var m = ModalService.showModal({
            "controller": "modalpopupCtrl",
            "templateUrl": 'myModalContent.html',
            
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                
            });
        });
    }

});


app.controller("modalpopupCtrl", function ($scope) {
    $scope.cancel = function (val) {       
      
        if (val == true) {
            window.localStorage.setItem("offer", 38);         
            $(".modal-backdrop").hide();
            $('#releaseModal').hide();
        }
        else {
            $(".modal-backdrop").hide();
            $('#releaseModal').hide();
        }
    };
});

//app.controller("modalpopupCtrl", function ($scope, $modal) {
//    if (window.localStorage.getItem("offer") == 5)
//        return;
//    $modal.open({
//        templateUrl: 'myModalContent.html',
//        backdrop: true,
//        windowClass: 'offermodal',
//        controller: function ($scope, $modalInstance) {


//            $scope.cancel = function (val) {
//                if (val == true) {
//                    window.localStorage.setItem("offer",5);
//                }
//                $modalInstance.dismiss('cancel');
//            };

//        }
//    });
//});



app.controller('ModalController', function ($scope, close, StartingDate, EndDate) {

    console.log(StartingDate + "__" + EndDate);


    if (StartingDate == "" || StartingDate == undefined || StartingDate == null) {
        $scope.finYearfrom = "";
    } else {
        $scope.finYearfrom = StartingDate;
    }
    if (EndDate == "" || EndDate == undefined || EndDate == null) {
        $scope.finYearto = "";
    } else {
        $scope.finYearto = EndDate;
    }

    $scope.isBillNumberReset = "";




    $scope.close = function (result) {





        close(result, 500); // close, but give 500ms for bootstrap to animate
        $(".modal-backdrop").hide();


    };
    $scope.open1 = function () {
        console.log("111")
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        console.log("322")
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];



    $scope.saveFinalcialyearStatus = function () {

        $scope.isBillNumberReset = parseInt($scope.isBillNumberReset);
        $scope.EmailId = $scope.EmailId;
        $scope.passWord = $scope.passWord;
        $scope.resultObj = {
            "finYearfrom": $scope.finYearfrom,
            "finYearto": $scope.finYearto,
            "isBillNumberReset": $scope.isBillNumberReset,
            "EmailId": $scope.EmailId,
            "passWord": $scope.passWord
        }

        $scope.close($scope.resultObj);
    }

});

