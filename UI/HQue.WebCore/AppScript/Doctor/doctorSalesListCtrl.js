﻿app.controller('doctorSalesListCtrl', ['$scope', '$http', '$interval', '$q', 'doctorService', 'doctorModel', 'salesModel', 'salesItemModel', function ($scope, $http, $interval, $q, doctorService, doctorModel, salesModel, salesItemModel) {
    
    var salesItem = salesItemModel;
    var sales = salesModel;
    $scope.search = salesItem;
    $scope.search.sales = sales;

 

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };


    $scope.init = function (doctorName) {
        $scope.doctorName = doctorName;
        $scope.doctorSalesReport(doctorName);
    }

    $scope.doctorSalesReport = function (doctorName) {
        $.LoadingOverlay("show");
        
        doctorService.getDoctorSales(doctorName).then(function (response) {
            $scope.data = response.data;
            $scope.type = "";


            $("#grid").kendoGrid({
                    excel: {
                        fileName: "Doctor Sales Item Details.xlsx",
                        allPages: true
                    },
                    pdf: {
                        allPages: true,
                        fileName: "Doctor Sales Item Details.pdf",
                        paperSize: "A4",
                        margin: { top: "3cm", right: "1cm", bottom: "1cm", left: "1cm" },
                        landscape: true,
                        template: $("#page-template").html()
                    },
                    columnMenu: true,
                    pageable: true,
                    resizable: true,
                    reorderable: true,
                    sortable: true,
                    filterable: {
                        mode: "column"
                    },
                columns: [
                  { field: "sales.invoiceDate", title: "Invoice Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(sales.invoiceDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                  { field: "sales.invoiceNo", title: "Invoice No", width: "100px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                  { field: "sales.doctorName", title: "Doctor", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                    { field: "sales.doctorMobile", title: "Doctor Mobile", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },                    
                    { field: "sales.name", title: "Customer Name", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                    { field: "sales.mobile", title: "Customer Mobile", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                    { field: "sales.deliveryType", title: "Delivery Type", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                    { field: "sales.paymentType", title: "Payment Type", width: "160px", attributes: { class: "text-left field-report" }, filterable: { cell: { showOperators: false } } },
                    { field: "productStock.product.name", title: "Product", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },
                    { field: "batchNo", title: "Batch", width: "160px", attributes: { class: "text-left field-highlight" }, filterable: { extra: false, cell: { operator: "startswith" } } },
                    { field: "productStock.expireDate", title: "Expire Date", width: "120px", type: "date", attributes: { class: "field-primary", ftype: "date", fformat: "dd-MM-yyyy" }, template: "#= kendo.toString(kendo.parseDate(productStock.expireDate, 'yyyy-MM-dd'), 'dd/MM/yyyy') #", filterable: { cell: { showOperators: false } } },
                    { field: "productStock.vat", title: "VAT/GST(%)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "productStock.vatAmount", title: "Vat/GST InPrice", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "Total :" },
                    { field: "quantity", title: "Qty", width: "100px", format: "{0}", type: "number", attributes: { class: "text-right field-report", ftype: "number", fformat: "0" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0') #</div>" },
                    { field: "discount", title: "Discount", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "productStock.purchasePrice", title: "Purchase Price", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },
                    { field: "sellingPrice", title: "Selling Price(MRP)", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" } },                    
                  { field: "sales.finalValue", title: "Final Value", width: "100px", format: "{0:n}", type: "number", attributes: { class: "text-right field-report" }, footerTemplate: "<div class='report-footer'>#= kendo.toString(sum, '0.00') #</div>" },
                  
                  
                ],
                dataSource: {
                    data: response.data,
                    aggregate: [
                        { field: "quantity", aggregate: "sum" },
                        { field: "productStock.purchasePrice", aggregate: "sum" },
                        { field: "sellingPrice", aggregate: "sum" },
                        { field: "sales.finalValue", aggregate: "sum" }
                    ],
                    schema: {
                        model: {
                            fields: {
                                "sales.invoiceDate": { type: "date" },
                                "sales.invoiceNo": { type: "number" },
                                "sales.doctorName": { type: "string" },
                                "sales.doctorMobile": { type: "string" },                                
                                "sales.name": { type: "string" },
                                "sales.mobile": { type: "string" },
                                "sales.deliveryType": { type: "string" },
                                "sales.paymentType": { type: "string" },
                                "productStock.product.name": { type: "string" },
                                "batchNo": { type: "string" },
                                "productStock.expireDate": { type: "date" },
                                "productStock.vat": { type: "number" },
                                "productStock.vatAmount": { type: "number" },
                                "quantity": { type: "number" },
                                "discount": { type: "number" },
                                "productStock.purchasePrice": { type: "number" },
                                "sellingPrice": { type: "number" },
                                "sales.finalValue": { type: "number" }
                            }
                        }
                    },
                    pageSize: 20
                },

                excelExport: function (e) {

                    addHeader(e);

                    var sheet = e.workbook.sheets[0];
                    for (var i = 0; i < sheet.rows.length; i++) {
                        var row = sheet.rows[i];
                        for (var ci = 0; ci < sheet.rows[i].cells.length; ci++) {
                            var cell = row.cells[ci];

                            if (row.type == "data" && this.columns[ci].attributes.ftype == "date") {
                                cell.hAlign = "left";
                                cell.format = this.columns[ci].attributes.fformat;
                                cell.value = new Date(cell.value);
                            }
                            if (row.type == "data" && this.columns[ci].attributes.ftype == "number") {
                                cell.hAlign = "right";
                                cell.format = "#" + this.columns[ci].attributes.fformat;
                            }

                            if (row.type == "group-footer" || row.type == "footer") {
                                if (cell.value) {
                                    cell.value = $.trim($('<div>').html(cell.value).text());
                                    cell.value = cell.value.replace('Total:', '');
                                    cell.hAlign = "right";
                                    cell.format = "#0.00";
                                    cell.bold = true;
                                }
                            }
                        }
                    }
                },
            });


            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    //


    $scope.filter = function (type) {
        $scope.type = type;
        $scope.doctorSalesReport();
    }


    // chng-3

    $scope.header = "Qbitz Technologies ◙ Anna Nagar ◙ 90384923843";
    function addHeader(e) {
        var clen = e.workbook.sheets[0].rows[0].cells.length;
        headerCell = { cells: [{ value: "Created By: " + $scope.userData + " at  " + $scope.today, bold: true, vAlign: "center", textAlign: "right", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: " Purchase Details", bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        var headerCell = { cells: [{ value: "DL No : " + $scope.instance.drugLicenseNo + " / " + "TIN No: " + $scope.instance.tinNo, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.fullAddress, bold: true, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "header" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
        headerCell = { cells: [{ value: $scope.instance.name, bold: true, fontSize: 20, vAlign: "center", hAlign: "center", colSpan: clen, }], type: "title" };
        e.workbook.sheets[0].rows.splice(0, 0, headerCell);
    }

    //
}]);
