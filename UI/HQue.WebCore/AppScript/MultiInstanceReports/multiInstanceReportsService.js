﻿app.factory('multiInstanceReportsService', function ($http) {
    return {
        
        getPermissions: function () {
            return $http.get('/Registration/GetPermissions');
        },

       
        bdoData: function () {
            return $http.get('/Registration/GetBDOs');
        },

        //Newly Added Gavaskar 07-10-2016 Start
        //list: function (type, allInstances, instanceIds, fromDate, toDate) {
        list: function (reportSearchRequest) {
            return $http.post('/MultiInstanceReports/SalesSummaryListData', reportSearchRequest);
        },

    }
});
