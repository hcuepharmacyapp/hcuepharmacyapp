﻿app.factory('userManagementService', function ($http) {
    return {
        "userAccessList": function (data) {
            return $http.get('/UserManagement/userAccessList', data);
        },
        "create" : function (user) {
           return $http.post('/UserManagement/index', user);
        },
        "update": function (user) {
            return $http.post('/UserManagement/update', user);
        },
        "addSalesPerson": function (user) {
            return $http.post('/UserManagement/addSalesPerson', user);
        },
        //added by nandhini on 4/1/18
        "updateUserValuesStatus": function (data) {
            return $http.post('/UserManagement/updateUserValuesStatus', data);
        },
        "SalesPersonList": function (data) {
            return $http.get('/UserManagement/SalesPersonList', data);
        },
        "enableSalesUserSettings": function (data) {
            return $http.get('/Sales/SaveSaleUserSettings?isEnableSalesUser=' + data);
        },
        "getSaleUserSettings": function () {
            return $http.get('/Sales/GetSaleUserSettings');
        },        
    };
});
