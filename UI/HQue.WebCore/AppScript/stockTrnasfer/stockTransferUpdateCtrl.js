﻿app.controller('stockTransferUpdateCtrl', function ($scope, stockTransferService, productStockService, pagerServcie, vendorOrderService, ModalService) {
    $scope.btnclicked = false; //Variable added to block the multiple time button pressed by Poongodi on 01/03/2017
    var ProductStock = (function () {




        //shortcutHelper.setScope($scope);
        //$scope.keydown = shortcutHelper.purchaseShortcuts;

        //var purchasePrice = 0;
        //var vat = 0;
        //var quantity = 0;

        function ProductStock(purchasePrice, vat, quantity) {
            this.purchasePrice = purchasePrice;
            this.vat = vat;
            this.quantity = quantity;
        }
        ProductStock.prototype.taxValue = function () {
            var taxAmt = 0;
            if (this.purchasePrice === undefined || this.vat === undefined) {
                if (purchasePrice !== undefined || vat !== undefined) {
                    taxAmt = purchasePrice * (vat / (100 + vat));
                }
            }
            else {
                taxAmt = this.purchasePrice * (this.vat / (100 + this.vat));
            }

            return taxAmt;
        };
        ProductStock.prototype.grossValue = function () {
            var gValue = 0;
            if (this.purchasePrice === undefined || this.quantity === undefined) {
                if (purchasePrice !== undefined || quantity !== undefined) {
                    gValue = quantity * purchasePrice;
                }
            }
            else {
                gValue = this.quantity * this.purchasePrice;
            }
            return gValue;

        };
        ProductStock.prototype.totalVatValue = function () {
            if (this.quantity === undefined)
                return 0;
            return this.quantity * this.taxValue();
        };
        ProductStock.prototype.netValue = function () {
            return this.grossValue();
        };
        return ProductStock;
    }());

    $scope.fromBranchs = [];
    $scope.toBranchs = [];
    $scope.model = { stockTransferItems: [] };
    $scope.addedTransfer = [];
    $scope.batchs = [];
    $scope.productToAdd = new ProductStock(0, 0, 0);
    $scope.productToAdd.quantity = angular.copy(null);
    $scope.batchDisplaySetting = null;
    $scope.showFullQtyError = false;

    $scope.getFromBranch = function () {
        $.LoadingOverlay("show");
        stockTransferService.getTransferBranch().then(function (response) {
            $scope.fromBranchs.push(response.data);
            $scope.model.fromInstance = response.data;
            $scope.getToInstance();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.getFromBranch();

    $scope.getToInstance = function () {
        stockTransferService.GetInstancesByAccountId()
            .then(function (response) {
                $scope.toBranchs = response.data;
                $scope.model.toInstance = $scope.toBranchs[0];
                document.getElementById('drugName').focus();
            });
    };

    function getTransfereNo() {
        stockTransferService.getTransfereNo()
            .then(function (response) { $scope.model.transferNo = response.data; });
    }

    $scope.getProducts = function (val) {
        $scope.showFullQtyError = false;
        if ($scope.model.fromInstance == null)
            return;
        return stockTransferService.getProductList(val, $scope.model.fromInstance.id).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.init = function (id) {
        $scope.enableAddnewStock();
        $scope.vendorPurchase.id = id;





        if (id != "") {
            $.LoadingOverlay("show");
            vendorPurchaseService.editVendorPurchaseData($scope.vendorPurchase.id).then(function (response) {
                $scope.vendorPurchase = response.data;
                $scope.vendorPurchase.selectedVendor = {};
                $scope.vendorPurchase.selectedVendor.id = $scope.vendorPurchase.vendorId;
                $scope.vendorPurchase.selectedVendor.discount = $scope.vendorPurchase.vendor.discount;

                $scope.creditset = true;
                $scope.debitset = false;

                if ($scope.vendorPurchase.noteType != undefined && $scope.vendorPurchase.noteType != null) {
                    if ($scope.vendorPurchase.noteType == "credit") {
                        $scope.vendorPurchase.noteType = 1;
                        $scope.creditset = true;
                        $scope.debitset = false;
                    }
                    else if ($scope.vendorPurchase.noteType == "debit") {
                        $scope.vendorPurchase.noteType = 2;
                        $scope.creditset = false;
                        $scope.debitset = true;
                    }
                }
                else {
                    $scope.vendorPurchase.noteType = 1;
                    $scope.creditset = true;
                    $scope.debitset = false;
                }

                if ($scope.vendorPurchase.noteAmount == undefined || $scope.vendorPurchase.noteAmount == null) {
                    $scope.vendorPurchase.noteAmount = 0;
                }
                $scope.percentDiscount = false;

                for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {


                    if ($scope.vendorPurchase.vendorPurchaseItem[i].fromDcId == "") {
                        $scope.vendorPurchase.vendorPurchaseItem[i].fromDcId = null;
                    }

                    if ($scope.vendorPurchase.vendorPurchaseItem[i].fromTempId == "") {
                        $scope.vendorPurchase.vendorPurchaseItem[i].fromTempId = null;
                    }

                    $scope.vendorPurchase.vendorPurchaseItem[i].isEdit = false;
                    $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct = {};
                    $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.name = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name;
                    $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.id;
                    $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty;

                    $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount) - Number($scope.vendorPurchase.discount);
                    $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number($scope.vendorPurchase.discount);
                    var qty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty;
                    $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);

                    $scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;


                    if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst > 0) {
                        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst / 100);

                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst;
                        $scope.TaxationType = "CST";
                    } else {
                        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat / 100);

                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat;
                        $scope.TaxationType = "VAT";
                    }


                }
                $scope.updateUI();
                setTotal();
                setVat();
                $scope.changeGrossAmount($scope.vendorPurchase.noteAmount);

                $scope.vendorPurchase.initialValue = null;


                if ($scope.vendorPurchase.paymentType == "Cheque") {
                    $scope.editCheque($scope.vendorPurchase, true, false);
                }
                else if ($scope.vendorPurchase.paymentType == "Credit") {
                    $scope.editCheque($scope.vendorPurchase, false, true);
                }
                window.localStorage.setItem("p_dataChange", JSON.stringify($scope.vendorPurchase));
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
            });
            $scope.initialValue();

        }
    };

    $scope.addProduct = function () {
        if (!productValid())
            return;

        if (!$scope.EditMode) {
            for (var i = 0; i < $scope.addedTransfer.length; i++) {
                if ($scope.addedTransfer[i].batch.id == $scope.productToAdd.batch.id) {
                    $scope.addedTransfer[i].quantity = parseInt($scope.addedTransfer[i].quantity) + parseInt($scope.productToAdd.quantity);
                    $scope.reset();
                    return;
                }
            }
            $scope.addedTransfer.push($scope.productToAdd);
            $scope.reset();
        }
        else {
            for (var i = 0; i < $scope.addedTransfer.length; i++) {
                if ($scope.productToAdd.isNew == true && $scope.addedTransfer[i].batch.id == $scope.productToAdd.batch.id && $scope.addedTransfer[i].batch.batchNo == $scope.productToAdd.batch.batchNo && $scope.addedTransfer[i].productID == $scope.productToAdd.productID) {
                    $scope.addedTransfer[i] = $scope.productToAdd;
                    $scope.addedTransfer[i].quantity = parseInt($scope.productToAdd.quantity);
                    $scope.EditMode == false
                    $scope.reset();
                    return;
                }
            }

        }

    };

    function productValid() {
        return $scope.validateQuantity() == "" && $scope.productToAdd.batch != null && $scope.productToAdd.selectedProduct != null;
    }

    $scope.reset = function () {
        $scope.productToAdd = new ProductStock(0, 0, 0);
        $scope.productToAdd.quantity = null;
        $scope.showFullQtyError = false;
        $scope.transferProduct.$setPristine();
        $scope.batchs = [];
        var drugName = document.getElementById("drugName");
        drugName.focus();
        //Following lines added to perform new transfer after updating the existing one
        $scope.model = { stockTransferItems: [] };
        $scope.getFromBranch();
        $scope.EditMode = false;
        $scope.btnclicked = false;
    };

    $scope.validateQuantity = function () {
        var inValid = $scope.productToAdd.quantity == null || $scope.productToAdd.quantity == 0;
        if (inValid)
            return "required";

        if ($scope.productToAdd.batch != null) {
            if ($scope.productToAdd.batch.batchNo !== " ") {
                var addedStock = getAddedStock($scope.productToAdd.batch.id);
                if ((parseInt($scope.productToAdd.quantity) + parseInt(addedStock)) > $scope.productToAdd.batch.stock) {
                    return "Quantity cannot be more than " + (parseInt($scope.productToAdd.batch.stock) - addedStock) + " already added stock is " + addedStock;
                }
            }

        }

        return "";
    };

    $scope.onProductSelect = function (productID) {
        var pdtId = "";
        if (productID != '') {
            pdtId = productID;
        }
        else if ($scope.productToAdd.selectedProduct != undefined) {
            pdtId = $scope.productToAdd.selectedProduct.product.id;
        }
        $.LoadingOverlay("show");
        stockTransferService.getSettings().then(function (response1) {
            $scope.batchDisplaySetting = response1.data;
            productStockService.productBatch(pdtId).then(function (response) {
                $scope.batchs = response.data;

                var tempBatch = [];
                for (var i = 0; i < $scope.batchs.length; i++) {
                    $scope.batchs[i].availableQty = $scope.getAvailableStock($scope.batchs[i]);
                    if ($scope.batchs[i].availableQty > 0)
                        tempBatch.push($scope.batchs[i]);
                }

                $scope.batchs = tempBatch;
                if ($scope.batchs.length > 0) {
                    $scope.productToAdd.batch = $scope.batchs[0];
                    $scope.batchSelected($scope.productToAdd.batch);
                }
                else {
                    $scope.showFullQtyError = true;
                    document.getElementById("drugName").focus();
                }

                if ($scope.batchDisplaySetting !== '2') {
                    if ($scope.batchs.length > 0) {
                        var m = ModalService.showModal({
                            "controller": "productBatchSearchCtrl",
                            "templateUrl": 'productBatchDetails',
                            "inputs": {
                                "productId": $scope.productToAdd.selectedProduct.product.id,
                                "productName": $scope.productToAdd.selectedProduct.name,
                                "items": $scope.addedTransfer,
                                "enableWindow": $scope.enablePopup
                            }
                        }).then(function (modal) {
                            modal.element.modal();
                            modal.close.then(function (result) {
                                if (result.status == "Yes") {
                                    $scope.productToAdd.batch = result.data;
                                    $scope.batchSelected($scope.productToAdd.batch);
                                    $.LoadingOverlay("hide");
                                } else {
                                    $.LoadingOverlay("hide");
                                }
                                var qty = document.getElementById("quantity");
                                qty.focus();
                            });
                        });
                        return false;
                    }
                    else {
                        $scope.showFullQtyError = true;
                        document.getElementById("drugName").focus();
                    }
                }

                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });

        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        })


        var qty = document.getElementById("quantity");
        qty.focus();
    };

    $scope.getAvailableStock = function (productStock) {
        return productStock.stock - getAddedStock(productStock.id);
    };

    function getAddedStock(id) {

        for (var i = 0; i < $scope.addedTransfer.length; i++) {
            if ($scope.addedTransfer[i].batch.id == id)
                return $scope.addedTransfer[i].quantity;
        }
        return 0;
    }

    $scope.getProductStock = function () {
        return new ProductStock($scope.model.purchasePrice, $scope.model.vat, $scope.model.quantity);
    };

    $scope.batchNoKeyDown = function (event) {

        if (event.which === 13) // Enter key
        {
            $scope.addProduct();
        }
    };

    $scope.batchSelected = function (selectedBatch) {
        if (selectedBatch !== undefined) {
            $scope.productToAdd.expireDate = selectedBatch.expireDate;
            $scope.productToAdd.vat = selectedBatch.vat;
            $scope.productToAdd.batchNo = selectedBatch.batchNo;
            $scope.productToAdd.purchasePrice = selectedBatch.purchasePrice;
            $scope.productToAdd.sellingPrice = selectedBatch.sellingPrice;
            //this.quantityCtl.updateValueAndValidity();
        }

    };



    getTransfereNo();

    $scope.transfer = function () {
        $scope.btnclicked = true;
        $scope.model.fromInstanceId = $scope.model.fromInstance.id;
        $scope.model.toInstanceId = $scope.model.toInstance.id;
        setTransferItem();
        stockTransferService.stockTransfer($scope.model).then(function (x) {
            transferSuccess(x.data);
        });

    };

    $scope.delete = function (data) {
        if (!confirm('Sure to delete ?')) {
            return;
        }
        var index = $scope.addedTransfer.indexOf(data);
        this.addedTransfer.splice(index, 1);
        var drugName = document.getElementById("drugName");
        drugName.focus();
    };

    $scope.keyEnter = function (event, e) {
        var ele = document.getElementById(e);
        if (event.which === 13) // Enter key
        {
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
            {
                ele.focus();
            }
        }
    };

    $scope.focusNextField = function (event, nextid) {
        if ($scope.batchDisplaySetting == 2) {
            if (nextid !== '')
                var ele = document.getElementById(nextid);

            if (event.which === 13 && nextid !== '') // Enter key
            {
                if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
                {
                    ele.focus();
                }
            }
        }
        else {
            if (event.which === 13) {
                $scope.addProduct();
            }
        }
    };
    function transferSuccess(transferNo) {
        alert('Transfer successful..!');
        $scope.reset();
        $scope.addedTransfer = [];
        $scope.model = { stockTransferItems: [] };
        $scope.model.transferNo = transferNo;
    }

    function setTransferItem() {
        for (var i = 0; i < $scope.addedTransfer.length; i++) {
            var item = {
                productStockId: $scope.addedTransfer[i].batch.id,
                quantity: $scope.addedTransfer[i].quantity
            };
            $scope.model.stockTransferItems.push(item);
        }
    }

    $scope.getNetValue = function () {
        var total = 0;

        for (var i = 0; i < $scope.addedTransfer.length; i++) {
            //ProductStock.bind( $scope.addedTransfer[i].purchasePrice, $scope.addedTransfer[i].vat, $scope.addedTransfer[i].quantity) ;

            ProductStock.prototype.constructor($scope.addedTransfer[i].purchasePrice, $scope.addedTransfer[i].vat, $scope.addedTransfer[i].quantity);
            total += parseFloat(ProductStock.prototype.netValue());
            //total += parseFloat($scope.addedTransfer[i].netValue());
        }

        return total;
    };

    $scope.showIndentTransfer = function () {

        vendorOrderService.getVendorOrder(null, false, null).then(function (response) {
            $scope.itList = response.data;
            if ($scope.itList.length > 0) {

                $scope.showITItems();
            }
        }, function () {

        });
    };

    function getSelectedDcItem(DcPoItem) {
        var poItem = DcPoItem;
        for (var j = 0; j < poItem.ITItem.length; j++) {
            if (poItem.ITItem[j].isSelected == true) {
                $scope.productStock = poItem.ITItem[j].productStock;
                $scope.productStock.selectedProduct = poItem.ITItem[j];
                $scope.productStock.selectedProduct.product.name = poItem.ITItem[j].productName;
                $scope.productStock.quantity = poItem.ITItem[j].quantity;
                $scope.productStock.batchNo = poItem.ITItem[j].productStock.batchNo;
                $scope.productStock.batch = poItem.ITItem[j].productStock;
                $scope.productStock.expireDate = poItem.ITItem[j].productStock.expireDate;
                $scope.productStock.purchasePrice = poItem.ITItem[j].purchasePrice;
                $scope.productStock.sellingPrice = poItem.ITItem[j].packageSellingPrice;
                $scope.productStock.vat = poItem.ITItem[j].product.vat;
                ProductStock($scope.productStock.purchasePrice, $scope.productStock.vat, $scope.productStock.quantity);
                $scope.addedTransfer.push($scope.productStock);
            }
        }

        $.LoadingOverlay("hide");
    }

    $scope.showITItems = function () {

        var itList = $scope.itList;
        //
        if (itList.length > 0) {
            var m = ModalService.showModal({
                "controller": "showIndentTransferCtrl",
                "templateUrl": "showIndentTransfer",
                "inputs": { "ITValue": [{ "itList": itList }] }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {

                    if (result.status == "Yes") {
                        getSelectedDcItem(result.data);
                        $scope.disableAddNewDc = true;
                    } else {
                        //var prod = document.getElementById("invoiceNo");
                        //prod.focus();
                    }
                });
            });
            return false;
        }

    };

    $scope.callculateNet = function () {
        if ($scope.productToAdd.isNew == true & $scope.EditMode == true) {
            ProductStock.prototype.constructor($scope.productToAdd.purchasePrice, $scope.productToAdd.vat, $scope.productToAdd.quantity);
            return ProductStock.prototype.netValue();
        }
    }

    $scope.EditMode = false;
    $scope.editPurchase = function (item, ind) {

        //if (item.fromDcId != null && item.fromDcId != "") {
        //    $scope.dcItemEdit = true;
        //}

        //if ($scope.EditMode) {
        //    return;
        //}
        $scope.productToAdd = item;
        $scope.productToAdd.isNew = true;

        $scope.selectedProduct = item.selectedProduct;

        $scope.EditMode = true;
        //ProductStock($scope.productToAdd.purchasePrice, $scope.productToAdd.vat, $scope.productToAdd.quantity;
        //ProductStock.
        //$scope.productToAdd.netValue();
        $scope.callculateNet();
        $scope.Editindex = ind;



    };

});