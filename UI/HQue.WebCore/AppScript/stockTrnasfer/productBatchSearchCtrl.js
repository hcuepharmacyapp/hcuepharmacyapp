﻿app.controller('productBatchSearchCtrl', function ($scope,$timeout, $rootScope, stockTransferService, productStockService, toastr, cacheService, productId, productName, items, TransferQtyType, close) {
        
    $scope.batchList1 = [];
    $scope.selectedProduct = null;
    $scope.selectedBatch = null;
    $scope.Math = window.Math;
       
    
    $scope.getAvailableStock = function (productStock) {
        return productStock.stock - getAddedStock(productStock.id);
    };


    function getAddedStock(id) {

        for (var i = 0; i < items.length; i++) {
            if (items[i].batch.id == id)
                return items[i].quantity;
        }
        return 0;
    }


    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {
            $('#chip-btn' + row).text('-');
        } else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.selectedIndex = -1;
    $scope.isSelectedItem = 1;
    $scope.shouldBeOpen = true;
    $scope.selectRow = function (val, data, index) {
        $scope.selectedIndex = index;
        if (data == undefined || data == null || data == 0) {
            $scope.packValue = "";
        }
        else {
            $scope.packValue = data;
        }
        $scope.isSelectedItem = ($scope.isSelectedItem == val) ? null : val;
    };

    $scope.checkSelectRow = function (val, data, index) {
        $scope.selectedIndex = index;
        if (data == undefined || data == null || data == 0) {
            $scope.packValue = "";
        }
        else {
            $scope.packValue = data;
        }        
    };


    $scope.keyPress = function (e,index) {
      
        if (e.keyCode == 38) {

            if ($scope.isSelectedItem == 1) {
                $scope.isSelectedItem = ($scope.batchList1.length) + 1;
            }

            var countList = $scope.isSelectedItem - 2;            

            var data = $scope.batchList1[countList];
            if (data.packageSize == undefined || data.packageSize == null || data.packageSize == 0) {
                $scope.packValue = "";
            }
            else {
                $scope.packValue = data;
            }

            $scope.isSelectedItem--;
            e.preventDefault();

        }
        if (e.keyCode == 40) {

            if ($scope.isSelectedItem == $scope.batchList1.length) {
                // return;
                $scope.isSelectedItem = 0;
            }

            var data = $scope.batchList1[$scope.isSelectedItem];
            if (data.packageSize == undefined || data.packageSize == null || data.packageSize == 0) {
                $scope.packValue = "";
            }
            else {
                $scope.packValue = data;
            }

            $scope.isSelectedItem++;
            e.preventDefault();

        }
        if (e.keyCode == 13) {
            var index1 = $scope.selectedIndex;
            var index = $scope.isSelectedItem + 1;
            var tempIndex = $scope.isSelectedItem - 1;
            var data = $scope.batchList1[tempIndex];
            if (data.packageSize == undefined || data.packageSize == null || data.packageSize == 0) {
                toastr.info('Enter Units/Strip');
                return false;
            }
            $scope.addDetail(tempIndex, index1);
            //$scope.submit($scope.batchList1[$scope.isSelectedItem - 1]);

        }
        if (e.keyCode == 27) {
            $scope.close("No");
        }

    };


    $scope.addDetail = function (val, i) {
        //productStockService.updateTransferProduct($scope.batchList1[i]).then(function (response) {
        //    $scope.list = response.data.list;
        //    $.LoadingOverlay("hide");
        //}, function () {
        //    $.LoadingOverlay("hide");
        //});
        productStockService.updateTransferProductStock($scope.batchList1[i]).then(function (response) {
            $scope.list = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
        $scope.submit($scope.batchList1[val]);
    };

    $scope.submit = function (selectedBatch) {
        if ($scope.loadingProcess == false) {
            $.LoadingOverlay("show");
            if (!$scope.TransferQtyType) {
                selectedBatch.purchasePrice = selectedBatch.purchasePrice * (selectedBatch.packageSize);
                selectedBatch.sellingPrice = selectedBatch.sellingPrice * (selectedBatch.packageSize);
                //MRP Implementation by Settu
                selectedBatch.mrp = selectedBatch.mrp * (selectedBatch.packageSize);
            }
            $scope.selectedBatch = selectedBatch;
            $scope.close("Yes");
            $.LoadingOverlay("hide");
        }
    };

    $scope.close = function (result) {
        var object1 = {};
        if (result == "Yes") {
            object1 = { "status": result, "data": $scope.selectedBatch };
        }
        else {
            object1 = { "status": result, "data": null };
        }
        
        close(object1, 100);
        
        $(".modal-backdrop").hide();
    };
    $scope.showExpDateQty = 0;
    $scope.TransferQtyType = true;
    $scope.getSettings = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        stockTransferService.getSettings().then(function (response) {
            $scope.TransferQtyType = response.data.transferQtyType;
         
            if (response.data.showExpDateQty !== null)
                $scope.showExpDateQty = response.data.showExpiredQty;

            $scope.isProcessing = false;
            $.LoadingOverlay("hide");

        }, function (error) {
            console.log(error);
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        })
    };

    $scope.getSettings();
    //10 seconds delay
    $scope.loadingProcess = true;
    $scope.isDisabled = true;
    $timeout(function () {
        if (productId != null) {
            $scope.selectedProductName = productName;

            productStockService.productBatchTransfer(productId, $scope.showExpDateQty, TransferQtyType).then(function (response) {               
                $scope.batchList1 = response.data;               
                totalQuantity = 0;
                var rack = "";
                var tempBatch1 = [];
                for (var i = 0; i < $scope.batchList1.length; i++) {
                    $scope.batchList1[i].availableQty = $scope.getAvailableStock($scope.batchList1[i]);
                    if ($scope.batchList1[i].packageSize == null || $scope.batchList1[i].packageSize == "" || $scope.batchList1[i].packageSize == 0 || $scope.batchList1[i].packageSize == undefined) {
                        $scope.isDisabled = false;
                    }
                    if ($scope.batchList1[i].availableQty > 0)
                        tempBatch1.push($scope.batchList1[i]);
                }

                $scope.batchList1 = tempBatch1;
                $scope.loadingProcess = false;
            }, function () {
                $scope.loadingProcess = false;
                $.LoadingOverlay("hide");
            });
        }
    }, 700);

    

    $scope.availStrip = function (a, b) {
        var c = a / b;
        if (c < 1) {
            return 0;
        }
        else {
            return parseInt(c);
        }

    }
   

    //MRP Implementation by Settu
    $scope.getEnableSelling = function () {
        productStockService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.getEnableSelling();
   
});


app.directive('focusMe', function ($timeout, $parse) {
    return {
        "link": function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            element.bind('blur', function () {
                scope.$apply(model.assign(scope, false));
            });
        }
    };
});




