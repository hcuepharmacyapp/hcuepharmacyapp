﻿app.controller('showIndentTransferCtrl', function ($scope, vendorPurchaseModel, productStockModel, productModel, close, dCVendorPurchaseItemModel, ITValue, stockTransferService) {

    $scope.minDate = new Date();
    var d = new Date();
    //ChequeMaxDate
    var today = new Date();
    today.setMonth(today.getMonth() + 6);
    $scope.chequeMaxDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());


    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };


    $scope.close = function (result) {
        var object = {};
        var ITObject = { "ITItem": $scope.poVendorPurchaseItem };
        if (result == "Yes") {
            object = { "status": result, "data": ITObject };
        } else {
            object = { "status": result, "data": null };
        }
        close(object, 100);

        $(".modal-backdrop").hide();
    };

    $scope.shouldBeOpen = true;
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    };

    

    $scope.isPOSelectedItem = 1;

    $scope.poKeyPressDown = function (e) {

        if (e.keyCode == 38) {
            if ($scope.isPOSelectedItem == 1) {
                //return;
                $scope.isPOSelectedItem = ($scope.poVendorPurchaseItem.length) + 1;
            }
            $scope.isPOSelectedItem--;
            var fs = $scope.
            e.preventDefault();

        }
        if (e.keyCode == 40) {
            if ($scope.isPOSelectedItem == $scope.pocVendorPurchaseItem.length) {
                // return;
                $scope.isPOSelectedItem = 0;
            }
            $scope.isPOSelectedItem++;
            e.preventDefault();
        }
        if (e.keyCode == 13) {
            $scope.submit($scope.poVendorPurchaseItem[$scope.isPOSelectedItem - 1]);
        }
        if (e.keyCode == 27) {
            $scope.close('No');
        }

    };


   


    $scope.modelOptions = {
        debounce: {
            default: 500,
            blur: 250
        },
        getterSetter: true
    };

    $scope.currency = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.00'; return N; }


    $scope.focusVendor = function (event) {
        if (event.which === 13) { // Enter key
            $("#vendor").trigger('chosen:open');
        }
    };
    $scope.keyEnter = function (event, e, previousid, currentid) {
        var ele = document.getElementById(e);
        //Enterkey
        if (event.which === 13) {
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
            {
                ele.focus();
                if (ele.nodeName != "BUTTON") {
                    ele.select();
                    event.preventDefault();
                }
            }
        }
        //BackSpace
        if (event.which == 8) {
            var text = document.getElementById(currentid).value;
            if (text.length === 0) {
                var ele = document.getElementById(previousid);
                ele.focus();
                event.preventDefault();
            }
        }
    };

    $scope.count = 0;
    $scope.addCount = function (val) {
        if (val == true) {
            $scope.count++;
        }
        else {
            $scope.count--;
        }
    }

    $scope.addItems = function () {
        $scope.close("Yes");
    };


    if (ITValue[0].itList != null && ITValue[0].itList.length > 0) {
        $scope.poVendorPurchaseItem = ITValue[0].itList;
        for (var i = 0; i < $scope.poVendorPurchaseItem.length; i++) {
            $scope.poVendorPurchaseItem[i].isSelected = false;
        }
    }


    $scope.TransferQtyType = true;
    $scope.getSettings = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        stockTransferService.getSettings().then(function (response) {
            $scope.TransferQtyType = response.data.transferQtyType;

            $scope.isProcessing = false;
            $.LoadingOverlay("hide");

        }, function (error) {
            console.log(error);
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        })
    };

    $scope.getSettings();



});

app.directive('numbersOnly', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.directive('focusBox', function () {
    return {
        "link": function (scope, element, attrs) {

            element.bind("keydown", function (event) {

                var valueLength = attrs.$$element[0].value.length;

                //Enter 
                if (event.which === 13) {
                    if (attrs.id === "freeQty" || attrs.id === "discount") {
                        ele = document.getElementById(attrs.nextid);
                        ele.focus();
                    } else {
                        if (valueLength !== 0) {
                            ele = document.getElementById(attrs.nextid);
                            ele.focus();

                        }
                    }
                }
                //BackSpace
                if (event.which === 8) {
                    if (valueLength === 0) {
                        ele = document.getElementById(attrs.previousid);
                        ele.focus();
                        event.preventDefault();
                    }
                }
            });
        }
    };
});
app.directive('focusMe', function ($timeout, $parse) {
    return {
        "link": function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {

                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            element.bind('blur', function () {
                scope.$apply(model.assign(scope, false));
            });
        }
    };
});