﻿app.controller('stockTransferCreateCtrl', function ($scope, $filter, stockTransferService, productStockService, pagerServcie, salesService, vendorPurchaseService, vendorOrderService, ModalService, toastr) {

    //Added by Settu for indent transfer with stock reducing
    $scope.indentStockTransfer = JSON.parse(window.localStorage.getItem("IndentStockTransfer"));
    if ($scope.indentStockTransfer != null) {
        $scope.fromVendorPurchaseTransfer = false;
        if ($scope.indentStockTransfer.fromVendorPurchase) {
            $scope.fromVendorPurchaseTransfer = true;
            $scope.indentStockTransfer = $scope.indentStockTransfer.data;
        }
    }
    window.localStorage.removeItem("IndentStockTransfer");
    $scope.autoIndentTransfer = false;

    var ele = "";
    $scope.btnclicked = false; //Variable added to block the multiple time button pressed by Poongodi on 01/03/2017
    var ProductStock = (function () {
        function ProductStock(purchasePrice, vat, gstTotal, quantity, packageQty) {
            this.purchasePrice = purchasePrice;
            this.vat = vat;
            this.gstTotal = gstTotal;
            this.quantity = quantity;
            this.packageQty = packageQty;
        }
        ProductStock.prototype.taxValue = function () {

            var GSTEnabled = angular.element(document.getElementById("GSTEnabled")).val();
            if (GSTEnabled == "True") {
                if (this.purchasePrice === undefined || this.gstTotal === undefined)
                    return 0;
                return this.purchasePrice * (this.gstTotal / (100 + this.gstTotal));
            } else {
                if (this.purchasePrice === undefined || this.vat === undefined)
                    return 0;
                return this.purchasePrice * (this.vat / (100 + this.vat));
            }

        };
        ProductStock.prototype.grossValue = function () {
            if (this.purchasePrice === undefined || this.quantity === undefined || this.packageQty == undefined) {
                return 0;
            } else {
                if ($scope.transferSettings) {
                    return this.quantity * this.purchasePrice;
                } else {
                    return this.packageQty * this.purchasePrice;
                }

            }

        };
        ProductStock.prototype.totalVatValue = function () {
            if (this.quantity === undefined)
                return 0;


            return ($scope.transferSettings == true ? this.quantity : this.packageQty) * this.taxValue();
        };
        ProductStock.prototype.netValue = function () {
            return this.grossValue();
        };
        return ProductStock;
    }());

    $scope.disableNT = false;
    $scope.disableIT = false;
    $scope.fromBranchs = [];
    $scope.toBranchs = [];
    $scope.model = { stockTransferItems: [] };
    $scope.addedTransfer = [];
    $scope.batchs = [];
    $scope.productToAdd = new ProductStock(0, 0, 0, 0, 0);
    $scope.productToAdd.quantity = angular.copy(null);
    $scope.batchDisplaySetting = null;
    $scope.showFullQtyError = false;
    $scope.transferSettings = true;
    $scope.scanBarcodeVal = "";
    $scope.eancodeAlert = "";

    $scope.getFromBranch = function () {
        $.LoadingOverlay("show");
        stockTransferService.getTransferBranch()
            .then(function (response) {
                $scope.fromBranchs.push(response.data);
                $scope.model.fromInstance = response.data;
                if ($scope.model.fromInstance.area != undefined && $scope.model.fromInstance.area.length > 60) {
                    $scope.model.fromInstance.name = ((($scope.model.fromInstance.area == undefined) ?
                        "" : ", " + $scope.model.fromInstance.area)).substring(0, 60);
                } else if ($scope.model.fromInstance.area == undefined || $scope.model.fromInstance.area == null || $scope.model.fromInstance.area == "") {
                    $scope.model.fromInstance.name = $scope.model.fromInstance.name;
                } else {
                    $scope.model.fromInstance.name = ($scope.model.fromInstance.area).substring(0, 60);
                }            //Added by Lawrence for InstanceName + Area
                $scope.getToInstance();
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
    };
    //added by nandhini
    $scope.getTaxValues = function () {
        vendorPurchaseService.getAllTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    //end
    $scope.getTaxValues();
    $scope.getFromBranch();

    $scope.getToInstance = function () {
        stockTransferService.GetInstancesByAccountId()
        .then(function (response) {
            $scope.toBranchs = response.data;
            //$scope.model.toInstance = $scope.toBranchs[0];

            //Moved to below method & reused from diff places
            setSameGSTinToInstance();
            $scope.model.toInstance = null;//$scope.toBranchs[0];



            setToInstanceValues(); //Moved to a method since it is needed for indent transfer with stock reducing
            document.getElementById('drugName').focus();
        });
    };

    function getTransfereNo() {
        stockTransferService.getTransfereNo()
            .then(function (response) { $scope.model.transferNo = response.data; });
    }

    $scope.showExpDateQty = null;
    $scope.getProducts = function (val) {

        stockTransferService.getSettings().then(function (response1) {
            $scope.showExpDateQty = response1.data.showExpiredQty;

        });
        $scope.eancodeAlert = "";
        $scope.showFullQtyError = false;
        if ($scope.model.fromInstance == null)
            return;
        return stockTransferService.getProductList(val, $scope.model.fromInstance.id, $scope.showExpDateQty).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });

    };

    $scope.getProduct = function () {
        if ($scope.scanBarcodeOption.scanBarcode === "1") {

            var eleId = document.getElementById("drugName");
            var scannedEancode = eleId.value
            if (($scope.productToAdd.selectedProduct === undefined || typeof ($scope.productToAdd.selectedProduct) !== "object" || $scope.productToAdd.selectedProduct === null) && eleId.value !== "") {
                productStockService.getEancodeExistData(scannedEancode).then(function (eanRes) {
                    if (eanRes.data.length !== 0) {
                        if (eanRes.data.length === 1) {
                            $scope.scanBarcodeVal = eleId.value;
                            eanRes.data[0].isNew = true;
                            $scope.productToAdd.selectedProduct = eanRes.data[0];
                            $scope.productToAdd.selectedProduct.product = eanRes.data[0];
                            $scope.onProductSelect('');
                        } else if (eanRes.data.length === 0) {
                            $scope.eancodeAlert = "Stock not available";
                        }
                    } else {
                        $scope.eancodeAlert = "Product not found";
                    }
                });
            }
        }
    };

    $scope.getScanBarcodeOption = function () {
        salesService.getScanBarcodeOption().then(function (resp) {
            $scope.scanBarcodeOption = resp.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getScanBarcodeOption();

    $scope.chkITItem = function () {
        var blnITitem = false;

        for (var i = 0; i < $scope.addedTransfer.length; i++) {
            if ($scope.addedTransfer[i].selectedProduct.isNew == false) {
                blnITitem = true;
            }
        }
        return blnITitem;
    };

    $scope.addProduct = function () {
        if (!productValid())
            return;

        if (!$scope.EditMode) {
            for (var i = 0; i < $scope.addedTransfer.length; i++) {
                if ($scope.addedTransfer[i].batch.id == $scope.productToAdd.batch.id && (!$scope.autoIndentTransfer || $scope.autoIndentTransfer && $scope.addedTransfer[i].VendorOrderItemId == $scope.productToAdd.VendorOrderItemId)) { //Added by Settu for indent transfer with stock reducing

                    if ($scope.transferSettings == true)
                        $scope.addedTransfer[i].quantity = parseInt($scope.addedTransfer[i].quantity) + parseInt($scope.TransferQty);//$scope.productToAdd.quantity);
                    else {
                        $scope.addedTransfer[i].quantity = parseInt($scope.addedTransfer[i].quantity) + (parseInt($scope.TransferQty) * $scope.addedTransfer[i].batch.packageSize);
                        $scope.addedTransfer[i].packageQty = parseInt($scope.addedTransfer[i].packageQty) + parseInt($scope.TransferQty); // Adding Sum
                    }

                    $scope.reset();
                    return;
                }
            }
            var gst = $scope.productToAdd.gstTotal;
            //$scope.productToAdd.quantity = parseInt($scope.TransferQty);
            $scope.productToAdd.quantity = parseInt($scope.TransferQty);
            $scope.productToAdd.gstTotal = gst;
            //$scope.productToAdd.packageQty = parseInt($scope.TransferQty);
            $scope.productToAdd.TransferQtyType = $scope.transferSettings;
            $scope.productToAdd.isStripTransfer = $scope.transferSettings;
            if (!$scope.productToAdd.TransferQtyType && parseInt($scope.TransferQty) > 0) {
                $scope.productToAdd.quantity = parseInt($scope.TransferQty) * $scope.productToAdd.batch.packageSize;
            }
            if ($scope.productToAdd.selectedProduct.packageSize == undefined)
                $scope.productToAdd.selectedProduct.packageSize = $scope.productToAdd.batch.packageSize;

            $scope.addedTransfer.push($scope.productToAdd);
            $scope.reset();
        } else {
            for (var i = 0; i < $scope.addedTransfer.length; i++) {
                if ($scope.productToAdd.isNew == true && $scope.addedTransfer[$scope.Editindex].batch.id == $scope.productToAdd.batch.id && $scope.addedTransfer[$scope.Editindex].batch.batchNo == $scope.productToAdd.batch.batchNo && $scope.addedTransfer[$scope.Editindex].productID == $scope.productToAdd.productID) {
                    $scope.productToAdd.TransferQtyType = $scope.transferSettings;
                    $scope.productToAdd.isStripTransfer = $scope.transferSettings;
                    $scope.addedTransfer[$scope.Editindex] = $scope.productToAdd;
                    //$scope.productToAdd.quantity = parseInt($scope.TransferQty);

                    if (!$scope.productToAdd.TransferQtyType) {
                        $scope.addedTransfer[$scope.Editindex].quantity = parseInt($scope.TransferQty) * ($scope.productToAdd.packageSize != undefined && $scope.productToAdd.packageSize != null && $scope.productToAdd.packageSize != "" ? $scope.productToAdd.packageSize : $scope.productToAdd.batch.packageSize); //$scope.productToAdd.quantity); //Added by Settu for indent transfer with stock reducing
                    }
                    //else {
                    //    $scope.addedTransfer[$scope.Editindex].packageQty = parseInt($scope.TransferQty);
                    //}
                    $scope.EditMode = false;

                }
            }
            $scope.reset();
            return;

        }

        if ($scope.chkITItem()) {
            $scope.disableIT = false;
            $scope.disableNT = true;
        } else {
            $scope.disableIT = true;
            $scope.disableNT = false;
        }

    };

    function productValid() {
        return $scope.validateQuantity() == "" && $scope.productToAdd.batch != null && $scope.productToAdd.selectedProduct != null && $scope.validateUnitPrice() == "" && $scope.productToAdd.batch.packageSize != "";
    }

    $scope.reset = function (blnCancel) {
        $scope.productToAdd = new ProductStock(0, 0, 0, 0, 0);
        $scope.productToAdd.TransferQtyType = $scope.transferSettings;
        $scope.productToAdd.quantity = null;
        $scope.showFullQtyError = false;
        $scope.transferProduct.$setPristine();
        $scope.batchs = [];
        var drugName = document.getElementById("drugName");
        drugName.focus();
        //Following lines added to perform new transfer after updating the existing one
        //
        //Added by bala for cancel the transfer item, on 13-June-2017
        if ($scope.addedTransfer.length > 0 && blnCancel == true) {
            if (confirm("Are you sure, Do you want to cancel? ")) {
                $scope.addedTransfer.length = 0;
                $scope.autoIndentTransfer = false;
                $scope.fromVendorPurchaseTransfer = false;
            }
        }
        if ($scope.chkITItem()) {
            $scope.disableIT = false;
            $scope.disableNT = true;
        } else {
            $scope.disableIT = true;
            $scope.disableNT = false;
        }
        if ($scope.addedTransfer.length <= 0) {
            //commented by bala for transfer value getting cleared, on 14/June/17
            //$scope.model = { stockTransferItems: [] };
            $scope.fromBranchs = [];
            $scope.getFromBranch();
            $scope.disableNT = false;
            $scope.disableIT = false;
        }
        //

        $scope.EditMode = false;
        $scope.TransferQty = "";
        $scope.btnclicked = false;
    };

    $scope.validateQuantity = function () {
        var inValid = ($scope.TransferQty == null || $scope.TransferQty == 0); //($scope.transferSettings == true ? ($scope.productToAdd.quantity == 0) : ($scope.productToAdd.packageQty == 0));
        if (inValid)
            return "required";

        if ($scope.productToAdd.batch != null) {
            if ($scope.productToAdd.batch.batchNo !== " ") {
                var addedStock = getAddedStock($scope.productToAdd.batch.id);


                if (!$scope.transferSettings) {
                    if ($scope.productToAdd.batch.packageSize >= 1) {
                        if ((parseInt($scope.TransferQty) + parseInt(addedStock / $scope.productToAdd.batch.packageSize)) > ($scope.productToAdd.batch.stock / $scope.productToAdd.batch.packageSize)) {
                            return "Strip cannot be more than " + parseInt($scope.productToAdd.batch.stock / $scope.productToAdd.batch.packageSize) + (addedStock > 0 ? (" already added Strip is " + addedStock / $scope.productToAdd.batch.packageSize) : "");
                        }
                    } else {
                        return "Packagesize should not empty";
                    }
                } else {
                    if ((parseInt($scope.TransferQty) + parseInt(addedStock)) > $scope.productToAdd.batch.stock) {
                        return "Quantity cannot be more than " + (parseInt($scope.productToAdd.batch.stock) - addedStock) + (addedStock > 0 ? (" already added Quantity is " + addedStock) : "");
                    }
                }

            }

        }

        return "";
    };

    $scope.validateUnitPrice = function () {
        var inValid = false;
        if ($scope.productToAdd.purchasePrice == null || $scope.productToAdd.purchasePrice == 0) {
            inValid = true;
        } else if (parseFloat($scope.productToAdd.purchasePrice) > parseFloat($scope.productToAdd.sellingPrice)) {
            inValid = true;
            return "MRP price must be greater than Cost Price + GST.";
        }
        if (inValid)
            return "required";

        return "";
    };

    $scope.onProductSelect = function (productID) {
        var pdtId = "";
        if (productID != '') {
            pdtId = productID;
        } else if ($scope.productToAdd.selectedProduct != undefined) {
            pdtId = $scope.productToAdd.selectedProduct.product.id;
        }
        $.LoadingOverlay("show");
        stockTransferService.getSettings().then(function (response1) {
            $scope.batchDisplaySetting = response1.data.productBatchDisplayType;
            $scope.showExpDateQty = 0;
            if (response1.data.showExpiredQty !== null)
                $scope.showExpDateQty = response1.data.showExpiredQty;

            productStockService.productBatchTransfer(pdtId, $scope.showExpDateQty, $scope.transferSettings).then(function (response) {
                $scope.batchs = response.data;
                //Moved to a method since it is needed for indent transfer with stock reducing
                var tempBatch = setDefaultValues();
                $scope.batchs = tempBatch;

                //Added by Sarubala for Barcode Scanner on 15-04-19 - start
                var tempBatchScan = [];
                if ($scope.scanBarcodeVal != null && $scope.scanBarcodeVal != undefined && $scope.scanBarcodeVal != "") {
                    for (var i = 0; i < $scope.batchs.length; i++) {
                        if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')($scope.batchs[i].purchaseBarcode)) {
                            tempBatchScan.push($scope.batchs[i]);
                        }
                    }

                    if (tempBatchScan.length > 0) {
                        $scope.batchs = tempBatchScan;
                    } else {
                        $scope.eancodeAlert = "Stock not available";
                    }
                }                

                //Added by Sarubala for Barcode Scanner on 15-04-19 - end

                if ($scope.batchs.length > 0) {
                    $scope.productToAdd.batch = $scope.batchs[0];
                    $scope.batchSelected($scope.productToAdd.batch);
                } else {
                    $scope.showFullQtyError = true;
                    document.getElementById("drugName").focus();
                }

                if ($scope.batchDisplaySetting !== '2' && ($scope.scanBarcodeVal == null || $scope.scanBarcodeVal == undefined || $scope.scanBarcodeVal == "")) {
                    if ($scope.batchs.length > 0) {
                        var m = ModalService.showModal({
                            "controller": "productBatchSearchCtrl",
                            "templateUrl": 'productBatchDetails',
                            "inputs": {
                                "productId": $scope.productToAdd.selectedProduct.product.id,
                                "productName": $scope.productToAdd.selectedProduct.name,
                                "items": $scope.addedTransfer,
                                "TransferQtyType": $scope.transferSettings,
                                "enableWindow": $scope.enablePopup
                            }
                        }).then(function (modal) {
                            modal.element.modal();
                            modal.close.then(function (result) {
                                if (result.status == "Yes") {


                                    //   result.data.product.packageSize = result.data.product.packageSize;
                                    if (result.data != null && result.data != undefined && result.data != '') {
                                        if (result.data.packageSize == null || result.data.packageSize < 1 || result.data.packageSize == undefined)
                                            result.data.packageSize = "";

                                        if (result.data.purchasePrice.toString().indexOf('.') != -1)
                                            result.data.purchasePrice = Math.round(result.data.purchasePrice * 100) / 100;

                                        if (result.data.vatInPrice.toString().indexOf('.') != -1)
                                            result.data.vatInPrice = Math.round(result.data.vatInPrice * 100) / 100;
                                    }

                                    $scope.productToAdd.batch = result.data;
                                    $scope.batchSelected($scope.productToAdd.batch);
                                    $.LoadingOverlay("hide");
                                } else {
                                    $.LoadingOverlay("hide");
                                }
                                var qty = document.getElementById("quantity");
                                qty.focus();
                            });
                        });
                        return false;
                    } else {
                        $scope.showFullQtyError = true;
                        document.getElementById("drugName").focus();
                    }
                } else {
                    if (!$scope.productToAdd.TransferQtyType && $scope.productToAdd.TransferQtyType != undefined) {
                        $scope.productToAdd.purchasePrice = $scope.productToAdd.purchasePrice * $scope.productToAdd.batch.packageSize;
                        $scope.productToAdd.sellingPrice = $scope.productToAdd.sellingPrice * $scope.productToAdd.batch.packageSize;
                        $scope.productToAdd.mrp = $scope.productToAdd.mrp * $scope.productToAdd.batch.packageSize;

                    }

                }


                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });

        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });


        var qty = document.getElementById("quantity");
        qty.focus();
    };

    $scope.getAvailableStock = function (productStock) {
        return productStock.stock - getAddedStock(productStock.id);
    };

    //Changed by Settu since it is needed for indent transfer with stock reducing in edit
    function getAddedStock(id) {
        var addedQty = 0;
        for (var i = 0; i < $scope.addedTransfer.length; i++) {
            if ($scope.addedTransfer[i].batch.id == id) {
                if (!$scope.EditMode || $scope.EditMode && $scope.Editindex != i)
                    addedQty += $scope.addedTransfer[i].quantity;
            }
        }
        return addedQty;
    }

    $scope.getProductStock = function () {
        return new ProductStock($scope.model.purchasePrice, $scope.model.vat, $scope.modal.gstTotal, $scope.model.quantity, $scope.model.packageQty);
    };

    $scope.batchNoKeyDown = function (event) {

        if (event.which === 13) // Enter key
        {
            $scope.addProduct();
        }
    };

    $scope.batchSelected = function (selectedBatch, batchSelected) {
        if (selectedBatch !== undefined) {
            if (batchSelected != undefined) {
                ProductStock(selectedBatch.purchasePrice, selectedBatch.vat, selectedBatch.gstTotal, selectedBatch.availableQty, (selectedBatch.availableQty / selectedBatch.packageSize));
                $scope.productToAdd.expireDate = selectedBatch.expireDate;
                $scope.productToAdd.gstTotal = selectedBatch.gstTotal;
                $scope.productToAdd.igst = selectedBatch.igst;
                $scope.productToAdd.cgst = selectedBatch.cgst;
                $scope.productToAdd.sgst = selectedBatch.sgst;
                $scope.productToAdd.vat = selectedBatch.vat;
                $scope.productToAdd.batchNo = selectedBatch.batchNo;
                if ($scope.transferSettings) {
                    $scope.productToAdd.purchasePrice = selectedBatch.purchasePrice;
                    $scope.productToAdd.sellingPrice = selectedBatch.sellingPrice;
                    $scope.productToAdd.mrp = selectedBatch.mrp;
                } else {
                    $scope.productToAdd.purchasePrice = selectedBatch.purchasePrice * selectedBatch.packageSize;
                    $scope.productToAdd.sellingPrice = selectedBatch.sellingPrice * selectedBatch.packageSize;
                    $scope.productToAdd.mrp = selectedBatch.mrp * selectedBatch.packageSize;
                }

            } else {
                $scope.productToAdd.expireDate = selectedBatch.expireDate;
                $scope.productToAdd.gstTotal = selectedBatch.gstTotal;
                $scope.productToAdd.igst = selectedBatch.igst;
                $scope.productToAdd.cgst = selectedBatch.cgst;
                $scope.productToAdd.sgst = selectedBatch.sgst;
                $scope.productToAdd.vat = selectedBatch.vat;
                $scope.productToAdd.batchNo = selectedBatch.batchNo;
                $scope.productToAdd.purchasePrice = selectedBatch.purchasePrice;
                $scope.productToAdd.sellingPrice = selectedBatch.sellingPrice;
                $scope.productToAdd.mrp = selectedBatch.mrp;
            }

        }

    };



    getTransfereNo();
    var addStockMsg = "";
    $scope.transfer = function () {
        $scope.ValidateIndentItem();
        if (addStockMsg == "") {
            $.LoadingOverlay("show");
            $scope.btnclicked = true;
            $scope.model.fromInstanceId = $scope.model.fromInstance.id;
            $scope.model.toInstanceId = $scope.model.toInstance.id;

            $scope.model.fromTinNo = $scope.model.fromInstance.gsTinNo;
            $scope.model.toTinNo = $scope.model.toInstance.gsTinNo;

            setTransferItem();
            for (var i = 0; i < $scope.model.stockTransferItems.length; i++) {

                if ($scope.model.packing == undefined || parseFloat($scope.model.packing) == "" || $scope.model.packing == null) {
                    if (parseFloat($scope.model.stockTransferItems[i].productStock.product.packing) > 0) {
                        $scope.model.packing = $scope.model.stockTransferItems[i].productStock.product.packing;
                    }

                }

            }
            $scope.model.transferItemCount = $scope.model.stockTransferItems.length;
            stockTransferService.stockTransfer($scope.model).then(function (x) {
                if (x.data.saveFailed && x.data.validateStockList != null) {
                    $scope.model.stockTransferItems = [];
                    ShowValidateStockWindow(x.data.validateStockList);
                    $scope.btnclicked = false;
                } else if (x.data.saveFailed) {
                    $scope.model.stockTransferItems = [];
                    toastr.error(x.data.errorLog.errorMessage, 'Error');
                    $scope.btnclicked = false;
                }

                else {
                    transferSuccess(x.data);
                }
                $.LoadingOverlay("hide");
            },
            function (error) {
                $scope.model.stockTransferItems = [];
                $scope.btnclicked = false;
                $.LoadingOverlay("hide");
            });
        }
        else {
            toastr.info(addStockMsg);
        }

    };

    function ShowValidateStockWindow(data) {
        var m = ModalService.showModal({
            "controller": "validateStockCtrl",
            "templateUrl": 'validateStockPopupModal',
            "inputs": { "params": [{ "list": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
            });
        });
    }

    $scope.ValidateIndentItem = function () {
        addStockMsg = "";
        var tmpTransferDetail = $scope.addedTransfer;

        for (var i = 0; i < tmpTransferDetail.length; i++) {
            if (tmpTransferDetail[i].purchasePrice > tmpTransferDetail[i].sellingPrice || tmpTransferDetail[i].sellingPrice == undefined) {
                //MRP Implementation by Settu
                addStockMsg += $scope.enableSelling ? "Selling price" : "MRP";
                addStockMsg += " must be greater than purchase Price.";
            }
            if ($scope.enableSelling && tmpTransferDetail[i].mrp < tmpTransferDetail[i].sellingPrice) {
                addStockMsg += " MRP must be greater than or equal to selling Price.";
            }
        }

        //Commented by Settu, due to price reset as empty when server throws stock validation exception
        //if (addStockMsg == "") {
        //    if ($scope.transferSettings) {
        //        for (var i = 0; i < tmpTransferDetail.length; i++) {
        //            tmpTransferDetail[i].purchasePrice = tmpTransferDetail[i].purchasePrice * tmpTransferDetail[i].packageSize;
        //            tmpTransferDetail[i].sellingPrice = tmpTransferDetail[i].sellingPrice * tmpTransferDetail[i].packageSize;
        //            tmpTransferDetail[i].mrp = tmpTransferDetail[i].mrp * tmpTransferDetail[i].packageSize;
        //        }
        //    }
        //}
    };

    $scope.delete = function (data) {
        if (!confirm('Sure to delete ?')) {
            return;
        }
        var index = $scope.addedTransfer.indexOf(data);
        this.addedTransfer.splice(index, 1);
        if (this.addedTransfer.length == 0) {
            $scope.disableNT = false;
            $scope.disableIT = false;
        } else {
            if ($scope.chkITItem()) {
                $scope.disableIT = false;
                $scope.disableNT = true;
            } else {
                $scope.disableIT = true;
                $scope.disableNT = false;
            }
        }


        var drugName = document.getElementById("drugName");
        drugName.focus();
    };

    $scope.keyEnter = function (event, e) {
        ele = document.getElementById(e);
        if (event.which === 13) // Enter key
        {
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
            {
                ele.focus();
            }
        }
    };

    $scope.focusNextField = function (event, nextid) {
        if ($scope.batchDisplaySetting == 2) {
            if (nextid !== '')
                ele = document.getElementById(nextid);

            if (event.which === 13 && nextid !== '') // Enter key
            {
                if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
                {
                    ele = document.getElementById(nextid);
                    ele.focus();
                }
            }
        } else {
            if (event.which === 13) {
                if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required && nextid !== '') {
                    if (nextid == 'unitPrice' || nextid == 'mrpUnit') {
                        ele = document.getElementById(nextid);
                        ele.focus();
                    } else {

                        var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.productToAdd.gstTotal) }, true);
                        if (taxList.length == 0) {
                            toastr.info("Invalid" + " " + $scope.productToAdd.gstTotal + " " + "% GST.Please update valid GST% in Inventory Data Update");
                            document.getElementById("gstTotal").focus();
                            return;
                        }
                        else {
                            $scope.addProduct();
                        }

                    }

                } else {
                    var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.productToAdd.gstTotal) }, true);
                    if (taxList.length == 0) {
                        toastr.info("Invalid" + " " + $scope.productToAdd.gstTotal + " " + "% GST.Please update valid GST% in Inventory Data Update");
                        document.getElementById("gstTotal").focus();
                        return;
                    } else {
                        $scope.addProduct();
                    }
                }

            }
        }
    };
    function transferSuccess(transferNo) {
        // alert('Transfer successful..!');
        toastr.success('Transfer Saved Successfully');
        $scope.addedTransfer.length = 0;
        $scope.reset();
        $scope.model = { stockTransferItems: [] };
        //Commented by settu for indent transfer with stock reducing
        //$scope.model.transferNo = transferNo;
        getTransfereNo();
        $scope.autoIndentTransfer = false;
        $scope.fromVendorPurchaseTransfer = false;
    }


    function setTransferItem() {
        for (var i = 0; i < $scope.addedTransfer.length; i++) {
            var item = {
                productStockId: $scope.addedTransfer[i].batch.id,
                packageSize: $scope.addedTransfer[i].batch.packageSize,
                quantity: $scope.addedTransfer[i].quantity,
                VendorOrderItemId: $scope.addedTransfer[i].VendorOrderItemId,
                VendorPurchaseItemId: $scope.addedTransfer[i].VendorPurchaseItemId,
                isStripTransfer: $scope.addedTransfer[i].isStripTransfer,
                gstTotal: $scope.addedTransfer[i].gstTotal,
                //MRP Implementation by Settu
                productStock: {
                    purchasePrice: $scope.addedTransfer[i].purchasePrice,
                    sellingPrice: $scope.addedTransfer[i].sellingPrice,
                    mrp: $scope.addedTransfer[i].mrp,

                    product: {
                        name: $scope.addedTransfer[i].batch.product.name,
                        packing: $scope.addedTransfer[i].batch.product.packageSize
                    }
                },
                //To update head office Id for stock transfer offline sync by Settu
                toInstanceId: $scope.model.toInstance.id
            };

            $scope.model.stockTransferItems.push(item);
        }
    }

    $scope.getNetValue = function () {
        var total = 0;

        for (var i = 0; i < $scope.addedTransfer.length; i++) {
            ////ProductStock.bind( $scope.addedTransfer[i].purchasePrice, $scope.addedTransfer[i].vat, $scope.addedTransfer[i].quantity) ;
            ProductStock.prototype.constructor($scope.addedTransfer[i].purchasePrice, $scope.addedTransfer[i].vat, $scope.addedTransfer[i].gstTotal, ($scope.transferSettings == true ? $scope.addedTransfer[i].quantity : $scope.addedTransfer[i].packageQty), $scope.addedTransfer[i].packageQty); //
            total += parseFloat(ProductStock.prototype.netValue()); //ProductStock.prototype.netValue());
            //total += parseFloat($scope.addedTransfer[i].netValue());
        }

        return total;
    };

    $scope.showIndentTransfer = function (toInstance) {

        vendorOrderService.getVendorOrder(null, false, toInstance).then(function (response) {
            $scope.itList = response.data;
            if ($scope.itList.length > 0) {

                $scope.showITItems();
            } else {
                toastr.info("No indent records found.");
            }
        }, function () {

        });
    };

    function getSelectedDcItem(DcPoItem) {

        //
        if (DcPoItem.ITItem.length > 0) {
            for (var n = 0; n < $scope.toBranchs.length; n++) {
                if (DcPoItem.ITItem[0].instanceId == $scope.toBranchs[n].id) {
                    $scope.model.toInstance = $scope.toBranchs[n];
                }
            }

        }

        var poItem = DcPoItem;
        for (var j = 0; j < poItem.ITItem.length; j++) {
            if (poItem.ITItem[j].isSelected == true) {
                $scope.productToAdd = new ProductStock(poItem.ITItem[j].purchasePrice, poItem.ITItem[j].product.vat, poItem.ITItem[j].product.gstTotal, poItem.ITItem[j].quantity, poItem.ITItem[j].packageQty);
                $scope.productToAdd.productStock = poItem.ITItem[j].productStock;
                $scope.productToAdd.selectedProduct = poItem.ITItem[j];
                $scope.productToAdd.selectedProduct.product.name = poItem.ITItem[j].productName;
                $scope.productToAdd.quantity = poItem.ITItem[j].quantity;
                $scope.productToAdd.packageSize = poItem.ITItem[j].packageSize;
                $scope.productToAdd.packageQty = poItem.ITItem[j].packageQty;
                $scope.productToAdd.batchNo = poItem.ITItem[j].productStock.batchNo;
                $scope.productToAdd.batch = poItem.ITItem[j].productStock;
                $scope.productToAdd.expireDate = poItem.ITItem[j].productStock.expireDate;
                $scope.productToAdd.gstTotal = poItem.ITItem[j].productStock.gstTotal;
                $scope.productToAdd.igst = poItem.ITItem[j].productStock.igst;
                $scope.productToAdd.cgst = poItem.ITItem[j].productStock.cgst;
                $scope.productToAdd.sgst = poItem.ITItem[j].productStock.sgst;
                $scope.productToAdd.purchasePrice = poItem.ITItem[j].purchasePrice;
                $scope.productToAdd.PackageSellingPrice = poItem.ITItem[j].packageSellingPrice;
                //MRP Implementation by Settu
                $scope.productToAdd.PackageMRP = poItem.ITItem[j].packageSellingPrice;
                $scope.productToAdd.sellingPrice = poItem.ITItem[j].sellingPrice;
                $scope.productToAdd.mrp = poItem.ITItem[j].sellingPrice;
                $scope.productToAdd.vat = poItem.ITItem[j].product.vat;
                $scope.productToAdd.VendorOrderItemId = poItem.ITItem[j].id;
                $scope.productToAdd.AccountId = poItem.ITItem[j].AccountId;
                $scope.productToAdd.TransferQtyType = poItem.ITItem[j].transferQtyType;
                $scope.productToAdd.isStripTransfer = $scope.transferSettings;
                //ProductStock($scope.productStock.purchasePrice, $scope.productStock.vat, $scope.productStock.quantity);
                $scope.addedTransfer.push($scope.productToAdd);
            }
        }

        if ($scope.chkITItem()) {
            $scope.disableIT = false;
            $scope.disableNT = true;
        } else {
            $scope.disableIT = true;
            $scope.disableNT = false;
        }

        $scope.getSettings();

        $.LoadingOverlay("hide");
    }

    $scope.showITItems = function () {

        var itList = $scope.itList;
        //
        if (itList.length > 0) {
            var m = ModalService.showModal({
                "controller": "showIndentTransferCtrl",
                "templateUrl": "showIndentTransfer",
                "inputs": { "ITValue": [{ "itList": itList }] }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {

                    if (result.status == "Yes") {
                        getSelectedDcItem(result.data);
                        $scope.disableAddNewDc = true;
                        $scope.productToAdd = new ProductStock(0, 0, 0, 0, 0);
                        $scope.productToAdd.quantity = null;
                    } else {
                        //var prod = document.getElementById("invoiceNo");
                        //prod.focus();
                    }
                });
            });
            return false;
        }

    };

    //$scope.callculateNet = function ()
    //{
    //    if ($scope.productToAdd.isNew == true & $scope.EditMode == true) {
    //        ProductStock.prototype.constructor($scope.productToAdd.purchasePrice, $scope.productToAdd.vat, $scope.productToAdd.quantity, $scope.p);
    //        return ProductStock.prototype.netValue();
    //    }
    //}

    $scope.EditMode = false;

    //(productToAdd.TransferQtyType == true ? (productToAdd.quantity) : productToAdd.packageQty)

    //$scope.TransferQty = 0;
    $scope.editPurchase = function (item, ind) {

        $scope.productToAdd = new ProductStock(item.purchasePrice, item.vat, item.gstTotal, item.quantity, item.packageQty);

        //$scope.productToAdd = item.productStock;
        $scope.productToAdd.selectedProduct = item.selectedProduct;
        //$scope.productToAdd.selectedProduct.product.name = item.productName;

        $scope.productToAdd.quantity = item.quantity;
        $scope.productToAdd.packageSize = item.packageSize;
        $scope.productToAdd.packageQty = item.packageQty;
        //if (item.TransferQtyType == true)
        //{
        //    $scope.productToAdd.quantity = item.quantity * item.packageSize;
        //}

        $scope.productToAdd.batchNo = item.batchNo;
        if ($scope.autoIndentTransfer) {
            item.selectedProduct.productStock = item.batch;
        }
        $scope.productToAdd.batch = item.selectedProduct.productStock;
        $scope.productToAdd.expireDate = item.selectedProduct.productStock.expireDate;
        $scope.productToAdd.gstTotal = item.gstTotal;
        $scope.productToAdd.igst = item.selectedProduct.productStock.igst;
        $scope.productToAdd.cgst = item.selectedProduct.productStock.cgst;
        $scope.productToAdd.sgst = item.selectedProduct.productStock.sgst;
        $scope.productToAdd.purchasePrice = item.purchasePrice;
        $scope.productToAdd.sellingPrice = item.sellingPrice;
        //MRP Implementation by Settu
        $scope.productToAdd.mrp = item.mrp;
        $scope.productToAdd.vat = item.vat;

        if ($scope.transferSettings == true) {
            $scope.TransferQty = $scope.productToAdd.quantity;
        } else {
            $scope.TransferQty = $scope.productToAdd.packageQty;
        }
        //$scope.productToAdd.selectedProduct.productStock.vendorOrderItemId = item.vendorOrderItemId;
        //
        $scope.productToAdd.isNew = true;
        $scope.productToAdd.VendorOrderItemId = item.VendorOrderItemId;

        $scope.selectedProduct = item.selectedProduct;
        //$scope.productToAdd.selectedProduct.productStock.vendorOrderItemId = item.vendorOrderItemId;
        $scope.EditMode = true;

        $scope.Editindex = ind;

        //Added by Settu for indent transfer with stock reducing
        if ($scope.autoIndentTransfer) {
            $scope.productToAdd.batch = item.batch;
            $scope.productToAdd.psBatchs = item.psBatchs;
            $scope.batchs = $scope.productToAdd.psBatchs;
        }
        if ($scope.fromVendorPurchaseTransfer) {
            $scope.productToAdd.VendorPurchaseItemId = item.VendorPurchaseItemId;
        }
    };

    $scope.productToAdd.TransferQtyType = true;
    $scope.getSettings = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        stockTransferService.getSettings().then(function (response) {
            $scope.transferSettings = response.data.transferQtyType;
            $scope.productToAdd.TransferQtyType = $scope.transferSettings;
            $scope.showExpDateQty = response.data.showExpiredQty;
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");

        }, function (error) {
            console.log(error);
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        });
    };

    $scope.getSettings();


    $scope.asignQty = function () {


        $scope.productToAdd.quantity = parseInt($scope.TransferQty);
        //$scope.productToAdd.packageQty = parseInt($scope.TransferQty);
        $scope.productToAdd.TransferQtyType = $scope.transferSettings;
        if (!$scope.productToAdd.TransferQtyType && parseInt($scope.TransferQty) > 0 && $scope.EditMode) {
            $scope.productToAdd.quantity = parseInt($scope.TransferQty) * $scope.productToAdd.packageSize;
            $scope.productToAdd.packageQty = parseInt($scope.TransferQty);
            $scope.productToAdd.selectedProduct.packageSize = $scope.productToAdd.batch.packageSize;
        } else if (!$scope.productToAdd.TransferQtyType && parseInt($scope.TransferQty) > 0 && !$scope.EditMode) {
            $scope.productToAdd.quantity = parseInt($scope.TransferQty) * ($scope.productToAdd.packageSize != undefined ? $scope.productToAdd.packageSize : $scope.productToAdd.batch.packageSize);
            $scope.productToAdd.packageQty = parseInt($scope.TransferQty);
            $scope.productToAdd.selectedProduct.packageSize = ($scope.productToAdd.packageSize != undefined ? $scope.productToAdd.packageSize : $scope.productToAdd.batch.packageSize);
        }

    };

    $scope.isNumber = function (val) {
        return isNaN(val);
    };

    $scope.availStrip = function (a, b) {
        var c = a / b;
        if (c < 1) {
            return 0;
        } else {
            return parseInt(c);
        }

    };

    //MRP Implementation by Settu
    $scope.getEnableSelling = function () {
        stockTransferService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.getEnableSelling();

    function setDefaultValues() {
        var tempBatch = [];
        for (var i = 0; i < $scope.batchs.length; i++) {
            $scope.batchs[i].availableQty = $scope.getAvailableStock($scope.batchs[i]);
            if ($scope.batchs[i].packageSize == null || $scope.batchs[i].packageSize < 1)
                $scope.batchs[i].packageSize = 1;

            if ($scope.batchs[i].purchasePrice.toString().indexOf('.') != -1)
                $scope.batchs[i].purchasePrice = Math.round($scope.batchs[i].purchasePrice * 100) / 100;

            if ($scope.batchs[i].vatInPrice.toString().indexOf('.') != -1)
                $scope.batchs[i].vatInPrice = Math.round($scope.batchs[i].vatInPrice * 100) / 100;

            if ($scope.batchs[i].availableQty > 0)
                tempBatch.push($scope.batchs[i]);
        }
        return tempBatch;
    }

    function loadIndentStockTransfer() {
        if ($scope.indentStockTransfer != null) {
            stockTransferService.getSettings().then(function (response) {
                $scope.autoIndentTransfer = true;
                $scope.transferSettings = response.data.transferQtyType;
                $scope.productToAdd.TransferQtyType = $scope.transferSettings;

                for (var i = 0; i < $scope.indentStockTransfer.vendorOrderItem.length; i++) {
                    if ($scope.indentStockTransfer.vendorOrderItem[i].productStock.stock > 0) {

                        $scope.batchs = $scope.indentStockTransfer.vendorOrderItem[i].productStockList;
                        var tempBatch = setDefaultValues();
                        $scope.batchs = tempBatch;

                        var requestedQty = $scope.indentStockTransfer.vendorOrderItem[i].quantity - $scope.indentStockTransfer.vendorOrderItem[i].receivedQty;

                        for (var j = 0; j < tempBatch.length && requestedQty > 0; j++) {

                            var addedStock = getAddedStock(tempBatch[j].id);
                            var balanceStock = tempBatch[j].stock - addedStock;

                            if (balanceStock > 0) {
                                $scope.productToAdd.selectedProduct = $scope.indentStockTransfer.vendorOrderItem[i].productStock;
                                $scope.batchs = tempBatch;
                                if (!$scope.transferSettings) {
                                    tempBatch[j].purchasePrice = tempBatch[j].purchasePrice * (tempBatch[j].packageSize);
                                    tempBatch[j].sellingPrice = tempBatch[j].sellingPrice * (tempBatch[j].packageSize);
                                    tempBatch[j].mrp = tempBatch[j].mrp * (tempBatch[j].packageSize);
                                }
                                $scope.productToAdd.batch = tempBatch[j];
                                $scope.batchSelected($scope.productToAdd.batch);

                                if (!$scope.transferSettings) {
                                    $scope.TransferQty = balanceStock / tempBatch[j].packageSize;
                                    $scope.TransferQty = Math.floor($scope.TransferQty);
                                    if (($scope.TransferQty * tempBatch[j].packageSize) > requestedQty) {
                                        $scope.TransferQty = Math.ceil(requestedQty / tempBatch[j].packageSize);
                                        if (($scope.TransferQty * tempBatch[j].packageSize) > balanceStock) {
                                            $scope.TransferQty = Math.floor(requestedQty / tempBatch[j].packageSize);
                                        }
                                    }
                                    requestedQty = requestedQty - ($scope.TransferQty * tempBatch[j].packageSize);
                                } else {
                                    $scope.TransferQty = balanceStock;
                                    if ($scope.TransferQty > requestedQty)
                                        $scope.TransferQty = requestedQty;
                                    requestedQty = requestedQty - $scope.TransferQty;
                                }
                                $scope.productToAdd.VendorPurchaseItemId = $scope.indentStockTransfer.vendorOrderItem[i].vendorPurchaseItemId;
                                $scope.productToAdd.VendorOrderItemId = $scope.indentStockTransfer.vendorOrderItem[i].id;

                                //$scope.productToAdd.selectedProduct.productStock = $scope.productToAdd.batch; //This is not working in edit
                                $scope.productToAdd.psBatchs = $scope.batchs;

                                $scope.asignQty();
                                $scope.addProduct();
                            }
                        }
                    }
                }
                if ($scope.toBranchs.length == 0) {
                    stockTransferService.GetInstancesByAccountId().then(function (response) {
                        $scope.toBranchs = response.data;
                        setSameGSTinToInstance();
                        setToInstanceValues();
                        $scope.model.toInstance = $filter("filter")($scope.toBranchs, { id: $scope.indentStockTransfer.instance.id }, true)[0];
                    });
                } else {
                    $scope.model.toInstance = $filter("filter")($scope.toBranchs, { id: $scope.indentStockTransfer.instance.id }, true)[0];
                }
                $scope.reset();
                if ($scope.transferSettings || $scope.addedTransfer.length > 0) {
                    //toastr.info("Stock available items are loaded successfully for indent transfer (OrderId: " + $scope.indentStockTransfer.orderId + ").");
                } else if ($scope.addedTransfer.length == 0) {
                    toastr.info("Indent items are loading failed since due to none of batch don't have stock in strip based transfer.");
                }
            });
        }
    }

    function setToInstanceValues1() {
        for (var i = 0; i < $scope.toBranchs.length; i++) {
            if ($scope.toBranchs[i].area != undefined && $scope.toBranchs[i].area.length > 30) {
                $scope.toBranchs[i].name = ($scope.toBranchs[i].name + (($scope.toBranchs[i].area == undefined) ?
                    "" : ", " + $scope.toBranchs[i].area)).substring(0, 60);
            } else if ($scope.toBranchs[i].area == undefined || $scope.toBranchs[i].area == null || $scope.toBranchs[i].area == "") {
                $scope.toBranchs[i].name = $scope.toBranchs[i].name;
            } else {
                $scope.toBranchs[i].name = ($scope.toBranchs[i].name + ", " + $scope.toBranchs[i].area).substring(0, 60);
            }
        }
    }

    function setToInstanceValues() {
        for (var i = 0; i < $scope.toBranchs.length; i++) {
            if ($scope.toBranchs[i].area != undefined && $scope.toBranchs[i].area.length > 60) {
                $scope.toBranchs[i].name = ((($scope.toBranchs[i].area == undefined) ?
                    "" : ", " + $scope.toBranchs[i].area)).substring(0, 60);
            } else if ($scope.toBranchs[i].area == undefined || $scope.toBranchs[i].area == null || $scope.toBranchs[i].area == "") {
                $scope.toBranchs[i].name = $scope.toBranchs[i].name;
            } else {
                $scope.toBranchs[i].name = ($scope.toBranchs[i].area).substring(0, 60);
            }
        }
    }
    //Need to add all startup methods in this init function
    $scope.init = function () {
        loadIndentStockTransfer();
    };
    $scope.init();

    function setSameGSTinToInstance() {
        var array = [];
        for (var i = 0; i < $scope.toBranchs.length; i++) {
            if ($scope.toBranchs[i].gsTinNo != "") {
                if ($scope.model.fromInstance.gsTinNo == $scope.toBranchs[i].gsTinNo) {
                    array.push($scope.toBranchs[i]);
                }
            }
        }
        $scope.toBranchs = array;
    }

});