﻿app.factory('shortcutHelper', function ($rootScope) {
    var scope;
    return {
        "setScope": function (parentScope) { scope = parentScope },
        "salesShortcuts": function (e) {
            // F1
            if (e.keyCode == 112) {
                event.preventDefault();
                document.getElementById("customerName").focus();
                if (scope.isShortCustomerName == true) {
                    document.getElementById("shortCustomerName").focus();
                }
            }
            //F2
            if (e.keyCode == 113) {
                event.preventDefault();
                document.getElementById("doctorName").focus();
                if (scope.isShortDoctorName == true) {
                    document.getElementById("shortDoctorname").focus();
                }
            }
            //F3
            if (e.keyCode == 114) {
                event.preventDefault();
                if (scope.sales.discountType == 1) {
                    document.getElementById("txtDiscount").focus();
                } else {
                    document.getElementById("txtDiscountValue").focus();
                }
            }
            //Ctrl + 
            /*if (e.keyCode ==  && e.ctrlKey == true) {
                event.preventDefault();
                document.getElementById("discountType").focus();
            }
            */
            //F4
            if (e.keyCode == 115) {
                event.preventDefault();
                document.getElementById("receivedAmount").focus();
            }
            //F5
            if (e.keyCode == 116) {
                event.preventDefault();
                scope.sales.cashType = "Credit";
                scope.sales.paymentType = "Cash";
                scope.focusCreditTextBox = true;
                scope.focusCardTextbox = false;
                scope.chequeCustomerRequired = false;
                document.getElementById("caseType").click();
            }
            //F6
            if (e.keyCode == 117) {
                event.preventDefault();
                if (scope.FinalNetAmount < 0) // Added by settu to disable cheque/card option in credit sales return
                    return;
                scope.sales.paymentType = "Card";
                scope.focusCardTextbox = true;
                scope.chequeCustomerRequired = false;
                scope.iscashtypevalid = false;
                document.getElementById("PayModeCard").click();
                //scope.editCheque(scope.sales, true); //No where Use this
            }
            //F7
            if (e.keyCode == 118) {
                event.preventDefault();
                if (scope.FinalNetAmount < 0) // Added by settu to disable cheque/card option in credit sales return
                    return;
                scope.sales.paymentType = "Cheque";
                scope.iscashtypevalid = false;
                document.getElementById("PayModeCheque").click();
            }
            //F8
            if (e.keyCode == 119) {
                event.preventDefault();
                document.getElementById("discount").focus();
            }
            //F9
            if (e.keyCode == 120) {
                event.preventDefault();
                document.getElementById("sellingPrice").focus();
            }
            //F10
            if (e.keyCode == 121) {
                event.preventDefault();
                document.getElementById("reminder").focus();
            }
            //F11
            if (e.keyCode == 122) {
                event.preventDefault();
                document.getElementById("saleType").focus();
            }
            //ctrl+D
            if (e.keyCode == 68 && e.ctrlKey == true) {
                event.preventDefault();
                scope.sales.deliveryType = "Home Delivery";
                document.getElementById("homeDelivery").click();
                //scope.saleType(true); //No where Use this
            }
            //ctrl+S
            if (e.keyCode == 83 && e.ctrlKey == true) {
                event.preventDefault();
                var myId = $("#shortNameCtrl").val();
                if (myId == "true") {
                    if (scope.isShortCustomerName == true) {
                        scope.isShortCustomerName = false;
                        var shortCustname = document.getElementById("shortCustomerName").value;
                        if (shortCustname == "") {
                            document.getElementById("shortCustomerName").focus();
                        }
                    } else {
                        scope.isShortCustomerName = true;
                        //document.getElementById("customerName").focus();
                        var shortCustname = document.getElementById("customerName").value;
                        if (shortCustname == "") {
                            document.getElementById("customerName").focus();
                        }
                    }
                }
                var myId1 = $("#shortDoctorNameCtrl").val();
                if (myId1 == "true") {
                    if (scope.isShortDoctorName == true) {
                        scope.isShortDoctorName = false;
                        // document.getElementById("shortDoctorname").focus();
                    } else {
                        scope.isShortDoctorName = true;
                        // document.getElementById("doctorName").focus();
                    }
                }
                scope.GetDefaultDoctor();
                var ele = document.getElementById("shortCustomerName");
                ele.focus();
            }
            //ctrl+G  // need to copy data arun
            if (e.keyCode == 71 && e.ctrlKey == true) {
                event.preventDefault();
                if (document.getElementById('drugName').disabled != true) {
                    if (scope.isProductGenericSearch == true) {
                        scope.isProductGenericSearch = false;
                        if (scope.isProductGenericSearch == false) {
                            if (document.getElementById("drugName").value != "" || document.getElementById("genericName").value != "") {
                                document.getElementById("drugName").value = "";
                                document.getElementById("genericName").value = "";
                                scope.CheckGenericName();
                            }
                            scope.focusGenericSearch = true;
                            scope.focusProductSearch = false;
                        }
                    } else {
                        scope.isProductGenericSearch = true;
                        if (scope.isProductGenericSearch == true) {
                            if (document.getElementById("drugName").value != "" || document.getElementById("genericName").value != "") {
                                document.getElementById("drugName").value = "";
                                document.getElementById("genericName").value = "";
                                scope.CheckName();
                            }
                            scope.focusProductSearch = true;
                            scope.focusGenericSearch = false;
                        }
                    }
                }
            }
            //Escape or END
            if (document.getElementById("saveType").value == 2) {
                //End
                if (e.keyCode == 35) {
                    event.preventDefault();
                    scope.completeSaleKeyPress();
                }
            } else {
                var completeSaleKeyCode = 27;
                // added by arun for closing the branch popup by pressing esc on 6jun 2017
                if (e.keyCode == 27 && scope.disableEsc) {
                    $rootScope.$emit("stockbranch", {});
                    scope.disableEsc = false;
                }
                else if (e.keyCode == 27 && scope.salesTemplateEsc) {
                    $rootScope.$emit("returnTemplatePopupEvent", {});
                    scope.salesTemplateEsc = false;
                }
                else if (e.keyCode == 27 && scope.salesOrderEstimateEsc) {
                    $rootScope.$emit("returnOrderEstimatePopupEvent", {});
                    scope.salesOrderEstimateEsc = false;
                }
                else if (e.keyCode == 27 && scope.disableEsc == false && scope.salesTemplateEsc == false && scope.salesOrderEstimateEsc == false) {
                    event.preventDefault();
                    scope.completeSaleKeyPress();
                }
            }
            //ctrl+r
            if (e.keyCode == 82 && e.ctrlKey == true) {
                event.preventDefault();
            }
            //ctrl+a
            if (e.keyCode == 65 && e.ctrlKey == true) {
                event.preventDefault();
                if (scope.isProductGenericSearch == true) {
                    document.getElementById("drugName").focus();
                } else {
                    document.getElementById("genericName").focus();
                }
            }
            //ctrl + b
            if (e.keyCode == 66 && e.ctrlKey == true) {
                event.preventDefault();
                scope.printBill();
            }
            //ctrl + m  - Added by Sarubala on 09-10-17
            if (e.keyCode == 77 && e.ctrlKey == true) {
                event.preventDefault();
                scope.sales.cashType == "MultiplePayment"
                scope.sales.paymentType = "Multiple";
                scope.sales.cardNo = "";
                scope.sales.cardDate = "";
                scope.sales.cardDigits = "";
                scope.sales.cardName = "";
                scope.sales.credit = "";
                scope.Iscredit = false;
                scope.iscashtypevalid = false;
                document.getElementById("MultiplePayment").click();
            }
            //ctrl + t
            if (e.keyCode == 89 && e.ctrlKey == true) {
                event.preventDefault();
                scope.showStockBranchWise();
            }
        },
        "salesReurnShortcuts": function (e) {
            if (document.getElementById("saveType").value == 2) {
                var ele = angular.element(document.getElementById("RtnSale"))[0].disabled;
                if (e.keyCode == 35 && ele == false) { // && ele == false
                    e.preventDefault();
                    document.getElementById("RtnSale").click();
                }
            } else {
                var ele = angular.element(document.getElementById("RtnSale"))[0].disabled;
                if (e.keyCode == 27 && ele == false) {
                    e.preventDefault();
                    document.getElementById("RtnSale").click();
                }
            }
            if (e.keyCode == 67 && e.ctrlKey == true) {
                scope.cancel();
            }
        },
        "alternateProductSearchShortcuts": function (e) {
            //Ctrl+p
            if (e.keyCode == 80 && e.ctrlKey == true) {
                event.preventDefault();
                scope.focusProduct = true;
            }
            //Ctrl+g
            if (e.keyCode == 71 && e.ctrlKey == true) {
                event.preventDefault();
                scope.focusGenericName = true;
            }
        }
    }
});
