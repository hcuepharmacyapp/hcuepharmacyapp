﻿app.controller('salesSearchCtrl', function ($scope, $filter, salesService,toolService, toastr, salesModel, pagerServcie, productService, productModel, salesReturnModel,
    salesReturnItemModel, printingHelper, patientService) {

    $scope.IsOffline = false;
    $scope.enableSelling = false;
    $scope.isValidInvoiceNo = false;
    $scope.productSelect = false;
    $scope.doctorSelect = false;
    $scope.isComposite = false;

    getOfflineStatus = function () {
        salesService.getOfflineStatus().then(function (response) {
            if (response.data) {
                //toastr.error("This is for online mode");
                $scope.IsOffline = true;
                $scope.offlineGreyButton = "offline-grey-button";
                $scope.paidMsg = $scope.offlineMsg;
            }
            else {
                $scope.IsOffline = false;
            }
        });
    };
    getOfflineStatus();

    var sales = salesModel;

    var salesReturn = salesReturnModel;
    var salesReturnItem = salesReturnItemModel;

    $scope.salesReturn = salesReturn;
    $scope.salesReturn.salesReturnItem = salesReturnItem;

    $scope.search = sales;
    $scope.list = [];
    $scope.IsEdit = false;
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination
    $scope.salesCondition = false;
    $scope.dateCondition = false;
    $scope.productCondition = false;
    $scope.search.address = "";
    var flag = false;

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'//newly added
    };


    $scope.CancelBillDays = 0;
    getCancelBillDays();
    function getCancelBillDays() {
        salesService.getCancelBillDays().then(function (response) {
           // console.log(JSON.stringify(response));
            if (response.data != "" && response.data != null) {
                $scope.CancelBillDays = response.data.postCancelBillDays;
               // console.log($scope.CancelBillDays);
            }
        }, function () {

        });
    }

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    function pageSearch() {
        $.LoadingOverlay("show");
        salesService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.init = function (val) {
        if (val == "True") {
            flag = true;
            $scope.search.select = 'billNo';
            $scope.InvoiceSeriesType = 'Custom';
            $scope.changefilters();
            $scope.ChangeInvoiceSeriesDropdown($scope.InvoiceSeriesType);
            $("#InvoiceType").val("Custom");
            $scope.search.select1 = "All";
            $scope.salesSearch();
            $scope.search.select1 = "top10";
            $scope.ispageload = 1;
        }
        else {
            flag = false;
            $scope.pageload();
        }
    }

    $scope.salesSearch = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        $scope.search.searchProductId = "";
        if ($scope.search.drugName)
            $scope.search.values = $scope.search.drugName.id;


       // console.log($scope.search);
        salesService.list($scope.search).then(function (response) {
            if (response.data.list == undefined) {
                $scope.list = [];
            } else {
                $scope.list = response.data.list;
                $scope.isComposite = $scope.list[0].gstselect;
            }

            if ($scope.search.select1 == "top10") {
                $scope.search.select1 = "equal";

            }
            $scope.showIstenRecords = false;
            if ($scope.ispageload == 1) {
                $scope.showIstenRecords = true;
                $scope.ispageload = 0;
            }
           
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.salesReturnDetails = function (id) {

        $scope.salesReturn.salesId = id;
        salesService.salesReturnDetail($scope.salesReturn.salesId).then(function (response) {
            $scope.salesReturn = response.data;
            var checkquantity = 0;
            for (var x = 0; x < $scope.salesReturn.salesReturnItem.length; x++) {

                var Reorderquntity = 0;
                if ($scope.salesReturn.salesReturnItem[x].returnedQuantity == undefined || $scope.salesReturn.salesReturnItem[x].returnedQuantity == null || $scope.salesReturn.salesReturnItem[x].returnedQuantity == "") {
                    Reorderquntity = 0;
                } else {
                    Reorderquntity = $scope.salesReturn.salesReturnItem[x].returnedQuantity
                }
                $scope.salesReturn.salesReturnItem[x].quantity = $scope.salesReturn.salesReturnItem[x].purchaseQuantity - Reorderquntity;

                if ($scope.salesReturn.salesReturnItem[x].quantity != 0) {
                    checkquantity++;
                    window.location = window.location.origin + "/salesReturn/index?id=" + id;
                    return;
                }
                
            }
            if (checkquantity == 0) {

               // alert("All products, full quantity returned");
                $.alert({
                    title: 'Return is not possible!',
                    content: 'It seems all products are returned in this bill',
                    animation: 'center',
                    closeAnimation: 'center',
                    backgroundDismiss: true,
                    buttons: {
                        okay: {
                            text: 'Ok [Esc]',
                            btnClass: 'primary-button custom-transform',
                            action: function () {
                            }
                        }
                    }
                });

            }
           
        }, function () {
        });
    }

    $scope.salesEditDetails = function (id) {

        $scope.salesReturn.salesId = id;
        salesService.salesReturnDetail($scope.salesReturn.salesId).then(function (response) {
            $scope.salesReturn = response.data;
            if ($scope.salesReturn.patient.status != null && ($scope.salesReturn.patient.status == 2 || $scope.salesReturn.patient.status == 3)) {
                toastr.info("Edit Restricted for Inactive/Hide Customer.. Please change the Customer status", 'Customer InActive/Hide');
                return;
            }
            var checkquantity = 0;
            for (var x = 0; x < $scope.salesReturn.salesReturnItem.length; x++) {

                var Reorderquntity = 0;
                if ($scope.salesReturn.salesReturnItem[x].returnedQuantity == undefined || $scope.salesReturn.salesReturnItem[x].returnedQuantity == null || $scope.salesReturn.salesReturnItem[x].returnedQuantity == "") {
                    Reorderquntity = 0;
                } else {
                    Reorderquntity = $scope.salesReturn.salesReturnItem[x].returnedQuantity
                }
                $scope.salesReturn.salesReturnItem[x].quantity = $scope.salesReturn.salesReturnItem[x].purchaseQuantity - Reorderquntity;

                if ($scope.salesReturn.salesReturnItem[x].quantity != 0) {
                    checkquantity++;
                    window.location = window.location.origin + "/sales/index?id=" + id;
                    return;
                }
            }
            if (checkquantity == 0) {
                
                $.alert({
                    title: 'Edit is not possible!',
                    content: 'It seems all products are returned in this bill',
                    animation: 'center',
                    closeAnimation: 'center',
                    backgroundDismiss: true,
                    buttons: {
                        okay: {
                            text: 'Ok [Esc]',
                            btnClass: 'primary-button custom-transform',
                            action: function () {
                                // do nothing
                            }
                        }
                    }
                });
               

            }
          
        }, function () {
        });
    }

    $scope.ispageload = 0;
    $scope.pageload = function () {
        $scope.search.select = "";
        $scope.search.PatientId = "";
        //$scope.search.select1 = 'equal';
        $scope.search.select1 = "top10";
        $scope.salesCondition = false;
        $scope.dateCondition = false;
        $scope.search.values = "";
        $scope.search.selectValue = "";
        $scope.maxDate = new Date();
        $scope.showIstenRecords = false; //Variable added to implement the top 10records on pageload and clear
        $scope.ispageload = 1;
        $scope.productCondition = false;
        $scope.CustomerCondition = false;
        //$scope.DoctorCondition = false;
        $scope.EnterCondition = false;
        $scope.invoicenumberCondition = false;
        $scope.salesSearch();
        //$scope.search.select1 = "equal";
    }

    //
    $scope.InvoiceSeriesCondition = false;
    $scope.changefilters = function () {
        $scope.search.select1 = "";
        $scope.search.PatientId = "";
        $scope.search.values = "";
        $scope.search.invoiceDate = "";
        $scope.search.drugName = undefined;
        $scope.doctorSelect = false;

        if ($scope.search.select == 'billNo') {
            $scope.InvoiceSeriesCondition = true;
        } else {
            $scope.InvoiceSeriesCondition = false;
            $scope.InvoiceSeriesType = "Default";
        }
 
        if ($scope.search.select == 'quantity' || $scope.search.select == 'mrp' || $scope.search.select == 'discount' || $scope.search.select == 'discountValue' || $scope.search.select == 'vat' || $scope.search.select == 'invValue') {
            $scope.salesCondition = true;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = false;          
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = true;
            $scope.invoicenumberCondition = false;
            $scope.search.select1 = "equal";
        } else if ($scope.search.select == 'billDate') {
            $scope.salesCondition = false;
            $scope.dateCondition = true;
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.maxDate = new Date();
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
        } else if ($scope.search.select == 'expiry') {
            $scope.salesCondition = false;
            $scope.dateCondition = true;
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.maxDate = "";
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
        } else if ($scope.search.select == 'product') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = true;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
        }
            // Added by arun for customer name on 22nd march by punithavel req.
        else if ($scope.search.select == 'customerName') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = true;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
        }

        else if ($scope.search.select == 'doctorName') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            $scope.EnterCondition = true;
            //$scope.DoctorCondition = false;
        }

        else if ($scope.search.select == 'batchNo' || $scope.search.select == 'customerMobile') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = true;
            $scope.invoicenumberCondition = false;
            $scope.InvoiceSeriesType = "Default";


        }
        else if ($scope.search.select == 'billNo') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = true;
            $scope.search.select1 = "Default";
        }
        
        else {

            $scope.search.name = "";
            $scope.search.PatientId = "";
            $scope.search.mobile = "";
            $scope.search.invoiceNo = "";
            $scope.search.select = "";
            $scope.search.values = "";
            $scope.search.selectValue = "";
            $scope.maxDate = new Date();
            $scope.search.productvalues = "";
            $scope.search.drugName = "";
            $scope.InvoiceSeriesCondition = false;
            $scope.InvoiceSeriesType = "Default";
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.EnterCondition = false;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.search.select1 = "top10"
            $scope.showIstenRecords = false;
            $scope.invoicenumberCondition = false;
            $scope.ispageload = 1;

            $scope.salesSearch();

        }
    };

    $scope.InvoiceSeriesType = "Default";

    $scope.departmentChangefilters = function (val) {
        $scope.search.select1 = "";
        $scope.search.values = "";
        $scope.search.address = "";
        if (val == undefined) {
            $scope.search.select = "billNo";
            $scope.InvoiceSeriesCondition = true;
            $scope.InvoiceSeriesType = "Custom";
        }
        else if (val == 'billNo') {
            $scope.search.select = "billNo";
            $scope.InvoiceSeriesCondition = true;
            $scope.InvoiceSeriesType = "Custom";
            $scope.isValidInvoiceNo = false;
        }
        else {
            $scope.search.select = val;
            $scope.InvoiceSeriesCondition = false;
            $scope.returnStatus = true;
            $scope.isValidInvoiceNo = false;
        }

        salesService.getInvoiceSeriesItems().then(function (response) {

            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }
            $scope.invoicenumberCondition = true;
        }, function () {

        });
    }

    $scope.ChangeInvoiceSeriesDropdown = function (seriestype) {


        $scope.InvoiceSeriesType = seriestype;

        $scope.search.select1 = "";

        if ($scope.InvoiceSeriesType == 'Default') {
            $scope.search.select1 = "Default";
            $scope.search.values = "";
        }
        if ($scope.InvoiceSeriesType == 'Custom') {
            getInvoiceSeriesItems();
            $scope.search.values = "";
        }
        if ($scope.InvoiceSeriesType == 'Manual') {
            $scope.search.select1 = "MAN";
            $scope.search.values = "";
        }

       // console.log($scope.InvoiceSeriesType);
        if ($scope.InvoiceSeriesType == 'CustomerCredit') {
            $scope.search.select1 = "CRE";
            $scope.search.values = "";
        }

    }

    $scope.changeCustomerName = function () {
        var custname = document.getElementById("customerName").value;
      
        if ($scope.sales == undefined)
            return;

        if ($scope.sales.salesItem.length > 0) {
            if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
                if (custname == "") {
                    $scope.iscustomerNameMandatory = true;
                } else {
                    $scope.iscustomerNameMandatory = false;
                    $scope.flagCustomername = false;
                }
            }
        }

    };

    // Exact Search on the Doctor name and autocomplete textbox as the doctor name
    $scope.onProductSelection = function (obj) {
        if (obj == undefined)
            $scope.productSelect = true;
        else
            $scope.productSelect = false;
    }

    $scope.productSelectiontChange = function (obj) {
        if (obj == undefined)
            $scope.productSelect = true;
        else
            $scope.productSelect = false;
    }

    $scope.onDoctorSelection = function (val) {
        if (val == undefined)
            $scope.doctorSelect = true;
        else
        {
            $scope.doctorSelect = false;
            $scope.productSelect = false;
            $scope.search.values = val.name;
        }
    }

    $scope.doctorSelectionChange = function (val) {
        if (val == undefined)
            $scope.doctorSelect = true;
        else
            $scope.doctorSelect = false;
    }

    $scope.doctNameSearch = function (val) {

        return salesService.localList(val).then(function (response) {

            var origArr = response.data;
            var newArr = [], origLen = origArr.length, found, x, y;
            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }
            return newArr.map(function (item) {
                return item;
            });
        }, function (error) {
            console.log(error);
        });

    }

    $scope.changeDoctorName = function () {
        var doctname = document.getElementById("DoctorName").value;

        //console.log(doctname);

        if ($scope.sales.salesItem.length > 0) {
            if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
                if (doctname == "") {
                    $scope.isDoctorNameMandatory = true;
                } else {
                    $scope.isDoctorNameMandatory = false;
                    $scope.flagDoctorName = false;
                }
            }
        }

    };

    $scope.Namecookie = "";
    $scope.getPatient = function (val) {
        // $scope.customerHelper.data.patientSearchData.name = val;
        $scope.Namecookie = val;

        return patientService.GetSalesPatientName(val, 1).then(function (response) {

            var origArr = response.data;
            var newArr = [],
       origLen = origArr.length,
       found, x, y;

            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if ($filter('uppercase')(origArr[x].id) === $filter('uppercase')(newArr[y].id)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }

            return newArr.map(function (item) {
                return item;
            });
        });
        //Gobal patient search Removed

    };

    //$scope.getCustomerBalance = function () {
    //    customerReceiptService.getCustomerBalance(customerHelper.data.selectedCustomer.mobile, customerHelper.data.selectedCustomer.name).then(function (resp) {
    //        if (resp.data.length == 0) {
    //            customerHelper.data.customerBalance = 0;
    //            return;
    //        }

    //        if (resp.data.credit != undefined)
    //            customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
    //    });
    //};
    var patientSelected = 0;
    $scope.onPatientSelect = function (obj, event) {
        if (obj !== undefined) {
            $scope.search.values = obj.name;
            $scope.search.PatientId = obj.id; // PatientId added by violet
        }




    };

    getPatientSearchType();
    $scope.PatientNameSearch = "1";
    function getPatientSearchType() {
        salesService.getPatientSearchType().then(function (response) {

            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.PatientNameSearch = "1";
            } else {
                if (response.data.patientSearchType != undefined) {
                    $scope.PatientNameSearch = response.data.patientSearchType;
                } else {
                    $scope.PatientNameSearch = "1";
                }
            }
        }, function () {

        });
    }



    $scope.InvoiceSeriesItems = [];
    function getInvoiceSeriesItems() {
        salesService.getInvoiceSeriesItems().then(function (response) {

            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }

        }, function () {

        });
    }



    $scope.productSearch = function () {

        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        $scope.search.searchProductId1 = "";
        if ($scope.search.productvalues)
            $scope.search.searchProductId1 = $scope.search.productvalues.id;

        salesService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            $scope.vendorPurchaseProduct1 = JSON.parse(window.localStorage.getItem("p_dataChange"));
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }
    //The below method added by Violet to get Sale Item details on 10/05/2017
    $scope.toggleProductDetail = function (obj, data) {
        var row = obj.target.getAttribute("data");
        //$('#chip-wrapper' + row).show();
        
            if ($('#chip-btn' + row).text() === '+') {
                $scope.search.SalesID = data.id;
                if (data.salesItem.length == 0) {
                    salesService.getlist($scope.search).then(function (response) {
                        data.salesItem = response.data.salesItem;
                        data.salesItemAudit = response.data.salesItemAudit;
                        data.salesReturnItem = response.data.salesReturnItem;
                        data.salesReturnItemHistory = response.data.salesReturnItemHistory;
                    });
                }
                $('#chip-wrapper' + row).delay(30).fadeToggle();
           
            $('#chip-btn' + row).text('-');
        }
        else {
            $('#chip-wrapper' + row).slideToggle();
            $('#chip-btn' + row).text('+');
        }

    }


    $scope.paidMsg = 'Credit Amount fully paid';
    //$scope.offlineMsg = 'Offline disabled, use in online';
    $scope.offlineMsg = 'Online disabled, please use offline';

    $scope.editSales = function (sales) {
        if (!$scope.IsOffline) {
            sales.IsEdit = true;
        }
        else {
            toastr.info($scope.offlineMsg);
        }
    }

    $scope.saveSales = function (sales) {
      
        sales.IsEdit = false;
        $.LoadingOverlay("show");
        salesService.UpdateSaleDetails(sales).then(function (response) {            
            sales.isEdit = !response.data;
            $.LoadingOverlay("hide");
            //toastr.success('Data Saved Successfully');
            //window.location.assign('/sales/list');
        }, function () {
            $.LoadingOverlay("hide");
            //toastr.error('Error Occured', 'Error');
        });
    }


    
    $scope.printBill = function (sales) {
        printingHelper.printInvoice(sales.id)
    }

    //Added for department a4 print by lawrence
    $scope.printBillNew = function (SeriesType, SeriesTypeValue, InvNo, printerType, isSalesReturn) {
        if ($scope.search.values == "") {
            $scope.isValidInvoiceNo = true;
            return false;
        }
        if ($scope.search.values == null || $scope.search.values == undefined) {
            $scope.isValidInvoiceNo = true;
            return false;
        }
        if (SeriesType == "Default")
            SeriesType = 1;
        else if (SeriesType == "Custom")
            SeriesType = 2;
        else if (SeriesType == "Manual")
            SeriesType = 3;
        if (SeriesTypeValue == "Default")
            SeriesTypeValue = "";
        $scope.salesId = null;
        $scope.salesReturnId = null;
        $scope.isSalesReturn = isSalesReturn;
        if ($scope.isSalesReturn != true) {
            salesService.getSalesIdByInvoiceNo(SeriesType, SeriesTypeValue, InvNo, $scope.isSalesReturn).then(function (response) {
                $scope.salesId = response.data;
                if ($scope.salesId == null || $scope.salesId == "") {
                    toastr.info("Enter Correct Invoice Number..");
                    return false;
                }
                else {
                    printingHelper.printInvoiceNew($scope.salesId, SeriesType, SeriesTypeValue, InvNo, printerType);
                }
            }, function () {

            });
        }
        else {            
            salesService.getSalesIdByInvoiceNo(SeriesType, SeriesTypeValue, InvNo, $scope.isSalesReturn).then(function (response) {
                $scope.salesReturnId = response.data;
                if ($scope.salesReturnId == null || $scope.salesReturnId == "") {
                    toastr.info("Enter Correct Return Number..");
                    return false;
                }
                else {
                    var w = window.open("/Invoice/ReturnWithoutSalesInvoice?id=" + $scope.salesReturnId + "&printer=" + printerType);
                }
            }, function () {

            });
        }
    }

    $scope.checkInvoiceNumber = function () {
        if ($scope.search.values == "") {
            $scope.isValidInvoiceNo = true;
        }
        if ($scope.search.values != '' && $scope.search.values != null) {
            $scope.isValidInvoiceNo = false;
        }
    }

    $scope.viewBill = function (sales) {
        printingHelper.getInvoice(sales.id)
    }

    $scope.clearSearch = function () {

        if (flag) {
            $scope.search.values = "";
            $scope.init("True");
            $scope.search.select1 = "All";
            $scope.showIstenRecords = false;
            $scope.search.select1 = "top10";
            return;
        }

        $scope.search.name = "";
        $scope.search.mobile = "";

        $scope.search.invoiceNo = "";
        $scope.search.select = "";
        $scope.search.values = "";
        $scope.search.selectValue = "";
        //$scope.search.select = 'billDate'
        //$scope.search.select1 = 'equal';
        //$scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.maxDate = new Date();
        $scope.search.productvalues = "";
        $scope.search.drugName = "";
        $scope.InvoiceSeriesCondition = false;
        $scope.InvoiceSeriesType = "Default";
        $scope.salesCondition = false;
        $scope.dateCondition = false;
        $scope.productCondition = false;
        $scope.EnterCondition = false;
        //added by nandhini 18.4.17
        $scope.invoicenumberCondition = false;
        //end
        $scope.CustomerCondition = false;
        $scope.search.select1 = "top10"
        $scope.showIstenRecords = false; //Variable added to implement the top 10records on pageload and clear
        $scope.ispageload = 1;
        $scope.search.PatientId = "";
        $scope.salesSearch();

    }
    $scope.product = productModel;

    $scope.getProducts = function (val) {

        return productService.SalesProductdrugFilter(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }


    getPrintType = function () {
        salesService.getPrintType().then(function (response) {
            $scope.isDotMatrix = response.data;
            window.localStorage.setItem("IsDotMatrix", JSON.stringify($scope.isDotMatrix));
        }, function () {

        });
    }
    getPrintType();

    $scope.CancelItem = function (id) {
       // console.log(id);
        if (!$scope.IsOffline) {
            $.LoadingOverlay("show");
            $scope.salesReturn.salesId = id;

            salesService.salesReturnDetail($scope.salesReturn.salesId).then(function (response) {
                $scope.salesReturn = response.data;
               // console.log(JSON.stringify($scope.salesReturn));
                $scope.salesTotal = $scope.salesReturn.purchasedTotal;
                $scope.salesDiscount = ($scope.salesReturn.purchasedTotal * $scope.salesReturn.discount / 100);
                var checkquantity = 0;
                for (var x = 0; x < $scope.salesReturn.salesReturnItem.length; x++) {

                    var Reorderquntity = 0;
                    if ($scope.salesReturn.salesReturnItem[x].returnedQuantity == undefined || $scope.salesReturn.salesReturnItem[x].returnedQuantity == null || $scope.salesReturn.salesReturnItem[x].returnedQuantity == "") {
                        Reorderquntity = 0;
                    } else {
                        Reorderquntity = $scope.salesReturn.salesReturnItem[x].returnedQuantity
                    }
                    $scope.salesReturn.salesReturnItem[x].quantity = $scope.salesReturn.salesReturnItem[x].purchaseQuantity - Reorderquntity;


                    if ($scope.salesReturn.salesReturnItem[x].quantity != 0) {
                        checkquantity++;
                    }
                    $scope.salesReturn.salesReturnItem[x].cancelType = 1;
                    if ($scope.salesReturn.salesReturnItem[x].sellingPrice > 0) {
                        $scope.salesReturn.salesReturnItem[x].discountAmount = ($scope.salesReturn.salesReturnItem[x].sellingPrice * $scope.salesReturn.salesReturnItem[x].purchaseQuantity) * $scope.salesReturn.salesReturnItem[x].discount / 100;
                        $scope.salesReturn.salesReturnItem[x].discountAmountPerQty = ($scope.salesReturn.salesReturnItem[x].discount * $scope.salesReturn.salesReturnItem[x].sellingPrice) / 100;
                    }
                    else {
                        $scope.salesReturn.salesReturnItem[x].discountAmount = ($scope.salesReturn.salesReturnItem[x].productStock.sellingPrice * $scope.salesReturn.salesReturnItem[x].purchaseQuantity) * $scope.salesReturn.salesReturnItem[x].discount / 100;
                        $scope.salesReturn.salesReturnItem[x].discountAmountPerQty = ($scope.salesReturn.salesReturnItem[x].discount * $scope.salesReturn.salesReturnItem[x].productStock.sellingPrice) / 100;
                    }
                }
                // setPrice();

                if (checkquantity == 0) {
                  
                    $.alert({
                        title: 'Cancel is not possible!',
                        content: 'It seems all products are returned in this bill',
                        animation: 'center',
                        closeAnimation: 'center',
                        backgroundDismiss: true,
                        buttons: {
                            okay: {
                                text: 'Ok [Esc]',
                                btnClass: 'primary-button custom-transform',
                                action: function () {
                                    // do nothing
                                }
                            }
                        }
                    });
                    $.LoadingOverlay("hide");

                } else {
                    var r = confirm("Are you sure want to Cancel all Items?");

                    if (r == true) {
                       // console.log(JSON.stringify($scope.salesReturn));
                        setModelValues();
                        salesService.createSalesReturn($scope.salesReturn).then(function (response) {
                            $scope.salesReturn = response.data;
                            //  toastr.success('Cancelled Items Successfully');
                            alert("cancelled successfully")
                            $scope.salesSearch(); //REdirection commented and Sales search invoked by poongodi on 06/03/2017 based on Punith input
                            // window.location.assign('/SalesReturn/CancelList');
                            $.LoadingOverlay("hide");
                        }, function () {
                            $.LoadingOverlay("hide");
                            // toastr.error('Error Occured', 'Error');
                        });

                    } else {
                        $scope.salesReturn = [];
                        $.LoadingOverlay("hide");
                    }
                }



            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        else {
            toastr.info($scope.paidMsg);
        }
    }

    $scope.getRoundOffSettings = function () {
        $.LoadingOverlay("show");
        salesService.getRoundOffSettings().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.isEnableRoundOff = response.data;
            if ($scope.isEnableRoundOff == null) {
                $scope.isEnableRoundOff = false;
            }
        }, function (error) {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getRoundOffSettings();

    function setModelValues() {
        $scope.salesReturn.totalDiscountValue = 0;
        $scope.salesReturn.returnItemAmount = 0;
        $scope.salesReturn.netAmount = 0;
        $scope.salesReturn.gstAmount = 0;
        $scope.salesReturn.roundOffNetAmount = 0;

        for (var i = 0; i < $scope.salesReturn.salesReturnItem.length; i++) {
            $scope.salesReturn.salesReturnItem[i].discount = $scope.salesReturn.salesReturnItem[i].discount || 0;
            $scope.salesReturn.discount = $scope.salesReturn.discount || 0;
            $scope.salesReturn.salesReturnItem[i].sellingPrice = $scope.salesReturn.salesReturnItem[i].sellingPrice || 0;

            var discount = parseFloat($scope.salesReturn.salesReturnItem[i].discount) + $scope.salesReturn.discount;
            $scope.salesReturn.salesReturnItem[i].discount = discount;

            var quantity = Number($scope.salesReturn.salesReturnItem[i].quantity);
            var sellingPrice = ($scope.salesReturn.salesReturnItem[i].sellingPrice == 0 ? ($scope.salesReturn.salesReturnItem[i].productStock.sellingPrice || 0) : $scope.salesReturn.salesReturnItem[i].sellingPrice);
            var totalItemAmount = sellingPrice * quantity;

            $scope.salesReturn.salesReturnItem[i].discountAmount = totalItemAmount * discount / 100;
            $scope.salesReturn.salesReturnItem[i].totalAmount = totalItemAmount - $scope.salesReturn.salesReturnItem[i].discountAmount;

            var taxPerc = $scope.salesReturn.salesReturnItem[i].productStock.gstTotal || 0;
            var taxAmt = ($scope.salesReturn.salesReturnItem[i].totalAmount - (($scope.salesReturn.salesReturnItem[i].totalAmount / (100 + parseFloat(taxPerc))) * 100));
            $scope.salesReturn.salesReturnItem[i].gstAmount = taxAmt;

            $scope.salesReturn.gstAmount += $scope.salesReturn.salesReturnItem[i].gstAmount;
            $scope.salesReturn.totalDiscountValue += $scope.salesReturn.salesReturnItem[i].discountAmount;
            $scope.salesReturn.returnItemAmount += totalItemAmount;
            $scope.salesReturn.netAmount += $scope.salesReturn.salesReturnItem[i].totalAmount;
        }
        $scope.salesReturn.discountValue = $scope.salesReturn.returnItemAmount * $scope.salesReturn.discount / 100;
        if ($scope.isEnableRoundOff) {
            $scope.salesReturn.isRoundOff = true;
            $scope.salesReturn.roundOffNetAmount = Math.round($scope.salesReturn.netAmount) - $scope.salesReturn.netAmount;
            $scope.salesReturn.netAmount = Math.round($scope.salesReturn.netAmount);
        }
        else {
            $scope.salesReturn.isRoundOff = false;
            $scope.salesReturn.roundOffNetAmount = 0;
        }
    }

    $scope.greyButton = ""

    dispCashType = function () {
        toastr.info($scope.paidMsg);
    }

    $scope.keyEnter = function (event, e) {


       // console.log(e);
        if (event.which === 8) {
            if ($scope.customerHelper.data.patientSearchData.name == "" || $scope.customerHelper.data.patientSearchData.name == undefined) {
                $scope.Namecookie = "";
            }
        }
        var ele = document.getElementById(e);

        if (event.which === 13) // Enter key
        {
            ele.focus();
        }

        if (event.which === 9) {
            $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
        }
        
    };


    $scope.moveCustomer = function (e) {

        var ele = document.getElementById(e);

        if (event.which === 13) // Enter key
        {
            ele.focus();
        }        
    };


    $scope.getEnableSelling = function () {
        salesService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getEnableSelling();

  getexportToCsvSetting();
  function getexportToCsvSetting() {
        toolService.getexportToCsvSetting().then(function (response) {
            $scope.csvSettings = response.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.exportToCsv = function (sales) {
        salesService.salesDetail(sales.id).then(function (response) {
            var saledata = response.data;
            $scope.saledata = saledata;
            if ($scope.saledata != null && $scope.saledata!=undefined)
            {
                var data = "Company Name, Bill Number, Bill Date, Total Discount%, Net Amount,\n";
                data = data + $scope.saledata.instance.name + ",";
                data = data + $scope.saledata.prefix + $scope.saledata.invoiceSeries + $scope.saledata.invoiceNo + ",";
                data = data + $filter('date')($scope.saledata.invoiceDate, 'dd-MMM-yyyy') + ",";
                data = data + $scope.saledata.discountPercentage.toFixed(2) + ",";
                if ($scope.saledata.salesReturnItem.length == 0) {
                    data = data + $scope.saledata.netAmount.toFixed(2) + ",";
                }
                else
                {
                    data = data + "0,";
                }
                data = data + ",\n";
                data = data + "Sr.no.,ProductName,BatchNo,ExpiryDate(dd-MMM-yyyy),GST%,Units/Strip,No.Strips,Price/Strip,MRP/Strip,Free Qty,Discount%,HsnCode,SchemeDiscount";
                data = data + ",\n";
                
                if ($scope.saledata.salesItem.length>0)
                {
                    for (i = 0; i < $scope.saledata.salesItem.length; i++) {
                        if ($scope.saledata.salesItem[i].quantity > 0)
                        {
                                data = data + (($scope.saledata.salesItem[i] == null || $scope.saledata.salesItem[i] == undefined) ? "" : i+1) + ",";
                                data = data + $scope.saledata.salesItem[i].productStock.product.name + ",";
                                data = data + $scope.saledata.salesItem[i].productStock.batchNo + ",";
                                data = data + $filter('date')($scope.saledata.salesItem[i].productStock.expireDate, 'dd-MMMM-yyyy') + ",";
                                data = data + $scope.saledata.salesItem[i].gstTotal.toFixed(2) + ",";
                                if ($scope.saledata.salesItem[i].productStock.packageSize == undefined) {
                                    $scope.saledata.salesItem[i].productStock.packageSize = 0;
                                    data = data + $scope.saledata.salesItem[i].productStock.packageSize + ",";
                                }
                                else {
                                    data = data + $scope.saledata.salesItem[i].productStock.packageSize + ",";
                                };
                                //data = data + $scope.saledata.salesItem[i].productStock.packageSize + ",";
                                data = data + $scope.saledata.salesItem[i].quantity + ",";
                                if ($scope.saledata.salesItem[i].productStock.packagePurchasePrice == undefined ) {
                                    $scope.saledata.salesItem[i].productStock.packagePurchasePrice = 0;
                                    data = data + $scope.saledata.salesItem[i].productStock.packagePurchasePrice + ",";
                                }
                                else {
                                    data = data + $scope.saledata.salesItem[i].productStock.packagePurchasePrice.toFixed(2) + ",";
                                };
                            // data = data + $scope.saledata.salesItem[i].productStock.packagePurchasePrice.toFixed(2) + ",";
                                if ($scope.saledata.salesItem[i].mrpPerStrip == undefined ) {
                                    $scope.saledata.salesItem[i].mrpPerStrip = 0;
                                    data = data + $scope.saledata.salesItem[i].mrpPerStrip + ",";
                                }
                                else {
                                    data = data + $scope.saledata.salesItem[i].mrpPerStrip.toFixed(2) + ",";
                                };
                                //data = data + $scope.saledata.salesItem[i].mrpPerStrip.toFixed(2) + ",";
                                data = data + "0,";
                                data = data + $scope.saledata.salesItem[i].discount.toFixed(2) + ",";
                                data = data + $scope.saledata.salesItem[i].productStock.product.hsnCode + ",";
                                data = data + "0,";
                                data = data + ",\n";
                            }
                    }
                }
            }
    
                data = data + ",\n";
                //var fromdate = $filter('date')(new Date(), 'dd-MM-yyyy');
                //var frmday = $scope.filter.fromdate.split('-');
                //var frmdayValue = frmday[2] + frmday[1];
                 
                var filename = 'Sales_' + $scope.saledata.prefix + $scope.saledata.invoiceSeries + $scope.saledata.invoiceNo + '.csv';

                var a = $('<a/>', {
                    style: 'display:none',
                    href: 'data:application/octet-stream;base64,' + btoa(data),
                    download: filename
                }).appendTo('body')
                a[0].click()
                a.remove();
            //}
        });
        };
});