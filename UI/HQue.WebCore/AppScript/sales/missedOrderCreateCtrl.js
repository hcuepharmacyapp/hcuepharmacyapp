app.controller('missedOrderCreateCtrl', function ($scope, $rootScope, $filter, close, toastr, cacheService, ModalService, salesService, productService, productStockService, productModel, vendorPurchaseService, missedOrderModel, patientId, patientPhone, patientName, patientAge, patientGender) {


    var product = productModel;

    $scope.search = product;
    $scope.GSTEnabled = true;
    var missedOrder = missedOrderModel;
    $scope.missedOrder = missedOrder;
    $scope.missedOrder.missedOrderItem = [];
    $scope.IsEdit = false;
    $scope.selectedProduct = null;
    $scope.reminder = false;
    $scope.quantityExceed = 0;
    $scope.searchType1 = null;
    $scope.focusInput = true;

    if (patientId != null) {
        $scope.missedOrder.patientId = patientId;
    }

    if (patientPhone != null) {
        $scope.missedOrder.patientMobile = patientPhone;
    }

    if (patientName != null) {
        $scope.missedOrder.patientName = patientName;
        console.log(patientName);
    }

    if (patientAge != null) {
        $scope.missedOrder.patientAge = patientAge;
        console.log(patientAge);
    }

    if (patientGender != null) {
        $scope.missedOrder.patientGender = patientGender;
        console.log(patientGender);
    }
  
    $scope.PopupAddNewProduct = function () {
        var m = ModalService.showModal({
            "controller": "productCreateCtrl",
            "templateUrl": "createProduct",
            "inputs": {
                "mode": "Create",
                "poproductname": '',
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();

        });
    };
    $scope.keyEnter = function (event, e) {
        var result = document.getElementById("OrderQuantity").value;
        if (result == "" || result == 0) {
        } else {
            var ele = document.getElementById(e);
            if (event.which === 13) { // Enter key
                ele.focus();
                if (ele.nodeName != "BUTTON") {
                    ele.select();
                }
            }
        }
    };

    $scope.changeQuantity = function (val) {
        if ($scope.selectedProduct.id != null) {
            if (val > 0) {
                $scope.quantityExceed = 1;
            }
            else {
                $scope.quantityExceed = 0;
            }
        }
    };

    $scope.onProductSelect = function ($event, selectedProduct1) {
        $scope.quantityExceed = 0;
        if (selectedProduct1 != null) {
            vendorPurchaseService.getPurchaseValues(selectedProduct1.id, selectedProduct1.name).then(function (response) {
                $scope.vendorPurchaseItem = response.data.vendorPurchaseItem;
                $scope.selectedProduct.sellingPrice = $scope.vendorPurchaseItem.packageSellingPrice;
                var qty = document.getElementById("OrderQuantity");
                qty.focus();
            }, function (response) {
                $scope.responses = response;
            });
        }
    };

    $scope.getSearchType1 = function () {

        vendorPurchaseService.getSearch().then(function (response1) {
            if (response1.data != null && response1.data != '') {
                $scope.searchType1 = response1.data;
            }
        }, function (error) {
            console.log(error);
        });
    };
   // $scope.getSearchType1();

    $scope.getProducts = function (val) {


        return productService.nonHiddenProductList(val).then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }

            return output.map(function (item) {
                return item;
            });
        });
        //if ($scope.searchType1 == "Global") {
        //    return productService.drugFilterData(val).then(function (response) {

        //        var flags = [], output = [], l = response.data.length, i;
        //        for (i = 0; i < l; i++) {
        //            if (flags[$filter('uppercase')(response.data[i].name)]) continue;
        //            flags[$filter('uppercase')(response.data[i].name)] = true;
        //            output.push(response.data[i]);
        //        }

        //        return output.map(function (item) {
        //            return item;
        //        });
        //    });
        //}
        //else {
        //    return productService.drugFilterLocalData(val).then(function (response) {

        //        var flags = [], output = [], l = response.data.length, i;
        //        for (i = 0; i < l; i++) {
        //            if (flags[$filter('uppercase')(response.data[i].name)]) continue;
        //            flags[$filter('uppercase')(response.data[i].name)] = true;
        //            output.push(response.data[i]);
        //        }

        //        return output.map(function (item) {
        //            return item;
        //        });
        //    });
        //}
    };


    $scope.addProduct = function () {

        var existItem = 0;

        if ($scope.selectedProduct.sellingPrice == "" || $scope.selectedProduct.sellingPrice == null) {
            $scope.selectedProduct.sellingPrice = 0;
        }

        if ($scope.selectedProduct.quantity > 0) {
            for (var i = 0; i < $scope.missedOrder.missedOrderItem.length; i++) {
                if ($scope.missedOrder.missedOrderItem[i].id == $scope.selectedProduct.id) {
                    existItem = 1;
                }
            }
            if (existItem == 0) {
                 if ($scope.selectedProduct.accountId === null || $scope.selectedProduct.accountId === undefined || $scope.selectedProduct.accountId === "") {
                    $scope.selectedProduct.productMasterID = $scope.selectedProduct.id;
                    $scope.selectedProduct.productMasterName = $scope.selectedProduct.name;
                    $scope.selectedProduct.manufacturer = $scope.selectedProduct.manufacturer;
                    $scope.selectedProduct.category = $scope.selectedProduct.category;
                    $scope.selectedProduct.schedule = $scope.selectedProduct.schedule;
                    $scope.selectedProduct.genericName = $scope.selectedProduct.genericName;
                    $scope.selectedProduct.packing = $scope.selectedProduct.packing;

                }
                else {
                    $scope.selectedProduct.productMasterID = null;
                }

               
                $scope.missedOrder.missedOrderItem.push($scope.selectedProduct);
                //$scope.missedOrder.missedOrderItem.productStock.push($scope.selectedProduct);
                //if ($scope.selectedProduct.totalstock === -1) {
                //    $scope.missedOrder.missedOrderItem.productStock.product.productMasterID = $scope.selectedProduct.id;
                //}
                //else {
                //    $scope.missedOrder.missedOrderItem.productStock.product.productMasterID = null;
                //}
                $scope.selectedProduct = null;
                $scope.quantityExceed = 0;
                document.getElementById("drugName1").focus();
            }
        }

    };

    $scope.tempQuantity = null;
    $scope.isAdd = false;

    $scope.editProduct = function (saleItem) {
        saleItem.IsEdit = true;
        $scope.isAdd = true;
        $scope.tempQuantity = saleItem.quantity;
    };

    $scope.saveProduct = function (saleItem) {
        saleItem.IsEdit = false;
        $scope.isAdd = false;
        var index1 = $scope.missedOrder.missedOrderItem.indexOf(saleItem);
        if (parseFloat(saleItem.quantity) > 0) {
            $scope.missedOrder.missedOrderItem[index1].quantity = saleItem.quantity;
        }
        else {
            $scope.missedOrder.missedOrderItem[index1].quantity = $scope.tempQuantity;
        }
        if (parseFloat(saleItem.sellingPrice) > 0) {
            $scope.missedOrder.missedOrderItem[index1].sellingPrice = saleItem.sellingPrice;
        }
        else {
            $scope.missedOrder.missedOrderItem[index1].sellingPrice = 0;
        }

    };

    $scope.removeProduct = function (item) {

        if (!confirm("Are you sure, Do you want to delete ? "))
            return;

        var index = $scope.missedOrder.missedOrderItem.indexOf(item);
        $scope.missedOrder.missedOrderItem.splice(index, 1);
    };

    $scope.addOrder = function () {

        $scope.close('Yes');
        $.LoadingOverlay("show");

        salesService.createMissedOrder($scope.missedOrder).then(function (respone) {
            var misseditems = respone.data;
            toastr.success('Missed order saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };




    $scope.close = function (result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
        if (result != "Yes") {
            $scope.search.drugName = null;
            $scope.search = null;
        }
        $(".modal-backdrop").hide();
    };

});