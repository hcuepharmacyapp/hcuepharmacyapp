﻿app.factory('customerHelper', function (patientService, salesService, customerReceiptService, cacheService, ModalService, $rootScope, toastr) {

    var data = {
        "isCustomerSelected": false,
        "customerList": [],
        "selectedCustomer": {},
        "patientSearchData": {},
        "customerBalance": 0
    };

    return {
        "data": data,
        "editPatient": editPatient,
        "doneEditPatient": doneEditPatient,
        "PopupAddNew": popupAddNew,
        "search": search,
        "EditCustomer": editCustomer,
        "ResetCustomer": reset,
        "RedeemPointsEnable": redeemPointsEnable,
        "doSelectPatient": doSelectPatient
    };

    function editCustomer(isAvaildiscount, discountValue, isCustomerOrDept) {

        if (isAvaildiscount == "Yes") {
            if (data.selectedCustomer.discount > discountValue) {
                data.selectedCustomer.discount = discountValue;
            }
        }
        cacheService.set('selectedPatient', data.selectedCustomer);
        var m = ModalService.showModal({
            controller: "patientCreateCtrl",
            templateUrl: 'createPatient',
            inputs: {
                //mode: "Edit",
                //isAvaildiscount: isAvaildiscount,
                //discountValue: discountValue,
                //isCustomerOrDept: isCustomerOrDept,
                param: {
                    "mode": "Edit",
                    "patData": data.selectedCustomer,
                    "isAvaildiscount": isAvaildiscount,
                    "discountValue": discountValue,
                    "isCustomerOrDept": isCustomerOrDept
                }
            }
        }).then(function (modal) {
            modal.element.modal();
            // Customer and Department disabled option
            if (data.selectedCustomer.patientType == 2) {

                document.getElementById('genderMale').disabled = true;
                document.getElementById('genderFemale').disabled = true;
                document.getElementById('dobDate').disabled = true;
                document.getElementById('Discount').disabled = true;
            } else {

                document.getElementById('genderMale').disabled = false;
                document.getElementById('genderFemale').disabled = false;
                document.getElementById('dobDate').disabled = false;
                document.getElementById('Discount').disabled = false;
            }

            //var pname = document.getElementById("patientName");
            //pname.focus();

            modal.close.then(function (result) {
                document.getElementById("drugName").focus();

            });
        });

    }

    function doneEditPatient(item) {
        item.isEditPatient = false;
        salesService.UpdateCustomer(item).then(function (response) {
            toastr.success('Customer updated successfully');
            data.selectedCustomer.age = getAge(data.selectedCustomer.dob);
        }, function () {
            toastr.error('Error Occured', 'Error');
        });
    }

    function reset() {
        data.isCustomerSelected = false;

        data.customerList = [];
        data.selectedCustomer = {};
        data.patientSearchData = {};
        data.customerBalance = 0;
        data.isCustomerDiscount = 0;
        //data.isReset = 1;
        //if (data.DoctorNameMandatory == 1) {
        //    data.iscustomerNameMandatory = true;
        //} else {
        //    data.iscustomerNameMandatory = false;
        //}
        $rootScope.$emit("ResetCustomerdetails");
        cacheService.remove('selectedPatient');


    }

    function redeemPointsEnable() {

        $rootScope.$emit("RedeemPointsEnable");
    }

    function editPatient(item) {
        item.isEditPatient = true;
        if ($(".pat-info-btn").text() === '+') {
            $(".alter-row-collapse").show();
            $(".pat-info-btn").text('-');
        }
    }



    function search(isedit, namesearch, maxDiscountFixed, maxDisountValue, isDepartmentsave, moborempid) {


        //Check Mobile or Id number on customer search - Added by Annadurai    

        if ((moborempid == 'mobile' && data.patientSearchData.mobile == undefined) || (moborempid == 'mobile' && data.patientSearchData.mobile == '')) {
            document.getElementById("customerID").focus();
            return false;
        }

        if ((moborempid == 'EmpId' && data.patientSearchData.empID == undefined) || (moborempid == 'EmpId' && data.patientSearchData.empID == '')) {
            document.getElementById("doctorName").focus();
            return false;
        }
        //End here
        $.LoadingOverlay("show");
        data.isCustomerDiscount = 0;

        var patientId = data.patientSearchData.patientId;
        var patientname = data.patientSearchData.name;
        var patientMobile = data.patientSearchData.mobile;
        data.patientSearchData.status = 1;

        if (isDepartmentsave == undefined || isDepartmentsave == "") {
            isDepartmentsave = 1;
        }
        patientService.localList(data.patientSearchData, isDepartmentsave).then(function (response) {
            data.customerList = response.data;

            for (p in data.customerList) {
                if (data.customerList[p].name == "" || data.customerList[p].name == null) {
                    data.customerList[p].nameIndex = "";
                } else {
                    data.customerList[p].nameIndex = data.customerList[p].name.substring(0, 1);
                }
                // data.customerList[p].nameIndex = data.customerList[p].name.substring(0, 1);
            }
            if (data.customerList.length > 0) {

                if (isedit == true) {
                    for (var p = 0; p < data.customerList.length; p++) {

                        if (patientId == data.customerList[p].patientId) {
                            data.selectedCustomer = data.customerList[p];
                        } else {
                            if (patientname == data.customerList[p].name) {
                                data.selectedCustomer = data.customerList[p];
                            }
                            if (patientMobile == data.customerList[p].mobile) {
                                data.selectedCustomer = data.customerList[p];
                            }
                        }

                    }
                } else {

                    data.selectedCustomer = data.customerList[0];
                }



                data.isCustomerSelected = true;
                var doctorName = document.getElementById("doctorName");
                doctorName.focus();
                if (data.selectedCustomer != null && data.selectedCustomer.name != undefined) {
                    patientService.getCustomerDiscount(data.selectedCustomer).then(function (response) {
                        data.selectedCustomer.discount = response.data;
                        if (maxDiscountFixed == "Yes") {
                            if (data.selectedCustomer.discount > maxDisountValue) {
                                toastr.info("Selected Customer Having more discount than the Fixed Max discount ,So we changed discount percentage to Max discount");
                                // data.selectedCustomer.discount = maxDisountValue;
                            }

                        }
                    });
                }
                getCustomerBalance();

                $rootScope.$emit("CheckCustomerdetails", { message: "true" });
            } else {
                $rootScope.$emit("CheckCustomerdetails", { message: "false" });
            }




            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });

    }

    function doSelectPatient() {
        var patient = cacheService.get('selectedPatient');
        cacheService.set('selectedPatientUpdate', "CustomerUpdate");
        reset();
        data.patientSearchData = patient;
        //if (isDepartmentsave == undefined || isDepartmentsave == "") {
        //    isDepartmentsave = 1;
        //}
        search(false, 1, "No", 0, 1);
    }

    function getAge(dob) {
        var today = new Date();
        var past = new Date(dob);
        return today.getFullYear() - past.getFullYear();
    }

    function popupAddNew(isAvaildiscount, discountValue, isCustomerOrDept) {
        data.patientSearchData.gender = "M";
        cacheService.set('selectedPatient', data.patientSearchData);
        var m = ModalService.showModal({
            controller: "patientCreateCtrl",
            templateUrl: 'createPatient',
            inputs: {
                //mode: "Create",
                //isAvaildiscount: isAvaildiscount,
                //discountValue: discountValue,
                //isCustomerOrDept: isCustomerOrDept,
                param: {
                    "mode": "Create",
                    "patData": data.patientSearchData,
                    "isAvaildiscount": isAvaildiscount,
                    "discountValue": discountValue,
                    "isCustomerOrDept": isCustomerOrDept
                }
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result == "No" || result == "Cancel") {
                    reset();
                }
            });
        });
    }

    function getCustomerBalance() {


        if (data.selectedCustomer.name == "" || data.selectedCustomer.name == null) {
            data.selectedCustomer.nameIndex = "";
        } else {
            data.selectedCustomer.nameIndex = data.selectedCustomer.name.substring(0, 1);
        }
        //data.selectedCustomer.nameIndex = data.selectedCustomer.name.substring(0, 1);
        customerReceiptService.getCustomerBalance(data.selectedCustomer.mobile, data.selectedCustomer.name)
            .then(function (customerResp) {

                if (customerResp.data.length == 0)
                    return;
                if (customerResp.data.credit != undefined)
                    data.customerBalance = customerResp.data.credit - customerResp.data.debit;
            });


    }
});