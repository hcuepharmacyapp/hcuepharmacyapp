﻿app.factory('newcustomerHelper', function (patientService, newsalesService, customerReceiptService, cacheService, ModalService, $rootScope, toastr) {

    var data = {
        isCustomerSelected: false,
        customerList: [],
        selectedCustomer: {},
        patientSearchData: {},
        customerBalance: 0,
        AddNewCustomerOpened: false
    };
   
    return {
        data: data,
        editPatient: editPatient,
        doneEditPatient: doneEditPatient,
        PopupAddNew: popupAddNew,
        search: search,
        EditCustomer: editCustomer,
        ResetCustomer: reset,
        doSelectPatient: doSelectPatient
    };

    function editCustomer(isAvaildiscount, discountValue, isCustomerOrDept) {
        cacheService.set('selectedPatient', data.selectedCustomer);
        var m = ModalService.showModal({
            controller: "patientCreateCtrl",
            templateUrl: 'createPatient',
            inputs: {
                //mode: "Edit",
                //isAvaildiscount: isAvaildiscount,
                //discountValue: discountValue,
                //isCustomerOrDept: isCustomerOrDept,
                param: {
                    "mode": "Edit",
                    "patData": data.selectedCustomer,
                    "isAvaildiscount": isAvaildiscount,
                    "discountValue": discountValue,
                    "isCustomerOrDept": isCustomerOrDept
                }
            }
        }).then(function (modal) {
            modal.element.modal();
            // Customer and Department disabled option
            if (data.selectedCustomer.patientType == 2) {

                document.getElementById('genderMale').disabled = true;
                document.getElementById('genderFemale').disabled = true;
                document.getElementById('dobDate').disabled = true;
                document.getElementById('Discount').disabled = true;
            }
            else {

                document.getElementById('genderMale').disabled = false;
                document.getElementById('genderFemale').disabled = false;
                document.getElementById('dobDate').disabled = false;
                document.getElementById('Discount').disabled = false;
            }

            //var pname = document.getElementById("patientName");
            //pname.focus();

            modal.close.then(function (result) {
                document.getElementById("drugName").focus();

            });
        });

    }

    function doneEditPatient(item) {
        item.isEditPatient = false;
        newsalesService.UpdateCustomer(item).then(function (response) {
            toastr.success('Customer updated successfully');
            data.selectedCustomer.age = getAge(data.selectedCustomer.dob);
        }, function () {
            toastr.error('Error Occured', 'Error');
        });
    }

    function reset() {
        data.isCustomerSelected = false;
        data.customerList = [];
        data.selectedCustomer = {};
        data.patientSearchData = {};
        data.customerBalance = 0;
        data.isCustomerDiscount = 0;
        //data.isReset = 1;
        //if (data.DoctorNameMandatory == 1) {
        //    data.iscustomerNameMandatory = true;
        //} else {
        //    data.iscustomerNameMandatory = false;
        //}
        $rootScope.$emit("ResetCustomerdetails");
        cacheService.remove('selectedPatient');
    }

    function editPatient(item) {
        item.isEditPatient = true;
        if ($(".pat-info-btn").text() === '+') {
            $(".alter-row-collapse").show();
            $(".pat-info-btn").text('-');
        }
    }



    function search(isedit, namesearch, maxDiscountFixed, maxDisountValue, isDepartmentsave) {

        var focusdocname = false;



        $.LoadingOverlay("show");
        data.isCustomerDiscount = 0;

        var patientname = data.patientSearchData.name;
        if (namesearch == 1) {

            if (isDepartmentsave == undefined || isDepartmentsave == "") {
                isDepartmentsave = 1;
            }
            if (data.patientSearchData.empID !== undefined) {


                patientService.localList(data.patientSearchData, isDepartmentsave).then(function (response) {
                    data.customerList = response.data;

                    for (p in data.customerList) {
                        if (data.customerList[p].name == "" || data.customerList[p].name == null) {
                            data.customerList[p].nameIndex = "";
                        }
                        else {
                            data.customerList[p].nameIndex = data.customerList[p].name.substring(0, 1);
                        }
                        // data.customerList[p].nameIndex = data.customerList[p].name.substring(0, 1);
                    }
                    if (data.customerList.length > 0) {

                        if (isedit == true) {
                            for (var p = 0; p < data.customerList.length; p++) {
                                if (patientname == data.customerList[p].name) {
                                    data.selectedCustomer = data.customerList[p];
                                }
                            }
                        }
                        else {

                            data.selectedCustomer = data.customerList[0];
                        }




                        data.isCustomerSelected = true;
                        var drugName = document.getElementById("doctorName");

                        if (drugName !== null && drugName !== undefined) {
                            drugName.focus();
                        }
                        if (data.selectedCustomer != null && data.selectedCustomer.name != undefined) {
                            patientService.getCustomerDiscount(data.selectedCustomer).then(function (response) {
                                data.selectedCustomer.discount = response.data;
                                if (maxDiscountFixed == "Yes") {
                                    if (data.selectedCustomer.discount > maxDisountValue) {
                                        toastr.info("Selected Customer Having more discount than the Fixed Max discount ,So we changed discount percentage to Max discount");
                                        // data.selectedCustomer.discount = maxDisountValue;
                                    }

                                }
                            });
                        }
                        getCustomerBalance();
                        // Added Gavaskar 12-04-2017 Start
                        // Comment Gavaskar 13-04-2017 Customer Credit Series Start
                        //if (data.patientSearchData.customerPaymentType == "Credit") {
                        //    $rootScope.$emit("CustomerCreditSeries");
                        //}
                        // Comment Gavaskar 13-04-2017 Customer Credit Series End
                        // Added Gavaskar 12-04-2017 End

                        focusdocname = true;
                        return;

                        $rootScope.$emit("CheckCustomerdetails", { message: "true" });
                    } else {
                        $rootScope.$emit("CheckCustomerdetails", { message: "false" });
                    }
                    //code added by nandhini

                    if (data.patientSearchData.mobile > 0 || data.patientSearchData.mobile == "NA") {
                        if (data.customerList.length == 0) {
                            toastr.info("Customer is Not Available For This Number");
                        }
                    }
                    else {
                        document.getElementById("doctorName").focus();
                        toastr.info("Please Enter The Mobile Number");

                    }


                    $.LoadingOverlay("hide");

                }, function (error) {
                    console.log(error);
                    $.LoadingOverlay("hide");
                });

            }
            else {
                document.getElementById("doctorName").focus();
                $.LoadingOverlay("hide");
            }
        }
        else {

            if (isDepartmentsave == undefined || isDepartmentsave == "") {
                isDepartmentsave = 1;
            }

            patientService.list(data.patientSearchData, isDepartmentsave).then(function (response) {
                data.customerList = response.data;
                if (data.customerList.length > 0) {

                    for (p in data.customerList) {
                        if (data.customerList[p].name == "" || data.customerList[p].name == null) {
                            data.customerList[p].nameIndex = "";
                        }
                        else {
                            data.customerList[p].nameIndex = data.customerList[p].name.substring(0, 1);
                        }
                        //data.customerList[p].nameIndex = data.customerList[p].name.substring(0, 1);
                    }

                    if (isedit == true) {
                        for (var p = 0; p < data.customerList.length; p++) {

                            if (data.patientSearchData.name == data.customerList[p].name) {
                                data.selectedCustomer = data.customerList[p];

                            }
                        }
                    }
                    else {

                        data.selectedCustomer = data.customerList[0];
                    }




                    data.isCustomerSelected = true;
                    var drugName = document.getElementById("drugName");

                    if (drugName !== null && drugName !== undefined) {
                        drugName.focus();
                    }

                    if (data.selectedCustomer != null && data.selectedCustomer.name != undefined) {
                        patientService.getCustomerDiscount(data.selectedCustomer).then(function (response) {
                            data.selectedCustomer.discount = response.data;
                            if (maxDiscountFixed == "Yes") {
                                if (data.selectedCustomer.discount > maxDisountValue) {
                                    toastr.info("Selected Customer Having more discount than the Fixed Max discount ,So we changed discount percentage to Max discount");
                                    //   data.selectedCustomer.discount = maxDisountValue;
                                }

                            }
                        });
                    }
                    getCustomerBalance();
                    $rootScope.$emit("CheckCustomerdetails", { message: "true" });
                } else {
                    if (data.customerList == 0 && isedit == true) {
                        patientService.localList(data.patientSearchData).then(function (response) {
                            data.customerList = response.data;

                            for (p in data.customerList) {
                                if (data.customerList[p].name == "" || data.customerList[p].name == null) {
                                    data.customerList[p].nameIndex = "";
                                }
                                else {
                                    data.customerList[p].nameIndex = data.customerList[p].name.substring(0, 1);
                                }

                            }
                            if (data.customerList.length > 0) {
                                for (p in data.customerList) {
                                    //data.customerList[p].nameIndex = data.customerList[p].name.substring(0, 1);
                                    if (data.customerList[p].name == "" || data.customerList[p].name == null) {
                                        data.customerList[p].nameIndex = "";
                                    }
                                    else {
                                        data.customerList[p].nameIndex = data.customerList[p].name.substring(0, 1);
                                    }
                                }
                                if (isedit == true) {
                                    for (var p = 0; p < data.customerList.length; p++) {

                                        if (data.patientSearchData.name == data.customerList[p].name) {
                                            data.selectedCustomer = data.customerList[p];


                                        }
                                    }
                                }
                                else {

                                    data.selectedCustomer = data.customerList[0];
                                }


                                data.isCustomerSelected = true;
                                var drugName = document.getElementById("drugName");

                                if (drugName !== null && drugName !== undefined) {
                                    drugName.focus();
                                }

                                if (data.selectedCustomer != null && data.selectedCustomer.name != undefined) {
                                    patientService.getCustomerDiscount(data.selectedCustomer).then(function (response) {
                                        data.selectedCustomer.discount = response.data;


                                        if (maxDiscountFixed == "Yes") {
                                            if (data.selectedCustomer.discount > maxDisountValue) {
                                                toastr.info("Selected Customer Having more discount than the Fixed Max discount ,So we changed discount percentage to Max discount");
                                                // data.selectedCustomer.discount = maxDisountValue;
                                            }

                                        }
                                    });
                                }
                                getCustomerBalance();
                                $rootScope.$emit("CheckCustomerdetails", { message: "true" });
                            } else {
                                $rootScope.$emit("CheckCustomerdetails", { message: "false" });
                            }
                            $.LoadingOverlay("hide");

                        }, function (error) {
                            console.log(error);
                            $.LoadingOverlay("hide");
                        });
                    } else {
                        $rootScope.$emit("CheckCustomerdetails", { message: "false" });
                    }
                }
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
            });
        }
        if (data.patientSearchData.empID !== undefined) {
            document.getElementById("doctorName").focus();
            $.LoadingOverlay("hide");
        }
    }

    function doSelectPatient() {
        var patient = cacheService.get('selectedPatient');
        reset();
        data.patientSearchData = patient;
        //if (isDepartmentsave == undefined || isDepartmentsave == "") {
        //    isDepartmentsave = 1;
        //}
        search(false, 1, "No", 0, 1);
    }

    function getAge(dob) {
        var today = new Date();
        var past = new Date(dob);
        return today.getFullYear() - past.getFullYear();
    }

    function popupAddNew(isAvaildiscount, discountValue, isCustomerOrDept) {
        data.patientSearchData.gender = "M";
        cacheService.set('selectedPatient', data.patientSearchData);
        data.AddNewCustomerOpened = true;

        var m = ModalService.showModal({
            controller: "patientCreateCtrl",
            templateUrl: 'createPatient',
            inputs: {
                //mode: "Create",
                //isAvaildiscount: isAvaildiscount,
                //discountValue: discountValue,
                //isCustomerOrDept: isCustomerOrDept,
                param: {
                    "mode": "Create",
                    "patData": null,
                    "isAvaildiscount": isAvaildiscount,
                    "discountValue": discountValue,
                    "isCustomerOrDept": isCustomerOrDept
                }
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result == "No" || result == "Cancel") {
                    data.AddNewCustomerOpened = false;
                    reset();
                }
            });
        });
    }
    function getCustomerBalance() {


        if (data.selectedCustomer.name == "" || data.selectedCustomer.name == null) {
            data.selectedCustomer.nameIndex = "";
        }
        else {
            data.selectedCustomer.nameIndex = data.selectedCustomer.name.substring(0, 1);
        }
        //data.selectedCustomer.nameIndex = data.selectedCustomer.name.substring(0, 1);
        customerReceiptService.getCustomerBalance(data.selectedCustomer.mobile, data.selectedCustomer.name).then(function (customerResp) {

            if (customerResp.data.length == 0)
                return;
            if (customerResp.data.credit != undefined)
                data.customerBalance = customerResp.data.credit - customerResp.data.debit;
        });


    }
});