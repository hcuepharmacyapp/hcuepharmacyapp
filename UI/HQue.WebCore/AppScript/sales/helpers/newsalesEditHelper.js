﻿app.factory('newsalesEditHelper', function (newsalesService, newcustomerHelper, salesPaymentModel) {
    var scope;



    var productItem = {
        "sno": 1,
        "rowid": 1,
        "schedule": '',
        "id": '',
        "accountId": '',
        "name": '',
        "product": '',
        "vatInPrice": 0,
        "discount": 0,
        "age": 0,
        "totalStock": 0,
        "quantity": 0,
        "soldqty": 0,
        "productId": "",
        "vendorId": "",
        "batchNo": "",
        "expireDate": "",
        "vat": 0,
        "sellingPrice": 0,
        "sellingValue": 0,
        "stock": 0,
        "packageSize": 0,
        "packagePurchasePrice": 0,
        "purchasePrice": 0,
        "offlineStatus": false,
        "cst": 0,
        "reminderFrequency": 'None',
        "reminderDate": '',
        "availableQty": 0,
        "selectedBatch": [],
        "isLastRow": true,
        "isQuantityFocused": false,
        "isMrpFocused": false,
        "isDiscountFocused": false,
        "salesReturn": false
    };

    return {
        setScope: function (parentScope) {
            scope = parentScope;
            loadEditData();
        }
    };
    function loadEditData() {
        $.LoadingOverlay("show");
        scope.Editsale = false;
        var salesEditId = document.getElementById('salesEditId').value;
        scope.sales = getSalesObject();
        if (salesEditId == "") {
            for (var i = 0; i < scope.sales.salesItem.length + 1; i++) {
                scope.product.items.push(angular.copy(productItem));
            }
            scope.isSelectedItemMain = 1;
            scope.isCurrentRow = parseInt(scope.isSelectedItemMain) - 1;
            scope.isLastRow = false;
            $.LoadingOverlay("hide");
            return;
        }
        newsalesService.salesDetail(salesEditId).then(function (response) {
            scope.sales = response.data;

            scope.doctorname = scope.sales.doctorName;

            if (scope.sales.discount > 0 || (scope.sales.discount == 0 && (scope.sales.discountValue == 0 || scope.sales.discountValue == undefined))) {
                scope.sales.discountType = 1;
                if (!(scope.sales.discount > 0))
                    scope.changeDiscount(0);
            }
            else {
                scope.sales.discountType = 2;
                //scope.changeDiscount(scope.sales.discountValue);
            }

            var salesItems = [];
            scope.schedulecount = 0;
            var schedulecount = 0;
            for (var i = 0; i < scope.sales.salesItem.length; i++) {
                var salesItem = scope.sales.salesItem[i].productStock;
                salesItem.productStockId = salesItem.id;
                salesItem.id = scope.sales.salesItem[i].id;
                salesItem.editId = i + 1;
                salesItem.Action = "I";
                salesItem.quantity = scope.sales.salesItem[i].quantity;
                salesItem.discount = scope.sales.salesItem[i].discount;
                salesItem.orginalQty = scope.sales.salesItem[i].quantity;
                salesItem.sellingPrice = scope.sales.salesItem[i].sellingPrice;
                salesItem.reminderFrequency = scope.sales.salesItem[i].reminderFrequency.toString();
                salesItem.reminderDate = scope.sales.salesItem[i].reminderDate;
                if (scope.sales.salesItem[i].productStock.product.schedule == "H" || scope.sales.salesItem[i].productStock.product.schedule == "H1" || scope.sales.salesItem[i].productStock.product.schedule == "X") {

                    schedulecount++;
                }
                salesItems.push(salesItem);
            }

            scope.schedulecount = schedulecount;
            scope.sales.salesItem = salesItems;



            for (var i = 0; i < scope.sales.salesItem.length + 1; i++) {

                scope.product.items.push(angular.copy(productItem));
            }

            for (var j = 0; j < scope.sales.salesItem.length; j++) {
                scope.product.items[j].sno = j + 1;
                scope.product.items[j].rowid = j + 1;
                scope.product.items[j].schedule = '';
                scope.product.items[j].id = scope.sales.salesItem[j].id;
                scope.product.items[j].accountId = scope.sales.salesItem[j].accountId;
                scope.product.items[j].name = scope.sales.salesItem[j].name;
                scope.product.items[j].product = scope.sales.salesItem[j].name;
                scope.product.items[j].vatInPrice = scope.sales.salesItem[j].vatInPrice;
                scope.product.items[j].discount = scope.sales.salesItem[j].discount;
                scope.product.items[j].age = scope.sales.salesItem[j].age;
                scope.product.items[j].totalStock = scope.sales.salesItem[j].totalStock;
                scope.product.items[j].soldqty = scope.sales.salesItem[j].soldqty;
                scope.product.items[j].productId = scope.sales.salesItem[j].productId;
                scope.product.items[j].vendorId = scope.sales.salesItem[j].vendorId;
                scope.product.items[j].batchNo = scope.sales.salesItem[j].batchNo;
                scope.product.items[j].expireDate = scope.sales.salesItem[j].expireDate;
                scope.product.items[j].vat = scope.sales.salesItem[j].vat;
                scope.product.items[j].sellingPrice = scope.sales.salesItem[j].sellingPrice;
                scope.product.items[j].sellingValue = scope.sales.salesItem[j].sellingPrice * parseInt(scope.sales.salesItem[j].quantity);
                scope.product.items[j].stock = scope.sales.salesItem[j].stock;
                scope.product.items[j].packageSize = scope.sales.salesItem[j].packageSize;
                scope.product.items[j].packagePurchasePrice = scope.sales.salesItem[j].packagePurchasePrice;
                scope.product.items[j].purchasePrice = scope.sales.salesItem[j].purchasePrice;
                scope.product.items[j].offlineStatus = scope.sales.salesItem[j].offlineStatus;
                scope.product.items[j].cst = scope.sales.salesItem[j].cst;
                scope.product.items[j].quantity = scope.sales.salesItem[j].quantity;
                scope.product.items[j].reminderFrequency = 'None';
                scope.product.items[j].reminderDate = '';

                scope.sales.salesItem[j].rowid = j + 1;

            }


            if (scope.isSelectedItemMain == 1) {
                scope.isSelectedItemMain = (scope.product.items.length) + 1;
            }
            scope.isSelectedItemMain--;
            scope.isCurrentRow = parseInt(scope.isSelectedItemMain) - 1;
            if (parseInt(scope.isSelectedItemMain) == parseInt(scope.product.items.length)) {
                scope.isLastRow = true;
            } else {
                scope.isLastRow = false;
            }




            scope.editId = salesItems.length + 1;
            scope.IsSelectedInvoiceSeries = false;
            scope.InvoiceSeriestype = 1;
            scope.Editsale = true;



            scope.sales.cashPayment = angular.copy(salesPaymentModel);
            scope.sales.cardPayment = angular.copy(salesPaymentModel);
            scope.sales.chequePayment = angular.copy(salesPaymentModel);
            scope.sales.walletPayment = angular.copy(salesPaymentModel);
            scope.sales.creditPayment = angular.copy(salesPaymentModel);




            scope.discountInValid = true;
            scope.isSalesTypeMandatory = false;

            scope.selectedBillDate = scope.sales.invoiceDate;
            loadCustomer(scope.sales);
            $.LoadingOverlay("hide");
            loadSalesType();
            scope.BillCalculations();
            scope.DiscountCalculations1();

            //Newly Added by DURGA 16-05-2017

            if (scope.sales.salesPayments.length > 0) {
                for (var i = 0; i < scope.sales.salesPayments.length; i++) {

                    if (scope.sales.salesPayments[i].paymentInd == 1) {
                        scope.sales.cashPayment = scope.sales.salesPayments[i];
                    }
                    if (scope.sales.salesPayments[i].paymentInd == 2) {
                        scope.sales.cardPayment = scope.sales.salesPayments[i];
                        scope.showCard = true;
                    }
                    if (scope.sales.salesPayments[i].paymentInd == 3) {
                        scope.sales.chequePayment = scope.sales.salesPayments[i];
                        scope.showCheque = true;
                    }
                    if (scope.sales.salesPayments[i].paymentInd == 4) {
                        scope.sales.creditPayment = scope.sales.salesPayments[i];
                    }
                    if (scope.sales.salesPayments[i].paymentInd == 5) {
                        scope.sales.walletPayment = scope.sales.salesPayments[i];
                        scope.showWallet = true;
                    }

                }
            }





        });

    }

    function loadCustomer(sales) {

        newcustomerHelper.data.patientSearchData.mobile = sales.mobile;
        newcustomerHelper.data.patientSearchData.name = sales.name;
        //if (sales.patient.empID != null && sales.patient.empID!= undefined)
        //    newcustomerHelper.data.patientSearchData.empID = sales.patient.empID;

        if (newcustomerHelper.data.patientSearchData.mobile != undefined && newcustomerHelper.data.patientSearchData.mobile != "") {

            newcustomerHelper.search(true, scope.PatientNameSearch);
        }

    }

    function loadSalesType() {
        newsalesService.getSalesType().then(function (response) {
            console.log(JSON.stringify(response.data));
            scope.sales.saleType = response.data;
        });
    }



    function getSalesObject() {
        return {
            "salesItem": [],
            "discount": "",
            "total": 0,
            "TotalQuantity": 0,
            "rackNo": "",
            "vat": 0,
            "cashType": "Full",
            "paymentType": "Cash",
            "deliveryType": "Counter",
            "billPrint": false,
            "sendEmail": false,
            "sendSms": false,
            "doctorMobile": null,
            "credit": null,
            "cardNo": null,
            "cardDate": null,
            "cardName": null,
            "cardDigits": null,
            "cardType": null,
            "isCreditEdit": false,
            "changeDiscount": "",
            "cashPayment": angular.copy(salesPaymentModel),
            "cardPayment": angular.copy(salesPaymentModel),
            "chequePayment": angular.copy(salesPaymentModel),
            "walletPayment": angular.copy(salesPaymentModel),
            "creditPayment": angular.copy(salesPaymentModel),
            "salesPayments": [],
            "overallDiscount": "",
            "pendingAmountChecked": false,
            "cardDateError": false,
            "cardDateFormatError": false,
            "chequeDateError": false,
            "chequeDateFormatError": false
        };
    }

});