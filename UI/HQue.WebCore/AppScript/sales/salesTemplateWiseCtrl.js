﻿app.controller('salesTemplateWiseCtrl', function ($scope, vendorOrderService, toastr, $rootScope, $filter, ModalService, salesService, salesTemplateModel, close, cacheService) {

    var salesTemplate = salesTemplateModel;
    $scope.salesTemplate = salesTemplate;
    $scope.salesTemplate.salesTemplateItem = [];
    $scope.templateNameList = [];
    $scope.SelectedTemplateNameList = [];
    $scope.focustemplate = true;

   
    //$scope.init = function () {

    //    vendorOrderService.getInstanceData().then(function (response) {
    //        $scope.instance = response.data;
    //    }, function () {
    //    });

    //}
    salesTemplateNameList();

    function salesTemplateNameList() {
       
        salesService.getTemplateNameList().then(function (response) {
            $scope.templateNameList = response.data;
            SetTemplateNameDetails();
            $scope.focustemplate = true;
        }, function () {
        });
    }

    function SetTemplateNameDetails() {
        for (var i = 0; i < $scope.templateNameList.length; i++) {
           
            $scope.templateNameList[i].templateName = ($scope.templateNameList[i].templateName);
        } 
    };
    
    $scope.selectedSalesTemplateName = function (selectedTemplate) {

        $scope.salesTemplate.salesTemplateItem = [];
        $scope.SelectedTemplateNameList = [];

        salesService.getSelectedTemplateNameList(selectedTemplate.id).then(function (response) {
            $scope.SelectedTemplateNameList = response.data;
            for (var i = 0; i < $scope.SelectedTemplateNameList.length; i++) {
                $scope.salesTemplate.salesTemplateItem.push($scope.SelectedTemplateNameList[i]);
            }

            for (var i = 0; i < $scope.salesTemplate.salesTemplateItem.length; i++) {
                if ($scope.salesTemplate.salesTemplateItem[i].name == null || $scope.salesTemplate.salesTemplateItem[i].name == undefined) {
                    $scope.salesTemplate.salesTemplateItem[i].name = $scope.salesTemplate.salesTemplateItem[i].product.name;
                }
            }
            document.getElementById("tempName").focus();
        }, function () {
        });

     

    };
    $scope.selectedSalesTemplate = function (selectedTemplate) {
         cacheService.set('selectedSalesTemplate', selectedTemplate);
         $scope.close('Yes');
    };
    
    $rootScope.$on("returnTemplatePopupEvent", function () {
        $scope.close("No");
    });

    $scope.close = function (result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
        if (result != "Yes") {
            $scope.templateNameList = [];
        }
        $(".modal-backdrop").hide();
    };

   
});

