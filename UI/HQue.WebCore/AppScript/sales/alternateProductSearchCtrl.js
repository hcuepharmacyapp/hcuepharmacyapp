app.controller('alternateProductSearchCtrl', function ($scope, $rootScope, selectedProductId, selectedProduct, close, toastr, cacheService, ModalService, productService, productStockService, productModel) {
    var product = productModel;
    $scope.search = product;
    $scope.focustxtProduct = true;
    $scope.list = [];
    $scope.search.drugName = null;
    $scope.isFormInvalid = true;
    $scope.searchValid = true;
    $scope.searchInValid = false;
    $scope.getProducts = function (val) {
        return productStockService.drugFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

   

    //code by nandhini 

    $scope.getGeneric = function (val) {

        return productService.genericFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    $scope.search.manufacShow = false;
    //end
    $scope.addProduct = function () {
        $scope.close('Yes');
        $.LoadingOverlay("show");
        productStockService.productBatch($scope.search.selectedAlternate).then(function (response) {
            cacheService.set('selectedAlternate1', response.data);
            $rootScope.$emit("alternateSelection", response.data);
            var qty = document.getElementById("quantity");
            qty.focus();
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            toastr.error("Error Ocured", "Error");
        });
    };
    $scope.getProductDetails = function () {
        return productStockService.getProductDetails($scope.search.drugName.product.id).then(function (response) {
            var name1 = $scope.search.drugName.name;
            $scope.search = response.data;
            $scope.search.manufacShow = true;
            $scope.search.drugName = name1;
            $scope.products.genericName = null;
        });
    };
    //code by nandhini
    $scope.getGenericDetails = function () {
        var pGenericName = "";

        if ($scope.products.genericName.genericName != undefined)
        {
            pGenericName = $scope.products.genericName.genericName;
        }
        else
        {
            pGenericName = $scope.products.genericName;
        }
        return productService.getGenericDetails(pGenericName).then(function (response) {
           // var name1 = $scope.products.genericName.genericName;
            //var temp1 = response.data;
            $scope.search = response.data;
            $scope.search.manufacShow = false;
            $scope.products.genericName = pGenericName;
        });
    };
    //end

    $scope.setFormValid = function (val) {
        if (val > 0) {
            $scope.isFormInvalid = false;
        } else {
            $scope.isFormInvalid = true;
        }
    };
    $scope.close = function (result) {
        close(result, 500);
        if (result != "Yes") {
            $scope.search.drugName = null;
            if ($scope.products != undefined)
            {
                $scope.products.genericName = null;
            }
            $scope.search = null;
            $rootScope.$emit("alternateDetailCancel", null);
        }
        $(".modal-backdrop").hide();
    };
    if (selectedProductId != null) {
        $scope.searchValid = false;
        $scope.searchInValid = true;
        return productStockService.getProductDetails(selectedProductId).then(function (response) {
            $scope.search = response.data;
            $scope.search.drugName = selectedProduct.name;
            if ($scope.products != undefined) {
                $scope.products.genericName = null;
            }
        }, function (error) {
            console.log(error);
            toastr.error("Error Ocured", "Error");
        });
    }
    //code nandhini
    if (selectedProductId != null) {
        $scope.searchValid = false;
        $scope.searchInValid = true;
        return productService.getGenericDetails(selectedProductId).then(function (response) {
            $scope.search = response.data;
            $scope.products.genericName = selectedProduct.name;
        }, function (error) {
            console.log(error);
            toastr.error("Error Ocured", "Error");
        });
    }

   
    if (selectedProduct != null) {
     
        return productService.getGenericDetails(selectedProduct.genericName).then(function (response) {
            $scope.search = response.data;

            //var str = selectedProduct.genericName.replace(/\s+/g, '%2B');
            //console.log(str);
          
            $scope.products = {genericName:selectedProduct.genericName};

        });
    }

    //end
});