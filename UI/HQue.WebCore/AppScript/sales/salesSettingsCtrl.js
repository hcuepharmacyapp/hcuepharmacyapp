﻿app.controller('salesSettingsCtrl', function ($scope, $rootScope, $location, $window, toastr, salesModel, salesService, cacheService, ModalService, discountRulesModel, $filter, customerReceiptService, inventorySmsSettingsModel, salesTypeModel, salesBatchPopUpSettingsModel, smsSettingsModel, vendorPurchaseService, taxValuesModel, loyaltyPointSettingsModel, saleSettingsModel) {
    $scope.BatchPopUpSettingsModel = salesBatchPopUpSettingsModel;
    $scope.taxValues = taxValuesModel;
    $scope.autoScanQty = 0;
    $scope.autoTempStockAdd = false;
    $scope.minDate = new Date();
    var d = new Date();
    //Added by Sarubala on 23-11-17
    $scope.smsSettingsModel = angular.copy(smsSettingsModel);
    $scope.selectAllSmsSettings = false;
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.open3 = function () {
        $scope.popup3.opened = true;
    };
    $scope.popup3 = {
        opened: false
    };
    $scope.open4 = function () {
        $scope.popup4.opened = true;
    };
    $scope.popup4 = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.smsSettings = inventorySmsSettingsModel;
    $scope.smsSettings.smsType = "Select";
    $scope.smsSettings.smsOption = "No";
    var sales = salesModel;
    $scope.count = 0;
    $scope.count1 = 0;
    $scope.slabDiscountList = [];
    $scope.Math = window.Math;
    $scope.sales = sales;
    $scope.sales.salesItem = [],
    $scope.sales.discount = 0;
    $scope.isDotMatrix = 0;
    $scope.printStatus = "Disable";
    $scope.printFooterNote = "";
    $scope.remainingSmsCount = 0; //Added by Sarubala on 28-11-17
    $scope.loyaltyPointSettings = angular.copy(loyaltyPointSettingsModel);
    $scope.loyaltyPointSettings.loyaltyProductPoints = null;
    $scope.saveSettings = function () {
        salesService.savePrintType($scope.isDotMatrix).then(function (response) {
            window.localStorage.setItem("IsDotMatrix", JSON.stringify($scope.isDotMatrix));
            toastr.success('Settings saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
        //window.localStorage.setItem("IsDotMatrix", JSON.stringify($scope.isDotMatrix));
        //toastr.success('Settings saved successfully');
    };
    $scope.saveBillPrintStatus = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        //console.log($scope.printStatus);
        //console.log($scope.printFooterNote);
        //salesService.saveBillPrintStatus($scope.printStatus).then(function (response) {
        salesService.saveBillPrintStatus($scope.printStatus, $scope.printFooterNote).then(function (response) {
            toastr.success('Print Status saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    //$scope.savePrintFooterNote = function () {
    //    $.LoadingOverlay("show");
    //    $scope.isProcessing = true;
    //    salesService.savePrintFooterNote($scope.printFooterNote).then(function (response) {
    //        toastr.success('Print Status saved successfully');
    //        $scope.isProcessing = false;
    //        $.LoadingOverlay("hide");
    //    }, function () {
    //        $.LoadingOverlay("hide");
    //        toastr.error('Error Occured', 'Error');
    //        $scope.isProcessing = false;
    //    });
    //};
    $scope.getSettings = function () {
        var salesDiscount = discountRulesModel;
        $scope.salesDiscount = salesDiscount;
        $scope.salesDiscount.discountItem = $scope.slabDiscountList;
        getPrintType();
        getPrintStatus();
        //$scope.isDotMatrix = JSON.parse(window.localStorage.getItem("IsDotMatrix"));
    };
    $scope.generaltab = "tabSelected";
    $scope.discounttab = "tabNormal";
    //Newly Added Gavaskar 24-10-2016 
    $scope.cardtab = "tabNormal";
    //Newly Added Gavaskar 07-11-2016
    $scope.smstab = "tabNormal";
    $scope.batchPopupSettingsTab = "tabNormal";
    $scope.taxSettingsTab = "tabNormal";
    $scope.Invoiceseries = "tabNormal";
    // $scope.batchPopupSettings = true;
    $scope.loyalPointSettingsTab = "tabNormal";
    $scope.prescriptiontab = "tabNormal";
    $scope.OnlyAlpha = function (event) {
        var charCode = event.keyCode;
        if ((document.getElementById("CustomseriesName").value.length == 0 && charCode == 111) || (document.getElementById("CustomerIDseriesName").value.length == 0 && charCode == 111)) {
            event.preventDefault();
            return;
        }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) {
            return true;
        } else {
            event.preventDefault();
        }
    };
    $scope.printSettings = function () {
        $scope.discountPanel = false;
        $scope.settingsPanel = false;
        $scope.InvoiceSeriesPanel = false;
        $scope.taxSettingPanel = false;
        //Newly Added Gavaskar 24-10-2016
        $scope.batchPopupSettings = false;
        $scope.loyaltyPointSettingsPanel = false;
        $scope.loyaltySettingsEnablePanel = false;
        $scope.loyaltySettingsOptionSave = false;
        $scope.CardPanel = false;
        $scope.generaltab = "tabSelected";
        $scope.discounttab = "tabNormal";
        $scope.batchPopupSettingsTab = "tabNormal";
        //Newly Added Gavaskar 24-10-2016
        $scope.cardtab = "tabNormal";
        //Newly Added Gavaskar 07-11-2016
        $scope.SmsPanel = false;
        $scope.smstab = "tabNormal";
        $scope.PrescriptionPanel = false;
        $scope.prescriptiontab = "tabNormal";
        $scope.taxSettingsTab = "tabNormal";
        $scope.Invoiceseries = "tabNormal";
        $scope.loyalPointSettingsTab = "tabNormal";
    };
    $scope.discountSettings = function () {
        $scope.discountPanel = true;
        $scope.settingsPanel = true;
        $scope.InvoiceSeriesPanel = false;
        $scope.taxSettingPanel = false;
        $scope.batchPopupSettings = false;
        $scope.loyaltyPointSettingsPanel = false;
        $scope.loyaltySettingsEnablePanel = false;
        $scope.loyaltySettingsOptionSave = false;
        //Newly Added Gavaskar 24-10-2016
        $scope.CardPanel = false;
        $scope.discounttab = "tabSelected";
        $scope.batchPopupSettingsTab = "tabNormal";
        $scope.generaltab = "tabNormal";
        //Newly Added Gavaskar 24-10-2016
        $scope.cardtab = "tabNormal";
        //Newly Added Gavaskar 07-11-2016
        $scope.SmsPanel = false;
        $scope.smstab = "tabNormal";
        $scope.PrescriptionPanel = false;
        $scope.taxSettingsTab = "tabNormal";
        $scope.prescriptiontab = "tabNormal";
        $scope.Invoiceseries = "tabNormal";
        $scope.loyalPointSettingsTab = "tabNormal";
        editDiscountRules();
        // $scope.loadSalesDiscount();
    };
    //Newly Added Gavaskar 24-10-2016 Start
    $scope.cardSettings = function () {
        $scope.discountPanel = false;
        $scope.settingsPanel = true;
        $scope.taxSettingPanel = false;
        $scope.InvoiceSeriesPanel = false;
        $scope.CardPanel = true;
        $scope.batchPopupSettings = false;
        $scope.loyaltyPointSettingsPanel = false;
        $scope.loyaltySettingsEnablePanel = false;
        $scope.loyaltySettingsOptionSave = false;
        $scope.cardtab = "tabSelected";
        $scope.discounttab = "tabNormal";
        $scope.batchPopupSettingsTab = "tabNormal";
        $scope.generaltab = "tabNormal";
        //Newly Added Gavaskar 07-11-2016
        $scope.SmsPanel = false;
        $scope.smstab = "tabNormal";
        $scope.PrescriptionPanel = false;
        $scope.prescriptiontab = "tabNormal";
        $scope.taxSettingsTab = "tabNormal";
        $scope.Invoiceseries = "tabNormal";
        $scope.loyalPointSettingsTab = "tabNormal";
        getCardType();
        getBatchListType();
        getShortCutKeySetting();
        getShortName();
        getCompleteSaleKeys();
    };
    $scope.InvoiceSeriesSettings = function () {
        $scope.discountPanel = false;
        $scope.settingsPanel = true;
        $scope.taxSettingPanel = false;
        $scope.batchPopupSettings = false;
        $scope.loyaltyPointSettingsPanel = false;
        $scope.loyaltySettingsEnablePanel = false;
        $scope.loyaltySettingsOptionSave = false;
        $scope.CardPanel = false;
        $scope.InvoiceSeriesPanel = true;
        $scope.Invoiceseries = "tabSelected";
        $scope.discounttab = "tabNormal";
        $scope.generaltab = "tabNormal";
        $scope.batchPopupSettingsTab = "tabNormal";
        $scope.cardtab = "tabNormal";
        //Newly Added Gavaskar 07-11-2016
        $scope.SmsPanel = false;
        $scope.smstab = "tabNormal";
        $scope.taxSettingsTab = "tabNormal";
        $scope.PrescriptionPanel = false;
        $scope.prescriptiontab = "tabNormal";
        $scope.loyalPointSettingsTab = "tabNormal";
    };
    //Newly Added Gavaskar 24-10-2016 End
    //Newly Added Gavaskar 07-11-2016 Start
    $scope.smsCallSetting = function () {
        $scope.discountPanel = false;
        $scope.settingsPanel = true;
        $scope.taxSettingPanel = false;
        $scope.CardPanel = false;
        $scope.SmsPanel = true;
        $scope.InvoiceSeriesPanel = false;
        $scope.batchPopupSettings = false;
        $scope.loyaltyPointSettingsPanel = false;
        $scope.loyaltySettingsEnablePanel = false;
        $scope.loyaltySettingsOptionSave = false;
        $scope.smstab = "tabSelected";
        $scope.cardtab = "tabNormal";
        $scope.discounttab = "tabNormal";
        $scope.generaltab = "tabNormal";
        $scope.PrescriptionPanel = false;
        $scope.batchPopupSettingsTab = "tabNormal";
        $scope.prescriptiontab = "tabNormal";
        $scope.taxSettingsTab = "tabNormal";
        $scope.Invoiceseries = "tabNormal";
        $scope.loyalPointSettingsTab = "tabNormal";
        getSmsSetting();
        $scope.smsSettings1();
    };
    $scope.prescribeSetting = function () {
        $scope.discountPanel = false;
        $scope.settingsPanel = true;
        $scope.taxSettingPanel = false;
        $scope.CardPanel = false;
        $scope.SmsPanel = false;
        $scope.InvoiceSeriesPanel = false;
        $scope.PrescriptionPanel = true;
        $scope.batchPopupSettings = false;
        $scope.loyaltyPointSettingsPanel = false;
        $scope.loyaltySettingsEnablePanel = false;
        $scope.loyaltySettingsOptionSave = false;
        $scope.prescriptiontab = "tabSelected";
        $scope.batchPopupSettingsTab = "tabNormal";
        $scope.cardtab = "tabNormal";
        $scope.discounttab = "tabNormal";
        $scope.taxSettingsTab = "tabNormal";
        $scope.generaltab = "tabNormal";
        $scope.smstab = "tabNormal";
        $scope.Invoiceseries = "tabNormal";
        $scope.loyalPointSettingsTab = "tabNormal";
        getPrescriptionSetting();
    };
    $scope.showbatchPopupSettings = function () {
        $scope.settingsPanel = true;
        $scope.discountPanel = false;
        $scope.taxSettingPanel = false;
        $scope.batchPopupSettings = true;
        $scope.loyaltyPointSettingsPanel = false;
        $scope.loyaltySettingsEnablePanel = false;
        $scope.loyaltySettingsOptionSave = false;
        $scope.CardPanel = false;
        $scope.SmsPanel = false;
        $scope.InvoiceSeriesPanel = false;
        $scope.PrescriptionPanel = false;
        $scope.batchPopupSettingsTab = "tabSelected";
        $scope.prescriptiontab = false;
        $scope.cardtab = "tabNormal";
        $scope.taxSettingsTab = "tabNormal";
        $scope.discounttab = "tabNormal";
        $scope.generaltab = "tabNormal";
        $scope.smstab = "tabNormal";
        $scope.Invoiceseries = "tabNormal";
        $scope.prescriptiontab = "tabNormal";
        $scope.loyalPointSettingsTab = "tabNormal";
    };
    //Newly Added Gavaskar 07-11-2016 End

    $scope.showLoyaltyPointSettings = function () {
        $scope.settingsPanel = true;
        $scope.discountPanel = false;
        $scope.batchPopupSettings = false;
        $scope.loyaltyPointSettingsPanel = false;
        $scope.loyaltySettingsEnablePanel = true;
        $scope.CardPanel = false;
        $scope.SmsPanel = false;
        $scope.InvoiceSeriesPanel = false;
        $scope.PrescriptionPanel = false;
        $scope.loyaltyPointSettings.loyaltyOption = "No";
        $scope.loyaltySettingsOptionSave = true;
        $scope.batchPopupSettingsTab = "tabNormal";
        $scope.cardtab = "tabNormal";
        $scope.discounttab = "tabNormal";
        $scope.generaltab = "tabNormal";
        $scope.smstab = "tabNormal";
        $scope.Invoiceseries = "tabNormal";
        $scope.loyalPointSettingsTab = "tabSelected";
        $scope.ChangeLoyaltyOption();
        $scope.kindOfProduct();

    };

    $scope.validFromDate = false;
    $scope.validToDate = false;
    $scope.today1 = $scope.minDate.toISOString().slice(0, 10);
    function editDiscountRules() {
        salesService.editDiscountDetail().then(function (response) {
            $scope.salesDiscount = response.data;
            var overall = document.getElementById("salesAmount");
            if ($scope.salesDiscount.billAmountType == 'overallDiscount') {
                $scope.salesDiscount.amount = 0;
                overall.disabled = true;
                $scope.overAllPanel = true;
                $scope.slabPanel = false;
                $scope.billAmount = false;
            }
            else if ($scope.salesDiscount.billAmountType == 'slabDiscount') {
                $scope.overAllPanel = false;
                $scope.slabPanel = true;
                $scope.billAmount = false;
                $scope.salesDiscount.amount = 0;
                $scope.salesDiscount.discount = 0;
                if ($scope.salesDiscount.discountItem.length != 5) {
                    var len = $scope.salesDiscount.discountItem.length;
                    for (var k = len; k < 5; k++)
                        $scope.salesDiscount.discountItem[k] = $scope.slabDiscountList[k];
                }
            }
            else if ($scope.salesDiscount.billAmountType == 'customerWiseDiscount' || $scope.salesDiscount.billAmountType == 'productWiseDiscount' || $scope.salesDiscount.billAmountType == 'productCustomerWiseDiscount') {
                $scope.overAllPanel = false;
                $scope.slabPanel = false;
                $scope.billAmount = false;
            }
            else {
                $scope.billAmount = true;
                overall.disabled = false;
                $scope.overAllPanel = true;
                $scope.slabPanel = false;
            }
            var fdate = $scope.salesDiscount.fromDate.slice(0, 10);
            var tdate = $scope.salesDiscount.toDate.slice(0, 10);
            $scope.validFromDate = false;
            $scope.validToDate = false;
            if (fdate > $scope.today1) {
                $scope.validFromDate = true;
            }
            if (tdate < $scope.today1) {
                $scope.validToDate = true;
            }
        });
    }
    $scope.overAll = function (overAllDisc) {
        var BillAmountType = null;
        if (overAllDisc == 'overAllDisc') {
            var overall = document.getElementById("salesAmount");
            $scope.salesDiscount.amount = 0;
            overall.disabled = true;
            $scope.overAllPanel = true;
            $scope.slabPanel = false;
            $scope.billAmount = false;
            BillAmountType = "overallDiscount";
            $scope.salesDiscount.discountItem[0].amount = 0;
            $scope.salesDiscount.discountItem[0].toAmount = 0;
            $scope.salesDiscount.discountItem[0].discount = 0;
        }
        else if (overAllDisc == 'slab') {
            $scope.overAllPanel = false;
            $scope.slabPanel = true;
            $scope.billAmount = false;
            $scope.salesDiscount.discountItem = $scope.slabDiscountList;
            $scope.salesDiscount.amount = 0;
            $scope.salesDiscount.discount = 0;
            BillAmountType = "slabDiscount";
        }
        else if (overAllDisc == 'customerBased') {
            var overall = document.getElementById("salesAmount");
            overall.disabled = true;
            $scope.overAllPanel = false;
            $scope.slabPanel = false;
            $scope.billAmount = false;
            BillAmountType = "customerWiseDiscount";
            $scope.salesDiscount.amount = 0;
            $scope.salesDiscount.discount = 0;
            $scope.salesDiscount.discountItem[0].amount = 0;
            $scope.salesDiscount.discountItem[0].toAmount = 0;
            $scope.salesDiscount.discountItem[0].discount = 0;
        }
        else if (overAllDisc == 'productBased') {
            var overall = document.getElementById("salesAmount");
            overall.disabled = true;
            $scope.overAllPanel = false;
            $scope.slabPanel = false;
            $scope.billAmount = false;
            BillAmountType = "productWiseDiscount";
            $scope.salesDiscount.amount = 0;
            $scope.salesDiscount.discount = 0;
            $scope.salesDiscount.discountItem[0].amount = 0;
            $scope.salesDiscount.discountItem[0].toAmount = 0;
            $scope.salesDiscount.discountItem[0].discount = 0;
        }
        else if (overAllDisc == 'productCustomerBased') {
            var overall = document.getElementById("salesAmount");
            overall.disabled = true;
            $scope.overAllPanel = false;
            $scope.slabPanel = false;
            $scope.billAmount = false;
            BillAmountType = "productCustomerWiseDiscount";
            $scope.salesDiscount.amount = 0;
            $scope.salesDiscount.discount = 0;
            $scope.salesDiscount.discountItem[0].amount = 0;
            $scope.salesDiscount.discountItem[0].toAmount = 0;
            $scope.salesDiscount.discountItem[0].discount = 0;
        }
        else {
            var overall = document.getElementById("salesAmount");
            overall.disabled = false;
            $scope.overAllPanel = true;
            $scope.billAmount = true;
            $scope.slabPanel = false;
            BillAmountType = "billAmountDiscount";
            $scope.salesDiscount.discountItem[0].amount = 0;
            $scope.salesDiscount.discountItem[0].toAmount = 0;
            $scope.salesDiscount.discountItem[0].discount = 0;
        }
        //$scope.salesDiscount = [];
        $scope.salesDiscount.billAmountType = BillAmountType;
        //salesService.editDiscountDetail1(BillAmountType).then(function (response) {
        //    $scope.salesDiscount = response.data;
        //    $scope.salesDiscount.billAmountType = BillAmountType;
        //    if ($scope.salesDiscount.billAmountType == "slabDiscount") {
        //        if ($scope.salesDiscount.discountItem.length != 5)
        //            $scope.salesDiscount.discountItem = $scope.slabDiscountList;
        //        $scope.salesDiscount.amount = 0.0;
        //        $scope.salesDiscount.discount = 0.0;
        //    }
        //    else
        //        $scope.salesDiscount.toAmount = null;
        //});
        //$scope.salesDiscount.amount = disabled;
    };
    $scope.slabDiscountList = [];
    $scope.slabDiscountList = [{ sid: 'slabDiscount1', errorMessage1: false, errorMessage2: false }, { sid: 'slabDiscount2', errorMessage1: false, errorMessage2: false }, { sid: 'slabDiscount3', errorMessage1: false, errorMessage2: false },
                                { sid: 'slabDiscount4', errorMessage1: false, errorMessage2: false }, { sid: 'slabDiscount5', errorMessage1: false, errorMessage2: false }];
    $scope.slabDiscount = function (overAllDisc) {
        //$scope.slabDiscountList[0] = $scope.salesDiscount;
        $scope.salesDiscount.discountItem[0] = $scope.salesDiscount;
        //$scope.salesDiscount.amount = disabled;
    };
    $scope.maxDiscountExceeds = false;
    $scope.keyEnter = function (event, e, x) {
        $scope.maxDiscountExceeds = false;
        if ($scope.maxDiscountFixed == 'Yes') {
            if ($scope.salesDiscount.discount > $scope.maxDisountValue) {
                $scope.maxDiscountExceeds = true;
                return false;
            }
        }
        //Added by Sarubala on 16-08-17
        if ($scope.salesDiscount.billAmountType == "slabDiscount" && $scope.maxDiscountFixed == 'Yes') {
            var errorCount = 0;
            $scope.maxDiscountExceeds = false;
            for (var x = 0; x < $scope.salesDiscount.discountItem.length; x++) {
                if ($scope.salesDiscount.discountItem[x].discount > $scope.maxDisountValue) {
                    errorCount++;
                }
            }
            if (errorCount > 0) {
                $scope.maxDiscountExceeds = true;
                return false;
            }
        }

        if ($scope.salesDiscount.billAmountType == "slabDiscount") {
            for (var x = 0; x < $scope.salesDiscount.discountItem.length; x++) {
                if (x == 0) {
                    if ($scope.salesDiscount.discountItem[x].toAmount != undefined) {
                        if (parseFloat($scope.salesDiscount.discountItem[x].toAmount) > parseFloat($scope.salesDiscount.discountItem[x].amount)) {
                            $scope.salesDiscount.discountItem[x].errorMessage2 = false;
                        }
                        else {
                            $scope.salesDiscount.discountItem[x].errorMessage2 = 'To Amount Must be greater than From Amount';
                        }
                    }
                }
                else {
                    var p = x - 1;
                    var f = parseFloat($scope.salesDiscount.discountItem[x].amount);
                    var g = parseFloat($scope.salesDiscount.discountItem[p].toAmount);
                    if (parseFloat($scope.salesDiscount.discountItem[x].amount) <= parseFloat($scope.salesDiscount.discountItem[p].toAmount)) {
                        $scope.salesDiscount.discountItem[x].errorMessage1 = 'From Amount Must be greater than previous To Amount';
                    }
                    else {
                        $scope.salesDiscount.discountItem[x].errorMessage1 = false;
                    }
                    if (parseFloat($scope.salesDiscount.discountItem[x].toAmount) <= parseFloat($scope.salesDiscount.discountItem[x].amount)) {
                        $scope.salesDiscount.discountItem[x].errorMessage2 = 'To Amount Must be greater than From Amount';
                    }
                    else {
                        $scope.salesDiscount.discountItem[x].errorMessage2 = false;
                    }
                }
            }
            $scope.count = 0;
            $scope.count1 = 0;
            for (var j = 0; j < $scope.salesDiscount.discountItem.length; j++) {
                if ($scope.salesDiscount.discountItem[j].hasOwnProperty("errorMessage1") && $scope.salesDiscount.discountItem[j].errorMessage1 != false) {
                    $scope.count = $scope.count + 1;
                }
                if ($scope.salesDiscount.discountItem[j].hasOwnProperty("errorMessage2") && $scope.salesDiscount.discountItem[j].errorMessage2 != false) {
                    $scope.count1 = $scope.count1 + 1;
                }
            }
        }
        var ele = document.getElementById(e);
        if (event.which === 13) // Enter key
        {
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
            {
                ele.focus();
                if (ele.nodeName != "BUTTON")
                    ele.select();
            }
        }
    };
    $scope.validateFromDate = function () {
        //var val = (toDate1($scope.salesDiscountForm.fromDate.$viewValue) <= $scope.minDate);        
        //$scope.salesDiscountForm.fromDate.$setValidity("InValidFromeDate", val);
        //$scope.isFormValid = val;
        if ($scope.salesDiscountForm.toDate.$viewValue != null) {
            val = (toDate1($scope.salesDiscountForm.fromDate.$viewValue) <= toDate1($scope.salesDiscountForm.toDate.$viewValue));
            $scope.salesDiscountForm.fromDate.$setValidity("InValidFromeDate", val);
            $scope.isFormValid = val;
            $scope.validateToDate();
        }
        var fromdate1 = new Date(toDate1($scope.salesDiscountForm.fromDate.$viewValue));
        $scope.minDate.setHours(0, 0, 0, 0);
        if (fromdate1 > $scope.minDate) {
            $scope.validFromDate = true;
        }
        else {
            $scope.validFromDate = false;
        }
    };
    $scope.validateToDate = function () {
        $scope.minDate.setHours(0, 0, 0, 0);
        var val = ((toDate1($scope.salesDiscountForm.toDate.$viewValue) >= $scope.minDate) && (toDate1($scope.salesDiscountForm.fromDate.$viewValue) <= toDate1($scope.salesDiscountForm.toDate.$viewValue)));
        $scope.salesDiscountForm.toDate.$setValidity("InValidToDate", val);
        var fval = (toDate1($scope.salesDiscountForm.fromDate.$viewValue) <= toDate1($scope.salesDiscountForm.toDate.$viewValue));
        $scope.salesDiscountForm.fromDate.$setValidity("InValidFromeDate", fval);
        $scope.isFormValid = val;
        //if ($scope.salesDiscountForm.fromDate.$viewValue != null) {
        //    val = (toDate1($scope.salesDiscountForm.fromDate.$viewValue) <= toDate1($scope.salesDiscountForm.toDate.$viewValue));
        //    $scope.salesDiscountForm.fromDate.$setValidity("InValidFromeDate", val);
        //    $scope.isFormValid = val;
        //}
        var todate1 = new Date(toDate1($scope.salesDiscountForm.toDate.$viewValue));
        if (todate1 < $scope.minDate) {
            $scope.validToDate = true;
        }
        else {
            $scope.validToDate = false;
        }
    };
    function toDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 0, parts[0]);
    }
    function toDate1(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
    $scope.saveSalesDiscount = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if ($scope.salesDiscount.billAmountType == "slabDiscount") {
            $scope.salesDiscount.id = null;
        }
        if ($scope.salesDiscount.billAmountType == "overallDiscount")
            $scope.salesDiscount.amount = 0;
        else if ($scope.salesDiscount.billAmountType == "customerWiseDiscount" || $scope.salesDiscount.billAmountType == "productWiseDiscount" || $scope.salesDiscount.billAmountType == "productCustomerWiseDiscount") {
            $scope.salesDiscount.amount = 0;
            $scope.salesDiscount.discount = 0;
        }

        salesService.saveDiscountRules($scope.salesDiscount).then(function (response) {
            $scope.salesDiscount = response.data;
            if (($scope.salesDiscount.billAmountType == "slabDiscount") && ($scope.salesDiscount.discountItem.length != 5)) {
                var len = $scope.salesDiscount.discountItem.length;
                for (var k = len; k < 5; k++)
                    $scope.salesDiscount.discountItem[k] = $scope.slabDiscountList[k];
            }
            toastr.success('Settings saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
        //window.localStorage.setItem("s_discount", JSON.stringify($scope.salesDiscount));
    };
    //Newly Added Gavaskar 24-10-2016 Start
    $scope.saveSalesCard = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        salesService.saveCardType($scope.cardType).then(function (response) {
            toastr.success('Sale card type completed successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    $scope.cardType = "Last4Digits";
    getCardType = function () {
        salesService.getCardTypeDetail().then(function (response) {
            $scope.cardType = response.data;
            if ($scope.cardType == '' || $scope.cardType == null) {
                $scope.cardType = "Last4Digits";
            }
        }, function () {
        });
    };
    //Newly Added Gavaskar 24-10-2016 End
    $scope.customSeriesName = "";
    $scope.saveInvoiceSeries = function () {
        //console.log($scope.InvocieSeries);
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        var Series = 10;
        if ($scope.InvocieSeries == 'InvoiceNumeric') {
            Series = 1; //invoice number in numeric
        }
        if ($scope.InvocieSeries == 'InvoiceCustom') {
            Series = 2;  //invoice in custom series
        }
        if ($scope.InvocieSeries == 'InvoiceManual') {
            Series = 3;  //invoice in Manual Entry
        }
        salesService.saveInvoiceSeries(Series).then(function (response) {
            toastr.success('Invoice Series Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    $scope.InvocieSeries = "";
    $scope.ChangeSeries = function () {
        //console.log($scope.InvocieSeries);
        if ($scope.InvocieSeries == "InvoiceCustom") {
            getInvoiceSeriesItems();
        }
    };
    $scope.SaveCustomSeries = function () {
        if ($scope.InvoiceSeriesItems.length < 5) {
            if (document.getElementById("CustomseriesName").value.toString().indexOf('O') == 0 || document.getElementById("CustomseriesName").value.toString().indexOf('o') == 0) {
                toastr.error('"O" should not be first letter to create custom series');
                $scope.customSeriesName = "";
                return;
            }
            $.LoadingOverlay("show");
            $scope.isProcessing = true;
            var customseriesName = $scope.customSeriesName.toUpperCase();
            for (var i = 0; i < $scope.InvoiceSeriesItems.length; i++) {
                if ($scope.InvoiceSeriesItems[i].seriesName == customseriesName) {
                    $.LoadingOverlay("hide");
                    toastr.error('' + customseriesName + ' Series Already Exists');
                    var seriestxt = document.getElementById("CustomseriesName");
                    seriestxt.focus();
                    $scope.isProcessing = false;
                    return false;
                }
            }
            var Series = 0;
            if ($scope.InvocieSeries == 'InvoiceNumeric') {
                Series = 1; //invoice number in numeric
            }
            if ($scope.InvocieSeries == 'InvoiceCustom') {
                Series = 2;  //invoice in custom series
            }
            if ($scope.InvocieSeries == 'InvoiceManual') {
                Series = 3;  //invoice in Manual Entry
            }
            if ($scope.InvocieSeries == 'InvoiceCustomCredit') {
                Series = 4;  //invoice in custom credit series
            }
            salesService.saveInvoiceseriesItem(customseriesName, Series).then(function (response) {
                //console.log(JSON.stringify(response));
                if (response.data != null && response.data != "") {
                    toastr.success('Invoice Series Added successfully');
                    $scope.customSeriesName = "";
                    $scope.isProcessing = false;
                    getInvoiceSeriesItems();
                    $.LoadingOverlay("hide");
                }
                else {
                    $.LoadingOverlay("hide");
                    toastr.error('Error Occured', 'Error');
                    $scope.isProcessing = false;
                }
            }, function () {
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
                $scope.isProcessing = false;
            });
        } else {
            toastr.error('5 Custom Series Exceeds');
            $scope.customSeriesName = "";
        }
    };

    $scope.customerSeriesSettings = saleSettingsModel;
    //Added by Sarubala on 03-12-18 - start
    $scope.getCustomerSeriesSettings = function () {
        $.LoadingOverlay("show");
        salesService.getCustomerIdSeriesSetting().then(function (response) {           
            $scope.customerSeriesSettings.isEnableCustomerSeries = (response.data.isEnableCustomerSeries != null && response.data.isEnableCustomerSeries != undefined) ? response.data.isEnableCustomerSeries : false;
            $scope.customerSeriesSettings.customerIDSeries = (response.data.customerIDSeries != undefined) ? response.data.customerIDSeries : null;
            $.LoadingOverlay("hide");

        }, function (error) {
            toastr.error("Customer ID Series Loading Error");
            $.LoadingOverlay("hide");
        });
    };
    $scope.getCustomerSeriesSettings();

    $scope.saveCustomerSeriesSettings = function () {
        $.LoadingOverlay("show");

        if ($scope.customerSeriesSettings.isEnableCustomerSeries == "0") {
            $scope.customerSeriesSettings.isEnableCustomerSeries = false;
        } else {
            $scope.customerSeriesSettings.isEnableCustomerSeries = true;
        }

        if ($scope.customerSeriesSettings.isEnableCustomerSeries == false) {
            $scope.customerSeriesSettings.customerIDSeries = null;
        }

        salesService.saveCustomerIdSeriesSetting($scope.customerSeriesSettings).then(function (response) {            
            $scope.customerSeriesSettings = saleSettingsModel;
            $scope.getCustomerSeriesSettings();
            toastr.success("Customer ID Series Saved Successfully");
            $.LoadingOverlay("hide");
        }, function (error) {
            toastr.error("Customer ID Series Error in Saving");
            $.LoadingOverlay("hide");
        });
    };

    //Added by Sarubala on 03-12-18 - end

    //Newly Durga Added for Credit series settings 10-04-2017
    $scope.IsCreditInvoiceSeries = false;
    $scope.btnSaveIsCreditInvoiceSeries = function () {
        salesService.IsCreditInvoiceSeries($scope.IsCreditInvoiceSeries).then(function (response) {
            toastr.success('Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    }
    getIsCreditInvoiceSeries();
    function getIsCreditInvoiceSeries() {
        salesService.getIsCreditInvoiceSeries().then(function (response) {
            //console.log(JSON.stringify(response.data));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.IsCreditInvoiceSeries = "false";
            } else {
                if (response.data.isCreditInvoiceSeries != undefined) {
                    $scope.IsCreditInvoiceSeries = response.data.isCreditInvoiceSeries.toString();
                } else {
                    $scope.IsCreditInvoiceSeries = "false";
                }
            }
        }, function () {
        });
    }
    getMaxDiscountValue();
    function getMaxDiscountValue() {
        salesService.getMaxDiscountValue().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data != "" && response.data != null) {
                $scope.maxDisountValue = response.data.instanceMaxDiscount;
            }
        }, function () {
        });
    }
    $scope.CancelBillDays = "";
    $scope.savecancelBillDays = function () {
        //console.log($scope.CancelBillDays);
        salesService.savecancelBillDays($scope.CancelBillDays).then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data != null && response.data != "") {
                toastr.success('CancelBill days saved successfully');
                $scope.isProcessing = false;
                getCancelBillDays();
                $.LoadingOverlay("hide");
            }
            else {
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
                $scope.isProcessing = false;
            }
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    getCancelBillDays();
    function getCancelBillDays() {
        salesService.getCancelBillDays().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data != "" && response.data != null) {
                $scope.CancelBillDays = response.data.postCancelBillDays;
            }
        }, function () {
        });
    }
    $scope.InvoiceSeriesItems = [];
    getInvoiceSeriesItems = function () {
        salesService.getInvoiceSeriesItems().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }
        }, function () {
        });
    };

    $scope.InvoiceCreditSeriesItems = [];
    getInvoiceSeries();
    $scope.InvoiceSeriestype = "";
    function getInvoiceSeries() {
        salesService.getInvoiceSeries().then(function (response) {
            //console.log(JSON.stringify(response.data));
            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriestype = response.data.invoiceseriesType;
                //console.log($scope.InvoiceSeriestype);
                if ($scope.InvoiceSeriestype == 1) {
                    $scope.InvocieSeries = "InvoiceNumeric";
                }
                if ($scope.InvoiceSeriestype == 2) {
                    $scope.InvocieSeries = "InvoiceCustom";
                    getInvoiceSeriesItems();
                }
                if ($scope.InvoiceSeriestype == 3) {
                    $scope.InvocieSeries = "InvoiceManual";
                }
                if ($scope.InvoiceSeriestype == 4) {
                    $scope.InvocieSeries = "InvoiceCustomCredit";
                    getInvoiceCreditSeriesItems();
                }
            }
            else {
                $scope.InvocieSeries = "InvoiceNumeric";
            }
        }, function () {
        });
    }
    $scope.savePatientSearchType = function () {
        //$.LoadingOverlay("show");
        //$scope.isProcessing = true;
        //console.log($scope.PatientNameSearch);
        salesService.savePatientSearchType($scope.PatientNameSearch).then(function (response) {
            toastr.success('Patient Search type Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    $scope.saveDoctorSearchType = function () {
        //$.LoadingOverlay("show");
        //$scope.isProcessing = true;
        //console.log($scope.DoctorNameSearch);
        salesService.saveDoctorSearchType($scope.DoctorNameSearch).then(function (response) {
            toastr.success('Doctor Search type Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    $scope.saveDoctorNameMandatoryType = function () {
        //$.LoadingOverlay("show");
        //$scope.isProcessing = true;
        //console.log($scope.DoctorNameMandatory);
        salesService.saveDoctorMandatoryType($scope.DoctorNameMandatory).then(function (response) {
            toastr.success('Customer Mandatory type Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    //$scope.saveDoctorCustomerRequiredforAll = function () {      
    //    salesService.saveDoctorCustomerRequiredforAll($scope.invoiceWiseDoctorCustomer).then(function (response) {
    //        toastr.success('Doctor/Customer Mandatory Saved successfully');
    //        $scope.isProcessing = false;
    //        $.LoadingOverlay("hide");
    //    }, function () {
    //        $.LoadingOverlay("hide");
    //        toastr.error('Error Occured', 'Error');
    //        $scope.isProcessing = false;
    //    });
    //};

    $scope.saveSalestypeMandatory = function () {
        //console.log($scope.SalesTypeMandatory);
        //var selectedSalesTypes = $filter($scope.prescriptionSetting.salesTypeList, { status: true });
        var selectedSalesTypes = $filter('filter')($scope.prescriptionSetting.salesTypeList, { status: true }, true);

        if (($scope.SalesTypeMandatory == 1 && selectedSalesTypes.length > 0) || $scope.SalesTypeMandatory == 2) {
            salesService.saveSalesTypeMandatory($scope.SalesTypeMandatory).then(function (response) {
                toastr.success('SalesType Mandatory Saved successfully');
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
                $scope.isProcessing = false;
            });
        }
        else {
            $scope.SalesTypeMandatory = 2;
            toastr.error('Please select minimum one customer type in Prescription Setting', 'Error');
        }
    }
    $scope.saveAutoTempStockAdd = function () {
        salesService.saveAutoTempStockAdd($scope.autoTempStockAdd).then(function (response) {
            toastr.success('SalesType Mandatory Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    $scope.btnAutosavecustomer = function () {
        //console.log($scope.AutosaveCustomer);
        salesService.isAutosavecustomer($scope.AutosaveCustomer).then(function (response) {
            toastr.success('Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    }
    $scope.saveDiscountType = function () {
        //$.LoadingOverlay("show");
        //$scope.isProcessing = true;
        //console.log($scope.discountType);
        salesService.saveDiscountType($scope.discountType).then(function (response) {
            toastr.success('Discount type Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    $scope.isMaximumDiscountAvail = function () {
        if ($scope.maxDiscountFixed == "No") {
            $scope.maxDiscountExceeds = false;
            $scope.maxDisountValue = 0;
        }

        //Added by Sarubala on 28-08-17 to check Maximum discount - start
        if ($scope.maxDiscountFixed == "Yes") {
            if ($scope.validToDate == false || (toDate1($scope.salesDiscountForm.fromDate.$viewValue) <= toDate1($scope.salesDiscountForm.toDate.$viewValue))) {

                if ($scope.salesDiscount.billAmountType == "overallDiscount") {
                    if ($scope.salesDiscount.discount > $scope.maxDisountValue) {
                        toastr.error("Overall Discount value is greater than Maximum Discount");
                        return false;
                    }
                }
                else if ($scope.salesDiscount.billAmountType == "billAmountDiscount") {
                    if ($scope.salesDiscount.discount > $scope.maxDisountValue) {
                        toastr.error("Bill Discount value is greater than Maximum Discount");
                        return false;
                    }
                }
                else if ($scope.salesDiscount.billAmountType == "slabDiscount") {

                    var errorCount = 0;
                    for (var x = 0; x < $scope.salesDiscount.discountItem.length; x++) {
                        if ($scope.salesDiscount.discountItem[x].discount > $scope.maxDisountValue) {
                            errorCount++;
                        }
                    }
                    if (errorCount > 0) {
                        toastr.error("Slab Discount value is greater than Maximum Discount");
                        return false;
                    }
                }
            }
        }
        //Added by Sarubala on 28-08-17 to check Maximum discount - end

        salesService.isMaximumDiscountAvail($scope.maxDiscountFixed).then(function (response) {
            //console.log(response.data);
            $scope.ChangeMaxDiscount();
            toastr.success('Saved successfully');
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.maxDiscountFixed = "No";
            } else {
                if (response.data.maxDiscountAvail != undefined) {
                    $scope.maxDiscountFixed = response.data.maxDiscountAvail;
                } else {
                    $scope.maxDiscountFixed = "No";
                }
            }
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    $scope.maxDisountValue = "";
    $scope.ChangeMaxDiscount = function (value) {
        //$scope.maxDisountValue = value;
        //console.log($scope.maxDisountValue);
        if ($scope.maxDisountValue == "") {
        } else {
            //  $.LoadingOverlay("show");
            $scope.isProcessing = true;
            salesService.saveMaxDiscount($scope.maxDisountValue).then(function (response) {
                //console.log(JSON.stringify(response));
                if (response.data != null && response.data != "") {
                    //   toastr.success('Invoice Series Added successfully');
                    // toastr.success('Saved successfully');
                    $scope.maxDisountValue = response.data.instanceMaxDiscount;
                    $scope.isProcessing = false;
                    // $.LoadingOverlay("hide");
                }
                else {
                    //   $.LoadingOverlay("hide");
                    //  toastr.error('Error Occured', 'Error');
                    $scope.isProcessing = false;
                }
            }, function () {
                //   $.LoadingOverlay("hide");
                // toastr.error('Error Occured', 'Error');
                $scope.isProcessing = false;
            });
        }
    };
    getInvoiceSeriesItems = function () {
        salesService.getInvoiceSeriesItems().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }
        }, function () {
        });
    };
    getPatientSearchType();
    $scope.PatientNameSearch = "1";
    function getPatientSearchType() {
        salesService.getPatientSearchType().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.PatientNameSearch = "1";
            } else {
                if (response.data.patientSearchType != undefined) {
                    $scope.PatientNameSearch = response.data.patientSearchType;
                } else {
                    $scope.PatientNameSearch = "1";
                }
            }
        }, function () {
        });
    }
    getDoctorSearchType();
    $scope.DoctorNameSearch = "1";
    function getDoctorSearchType() {
        salesService.getDoctorSearchType().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.DoctorNameSearch = "1";
            } else {
                if (response.data.doctorSearchType != undefined) {
                    $scope.DoctorNameSearch = response.data.doctorSearchType;
                } else {
                    $scope.DoctorNameSearch = "1";
                }
            }
        }, function () {
        });
    }
    getDoctorNameMandatoryType();
    $scope.DoctorNameMandatory = "2";
    function getDoctorNameMandatoryType() {
        salesService.getAllSalesSettings().then(function (response) {  //Altered by Sarubala on 11-11-17
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.DoctorNameMandatory = "2";
            } else {
                if (response.data.doctorNameMandatoryType != undefined) {
                    $scope.DoctorNameMandatory = response.data.doctorNameMandatoryType;
                } else {
                    $scope.DoctorNameMandatory = "2";
                }
            }
        }, function () {
        });
    }
    getSalestypeMandatory();
    $scope.SalesTypeMandatory = "2";
    function getSalestypeMandatory() {
        salesService.getSalestypeMandatory().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.SalesTypeMandatory = "2";
            } else {
                if (response.data.saleTypeMandatory != undefined) {
                    $scope.SalesTypeMandatory = response.data.saleTypeMandatory;
                } else {
                    $scope.SalesTypeMandatory = "2";
                }
            }
        }, function () {
        });
    }
    $scope.getAutoTempstockAdd = function () {
        salesService.getAutoTempstockAdd()
            .then(function (response) {
                if (response.data != null || response.data != undefined) {
                    $scope.autoTempStockAdd = response.data.autoTempStockAdd;
                }
            }, function (error) {
                console.log(error);
            });
    };
    $scope.getAutoTempstockAdd();
    getautoSaveisMandatory();
    $scope.AutosaveCustomer = "2";
    function getautoSaveisMandatory() {
        salesService.getautoSaveisMandatory().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.AutosaveCustomer = "2";
            } else {
                if (response.data.autosaveCustomer != undefined) {
                    $scope.AutosaveCustomer = response.data.autosaveCustomer;
                } else {
                    $scope.AutosaveCustomer = "2";
                }
            }
        }, function () {
        });
    }
    getPharmacyDiscountType();
    $scope.discountType = "1";
    function getPharmacyDiscountType() {
        salesService.getPharmacyDiscountType().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.discountType = "1";
            } else {
                if (response.data.discountType != undefined) {
                    $scope.discountType = response.data.discountType;
                } else {
                    $scope.discountType = "1";
                }
            }
        }, function () {
        });
    }
    getIsMaxDiscountAvail();
    $scope.maxDiscountFixed = "No";
    function getIsMaxDiscountAvail() {
        salesService.getIsMaxDiscountAvail().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.maxDiscountFixed = "No";
            } else {
                if (response.data.maxDiscountAvail != undefined) {
                    $scope.maxDiscountFixed = response.data.maxDiscountAvail;
                } else {
                    $scope.maxDiscountFixed = "No";
                }
            }
        }, function () {
        });
    }
    getBatchListType = function () {
        salesService.getBatchListDetail().then(function (response) {
            $scope.batchListType = response.data;
            if ($scope.batchListType == null || $scope.batchListType == '') {
                $scope.batchListType = "Product";
            }
        }, function () {
        });
    };
    $scope.saveBatchType = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        salesService.saveBatchType($scope.batchListType).then(function (response) {
            toastr.success('Sale Batch type completed successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    //Newly Added Gavaskar 03-02-2017 Start
    getShortCutKeySetting = function () {
        salesService.getShortCutKeySetting().then(function (response) {
            if (response.data != "") {
                $scope.keyType = response.data.shortCutKeysType;
                if ($scope.keyType == true) {
                    $scope.keyType = "Yes";
                }
                else {
                    $scope.keyType = "No";
                }
                $scope.newWindowType = response.data.newWindowType;
                if ($scope.newWindowType == true) {
                    $scope.newWindowType = "Yes";
                }
                else {
                    $scope.newWindowType = "No";
                }
            }
            else {
                $scope.keyType = "Yes";
                $scope.newWindowType = "No";
            }
        }, function () {
        });
    };
    $scope.saveShortCutKeySetting = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if ($scope.keyType == "Yes") {
            $scope.keyType = true;
        }
        else {
            $scope.keyType = false;
        }
        if ($scope.newWindowType == "Yes") {
            $scope.newWindowType = true;
        }
        else {
            $scope.newWindowType = false;
        }
        salesService.saveShortCutKeySetting($scope.keyType, $scope.newWindowType).then(function (response) {
            toastr.success('ShortCut Keys Saved successfully');
            $scope.isProcessing = false;
            getShortCutKeySetting();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    //Newly Added Gavaskar 03-02-2017 End
    //Newly Added Gavaskar 24-10-2016 End
    //Newly Added Gavaskar 07-11-2016 Start
    $scope.saveSmsSetting = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        salesService.saveSmsSetting($scope.smsSettings).then(function (response) {
            toastr.success('SMS setting completed successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    getSmsSetting = function () {
        salesService.getSmsSetting($scope.smsSettings).then(function (response) {
            if (response.data != "") {
                $scope.smsSettings = response.data;
            }
        }, function () {
        });
    };
    //Newly Added Gavaskar 07-11-2016 End
    var salesType = salesTypeModel;
    $scope.prescriptionSetting = salesType;
    getPrescriptionSetting = function () {
        $scope.salesType = null;
        $scope.saveDisabled = true;
        salesService.getPrescriptionSetting().then(function (response) {
            if (response.data != "" && response.data != null) {
                $scope.prescriptionSetting = response.data;
            }
        }, function () {
        });
    };
    getPrescriptionSetting();
    $scope.selectType = function () {
        $scope.saveDisabled = false;
    };
    $scope.save = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        var selectedSalesTypes = $filter('filter')($scope.prescriptionSetting.salesTypeList, { status: true }, true);
        if ($scope.SalesTypeMandatory == 2 || ($scope.SalesTypeMandatory == 1 && selectedSalesTypes.length > 0)) {
            salesService.saveStatus($scope.prescriptionSetting).then(function (response) {
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
                toastr.success('Customer Type setting completed successfully');
                getPrescriptionSetting();
            }, function () {
                $.LoadingOverlay("hide");
                $scope.isProcessing = false;
            });
        }
        else {
            getPrescriptionSetting();
            toastr.error('The Customer type is mandatory in Sales Setting tab', 'Error');
            $.LoadingOverlay("hide");
        }
    };
    $scope.removeSalesType = function (id) {
        if (!confirm("Are you sure, Do you want to delete this sales type ? "))
            return;
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        salesService.removeSalesType(id).then(function (response) {
            if (response.data != null && response.data != "") {
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
                getPrescriptionSetting();
            }
            else {
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
                $scope.isProcessing = false;
            }
        }, function () {
            $.LoadingOverlay("hide");
            $scope.isProcessing = false;
        });
    };
    $scope.editSalesType = function (val) {
        val.isEdit = true;
        $scope.tmpValue = val.name;
    };
    $scope.updateSalesType = function (type1) {
        type1.isEdit = false;
        if (type1.name != null && type1.name != "") {
            $.LoadingOverlay("show");
            $scope.isProcessing = true;
            salesService.updateSalesType(type1).then(function (response) {
                if (response.data != null && response.data != "") {
                    $scope.isProcessing = false;
                    $.LoadingOverlay("hide");
                    getPrescriptionSetting();
                }
                else {
                    type1.name = $scope.tmpValue;
                    $.LoadingOverlay("hide");
                    toastr.error('Error Occured', 'Error');
                    $scope.isProcessing = false;
                }
            }, function () {
                $.LoadingOverlay("hide");
                $scope.isProcessing = false;
                toastr.error('Error Occured', 'Error');
                getPrescriptionSetting();
            });
        }
        else {
            type1.name = $scope.tmpValue;
        }
    };
    $scope.saveSalesType = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        salesService.saveSalesType($scope.salesType).then(function (response) {
            if (response.data != null && response.data != "") {
                toastr.success('Sales Type created successfully');
                $scope.isProcessing = false;
                $.LoadingOverlay("hide");
                getPrescriptionSetting();
            }
            else {
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
                $scope.isProcessing = false;
            }
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    getPrintType = function () {
        salesService.getPrintType().then(function (response) {
            $scope.isDotMatrix = response.data;
            window.localStorage.setItem("IsDotMatrix", JSON.stringify($scope.isDotMatrix));
        }, function () {
        });
    };
    //getPrintStatus = function () {
    //    salesService.getPrintStatus().then(function (response) {
    //        console.log("getPrintStatus data");
    //        console.log(JSON.stringify(response.data));
    //        $scope.printStatus = response.data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //};
    getPrintStatus = function () {
        salesService.getPrintStatus().then(function (response) {
            $scope.printStatus = "Disable";
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.printStatus = "Disable";
            } else {
                if (response.data.item1 != undefined) {
                    $scope.printStatus = response.data.item1;
                } else {
                    $scope.printStatus = "Disable";
                }
                $scope.printFooterNote = response.data.item2;
            }
        }, function (error) {
            console.log(error);
        });
    };
    getPrintFooterNote = function () {
        salesService.getPrintFooterNote().then(function (response) {
            $scope.printFooterNote = response.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.changeRoundSetting = function (value) {
        if (value) {
            $scope.isRoundEdit = true;
        } else {
            $scope.isRoundEdit = false;
        }
    };
    $scope.BatchPopUpSettings = function () {
        salesService.getBatchPopUpSettings().then(function (resp) {
            $scope.BatchPopUpSettingsModel = resp.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.BatchPopUpSettings();
    $scope.saveBatchPopupSettings = function () {
        $.LoadingOverlay("show");
        salesService.saveBatchPopUpSettings($scope.BatchPopUpSettingsModel)
            .then(function (resp) {
                $scope.BatchPopUpSettingsModel = resp.data;
                $.LoadingOverlay("hide");
                toastr.success('Settings saved successfully');
            }, function (error) {
                $.LoadingOverlay("hide");
                console.log(error);
                toastr.error('Error Occured', 'Error');
            });
    };
    getShortName = function () {
        salesService.getShortName().then(function (response) {
            if (response.data != "") {
                $scope.cutomerName = response.data.shortCustomerName;
                $scope.doctorName = response.data.shortDoctorName;
            }
            else {
                $scope.cutomerName = false;
                $scope.doctorName = false;
            }
        }, function () {
        });
    };
    $scope.saveShortName = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        salesService.saveShortName($scope.cutomerName, $scope.doctorName).then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data != null && response.data != "") {
                toastr.success('Short customer and doctor name saved successfully');
                $scope.isProcessing = false;
                getShortName();
                $.LoadingOverlay("hide");
            }
            else {
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
                $scope.isProcessing = false;
            }
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    $scope.saveCompleteSaleKeys = function () {
        salesService.saveCompleteSaleKeys($scope.completeSaleKeys).then(function (response) {
            toastr.success('Saved complete sale key successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    }
    $scope.completeSaleKeys = "1";
    function getCompleteSaleKeys() {
        salesService.getCompleteSaleKeys().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.completeSaleKeys = "1";
            } else {
                if (response.data.completeSaleKeyType != undefined) {
                    $scope.completeSaleKeys = response.data.completeSaleKeyType;
                } else {
                    $scope.completeSaleKeys = "1";
                }
            }
        }, function () {
        });
    }
    $scope.btnSaveScanBarcode = function () {
        //console.log($scope.scanBarcode, $scope.autoScanQty);
        salesService.saveBarcodeOption($scope.scanBarcode, $scope.autoScanQty).then(function (response) {
            toastr.success('Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    }
    getScanBarcodeOption();
    $scope.scanBarcode = "2";
    function getScanBarcodeOption() {
        salesService.getScanBarcodeOption().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.scanBarcode = "2";
            } else {
                if (response.data.scanBarcode != undefined) {
                    $scope.scanBarcode = response.data.scanBarcode;
                    $scope.autoScanQty = response.data.autoScanQty || 0;
                } else {
                    $scope.scanBarcode = "2";
                }
            }
        }, function () {
        });
    }


    //Newly Added By Gavaskar for Loyalty Point Settings
    $scope.getLoyaltyPointSettings = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        var loyaltyProductPoints = $scope.loyaltyPointSettings.loyaltyProductPoints;
        salesService.getLoyaltySettings().then(function (response) {

            $scope.loyaltyPointSettings = response.data;

            if ($scope.loyaltyPointSettings == undefined || $scope.loyaltyPointSettings == null || $scope.loyaltyPointSettings == "") {
                $scope.loyaltyPointSettings = angular.copy(loyaltyPointSettingsModel);
            }

            if ($scope.loyaltyPointSettings.loyaltyOption == true) {
                $scope.loyaltyPointSettings.loyaltyOption = "Yes";
                $scope.loyaltyPointSettingsPanel = true;
                $scope.loyaltySettingsOptionSave = false;
            }
            else {
                $scope.loyaltyPointSettings.loyaltyOption = "No";
                $scope.loyaltyPointSettingsPanel = false;
                $scope.loyaltySettingsOptionSave = true;
            }
            if ($scope.loyaltyPointSettings.isMinimumSale == true) {
                $scope.showMinimumSalaAmt = true;
            }
            else {
                $scope.showMinimumSalaAmt = false;
                $scope.loyaltyPointSettings.minimumSalesAmt = "0";
            }

            if ($scope.loyaltyPointSettings.isMaximumRedeem == true) {
                $scope.showMaximumRedeemAmt = true;
            }
            else {
                $scope.showMaximumRedeemAmt = false;
                $scope.loyaltyPointSettings.maximumRedeemPoint = "0";
            }

            if ($scope.loyaltyPointSettings.isEndDate == true) {
                $scope.showEndDate = true;
            }
            else {
                $scope.showEndDate = false;
                $scope.loyaltyPointSettings.endDate = "";
            }

            if ($scope.loyaltyPointSettings.loyaltyProductPoints == null || $scope.loyaltyPointSettings.loyaltyProductPoints == undefined || $scope.loyaltyPointSettings.loyaltyProductPoints.length == 0) {
                $scope.loyaltyPointSettings.loyaltyProductPoints = loyaltyProductPoints;
            }
            else if ($scope.loyaltyPointSettings.loyaltyProductPoints != null) {
                for (var i = 0; i < $scope.loyaltyPointSettings.loyaltyProductPoints.length; i++) {
                    $scope.loyaltyPointSettings.loyaltyProductPoints[i].kindOfProductPts = $scope.loyaltyPointSettings.loyaltyProductPoints[i].kindOfProductPts;
                }
            }

            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        });
    }

    $scope.saveLoyaltyPointSettings = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if ($scope.loyaltyPointSettings.loyaltyOption == "Yes") {
            $scope.loyaltyPointSettings.loyaltyOption = true;
        }
        else {
            $scope.loyaltyPointSettings.loyaltyOption = false;
        }
        salesService.saveLoyaltySettings($scope.loyaltyPointSettings).then(function (response) {
            toastr.success('Loyalty setting saved successfully');
            $scope.getLoyaltyPointSettings();
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        });
    }

    $scope.saveLoyaltyPointSettingsOption = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        if ($scope.loyaltyPointSettings.loyaltyOption == "Yes") {
            $scope.loyaltyPointSettings.loyaltyOption = true;
        }
        else {
            $scope.loyaltyPointSettings.loyaltyOption = false;
        }
        salesService.saveLoyaltySettings($scope.loyaltyPointSettings).then(function (response) {
            toastr.success('Loyalty setting saved successfully');
            $scope.getLoyaltyPointSettings();
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        });
    }

    $scope.ChangeLoyaltyOption = function () {
        if ($scope.loyaltyPointSettings.loyaltyOption == "Yes") {
            $scope.loyaltyPointSettingsPanel = true;
            $scope.loyaltySettingsOptionSave = false;

            $scope.loyaltyPointSettings.startDate = new Date();

            if ($scope.loyaltyPointSettings.startDate == "" || $scope.loyaltyPointSettings.startDate == undefined || $scope.loyaltyPointSettings.startDate == null) {
                $scope.currentdate = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');

                $scope.loyaltyPointSettings.startDate = $scope.currentdate;
            }
        }
        else {
            $scope.loyaltyPointSettingsPanel = false;
            $scope.loyaltySettingsOptionSave = true;

        }
    };

    $scope.setMinimumSale = function () {
        if ($scope.loyaltyPointSettings.isMinimumSale == true) {
            $scope.showMinimumSalaAmt = true;
            $scope.loyaltyPointSettings.minimumSalesAmt = "";
        }
        else {
            $scope.showMinimumSalaAmt = false;
            $scope.loyaltyPointSettings.minimumSalesAmt = "0";
        }
    };

    $scope.setEndDate = function () {
        if ($scope.loyaltyPointSettings.isEndDate == true) {
            $scope.showEndDate = true;
            $scope.loyaltyPointSettings.endDate = "";
        }
        else {
            $scope.showEndDate = false;
            $scope.loyaltyPointSettings.endDate = "";
        }
    };

    $scope.setMaximumRedeem = function () {
        if ($scope.loyaltyPointSettings.isMaximumRedeem == true) {
            $scope.showMaximumRedeemAmt = true;
            $scope.loyaltyPointSettings.maximumRedeemPoint = "";
        }
        else {
            $scope.showMaximumRedeemAmt = false;
            $scope.loyaltyPointSettings.maximumRedeemPoint = "0";
        }
    };

    $scope.validateLoyaltyFromDate = function () {
        //if ($scope.loyaltyPointSettings.startDate.$viewValue != null) {
        //    val = (toDate1($scope.loyaltyPointSettings.startDate.$viewValue) <= toDate1($scope.salesDiscountForm.toDate.$viewValue));
        //    $scope.loyaltyPointSettings.startDate.$setValidity("InValidFromeDate", val);
        //    $scope.isFormValid = val;
        //    $scope.validateToDate();
        //}
        $scope.errMessage1 = '';

        if ($scope.loyaltyPointSettings.endDate != "") {
            if ($scope.loyaltyPointSettings.endDate < $scope.loyaltyPointSettings.startDate) {
                $scope.errMessage1 = 'Start Date should be less than end date';
                return false;
            }
        }
        var fromdate1 = new Date(toDate1($scope.loyaltyPointSettings.startDate.$viewValue));
        $scope.minDate.setHours(0, 0, 0, 0);
        if (fromdate1 > $scope.minDate) {
            $scope.validFromDate = true;
        }
        else {
            $scope.validFromDate = false;
        }
    };

    $scope.validateLoyaltyToDate = function () {

        //var val = ((toDate1($scope.salesDiscountForm.toDate.$viewValue) >= $scope.minDate) && (toDate1($scope.salesDiscountForm.fromDate.$viewValue) <= toDate1($scope.salesDiscountForm.toDate.$viewValue)));
        //$scope.salesDiscountForm.toDate.$setValidity("InValidToDate", val);
        //var fval = (toDate1($scope.salesDiscountForm.fromDate.$viewValue) <= toDate1($scope.salesDiscountForm.toDate.$viewValue));
        //$scope.salesDiscountForm.fromDate.$setValidity("InValidFromeDate", fval);
        //$scope.isFormValid = val;
        $scope.errMessage = '';

        if ($scope.loyaltyPointSettings.startDate > $scope.loyaltyPointSettings.endDate) {
            $scope.errMessage = 'End Date should be greate than start date';
            return false;
        }
        var todate1 = new Date(toDate1($scope.loyaltyPointSettings.endDate.$viewValue));
        $scope.minDate.setHours(0, 0, 0, 0);
        if (todate1 < $scope.minDate) {
            $scope.validToDate = true;
        }
        else {
            $scope.validToDate = false;
        }
    };

    $scope.kindOfProductList = {};
    $scope.kindOfProduct = function () {
        salesService.getKindOfProductDataList().then(function (response) {
            // $scope.kindOfProductList = response.data;
            $scope.loyaltyPointSettings.loyaltyProductPoints = response.data;
            if ($scope.loyaltyPointSettings.loyaltyProductPoints != null) {
                for (var i = 0; i < $scope.loyaltyPointSettings.loyaltyProductPoints.length; i++) {
                    $scope.loyaltyPointSettings.loyaltyProductPoints[i].kindOfProductPts = 0;
                }
            }
            $scope.getLoyaltyPointSettings();
        }, function () {
            toastr.error('Error Occured', 'Error');
            $scope.getLoyaltyPointSettings();
        });
    }


    //End Loyalty Point Settings

    //Newly Added Code By Durga on 19-04-2017 (Adding a Department)
    $scope.saveisDepartment = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        //console.log($scope.isDepartmentsave);
        var boolval = false;
        if ($scope.isDepartmentsave == "1") {
            boolval = true;
        }
        salesService.saveisDepartment(boolval).then(function (response) {
            toastr.success('Saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };
    $scope.isDepartmentsave = "0";
    getIsDepartmentadded();
    function getIsDepartmentadded() {
        salesService.getIsDepartmentadded().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.isDepartmentsave = "0";
            } else {
                if (response.data.patientTypeDept != undefined) {
                    $scope.isDepartmentsave = "0";
                    if (response.data.patientTypeDept == true) {
                        $scope.isDepartmentsave = "1";
                    }
                } else {
                    $scope.isDepartmentsave = "0";
                }
            }
        }, function () {
        });
    }

    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        salesService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    }
    $scope.getEnableSelling();

    //Added by Sarubala on 29-11-17 - start
    $scope.smsSettings1 = function () {
        $.LoadingOverlay("show");
        salesService.getSmsSettingsForInstance().then(function (response) {
            $scope.smsSettingsModel = response.data;

            if ($scope.smsSettingsModel.isSalesCreateSms && $scope.smsSettingsModel.isSalesEditSms && $scope.smsSettingsModel.isSalesEstimateSms && $scope.smsSettingsModel.isSalesOrderSms &&
            $scope.smsSettingsModel.isVendorCreateSms && $scope.smsSettingsModel.isCustomerBulkSms &&
            $scope.smsSettingsModel.isOrderCreateSms && $scope.smsSettingsModel.isUserCreateSms) {
                $scope.selectAllSmsSettings = true;
            }
            $.LoadingOverlay("hide");
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    };

    $scope.changeSmsSettings = function () {
        if ($scope.selectAllSmsSettings == true) {
            $scope.smsSettingsModel.isSalesCreateSms = true;
            $scope.smsSettingsModel.isSalesEditSms = true;
            $scope.smsSettingsModel.isSalesEstimateSms = true;
            $scope.smsSettingsModel.isSalesOrderSms = true;
            $scope.smsSettingsModel.isVendorCreateSms = true;
            $scope.smsSettingsModel.isCustomerBulkSms = true;
            $scope.smsSettingsModel.isOrderCreateSms = true;
            $scope.smsSettingsModel.isUserCreateSms = true;

        }
        else {
            $scope.smsSettingsModel.isSalesCreateSms = false;
            $scope.smsSettingsModel.isSalesEditSms = false;
            $scope.smsSettingsModel.isSalesEstimateSms = false;
            $scope.smsSettingsModel.isSalesOrderSms = false;
            $scope.smsSettingsModel.isVendorCreateSms = false;
            $scope.smsSettingsModel.isCustomerBulkSms = false;
            $scope.smsSettingsModel.isOrderCreateSms = false;
            $scope.smsSettingsModel.isUserCreateSms = false;
        }
    }

    $scope.setSelectAll = function () {
        if ($scope.smsSettingsModel.isSalesCreateSms && $scope.smsSettingsModel.isSalesEditSms && $scope.smsSettingsModel.isSalesEstimateSms && $scope.smsSettingsModel.isSalesOrderSms &&
            $scope.smsSettingsModel.isVendorCreateSms && $scope.smsSettingsModel.isCustomerBulkSms &&
            $scope.smsSettingsModel.isOrderCreateSms && $scope.smsSettingsModel.isUserCreateSms) {
            $scope.selectAllSmsSettings = true;
        }
        else {
            $scope.selectAllSmsSettings = false;
        }
    }

    $scope.saveSmsSettings = function () {

        salesService.saveSmsSettingsForInstance($scope.smsSettingsModel).then(function (response) {
            toastr.success("Sms Settings saved successfully");
        }, function (error) {
            console.log(error);
            toastr.error("Error Occurred");
        })
    }

    $scope.clearSmsSettings = function () {
        $scope.selectAllSmsSettings = false;
        $scope.smsSettingsModel = angular.copy(smsSettingsModel);
    }


    //Added by Sarubala on 28-11-17
    $scope.getSmsCount = function () {
        salesService.getRemainingSmsCount().then(function (response) {
            $scope.remainingSmsCount = response.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getSmsCount();

    //Added by Sarubala on 29-11-17 - end


    $scope.btnSaveRoundOff = function () {
        salesService.saveRoundOff($scope.isEnableRoundOff).then(function (response) {
            toastr.success('Saved complete sale key successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    }
    $scope.btnSaveFreeQty = function () {
        salesService.saveFreeQty($scope.isEnableFreeQty).then(function (response) {
            toastr.success('Saved complete successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    }


    $scope.getFreeQtySettings = function () {
        $.LoadingOverlay("show");
        salesService.getFreeQtySettings().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.isEnableFreeQty = response.data;
            if ($scope.isEnableFreeQty == true) {
                $scope.isEnableFreeQty = 1;
            }
            else if ($scope.isEnableFreeQty == false) {
                $scope.isEnableFreeQty = 0;
            }
        }, function (error) {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getFreeQtySettings();

    $scope.getRoundOffSettings = function () {
        $.LoadingOverlay("show");
        salesService.getRoundOffSettings().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.isEnableRoundOff = response.data;
            if ($scope.isEnableRoundOff == true) {
                $scope.isEnableRoundOff = 1;
            }
            else if ($scope.isEnableRoundOff == false) {
                $scope.isEnableRoundOff = 0;
            }
        }, function (error) {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getRoundOffSettings();


    $scope.isEnableNewSalesScreen = false;

    $scope.getSalesScreenSetting = function () {
        $.LoadingOverlay("show)");
        salesService.getSalesScreenSettings().then(function (response) {
            $.LoadingOverlay("hide");

            var tempData = response.data;
            if (tempData == true) {
                $scope.isEnableNewSalesScreen = 1;
            } else {
                $scope.isEnableNewSalesScreen = 0;
            }
        }, function (error) { 
            $.LoadingOverlay("hide");
        });
    }

    $scope.getSalesScreenSetting();

    $scope.saveSalesScreen = function () {
        $.LoadingOverlay("show)");
        salesService.saveSalesScreenSettings($scope.isEnableNewSalesScreen).then(function (response) {

            $.LoadingOverlay("hide");
            toastr.success('Sales screen settings saved successfully');
            $scope.isProcessing = false;
            $scope.getSalesScreenSetting();

        }, function (error) {
            $.LoadingOverlay("hide");
            toastr.error("Error Occurred");
        });

    }

});
app.directive('numbersOnly', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.directive('noSpecialChar', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue === undefined)
                    return '';
                cleanInputValue = inputValue.replace("#", '');
                cleanInputValue = cleanInputValue.replace("^", '');
                cleanInputValue = cleanInputValue.replace("&", '');
                if (cleanInputValue != inputValue) {
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                }
                return cleanInputValue;
            });
        }
    };
});