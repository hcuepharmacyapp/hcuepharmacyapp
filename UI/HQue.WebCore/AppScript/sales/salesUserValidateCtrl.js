app.controller('salesUserValidateCtrl', function ($scope, hQueUserModel, salesService, close, toastr) {

    var user = hQueUserModel;
    $scope.user = user;

    $scope.close = function (result) {
        close(result, 100);
        $(".modal-backdrop").hide();
    };

    $scope.completeSale = function () {

        $.LoadingOverlay("show");
        salesService.validateExistUser($scope.user).then(function (response) {
            $scope.user = response.data;
            if (!angular.isUndefinedOrNull($scope.user.id)) {
                $scope.invalidErrorMsg = false;
                $scope.close($scope.user);
            }
            else {
                $.LoadingOverlay("hide");
                $scope.invalidErrorMsg = true;
                setFocus();
            }

        }, function (response) {
            $.LoadingOverlay("hide");
            toastr.info('Invalid Password');
            setFocus();
        });
    };

    $scope.keyEnterSale = function (nextid) {
        $scope.completeSale();
        var ele = document.getElementById(nextid);
        ele.focus();
    };

    // Added by San 
    angular.isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null || val === ""
    }

    function setFocus() {
        var ele = document.getElementById("userPwd");
        if (ele != null) {
            ele.focus();
            user.password = "";
        }
    }
    setFocus();

    $scope.keyDownEvent = function (event) {        
        if (event.keyCode == 27) {
            event.preventDefault();
            $scope.close(false);
        }
    };

});