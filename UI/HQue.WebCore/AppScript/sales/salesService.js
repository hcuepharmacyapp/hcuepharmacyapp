﻿app.factory('salesService', function ($http, $filter) {
    return {
        "list": function (searchData) {
            return $http.post('/sales/listData', searchData);
        },
        // Added by Violet
        "getlist": function (searchData) {
            return $http.post('/sales/getlistData', searchData);
        },
        "GetPatientName": function (filter) {
            return $http.get('/sales/GetPatientName?patientName=' + filter);
        },
        "GetPatientNameList": function (instanceid, filter) {
            return $http.get('/sales/GetPatientNameList?instanceid=' + instanceid + '&patientName=' + filter);
        },
        "create": function (sales, file, Invoicedate) {
            //console.log(sales);
            //var formData = new FormData();
            //formData.append('file', file);
            //formData.append('InvoiceDate', Invoicedate);
            //angular.forEach(sales, function (value, key) {
            //    if (key === "salesItem") {
            //        for (var i = 0; i < sales.salesItem.length; i++) {
            //            angular.forEach(sales.salesItem[i], function (value, key) {
            //                if (value === "" || value === null || value === undefined)
            //                    return;
            //                formData.append("salesItem[" + i + "]."+key,value );
            //            });
            //        }
            //    } else {
            //        if (value === "" || value === null || value === undefined)
            //            return;
            //        formData.append(key, value);
            //    }
            //});
            ////console.log(formData.get("salesItem[29].salesItem"));
            //console.log("after");
            //console.log(formData.sales);
            //return $http.post('/sales/index', formData, {
            //    withCredentials: true,
            //    headers: { 'Content-Type': undefined },
            //    transformRequest: angular.identity
            //});
            //return $http.post('/sales/index', formData, {
            //    withCredentials: true,
            //    headers: { 'Content-Type': undefined },
            //    transformRequest: angular.identity
            //});
            //console.log(sales.discount);
            //console.log(sales.discountValue);
            sales.invoicedate = $filter("date")(Invoicedate, "MM/dd/yyyy");
            return $http.post('/sales/index', sales);
        },
        //Added by Annadurai 07242017 - starts
        uploadFile: function (sales, file, Invoicedate) {
            var formData = new FormData();
            formData.append('file', file);
            formData.append('InvoiceDate', Invoicedate);
            formData.append('id', sales);
            return $http.post('/sales/uploadFile', formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }
           );
        },
        //Added by Annadurai 07242017 - Ends
        "IsInvoiceManualSeriesAvail": function (InvoiceSeries, billdate) {
            return $http.post('/sales/IsInvoiceManualSeriesAvail?Invocieseries=' + InvoiceSeries + '&Billdate=' + billdate);
        },
        "salesDetail": function (salesId) {
            return $http.get('/sales/SalesDetail?id=' + salesId);
        },
        "createSalesReturn": function (salesReturn) {
            return $http.post('/salesReturn/index', salesReturn);
        },
        updateSalesReturn: function (salesReturn) {
            return $http.post('/salesReturn/updateReturn', salesReturn);
        },
        "salesReturnDetail": function (salesId) {
            return $http.get('/salesReturn/SalesReturnDetail?id=' + salesId);
        },
        "returnList": function (searchData) {
            return $http.post('/salesReturn/listData', searchData);
        },
        "UpdateCustomer": function (data) {
            return $http.post('/Patient/Update', data);
        },
        "UpdateSaleDetails": function (data) {
            return $http.post('/sales/update', data);
        },
        "saveDiscountRules": function (data) {
            return $http.post('/sales/SaveDiscountRules', data);
        },
        "getDiscountDetail": function () {
            return $http.get('/sales/SalesDiscountDetail');
        },
        "editDiscountDetail": function () {
            return $http.get('/sales/editDiscountDetail');
        },
        "editDiscountDetail1": function (billAmountType) {
            return $http.get('/sales/editDiscountDetail1?type=' + billAmountType);
        },
        "purchasePrice": function (data) {
            return $http.get('/vendorpurchase/getPurchasePrice?id=' + data);
        },
        "createMissedOrder": function (data) {
            return $http.post('/sales/createMissedOrder', data);
        },
        //Newly Added Gavaskar 24-10-2016 Start
        "saveCardType": function (data) {
            return $http.get('/sales/saveCardType?CardType=' + data);
        },
        "savePatientSearchType": function (data) {
            return $http.get('/sales/savePatientSearchType?PatientSearchType=' + data);
        },
        "saveDoctorSearchType": function (data) {
            return $http.get('/sales/saveDoctorSearchType?DoctorSearchType=' + data);
        },
        "saveDoctorMandatoryType": function (data) {
            return $http.get('/sales/isDoctorMandatory?DoctorMandatoryType=' + data);
        },
        "saveSalesTypeMandatory": function (data) {
            return $http.get('/sales/saveSalesTypeMandatory?SalestypeMandatory=' + data);
        },
        "isMaximumDiscountAvail": function (data) {
            return $http.get('/sales/isMaximumDiscountAvail?MaximumDiscountAvail=' + data);
        },
        "saveAutoTempStockAdd": function (data) {
            return $http.get('/sales/saveAutoTempStockAdd?status=' + data);
        },
        isAutosavecustomer: function (data) {
            return $http.get('/sales/isAutosavecustomer?AutosaveCustomer=' + data);
        },
        IsCreditInvoiceSeries: function (data) {
            return $http.get('/sales/saveIsCreditInvoiceSeries?IsCreditInvoiceSeries=' + data);
        },
        "getIsCreditInvoiceSeries": function () {
            return $http.get('/sales/getIsCreditInvoiceSeries');
        },
        "saveDiscountType": function (data) {
            return $http.get('/sales/saveDiscountType?DiscountType=' + data);
        },
        "saveCustomseries": function (data) {
            return $http.get('/sales/saveCustomseries?Customseries=' + data);
        },
        "saveInvoiceSeries": function (data) {
            return $http.get('/sales/saveInvoiceSeries?InvoiceSeries=' + data);
        },
        "saveisDepartment": function (data) {
            return $http.get('/sales/saveisDepartment?IsDepartment=' + data);
        },
        "getIsDepartmentadded": function () {
            return $http.get('/sales/getIsDepartmentadded');
        },
        "getCardTypeDetail": function () {
            return $http.get('/sales/getCardTypeDetail');
        },
        "getIsMaxDiscountAvail": function () {
            return $http.get('/sales/getIsMaxDiscountAvail');
        },
        "getPatientSearchType": function () {
            return $http.get('/sales/getPatientSearchType');
        },
        "getDoctorSearchType": function () {
            return $http.get('/sales/getDoctorSearchType');
        },
        "getDoctorNameMandatoryType": function () {
            return $http.get('/sales/getDoctorNameMandatoryType');
        },
        // autocomplete textbox as the doctor name
        "localList": function (searchData) {
            return $http.post("/doctor/LocalDoctorList?doctorName=" + searchData);
        },
        "getSalestypeMandatory": function () {
            return $http.get('/sales/getSalestypeMandatory');
        },
        "getAutoTempstockAdd": function () {
            return $http.get('/sales/getAutoTempstockAdd');
        },
        "getautoSaveisMandatory": function () {
            return $http.get('/sales/getautoSaveisMandatory');
        },
        "getPharmacyDiscountType": function () {
            return $http.get('/sales/getPharmacyDiscountType');
        },
        "getBatchListDetail": function () {
            return $http.get('/sales/getBatchListDetail');
        },
        "saveBatchType": function (data) {
            return $http.post('/sales/saveBatchType?batchType=' + data);
        },
        //Newly Added Gavaskar 03-02-2017 Start
        "saveShortCutKeySetting": function (keyType, newWindowType) {
            var formData = new FormData();
            formData.append('keyType', keyType);
            formData.append('newWindowType', newWindowType);
            return $http.post('/sales/saveShortCutKeySetting', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });
        },
        "getShortCutKeySetting": function () {
            return $http.get('/sales/getShortCutKeySetting');
        },
        //Added by Sarubala on 20-10-2017
        getAllSalesSettings: function () {
            return $http.get('/sales/getAllSalesSettings');
        },
        //Added by Sarubala on 20-10-2017
        getLastSaleDetails: function () {
            return $http.get('/sales/getLastSaleDetails');
        },
        //Added by Sarubala on 23-10-2017
        getInstanceDetails: function () {
            return $http.get('/sales/getInstanceValues');
        },
        //Added by Sarubala on 06-11-2017
        getSalesPriceSettings: function () {
            return $http.get('/sales/getSalesPriceSetting');
        },
        //Added by Sarubala on 15-11-2017
        getSmsSettings: function () {
            return $http.get('/sales/getSmsSettings');
        },
        //Added by Sarubala on 23-10-2017
        getRemainingSmsCount: function () {
            return $http.get('/sales/getRemainingSmsCount');
        },
        //Newly Added Gavaskar 03-02-2017 End
        "getCustomSeriesSelected": function () {
            return $http.get('/sales/getCustomSeriesSelected');
        },
        //Added by Sarubala on 03-12-18 - start
        getCustomerIdSeriesSetting: function(){
            return $http.get('/sales/getCustomerIdSeriesSettings');
        },
        saveCustomerIdSeriesSetting: function(data){
            return $http.post('/sales/saveCustomerIdSeriesSettings', data);
        },
        //Added by Sarubala on 03-12-18 - end
        "getCancelBillDays": function () {
            return $http.get('/sales/getCancelBillDays');
        },
        "getMaxDiscountValue": function () {
            return $http.get('/sales/getMaxDiscountValue');
        },
        "getInvoiceSeries": function () {
            return $http.get('/sales/getInvoiceSeries');
        },
        //Added by Manivannan for retrieving the Invoice No in sales screen on 30-Jan-2017 begins here
        "getNumericInvoiceNo": function () {
            return $http.get('/sales/GetNumericInvoiceNo');
        },
        "getCustomInvoiceNo": function (customSeries) {
            return $http.get('/sales/GetCustomInvoiceNo?customSeries=' + customSeries);
        },
        //Added by Manivannan ends here        

        "saveSmsSetting": function (inventorySmsSettings) {
            return $http.post('/sales/saveSmsSetting', inventorySmsSettings);
        },
        "saveInvoiceseriesItem": function (data, seriesType) {
            return $http.get('/sales/saveInvoiceSeriesItem?Invoiceseries=' + data + '&SeriesType=' + seriesType);
        },

        "savecancelBillDays": function (data) {
            return $http.get('/sales/savecancelBillDays?CancelBillDays=' + data);
        },
        "saveMaxDiscount": function (data) {
            return $http.get('/sales/saveMaxDiscount?MaxDiscount=' + data);
        },
        "getSmsSetting": function (inventorySmsSettings) {
            return $http.get('/sales/getSmsSetting', inventorySmsSettings);
        },
        "getPrescriptionSetting": function () {
            return $http.get('/sales/getPrescriptionSetting');
        },
        "getInvoiceSeriesItems": function () {
            return $http.get('/sales/getInvoiceSeriesItems');
        },
        //Added by Bikas on 02/07/18        
        "getCustomerIDSeriesItem": function () {
            return $http.get('/sales/getCustomerIDSeriesItem');
        },
        "getInvoiceSeriesItemForGRNSettings": function () {
            return $http.get('/sales/getInvoiceSeriesItemForPurchaseSettings');
        },
        "saveSalesType": function (name) {
            return $http.get('/sales/saveSalesType?typeName=' + name);
        },
        "removeSalesType": function (id) {
            return $http.get('/sales/removeSalesType?id=' + id);
        },
        "saveStatus": function (salesType) {
            return $http.post('/sales/saveStatus', salesType);
        },
        "getSalesType": function () {
            return $http.get('/sales/getSalesType');
        },
        "updateSalesType": function (saleType) {
            return $http.post('/sales/updateSalesType', saleType);
        },
        "savePrintType": function (id) {
            return $http.get('/sales/savePrintType?printType=' + id);
        },
        "getPrintType": function () {
            return $http.get('/sales/getPrintType');
        },
        "getPrintStatus": function () {
            return $http.get('/sales/GetBillPrintStatus');
        },
        "saveBillPrintStatus": function (status, printFooterNotes) {
            //return $http.post('/sales/saveBillPrintStatus?printStatus=' + status);
            var data = {};
            data.printStatus = status;
            //console.log(JSON.stringify(data));
            if (printFooterNotes == null || printFooterNotes == "undefined")
                printFooterNotes = "";
            data.printFooterNotes = printFooterNotes;
            return $http.post('/sales/saveBillPrintStatus', data);
            //return $http.post('/sales/saveBillPrintStatus?printStatus=' + status + '&printFooterNotes=' + printFooterNotes);
        },
        ////Newly added by Manivannan on 27-02-2017
        //getPrintFooterNote: function () {
        //    return $http.get('/sales/GetPrintFooterNote');
        //},
        //savePrintFooterNote: function (data) {
        //    return $http.post('/sales/SavePrintFooterNote', data);
        //},
        ////New services for Print Footer note ends
        "saveBatchPopUpSettings": function (data) {
            return $http.post('/sales/saveBatchPopUpSettings', data);
        },
        "getBatchPopUpSettings": function () {
            return $http.get('/sales/getBatchPopUpSettings');
        },
        "getDefaultDoctor": function () {
            return $http.get('/doctor/GetDefaultDoctor');
        },
        "getOfflineStatus": function () {
            return $http.get('/sales/getOfflineStatus');
        },
        "searchSales": function (data, reOrderFactor, orderType, loadAllProducts) {
            return $http.post("/sales/searchSales?reOrderFactor=" + reOrderFactor + "&type=" + orderType + "&loadAllProducts=" + loadAllProducts, data);
        },
        "getLastSalesPrintType": function () {
            return $http.get('/sales/getLastSalesPrintType');
        },
        "saveShortName": function (cutomerName, doctorName) {
            var formData = new FormData();
            formData.append('cutomerName', cutomerName);
            formData.append('doctorName', doctorName);
            return $http.post('/sales/saveShortName', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });
        },
        "getShortName": function () {
            return $http.get('/sales/getShortName');
        },
        "saveCompleteSaleKeys": function (data) {
            return $http.get('/sales/saveCompleteSaleKeys?CompleteSaleKeys=' + data);
        },
        "getCompleteSaleKeys": function () {
            return $http.get('/sales/getCompleteSaleKeys');
        },
        "getScanBarcodeOption": function () {
            return $http.get('/sales/getScanBarcodeOption');
        },
        "saveBarcodeOption": function (scanBarcode, autoScanQty) {
            return $http.get('/sales/saveBarcodeOption?barcodeOption=' + scanBarcode + '&autoScanQty=' + autoScanQty);
        },
        "getCustomCreditInvoiceNo": function (customSeries) {
            return $http.get('/sales/GetCustomCreditInvoiceNo?customSeries=' + customSeries);
        },
        "getCustomCreditSeriesSelected": function () {
            return $http.get('/sales/getCustomCreditSeriesSelected');
        },
        "getLastSalesId": function () {
            return $http.get('/sales/getLastSalesId');
        },
        "getLastSalesSmsType": function () {
            return $http.get('/sales/getLastSalesSmsType');
        },
        "getLastSalesEmailType": function () {
            return $http.get('/sales/getLastSalesEmailType');
        },
        "getEnableSelling": function () {
            return $http.get('/vendorPurchase/getEnableSelling');
        },
        // Added by Gavaskar Loyalty Points Start
        getLoyaltySettings: function () {
            return $http.get('/sales/GetLoyaltyPointSettings');
        },
        saveLoyaltySettings: function (item) {
            return $http.post('/sales/SaveLoyaltyPointSettings', item);
        },
        getfirstLoyaltyCheck: function (patientId) {
            return $http.get('/sales/GetfirstLoyaltyCheck?patientId=' + patientId);
        },
        getKindOfProductDataList: function () {
            return $http.get('/sales/GetKindOfProductDataList');
        },
        // Added by Gavaskar Loyalty Points End
        getOnlyReturnItems: function (id) {
            return $http.post('/salesReturn/GetReturnOnlyDetail?id=' + id);
        },
        getReturnListAudit: function (id) {
            return $http.get('/salesReturn/getReturnItemAuditList?id=' + id);
        },
        getGSTINDetail: function () {
            return $http.get('/sales/getGSTINDetail');
        },
        saveGstIn: function (data) {
            return $http.post('/sales/saveGstIn', data);
        },
        //Added by Annadurai on 07032017 for showing History Popup
        getPreviousSalesDetails: function (Name, Mobile, SelectedProduct) {
            return $http.post('/sales/PreviousSalesDetailsForSelectedCustomer?Name=' + Name + '&Mobile=' + Mobile + '&SelectedProduct=' + SelectedProduct);
        },
        //Added for department a4 print by lawrence
        getSalesIdByInvoiceNo: function (SeriesType, SeriesTypeValue, InvNo, isSalesReturn) {
            return $http.get("/sales/GetSalesIdByInvoiceNo?SeriesType=" + SeriesType + "&SeriesTypeValue=" + SeriesTypeValue + "&InvNo=" + InvNo + "&returnStatus=" + isSalesReturn);
        },

        getSalesReturnIdByRetrunNo: function (SeriesType, SeriesTypeValue, InvNo) {
            return $http.get("/salesReturn/GetSalesReturnIdByRetrunNo?SeriesType=" + SeriesType + "&SeriesTypeValue=" + SeriesTypeValue + "&InvNo=" + InvNo);
        },
        //Added by Sarubala on 05-10-17
        getPaymentDomainValues: function () {
            return $http.get("/sales/GetDomainValues");
        },

        // Added by Gavaskar 03-10-2017 Start
        "createSalesOrder": function (data) {
            return $http.post('/sales/CreateSalesOrder', data);
        },
        "createSalesNewTemplate": function (data) {
            return $http.post('/sales/CreateSalesNewTemplate', data);
        },
        "getTemplateNameList": function () {
            return $http.get('/sales/GetTemplateNameList');
        },
        "getSelectedTemplateNameList": function (templateId) {
            return $http.get('/sales/getSelectedTemplateNameList?templateId=' + templateId);
        },
        "updateTemplate": function (data) {
            return $http.post('/sales/updateTemplate', data);
        },
        "salesOrderList": function (data) {
            return $http.post('/sales/SalesOrderList', data);
        },
        "salesTemplateList": function (data) {
            return $http.post('/sales/SalesTemplateList', data);
        },
        "updateSalesTemplateStatus": function (data) {
            return $http.post('/sales/UpdateSalesTemplateStatus', data);
        },
        "getSalesOrderById": function (salesOrderEstimateId) {
            return $http.get('/sales/GetSalesOrderById?salesOrderEstimateId=' + salesOrderEstimateId);
        },
        "updateSalesOrderEstimate": function (data) {
            return $http.post('/sales/updateSalesOrderEstimateItem', data);
        },
        "getSelectedProductStockList": function (templateId, types) {
            return $http.get('/sales/GetSelectedProductStockList?templateId=' + templateId + '&types=' + types);
        },
        "resendSalesOrderEstimateSms": function (salesOrderEstimate) { return $http.post('/sales/ResendSalesOrderEstimateSms', salesOrderEstimate); },
        "resendSalesOrderEstimateEmail": function (salesOrderEstimate) { return $http.post('/sales/ResendSalesOrderEstimateEmail', salesOrderEstimate); },
        "getSelectedCustomerOrderEstimateList": function (patientId) {
            return $http.get('/sales/GetSelectedCustomerOrderEstimateList?patientId=' + patientId);
        },
        // Added by Gavaskar 03-10-2017 End
        saveRoundOff: function (data) {
            return $http.get('/sales/saveRoundOff?isEnableRoundOff=' + data);
        },
        getRoundOffSettings: function () {
            return $http.get('/sales/getRoundOffSettings');
        },
        //Added by Sarubala on 29-11-17
        getSmsSettingsForInstance: function () {
            return $http.post('/sales/getSmsSettingsForInstance');
        },
        saveSmsSettingsForInstance: function (data) {
            return $http.post('/sales/saveSmsSettingsForInstance', data);
        },
        getSalesScreenSettings: function(){
            return $http.get('/sales/getSalesScreenSettings');
        },
        saveSalesScreenSettings: function(data){
            return $http.post('/sales/saveSalesScreenSettings?&isEnableNewSalesScreen=' + data);
        },
        getIsSalesTemplateEnabled: function () {
            return $http.get('/sales/GetIsSalesTemplateEnabled');
        },
        validateExistUser: function (user) {
            return $http.post('/UserManagement/validateExistUser', user);
        },
        getOnlineEnableStatus: function () {
            return $http.get('/sales/getOnlineEnabledStatus');
        },
        saveFreeQty: function (data) {
            return $http.get('/sales/saveFreeQty?isEnableFreeQty=' + data);
        },
        
        getFreeQtySettings: function () {
            return $http.get('/sales/getFreeQtySettings');
        },
        getSaleLoyaltyPointSettings: function (loyaltyId) {
            return $http.get('/sales/getSaleLoyaltyPointSettings?loyaltyId=' + loyaltyId);
        },
        createOtherSales: function (data, Invoicedate) {
            data.invoicedate = $filter("date")(Invoicedate, "MM/dd/yyyy");
            return $http.post('/sales/otherSales', data);
        },
        sendOtp: function (mobile) {
            return $http.post('/sales/SendOTPToAPI?&mobileNo='+ mobile);
        },
        validateOtp: function (otp, mobile) {
            return $http.post('/sales/ValidateOTP?&otp=' + otp + '&mobileNo=' + mobile);
        },
    }
});