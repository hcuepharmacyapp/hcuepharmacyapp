﻿app.controller('salesCreateCtrl', function ($scope, $rootScope, $location, $window, toastr, salesEditHelper,
    customerHelper, printingHelper, shortcutHelper, salesService, productStockService, doctorService, cacheService, productModel,
    productStockModel, ModalService, $filter, customerReceiptService, patientService, missedOrderModel,
    tempVendorPurchaseItemModel, vendorPurchaseService, salesModel, pagerServcie, leadsModel, salesReturnModel, vendorOrderService, salesTemplateModel, salesOrderEstimateModel, smsSettingsModel, loyaltyPointSettingsModel) {
    shortcutHelper.setScope($scope);
    $scope.minDate = new Date();
    var d = new Date();
    var ele = "";
    $scope.Enablesalesbtn = false;
    var valqty = false;
    var valdisct = false;
    var selprice = false;
    console.log('in', $scope)
    $scope.valqty = false;
    $scope.valdisct = false;
    $scope.selprice = false;
    $scope.edititem = false;
    $scope.errLast4digit = false;
    $scope.isEnableRoundOff = false;
    $scope.isEnableSalesUser = false;
    $scope.isEnableCustomerID = false;
    $scope.redeemError = false;
    $scope.loyaltyError = false;
    $scope.netAmtError = false;
    $scope.returnRedeemError = false;
    $scope.oldRedeemPoint = null;
    $scope.returnRedeemPt = 0;
    $scope.avilableRedeemPt = 0;
    $scope.loyaltyPointSettings = angular.copy(loyaltyPointSettingsModel);
    $scope.salesEditId = document.getElementById('salesEditId').value;
    $scope.open = function () {
        $scope.popup.opened = true;
    };
    $scope.popup = {
        opened: false
    };
    $scope.Iscredit = false;
    $scope.isComposite = false;
    //$scope.IsCheckNoEntered = false; //Added by Annadurai
    var salesPrint = salesModel;
    var leads = leadsModel;
    $scope.search = salesPrint;

    $scope.smsSettings = angular.copy(smsSettingsModel); //Added by Sarubala on 15-11-17

    $scope.showSms = true;
    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    $scope.previousdetails = []; //Added by Annadurai on 07032017
    //Check offline status
    $scope.IsOffline = false;
    $scope.isOnlineEnabled = false;
    $scope.taxValuesList = [];
    $scope.gstMode = false;
    $scope.getGstvalue = null;
    $scope.discountValueInPerc = 0;
    //Modified by Sarubala on 23-10-17
    //getOfflineStatus = function () {
    //    salesService.getOfflineStatus().then(function (response) {
    //        if (response.data) {
    //            $scope.IsOffline = true;
    //        }
    //    });
    //};
    //getOfflineStatus();

    //Added by Sarubala on 23-10-17
    getInstanceValues = function () {
        salesService.getInstanceDetails().then(function (response) {
            console.log(JSON.stringify(response.data))
            if (response.data.gsTinNo != undefined) {
                $scope.gstIn = response.data.gsTinNo;
            }
            else {
                $scope.gstIn = null;
            }
            if (response.data.gstselect) {
                $scope.isComposite = response.data.gstselect;
            }
            if (response.data.offlineStatus) {
                $scope.IsOffline = true;
            }

        }, function (error) {
            console.log(error);
        });
    };
    getInstanceValues();


    //Added by Sarubala on 18-05-18
    getOnlineEnabledStatus = function () {
        salesService.getOnlineEnableStatus().then(function (response) {
            if (response.data) {
                $scope.isOnlineEnabled = true;
            }
        }, function (error) {
            console.log(error);
        });
    };

    getOnlineEnabledStatus();

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.remiderOpen = function () {
        $scope.reminderPopup.opened = true;
    };
    $scope.reminderPopup = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.customerHelper = customerHelper;
    console.log(JSON.stringify($scope.customerHelper));
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.selectedBillDate = new Date();
    $scope.scanBarcodeVal = "";
    $scope.eancodeAlert = "";
    $scope.scanBarcodeOption = {
        "scanBarcode": "0",
        "autoScanQty": 0
    };
    $scope.batchList = [];
    $scope.selectedProduct = null;
    $scope.selectedBatch = { reminderFrequency: "0" };
    $scope.selectedBatch.transactionType = "sales";
    $scope.selectedBatch.salesReturn = false;
    $scope.selectedBatch.canEnableTaxEdit = false;
    $scope.salesAmount = 0;
    $scope.returnAmount = 0;
    $scope.Math = window.Math;
    $scope.selectedDoctor = null;
    $scope.totalBalanceAmount = 0;
    $scope.doctor = {};
    $scope.customerHelper.data.isCustomerDiscount = 0;
    $scope.customerHelper.data.isReset = 0;
    $scope.enableBillPrint = true;
    $scope.returnItemCount = 0;
    $scope.returnDiscountInValid = false;
    $scope.returnItems = null;
    $scope.salesReturnCount = 0;
    $scope.salesCount = 0;
    $scope.printStatus = "Disable";
    $scope.zeroBatch = false;
    $scope.zeroQty = false;

    //Newly added by Mani on 28-Jan-17
    $scope.dispInvoiceNo = "";
    $scope.sales = getSalesObject();
    $scope.sales.doctorid = 0;
    $scope.sales.discountType = "1";
    $scope.chkPrintValue = false;
    $scope.sales.sendSms = false;
    $scope.sales.sendEmail = false;
    //Modified by Sarubala on 20-10-17
    $scope.getLastSalesDetail = function () {
        salesService.getLastSaleDetails().then(function (response) {

            if (response.data != null && response.data != undefined && response.data != "") {
                $scope.chkPrintValue = response.data.billPrint;
                $scope.sales.sendSms = $scope.showSms == true ? response.data.sendSms : false; //Modified by Sarubala on 16-11-17
                $scope.sales.sendEmail = response.data.sendEmail;
                $scope.sales.discountType = response.data.discountType.toString();
            }

        }, function (error) {
            console.log(error);
        });
    };

    $scope.getLastSalesDetail();
    $scope.sales.billPrint = $scope.chkPrintValue;

    //Added by Annadurai on 07122017
    //var plusPharActId = "c503ca6e-f418-4807-a847-b6886378cf0b";
    var plusPharActId = "852eaefa-f670-4a6a-a0e7-db3fa252d576";


    $scope.getLastSalesPrintType = function () {
        salesService.getLastSalesPrintType().then(function (response) {
            $scope.chkPrintValue = response.data;
        }, function () {
        });
    };
    //$scope.getLastSalesPrintType(); //Modified by Sarubala on 20-10-17


    //$scope.getLastSalesSmsType = function () {
    //    salesService.getLastSalesSmsType().then(function (response) {
    //        $scope.sales.sendSms = response.data;
    //    }, function () {
    //    });
    //};
    //$scope.getLastSalesSmsType();

    //$scope.getLastSalesEmailType = function () {
    //    salesService.getLastSalesEmailType().then(function (response) {
    //        $scope.sales.sendEmail = response.data;
    //    }, function () {
    //    });
    //};
    //$scope.getLastSalesEmailType();
    //$scope.getLastSalesDiscountType = function () {
    //    salesService.getLastSalesId().then(function (response) {
    //        if (response.data != null && response.data != undefined && response.data != "")
    //            $scope.sales.discountType = response.data.discountType.toString();
    //    }, function () { });
    //};
    //$scope.getLastSalesDiscountType();
    $scope.getIsGstEnabled = function () {
        var gstEnabled = document.getElementById("isGstEnabled").value;
        $scope.isGstEnabled = gstEnabled == "True" ? true : false;
    };
    $rootScope.$on("doSelectPatient", function (data, pType) {
        $scope.enableSaleBtn();
        // Comment Gavaskar 13-04-2017 Customer Credit Series Start
        if (pType.customerPaymentType == "Credit") {
            $scope.sales.cashType = "Credit";
            //$scope.focusCreditTextBox = true;
            $scope.iscashtypevalid = true;
        } else {
            $scope.sales.cashType = "Full";
            $scope.iscashtypevalid = false;
        }
        // Comment Gavaskar 13-04-2017 Customer Credit Series End
        customerHelper.doSelectPatient();
        $scope.enableSaleBtn();
    });
    $scope.doctorSelected = function () {
        $scope.selectedDoctor;
    };
    $scope.$on("doctorSelected", function (event, args) {
        $scope.sales.doctorMobile = args.value.mobile;
    });
    $scope.$on("update_getValue", function (event, value) {
        $scope.doctor.name = value;
    });
    //Newly added by Saru on 31-03-17 begins
    $scope.changeTransactionType = function () {
        if ($scope.selectedBatch.salesReturn) {
            $scope.selectedBatch.transactionType = "return";
            $scope.returnItemCount++;
        } else {
            $scope.selectedBatch.transactionType = "sales";
            $scope.returnItemCount--;
        }
        if ($scope.returnItemCount > 0) {
            $scope.returnDiscountInValid = true;
        } else {
            $scope.returnDiscountInValid = false;
        }
        document.getElementById("drugName").focus();
    };
    //Newly added by Saru on 31-03-17 ends
    $scope.leadsProductArray = [];
    // Added by Gavaskar 10-10-2017 Start Sales Order , Estimate ,Template Convert to Sales Start
    $scope.salesTemplateProductArray = [];
    var salesTemplate = salesTemplateModel;
    $scope.salesTemplate = salesTemplate;
    $scope.salesTemplate.salesTemplateItem = [];
    $scope.salesEstimateProductArray = [];
    var salesOrderEstimate = salesOrderEstimateModel;
    $scope.salesOrderEstimate = salesOrderEstimate;
    $scope.salesOrderEstimate.salesOrderEstimateItem = [];
    // Added by Gavaskar 10-10-2017 Start Sales Order , Estimate ,Template Convert to Sales End

    $scope.getProducts = function (val) {
        if ($scope.salesManUserPopup == true) {
            return;
        }
        $scope.eancodeAlert = "";
        if ($scope.scanBarcodeOption.scanBarcode === "1") {
            if (val.length > 3 && !isNaN(val))
                return;
        }
        // if (!$scope.selectedBatch.salesReturn) {
        if ($scope.isProductGenericSearch == true) {
            return productStockService.getSalesProduct(val).then(function (response) {
                return response.data.map(function (item) {
                    return item;
                });
            });
        } else {
            return productStockService.genericFilterListData(val).then(function (response) {
                //console.log(response.data);
                return response.data.map(function (item) {
                    return item;
                });
            });
        }
        //}
        //else {
        //    return productStockService.getAllStockData(val).then(function (response) {
        //        return response.data.map(function (item) {
        //            return item;
        //        });
        //    });
        //}
    };

    //Added by Sarubala on 06-11-2017
    $scope.salesPriceSettings = 0;
    $scope.getSalesPriceSetting = function () {
        salesService.getSalesPriceSettings().then(function (response) {
            $scope.salesPriceSettings = response.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getSalesPriceSetting();

    $scope.CheckName = function () {
        var drugName = document.getElementById("drugName");
        if (drugName.value == "") {
            // var sreturn = $scope.selectedBatch.salesReturn;
            // var ttype = $scope.selectedBatch.transactionType;
            $scope.selectedBatch = { reminderFrequency: "0", purchasePrice: 0 };
            $scope.selectedBatch.transactionType = "sales";
            $scope.selectedBatch.salesReturn = false;
            $scope.selectedBatch.canEnableTaxEdit = false;
            //   $scope.selectedBatch.salesReturn = sreturn;
            //   $scope.selectedBatch.transactionType = ttype;
            //Clearing the ZeroQuantity error message
            //$scope.zeroQty = false;
        }
    };
    $scope.CheckGenericName = function () {
        var genericName = document.getElementById("genericName");
        if (genericName.value == "") {
            $scope.selectedBatch = { reminderFrequency: "0", purchasePrice: 0 };
            $scope.selectedBatch.transactionType = "sales";
            $scope.selectedBatch.salesReturn = false;
            $scope.selectedBatch.canEnableTaxEdit = false;
            //Clearing the ZeroQuantity error message
            //$scope.zeroQty = false;
        }
    };
    $scope.Namecookie = "";
    $scope.getPatient = function (val) {
        // $scope.customerHelper.data.patientSearchData.name = val;
        $scope.Namecookie = val;
        return patientService.GetActivePatientName(val, $scope.isDepartmentsave, 1).then(function (response) {
            //return patientService.GetPatientName(val, $scope.isDepartmentsave).then(function (response) {
            var origArr = response.data;
            var newArr = [], origLen = origArr.length, found, x, y;
            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name)) {
                        if (origArr[x].mobile != undefined || newArr[y].mobile != undefined) {
                            if (origArr[x].mobile === newArr[y].mobile) {
                                found = true;
                                break;
                            }
                        }
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }
            return newArr.map(function (item) {
                return item;
            });
        });
    };
    $scope.ShortDoctorsList = function (val) {
        if ($scope.DoctorNameSearch == "" || $scope.DoctorNameSearch == undefined) {
            $scope.DoctorNameSearch = 2;
        }
        if ($scope.DoctorNameSearch == 1) {
            return doctorService.LocalShortlist(val).then(function (response) {
                //return response.data;
                var origArr = response.data;
                var newArr = [], origLen = origArr.length, found, x, y;
                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].shortName) === $filter('uppercase')(newArr[y].shortName) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }
                return newArr.map(function (item) {
                    return item;
                });
            }, function (error) {
                console.log(error);
            });
        }
    };
    $scope.changeShortDoctorname = function (name) {
        $scope.shortDoctorname = name;
        if ($scope.sales.salesItem.length > 0) {
            if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
                if ($scope.shortDoctorname == "") {
                    $scope.isdoctornameMandatory = true;
                } else {
                    $scope.isdoctornameMandatory = false;
                }
            } //Modified by Sarubala on 24-11-17
        }
        if ($scope.shortDoctorname == "") {
            $scope.sales.doctorMobile = "";
        }
    };
    $scope.ShortNamecookie = "";
    $scope.getShortPatient = function (val) {
        // $scope.customerHelper.data.patientSearchData.name = val;
        $scope.Namecookie = "";
        $scope.ShortNamecookie = val;
        //if ($scope.PatientNameSearch == "" || $scope.PatientNameSearch == undefined) {
        //    $scope.PatientNameSearch = 2;
        //}
        //if ($scope.PatientNameSearch == 1) {
        return patientService.GetShortPatientName(val)
            .then(function (response) {
                var origArr = response.data;
                var newArr = [], origLen = origArr.length, found, x, y;
                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].shortName) === $filter('uppercase')(newArr[y].shortName) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }
                return newArr.map(function (item) {
                    return item;
                });
            });
    };
    $scope.blurShortCustname = function () {
        $scope.customerHelper.data.patientSearchData.shortName = $scope.ShortNamecookie;
    };
    $scope.changeShortCustomerName = function () {
        var custname = document.getElementById("shortCustomerName").value.trim();
        if ($scope.sales.salesItem.length > 0) {
            if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
                if (custname == "") {
                    $scope.iscustomerNameMandatory = true;
                } else {
                    $scope.iscustomerNameMandatory = false;
                    $scope.flagCustomername = false;
                }
            }
        }
    };
    var patientSelected = 0;
    $scope.onPatientShortSelect = function (obj, event) {
        var TABKEY = 9;
        if (event.which == TABKEY) {
            event.preventDefault();
            $scope.customerHelper.data.patientSearchData.shortName = $scope.ShortNamecookie;
            $scope.ShortNamecookie = "";
            $scope.Namecookie = "";
            var qty = document.getElementById("searchPatientMobile");
            qty.focus();
            return false;
        } else {
            patientSelected = 1;
            $scope.customerHelper.data.patientSearchData = obj;
            $scope.customerHelper.data.patientSearchData.patientSearchType = $scope.PatientNameSearch;
            patientService.list($scope.customerHelper.data.patientSearchData, $scope.isDepartmentsave).then(function (response) {
                $scope.customerHelper.data.customerList = response.data;
                $scope.Namecookie = "";
                if ($scope.customerHelper.data.customerList.length > 0) {
                    for (var p = 0; p < $scope.customerHelper.data.customerList.length; p++) {
                        if ($scope.customerHelper.data.patientSearchData.shortName == $scope.customerHelper.data.customerList[p].shortName) {
                            $scope.customerHelper.data.customerList[p].nameIndex = $scope.customerHelper.data.customerList[p].shortName.substring(0, 1);
                            $scope.customerHelper.data.selectedCustomer = $scope.customerHelper.data.customerList[p];
                        }
                    }
                    $scope.customerHelper.data.isCustomerSelected = true;
                    ////if ($scope.DoctorNameMandatory == 1) {
                    ////    var docname = document.getElementById("doctorName").value;
                    ////    if (docname == "") {
                    ////        var docname = document.getElementById("doctorName");
                    ////        docname.focus();
                    ////    } else {
                    ////        var drugName = document.getElementById("drugName");
                    ////        drugName.focus();
                    ////    }
                    ////}
                    ////else {
                    ////    var drugName = document.getElementById("drugName");
                    ////    drugName.focus();
                    ////}
                    $scope.iscustomerNameMandatory = false;
                    $scope.iscustomerMobileMandatory = false;
                    $scope.chequeCustomerRequired = false;
                    if ($scope.iscashtypevalid == true) {
                        $scope.iscashtypevalid = false;
                    }
                    if ($scope.isdeliverytypevalid == true) {
                        $scope.isdeliverytypevalid = false;
                    }
                    customerReceiptService.getCustomerBalance($scope.customerHelper.data.selectedCustomer.mobile, $scope.customerHelper.data.selectedCustomer.name)
                        .then(function (resp) {
                            if (resp.data.length == 0)
                                return;
                            if (resp.data.credit != undefined && resp.data.debit != undefined)
                                $scope.customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
                        });
                    if ($scope.customerHelper.data.selectedCustomer != null && $scope.customerHelper.data.selectedCustomer.name != undefined) {
                        patientService.getCustomerDiscount($scope.customerHelper.data.selectedCustomer).then(function (response) {
                            $scope.customerHelper.data.selectedCustomer.discount = response.data;
                            if ($scope.maxDiscountFixed == "Yes") {
                                if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue) {
                                    toastr.info("Selected Customer Having more discount than the Fixed Max discount ,So we changed discount percentage to Max discount");
                                    //  $scope.customerHelper.data.selectedCustomer.discount = $scope.maxDisountValue;
                                }
                            }
                        });
                    }
                }
                $.LoadingOverlay("hide");
            }, function () {
                $.LoadingOverlay("hide");
            });
        }
    };
    //$scope.getCustomerBalance = function () {
    //    customerReceiptService.getCustomerBalance(customerHelper.data.selectedCustomer.mobile, customerHelper.data.selectedCustomer.name).then(function (resp) {
    //        if (resp.data.length == 0) {
    //            customerHelper.data.customerBalance = 0;
    //            return;
    //        }
    //        if (resp.data.credit != undefined)
    //            customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
    //    });
    //};
    //var patientSelected = 0;
    $scope.onPatientSelect = function (obj, event) {
        var TABKEY = 9;
        if (event.which == TABKEY) {
            event.preventDefault();
            var custname = document.getElementById("customerName").value.trim();
            if (custname != "") {
                $scope.customerHelper.data.patientSearchData.name = custname;
                $scope.Namecookie = custname;
            } else {
                $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
                // $scope.Namecookie = "";
            }
            var qty = document.getElementById("searchPatientMobile");
            qty.focus();
            return false;
        } else {
            patientSelected = 1;
            $scope.customerHelper.data.patientSearchData = obj;
            $scope.customerHelper.data.patientSearchData.patientSearchType = $scope.PatientNameSearch;
            $scope.Namecookie = $scope.customerHelper.data.patientSearchData.name;
            //$scope.sales.paymentType = "Cash"; // Added Gavaskar 13-04-2017
            // no more global search
            patientService.localList($scope.customerHelper.data.patientSearchData, $scope.isDepartmentsave)
                .then(function (response) {
                    if (response.data.length > 0) {
                        $scope.customerHelper.data.customerList = response.data;
                        for (var p = 0; p < $scope.customerHelper.data.customerList.length; p++) {
                            $scope.customerHelper.data.customerList[p].nameIndex = $scope.customerHelper.data.customerList[p].name.substring(0, 1);
                        }
                        $scope.Namecookie = "";
                        // Comment Gavaskar 13-04-2017 Customer Credit Series Start
                        if ($scope.customerHelper.data.patientSearchData.customerPaymentType == "Credit") {
                            $scope.sales.cashType = "Credit";
                            //$scope.focusCreditTextBox = true;
                            $scope.iscashtypevalid = true;
                            //Added by Settu on 04/07/17 for auto assign credit amount
                            $scope.changeCashType();
                            $scope.focusCreditTextBox = false;
                        } else {
                            if ($scope.sales.cashType != "MultiplePayment") { // Added by Sarubala on 20-09-17
                                $scope.sales.cashType = "Full";
                                $scope.iscashtypevalid = false;
                                $scope.sales.credit = "";
                                //Added by Settu on 04/07/17 for auto assign credit amount
                                $scope.changeCashType();
                            }
                        }
                        if ($scope.customerHelper.data.patientSearchData.patientType == 2) {
                            document.getElementById('sellingPrice').disabled = true;
                            document.getElementById('discount').disabled = true;
                            document.getElementById('txtDiscount').disabled = true;
                            // $scope.removeDepartmentSales();
                        }
                        // Comment Gavaskar 13-04-2017 Customer Credit Series End
                        if ($scope.customerHelper.data.customerList.length > 0) {
                            for (var p = 0; p < $scope.customerHelper.data.customerList.length; p++) {
                                //Add the patient table unique id check condition as well -- Annadurai
                                //if (($scope.customerHelper.data.patientSearchData.name == $scope.customerHelper.data.customerList[p].name)                                
                                if (($scope.customerHelper.data.patientSearchData.name == $scope.customerHelper.data.customerList[p].name) && ($scope.customerHelper.data.patientSearchData.id == $scope.customerHelper.data.customerList[p].id)) {
                                    //Ends here

                                    console.log(JSON.stringify($scope.customerHelper.data.customerList[p]));
                                    $scope.customerHelper.data.selectedCustomer = $scope.customerHelper.data.customerList[p];
                                }
                            }
                            $scope.customerHelper.data.isCustomerSelected = true;
                            if ($scope.Editsale != true) {
                                salesService.getSelectedCustomerOrderEstimateList($scope.customerHelper.data.selectedCustomer.id).then(function (response) {

                                    $scope.SelectedCustomerOrderEstimateList = response.data;
                                    if ($scope.SelectedCustomerOrderEstimateList.length > 0 || $scope.SelectedCustomerOrderEstimateList.length > 0) {
                                        $scope.showSalesOrderEstimateItems();
                                    }

                                }, function () {
                                });
                            }


                            // $scope.showSalesOrderEstimateItems($scope.customerHelper.data.selectedCustomer.id); // Added by Gavaskar 30-10-2017 showSalesOrderEstimateItems

                            if ($scope.maxDiscountFixed == "Yes") {
                                if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue) {
                                    toastr.info("Selected Customer Having more discount than the Fixed Max discount ,So we changed discount percentage to Max discount");
                                    $scope.customerHelper.data.selectedCustomer.discount = $scope.maxDisountValue;
                                }
                            }

                            var docname = document.getElementById("doctorName");
                            docname.focus();
                            $scope.iscustomerNameMandatory = false;
                            $scope.iscustomerMobileMandatory = false;
                            $scope.chequeCustomerRequired = false;
                            if ($scope.iscashtypevalid == true) {
                                $scope.iscashtypevalid = false;
                            }
                            if ($scope.isdeliverytypevalid == true) {
                                $scope.isdeliverytypevalid = false;
                            }
                            customerReceiptService.getCustomerBalance($scope.customerHelper.data.selectedCustomer.mobile, $scope.customerHelper.data.selectedCustomer.name)
                                .then(function (resp) {
                                    if (resp.data.length == 0)
                                        return;
                                    if (resp.data.credit != undefined && resp.data.debit != undefined)
                                        $scope.customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
                                });
                        }
                    } else {
                        $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
                        $scope.iscustomerNameMandatory = false;
                        $scope.iscustomerMobileMandatory = false;
                        var qty = document.getElementById("searchPatientMobile");
                        qty.focus();
                    }

                    // Added by San 10-10-2017
                    if (!angular.isUndefinedOrNull($scope.discountRules)) {
                        if ($scope.discountRules.billAmountType == "productCustomerWiseDiscount")
                            editProductCustomerwisDiscount();
                    }


                    $.LoadingOverlay("hide");
                }, function () {
                    $.LoadingOverlay("hide");
                });
        }
    };


    //var drugName = document.getElementById("drugName");
    //drugName.focus;
    function LoadPreviousSalesItemHistory() {
        $scope.previousdetails = [];
        //Added for HistoryPopup on 07032017
        if ($scope.scanBarcodeOption.showSalesItemHistory && $scope.customerHelper.data.selectedCustomer.id != "" && $scope.customerHelper.data.selectedCustomer.id != null && $scope.customerHelper.data.selectedCustomer.id != undefined) {
            salesService.getPreviousSalesDetails($scope.customerHelper.data.selectedCustomer.id, $scope.customerHelper.data.selectedCustomer.mobile,
            $scope.selectedProduct.productId).then(function (response) {
                $scope.previousdetails = response.data;
            }, function () {
            });
        }
    }
    $scope.onProductSelect = function () {
        // if (!$scope.selectedBatch.salesReturn) {
        $scope.previousdetails = []; //added by Annadurai
        $scope.selectedBatch = { reminderFrequency: "0" };
        $scope.selectedBatch.transactionType = "sales";
        $scope.selectedBatch.salesReturn = false;
        $scope.selectedBatch.canEnableTaxEdit = false;
        if ($scope.selectedProduct.stock > 0) {
            $scope.zeroQty = false;
            productStockService.productBatch($scope.selectedProduct.product.id).then(function (response) {
                var tempBatchList1 = [];
                $scope.batchList = response.data;
                var x = 0;
                for (var i = 0; i < $scope.batchList.length; i++) {
                    $scope.batchList[i].productStockId = $scope.batchList[i].id;
                    $scope.batchList[i].id = null;
                    var availableStock = $scope.getAvailableStock($scope.batchList[i], null);
                    if (availableStock == 0)
                        continue;
                    $scope.batchList[i].availableQty = availableStock;
                }
                for (var i = 0; i < $scope.batchList.length; i++) {
                    if ($scope.batchList[i].tempStock != null && $scope.batchList[i].tempStock != undefined && $scope.batchList[i].tempStock.id != null) {
                        tempBatchList1[x] = $scope.batchList[i];
                        x++;
                    }
                }
                if ($scope.Editsale != true) {
                    $scope.Editsale = false;
                }
                $scope.batchList = {};
                if (tempBatchList1.length > 0) {
                    $scope.enableTempStockPopup = true;
                    var m = ModalService.showModal({
                        "controller": "tempStockAlertCtrl",
                        "templateUrl": 'tempStockAlert',
                        "inputs": {
                            "tempBatchList": tempBatchList1,
                            "productName": $scope.selectedProduct.name
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            $scope.message = "You said " + result;
                        });
                    });
                    return false;
                } else if (tempBatchList1.length <= 0) {
                    // if (!$scope.selectedBatch.salesReturn) {
                    productStockService.productBatch($scope.selectedProduct.product.id).then(function (responses) {
                        var batchListCheck = responses.data;
                        var totalQuantityCheck = 0;
                        var tempBatchCheck = [];
                        var editBatchCheck = null;
                        var availableStockCheck = null;
                        for (var i = 0; i < batchListCheck.length; i++) {
                            batchListCheck[i].productStockId = batchListCheck[i].id;
                            batchListCheck[i].id = null;
                            availableStockCheck = $scope.getAvailableStock(batchListCheck[i], editBatchCheck);
                            if (editBatchCheck != null && editBatchCheck.productStockId == batchListCheck[i].productStockId)
                                editBatchCheck.availableQty = availableStockCheck;
                            totalQuantityCheck += availableStockCheck;
                            if (availableStockCheck == 0)
                                continue;
                            batchListCheck[i].availableQty = availableStockCheck;
                            tempBatchCheck.push(batchListCheck[i]);
                        }
                        if (editBatchCheck != null && tempBatchCheck.length == 0) {
                            availableStockCheck = $scope.getAvailableStock(editBatchCheck, editBatchCheck);
                            editBatchCheck.availableQty = availableStockCheck;
                            totalQuantityCheck = availableStockCheck;
                            tempBatchCheck.push(editBatchCheck);
                        }
                        batchListCheck = tempBatchCheck;
                        if (batchListCheck.length > 0) {
                            $scope.zeroBatch = false;
                            salesService.getBatchListDetail().then(function (response1) {
                                $scope.batchListType = response1.data;
                                $scope.previousdetails = [];//Added for HistoryPopup on 07032017
                                if ($scope.batchListType == "Batch") {
                                    $scope.ProductDetails();
                                } else {
                                    LoadPreviousSalesItemHistory();
                                    loadBatch(null);
                                }
                            }, function () {
                            });
                        } else {
                            $scope.zeroBatch = true;
                        }
                    }, function () { });

                }
            }, function (error) {
                console.log(error);
            });
        } else {
            $.alert({
                title: "Stock not available!",
                content: 'Stock not available for the selected item (' + $scope.selectedProduct.product.name + ')',
                animation: 'center',
                closeAnimation: 'center',
                backgroundDismiss: true,
                buttons: {
                    okay: {
                        text: 'Ok [Esc]',
                        btnClass: 'primary-button custom-transform',
                        action: function () {
                            // do nothing
                        }
                    }
                }
            });
            //$scope.zeroQty = true;
            $scope.selectedProduct = "";
        }
        //}
        //else {
        //    productStockService.getBatchForReturn($scope.selectedProduct.product.id).then(function (response) {
        //        var tempBatchList1 = [];
        //        $scope.batchList = response.data;
        //        var x = 0;
        //        for (var i = 0; i < $scope.batchList.length; i++) {
        //            $scope.batchList[i].productStockId = $scope.batchList[i].id;
        //            $scope.batchList[i].id = null;
        //            var availableStock = $scope.getAvailableStock($scope.batchList[i], null);
        //            //if (availableStock == 0)
        //            //    continue;
        //            $scope.batchList[i].availableQty = availableStock;
        //        }
        //        for (var i = 0; i < $scope.batchList.length; i++) {
        //            if ($scope.batchList[i].tempStock != null && $scope.batchList[i].tempStock != undefined && $scope.batchList[i].tempStock.id != null) {
        //                tempBatchList1[x] = $scope.batchList[i];
        //                x++;
        //            }
        //        }
        //        if ($scope.Editsale != true) {
        //            $scope.Editsale = false;
        //        }
        //        $scope.batchList = {};
        //        if (tempBatchList1.length > 0) {
        //            $scope.enableTempStockPopup = true;
        //            var m = ModalService.showModal({
        //                "controller": "tempStockAlertCtrl",
        //                "templateUrl": 'tempStockAlert',
        //                "inputs": {
        //                    "tempBatchList": tempBatchList1,
        //                    "productName": $scope.selectedProduct.name
        //                }
        //            }).then(function (modal) {
        //                modal.element.modal();
        //                modal.close.then(function (result) {
        //                    $scope.message = "You said " + result;
        //                });
        //            });
        //            return false;
        //        }
        //        else if (tempBatchList1.length <= 0) {
        //            productStockService.getBatchForReturn($scope.selectedProduct.product.id).then(function (responses) {
        //                var batchListCheck = responses.data;
        //                var totalQuantityCheck = 0;
        //                var tempBatchCheck = [];
        //                var editBatchCheck = null;
        //                for (var i = 0; i < batchListCheck.length; i++) {
        //                    batchListCheck[i].productStockId = batchListCheck[i].id;
        //                    batchListCheck[i].id = null;
        //                    var availableStockCheck = $scope.getAvailableStock(batchListCheck[i], editBatchCheck);
        //                    if (editBatchCheck != null && editBatchCheck.productStockId == batchListCheck[i].productStockId)
        //                        editBatchCheck.availableQty = availableStockCheck;
        //                    totalQuantityCheck += availableStockCheck;
        //                    //if (availableStockCheck == 0)
        //                    //    continue;
        //                    batchListCheck[i].availableQty = availableStockCheck;
        //                    tempBatchCheck.push(batchListCheck[i]);
        //                }
        //                if (editBatchCheck != null && tempBatchCheck.length == 0) {
        //                    var availableStockCheck = $scope.getAvailableStock(editBatchCheck, editBatchCheck);
        //                    editBatchCheck.availableQty = availableStockCheck;
        //                    totalQuantityCheck = availableStockCheck;
        //                    tempBatchCheck.push(editBatchCheck);
        //                }
        //                batchListCheck = tempBatchCheck;
        //                if (batchListCheck.length > 0) {
        //                    $scope.zeroBatch = false;
        //                    salesService.getBatchListDetail().then(function (response1) {
        //                        $scope.batchListType = response1.data;
        //                        if ($scope.batchListType == "Batch") {
        //                            $scope.ProductDetails();
        //                        }
        //                        else {
        //                            loadBatch(null);
        //                        }
        //                    }, function () {
        //                    });
        //                }
        //                else {
        //                    $scope.zeroBatch = true;
        //                }
        //            }, function () { });
        //        }
        //    }, function (error) {
        //        console.log(error);
        //    });
        //}
    };
    $rootScope.$on("tempStockAlertClose", function () {
        var keyPressed = cacheService.get('pressedKey');
        if (parseInt(keyPressed) == 27) {
            $scope.enableTempStockPopup = true;
        } else {
            $scope.enableTempStockPopup = false;
        }
        salesService.getBatchListDetail().then(function (response) {
            $scope.batchListType = response.data;
            if ($scope.batchListType == "Batch") {
                $scope.ProductDetails();
            } else {
                loadBatch(null);
            }
        }, function () {
        });
    });
    $scope.getAvailableStock = function (productStock, editBatch) {
        var addedItem = getAddedItem(productStock.productStockId);
        var newAddedQty = getAddedItemQuantity(addedItem);
        if (editBatch == null)
            return productStock.stock - newAddedQty;
        else if (editBatch.id != null && productStock.productStockId == editBatch.productStockId)
            return productStock.stock + parseInt(editBatch.quantity) - newAddedQty;
        else if (editBatch != null && productStock.productStockId == editBatch.productStockId) {
            return productStock.stock - (newAddedQty - parseInt(editBatch.quantity));
        }
        else if (editBatch != null && productStock.productStockId != editBatch.productStockId) {
            return productStock.stock - newAddedQty;
        }
        return productStock.stock;
    };
    function getAddedItemQuantity(addedItems) {
        var qty = 0;
        for (var i = 0; i < addedItems.length; i++) {
            if (addedItems[i].id != null) {
                qty += parseInt(addedItems[i].quantity) - parseInt(addedItems[i].orginalQty);
            } else {
                qty += parseInt(addedItems[i].quantity);
            }
        }
        return qty;
    }
    function getAddedItem(id) {
        var addedItems = [];
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ($scope.sales.salesItem[i].productStockId == id)
                addedItems.push($scope.sales.salesItem[i]);
        }
        return addedItems;
    }
    function getAddedItems(id, sellingPrice, discount) {
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ($scope.sales.salesItem[i].productStockId == id && $scope.sales.salesItem[i].sellingPrice == sellingPrice && $scope.sales.salesItem[i].discount == discount) {
                return $scope.sales.salesItem[i];
            }
        }
        return null;
    }
    $scope.editId = 1;
    $scope.validateDiscount = function (discount) {
        if (parseFloat(discount) > 100) {
            $scope.salesItems.$valid = true;
        } else {
            $scope.salesItems.$valid = false;
        }
    };
    $scope.Quantityexceeds = 0;
    $scope.validateQty = function (valqty) {
        if ($scope.Editsale == true) {
            if ($scope.selectedBatch.salesOrderEstimateId != null && $scope.selectedBatch.salesOrderProductSelected == 1) {
                if ($scope.selectedBatch.orginalQty > $scope.selectedBatch.quantity) {
                    toastr.info("Sales qty cannot be lesser than ordered qty");
                    $scope.addbtnenable = true;
                    $scope.salesItems.$valid = true;
                    return;
                }
            }
        }
        if (valqty == true) {
            $scope.valqty = true;
            $scope.addbtnenable = false;
            $scope.enableSaleBtn();
        }
        if ($scope.selectedBatch == null)
            return;
        if ($scope.selectedBatch.salesReturn) {
            if ($scope.selectedBatch.quantity >= 0) {
                $scope.salesItems.$valid = true;
                $scope.Quantityexceeds = 0;
            } else {
                $scope.salesItems.$valid = false;
                $scope.Quantityexceeds = 0;
            }
            return;
        }
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.totalQuantity && $scope.selectedBatch.editId == null) {
            $scope.Quantityexceeds = 1;
        }
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty && $scope.selectedBatch.editId != null) {
            $scope.Quantityexceeds = 1;
        }
        //Code added by Sarubala - For Return in Sales
        if ($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty && !$scope.selectedBatch.salesReturn) {
            for (var k = 0; k < $scope.sales.salesItem.length; k++) {
                if ($scope.sales.salesItem[k].salesReturn && ($scope.selectedBatch.product.id == $scope.sales.salesItem[k].product.id)) {
                    $scope.Quantityexceeds = 1;
                    toastr.info("Product already in Return.Please enter quantity available in selected batch");
                    document.getElementById("quantity").focus();
                    return;
                }
            }
        }
        //End
        var qty = document.getElementById("quantity");
        if (qty.value == "") {
            $scope.salesItems.$valid = false;
            $scope.Quantityexceeds = 0;
        } else {
            if ((qty.value > $scope.selectedBatch.totalQuantity && $scope.selectedBatch.editId == null) || (qty.value > $scope.selectedBatch.availableQty && $scope.selectedBatch.editId != null)) {
                $scope.IsFormInvalid = false;
                $scope.salesItems.$valid = true;
                if ($scope.Quantityexceeds == 1) {
                    $scope.IsFormInvalid = false;
                    $scope.salesItems.$valid = true;
                } else {
                    $scope.salesItems.$valid = false;
                    $scope.IsFormInvalid = true;
                }
            } else {
                if (typeof ($scope.selectedBatch.discount) == "string") {
                    //if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || $scope.selectedBatch.discount == "" || $scope.selectedBatch.discount == undefined || parseFloat($scope.selectedBatch.discount) > 100) {
                    if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || parseFloat($scope.selectedBatch.discount) > 100) {
                        $scope.salesItems.$valid = false;
                        //var sell = document.getElementById("sellingPrice");
                        //sell.focus();
                        $scope.IsFormInvalid = true;
                    } else {
                        $scope.IsFormInvalid = false;
                        $scope.salesItems.$valid = true;
                    }
                } else {
                    if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == "" || parseFloat($scope.selectedBatch.discount) > 100) {
                        $scope.salesItems.$valid = false;
                        //var sell = document.getElementById("sellingPrice");
                        //sell.focus();
                        $scope.IsFormInvalid = true;
                    } else {
                        $scope.IsFormInvalid = false;
                        $scope.salesItems.$valid = true;
                    }
                }
                $scope.Quantityexceeds = 0;
            }
        }
    };

    //Added by Sarubala on 16-08-17
    $scope.validateGst = function () {
        $scope.validateQty(true);
    }

    $scope.hasCustomerData = function () {
        var hasCustomer = false;
        //if ($scope.customerHelper.data.selectedCustomer.name == null || $scope.customerHelper.data.selectedCustomer.mobile == null || $scope.customerHelper.data.selectedCustomer.id == null) {
        if ($scope.customerHelper.data.selectedCustomer.name == null) {
            hasCustomer = true;
        } else {
            hasCustomer = false;
        }
        return hasCustomer;
    };
    $scope.MissedOrder = function () {
        var custname = document.getElementById("customerName").value.trim();
        var custmobile = document.getElementById("searchPatientMobile").value;
        $scope.enablePopup = true; //Added by Sarubala on 09-10-17
        var m = ModalService.showModal({
            "controller": "missedOrderCreateCtrl",
            "templateUrl": 'missedOrders',
            "inputs": {
                "patientId": $scope.customerHelper.data.selectedCustomer.id,
                "patientPhone": $scope.customerHelper.data.selectedCustomer.mobile == undefined ? custmobile : $scope.customerHelper.data.selectedCustomer.mobile,
                "patientName": $scope.customerHelper.data.selectedCustomer.name == undefined ? custname : $scope.customerHelper.data.selectedCustomer.name,
                "patientAge": $scope.customerHelper.data.selectedCustomer.age,
                "patientGender": $scope.customerHelper.data.selectedCustomer.gender
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
                $scope.enablePopup = false; //Added by Sarubala on 09-10-17
            });
        });
        return false;
    };
    $scope.ReturnProduct = function () {
        $scope.returnPopup = true;
        var m = ModalService.showModal({
            "controller": "productReturnCtrl",
            "templateUrl": 'productReturn',
            "inputs": {
                "salesItem": $scope.sales.salesItem,
                "gstEnable": $scope.isGstEnabled,
                "patientType": customerHelper.data.selectedCustomer.patientType,  //Patient Type Added by Poongodi on 30/05/2017
                "patientId": customerHelper.data.selectedCustomer.id, //Added by lawrence for department print "only return"
                "salesPriceSettings": $scope.salesPriceSettings, //Added by Sarubala on 07-11-17
                "isComposite": $scope.isComposite,
                "taxValuesList": $scope.taxValuesList
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.returnItems = result;
                $scope.returnPopup = false;
                if ($scope.returnItems != null) {
                    $scope.addReturnItems();
                }
                document.getElementById("drugName").focus();
            });
        });
        return false;
    };
    //$scope.ProductDetails = function () {
    //    $scope.enablePopup = true;
    //    cacheService.set('selectedProduct1', $scope.selectedProduct)
    //    var m = ModalService.showModal({
    //        controller: "productDetailSearchCtrl",
    //        templateUrl: 'productDetails'
    //        , inputs: {
    //            productId: $scope.selectedProduct.product.id,
    //            productName: $scope.selectedProduct.name,
    //            items: $scope.sales.salesItem,
    //            enableWindow: $scope.enablePopup
    //        }
    //    }).then(function (modal) {
    //        modal.element.modal();
    //        modal.close.then(function (result) {
    //            $scope.message = "You said " + result;
    //        });
    //    });
    //    return false;
    //};
    $scope.addReturnItems = function () {
        if ($scope.returnItems.length > 0) {
            $scope.returnDiscountInValid = true;
            for (var x = 0; x < $scope.returnItems.length; x++) {
                if ($scope.returnItems[x].editId == null || $scope.returnItems[x].editId == undefined) {
                    $scope.returnItems[x].editId = $scope.editId++;
                    $scope.returnItems[x].productName = $scope.returnItems[x].product.name; //Added by Sarubala on 07-09-17 to save product name
                    //Added by Sarubala on 07-11-17 to show whole sale price
                    if ($scope.salesPriceSettings == 3) {
                        $scope.returnItems[x].sellingPrice = parseFloat(($scope.returnItems[x].purchasePriceWithoutTax * (1 + ($scope.returnItems[x].gstTotal / 100))).toFixed(6));
                    }

                    $scope.sales.salesItem.push($scope.returnItems[x]);
                }
            }
            $scope.BillCalculations();
            //getDiscountRules();
            $scope.sales.discount = 0;
            if ($scope.sales.discountValue == undefined) {
                $scope.sales.discountValue = 0;
            }
            $scope.DiscountCalculations1();
        } else {
            $scope.returnDiscountInValid = false;
        }
        $scope.returnItems = null;
    };
    $scope.ProductDetails = function () {
        //if ($scope.selectedBatch == undefined && !$scope.selectedBatch.salesReturn) {
        if (!$scope.selectedBatch.salesReturn) {
            productStockService.productBatch($scope.selectedProduct.product.id).then(function (response) {
                var batchList = response.data;
                var totalQuantity = 0;
                var rack = "";
                var tempBatch = [];
                var tempBatchScan = [];
                $scope.changediscountsale("");
                var editBatch = null;
                var availableStock = null;
                for (var i = 0; i < batchList.length; i++) {
                    batchList[i].productStockId = batchList[i].id;
                    batchList[i].id = null;
                    availableStock = $scope.getAvailableStock(batchList[i], editBatch);
                    if (editBatch != null && editBatch.productStockId == batchList[i].productStockId)
                        editBatch.availableQty = availableStock;
                    totalQuantity += availableStock;
                    if (availableStock == 0)
                        continue;
                    batchList[i].availableQty = availableStock;
                    if (batchList[i].rackNo != null || batchList[i].rackNo != undefined) {
                        rack = batchList[i].rackNo;
                    }
                    tempBatch.push(batchList[i]);
                    if ($scope.scanBarcodeVal !== undefined && $scope.scanBarcodeVal !== "" && $scope.scanBarcodeVal !== null) {
                        if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')(batchList[i].purchaseBarcode)) {
                            tempBatchScan.push(batchList[i]);
                        }
                        else if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')(batchList[i].eancode)) {
                            tempBatchScan.push(batchList[i]);
                        }
                    }
                }
                if (editBatch != null && tempBatch.length == 0) {
                    availableStock = $scope.getAvailableStock(editBatch, editBatch);
                    editBatch.availableQty = availableStock;
                    totalQuantity = availableStock;
                    tempBatch.push(editBatch);
                }
                batchList = tempBatch;
                if (tempBatchScan.length > 0) {
                    batchList = rearrangeBatchList(batchList, tempBatchScan);
                }
                if (tempBatchScan.length === 1) {
                    tempBatchScan[0].totalQuantity = totalQuantity;
                    cacheService.set('selectedProduct1', tempBatchScan[0]);
                    cacheService.set('batchList1', batchList);
                    $scope.productSelection();
                    loadAutoQty();
                } else if (batchList.length > 0) {
                    $scope.zeroBatch = false;
                    $scope.enablePopup = true;
                    cacheService.set('selectedProduct1', $scope.selectedProduct);
                    var m = ModalService.showModal({
                        "controller": "productDetailSearchCtrl",
                        "templateUrl": 'productDetails',
                        "inputs": {
                            "productId": $scope.selectedProduct.product.id,
                            "productName": $scope.selectedProduct.name,
                            "items": $scope.sales.salesItem,
                            "salesReturn": $scope.selectedBatch.salesReturn,
                            "enableWindow": $scope.enablePopup,
                            "salesPriceSettings": $scope.salesPriceSettings
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            $scope.message = "You said " + result;
                        });
                    });
                    return false;
                } else {
                    $scope.zeroBatch = true;
                }
            }, function () { });
        } else {
            productStockService.getBatchForReturn($scope.selectedProduct.product.id).then(function (response) {
                var batchList = response.data;
                var totalQuantity = 0;
                var rack = "";
                var tempBatch = [];
                var tempBatchScan = [];
                $scope.changediscountsale("");
                var editBatch = null;
                var availableStock = null;
                for (var i = 0; i < batchList.length; i++) {
                    batchList[i].productStockId = batchList[i].id;
                    batchList[i].id = null;

                    //Added by Sarubala on 07-11-17 to show whole sale price
                    if ($scope.salesPriceSettings == 3) {
                        $scope.batchList[i].sellingPrice = parseFloat(($scope.batchList[i].returnPurchasePriceWithoutTax * (1 + ($scope.batchList[i].gstTotal / 100))).toFixed(6));
                        $scope.batchList[i].purchasePriceWithoutTax = $scope.batchList[i].returnPurchasePriceWithoutTax;
                    }

                    availableStock = $scope.getAvailableStock(batchList[i], editBatch);
                    if (editBatch != null && editBatch.productStockId == batchList[i].productStockId)
                        editBatch.availableQty = availableStock;
                    totalQuantity += availableStock;
                    //if (availableStock == 0)
                    //    continue;
                    batchList[i].availableQty = availableStock;
                    if (batchList[i].rackNo != null || batchList[i].rackNo != undefined) {
                        rack = batchList[i].rackNo;
                    }
                    tempBatch.push(batchList[i]);
                    if ($scope.scanBarcodeVal !== undefined && $scope.scanBarcodeVal !== "" && $scope.scanBarcodeVal !== null) {
                        if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')(batchList[i].purchaseBarcode)) {
                            tempBatchScan.push(batchList[i]);
                        }
                        else if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')(batchList[i].eancode)) {
                            tempBatchScan.push(batchList[i]);
                        }
                    }
                }
                if (editBatch != null && tempBatch.length == 0) {
                    availableStock = $scope.getAvailableStock(editBatch, editBatch);
                    editBatch.availableQty = availableStock;
                    totalQuantity = availableStock;
                    tempBatch.push(editBatch);
                }
                batchList = tempBatch;
                if (tempBatchScan.length > 0) {
                    batchList = rearrangeBatchList(batchList, tempBatchScan);
                }
                if (tempBatchScan.length === 1) {
                    tempBatchScan[0].totalQuantity = totalQuantity;
                    cacheService.set('selectedProduct1', tempBatchScan[0]);
                    cacheService.set('batchList1', batchList);
                    $scope.productSelection();
                    loadAutoQty();
                } else if (batchList.length > 0) {
                    $scope.zeroBatch = false;
                    $scope.enablePopup = true;
                    cacheService.set('selectedProduct1', $scope.selectedProduct);
                    var m = ModalService.showModal({
                        "controller": "productDetailSearchCtrl",
                        "templateUrl": 'productDetails',
                        "inputs": {
                            "productId": $scope.selectedProduct.product.id,
                            "productName": $scope.selectedProduct.name,
                            "items": $scope.sales.salesItem,
                            // "salesReturn": $scope.selectedBatch.salesReturn,
                            "enableWindow": $scope.enablePopup
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            $scope.message = "You said " + result;
                        });
                    });
                    return false;
                } else {
                    $scope.zeroBatch = true;
                }
            }, function () { });
            //}
            //else {
            //    productStockService.getBatchForReturn($scope.selectedProduct.product.id).then(function (response) {
            //        var batchList = response.data;
            //        var totalQuantity = 0;
            //        var rack = "";
            //        var tempBatch = [];
            //        var tempBatchScan = [];
            //        $scope.changediscountsale("");
            //        var editBatch = null;
            //        for (var i = 0; i < batchList.length; i++) {
            //            batchList[i].productStockId = batchList[i].id;
            //            batchList[i].id = null;
            //            var availableStock = $scope.getAvailableStock(batchList[i], editBatch);
            //            if (editBatch != null && editBatch.productStockId == batchList[i].productStockId)
            //                editBatch.availableQty = availableStock;
            //            totalQuantity += availableStock;
            //            //if (availableStock == 0)
            //            //    continue;
            //            batchList[i].availableQty = availableStock;
            //            if (batchList[i].rackNo != null || batchList[i].rackNo != undefined) {
            //                rack = batchList[i].rackNo;
            //            }
            //            tempBatch.push(batchList[i]);
            //            if ($scope.scanBarcodeVal !== undefined && $scope.scanBarcodeVal !== "" && $scope.scanBarcodeVal !== null) {
            //                if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')(batchList[i].eancode)) {
            //                    tempBatchScan.push(batchList[i]);
            //                }
            //            }
            //        }
            //        if (editBatch != null && tempBatch.length == 0) {
            //            var availableStock = $scope.getAvailableStock(editBatch, editBatch);
            //            editBatch.availableQty = availableStock;
            //            totalQuantity = availableStock;
            //            tempBatch.push(editBatch);
            //        }
            //        batchList = tempBatch;
            //        $scope.scanBarcodeVal = "";
            //        if (tempBatchScan.length === 1) {
            //            tempBatchScan[0].totalQuantity = totalQuantity;
            //            cacheService.set('selectedProduct1', tempBatchScan[0]);
            //            cacheService.set('batchList1', batchList);
            //            $scope.productSelection();
            //            if ($scope.scanBarcodeOption.autoScanQty != 0 && $scope.scanBarcodeOption.autoScanQty != undefined && $scope.scanBarcodeOption.autoScanQty != null) {
            //                $scope.selectedBatch.quantity = $scope.scanBarcodeOption.autoScanQty;
            //                angular.element("#spnquantity").attr("data-quantityId", $scope.selectedBatch.quantity);
            //                angular.element("#spnquantity").attr("data-price", $scope.selectedBatch.sellingPrice);
            //                angular.element("#spnquantity").attr("data-discount", $scope.selectedBatch.discount);
            //                document.getElementById("quantity").value = $scope.selectedBatch.quantity;
            //                $scope.validateQty();
            //                if ($scope.Quantityexceeds != 1) {
            //                    $scope.addSales();
            //                    angular.element("#spnquantity").attr("data-quantityId", "");
            //                    angular.element("#spnquantity").attr("data-price", "");
            //                    angular.element("#spnquantity").attr("data-discount", "");
            //                    document.getElementById("quantity").value = "";
            //                }
            //            }
            //        }
            //        else if (batchList.length > 0) {
            //            $scope.zeroBatch = false;
            //            $scope.enablePopup = true;
            //            cacheService.set('selectedProduct1', $scope.selectedProduct);
            //            var m = ModalService.showModal({
            //                "controller": "productDetailSearchCtrl",
            //                "templateUrl": 'productDetails',
            //                "inputs": {
            //                    "productId": $scope.selectedProduct.product.id,
            //                    "productName": $scope.selectedProduct.name,
            //                    "items": $scope.sales.salesItem,
            //               //     "salesReturn": $scope.selectedBatch.salesReturn,
            //                    "enableWindow": $scope.enablePopup
            //                }
            //            }).then(function (modal) {
            //                modal.element.modal();
            //                modal.close.then(function (result) {
            //                    $scope.message = "You said " + result;
            //                });
            //            });
            //            return false;
            //        }
            //        else {
            //            $scope.zeroBatch = true;
            //        }
            //    }, function () { });
            //}
        }
    };
    $rootScope.$on("productDetail", function (data) {

        var getGst = cacheService.get('selectedProduct1');
        $scope.getGstvalue = getGst.gstTotal;
        if ($scope.taxValuesList.length > 0) {
            var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.getGstvalue) }, true);
            if (taxList.length == 0) {
                $scope.gstMode = true;
                return;
            }
            else {
                $scope.gstMode = false;
            }
        }
        $scope.productSelection();
        if ($scope.scanBarcodeVal !== undefined && $scope.scanBarcodeVal !== "" && $scope.scanBarcodeVal !== null) {
            loadAutoQty();
        }
    });
    $rootScope.$on("productDetailCancel", function (data) {
        var prod = document.getElementById("drugName");
        prod.focus();
    });
    // Added Gavaskar 12-04-2017 Start
    // Comment Gavaskar 13-04-2017 Customer Credit Series Start
    //$rootScope.$on("CustomerCreditSeries", function () {
    //if ($scope.customerHelper.data.patientSearchData.customerPaymentType == "Credit") {
    //    $scope.InvoiceSeriestype = 4;
    //    getCreditCustomerInvoicenumber();
    //    $scope.IsSelectedInvoiceSeries = false;
    //}
    //});
    // Comment Gavaskar 13-04-2017 Customer Credit Series End
    // Added Gavaskar 12-04-2017 End
    $rootScope.$on("ResetCustomerdetails", function () {
        var custname = document.getElementById("customerName").value.trim();

        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                $scope.iscustomerNameMandatory = true;
            } else {
                $scope.iscustomerNameMandatory = false;
                $scope.flagCustomername = true;
            }
        }
        if ($scope.sales.credit == null || $scope.sales.credit == undefined) {
            $scope.sales.credit = "";
        }
        if ($scope.sales.cashType == 'Credit' && $scope.sales.credit == "") {
            $scope.iscashtypevalid = true;
        }
        if ($scope.sales.deliveryType == 'Home Delivery') {
            $scope.isdeliverytypevalid == true;
        }
        if ($scope.InvoiceSeriestype == 4) {
            getInvoiceSeries();
        }

        if ($scope.sales.paymentType != "Multiple" && $scope.sales.cashType != "MultiplePayment") { //Modified by Sarubala on 10-11-17
            $scope.sales.paymentType = "Cash"; // Added Gavaskar 13-04-2017
        }

        $scope.enableSaleBtn();

        document.getElementById('sellingPrice').disabled = false;
        document.getElementById('discount').disabled = false;


        if ((angular.isUndefinedOrNullZero($scope.sales.discount)) && ($scope.sales.salesItem.length > 0)) {

            for (var j = 0; j < $scope.sales.salesItem.length; j++) {
                if ($scope.sales.salesItem[j].discount > 0) {
                    document.getElementById('txtDiscount').disabled = true;
                    break;
                }
                else
                    document.getElementById('txtDiscount').disabled = false;
            }

        }


        if ($scope.Editsale == true) {
            $scope.sales.patientId = "";
            $scope.sales.name = "";
            $scope.sales.mobile = "";
            $scope.sales.gender = "";
            $scope.sales.email = "";
        }
        else {
            // Added by Gavaskar 06-11-2017 Start 
            $scope.selectedPatientUpdate = cacheService.get("selectedPatientUpdate");
            if ($scope.selectedPatientUpdate != "CustomerUpdate") {
                if ($scope.orderEstimateId != "" && $scope.orderEstimateId != null && $scope.orderEstimateId != undefined) {

                    //var r = confirm("Are you sure want to Sales all Items?");

                    //if (r == true) {
                    $scope.sales.salesItem = [];
                    $scope.salesEstimateProductArray = [];
                    $scope.orderEstimateId = null;

                    //}
                    //else
                    //{
                    //}
                }
            }
            cacheService.remove('selectedPatientUpdate');

            // Added by Gavaskar 06-11-2017 End 
        }
        if (redeemEnable.checked == false) {
            $scope.isRedeemPts = false;
            $scope.isRedeemValue = false;
            $scope.sales.redeemPts = "";
            $scope.sales.redeemAmt = "";
            $scope.focusRedeemPts = false;
        }
        patientSelected = 0;

        $scope.focusCustomerName = true;

    });
    $rootScope.$on("CheckCustomerdetails", function (event, args) {
        var doctorName = "";
        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                if (args.message == "false") {
                    $scope.iscustomerNameMandatory = true;
                } else {
                    $scope.iscustomerNameMandatory = false;
                }
            }

            // Added by San 10-10-2017
            if (!angular.isUndefinedOrNull($scope.discountRules)) {
                if ($scope.discountRules.billAmountType == "productCustomerWiseDiscount") {
                    if ($scope.maxDiscountFixed == 'Yes') {
                        if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue)
                            $scope.customerHelper.data.selectedCustomer.discount = $scope.maxDisountValue;
                    }

                    editProductCustomerwisDiscount();
                }
            }
            else {
                salesService.getDiscountDetail().then(function (response) {
                    $scope.discountRules = response.data;
                });
                if ($scope.maxDiscountFixed == 'Yes') {
                    if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue)
                        $scope.customerHelper.data.selectedCustomer.discount = $scope.maxDisountValue;
                }
            }
        }
        if ($scope.Editsale != true) {
            if ($scope.orderEstimateId == null || $scope.orderEstimateId == "") {
                salesService.getSelectedCustomerOrderEstimateList($scope.customerHelper.data.selectedCustomer.id).then(function (response) {

                    $scope.SelectedCustomerOrderEstimateList = response.data;
                    if ($scope.SelectedCustomerOrderEstimateList.length > 0 || $scope.SelectedCustomerOrderEstimateList.length > 0) {
                        $scope.showSalesOrderEstimateItems();
                    }

                }, function () {
                });
            }
        }

        if ($scope.salesEditId && customerHelper.data.selectedCustomer.loyaltyPoint > 0) {
            customerHelper.data.selectedCustomer.loyaltyPoint = customerHelper.data.selectedCustomer.loyaltyPoint - $scope.sales.loyaltyPts;
            if (parseFloat(customerHelper.data.selectedCustomer.loyaltyPoint) < 0) {
                customerHelper.data.selectedCustomer.loyaltyPoint = 0;
            }
        }

        if ($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) {
            var custname = document.getElementById("customerName").value.trim();
            var docname = document.getElementById("doctorName").value;
            if (custname == "" && $scope.customerHelper.data.isCustomerSelected != true) {
                var customername = document.getElementById("customerName");
                customername.focus();
            }
            else {
                doctorName = document.getElementById("doctorName");
                doctorName.focus();
            }
        } else {
            doctorName = document.getElementById("doctorName");
            doctorName.focus();
        }
        if (customerHelper.data.selectedCustomer != null) {
            if ($scope.chequeCustomerRequired) {
                $scope.chequeCustomerRequired = false;
            }
        }
        if ($scope.iscashtypevalid == true) {
            $scope.iscashtypevalid = false;
        }
        if ($scope.isdeliverytypevalid == true) {
            $scope.isdeliverytypevalid = false;
        }
        if (customerHelper.data.selectedCustomer.patientType == 2) {
            document.getElementById('sellingPrice').disabled = true;
            document.getElementById('discount').disabled = true;
            document.getElementById('txtDiscount').disabled = true;
            if ($scope.salesEditId != "")
                $scope.customerHelper.data.selectedCustomer.address = $scope.sales.address;
        }
        if ($scope.sales.paymentType == null)
            $scope.sales.paymentType = "Cash"; // Added Gavaskar 13-04-2017
    });
    $scope.LeadProductIndex = "";
    $scope.LeadProductId = "";
    $scope.showproductSelected = {};
    $scope.LeadproductSelection = function (bool, ind, id) {
        var product1 = cacheService.get('selectedLeadProduct');
        $scope.selectedProduct = product1;
        $scope.LeadsProductSelected = bool;
        $scope.LeadProductIndex = ind;
        $scope.LeadProductId = id;
        $scope.onProductSelect();
        //if (product1 != null) {
        //    $scope.selectedBatch = product1;
        //}
        //if (blist.length > 0) {
        //    $scope.batchList = blist;
        //}
        //dayDiff($scope.selectedBatch.expireDate);
        //$scope.selectedBatch.totalQuantity = product1.totalQuantity;
        //var qty = document.getElementById("quantity");
        //qty.focus();
    };
    $scope.productSelection = function () {
        var product1 = cacheService.get('selectedProduct1');
        var blist = cacheService.get('batchList1');
        $scope.enablePopup = cacheService.get('enableWindow1');
        var salesReturn = $scope.selectedBatch.salesReturn;
        var transactionType = $scope.selectedBatch.transactionType;
        if (product1 != null) {
            $scope.selectedBatch = product1;
            $scope.selectedBatch.rackNo = product1.product.rackNo;
            $scope.selectedBatch.canEnableTaxEdit = (product1.gstTotal == 0);
        }
        if ($scope.selectedProduct == undefined) {
            $scope.selectedProduct = product1;
        }
        //if ($scope.customerHelper.data.patientSearchData.patientType == 2) {
        //    $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.vat / 100))) * 1.1).toFixed(2);
        //}
        //if ($scope.customerHelper.data.customerList.length > 0)
        //{
        //    if ($scope.customerHelper.data.customerList[0].patientType == 2) {
        //        $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.vat / 100))) * 1.1).toFixed(2);
        //    }
        //}
        //for (var i = 0; i < $scope.customerHelper.data.customerList.length; i++) {
        //    if ($scope.customerHelper.data.patientSearchData.mobile == $scope.customerHelper.data.customerList[i].mobile) {
        //        if ($scope.customerHelper.data.selectedCustomer.name == $scope.customerHelper.data.customerList[i].name) {
        if ($scope.customerHelper.data.selectedCustomer.patientType == 2) {

            var vatPercent = 0;
            if ($scope.isGstEnabled == true) {
                if ($scope.selectedBatch.gstTotal != null && $scope.selectedBatch.gstTotal != undefined) {
                    vatPercent = $scope.selectedBatch.gstTotal;
                } else if ($scope.selectedBatch.product.gstTotal != null && $scope.selectedBatch.product.gstTotal != undefined) {
                    vatPercent = $scope.selectedBatch.product.gstTotal;
                }

            } else {
                vatPercent = $scope.selectedBatch.vat;
            }
            if ($scope.customerHelper.data.selectedCustomer.id != "30fdae11-4a06-4f9d-a87d-3ead26a8d110") {
                $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * (vatPercent / 100))) * 1.1).toFixed(2);
            }
            else {
                $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * (vatPercent / 100)))).toFixed(2);
            }
        }
        //        }
        //    }
        //}
        $scope.selectedBatch.salesReturn = salesReturn;
        $scope.selectedBatch.transactionType = transactionType;
        if (blist.length > 0) {
            $scope.batchList = blist;
        }
        dayDiff($scope.selectedBatch.expireDate);
        $scope.selectedBatch.name = $scope.selectedProduct.product.name;

        // By San 23-09-2017
        if (!angular.isUndefinedOrNull($scope.selectedBatch.product.discount) || !angular.isUndefinedOrNull($scope.customerHelper.data.selectedCustomer.discount)) {
            getProductwisDiscounteSettings();
        }
        else
            $scope.selectedBatch.discount = "";


        if (LeadsRequiredquantity != "") {
            $scope.selectedBatch.quantity = LeadsRequiredquantity;
        }
        $scope.selectedBatch.totalQuantity = product1.totalQuantity;
        var qty = document.getElementById("quantity");
        qty.focus();
        //Added by Annadurai on 07032017 for showing History Popup.
        LoadPreviousSalesItemHistory();
    };
    $scope.PopupAlternates = function () {
        var showPopup = false;
        if (($scope.selectedProduct == null || $scope.selectedProduct == undefined)) {
            showPopup = true;
        } else {
            if ($scope.selectedProduct.stock == 0) {
                showPopup = false;
            } else {
                showPopup = true;
            }
        }
        if (showPopup) {
            //if ($scope.selectedProduct.stock > 0) {
            $scope.zeroQty = false;
            $scope.IsFormInvalid = false;
            $scope.salesItems.$valid = true;
            $scope.enablePopup = true; //Added by Sarubala on 09-10-17
            cacheService.set('selectedAlternate1', $scope.selectedProduct);
            var m = ModalService.showModal({
                "controller": "alternateProductSearchCtrl",
                "templateUrl": 'searchProduct',
                "inputs": {
                    "selectedProduct": ($scope.selectedProduct != null) ? $scope.selectedProduct : null,
                    "selectedProductId": ($scope.selectedProduct != null && $scope.selectedProduct.product != null) ? $scope.selectedProduct.product.id : null
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.message = "You said " + result;
                    $scope.enablePopup = false; //Added by Sarubala on 09-10-17
                });
            });
        } else {
            $.alert({
                title: 'Stock not available!',
                content: 'Stock not available for the selected item (' + $scope.selectedProduct.product.name + ')',
                animation: 'center',
                closeAnimation: 'center',
                backgroundDismiss: true,
                buttons: {
                    okay: {
                        text: 'Ok [Esc]',
                        btnClass: 'primary-button custom-transform',
                        action: function () {
                            // do nothing
                        }
                    }
                }
            });
            //$scope.zeroQty = true;
            $scope.selectedProduct = "";
        }
        return false;
    };
    $scope.showAlternatesFromProduct = function () {
        $scope.selectedProduct.genericName = $scope.selectedProduct.name;
        $scope.selectedProduct.name = "";
        $scope.PopupAlternates();
    };
    $rootScope.$on("alternateDetailCancel", function (data) {
        // need to copy data by arun 
        if ($scope.isProductGenericSearch == true) {
            var drugName = document.getElementById("drugName");
            drugName.focus();
        } else {
            var genericName = document.getElementById("genericName");
            genericName.focus();
        }
    });
    $scope.showTempStockScreen = function () {
        $scope.focusInput = true;
        $scope.enablePopup = true; //Added by Sarubala on 09-10-17
        cacheService.set('tempStockSection', $scope.selectedProduct);
        var m = ModalService.showModal({
            "controller": "tempStockCtrl",
            "templateUrl": 'tempStock',
            inputs: {
                "screenType": "old",
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.message = "You said " + result;
                $scope.enablePopup = false; //Added by Sarubala on 09-10-17
                if ($scope.zeroBatch) {
                    $scope.zeroBatch = false;
                }
            });
        });
        return false;
    };
    //Added for Display Brach wise stock by bala, on 1/June/2017
    $scope.disableEsc = false;
    $scope.showStockBranchWise = function () {
        $scope.focusInput = true;
        $scope.disableEsc = true;
        var m = ModalService.showModal({
            "controller": "stockBranchWiseCtrl",
            "templateUrl": 'stockBranchWise',
            "inputs": { "tempProduct": null }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.disableEsc = false;
                $scope.message = "You said " + result;
                if ($scope.zeroBatch) {
                    $scope.zeroBatch = false;
                }
            });
        });
        return false;
    };
    $rootScope.$on("alternateSelection", function (data) {
        $scope.alternateSelection();
    });
    $rootScope.$on("tempStockSelection", function (data) {
        $scope.tempStockSelection();
    });
    $scope.tempStockSelection = function () {
        var tempStockValues = cacheService.get('tempStockValues');
        if ($scope.zeroBatch) {
            $scope.zeroBatch = false;
        }
        if (tempStockValues != null) {
            $scope.selectedProduct = tempStockValues.productStock.product.name;
            var editBatch = null;
            //productStockService.productBatch(tempStockValues.productStock.productId).then(function (response) {
            //    $scope.batchList = response.data;
            //    var totalQuantity = 0;
            //    var rack = "";
            //    var tempBatch = [];
            //    for (var x = 0; x < $scope.sales.salesItem.length; x++) {
            //        for (var y = 0; y < $scope.batchList.length; y++) {
            //            if ($scope.sales.salesItem[x].productStockId == $scope.batchList[y].id) {
            //                $scope.batchList[y].stock = $scope.batchList[y].stock - parseInt($scope.sales.salesItem[x].quantity);
            //            }
            //        }
            //    }
            //    for (var i = 0; i < $scope.batchList.length; i++) {
            //        if ($scope.batchList[i].purchasePrice == 0) {
            //            $scope.batchList[i].purchasePrice = ($scope.batchList[i].sellingPrice - ($scope.batchList[i].sellingPrice * 20) / 100);
            //        }
            //        $scope.batchList[i].productStockId = $scope.batchList[i].id;
            //        $scope.batchList[i].id = null;
            //        var availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
            //        totalQuantity += availableStock;
            //        if (availableStock == 0)
            //            continue;
            //        $scope.batchList[i].availableQty = availableStock;
            //        if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
            //            rack = $scope.batchList[i].rackNo;
            //        }
            //        tempBatch.push($scope.batchList[i]);
            //    }
            //    if (editBatch != null && tempBatch.length == 0) {
            //        tempBatch.push(editBatch);
            //    }
            //    $scope.batchList = tempBatch;
            //    if ($scope.batchList.length > 0) {
            //        if (editBatch == null) {
            //            $scope.selectedBatch = $scope.batchList[0];
            //            $scope.selectedBatch.reminderFrequency = "0";
            //        }
            //        else {
            //            $scope.selectedBatch = editBatch;
            //            $scope.selectedBatch.availableQty = availableStock;
            //        }
            //        $scope.selectedBatch.totalQuantity = totalQuantity;
            //        $scope.selectedBatch.rackNo = rack;
            //        $scope.selectedBatch.discount = "";
            //        dayDiff($scope.selectedBatch.expireDate);
            //    }
            //    //$scope.selectedBatch.batchNo = tempStockValues.productStock.batchNo;
            //    //$scope.selectedBatch.expireDate = tempStockValues.productStock.expireDate;
            //    //$scope.selectedBatch.vat = tempStockValues.productStock.vAT;
            //}, function () { });
            if (tempStockValues.productStock.productId == undefined) {
                tempStockValues.productStock.productId = tempStockValues.productStock.product.id;
            }
            if (!$scope.selectedBatch.salesReturn) {
                productStockService.productBatch(tempStockValues.productStock.productId).then(function (responses) {
                    var batchListCheck = responses.data;
                    var totalQuantityCheck = 0;
                    var tempBatchCheck = [];
                    var editBatchCheck = null;
                    var availableStockCheck = null;
                    for (var i = 0; i < batchListCheck.length; i++) {
                        batchListCheck[i].productStockId = batchListCheck[i].id;
                        batchListCheck[i].id = null;
                        //Added by Sarubala on 06-11-17
                        if ($scope.salesPriceSettings == 3) {
                            batchListCheck[i].sellingPrice = parseFloat((batchListCheck[i].purchasePriceWithoutTax * (1 + (batchListCheck[i].gstTotal / 100))).toFixed(6));
                        }

                        availableStockCheck = $scope.getAvailableStock(batchListCheck[i], editBatchCheck);
                        if (editBatchCheck != null && editBatchCheck.productStockId == batchListCheck[i].productStockId)
                            editBatchCheck.availableQty = availableStockCheck;
                        totalQuantityCheck += availableStockCheck;
                        if (availableStockCheck == 0)
                            continue;
                        batchListCheck[i].availableQty = availableStockCheck;
                        tempBatchCheck.push(batchListCheck[i]);
                    }
                    if (editBatchCheck != null && tempBatchCheck.length == 0) {
                        availableStockCheck = $scope.getAvailableStock(editBatchCheck, editBatchCheck);
                        editBatchCheck.availableQty = availableStockCheck;
                        totalQuantityCheck = availableStockCheck;
                        tempBatchCheck.push(editBatchCheck);
                    }
                    batchListCheck = tempBatchCheck;
                    if (batchListCheck.length > 0) {
                        $scope.zeroBatch = false;
                        //   loadBatch(null);
                        productStockService.productBatch(tempStockValues.productStock.productId).then(function (response) {
                            $scope.batchList = response.data;
                            var qty = document.getElementById("quantity");
                            qty.focus();
                            var totalQuantity = 0;
                            var rack = "";
                            var tempBatch = [];
                            var availableStock = "";
                            $scope.changediscountsale("");
                            for (var i = 0; i < $scope.batchList.length; i++) {
                                if ($scope.Editsale == false) {
                                    $scope.batchList[i].productStockId = $scope.batchList[i].id;
                                    $scope.batchList[i].id = null;

                                    //Added by Sarubala on 06-11-17
                                    if ($scope.salesPriceSettings == 3) {
                                        $scope.batchList[i].sellingPrice = parseFloat(($scope.batchList[i].purchasePriceWithoutTax * (1 + ($scope.batchList[i].gstTotal / 100))).toFixed(6));
                                    }
                                    availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                                    if (editBatch != null && editBatch.productStockId == $scope.batchList[i].productStockId)
                                        editBatch.availableQty = availableStock;
                                    totalQuantity += availableStock;
                                    if (availableStock == 0)
                                        continue;
                                    $scope.batchList[i].availableQty = availableStock;
                                    if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                                        rack = $scope.batchList[i].rackNo;
                                    }
                                    tempBatch.push($scope.batchList[i]);
                                } else {
                                    if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                                        rack = $scope.batchList[i].rackNo;
                                    }
                                }
                            }
                            if (editBatch != null && tempBatch.length == 0) {
                                availableStock = $scope.getAvailableStock(editBatch, editBatch);
                                editBatch.availableQty = availableStock;
                                totalQuantity = availableStock;
                                tempBatch.push(editBatch);
                            }
                            $scope.batchList = tempBatch;
                            if ($scope.batchList.length > 0) {
                                if (editBatch == null) {
                                    $scope.selectedBatch = $scope.batchList[0];
                                    $scope.selectedBatch.reminderFrequency = "0";

                                    //By San 25-09-2017
                                    if (!angular.isUndefinedOrNull($scope.selectedBatch.product.discount) || !angular.isUndefinedOrNull($scope.customerHelper.data.selectedCustomer.discount)) {
                                        getProductwisDiscounteSettings();
                                    }
                                    else
                                        $scope.selectedBatch.discount = "";

                                    $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                                } else {
                                    $scope.selectedBatch = editBatch;
                                    $scope.selectedBatch.discount = editBatch.discount;
                                    $scope.selectedBatch.previoussellingPrice = editBatch.sellingPrice;
                                    $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                                }
                                $scope.selectedBatch.totalQuantity = totalQuantity;
                                $scope.selectedBatch.rackNo = rack;
                                dayDiff($scope.selectedBatch.expireDate);
                                //if (editBatch == null) {
                                //    $scope.selectedBatch.discount = "";
                                //}
                            }
                        }, function () { });
                        $scope.valPurchasePrice = "";
                        var qty = document.getElementById("quantity");
                        qty.focus();
                    } else {
                        $scope.zeroBatch = true;
                    }
                }, function () { });
            } else {
                productStockService.getBatchForReturn(tempStockValues.productStock.productId).then(function (responses) {
                    var batchListCheck = responses.data;
                    var totalQuantityCheck = 0;
                    var tempBatchCheck = [];
                    var editBatchCheck = null;
                    var availableStockCheck = "";
                    for (var i = 0; i < batchListCheck.length; i++) {
                        batchListCheck[i].productStockId = batchListCheck[i].id;
                        batchListCheck[i].id = null;
                        availableStockCheck = $scope.getAvailableStock(batchListCheck[i], editBatchCheck);
                        if (editBatchCheck != null && editBatchCheck.productStockId == batchListCheck[i].productStockId)
                            editBatchCheck.availableQty = availableStockCheck;
                        totalQuantityCheck += availableStockCheck;
                        //if (availableStockCheck == 0)
                        //    continue;
                        batchListCheck[i].availableQty = availableStockCheck;
                        tempBatchCheck.push(batchListCheck[i]);
                    }
                    if (editBatchCheck != null && tempBatchCheck.length == 0) {
                        availableStockCheck = $scope.getAvailableStock(editBatchCheck, editBatchCheck);
                        editBatchCheck.availableQty = availableStockCheck;
                        totalQuantityCheck = availableStockCheck;
                        tempBatchCheck.push(editBatchCheck);
                    }
                    batchListCheck = tempBatchCheck;
                    if (batchListCheck.length > 0) {
                        $scope.zeroBatch = false;
                        //   loadBatch(null);
                        productStockService.getBatchForReturn(tempStockValues.productStock.productId).then(function (response) {
                            $scope.batchList = response.data;
                            var qty = document.getElementById("quantity");
                            qty.focus();
                            var totalQuantity = 0;
                            var rack = "";
                            var tempBatch = [];
                            var availableStock = "";
                            $scope.changediscountsale("");
                            for (var i = 0; i < $scope.batchList.length; i++) {
                                if ($scope.Editsale == false) {
                                    $scope.batchList[i].productStockId = $scope.batchList[i].id;
                                    $scope.batchList[i].id = null;
                                    availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                                    if (editBatch != null && editBatch.productStockId == $scope.batchList[i].productStockId)
                                        editBatch.availableQty = availableStock;
                                    totalQuantity += availableStock;
                                    //if (availableStock == 0)
                                    //    continue;
                                    $scope.batchList[i].availableQty = availableStock;
                                    if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                                        rack = $scope.batchList[i].rackNo;
                                    }
                                    tempBatch.push($scope.batchList[i]);
                                } else {
                                    if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                                        rack = $scope.batchList[i].rackNo;
                                    }
                                }
                            }
                            if (editBatch != null && tempBatch.length == 0) {
                                availableStock = $scope.getAvailableStock(editBatch, editBatch);
                                editBatch.availableQty = availableStock;
                                totalQuantity = availableStock;
                                tempBatch.push(editBatch);
                            }
                            $scope.batchList = tempBatch;
                            if ($scope.batchList.length > 0) {
                                if (editBatch == null) {
                                    $scope.selectedBatch = $scope.batchList[0];
                                    $scope.selectedBatch.reminderFrequency = "0";
                                    $scope.selectedBatch.discount = "";
                                    $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                                } else {
                                    $scope.selectedBatch = editBatch;
                                    $scope.selectedBatch.discount = editBatch.discount;
                                    $scope.selectedBatch.previoussellingPrice = editBatch.sellingPrice;
                                    $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                                }
                                $scope.selectedBatch.totalQuantity = totalQuantity;
                                $scope.selectedBatch.rackNo = rack;
                                dayDiff($scope.selectedBatch.expireDate);
                                //if (editBatch == null) {
                                //    $scope.selectedBatch.discount = "";
                                //}
                            }
                        }, function () { });
                        $scope.valPurchasePrice = "";
                        var qty = document.getElementById("quantity");
                        qty.focus();
                    } else {
                        $scope.zeroBatch = true;
                    }
                }, function () { });
            }
        }
        //var qty = document.getElementById("quantity");
        //qty.disabled = false;
        //qty.focus();
        //  $scope.selectedBatch = tempStockValues.productStock.batchNo;
    };
    $scope.alternateSelection = function () {
        var alter1 = cacheService.get('selectedAlternate1');
        if ($scope.zeroBatch) {
            $scope.zeroBatch = false;
        }
        if (alter1 != null) {
            $scope.batchList = alter1;
            $scope.selectedProduct = alter1[0];
            $scope.selectedProduct.product.id = alter1[0].productId;
            // need to copy data arun
            if ($scope.isProductGenericSearch == true) {
                $scope.selectedProduct.product.name = alter1[0].name;
            } else {
                $scope.selectedProduct.product.genericName = alter1[0].name;
                //Generic name not showing after select alternate product By SETTU
                $scope.selectedProduct.genericName = $scope.selectedProduct.product.genericName;
            }
            loadBatch(null);
        }
        var qty = document.getElementById("quantity");
        qty.focus();
    };
    $scope.isdoctornameMandatory = false;
    // Added by arun for enable and disable sales btn 
    $scope.enableSaleBtn = function () {
        $scope.Enablesalesbtn = true;
    };
    // Ended
    $scope.isSalesTypeMandatory = false;
    $scope.iscustomerNameMandatory = false;
    $scope.iscustomerMobileMandatory = false;
    $scope.maxDiscountExceeds = false;
    $scope.addSales = function () {

        if ($scope.selectedBatch.quantity <= 0 || $scope.selectedBatch.quantity == undefined) {
            return;
        }
        if ($scope.batchList.length == 0) {
            return;
        }

        if ($scope.isComposite == false) {
            if ($scope.selectedBatch.gstTotal == null) {
                toastr.info("Please provide GST % and try again.");
                document.getElementById("gstTotal").focus();
                return;
            }
            if ($scope.taxValuesList.length > 0) {
                var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.selectedBatch.gstTotal) }, true);
                if (taxList.length == 0) {
                    toastr.info("Please provide GST % and try again.");
                    document.getElementById("gstTotal").focus();
                    return;
                }
            }
        }


        //// Added by San - Product/Customer Wise Discount disable discount field
        //if ($scope.discountRules.billAmountType == "productCustomerWiseDiscount") {
        //    document.getElementById('discount').disabled = false;
        //}

        if (($scope.selectedBatch.quantity < $scope.salesReturnedQuantity && $scope.salesReturnedQuantity != 0)) {
            return false;
        }

        if ($scope.isGstEnabled) {
            if ((parseFloat($scope.selectedBatch.gstTotal) == "" || $scope.selectedBatch.gstTotal >= 100) && ($scope.selectedBatch.gstTotal != 0)) {
                //  console.log(parseFloat($scope.selectedBatch.gstTotal));
                return false;
            }
            ////Added by Sarubala on 16-08-17
            //if ($scope.selectedBatch.gstTotal == "") {
            //    $scope.selectedBatch.gstTotal = 0;
            //}
        }


        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            if (($scope.sales.salesItem[k].productStockId == $scope.selectedBatch.productStockId) && ($scope.sales.salesItem[k].productId == $scope.selectedBatch.productId) && ($scope.sales.salesItem[k].salesReturn != $scope.selectedBatch.salesReturn)) {
                toastr.info("Product already added in Return");
                document.getElementById("drugName").focus();
                return;
            }
        }
        //if ($scope.Editsale == true) {
        //    if (!confirm("Adding a New Product will remove existing Invoice wise Discount, Do you want to Continue ? "))
        //        return;
        //}
        $scope.maxDiscountExceeds = false;
        if ($scope.maxDiscountFixed == 'Yes') {
            if ($scope.selectedBatch.discount > $scope.maxDisountValue) {
                $scope.maxDiscountExceeds = true;
                return false;
            }
        }
        $scope.enableTempStockPopup = false;
        if ($scope.Quantityexceeds == 1) return;
        // $scope.enableSaleBtn();
        if ($scope.selprice == true || $scope.valdisct == true || $scope.valqty == true || $scope.edititem == false) {
            selprice = false;
            valdisct = false;
            valqty = false;
            $scope.edititem = false;
            $scope.valqty = false;
            $scope.valdisct = false;
            $scope.selprice = false;
            $scope.Enablesalesbtn = false;
            $scope.maxDiscountExceeds = false;
            if ($scope.maxDiscountFixed == 'Yes') {
                if ($scope.selectedBatch.discount > $scope.maxDisountValue) {
                    $scope.maxDiscountExceeds = true;
                    return false;
                }
            }
            $scope.enableTempStockPopup = false;
            if ($scope.Quantityexceeds == 1) return;
            if ($scope.selectedBatch.sellingPrice == undefined || $scope.selectedBatch.sellingPrice == null || ($scope.selectedBatch.purchasePrice > $scope.selectedBatch.sellingPrice && $scope.salesPriceSettings != 3) || (($scope.selectedBatch.purchasePriceWithoutTax == '' || $scope.selectedBatch.purchasePriceWithoutTax == null || $scope.selectedBatch.purchasePriceWithoutTax == undefined) && $scope.salesPriceSettings == 3)) { //Modified by Sarubala on 08-11-17
                return;
            }
            if (typeof ($scope.selectedBatch.discount) == "string") {
                if (parseFloat($scope.selectedBatch.discount) > 100 || $scope.selectedBatch.discount == '.') {
                    return;
                } else if ($scope.selectedBatch.discount == undefined || $scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "") {
                    $scope.selectedBatch.discount = 0;
                }
            } else {
                if (parseFloat($scope.selectedBatch.discount) > 100) {
                    return;
                } else if ($scope.selectedBatch.discount == undefined || $scope.selectedBatch.discount == null || $scope.selectedBatch.discount == "") {
                    $scope.selectedBatch.discount = 0;
                }
            }

            var RequiredQuantity = angular.element("#spnquantity").attr("data-quantityId");
            var ChangedMrp = angular.element("#spnquantity").attr("data-price");
            var ChangedDicount = angular.element("#spnquantity").attr("data-discount") || 0;
            if ($scope.selectedBatch.salesReturn) {
                $scope.selectedBatch.quantity = $scope.selectedBatch.quantity * (-1);
            }
            if (($scope.selectedBatch.quantity > $scope.selectedBatch.availableQty) && !$scope.selectedBatch.salesReturn) {
                var addedItem = "";
                for (var i = 0; i < $scope.batchList.length; i++) {
                    if ($scope.batchList[i].editId == null) {
                        $scope.batchList[i].editId = $scope.editId++;
                        addedItem = getAddedItems($scope.batchList[i].productStockId, $scope.batchList[i].sellingPrice, ChangedDicount);
                        if (addedItem != null) {
                            //if (RequiredQuantity > $scope.batchList[i].quantity) {
                            //    addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.batchList[i].quantity);
                            //} else {
                            //    addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.batchList[i].availableQty);
                            //}

                            if (RequiredQuantity != 0) {
                                if (RequiredQuantity > $scope.batchList[i].availableQty) {
                                    addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.batchList[i].availableQty);
                                    RequiredQuantity = RequiredQuantity - $scope.batchList[i].availableQty;
                                    $scope.batchList[i].availableQty = 0;
                                } else {
                                    $scope.batchList[i].availableQty = $scope.batchList[i].availableQty - RequiredQuantity;
                                    addedItem.quantity = parseInt(addedItem.quantity) + parseInt(RequiredQuantity);
                                    RequiredQuantity = 0;
                                }
                            }
                        } else {
                            if (RequiredQuantity != 0) {
                                if (RequiredQuantity > $scope.batchList[i].availableQty) {
                                    $scope.batchList[i].quantity = $scope.batchList[i].availableQty;
                                    RequiredQuantity = RequiredQuantity - $scope.batchList[i].availableQty;
                                    $scope.batchList[i].availableQty = 0;
                                } else {
                                    $scope.batchList[i].availableQty = $scope.batchList[i].availableQty - RequiredQuantity;
                                    $scope.batchList[i].quantity = RequiredQuantity;
                                    RequiredQuantity = 0;
                                }
                                $scope.batchList[i].discount = ChangedDicount;
                                $scope.batchList[i].salesReturn = false;
                                //$scope.batchList[i].sellingPrice = ChangedMrp;
                                //if ($scope.batchList[i].discount == 0) {
                                //    $scope.batchList[i].discount = "";
                                //}

                                if ($scope.customerHelper.data.selectedCustomer.patientType == 2) {

                                    var vatPercent = 0;
                                    if ($scope.isGstEnabled == true) {
                                        if ($scope.batchList[i].gstTotal != null && $scope.batchList[i].gstTotal != undefined) {
                                            vatPercent = $scope.batchList[i].gstTotal;
                                        } else if ($scope.batchList[i].product.gstTotal != null && $scope.batchList[i].product.gstTotal != undefined) {
                                            vatPercent = $scope.batchList[i].product.gstTotal;
                                        }

                                    } else {
                                        vatPercent = $scope.batchList[i].vat;
                                    }

                                    $scope.batchList[i].sellingPrice = ((($scope.batchList[i].packagePurchasePrice / $scope.batchList[i].packageSize) + (($scope.batchList[i].packagePurchasePrice / $scope.batchList[i].packageSize) * (vatPercent / 100))) * 1.1).toFixed(2);
                                }

                                $scope.batchList[i].saleProductName = $scope.batchList[i].product.name; //Added by Sarubala on 07-09-17 to save product name
                                $scope.batchList[i].transactionType = $scope.selectedBatch.transactionType;
                                $scope.sales.salesItem.push($scope.batchList[i]);
                            }
                        }
                    } else {
                        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                            if ($scope.sales.salesItem[i].editId == $scope.batchList[i].editId) {
                                $scope.sales.salesItem[i] = $scope.batchList[i];
                                $scope.sales.salesItem[i].originalQty = $scope.batchList[i].orginalQty;
                                if (!angular.isUndefinedOrNull($scope.salesEditId) && !angular.isUndefinedOrNull($scope.sales.salesItem[i].Action))
                                    $scope.sales.salesItem[i].Action = "U";
                                break;
                            }
                        }
                    }
                }
                $scope.salesItems.$setPristine();
                $scope.selectedProduct = null;
                $scope.selectedBatch = { "reminderFrequency": "0", "purchasePrice": 0 };
                $scope.selectedBatch.transactionType = "sales";
                $scope.selectedBatch.salesReturn = false;
                $scope.selectedBatch.canEnableTaxEdit = false;
                $scope.batchList = {};
                $scope.highlight = "";
                ChangedDicount = 0;
                ChangedMrp = 0;
                //$scope.returnItemCount = 0;
                //$scope.returnDiscountInValid = false;
            } else {
                if ($scope.selectedBatch.editId == null) {
                    $scope.selectedBatch.editId = $scope.editId++;
                    addedItem = getAddedItems($scope.selectedBatch.productStockId, $scope.selectedBatch.sellingPrice, $scope.selectedBatch.discount);
                    if (addedItem != null && (addedItem.transactionType == $scope.selectedBatch.transactionType)) {
                        addedItem.quantity = parseInt(addedItem.quantity) + parseInt($scope.selectedBatch.quantity);
                    } else {
                        $scope.selectedBatch.saleProductName = $scope.selectedBatch.product.name;//Added by Sarubala on 07-09-17 to save product name
                        $scope.sales.salesItem.push($scope.selectedBatch);
                    }
                } else {
                    for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                        if ($scope.sales.salesItem[i].editId == $scope.selectedBatch.editId) {
                            $scope.sales.salesItem[i] = $scope.selectedBatch;
                            $scope.sales.salesItem[i].originalQty = $scope.selectedBatch.orginalQty;
                            if (!angular.isUndefinedOrNull($scope.salesEditId) && !angular.isUndefinedOrNull($scope.sales.salesItem[i].Action))
                                $scope.sales.salesItem[i].Action = "U";
                            break;
                        }
                    }
                }
                $scope.salesItems.$setPristine();
                $scope.selectedProduct = null;
                $scope.selectedBatch = { "reminderFrequency": "0", "purchasePrice": 0 };
                $scope.selectedBatch.transactionType = "sales";
                $scope.selectedBatch.salesReturn = false;
                $scope.selectedBatch.canEnableTaxEdit = false;
                //$scope.returnItemCount = 0;
                //$scope.returnDiscountInValid = false;
                $scope.batchList = {};
                $scope.highlight = "";
                var drugName = document.getElementById('drugName');
                drugName.disabled = false;
            }
            //Discount Length Code
            var disclength = 0;
            for (var k = 0; k < $scope.sales.salesItem.length; k++) {
                if ($scope.sales.salesItem[k].discount == 0) {
                    disclength++;
                }
            }
            if (disclength == $scope.sales.salesItem.length) {
                $scope.discountInValid = false;
            }
            if ($scope.sales.credit != 0 || $scope.sales.credit != undefined) {
                if ($scope.sales.credit > $scope.sales.total) {
                    $scope.Iscredit = true;
                } else {
                    $scope.Iscredit = false;
                }
            }
            //Doctor And Schedule Code
            $scope.isdoctornameMandatory = false;
            $scope.iscustomerNameMandatory = false;
            $scope.schedulecount = 0;
            if ($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) {
                var schedulecount = 0;
                for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                    if ($scope.DoctorNameMandatory == 3) {
                        schedulecount = 1;
                    } else {
                        if ($scope.sales.salesItem[i].product.schedule == "H" || $scope.sales.salesItem[i].product.schedule == "H1" || $scope.sales.salesItem[i].product.schedule == "X") {
                            schedulecount++;
                        }
                    }
                }
                $scope.schedulecount = schedulecount;
                if ($scope.schedulecount > 0) {
                    $scope.isdoctornameMandatory = true;
                    $scope.iscustomerNameMandatory = true;
                    var custname = document.getElementById("customerName").value.trim();
                    if ($scope.customerHelper.data.selectedCustomer.name == undefined || $scope.customerHelper.data.selectedCustomer.name == null) {
                        $scope.customerHelper.data.selectedCustomer.name = "";
                    }
                    if (custname != "" || $scope.customerHelper.data.selectedCustomer.name != "") {
                        $scope.iscustomerNameMandatory = false;
                    }
                    var docname = document.getElementById("doctorName").value;
                    if (docname != "") {
                        $scope.isdoctornameMandatory = false;
                    }
                }
            }
            $scope.isSalesTypeMandatory = false;
            if ($scope.SalesTypeMandatory == 1) {
                $scope.isSalesTypeMandatory = true;
            }
            if ($scope.LeadProductIndex == undefined || $scope.LeadProductIndex == null) {
                $scope.LeadProductIndex = "";
            }
            if ($scope.LeadProductIndex == 0) {
                $scope.showproductSelected[0] = true;
            }
            if ($scope.LeadProductIndex != "") {
                var ind = $scope.LeadProductIndex;
                $scope.showproductSelected[ind] = true;
            }
            resetFocus();
            LeadsRequiredquantity = "";
            $scope.BillCalculations();
            getDiscountRules();
            $scope.changeRedeemValue($scope.sales.redeemPts);
            $scope.salesReturnCount = 0;
            $scope.salesCount = 0;
            for (var j = 0; j < $scope.sales.salesItem.length; j++) {
                if ($scope.sales.salesItem[j].salesReturn) {
                    $scope.salesReturnCount++;
                } else {
                    $scope.salesCount++;
                }
            }
            $scope.changeCashType();
            if ($scope.sales.cashType == "Credit") {
                $scope.focusCreditTextBox = false;
                if ($scope.sales.credit != "") {
                    if ($scope.sales.credit > $scope.FinalNetAmount) {
                        $scope.Iscredit = true;
                    } else {
                        $scope.Iscredit = false;
                    }
                }
                document.getElementById("drugName").focus();
            }
        }
        // need to copy data arun
        if ($scope.isProductGenericSearch == true) {
            $scope.focusProductSearch = true;
            $scope.focusGenericSearch = false;
            var ele = document.getElementById("drugName");
            ele.focus();
        } else {
            $scope.focusProductSearch = false;
            $scope.focusGenericSearch = true;
            var ele = document.getElementById("genericName");
            ele.focus();
        }
    };
    $scope.schedulecount = 0;
    $scope.removeSales = function (item) {

        if ($scope.Editsale == true) {
            if (item.salesOrderEstimateId != null && item.salesOrderProductSelected == 1) {
                toastr.info("Cannot delete order to sales converted items from sales bill");
                return;
            }
        }

        if (!confirm("Are you sure, Do you want to delete ? "))
            return;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            $scope.sales.total += ($scope.sales.salesItem[k].sellingPrice * $scope.sales.salesItem[k].quantity);
        }
        $scope.sales.total -= (item.sellingPrice * item.quantity);
        var index = $scope.sales.salesItem.indexOf(item);
        $scope.sales.salesItem.splice(index, 1);
        $scope.discountInValid = false;
        $scope.schedulecount = 0;
        var schedulecount = 0;
        if ($scope.sales.salesItem.length > 0) {
            for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                if (($scope.sales.salesItem[i].discount > 0) && ($scope.sales.salesItem[i].discount <= 100)) {
                    $scope.discountInValid = true;
                }
                if ($scope.DoctorNameMandatory == 1) {
                    if ($scope.sales.salesItem[i].product.schedule == "H" || $scope.sales.salesItem[i].product.schedule == "H1" || $scope.sales.salesItem[i].product.schedule == "X") {
                        schedulecount++;
                    }
                }
                if ($scope.DoctorNameMandatory == 3) {
                    schedulecount = 1;
                }
            }
            $scope.schedulecount = schedulecount;
            var custname = document.getElementById("customerName").value.trim();
            var docname = document.getElementById("doctorName").value;
            if (custname == "" && $scope.schedulecount > 0) {
                $scope.iscustomerNameMandatory = true;
            } else {
                $scope.iscustomerNameMandatory = false;
                $scope.sales.redeemPts = "";
                $scope.sales.redeemAmt = "0.00";
                $scope.sales.redeemPercent = "";
            }
            if (docname == "" && $scope.schedulecount > 0) {
                $scope.isdoctornameMandatory = true;
            }
            else {
                $scope.isdoctornameMandatory = false;
            }
        } else {
            $scope.iscustomerNameMandatory = false;
            $scope.isdoctornameMandatory = false;
        }
        if (index == 0) {
            $scope.sales.redeemPts = "";
            $scope.sales.redeemAmt = "0.00";
            $scope.sales.redeemPercent = "";
        }
        //  getDiscountRules();
        $scope.BillCalculations();
        getDiscountRules(); // Added by Sarubala on 04-07-17
        $scope.sales.discount = "";
        $scope.sales.discountValue = "";
        $scope.selectedProduct = null;
        var drugName = document.getElementById('drugName'); //Added by Sarubala on 04-08-17
        drugName.disabled = false;
        $scope.selectedBatch = { "reminderFrequency": "0", "purchasePrice": 0 };
        $scope.selectedBatch.transactionType = "sales";
        $scope.selectedBatch.salesReturn = false;
        $scope.selectedBatch.canEnableTaxEdit = false;
        $scope.batchList = {};
        var rcount = 0;
        $scope.salesReturnCount = 0;
        $scope.salesCount = 0;
        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            if ($scope.sales.salesItem[j].salesReturn) {
                rcount++;
                $scope.salesReturnCount++;
            } else {
                $scope.salesCount++;
            }
        }
        if (rcount > 0) {
            $scope.returnDiscountInValid = true;
        } else {
            $scope.returnDiscountInValid = false;
        }
        //Added by Settu on 04/07/17 for auto assign credit amount
        $scope.changeCashType();
        $scope.focusCreditTextBox = false;
        document.getElementById('drugName').focus();
    };
    $scope.removeDepartmentSales = function () {
        if (!confirm("Are you sure, Do you want to delete ? "))
            return;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            $scope.sales.total += ($scope.sales.salesItem[k].sellingPrice * $scope.sales.salesItem[k].quantity);
        }
        $scope.sales.salesItem = [];
        //$scope.sales.salesItem.splice(index, 1);
        $scope.discountInValid = false;
        $scope.schedulecount = 0;
        var schedulecount = 0;
        if ($scope.sales.salesItem.length > 0) {
            for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                if (($scope.sales.salesItem[i].discount > 0) && ($scope.sales.salesItem[i].discount <= 100)) {
                    $scope.discountInValid = true;
                }
                if ($scope.DoctorNameMandatory == 1) {
                    if ($scope.sales.salesItem[i].product.schedule == "H" || $scope.sales.salesItem[i].product.schedule == "H1" || $scope.sales.salesItem[i].product.schedule == "X") {
                        schedulecount++;
                    }
                }
                if ($scope.DoctorNameMandatory == 3) {
                    schedulecount = 1;
                }
            }
            $scope.schedulecount = schedulecount;
            var custname = document.getElementById("customerName").value.trim();
            var docname = document.getElementById("doctorName").value;
            if (custname == "" && $scope.schedulecount > 0) {
                $scope.iscustomerNameMandatory = true;
            } else {
                $scope.iscustomerNameMandatory = false;
                $scope.sales.redeemPts = "";
                $scope.sales.redeemAmt = "0.00";
                $scope.sales.redeemPercent = "";
            }
            if (docname == "" && $scope.schedulecount > 0) {
                $scope.isdoctornameMandatory = true;
            } else {
                $scope.isdoctornameMandatory = false;
            }
        } else {
            $scope.iscustomerNameMandatory = false;
            $scope.isdoctornameMandatory = false;
        }

        if ($scope.sales.salesItem.length == 0) {
            $scope.sales.redeemPts = "";
            $scope.sales.redeemAmt = "0.00";
            $scope.sales.redeemPercent = "";
        }

        $scope.BillCalculations();
        $scope.sales.discount = "";
        $scope.sales.discountValue = "";
        var rcount = 0;
        $scope.salesReturnCount = 0;
        $scope.salesCount = 0;
        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            if ($scope.sales.salesItem[j].salesReturn) {
                rcount++;
                $scope.salesReturnCount++;
            } else {
                $scope.salesCount++;
            }
        }
        if (rcount > 0) {
            $scope.returnDiscountInValid = true;
        } else {
            $scope.returnDiscountInValid = false;
        }
    };
    $scope.addbtnenable = false;
    $scope.manualSeries = "";
    $scope.selectedSeriesItem = "";
    $scope.selectedCreditSeriesItem = "";
    $scope.editSales = function (item) {
        if (item.id == '' || item.id == null) {
            $scope.addbtnenable = false;
            $scope.Enablesalesbtn = true;
            $scope.edititem = false;
        } else {
            $scope.addbtnenable = true;
            $scope.edititem = true;
        }
        // Added 13-05-2017
        // need to copy data arun
        $scope.isProductGenericSearch = true;
        $scope.focusProductSearch = true;
        $scope.focusGenericSearch = false;
        var drugName = document.getElementById('drugName');
        drugName.disabled = "disabled";
        if ($scope.zeroBatch) {
            $scope.zeroBatch = false;
        }
        item.quantity = Math.abs(item.quantity);
        var newObject = jQuery.extend(true, {}, item);
        $scope.selectedProduct = newObject;
        $scope.selectedBatch.salesReturn = item.salesReturn;
        if (item.salesReturnId != null && item.salesReturnId != undefined) {
            $scope.selectedBatch.salesReturnId = item.salesReturnId;
        }
        loadBatch(newObject);
        var rcount = 0;
        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            if ($scope.sales.salesItem[j].salesReturn) {
                rcount++;
                if (item.editId != $scope.sales.salesItem[j].editId && $scope.sales.salesItem[j].quantity > 0) {
                    $scope.sales.salesItem[j].quantity = $scope.sales.salesItem[j].quantity * (-1);
                }
            }
        }
        if (rcount > 0) {
            $scope.returnDiscountInValid = true;
        } else {
            $scope.returnDiscountInValid = false;
        }
        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                $scope.iscustomerNameMandatory = true;
            } else {
                $scope.iscustomerNameMandatory = false;
                $scope.flagCustomername = false;
            }
        }
        // Partial  Return quantity validation edit Sales paticular Product.Gavaskar 19-06-2017
        if ($scope.Editsale == true) {
            salesService.salesReturnDetail($scope.sales.id).then(function (response) {
                $scope.salesReturn = response.data;
                for (var x = 0; x < $scope.salesReturn.salesReturnItem.length; x++) {
                    if ($scope.salesReturn.salesReturnItem[x].productStockId == $scope.selectedBatch.productStockId) {
                        if ($scope.salesReturn.salesReturnItem[x].returnedQuantity == undefined || $scope.salesReturn.salesReturnItem[x].returnedQuantity == null || $scope.salesReturn.salesReturnItem[x].returnedQuantity == "") {
                            $scope.salesReturnedQuantity = 0;
                        } else {
                            $scope.salesReturnedQuantity = $scope.salesReturn.salesReturnItem[x].returnedQuantity;
                        }
                    }
                }
            }, function () {
            });
        }
    };

    $scope.multiplePaymentPopup = function () {
        if ($scope.FinalNetAmount > 0) {

            $scope.enablePopup = true;

            var m = ModalService.showModal({
                "controller": "salesPaymentCtrl",
                "templateUrl": 'paymentPopup',
                "inputs": {
                    "pendingAmt": $scope.FinalNetAmount,
                    "customerBalanceAmt": $scope.customerHelper.data.customerBalance,
                    "salesPaymentDetails": $scope.sales.salesPayments
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.enablePopup = false;
                    $scope.multiplePayment = result;
                    if (result != null) {
                        $scope.sales.salesPayments = $scope.multiplePayment.salesPayments;

                        if (!$scope.enableBillPrint) {
                            $scope.enableBillPrint = result.printStatus;
                        }
                        $scope.saveAfterConfirm();
                    }
                    else {
                        drugName = document.getElementById("drugName");
                        drugName.focus();
                        return false;
                    }

                });
            });
            return false;
        }
    }

    $scope.saveAfterConfirm = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        $scope.discountInValid = false;
        //Add patient information
        if (customerHelper.data.isCustomerSelected) {
            //if ($scope.PatientNameSearch != 1) {
            $scope.sales.patientId = customerHelper.data.selectedCustomer.id;
            //}
            //else {
            //    $scope.sales.patientId = "";
            //}
            //Added by Sarubala for Return only in Sales
            if ($scope.salesAmount == 0 && $scope.returnAmount > 0) {
                $scope.sales.patientId = customerHelper.data.selectedCustomer.id;
            }
            //End by Sarubala for Return only in Sales
            $scope.sales.name = customerHelper.data.selectedCustomer.name;
            $scope.sales.mobile = customerHelper.data.selectedCustomer.mobile;
            $scope.sales.email = customerHelper.data.selectedCustomer.email;
            $scope.sales.dOB = customerHelper.data.selectedCustomer.dOB;
            $scope.sales.age = customerHelper.data.selectedCustomer.age;
            $scope.sales.gender = customerHelper.data.selectedCustomer.gender;
            $scope.sales.address = customerHelper.data.selectedCustomer.address;
            $scope.sales.empID = customerHelper.data.selectedCustomer.empID;
            $scope.sales.isValidEmail = customerHelper.data.selectedCustomer.isValidEmail;
        } else {
            if (customerHelper.data.patientSearchData.name == undefined) {
                $scope.sales.name = document.getElementById("customerName").value.trim();
            } else {
                $scope.sales.name = customerHelper.data.patientSearchData.name;
            }
            $scope.sales.mobile = customerHelper.data.patientSearchData.mobile;
            $scope.sales.empID = customerHelper.data.patientSearchData.empID;
        }
        //if ($scope.selectedDoctor == null) {
        //    //$scope.$root.$broadcast('getValue', 'doctorName');
        //    //$scope.sales.doctorName = $scope.doctor.name;
        //    $scope.sales.doctorName = $scope.doctorname;            
        //}
        //else {
        //    $scope.sales.doctorName = $scope.selectedDoctor.originalObject.name;
        //}
        if ($scope.doctorname != null && $scope.doctorname != undefined) {
            if (typeof ($scope.doctorname) == "object") {
                $scope.sales.doctorName = $scope.doctorname.name;
            } else {
                $scope.sales.doctorName = $scope.doctorname;
            }
        }
        if ($scope.shortDoctorname != null && $scope.shortDoctorname != undefined) {
            if (typeof ($scope.shortDoctorname) == "object") {
                $scope.sales.doctorName = $scope.doctorname.name;
            } else {
                $scope.sales.doctorName = $scope.doctorname;
            }
        }
        if ($scope.sales.cashType == "Credit") {
            $scope.sales.paymentType = "Credit";
        }
        if ($scope.selectedBillDate == "" || $scope.selectedBillDate == undefined || $scope.selectedBillDate == null) {
            $scope.currentdate = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
            $scope.selectedBillDate = $scope.currentdate;
        }
        //$scope.sales.autoSavecust = false;
        //if ($scope.AutosaveCustomer == 1) {
        var custname = document.getElementById("customerName").value.trim();
        //    if ($scope.customerHelper.data.selectedCustomer.name == undefined || $scope.customerHelper.data.selectedCustomer.name == null) {
        //        $scope.customerHelper.data.selectedCustomer.name = "";
        //    }
        if ((custname != "" || $scope.customerHelper.data.selectedCustomer.name != "") && $scope.customerHelper.data.selectedCustomer.id == undefined) {
            $scope.sales.autoSavecust = true;
        }
        //}
        //Code added by Sarubala to capture Edit log - start
        if ($scope.salesEditId != null && $scope.salesEditId != undefined && $scope.salesEditId != "") {
            for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                if ($scope.sales.salesItem[i].Action != "U" && $scope.sales.salesItem[i].Action != "I") {
                    $scope.sales.salesItem[i].Action = "N";
                }
            }
        }

        //Code added by Sarubala to capture Edit log - end
        if ($scope.InvoiceSeriestype == "" || $scope.InvoiceSeriestype == undefined) {
            $scope.InvoiceSeriestype = 1;
        }
        if ($scope.InvoiceSeriestype == 1) {
            if ($scope.Editsale == false) {
                $scope.sales.InvoiceSeries = "";
            }
            Savesale();
        }
        if ($scope.InvoiceSeriestype == 2) {
            if ($scope.selectedSeriesItem == "" || $scope.selectedSeriesItem == undefined) {
                toastr.error("Select Custom Series");
                $.LoadingOverlay("hide");
                return false;
            } else {
                $scope.sales.InvoiceSeries = $scope.selectedSeriesItem;
                $scope.IsSelectedInvoiceSeries = false;
                salesService.saveCustomseries($scope.selectedSeriesItem).then(function (response) {
                    // $scope.isProcessing = false;
                    //  $.LoadingOverlay("hide");
                }, function () {
                    $.LoadingOverlay("hide");
                    $scope.isProcessing = false;
                });
            }
            Savesale();
        }
        if ($scope.InvoiceSeriestype == 3) {
            var val1 = document.getElementById("ManualSeries").value;
            if ($scope.manualSeries == "" && val1 != "") {
                $scope.manualSeries = val1;
            }
            if ($scope.manualSeries == "") {
                toastr.error("Enter Manual Series");
                $.LoadingOverlay("hide");
                var Manual = document.getElementById("ManualSeries");
                Manual.focus();
                return false;
            } else {
                $scope.sales.InvoiceSeries = "MAN";
                $scope.sales.InvoiceNo = $scope.manualSeries;
                var sdate = $filter("date")($scope.selectedBillDate, "dd/MM/yyyy");
                salesService.IsInvoiceManualSeriesAvail($scope.manualSeries, sdate)
                  .then(function (resp) {
                      bReturn = resp.data;
                      if (bReturn) {
                          //toastr.error("Invoice No. " + $scope.manualSeries + " already Exists");
                          toastr.error("Invoice No. already Exists");
                          var Manual = document.getElementById("ManualSeries");
                          Manual.focus();
                          $scope.IsSelectedInvoiceSeries = true;
                          $.LoadingOverlay("hide");
                          return false;
                      } else {
                          $scope.IsSelectedInvoiceSeries = false;
                          Savesale();
                      }
                  }, function (response) {
                      $scope.responses = response;
                  });
            }
        }
        if ($scope.InvoiceSeriestype == 4) {
            //if ($scope.selectedCreditSeriesItem == "" || $scope.selectedCreditSeriesItem == undefined) {
            //    toastr.error("Select Customer Credit Series");
            //    $.LoadingOverlay("hide");
            //    return false;
            //} else {
            //    $scope.sales.InvoiceSeries = "CRE";
            //    $scope.IsSelectedInvoiceSeries = false;
            //    salesService.saveCustomseries($scope.selectedCreditSeriesItem).then(function (response) {
            //        // $scope.isProcessing = false;
            //        //  $.LoadingOverlay("hide");
            //    }, function () {
            //        $.LoadingOverlay("hide");
            //        $scope.isProcessing = false;
            //    });
            //}
            if ($scope.Editsale == false) {
                $scope.sales.InvoiceSeries = "CRE";
            }
            Savesale();
        }
    };
    $scope.save = function () {
        var custname = null;
        if ($scope.salesEditId == "") {
            if ($scope.sales.deliveryType == "Home Delivery" && $scope.sales.cashType == "Credit") {
                $scope.sales.doorDeliveryStatus = null;
            } else if ($scope.sales.deliveryType == "Home Delivery") {
                $scope.sales.doorDeliveryStatus = "Pending";
            } else {
                $scope.sales.doorDeliveryStatus = null;
            }
        } else {
            if ($scope.sales.deliveryType == "Counter") {
                $scope.sales.doorDeliveryStatus = null;
            } else if ($scope.sales.deliveryType == "Home Delivery" && $scope.sales.cashType == "Credit") {
                $scope.sales.doorDeliveryStatus = null;
            } else if ($scope.sales.deliveryType == "Home Delivery") {
                if ($scope.sales.doorDeliveryStatus == "undefined"
                    || $scope.sales.doorDeliveryStatus == "") {
                    $scope.sales.doorDeliveryStatus = "Pending";
                }
            }
        }
        if (($scope.salesAmount == 0 && $scope.returnAmount > 0) && (customerHelper.data.selectedCustomer.id == null || customerHelper.data.selectedCustomer.id == undefined || customerHelper.data.selectedCustomer.id == '')) {
            toastr.info("Enter customer name");
            return;
        }
        else if ($scope.sales.cashType == "Credit" && (customerHelper.data.patientSearchData.mobile == null || customerHelper.data.patientSearchData.mobile == undefined || customerHelper.data.patientSearchData.mobile == '')) { //Added by settu on 09-08-2017 for credit bills
            toastr.info("Enter customer mobile");
            return;
        }

        //Added by Sarubala on 11-06-18 for loyalty point - start

        if ($scope.FinalNetAmount < 0 && $scope.sales.redeemAmt > 0) {
            $scope.netAmtError = true;
        }

        if ($scope.loyaltyError || $scope.redeemError || $scope.netAmtError || $scope.returnRedeemError) {
            toastr.info("Redeem points error");
            return;
        }

        //Added by Sarubala on 11-06-18 for loyalty point - end

        //Added by Sarubala on 19-09-17 for Muliple payment - start
        if (($scope.sales.cashType == "MultiplePayment") && ($scope.FinalNetAmount > 0)) {
            if ((customerHelper.data.selectedCustomer.id == null || customerHelper.data.selectedCustomer.id == undefined || customerHelper.data.selectedCustomer.id == '') && (customerHelper.data.patientSearchData.name == null || customerHelper.data.patientSearchData.name == undefined || customerHelper.data.patientSearchData.name == '')) {
                toastr.info("Enter customer name");
                return;
            }
            if ((customerHelper.data.patientSearchData.mobile == null || customerHelper.data.patientSearchData.mobile == undefined || customerHelper.data.patientSearchData.mobile == '')) {
                toastr.info("Enter customer mobile");
                return;
            }
            if ((customerHelper.data.selectedCustomer.id == null || customerHelper.data.selectedCustomer.id == undefined || customerHelper.data.selectedCustomer.id == '') && (customerHelper.data.patientSearchData.mobile == null || customerHelper.data.patientSearchData.mobile == undefined || customerHelper.data.patientSearchData.mobile == '' || customerHelper.data.patientSearchData.mobile.length < 10)) {
                toastr.info("Enter correct mobile no");
                return;
            }
        }
        //Added by Sarubala on 19-09-17 for Muliple payment - end

        if (angular.element(document.getElementById("saleComplete"))[0].disabled == true)
            return;

        if ($scope.sales.cashType != "MultiplePayment") { // Added by Sarubala on 19-09-17

            if ($scope.sales.paymentType == "Card" && $scope.isFourDigit == false && $scope.sales.paymentType != "Cheque" && $scope.sales.paymentType != "Cash") {
                if ($scope.sales.cardNo == "") { //$scope.sales.cardNo == ""
                    ele = angular.element(document.getElementById("cardNo"));
                    ele.focus();
                    return;
                }
                if ($scope.sales.cardDate == "") {
                    ele = angular.element(document.getElementById("cardDate"));
                    ele.focus();
                    return;
                }
                if ($scope.sales.cardDate == null) {
                    ele = angular.element(document.getElementById("cardDate"));
                    ele.focus();
                    return;
                }
            } else if ($scope.sales.paymentType == "Card" && $scope.isFourDigit && $scope.sales.paymentType != "Cash") {
                if ($scope.sales.cardDigits == "") { //$scope.sales.cardNo == "" 
                    ele = angular.element(document.getElementById("last4digit"));
                    ele.focus();
                    return;
                }
                if ($scope.sales.cardName == "") {
                    ele = angular.element(document.getElementById("cardName"));
                    ele.focus();
                    return;
                }
            } else if ($scope.sales.paymentType == "Cheque" && $scope.sales.paymentType != "Card" && $scope.sales.paymentType != "Cash") {
                if ($scope.sales.chequeNo == "") {
                    ele = angular.element(document.getElementById("chequeNo"));
                    ele.focus();
                    return;
                }
                if ($scope.sales.chequeDate == "") {
                    ele = angular.element(document.getElementById("chequeDate"));
                    ele.focus();
                    return;
                }
                if ($scope.sales.chequeDate == null) {
                    ele = angular.element(document.getElementById("chequeDate"));
                    ele.focus();
                    return;
                }
                //var custname = document.getElementById("customerName").value;
                //var custname = document.getElementById("searchPatientMobile").value;
                //patientService.create($scope.savePatient)
                //   .then(function (response) {
                //   }
                //   );
                if ($scope.customerHelper.data.selectedCustomer.name == undefined && $scope.customerHelper.data.selectedCustomer.mobile == undefined) {
                    var custMobile = document.getElementById("searchPatientMobile").value;
                    if (custMobile == "") {
                        document.getElementById("searchPatientMobile").focus();
                        return false;
                    }
                    custname = document.getElementById("customerName").value.trim();
                    if (custname == "") {
                        document.getElementById("customerName").focus();
                        return false;
                    }
                }
            }
        }

        if ($scope.SaveProcessing == true) {
            return false;
        }
        custname = document.getElementById("customerName").value.trim();
        if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && ($scope.schedulecount > 0)) {
            if (custname == "" && $scope.Namecookie == "") {
                $scope.iscustomerNameMandatory = true;
                document.getElementById("customerName").focus();
            } else {
                $scope.iscustomerNameMandatory = false;
            }
        }
        //Added doctormandatory check 06062017
        if ($scope.iscustomerNameMandatorvary == true || (($scope.DoctorNameMandatory == 3 || $scope.DoctorNameMandatory == 1) && $scope.schedulecount > 0)) {
            custname = document.getElementById("customerName").value.trim();
            if (custname == "" && ($scope.customerHelper.data.selectedCustomer.name == undefined || $scope.customerHelper.data.selectedCustomer.name == "")) {
                document.getElementById("customerName").focus();
                return false;
            } else {
                $scope.iscustomerNameMandatory = false;
            }
            if ($scope.isShortCustomerName == true) {
                var shortCustname = document.getElementById("shortCustomerName").value.trim();
                if (shortCustname == "") {
                    document.getElementById("shortCustomerName").focus();
                    return false;
                } else {
                    $scope.iscustomerNameMandatory = false;
                }
            }
        }
        if ($scope.isdoctornameMandatory == true || (($scope.DoctorNameMandatory == 3 || $scope.DoctorNameMandatory == 1) && $scope.schedulecount > 0)) {
            var docname = document.getElementById("doctorName").value;
            if (docname == "") {
                document.getElementById("doctorName").focus();
                $scope.isdoctornameMandatory = true;
                return false;
            } else {
                $scope.isdoctornameMandatory = false;
            }
            if ($scope.isShortDoctorName == true) {
                var shortDocname = document.getElementById("shortDoctorname").value;
                if (shortDocname == "") {
                    document.getElementById("shortDoctorname").focus();
                    return false;
                } else {
                    $scope.isdoctornameMandatory = false;
                }
            }
        }
        if ($scope.isSalesTypeMandatory == true) {
            if ($scope.sales.salesType == "") {
                document.getElementById("saleType").focus();
                return false;
            } else {
                $scope.isSalesTypeMandatory = false;
            }
        }
        if ($scope.isInvoicedatevalid == true) {
            document.getElementById("txtBillDate").focus();
            return false;
        }
        //Added by Gavaskar 21-11-2017 Start
        if ($scope.maxDiscountFixed == "Yes") {
            if ($scope.Editsale != true) {
                for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                    if ($scope.sales.salesItem[i].discount > $scope.maxDisountValue) {
                        toastr.info("Discount should be less than Max discount");
                        return;
                    }
                }
            }
        }
        if ($scope.isComposite == false) {
            if ($scope.taxValuesList.length > 0) {
                for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                    var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.sales.salesItem[i].gstTotal) }, true);
                    if (taxList.length == 0) {
                        toastr.info("Please provide valid GST % and try again." + " Product Name : " + $scope.sales.salesItem[i].name);
                        return;
                    }
                }
            }
        }


        //Added by Gavaskar 21-11-2017 End
        //if ($scope.chkPrintValue) {
        salesService.getPrintStatus().then(function (response) {
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.printStatus = "Disable";
            } else {
                if (response.data.item1 != undefined) {
                    $scope.printStatus = response.data.item1;
                } else {
                    $scope.printStatus = "Disable";
                }
                $scope.printFooterNote = response.data.item2;
            }
            if ($scope.printStatus == "Enable") {
                $scope.confirmPrintPopUp = true;
                //ModalService.showModal({
                //    templateUrl: 'confirmPrint',
                //    controller: "confirmPrintCreateCtrl"                        
                //}).then(function (modal) {
                //    modal.element.modal();
                //    modal.close.then(function (result) {
                //        $scope.printMessage = result;
                //        if ($scope.printMessage == 'No') {
                //            $scope.enableBillPrint = false;
                //        }
                //        else {
                //            $scope.enableBillPrint = true;
                //        }
                //        if ($scope.confirmPrintPopUp) {
                //            $scope.confirmPrintPopUp = false;
                //            $scope.saveAfterConfirm();
                //        }
                //    });
                //});
                $.confirm({
                    title: 'Print Invoice Confirm',
                    content: 'Do you want to Print Invoice ?',
                    closeIcon: function () {
                        $scope.enableBillPrint = false;
                        if ($scope.confirmPrintPopUp) {
                            $scope.confirmPrintPopUp = false;

                            if (($scope.sales.cashType == "MultiplePayment") && ($scope.FinalNetAmount > 0)) {
                                $scope.multiplePaymentPopup();
                            }
                            else {
                                if (($scope.sales.cashType == "MultiplePayment") && ($scope.FinalNetAmount <= 0)) {
                                    $scope.sales.cashType = "Full";
                                    $scope.sales.paymentType = "Cash";
                                }
                                $scope.saveAfterConfirm();
                            }
                        }
                    },
                    buttons: {
                        yes: {
                            text: 'yes [y]',
                            btnClass: 'primary-button',
                            keys: ['enter', 'y'],
                            action: function () {
                                $scope.enableBillPrint = true;
                                if ($scope.confirmPrintPopUp) {
                                    $scope.confirmPrintPopUp = false;
                                    if (($scope.sales.cashType == "MultiplePayment") && ($scope.FinalNetAmount > 0)) {
                                        $scope.multiplePaymentPopup();
                                    }
                                    else {
                                        if (($scope.sales.cashType == "MultiplePayment") && ($scope.FinalNetAmount <= 0)) {
                                            $scope.sales.cashType = "Full";
                                            $scope.sales.paymentType = "Cash";
                                        }
                                        $scope.saveAfterConfirm();
                                    }
                                }
                            }
                        },
                        no: {
                            text: 'no [n]',
                            btnClass: 'secondary-button',
                            keys: ['n'],
                            action: function () {
                                $scope.enableBillPrint = false;
                                if ($scope.confirmPrintPopUp) {
                                    $scope.confirmPrintPopUp = false;
                                    if (($scope.sales.cashType == "MultiplePayment") && ($scope.FinalNetAmount > 0)) {
                                        $scope.multiplePaymentPopup();
                                    }
                                    else {
                                        if (($scope.sales.cashType == "MultiplePayment") && ($scope.FinalNetAmount <= 0)) {
                                            $scope.sales.cashType = "Full";
                                            $scope.sales.paymentType = "Cash";
                                        }
                                        $scope.saveAfterConfirm();
                                    }
                                }
                            }
                        }
                    }
                });
            } else {
                $scope.enableBillPrint = false;
                if (($scope.sales.cashType == "MultiplePayment") && ($scope.FinalNetAmount > 0)) {
                    $scope.multiplePaymentPopup();
                }
                else {
                    if (($scope.sales.cashType == "MultiplePayment") && ($scope.FinalNetAmount <= 0)) {
                        $scope.sales.cashType = "Full";
                        $scope.sales.paymentType = "Cash";
                    }
                    $scope.saveAfterConfirm();
                }
            }
        }, function (error) {
            console.log(error);
        });
        //}
        //else {
        //    $scope.enableBillPrint = false;
        //    $scope.saveAfterConfirm();
        //}
    };
    $("#txtBillDate").keyup(function (e) {
        if ($(this).val().length == 2 || $(this).val().length == 5) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $(this).val($(this).val() + "/");
        }
        // validatedate($(this).val());
    });
    $scope.validatedate = function () {
        var val = document.getElementById("txtBillDate").value;
        if (val.length == 10) {
            validatedate(val);
        }
    };
    $scope.isInvoicedatevalid = false;
    function validatedate(val) {
        var splits = val.split("/");
        var dt = new Date(splits[1] + "/" + splits[0] + "/" + splits[2]);
        //  if (val.length ==10) {
        //Validation for Dates
        if (dt.getDate() == splits[0] && dt.getMonth() + 1 == splits[1] && dt.getFullYear() == splits[2]) {
            $scope.isInvoicedatevalid = false;
        } else {
            $scope.isInvoicedatevalid = true;
            return;
        }
        FutureDateValidation(dt);
        //   }
    }
    function FutureDateValidation(dt) {
        var dtToday = new Date();
        var pastDate = new Date(Date.parse(dtToday.getMonth() + "/" + dtToday.getDate() + "/" + parseInt(dtToday.getFullYear() - 100)));
        if (dt < pastDate || dt >= dtToday) {
            $scope.isInvoicedatevalid = true;
        } else {
            $scope.isInvoicedatevalid = false;
        }
    }
    //Durga Modified Code on 27th January 2017
    $scope.totalAmountwithoutVat = 0;
    $scope.vatAmount = 0;
    $scope.discountedTotalNetValue = 0;
    $scope.roundoffNetAmount = 0;
    $scope.totalNetValue = 0;
    $scope.FinalNetAmount = 0;
    $scope.BillCalculations = function () {
        var sumvaluewithoutVat = 0;
        var sumvaluewithVat = 0;
        var discountAmt = 0;
        var TotalDiscountedAmount = 0;
        var vatAmt = 0;
        var totalvatAmt = 0;
        $scope.totalAmountwithoutVat = 0;
        $scope.vatAmount = 0;
        $scope.discountedTotalNetValue = 0;
        $scope.roundoffNetAmount = 0;
        $scope.totalNetValue = 0;
        $scope.FinalNetAmount = 0;
        var salesAmt = 0;
        var returnAmt = 0;
        if ($scope.sales.discount == 100)
            return 0;
        //for (var i = 0; i < $scope.sales.salesItem.length; i++) {
        //    if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100)) {
        //        discountAmt = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
        //        TotalDiscountedAmount += discountAmt;
        //        sumvaluewithVat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - discountAmt);
        //        vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + $scope.sales.salesItem[i].vat)) * 100));
        //        totalvatAmt += vatAmt;
        //        sumvaluewithoutVat = (sumvaluewithVat - totalvatAmt);
        //    }
        //}
        //added by sarubala for return with sales
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100) && ($scope.sales.salesItem[i].salesReturn != true)) {
                var vatPercent1 = 0;
                if ($scope.isGstEnabled == true) {
                    if ($scope.sales.salesItem[i].gstTotal != null && $scope.sales.salesItem[i].gstTotal != undefined) {
                        vatPercent1 = $scope.sales.salesItem[i].gstTotal;
                    } else if ($scope.sales.salesItem[i].product.gstTotal != null && $scope.sales.salesItem[i].product.gstTotal != undefined) {
                        vatPercent1 = $scope.sales.salesItem[i].product.gstTotal;
                    }

                } else {
                    vatPercent1 = $scope.sales.salesItem[i].vat;
                }
                if (isNaN(vatPercent1) == false) {
                    vatPercent1 = parseFloat(vatPercent1);
                } else {
                    vatPercent1 = 0;
                }
                discountAmt = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
                TotalDiscountedAmount += discountAmt;
                sumvaluewithVat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - discountAmt);
                vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + parseFloat(vatPercent1))) * 100));
                totalvatAmt += vatAmt;
                if ($scope.isComposite == true) {
                    sumvaluewithoutVat = sumvaluewithVat;
                }
                else {
                    sumvaluewithoutVat = (sumvaluewithVat - totalvatAmt);
                }

            }
        }
        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            if ((parseFloat($scope.sales.salesItem[j].discount) >= 0) && (parseFloat($scope.sales.salesItem[j].discount) < 100)) {
                var discountAmt1 = $scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity * parseFloat($scope.sales.salesItem[j].discount) / 100;
                if ($scope.sales.salesItem[j].salesReturn) {
                    returnAmt += (($scope.sales.salesItem[j].sellingPrice * Math.abs($scope.sales.salesItem[j].quantity)) + discountAmt1);
                } else {
                    salesAmt += (($scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity) - discountAmt1);
                }
            }
        }
        $scope.returnAmount = returnAmt;
        $scope.salesAmount = salesAmt;
        $scope.totalAmountwithoutVat = sumvaluewithoutVat;
        //$scope.totalNetValue = sumvaluewithVat; // Code comment by Gavaskar Loyalty Points 12-12-2017
        // Added by Gavaskar Loyalty Points 12-12-2017 Start
        if ($scope.sales.redeemAmt > 0) {
            $scope.totalNetValue = parseFloat(sumvaluewithVat) - parseFloat($scope.sales.redeemAmt);
        }
        else {
            $scope.totalNetValue = sumvaluewithVat;
        }
        // Added by Gavaskar Loyalty Points 12-12-2017 End
        //if ($scope.totalNetValue > 1) {
        if ($scope.returnDiscountInValid && $scope.returnAmount > 0) {
            //var amt1 = $scope.salesAmount - $scope.returnAmount; // Code comment by Gavaskar Loyalty Points 12-12-2017
            // Added by Gavaskar Loyalty Points 12-12-2017 Start
            var amt1 = 0;
            if ($scope.sales.redeemAmt > 0) {
                amt1 = parseFloat($scope.salesAmount) - parseFloat($scope.returnAmount) - parseFloat($scope.sales.redeemAmt);
            }
            else {
                amt1 = $scope.salesAmount - $scope.returnAmount;
            }
            // Added by Gavaskar Loyalty Points 12-12-2017 End
            var tempamt = 0;
            if (amt1 < 0) {
                tempamt = angular.copy(amt1);
                amt1 = amt1 * (-1);
            }
            // By San - RoundOff 15-11-2017
            if ($scope.isEnableRoundOff == true) {
                if (tempamt == 0) {
                    $scope.roundoffNetAmount = Math.round(amt1) - amt1;
                    $scope.FinalNetAmount = Math.round(amt1);
                    $scope.finalNetVAL = Math.round(amt1);
                } else {
                    $scope.roundoffNetAmount = (Math.round(amt1) - amt1) * (-1);
                    $scope.FinalNetAmount = (Math.round(amt1)) * (-1);
                    $scope.finalNetVAL = (Math.round(amt1)) * (-1);
                }
            }
            else {
                if (tempamt == 0) {
                    $scope.roundoffNetAmount = amt1 - amt1;
                    $scope.FinalNetAmount = amt1;
                    $scope.finalNetVAL = amt1;
                } else {
                    $scope.roundoffNetAmount = (amt1 - amt1) * (-1);
                    $scope.FinalNetAmount = (amt1) * (-1);
                    $scope.finalNetVAL = (amt1) * (-1);
                }
            }


        } else {
            //var number = Math.round($scope.totalNetValue * Math.pow(10, 0)) / Math.pow(10, 0);
            //if ($scope.totalNetValue - number > 0) {
            //    $scope.roundoffNetAmount = (number + Math.floor(2 * Math.round(($scope.totalNetValue - number) * Math.pow(10, (0 + 1))) / 10) / Math.pow(10, 0));
            //} else {
            //    $scope.roundoffNetAmount = number;
            //}
            if ($scope.isEnableRoundOff == true) {
                $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
                $scope.FinalNetAmount = Math.round($scope.totalNetValue);
                $scope.finalNetVAL = Math.round(sumvaluewithVat);
            }
            else {
                $scope.roundoffNetAmount = $scope.totalNetValue - $scope.totalNetValue;
                $scope.FinalNetAmount = $scope.totalNetValue;
                $scope.finalNetVAL = sumvaluewithVat;
            }

        }
        //} else {
        //    if ($scope.returnDiscountInValid && $scope.returnAmount > 0) {
        //        var amt1 = $scope.salesAmount - $scope.returnAmount;
        //        $scope.roundoffNetAmount = 0;
        //        $scope.FinalNetAmount = amt1;
        //    }
        //    else {
        //        $scope.roundoffNetAmount = 0;
        //        $scope.FinalNetAmount = $scope.totalNetValue;
        //    }
        //}
        //if ($scope.totalNetValue > 1) {
        //    $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
        //    $scope.FinalNetAmount = Math.round($scope.totalNetValue);
        //    $scope.finalNetVAL = Math.round(sumvaluewithVat);
        //} else {
        //    $scope.roundoffNetAmount = 0;
        //    $scope.FinalNetAmount = $scope.totalNetValue;
        //}

        $scope.discountedTotalNetValue = TotalDiscountedAmount;

        $scope.vatAmount = totalvatAmt;


        if ($scope.sales.discountType == "1" && $scope.sales.discount > 0) {
            // alert("1");

            $scope.sales.discountValue = $scope.sales.discount;
        } else if ($scope.sales.discountType == "2" && $scope.sales.discountValue > 0 && $scope.discountedTotalNetValue <= 0) {
            $scope.sales.discount = $scope.sales.discountValue;
            $scope.discountedTotalNetValue = $scope.sales.discountValue;
            // alert("2");
        }
        // Added by Gavaskar Loyalty Points 12-12-2017 Start
        if ($scope.Editsale == true) {
            if ($scope.sales.discountValue == "") {
                $scope.sales.discountValue = 0;
            }
            $scope.salesAmount = parseFloat(salesAmt) - parseFloat($scope.sales.redeemAmt);
            $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat($scope.sales.redeemAmt) - parseFloat($scope.returnAmount) - parseFloat($scope.sales.discountValue);
            //  $scope.FinalNetAmount = parseFloat($scope.FinalNetAmount) - parseFloat($scope.sales.redeemAmt);
            $scope.FinalNetAmount = Math.round($scope.FinalNetAmount);
        }
        // Added by Gavaskar Loyalty Points 12-12-2017 End

        setModelValues();
    };
    //end
    $scope.callDiscount = function () {
        if ($scope.sales.discountType == '1' && $scope.sales.discount > 0) {
            $scope.changeDiscount($scope.sales.discount);
        } else if ($scope.sales.discountType == '2' && $scope.sales.discountValue > 0) {
            $scope.changeDiscountValue($scope.sales.discountValue);
        }
    };
    $scope.maxDiscountExceeded = false;
    $scope.changeDiscount = function (discount) {
        $scope.sales.discountValue = discount;
        if (discount == "" || discount == "." || discount == undefined) {
            discount = 0;
        }
        //$scope.sales.discount = parseInt(discount);
        $scope.maxDiscountExceeded = false;
        if ($scope.maxDiscountFixed == 'Yes') {
            if ($scope.sales.discount > $scope.maxDisountValue) {
                $scope.maxDiscountExceeded = true;
                return false;
            }
        }
        $scope.salesItems.$valid = true;
        discountchanged();
        //if (discount == 0) {
        //    $scope.sales.discount = "";
        //}
        //Added by Settu on 04/07/17 for auto assign credit amount
        $scope.changeCashType();
        $scope.focusCreditTextBox = false;
        document.getElementById("txtDiscount").focus();
    };
    $scope.discountFocus = function (discount) {
        if ($scope.sales.discount == 0 || $scope.sales.discount == "0" || discount == undefined) {
            $scope.sales.discount = "";
        }
    };
    $scope.changeDiscountValue = function (discountValue) {
        $scope.sales.discount = discountValue;
        if (discountValue == "") {
            discountValue = 0;
        }
        //$scope.sales.discountValue = parseInt(discountValue);
        $scope.maxDiscountExceeded = false;
        //if ($scope.maxDiscountFixed == 'Yes') {
        if ($scope.sales.discountValue >= $scope.finalNetVAL) {
            $scope.maxDiscountExceeded = true;
            return false;
        }
        //}
        var tmp1 = ($scope.sales.discountValue / $scope.finalNetVAL) * 100;
        //var tmp1 = ($scope.sales.discountValue / $scope.FinalNetAmount) * 100;
        //if (parseFloat(tmp1) > $scope.maxDisountValue) {
        if ($scope.maxDiscountFixed == 'Yes') {
            if (parseFloat(tmp1) > $scope.maxDisountValue) {
                $scope.maxDiscountExceeded = true;
                return false;
            } else {
                $scope.maxDiscountExceeded = false;
            }
        }
        $scope.salesItems.$valid = true;
        discountchanged();
        //if (discountValue == 0) {
        //    $scope.sales.discountValue = "";
        //}

        //Added by Sarubala on 27/04/2020 for auto assign credit amount
        $scope.changeCashType();
        $scope.focusCreditTextBox = false;
        document.getElementById("txtDiscountValue").focus();
    };
    function discountchanged() {
        $scope.DiscountCalculations1();
        //if ($scope.discountType == "1") {
        //    $scope.DiscountCalculations1();
        //} else {
        //    $scope.DiscountCalculations2();
        //}
    }
    $scope.finalNetVAL = 0;
    //added by sarubala for return with sales
    $scope.DiscountCalculations1 = function () {
        var discountInPercent = null;
        if (($scope.sales.discount == 0 || $scope.sales.discount == "") && ($scope.sales.discountValue == 0 || $scope.sales.discountValue == 0)) {
            discountInPercent = null;
        } else if ($scope.sales.discount > 0 && $scope.sales.discountType == '1') {
            discountInPercent = true;
        } else if ($scope.sales.discountValue > 0 && $scope.sales.discountType == '2') {
            discountInPercent = false;
        }

        var afterDiscountFinalNetAmount = 0;
        var afterDisocutTotalAmountwithoutvat = 0;
        var afterDiscountVatAmount = 0;
        var sumvaluewithoutVat = 0;
        var sumvaluewithVat = 0;
        var discountAmt = 0;
        var TotalDiscountedAmount = 0;
        var vatAmt = 0;
        var totalvatAmt = 0;
        var returnAmt = 0;
        var salesAmt = 0;
        if (discountInPercent) {
            if ($scope.sales.discount > 100)
                return 0;
        }
        //for (var i = 0; i < $scope.sales.salesItem.length; i++) {
        //    if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100)) {
        //        discountAmt = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
        //        TotalDiscountedAmount += discountAmt;
        //        sumvaluewithVat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - discountAmt);
        //        vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + $scope.sales.salesItem[i].vat)) * 100));
        //        totalvatAmt += vatAmt;
        //        sumvaluewithoutVat = (sumvaluewithVat - totalvatAmt);
        //    }
        //}
        for (var i = 0; i < $scope.sales.salesItem.length; i++) {
            if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100) && ($scope.sales.salesItem[i].salesReturn != true)) {

                var vatPercent2 = 0;
                if ($scope.isGstEnabled == true) {
                    if ($scope.sales.salesItem[i].gstTotal != null && $scope.sales.salesItem[i].gstTotal != undefined) {
                        vatPercent2 = $scope.sales.salesItem[i].gstTotal;
                    } else if ($scope.sales.salesItem[i].product.gstTotal != null && $scope.sales.salesItem[i].product.gstTotal != undefined) {
                        vatPercent2 = $scope.sales.salesItem[i].product.gstTotal;
                    }
                } else {
                    vatPercent2 = $scope.sales.salesItem[i].vat;
                }

                if (isNaN(vatPercent2) == false) {
                    vatPercent2 = parseFloat(vatPercent2);
                } else {
                    vatPercent2 = 0;
                }

                discountAmt = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
                TotalDiscountedAmount += discountAmt;
                sumvaluewithVat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - discountAmt);
                vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + parseFloat(vatPercent2))) * 100));
                totalvatAmt += vatAmt;
                if ($scope.isComposite == true) {
                    sumvaluewithoutVat = sumvaluewithVat;
                }
                else {
                    sumvaluewithoutVat = (sumvaluewithVat - totalvatAmt);
                }

            }
        }
        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            if ((parseFloat($scope.sales.salesItem[j].discount) >= 0) && (parseFloat($scope.sales.salesItem[j].discount) < 100)) {
                var discountAmt1 = $scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity * parseFloat($scope.sales.salesItem[j].discount) / 100;
                if ($scope.sales.salesItem[j].salesReturn) {
                    returnAmt += (($scope.sales.salesItem[j].sellingPrice * Math.abs($scope.sales.salesItem[j].quantity)) + discountAmt1);
                } else {
                    salesAmt += (($scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity) - discountAmt1);
                }
            }
        }
        $scope.returnAmount = returnAmt;
        $scope.salesAmount = salesAmt;
        if (discountInPercent) {
            //Added By Sarubala - for return in sales
            if ($scope.returnDiscountInValid || $scope.sales.discount == null || $scope.sales.discount == undefined || $scope.sales.discount == "") {
                $scope.sales.discount = 0;
            }
            //End - for return in sales
            //$scope.totalNetValue = (sumvaluewithVat - ((parseFloat($scope.sales.discount).toFixed(2) / 100) * sumvaluewithVat)); // Code Comment by Gavaskar Loyalty Points 12-12-2017
            // Added by Gavaskar Loyalty Points 12-12-2017 Start
            if ($scope.sales.redeemAmt > 0) {
                $scope.totalNetValue = (parseFloat(sumvaluewithVat) - ((parseFloat($scope.sales.discount).toFixed(2) / 100) * parseFloat(sumvaluewithVat)) - parseFloat($scope.sales.redeemAmt));
            }
            else {
                $scope.totalNetValue = (sumvaluewithVat - ((parseFloat($scope.sales.discount).toFixed(2) / 100) * sumvaluewithVat));
            }
            // Added by Gavaskar Loyalty Points 12-12-2017 End
            //  $scope.FinalNetAmount = Math.round($scope.totalNetValue);
            $scope.vatAmount = totalvatAmt - (($scope.sales.discount / 100) * totalvatAmt);
            $scope.discountedTotalNetValue = (($scope.sales.discount / 100) * sumvaluewithVat);
            $scope.totalAmountwithoutVat = (sumvaluewithoutVat - (($scope.sales.discount / 100) * sumvaluewithoutVat));
            ////  $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
        } else {
            //Added By Sarubala - for return in sales
            if ($scope.returnDiscountInValid || $scope.sales.discountValue == null || $scope.sales.discountValue == undefined || $scope.sales.discountValue == "") {
                $scope.sales.discountValue = 0;
            }
            //End - for return in sales
            //$scope.totalNetValue = sumvaluewithVat - $scope.sales.discountValue;  // Code Comment by Gavaskar Loyalty Points 12-12-2017
            // Added by Gavaskar Loyalty Points 12-12-2017 Start
            if ($scope.sales.redeemAmt > 0) {
                $scope.totalNetValue = parseFloat(sumvaluewithVat) - parseFloat($scope.sales.discountValue) - parseFloat($scope.sales.redeemAmt);

            }
            else {
                $scope.totalNetValue = sumvaluewithVat - $scope.sales.discountValue;
            }
            // Added by Gavaskar Loyalty Points 12-12-2017 End
            //  $scope.FinalNetAmount = Math.round($scope.totalNetValue);
            var tempDiscountPercent = (sumvaluewithVat == 0) ? 0 : (parseFloat($scope.sales.discountValue).toFixed(2) / sumvaluewithVat) * 100;
            $scope.discountValueInPerc = tempDiscountPercent;
            //$scope.vatAmount = totalvatAmt - $scope.sales.discountValue;
            $scope.vatAmount = totalvatAmt - ((tempDiscountPercent / 100) * totalvatAmt);
            //$scope.discountedTotalNetValue = $scope.sales.discountValue;
            $scope.discountedTotalNetValue = TotalDiscountedAmount;
            //$scope.totalAmountwithoutVat = sumvaluewithoutVat - $scope.sales.discountValue;
            $scope.totalAmountwithoutVat = sumvaluewithoutVat - ((tempDiscountPercent / 100) * sumvaluewithoutVat);
            ////  $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
        }
        //if ($scope.totalNetValue > 1) {
        //    $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
        //    $scope.FinalNetAmount = Math.round($scope.totalNetValue);
        //} else {
        //    $scope.roundoffNetAmount = 0;
        //    $scope.FinalNetAmount = $scope.totalNetValue;
        //}
        //if ($scope.totalNetValue > 1) {
        if ($scope.returnDiscountInValid && $scope.returnAmount > 0) {
            //var amt1 = $scope.salesAmount - $scope.returnAmount; // Code Comment by Gavaskar Loyalty Points 12-12-2017 Start
            // Added by Gavaskar Loyalty Points 12-12-2017 Start
            var amt1 = 0;
            if ($scope.sales.redeemAmt > 0) {
                amt1 = parseFloat($scope.salesAmount) - parseFloat($scope.returnAmount) - parseFloat($scope.sales.redeemAmt);
            }
            else {
                amt1 = $scope.salesAmount - $scope.returnAmount;
            }
            // Added by Gavaskar Loyalty Points 12-12-2017 End
            var tempamt = 0;
            if (amt1 < 0) {
                tempamt = angular.copy(amt1);
                amt1 = amt1 * (-1);
            }
            // By San - Round Off
            if ($scope.isEnableRoundOff == true) {
                if (tempamt == 0) {
                    $scope.roundoffNetAmount = Math.round(amt1) - amt1;
                    $scope.FinalNetAmount = Math.round(amt1);
                } else {
                    $scope.roundoffNetAmount = (Math.round(amt1) - amt1) * (-1);
                    $scope.FinalNetAmount = (Math.round(amt1)) * (-1);
                }
            }
            else {
                if (tempamt == 0) {
                    $scope.roundoffNetAmount = amt1 - amt1;
                    $scope.FinalNetAmount = amt1;
                } else {
                    $scope.roundoffNetAmount = (amt1 - amt1) * (-1);
                    $scope.FinalNetAmount = (amt1) * (-1);
                }
            }



        } else {
            //var number = Math.round($scope.totalNetValue * Math.pow(10, 0)) / Math.pow(10, 0);
            //if ($scope.totalNetValue - number > 0) {
            //    $scope.roundoffNetAmount = (number + Math.floor(2 * Math.round(($scope.totalNetValue - number) * Math.pow(10, (0 + 1))) / 10) / Math.pow(10, 0));
            //} else {
            //    $scope.roundoffNetAmount = number;
            //}
            //$scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
            //$scope.FinalNetAmount = Math.round($scope.totalNetValue);
            //$scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;

            if ($scope.isEnableRoundOff == true) {

                $scope.roundoffNetAmount = Math.round(Math.round(($scope.totalNetValue * 100)) / 100) - $scope.totalNetValue;
                $scope.FinalNetAmount = Math.round(Math.round(($scope.totalNetValue * 100)) / 100);
            }
            else {
                $scope.roundoffNetAmount = $scope.totalNetValue - $scope.totalNetValue;
                $scope.FinalNetAmount = $scope.totalNetValue;
            }
        }
        //} else {
        //    if ($scope.returnDiscountInValid && $scope.returnAmount > 0) {
        //        var amt1 = $scope.salesAmount - $scope.returnAmount;
        //        $scope.roundoffNetAmount = 0;
        //        $scope.FinalNetAmount = amt1;
        //    }
        //    else {
        //        $scope.roundoffNetAmount = 0;
        //        $scope.FinalNetAmount = $scope.totalNetValue;
        //    }
        //}
        if ($scope.sales.discountType == "1" && $scope.sales.discount > 0) {
            $scope.sales.discountValue = $scope.sales.discount;
        } else if ($scope.sales.discountType == "2" && $scope.sales.discountValue > 0) {
            $scope.sales.discount = $scope.sales.discountValue;
            $scope.discountedTotalNetValue = $scope.sales.discountValue;
            if ($scope.maxDiscountFixed == 'Yes') {
                if ($scope.sales.salesType == '1') {
                    if (parseFloat($scope.sales.discount) > $scope.maxDisountValue) {
                        $scope.maxDiscountExceeded = true;
                        return false;
                    } else {
                        $scope.maxDiscountExceeded = false;
                    }
                } else if ($scope.sales.salesType == '2')
                    var tmp1 = ($scope.sales.discountValue / sumvaluewithVat) * 100;
                if (parseFloat(tmp1) > $scope.maxDisountValue) {
                    $scope.maxDiscountExceeded = true;
                    return false;
                } else {
                    $scope.maxDiscountExceeded = false;
                }
            }
        }
        if ($scope.sales.discountType == '2' && $scope.sales.discount > 0 && (($scope.sales.discountValue == 0) || ($scope.sales.discountValue == ''))) {
            $scope.sales.discountValue = ($scope.sales.discount * sumvaluewithVat) / 100;
        }
        setModelValues();
    };
    $scope.discountTypeChanged = function () {
        $scope.sales.discountValue = "";
        $scope.discountedTotalNetValue = "";
        if ($scope.sales.discountType == 1) {
            $scope.changeDiscount("");
        } else {
            $scope.changeDiscountValue("");
        }
    };
    //$scope.DiscountCalculations2 = function () {
    //    var afterDiscountFinalNetAmount = 0;
    //    var afterDisocutTotalAmountwithoutvat = 0;
    //    var afterDiscountVatAmount = 0;
    //    var sumvaluewithoutVat = 0;
    //    var sumvaluewithVat = 0;
    //    var discountAmt = 0;
    //    var TotalDiscountedAmount = 0;
    //    var vatAmt = 0;
    //    var totalvatAmt = 0;
    //    if ($scope.sales.discount > 100)
    //        return 0;
    //    for (var i = 0; i < $scope.sales.salesItem.length; i++) {
    //        if ((parseFloat($scope.sales.salesItem[i].discount) >= 0) && (parseFloat($scope.sales.salesItem[i].discount) < 100)) {
    //            discountAmt = $scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity * parseFloat($scope.sales.salesItem[i].discount) / 100;
    //            TotalDiscountedAmount += discountAmt;
    //            sumvaluewithVat += (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity) - discountAmt);
    //            vatAmt = (($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) - ((($scope.sales.salesItem[i].sellingPrice * $scope.sales.salesItem[i].quantity - discountAmt) / (100 + $scope.sales.salesItem[i].vat)) * 100));
    //            totalvatAmt += vatAmt;
    //            sumvaluewithoutVat = (sumvaluewithVat - totalvatAmt);
    //        }
    //    }
    //    $scope.totalAmountwithoutVat = (sumvaluewithoutVat - (($scope.sales.discount / 100) * sumvaluewithoutVat));
    //    $scope.totalNetValue = (sumvaluewithVat - (($scope.sales.discount / 100) * sumvaluewithoutVat));
    //    //   $scope.FinalNetAmount = Math.round($scope.totalNetValue);
    //    $scope.discountedTotalNetValue = (($scope.sales.discount / 100) * sumvaluewithoutVat);
    //    $scope.vatAmount = totalvatAmt;
    //    //  $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
    //    if ($scope.totalNetValue > 1) {
    //        $scope.roundoffNetAmount = Math.round($scope.totalNetValue) - $scope.totalNetValue;
    //        $scope.FinalNetAmount = Math.round($scope.totalNetValue);
    //    } else {
    //        $scope.roundoffNetAmount = 0;
    //        $scope.FinalNetAmount = $scope.totalNetValue;
    //    }
    //};

    $scope.disableEsc = false;
    $scope.showNonSoldItems = function (sales) {
        //console.log(JSON.stringify(sales));
        $scope.focusInput = true;
        $scope.disableEsc = true;
        cacheService.set('salesObj', sales);
        cacheService.set('EditSale', $scope.Editsale);

        //cacheService.set('salesObj', sales);
        var m = ModalService.showModal({
            "controller": "partialOrderCtrl",
            "templateUrl": 'partialOrder'
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.disableEsc = false;
                $scope.message = "You said " + result;
                if ($scope.zeroBatch) {
                    $scope.zeroBatch = false;
                }
            });
        });
        return false;
    };

    //by San Sales User popup - 13-12-2017

    function Savesale() {
        if ($scope.isEnableSalesUser == true) {
            $scope.salesManUserPopup = true;
            $scope.enablePopup = true;
            $.LoadingOverlay("hide");

            var m = ModalService.showModal({
                "controller": "salesUserValidateCtrl",
                "templateUrl": "SalesUserValidate",
            }).then(function (modal) {
                modal.element.modal();
                var ele = document.getElementById("saleComplete");
                ele.focus();
                modal.close.then(function (result1) {
                    $scope.salesManUserPopup = false;
                    if (result1 != false) {
                        $scope.enablePopup = false;
                        $scope.sales.isEnableSalesUser = $scope.isEnableSalesUser;
                        $scope.sales.createdBy = result1.id;
                        $scope.sales.updatedBy = result1.id;
                        completeSale();
                    }
                    else {
                        if ($scope.isProductGenericSearch) {
                            var ele = document.getElementById("drugName");
                            ele.focus();
                        }
                        else if ($scope.isProductGenericSearch) {
                            var ele = document.getElementById("genericName");
                            ele.focus();
                        }
                        $scope.isProcessing = false;
                        $scope.enablePopup = false;
                    }
                });
            });

        }
        else {
            completeSale();
        }
    }

    function completeSale() {
        if ($scope.Editsale == true) {
            $scope.sales.isNew = false;
        }
        if ($scope.LeadsProductSelected == true) {
            $scope.sales.leadsProductSelected = true;
        } else {
            $scope.sales.leadsProductSelected = false;
            $scope.sales.leadId = "";
        }
        $scope.sales.billPrint = $scope.chkPrintValue;
        if ($scope.SaveProcessing == false) {
            $scope.SaveProcessing = true;
            $scope.sales.roundoffNetAmount = parseFloat($scope.roundoffNetAmount.toFixed(2));
            $scope.sales.netAmount = $scope.FinalNetAmount;
            $scope.sales.vatAmount = parseFloat($scope.vatAmount.toFixed(2));
            $scope.sales.returnAmount = $scope.returnAmount;
            // Added by Gavaskar Loyalty Points 12-12-2017 Start
            if ($scope.sales.redeemAmt == undefined) {
                $scope.sales.redeemAmt = 0;

            }
            if ($scope.sales.loyaltyPts == undefined) {
                $scope.sales.loyaltyPts = 0;
            }
            if ($scope.sales.redeemPts == undefined) {
                $scope.sales.redeemPts = 0;
            }
            if ($scope.sales.redeemPercent == undefined) {
                $scope.sales.redeemPercent = 0;
            }
            if ($scope.sales.discount == '' || $scope.sales.discount == null) {
                $scope.sales.discount = 0;
            }
            if ($scope.sales.redeemAmt == '' || $scope.sales.redeemAmt == null) {
                $scope.sales.redeemAmt = 0;
            }

            if ($scope.sales.discountType == 1) {
                $scope.sales.discountValue = (parseFloat($scope.sales.discount) * parseFloat($scope.sales.netAmount) / (100 - parseFloat($scope.sales.discount)));
            }
            else {
                //  $scope.sales.discount = $scope.sales.discountValue / ($scope.sales.discountValue + $scope.sales.netAmount + $scope.sales.redeemAmt) * 100;
                $scope.sales.discount = (parseFloat($scope.sales.discountValue) / (parseFloat($scope.sales.discountValue) + parseFloat($scope.sales.netAmount) + parseFloat($scope.sales.redeemAmt))) * 100;
            }

            if ($scope.sales.discountValue == null && $scope.sales.discount == null) {
                $scope.sales.discountValue = 0;
                $scope.sales.discount = 0;
            }

            $scope.sales.discountValue = parseFloat($scope.sales.discountValue) + parseFloat($scope.sales.redeemAmt);
            $scope.sales.discount = parseFloat($scope.sales.discount) + parseFloat($scope.sales.redeemPercent);

            if ($scope.Editsale != true) {
                if ($scope.loyaltyPointSettings != null) {
                    $scope.sales.loyaltyId = $scope.loyaltyPointSettings.id;
                }
            }
            // Added by Gavaskar Loyalty Points 12-12-2017 End
            var index = -1;
            for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                $scope.sales.salesItem[i].salesItemSno = i;
            }

            salesService.create($scope.sales, $scope.SelectedFileForUpload, $scope.selectedBillDate).then(function (response) {
                var sales = response.data;
                if (sales.saveFailed && sales.errorLog != null && sales.errorLog.errorMessage == "Sent Sms or Email Failed.") {
                    toastr.error("Only " + sales.errorLog.errorMessage, 'Error');
                    $scope.isProcessing = false;
                    $scope.SaveProcessing = false;
                    sales.saveFailed = false;
                }
                if (sales.saveFailed && sales.validateStockList != null) {
                    $scope.isProcessing = false;
                    $scope.SaveProcessing = false;
                    ShowValidateStockWindow(sales.validateStockList);
                }
                else if (sales.saveFailed) {
                    toastr.error(sales.errorLog.errorMessage, 'Error');
                    $scope.isProcessing = false;
                    $scope.SaveProcessing = false;
                }
                else if (sales.isEmpID == true) {
                    toastr.error('EmpID Already Exists', 'Error');
                    $scope.isProcessing = false;
                    $scope.SaveProcessing = false;
                } else {
                    $scope.getLastSalesPrintType();
                    $scope.sales.billPrint = $scope.chkPrintValue;
                    $scope.SaveProcessing = false;
                    //if (sales.availableItems.length > 0) {
                    //    angular.forEach(sales.availableItems, function (data, index) {
                    //        var noQuantityItemIndex = GetIndexofItem($scope.sales.salesItem, data.id);
                    //        if (noQuantityItemIndex != -1) {
                    //            $scope.sales.salesItem[noQuantityItemIndex].availablestock = data.stock;
                    //        }
                    //    });
                    //    $.LoadingOverlay("hide");
                    //    toastr.info("Quantity Not Available", "", 2000);
                    //    $scope.isProcessing = false;
                    //} else {

                    //if (sales.isCreated && !sales.isPartial)
                    if (!sales.isPartial)
                        toastr.success('Sale completed successfully');
                    else {
                        $.LoadingOverlay("hide");
                        if (sales.isCreated)
                            toastr.info('Sale completed partially');
                        else {
                            toastr.error('Sale not completed');
                            //$scope.sales = sales;


                            $scope.showNonSoldItems(sales);
                            return;

                        }
                    }
                    if (sales != undefined && sales != null) {
                        if (sales.id != undefined && sales.id != null) {
                            if ($scope.enableBillPrint) {
                                printingHelper.printInvoice(sales.id);
                            } else if ($scope.printStatus != "Enable" && $scope.sales.billPrint) {
                                printingHelper.printInvoice(sales.id);
                            }
                        } else {
                            if (sales.salesReturn != undefined && sales.salesReturn != null) {

                                if ($scope.enableBillPrint && sales.salesReturn.id != undefined && sales.salesReturn.id != null) {
                                    printingHelper.printReturnInvoice(sales.salesReturn.id);
                                } else if ($scope.printStatus != "Enable" && $scope.sales.billPrint) {
                                    printingHelper.printReturnInvoice(sales.salesReturn.id);
                                }
                            }
                        }
                    }

                    if (sales.isPartial && sales.isCreated) {
                        $scope.showNonSoldItems(sales);
                        return;
                    }
                    var reloadPage = false;
                    if (!$scope.enableBillPrint && !($scope.printStatus != "Enable" && $scope.sales.billPrint)) {
                        reloadPage = true;
                    }
                    ////Added by Annadurai on 07242017 - Starts here
                    $scope.sales = sales;
                    if ($scope.SelectedFileForUpload != null) {
                        $scope.uploadFile($scope.sales);
                    }
                    //else
                    //{
                    //    $scope.salesItems.$setPristine();
                    //    $scope.sales = getSalesObject();
                    //    $scope.selectedProduct = null;
                    //    $scope.selectedBatch = {};
                    //    $scope.salesAmount = 0;
                    //    $scope.returnAmount = 0;
                    //    customerHelper.ResetCustomer();
                    //    var salesEditId = document.getElementById('salesEditId').value;
                    //    if (salesEditId != "") {
                    //        window.location = window.location.origin + "/Sales/List";
                    //    } else {
                    //        window.location.reload();
                    //    }
                    //    //$scope.resetallDetails();
                    //    //  $location.path('#/pos');
                    //    $scope.isProcessing = false;
                    //    $scope.Editsale = false;
                    //    $scope.confirmPrintPopUp = false;
                    //    $scope.printMessage = null;
                    //    $scope.iscustomerNameMandatory = false;
                    //}
                    $scope.salesItems.$setPristine();
                    $scope.sales = getSalesObject();
                    $scope.selectedProduct = null;
                    $scope.selectedBatch = {};
                    $scope.salesAmount = 0;
                    $scope.returnAmount = 0;
                    customerHelper.ResetCustomer();
                    var salesEditId = document.getElementById('salesEditId').value;
                    if (($scope.isDotMatrix != 18 && $scope.isDotMatrix != 19 && $scope.isDotMatrix != 20) || reloadPage == true) {
                        if (salesEditId != "") {
                            window.location = window.location.origin + "/Sales/List";
                        } else {
                            window.location.reload();
                        }
                    }
                    //$scope.resetallDetails();
                    //  $location.path('#/pos');
                    $scope.isProcessing = false;
                    $scope.Editsale = false;
                    $scope.confirmPrintPopUp = false;
                    $scope.printMessage = null;
                    $scope.iscustomerNameMandatory = false;

                }
                $.LoadingOverlay("hide");
                //}
            }, function () {
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
                $scope.isProcessing = false;
                $scope.SaveProcessing = false;
            });
        }
    }
    //Added by Annadurai on 07242017 - Starts here
    $scope.uploadFile = function (sales) {
        salesService.uploadFile($scope.sales.id, $scope.SelectedFileForUpload, $scope.selectedBillDate).then(function (response) {
            toastr.success('Sale completed successfully');
            $scope.salesItems.$setPristine();
            $scope.sales = getSalesObject();
            $scope.selectedProduct = null;
            $scope.selectedBatch = {};
            $scope.salesAmount = 0;
            $scope.returnAmount = 0;
            customerHelper.ResetCustomer();
            var salesEditId = document.getElementById('salesEditId').value;
            if (salesEditId != "") {
                window.location = window.location.origin + "/Sales/List";
            } else {
                window.location.reload();
            }
            //$scope.resetallDetails();
            //  $location.path('#/pos');
            $scope.isProcessing = false;
            $scope.Editsale = false;
            $scope.confirmPrintPopUp = false;
            $scope.printMessage = null;
            $scope.iscustomerNameMandatory = false;
        }, function () {
            //console.log("fail");
        });
    };
    //Added by Annadurai on 07242017 - Ends here
    //reset all calculation after sale added by DURGA 07/04/2017
    $scope.resetallDetails = function () {
        location.reload();
        //$scope.BillCalculations();
        ////$scope.sales.discount = "";
        ////$scope.sales.discountValue = "";
        //$scope.doctorname = "";
        //$scope.sales.doctorMobile = "";
        //$scope.sales.doctorName = "";
        //$scope.customerHelper.data.patientSearchData.name = "";
        //$scope.customerHelper.data.patientSearchData.mobile = "";
        //$scope.customerHelper.data.patientSearchData.empID = "";
        //if ($scope.DoctorNameMandatory == 3) {
        //    var customername = document.getElementById("customerName");
        //    customername.focus();
        //} else {
        //    var drugName = document.getElementById("drugName");
        //    drugName.focus();
        //}
        //getInvoiceSeries();
        //$scope.trackInvoiceNo();
    };
    $scope.manualSeriesChange = function (series) {
        $scope.manualSeries = series;
        $scope.IsSelectedInvoiceSeries = false;
        $scope.isProcessing = false;
    };
    // Commented by arun Confirmed with manivannan on 18.04.2017
    //$scope.ValidateManualSeries = function (series) {
    //    $scope.manualSeries = series;
    //    if ($scope.manualSeries != "") {
    //        var sdate = $filter("date")($scope.selectedBillDate, "dd/MM/yyyy");
    //        salesService.IsInvoiceManualSeriesAvail($scope.manualSeries, sdate)
    //  .then(function (resp) {
    //      bReturn = resp.data;
    //      if (bReturn) {
    //          toastr.error("Invoice No. " + $scope.manualSeries + " already Exists");
    //          var Manual = document.getElementById("ManualSeries");
    //          Manual.focus();
    //          $scope.IsSelectedInvoiceSeries = true;
    //          $.LoadingOverlay("hide");
    //          return false;
    //      } else {
    //          $scope.IsSelectedInvoiceSeries = false;
    //          $scope.isProcessing = false;
    //          //$scope.IsFormInvalid = false;
    //          //$scope.salesItems.$valid = true;
    //          return true;
    //      }
    //  }, function (response) {
    //      $scope.responses = response;
    //  });
    //    } else {
    //        toastr.error("Enter Manual Series");
    //        var Manual = document.getElementById("ManualSeries");
    //        Manual.focus();
    //        $scope.IsSelectedInvoiceSeries = true;
    //    }
    //};
    //Newly created by Manivannan on 30-Jan-2017 begins
    function getCustomInvoiceNo(customSeries) {
        var salesEditId = document.getElementById('salesEditId').value;
        if (salesEditId == "") {
            salesService.getCustomInvoiceNo(customSeries).then(function (response) {
                if (response.data != "" && response.data != null && response.data != undefined) {
                    //$scope.sales.invoiceNo = response.data;
                    var invoiceno = response.data;
                    $scope.dispInvoiceNo = customSeries + "" + invoiceno;
                    $scope.sales.invoiceNo = invoiceno;
                }
                //   $scope.trackInvoiceNo();
            }, function () {
            });
        }
    }


    //Newly created by Manivannan on 30-Jan-2017 ends
    ////Newly created by Gavaskar on 31-Mar-2017 begins
    //function getCustomCreditInvoiceNo(customSeries) {
    //    salesService.getCustomCreditInvoiceNo(customSeries).then(function (response) {
    //        //console.log("CustomInvoiceNo");
    //        //console.log(response.data);
    //        if ($scope.InvoiceSeriestype == 4) {
    //            $scope.sales.invoiceNo = response.data;
    //        }
    //        $scope.trackInvoiceNo();
    //    }, function () {
    //    });
    //}
    ////Newly created by Gavaskar on 31-Mar-2017 ends
    $scope.ValidateCustomSeries = function (series) {
        $scope.selectedSeriesItem = series;
        if ($scope.selectedSeriesItem == "" || $scope.selectedSeriesItem == undefined) {
            $scope.IsSelectedInvoiceSeries = true;
            return false;
        } else {
            $scope.IsSelectedInvoiceSeries = false;
            $scope.isProcessing = false;
            getCustomInvoiceNo(series); //Newly created for showing Invoice No
            //window.localStorage.setItem("CustomSeriesCookie", $scope.selectedSeriesItem);
            window.localStorage.setItem("SelectedCustomSeriesCookie", $scope.selectedSeriesItem);
            return true;
        }
    };

    $scope.CustomSeriesOnEnter = function (series) {
        $scope.selectedSeriesItem = series;
        //Added by Annadurai 07122017 for plus pharmacy
        if (document.getElementById("accountId").value == plusPharActId && $scope.InvoiceSeriestype == 2) {
            var eleDrugName = document.getElementById("drugName");
            eleDrugName.focus();
        }
        return true;
    };
    //$scope.ValidateCustomCreditSeries = function (series) {
    //    $scope.selectedCreditSeriesItem = series;
    //    if ($scope.selectedCreditSeriesItem == "" || $scope.selectedCreditSeriesItem == undefined) {
    //        $scope.IsSelectedInvoiceSeries = true;
    //        return false;
    //    } else {
    //        $scope.IsSelectedInvoiceSeries = false;
    //        $scope.isProcessing = false;
    //        getCustomCreditInvoiceNo(series); //Newly created for showing Invoice No
    //        return true;
    //    }
    //};
    //  getPatientSearchType();
    $scope.PatientNameSearch = "1";
    //function getPatientSearchType() {
    //    salesService.getPatientSearchType().then(function (response) {
    //        if (response.data == "" || response.data == null || response.data == undefined) {
    //            $scope.PatientNameSearch = "1";
    //        } else {
    //            if (response.data.patientSearchType != undefined) {
    //                $scope.PatientNameSearch = response.data.patientSearchType;
    //            } else {
    //                $scope.PatientNameSearch = "1";
    //            }
    //        }
    //    }, function () {
    //    });
    //}

    //Modified by Sarubala on 20-10-17
    //getDoctorSearchType();
    //$scope.DoctorNameSearch = "1";
    //function getDoctorSearchType() {
    //    salesService.getDoctorSearchType().then(function (response) {
    //        if (response.data == "" || response.data == null || response.data == undefined) {
    //            $scope.DoctorNameSearch = "1";
    //        } else {
    //            if (response.data.doctorSearchType != undefined) {
    //                $scope.DoctorNameSearch = response.data.doctorSearchType;
    //            } else {
    //                $scope.DoctorNameSearch = "1";
    //            }
    //        }
    //    }, function () {
    //    });
    //}

    //Added by Sarubala on 15-11-17
    function getAllSmsSettings() {
        salesService.getSmsSettings().then(function (response) {
            $scope.smsSettings = response.data;

            if ($scope.salesEditId != null && $scope.salesEditId != undefined && $scope.salesEditId != "") {
                if ($scope.smsSettings.isSalesEditSms == true) {
                    $scope.showSms = true;
                } else {
                    $scope.showSms = false;
                    $scope.sales.sendSms = false;
                }
            }
            else {
                if ($scope.smsSettings.isSalesCreateSms == true) {
                    $scope.showSms = true;
                } else {
                    $scope.showSms = false;
                    $scope.sales.sendSms = false;
                }
            }

        }, function (error) {
            console.log(error);
        });
    };
    getAllSmsSettings();

    //Added by Sarubala on 20-10-17
    $scope.DoctorNameSearch = "1";
    $scope.SalesTypeMandatory = "2";
    $scope.DoctorNameMandatory = "2";
    $scope.AutosaveCustomer = "2";
    $scope.IsCreditInvoiceSeries = false;
    $scope.discountType = "1";
    $scope.maxDiscountFixed = "No";
    $scope.isDepartmentsave = "0";

    function getSalesSettings() {
        salesService.getAllSalesSettings().then(function (response) {
            var drugName = null;
            $scope.scanBarcodeOption = response.data;
            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.DoctorNameSearch = "1";
                $scope.SalesTypeMandatory = "2";
                $scope.AutosaveCustomer = "2";
                $scope.IsCreditInvoiceSeries = false;
                $scope.discountType = "1";
                $scope.isDotMatrix = response.data.printerType;
                window.localStorage.setItem("IsDotMatrix", JSON.stringify($scope.isDotMatrix));
                $scope.maxDiscountFixed = "No";
                $scope.isDepartmentsave = "0";
                $scope.DoctorNameMandatory = "2";
                drugName = document.getElementById("drugName");
                drugName.focus();
            } else {
                if (response.data.doctorSearchType != undefined) {
                    $scope.DoctorNameSearch = response.data.doctorSearchType;
                } else {
                    $scope.DoctorNameSearch = "1";
                }
                if (response.data.saleTypeMandatory != undefined) {
                    $scope.SalesTypeMandatory = response.data.saleTypeMandatory;
                } else {
                    $scope.SalesTypeMandatory = "2";
                }
                if (response.data.doctorNameMandatoryType != undefined) {
                    $scope.DoctorNameMandatory = response.data.doctorNameMandatoryType;
                    if ($scope.DoctorNameMandatory == 3) {
                        if ($scope.salesEditId != null && $scope.salesEditId != '') {
                            $scope.isdoctornameMandatory = true;
                            $scope.iscustomerNameMandatory = true;
                        }

                        var customername = document.getElementById("customerName");
                        customername.focus();
                    } else {
                        drugName = document.getElementById("drugName");
                        drugName.focus();
                    }
                } else {
                    $scope.DoctorNameMandatory = "2";
                    drugName = document.getElementById("drugName");
                    drugName.focus();
                }
                if (response.data.autosaveCustomer != undefined) {
                    $scope.AutosaveCustomer = response.data.autosaveCustomer;
                } else {
                    $scope.AutosaveCustomer = "2";
                }
                if (response.data.isCreditInvoiceSeries != undefined) {
                    $scope.IsCreditInvoiceSeries = response.data.isCreditInvoiceSeries;
                } else {
                    $scope.IsCreditInvoiceSeries = false;
                }
                if (response.data.discountType != undefined) {
                    $scope.discountType = response.data.discountType;
                } else {
                    $scope.discountType = "1";
                }
                if (response.data.maxDiscountAvail != undefined) {
                    $scope.maxDiscountFixed = response.data.maxDiscountAvail;
                } else {
                    $scope.maxDiscountFixed = "No";
                }
                if (response.data.patientTypeDept != undefined) {
                    $scope.isDepartmentsave = "0";
                    if (response.data.patientTypeDept == true) {
                        $scope.isDepartmentsave = "1";
                    }
                } else {
                    $scope.isDepartmentsave = "0";
                }

                if (response.data.printerType != undefined) {
                    $scope.isDotMatrix = response.data.printerType;
                    window.localStorage.setItem("IsDotMatrix", JSON.stringify($scope.isDotMatrix));
                } else {
                    $scope.isDotMatrix = 0;
                }
                if (response.data.isEnableRoundOff == true) {
                    $scope.isEnableRoundOff = response.data.isEnableRoundOff;
                    $scope.sales.isRoundOff = true;
                } else {
                    $scope.isEnableRoundOff = false;
                    $scope.sales.isRoundOff = false;
                }
                if (response.data.isEnableSalesUser == true) {
                    $scope.isEnableSalesUser = response.data.isEnableSalesUser;

                } else {
                    $scope.isEnableSalesUser = false;
                }

            }

            if ($scope.maxDiscountFixed == "Yes") {
                getMaxDiscountValue();
            }

        }, function (error) {
            console.log(error);
        });
    }

    getSalesSettings();

    //Modified by Sarubala on 20-10-17
    //getDoctorNameMandatoryType();
    //$scope.DoctorNameMandatory = "2";
    //function getDoctorNameMandatoryType() {
    //    salesService.getDoctorNameMandatoryType().then(function (response) {
    //        var drugName = null;
    //        if (response.data == "" || response.data == null || response.data == undefined) {
    //            $scope.DoctorNameMandatory = "2";
    //            drugName = document.getElementById("drugName");
    //            drugName.focus();
    //        } else {
    //            if (response.data.doctorNameMandatoryType != undefined) {
    //                $scope.DoctorNameMandatory = response.data.doctorNameMandatoryType;
    //                if ($scope.DoctorNameMandatory == 3) {
    //                    var customername = document.getElementById("customerName");
    //                    customername.focus();
    //                } else {
    //                    drugName = document.getElementById("drugName");
    //                    drugName.focus();
    //                }
    //            } else {
    //                $scope.DoctorNameMandatory = "2";
    //                drugName = document.getElementById("drugName");
    //                drugName.focus();
    //            }
    //        }
    //    }, function () {
    //    });
    //}

    //Modified by Sarubala on 20-10-17
    //getSalestypeMandatory();
    //$scope.SalesTypeMandatory = "2";
    //function getSalestypeMandatory() {
    //    salesService.getSalestypeMandatory().then(function (response) {
    //        if (response.data == "" || response.data == null || response.data == undefined) {
    //            $scope.SalesTypeMandatory = "2";
    //        } else {
    //            if (response.data.saleTypeMandatory != undefined) {
    //                $scope.SalesTypeMandatory = response.data.saleTypeMandatory;
    //            } else {
    //                $scope.SalesTypeMandatory = "2";
    //            }
    //        }
    //    }, function () {
    //    });
    //}    

    //Modified by Sarubala on 20-10-17
    //getautoSaveisMandatory();
    //$scope.AutosaveCustomer = "2";
    //function getautoSaveisMandatory() {
    //    salesService.getautoSaveisMandatory().then(function (response) {
    //        if (response.data == "" || response.data == null || response.data == undefined) {
    //            $scope.AutosaveCustomer = "2";
    //        } else {
    //            if (response.data.autosaveCustomer != undefined) {
    //                $scope.AutosaveCustomer = response.data.autosaveCustomer;
    //            } else {
    //                $scope.AutosaveCustomer = "2";
    //            }
    //        }
    //    }, function () {
    //    });
    //}
    $scope.saletypeDropdownchnage = function (model) {
        $scope.sales.salesType = model;
        $scope.isSalesTypeMandatory = false;
    };

    //Modified by Sarubala on 20-10-17
    //getPharmacyDiscountType();
    //$scope.discountType = "1";
    //function getPharmacyDiscountType() {
    //    salesService.getPharmacyDiscountType().then(function (response) {
    //        if (response.data == "" || response.data == null || response.data == undefined) {
    //            $scope.discountType = "1";
    //        } else {
    //            if (response.data.discountType != undefined) {
    //                $scope.discountType = response.data.discountType;
    //            } else {
    //                $scope.discountType = "1";
    //            }
    //        }
    //    }, function () {
    //    });
    //}
    function getCustomSeriesSelected() {
        //Added by settu for reloading custom series if user using different machine for same branch.
        var selectedCustomSeries = window.localStorage.getItem("SelectedCustomSeriesCookie");
        if (selectedCustomSeries == null || selectedCustomSeries == "" || selectedCustomSeries == undefined) {
            salesService.getCustomSeriesSelected().then(function (response) {
                if (response.data != "" && response.data != null) {
                    if (response.data.customSeriesInvoice != undefined || response.data.customSeriesInvoice != null || response.data.customSeriesInvoice != "") {
                        $scope.selectedSeriesItem = response.data.customSeriesInvoice;
                        $scope.IsSelectedInvoiceSeries = false;
                        //Newly added by Manivannan on 30-Jan-17 
                        getCustomInvoiceNo($scope.selectedSeriesItem);
                    }
                }
            }, function () {
            });
        }
        else {
            $scope.selectedSeriesItem = selectedCustomSeries;
            $scope.IsSelectedInvoiceSeries = false;
            getCustomInvoiceNo($scope.selectedSeriesItem);
        }
    }
    function GetIndexofItem(array, productstockid) {
        var index = -1;
        for (var i = 0; i < array.length; i++) {
            if (array[i].productStockId == productstockid) {
                index = i;
            }
        }
        return index;
    }
    $scope.batchSelected = function (batch) {
        $scope.valPurchasePrice = "";
        var totalQuantity = 0;
        $scope.selectedBatch = $scope.batchList[batch];
        $scope.selectedBatch.transactionType = "sales";
        $scope.selectedBatch.salesReturn = false;

        for (var i = 0; i < $scope.batchList.length; i++) {
            totalQuantity += parseInt($scope.batchList[i].stock);
        }
        if ($scope.taxValuesList.length > 0) {
            $scope.getGstvalue = $scope.selectedBatch.gstTotal;
            var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.getGstvalue) }, true);
            if (taxList.length == 0) {
                $scope.gstMode = true;
                return;
            }
            else {
                $scope.gstMode = false;
            }
        }
        $scope.batchList = rearrangeBatchList1($scope.batchList, $scope.selectedBatch);


        //if ($scope.customerHelper.data.patientSearchData.patientType == 2) {
        //    $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.vat / 100))) * 1.1).toFixed(2);
        //}
        //for (var i = 0; i < $scope.customerHelper.data.customerList.length; i++) {
        //    if ($scope.customerHelper.data.patientSearchData.mobile == $scope.customerHelper.data.customerList[i].mobile) {
        //        if ($scope.customerHelper.data.selectedCustomer.name == $scope.customerHelper.data.customerList[i].name) {
        if ($scope.customerHelper.data.selectedCustomer.patientType == 2) {


            var vatPercent = 0;
            if ($scope.isGstEnabled == true) {
                if ($scope.selectedBatch.gstTotal != null && $scope.selectedBatch.gstTotal != undefined) {
                    vatPercent = $scope.selectedBatch.gstTotal;
                } else if ($scope.selectedBatch.product.gstTotal != null && $scope.selectedBatch.product.gstTotal != undefined) {
                    vatPercent = $scope.selectedBatch.product.gstTotal;
                }

            } else {
                vatPercent = $scope.selectedBatch.vat;
            }

            $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * (vatPercent / 100))) * 1.1).toFixed(2);
        }
        //        }
        //    }
        //}
        if ($scope.selectedBatch != undefined) {
            if ($scope.selectedBatch.totalQuantity == undefined) {
                $scope.selectedBatch.totalQuantity = "";
            } else {
                $scope.selectedBatch.totalQuantity = totalQuantity;
            }
            if ($scope.selectedBatch.availableQty != null && $scope.selectedBatch.batchNo != null) {
                $scope.selectedBatch.totalQuantity = totalQuantity;
            }

            if ($scope.discountRules.billAmountType == "productWiseDiscount") {
                $scope.selectedBatch.discount = $scope.selectedBatch.product.discount;
            }
            else if ($scope.discountRules.billAmountType == "productCustomerWiseDiscount") {
                getProductCustomerwiseDiscount();
            }
            else
                $scope.selectedBatch.discount = "";

            dayDiff($scope.selectedBatch.expireDate);
        }
    };
    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    };
    //File Upload
    $scope.SelectedFileForUpload = null;
    //File Select event 
    $scope.selectFileforUpload = function (file) {
        $scope.SelectedFileForUpload = file[0];
    };
    //Amount Calculation
    $scope.receivedAmount2 = "";
    $scope.sales.credit = "";
    //Newly Added for CurrentlyRecieved Amount by DURGA 11-04-2017
    $scope.receivedAmount = function (receivedAmount2) {
        //$scope.Iscredit = false;
        //if (receivedAmount2=="") {
        //    $scope.Iscredit = false;
        //} else {
        //    if (receivedAmount2 < $scope.FinalNetAmount) {
        //        console.log("inside")
        //        $scope.Iscredit = true;
        //    } else {
        //        $scope.Iscredit = false;
        //    }
        //}
        if (parseFloat(receivedAmount2) > 0) {
            $scope.balanceAmount = parseFloat(receivedAmount2) - $scope.FinalNetAmount + ($scope.sales.credit == "" || $scope.sales.credit == null ? 0 : parseFloat($scope.sales.credit));
        } else {
            $scope.balanceAmount = "";
        }
    };
    $scope.receivedAmount1 = function (receivedAmount2) {
        //Added by Annadurai
        $scope.chkCustomerEntered();
        //Ends here
        $scope.sales.credit = parseFloat($scope.sales.credit);
        if ($scope.sales.credit > $scope.FinalNetAmount || $scope.sales.credit == "" || $scope.sales.credit == 0) {
            $scope.Iscredit = true;
        } else {
            $scope.Iscredit = false;
        }
        if (parseFloat(receivedAmount2) > 0) {
            $scope.balanceAmount = parseFloat(receivedAmount2) - $scope.FinalNetAmount + ($scope.sales.credit == "" || $scope.sales.credit == null ? 0 : parseFloat($scope.sales.credit));
        } else {
            $scope.balanceAmount = "";
        }
    };
    $scope.iscashtypevalid = false;
    $scope.changeCashType = function () {
        $scope.iscustomerNameMandatory = false;
        $scope.iscustomerMobileMandatory = false;
        $scope.chequeCustomerRequired = false;
        if ($scope.sales.cashType == "Credit") {
            $scope.sales.paymentType = "Cash";
            $scope.sales.cardNo = "";
            $scope.sales.cardDate = "";
            $scope.sales.cardDigits = "";
            $scope.sales.cardName = "";
            $scope.focusCreditTextBox = true;
            var custname = document.getElementById("customerName").value.trim();
            var custmobile = document.getElementById("searchPatientMobile").value;
            if (custname == '' && custmobile == '') {
                if (!customerHelper.data.isCustomerSelected) {
                    $scope.iscashtypevalid = true;
                }
            } else if (custmobile == '')
            { $scope.iscashtypevalid = true; } else {
                $scope.iscashtypevalid = false;
            }
            $scope.Iscredit = true;
            //Added by Settu on 04/07/17 for auto assign credit amount
            $scope.FinalNetAmount = parseFloat($scope.FinalNetAmount).toFixed(2);
            if ($scope.FinalNetAmount > 0) {
                $scope.sales.credit = $scope.FinalNetAmount;
                $scope.receivedAmount1($scope.receivedAmount2);
            }
        }
        if ($scope.sales.cashType == "Full") {
            //Full cash type should be selected by Cash in Credit bill EDIT - Settu
            $scope.sales.paymentType = "Cash";
            $scope.sales.credit = "";
            $scope.Iscredit = false;
            $scope.iscashtypevalid = false;
            $scope.receivedAmount($scope.receivedAmount2);
        }

        if ($scope.sales.cashType == "MultiplePayment") {  //Added by Sarubala on 15/09/17

            $scope.sales.paymentType = "Multiple";
            $scope.sales.cardNo = "";
            $scope.sales.cardDate = "";
            $scope.sales.cardDigits = "";
            $scope.sales.cardName = "";
            $scope.sales.credit = "";
            $scope.Iscredit = false;
            $scope.iscashtypevalid = false;

        }

    };


    $scope.isdeliverytypevalid = false;
    $scope.changedeliveryType = function () {
        if ($scope.sales.deliveryType == "Home Delivery") {
            var custname = document.getElementById("customerName").value.trim();
            var custmobile = document.getElementById("searchPatientMobile").value;
            if (custname == '' && custmobile == '') {
                if (!customerHelper.data.isCustomerSelected) {
                    $scope.isdeliverytypevalid = true;
                }
            } else {
                $scope.isdeliverytypevalid = false;
            }
        } else {
            $scope.isdeliverytypevalid = false;
        }
    };
    $scope.cancel = function () {
        if (window.confirm('Are you sure, Do you want to cancel?')) {
            window.location = window.location.origin + window.location.pathname;
        }
        $scope.discountInValid = false;
    };
    $scope.loadSalesDiscount = function () {
        if (window.localStorage.getItem("s_discount") == null)
            return;
        $scope.salesDiscount = JSON.parse(window.localStorage.getItem("s_discount"));
    };
    $scope.isFormValid = true;
    $scope.chequeCustomerRequired = false;
    var discountFlag = 0;
    $scope.selectedBatch.purchasePrice = 0;
    $scope.collectCardInfo = function () {
        if ($scope.sales.paymentType == "Cash") {
            $scope.sales.cardNo = "";
            $scope.sales.cardDate = "";
            $scope.sales.cardDigits = "";
            $scope.sales.cardName = "";
            $scope.chequeCustomerRequired = false;
            $scope.iscustomerNameMandatory = false;
            $scope.iscustomerMobileMandatory = false;
            if ($scope.sales.cashType == "MultiplePayment") {
                $scope.sales.cashType = "Full";
                $scope.changeCashType();
            }
        }
        if ($scope.sales.paymentType == "Card") {
            $scope.sales.cashType = "Full";
            $scope.sales.cardNo = "";
            $scope.sales.cardDate = "";
            $scope.sales.cardDigits = "";
            $scope.sales.cardName = "";
            $scope.sales.credit = "";
            $scope.Iscredit = false;
            $scope.isChequeDateValid = false;
            $scope.chequeCustomerRequired = false;
            $scope.iscustomerNameMandatory = false;
            $scope.iscustomerMobileMandatory = false;
        }
        if ($scope.sales.paymentType == "Cheque") {
            $scope.sales.cashType = "Full";
            $scope.sales.cardNo = "";
            $scope.sales.cardDate = "";
            $scope.sales.cardDigits = "";
            $scope.sales.cardName = "";
            $scope.sales.credit = "";
            $scope.sales.chequeNo = "";
            $scope.sales.chequeDate = "";
            $scope.Iscredit = false;
            $scope.isCardDateValid = false;
            // $scope.IsCheckNoEntered = true;//Added by Annadurai
            var custname = document.getElementById("customerName").value.trim();
            var custmobile = document.getElementById("searchPatientMobile").value;
            if (custname == '' || custmobile == '') {//changed by Annadurai
                if (!customerHelper.data.isCustomerSelected) {
                    $scope.chequeCustomerRequired = true;
                    $scope.iscustomerNameMandatory = true;
                    $scope.iscustomerMobileMandatory = true;
                    angular.element(document.getElementById("customerName")).focus();
                }
            } else {
                $scope.chequeCustomerRequired = false;
                $scope.iscustomerNameMandatory = false;
            }
        }
    };
    //Start By Lawrence
    $scope.clkCollectCardInfo = function () {
        $scope.chequeCustomerRequired = false;
        var CardType = angular.element(document.getElementById("CardType")).val();
        if (CardType != "Last4Digits") {
            angular.element(document.getElementById("cardNo")).focus();
            $scope.isFourDigit = false;
        } else {
            angular.element(document.getElementById("last4digit")).focus();
            $scope.isFourDigit = true;
        }
    };
    $scope.clkPayModeCheque = function () {
        $scope.iscashtypevalid = false;
        $scope.chequeCustomerRequired = true;
        //$scope.IsCheckNoEntered = true;
        angular.element(document.getElementById("chequeNo")).focus();
    };
    //Added by Annadurai
    $scope.chkCustomerEntered = function () {
        var custname = document.getElementById("customerName").value.trim();
        var custmobile = document.getElementById("searchPatientMobile").value;
        $scope.iscustomerNameMandatory = false;
        $scope.iscustomerMobileMandatory = false;
        if (!$scope.customerHelper.data.isCustomerSelected) {
            if ($scope.sales.cashType == 'Credit') {
                if (custmobile == '') {
                    $scope.iscustomerMobileMandatory = true;
                    $scope.Iscredit = true;
                    $scope.sales.credit = "";
                    angular.element(document.getElementById("searchPatientMobile")).focus();
                }
                if (custname == '') {
                    $scope.iscustomerNameMandatory = true;
                    $scope.Iscredit = true;
                    $scope.sales.credit = "";
                    angular.element(document.getElementById("customerName")).focus();
                }
                if (custmobile != "" && custname != "") {
                    $scope.iscashtypevalid = false;
                }
            }
            if ($scope.sales.paymentType == 'Cheque') {
                $scope.chequeCustomerRequired = false;
                if (custmobile == '') {
                    $scope.iscustomerMobileMandatory = true;
                    $scope.chequeCustomerRequired = true;
                    $scope.sales.chequeNo = "";
                    angular.element(document.getElementById("searchPatientMobile")).focus();
                }
                if (custname == '') {
                    $scope.iscustomerNameMandatory = true;
                    $scope.chequeCustomerRequired = true;
                    $scope.sales.chequeNo = "";
                    angular.element(document.getElementById("customerName")).focus();
                }
            }
            //if($scope.sales.paymentType == 'Cheque')
            //{
            //    $scope.chequeCustomerRequired = false;
            //    if (custmobile == '')
            //    {
            //        $scope.iscustomerMobileMandatory = true;
            //        $scope.chequeCustomerRequired = true;
            //        $scope.sales.chequeNo = "";
            //        angular.element(document.getElementById("searchPatientMobile")).focus();
            //    }
            //}
        }
        if ($scope.customerHelper.data.isCustomerSelected) {
            if ($scope.sales.cashType == 'Credit') {
                if ($scope.customerHelper.data.selectedCustomer.name == "" || $scope.customerHelper.data.selectedCustomer.name == undefined) {
                    $scope.Iscredit = true;
                    $scope.sales.credit = "";
                    $scope.iscustomerNameMandatory = true;
                    $scope.iscashtypevalid = true;
                }
                if ($scope.customerHelper.data.selectedCustomer.mobile == "" || $scope.customerHelper.data.selectedCustomer.mobile == undefined) {
                    $scope.Iscredit = true;
                    $scope.sales.credit = "";
                    $scope.iscustomerMobileMandatory = true;
                    $scope.iscashtypevalid = true;
                }
            }
            if ($scope.sales.paymentType == "Cheque") {
                $scope.chequeCustomerRequired = false;
                if ($scope.customerHelper.data.selectedCustomer.name == "" || $scope.customerHelper.data.selectedCustomer.name == undefined) {
                    $scope.iscustomerNameMandatory = true;
                    $scope.sales.chequeNo = "";
                    $scope.chequeCustomerRequired = true;
                }
                if ($scope.customerHelper.data.selectedCustomer.mobile == "" || $scope.customerHelper.data.selectedCustomer.mobile == undefined) {
                    $scope.iscustomerMobileMandatory = true;
                    $scope.sales.chequeNo = "";
                    $scope.chequeCustomerRequired = true;
                }
            }
        }
    };
    //Ends here
    $scope.focusToCardName = function () {
        var elem = document.getElementById("last4digit");
        if (elem.value.length == 4) {
            $scope.errLast4digit = false;
            ele = document.getElementById("cardName");
            ele.focus();
        } else {
            $scope.errLast4digit = true;
            return;
        }
    };
    $scope.focusToCardDate = function () {
        ele = document.getElementById("cardDate");
        ele.focus();
    };
    $scope.focusToChequeDate = function () {
        //if ($scope.chequeCustomerRequired == false)
        document.getElementById("chequeDate").focus();
    };
    $scope.focusToCompleteSale = function () {
        if ($scope.isCardDateValid) {
            ele = angular.element(document.getElementById("cardDate"));
            ele.focus();
            return;
        }
        if ($scope.isChequeDateValid) {
            ele = angular.element(document.getElementById("chequeDate"));
            ele.focus();
            return;
        }
        if ($scope.sales.paymentType == "Card") {
            $scope.iscustomerNameMandatory = false;
            ele = angular.element(document.getElementById("drugName"));
            ele.focus();
            return;
        }
        if ($scope.iscustomerNameMandatory == true) {
            var custname = document.getElementById("customerName").value.trim();
            if (custname == "") {
                window.setTimeout(function () {
                    ele = angular.element(document.getElementById("customerName"));
                    ele.focus();
                    return false;
                }, 250);
            } else {
                $scope.iscustomerNameMandatory = false;
                angular.element(document.getElementById("drugName")).focus();
            }
        }
        else if ($scope.isdoctornameMandatory == true) {
            var docname = document.getElementById("doctorName").value;
            if (docname == "") {
                document.getElementById("doctorName").focus();
                return false;
            } else {
                $scope.isdoctornameMandatory = false;
            }
        }
        else {
            angular.element(document.getElementById("drugName")).focus();
        }
    };
    $scope.focusToDurgName = function () {
        ele = document.getElementById("drugName");
        ele.focus();
    };
    //End By Lawrence

    //Added by Sarubala on 08-11-17
    $scope.calculateSellingPrice = function (temp) {
        if ($scope.salesPriceSettings == 3) {
            if (parseFloat(temp.purchasePriceWithoutTax) > 0) {
                var gst = (temp.gstTotal != null && temp.gstTotal != undefined && temp.gstTotal != '') ? temp.gstTotal : 0;
                temp.sellingPrice = parseFloat((temp.purchasePriceWithoutTax * (1 + (gst / 100))).toFixed(6));
                $scope.selectedBatch.sellingPrice = temp.sellingPrice;

                if (temp.quantity != undefined || temp.quantity != 0) {
                    if (temp.sellingPrice == "." || temp.sellingPrice == 0 || temp.purchasePrice > temp.sellingPrice || temp.sellingPrice == undefined) {
                        $scope.valPurchasePrice = "Price must be greater than Purchase Price";
                        $scope.salesItems.$valid = false;
                    } else {
                        $scope.valPurchasePrice = "";
                        $scope.salesItems.$valid = true;
                    }
                }
            }
        }
    }

    $scope.validateSellingPrice = function (selectedBatch, selprice) {
        if (selprice == true) {
            $scope.selprice = true;
            $scope.addbtnenable = false;
            $scope.enableSaleBtn();
        }
        if (selectedBatch.quantity != undefined || selectedBatch.quantity != 0) {
            if (selectedBatch.sellingPrice == "." || selectedBatch.sellingPrice == 0 || selectedBatch.purchasePrice > selectedBatch.sellingPrice || selectedBatch.sellingPrice == undefined) {
                $scope.valPurchasePrice = "Price must be greater than Purchase Price";
                $scope.salesItems.$valid = false;
            } else {
                $scope.valPurchasePrice = "";
                $scope.salesItems.$valid = true;
            }
        }
        if ($scope.salesPriceSettings == 3) {   //Added by Sarubala on 08-11-17
            if (parseFloat(selectedBatch.sellingPrice) > 0) {
                var gst = (selectedBatch.gstTotal != null && selectedBatch.gstTotal != undefined && selectedBatch.gstTotal != '') ? selectedBatch.gstTotal : 0;
                selectedBatch.purchasePriceWithoutTax = parseFloat(((selectedBatch.sellingPrice * 100) / (100 + gst)).toFixed(6));
                $scope.selectedBatch.purchasePriceWithoutTax = selectedBatch.purchasePriceWithoutTax;
            }
        }
    };
    $scope.discountInValid = false;
    $scope.changediscountsale = function (discount, valdisct) {
        if (valdisct == true) {
            $scope.valdisct = true;
            $scope.addbtnenable = false;
            $scope.enableSaleBtn();
        }
        $scope.maxDiscountExceeds = false;
        if ($scope.maxDiscountFixed == 'Yes') {
            if ($scope.selectedBatch != undefined) {
                if ($scope.selectedBatch.discount > $scope.maxDisountValue) {
                    $scope.maxDiscountExceeds = true;
                    return false;
                }
            }
        }
        //if (discount == "" || discount == undefined || parseFloat(discount) > 100) {
        if (parseFloat(discount) > 100 || discount == ".") {
            $scope.salesItems.$valid = false;
        } else {
            //$scope.salesItems.$valid = true;
            $scope.validateQty(valqty);
        }
        if ((discount > 0) && (discount <= 100)) {
            $scope.discountInValid = true;
        } else if ((discount == 0) && ($scope.sales.salesItem.length == 0)) {
            $scope.discountInValid = false;
        } else if ((discount == 0) && ($scope.sales.salesItem.length > 0)) {
            for (var j = 0; j < $scope.sales.salesItem.length; j++) {
                if ($scope.sales.salesItem[j].discount > 0) {
                    $scope.discountInValid = true;
                }
            }
        }
    };
    $scope.isShortCustomerName = false;
    $scope.isShortDoctorName = false;
    //need to copy data arun
    $scope.isProductGenericSearch = true;
    $scope.keydown = shortcutHelper.salesShortcuts;
    $scope.enableTempStockPopup = false;
    $scope.SaveProcessing = false;
    $scope.completeSaleKeyPress = function () {
        var custname = "";
        var custmobile = "";
        if ($scope.returnPopup) {
            $rootScope.$emit("returnPopupEvent", {});
            $scope.returnPopup = false;
            return false;
        }
        if ($scope.iscustomerNameMandatory == true) {
            custname = document.getElementById("customerName").value.trim();
            custmobile = document.getElementById("searchPatientMobile").value;
            if (custname == '' || custmobile == '') {
                //if (custname == "") {
                document.getElementById("customerName").focus();
                return false;
            } else {
                $scope.iscustomerNameMandatory = false;
            }
            if ($scope.isShortCustomerName == true) {
                var shortCustname = document.getElementById("shortCustomerName").value;
                if (shortCustname == "") {
                    document.getElementById("shortCustomerName").focus();
                    return false;
                } else {
                    $scope.iscustomerNameMandatory = false;
                }
            }
        }
        if ($scope.isdoctornameMandatory == true) {
            var docname = document.getElementById("doctorName").value;
            if (docname == "") {
                document.getElementById("doctorName").focus();
                return false;
            } else {
                $scope.isdoctornameMandatory = false;
            }
            if ($scope.isShortDoctorName == true) {
                var shortDocname = document.getElementById("shortDoctorname").value;
                if (shortDocname == "") {
                    document.getElementById("shortDoctorname").focus();
                    return false;
                } else {
                    $scope.isdoctornameMandatory = false;
                }
            }
        }
        if ($scope.isSalesTypeMandatory == true) {
            if ($scope.sales.salesType == "") {
                document.getElementById("saleType").focus();
                return false;
            }
        }
        var valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing);
        if ($scope.sales.discountType == "1" && $scope.sales.discount > 100) {
            valid = false;
        }
        if (!valid) {
            return false;
        }
        if ($scope.sales.cashType == 'Credit' || $scope.sales.deliveryType == 'Home Delivery') {
            custname = document.getElementById("customerName").value.trim();
            custmobile = document.getElementById("searchPatientMobile").value;
            var creditAmount = document.getElementById("creditAmount").value;
            if (custname == '' && custmobile == '') {
                if (!customerHelper.data.isCustomerSelected) {
                    return false;
                }
                if (creditAmount == '') {
                    return false;
                }
            }
        }
        if ($scope.Iscredit && !($scope.FinalNetAmount < 0)) { // Added by Settu to enable credit sales return while esc press
            return false;
        }
        if ($scope.maxDiscountExceeded == true) {
            return false;
        }
        if ($scope.enableTempStockPopup == true) {
            $scope.enableTempStockPopup = false;
            return false;
        }
        if ($scope.enablePopup == true) {
            $scope.enablePopup = false;
            return false;
        }
        if ($scope.confirmPrintPopUp == true) {
            return false;
        }
        if ($scope.returnPopup == true) {
            $scope.returnPopup = false;
            return;
        }
        $scope.save();
    };
    $scope.isSaveValid = function () {
        var valid = null;
        if ($scope.InvoiceSeriestype != 1) {
            if ($scope.sales.salesItem.length == 0 || $scope.IsSelectedInvoiceSeries == false) {
                if ($scope.selectedBatch == undefined) {
                    $scope.salesItems.$valid = false;
                    valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing);
                    if ($scope.sales.discountType == "1" && $scope.sales.discount > 100) {
                        valid = false;
                    }
                    if (!valid)
                        return false;
                } else {
                    if ($scope.selectedBatch.name != null && $scope.selectedBatch.quantity != null) {
                        $scope.salesItems.$valid = true;
                    }
                    valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing);
                    if ($scope.sales.discountType == "1" && $scope.sales.discount > 100) {
                        valid = false;
                    }
                    if (!valid)
                        return false;
                }
            }
        } else {
            //   if ($scope.sales.salesItem.length == 0) {
            if ($scope.selectedBatch == undefined) {
                $scope.salesItems.$valid = false;
                valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing);
                if ($scope.sales.discountType == "1" && $scope.sales.discount > 100) {
                    valid = false;
                }
                if (!valid)
                    return false;
            } else {
                if ($scope.selectedBatch.name != null && $scope.selectedBatch.quantity != null) {
                    $scope.salesItems.$valid = true;
                }
                valid = ($scope.sales.salesItem.length > 0 && !$scope.isProcessing);
                if ($scope.sales.discountType == "1" && $scope.sales.discount > 100) {
                    valid = false;
                }
                if (!valid)
                    return false;
                //  }
            }
        }
        if ($scope.InvoiceSeriestype != 1) {
            if ($scope.IsSelectedInvoiceSeries == true) {
                return false;
            }
        }
        if ($scope.sales.cashType == 'Credit' || $scope.sales.deliveryType == 'Home Delivery') {
            var custname = document.getElementById("customerName").value.trim();
            var custmobile = document.getElementById("searchPatientMobile").value;
            //&& changed to || by Mani
            if (custname == '' || custmobile == '') {//changed by annadurai
                if (!customerHelper.data.isCustomerSelected) {
                    return false;
                }
            }
        }
        if ($scope.customerHelper.data.isCustomerDiscount == 0)
            if ($scope.sales.salesItem.length > 0 && $scope.customerHelper.data.selectedCustomer.name != undefined) {
                //$scope.setTotal1();
                //$scope.vat1();
                //$scope.salesdiscount1();
                //$scope.netTotal();
                $scope.customerHelper.data.isCustomerDiscount = 1;
            }
        if ($scope.customerHelper.data.isReset == 1) {
            //$scope.setTotal1();
            //$scope.vat1();
            //$scope.salesdiscount1();
            //$scope.netTotal();
            $scope.customerHelper.data.isReset = 0;
        }
        return true;
    };
    salesEditHelper.setScope($scope);
    $scope.sales.salesItemDiscount = 0;
    $scope.init = function (GSTEnabled) {

        if (GSTEnabled == "True") {
            $scope.GSTEnabled = true;
        } else {
            $scope.GSTEnabled = false;
        }
        salesService.getSalesType().then(function (response) {
            $scope.sales.saleType = response.data;
            if ($scope.sales.saleType.salesTypeList.length > 0) {
                if ($scope.Editsale == false) {
                    $scope.sales.salesType = "";
                }
                //$scope.sales.salesType = $scope.sales.saleType.salesTypeList[0].id;
            }
            $scope.GetDefaultDoctor();
            // document.getElementById("drugName").focus();
        }, function () {
        });
    };
    $scope.GetDefaultDoctor = function () {
        salesService.getDefaultDoctor().then(function (response) {
            if (response.data.name != null) {
                if (($scope.doctorname == null || $scope.doctorname == undefined) && ($scope.sales.doctorMobile == null || $scope.sales.doctorMobile == undefined)) {
                    $scope.doctorname = response.data.name;
                    $scope.shortDoctorname = response.data.name;
                    $scope.sales.doctorid = response.data.id;
                    $scope.sales.doctorMobile = response.data.mobile;
                }
            }
        }, function (error) {
            console.log(error);
        });
    };
    function getDiscountRules() {
        $scope.sales.salesItemDiscount = 0;
        if ($scope.sales.discount > 0)
            $scope.sales.discount = $scope.sales.discount;
        else
            $scope.sales.discount = "";
        if (!($scope.sales.discountValue > 0))
            $scope.sales.discountValue = "";
        discountFlag = 0;
        if (($scope.sales.salesItem != null) && ($scope.sales.salesItem.length > 0)) {
            var total1 = 0;
            for (var x = 0; x < $scope.sales.salesItem.length; x++) {
                total1 = total1 + ((parseFloat($scope.sales.salesItem[x].discount) / 100) * ($scope.sales.salesItem[x].sellingPrice * parseFloat($scope.sales.salesItem[x].quantity)));
            }
            $scope.sales.salesItemDiscount = total1;
        }
        var overalldisc = true;
        for (var k = 0; k < $scope.sales.salesItem.length; k++) {
            if (($scope.sales.salesItem[k].discount > 0) && ($scope.sales.salesItem[k].discount <= 100)) {
                overalldisc = false;
            }
        }
        // San Modified  10-09-2017
        if (angular.isUndefinedOrNull($scope.customerHelper.data.selectedCustomer.discount)) {
            if ($scope.customerHelper.data.patientSearchData.discount != null || $scope.customerHelper.data.patientSearchData.discount != undefined) {
                $scope.customerHelper.data.selectedCustomer.discount = $scope.customerHelper.data.patientSearchData.discount;
            }
        }



        if ($scope.customerHelper.data.selectedCustomer.name == undefined || $scope.customerHelper.data.selectedCustomer.name == null) {
            $scope.customerHelper.data.selectedCustomer.name = "";
        }

        if (overalldisc) {
            salesService.getDiscountDetail().then(function (response) {


                $scope.discountRules = response.data;

                console.log(JSON.stringify($scope.discountRules));
                if (response.data == "" || response.data == null || response.data == undefined) {
                    $scope.sales.discount = 0;
                    $scope.sales.discountValue = 0;
                    if ($scope.customerHelper.data.selectedCustomer.name != "") {
                        $scope.sales.discountType = "1"; //Added by Sarubala on 09-08-17

                        if ($scope.sales.discountType == "2") {
                            $scope.sales.discountValue = $scope.customerHelper.data.selectedCustomer.discount;

                        } else {
                            $scope.sales.discount = $scope.customerHelper.data.selectedCustomer.discount;
                            if ($scope.maxDiscountFixed == "Yes") {
                                if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue) {
                                    $scope.sales.discount = $scope.maxDisountValue;
                                }
                            }
                        }


                        discountFlag = 1;
                    }
                } else {

                    //Added by Sarubala on 04-08-17
                    var totalAmount = 0;
                    for (var k = 0; k < $scope.sales.salesItem.length; k++) {
                        totalAmount += ($scope.sales.salesItem[k].sellingPrice * $scope.sales.salesItem[k].quantity);
                    }

                    if ($scope.discountRules.billAmountType != "slabDiscount" && $scope.discountRules.billAmountType != "customerWiseDiscount" && $scope.discountRules.billAmountType != "productWiseDiscount" && $scope.discountRules.billAmountType != "productCustomerWiseDiscount") {
                        $scope.sales.discount = 0;
                        $scope.sales.discountValue = 0;
                        if (!$scope.discountRules.amount != null) {
                            if ($scope.discountRules.amount <= totalAmount) {
                                $scope.sales.discountType = "1"; //Added by Sarubala on 09-08-17
                                $scope.sales.discount = $scope.discountRules.discount;
                                discountFlag = 1;
                            } else {
                                if (discountFlag == 1) {
                                    discountFlag = 0;
                                }
                            }
                        }
                    } else if ($scope.discountRules.billAmountType == "slabDiscount") {
                        $scope.sales.discount = 0;
                        $scope.sales.discountValue = 0;
                        for (var z = 0; z < $scope.discountRules.discountItem.length; z++) {
                            if ((!$scope.discountRules.discountItem[z].amount != null) && (!$scope.discountRules.discountItem[z].toAmount != null)) {
                                if (($scope.discountRules.discountItem[z].amount <= totalAmount) && ($scope.discountRules.discountItem[z].toAmount >= totalAmount)) {
                                    $scope.sales.discountType = "1"; //Added by Sarubala on 09-08-17
                                    $scope.sales.discount = $scope.discountRules.discountItem[z].discount;
                                    discountFlag = 1;
                                } else if ($scope.discountRules.discountItem[z].toAmount < totalAmount) {
                                    $scope.sales.discountType = "1"; //Added by Sarubala on 09-08-17
                                    $scope.sales.discount = $scope.discountRules.discountItem[z].discount;
                                    discountFlag = 1;
                                }
                            }
                        }
                    } else {
                        $scope.sales.discount = 0;
                        $scope.sales.discountValue = 0;
                        if ($scope.customerHelper.data.selectedCustomer.name != "") {
                            $scope.sales.discountType = "1"; //Added by Sarubala on 09-08-17
                            $scope.sales.discount = $scope.customerHelper.data.selectedCustomer.discount;
                            if ($scope.maxDiscountFixed == "Yes") {
                                if ($scope.customerHelper.data.selectedCustomer.discount > $scope.maxDisountValue) {
                                    $scope.sales.discount = $scope.maxDisountValue;
                                }
                            }
                            discountFlag = 1;
                        }
                    }
                    //Added by Sarubala on 04-08-17
                    if ($scope.sales.discountType == '2' && $scope.sales.discount > 0 && (($scope.sales.discountValue == 0) || ($scope.sales.discountValue == ''))) {
                        $scope.sales.discountValue = ($scope.sales.discount * totalAmount) / 100;
                    }
                }
                if ($scope.sales.discount == "" || $scope.sales.discount == undefined) {
                    $scope.sales.discount = 0;
                }
                if ($scope.sales.discountValue == "" || $scope.sales.discountValue == undefined) {
                    $scope.sales.discountValue = 0;
                }
                //Changed as parameter value 1 for discount percentage, 2 for discount value
                discountchanged();
                //To auto assign credit amt if discount applied - Settu
                $scope.changeCashType();
                $scope.focusCreditTextBox = false;
            });
        } else {
            $scope.sales.discount = 0;
            $scope.sales.discountValue = 0;
        }
        if ($scope.sales.discount == 0) {
            $scope.sales.discount = "";
        }
        if ($scope.sales.discountValue == 0) {
            $scope.sales.discountValue = "";
        }
    }
    function dayDiff(expireDate) {
        $scope.highlight = "";
        var today = $filter('date')(new Date(), 'dd/MM/yyyy');
        expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date(formatString(expireDate));
        var date1 = new Date(formatString(today));
        //var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference < 30) {
            var dt = expireDate;
            $scope.highlight = "Expiry";
        } else {
            $scope.highlight = "";
        }
    }
    function formatString(format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    }
    function resetFocus() {
        var qty = document.getElementById("drugName");
        qty.focus();
    }
    $scope.focusOnProduct = function () {
        var qty = document.getElementById("drugName");
        qty.focus();
    };
    $scope.sales.discountTotal = 0;
    $scope.sales.discountVat = 0;
    $scope.keyEnter = function (event, e) {
        //console.log("enter key pressed");
        var custname = document.getElementById("customerName").value.trim();
        if (custname != '') {
            $scope.iscustomerNameMandatory = false;
        }
        if (custname == "") {
            $scope.Namecookie = "";
        }
        if ($scope.isShortCustomerName == false) {
            if (event.which === 8) {
                ////if ($scope.customerHelper.data.patientSearchData.name == "" || $scope.customerHelper.data.patientSearchData.name == undefined) {
                ////    $scope.Namecookie = "";
                ////}
            }
            ele = document.getElementById(e);
            if (event.which === 13) // Enter key
            {
                if (patientSelected == 0) {
                    ele.focus();
                    if ($scope.edititem == true) {
                        if (ele.nodeName == "BUTTON") {
                            ele.focus();
                        }
                    }
                    if (ele.nodeName != "BUTTON" && ele.nodeName != "SELECT")
                        ele.select();
                } else {
                    ele.focus();
                    if (ele.nodeName != "BUTTON" && ele.nodeName != "SELECT")
                        ele.select();
                }
                if ($scope.Namecookie != "" && $scope.Namecookie != null && $scope.Namecookie != undefined) {
                    $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
                }


                if (ele.id == 'gstTotal' && ele.disabled == true) {
                    if ($scope.salesPriceSettings == 3) {  //Added by Sarubala on 08-11-17
                        document.getElementById("PPrice").focus();
                    }
                    else {
                        document.getElementById("sellingPrice").focus();
                    }
                }
            }
            if (event.which === 9) {
                if ($scope.Namecookie != "" && $scope.Namecookie != null && $scope.Namecookie != undefined)
                    $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
            }
        } else {
            if (event.which === 8) {
                if ($scope.customerHelper.data.patientSearchData.shortName == "" || $scope.customerHelper.data.patientSearchData.shortName == undefined) {
                    $scope.ShortNamecookie = "";
                }
            }
            ele = document.getElementById(e);
            if (event.which === 13) // Enter key
            {
                if (patientSelected == 0) {
                    ele.focus();
                    if (ele.nodeName != "BUTTON")
                        ele.select();
                } else {
                    ele.focus();
                    if (ele.nodeName != "BUTTON")
                        ele.select();
                }

                if (ele.id == 'gstTotal' && ele.disabled == true) {
                    if ($scope.salesPriceSettings == 3) {  //Added by Sarubala on 08-11-17
                        document.getElementById("PPrice").focus();
                    }
                    else {
                        document.getElementById("sellingPrice").focus();
                    }
                }
            }
            if (event.which === 9) {
                if ($scope.ShortNamecookie != "" && $scope.ShortNamecookie != undefined && $scope.ShortNamecookie != null)
                    $scope.customerHelper.data.patientSearchData.shortName = $scope.ShortNamecookie;
            }
        }
    };
    $scope.loadEditSalesFocus = function () {
        if ($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) {
            if ($scope.customerHelper.data.patientSearchData.name != "" && $scope.customerHelper.data.patientSearchData.name != null) {
                document.getElementById("drugName").focus();
            } else {
                document.getElementById("customerName").focus();
            }
        } else {
            document.getElementById("drugName").focus();
        }
    };
    ////$scope.blurCustname = function () {
    ////    if ($scope.Namecookie != null) {
    ////        $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
    ////    }
    ////    if ($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) {
    ////        //if ($scope.customerHelper.data.patientSearchData.name != "" && $scope.customerHelper.data.patientSearchData.name != null) {
    ////        //    $scope.iscustomerNameMandatory = false;
    ////        //}
    ////        //else {
    ////        //    $scope.iscustomerNameMandatory = true;
    ////        //}
    ////        //if ($scope.customerHelper.data.patientSearchData.name == "" || $scope.customerHelper.data.patientSearchData.name == null || $scope.customerHelper.data.patientSearchData.name==undefined) {
    ////        //    $scope.iscustomerNameMandatory = true;
    ////        //}
    ////        //else {
    ////        //    $scope.iscustomerNameMandatory = false;
    ////        //}
    ////        var custname = document.getElementById("customerName").value;
    ////        if (custname == "") {
    ////            $scope.iscustomerNameMandatory = true;
    ////        } else {
    ////            $scope.iscustomerNameMandatory = false;
    ////       }
    ////    }
    ////    else {
    ////        $scope.iscustomerNameMandatory = false;
    ////    }
    ////    //if ($scope.iscustomerNameMandatory == true) {
    ////    //    var custname = document.getElementById("customerName").value;
    ////    //    if (custname == "") {
    ////    //        document.getElementById("customerName").focus();
    ////    //        return true;
    ////    //    } else {
    ////    //        $scope.iscustomerNameMandatory = false;
    ////    //        return true;
    ////    //    }
    ////    //}
    ////};
    $scope.blurCustname = function () {
        if ($scope.Namecookie != null) {
            if ($scope.Namecookie != "") {
                $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
            }
        }
        var custname = document.getElementById("customerName").value.trim();
        //Added by 06062017
        //if (custname == "") {
        //    $scope.Namecookie = "";
        //}
        if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && ($scope.schedulecount > 0)) {
            if (custname == "" && $scope.Namecookie == "") {
                $scope.iscustomerNameMandatory = true;
            } else {
                $scope.iscustomerNameMandatory = false;
            }
        }
    };
    $scope.doctorNamekeyEnter = function (nextid) {
        if ($scope.doctorname != null) {
            if ($scope.doctorname.name == null || $scope.doctorname.name == undefined || $scope.doctorname.name == '') {
                $scope.showAddress = true;
            } else {
                $scope.showAddress = false;
            }
        }
        ele = document.getElementById(nextid);
        ele.focus();
        var eleCustomSeries = null;
        //Added by Annadurai 0712017 for plus pharmacy - Starts
        if (document.getElementById("accountId").value == plusPharActId && $scope.showAddress == false && $scope.InvoiceSeriestype == 2 &&
        ($scope.doctorname.name != null || $scope.doctorname.name != undefined || $scope.doctorname.name != '')
             && ($scope.sales.doctorMobile != null || $scope.sales.doctorMobile != undefined || $scope.sales.doctorMobile != '')
            ) {
            eleCustomSeries = document.getElementById("ddlCustomSeries");
            eleCustomSeries.focus();
        }

        if (document.getElementById("accountId").value == plusPharActId && $scope.showAddress == undefined && $scope.InvoiceSeriestype == 2 &&
        ($scope.doctorname == null || $scope.doctorname == undefined || $scope.doctorname == '')
             && ($scope.sales.doctorMobile == null || $scope.sales.doctorMobile == undefined || $scope.sales.doctorMobile == '')
            ) {
            eleCustomSeries = document.getElementById("ddlCustomSeries");
            eleCustomSeries.focus();
        }
        //Added by Annadurai 0712017 for plus pharmacy - Ends
    };
    $scope.doctorShortNamekeyEnter = function (nextid) {
        if ($scope.shortDoctorname != null) {
            if ($scope.shortDoctorname.name == null || $scope.shortDoctorname.name == undefined || $scope.shortDoctorname.name == '') {
                $scope.showAddress = true;
            } else {
                $scope.showAddress = false;
            }
        }
        ele = document.getElementById(nextid);
        ele.focus();
    };
    $scope.changedoctorname = function (name) {
        $scope.doctorname = name;
        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                if ($scope.doctorname == "") {
                    $scope.isdoctornameMandatory = true;
                } else {
                    $scope.isdoctornameMandatory = false;
                }
            }
        }
        if ($scope.DoctorNameMandatory == 1 && $scope.isdoctornameMandatory == true) {
            if ($scope.doctorname == "") {
                $scope.isdoctornameMandatory = true;
            } else {
                $scope.isdoctornameMandatory = false;
                $scope.flagDoctorname = false;
            }
        }
        else if ($scope.DoctorNameMandatory == 1 && $scope.flagDoctorname == false) {
            if ($scope.doctorname == "") {
                $scope.isdoctornameMandatory = true;
            } else {
                $scope.isdoctornameMandatory = false;
                $scope.flagDoctorname = false;
            }
        }
        if ($scope.doctorname == "") {
            $scope.sales.doctorMobile = "";
        }
    };
    //Added by Annadurai
    $scope.changeMobilerNumber = function () {
        var custname = document.getElementById("customerName").value.trim();
        var custMobile = document.getElementById("searchPatientMobile").value;
        if ($scope.sales.paymentType == 'Cheque') {
            if (custMobile == '') {
                $scope.iscustomerMobileMandatory = true;
                $scope.chequeCustomerRequired = true;
                $scope.sales.chequeNo = "";
                angular.element(document.getElementById("searchPatientMobile")).focus();
            } else {
                $scope.iscustomerMobileMandatory = false;
            }
        }
        if ($scope.sales.cashType == 'Credit') { //|| $scope.sales.paymentType == 'Cheque') {            
            if (custMobile == '') {
                $scope.iscustomerMobileMandatory = true;
                //$scope.iscashtypevalid = true;
                $scope.sales.chequeNo = "";
                angular.element(document.getElementById("searchPatientMobile")).focus();
            } else { $scope.iscustomerMobileMandatory = false; }
            if (custMobile != "" && custname != "") {
                $scope.iscashtypevalid = false;
            } else {
                $scope.iscashtypevalid = true;
            }
        }
    };
    $scope.changeCustomerName = function () {
        var custname = document.getElementById("customerName").value.trim();
        var custMobile = document.getElementById("searchPatientMobile").value;
        ////Added by Annadurai
        //if (custname != "") {
        //    $scope.iscustomerNameMandatory = false;
        //}
        //if (custname == "") {
        //    $scope.iscustomerNameMandatory = true;
        //}
        if ($scope.sales.cashType == 'Credit') { //|| $scope.sales.paymentType == 'Cheque') {            
            if (custname == '') {
                $scope.iscustomerNameMandatory = true;
                $scope.Iscredit = true;
                $scope.sales.credit = "";
                angular.element(document.getElementById("customerName")).focus();
            } else { $scope.iscustomerNameMandatory = false; }
            if (custMobile != "" && custname != "") {
                $scope.iscashtypevalid = false;
            } else {
                $scope.iscashtypevalid = true;
            }
        }
        if ($scope.sales.paymentType == 'Cheque') {
            if (custname == '') {
                $scope.iscustomerNameMandatory = true;
                $scope.chequeCustomerRequired = true;
                $scope.sales.chequeNo = "";
                angular.element(document.getElementById("customerName")).focus();
            } else { $scope.iscustomerNameMandatory = false; }
        }
        $scope.Namecookie = custname;
        if ($scope.sales.salesItem.length > 0) {
            if (($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) && $scope.schedulecount > 0) {
                if (custname == "") {
                    $scope.iscustomerNameMandatory = true;
                } else {
                    $scope.iscustomerNameMandatory = false;
                    $scope.flagCustomername = false;
                }
            } else {
                $scope.iscustomerNameMandatory = false;
                $scope.flagCustomername = false;
            }
            // }
        }
        //if ($scope.DoctorNameMandatory == 1 && schedulecount > 0) {
        //    if (custname == "") {
        //        $scope.iscustomerNameMandatory = true;
        //    } else {
        //        $scope.iscustomerNameMandatory = false;
        //        $scope.flagCustomername = false;
        //    }
        //}
        //else if ($scope.DoctorNameMandatory == 1 && $scope.flagCustomername == false) {
        //    if (custname == "") {
        //        $scope.iscustomerNameMandatory = true;
        //    } else {
        //        $scope.iscustomerNameMandatory = false;
        //        $scope.flagCustomername = false;
        //    }
        //}
    };
    $scope.doctorMobilekeyEnter = function (nextid) {
        ele = document.getElementById(nextid);
        ele.focus();
        if ($scope.showAddress) {
            ele = document.getElementById("doctorAddress");
            ele.focus();
        }
    };
    $scope.doctorAddresskeyEnter = function (nextid) {
        ele = document.getElementById(nextid);
        ele.focus();
    };
    function getSalesObject() {
        return {
            "salesItem": [],
            "discount": "",
            "total": 0,
            "TotalQuantity": 0,
            "rackNo": "",
            "vat": 0,
            "cashType": "Full",
            "paymentType": "Cash",
            "deliveryType": "Counter",
            "billPrint": false,
            "sendEmail": false,
            "sendSms": false,
            "doctorMobile": null,
            "credit": null,
            "cardNo": null,
            "cardDate": null,
            "cardName": null,
            "cardDigits": null,
            "isCreditEdit": false,
            "changeDiscount": "",
            "salesPayments": []
        };
    }
    function loadBatch(editBatch) {
        //console.log(editBatch);
        if (!$scope.selectedBatch.salesReturn) {
            productStockService.productBatch($scope.selectedProduct.product.id).then(function (response) {
                $scope.batchList = response.data;
                $scope.batchList = $filter('orderBy')($scope.batchList, 'expireDate');
                var qty = document.getElementById("quantity");
                qty.focus();
                var totalQuantity = 0;
                var rack = "";
                var tempBatch = [];
                var tempBatchScan = [];
                var availableStock = null;
                $scope.changediscountsale("");
                for (var i = 0; i < $scope.batchList.length; i++) {
                    if ($scope.Editsale == false || editBatch == null) {
                        $scope.batchList[i].productStockId = $scope.batchList[i].id;
                        $scope.batchList[i].id = null;

                        //Added by Sarubala on 06-11-17
                        if ($scope.salesPriceSettings == 3) {
                            $scope.batchList[i].sellingPrice = parseFloat(($scope.batchList[i].purchasePriceWithoutTax * (1 + ($scope.batchList[i].gstTotal / 100))).toFixed(6));
                        }

                        availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                        if (editBatch != null && editBatch.productStockId == $scope.batchList[i].productStockId)
                            editBatch.availableQty = availableStock;
                        totalQuantity += availableStock;
                        if (availableStock == 0)
                            continue;
                        $scope.batchList[i].availableQty = availableStock;
                        if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                            rack = $scope.batchList[i].rackNo;
                        }
                        tempBatch.push($scope.batchList[i]);
                    } else {
                        if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                            rack = $scope.batchList[i].rackNo;
                        }
                    }
                    if ($scope.batchList[i].rackNo == null || $scope.batchList[i].rackNo == '' || $scope.batchList[i].rackNo == undefined) {
                        $scope.batchList[i].rackNo = '-';
                        rack = $scope.batchList[i].rackNo;
                    }
                    if ($scope.batchList[i].product.boxNo == null || $scope.batchList[i].product.boxNo == '' || $scope.batchList[i].product.boxNo == undefined) {
                        $scope.batchList[i].product.boxNo = '-';
                    }
                    if ($scope.scanBarcodeVal !== undefined && $scope.scanBarcodeVal !== "" && $scope.scanBarcodeVal !== null) {
                        if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')($scope.batchList[i].purchaseBarcode)) {
                            tempBatchScan.push($scope.batchList[i]);
                        }
                        else if ($filter('uppercase')($scope.scanBarcodeVal) === $filter('uppercase')($scope.batchList[i].eancode)) {
                            tempBatchScan.push($scope.batchList[i]);
                        }
                    }
                }
                if (editBatch != null && tempBatch.length == 0) {
                    availableStock = $scope.getAvailableStock(editBatch, editBatch);
                    editBatch.availableQty = availableStock;
                    totalQuantity = availableStock;
                    tempBatch.push(editBatch);
                }
                $scope.batchList = tempBatch;
                if (tempBatchScan.length == 1) {
                    $scope.batchList = rearrangeBatchList($scope.batchList, tempBatchScan);
                }
                if ($scope.batchList.length > 0) {
                    var sreturn = $scope.selectedBatch.salesReturn;
                    var ttype = $scope.selectedBatch.transactionType;
                    if (editBatch == null) {
                        $scope.selectedBatch = $scope.batchList[0];
                        $scope.selectedBatch.reminderFrequency = "0";

                        //By san ProductDisc
                        if (!angular.isUndefinedOrNull($scope.selectedBatch.product.discount) || !angular.isUndefinedOrNull($scope.customerHelper.data.selectedCustomer.discount)) {
                            getProductwisDiscounteSettings();
                        }
                        else
                            $scope.selectedBatch.discount = "";

                        $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                    } else {
                        $scope.selectedBatch = editBatch;

                        //// Added by San - Product/Customer Wise Discount disable discount field
                        //if ($scope.discountRules.billAmountType == "productCustomerWiseDiscount") {
                        //    document.getElementById('discount').disabled = true;
                        //}

                        $scope.selectedBatch.discount = editBatch.discount;
                        $scope.selectedBatch.previoussellingPrice = editBatch.sellingPrice;
                        $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                    }
                    if (LeadsRequiredquantity != "") {
                        $scope.selectedBatch.quantity = LeadsRequiredquantity;
                    }
                    $scope.selectedBatch.totalQuantity = totalQuantity;
                    $scope.selectedBatch.rackNo = rack;
                    $scope.selectedBatch.salesReturn = sreturn;
                    $scope.selectedBatch.transactionType = ttype;
                    dayDiff($scope.selectedBatch.expireDate);

                    if ($scope.batchListType == "Product") {
                        if ($scope.taxValuesList.length > 0) {
                            $scope.getGstvalue = $scope.selectedBatch.gstTotal;
                            var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.getGstvalue) }, true);
                            if (taxList.length == 0) {
                                $scope.gstMode = true;
                                return;
                            }
                            else {
                                $scope.gstMode = false;
                            }
                        }
                    }


                    if ($scope.customerHelper.data.selectedCustomer.patientType == 2) {

                        var vatPercent = 0;
                        if ($scope.isGstEnabled == true) {
                            if ($scope.selectedBatch.gstTotal != null && $scope.selectedBatch.gstTotal != undefined) {
                                vatPercent = $scope.selectedBatch.gstTotal;
                            } else if ($scope.selectedBatch.product.gstTotal != null && $scope.selectedBatch.product.gstTotal != undefined) {
                                vatPercent = $scope.selectedBatch.product.gstTotal;
                            }

                        } else {
                            vatPercent = $scope.selectedBatch.vat;
                        }

                        if ($scope.customerHelper.data.selectedCustomer.id != "30fdae11-4a06-4f9d-a87d-3ead26a8d110") {
                            $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * (vatPercent / 100))) * 1.1).toFixed(2);
                        }
                        else {
                            $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * (vatPercent / 100)))).toFixed(2);
                        }
                        //$scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * (vatPercent / 100))) * 1.1).toFixed(2);
                    }
                    //        }
                    //    }
                    //}
                    //if ($scope.customerHelper.data.patientSearchData.patientType == 2) {
                    //    $scope.selectedBatch.sellingPrice = ((($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) + (($scope.selectedBatch.packagePurchasePrice / $scope.selectedBatch.packageSize) * ($scope.selectedBatch.vat / 100))) * 1.1).toFixed(2);
                    //}
                    //if (editBatch == null) {
                    //    $scope.selectedBatch.discount = "";
                    //}
                    if (tempBatchScan.length > 0) {
                        loadAutoQty();
                    }
                } else {
                    $scope.zeroBatch = true;
                }
            }, function () { });
        } else {
            productStockService.getBatchForReturn($scope.selectedProduct.product.id).then(function (response) {
                $scope.batchList = response.data;
                var qty = document.getElementById("quantity");
                qty.focus();
                var totalQuantity = 0;
                var rack = "";
                var tempBatch = [];
                $scope.changediscountsale("");
                var availableStock = null;
                for (var i = 0; i < $scope.batchList.length; i++) {
                    if ($scope.Editsale == false) {
                        $scope.batchList[i].productStockId = $scope.batchList[i].id;
                        $scope.batchList[i].id = null;

                        //Added by Sarubala on 07-11-17 to show whole sale price
                        if ($scope.salesPriceSettings == 3) {
                            $scope.batchList[i].sellingPrice = parseFloat(($scope.batchList[i].returnPurchasePriceWithoutTax * (1 + ($scope.batchList[i].gstTotal / 100))).toFixed(6));
                            $scope.batchList[i].purchasePriceWithoutTax = $scope.batchList[i].returnPurchasePriceWithoutTax;
                        }

                        availableStock = $scope.getAvailableStock($scope.batchList[i], editBatch);
                        if (editBatch != null && editBatch.productStockId == $scope.batchList[i].productStockId)
                            editBatch.availableQty = availableStock;
                        totalQuantity += availableStock;
                        //if (availableStock == 0)
                        //    continue;
                        $scope.batchList[i].availableQty = availableStock;
                        if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                            rack = $scope.batchList[i].rackNo;
                        }
                        tempBatch.push($scope.batchList[i]);
                    } else {
                        if ($scope.batchList[i].rackNo != null || $scope.batchList[i].rackNo != undefined) {
                            rack = $scope.batchList[i].rackNo;
                        }
                    }
                }
                if (editBatch != null && tempBatch.length == 0) {
                    availableStock = $scope.getAvailableStock(editBatch, editBatch);
                    editBatch.availableQty = availableStock;
                    totalQuantity = availableStock;
                    tempBatch.push(editBatch);
                }
                $scope.batchList = tempBatch;
                if ($scope.batchList.length > 0) {
                    var sreturn = $scope.selectedBatch.salesReturn;
                    var ttype = $scope.selectedBatch.transactionType;
                    if (editBatch == null) {
                        $scope.selectedBatch = $scope.batchList[0];
                        $scope.selectedBatch.reminderFrequency = "0";
                        $scope.selectedBatch.discount = "";
                        $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                    } else {
                        $scope.selectedBatch = editBatch;
                        $scope.selectedBatch.discount = editBatch.discount;
                        $scope.selectedBatch.previoussellingPrice = editBatch.sellingPrice;
                        $scope.selectedBatch.canEnableTaxEdit = ($scope.selectedBatch.gstTotal == 0);
                    }
                    $scope.selectedBatch.salesReturn = sreturn;
                    $scope.selectedBatch.transactionType = ttype;
                    if (LeadsRequiredquantity != "") {
                        $scope.selectedBatch.quantity = LeadsRequiredquantity;
                    }
                    $scope.selectedBatch.totalQuantity = totalQuantity;
                    $scope.selectedBatch.rackNo = rack;
                    dayDiff($scope.selectedBatch.expireDate);
                    //if (editBatch == null) {
                    //    $scope.selectedBatch.discount = "";
                    //}
                } else {
                    $scope.zeroBatch = true;
                }
            }, function () { });
        }
        $scope.valPurchasePrice = "";
        var qty = document.getElementById("quantity");
        qty.focus();
        LoadPreviousSalesItemHistory();
    }
    //Durga Code For Batch Series
    getInvoiceSeries();
    //Newly created by Manivannan on 30-Jan-2017 begins
    function getNumericInvoiceNo() {
        var salesEditId = document.getElementById('salesEditId').value;
        if (salesEditId == "") {
            if ($scope.InvoiceSeriestype == 1 || $scope.InvoiceSeriestype == "") {
                salesService.getNumericInvoiceNo().then(function (response) {
                    // $scope.trackInvoiceNo();
                    if (response.data != "" && response.data != null && response.data != undefined) {
                        //   $scope.sales.invoiceNo = response.data;
                        var invoiceno = response.data;
                        $scope.dispInvoiceNo = invoiceno;
                        $scope.sales.invoiceNo = $scope.dispInvoiceNo;
                    }
                }, function () {
                });
            }
        }
        //else {
        //    $scope.trackInvoiceNo();
        //}
    }
    function getCreditCustomerInvoicenumber() {
        var salesEditId = document.getElementById('salesEditId').value;
        if (salesEditId == "") {
            getCustomInvoiceNo("CRE");
        }
    }
    //Newly created by Manivannan on 30-Jan-2017 ends
    $scope.InvoiceSeriestype = "";
    function getInvoiceSeries() {
        salesService.getInvoiceSeries().then(function (response) {
            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriestype = response.data.invoiceseriesType;
                if ($scope.InvoiceSeriestype == 2) {
                    getInvoiceSeriesItems();
                    $scope.IsSelectedInvoiceSeries = true;
                }
                if ($scope.InvoiceSeriestype == 1) {
                    //Newly added by Manivannan on 31-Jan-17
                    getNumericInvoiceNo();
                    $scope.IsSelectedInvoiceSeries = false;
                }
                if ($scope.InvoiceSeriestype == 3) {
                    $scope.IsSelectedInvoiceSeries = false;
                }
                //if ($scope.InvoiceSeriestype == 4) {
                //  //  getInvoiceCreditSeriesItems();
                //    getCreditCustomerInvoicenumber();
                //    $scope.IsSelectedInvoiceSeries = false;
                //}
            } else {
                //$scope.IsSelectedInvoiceSeries = false;
                getNumericInvoiceNo();
                //Newly added by Manivannan on 31-Jan-17
                $scope.IsSelectedInvoiceSeries = false;
            }
        }, function () {
        });
    }

    //Modified by Sarubala on 20-10-17
    //Newly added by Durga on 10-04-2017
    //$scope.IsCreditInvoiceSeries = false;
    //getIsCreditInvoiceSeries();
    //function getIsCreditInvoiceSeries() {
    //    salesService.getIsCreditInvoiceSeries().then(function (response) {
    //        if (response.data == "" || response.data == null || response.data == undefined) {
    //            $scope.IsCreditInvoiceSeries = false;
    //        } else {
    //            if (response.data.isCreditInvoiceSeries != undefined) {
    //                $scope.IsCreditInvoiceSeries = response.data.isCreditInvoiceSeries;
    //            } else {
    //                $scope.IsCreditInvoiceSeries = false;
    //            }
    //        }
    //    }, function () {
    //    });
    //}
    $scope.InvoiceSeriesItems = [];
    function getInvoiceSeriesItems() {
        salesService.getInvoiceSeriesItems().then(function (response) {
            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }
            getCustomSeriesSelected();
        }, function () {
        });
    }



    //$scope.InvoiceCreditSeriesItems = [];
    //function getInvoiceCreditSeriesItems() {
    //    salesService.getInvoiceCreditSeriesItems().then(function (response) {
    //        if (response.data != "" && response.data != null) {
    //            $scope.InvoiceCreditSeriesItems = response.data;
    //        }
    //        getCustomCreditSeriesSelected();
    //    }, function () {
    //    });
    //}
    //function getCustomCreditSeriesSelected() {
    //    salesService.getCustomCreditSeriesSelected().then(function (response) {
    //        if (response.data != "" && response.data != null) {
    //            if (response.data.customSeriesInvoice != undefined || response.data.customSeriesInvoice != null || response.data.customSeriesInvoice != "") {
    //                $scope.selectedCreditSeriesItem = response.data.customSeriesInvoice;
    //                $scope.IsSelectedInvoiceSeries = false;
    //                //Newly added by Manivannan on 30-Jan-17 
    //                getCustomCreditInvoiceNo($scope.selectedCreditSeriesItem);
    //            }
    //        }
    //    }, function () {
    //    });
    //}
    Checkleads();
    $scope.SaleLeadsList = {};
    function Checkleads() {
        if (window.localStorage.getItem("ConvertLeadstoSaleObj") == null) {
        } else {
            $scope.SaleLeadsList = JSON.parse(window.localStorage.getItem("ConvertLeadstoSaleObj"));
            window.localStorage.removeItem('ConvertLeadstoSaleObj');
            $scope.leadsProductArray = $scope.SaleLeadsList.leadsProduct;
            $scope.sales.name = $scope.SaleLeadsList.name;
            $scope.sales.mobile = $scope.SaleLeadsList.mobile;
            $scope.sales.patientId = $scope.SaleLeadsList.patientId;
            $scope.sales.leadId = $scope.SaleLeadsList.id;
            if ($scope.SaleLeadsList.doctorName != null && $scope.SaleLeadsList.doctorName != undefined && $scope.SaleLeadsList.doctorName != "") {
                $scope.doctorname = $scope.SaleLeadsList.doctorName;
            }
            if ($scope.SaleLeadsList.doctorMobile != null && $scope.SaleLeadsList.doctorMobile != undefined && $scope.SaleLeadsList.doctorMobile != "") {
                $scope.sales.doctorMobile = $scope.SaleLeadsList.doctorMobile;
            }
            $scope.customerHelper.data.patientSearchData.mobile = $scope.sales.mobile;
            $scope.customerHelper.data.patientSearchData.name = $scope.sales.name;
            if ($scope.customerHelper.data.patientSearchData.mobile != undefined && $scope.customerHelper.data.patientSearchData.mobile != "") {
                $scope.customerHelper.search(false, 2, "No", 0, 1);
            }
        }
    }

    // Added by Gavaskar 10-10-2017 Start Sales Order , Estimate ,Template Convert to Sales Start

    $scope.saleTemplateList = [];
    CheckSalesTemplates();
    var i = 0;
    $scope.selectedTemplateNameList = {};

    function CheckSalesTemplates() {
        if (window.localStorage.getItem("ConvertTemplatetoSaleObj") == null) {
        } else {
            $scope.salesTemplate = JSON.parse(window.localStorage.getItem("ConvertTemplatetoSaleObj"));
            window.localStorage.removeItem('ConvertTemplatetoSaleObj');

            if ($scope.templateName != null) {
                toastr.info('Template Already added');
                return;
            }

            if ($scope.salesTemplate == null || $scope.salesTemplate == "" || $scope.salesTemplate == undefined) {
                $scope.templateName = "";
            }
            else {
                $scope.templateName = $scope.salesTemplate.templateName;
            }

            $scope.Types1 = "Template";
            ////for (i = 0; i < $scope.salesTemplate.length; i++) {
            salesService.getSelectedProductStockList($scope.salesTemplate.id, $scope.Types1).then(function (response) {

                $scope.selectedTemplateNameList = response.data;

                for (i = 0; i < $scope.selectedTemplateNameList.length; i++) {

                    if ($scope.selectedTemplateNameList[i] != "" || $scope.selectedTemplateNameList[i] != null) {
                        if ($scope.selectedTemplateNameList[i].orderQty > $scope.selectedTemplateNameList[i].stock) {
                            if ($scope.selectedTemplateNameList[i].stock == null || $scope.selectedTemplateNameList[i].stock == undefined || $scope.selectedTemplateNameList[i].stock == 0 || $scope.selectedTemplateNameList[i].stock == "") {
                                $scope.selectedTemplateNameList[i].orderQty = parseFloat($scope.selectedTemplateNameList[i].orderQty);
                                // $scope.selectedTemplateNameList[i].pendingTemplateQty = parseFloat($scope.selectedTemplateNameList[i].orderQty);
                            }
                            else {
                                $scope.selectedTemplateNameList[i].quantity = parseFloat($scope.selectedTemplateNameList[i].stock);
                                $scope.selectedTemplateNameList[i].pendingTemplateQty = parseFloat($scope.selectedTemplateNameList[i].orderQty) - parseFloat($scope.selectedTemplateNameList[i].stock);
                            }
                            $scope.salesTemplateProductArray.push($scope.selectedTemplateNameList[i]);

                        }
                        else {
                            $scope.selectedTemplateNameList[i].quantity = parseFloat($scope.selectedTemplateNameList[i].orderQty);
                            $scope.selectedTemplateNameList[i].availableQty = $scope.selectedTemplateNameList[i].stock;
                        }

                    }
                    if ($scope.selectedTemplateNameList[i].quantity != null) {
                        $scope.selectedTemplateNameList[i].selectedBatch = $scope.selectedTemplateNameList[i].batchNo;

                        if ($scope.selectedTemplateNameList[i].product.productInstance.rackNo == null || $scope.selectedTemplateNameList[i].product.productInstance.rackNo == '' || $scope.selectedTemplateNameList[i].product.productInstance.rackNo == undefined) {
                            $scope.selectedTemplateNameList[i].rackNo = '-';

                        }
                        else {
                            $scope.selectedTemplateNameList[i].selectedBatch.rackNo = $scope.selectedTemplateNameList[i].product.productInstance.rackNo;
                            $scope.selectedTemplateNameList[i].rackNo = $scope.selectedTemplateNameList[i].product.productInstance.rackNo;
                        }
                        //Added by Gavaskar on 25-11-2017 Start
                        if ($scope.selectedTemplateNameList[i].sellingPrice == null || $scope.selectedTemplateNameList[i].sellingPrice == '' || $scope.selectedTemplateNameList[i].sellingPrice == undefined) {
                            $scope.selectedTemplateNameList[i].sellingPrice = 0;
                        }
                        else {
                            $scope.selectedTemplateNameList[i].sellingPrice = $scope.selectedTemplateNameList[i].sellingPrice;
                        }
                        if ($scope.salesPriceSettings == 3) {
                            if (parseFloat($scope.selectedTemplateNameList[i].sellingPrice) > 0) {
                                $scope.selectedTemplateNameList[i].sellingPrice = parseFloat(($scope.selectedTemplateNameList[i].returnPurchasePriceWithoutTax * (1 + ($scope.selectedTemplateNameList[i].gstTotal / 100))).toFixed(6));
                                $scope.selectedTemplateNameList[i].purchasePriceWithoutTax = $scope.selectedTemplateNameList[i].returnPurchasePriceWithoutTax;
                            }
                        }
                        //Added by Gavaskar on 25-11-2017 End

                        if ($scope.selectedTemplateNameList[i].product.productInstance.boxNo == null || $scope.selectedTemplateNameList[i].product.productInstance.boxNo == '' || $scope.selectedTemplateNameList[i].product.productInstance.boxNo == undefined) {
                            $scope.selectedTemplateNameList[i].product.boxNo = '-';
                        }
                        else {
                            $scope.selectedTemplateNameList[i].product.boxNo = $scope.selectedTemplateNameList[i].product.productInstance.boxNo;
                        }

                        $scope.selectedTemplateNameList[i].productStockId = $scope.selectedTemplateNameList[i].id;
                        $scope.selectedTemplateNameList[i].product.id = $scope.selectedTemplateNameList[i].productId;
                        $scope.selectedTemplateNameList[i].id = null;

                        $scope.selectedTemplateNameList[i].salesReturn = false;
                        $scope.selectedTemplateNameList[i].discount = 0;
                        $scope.selectedTemplateNameList[i].isNew = false;

                        if ($scope.selectedTemplateNameList[i].sellingPrice == 0 || $scope.selectedTemplateNameList[i].sellingPrice == "" || $scope.selectedTemplateNameList[i].sellingPrice == undefined) {
                            $scope.selectedTemplateNameList[i].sellingPrice = $scope.selectedTemplateNameList[i].mrp;
                        }
                        if ($scope.selectedTemplateNameList[i].editId == null || $scope.selectedTemplateNameList[i].editId == undefined) {
                            $scope.selectedTemplateNameList[i].editId = $scope.editId++;
                        }

                        $scope.sales.salesItem.push($scope.selectedTemplateNameList[i]);
                    }

                }
                $scope.sales.salesTemplateId = $scope.salesTemplate.id;
                orderEstimateTemplateMandatory();

            }, function () {
            });

        }
    }

    // Added Estimate Functions Start

    CheckSalesEstimate();
    var j = 0;
    $scope.selectedEstimateList = {};

    function CheckSalesEstimate() {
        if (window.localStorage.getItem("ConvertEstimatetoSaleObj") == null) {
        } else {
            $scope.salesEstimate = JSON.parse(window.localStorage.getItem("ConvertEstimatetoSaleObj"));
            window.localStorage.removeItem('ConvertEstimatetoSaleObj');

            if ($scope.orderEstimateId != null) {
                toastr.info('Sales Order / Estimate Already added');
                return;
            }

            if ($scope.salesEstimate == null || $scope.salesEstimate == "" || $scope.salesEstimate == undefined) {
                $scope.orderEstimateId = null;
            }
            else {
                $scope.orderEstimateId = $scope.salesEstimate.id;
            }
            if ($scope.salesEstimate.salesOrderEstimateType == 1) {
                $scope.Types = "Order";
            }
            else {
                $scope.Types = "Estimate";
            }

            salesService.getSelectedProductStockList($scope.salesEstimate.id, $scope.Types).then(function (response) {

                $scope.selectedEstimateList = response.data;

                for (j = 0; j < $scope.selectedEstimateList.length; j++) {

                    if ($scope.selectedEstimateList[j] != "" || $scope.selectedEstimateList[j] != null) {
                        if ($scope.selectedEstimateList[j].orderQty > $scope.selectedEstimateList[j].stock) {
                            if ($scope.selectedEstimateList[j].stock == null || $scope.selectedEstimateList[j].stock == undefined || $scope.selectedEstimateList[j].stock == 0 || $scope.selectedEstimateList[j].stock == "") {
                                $scope.selectedEstimateList[j].orderQty = parseFloat($scope.selectedEstimateList[j].orderQty);
                                $scope.selectedEstimateList[j].pendingTemplateQty = $scope.selectedEstimateList[j].orderQty;
                            }
                            else {
                                $scope.selectedEstimateList[j].quantity = parseFloat($scope.selectedEstimateList[j].stock);
                                $scope.selectedEstimateList[j].pendingTemplateQty = parseFloat($scope.selectedEstimateList[j].orderQty) - parseFloat($scope.selectedEstimateList[j].stock);
                            }
                            $scope.selectedEstimateList[j].salesOrderEstimateId = $scope.salesEstimate.id;
                            $scope.selectedEstimateList[j].salesOrderEstimatePatientId = $scope.selectedEstimateList[0].orderEstimateCutomerId;
                            if ($scope.selectedEstimateList[j].orderEstimateType == 2) {
                                var tmp = $filter("filter")($scope.salesEstimateProductArray, { productId: $scope.selectedEstimateList[j].productId }, true);
                                if (tmp.length == 0) {
                                    $scope.salesEstimateProductArray.push($scope.selectedEstimateList[j]);
                                }
                                else {
                                    $scope.salesEstimateProductArray[$scope.salesEstimateProductArray.indexOf(tmp[0])].pendingTemplateQty += $scope.selectedEstimateList[j].pendingTemplateQty;
                                }
                            }
                            else {
                                $scope.salesEstimateProductArray.push($scope.selectedEstimateList[j]);
                            }

                        }
                        else {
                            $scope.selectedEstimateList[j].quantity = parseFloat($scope.selectedEstimateList[j].orderQty);
                            $scope.selectedEstimateList[j].availableQty = $scope.selectedEstimateList[j].stock;
                        }

                    }
                    if ($scope.selectedEstimateList[j].quantity != null) {
                        $scope.selectedEstimateList[j].selectedBatch = $scope.selectedEstimateList[j].batchNo;

                        //Added by Gavaskar on 25-11-2017 Start
                        if ($scope.selectedEstimateList[j].sellingPrice == null || $scope.selectedEstimateList[j].sellingPrice == '' || $scope.selectedEstimateList[j].sellingPrice == undefined) {
                            $scope.selectedEstimateList[j].sellingPrice = 0;
                        }
                        else {
                            $scope.selectedEstimateList[j].sellingPrice = $scope.selectedEstimateList[j].sellingPrice;
                        }
                        if ($scope.salesPriceSettings == 3) {
                            if (parseFloat($scope.selectedEstimateList[j].sellingPrice) > 0) {
                                $scope.selectedEstimateList[j].sellingPrice = parseFloat(($scope.selectedEstimateList[j].returnPurchasePriceWithoutTax * (1 + ($scope.selectedEstimateList[j].gstTotal / 100))).toFixed(6));
                                $scope.selectedEstimateList[j].purchasePriceWithoutTax = $scope.selectedEstimateList[j].returnPurchasePriceWithoutTax;
                            }
                        }
                        //Added by Gavaskar on 25-11-2017 End

                        if ($scope.selectedEstimateList[j].product.productInstance.rackNo == null || $scope.selectedEstimateList[j].product.productInstance.rackNo == '' || $scope.selectedEstimateList[j].product.productInstance.rackNo == undefined) {
                            $scope.selectedEstimateList[j].rackNo = '-';

                        }
                        else {
                            $scope.selectedEstimateList[j].rackNo = $scope.selectedEstimateList[j].product.productInstance.rackNo;
                        }
                        if ($scope.selectedEstimateList[j].product.productInstance.boxNo == null || $scope.selectedEstimateList[j].product.productInstance.boxNo == '' || $scope.selectedEstimateList[j].product.productInstance.boxNo == undefined) {
                            $scope.selectedEstimateList[j].product.boxNo = '-';
                        }
                        else {
                            $scope.selectedEstimateList[j].product.boxNo = $scope.selectedEstimateList[j].product.productInstance.boxNo;
                        }


                        $scope.selectedEstimateList[j].discount = $scope.selectedEstimateList[j].orderEstimateDiscount;
                        $scope.selectedEstimateList[j].productStockId = $scope.selectedEstimateList[j].id;
                        $scope.selectedEstimateList[j].product.id = $scope.selectedEstimateList[j].productId;
                        $scope.selectedEstimateList[j].id = null;

                        $scope.selectedEstimateList[j].salesReturn = false;
                        $scope.selectedEstimateList[j].isNew = false;
                        $scope.orderEstimateType = $scope.selectedEstimateList[i].orderEstimateType;
                        if ($scope.orderEstimateType == 1) {
                            $scope.selectedEstimateList[j].salesOrderProductSelected = 1;
                        }
                        else {
                            $scope.selectedEstimateList[j].salesEstimateProductSelected = 2;
                        }
                        $scope.selectedEstimateList[j].salesOrderEstimateId = $scope.salesEstimate.id;
                        $scope.selectedEstimateList[j].salesOrderEstimatePatientId = $scope.selectedEstimateList[0].orderEstimateCutomerId;
                        if ($scope.selectedEstimateList[j].sellingPrice == 0 || $scope.selectedEstimateList[j].sellingPrice == "" || $scope.selectedEstimateList[j].sellingPrice == undefined) {
                            $scope.selectedEstimateList[j].sellingPrice = $scope.selectedEstimateList[j].mrp;
                        }

                        if ($scope.selectedEstimateList[j].editId == null || $scope.selectedEstimateList[j].editId == undefined) {
                            $scope.selectedEstimateList[j].editId = $scope.editId++;
                        }
                        $scope.selectedEstimateList[j].salesOrderEstimate = true;
                        $scope.sales.salesItem.push($scope.selectedEstimateList[j]);
                    }
                }
                if ($scope.selectedEstimateList.length > 0) {
                    $scope.sales.name = $scope.selectedEstimateList[0].orderEstimateCutomerName;
                    $scope.sales.mobile = $scope.selectedEstimateList[0].orderEstimateCutomerMobile;
                    $scope.sales.patientId = $scope.selectedEstimateList[0].orderEstimateCutomerId;
                }

                $scope.customerHelper.data.patientSearchData.mobile = $scope.sales.mobile;
                $scope.customerHelper.data.patientSearchData.name = $scope.sales.name;
                if ($scope.customerHelper.data.patientSearchData.mobile != undefined && $scope.customerHelper.data.patientSearchData.mobile != "") {
                    $scope.customerHelper.search(false, 2, "No", 0, 1);
                }

                $scope.sales.salesEstimateId = $scope.salesEstimate.id;
                orderEstimateTemplateMandatory();
                if ($scope.sales.salesItem.length > 0) {
                    for (var j = 0; j < $scope.sales.salesItem.length; j++) {
                        if ($scope.sales.salesItem[j].discount > 0) {
                            $scope.discountInValid = true;
                        }
                    }
                }

            }, function () {
            });
        }
    }

    // Added Estimate Functions End


    var SalesTemplateRequiredQty = "";
    $scope.SalesTemplateProductSelected = false;
    $scope.AddItemSalesTemplatetoSave = function (obj, index) {
        $scope.SalesTemplateProductIndex = "";
        $scope.SalesTemplateProductId = "";
        $scope.SalesTemplateProductSelected = false;
        var productname = obj.product.name;
        SalesTemplateRequiredQty = obj.pendingTemplateQty;
        var val = productname.split(" ");
        productStockService.getSalesProduct(productname).then(function (response) {
            $scope.salesTemplateproductlistArray = [];
            $scope.salesTemplateproductlistArray = response.data;
            if ($scope.salesTemplateproductlistArray.length == 0) {
                var threecharVal = productname.slice(0, 3);
                productStockService.getSalesProduct(productname).then(function (response) {
                    $scope.salesTemplateproductlistArray = [];
                    $scope.salesTemplateproductlistArray = response.data;
                    $scope.enableTempProductsPopup = true;
                    var m = ModalService.showModal({
                        "controller": "tempproductsCtrl",
                        "templateUrl": 'TempProductDetails',
                        "inputs": {
                            "tempproductlistArray": $scope.salesTemplateproductlistArray,
                            "productName": productname,
                            "Requiredquantity": SalesTemplateRequiredQty
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            $scope.message = "You said " + result;
                            if (result == "Yes") {
                                $scope.LeadproductSelection(true, index, obj.id);
                            }
                        });
                    });
                    return false;
                }, function (error) {
                    console.log(error);
                });
            } else {
                $scope.enableTempProductsPopup = true;
                var m = ModalService.showModal({
                    "controller": "tempproductsCtrl",
                    "templateUrl": 'TempProductDetails',
                    "inputs": {
                        "tempproductlistArray": $scope.salesTemplateproductlistArray,
                        "productName": productname,
                        "Requiredquantity": SalesTemplateRequiredQty
                    }
                }).then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                        $scope.message = "You said " + result;
                        if (result == "Yes") {
                            $scope.LeadproductSelection(true, index, obj.id);
                        }
                    });
                });
                return false;
            }
        }, function (error) {
            console.log(error);
        });
    };

    $scope.salesTemplateEsc = false;
    $scope.showTemplates = function () {
        $scope.salesTemplateEsc = true;
        var m = ModalService.showModal({
            "controller": "salesTemplateWiseCtrl",
            "templateUrl": 'salesTemplates'
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.salesTemplateEsc = false;
                $scope.message = "You said " + result;
                if (result == "Yes") {
                    var selectedTemplate = cacheService.get('selectedSalesTemplate');
                    selectedTemplateProductStockList(selectedTemplate);
                }
            });
        });
        return false;
    };

    function selectedTemplateProductStockList(selectedTemplate) {

        //cacheService.set('selectedSalesTemplate', selectedTemplate.id);
        if ($scope.templateName != null) {
            toastr.info('Template Already added');
            return;
        }

        if (selectedTemplate == null || selectedTemplate == "" || selectedTemplate == undefined) {
            $scope.templateName = "";
        }
        else {
            $scope.templateName = selectedTemplate.templateName;
        }


        $scope.Types = "Template";
        salesService.getSelectedProductStockList(selectedTemplate.id, $scope.Types).then(function (response) {

            $scope.selectedTemplateNameList = response.data;

            for (i = 0; i < $scope.selectedTemplateNameList.length; i++) {

                if ($scope.selectedTemplateNameList[i] != "" || $scope.selectedTemplateNameList[i] != null) {
                    if ($scope.selectedTemplateNameList[i].orderQty > $scope.selectedTemplateNameList[i].stock) {
                        if ($scope.selectedTemplateNameList[i].stock == null || $scope.selectedTemplateNameList[i].stock == undefined || $scope.selectedTemplateNameList[i].stock == 0 || $scope.selectedTemplateNameList[i].stock == "") {
                            $scope.selectedTemplateNameList[i].orderQty = parseFloat($scope.selectedTemplateNameList[i].orderQty);
                            // $scope.selectedTemplateNameList[i].pendingTemplateQty = parseFloat($scope.selectedTemplateNameList[i].orderQty);
                        }
                        else {
                            $scope.selectedTemplateNameList[i].quantity = parseFloat($scope.selectedTemplateNameList[i].stock);
                            $scope.selectedTemplateNameList[i].pendingTemplateQty = parseFloat($scope.selectedTemplateNameList[i].orderQty) - parseFloat($scope.selectedTemplateNameList[i].stock);
                        }
                        $scope.salesTemplateProductArray.push($scope.selectedTemplateNameList[i]);

                    }
                    else {
                        $scope.selectedTemplateNameList[i].quantity = parseFloat($scope.selectedTemplateNameList[i].orderQty);
                        $scope.selectedTemplateNameList[i].availableQty = $scope.selectedTemplateNameList[i].stock;
                    }

                }
                if ($scope.selectedTemplateNameList[i].quantity != null) {

                    $scope.selectedTemplateNameList[i].selectedBatch = { reminderFrequency: "0" };
                    $scope.selectedTemplateNameList[i].selectedBatch = $scope.selectedTemplateNameList[i].batchNo;
                    if ($scope.selectedTemplateNameList[i].product.productInstance.rackNo == null || $scope.selectedTemplateNameList[i].product.productInstance.rackNo == '' || $scope.selectedTemplateNameList[i].product.productInstance.rackNo == undefined) {
                        $scope.selectedTemplateNameList[i].rackNo = '-';

                    }
                    else {
                        $scope.selectedTemplateNameList[i].selectedBatch.rackNo = $scope.selectedTemplateNameList[i].product.productInstance.rackNo;
                        $scope.selectedTemplateNameList[i].rackNo = $scope.selectedTemplateNameList[i].product.productInstance.rackNo;
                    }
                    if ($scope.selectedTemplateNameList[i].product.productInstance.boxNo == null || $scope.selectedTemplateNameList[i].product.productInstance.boxNo == '' || $scope.selectedTemplateNameList[i].product.productInstance.boxNo == undefined) {
                        $scope.selectedTemplateNameList[i].product.boxNo = '-';
                    }
                    else {
                        $scope.selectedTemplateNameList[i].product.boxNo = $scope.selectedTemplateNameList[i].product.productInstance.boxNo;
                    }
                    //Added by Gavaskar on 25-11-2017 Start
                    if ($scope.selectedTemplateNameList[i].sellingPrice == null || $scope.selectedTemplateNameList[i].sellingPrice == '' || $scope.selectedTemplateNameList[i].sellingPrice == undefined) {
                        $scope.selectedTemplateNameList[i].sellingPrice = 0;
                    }
                    else {
                        $scope.selectedTemplateNameList[i].sellingPrice = $scope.selectedTemplateNameList[i].sellingPrice;
                    }
                    if ($scope.salesPriceSettings == 3) {
                        if (parseFloat($scope.selectedTemplateNameList[i].sellingPrice) > 0) {
                            $scope.selectedTemplateNameList[i].sellingPrice = parseFloat(($scope.selectedTemplateNameList[i].returnPurchasePriceWithoutTax * (1 + ($scope.selectedTemplateNameList[i].gstTotal / 100))).toFixed(6));
                            $scope.selectedTemplateNameList[i].purchasePriceWithoutTax = $scope.selectedTemplateNameList[i].returnPurchasePriceWithoutTax;
                        }
                    }
                    //Added by Gavaskar on 25-11-2017 End
                    $scope.selectedTemplateNameList[i].productStockId = $scope.selectedTemplateNameList[i].id;
                    $scope.selectedTemplateNameList[i].product.id = $scope.selectedTemplateNameList[i].productId;
                    $scope.selectedTemplateNameList[i].id = null;

                    $scope.selectedTemplateNameList[i].salesReturn = false;
                    $scope.selectedTemplateNameList[i].discount = 0;
                    $scope.selectedTemplateNameList[i].isNew = false;

                    if ($scope.selectedTemplateNameList[i].sellingPrice == 0 || $scope.selectedTemplateNameList[i].sellingPrice == "" || $scope.selectedTemplateNameList[i].sellingPrice == undefined) {
                        $scope.selectedTemplateNameList[i].sellingPrice = $scope.selectedTemplateNameList[i].mrp;
                    }

                    if ($scope.selectedTemplateNameList[i].editId == null || $scope.selectedTemplateNameList[i].editId == undefined) {
                        $scope.selectedTemplateNameList[i].editId = $scope.editId++;
                    }
                    $scope.sales.salesItem.push($scope.selectedTemplateNameList[i]);
                }

            }
            $scope.sales.salesTemplateId = $scope.salesTemplate.id;

            orderEstimateTemplateMandatory();

        }, function () {
        });

    };



    function orderEstimateTemplateMandatory() {
        $scope.isdoctornameMandatory = false;
        $scope.iscustomerNameMandatory = false;
        $scope.schedulecount = 0;
        if ($scope.DoctorNameMandatory == 1 || $scope.DoctorNameMandatory == 3) {
            var schedulecount = 0;
            for (var i = 0; i < $scope.sales.salesItem.length; i++) {
                if ($scope.DoctorNameMandatory == 3) {
                    schedulecount = 1;
                } else {
                    if ($scope.sales.salesItem[i].product.schedule == "H" || $scope.sales.salesItem[i].product.schedule == "H1" || $scope.sales.salesItem[i].product.schedule == "X") {
                        schedulecount++;
                    }
                }
            }
            $scope.schedulecount = schedulecount;
            if ($scope.schedulecount > 0) {
                $scope.iscustomerNameMandatory = true;
                var custname = document.getElementById("customerName").value.trim();
                if ($scope.customerHelper.data.selectedCustomer.name == undefined || $scope.customerHelper.data.selectedCustomer.name == null) {
                    $scope.customerHelper.data.selectedCustomer.name = "";
                }
                if (custname != "" || $scope.customerHelper.data.selectedCustomer.name != "") {
                    $scope.iscustomerNameMandatory = false;
                }
                var docname = document.getElementById("doctorName").value;
                if (docname != "") {
                    $scope.isdoctornameMandatory = false;
                }
            }
        }
        $scope.isSalesTypeMandatory = false;
        if ($scope.SalesTypeMandatory == 1) {
            $scope.isSalesTypeMandatory = true;
        }
        if ($scope.LeadProductIndex == undefined || $scope.LeadProductIndex == null) {
            $scope.LeadProductIndex = "";
        }
        if ($scope.LeadProductIndex == 0) {
            $scope.showproductSelected[0] = true;
        }
        if ($scope.LeadProductIndex != "") {
            var ind = $scope.LeadProductIndex;
            $scope.showproductSelected[ind] = true;
        }
        resetFocus();
        LeadsRequiredquantity = "";
        $scope.BillCalculations();
        getDiscountRules();
        $scope.salesReturnCount = 0;
        $scope.salesCount = 0;
        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            if ($scope.sales.salesItem[j].salesReturn) {
                $scope.salesReturnCount++;
            } else {
                $scope.salesCount++;
            }
        }
        $scope.changeCashType();
        if ($scope.sales.cashType == "Credit") {
            $scope.focusCreditTextBox = false;
            if ($scope.sales.credit != "") {
                if ($scope.sales.credit > $scope.FinalNetAmount) {
                    $scope.Iscredit = true;
                } else {
                    $scope.Iscredit = false;
                }
            }
            document.getElementById("drugName").focus();
        }

    };

    $scope.SelectedCustomerOrderEstimateList = {};
    $scope.salesOrderEstimateEsc = false;
    $scope.showSalesOrderEstimateItems = function () {

        if ($scope.SelectedCustomerOrderEstimateList.length > 0) {
            $scope.salesOrderEstimateEsc = true;
            var m = ModalService.showModal({
                "controller": "showSalesOrderEstimateCtrl",
                "templateUrl": "showSalesOrderEstimate",
                "inputs": {
                    "SalesOrderEstimateValue": [{ "salesOrderList": $scope.SelectedCustomerOrderEstimateList }],
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.salesOrderEstimateEsc = false;
                    if (result == "Yes") {
                        var SelectedCustomerOrderEstimete = cacheService.get('SelectedCustomerOrderEstimete');
                        selectedOrderEstimeteProductStockList(SelectedCustomerOrderEstimete);
                        //getSelectedDcItem(result.data);
                        // $scope.disableAddNewDc = true;
                    } else {
                        //var prod = document.getElementById("invoiceNo");
                        //prod.focus();
                    }
                });
            });
            return false;
        } else {
            //$scope.onVendorSelect();
        }
    };

    function selectedOrderEstimeteProductStockList(selectedOrderEstimete) {


        if ($scope.orderEstimateId != null) {
            toastr.info('Sales Order / Estimate Already added');
            return;
        }

        if (selectedOrderEstimete == null || selectedOrderEstimete == "" || selectedOrderEstimete == undefined) {
            $scope.orderEstimateId = null;
        }
        else {
            $scope.orderEstimateId = selectedOrderEstimete.orderEstimateId;
        }
        if (selectedOrderEstimete.salesOrderEstimateType == 1) {
            $scope.Types3 = "Order";
        }
        else {
            $scope.Types3 = "Estimate";
        }
        salesService.getSelectedProductStockList(selectedOrderEstimete.id, $scope.Types3).then(function (response) {

            $scope.selectedEstimateList = response.data;

            for (j = 0; j < $scope.selectedEstimateList.length; j++) {

                if ($scope.selectedEstimateList[j] != "" || $scope.selectedEstimateList[j] != null) {
                    if ($scope.selectedEstimateList[j].orderQty > $scope.selectedEstimateList[j].stock) {
                        if ($scope.selectedEstimateList[j].stock == null || $scope.selectedEstimateList[j].stock == undefined || $scope.selectedEstimateList[j].stock == 0 || $scope.selectedEstimateList[j].stock == "") {
                            $scope.selectedEstimateList[j].orderQty = parseFloat($scope.selectedEstimateList[j].orderQty);
                            $scope.selectedEstimateList[j].pendingTemplateQty = $scope.selectedEstimateList[j].orderQty;
                        }
                        else {
                            $scope.selectedEstimateList[j].quantity = parseFloat($scope.selectedEstimateList[j].stock);
                            $scope.selectedEstimateList[j].pendingTemplateQty = parseFloat($scope.selectedEstimateList[j].orderQty) - parseFloat($scope.selectedEstimateList[j].stock);
                        }
                        $scope.selectedEstimateList[j].salesOrderEstimateId = selectedOrderEstimete.id;
                        $scope.selectedEstimateList[j].salesOrderEstimatePatientId = $scope.selectedEstimateList[0].orderEstimateCutomerId;
                        if ($scope.selectedEstimateList[j].orderEstimateType == 2) {
                            var tmp = $filter("filter")($scope.salesEstimateProductArray, { productId: $scope.selectedEstimateList[j].productId }, true);
                            if (tmp.length == 0) {
                                $scope.salesEstimateProductArray.push($scope.selectedEstimateList[j]);
                            }
                            else {
                                $scope.salesEstimateProductArray[$scope.salesEstimateProductArray.indexOf(tmp[0])].pendingTemplateQty += $scope.selectedEstimateList[j].pendingTemplateQty;
                            }
                        }
                        else {
                            $scope.salesEstimateProductArray.push($scope.selectedEstimateList[j]);
                        }

                    }
                    else {
                        $scope.selectedEstimateList[j].quantity = parseFloat($scope.selectedEstimateList[j].orderQty);
                        $scope.selectedEstimateList[j].availableQty = $scope.selectedEstimateList[j].stock;
                    }

                }
                if ($scope.selectedEstimateList[j].quantity != null) {
                    $scope.selectedEstimateList[j].selectedBatch = $scope.selectedEstimateList[j].batchNo;

                    //Added by Gavaskar on 25-11-2017 Start
                    if ($scope.selectedEstimateList[j].sellingPrice == null || $scope.selectedEstimateList[j].sellingPrice == '' || $scope.selectedEstimateList[j].sellingPrice == undefined) {
                        $scope.selectedEstimateList[j].sellingPrice = 0;
                    }
                    else {
                        $scope.selectedEstimateList[j].sellingPrice = $scope.selectedEstimateList[j].sellingPrice;
                    }
                    if ($scope.salesPriceSettings == 3) {
                        if (parseFloat($scope.selectedEstimateList[j].sellingPrice) > 0) {
                            $scope.selectedEstimateList[j].sellingPrice = parseFloat(($scope.selectedEstimateList[j].returnPurchasePriceWithoutTax * (1 + ($scope.selectedEstimateList[j].gstTotal / 100))).toFixed(6));
                            $scope.selectedEstimateList[j].purchasePriceWithoutTax = $scope.selectedEstimateList[j].returnPurchasePriceWithoutTax;
                        }
                    }
                    //Added by Gavaskar on 25-11-2017 End

                    if ($scope.selectedEstimateList[j].product.productInstance.rackNo == null || $scope.selectedEstimateList[j].product.productInstance.rackNo == '' || $scope.selectedEstimateList[j].product.productInstance.rackNo == undefined) {
                        $scope.selectedEstimateList[j].rackNo = '-';

                    }
                    else {
                        $scope.selectedEstimateList[j].rackNo = $scope.selectedEstimateList[j].product.productInstance.rackNo;
                    }
                    if ($scope.selectedEstimateList[j].product.productInstance.boxNo == null || $scope.selectedEstimateList[j].product.productInstance.boxNo == '' || $scope.selectedEstimateList[j].product.productInstance.boxNo == undefined) {
                        $scope.selectedEstimateList[j].product.boxNo = '-';
                    }
                    else {
                        $scope.selectedEstimateList[j].product.boxNo = $scope.selectedEstimateList[j].product.productInstance.boxNo;
                    }


                    $scope.selectedEstimateList[j].discount = $scope.selectedEstimateList[j].orderEstimateDiscount;
                    $scope.selectedEstimateList[j].productStockId = $scope.selectedEstimateList[j].id;
                    $scope.selectedEstimateList[j].product.id = $scope.selectedEstimateList[j].productId;
                    $scope.selectedEstimateList[j].id = null;

                    $scope.selectedEstimateList[j].salesReturn = false;
                    $scope.selectedEstimateList[j].isNew = false;
                    $scope.orderEstimateType = $scope.selectedEstimateList[j].orderEstimateType;
                    if ($scope.orderEstimateType == 1) {
                        $scope.selectedEstimateList[j].salesOrderProductSelected = 1;
                    }
                    else {
                        $scope.selectedEstimateList[j].salesEstimateProductSelected = 2;
                    }
                    $scope.selectedEstimateList[j].salesOrderEstimateId = selectedOrderEstimete.id;
                    $scope.selectedEstimateList[j].salesOrderEstimatePatientId = $scope.selectedEstimateList[0].orderEstimateCutomerId;

                    if ($scope.selectedEstimateList[j].sellingPrice == 0 || $scope.selectedEstimateList[j].sellingPrice == "" || $scope.selectedEstimateList[j].sellingPrice == undefined) {
                        $scope.selectedEstimateList[j].sellingPrice = $scope.selectedEstimateList[j].mrp;
                    }

                    if ($scope.selectedEstimateList[j].editId == null || $scope.selectedEstimateList[j].editId == undefined) {
                        $scope.selectedEstimateList[j].editId = $scope.editId++;
                    }
                    $scope.selectedEstimateList[j].salesOrderEstimate = true;
                    $scope.sales.salesItem.push($scope.selectedEstimateList[j]);
                }
            }
            if ($scope.selectedEstimateList.length > 0) {
                $scope.sales.name = $scope.selectedEstimateList[0].orderEstimateCutomerName;
                $scope.sales.mobile = $scope.selectedEstimateList[0].orderEstimateCutomerMobile;
                $scope.sales.patientId = $scope.selectedEstimateList[0].orderEstimateCutomerId;
            }

            $scope.customerHelper.data.patientSearchData.mobile = $scope.sales.mobile;
            $scope.customerHelper.data.patientSearchData.name = $scope.sales.name;
            ////if ($scope.customerHelper.data.patientSearchData.mobile != undefined && $scope.customerHelper.data.patientSearchData.mobile != "") {
            ////    $scope.customerHelper.search(false, 2, "No", 0, 1);
            ////}

            $scope.sales.salesEstimateId = selectedOrderEstimete.id;
            orderEstimateTemplateMandatory();

            if ($scope.sales.salesItem.length > 0) {
                for (var j = 0; j < $scope.sales.salesItem.length; j++) {
                    if ($scope.sales.salesItem[j].discount > 0) {
                        $scope.discountInValid = true;
                    }
                }
            }

        }, function () {
        });

    };

    // Added by Gavaskar 10-10-2017 Start Sales Order , Estimate ,Template Convert to Sales End

    var LeadsRequiredquantity = "";
    $scope.LeadsProductSelected = false;
    $scope.AddItemtoSave = function (obj, index) {
        $scope.LeadProductIndex = "";
        $scope.LeadProductId = "";
        $scope.LeadsProductSelected = false;
        var productname = obj.name;
        LeadsRequiredquantity = obj.Totaltablets;
        var val = productname.split(" ");
        productStockService.getSalesProduct(val[0]).then(function (response) {
            $scope.tempproductlistArray = [];
            $scope.tempproductlistArray = response.data;
            if ($scope.tempproductlistArray.length == 0) {
                var threecharVal = productname.slice(0, 3);
                productStockService.getSalesProduct(threecharVal).then(function (response) {
                    $scope.tempproductlistArray = [];
                    $scope.tempproductlistArray = response.data;
                    $scope.enableTempProductsPopup = true;
                    var m = ModalService.showModal({
                        "controller": "tempproductsCtrl",
                        "templateUrl": 'TempProductDetails',
                        "inputs": {
                            "tempproductlistArray": $scope.tempproductlistArray,
                            "productName": productname,
                            "Requiredquantity": LeadsRequiredquantity
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            $scope.message = "You said " + result;
                            if (result == "Yes") {
                                $scope.LeadproductSelection(true, index, obj.id);
                            }
                        });
                    });
                    return false;
                }, function (error) {
                    console.log(error);
                });
            } else {
                $scope.enableTempProductsPopup = true;
                var m = ModalService.showModal({
                    "controller": "tempproductsCtrl",
                    "templateUrl": 'TempProductDetails',
                    "inputs": {
                        "tempproductlistArray": $scope.tempproductlistArray,
                        "productName": productname,
                        "Requiredquantity": LeadsRequiredquantity
                    }
                }).then(function (modal) {
                    modal.element.modal();
                    modal.close.then(function (result) {
                        $scope.message = "You said " + result;
                        if (result == "Yes") {
                            $scope.LeadproductSelection(true, index, obj.id);
                        }
                    });
                });
                return false;
            }
            //productId: $scope.selectedProduct.product.id,
            //productName: $scope.selectedProduct.name,
            //items: $scope.sales.salesItem,
            //enableWindow: $scope.enablePopup
        }, function (error) {
            console.log(error);
        });
    };
    $scope.DoctorsList = function (val) {
        if ($scope.DoctorNameSearch == "" || $scope.DoctorNameSearch == undefined) {
            $scope.DoctorNameSearch = 2;
        }
        if ($scope.DoctorNameSearch == 1) {
            return doctorService.Locallist(val).then(function (response) {
                //return response.data;
                var origArr = response.data;
                var newArr = [], origLen = origArr.length, found, x, y;
                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }
                return newArr.map(function (item) {
                    return item;
                });
            }, function (error) {
                console.log(error);
            });
        } else {
            return doctorService.list(val).then(function (response) {
                var origArr = response.data;
                var newArr = [], origLen = origArr.length, found, x, y;
                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }
                return newArr.map(function (item) {
                    return item;
                });
            }, function (error) {
                console.log(error);
            });
        }
    };
    $scope.onDoctorSelect = function (obj, event) {
        $scope.sales.doctorMobile = obj.mobile;
        $scope.sales.doctorid = obj.id;
        $scope.shortDoctorname = obj; //Added by Sarubala on 29-11-17
        ele = document.getElementById("doctorMobile");
        ele.focus();
        $scope.isdoctornameMandatory = false;
    };

    //Modified by Sarubala on 20-10-17
    //getPrintType = function () {
    //    salesService.getPrintType().then(function (response) {
    //        $scope.isDotMatrix = response.data;
    //        window.localStorage.setItem("IsDotMatrix", JSON.stringify($scope.isDotMatrix));
    //    }, function () {
    //    });
    //};
    //getPrintType();
    var seperator = "/";
    $scope.isInvoicedatevalid = false;
    $("#txtBillDate").keyup(function (e) {
        if ($(this).val() == "") {
            $scope.isInvoicedatevalid = false;
        } else {
            if ($(this).val().length == 2 || $(this).val().length == 5) {
                if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                    $(this).val($(this).val() + "/");
            }
            //else if ($(this).val().length == 10) {
            //    var value = $(this).val();
            //            var d = moment(value, 'DD/MM/YYYY');
            //            console.log(d);
            //            if (d == null || !d.isValid()) {
            //                $scope.isInvoicedatevalid = true;
            //                return false;
            //            } else {
            //               // validatedate(value);
            //                return true;
            //            }
            //        }
            var value = $(this).val();
            var splits = value.split("/");
            if (splits[1] > "12") {
                $scope.isInvoicedatevalid = true;
                return false;
            } else {
                $scope.isInvoicedatevalid = false;
            }
            if (splits[1] <= "12" && splits[0] > "31") {
                $scope.isInvoicedatevalid = true;
                return false;
            } else {
                $scope.isInvoicedatevalid = false;
            }
        }
    });
    //Lawrence
    //$scope.validatedate = function (event) {
    //    var cheque = event.target.id; //chequeDate
    //    var card = event.target.id;//cardDate        
    //    if (cheque.length == 10 && cheque == "chequeDate") {
    //        validatedate(val);
    //    }
    //    if (card.length == 10 && card == "cardDate") {
    //        validatedate(val);
    //    }
    //};
    $("#cardDate").keyup(function (e) {
        $scope.day = $filter('date')(new Date(), 'dd');
        $scope.month = $filter('date')(new Date(), 'MM');
        $scope.year = $filter('date')(new Date(), 'yyyy');
        if ($(this).val() == "") {
            $scope.isCardDateValid = false;
        } else {
            if ($(this).val().length == 2 || $(this).val().length == 5) {
                if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                    $(this).val($(this).val() + "/");
            }
            var value = $(this).val();
            var splits = value.split("/");
            if ((parseInt(splits[0]) > 31) || (parseInt(splits[1]) > 12) || (parseInt(splits[2]) < parseInt($scope.year))) {
                $scope.isCardDateValid = true;
                $scope.$apply();
                return false;
            } else {
                $scope.isCardDateValid = false;
                $scope.$apply();
            }
            if (parseInt(splits[2]) <= parseInt($scope.year) && splits[2] != "") {
                if ((parseInt(splits[1]) < parseInt($scope.month)) && splits[1] != "") { //&& (parseInt(splits[1]) != parseInt($scope.month)) 
                    $scope.isCardDateValid = true;
                    $scope.$apply();
                    return false;
                }
                if ((parseInt(splits[1]) >= parseInt($scope.month)) && (splits[1] != "")) {
                    $scope.isCardDateValid = false;
                    $scope.$apply();
                    if (parseInt(splits[2]) == parseInt($scope.year)
                        && (parseInt(splits[1]) == parseInt($scope.month))
                        && (parseInt(splits[0]) < parseInt($scope.day))
                        && (parseInt(splits[0]) != parseInt($scope.day))) {
                        $scope.isCardDateValid = true;
                        $scope.$apply();
                        return;
                    }
                    if (parseInt(splits[2]) == parseInt($scope.year) && (parseInt(splits[1]) < parseInt($scope.month))) {
                        $scope.isCardDateValid = true;
                        $scope.$apply();
                        return;
                    }
                    return;
                } else {
                    $scope.isCardDateValid = true;
                    $scope.$apply();
                    return;
                }
            } else {
                $scope.isCardDateValid = false;
                return true;
            }
        }
    });
    $("#chequeDate").keyup(function (e) {
        $scope.day = $filter('date')(new Date(), 'dd');
        if ($(this).val() == "") {
            $scope.isChequeDateValid = false;
        } else {
            if ($(this).val().length == 2 || $(this).val().length == 5) {
                if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                    $(this).val($(this).val() + "/");
            }
            var value = $(this).val();
            var splits = value.split("/");
            if (splits[0] > "31") {
                $scope.isChequeDateValid = true;
                return false;
            } else {
                $scope.isChequeDateValid = false;
            }
            if (splits[1] > "12") {
                $scope.isChequeDateValid = true;
                return false;
            } else {
                $scope.isChequeDateValid = false;
            }
            if (splits[1] <= "12" && splits[0] > "31") {
                $scope.isChequeDateValid = true;
                return false;
            } else {
                $scope.isChequeDateValid = false;
            }
        }
    });
    //Lawrence
    function validatedate(val) {
        var splits = val.split("/");
        var dt = new Date(splits[1] + "/" + splits[0] + "/" + splits[2]);
        if (dt == "Invalid Date") {
            $scope.isInvoicedatevalid = true;
            return false;
        } else {
            if (dt.getDate() == splits[0] && dt.getMonth() + 1 == splits[1] && dt.getFullYear() == splits[2]) {
                $scope.isInvoicedatevalid = false;
            } else {
                $scope.isInvoicedatevalid = true;
                return;
            }
            FutureDateValidation(dt);
        }
    }
    function FutureDateValidation(dt) {
        var dtToday = new Date();
        var pastDate = new Date(Date.parse(dtToday.getMonth() + "/" + dtToday.getDate() + "/" + parseInt(dtToday.getFullYear() - 100)));
        if (dt < pastDate || dt >= dtToday) {
            $scope.isInvoicedatevalid = true;
        } else {
            $scope.isInvoicedatevalid = false;
        }
    }

    //Modified by Sarubala on 20-10-17
    //getIsMaxDiscountAvail();
    //$scope.maxDiscountFixed = "No";
    //function getIsMaxDiscountAvail() {
    //    salesService.getIsMaxDiscountAvail().then(function (response) {
    //        if (response.data == "" || response.data == null || response.data == undefined) {
    //            $scope.maxDiscountFixed = "No";
    //        } else {
    //            if (response.data.maxDiscountAvail != undefined) {
    //                $scope.maxDiscountFixed = response.data.maxDiscountAvail;
    //            } else {
    //                $scope.maxDiscountFixed = "No";
    //            }
    //        }
    //        if ($scope.maxDiscountFixed == "Yes") {
    //            getMaxDiscountValue();
    //        }
    //    }, function () {
    //    });
    //}
    $scope.maxDisountValue = "";
    function getMaxDiscountValue() {
        salesService.getMaxDiscountValue().then(function (response) {
            if (response.data != "" && response.data != null) {
                $scope.maxDisountValue = response.data.instanceMaxDiscount;
            }
        }, function () {
        });
    }
    $scope.billList = [];
    $scope.lastSaleId = "";
    $scope.printBill = function () {
        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        $scope.search.searchProductId = "";
        //By San
        salesService.getLastSalesId($scope.search).then(function (response) {
            if (response.data != undefined && response.data != "") {
                $scope.lastSaleId = response.data.id;
                printingHelper.printInvoice($scope.lastSaleId);
            } else {
                alert("There is no previous sales available for this Pharmacy.");
            }
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    $scope.chkPrint = function () {
        $scope.sales.billPrint = $scope.chkPrintValue;
    };
    //
    $scope.AddJson = function () {
        $scope.conditionsarray = [];
        $scope.TreatmentArray = [];
        $scope.TreatmentArray = [
            {
                "Treatment": "Treatment one",
                "Treatment Cost": "1500"
            },
             {
                 "Treatment": "Treatment two",
                 "Treatment Cost": "1000"
             }
        ];
        $scope.conditionsarray = [
            {
                "Condition": "New Condition1",
                "Condition Treatment": $scope.TreatmentArray
            },
             {
                 "Condition": "New Condition2",
                 "Condition Treatment": $scope.TreatmentArray
             }
        ];
        var JsonObj = {
            "Date": "12/03/17",
            "ConsultationType": "New",
            "Conditions": $scope.conditionsarray
        };
        salesService.saveJsonData(JSON.stringify(JsonObj)).then(function (response) {
        }, function () {
        });
    };
    $scope.getProduct = function () {
        if ($scope.scanBarcodeOption.scanBarcode === "1") {

            var eleId = document.getElementById("drugName");
            var scannedEancode = eleId.value
            if (($scope.selectedProduct === undefined || typeof ($scope.selectedProduct) !== "object" || $scope.selectedProduct === null) && eleId.value !== "") {
                productStockService.getEancodeExistData(scannedEancode).then(function (eanRes) {
                    if (eanRes.data.length !== 0) {
                        productStockService.getSalesProduct(eanRes.data[0].id).then(function (response) {
                            if (response.data.length === 1) {
                                $scope.scanBarcodeVal = eleId.value;
                                $scope.selectedProduct = response.data[0];
                                $scope.onProductSelect();
                            } else if (response.data.length === 0) {
                                $scope.eancodeAlert = "Stock not available";
                            }
                        });
                    } else {
                        $scope.eancodeAlert = "Product not found";
                    }
                });
            }
        }
    };
    //Modified by Sarubala on 20-10-17
    //$scope.getScanBarcodeOption = function () {
    //    salesService.getScanBarcodeOption().then(function (resp) {
    //        $scope.scanBarcodeOption = resp.data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //};
    //$scope.getScanBarcodeOption();

    //Modified by Sarubala on 20-10-17
    //$scope.isDepartmentsave = "0";
    //getIsDepartmentadded();
    //function getIsDepartmentadded() {
    //    salesService.getIsDepartmentadded().then(function (response) {
    //        if (response.data == "" || response.data == null || response.data == undefined) {
    //            $scope.isDepartmentsave = "0";
    //        } else {
    //            if (response.data.patientTypeDept != undefined) {
    //                $scope.isDepartmentsave = "0";
    //                if (response.data.patientTypeDept == true) {
    //                    $scope.isDepartmentsave = "1";
    //                }
    //            } else {
    //                $scope.isDepartmentsave = "0";
    //            }
    //        }
    //    }, function () {
    //    });
    //}
    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        salesService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    };
    $scope.getEnableSelling();


    function getIsSalesTemplateEnabled() {
        salesService.getIsSalesTemplateEnabled().then(function (response) {
            $scope.isSalesTemplateEnabled = response.data;
        }, function () {
        });
    }
    getIsSalesTemplateEnabled();

    // Gavaskar Added Loyalty Points 12-12-2017 Start

    $scope.getLoyaltyPointSettings = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        salesService.getLoyaltySettings().then(function (response) {

            $scope.loyaltyPointSettings = response.data;
            $scope.loyaltyPointSettings.maximumRedeemPoint = $scope.loyaltyPointSettings.maximumRedeemPoint >= 0 ? $scope.loyaltyPointSettings.maximumRedeemPoint : 0;
            $scope.loyaltyPointSettings.isMaximumRedeem = $scope.loyaltyPointSettings.isMaximumRedeem == true ? $scope.loyaltyPointSettings.isMaximumRedeem : false;
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        });
    }

    if ($scope.salesEditId == null || $scope.salesEditId == undefined || $scope.salesEditId == "") {

        $scope.getLoyaltyPointSettings();
    }


    $scope.isRedeemPts = false;
    $scope.isRedeemValue = false;
    $scope.focusRedeemPts = false;

    $rootScope.$on("RedeemPointsEnable", function () {

        //if ($scope.Editsale == true)

        if (redeemEnable.checked == true) {

            if ($scope.Editsale == true) {
                if (customerHelper.data.selectedCustomer.id != null) {
                    salesService.getfirstLoyaltyCheck(customerHelper.data.selectedCustomer.id).then(function (response) {

                        if (response.data == 1) {
                            $scope.isRedeemPts = false;
                            $scope.isRedeemValue = false;
                        }
                        else {
                            $scope.isRedeemPts = true;
                            $scope.isRedeemValue = true;

                            if ($scope.FinalNetAmount > 0) {
                                if (customerHelper.data.selectedCustomer.loyaltyPoint <= $scope.FinalNetAmount) {
                                    var arr = String(customerHelper.data.selectedCustomer.loyaltyPoint).split(".");
                                    $scope.sales.redeemPts = arr[0];
                                    $scope.changeRedeemValue($scope.sales.redeemPts);
                                }
                                else {
                                    $scope.sales.redeemAmt = "0.00";
                                }

                            }
                            else {
                                $scope.sales.redeemAmt = "0.00";
                            }

                            document.getElementById("redeemPts").focus();
                            $scope.focusRedeemPts = true;


                        }
                    }, function (error) {
                        console.log(error);
                    });
                }


            }
            else {
                $scope.isRedeemPts = true;
                $scope.isRedeemValue = true;

                if ($scope.FinalNetAmount > 0) {
                    if (customerHelper.data.selectedCustomer.loyaltyPoint <= $scope.FinalNetAmount) {
                        var arr = String(customerHelper.data.selectedCustomer.loyaltyPoint).split(".");
                        $scope.sales.redeemPts = arr[0];
                        $scope.changeRedeemValue($scope.sales.redeemPts);
                    }
                    else {
                        $scope.sales.redeemAmt = "0.00";
                    }

                }
                else {
                    $scope.sales.redeemAmt = "0.00";
                }

                document.getElementById("redeemPts").focus();
                $scope.focusRedeemPts = true;

            }

        }
        else {
            $scope.isRedeemPts = false;
            $scope.isRedeemValue = false;
            $scope.sales.redeemPts = "";
            $scope.sales.redeemAmt = "0.00";
            $scope.focusRedeemPts = false;

            var salesAmt = 0;
            var returnAmt = 0;

            for (var j = 0; j < $scope.sales.salesItem.length; j++) {
                if ((parseFloat($scope.sales.salesItem[j].discount) >= 0) && (parseFloat($scope.sales.salesItem[j].discount) < 100)) {
                    var discountAmt1 = $scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity * parseFloat($scope.sales.salesItem[j].discount) / 100;
                    if ($scope.sales.salesItem[j].salesReturn) {
                        returnAmt += (($scope.sales.salesItem[j].sellingPrice * Math.abs($scope.sales.salesItem[j].quantity)) + discountAmt1);
                    }
                    else {
                        salesAmt += (($scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity) - discountAmt1);
                    }
                }
            }

            $scope.sales.redeemPercent = "";
            $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat($scope.discountedTotalNetValue) - parseFloat($scope.sales.redeemAmt);
            $scope.roundoffNetAmount = Math.round($scope.FinalNetAmount) - $scope.FinalNetAmount;
            $scope.FinalNetAmount = Math.round($scope.FinalNetAmount);

        }


    });

    $scope.changeRedeemValue = function (redeemValue) {

        var salesAmt = 0;
        var returnAmt = 0;
        var discountAmt1 = 0;
        var tempdiscountAmt = 0;
        var overallDiscAmt = 0;

        for (var j = 0; j < $scope.sales.salesItem.length; j++) {
            if ((parseFloat($scope.sales.salesItem[j].discount) >= 0) && (parseFloat($scope.sales.salesItem[j].discount) < 100)) {
                tempdiscountAmt = $scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity * parseFloat($scope.sales.salesItem[j].discount) / 100;
                discountAmt1 += tempdiscountAmt;
                if ($scope.sales.salesItem[j].salesReturn) {
                    returnAmt += (($scope.sales.salesItem[j].sellingPrice * Math.abs($scope.sales.salesItem[j].quantity)) + tempdiscountAmt);
                }
                else {
                    salesAmt += (($scope.sales.salesItem[j].sellingPrice * $scope.sales.salesItem[j].quantity) - tempdiscountAmt);
                }
            }
        }

        if ((parseFloat($scope.sales.discount) > 0 || parseFloat($scope.sales.discountValue) > 0) && $scope.discountedTotalNetValue > 0) {
            overallDiscAmt = $scope.discountedTotalNetValue;
        }

        if ($scope.FinalNetAmount > 0) {
            if (parseFloat(redeemValue) > 0) {
                $scope.sales.redeemAmt = $scope.loyaltyPointSettings.redeemValue * redeemValue / $scope.loyaltyPointSettings.redeemPoint;

                if ($scope.sales.redeemAmt == '' || $scope.sales.redeemAmt == null) {
                    $scope.sales.redeemAmt = "0.00";
                }

                // $scope.sales.redeemPercent = (parseFloat($scope.sales.redeemAmt) / (parseFloat($scope.discountedTotalNetValue) + parseFloat(salesAmt))) * 100

                if ($scope.returnDiscountInValid && $scope.returnAmount > 0) {

                    if (discountAmt1 > 0) {
                        $scope.sales.redeemPercent = (parseFloat($scope.sales.redeemAmt) / (parseFloat(salesAmt) + parseFloat(overallDiscAmt))) * 100;
                        $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat($scope.sales.redeemAmt);
                    }
                    else {
                        $scope.sales.redeemPercent = (parseFloat($scope.sales.redeemAmt) / parseFloat(salesAmt)) * 100;
                        $scope.salesAmount = parseFloat(salesAmt) - parseFloat($scope.sales.redeemAmt);
                        $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat(overallDiscAmt) - parseFloat($scope.sales.redeemAmt) - parseFloat(returnAmt);
                    }
                }
                else {
                    if (discountAmt1 > 0) {
                        $scope.sales.redeemPercent = (parseFloat($scope.sales.redeemAmt) / (parseFloat(salesAmt) + parseFloat(overallDiscAmt))) * 100;
                        // $scope.FinalNetAmount = $scope.FinalNetAmount = parseFloat(salesAmt)  - parseFloat($scope.sales.redeemAmt);
                        $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat($scope.sales.redeemAmt);
                        // -parseFloat(returnAmt)
                    }
                    else {
                        $scope.sales.redeemPercent = (parseFloat($scope.sales.redeemAmt) / parseFloat(salesAmt)) * 100;
                        $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat($scope.discountedTotalNetValue) - parseFloat($scope.sales.redeemAmt);
                        //-parseFloat(returnAmt)
                    }
                }


                if ($scope.Editsale == true) {
                    if ($scope.isEnableRoundOff == true) {
                        $scope.roundoffNetAmount = Math.round($scope.FinalNetAmount) - $scope.FinalNetAmount;
                        $scope.FinalNetAmount = Math.round($scope.FinalNetAmount);
                    } else {
                        $scope.roundoffNetAmount = $scope.FinalNetAmount - $scope.FinalNetAmount;
                    }
                }
                else {
                    if ($scope.isEnableRoundOff == true) {
                        //$scope.sales.netAmount = parseFloat($scope.FinalNetAmount) - parseFloat($scope.sales.redeemAmt);
                        $scope.roundoffNetAmount = Math.round($scope.FinalNetAmount) - $scope.FinalNetAmount;
                        $scope.FinalNetAmount = Math.round($scope.FinalNetAmount);
                    }
                    else {
                        $scope.roundoffNetAmount = $scope.FinalNetAmount - $scope.FinalNetAmount;
                    }
                }

            }
            else {
                $scope.sales.redeemAmt = "0.00";
                $scope.sales.redeemPercent = 0;
                if ($scope.returnDiscountInValid && $scope.returnAmount > 0) {
                    if (discountAmt1 > 0) {
                        $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat(returnAmt);
                    }
                    else {
                        $scope.salesAmount = parseFloat(salesAmt);
                        $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat(returnAmt) - parseFloat(overallDiscAmt);
                    }
                }
                else {
                    if (discountAmt1 > 0) {
                        $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat(returnAmt);
                    }
                    else {
                        $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat(returnAmt) - parseFloat(overallDiscAmt);
                    }
                }

                if ($scope.isEnableRoundOff == true) {
                    $scope.roundoffNetAmount = Math.round($scope.FinalNetAmount) - $scope.FinalNetAmount;
                    $scope.FinalNetAmount = Math.round($scope.FinalNetAmount);
                } else {
                    $scope.roundoffNetAmount = $scope.FinalNetAmount - $scope.FinalNetAmount;
                }


            }
        }
        else {

            $scope.sales.redeemAmt = $scope.loyaltyPointSettings.redeemValue * redeemValue / $scope.loyaltyPointSettings.redeemPoint;
            if ($scope.returnDiscountInValid && $scope.returnAmount > 0) {
                if (discountAmt1 > 0) {
                    $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat(returnAmt) - parseFloat($scope.sales.redeemAmt);
                }
                else {
                    $scope.salesAmount = parseFloat(salesAmt);
                    $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat(returnAmt) - parseFloat(overallDiscAmt) - parseFloat($scope.sales.redeemAmt);
                }
            }
            else {
                if (discountAmt1 > 0) {
                    $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat(returnAmt) - parseFloat($scope.sales.redeemAmt);
                }
                else {
                    $scope.FinalNetAmount = parseFloat(salesAmt) - parseFloat(returnAmt) - parseFloat(overallDiscAmt) - parseFloat($scope.sales.redeemAmt);
                }
            }

            if ($scope.isEnableRoundOff == true) {
                $scope.roundoffNetAmount = Math.round($scope.FinalNetAmount) -$scope.FinalNetAmount;
                $scope.FinalNetAmount = Math.round($scope.FinalNetAmount);
            } else {
                $scope.roundoffNetAmount = $scope.FinalNetAmount -$scope.FinalNetAmount;
            }


    }

        $scope.redeemError = false;
        $scope.loyaltyError = false;
        $scope.netAmtError = false;
        $scope.returnRedeemError = false;
        if ($scope.salesEditId) {
            if (parseFloat($scope.loyaltyPointSettings.maximumRedeemPoint) < parseFloat($scope.sales.redeemPts)) {
                $scope.redeemError = true;
        }
            if (parseFloat($scope.customerHelper.data.selectedCustomer.loyaltyPoint) < parseFloat($scope.sales.redeemPts)) {
                if (parseFloat($scope.sales.redeemPts) > parseFloat($scope.oldRedeemPoint)) {
                    $scope.loyaltyError = true;
            }
        }

        } else {
            if (parseFloat($scope.customerHelper.data.selectedCustomer.loyaltyPoint) < parseFloat($scope.sales.redeemPts)) {
                $scope.loyaltyError = true;
        }
            if ((parseFloat($scope.loyaltyPointSettings.maximumRedeemPoint) < parseFloat($scope.sales.redeemPts)) && $scope.loyaltyPointSettings.isMaximumRedeem) {
                $scope.redeemError = true;
        }
    }

        if ($scope.FinalNetAmount < 0 && $scope.sales.redeemAmt > 0) {
            $scope.netAmtError = true;
    }

        $scope.returnRedeemPt = 0;
        if ($scope.loyaltyPointSettings) {
            if ($scope.loyaltyPointSettings.loyaltyProductPoints) {

                angular.forEach($scope.sales.salesItem, function (value, key) {
                    value.isProductPointCalculated = false;
            });

                for (var x = 0; x < $scope.sales.salesItem.length; x++) {
                    if ($scope.sales.salesItem[x].salesReturn) {
                        for (var y = 0; y < $scope.loyaltyPointSettings.loyaltyProductPoints.length; y++) {
                            if ($scope.sales.salesItem[x].product.kindName == $scope.loyaltyPointSettings.loyaltyProductPoints[y].kindName) {
                                tempReturnPt = parseFloat($scope.sales.salesItem[x].totalAmount) * parseFloat($scope.loyaltyPointSettings.loyaltyPoint) * parseFloat($scope.loyaltyPointSettings.loyaltyProductPoints[y].kindOfProductPts) / parseFloat($scope.loyaltyPointSettings.loyaltySaleValue);
                                $scope.sales.salesItem[x].isProductPointCalculated = true;
                                $scope.returnRedeemPt = $scope.returnRedeemPt +tempReturnPt;
                        }
                    }

                        if ($scope.sales.salesItem[x].isProductPointCalculated == false) {
                            tempReturnPt = parseFloat($scope.sales.salesItem[x].totalAmount) * parseFloat($scope.loyaltyPointSettings.loyaltyPoint) / parseFloat($scope.loyaltyPointSettings.loyaltySaleValue);
                            $scope.sales.salesItem[x].isProductPointCalculated = true;
                            $scope.returnRedeemPt = $scope.returnRedeemPt +tempReturnPt;
                    }
                }
            }

            } else {
                for (var x = 0; x < $scope.sales.salesItem.length; x++) {
                    var tempReturnPt = parseFloat($scope.sales.salesItem[x].totalAmount) * parseFloat($scope.loyaltyPointSettings.loyaltyPoint) / parseFloat($scope.loyaltyPointSettings.loyaltySaleValue);
                    $scope.returnRedeemPt = $scope.returnRedeemPt +tempReturnPt;
            }
        }
    }

        if ($scope.returnRedeemPt > 0) {
            $scope.avilableRedeemPt = 0;
            if ($scope.salesEditId) {
                var tempRedeemPt = parseFloat($scope.customerHelper.data.selectedCustomer.loyaltyPoint) +parseFloat($scope.oldRedeemPoint) -$scope.returnRedeemPt;
                if (tempRedeemPt < parseFloat($scope.sales.redeemPts)) {
                    $scope.avilableRedeemPt = tempRedeemPt;
                    $scope.returnRedeemError = true;
            }
            } else {
                var tempRedeemPt = parseFloat($scope.customerHelper.data.selectedCustomer.loyaltyPoint) -$scope.returnRedeemPt;
                if (tempRedeemPt < parseFloat($scope.sales.redeemPts)) {
                    $scope.avilableRedeemPt = tempRedeemPt;
                    $scope.returnRedeemError = true;
            }
        }
            if ($scope.avilableRedeemPt < 0) {
                $scope.avilableRedeemPt = 0;
        }

    }

        angular.forEach($scope.sales.salesItem, function (value, key) {
            if (parseFloat(redeemValue) > 0) {
                value.redeemAmtPerItem = ($scope.sales.redeemPercent * value.totalAmount / 100).toFixed(2);
        }
    });

        $scope.changeCashType();
        $scope.focusCreditTextBox = false;
        document.getElementById("redeemPts").focus();
};

    // Gavaskar Added Loyalty Points 12-12-2017 End


    //Modified by Sarubala on 23-10-17
    ////added by nandhini for hide the update gstin button
    //$scope.getExistingGSTIn = function () {
    //    salesService.getGSTINDetail().then(function (response) {
    //        $scope.gstIn = response.data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //};
    ////end
    //$scope.getExistingGSTIn();
    $scope.getGSTIN = function () {
        salesService.getGSTINDetail().then(function (response) {
            var m = ModalService.showModal({
                "controller": "updateGSTINPopupCtrl",
                "templateUrl": 'updateGSTINPopup',
                "inputs": {
                    "gstIn": response.data
            }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.message = "You said " +result;
            });
        });
            return false;
        }, function (error) {
            console.log(error);
    });
};

    // Added by San - Customer/ProductWiseDiscount
    function getProductwisDiscounteSettings() {
        salesService.getDiscountDetail().then(function (response) {
            $scope.discountRules = response.data;

            if (!angular.isUndefinedOrNull($scope.batchList[0].product.discount)) {
                $scope.selectedBatch.discount = $scope.batchList[0].product.discount;
            }
            else {
                $scope.selectedBatch.discount = 0;
        }

            if ($scope.discountRules.billAmountType == "productWiseDiscount") {
                $scope.changediscountsale($scope.selectedBatch.discount, true);
            }
            else if ($scope.discountRules.billAmountType == "productCustomerWiseDiscount") {
                getProductCustomerwiseDiscount();
            }
            else
                $scope.selectedBatch.discount = "";

        }, function () {
    });
}
    // Added by San - Customer/ProductWiseDiscount
    function getProductCustomerwiseDiscount() {

        if (!angular.isUndefinedOrNull($scope.batchList[0].product.discount)) {
            $scope.selectedBatch.discount = $scope.batchList[0].product.discount;

            if ($scope.customerHelper.data.selectedCustomer.discount >= 0) {
                if ($scope.customerHelper.data.selectedCustomer.discount < $scope.selectedBatch.discount)
                    $scope.selectedBatch.discount = $scope.customerHelper.data.selectedCustomer.discount;

                $scope.changediscountsale($scope.selectedBatch.discount, true);
            }
            else {
                //if ($scope.maxDiscountFixed == "Yes" && $scope.maxDisountValue < $scope.batchList[0].product.discount) {
                //    $scope.selectedBatch.discount = $scope.maxDisountValue;
                //    $scope.changediscountsale($scope.selectedBatch.discount, true);
                //}
                //else
                //    $scope.changediscountsale($scope.selectedBatch.discount, true);

                $scope.selectedBatch.discount = 0;
                $scope.changediscountsale($scope.selectedBatch.discount, true);
        }
        }
        else {
            if ($scope.customerHelper.data.selectedCustomer.discount >= 0) {
                $scope.selectedBatch.discount = $scope.customerHelper.data.selectedCustomer.discount;
                $scope.changediscountsale($scope.selectedBatch.discount, true);
            }
            else {
                $scope.selectedBatch.discount = 0;
                $scope.changediscountsale($scope.selectedBatch.discount, true);
        }
    }

}

    // Added by San - Change Customer in Sales Edit
    function editProductCustomerwisDiscount() {
        salesService.getDiscountDetail().then(function (response) {
            $scope.discountRules = response.data;

            if ($scope.discountRules.billAmountType == "productCustomerWiseDiscount") {
                if ($scope.customerHelper.data.selectedCustomer.discount >= 0) {
                    for (var i = 0; i < $scope.sales.salesItem.length; i++) {

                        if ($scope.customerHelper.data.selectedCustomer.discount < $scope.sales.salesItem[i].product.discount)
                            $scope.sales.salesItem[i].discount = $scope.customerHelper.data.selectedCustomer.discount;
                        else
                            //$scope.sales.salesItem[i].discount = angular.isUndefinedOrNull($scope.sales.salesItem[i].product.discount) ? 0 : $scope.sales.salesItem[i].product.discount;
                            $scope.sales.salesItem[i].discount = 0;
                }

                    $scope.BillCalculations();
                    $scope.DiscountCalculations1();
                    $scope.loadEditSalesFocus();
            }
            }
            else
                $scope.selectedBatch.discount = "";

        }, function () {
    });
}

    // Added by San 
    angular.isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null || val === ""
}
    // Added by San 
    angular.isUndefinedOrNullZero = function (val) {
        return angular.isUndefined(val) || val === null || val === "" || val === 0
}

    function rearrangeBatchList(batchList1, batchList2) {
        var tmpList =[];
        for (var j = 0; j < batchList2.length; j++) {
            tmpList.push(batchList2[j]);
    }
        for (var i = 0; i < batchList1.length; i++) {
            var exist = false;
            for (var j = 0; j < batchList2.length; j++) {
                if (batchList1[i] == batchList2[j]) {
                    exist = true;
            }
        }
            if (!exist) {
                tmpList.push(batchList1[i]);
        }
    }
        return tmpList;
};

    function rearrangeBatchList1(batchList1, batchList2) {
        var tmpList =[];
        tmpList.push(batchList2);
        for (var i = 0; i < batchList1.length; i++) {
            var exist = false;
            if (batchList1[i] == batchList2) {
                exist = true;
        }
            if (!exist) {
                tmpList.push(batchList1[i]);
        }
    }
        return tmpList;
};

    function loadAutoQty() {
        $scope.scanBarcodeVal = "";

        //Changed by Sarubala on 23-02-2019 to fix Bar-Code issue

        //    if ($scope.scanBarcodeOption.autoScanQty != 0 && $scope.scanBarcodeOption.autoScanQty != undefined && $scope.scanBarcodeOption.autoScanQty != null) {
        //        $scope.selectedBatch.quantity = $scope.scanBarcodeOption.autoScanQty;
        //        angular.element("#spnquantity").attr("data-quantityId", $scope.selectedBatch.quantity);
        //        angular.element("#spnquantity").attr("data-price", $scope.selectedBatch.sellingPrice);
        //        angular.element("#spnquantity").attr("data-discount", $scope.selectedBatch.discount);
        //        document.getElementById("quantity").value = $scope.selectedBatch.quantity;
        //        $scope.validateQty();
        //        if ($scope.Quantityexceeds != 1) {
        //            // if($scope.Editsale==false){
        //            $scope.addSales();
        //            // }
        //            angular.element("#spnquantity").attr("data-quantityId", "");
        //            angular.element("#spnquantity").attr("data-price", "");
        //            angular.element("#spnquantity").attr("data-discount", "");
        //            document.getElementById("quantity").value = "";
        //    }
        //}

        if ($scope.scanBarcodeOption.autoScanQty != 0 && $scope.scanBarcodeOption.autoScanQty != undefined && $scope.scanBarcodeOption.autoScanQty != null) {
            $scope.selectedBatch.quantity = $scope.scanBarcodeOption.autoScanQty;
            angular.element("#spnquantity").attr("data-quantityId", $scope.selectedBatch.quantity);
            angular.element("#spnquantity").attr("data-price", $scope.selectedBatch.sellingPrice);
            angular.element("#spnquantity").attr("data-discount", $scope.selectedBatch.discount);
            document.getElementById("quantity").value = $scope.selectedBatch.quantity;
            $scope.validateQty();            
        }
};

    $scope.getTaxValues = function () {
        vendorPurchaseService.getAllTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
    });
};
    $scope.getTaxValues();

    $scope.keyEnterInManField = function (currentid, nextid) {
        var ele = document.getElementById(currentid)
        if (ele != null) {
            if (currentid == "gstTotal") {
                if ($scope.selectedBatch.gstTotal == null) {
                    return;
            }
                if ($scope.taxValuesList.length > 0) {
                    var taxList = $filter("filter") ($scope.taxValuesList, { tax: parseFloat($scope.selectedBatch.gstTotal) }, true);
                    if (taxList.length == 0) {
                        return;
                }
            }
        }
            if (ele.value == null || ele.value == "") {
                return;
        }
    }
        ele = document.getElementById(nextid);
        if (ele != null) {
            ele.focus();
    }
};

    function setModelValues() {
        if ($scope.sales.salesReturn == null) {
            $scope.sales.salesReturn = {
        };
    }
        $scope.sales.isRoundOff = $scope.isEnableRoundOff;
        $scope.sales.salesReturn.isRoundOff = $scope.isEnableRoundOff;
        $scope.sales.totalDiscountValue = $scope.discountedTotalNetValue;
        $scope.sales.salesItemAmount = 0;
        $scope.sales.discount = $scope.sales.discount || 0;
        $scope.sales.discountValue = $scope.sales.discountValue || 0;
        $scope.sales.gstAmount = 0;

        $scope.sales.salesReturn.totalDiscountValue = 0;
        $scope.sales.salesReturn.netAmount = 0;
        $scope.sales.salesReturn.roundOffNetAmount = 0;
        $scope.sales.salesReturn.returnItemAmount = 0;
        $scope.sales.salesReturn.gstAmount = 0;
        $scope.sales.salesReturn.discount = $scope.sales.salesReturn.discount || 0;
        $scope.sales.salesReturn.discountValue = $scope.sales.salesReturn.discountValue || 0;

        for (var i = 0; i < $scope.sales.salesItem.length; i++) {

            var discount = 0;
            $scope.sales.salesItem[i].discount = $scope.sales.salesItem[i].discount || 0;
            discount = $scope.sales.salesItem[i].discount;
            if (discount == 0) {
                if ($scope.sales.discountType == "1") {
                    discount = $scope.sales.discount;
                } else if ($scope.sales.discountType == "2") {
                    discount = $scope.discountValueInPerc;
            }
            }
            else {
                if ($scope.sales.isRoundOff == false) { //if (isRoundOff == false) { //Chnaged By sumathi on 30112018
                    $scope.salesAmount = $scope.sales.salesAmount;
            }
        }

            var totalItemAmount = Math.abs($scope.sales.salesItem[i].quantity) * $scope.sales.salesItem[i].sellingPrice;
            var totalDiscountAmt = totalItemAmount * discount / 100;
            $scope.sales.salesItem[i].totalAmount = totalItemAmount -totalDiscountAmt;
            $scope.sales.salesItem[i].discountAmount = totalDiscountAmt;

            if ($scope.sales.salesItem[i].salesReturn) {
                $scope.sales.salesReturn.totalDiscountValue += $scope.sales.salesItem[i].discountAmount;
                $scope.sales.salesReturn.returnItemAmount += totalItemAmount;
                $scope.sales.salesReturn.netAmount += $scope.sales.salesItem[i].totalAmount;
            }
            else {
                $scope.sales.salesItemAmount += totalItemAmount;
                $scope.sales.salesItem[i].itemAmount = totalItemAmount;
        }

            var taxPerc = 0;
            if ($scope.isGstEnabled == true) {
                if ($scope.sales.salesItem[i].gstTotal != null && $scope.sales.salesItem[i].gstTotal != undefined) {
                    taxPerc = $scope.sales.salesItem[i].gstTotal;
                } else if ($scope.sales.salesItem[i].product.gstTotal != null && $scope.sales.salesItem[i].product.gstTotal != undefined) {
                    taxPerc = $scope.sales.salesItem[i].product.gstTotal;
            }
            } else {
                taxPerc = $scope.sales.salesItem[i].vat;
        }
            if (isNaN(taxPerc) == false) {
                taxPerc = parseFloat(taxPerc);
            } else {
                taxPerc = 0;
        }
            var taxAmt = ($scope.sales.salesItem[i].totalAmount - (($scope.sales.salesItem[i].totalAmount / (100 +parseFloat(taxPerc))) * 100));
            if ($scope.isGstEnabled == true) {
                $scope.sales.salesItem[i].gstAmount = taxAmt;
                $scope.sales.salesItem[i].vatAmount = 0;
                $scope.sales.salesItem[i].vat = null;
                if ($scope.sales.salesItem[i].salesReturn) {
                    $scope.sales.salesReturn.gstAmount += taxAmt;
                }
                else {
                    $scope.sales.gstAmount += taxAmt;
            }
            }
            else {
                $scope.sales.salesItem[i].vatAmount = taxAmt;
                $scope.sales.salesItem[i].gstAmount = 0;
                $scope.sales.salesItem[i].gstTotal = null;
        }
    }

        if ($scope.isEnableRoundOff == true) {
            $scope.sales.saleAmount = Math.round($scope.totalNetValue);
            $scope.sales.roundoffSaleAmount = $scope.sales.saleAmount -$scope.totalNetValue;

            $scope.sales.salesReturn.roundOffNetAmount = Math.round($scope.sales.salesReturn.netAmount) -$scope.sales.salesReturn.netAmount;
            $scope.sales.salesReturn.netAmount = Math.round($scope.sales.salesReturn.netAmount);
        }
        else {
            $scope.sales.saleAmount = $scope.totalNetValue;
            $scope.sales.roundoffSaleAmount = 0;

            $scope.sales.salesReturn.roundOffNetAmount = 0;
    }
};

    function ShowValidateStockWindow(data) {
        var m = ModalService.showModal({
            "controller": "validateStockCtrl",
            "templateUrl": 'validateStockPopupModal',
            "inputs": { "params": [{ "list": data }]
        },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
        });
    });
}

});
app.filter('dropDigits', function () {
    return function (floatNum) {
        return String(floatNum)
            .split('.')
            .map(function (d, i) { return i ? d.substr(0, 2) : d; })
            .join('.');
};
});
