﻿app.factory('newsalesService', function ($http, $filter) {
    return {
        "list": function (searchData) {
            return $http.post('/sales/listData', searchData);
        },
        // Added by Violet
        "getlist": function (searchData) {
            return $http.post('/sales/getlistData', searchData);
        },
        "GetPatientName": function (filter) {
            return $http.get('/sales/GetPatientName?patientName=' + filter);
        },
        "GetPatientNameList": function (instanceid, filter) {
            return $http.get('/sales/GetPatientNameList?instanceid=' + instanceid + '&patientName=' + filter);
        },
        "GetSaleSettings": function (instanceid, filter) {
            return $http.get('/sales/GetSaleSettings');
        },
        "SaveDraft": function (draftName, salesJSON) {
            return $http.post('/sales/SaveDraft', draftName, salesJSON);
        },
        "GetDrafts": function () {
            return $http.get('/sales/GetDrafts');
        },
        "LoadDraft": function (draftId) {
            return $http.get('/sales/LoadDraft?draftId=' + draftId);
        },
        "EditDraft": function (draftId, draftName) {
            return $http.post('/sales/EditDraft', draftId, draftName);
        },
        "DeleteDraft": function (draftId) {
            return $http.post('/sales/LoadDraft?draftId', draftId);
        },
        "create": function (sales, file, Invoicedate) {
            //console.log(sales);
            //var formData = new FormData();
            //formData.append('file', file);
            //formData.append('InvoiceDate', Invoicedate);
            //angular.forEach(sales, function (value, key) {
            //    if (key === "salesItem") {
            //        for (var i = 0; i < sales.salesItem.length; i++) {
            //            angular.forEach(sales.salesItem[i], function (value, key) {
            //                if (value === "" || value === null || value === undefined)
            //                    return;
            //                formData.append("salesItem[" + i + "]."+key,value );
            //            });
            //        }
            //    } else {
            //        if (value === "" || value === null || value === undefined)
            //            return;
            //        formData.append(key, value);
            //    }
            //});
            ////console.log(formData.get("salesItem[29].salesItem"));
            //console.log("after");
            //console.log(formData.sales);
            //return $http.post('/sales/index', formData, {
            //    withCredentials: true,
            //    headers: { 'Content-Type': undefined },
            //    transformRequest: angular.identity
            //});
            //return $http.post('/sales/index', formData, {
            //    withCredentials: true,
            //    headers: { 'Content-Type': undefined },
            //    transformRequest: angular.identity
            //});
            //console.log(sales.discount);
            //console.log(sales.discountValue);
            sales.invoicedate = $filter("date")(Invoicedate, "MM/dd/yyyy");
            return $http.post('/sales/newindex', sales);
        },
        "IsInvoiceManualSeriesAvail": function (InvoiceSeries, billdate) {
            return $http.post('/sales/IsInvoiceManualSeriesAvail?Invocieseries=' + InvoiceSeries + '&Billdate=' + billdate);
        },
        "salesDetail": function (salesId) {
            return $http.get('/sales/SalesDetail?id=' + salesId);
        },
        "createSalesReturn": function (salesReturn) {
            return $http.post('/salesReturn/index', salesReturn);
        },
        "salesReturnDetail": function (salesId) {
            return $http.get('/salesReturn/SalesReturnDetail?id=' + salesId);
        },
        "returnList": function (searchData) {
            return $http.post('/salesReturn/listData', searchData);
        },
        "UpdateCustomer": function (data) {
            return $http.post('/Patient/Update', data);
        },
        "UpdateSaleDetails": function (data) {
            return $http.post('/sales/update', data);
        },
        "saveDiscountRules": function (data) {
            return $http.post('/sales/SaveDiscountRules', data);
        },
        "getDiscountDetail": function () {
            return $http.get('/sales/SalesDiscountDetail');
        },
        "editDiscountDetail": function () {
            return $http.get('/sales/editDiscountDetail');
        },
        "editDiscountDetail1": function (billAmountType) {
            return $http.get('/sales/editDiscountDetail1?type=' + billAmountType);
        },
        "purchasePrice": function (data) {
            return $http.get('/vendorpurchase/getPurchasePrice?id=' + data);
        },
        "createMissedOrder": function (data) {
            return $http.post('/sales/createMissedOrder', data);
        },
        //Newly Added Gavaskar 24-10-2016 Start
        "saveCardType": function (data) {

            return $http.get('/sales/saveCardType?CardType=' + data);
        },
        "savePatientSearchType": function (data) {

            return $http.get('/sales/savePatientSearchType?PatientSearchType=' + data);
        },
        "saveDoctorSearchType": function (data) {

            return $http.get('/sales/saveDoctorSearchType?DoctorSearchType=' + data);
        },
        "saveDoctorMandatoryType": function (data) {
            return $http.get('/sales/isDoctorMandatory?DoctorMandatoryType=' + data);
        },
        "saveSalesTypeMandatory": function (data) {
            return $http.get('/sales/saveSalesTypeMandatory?SalestypeMandatory=' + data);
        },
        "isMaximumDiscountAvail": function (data) {
            return $http.get('/sales/isMaximumDiscountAvail?MaximumDiscountAvail=' + data);
        },
        "saveAutoTempStockAdd": function (data) {
            return $http.get('/sales/saveAutoTempStockAdd?status=' + data);
        },
        "isAutosavecustomer": function (data) {
            return $http.get('/sales/isAutosavecustomer?AutosaveCustomer=' + data);
        },
        "IsCreditInvoiceSeries": function (data) {
            return $http.get('/sales/saveIsCreditInvoiceSeries?IsCreditInvoiceSeries=' + data);
        },
        "getIsCreditInvoiceSeries": function () {
            return $http.get('/sales/getIsCreditInvoiceSeries');
        },
        "saveDiscountType": function (data) {

            return $http.get('/sales/saveDiscountType?DiscountType=' + data);
        },
        "saveCustomseries": function (data) {

            return $http.get('/sales/saveCustomseries?Customseries=' + data);
        },
        "saveInvoiceSeries": function (data) {

            return $http.get('/sales/saveInvoiceSeries?InvoiceSeries=' + data);
        },
        "saveisDepartment": function (data) {
            return $http.get('/sales/saveisDepartment?IsDepartment=' + data);
        },
        "getIsDepartmentadded": function () {
            return $http.get('/sales/getIsDepartmentadded');
        },
        "getCardTypeDetail": function () {
            return $http.get('/sales/getCardTypeDetail');
        },
        "getIsMaxDiscountAvail": function () {
            return $http.get('/sales/getIsMaxDiscountAvail');
        },
        "getPatientSearchType": function () {
            return $http.get('/sales/getPatientSearchType');
        },
        "getDoctorSearchType": function () {
            return $http.get('/sales/getDoctorSearchType');
        },
        "getDoctorNameMandatoryType": function () {
            return $http.get('/sales/getDoctorNameMandatoryType');
        },
        "getSalestypeMandatory": function () {
            return $http.get('/sales/getSalestypeMandatory');
        },
        "getAutoTempstockAdd": function () {
            return $http.get('/sales/getAutoTempstockAdd');
        },
        "getautoSaveisMandatory": function () {
            return $http.get('/sales/getautoSaveisMandatory');
        },
        "getPharmacyDiscountType": function () {
            return $http.get('/sales/getPharmacyDiscountType');
        },
        "getBatchListDetail": function () {
            return $http.get('/sales/getBatchListDetail');
        },
        "saveBatchType": function (data) {
            return $http.post('/sales/saveBatchType?batchType=' + data);
        },
        //Newly Added Gavaskar 03-02-2017 Start
        "saveShortCutKeySetting": function (keyType, newWindowType) {
            var formData = new FormData();
            formData.append('keyType', keyType);
            formData.append('newWindowType', newWindowType);
            return $http.post('/sales/saveShortCutKeySetting', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });

        },
        "getShortCutKeySetting": function () {
            return $http.get('/sales/getShortCutKeySetting');
        },
        //Newly Added Gavaskar 03-02-2017 End
        "getCustomSeriesSelected": function () {
            return $http.get('/sales/getCustomSeriesSelected');
        },
        "getCancelBillDays": function () {
            return $http.get('/sales/getCancelBillDays');
        },
        "getMaxDiscountValue": function () {
            return $http.get('/sales/getMaxDiscountValue');
        },
        "getInvoiceSeries": function () {
            return $http.get('/sales/getInvoiceSeries');
        },
        //Added by Manivannan for retrieving the Invoice No in sales screen on 30-Jan-2017 begins here
        "getNumericInvoiceNo": function () {
            return $http.get('/sales/GetNumericInvoiceNo');
        },
        "getCustomInvoiceNo": function (customSeries) {
            return $http.get('/sales/GetCustomInvoiceNo?customSeries=' + customSeries);
        },
        //Added by Manivannan ends here
        "saveSmsSetting": function (inventorySmsSettings) {
            return $http.post('/sales/saveSmsSetting', inventorySmsSettings);
        },
        "saveInvoiceseriesItem": function (data, seriesType) {
            return $http.get('/sales/saveInvoiceSeriesItem?Invoiceseries=' + data + '&SeriesType=' + seriesType);
        },
        "savecancelBillDays": function (data) {
            return $http.get('/sales/savecancelBillDays?CancelBillDays=' + data);
        },
        "saveMaxDiscount": function (data) {
            return $http.get('/sales/saveMaxDiscount?MaxDiscount=' + data);
        },
        "getSmsSetting": function (inventorySmsSettings) {
            return $http.get('/sales/getSmsSetting', inventorySmsSettings);
        },
        "getPrescriptionSetting": function () {
            return $http.get('/sales/getPrescriptionSetting');
        },
        "getInvoiceSeriesItems": function () {
            return $http.get('/sales/getInvoiceSeriesItems');
        },
        "getInvoiceSeriesItemForGRNSettings": function () {
            return $http.get('/sales/getInvoiceSeriesItemForPurchaseSettings');
        },
        "saveSalesType": function (name) {
            return $http.get('/sales/saveSalesType?typeName=' + name);
        },
        "removeSalesType": function (id) {
            return $http.get('/sales/removeSalesType?id=' + id);
        },
        "saveStatus": function (salesType) {
            return $http.post('/sales/saveStatus', salesType);
        },
        "getSalesType": function () {
            return $http.get('/sales/getSalesType');
        },
        "updateSalesType": function (saleType) {
            return $http.post('/sales/updateSalesType', saleType);
        },
        "savePrintType": function (id) {
            return $http.get('/sales/savePrintType?printType=' + id);
        },
        "getPrintType": function () {
            return $http.get('/sales/getPrintType');
        },
        "getPrintStatus": function () {
            return $http.get('/sales/GetBillPrintStatus');
        },
        "saveBillPrintStatus": function (status, printFooterNotes) {
            //return $http.post('/sales/saveBillPrintStatus?printStatus=' + status);
            var data = {};
            data.printStatus = status;
            console.log(JSON.stringify(data));
            if (printFooterNotes == null || printFooterNotes == "undefined")
                printFooterNotes = "";
            data.printFooterNotes = printFooterNotes;

            return $http.post('/sales/saveBillPrintStatus', data);
            //return $http.post('/sales/saveBillPrintStatus?printStatus=' + status + '&printFooterNotes=' + printFooterNotes);
        },
        ////Newly added by Manivannan on 27-02-2017
        //getPrintFooterNote: function () {
        //    return $http.get('/sales/GetPrintFooterNote');
        //},
        //savePrintFooterNote: function (data) {
        //    return $http.post('/sales/SavePrintFooterNote', data);
        //},
        ////New services for Print Footer note ends
        "saveBatchPopUpSettings": function (data) {
            return $http.post('/sales/saveBatchPopUpSettings', data);
        },
        "getBatchPopUpSettings": function () {
            return $http.get('/sales/getBatchPopUpSettings');
        },
        "getDefaultDoctor": function () {
            return $http.get('/doctor/GetDefaultDoctor');
        },
        "getOfflineStatus": function () {
            return $http.get('/sales/getOfflineStatus');
        },
        "searchSales": function (data, reOrderFactor) {
            return $http.post("/sales/searchSales?reOrderFactor=" + reOrderFactor, data);
        },
        "getLastSalesPrintType": function () {
            return $http.get('/sales/getLastSalesPrintType');
        },
        "saveShortName": function (cutomerName, doctorName) {
            var formData = new FormData();
            formData.append('cutomerName', cutomerName);
            formData.append('doctorName', doctorName);
            return $http.post('/sales/saveShortName', formData, {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });

        },
        "getShortName": function () {
            return $http.get('/sales/getShortName');
        },
        "saveCompleteSaleKeys": function (data) {
            return $http.get('/sales/saveCompleteSaleKeys?CompleteSaleKeys=' + data);
        },
        "getCompleteSaleKeys": function () {
            return $http.get('/sales/getCompleteSaleKeys');
        },
        "getScanBarcodeOption": function () {
            return $http.get('/sales/getScanBarcodeOption');
        },
        "saveBarcodeOption": function (scanBarcode, autoScanQty) {
            return $http.get('/sales/saveBarcodeOption?barcodeOption=' + scanBarcode + '&autoScanQty=' + autoScanQty);
        },
        "getCustomCreditInvoiceNo": function (customSeries) {
            return $http.get('/sales/GetCustomCreditInvoiceNo?customSeries=' + customSeries);
        },
        "getCustomCreditSeriesSelected": function () {
            return $http.get('/sales/getCustomCreditSeriesSelected');
        },
        "getLastSalesId": function () {
            return $http.get('/sales/getLastSalesId');
        },
        "getLastSalesSmsType": function () {
            return $http.get('/sales/getLastSalesSmsType');
        },
        "getLastSalesEmailType": function () {
            return $http.get('/sales/getLastSalesEmailType');
        },
        "getEnableSelling": function () {
            return $http.get('/vendorPurchase/getEnableSelling');
        }
    }
});