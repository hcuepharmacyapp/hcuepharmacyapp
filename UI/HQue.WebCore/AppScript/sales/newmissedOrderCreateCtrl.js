app.controller('newmissedOrderCreateCtrl', function ($scope, $rootScope, $filter, close, toastr, cacheService, ModalService, newsalesService, productService, productStockService, productModel, vendorPurchaseService, missedOrderModel, patientId, patientPhone, patientName, patientAge, patientGender) {


    var product = productModel;

    $scope.search = product;

    var missedOrder = missedOrderModel;
    $scope.missedOrder = missedOrder;
    $scope.missedOrder.missedOrderItem = [];
    $scope.IsEdit = false;
    $scope.selectedMissedProduct = null;
    $scope.reminder = false;
    $scope.quantityExceed = 0;
    $scope.searchType1 = null;
    $scope.focusProductName = true;

    //$scope.focusQuantity = false;

    if (patientId != null) {
        $scope.missedOrder.patientId = patientId;
    }

    if (patientPhone != null) {
        $scope.missedOrder.patientMobile = patientPhone;
    }

    if (patientName != null) {
        $scope.missedOrder.patientName = patientName;
        //console.log(patientName);
    }

    if (patientAge != null) {
        $scope.missedOrder.patientAge = patientAge;
        console.log(patientAge);
    }

    if (patientGender != null) {
        $scope.missedOrder.patientGender = patientGender;
        console.log(patientGender);
    }

   
    $scope.keyEnter = function (event,currentid, nextid, value) {
        if (event.which == 13) {

            var ele = "";
            if (currentid == "txtOrderQuantity") {

                if (value == "0" || value == undefined || value == null) {

                } else {
                    ele = document.getElementById(nextid);
                    ele.focus();
                }
            }

            if (currentid == "price") {
                ele = document.getElementById(nextid);
                ele.focus();
            }
        }


        //var result = document.getElementById("txtOrderQuantity").value;
        //if (result == "" || result == 0) {
        //} else {
        //    var ele = document.getElementById(e);
        //    if (event.which === 13) { // Enter key
        //        ele.focus();
        //        if (ele.nodeName != "BUTTON") {
        //            ele.select();
        //        }
        //    }
        //}
    };

    $scope.changeQuantity = function (val) {
        if ($scope.selectedMissedProduct.id != null) {
            if (val > 0) {
                $scope.quantityExceed = 1;
            }
            else {
                $scope.quantityExceed = 0;
            }
        }
    };

    $scope.onProductSelect = function (selectedProduct1) {
        try {
            $scope.quantityExceed = 0;
            if (selectedProduct1 != null) {
                $.LoadingOverlay("show");
                vendorPurchaseService.getPurchaseValues(selectedProduct1.id, selectedProduct1.name)
                    .then(function (response) {
                        $scope.vendorPurchaseItem = response.data.vendorPurchaseItem;
                        $scope.selectedMissedProduct.sellingPrice = $scope.vendorPurchaseItem.packageSellingPrice;
                        var qty = document.getElementById("txtOrderQuantity");
                        qty.focus();
                        //$scope.focusQuantity = true;
                        $.LoadingOverlay("hide");
                    }, function (error) {
                        console.log(error);
                        $.LoadingOverlay("hide");
                        toastr.error('Error Occured', 'Error');
                    });
            }
        } catch (e) {
            console.log(e.message);
        }
    };

    $scope.getSearchType1 = function () {

        vendorPurchaseService.getSearch().then(function (response1) {
            if (response1.data != null && response1.data != '') {
                $scope.searchType1 = response1.data;
            }
        }, function (error) {
            console.log(error);
        });
    };
    // $scope.getSearchType1();

    $scope.getProducts = function (val) {


        return productService.drugFilterData(val).then(function (response) {

            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }

            return output.map(function (item) {
                return item;
            });
        });
        //if ($scope.searchType1 == "Global") {
        //    return productService.drugFilterData(val).then(function (response) {

        //        var flags = [], output = [], l = response.data.length, i;
        //        for (i = 0; i < l; i++) {
        //            if (flags[$filter('uppercase')(response.data[i].name)]) continue;
        //            flags[$filter('uppercase')(response.data[i].name)] = true;
        //            output.push(response.data[i]);
        //        }

        //        return output.map(function (item) {
        //            return item;
        //        });
        //    });
        //}
        //else {
        //    return productService.drugFilterLocalData(val).then(function (response) {

        //        var flags = [], output = [], l = response.data.length, i;
        //        for (i = 0; i < l; i++) {
        //            if (flags[$filter('uppercase')(response.data[i].name)]) continue;
        //            flags[$filter('uppercase')(response.data[i].name)] = true;
        //            output.push(response.data[i]);
        //        }

        //        return output.map(function (item) {
        //            return item;
        //        });
        //    });
        //}
    };


    $scope.addProduct = function () {

        var existItem = 0;

        if ($scope.selectedMissedProduct.sellingPrice == "" || $scope.selectedMissedProduct.sellingPrice == null) {
            $scope.selectedMissedProduct.sellingPrice = 0;
        }

        if ($scope.selectedMissedProduct.quantity > 0) {
            for (var i = 0; i < $scope.missedOrder.missedOrderItem.length; i++) {
                if ($scope.missedOrder.missedOrderItem[i].id == $scope.selectedMissedProduct.id) {
                    existItem = 1;
                }
            }
            if (existItem == 0) {
                if ($scope.selectedMissedProduct.accountId === null || $scope.selectedMissedProduct.accountId === undefined || $scope.selectedMissedProduct.accountId === "") {
                    $scope.selectedMissedProduct.productMasterID = $scope.selectedMissedProduct.id;
                    $scope.selectedMissedProduct.productMasterName = $scope.selectedMissedProduct.name;
                    $scope.selectedMissedProduct.manufacturer = $scope.selectedMissedProduct.manufacturer;
                    $scope.selectedMissedProduct.category = $scope.selectedMissedProduct.category;
                    $scope.selectedMissedProduct.schedule = $scope.selectedMissedProduct.schedule;
                    $scope.selectedMissedProduct.genericName = $scope.selectedMissedProduct.genericName;
                    $scope.selectedMissedProduct.packing = $scope.selectedMissedProduct.packing;

                }
                else {
                    $scope.selectedMissedProduct.productMasterID = null;
                }

                $scope.selectedMissedProduct.focusSellingPrice = false;
                $scope.missedOrder.missedOrderItem.push($scope.selectedMissedProduct);
                //$scope.missedOrder.missedOrderItem.productStock.push($scope.selectedMissedProduct);
                //if ($scope.selectedMissedProduct.totalstock === -1) {
                //    $scope.missedOrder.missedOrderItem.productStock.product.productMasterID = $scope.selectedMissedProduct.id;
                //}
                //else {
                //    $scope.missedOrder.missedOrderItem.productStock.product.productMasterID = null;
                //}
                $scope.selectedMissedProduct = null;
                $scope.quantityExceed = 0;
                document.getElementById("txtProduct").focus();
            }
        }

    };

    $scope.tempQuantity = null;
    $scope.isAdd = false;

    $scope.editProduct = function (saleItem) {
        saleItem.IsEdit = true;
        saleItem.focusSellingPrice = true;
        $scope.isAdd = true;
        $scope.tempQuantity = saleItem.quantity;
    };

    $scope.saveProduct = function (saleItem) {
        saleItem.IsEdit = false;
        $scope.isAdd = false;
        var index1 = $scope.missedOrder.missedOrderItem.indexOf(saleItem);
        if (parseFloat(saleItem.quantity) > 0) {
            $scope.missedOrder.missedOrderItem[index1].quantity = saleItem.quantity;
        } else {
            $scope.missedOrder.missedOrderItem[index1].quantity = $scope.tempQuantity;
        }
        if (parseFloat(saleItem.sellingPrice) > 0) {
            $scope.missedOrder.missedOrderItem[index1].sellingPrice = saleItem.sellingPrice;
        }
        else {
            $scope.missedOrder.missedOrderItem[index1].sellingPrice = 0;
        }

    };

    $scope.removeProduct = function (item) {

        if (!confirm("Are you sure, Do you want to delete ? "))
            return;

        var index = $scope.missedOrder.missedOrderItem.indexOf(item);
        $scope.missedOrder.missedOrderItem.splice(index, 1);

        if ($scope.missedOrder.missedOrderItem.length == 0) {
            document.getElementById("txtProduct").focus();
        }
    };

    $scope.addOrder = function () {

        $scope.close('Yes');
        $.LoadingOverlay("show");

        newsalesService.createMissedOrder($scope.missedOrder).then(function (respone) {
            var misseditems = respone.data;
            toastr.success('Missed order saved successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };

    $rootScope.$on("MissedOrderPopUpEvent", function () {
        $scope.close("No");
    });
    $scope.close = function (result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
        if (result != "Yes") {
           // $scope.search.drugName = null;
            $scope.search = null;
        }
        $(".modal-backdrop").hide();
    };
});