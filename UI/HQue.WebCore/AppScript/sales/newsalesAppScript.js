﻿var app = angular.module('hcue', ['ngRoute', 'commonApp', 'toastr', 'angucomplete', 'localytics.directives', 'ui.bootstrap', 'angularModalService']);
app.directive('autoFocus', function ($timeout) {
    return {
        "link": function (scope, element, attrs) {
            attrs.$observe("autoFocus", function (newValue) {
                if (newValue === "true")
                    $timeout(function () { element[0].focus() });
            });
        }
    };
});
app.directive('setFocus', function ($timeout, $rootScope) {
    return {
        "restrict": 'A',
        "scope": {
            watchId: '@',
            index: '@',
            selectedWatchId: '@'
        },
        "link": function ($scope, $element, attrs) {
            $scope.$watch("index", function (currentValue, previousValue) {
                if ($scope.watchId == $scope.selectedWatchId) {
                    $timeout(function () {
                        $element[0].focus();
                    });
                }
            })
        }
    }
});
app.directive('escapeClick', function ($timeout, $rootScope) {
    return {
        "restrict": 'A',
        "scope": {
            escapeClicked: '@'
        },
        "link": function ($scope, $element, attrs) {
            if ($scope.escapeClicked) {
                $timeout(function () {
                    $element[0].focus();
                });
            }
        }
    }
});
app.directive('divFocus', function ($timeout, $rootScope) {
    return {
        "restrict": 'A',
        "scope": {
            watchid: '@'
        },
        "link": function ($scope, $element, attrs) {
            $scope.$watch("watchid", function (currentValue, previousValue) {
                if ($scope.watchid) {
                    $timeout(function () {
                        angular.element('#tempfield0').focus();
                    });
                }
            })
        }
    }
});
app.directive('focusItem', function () {
    return {
        "link": function (scope, element, attrs) {
            scope.$watch(attrs.focusItem, function () {
                element[0].focus();
            });
        }
    };
});
app.directive('focusOnFirst', function () {
    return {
        "restrict": 'A',
        "scope": {
            isLastRow: '=',
            isCurrentRow: '=',
            focusedTextid:'='
        },
        "link": function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 40 || event.which === 38) {
                    event.preventDefault();
                    var index = parseInt(scope.isCurrentRow);
                    if (scope.isLastRow === true) {
                        elementToFocus = element.children().eq(index).find('input')[0];
                    } else {
                        elementToFocus = element.children().eq(index).find('input')[scope.focusedTextid];

                    }
                    if (angular.isDefined(elementToFocus))
                        elementToFocus.focus();
                }
            });
        }
    }
});
app.directive('focusOnEnter', function () {
    return {
        "restrict": 'A',
        "scope": {
            "isLastRow": '=',
            "isCurrentRow": '='
        },
        "link": function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    event.preventDefault();
                    var index = parseInt(scope.isCurrentRow);
                    var element = document.getElementById('divmain');
                    if (scope.isLastRow === true) {
                        elementToFocus = document.getElementById('divmain').children[index].getElementsByTagName("input")[0];
                    } else {
                        elementToFocus = document.getElementById('divmain').children[index].getElementsByTagName("input")[1];
                    }

                    //if (scope.isLastRow === true) {
                    //    elementToFocus = angular.element(element.children[1]).find('input')[0];
                    //} else {
                    //    elementToFocus = angular.element(element.children[1]).find('input')[1];
                    //}

                    //if (scope.isLastRow === true) {
                    //    elementToFocus = element.children().eq(index).find('input')[0];
                    //} else {
                    //    elementToFocus = element.children().eq(index).find('input')[1];

                    //}

                    if (angular.isDefined(elementToFocus)) {
                        elementToFocus.focus();
                    }
                }
            });
        }
    }
});
app.directive('focusOnNext', function () {
    return {
        "restrict": 'A',
        "scope": {
            "focusedTextid": '@',
            "focusSelectbox": '@',
            "indexVal": '@',
            "currentrow": '@',
            "lastVal": '@'
        },
        "link": function (scope, element, attrs) {
            var elementToFocus = undefined
            element.bind("keydown keypress", function (event) {
                //  var element = document.getElementById('divmain');
                var index = parseInt(scope.currentrow);
                if (event.which === 13) {
                    event.preventDefault();
                    if (scope.focusSelectbox === 'false') {
                        //elementToFocus = element.find('input')[scope.focusedTextid];
                        elementToFocus = document.getElementById('divmain').children[index-1].getElementsByTagName("input")[scope.focusedTextid];
                    } else {
                        elementToFocus = document.getElementById('divmain').children[index-1].getElementsByTagName("input")[0];
                    }


                    var type = angular.element(window.document.activeElement)[0].type;
                    if (type == "checkbox") {
                        var number = parseFloat(scope.currentrow);
                        elementToFocus = document.getElementById("txtQty" + number);
                    }





                    if (angular.isDefined(elementToFocus)) {
                        elementToFocus.focus();
                    }
                } else if (event.which === 39) {
                    event.preventDefault();
                    if (scope.focusSelectbox === 'false') {
                        elementToFocus = document.getElementById('divmain').children[index - 1].getElementsByTagName("input")[scope.focusedTextid];
                    } else {
                        elementToFocus = document.getElementById('divmain').children[index - 1].getElementsByTagName("input")[0];
                    }
                    if (angular.isDefined(elementToFocus)) {
                        elementToFocus.focus();
                    }
                } else if (event.which === 37) {
                    event.preventDefault();
                    if (scope.focusSelectbox === 'false') {
                        elementToFocus = document.getElementById('divmain').children[index - 1].getElementsByTagName("input")[scope.focusedTextid];
                    } else {
                        elementToFocus = document.getElementById('divmain').children[index - 1].getElementsByTagName("input")[0];
                    }
                    if (angular.isDefined(elementToFocus))
                        elementToFocus.focus();
                }
            });
        }
    }
});

app.directive("keepFocus", ['$timeout', function ($timeout) {
    return {
        "restrict": 'A',
        "require": 'ngModel',
        "link": function ($scope, $element, attrs, ngModel) {
            ngModel.$parsers.unshift(function (value) {
                $timeout(function () {
                    $element[0].focus();
                });
                return value;
            });
        }
    };
}])

app.directive('numbersOnly', function () {
    return {
        "require": 'ngModel',
        "link": function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});
//app.directive("scrollBottom", function () {
//    return {
//        scope: {
//            indexVal: '@',
//            heightOfIndex:'@',
//        },
//        link: function (scope, element, attr) {
//            var $id = $("#" + attr.scrollBottom);
//            element.bind("keydown keypress", function (event) {
//                if (event.which === 13) {
//                    $id.scrollTop(scope.heightOfIndex);
//                }
//            });
//        }
//    }
//});
//app.filter('reverse', function () {
//    return function (items) {
//        return items.slice().reverse();
//    };
//});
