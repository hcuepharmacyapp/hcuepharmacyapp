app.controller('tempStockCtrl', function ($scope, $filter, $rootScope, close, toastr, cacheService, ModalService, productService, vendorPurchaseService, productModel, productStockModel, tempVendorPurchaseItemModel, screenType, GSTEnabled) {
    $scope.focusInput = true;
    $scope.minDate = new Date();
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };

    $scope.GSTEnabled = GSTEnabled;

    var productStock = productStockModel;
    var product = productModel;
    productStock.product = product;
    var vendorPurchaseItem = tempVendorPurchaseItemModel;
    vendorPurchaseItem.productStock = productStock;
    //$scope.temp = [];//Changed by Annadurai
    $scope.temp = vendorPurchaseItem;
    $scope.tempProduct = null;
    $scope.temp.productStock.batchNo = null;
    $scope.temp.productStock.expireDate = null;
    $scope.temp.productStock.vAT = 0;
    $scope.temp.packageSize = null;
    $scope.temp.packageQty = null;
    $scope.temp.packagePurchasePrice = 0;
    $scope.temp.packageSellingPrice = null;
    $scope.temp.packageMRP = null;
    $scope.searchType1 = null;
    //Added by Annadurai Starts 07312017

    //Added by Gavaskar 22-08-2017 Start
    $scope.temp.productStock.gstTotal = 0;
    $scope.temp.productStock.cgst = 0;
    $scope.temp.productStock.sgst = 0;
    $scope.temp.productStock.igst = 0;
    $scope.temp.productStock.hsnCode = null;
    //Added by Gavaskar 22-08-2017 End

    $scope.isAllowDecimal = false;
    $scope.HoldQty = "";
    $scope.IsSave = true;
    $scope.IsSaveTempStock = false;

   

    function getIsAllowDecimalSettings() {
        vendorPurchaseService.getDecimalSetting().then(function (response) {
            $scope.isAllowDecimal = response.data;
        }, function (error) {
            console.log(error);
        });
    }
   
    $scope.getProducts = function (val) {

        return productService.nonHiddenProductList(val).then(function (response) {
            var flags = [], output = [], l = response.data.length, i;

            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }
            // console.log(output);
            return output.map(function (item) {
                return item;
            });
        });

    };
    $scope.onProductSelect = function (item, model) {
        window.setTimeout(function () {
            var ele = document.getElementById("batchNo");
            ele.focus();
        }, 0);
    };
    
    $scope.onTempProductSelect = function () {

        var ele = document.getElementById("batchNo");       
        
        var productId = "";
        if ($scope.tempProduct != undefined && $scope.tempProduct != null) {
            productId = $scope.tempProduct.id;
        }
         
        vendorPurchaseService.getProductGST(productId)
            .then(function (response) {
                $scope.temp.productStock.product = response.data;
                $scope.temp.productStock.hsnCode = response.data.hsnCode;
                $scope.temp.packageSize = response.data.packageSize;

                var gstTotal = parseFloat(response.data.gstTotal);

                $scope.temp.productStock.gstTotal = gstTotal;
                var ngst = $filter("filter")($scope.taxValuesList, { "tax": $scope.temp.productStock.gstTotal }, true);

                if (ngst.length == 0) {
                    $scope.temp.productStock.gstTotal = "";
                }
                $scope.temp.productStock.cgst = ($scope.temp.productStock.gstTotal / 2).toFixed(2);
                $scope.temp.productStock.sgst = ($scope.temp.productStock.gstTotal / 2).toFixed(2);
                $scope.temp.productStock.igst = $scope.temp.productStock.gstTotal;
               
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
            });
        
        $scope.showTempStockScreen(productId);
        ele.focus();
    };
    $scope.tempItems = [];
    $scope.showTempStockScreen = function (productId) {
        vendorPurchaseService.loadTempVendorPurchaseItem(productId).then(function (response) {
            $scope.tempItems = response.data;
            //for (var i = 0; i < $scope.tempItems.length; i++) {
            //    $scope.tempItems[i].productStock.vAT = $scope.tempItems[i].productStock.vat;
            //    $scope.tempItems[i].packagePurchasePrice = 0;
            //    $scope.tempItems[i].packagePurchasePrice = $scope.tempItems[i].packageSellingPrice - ($scope.tempItems[i].packageSellingPrice * 20) / 100;
            //}
            //for (var i = 0; i < $scope.tempItems.length; i++)
            //{
            //    if ($scope.tempItems[i].isItemSaled == true) {
            //        $scope.isItemSaled = true;
            //    }
            //    else
            //    {
            //        $scope.isItemSaled = false;
            //    }
            //}

            //var count = $filter("filter")($scope.tempItems, { "isItemSaled": false }).length;
            //if (count == 1) {
            //    var object = $filter("filter")($scope.tempItems, { "isItemSaled": false });
            //    var index = $scope.tempItems.map(function (e) { return e.isItemSaled; }).indexOf(false);
            //    $scope.tempItems[index].disableDelete = true;
            //}

            for (var i = 0; i < $scope.tempItems.length; i++) {
                if ($scope.tempItems[i].isItemSaled == true) {
                    $scope.tempItems[i].disableDelete = true;
                }
                else {
                    $scope.tempItems[i].disableDelete = false;
                }
            }

            //if ($scope.tempItems.length > 0) {
            //    document.getElementById('tempDrugName').disabled = true;
            //}
            $.LoadingOverlay("hide");
            //$scope.temp = $scope.tempItems[0];
            //$scope.temp = $scope.tempItems;
        }, function () { });
    };
    $scope.checkPackageSize = function (size) {
        if (size >= 1) {
            $scope.frmTempStock.packageSize.$setValidity("checkPackageSizeError", true);
            $scope.IsSaveTempStock = true;
        } else {
            $scope.frmTempStock.packageSize.$setValidity("checkPackageSizeError", false);
            $scope.IsSaveTempStock = false;
        }
        if (size == undefined) {
            $scope.frmTempStock.packageSize.$setValidity("checkPackageSizeError", false);
            $scope.IsSaveTempStock = false;

        }
        if ($scope.frmTempStock.packageQty.$valid == false || $scope.frmTempStock.packageSellingPrice.$valid == false
            || $scope.frmTempStock.expDate.$error.InValidexpDate == false || document.getElementById("batchNo").value == "0" || document.getElementById("gstTotal1").value == "") {
            $scope.IsSaveTempStock = false;
        }
    };
    $scope.ResetTempStock = function () {
        
        if ($scope.temp != null)
        {
            $scope.temp.productStock.batchNo = null;
            $scope.temp.productStock.expireDate = null;
            $scope.temp.productStock.vAT = 0;
            $scope.temp.packageSize = null;
            $scope.temp.packageQty = null;
            $scope.temp.packagePurchasePrice = 0;
            $scope.temp.packageSellingPrice = null;
            $scope.temp.packageMRP = null;
            $scope.temp.productStock.gstTotal = 0;
            $scope.temp.productStock.cgst = 0;
            $scope.temp.productStock.sgst = 0;
            $scope.temp.productStock.igst = 0;
            $scope.temp.productStock.hsnCode = null;
        }
        

       // $scope.temp = null;
        $scope.EditMode = false;
        $scope.frmTempStock.packageQty.$setValidity("checkNoofStripError", true);
        $scope.frmTempStock.expDate.$setValidity("InValidexpDate", true);
        $scope.frmTempStock.packageSellingPrice.$setValidity("checkMrpError", true);
       // document.getElementById('tempDrugName').disabled = false;
        // Added by Gavaskar 30-08-2017
        var ele = document.getElementById("tempDrugName");
        ele.focus();
        
    };
    $scope.AddTempStock = function () {
        $scope.IsSaveTempStock = true;
        $scope.IsSave = true;
        
        if (document.getElementById("tempDrugName").value == "") {
            var ele = document.getElementById("tempDrugName");
            ele.focus();
            $scope.IsSave = false;
        }
       else if (document.getElementById("batchNo").value == "")
        {
            var ele = document.getElementById("batchNo");
            ele.focus();
            $scope.IsSave = false;
        }
        else if (document.getElementById("expDate").value == "")
        {
            var ele = document.getElementById("expDate");
            ele.focus();
            $scope.IsSave = false;
        }
        
        else if (document.getElementById("packageSize").value == "" || document.getElementById("packageSize").value == 0) {
            var ele = document.getElementById("packageSize");
            ele.focus();
            $scope.IsSave = false;
        }        
        else if (document.getElementById("packageQty").value == "" || document.getElementById("packageQty").value == 0) {
            var ele = document.getElementById("packageQty");
            ele.focus();
            $scope.IsSave = false;
        }
        else if (document.getElementById("packagePurchasePrice").value == "" || document.getElementById("packagePurchasePrice").value == 0) {
            var ele = document.getElementById("packagePurchasePrice");
            ele.focus();
            $scope.IsSave = false;
        }
        else if (document.getElementById("packageSellingPrice").value == "" || document.getElementById("packageSellingPrice").value == 0) {
            var ele = document.getElementById("packageSellingPrice");
            ele.focus();
            $scope.IsSave = false;
        }
        else if (document.getElementById("packageMRP").value == "" || document.getElementById("packageMRP").value == 0) {
            var ele = document.getElementById("packageMRP");
            ele.focus();
            $scope.IsSave = false;
        }
        else if (document.getElementById("gstTotal1")) {

            if ($scope.temp.productStock.gstTotal == "")
                $scope.temp.productStock.gstTotal = 0;
            var ngst = $filter("filter")($scope.taxValuesList, { "tax": parseFloat($scope.temp.productStock.gstTotal) }, true);
            if (ngst.length == 0) {
                var ele = document.getElementById("gstTotal1");
                ele.focus();
                $scope.IsSave = false;
            }            
        }
        if ($scope.IsSave) {
            $scope.temp.productStock.product = $scope.tempProduct;
            $scope.temp.productStock.productId = $scope.tempProduct.id;

            if ($scope.tempProduct.accountId === null || $scope.tempProduct.accountId === undefined || $scope.tempProduct.accountId === "") {
                $scope.temp.productStock.product.productMasterID = $scope.tempProduct.id;
            } else {
                $scope.temp.productStock.product.productMasterID = null;
            }
            //$scope.temp.packagePurchasePrice = $scope.temp.packageSellingPrice - ($scope.temp.packageSellingPrice * 20) / 100;
            //need to check the duplicate here -starts
            var status = 0;
            $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
            if (!$scope.EditMode && $scope.tempItems.length > 0) {

                for (var i = 0; i < $scope.tempItems.length; i++) {
                    //Same ProductExist
                   
                    $scope.createdDate = $filter('date')(new Date($scope.tempItems[i].createdAt), 'dd/MM/yyyy');
                    if ($scope.tempItems[i].productStock.expireDate == null || $scope.tempItems[i].productStock.expireDate == undefined)
                    {
                        $scope.tempItems[i].productStock.expireDate = "";
                    }
                    else
                    {
                        $scope.tempItems[i].productStock.expireDate = $filter('date')(new Date($scope.tempItems[i].productStock.expireDate), 'yyyy-MM-dd')
                    }
                    

                    if ($scope.tempItems[i].productStock.product.id == $scope.temp.productStock.productId &&
                        $scope.tempItems[i].productStock.batchNo == $scope.temp.productStock.batchNo &&
                        $scope.tempItems[i].productStock.gstTotal == $scope.temp.productStock.gstTotal &&
                        //$scope.tempItems[i].productStock.purchasePrice == $scope.temp.productStock.purchasePrice &&
                        $scope.tempItems[i].packageSize == $scope.temp.packageSize &&
                        $scope.tempItems[i].productStock.expireDate == $scope.temp.productStock.expireDate &&
                        //$scope.tempItems[i].packageQty == $scope.temp.packageQty &&
                        $scope.tempItems[i].packagePurchasePrice == $scope.temp.packagePurchasePrice &&
                        $scope.tempItems[i].packageSellingPrice == $scope.temp.packageSellingPrice &&
                        $scope.tempItems[i].packageMRP == $scope.temp.packageMRP && $scope.createdDate == $scope.today)
                    {
                        $scope.tempItems[i].packageQty = parseFloat($scope.tempItems[i].packageQty) + parseFloat($scope.temp.packageQty);
                        status = 1;

                    }
                }
            }
            if (status == 0) {
                $scope.tempItems.push($scope.temp);
            }
            
            $scope.vendorPurchaseItem = { "productStock": { "product": {} } };

            $scope.temp = $scope.vendorPurchaseItem;

            document.getElementById('btnSubmit').disabled = false;
            var ele = document.getElementById("batchNo");
            ele.focus();
        }
        
    };
    $scope.UpdateTempStock = function () {
        $scope.IsSaveTempStock = true;
        if ($scope.EditMode)
            $scope.tempItems[$scope.Editindex] = JSON.parse(JSON.stringify($scope.temp));
        //if ($scope.tempItems.length > 0) {
        //    document.getElementById('tempDrugName').disabled = true;
        //}
        $scope.temp = null;
        $scope.EditMode = false;
        var ele = document.getElementById("tempDrugName");
        ele.focus();
    };
    $scope.editTempStock = function (item, ind) {
        $scope.EditMode = true;
        $scope.Editindex = ind;
       
        $scope.temp = JSON.parse(JSON.stringify(item));
        if ($scope.temp.isItemSaled == true)
        {
            var ele = document.getElementById("packageQty");
            ele.focus();
        }
        else
        {
            var ele = document.getElementById("batchNo");
            ele.focus();
        }
        var ngst = $filter("filter")($scope.taxValuesList, { "tax": parseFloat(item.productStock.gstTotal) }, true);

        if (ngst.length == 0) {
            item.productStock.gstTotal = null;
        }
       
    };

    $scope.chkBatchNo = function () {
        $scope.frmTempStock.batchNo.$setValidity("checkBatchNoError", true);
        $scope.IsSaveTempStock = true;
        if ( document.getElementById("batchNo").value == "" || document.getElementById("batchNo").value == "0") 
            // $scope.frmTempStock.packageQty.$valid == false || $scope.frmTempStock.packageSellingPrice.$valid == false || $scope.frmTempStock.expDate.$valid == false || document.getElementById("gstTotal1").value == "" || frmTempStock.packageSize.$valid == false // Comment by Gavaskar 12-08-2017
        {
            $scope.IsSaveTempStock = false;
            $scope.frmTempStock.batchNo.$setValidity("checkBatchNoError", false);
        }     
    };
    $scope.removeTempStock = function (item, ind) {

        /*   */
        vendorPurchaseService.deleteTempVendorPurchaseItem(item)
                .then(function (response) {
                    console.log(response)
                }, function (error) {
                    console.log(error);
                });
        /*  */

        $scope.tempItems[ind].isDeleted = 1;
        $scope.IsSaveTempStock = true;
        for (var i = 0; i < $scope.tempItems.length; i++) {
            //if ($scope.tempItems.length == 1) {
                if ($scope.tempItems[i].isDeleted == 1) {
                    document.getElementById('btnSubmit').disabled = true;
                }
                else {
                    document.getElementById('btnSubmit').disabled = false;
                }
            //}
            //else {
            //    document.getElementById('btnSubmit').disabled = false;
            //}
        }

        var ele = document.getElementById("tempDrugName");
        ele.focus();
        
    };
    $scope.checkMrp = function (purchase, sell, vat) {
        purchase = purchase != undefined ? parseFloat(purchase) : 0;
        sell = sell != undefined ? parseFloat(sell) : 0;
        vat = vat != undefined ? parseFloat(vat) : 0;
        if (sell < parseInt(purchase) + (parseInt(purchase) * vat / 100) || sell == undefined || purchase == undefined || sell == 0 || purchase == 0) {
            $scope.frmTempStock.packageSellingPrice.$setValidity("checkMrpError", false);
            $scope.IsSave = false;
            $scope.IsSaveTempStock = false;
            //$scope.frmTempStock.$valid = false;
            
        } else {
            $scope.frmTempStock.packageSellingPrice.$setValidity("checkMrpError", true);
            //$scope.frmTempStock.$valid = true;
            $scope.IsSave = true;
            $scope.IsSaveTempStock = true;           
        }

        if (!$scope.enableSelling) {
            $scope.temp.packageMRP = $scope.temp.packageSellingPrice;
        } else {
            if ($scope.temp.packageMRP > 0) {
                $scope.checkSellingMrp($scope.temp.packageSellingPrice, $scope.temp.packageMRP);
            }
        }
    };
    $scope.checkSellingMrp = function (selling, mrp) {
        if ($scope.enableSelling != true)
            return;

        selling = selling != undefined ? parseFloat(selling) : 0;
        mrp = mrp != undefined ? parseFloat(mrp) : 0;

        if (mrp < selling || (mrp == 0)) {
            $scope.frmTempStock.packageMRP.$setValidity("checkMrpError", false);
            //$scope.frmTempStock.$valid = false;
            $scope.IsSave = false;
           
        } else {
            $scope.frmTempStock.packageMRP.$setValidity("checkMrpError", true);
            // $scope.frmTempStock.$valid = true;
            $scope.IsSave = true;
           
        }
    };
    $scope.soldOrReturned = 0;
    $scope.validateFreeQty = function () {
        $scope.tempPakageQty = 0;
        if ($scope.temp.packageQty == undefined || $scope.temp.packageQty == 0 || $scope.temp.packageQty == '')
        {
            //Raise Error Message
            $scope.frmTempStock.packageQty.$setValidity("checkNoofStripError", false);
            $scope.IsSave = false;
            $scope.IsSaveTempStock = false;
            $scope.HoldQty = 0;            
        }
        else
        {          
            $scope.tempPakageQty = $scope.temp.packageQty;
            //No Error Message
            $scope.frmTempStock.packageQty.$setValidity("checkNoofStripError", true);
            $scope.IsSaveTempStock = true;
            $scope.IsSave = true;
        }
        if ($scope.EditMode)
        {
            $scope.AvailableStrip = Math.floor(($scope.temp.productStock.stock / $scope.temp.productStock.packageSize));
            $scope.HoldQty = $scope.temp.productStock.packageQty - $scope.AvailableStrip;

            if (($scope.tempPakageQty < $scope.HoldQty) || $scope.tempPakageQty == 0) {
                //Raise Error Message
                $scope.frmTempStock.packageQty.$setValidity("checkNoofStripError", false);
                $scope.IsSave = false;
                $scope.IsSaveTempStock = false;

            }
            else {
                //No Error Message
                $scope.frmTempStock.packageQty.$setValidity("checkNoofStripError", true);
                $scope.IsSave = true;
                $scope.IsSaveTempStock = true;

            }
        }
        
    };
    
    $scope.addStock = function () {
        $scope.close('Yes');        
        if ($scope.IsSaveTempStock) {
            $.LoadingOverlay("show");
            vendorPurchaseService.saveTempVendorPurchaseItem($scope.tempItems)
            .then(function (response) {
                cacheService.set('tempStockValues', response.data[0]);
                $rootScope.$emit("tempStockSelection", response.data[0]);
                $scope.zeroBatch = false;
                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
            });
        }
        
    };
    $rootScope.$on("TempStockEvent", function () {
        $scope.close("No");
    });
    $scope.close = function (result) {
        close(result, 100); // close, but give 100ms for bootstrap to animate
        if (result != "Yes") {
            if ($scope.temp != undefined)
            {
                $scope.temp.drugName = null;
            }
           // $scope.temp = null;
        }
        $(".modal-backdrop").hide();
    };
    $scope.expDateFormat = function (e) {

        var TXT = document.getElementById("expDate");



        if (TXT.length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $scope.temp.productStock.expireDate = TXT + "/";
        }
    };
    function toDate(dateStr) {
        if (dateStr != null) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
    }
    $scope.getStrLength = function (text, e) {
        if (text.length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $scope.inputText = text + "/";
        }
    };
    $scope.formatString = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    };
    function toDate(dateStr) {
        if (dateStr != null) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
    }
    $scope.dayDiff = function (expireDate) {
        var val = (toDate($scope.frmTempStock.expDate.$viewValue) > $scope.minDate);
        $scope.frmTempStock.expDate.$setValidity("InValidexpDate", val);
        $scope.isFormValid = val;
        $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date($scope.formatString($scope.expireDate));
        var date1 = new Date($scope.formatString($scope.today));
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference < 30) {
            $scope.highlight = "highlight";
            $scope.frmTempStock.expDate.$setValidity("InValidexpDate", false);
            $scope.isFormValid = false;
            $scope.IsSave = false;
            $scope.IsSaveTempStock = false;
        } else {
            $scope.frmTempStock.expDate.$setValidity("InValidexpDate", true);
            $scope.isFormValid = true;
            $scope.highlight = "";
            $scope.IsSave = true;
            $scope.IsSaveTempStock = true;
        }
        if ($scope.frmTempStock.packageQty.$valid == false || $scope.frmTempStock.packageSellingPrice.$valid == false ||
           $scope.frmTempStock.expDate.$error.InValidexpDate == false || document.getElementById("batchNo").value == "" ||
           document.getElementById("batchNo").value == "0" || document.getElementById("gstTotal1").value == ""
           || $scope.frmTempStock.packageSize.$valid == false) {
            $scope.IsSaveTempStock = false;            
        }

    };
    $scope.keyEnter = function (event, nextid, currentid, value) {

        var ele = document.getElementById(nextid);

        if (event.which === 13) {

            if (currentid == "batchNo") {
                if ($scope.temp.productStock.batchNo == "" || $scope.temp.productStock.batchNo == undefined) {
                    return false;
                }
            }


            if (currentid == "expDate") {
                if ($scope.temp.productStock.expireDate == "" || $scope.temp.productStock.expireDate == undefined) {
                    return false;
                }
            }

            if (currentid == "vat1") {
                if (value == null || value < 0) {
                    ele = document.getElementById(currentid);
                }
            }

            if (currentid == "igst") {
                if ($scope.temp.productStock.igst == "" || $scope.temp.productStock.igst == undefined) {
                    return false;
                }
            }

            if (currentid == "cgst") {
                if ($scope.temp.productStock.cgst == "" || $scope.temp.productStock.cgst == undefined) {
                    return false;
                }
            }

            if (currentid == "sgst") {
                if ($scope.temp.productStock.sgst == "" || $scope.temp.productStock.sgst == undefined) {
                    return false;
                }
            }

            if (currentid == "gstTotal1") {
                if ($scope.temp.productStock.gstTotal == "" || $scope.temp.productStock.gstTotal == undefined) {
                    return false;
                }
            }

            if (currentid == "packageSize" || currentid == "packageQty" || currentid == "packageSellingPrice" || currentid == "packageMRP") {
                if (value == 0 || value == null) {
                    ele = document.getElementById(currentid);
                }
            }
            ele.focus();
        }
    };
    //cacheService.set("isProductCreateOpened", false);
    //$scope.PopupAddNewProduct = function () {
    //    $scope.close("No");
    //    cacheService.set("isProductCreateOpened", true);

    //    var templateUrl = "";
    //    if (screenType == "old") {
    //        templateUrl = "createProduct";
    //    }
    //    if (screenType == "new") {
    //        templateUrl = "NewCreateProduct";
    //    }
    //    var m = ModalService.showModal({
    //        controller: "productCreateCtrl",
    //        templateUrl: templateUrl,
    //        inputs: {
    //            mode: "Create",
    //            "poproductname": '',
    //            "GSTEnabled": $scope.GSTEnabled
    //        }
    //    }).then(function (modal) {
    //        modal.element.modal();
    //        modal.close.then(function (result) {
    //            $scope.message = "You said " + result;
    //        });
    //    });
    //};
    $scope.PopupAddNewProduct = function () {
        var m = ModalService.showModal({
            "controller": "productCreateCtrl",
            "templateUrl": "createProduct",
            "inputs": {
                "mode": "Create",
                "poproductname": '',
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();

        });
    };

    $scope.keyUp = function (e, nextid, currentid, value) {
        if ($("#expDate").val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $("#expDate").val($("#expDate").val() + "/");
        }
        if ($("#expDate").val().length < 5) {
            $scope.frmTempStock.expDate.$setValidity("InValidexpDate", false);           
            $scope.IsSave = false;
            $scope.IsSaveTempStock = false;
        }
        if (!$scope.GSTEnabled) {
            nextid = "vat1";
        }
        $scope.keyEnter(e, nextid, currentid, value);
    };
    $scope.getEnableSelling = function () {
        vendorPurchaseService.getEnableSelling()
            .then(function (response) {
                $scope.enableSelling = response.data;
            }, function (error) {
                console.log(error);
            });
    };
    $scope.getEnableSelling();
    $scope.changeGST = function (value) {
        $scope.IsSaveTempStock = true;
        if (value == "" )
        // ||$scope.frmTempStock.packageQty.$valid == false || $scope.frmTempStock.packageSellingPrice.$valid == false || $scope.frmTempStock.expDate.$error.InValidexpDate == false || $scope.frmTempStock.packageSize.$valid == false // Comment by Gavaskar 22-08-2017
        {
            $scope.IsSaveTempStock = false;
            // Added by Gavaskar 22-08-2017 Start
            $scope.temp.productStock.cgst = 0;
            $scope.temp.productStock.sgst = 0;
            $scope.temp.productStock.igst = 0;
            // Added by Gavaskar 22-08-2017 End
        }
        else
        {
            var gstTotal = parseFloat($scope.temp.productStock.gstTotal) || 0;
            $scope.temp.productStock.cgst = (gstTotal / 2).toFixed(2);
            $scope.temp.productStock.sgst = (gstTotal / 2).toFixed(2);
            $scope.temp.productStock.igst = gstTotal;
        }
       
    };
   
    $scope.getTaxValues = function () {
        vendorPurchaseService.getTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;
            if ($scope.taxValuesList.length>0)
                $scope.temp.productStock.gstTotal = $scope.taxValuesList[0].tax;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    
    $scope.notEmptyOrNull = function (item) {
        return !(item.tax === null || item.tax.trim().length === 0)
    }

    //Added by San 
    $scope.getTaxValues();

});
appCommon.directive('myDate', function (dateFilter, $parse) {
    return {
        "restrict": 'EAC',
        "require": '?ngModel',
        "link": function (scope, element, attrs, ngModel, ctrl) {
            ngModel.$parsers.push(function (viewValue) {
                return dateFilter(viewValue, 'yyyy-MM-dd');
            });
        }
    };
});


