﻿app.controller('newsalesSearchCtrl', function ($scope, $filter, newsalesService, toastr, salesModel, pagerServcieforsales, productService, productModel, salesReturnModel,
    salesReturnItemModel, printingHelper, patientService) {

    $scope.IsOffline = false;
    $scope.enableSelling = false;
    getOfflineStatus = function () {
        newsalesService.getOfflineStatus().then(function (response) {
            if (response.data) {
                //toastr.error("This is for online mode");
                $scope.IsOffline = true;
                $scope.offlineGreyButton = "offline-grey-button";
                $scope.paidMsg = $scope.offlineMsg;
            }
            else {
                $scope.IsOffline = false;
            }
        });
    };
    getOfflineStatus();

    var sales = salesModel;

    var salesReturn = salesReturnModel;
    var salesReturnItem = salesReturnItemModel;

    $scope.salesReturn = salesReturn;
    $scope.salesReturn.salesReturnItem = salesReturnItem;

    $scope.search = sales;
    $scope.list = [];
    $scope.selectedlist =
        {
            "name": "",
            "mobile": "",
            "doctorName": ""
        };
     //Senthil vars
    $scope.selectedSale = [];
    $scope.isSelectedItemMain = 1;

    //end
    $scope.IsEdit = false;
    //pagination
    $scope.search.page = pagerServcieforsales.page;
    $scope.pages = pagerServcieforsales.pages;
    $scope.paginate = pagerServcieforsales.paginate;
    //pagination
    $scope.salesCondition = false;
    $scope.dateCondition = false;
    $scope.productCondition = false;
    $scope.amt = 0; // added by arun for display net amount

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.popup2 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'//newly added
    };


    $scope.CancelBillDays = 0;
    getCancelBillDays();
    function getCancelBillDays() {
        newsalesService.getCancelBillDays().then(function (response) {
            console.log(JSON.stringify(response));
            if (response.data != "" && response.data != null) {
                $scope.CancelBillDays = response.data.postCancelBillDays;
                console.log($scope.CancelBillDays);
            }
        }, function () {

        });
    }

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');

    $scope.data = [];

    $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    function pageSearch() {
        $.LoadingOverlay("show");
        newsalesService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            if ($scope.list.length > 0) {
                $scope.selectedSale = $scope.list[0];
                $scope.buildselectedSales($scope.list[0], 0);
            }
            $scope.isSelectedItemMain = 1;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.salesSearch = function () {
        $.LoadingOverlay("show");
        document.getElementById('productbatchList').scrollTop = 0;
        $scope.search.page.pageNo = 1;
        $scope.search.searchProductId = "";
        if ($scope.search.drugName)
            $scope.search.values = $scope.search.drugName.id;


        $scope.amt = 0;
        newsalesService.list($scope.search).then(function (response) {
            if (response.data.list == undefined) {
                $scope.list = [];
            } else {
                $scope.list = response.data.list;
                if ($scope.list.length > 0) {
                    $scope.selectedSale = $scope.list[0];
                    for (var i = 0; i < $scope.selectedSale.salesItem.length; i++) {
                        $scope.amt += Math.round($scope.selectedSale.salesItem[i].total);
                    }
                }
              
            }
            $scope.isSelectedItemMain = 1;
            if ($scope.search.select1 == "top10")
            {
                $scope.search.select1 = "equal";
               
            }
            $scope.showIstenRecords = false;
            if ($scope.ispageload==1) {
                $scope.showIstenRecords = true;
                $scope.ispageload = 0;
            }
            pagerServcieforsales.init(response.data.noOfRows, pageSearch);
          
           
            $.LoadingOverlay("hide");
          
            $scope.buildselectedSales($scope.list[0], 0);
         
        }, function () {
            $.LoadingOverlay("hide");
        });
       
    }

   $scope.ispageload = 0;
    $scope.pageload = function () {
        $scope.search.select = "";
        //$scope.search.select1 = 'equal';
        $scope.search.select1 = "top10";
        $scope.salesCondition = false;
        $scope.dateCondition = false;
        $scope.search.values = "";
        $scope.search.selectValue = "";
        $scope.maxDate = new Date();
        $scope.showIstenRecords = false; //Variable added to implement the top 10records on pageload and clear
        $scope.ispageload = 1;
        $scope.productCondition = false;
        $scope.CustomerCondition = false;
        //$scope.DoctorCondition = false;
        $scope.EnterCondition = false;
        $scope.invoicenumberCondition = false;
        $scope.salesSearch();
        //$scope.search.select1 = "equal";
    }
    $scope.pageload();
    //
    $scope.InvoiceSeriesCondition = false;
    $scope.changefilters = function () {
        $scope.search.select1 = "";
        $scope.search.values = "";
        $scope.search.invoiceDate = "";
        $scope.search.drugName = undefined;


        if ($scope.search.select == 'billNo') {
            $scope.InvoiceSeriesCondition = true;
        } else {
            $scope.InvoiceSeriesCondition = false;
            $scope.InvoiceSeriesType = "Default";
        }

        if ($scope.search.select == 'quantity' || $scope.search.select == 'mrp' || $scope.search.select == 'discount' || $scope.search.select == 'discountValue' || $scope.search.select == 'vat' || $scope.search.select == 'invValue') {
            $scope.salesCondition = true;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = true;
            $scope.invoicenumberCondition = false;
            $scope.search.select1 = "equal";
        } else if ($scope.search.select == 'billDate') {
            $scope.salesCondition = false;
            $scope.dateCondition = true;
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.maxDate = new Date();
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
        } else if ($scope.search.select == 'expiry') {
            $scope.salesCondition = false;
            $scope.dateCondition = true;
            $scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.maxDate = "";
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
        } else if ($scope.search.select == 'product') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = true;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
        }
            // Added by arun for customer name on 22nd march by punithavel req.
        else if ($scope.search.select == 'customerName') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = true;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = false;
        }

        else if ($scope.search.select == 'doctorName') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            $scope.EnterCondition = true;
            //$scope.DoctorCondition = false;
        }

        else if ($scope.search.select == 'batchNo'  || $scope.search.select == 'customerMobile') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.EnterCondition = true;
            $scope.invoicenumberCondition = false;
            $scope.InvoiceSeriesType = "Default";


        }
        else if ($scope.search.select == 'billNo') {
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.CustomerCondition = false;
            $scope.EnterCondition = false;
            $scope.invoicenumberCondition = true;
        }

        else {

            $scope.search.name = "";
            $scope.search.mobile = "";
            $scope.search.invoiceNo = "";
            $scope.search.select = "";
            $scope.search.values = "";
            $scope.search.selectValue = "";
            $scope.maxDate = new Date();
            $scope.search.productvalues = "";
            $scope.search.drugName = "";
            $scope.InvoiceSeriesCondition = false;
            $scope.InvoiceSeriesType = "Default";
            $scope.salesCondition = false;
            $scope.dateCondition = false;
            $scope.productCondition = false;
            $scope.EnterCondition = false;
            $scope.CustomerCondition = false;
            //$scope.DoctorCondition = false;
            $scope.search.select1 = "top10"
            $scope.showIstenRecords = false;
            $scope.invoicenumberCondition = false;
            $scope.ispageload = 1;

            $scope.salesSearch();
           
        }
    };

    $scope.InvoiceSeriesType = "Default";
   
    $scope.ChangeInvoiceSeriesDropdown = function (seriestype) {
        

        $scope.InvoiceSeriesType = seriestype;
       
        $scope.search.select1 = "";

        if ($scope.InvoiceSeriesType == 'Default') {
            $scope.search.values = "";
        }
        if ($scope.InvoiceSeriesType == 'Custom') {
            getInvoiceSeriesItems();
            $scope.search.values = "";
        }
        if ($scope.InvoiceSeriesType == 'Manual') {
            $scope.search.select1 = "MAN";
            $scope.search.values = "";
        }

        console.log($scope.InvoiceSeriesType);
        if ($scope.InvoiceSeriesType == 'CustomerCredit') {
            $scope.search.select1 = "CRE";
            $scope.search.values = "";
        }

    }

    $scope.changeCustomerName = function () {
        var custname = document.getElementById("customerName").value;


        console.log(custname);

        if ($scope.sales.salesItem.length > 0) {
            if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
                if (custname == "") {
                    $scope.iscustomerNameMandatory = true;
                } else {
                    $scope.iscustomerNameMandatory = false;
                    $scope.flagCustomername = false;
                }
            }
        }

    };

    $scope.changeDoctorName = function () {
        var doctname = document.getElementById("DoctorName").value;

        console.log(doctname);

        if ($scope.sales.salesItem.length > 0) {
            if ($scope.DoctorNameMandatory == 1 && $scope.schedulecount > 0) {
                if (doctname == "") {
                    $scope.isDoctorNameMandatory = true;
                } else {
                    $scope.isDoctorNameMandatory = false;
                    $scope.flagDoctorName = false;
                }
            }
        }

    };

    $scope.Namecookie = "";
    $scope.getPatient = function (val) {
        // $scope.customerHelper.data.patientSearchData.name = val;
        $scope.Namecookie = val;

        if ($scope.PatientNameSearch == "" || $scope.PatientNameSearch == undefined) {
            $scope.PatientNameSearch = 2;
        }

        if ($scope.PatientNameSearch == 1) {
            return patientService.GetPatientName(val).then(function (response) {

                var origArr = response.data;
                var newArr = [],
           origLen = origArr.length,
           found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });
            });
        } else {
            return patientService.Patientlist(val).then(function (response) {

                var origArr = response.data;
                var newArr = [],
           origLen = origArr.length,
           found, x, y;

                for (x = 0; x < origLen; x++) {
                    found = undefined;
                    for (y = 0; y < newArr.length; y++) {
                        if ($filter('uppercase')(origArr[x].name) === $filter('uppercase')(newArr[y].name) && origArr[x].mobile === newArr[y].mobile) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        newArr.push(origArr[x]);
                    }
                }

                return newArr.map(function (item) {
                    return item;
                });
            });
        }
    };

    //$scope.getCustomerBalance = function () {
    //    customerReceiptService.getCustomerBalance(customerHelper.data.selectedCustomer.mobile, customerHelper.data.selectedCustomer.name).then(function (resp) {
    //        if (resp.data.length == 0) {
    //            customerHelper.data.customerBalance = 0;
    //            return;
    //        }

    //        if (resp.data.credit != undefined)
    //            customerHelper.data.customerBalance = resp.data.credit - resp.data.debit;
    //    });
    //};
    var patientSelected = 0;
    $scope.onPatientSelect = function (obj, event) {
        if (obj !== undefined) {
            $scope.search.values = obj.name;
        }




    };

    getPatientSearchType();
    $scope.PatientNameSearch = "1";
    function getPatientSearchType() {
        newsalesService.getPatientSearchType().then(function (response) {

            if (response.data == "" || response.data == null || response.data == undefined) {
                $scope.PatientNameSearch = "1";
            } else {
                if (response.data.patientSearchType != undefined) {
                    $scope.PatientNameSearch = response.data.patientSearchType;
                } else {
                    $scope.PatientNameSearch = "1";
                }
            }
        }, function () {

        });
    }



    $scope.InvoiceSeriesItems = [];
    function getInvoiceSeriesItems() {
        newsalesService.getInvoiceSeriesItems().then(function (response) {

            if (response.data != "" && response.data != null) {
                $scope.InvoiceSeriesItems = response.data;
            }

        }, function () {

        });
    }

    

    $scope.productSearch = function () {

        $.LoadingOverlay("show");
        $scope.search.page.pageNo = 1;
        $scope.search.searchProductId1 = "";
        if ($scope.search.productvalues)
            $scope.search.searchProductId1 = $scope.search.productvalues.id;

        newsalesService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
             if ($scope.list.length > 0) {
                $scope.selectedSale = $scope.list[0];
            }
            $scope.vendorPurchaseProduct1 = JSON.parse(window.localStorage.getItem("p_dataChange"));
            pagerServcieforsales.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

        $scope.toggleProductDetail = function (obj, data) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+') {

            $scope.search.SalesID = data.id;
            if (data.salesItem.length == 0) {
                newsalesService.getlist($scope.search).then(function (response) {
                    data.salesItem = response.data.salesItem;
                    data.salesItemAudit = response.data.salesItemAudit;
                    data.salesReturnItem = response.data.salesReturnItem;
                });
            }
            $('#chip-btn' + row).text('-');
        }
        else {
            $('#chip-btn' + row).text('+');
        }
    }


    $scope.paidMsg = 'Credit Amount fully paid';
    $scope.offlineMsg = 'Offline disabled, use in online';
    $scope.editSales = function (sales) {
        if (!$scope.IsOffline) {
            sales.IsEdit = true;
        }
        else {
            toastr.info($scope.offlineMsg);
        }
    }

    $scope.saveSales = function (sales) {
        sales.IsEdit = false;
        $.LoadingOverlay("show");
        newsalesService.UpdateSaleDetails(sales).then(function (response) {
            //toastr.success('Data Saved Successfully');
            window.location.assign('/sales/list');
        }, function () {
            $.LoadingOverlay("hide");
            //toastr.error('Error Occured', 'Error');
        });
    }

    $scope.printBill = function (sales) {
        printingHelper.printInvoice(sales.id)
    }

    $scope.viewBill = function (sales) {
        printingHelper.getInvoice(sales.id)
    }

    $scope.clearSearch = function () {
        $scope.search.name = "";
        $scope.search.mobile = "";

        $scope.search.invoiceNo = "";
        $scope.search.select = "";
        $scope.search.values ="";
        $scope.search.selectValue = "";
        //$scope.search.select = 'billDate'
        //$scope.search.select1 = 'equal';
        //$scope.search.values = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.search.selectValue = $filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.maxDate = new Date();
        $scope.search.productvalues = "";
        $scope.search.drugName = "";
        $scope.InvoiceSeriesCondition = false;
        $scope.InvoiceSeriesType = "Default";
        $scope.salesCondition = false;
        $scope.dateCondition = false;
        $scope.productCondition = false;
        $scope.EnterCondition = false;
        //added by nandhini 18.4.17
        $scope.invoicenumberCondition = false;
        //end
        $scope.CustomerCondition = false;
        $scope.search.select1 = "top10"
        $scope.showIstenRecords = false; //Variable added to implement the top 10records on pageload and clear
        $scope.ispageload = 1;
        document.getElementById('productbatchList').scrollTop = 0;
        $scope.salesSearch();
  
    }
    $scope.product = productModel;

    $scope.getProducts = function (val) {

        return productService.SalesProductdrugFilter(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });


    }


    getPrintType = function () {
        newsalesService.getPrintType().then(function (response) {
            $scope.isDotMatrix = response.data;
            window.localStorage.setItem("IsDotMatrix", JSON.stringify($scope.isDotMatrix));
        }, function () {

        });
    }
    getPrintType();

    $scope.CancelItem = function (id) {
        console.log(id);
        if (!$scope.IsOffline) {
            $.LoadingOverlay("show");
            $scope.salesReturn.salesId = id;

            newsalesService.salesReturnDetail($scope.salesReturn.salesId).then(function (response) {
                $scope.salesReturn = response.data;
                console.log(JSON.stringify($scope.salesReturn));
                $scope.salesTotal = $scope.salesReturn.purchasedTotal;
                $scope.salesDiscount = ($scope.salesReturn.purchasedTotal * $scope.salesReturn.discount / 100);
                var checkquantity = 0;
                for (var x = 0; x < $scope.salesReturn.salesReturnItem.length; x++) {

                    var Reorderquntity = 0;
                    if ($scope.salesReturn.salesReturnItem[x].returnedQuantity == undefined || $scope.salesReturn.salesReturnItem[x].returnedQuantity == null || $scope.salesReturn.salesReturnItem[x].returnedQuantity == "") {
                        Reorderquntity = 0;
                    } else {
                        Reorderquntity = $scope.salesReturn.salesReturnItem[x].returnedQuantity
                    }
                    $scope.salesReturn.salesReturnItem[x].quantity = $scope.salesReturn.salesReturnItem[x].purchaseQuantity - Reorderquntity;


                    if ($scope.salesReturn.salesReturnItem[x].quantity != 0) {
                        checkquantity++;
                    }
                    $scope.salesReturn.salesReturnItem[x].cancelType = 1;
                    if ($scope.salesReturn.salesReturnItem[x].sellingPrice > 0) {
                        $scope.salesReturn.salesReturnItem[x].discountAmount = ($scope.salesReturn.salesReturnItem[x].sellingPrice * $scope.salesReturn.salesReturnItem[x].purchaseQuantity) * $scope.salesReturn.salesReturnItem[x].discount / 100;
                        $scope.salesReturn.salesReturnItem[x].discountAmountPerQty = ($scope.salesReturn.salesReturnItem[x].discount * $scope.salesReturn.salesReturnItem[x].sellingPrice) / 100;
                    }
                    else {
                        $scope.salesReturn.salesReturnItem[x].discountAmount = ($scope.salesReturn.salesReturnItem[x].productStock.sellingPrice * $scope.salesReturn.salesReturnItem[x].purchaseQuantity) * $scope.salesReturn.salesReturnItem[x].discount / 100;
                        $scope.salesReturn.salesReturnItem[x].discountAmountPerQty = ($scope.salesReturn.salesReturnItem[x].discount * $scope.salesReturn.salesReturnItem[x].productStock.sellingPrice) / 100;
                    }
                }
                // setPrice();

                if (checkquantity == 0) {

                    alert("All products, full quantity returned");
                    $.LoadingOverlay("hide");

                } else {
                    var r = confirm("Are you sure want to Cancel all Items?");

                    if (r == true) {
                        console.log(JSON.stringify($scope.salesReturn));

                        newsalesService.createSalesReturn($scope.salesReturn).then(function (response) {
                            $scope.salesReturn = response.data;
                            //  toastr.success('Cancelled Items Successfully');
                            alert("cancelled successfully")
                            $scope.salesSearch(); //REdirection commented and Sales search invoked by poongodi on 06/03/2017 based on Punith input
                            // window.location.assign('/SalesReturn/CancelList');
                            $.LoadingOverlay("hide");
                        }, function () {
                            $.LoadingOverlay("hide");
                            // toastr.error('Error Occured', 'Error');
                        });

                    } else {
                        $scope.salesReturn = [];
                        $.LoadingOverlay("hide");
                    }
                }



            }, function () {
                $.LoadingOverlay("hide");
            });
        }
        else {
            toastr.info($scope.paidMsg);
        }
    }


    $scope.greyButton = ""

    dispCashType = function () {
        toastr.info($scope.paidMsg);
    }

    $scope.buildselectedSales = function (selectedSaleItem,index) {
       
        $scope.isSelectedItemMain = index + 1;
        $scope.amt = 0;
        //$scope.selectedSale = selectedSaleItem;
        $scope.selectedSale.id = selectedSaleItem.id;
      
        $scope.selectedSale.cancelstatus = selectedSaleItem.cancelstatus;
     
       
            $scope.search.SalesID = selectedSaleItem.id;
            newsalesService.getlist($scope.search).then(function (response) {
              
                $scope.selectedSale.salesItem = response.data.salesItem;
                $scope.selectedSale.salesItemAudit = response.data.salesItemAudit;
                $scope.selectedSale.salesReturnItem = response.data.salesReturnItem;
                $scope.selectedlist.name = selectedSaleItem.name;
                $scope.selectedlist.mobile = selectedSaleItem.mobile;
                $scope.selectedlist.doctorName = selectedSaleItem.doctorName;
                if($scope.selectedSale.salesItem !== undefined && $scope.selectedSale.salesItem.length > 0)
                {
                          for (var i = 0; i < $scope.selectedSale.salesItem.length; i++) {
                        $scope.amt += Math.round($scope.selectedSale.salesItem[i].total);
                    }
                }
            });

            $scope.focusInputsalehistory = true;
            document.getElementById('productbatchList').focus();
            document.getElementById('historydata').scrollTop = 0;
    }
   
    //var item = { "salesItem": [] };
   

    $scope.keyPressGDmain = function (e,index) {
        if ($scope.isSelectedItemMain == 1) {

        }
        else {
            if (e.keyCode == 38) {
                if ($scope.isSelectedItemMain == 1) {
                    $scope.isSelectedItemMain = ($scope.list.length) + 1;
                    document.getElementById('historydata').scrollTop += 42;
                }
                else {
                    document.getElementById('historydata').scrollTop -= 42;
                }
                $scope.isSelectedItemMain--;
                e.preventDefault();


            }
        }

        if (e.keyCode == 40) { //down arrow
           
            if ($scope.isSelectedItemMain == $scope.list.length) {
                document.getElementById('historydata').scrollTop = 0;
                $scope.isSelectedItemMain = 0;
            }
            else
            {
                document.getElementById('historydata').scrollTop += 42;
            }
            $scope.isSelectedItemMain++;
            e.preventDefault();
          

        }

        if (e.keyCode == 13) {

            $scope.buildselectedSales($scope.list[$scope.isSelectedItemMain - 1], $scope.isSelectedItemMain-1);
            
        }

    }


   

 
    $scope.keyEnter = function (event, e) {



        console.log(e);
        if (event.which === 8) {
            if ($scope.customerHelper.data.patientSearchData.name == "" || $scope.customerHelper.data.patientSearchData.name == undefined) {
                $scope.Namecookie = "";
            }
        }
        var ele = document.getElementById(e);

        if (event.which === 13) // Enter key
        {
            ele.focus();

        }


        if (event.which === 9) {
            $scope.customerHelper.data.patientSearchData.name = $scope.Namecookie;
        }



    };
    $scope.getEnableSelling = function () {
        newsalesService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getEnableSelling();
});