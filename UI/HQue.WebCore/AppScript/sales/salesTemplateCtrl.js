app.controller('salesTemplateCtrl', function ($scope, close, toastr, $rootScope) {
   
    var salesTemplateName = "";
    $scope.focustemplate = true;
    
    $scope.includeTempItem = function () {
        if ($scope.salesTempNewName == null || $scope.salesTempNewName == undefined || $scope.salesTempNewName=='') {
            toastr.error('Please enter the template name');
            $scope.focustemplate = true;
            return;
        }
        salesTemplateName = $scope.salesTempNewName;
        $scope.close('Yes');

    };

    $rootScope.$on("returnPopupEvent", function () {
        $scope.close("No");
    });

    $scope.close = function (result) {
        var object = {};
        $(".modal-backdrop").hide();

        if (result == "Yes") {

            object = { "status": result, "data": salesTemplateName };
        } else {

            $scope.temp.drugName = null;
            $scope.temp = null;
            object = { "status": result, "data": null };

        }
        close(object, 500);

    };
   
   
});