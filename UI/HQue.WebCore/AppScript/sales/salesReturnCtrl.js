﻿app.controller('salesReturnCtrl', function ($scope, toastr, salesReturnModel, salesReturnItemModel, salesService, shortcutHelper, $timeout) {
   
    var salesReturn = salesReturnModel;
    var salesReturnItem = salesReturnItemModel;

    shortcutHelper.setScope($scope);
    $scope.showprice = false;
    $scope.isComposite = false;

    $scope.total = 0;
    $scope.Math = window.Math;
    //$scope.returnQty = 0;
    $scope.saveRtn = true;

    $scope.salesReturn = salesReturn;
    $scope.salesReturn.salesReturnItem = salesReturnItem;
    $scope.returnTotal = 0;
    $scope.totalQuantity = 0;
    $scope.isEnableRoundOff = false;

    $scope.init = function (id) {
        $.LoadingOverlay("show");
        $scope.salesReturn.salesId = id;

        salesService.salesReturnDetail($scope.salesReturn.salesId).then(function (response) {
            $scope.salesReturn = response.data;
            $scope.isComposite = angular.isUndefinedOrNull($scope.salesReturn.isComposite) ? false : $scope.salesReturn.isComposite;

            //console.log(JSON.stringify($scope.salesReturn));
            $scope.salesTotal = $scope.salesReturn.returnItemAmount;

            $scope.salesDiscount = ($scope.salesReturn.purchasedTotal * $scope.salesReturn.discount / 100);

            for (var x = 0; x < $scope.salesReturn.salesReturnItem.length; x++) {


                $scope.salesReturn.salesReturnItem[x].quantity = "";

                $scope.salesReturn.salesReturnItem[x].orgDiscount = $scope.salesReturn.salesReturnItem[x].discount || 0;
                $scope.salesReturn.salesReturnItem[x].returndisct = $scope.salesReturn.salesReturnItem[x].orgDiscount + $scope.salesReturn.discount;

                $scope.totalQuantity += ($scope.salesReturn.salesReturnItem[x].purchaseQuantity - $scope.salesReturn.salesReturnItem[x].returnedQuantity);

                if ($scope.salesReturn.salesReturnItem[x].purchaseQuantity == $scope.salesReturn.salesReturnItem[x].returnedQuantity) {
                    $scope.salesReturn.salesReturnItem[x].disabled = "yes";
                } else {
                    $scope.salesReturn.salesReturnItem[x].disabled = "no";
                }


                if ($scope.salesReturn.salesReturnItem[x].sellingPrice > 0) {
                    $scope.salesReturn.salesReturnItem[x].discountAmount = ($scope.salesReturn.salesReturnItem[x].sellingPrice * $scope.salesReturn.salesReturnItem[x].purchaseQuantity) * $scope.salesReturn.salesReturnItem[x].orgDiscount / 100;
                    $scope.salesReturn.salesReturnItem[x].discountAmountPerQty = ($scope.salesReturn.salesReturnItem[x].orgDiscount * $scope.salesReturn.salesReturnItem[x].sellingPrice) / 100;
                }
                else {
                    $scope.salesReturn.salesReturnItem[x].discountAmount = ($scope.salesReturn.salesReturnItem[x].productStock.sellingPrice * $scope.salesReturn.salesReturnItem[x].purchaseQuantity) * $scope.salesReturn.salesReturnItem[x].orgDiscount / 100;
                    $scope.salesReturn.salesReturnItem[x].discountAmountPerQty = ($scope.salesReturn.salesReturnItem[x].orgDiscount * $scope.salesReturn.salesReturnItem[x].productStock.sellingPrice) / 100;
                }
            }
            setPrice();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };

    $scope.discountPerItem = 0;

    function setPrice() {

        for (var x = 0; x < $scope.salesReturn.salesReturnItem.length; x++) {
            if ($scope.salesReturn.salesReturnItem[x].sellingPrice > 0) {
                $scope.discountPerItem += ($scope.salesReturn.salesReturnItem[x].sellingPrice * $scope.salesReturn.salesReturnItem[x].purchaseQuantity * $scope.salesReturn.salesReturnItem[x].orgDiscount) / 100;
            }
            else {
                $scope.discountPerItem += ($scope.salesReturn.salesReturnItem[x].productStock.sellingPrice * $scope.salesReturn.salesReturnItem[x].purchaseQuantity * $scope.salesReturn.salesReturnItem[x].orgDiscount) / 100;
            }
        }

        for (var i = 0; i < $scope.salesReturn.salesReturnItem.length; i++) {
            $scope.salesReturn.salesReturnItem[i].productStock.sellingPrice -= ($scope.salesReturn.salesReturnItem[i].productStock.sellingPrice * $scope.salesReturn.discount / 100);
        }

        //$scope.salesReturn.purchasedTotal -= ($scope.salesReturn.purchasedTotal * $scope.salesReturn.discount / 100);
        $scope.salesReturn.purchasedTotal = $scope.salesReturn.sales.netAmount;
    }

    $scope.canSaveReturns = true;

    $scope.disableSave = function (data) {
        //if (data.purchaseQuantity - data.returnedQuantity - data.quantity <= 0)
        for (var x = 0; x < data.length; x++)
        {
            if (data[x].purchaseQuantity - data[x].quantity < 0)
                $scope.returnTotal = 0;
            if (data[x].originaldiscount > data[x].discount)
                $scope.returnTotal = 0;

        }
            
    };

    $scope.showRoundedValue = false;

    $scope.editItem = function (item, discount) {

        $scope.returnTotal = 0;
        var rQty = 0;
        var returnTotalQuantity = 0;
        for (var i = 0; i < item.length; i++) {
            if (item[i].quantity != "") {
                returnTotalQuantity += parseInt(item[i].quantity);
            }
            if (item[i].returnedQuantity) {
                rQty = item[i].returnedQuantity;
            } else {
                rQty = 0;
            }
            if (item[i].quantity > (item[i].purchaseQuantity - rQty)) {
                $scope.returnTotal = 0;
                $scope.disableSave(item);
            } else {

                item[i].returndisct = parseFloat(item[i].orgDiscount == "" ? 0 : item[i].orgDiscount) + parseFloat($scope.salesReturn.discount);
                //if (item[i].sellingPrice > 0 && item[i].quantity) {
                //    if (item[i].sellingPrice > 0 && $scope.salesDiscount > 0) {
                //        $scope.returnTotal += ((((item[i].sellingPrice - item[i].discountAmountPerQty) * item[i].purchaseQuantity) - ((item[i].sellingPrice * item[i].purchaseQuantity) * discount / 100)) / item[i].purchaseQuantity) * item[i].quantity;
                //        item[i].rettotsellprice = item[i].sellingPrice - item[i].discountAmountPerQty;
                //        item[i].showprice = false;
                //    } else {
                //        $scope.returnTotal += (item[i].sellingPrice - item[i].discountAmountPerQty) * item[i].quantity;
                //        item[i].rettotsellprice = item[i].sellingPrice - item[i].discountAmountPerQty;
                //        item[i].showprice = false;
                //    }
                //} else {
                //    if (item[i].quantity)
                //        $scope.returnTotal += (item[i].productStock.sellingPrice - item[i].discountAmountPerQty) * item[i].quantity;
                //    item[i].rettotsellprice = item[i].sellingPrice - item[i].discountAmountPerQty;
                //    item[i].showprice = false;
                //}
                //$scope.disableSave(item);

                if (item[i].sellingPrice > 0 && item[i].quantity) {
                    if (item[i].sellingPrice > 0) {
                        $scope.returnTotal += ((((item[i].sellingPrice) * item[i].purchaseQuantity) - ((item[i].sellingPrice * item[i].purchaseQuantity) * item[i].returndisct / 100)) / item[i].purchaseQuantity) * item[i].quantity;
                        item[i].rettotsellprice = item[i].sellingPrice;
                        item[i].showprice = false;
                    } else {
                        $scope.returnTotal += (item[i].sellingPrice) * item[i].quantity;
                        item[i].rettotsellprice = item[i].sellingPrice;
                        item[i].showprice = false;
                    }
                } else {
                    if (item[i].quantity)
                        $scope.returnTotal += (item[i].productStock.sellingPrice) * item[i].quantity;
                    item[i].rettotsellprice = item[i].sellingPrice;
                    item[i].showprice = false;
                }
                $scope.disableSave(item);
            
            }
            rQty = 0;
        }
        $scope.salesReturn.returnTotal = $scope.returnTotal;
        if ($scope.returnTotal > 0) {
            $scope.salesReturn.returnChargePercent = $scope.salesReturn.returnCharges * 100 / $scope.returnTotal;
        }
        $scope.returnTotal -= $scope.salesReturn.returnCharges;
       
        if ($scope.isEnableRoundOff && $scope.totalQuantity == returnTotalQuantity) {
            $scope.showRoundedValue = true;
            $scope.salesReturn.returnTotal = Math.round($scope.returnTotal);
        } else {
            $scope.showRoundedValue = false;
        }
        setModelValues();
    };

    $scope.save = function () {
        $.LoadingOverlay("show");
        if ($scope.saveRtn) {
            $scope.saveRtn = false;
            salesService.createSalesReturn($scope.salesReturn).then(function (response) {
                //$scope.salesReturn = response.data;                
                if (response.data.errorStatus === 1) {
                    $.LoadingOverlay("hide");                    
                    toastr.info(response.data.errorMessage, "", 2000);


                    angular.forEach($scope.salesReturn.salesReturnItem, function (data, index) {
                        data.quantity = null;
                    });
                    $scope.returnTotal = 0;

                    $timeout(function () {
                        window.location.assign('/salesReturn/index?id=' + $scope.salesReturn.salesId);
                    }, 2000);






                } else {
                    if ($scope.saveRtn == false && response.data.errorStatus == 0) {
                        toastr.success('Data Saved Successfully');
                        window.location.assign('/sales/list');
                    }
                }
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
            });
        }
    };

    $scope.keydown = shortcutHelper.salesReurnShortcuts;

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to cancel?');
        if (cancel) {
            for (var i = 0; i < $scope.salesReturn.salesReturnItem.length; i++) {
                $scope.salesReturn.salesReturnItem[i].quantity = "";
            }
            $scope.returnTotal = 0;
            window.location.assign('/sales/list');
        }
    };

    $scope.togglePageLoad = function () {
        $(".pat-info-btn").text('+');
        $(".alter-row-collapse").hide();
    };

    $scope.togglePageLoad();

    $scope.togglePatientDetail = function () {
        $(".alter-row-collapse").slideToggle();
        $(".alter-row-collapse").show();
        if ($(".pat-info-btn").text() === '+')
            $(".pat-info-btn").text('-');
        else {
            $(".pat-info-btn").text('+');
        }
    };

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        salesService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    }
    $scope.getEnableSelling();



    //Added by arun for return % calculation 0n 21.09.2017
    $scope.Retprice = 0;
    $scope.addvalue = [];
    var cc = 0;
  
    $scope.retDisctChange = function(item,disct)
    {
        $scope.showprice = true;
        for (var i = 0; i < item.length; i++) {
            
            item[i].returndisct = parseFloat(item[i].orgDiscount == "" ? 0 : item[i].orgDiscount) + parseFloat($scope.salesReturn.discount);
           
            }
        }

    // Ended by arun

    angular.isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null || val === ""
    }

    //By San - 15-11-2017
    $scope.getRoundOffSettings = function () {
        $.LoadingOverlay("show");
        salesService.getRoundOffSettings().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.isEnableRoundOff = response.data;
            if ($scope.isEnableRoundOff == null) {
                $scope.isEnableRoundOff = false;
            }
        }, function (error) {
            $.LoadingOverlay("hide");
        });
    }
    $scope.getRoundOffSettings();

    function setModelValues() {        
        $scope.salesReturn.totalDiscountValue = 0;
        $scope.salesReturn.returnItemAmount = 0;
        $scope.salesReturn.netAmount = 0;
        $scope.salesReturn.gstAmount = 0;
        $scope.salesReturn.roundOffNetAmount = 0;

        for (var i = 0; i < $scope.salesReturn.salesReturnItem.length; i++) {
            $scope.salesReturn.salesReturnItem[i].orgDiscount = $scope.salesReturn.salesReturnItem[i].orgDiscount || 0;
            $scope.salesReturn.discount = $scope.salesReturn.discount || 0;

            var discount = parseFloat($scope.salesReturn.salesReturnItem[i].orgDiscount) + $scope.salesReturn.discount;
            $scope.salesReturn.salesReturnItem[i].discount = discount;

            var quantity = Number($scope.salesReturn.salesReturnItem[i].quantity);
            var sellingPrice = $scope.salesReturn.salesReturnItem[i].sellingPrice;
            var totalItemAmount = sellingPrice * quantity;

            $scope.salesReturn.salesReturnItem[i].discountAmount = totalItemAmount * discount / 100;
            $scope.salesReturn.salesReturnItem[i].totalAmount = totalItemAmount - $scope.salesReturn.salesReturnItem[i].discountAmount;

            var taxPerc = $scope.salesReturn.salesReturnItem[i].productStock.gstTotal || 0;
            var taxAmt = ($scope.salesReturn.salesReturnItem[i].totalAmount - (($scope.salesReturn.salesReturnItem[i].totalAmount / (100 + parseFloat(taxPerc))) * 100));
            $scope.salesReturn.salesReturnItem[i].gstAmount = taxAmt;

            $scope.salesReturn.gstAmount += $scope.salesReturn.salesReturnItem[i].gstAmount;
            $scope.salesReturn.totalDiscountValue += $scope.salesReturn.salesReturnItem[i].discountAmount;
            $scope.salesReturn.returnItemAmount += totalItemAmount;
            $scope.salesReturn.netAmount += $scope.salesReturn.salesReturnItem[i].totalAmount;
        }
        $scope.salesReturn.discountValue = $scope.salesReturn.returnItemAmount * $scope.salesReturn.discount / 100;
        if ($scope.isEnableRoundOff && $scope.showRoundedValue) {
            $scope.salesReturn.isRoundOff = true;
            $scope.salesReturn.roundOffNetAmount = Math.round($scope.salesReturn.netAmount) - $scope.salesReturn.netAmount;
            $scope.salesReturn.netAmount = Math.round($scope.salesReturn.netAmount);
        }
        else {
            $scope.salesReturn.isRoundOff = false;
            $scope.salesReturn.roundOffNetAmount = 0;
        }
    };

});

app.filter('round', function () {
    return function (input) {
        return Math.round(input);
    };
});