﻿app.controller('editReturnOnlyCtrl', function ($scope, toastr, salesReturnModel, salesReturnItemModel, salesService, shortcutHelper, $timeout) {
   
    var salesReturn = salesReturnModel;
    var salesReturnItem = salesReturnItemModel;

    shortcutHelper.setScope($scope);
    $scope.retqtychange = false;

    $scope.total = 0;
    $scope.Math = window.Math;
    $scope.saveRtn = true;
    $scope.showbtn = false;
    $scope.salesReturn = salesReturn;
    $scope.salesReturn.salesReturnItem = salesReturnItem;
    $scope.returnTotal = 0;
    $scope.roundOff = 0;
    $scope.totalQuantity = 0;
    $scope.editCount = 0;
    $scope.returnType = null;
    $scope.returnAmount = 0;
    $scope.discountPerItem = 0;

    $scope.init = function (id) {
        $.LoadingOverlay("show");
        $scope.salesReturn.id = id;

        salesService.getOnlyReturnItems(id).then(function (response) {
            $scope.salesReturn = response.data;

            if ($scope.salesReturn.salesId != null && $scope.salesReturn.salesId != undefined && $scope.salesReturn.salesId != "") {
                if ($scope.salesReturn.isAlongWithSale == 1) {
                    $scope.returnType = "return";
                }
                else {
                    $scope.returnType = "sales";
                }
                
            }
            else {
                $scope.returnType = "return";
            }

            $scope.salesReturnTotal = $scope.salesReturn.totalWithoutDiscount;
            
            if ($scope.salesReturn.discount != null && $scope.salesReturn.discount != undefined) {
                $scope.returnDiscountAmt = ($scope.salesReturnTotal * $scope.salesReturn.discount) / 100;
            }
            else {
                $scope.returnDiscountAmt = 0;
                $scope.salesReturn.discount = 0;
            }

            $scope.previousReturnedAmt = $scope.salesReturnTotal - ($scope.returnDiscountAmt + $scope.salesReturn.totalItemDiscountAmount);

            for (var x = 0; x < $scope.salesReturn.salesReturnItem.length; x++) {
                
                $scope.salesReturn.salesReturnItem[x].isEdit = false;
                $scope.salesReturn.salesReturnItem[x].originaldiscount = $scope.salesReturn.salesReturnItem[x].discount;
                //$scope.salesReturn.salesReturnItem[x].soldPrice = $scope.salesReturn.salesReturnItem[x].mrpSellingPrice > 0 ?                                       $scope.salesReturn.salesReturnItem[x].mrpSellingPrice : $scope.salesReturn.salesReturnItem[x].productStock.sellingPrice;
                $scope.salesReturn.salesReturnItem[x].returnedQuantity = $scope.salesReturn.salesReturnItem[x].quantity;
                //if ($scope.salesReturn.salesReturnItem[x].mrpSellingPrice > 0) {
                //    $scope.salesReturn.salesReturnItem[x].discountAmount = ($scope.salesReturn.salesReturnItem[x].mrpSellingPrice * $scope.salesReturn.salesReturnItem[x].quantity) * $scope.salesReturn.salesReturnItem[x].discount / 100;
                //    $scope.salesReturn.salesReturnItem[x].discountAmountPerQty = ($scope.salesReturn.salesReturnItem[x].discount * $scope.salesReturn.salesReturnItem[x].mrpSellingPrice) / 100;
                //}
                //else {
                //    $scope.salesReturn.salesReturnItem[x].discountAmount = ($scope.salesReturn.salesReturnItem[x].productStock.sellingPrice * $scope.salesReturn.salesReturnItem[x].quantity) * $scope.salesReturn.salesReturnItem[x].discount / 100;
                //    $scope.salesReturn.salesReturnItem[x].discountAmountPerQty = ($scope.salesReturn.salesReturnItem[x].discount * $scope.salesReturn.salesReturnItem[x].productStock.sellingPrice) / 100;
                //}

                if ($scope.salesReturn.salesReturnItem[x].mrpSellingPrice > 0 && $scope.salesReturn.salesReturnItem[x].quantity) {
                    if ($scope.salesReturn.salesReturnItem[x].mrpSellingPrice > 0) {
                        $scope.returnTotal += (((($scope.salesReturn.salesReturnItem[x].mrpSellingPrice) * $scope.salesReturn.salesReturnItem[x].purchaseQuantity) -
                            (($scope.salesReturn.salesReturnItem[x].mrpSellingPrice * $scope.salesReturn.salesReturnItem[x].purchaseQuantity) *
                               (($scope.salesReturn.salesReturnItem[x].discount + $scope.salesReturn.discount) / 100))) / $scope.salesReturn.salesReturnItem[x].purchaseQuantity) *
                            $scope.salesReturn.salesReturnItem[x].quantity;
                        $scope.salesReturn.salesReturnItem[x].rettotsellprice = $scope.salesReturn.salesReturnItem[x].mrpSellingPrice;
                        $scope.salesReturn.salesReturnItem[x].showprice = false;
                        $scope.salesReturn.salesReturnItem[x].returndisct = parseFloat($scope.salesReturn.salesReturnItem[x].discount == "" || $scope.salesReturn.salesReturnItem[x].discount == undefined ? 0 : $scope.salesReturn.salesReturnItem[x].discount) + parseFloat($scope.salesReturn.discount);
                    } else {
                        $scope.returnTotal += ($scope.salesReturn.salesReturnItem[x].mrpSellingPrice) * $scope.salesReturn.salesReturnItem[x].quantity;
                        $scope.salesReturn.salesReturnItem[x].rettotsellprice = $scope.salesReturn.salesReturnItem[x].mrpSellingPrice;
                        $scope.salesReturn.salesReturnItem[x].showprice = false;
                        $scope.salesReturn.salesReturnItem[x].returndisct = parseFloat($scope.salesReturn.salesReturnItem[x].discount == "" || $scope.salesReturn.salesReturnItem[x].discount == undefined ? 0 : $scope.salesReturn.salesReturnItem[x].discount) + parseFloat($scope.salesReturn.discount);

                    }
                } else {
                    if ($scope.salesReturn.salesReturnItem[x].quantity)
                        $scope.returnTotal += ($scope.salesReturn.salesReturnItem[x].productStock.sellingPrice) * $scope.salesReturn.salesReturnItem[x].quantity;
                    $scope.salesReturn.salesReturnItem[x].rettotsellprice = $scope.salesReturn.salesReturnItem[x].mrpSellingPrice;
                    $scope.salesReturn.salesReturnItem[x].showprice = false;
                    $scope.salesReturn.salesReturnItem[x].returndisct = parseFloat($scope.salesReturn.salesReturnItem[x].discount == "" || $scope.salesReturn.salesReturnItem[x].discount == undefined ? 0 : $scope.salesReturn.salesReturnItem[x].discount) + parseFloat($scope.salesReturn.discount);

                }
            }


            
            //setPrice();
          $scope.calculateTotalReturn();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };


    function setPrice() {

        for (var x = 0; x < $scope.salesReturn.salesReturnItem.length; x++) {
            if ($scope.salesReturn.salesReturnItem[x].mrpSellingPrice > 0) {
                $scope.discountPerItem += ($scope.salesReturn.salesReturnItem[x].mrpSellingPrice * $scope.salesReturn.salesReturnItem[x].quantity * $scope.salesReturn.salesReturnItem[x].discount) / 100;
            }
            else {
                $scope.discountPerItem += ($scope.salesReturn.salesReturnItem[x].productStock.sellingPrice * $scope.salesReturn.salesReturnItem[x].quantity * $scope.salesReturn.salesReturnItem[x].discount) / 100;
            }
        }
        
    }

        
    $scope.editSalesReturnItem = function (item1) {
        item1.isEdit = true;
        $scope.tempItem = item1;
        $scope.editCount++;
        $scope.editdata(item1);
    }


    $scope.editdata = function(item1)
    {
        $scope.returnTotal = 0;
        for (i = 0; i < item1.length; i++) {
            if (item1[i].isEdit) {

                if (item1[i].mrpSellingPrice > 0 && item1[i].quantity) {
                    if (item1[i].mrpSellingPrice > 0) {
                        $scope.returnTotal += ((((item1[i].mrpSellingPrice) * item1[i].purchaseQuantity) - ((item1[i].mrpSellingPrice * item1[i].purchaseQuantity) * (parseFloat(item1[i].discount) + parseFloat($scope.salesReturn.discount)) / 100)));
                        item1[i].rettotsellprice = item1[i].mrpSellingPrice;
                        item1[i].showprice = false;
                        item1[i].returndisct = parseFloat(item1[i].discount == "" || item1[i].discount == undefined ? 0 : item1[i].discount) + parseFloat($scope.salesReturn.discount);

                    } else {
                        $scope.returnTotal += (item1[i].mrpSellingPrice) * item1[i].quantity;
                        item1[i].rettotsellprice = item1[i].mrpSellingPrice;
                        item1[i].showprice = false;
                        item1[i].returndisct = parseFloat(item1[i].discount == "" || item1[i].discount == undefined ? 0 : item1[i].discount) + parseFloat($scope.salesReturn.discount);
                    }
                } else {
                    if (item1[i].quantity)
                        $scope.returnTotal += (item1[i].productStock.sellingPrice) * item1[i].quantity;
                    item1[i].rettotsellprice = item1[i].mrpSellingPrice;
                    item1[i].showprice = false;
                    item1[i].returndisct = parseFloat(item1[i].discount == "" || item1[i].discount == undefined ? 0 : item1[i].discount) + parseFloat($scope.salesReturn.discount);
                }
            }
        }
    }

    $scope.removeSalesReturnItem = function (item1) {
        if (!confirm("Are you sure, Do you want to delete ? "))
            return;
        var index = $scope.salesReturn.salesReturnItem.indexOf(item1);

        if (item1.isEdit == true) {
            $scope.editCount--;
        }

        //$scope.salesReturn.salesReturnItem[index].isDeleted = true;
        //item1.isDeleted = true;
        $scope.salesReturn.salesReturnItem.splice(index, 1);
        $scope.calculateTotalReturn();
    }

    $scope.saveSalesReturnItem = function (item1) {
        item1.isEdit = false;
        //if (item1.mrpSellingPrice == 0 || item1.mrpSellingPrice == "" || item1.mrpSellingPrice == null || item1.mrpSellingPrice == undefined) {
        //    item1.mrpSellingPrice = item1.productStock.sellingPrice;
        //}
        //if (item1.discount == "" || item1.discount == null || item1.discount == undefined) {
        //    item1.discount = 0;
        //}
        //if (item1.returnedQuantity == "" || item1.returnedQuantity == null || item1.returnedQuantity == undefined) {
        //    item1.returnedQuantity = 0;
        //}

        $scope.calculateTotalReturn();
        $scope.editCount--;
        $scope.showbtn = true;
        $scope.tempItem = null; 
    }

    $scope.updateAction = function (data) {
        if (data.action == 'I') {
            data.action = 'U';
        }        
        $scope.calculateTotalReturn();
    }

    $scope.calculateTotalReturn = function () {
        $scope.returnTotal = 0;
        $scope.returnAmount = 0;
        var temp1 = 0;

        //for (var i = 0 ; i < $scope.salesReturn.salesReturnItem.length; i++) {
        //    //if ($scope.salesReturn.salesReturnItem[i].isDeleted != true) {
        //        var sellPrice = $scope.salesReturn.salesReturnItem[i].mrpSellingPrice > 0 ? $scope.salesReturn.salesReturnItem[i].mrpSellingPrice : $scope.salesReturn.salesReturnItem[i].productStock.sellingPrice;
        //        $scope.salesReturn.salesReturnItem[i].returndisct = parseFloat($scope.salesReturn.salesReturnItem[i].discount == "" || $scope.salesReturn.salesReturnItem[i].discount == undefined ? 0 : $scope.salesReturn.salesReturnItem[i].discount) + parseFloat($scope.salesReturn.discount);

        //        if ($scope.salesReturn.salesReturnItem[i].returnedQuantity >= 0) {
        //            temp1 += ($scope.salesReturn.salesReturnItem[i].returnedQuantity * sellPrice) -
        //                ($scope.salesReturn.salesReturnItem[i].returnedQuantity * sellPrice * ($scope.salesReturn.discount + $scope.salesReturn.salesReturnItem[i].discount) / 100 );
        //        }
        //        else {
        //            temp1 += ($scope.salesReturn.salesReturnItem[i].quantity * sellPrice) - ($scope.salesReturn.salesReturnItem[i].quantity * sellPrice * $scope.salesReturn.salesReturnItem[i].discount / 100);
        //        }
        //    //}            
        //        $scope.returnTotal += (((($scope.salesReturn.salesReturnItem[i].mrpSellingPrice) * $scope.salesReturn.salesReturnItem[i].purchaseQuantity) -
        //            (($scope.salesReturn.salesReturnItem[i].mrpSellingPrice * $scope.salesReturn.salesReturnItem[i].purchaseQuantity) * (parseFloat($scope.salesReturn.salesReturnItem[i].discount) + parseFloat($scope.salesReturn.discount)) / 100)));
               
        //}
        //$scope.returnTotal -= $scope.salesReturn.returnCharges;
        //if ($scope.salesReturn.discount > 0) {
        //    $scope.returnDiscountAmt = (temp1 * $scope.salesReturn.discount) / 100;
        //}
        

        for (var i = 0 ; i < $scope.salesReturn.salesReturnItem.length; i++) {
            
            var sellPrice = $scope.salesReturn.salesReturnItem[i].mrpSellingPrice > 0 ? $scope.salesReturn.salesReturnItem[i].mrpSellingPrice : $scope.salesReturn.salesReturnItem[i].productStock.sellingPrice;
            $scope.salesReturn.salesReturnItem[i].returndisct = parseFloat($scope.salesReturn.salesReturnItem[i].discount == "" || $scope.salesReturn.salesReturnItem[i].discount == undefined ? 0 : $scope.salesReturn.salesReturnItem[i].discount) + parseFloat($scope.salesReturn.discount);

            if ($scope.salesReturn.salesReturnItem[i].Quantity >= 0) {
                temp1 += ($scope.salesReturn.salesReturnItem[i].Quantity * sellPrice) -
                    ($scope.salesReturn.salesReturnItem[i].Quantity * sellPrice * ($scope.salesReturn.discount + $scope.salesReturn.salesReturnItem[i].discount) / 100);
            }
            else {
                temp1 += ($scope.salesReturn.salesReturnItem[i].quantity * sellPrice) - ($scope.salesReturn.salesReturnItem[i].quantity * sellPrice * $scope.salesReturn.salesReturnItem[i].discount / 100);
            }
        }


        $scope.returnTotal = temp1 - $scope.returnDiscountAmt;

        var temp2 = Math.round($scope.returnTotal);
        $scope.roundOff = temp2 - $scope.returnTotal;

        //$scope.returnAmount = Math.abs($scope.salesReturn.subTotal - $scope.returnTotal);

    }

    $scope.save = function () {
        $.LoadingOverlay("show");
        if ($scope.saveRtn) {
            $scope.saveRtn = false;
            $scope.salesReturn.returnTotal = $scope.returnTotal; // Added by Settu for voucher adjustment in credit sales return edit

            angular.forEach($scope.salesReturn.salesReturnItem, function (data, index) {
                data.returnedQuantity = data.quantity;
            });

            salesService.updateSalesReturn($scope.salesReturn).then(function (response) {
                //$scope.salesReturn = response.data;                
                if (response.data.errorStatus === 1) {
                    $.LoadingOverlay("hide");                    
                    toastr.info(response.data.errorMessage, "", 2000);


                    angular.forEach($scope.salesReturn.salesReturnItem, function (data, index) {
                        data.isEdit = false;
                        data.returnedQuantity = data.quantity;
                    });
                    $scope.returnTotal = 0;

                    $timeout(function () {
                        window.location.assign('/salesReturn/editReturnOnly?id=' + $scope.salesReturn.id);
                    }, 2000);


                } else {                    
                        toastr.success('Data Saved Successfully');
                        window.location.assign('/salesReturn/list');                    
                }
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
            });
        }
    };

    $scope.keydown = shortcutHelper.salesReurnShortcuts;

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to cancel?');
        if (cancel) {
            //for (var i = 0; i < $scope.salesReturn.salesReturnItem.length; i++) {
            //    $scope.salesReturn.salesReturnItem[i].isEdit = false;
            //    $scope.salesReturn.salesReturnItem[i].returnedQuantity = $scope.salesReturn.salesReturnItem[i].quantity;
            //}
            $scope.returnTotal = 0;
            window.location.assign('/SalesReturn/List');
        }
    };

    $scope.togglePageLoad = function () {
        $(".pat-info-btn").text('+');
        $(".alter-row-collapse").hide();
    };

    $scope.togglePageLoad();

    $scope.togglePatientDetail = function () {
        $(".alter-row-collapse").slideToggle();
        $(".alter-row-collapse").show();
        if ($(".pat-info-btn").text() === '+')
            $(".pat-info-btn").text('-');
        else {
            $(".pat-info-btn").text('+');
        }
    };

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    };

    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        salesService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    }
    $scope.getEnableSelling();
});




