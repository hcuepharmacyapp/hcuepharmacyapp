﻿app.controller('showSalesOrderEstimateCtrl', function ($scope, vendorPurchaseModel, productStockModel, productModel, close, dCVendorPurchaseItemModel, SalesOrderEstimateValue, salesOrderEstimateModel, salesOrderEstimateItemModel, cacheService, $rootScope) {


    $rootScope.$on("returnOrderEstimatePopupEvent", function () {
        $scope.close("No");
    });

    $scope.close = function (result) {
        //var object = {};
        ////var dcpoObject = { "DCItem": $scope.dcVendorPurchaseItem, "POItem": $scope.poVendorPurchaseItem };
        //if (result == "Yes") {
        //    //object = { "status": result, "data": dcpoObject };
        //} else {
        //    object = { "status": result, "data": null };
        //}
        //close(object, 100);

        //$(".modal-backdrop").hide();

        close(result, 500); // close, but give 500ms for bootstrap to animate
        if (result != "Yes") {
            //$scope.templateNameList = [];
        }
        $(".modal-backdrop").hide();
    };


    if (SalesOrderEstimateValue[0].salesOrderList != null && SalesOrderEstimateValue[0].salesOrderList.length > 0) {
        var salesOrderItem = SalesOrderEstimateValue[0].salesOrderList;
        $scope.dcVendorPurchaseItem = salesOrderItem;
        for (var i = 0; i < $scope.dcVendorPurchaseItem.length; i++) {
            if ($scope.dcVendorPurchaseItem[i].salesOrderEstimateType == 1)
            {
                $scope.isOrder = 1;
            }
            else
            {
                $scope.isEstimate = 2;
            }
        }
    }
    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper1' + row).slideToggle();
        $('#chip-wrapper1' + row).show();
        if ($('#chip-btn1' + row).text() === '+')
            $('#chip-btn1' + row).text('-');
        else {
            $('#chip-btn1' + row).text('+');
        }
    };
    $scope.toggleProductEstimateDetail = function (obj) {
        var row = obj.target.getAttribute("data");
        $('#chip-wrapper2' + row).slideToggle();
        $('#chip-wrapper2' + row).show();
        if ($('#chip-btn2' + row).text() === '+')
            $('#chip-btn2' + row).text('-');
        else {
            $('#chip-btn2' + row).text('+');
        }
    };
    $scope.EstimateConvertTosale = function (ind, obj) {

       // window.localStorage.setItem("SelectedCustomerOrderEstimete", JSON.stringify(obj));
        cacheService.set('SelectedCustomerOrderEstimete', obj);
        $scope.close('Yes');
        //window.location.assign('/sales/index');
    }

});



