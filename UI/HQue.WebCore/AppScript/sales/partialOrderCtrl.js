app.controller('partialOrderCtrl', function ($scope, $filter, $rootScope, close, toastr, cacheService, ModalService, productService, salesService, productModel, productStockService, productStockModel, tempVendorPurchaseItemModel) {

    
    function init()
    {
        $scope.sales = cacheService.get('salesObj');
        editSale = cacheService.get('EditSale');
        cacheService.remove('salesObj');
    }
    init();

    $scope.close = function () {
        console.log("inside close");
        close(100); // close, but give 100ms for bootstrap to animate
        if (editSale) {
            window.location = window.location.origin + "/Sales/List";
        } else {
            window.location.reload();
        }
        //$rootScope.$emit("disableEscEvent");
        $(".modal-backdrop").hide();
    };

});

