app.controller('updateGSTINPopupCtrl', function ($scope, $rootScope, close, toastr, cacheService, ModalService, salesService, gstIn) {
    $scope.gsTinNo = null;
    $scope.isEmail = false;
    $scope.originalGsTinNo = null;
    $scope.instanceModel = { "gsTinNo": null, "isEmail": null };
    $scope.focusgst = true;
    $scope.close = function (result) {
        close(result, 500);
        $(".modal-backdrop").hide();
    };
    $scope.update = function () {
        if ($scope.gsTinNo != null && $scope.gsTinNo != "") {
            $scope.instanceModel.gsTinNo = $scope.gsTinNo;
            $scope.instanceModel.isEmail = $scope.isEmail;
            salesService.saveGstIn($scope.instanceModel)
                .then(function (response) {
                    $scope.close("No");
                    toastr.success("Your GSTIN No Updated Successfully");
                }, function (error) {
                    console.log(error);
                });
        }
    };
    if (gstIn != null) {
        $scope.gsTinNo = gstIn;
        $scope.originalGsTinNo = gstIn;
    }
});