﻿app.controller('barcodePRNDesignCtrl', function ($scope, ModalService, $filter, productStockService) {

    $scope.setData = function () {
        $scope.mode = "NEW";
        angular.element("input[type='file']").val(null);
        lblPRNFile.innerHTML = "Please Browse PRN File";
        $scope.prnDesign = {
            id: null,
            prnName: "",
            prnFileData: "",
            prnFieldJson: [
                {
                    seqId: 1,
                    field: "Barcode",
                    prnField: "",
                    tag: "barcodeValue",
                    isMandatory: true,
                },
                {
                    seqId: 2,
                    field: "No.of Rows",
                    prnField: "",
                    tag: "noOfStickersQty",
                    isMandatory: true,
                },
                {
                    seqId: 3,
                    field: "Product Code",
                    prnField: "",
                    tag: "productCode"
                },
                {
                    seqId: 4,
                    field: "Product Name",
                    prnField: "",
                    tag: "productName"
                },
                {
                    seqId: 5,
                    field: "Batch No",
                    prnField: "",
                    tag: "batchNo"
                },
                {
                    seqId: 6,
                    field: "Expiry Date",
                    prnField: "",
                    tag: "expireDate"
                },
                {
                    seqId: 7,
                    field: "Rate",
                    prnField: "",
                    tag: "rate"
                },
                {
                    seqId: 8,
                    field: "MRP",
                    prnField: "",
                    tag: "mrp"
                },
            ]
        };
        $scope.setFocus("barcodePRNDesignName");
    };

    $scope.savePRNDesign = function () {
        if (document.getElementById("barcodePRNDesignName").value == "") {
            ShowConfirmMsgWindow("Please enter [Barcode PRN Design Name] and try again.", "barcodePRNDesignName");
            return;
        }

        var file = document.getElementById("selectBarcodePRNDesignFile").files[0];
        if (file) {
            var aReader = new FileReader();
            aReader.readAsText(file, "UTF-8");
            aReader.onload = function (evt) {
                $scope.prnDesign.prnFileData = aReader.result;
                $scope.prnDesign.prnFileName = document.getElementById("selectBarcodePRNDesignFile").files[0].name;
                if ($scope.prnDesign.prnFileName.substring($scope.prnDesign.prnFileName.indexOf(".") + 1, $scope.prnDesign.prnFileName.length).toUpperCase() != "PRN") {
                    ShowConfirmMsgWindow("Please select PRN file and try again.", "selectBarcodePRNDesignFile", true, "popup-width-500");
                    return;
                }
                save();             
            }
            aReader.onerror = function (evt) {
                $scope.prnDesign.prnFileData = "";
                ShowConfirmMsgWindow("Unable to read selected [Barcode PRN Design File]. Please select correct file and try again.", "selectBarcodePRNDesignFile", true, "popup-width-500");
                return;
            }
        }
        else {
            if ($scope.mode == "EDIT") {
                save();
            }
            else {
                ShowConfirmMsgWindow("Please select [Barcode PRN Design File] and try again.", "selectBarcodePRNDesignFile");
                return;
            }
        }
    };

    function save() {
        var isPRNFieldMapped = false;
        for (var i = 0; i < $scope.prnDesign.prnFieldJson.length; i++) {
            if ($scope.prnDesign.prnFieldJson[i].prnField != "") {
                isPRNFieldMapped = true;
                if (!($scope.prnDesign.prnFileData.indexOf($scope.prnDesign.prnFieldJson[i].prnField) !== -1)) {
                    ShowConfirmMsgWindow("PRN Field[" + $scope.prnDesign.prnFieldJson[i].prnField + "] not found in the selected file[" + $scope.prnDesign.prnFileName + "]. Please enter correct PRN Field and try again.", "prnField" + i);
                    return;
                }
            }
            else if ($scope.prnDesign.prnFieldJson[i].isMandatory) {
                ShowConfirmMsgWindow("Please enter mandatory PRN Field [" + $scope.prnDesign.prnFieldJson[i].field + "].", "prnField" + i);
                return;
            }
        }
        if (!isPRNFieldMapped) {
            ShowConfirmMsgWindow("Please enter atleast one [PRN Field] and try again.", "prnField0");
            return;
        }

        $.LoadingOverlay("show");
        productStockService.getBarcodePrnDesignList("").then(function (response) {
            $.LoadingOverlay("hide");

            $scope.barcodePrnDesignList = response.data;
            var prnExist = $filter("filter")($scope.barcodePrnDesignList, { prnFileData: $scope.prnDesign.prnFileData }, true);
            if (prnExist.length != 0 && ($scope.mode == "NEW" || prnExist[0].id != $scope.prnDesign.id && $scope.mode == "EDIT")) {
                ShowConfirmMsgWindow("Same [Barcode PRN Design] already exists with [" + prnExist[0].prnName + "].");
                return;
            }
            var nameExist = $filter("filter")($scope.barcodePrnDesignList, { prnName: $scope.prnDesign.prnName }, true);
            if (nameExist.length != 0 && ($scope.mode == "NEW" || nameExist[0].id != $scope.prnDesign.id && $scope.mode == "EDIT")) {
                ShowConfirmMsgWindow("Barcode PRN Design Name already exists. Please provide different one.", "barcodePRNDesignName");
                return;
            }

            var model = JSON.parse(JSON.stringify($scope.prnDesign));
            model.prnFieldJson = JSON.stringify(model.prnFieldJson);

            var data = {
                msgTitle: "",
                msg: "Are you sure to " + ($scope.mode == "NEW" ? "save" : "update") + " [" + $scope.prnDesign.prnName + "]?",
                showOk: false,
                showYes: true,
                showNo: true,
                showCancel: false,
                focusOnNo: false
            };
            var m = ModalService.showModal({
                "controller": "showConfirmMsgCtrl",
                "templateUrl": 'showConfirmMsgModal',
                "inputs": { "params": [{ "data": data }] },
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result == "Yes") {
                        $.LoadingOverlay("show");
                        productStockService.updateBarcodePrnDesign(model).then(function (response) {
                            $.LoadingOverlay("hide");
                            ShowConfirmMsgWindow('Barcode PRN Design [' + $scope.prnDesign.prnName + '] has been ' + ($scope.mode == "NEW" ? 'uploaded' : 'updated') + ' successfully.', "barcodePRNDesignName", false, "", true);
                        }, function (error) {
                            console.log(error);
                            $.LoadingOverlay("hide");
                            ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
                        });
                    };
                });
            });

        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
        });
    };

    $scope.setFocus = function (tag) {
        var ele = document.getElementById(tag);
        if (ele != null) {
            ele.focus();
        }
    };

    function ShowConfirmMsgWindow(msg, focusTag, showExclamation, width, isReset) {
        var data = {
            msgTitle: "",
            msg: msg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: "",
            showExclamation: showExclamation,
            popupWidth: width
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.setFocus(focusTag);
                if (isReset) {
                    $scope.setData();
                }
            });
        });
    };

    $scope.getPrnDesignList = function (val) {
        return productStockService.getBarcodePrnDesignList(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.onSelect = function (obj) {
        if (typeof ($scope.prnDesign.prnName) == "object") {
            $scope.setData();
            $scope.mode = "EDIT";
            $scope.prnDesign = obj;
            $scope.prnDesign.prnFieldJson = JSON.parse(obj.prnFieldJson);
            lblPRNFile.innerHTML = $scope.prnDesign.prnFileName;
        }
    };

    window.pressed = function () {
        var a = document.getElementById('selectBarcodePRNDesignFile');
        if (a.value == "") {
            lblPRNFile.innerHTML = "Please Browse PRN File";
        }
        else {
            var theSplit = a.value.split('\\');
            lblPRNFile.innerHTML = theSplit[theSplit.length - 1];
        }
    };

    $scope.browseFile = function () {
        var file = document.getElementById("selectBarcodePRNDesignFile").files[0];
        if (!file && $scope.mode == "NEW") {
            $("#selectBarcodePRNDesignFile").click();
        }
        else {
            $scope.setFocus('prnField0');
        }
    };

    $scope.setData();

});