﻿app.controller('barcodeGenerationCtrl', function ($scope, ModalService, $filter, productStockService, vendorPurchaseService, stockTransferService) {

    $scope.barcodeProfileList = [];
    $scope.prnDesignList = [];
    $scope.barcodeProfile = "";
    $scope.prnDesign = "";
    $scope.filterType = "1";
    $scope.invoiceNo = "";
    $scope.purchase = {
        page: { pageNo: 1, pageSize: 30 },
        select: "grNo",
        values: ""
    };
    $scope.transferNo = "";
    $scope.scanBarcodeOption = "";
    $scope.productList = [];

    $scope.clear = function () {
        $scope.filterType = "1";
        $scope.product = "";
        $scope.list = [];
        $scope.barcodeProfile = "";
        $scope.prnDesign = "";
        $scope.invoiceNo = "";
        $scope.purchase = {
            page: { pageNo: 1, pageSize: 30 },
            select: "grNo",
            values: ""
        };
        $scope.transferNo = "";
        $scope.setFocus("productName");
    }

    $scope.getProducts = function (val) {
        return productStockService.getSalesProduct(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.onSelect = function () {
        //$scope.list = [];
    };

    $scope.checkProduct = function () {
        if (typeof ($scope.product) == "object" && $scope.filterType == "1" || $scope.filterType == "2" && $scope.purchase.values != "" || $scope.filterType == "3" && $scope.invoiceNo != "" || $scope.filterType == "4" && $scope.transferNo != "" || $scope.list.length != 0) {
            $scope.setFocus("loadData");
        }
    };

    $scope.loadData = function () {
        if ($scope.list.length == 0) {
            if ($scope.filterType == "1" && $scope.product == "") {
                ShowConfirmMsgWindow("Please select [Product] and try again.", "productName");
                return;
            }
            else if ($scope.filterType == "2" && $scope.purchase.values == "") {
                ShowConfirmMsgWindow("Please enter [GRN No] and try again.", "grnNo");
                return;
            }
            else if ($scope.filterType == "3" && $scope.invoiceNo == "") {
                ShowConfirmMsgWindow("Please enter [Invoice No] and try again.", "invoiceNo");
                return;
            }
            else if ($scope.filterType == "4" && $scope.transferNo == "") {
                ShowConfirmMsgWindow("Please enter [Transfer No] and try again.", "transferNo");
                return;
            }
        }
        else if ($scope.filterType == "1" && $scope.product == "" || $scope.filterType == "2" && $scope.purchase.values == "" || $scope.filterType == "3" && $scope.invoiceNo == "" || $scope.filterType == "4" && $scope.transferNo == "") {
            $scope.setFocus("profileType");
            return;
        }

        if ($scope.list.length != 0) {
            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].productId == $scope.product.productId) {
                    ShowConfirmMsgWindow("Same product already added in the list. Please select different one and try again.", "productName");
                    return;
                }
            }
        }

        if ($scope.filterType == "1") {
            $.LoadingOverlay("show");
            productStockService.productBatch($scope.product.productId).then(function (response) {

                var templist = response.data;
                templist = $filter('orderBy')(templist, 'expireDate');
                for (var i = 0; i < templist.length; i++) {
                    var filter = $filter("filter")($scope.list, { id: templist[i].id }, true);
                    if (filter.length == 0 && templist[i].stock > 0) {
                        $scope.list.push(templist[i]);
                    }
                }

                $scope.setFocus("productName");
                $scope.product = "";

                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
            });
        }
        else if ($scope.filterType == "2") {
            $.LoadingOverlay("show");
            vendorPurchaseService.list($scope.purchase).then(function (response) {

                var data = $filter("filter")(response.data.list, { actualInvoice: $scope.purchase.values }, true);
                data = $filter('orderBy')(data, 'createdAt', true);
                if (data != null && data.length > 0) {
                    var templist = data[0].vendorPurchaseItem;
                    for (var i = 0; i < templist.length; i++) {
                        var filter = $filter("filter")($scope.list, { id: templist[i].productStock.id }, true);
                        if (filter.length == 0 && templist[i].productStock.stock > 0) {
                            $scope.list.push(templist[i].productStock);
                        }
                    }
                }

                $scope.setFocus("grnNo");
                $scope.purchase.values = "";

                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
            });
        }
        else if ($scope.filterType == "3") {
            $.LoadingOverlay("show");
            productStockService.getNewOpenedInventory("", $scope.invoiceNo).then(function (response) {

                var templist = response.data;
                for (var i = 0; i < templist.length; i++) {
                    var filter = $filter("filter")($scope.list, { id: templist[i].id }, true);
                    if (filter.length == 0 && templist[i].stock > 0) {
                        $scope.list.push(templist[i]);
                    }
                }

                $scope.setFocus("invoiceNo");
                $scope.invoiceNo = "";

                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
            });
        }
        else if ($scope.filterType == "4") {
            $.LoadingOverlay("show");
            stockTransferService.getAcceptedTransferList($scope.transferNo).then(function (response) {

                var templist = response.data;
                for (var i = 0; i < templist.length; i++) {
                    var filter = $filter("filter")($scope.list, { id: templist[i].id }, true);
                    if (filter.length == 0 && templist[i].stock > 0) {
                        $scope.list.push(templist[i]);
                    }
                }

                $scope.setFocus("transferNo");
                $scope.transferNo = "";

                $.LoadingOverlay("hide");
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
            });
        }
    };

    $scope.setFocus = function (tag) {
        var ele = document.getElementById(tag);
        if (ele != null) {
            ele.focus();
        }
    };
    
    $scope.getProfileList = function () {
        productStockService.getBarcodeProfileList("").then(function (response) {
            $scope.barcodeProfileList = response.data;
            var defaultProfile = $filter("filter")($scope.barcodeProfileList, { isDefaultProfile: true }, true);
            if (defaultProfile.length > 0) {
                $scope.barcodeProfile = defaultProfile[0].id;
            }
            if ($scope.barcodeProfileList.length == 0) {
                var model = '{"id":null,"profileName":"Default Profile","profileJson":[{"seqId":1,"field":"Product Code","setYesNo":"Y","order":"1","maxLength":"5","tag":"productCode"},{"seqId":2,"field":"Batch No [Start From Last]","setYesNo":"Y","order":"2","maxLength":"4","tag":"batchNo"},{"seqId":3,"field":"Expiry Date [MMyy]","setYesNo":"Y","order":"3","maxLength":"4","tag":"expireDate"},{"seqId":4,"field":"Transaction Date [ddMMyy]","setYesNo":"N","order":"","maxLength":"","tag":"transactionDate"},{"seqId":5,"field":"(Month/Year)Transaction Date [MMyy]","setYesNo":"N","order":"","maxLength":"","tag":"monthYearTransactionDate"}],"isDefaultProfile":true}';
                model = JSON.parse(model);
                model.profileJson = JSON.stringify(model.profileJson);
                $.LoadingOverlay("show");
                productStockService.updateBarcodeProfile(model).then(function (response) {
                    $scope.getProfileList();
                    $.LoadingOverlay("hide");
                }, function (error) {
                    console.log(error);
                    $.LoadingOverlay("hide");
                    ShowConfirmMsgWindow('Something went wrong, please refresh page again.', "", true, "popup-width-500");
                });
            }
        });
    };

    $scope.generateBarcode = function (regenerate) {
        if ($scope.list.length == 0) {
            ShowConfirmMsgWindow("Please load data then try again.", focusOn());
            return;
        }
        if ($scope.barcodeProfile == null || $scope.barcodeProfile == "") {
            ShowConfirmMsgWindow("Please select [Barcode Profile] and try again.", "profileType");
            return;
        }

        checkProductCodes();
        if ($scope.productList.length > 0) {
            getProductCodes(regenerate);
            return;
        }

        var isGenerateBarcode = false;
        if (!regenerate) {
            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].purchaseBarcode == null || $scope.list[i].purchaseBarcode == "") {
                    isGenerateBarcode = true;
                }
            }
        }
        else {
            isGenerateBarcode = true;
        }
        if (isGenerateBarcode) {
            var data = {
                msgTitle: "",
                msg: (regenerate ? "You have to re-stick barcode stickers if you already print barcode stickers. Are you sure to re-generate barcode?" : "Barcode will be generated for empty barcode rows. Are you sure to continue?"),
                showOk: false,
                showYes: true,
                showNo: true,
                showCancel: false,
                focusOnNo: false
            };
            var m = ModalService.showModal({
                "controller": "showConfirmMsgCtrl",
                "templateUrl": 'showConfirmMsgModal',
                "inputs": { "params": [{ "data": data }] },
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result == "Yes") {
                        $.LoadingOverlay("show");
                        if (regenerate) {
                            for (var i = 0; i < $scope.list.length; i++) {
                                $scope.list[i].purchaseBarcode = "";
                            }
                        }
                        productStockService.generateBarcode($scope.list, $scope.barcodeProfile).then(function (response) {
                            $scope.list = response.data;
                            $scope.setFocus("quantity0");

                            $.LoadingOverlay("hide");
                        }, function (error) {
                            console.log(error);
                            $.LoadingOverlay("hide");
                            ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
                        });
                    }
                });
            });
        }
        else {
            ShowConfirmMsgWindow("Barcode already generated for loaded data. Please proceed to print barcode stickers.", "print");
            return;
        }
    };

    function checkProductCodes() {
        $scope.productList = [];
        for (var i = 0; i < $scope.list.length; i++) {
            if ($scope.list[i].product.code == null || $scope.list[i].product.code == "") {
                var obj = {
                    id: null,
                    code: null
                };
                if ($scope.list[i].productId != null && $scope.list[i].productId != "") {
                    obj.id = $scope.list[i].productId;
                }
                else if ($scope.list[i].product.id != null && $scope.list[i].product.id != "") {
                    obj.id = $scope.list[i].product.id;
                }
                $scope.productList.push(obj);
            }
        }        
    };

    function getProductCodes(regenerate) {
        if ($scope.productList.length > 0) {
            if (navigator.onLine) {
                $.LoadingOverlay("show");
                productStockService.getProductCodes($scope.productList).then(function (response) {
                    $scope.productList = response.data;

                    for (var j = 0; j < $scope.productList.length; j++) {
                        for (var i = 0; i < $scope.list.length; i++) {
                            if ($scope.list[i].product.id == $scope.productList[j].id || $scope.list[i].productId == $scope.productList[j].id) {
                                $scope.list[i].product.code = $scope.productList[j].code;
                            }
                        }
                    }

                    $.LoadingOverlay("hide");
                    checkProductCodes();
                    if ($scope.productList.length > 0) {
                        ShowConfirmMsgWindow("Some of new products is not synced with online. Please sync data to online and try again.");
                        return;
                    }
                    else {
                        $scope.generateBarcode(regenerate);
                    }

                }, function (error) {
                    console.log(error);
                    $.LoadingOverlay("hide");
                    ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
                });
            }
            else {
                ShowConfirmMsgWindow("Product code not available for some products. Please connect internet to get it from online and try again.");
                return false;
            }
        }
    };

    $scope.removeItem = function (idx) {
        $scope.list.splice(idx, 1);
    };

    $scope.getPrnDesignList = function () {
        productStockService.getBarcodePrnDesignList("").then(function (response) {
            $scope.prnDesignList = response.data;
        });
    };

    function focusOn() {        
        if ($scope.filterType == "1") {
            return "productName";
        }
        else if ($scope.filterType == "2") {
            return "grnNo";
        }
        else if ($scope.filterType == "3") {
            return "invoiceNo";
        }
        else if ($scope.filterType == "4") {
            return "transferNo";
        }
    };

    $scope.printStickers = function () {
        if ($scope.list.length == 0) {
            ShowConfirmMsgWindow("Please load data then try again.", focusOn());
            return;
        }
        if ($scope.prnDesign == null || $scope.prnDesign == "") {
            ShowConfirmMsgWindow("Please select [Barcode PRN Design] and try again.", "prnDesign");
            return;
        }

        var prnData = $filter("filter")($scope.prnDesignList, { id: $scope.prnDesign }, true)[0];
        var prnFields = JSON.parse(prnData.prnFieldJson);
        var fileData = "";

        for (var i = 0; i < $scope.list.length; i++) {
            if ($scope.list[i].purchaseBarcode == null || $scope.list[i].purchaseBarcode == "") {
                ShowConfirmMsgWindow("Please generate barcode for empty rows and try again.", "quantity" + i);
                return;
            }
            if ($scope.list[i].requiredQty == null || $scope.list[i].requiredQty == "" || $scope.list[i].requiredQty == 0) {
                ShowConfirmMsgWindow("Please enter no. of sticker rows to be printed and try again.", "quantity" + i);
                return;
            }

            var data = prnData.prnFileData;
            for (var j = 0; j < prnFields.length; j++) {
                if (prnFields[j].prnField != null && prnFields[j].prnField != "") {
                    if (prnFields[j].tag == "productCode") {
                        data = data.replace(new RegExp(prnFields[j].prnField, 'g'), $scope.list[i].product.code);
                    }
                    else if (prnFields[j].tag == "productName") {
                        data = data.replace(new RegExp(prnFields[j].prnField, 'g'), $scope.list[i].product.name);
                    }
                    else if (prnFields[j].tag == "batchNo") {
                        data = data.replace(new RegExp(prnFields[j].prnField, 'g'), $scope.list[i].batchNo);
                    }
                    else if (prnFields[j].tag == "expireDate") {
                        data = data.replace(new RegExp(prnFields[j].prnField, 'g'), $filter('date')($scope.list[i].expireDate, 'MM/yyyy'));
                    }
                    else if (prnFields[j].tag == "rate") {
                        data = data.replace(new RegExp(prnFields[j].prnField, 'g'), $scope.list[i].sellingPrice);
                    }
                    else if (prnFields[j].tag == "mrp") {
                        data = data.replace(new RegExp(prnFields[j].prnField, 'g'), $scope.list[i].mrp);
                    }
                    else if (prnFields[j].tag == "barcodeValue") {
                        data = data.replace(new RegExp(prnFields[j].prnField, 'g'), $scope.list[i].purchaseBarcode);
                    }
                    else if (prnFields[j].tag == "noOfStickersQty") {
                        data = data.replace(new RegExp(prnFields[j].prnField, 'g'), $scope.list[i].requiredQty);
                    }
                }
            }
            fileData = fileData + data + "\n";
        }

        var data = {
            msgTitle: "",
            msg: "Are you sure to print barcode stickers?",
            showOk: false,
            showYes: true,
            showNo: true,
            showCancel: false,
            focusOnNo: false
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result == "Yes") {
                    var a = $('<a/>', {
                        style: 'display:none',
                        href: 'data:application/octet-stream;base64,' + btoa(fileData),
                        download: "hq_barcode_stickers.prn"
                    }).appendTo('body')
                    a[0].click()
                    a.remove();
                    if (!($scope.scanBarcodeOption.scanBarcode === "1")) {
                        ShowConfirmMsgWindow("Please enable [Enable Barcode & UPC/EAN Code] option in sales settings. Otherwise barcode scanning won't work.", "", true, "", true);
                    }
                    else {
                        $scope.clear();
                    }
                }
            });
        });

    };

    function ShowConfirmMsgWindow(msg, focusTag, showExclamation, width, isReset) {
        var data = {
            msgTitle: "",
            msg: msg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: "",
            showExclamation: showExclamation,
            popupWidth: width
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.setFocus(focusTag);
                if (isReset) {
                    $scope.clear();
                }
            });
        });
    };

    $scope.reGenerateBarcode = function () {
        $scope.generateBarcode(true);
    };
            
    function getScanBarcodeOption() {
        vendorPurchaseService.getScanBarcodeOption().then(function (resp) {
            $scope.scanBarcodeOption = resp.data;
        }, function (error) {
            toastr.error('Error Occured', 'Error');
            console.log(error);
        });
    };

    function loadPurchaseBarcode() {
        var data = window.localStorage.getItem("loadPurchaseBarcode");
        if (data != null) {
            window.localStorage.removeItem("loadPurchaseBarcode");
            $scope.purchase.values = data;
            $scope.filterType = "2";
            $scope.loadData();
        }
    };

    $scope.validateQty = function (item) {
        if (item.requiredQty > item.stock) {
            item.requiredQty = "";
        }
    };

    $scope.clear();
    $scope.getProfileList();
    $scope.getPrnDesignList();
    getScanBarcodeOption();
    loadPurchaseBarcode();    

});