﻿app.controller('barcodeProfileCtrl', function ($scope, ModalService, $filter, productStockService) {

    $scope.totalMaxLength = 15;
    $scope.setData = function () {
        $scope.mode = "NEW";
        $scope.profile = {
            id: null,
            profileName: "",
            profileJson: [
                {
                    seqId: 1,
                    field: "Product Code",
                    setYesNo: "N",
                    order: "",
                    maxLength: "",
                    tag: "productCode"
                },
                {
                    seqId: 2,
                    field: "Batch No [Start From Last]",
                    setYesNo: "N",
                    order: "",
                    maxLength: "",
                    tag: "batchNo"
                },
                {
                    seqId: 3,
                    field: "Expiry Date [MMyy]",
                    setYesNo: "N",
                    order: "",
                    maxLength: "",
                    tag: "expireDate"
                },
                {
                    seqId: 4,
                    field: "Transaction Date [ddMMyy]",
                    setYesNo: "N",
                    order: "",
                    maxLength: "",
                    tag: "transactionDate"
                },
                {
                    seqId: 5,
                    field: "(Month/Year)Transaction Date [MMyy]",
                    setYesNo: "N",
                    order: "",
                    maxLength: "",
                    tag: "monthYearTransactionDate"
                }
            ],
        };
        $scope.setFocus("barcodeProfileName");
    };

    $scope.setYesNo = function (data) {
        if (data.setYesNo != "N" && data.setYesNo != "Y" && data.setYesNo != "n" && data.setYesNo != "y") {
            data.setYesNo = "";
        }
        if (data.setYesNo == "y") {
            data.setYesNo = "Y";
        }
        else if (data.setYesNo == "n") {
            data.setYesNo = "N";
        }
    };

    $scope.setOrder = function (data) {
        if (data.order > $scope.profile.profileJson.length || data.order <= 0) {
            data.order = "";
        }
        for (var i = 0; i < $scope.profile.profileJson.length; i++) {
            if (data != $scope.profile.profileJson[i]) {
                if (data.order == $scope.profile.profileJson[i].order) {
                    $scope.profile.profileJson[i].order = "";
                }
            }
        };
    };

    $scope.setMaxLength = function (data) {
        if (data.maxLength > 10 || data.maxLength <= 0) {
            data.maxLength = "";
        }
        var maxLen = 0;
        angular.forEach($scope.profile.profileJson, function (value, key) {
            maxLen = maxLen + Number(value.maxLength) || 0;
        });
        if (maxLen > $scope.totalMaxLength) {
            data.maxLength = "";
        }
    };

    $scope.saveProfile = function () {
        if (document.getElementById("barcodeProfileName").value == "") {
            ShowConfirmMsgWindow("Please enter [Barcode Profile Name] and try again.", "barcodeProfileName");
            return;
        }
        var isSettingsEnabled = false;
        for (var i = 0; i < $scope.profile.profileJson.length; i++) {
            if ($scope.profile.profileJson[i].setYesNo == "Y" && $scope.profile.profileJson[i].order == "") {
                ShowConfirmMsgWindow("Please fill barcode [Order] and try again.", "order" + i);
                return;
            }
            else if ($scope.profile.profileJson[i].setYesNo == "Y" && $scope.profile.profileJson[i].order != "" && $scope.profile.profileJson[i].maxLength == "") {
                ShowConfirmMsgWindow("Please fill barcode [Max Length] and try again.", "maxLength" + i);
                return;
            }
            else if ($scope.profile.profileJson[i].setYesNo == "") {
                $scope.profile.profileJson[i].setYesNo = "N";
            }
            else if ($scope.profile.profileJson[i].setYesNo == "Y") {
                isSettingsEnabled = true;
            }

            if ($scope.profile.profileJson[i].setYesNo == "N") {
                $scope.profile.profileJson[i].order = "";
                $scope.profile.profileJson[i].maxLength = "";
            }
        }
        if (!isSettingsEnabled) {
            ShowConfirmMsgWindow("Please enable atleast one barcode settings and try again.", "setYesNo0");
            return;
        }

        var list = $filter("filter")($scope.profile.profileJson, { setYesNo: "Y" }, true);
        list = $filter("orderBy")(list, 'order');
        for (var i = 0; i < list.length; i++) {
            if (i + 1 != list[i].order) {
                var index = $scope.profile.profileJson.indexOf(list[i]);
                ShowConfirmMsgWindow("Please enter [Order] should be sequentially.", "order" + index);
                return;
            }
        }

        $.LoadingOverlay("show");
        productStockService.getBarcodeProfileList("").then(function (response) {
            $.LoadingOverlay("hide");

            $scope.barcodeProfileList = response.data;
            var profileExist = $filter("filter")($scope.barcodeProfileList, { profileJson: JSON.stringify($scope.profile.profileJson) }, true);
            if (profileExist.length != 0 && ($scope.mode == "NEW" || profileExist[0].id != $scope.profile.id && $scope.mode == "EDIT")) {
                ShowConfirmMsgWindow("Same barcode profile already exists with [" + profileExist[0].profileName + "].");
                return;
            }
            var nameExist = $filter("filter")($scope.barcodeProfileList, { profileName: $scope.profile.profileName }, true);
            if (nameExist.length != 0 && ($scope.mode == "NEW" || nameExist[0].id != $scope.profile.id && $scope.mode == "EDIT")) {
                ShowConfirmMsgWindow("Barcode Profile Name already exists. Please provide different one.", "barcodeProfileName");
                return;
            }

            if ($scope.profile.isDefaultProfile) {
                var defaultProfileExist = $filter("filter")($scope.barcodeProfileList, { isDefaultProfile: true }, true);
                if (defaultProfileExist.length != 0 && ($scope.mode == "NEW" || defaultProfileExist[0].id != $scope.profile.id && $scope.mode == "EDIT")) {
                    ShowConfirmMsgWindow("Default Profile already set with [" + defaultProfileExist[0].profileName + "]. Please remove that one and set this then try again.", "defaultProfile");
                    return;
                }
            }

            var model = JSON.parse(JSON.stringify($scope.profile));
            model.profileJson = JSON.stringify(model.profileJson);

            var data = {
                msgTitle: "",
                msg: "Are you sure to " + ($scope.mode == "NEW" ? "save" : "update") + " [" + $scope.profile.profileName + "]?",
                showOk: false,
                showYes: true,
                showNo: true,
                showCancel: false,
                focusOnNo: false
            };
            var m = ModalService.showModal({
                "controller": "showConfirmMsgCtrl",
                "templateUrl": 'showConfirmMsgModal',
                "inputs": { "params": [{ "data": data }] },
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    if (result == "Yes") {
                        $.LoadingOverlay("show");
                        productStockService.updateBarcodeProfile(model).then(function (response) {
                            $.LoadingOverlay("hide");
                            ShowConfirmMsgWindow('Barcode profile [' + $scope.profile.profileName + '] has been ' + ($scope.mode == "NEW" ? 'created' : 'updated') + ' successfully.', "barcodeProfileName", false, "", true);
                        }, function (error) {
                            console.log(error);
                            $.LoadingOverlay("hide");
                            ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
                        });
                    };
                });
            });

        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            ShowConfirmMsgWindow('Something went wrong, please try again', "", true, "popup-width-500");
        });
    };

    $scope.setFocus = function (tag) {
        var ele = document.getElementById(tag);
        if (ele != null) {
            ele.focus();
        }
    };

    function ShowConfirmMsgWindow(msg, focusTag, showExclamation, width, isReset) {
        var data = {
            msgTitle: "",
            msg: msg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: "",
            showExclamation: showExclamation,
            popupWidth: width
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.setFocus(focusTag);
                if (isReset) {
                    $scope.setData();
                }
            });
        });
    };

    $scope.getProfileList = function (val) {
        return productStockService.getBarcodeProfileList(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };

    $scope.getInsertDefaultProfile = function () {
        productStockService.getBarcodeProfileList("").then(function (response) {
            $scope.barcodeProfileList = response.data;
            if ($scope.barcodeProfileList.length == 0) {
                var model = '{"id":null,"profileName":"Default Profile","profileJson":[{"seqId":1,"field":"Product Code","setYesNo":"Y","order":"1","maxLength":"5","tag":"productCode"},{"seqId":2,"field":"Batch No [Start From Last]","setYesNo":"Y","order":"2","maxLength":"4","tag":"batchNo"},{"seqId":3,"field":"Expiry Date [MMyy]","setYesNo":"Y","order":"3","maxLength":"4","tag":"expireDate"},{"seqId":4,"field":"Transaction Date [ddMMyy]","setYesNo":"N","order":"","maxLength":"","tag":"transactionDate"},{"seqId":5,"field":"(Month/Year)Transaction Date [MMyy]","setYesNo":"N","order":"","maxLength":"","tag":"monthYearTransactionDate"}],"isDefaultProfile":true}';
                model = JSON.parse(model);
                model.profileJson = JSON.stringify(model.profileJson);
                $.LoadingOverlay("show");
                productStockService.updateBarcodeProfile(model).then(function (response) {
                    $.LoadingOverlay("hide");
                }, function (error) {
                    console.log(error);
                    $.LoadingOverlay("hide");
                    ShowConfirmMsgWindow('Something went wrong, please refresh page again.', "", true, "popup-width-500");
                });
            }
        });
    };

    $scope.onSelect = function (obj) {
        if (typeof ($scope.profile.profileName) == "object") {
            $scope.setData();
            $scope.mode = "EDIT";
            $scope.profile = obj;
            $scope.profile.profileJson = JSON.parse(obj.profileJson);
        }
    };

    $scope.setData();
    $scope.getInsertDefaultProfile();

});