﻿app.controller('drugListCtrl', function ($scope, toastr, productService, productModel, pagerServcie,$filter) {

    var sortingOrder = 'name';
    var product = productModel;

    $scope.search = product;

    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    $scope.kindNames = [
            { value: 'OTC', label: 'OTC' },
           { value: 'Prescription', label: 'Prescription' },
           { value: 'FMCG', label: 'FMCG' },
           { value: 'F&B', label: 'F&B' }
    ];
    function pageSearch() {
        productService.listProduct($scope.search).then(function (response) {
            $scope.list = response.data.list;
            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].status == 1) {
                    $scope.list[i].status = 'Active';
                }
                else {
                    $scope.list[i].status = 'InActive';
                }
            }
            $scope.selectedIndex = -1;
        }, function () { });
    }

    $scope.getData=function()
    {
        productService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    }
    $scope.productSearch = function (productName, obj) {
         $.LoadingOverlay("show");
        if (productName != undefined && productName != null) {
            $scope.search.name = productName;
        }
        else if ($scope.selectedProduct != null) {
            $scope.search.name = $scope.selectedProduct.title;
        }
        else {
            $scope.search.name = null;
        }

        $scope.search.page.pageNo = 1;
        productService.listProduct($scope.search).then(function (response) {
            //$scope.list = response.data.list;
            $scope.list = response.data;
            //$scope.exportList = $scope.list.name;
           
            for (var i = 0; i < $scope.list.length; i++)
            {
                if ($scope.list[i].status == 1)
                {
                    $scope.list[i].status = 'Active';
                }
                else
                {
                    $scope.list[i].status = 'InActive';
                }
                
            }
            $scope.searchProduct();
           //pagerServcie.init(response.data.noOfRows, pageSearch);
           $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }



    $scope.productSearch();

    function showDetails(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $scope.editProducts(dataItem);
    }


    $scope.editProducts = function (item, index) {
        //$scope.showPanel = true;
        //for (var i = 0; i < $scope.list.length; i++) {
        //    if (i == index) {
        //        $scope.indexResult = true;
        //    }
        //    //else {
        //    //    $scope.indexResult = false;
        //    //}
        //}
        $scope.selectedIndex;
    
       $scope.productsItem = JSON.parse(JSON.stringify(item));
         //if (!$scope.productsItem.status)
         //    $scope.productsItem.status = 2
         if ($scope.productsItem.status == 'Active')
             $scope.productsItem.status = 1
          else
             $scope.productsItem.status = 2
         
         $scope.categories = [
            { CategoryID: 1, Name: 'Active' },
           { CategoryID: 2, Name: 'Inactive' },
         ];       

         //$scope.productsItem.kindName = 'OTC';
          $scope.kindNames = [
            { value: 'OTC', label: 'OTC' },
           { value: 'Prescription', label: 'Prescription' },
           { value: 'FMCG', label: 'FMCG' },
           { value: 'F&B', label: 'F&B' }
          ];
          
          $scope.scheduleList = [
             { schedule: 'H', label: 'H' },
            { schedule: 'H1', label: 'H1' },
            { schedule: 'NON-SCHEDULED', label: 'NON-SCHEDULED' },
            { schedule: 'X', label: 'X' }
          ];

          $scope.catogoryList = [
              { category: 'LIQUID', label: 'LIQUID' },
             { category: 'POWDER', label: 'POWDER' },
             { category: 'GENERAL', label: 'GENERAL' },
             { category: 'INJECTION', label: 'INJECTION' },
              { category: 'INHALER', label: 'INHALER' },
              { category: 'OINTMENTS', label: 'OINTMENTS' },
              { category: 'DROPS', label: 'DROPS' },
              { category: 'SYRUP', label: 'SYRUP' },
              { category: 'SURGICAL', label: 'SURGICAL' },
              { category: 'FMCG', label: 'FMCG' },
              { category: 'TABLET & CAPSULE', label: 'TABLET & CAPSULE' },
              { category: 'REFREGERATOR', label: 'REFREGERATOR' },

          ];
         $scope.categoryField = $scope.categories[$scope.productsItem.status - 1];
         $scope.productsItem.status = $scope.categoryField.CategoryID;

        //$scope.productsItem.kind = $scope.kindName;
        //var productName = document.getElementById("productName");
        //productName.focus();
       
    }

    $scope.updateProduct = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        
        if ($scope.productsItem.genericName !=null && $scope.productsItem.genericName.genericName !== undefined)
            $scope.productsItem.genericName = $scope.productsItem.genericName.genericName;
             
        if ($scope.productsItem.manufacturer != null && $scope.productsItem.manufacturer.manufacturer !== undefined)
            $scope.productsItem.manufacturer = $scope.productsItem.manufacturer.manufacturer;

        if ($scope.productsItem.schedule != null && $scope.productsItem.schedule.schedule !== undefined)
            $scope.productsItem.schedule = $scope.productsItem.schedule.schedule;

        if ($scope.productsItem.type != null && $scope.productsItem.type.type !== undefined)
            $scope.productsItem.type = $scope.productsItem.type.type;

        if ($scope.productsItem.category != null && $scope.productsItem.category.category !== undefined)
            $scope.productsItem.category = $scope.productsItem.category.category;

        if ($scope.productsItem.commodityCode != null && $scope.productsItem.commodityCode.commodityCode !== undefined)
            $scope.productsItem.commodityCode = $scope.productsItem.commodityCode.commodityCode;

        if ($scope.productsItem.kindName != null && $scope.productsItem.kindName.value !== undefined)
            $scope.productsItem.kindName = $scope.productsItem.kindName.value;
        
           productService.update($scope.productsItem).then(function (response) {
            $scope.productSearch();
            toastr.success('Product updated successfully');
            $scope.selectedIndex = -1;
            //$scope.productForm.$setPristine();
            //$scope.showPanel = false;
            $scope.productsItem = {};
            $scope.isProcessing = false;
           $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Product already exist', 'Error');
            $scope.isProcessing = false;
        });
    }

    $scope.cancel = function () {
        //$scope.productForm.$setPristine();
        //$scope.productsItem = {};
        //$scope.showPanel = false;
        $scope.selectedIndex = -1;       
    }

    $scope.keyEnter = function (event, e) {
        var ele = document.getElementById(e);
        if (event.which === 13) // Enter key
        {
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
            {
                ele.focus();
                if (ele.nodeName != "BUTTON")
                    ele.select();
            }
        }
    }

 

    $scope.getGeneric = function (val) {
        return productService.genericFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getManufacturer = function (val) {

        return productService.manufacturerFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getSchedule = function (val) {

        return productService.scheduleFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getType = function (val) {

        return productService.typeFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getCategory = function (val) {

        return productService.categoryFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getCommodity = function (val) {

        return productService.commodityFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }


    $scope.getKindName = function (val) {

        return productService.kindNameFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }
   
    function exportList() {
         productService.exportList($scope.search).then(function (response) {
            $scope.exportList = response.data.list;

        }, function () { });
    }

    $scope.sortingOrder = sortingOrder;
    $scope.reverse = false;
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 50;
    $scope.pagedItems = [];
    $scope.currentPage = 0;  

    var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    // init the filtered items
    $scope.searchProduct = function () {
        $scope.filteredItems = $filter('filter')($scope.list, function (item) {
            for (var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        // take care of the sorting order
        if ($scope.sortingOrder !== '') {
            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
        }
        $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    };

    // calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];

        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
    };

    $scope.range = function (start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
            $scope.currentPage++;
        }
    };

    $scope.setPage = function () {
        $scope.currentPage = this.n;
    };

    // functions have been describe process the data for display
   

    // change sorting order
    $scope.sort_by = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

        // icon setup
        $('th i').each(function () {
            // icon reset
            $(this).removeClass().addClass('icon-sort');
        });
        if ($scope.reverse)
            $('th.' + new_sorting_order + ' i').removeClass().addClass('icon-chevron-up');
        else
            $('th.' + new_sorting_order + ' i').removeClass().addClass('icon-chevron-down');
    };

});

app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});
