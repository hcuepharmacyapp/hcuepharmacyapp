﻿app.factory('productService', function ($http) {
    return {
        drugData: function () {
            return $http.get('/product/ProductList');
        },
        create: function (data) {
            return $http.post('/product/Create', data);
        },
        update: function (data) {
            return $http.post('/product/UpdateProduct', data);
        },
        updateList: function (data) {
            return $http.post('/product/listUpdate', data);
        },
        list: function (searchData) {
            return $http.post('/product/listData', searchData);
        },
        Masterlist: function (searchData) {
            return $http.post('/product/MasterlistData', searchData);
        },
        //alternate product search
        "getGenericDetails": function (item) {
            item = item.replace(/&/g, '%26');
            //item = item.replace(/\s+/g, '%2B');
            item = item.replace(/=/g, '%3D');
            return $http.get('/product/getGenericDetails?genericName=' + item);
        },
        //alternate product search
        EntireMasterlist: function (searchData) {
            return $http.post('/product/EntireMasterlistData', searchData);
        },
        listProduct: function (searchData) {
            return $http.post('/product/ListProduct', searchData);
        },
        drugFilterData: function (filter) {
            return $http.get('/product/ProductList?productName=' + escape(filter));
        },
        getProductszero: function (filter) {
            return $http.get('/ProductStockData/InStockProductList?productName=' + escape(filter));
        },
        accountWiseFilterData: function (filter) {
            return $http.get('/product/AccountWiseProductList?productName=' + escape(filter));
        },
        nonHiddenProductList: function (filter) {
            return $http.get('/product/NonHiddenProductList?productName=' + escape(filter));
        },
        drugFilterLocalData: function (filter) {
            return $http.get('/product/LocalProductList?productName=' + escape(filter));
        },
        getProducts: function (filter) {
            return $http.get('/product/getProducts?productName=' + escape(filter));
        },
        InstancedrugFilter: function (filter) {
            return $http.get('/product/InstanceProduct?productName=' + escape(filter));
        },
        InstanceWisedrugFilter: function (instanceid, filter) {
            return $http.get('/product/InstanceProductList?instanceid=' + instanceid + '&productName=' + escape(filter));
        },
        ReturndrugFilter: function (filter) {
            return $http.get('/product/ReturnProduct?productName=' + escape(filter));
        },
        SalesProductdrugFilter: function (filter) {
            return $http.get('/product/SalesProduct?productName=' + escape(filter));
        },
        //newly added by nandhini 13.09.17
        SalesReturnProductdrugFilter: function (filter) {
            return $http.get('/product/SalesReturnProduct?productName=' + escape(filter));
        },
        productFilterData: function (filter) {
            return $http.get('/product/productNameList?productName=' + escape(filter));
        },
        //newly added by nandhini
        genericFilterData: function (filter) {
            return $http.get('/product/GenericList?genericName=' + filter);
        },
        //newly added by nandhini
        manufacturerFilterData: function (filter) {
            return $http.get('/product/ManufacturerList?manufacturercName=' + filter);
        },
        scheduleFilterData: function (filter) {
            return $http.get('/product/ScheduledCategoryList?scheduledCatName=' + filter);
        },
        typeFilterData: function (filter) {
            return $http.get('/product/TypeList?typeName=' + filter);
        },
        categoryFilterData: function (filter) {
            return $http.get('/product/CategoryList?categoryName=' + filter);
        },
        subcategoryFilterData: function (filter) {
            return $http.get('/product/SubCategoryList?categoryName=' + filter);
        },
        commodityFilterData: function (filter) {
            return $http.get('/product/CommodityCodeList?commodityName=' + filter);
        },
        kindNameFilterData: function (filter) {
            return $http.get('/product/KindNameList?kindName=' + filter);
        },

        categoryList: function () {
            return $http.get('/product/CategoryList1');
        },

        exportList: function (searchData) {
            return $http.post('/product/Export', searchData);
        },
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        saveAlternateName: function (model) {
            return $http.post('/importBuy/saveAlternateName', model);
        },
        updatePackageSize: function (alternateVendorProduct) {
            return $http.post('/importBuy/updatePackageSize', alternateVendorProduct);
        },
        // Newly Added Gavaskar 22-02-2017 Start
        drugFilterDataInventoryUpdate: function (filter) {
            return $http.get('/product/InventoryUpdateProductList?productName=' + escape(filter));
        },
        // Newly Added Gavaskar 22-02-2017 End

        // Added by Violet Raj 16-03-2017
        stockdrugFilterData: function (filter) {
            return $http.get('/product/StockProductList?productName=' + escape(filter));
        },
        // Added by Violet Raj 16-03-2017
        getReturnProducts: function (filter) {
            return $http.get('/product/getReturnProducts?productName=' + escape(filter));
        },
        reorderMinMaxBulkUpdate: function (fromDate, toDate, minReorderFactor, maxReorderFactor) {
            return $http.post('/product/ReorderMinMaxBulkUpdate?fromDate=' + fromDate + '&toDate=' + toDate + '&minReorderFactor=' + minReorderFactor + '&maxReorderFactor=' + maxReorderFactor);
        },
        bulkUpdate: function (data) {
            return $http.post('/product/bulkUpdate', data);
        },
        getTaxValues: function () {
            return $http.get('/vendorPurchase/getTaxValues');
        },
    }
});
