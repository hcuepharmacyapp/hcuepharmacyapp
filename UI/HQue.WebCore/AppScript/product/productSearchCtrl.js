﻿app.controller('productSearchCtrl', function ($scope, productService, productModel, pagerServcie) {

    var product = productModel;

    $scope.search = product;

    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination

    function pageSearch() {
        productService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    }

    $scope.productSearch = function () {
        $scope.search.page.pageNo = 1;
        productService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
            pagerServcie.init(response.data.noOfRows, pageSearch);
        }, function () { });
    }

    $scope.productSearch();

});
