﻿app.factory('productMasterService', function ($http) {
    return {
        drugData: function () {
            return $http.get('/product/ProductList');
        },
        create: function (data) {
            return $http.post('/product/Create', data);
        },
        update: function (data) {
            return $http.post('/product/UpdateHcueProductMaster', data);
        },
        updateList: function (data) {
            return $http.post('/product/listUpdate', data);
        },
        list: function (searchData) {
            return $http.post('/product/listData', searchData);
        },
        Masterlist: function (searchData) {
            return $http.post('/product/MasterlistData', searchData);
        },
        //alternate product search
        "getGenericDetails": function (item) {
            return $http.get('/product/getGenericDetails?genericName=' + item);
        },
        //alternate product search
        EntireMasterlist: function (searchData) {
            return $http.post('/product/EntireMasterlistData', searchData);
        },
        listProduct: function (searchData) {
            return $http.post('/product/HcueProductMasterList', searchData);
        },
        drugFilterData: function (filter) {
            return $http.get('/product/ProductList?productName='+filter);
        },
	drugFilterLocalData: function (filter) {
            return $http.get('/product/LocalProductList?productName=' + filter);
        },
        getProducts: function (filter) {
            return $http.get('/product/getProducts?productName=' + filter);
        },
        InstancedrugFilter: function (filter) {
            return $http.get('/product/InstanceProduct?productName=' + filter);
        },
        //InstancedrugFilter: function (instanceid, filter) {
        //    return $http.get('/product/InstanceProductList?instanceid=' + instanceid + '&productName=' + filter);
        //},
        ReturndrugFilter: function (filter) {
            return $http.get('/product/ReturnProduct?productName=' + filter);
        },
        SalesProductdrugFilter: function (filter) {
            return $http.get('/product/SalesProduct?productName=' + filter);
        },
        productFilterData: function (filter) {
            return $http.get('/product/productNameList?productName=' + filter);
        },
        //newly added by nandhini
        genericFilterData: function (filter) {
            return $http.get('/product/GetProductMasterGeneric?genericName=' + filter);
        },
        //newly added by nandhini
        manufacturerFilterData: function (filter) {
            return $http.get('/product/ManufacturerList?manufacturercName=' + filter);
        },
        scheduleFilterData: function (filter) {
            return $http.get('/product/ScheduledCategoryList?scheduledCatName=' + filter);
        },
        typeFilterData: function (filter) {
            return $http.get('/product/TypeList?typeName=' + filter);
        },
        categoryFilterData: function (filter) {
            return $http.get('/product/CategoryList?categoryName=' + filter);
        },
        commodityFilterData: function (filter) {
            return $http.get('/product/CommodityCodeList?commodityName=' + filter);
        },
        kindNameFilterData: function (filter) {
            return $http.get('/product/KindNameList?kindName=' + filter);
        },

        categoryList: function () {
            return $http.get('/product/CategoryList1');
        },
        exportList: function (searchData) {
            return $http.post('/product/Export', searchData);
        },
        getInstanceData: function () {
            return $http.post('/salesReport/getInstanceData');
        },
        saveAlternateName: function (model) {
            return $http.post('/importBuy/saveAlternateName', model);
        },
        // Newly Added Gavaskar 22-02-2017 Start
        drugFilterDataInventoryUpdate: function (filter) {
            return $http.get('/product/InventoryUpdateProductList?productName=' + filter);
        },
        // Newly Added Gavaskar 22-02-2017 End

        // Added by Violet Raj 16-03-2017
        productMasterUpdateCount: function (filter) {
            return $http.get('/product/ProductMasterUpdateCount');
        }
    }
});
