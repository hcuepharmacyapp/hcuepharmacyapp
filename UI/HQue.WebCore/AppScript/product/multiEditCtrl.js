﻿app.controller('multiEditCtrl', function ($scope, toastr, productService, productModel, pagerServcie, $filter) {

    var product = productModel;

    $scope.search = product;

    $scope.list = [];

    var noGst = document.getElementById("NoGst").value.toLowerCase();
    $scope.isGstNullSearch = false;
    if (noGst == "true")
        $scope.isGstNullSearch = true;

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
    //pagination
    $scope.isUpdate = false;

    function pageSearch() {
        //if ($scope.isUpdate)
        //    $scope.updateAllProduct($scope.list);

        $scope.search.isGstNullSearch = $scope.isGstNullSearch;

        productService.Masterlist($scope.search).then(function (response) {
            $scope.list = response.data.list;

            $scope.selectedIndex = -1;
        }, function () { });
    }
    $scope.Entirelist = [];

    //Vat validation added by nandhini  on 18/05/2017
    $scope.vatErrorMessage = false;
    $scope.changeVat = function (vat) {
        if (vat != "" || vat != undefined) {
            if (vat > 100) {
                $scope.vatErrorMessage = true;
            } else {
                $scope.vatErrorMessage = false;
            }
        }
    };
    //GST related starts

    $scope.igstErrorMessage = false;
    $scope.changeIGST = function () {
        var igst = $scope.productsItem.igst;

        if (igst != "" || igst != undefined) {
            if (igst > 100) {
                $scope.igstErrorMessage = true;
            } else {
                $scope.igstErrorMessage = false;
            }
        }
    };
    $scope.cgstErrorMessage = false;
    $scope.changeCGST = function () {
        var cgst = $scope.productsItem.cgst;
        if (cgst != "" || cgst != undefined) {
            if (cgst > 100) {
                $scope.cgstErrorMessage = true;
            } else {
                $scope.cgstErrorMessage = false;
            }
        }
    };
    $scope.sgstErrorMessage = false;
    $scope.changeSGST = function () {
        var sgst = $scope.productsItem.sgst;
        if (sgst != "" || sgst != undefined) {
            if (sgst > 100) {
                $scope.sgstErrorMessage = true;
            } else {
                $scope.sgstErrorMessage = false;
            }
        }
    };
    $scope.gstTotalErrorMessage = false;
    $scope.changeGSTTotal = function () {
        var gstTotal = $scope.productsItem.gstTotal;
        if (gstTotal != "" || gstTotal != undefined) {
            if (gstTotal > 100) {
                $scope.gstTotalErrorMessage = true;
            } else {
                $scope.gstTotalErrorMessage = false;
                $scope.isUpdate = true;
            }
        }
    };
    

    //GST related ends

    $scope.productSearch = function (productName, obj) {
        

        if ($scope.isUpdate) {

            $.confirm({
                title: "The changes made will be unsaved. Do you still want to navigate?",
                content: "",
                closeIcon: function () {
                },
                buttons: {
                    yes: {
                        text: 'yes [y]',
                        btnClass: 'primary-button',
                        keys: ['y'],
                        action: function () {
                            loadMasterList(productName);
                            $scope.isUpdate = false;
                        }
                    },
                    no: {
                        text: 'no [n]',
                        btnClass: 'secondary-button',
                        keys: ['n'],
                        action: function () {
                            $.LoadingOverlay("hide");
                        }
                    }
                }
            });
        }
        else
        {
            loadMasterList(productName);
            $scope.isUpdate = false;
        }
    }

    function loadMasterList(productName) {
        $.LoadingOverlay("show");

        if (productName != undefined && productName != null) {
            $scope.search.name = productName;
        }
        else if ($scope.selectedProduct != null) {
            $scope.search.name = $scope.selectedProduct.title;
        }
        else {
            $scope.search.name = null;
        }
        $scope.search.page.pageNo = 1;
        //if ($scope.isUpdate)
        //    $scope.updateAllProduct($scope.list);
        $scope.search.isGstNullSearch = $scope.isGstNullSearch;

        productService.Masterlist($scope.search).then(function (response) {
            //  console.log(JSON.stringify(response.data));
            $scope.list = response.data.list;
            //$scope.exportList = $scope.list.name;
            
            for (var i = 0; i < $scope.list.length; i++) {
                if ($scope.list[i].status == 1 || $scope.list[i].status == undefined || $scope.list[i].status == null) {
                    $scope.list[i].status = 'Active';
                }
                else {
                    $scope.list[i].status = 'InActive';
                }

            }
            //if ($scope.list.length > 0)
            //{
            //    $('table#datatable1 tr:nth-child(2) td:nth-child(8)').focus();
            //}
            pagerServcie.init(response.data.noOfRows, pageSearch);
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.productSearch();

    $scope.cancel = function () {
        $scope.selectedIndex = -1;
    }

    $scope.focusNextField = function (nextid) {
        var ele = document.getElementById(nextid);
        ele.focus();
    };

    // Bulk Update
    $scope.updateAllProduct = function (list) {

        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        for (var i = 0; i < list.length; i++) {
            if (list[i].status == 'Active') {
                list[i].status = 1;
            }
            else {
                list[i].status = '2';
            }
        }
        productService.bulkUpdate(list).then(function (response) {
            $scope.isUpdate = false;
            $scope.productSearch($scope.search.name);
            ///$scope.searchProduct();
            toastr.success('Product updated successfully');
            $scope.selectedIndex = -1;
            $scope.productsItem = {};
            $scope.isProcessing = false;
            
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Product already exist', 'Error');
            $scope.isProcessing = false;
            
        });
    }

    //$scope.$watch('product.gstTotal', function (newValue, oldValue, $scope) {
    //    if (newValue) {
    //        $scope.product.sgst = newValue / 2;
    //        $scope.product.cgst = newValue / 2;
    //        $scope.product.igst = newValue;
    //    }
    //});

    $scope.changeGstTotal = function (product) {
        var gstTotal = parseFloat(product.gstTotal);
        if (isNaN(gstTotal)) {
            gstTotal = "";
            product.sgst = "";
            product.cgst = "";
            product.igst = "";
        }
        else {
            product.sgst = gstTotal / 2;
            product.cgst = gstTotal / 2;
            product.igst = gstTotal;
            $scope.isUpdate = true;
            product.isEdit = true;
        }
    }
    $scope.changeHsn = function (product) {
        $scope.isUpdate = true;
        product.isEdit = true;
    }

    
    $scope.customPagination = function (pageNo)
    {
        if ($scope.isUpdate) {
            //var htmlContent = "<ul>Please press update button before going to another page.</ul>";
            $.confirm({
                title: "The changes made will be unsaved. Do you still want to navigate?",
                content: "",
                closeIcon: function () {
                },
                buttons: {
                    yes: {
                        text: 'yes [y]',
                        btnClass: 'primary-button',
                        keys: ['y'],
                        action: function () {
                            $scope.paginate(pageNo);
                            $scope.isUpdate = false;
                        }
                    },
                    no: {
                        text: 'no [n]',
                        btnClass: 'secondary-button',
                        keys: ['n'],
                        action: function () {
                        }
                    }
                }
            });
        }
        else {
            $scope.paginate(pageNo);
            $scope.isUpdate = false;
        }
    }

    $scope.getTaxValues = function () {
        productService.getTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    //by San 
    $scope.getTaxValues();

});

app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive("contenteditable", function () {
    return {
        restrict: "A",
        require: "ngModel",
        
        link: function (scope, element, attrs, ngModel) {

            function read() {
                ngModel.$setViewValue(element.html());
            }
            ngModel.$render = function () {
                element.html(ngModel.$viewValue || "");
            };
            element.bind("blur keyup change", function () {
                scope.$apply(read);
                //alert(scope);
            });

            element.css('outline', 'none');
            element.bind("blur", function (event) {
                element[0].blur();
                if (element[0].innerText != '' && element[0].innerText != null) {

                    if (attrs.ngModel != "product.hsnCode") {
                        if ((parseFloat(element[0].innerText) > 100 || ((element[0].innerText).length) >= 6) && event.which != 9 && event.which != 8 && event.which != 13 && event.which != 110 && event.which != 190 || (event.which < 48 && event.which > 57) || (event.which < 37 && event.which > 40)) {
                            event.preventDefault();
                            return false;
                        }
                    }
                    else if (attrs.ngModel == "product.hsnCode") {
                        if (element[0].innerText.length >= 10 && event.which != 8 && event.which != 13 && event.which != 9)
                            event.preventDefault();
                        return false;
                    }
                    scope.product.isEdit = true;
                    //scope.isUpdate = true;
                }

                event.preventDefault();
            });

            element.bind("keydown", function (event) {

                if (element[0].innerText != '' && element[0].innerText != null && element[0].innerText != undefined) {
                    if (attrs.ngModel != "product.hsnCode") {
                        if ((parseFloat(element[0].innerText + event.charCode) > 100 || ((element[0].innerText + event.charCode).length) >= 6) && event.which != 9 && event.which != 8 && event.which != 13 && event.which != 110 && event.which != 190 || (event.which < 48 && event.which > 57) || (event.which < 37 && event.which > 40)) {
                            event.preventDefault();
                            return false;
                        }
                        //else
                        //{
                        //    var gstTotal = parseInt(element[0].innerText);
                        //    $scope.product.sgst = gstTotal / 2;
                        //    $scope.product.cgst = gstTotal / 2;
                        //    $scope.product.igst = gstTotal;
                        //}
                    }
                    else if (attrs.ngModel == "product.hsnCode") {
                        if (element[0].innerText.length >= 10 && event.which != 8 && event.which != 9 && event.which != 13) {
                            event.preventDefault();
                            return false;
                        }
                    }
                    scope.product.isEdit = true;
                }
                if (event.which === 13) {
                    event.preventDefault();
                    element.next().focus();
                }
            });
        }
    };
});


