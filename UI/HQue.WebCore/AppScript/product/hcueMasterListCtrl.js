﻿app.controller('hcueMasterListCtrl', function ($scope, toastr, productMasterService, productModel, pagerServcie, $filter) {

    var sortingOrder = 'name';
    var product = productModel;

    $scope.search = product;

    $scope.list = [];

    //pagination
    $scope.search.page = pagerServcie.page;
    $scope.pages = pagerServcie.pages;
    $scope.paginate = pagerServcie.paginate;
      
    function pageSearch() {
        productMasterService.listProduct($scope.search).then(function (response) {
            $scope.list = response.data.list;            
            $scope.selectedIndex = -1;
        }, function () { });
    }

    $scope.getData=function()
    {
        productMasterService.list($scope.search).then(function (response) {
            $scope.list = response.data.list;
        }, function () { });
    }
    $scope.productSearch = function (productName, obj) {
        $.LoadingOverlay("show");

        if (productName != undefined && productName != null) {
            $scope.search.name = productName;
        }
        else if ($scope.selectedProduct != null) {
            $scope.search.name = $scope.selectedProduct.title;
        }
        else {
            $scope.search.name = null;
        }

        $scope.search.page.pageNo = 1;
        productMasterService.listProduct($scope.search).then(function (response) {
            //$scope.list = response.data.list;
            $scope.list = response.data;
            if (obj == 0)
                $scope.currentPage = 0;
            else
                $scope.currentPage = $scope.currentPage;
            
            $scope.searchProduct();
            $scope.productMasterCountToday();
           $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }


    $scope.productMasterCountToday = function () {
           productMasterService.productMasterUpdateCount().then(function (response) {
            $scope.count = response.data;           
         
        }, function () {
          
        });
    }
    $scope.productMasterCountToday();


    //by San
    //$scope.productSearch();

    function showDetails(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $scope.editProducts(dataItem);
    }


    $scope.editProducts = function (item, index) {
       
        $scope.selectedIndex;
    
        $scope.productsItem = JSON.parse(JSON.stringify(item));

          $scope.scheduleList = [
             { schedule: 'H', label: 'H' },
            { schedule: 'H1', label: 'H1' },
            { schedule: 'NH', label: 'NH' },
            { schedule: 'X', label: 'X' }
          ];
    }

    $scope.updateProduct = function () {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;
        
        if ($scope.productsItem.genericName !=null && $scope.productsItem.genericName.genericName !== undefined)
            $scope.productsItem.genericName = $scope.productsItem.genericName.genericName;
             
        //if ($scope.productsItem.manufacturer != null && $scope.productsItem.manufacturer.manufacturer !== undefined)
        //    $scope.productsItem.manufacturer = $scope.productsItem.manufacturer.manufacturer;

        if ($scope.productsItem.schedule != null && $scope.productsItem.schedule.schedule !== undefined)
            $scope.productsItem.schedule = $scope.productsItem.schedule.schedule;

           productMasterService.update($scope.productsItem).then(function (response) {
               $scope.productSearch($scope.search.name);
              $scope.searchProduct();              
            toastr.success('Product updated successfully');
            $scope.selectedIndex = -1;
           
            $scope.productsItem = {};
            $scope.isProcessing = false;
           $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Product already exist', 'Error');
            $scope.isProcessing = false;
        });
    }

    $scope.cancel = function () {       
        $scope.selectedIndex = -1;       
    }

    $scope.keyEnter = function (event, e) {
        var ele = document.getElementById(e);
        if (event.which === 13) // Enter key
        {
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
            {
                ele.focus();
                if (ele.nodeName != "BUTTON")
                    ele.select();
            }
        }
    }

 

    $scope.getGeneric = function (val) {
        return productMasterService.genericFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getManufacturer = function (val) {

        return productMasterService.manufacturerFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getSchedule = function (val) {

        return productMasterService.scheduleFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getType = function (val) {

        return productMasterService.typeFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getCategory = function (val) {

        return productMasterService.categoryFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }

    $scope.getCommodity = function (val) {

        return productMasterService.commodityFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }


    $scope.getKindName = function (val) {

        return productMasterService.kindNameFilterData(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }
   
    function exportList() {
         productMasterService.exportList($scope.search).then(function (response) {
            $scope.exportList = response.data.list;

        }, function () { });
    }

    $scope.sortingOrder = sortingOrder;
    $scope.reverse = false;
    $scope.filteredItems = [];
    $scope.groupedItems = [];
    $scope.itemsPerPage = 50;
    $scope.pagedItems = [];
    $scope.currentPage = 0;  

    var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    };

    // init the filtered items
    $scope.searchProduct = function () {
        $scope.filteredItems = $filter('filter')($scope.list, function (item) {
            for (var attr in item) {
                if (searchMatch(item[attr], $scope.query))
                    return true;
            }
            return false;
        });
        // take care of the sorting order
        if ($scope.sortingOrder !== '') {
            $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
        }
        $scope.currentPage = $scope.currentPage;
       // $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    };

    // calculate page in place
    $scope.groupToPages = function () {
        $scope.pagedItems = [];

        for (var i = 0; i < $scope.filteredItems.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
            }
        }
    };

    $scope.range = function (start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pagedItems.length - 1) {
            $scope.currentPage++;
        }
    };

    $scope.setPage = function () {
        $scope.currentPage = this.n;
    };

    // functions have been describe process the data for display
   

    // change sorting order
    $scope.sort_by = function (newSortingOrder) {
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

        // icon setup
        $('th i').each(function () {
            // icon reset
            $(this).removeClass().addClass('icon-sort');
        });
        if ($scope.reverse)
            $('th.' + new_sorting_order + ' i').removeClass().addClass('icon-chevron-up');
        else
            $('th.' + new_sorting_order + ' i').removeClass().addClass('icon-chevron-down');
    };

    $scope.updateProductById = function (product) {
        $.LoadingOverlay("show");
        $scope.isProcessing = true;

       
        productMasterService.update(product).then(function (response) {
            $scope.productSearch($scope.search.name);
            $scope.searchProduct();
            //toastr.success('Product updated successfully');
            $scope.selectedIndex = -1;
            $scope.productsItem = {};
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Product already exist', 'Error');
            $scope.isProcessing = false;
        });
    }

    $scope.clear=function()
    {
        $scope.search.name = null;
        $scope.search.code = "";
        $scope.productSearch();
    }



});

app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive("contenteditable", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function(scope, element, attrs, ngModel) {

            function read() {
                ngModel.$setViewValue(element.html());
            }
            ngModel.$render = function() {
                element.html(ngModel.$viewValue || "");
            };
            element.bind("blur keyup change", function() {
                scope.$apply(read);
                //alert(scope);
            });


            element.css('outline','none');
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    element[0].blur();
                    //$scope.product;
                    //alert(scope.product);
                    if (scope.product.genericName != null && scope.product.genericName != undefined && scope.product.genericName!="")
                        scope.updateProductById(scope.product);
                    event.preventDefault();
                }
            });

        }
    };
});