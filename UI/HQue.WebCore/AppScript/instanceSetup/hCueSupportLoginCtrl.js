app.controller('hCueSupportLoginCtrl', function ($scope, close, hQueUserModel, url, $http, toastr) {

    $scope.email = '';
    $scope.password = '';
    $scope.showloading = false;

    $scope.close = function (result) {
        close(result, 500);
        $(".modal-backdrop").hide();
    };

    $scope.submit = function () {
        if ($scope.email == null || $scope.email == '') {
            toastr.error('Please enter Email');
            return;
        }
        if ($scope.password == null || $scope.password == '') {
            toastr.error('Please enter Password');
            return;
        }

        var obj = hQueUserModel;
        obj.userId = $scope.email;
        obj.password = $scope.password;

        $scope.showloading = true;
        $http({
            method: 'POST',
            url: '/user/LoginBySupport',
            data: obj
        }).then(function (response) {
            $scope.showloading = false;
            if (response.data == 'Success') {
                $scope.close("Yes");
                window.open(url);
            }
        }, function (error) {
            $scope.showloading = false;
        });
    }
});