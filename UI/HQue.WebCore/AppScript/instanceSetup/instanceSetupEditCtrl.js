﻿app.controller('updateInstanceCtrl', function ($scope, instanceModel, instanceSetupService, toastr) {

    var instance = instanceModel;
   // instance.user = hQueUserModel;

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.instance = instance;
    //$scope.instance.registrationDate = new Date();
   // $scope.user = instance.user;
   // $scope.instance.userList = [];

    $scope.init = function (id) {
        $.LoadingOverlay("show");
        $scope.instance.id = id;

        instanceSetupService.editInstance($scope.instance.id).then(function (response) {
            $scope.instance = response.data;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    $scope.updateInstance = function () {
        $.LoadingOverlay("show");
        instanceSetupService.updateInstance($scope.instance).then(function (response) {
            $scope.instanceUser.$setPristine();
            window.location.assign("/instanceSetup/list");
            toastr.success('Pharmacy Updated Successfully');
            $.LoadingOverlay("hide");
        }, function (reason) {
            $.LoadingOverlay("hide");
        });
    }
});

