﻿app.controller('instanceSetupCreateCtrl', function ($scope, instanceModel, hQueUserModel, instanceSetupService, toastr) {

    var instance = instanceModel;
    instance.user = hQueUserModel;

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.instance = instance;
    $scope.instance.registrationDate = new Date();
    $scope.user = instance.user;
    $scope.instance.userList = [];

    $scope.addUser = function () {
        if ($scope.user.name == null || $scope.user.mobile == null || $scope.user.userId == null) {
            toastr.error('Please Fill User Information');
        }
        else {
            var isUpdated = false;

            if (!isUpdated)
                $scope.instance.userList.push($scope.user);

            $scope.user = null;
            $scope.instanceUser.$setPristine();
        }
    }

    $scope.removeUser = function (item) {
        var index = $scope.instance.userList.indexOf(item);
        $scope.instance.userList.splice(index, 1);
    }

    $scope.instanceCreate = function () {
        $.LoadingOverlay("show");
        instanceSetupService.create($scope.instance).then(function (response) {
            $scope.instanceUser.$setPristine();
            toastr.success('Pharmacy Created Successfully');
            $scope.instance = {
                registrationDate: new Date(),
                userList: []
            };
            $.LoadingOverlay("hide");
        }, function (reason) {
            $.LoadingOverlay("hide");
            toastr.error('Email Id or Mobile Number already exists', 'error');
        });
    }
});

