﻿app.config(function ($routeProvider) {
    $routeProvider.when('/customerReminder', {
        "templateUrl": '/Reminder/Customer'
    });
    $routeProvider.when('/personalReminder', {
        "templateUrl": '/Reminder/Personal'
    });
  
    $routeProvider.otherwise({ redirectTo: '/customerReminder' });
});