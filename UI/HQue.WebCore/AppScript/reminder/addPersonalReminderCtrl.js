app.controller('addPersonalReminderCtrl', function ($scope, reminderService, patientService, productService, $rootScope,toastr) {
    $scope.reminderType = 1;
    $scope.selectedProduct = { qty: 0 };
    $scope.selectedCustomer = null;
    $scope.patientSearchData = {};
    $scope.addedProduct = [];
    $scope.model = { reminderFrequency: "16", reminderTime: "9" };

    $scope.init = function (value) {
        $scope.reminderType = value;
    }

    $scope.minDate = new Date();
    var d = new Date();

    $scope.popup1 = {
        opened: false
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,

    };

    $scope.addReminder = function () {
        var data = getSaveData();

        reminderService.addReminnder(data).then(function (response) {
            $rootScope.$broadcast('personal-reminder-added', { any: {} });
            $.LoadingOverlay("hide");
            $scope.clear();
            toastr.success('Reminder added successfully');
            hcueUserReminderGlobal();
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    function getSaveData() {
        var data = {
            startDate: $scope.model.startDate,
            reminderTime: $scope.model.reminderTime,
            reminderFrequency: $scope.model.reminderFrequency,
            reminderType: $scope.reminderType,
            description: $scope.model.description,
            remarks: $scope.model.remarks,
        };

        return data;
    }

    $scope.clear = function () {
        $scope.model = { reminderFrequency: "16", reminderTime: "9" };
        $scope.addReminderForm.$setPristine();
        document.getElementById('description').focus();
    }
});