var hcueUserReminderGlobal = function () {
    $.get("/UserReminderData/OpenReminderCount", function (data) {
        var counter = $('#userReminderLayoutCounter');
        var count = parseInt(data);
        if (count == 0) {
            counter.hide();
            counter.html('');
            return;
        }

        var countText = count;
        if (count > 99)
            countText = "99+";

        counter.show();
        counter.html(countText);
    });

};

$(document).ready(function () {
    hcueUserReminderGlobal();
});