﻿app.factory('reminderService', function ($http) {
    return {
        customerReminderList: function (isUpcoming, data) {
            return $http.post('/UserReminderData/CustomerReminderList?isUpcoming=' + isUpcoming, data)
        },
        allCustomerReminder: function (data) {
            return $http.post('/UserReminderData/AllCustomerReminder', data)
        },
        allPersonalReminder: function () {
            return $http.post('/UserReminderData/AllPersonalReminder')
        },
        personalReminderList: function (isUpcoming) {
            return $http.post('/UserReminderData/PersonalReminderList?isUpcoming=' + isUpcoming)
        },
        addReminnder: function (data) { return $http.post('/UserReminderData/CreateCustomerReminder', data) },
        cancelUserReminder: function (id) { return $http.post('/userReminderData/CancelReminder?id=' + id) },
        saveRemark: function (data) { return $http.post('/userReminderData/saveRemark', data); },
    };
});