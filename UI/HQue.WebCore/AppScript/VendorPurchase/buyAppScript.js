﻿var app = angular.module('hcue', ['commonApp', 'toastr', 'angucomplete', 'localytics.directives', "ui.bootstrap", 'angularModalService']);

//Added for allowing only two decimal places in DC percent, by bala - 19/05/17 
app.directive('decimalPlaces', function () {
    return {
        link: function (scope, ele, attrs) {
            ele.bind('keypress', function (e) {
                var newVal = $(this).val() + (e.charCode !== 0 ? String.fromCharCode(e.charCode) : '');
                if ($(this).val().search(/(.*)\.[0-9][0-9]/) === 0 && newVal.length > $(this).val().length) {
                    e.preventDefault();
                }
            });
        }
    };
});
