﻿app.controller('DraftListCtrl', function ($scope, close, vendorPurchaseArray, vendorPurchaseService, draftCount) {
    $scope.vendorPurchaseArray = vendorPurchaseArray;
    var deletedDraftIndex = -1;
    var UpdatedDraftCount = draftCount;


    $scope.deleteDraft = function (item, index) {
        vendorPurchaseService.DeleteDraft(item.id).then(function (resp) {
            UpdatedDraftCount = resp.data;
            deletedDraftIndex = index;
            $scope.vendorPurchaseArray.splice(index, 1);
        }, function (error) {
            console.log(error);
        });
    };
    $scope.selectedDraft = function (item) {
        $scope.close({ "data": item, "deletedDraftIndex": deletedDraftIndex });
    };

    $scope.closeModal = function () {
        $scope.close({ "data": null, "deletedDraftIndex": deletedDraftIndex });
    };
    $scope.close = function (result) {
        result.UpdatedDraftCount = UpdatedDraftCount;
        close(result, 500);
        $(".modal-backdrop").hide();
    };
});
