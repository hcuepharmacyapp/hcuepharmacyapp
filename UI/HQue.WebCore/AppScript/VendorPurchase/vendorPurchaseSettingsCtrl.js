﻿app.controller('vendorPurchaseSettingsCtrl', function ($scope, $rootScope, toastr, vendorPurchaseService, vendorPurchaseModel, vendorPurchaseItemModel, productStockModel, vendorService, productService, productModel, ModalService, $filter, productStockService, cacheService, purchaseSettingsModel, salesService) {

    //Check offline status
    $scope.IsOffline = false;
    $scope.isOnlineEnabled = false;
    $scope.invoiceDateEditOption = "No";
    getOfflineStatus = function () {
        vendorPurchaseService.getOfflineStatus().then(function (response) {
            if (response.data) {
                $scope.IsOffline = true;
            }
        });
    };
    //
    getOfflineStatus();

    //Added by Sarubala on 18-05-18
    getOnlineEnabledStatus = function () {
        vendorPurchaseService.getOnlineEnableStatus().then(function (response) {
            if (response.data) {
                $scope.isOnlineEnabled = true;
            }
        }, function (error) {
            console.log(error);
        });
    };

    getOnlineEnabledStatus();

    getTaxtype();
    $scope.SaveTaxSeries = "";
    $scope.searchType = null;
    $scope.isAllowDecimal = null;
    $scope.salesSettings = null;
    $scope.enableSelling = 0;
    $scope.isSortByLowestPrice = "false"; // Added by Sarubala on 26-09-17

    $scope.invoiceValueSetting = "false"; // Added by Sarubala on 11/10/18
    $scope.invoiceValueDifference = 0; // Added by Sarubala on 11/10/18

    $scope.purchaseSettings = purchaseSettingsModel;
    //added by nandhini for tax settings start 8/12/17
    $scope.taxSettingsTab = "tabNormal";
    $scope.purchaseSettingsTab = "tabSelected";
   
    $scope.showTaxSettings = function () {
        $scope.settingsPanel = true;
        $scope.taxSettingPanel = true;         
        $scope.taxSettingsTab = "tabSelected";
        $scope.purchaseSettingsTab = "tabNormal";
        $scope.getTaxValues();
    };
    $scope.showPurchaseSettings = function () {
        $scope.settingsPanel = false;
        $scope.taxSettingPanel = false;
        $scope.purchaseSettingsTab = "tabSelected";
        $scope.taxSettingsTab = "tabNormal";
    };
    $scope.SaveTaxSeriesList = function () {
        $scope.taxValues.isActive = 0;
        $scope.taxValues.taxGroupId = 1;
        vendorPurchaseService.saveTaxSeries($scope.taxValues).then(function (response) {
            //console.log(JSON.stringify(response));
            $scope.taxValues.tax = "";
            toastr.success('Tax % Added Successfully');
            $scope.getTaxValues();
            $.LoadingOverlay("hide");
        }, function (response) {
            $scope.responses = response;
            var msg = response.data.errorDesc;
            var taxPercentage = "Tax %  Already Exist";
            if (msg.indexOf(taxPercentage) != -1)
                toastr.info(response.data.errorDesc, 'Info..');
            else
                toastr.error(response.data.errorDesc, 'Error');
            $scope.isProcessing = false;
            document.getElementById("taxSeries").focus();
        });
    };
   
    $scope.getTaxValues = function () {
        vendorPurchaseService.getAllTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;
            $scope.options = { Active: 0, Hide: 1 };
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };

    $scope.updateTaxValuesStatus = function (item) {
        $.LoadingOverlay("show");
        vendorPurchaseService.updateTaxValuesStatus(item).then(function (response) {
            item.isEdit = !response.data;
            toastr.success('Tax status  updated Successfully');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    };
    //added by nandhini for tax settings end 8/12/17
    function getTaxtype() {
        //console.log("Calling");
        vendorPurchaseService.getTaxtype().then(function (response) {
            // console.log(JSON.stringify(response.data));

            if (response.data != "" && response.data != null) {
                $scope.SaveTaxSeries = response.data.taxType;
                //console.log($scope.InvoiceSeriestype);
                if ($scope.SaveTaxSeries == 1) {
                    $scope.SaveTaxType = "DefaultTax";
                }

                if ($scope.SaveTaxSeries == 2) {
                    $scope.SaveTaxType = "CustomTax";
                    getCustomTaxSeries();
                }

                //Added by Sarubala on 26-09-19
                if (response.data.isSortByLowestPrice != undefined) {
                    $scope.isSortByLowestPrice = response.data.isSortByLowestPrice.toString(); 
                }
                

                //if ($scope.InvoiceSeriestype == 3) {
                //    $scope.InvocieSeries = "InvoiceManual";
                //}
            }
        }, function () {

        });
    }

    //Added by Sarubala on 26-09-19 - start
    $scope.saveSortByLowestPrice = function () {
        $.LoadingOverlay("show");
        var isSort = $scope.isSortByLowestPrice == "true" ? true : false;
        vendorPurchaseService.SaveSortLowestPrice(isSort).then(function (response) {
            
            toastr.success('Sort By Lowest Price Saved successfully');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');

        });
    }
    //Added by Sarubala on 26-09-19 - end

    $scope.ChangeTax = function () {
        if ($scope.SaveTaxType == 'CustomTax') {
            getCustomTaxSeries();
        }
    };

    $scope.SaveTaxtypeseries = function () {
        //console.log($scope.SaveTaxType);
        $.LoadingOverlay("show");

        var Series = 0;
        if ($scope.SaveTaxType == 'DefaultTax') {
            Series = 1;
        }

        if ($scope.SaveTaxType == 'CustomTax') {
            Series = 2;
        }



        vendorPurchaseService.SaveTaxType(Series).then(function (response) {
            // console.log(JSON.stringify(response));
            toastr.success('Tax Type Saved successfully');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');

        });

    };

    $scope.SaveCustomTaxSeries = function () {


        //console.log($scope.CustomTaxName);

        $.LoadingOverlay("show");


        var customseriesName = $scope.CustomTaxName.toUpperCase();

        for (var i = 0; i < $scope.TaxSeriesItems.length; i++) {
            if ($scope.TaxSeriesItems[i].taxSeriesName == customseriesName) {
                $.LoadingOverlay("hide");
                toastr.error('' + customseriesName + ' Series Already Exists');
                var seriestxt = document.getElementById("CustomTaxName");
                seriestxt.focus();
                return false;
            }
        }

        vendorPurchaseService.saveTaxseriesItem(customseriesName).then(function (response) {
            //console.log(JSON.stringify(response));


            if (response.data != null && response.data != "") {
                toastr.success('Added successfully');
                $scope.CustomTaxName = "";
                //$scope.isProcessing = false;

                getCustomTaxSeries();
                $.LoadingOverlay("hide");
            }
            else {
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
                //   $scope.isProcessing = false;
            }

        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            // $scope.isProcessing = false;
        });


    };

    // Added Gavaskar 07-02-2017 Start 

    getBuyInvoiceDateEditSetting = function () {
        vendorPurchaseService.getBuyInvoiceDateEditSetting().then(function (response) {
            if (response.data != "" && response.data.buyInvoiceDateEdit != undefined) {
                $scope.invoiceDateEditOption = response.data.buyInvoiceDateEdit;

                if ($scope.invoiceDateEditOption == 1) {
                    $scope.invoiceDateEditOption = "Yes";
                }
                if ($scope.invoiceDateEditOption == 2) {
                    $scope.invoiceDateEditOption = "No";
                }
            }

        }, function () {

        });
    }

    getBuyInvoiceDateEditSetting();

    $scope.saveInvoiceDateEditOption = function () {

        $.LoadingOverlay("show");
        $scope.isProcessing = true;

        if ($scope.invoiceDateEditOption == "Yes") {
            $scope.invoiceDateEditOption = 1;
        }
        if ($scope.invoiceDateEditOption == "No") {
            $scope.invoiceDateEditOption = 2;
        }

        vendorPurchaseService.saveInvoiceDateEditOption($scope.invoiceDateEditOption).then(function (response) {
            toastr.success('Invoice Date Edit Option Saved successfully');
            getBuyInvoiceDateEditSetting();
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');

        });
    };

    // Added Gavaskar 07-02-2017 End 

    $scope.TaxSeriesItems = [];
    getCustomTaxSeries = function () {

        vendorPurchaseService.getCustomTaxSeriesItems().then(function (response) {
            //console.log(JSON.stringify(response));
            if (response.data != "" && response.data != null) {
                $scope.TaxSeriesItems = response.data;
                //console.log(JSON.stringify($scope.TaxSeriesItems));
            }

        }, function () {

        });
    }


    $scope.saveSearch = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.saveSearch($scope.searchType).then(function (response) {
            if (response.data != null) {
                $scope.searchType = response.data.searchType;
                toastr.success('Product Search Type Saved successfully');
                $.LoadingOverlay("hide");
            }
        }, function (error) {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            console.log(error);
        });
    };

    getSearch();

    function getSearch() {
        vendorPurchaseService.getSearch().then(function (response) {
            if (response.data != null) {
                $scope.searchType = response.data;
            }
        }, function (error) {
            console.log(error);
        });
    };

    $scope.saveDecimalSettings = function () {
        $.LoadingOverlay("show");
        $scope.isAllowDecimal = ($scope.isAllowDecimal == "true");
        vendorPurchaseService.saveDecimalSettings($scope.isAllowDecimal).then(function (response) {

            $scope.isAllowDecimal = response.data.isAllowDecimal.toString();
            toastr.success("Allow Decimal Settings saved successfully");
            $.LoadingOverlay("hide");


        }, function (error) {
            console.log(error);
            toastr.error("Error Occurred", 'Error');
            $.LoadingOverlay("hide");
        })
    };

    $scope.getDecimalSettings = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getDecimalSetting().then(function (response) {

            $scope.isAllowDecimal = response.data.toString();
            $.LoadingOverlay("hide");

        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        })
    };

    $scope.getDecimalSettings();

    $scope.getCompletePurchaseSettings = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getCompletePurchaseSetting().then(function (response) {

            $scope.completePurchaseKeyType = response.data.toString();
            if ($scope.completePurchaseKeyType == "2")
                $scope.completePurchaseKeyType = "2";
            else
                $scope.completePurchaseKeyType = "1";

            $.LoadingOverlay("hide");

        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        })
    };

    $scope.getCompletePurchaseSettings();


    $scope.savecompletePurchaseKeyType = function () {
        vendorPurchaseService.savecompletePurchaseKeyType($scope.completePurchaseKeyType).then(function (response) { //completeSaleKeys
            toastr.success('Saved complete sale key successfully');
            $scope.isProcessing = false;
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };

    $scope.getPurchaseBillSeriesType = function () {
        vendorPurchaseService.getBillSeriesType().then(function (response) {
            $scope.purchaseSettings = response.data;
            if ($scope.purchaseSettings.billSeriesType == null || $scope.purchaseSettings.billSeriesType == undefined) {
                $scope.purchaseSettings.billSeriesType = 1;
            }

            $scope.isProcessing = false;
            $.LoadingOverlay("hide");

        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
            $scope.isProcessing = false;
        });
    };

    $scope.getPurchaseBillSeriesType();

    $scope.saveBillSeriesType = function () {
        var enableSave = true;
        $scope.isProcessing = true;
        $.LoadingOverlay("show");
        if ($scope.purchaseSettings.billSeriesType == 2) {
            if ($scope.salesSettings == null || $scope.salesSettings == undefined || $scope.salesSettings == '') {
                enableSave = false;
            }
            else {
                enableSave = true;
            }
        }
        else {
            enableSave = true;
        }
        if (enableSave) {
            vendorPurchaseService.saveBillSeriesType($scope.purchaseSettings).then(function (response) {
                $scope.isProcessing = false;
                toastr.success("Bill Series Saved Successfully");
                $.LoadingOverlay("hide");

            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                toastr.error('Error Occured', 'Error');
                $scope.isProcessing = false;
            });
        }
        else {
            $.LoadingOverlay("hide");
            toastr.error('Please create custom series name in sales settings');
            $scope.isProcessing = false;
        }

    };

    $scope.checkBillSeriesType = function () {
        if ($scope.purchaseSettings.billSeriesType == 1) {
            $scope.purchaseSettings.customSeriesName = null;
        }
        else {
            salesService.getInvoiceSeriesItemForGRNSettings().then(function (response) {
                $scope.salesSettings = response.data;
                if ($scope.salesSettings == null || $scope.salesSettings == undefined || $scope.salesSettings == '') {
                    toastr.error("Please create custom series name in sales settings");
                }
                else {
                    $scope.purchaseSettings.customSeriesName = $scope.salesSettings;
                }

            }, function (error) {

            });
        }

    };


    $scope.btnSaveEnableSelling = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.saveEnableSelling($scope.enableSelling).then(function (response) {
            $.LoadingOverlay("hide");
            toastr.success("Enable Selling Price Settings saved successfully");
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
            toastr.error("Error Occurred", 'Error');
        });
    };

    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    };
    $scope.getEnableSelling();

    // Added by Sarubala on 11/10/18 - start
    $scope.getInvoiceValueSetting = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getInvoiceValueSettings().then(function (response) {
            $scope.invoiceValueSetting = response.data.invoiceValueSetting == true ? response.data.invoiceValueSetting.toString() : "false";
            $scope.invoiceValueDifference = response.data.invoiceValueDifference;
            $.LoadingOverlay("hide");

        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    };

    $scope.getInvoiceValueSetting();

    $scope.saveInvoiceValueSetting = function () {

        if ($scope.invoiceValueSetting == "true" && ($scope.invoiceValueDifference == null || $scope.invoiceValueDifference == undefined)) {
            toastr.error("Enter InvoiceValueDifference Amount");
            return false;
        }

        $.LoadingOverlay("show");
        if ($scope.invoiceValueSetting != "true") {
            $scope.invoiceValueDifference = 0;
        }
        $scope.purchaseSettings.invoiceValueSetting = $scope.invoiceValueSetting;
        $scope.purchaseSettings.invoiceValueDifference = $scope.invoiceValueDifference;
        vendorPurchaseService.saveInvoiceSettings($scope.purchaseSettings).then(function (response) {
            $scope.invoiceValueSetting = response.data.invoiceValueSetting == true ? response.data.invoiceValueSetting.toString() : "false";
            $scope.invoiceValueDifference = response.data.invoiceValueDifference;
            $.LoadingOverlay("hide");
            toastr.success("Settings saved successfully");
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });

    };

    // Added by Sarubala on 11/10/18 - end

});
