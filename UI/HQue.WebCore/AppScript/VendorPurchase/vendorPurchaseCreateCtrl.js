﻿app.controller('vendorPurchaseCreateCtrl', function ($scope, toastr, vendorPurchaseService, vendorPurchaseModel, draftVendorPurchaseModel, vendorPurchaseItemModel, productStockModel, vendorService, productService, productModel, ModalService, $filter, productStockService, cacheService, dCVendorPurchaseItemModel, vendorOrderService, vendorOrderItemModel, shortcutHelper, vendorReturnModel, vendorReturnItemModel, alternateVendorProductModel) {
    $scope.previouspurchases = [];
    $scope.isItScan = false;
    $scope.scanBarcodeOption = {
        "scanBarcode": "0"
    };
    $scope.GSTEnabled = null;
    $scope.enableSelling = false;
    $scope.chkPODate = false;
    $scope.disableAddNewDc = false;
    $scope.AnyPopupOpened = false;
    //Check offline status
    $scope.IsOffline = false;
    $scope.isOnlineEnabled = false;
    $scope.IsAddStockRow = false;
    $scope.addStockRowIndex = -1;
    $scope.mode = "NEW";
    $scope.soldOrReturned = 0;
    $scope.remainStock = 0;
    $scope.purchaseImportFromExcel = false;
    $scope.vendorStatus = 1;
    $scope.editModeVendorId = "";
    $scope.showSchemeDisc = true;

    $scope.defaultPurchaseFormula = {
        id: null,
        formulaName: "Default Formula",
        formulaJSON: '[{"formulaTag":"markup","operation":"+","order":1},{"formulaTag":"discount","operation":"-","order":2},{"formulaTag":"schemediscount","operation":"-","order":3},{"formulaTag":"tax","operation":"+","order":4}]'
    };
    $scope.IsDcOrTempItemExist = false;
    $scope.enablePopup = false;
    //$scope.isIGST = true;
    //$scope.isCGST = true;
    //$scope.isSGST = true;
    //$scope.isGstTotal = true;

    var igst = "";
    var cgst = "";
    var sgst = "";
    $scope.invoiceValueSetting = false; // Added by Sarubala on 11/10/18
    $scope.invoiceValueDifference = 0; // Added by Sarubala on 11/10/18


    //  $scope.isRupeesCalRequired = true;
    getOfflineStatus = function () {
        vendorPurchaseService.getOfflineStatus()
            .then(function (response) {
                if (response.data) {
                    $scope.IsOffline = true;
                }
            }, function (error) {
                toastr.error('Error Occured', 'Error');
                console.log(error);
            });
    };
    //
    getOfflineStatus();

    //Added by Sarubala on 18-05-18
    getOnlineEnabledStatus = function () {
        vendorPurchaseService.getOnlineEnableStatus().then(function (response) {
            if (response.data) {
                $scope.isOnlineEnabled = true;
            }
        }, function (error) {
            console.log(error);
        });
    };

    getOnlineEnabledStatus();

    $scope.showPriceMessage = "";
    var vendorPurchase = vendorPurchaseModel;
    var vendorPurchaseItem = vendorPurchaseItemModel;
    vendorPurchaseItem.productStock = productStockModel;
    vendorPurchaseItem.productStock.product = productModel;
    //  $scope.draftvendorPurchase = draftVendorPurchaseModel;
    $scope.product = productModel;
    $scope.vendorPurchase = vendorPurchase;
    $scope.vendorPurchase.paymentType = "Credit";
    $scope.vendorPurchase.noteType = "credit";

    $scope.vendorPurchase.overAllDiscount = 0;
    $scope.vendorPurchaseItem = vendorPurchaseItem;
    //if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
    //    $scope.vendorPurchaseItem.productStock.vAT = 5;
    //}

    $scope.vendorPurchaseItem.tax = 0;
    $scope.vendorPurchaseItem.total = 0;
    $scope.vendorPurchaseItem.freeQty = "";
    $scope.vendorPurchaseItem.discount = "";
    $scope.vendorPurchaseItem.orgDiscount = 0;
    $scope.vendorPurchaseItem.discountValue = 0;
    $scope.vendorPurchaseItem.individualSchemeValue = '';
    $scope.vendorPurchaseItem.isSchemeDiscountValue = false;
    $scope.vendorPurchaseItem.orderedQty = 0;
    $scope.vendorPurchaseItem.vendorOrderId = 0;
    $scope.vendorPurchase.vendorPurchaseItem = [];
    $scope.vendorPurchase.selectedVendor = { "locationType": 1 };
    $scope.selectedProduct = null;
    $scope.Math = window.Math;
    $scope.isAllowDecimal = false;
    $scope.fooDC = false;
    $scope.fooAddProd = false;
    shortcutHelper.setScope($scope);
    $scope.keydown = shortcutHelper.purchaseShortcuts;
    $scope.getCompletePurchaseSettings = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getCompletePurchaseSetting().then(function (response) {
            if (response.data.toString() == "1")
                $scope.completePurchaseKeyType = "ESC";
            else if (response.data.toString() == "2")
                $scope.completePurchaseKeyType = "END";
            else
                $scope.completePurchaseKeyType = "ESC";
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
            $.LoadingOverlay("hide");
        });
    };
    $scope.getCompletePurchaseSettings();
    $scope.discountTypePer = function () {
        angular.element(document.getElementById("txtDiscountPercent")).focus();
    };
    $scope.discountTypeRs = function () {
        angular.element(document.getElementById("txtDiscountRupees")).focus();
    };
    $scope.vendorPurchaseItem.poList = vendorOrderItemModel;
    $scope.minDate = new Date();
    var d = new Date();
    //ChequeMaxDate
    var today = new Date();
    today.setMonth(today.getMonth() + 6);
    $scope.chequeMaxDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    // $scope.HideDraftEditLabel = true;
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        "opened": false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.open3 = function (purchaseItem) {
        purchaseItem.opened = true;
    };
    $scope.popup3 = {
        "opened": false
    };
    $scope.open4 = function () {
        $scope.popup4.opened = true;
    };
    $scope.popup4 = {
        "opened": false
    };
    $scope.dateOptions = {
        "formatYear": 'yy',
        "startingDay": 1
    };
    $scope.draftCount = 0;
    $scope.isFormValid = true;
    $scope.validateInvoiceDate = function () {
        var val = (toDate($scope.vendorPurchaseItems.invoiceDate.$viewValue) < $scope.minDate);
        $scope.vendorPurchaseItems.invoiceDate.$setValidity("InValidInvoiceDate", val);
        $scope.isFormValid = val;
    };
    $scope.validateFreeQty = function () {
        if ($scope.vendorPurchaseItem.packageQty != null && $scope.vendorPurchaseItem.packageQty != undefined) {
            if (!$scope.isAllowDecimal) {
                if (parseFloat($scope.vendorPurchaseItem.packageQty) % 1 != 0) {
                    $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidRoundQty", false);
                } else {
                    $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidRoundQty", true);
                }
            }
        }
        if ($scope.vendorPurchaseItem.freeQty != null && $scope.vendorPurchaseItem.freeQty != undefined && $scope.vendorPurchaseItem.freeQty != "") {
            if (!$scope.isAllowDecimal) {
                if (parseFloat($scope.vendorPurchaseItem.freeQty) % 1 != 0) {
                    $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", false);
                } else {
                    $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", true);
                }
            }
        } else {
            $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", true);
        }
        if ($scope.isAllowDecimal) {
            var qty = ($scope.vendorPurchaseItem.packageQty != null && $scope.vendorPurchaseItem.packageQty != undefined && $scope.vendorPurchaseItem.packageQty != "") ? parseFloat($scope.vendorPurchaseItem.packageQty) : 0;
            var freeqty = ($scope.vendorPurchaseItem.freeQty != null && $scope.vendorPurchaseItem.freeQty != undefined && $scope.vendorPurchaseItem.freeQty != "") ? parseFloat($scope.vendorPurchaseItem.freeQty) : 0;
            var tot = (qty + freeqty) % 1;
            if (tot !== 0) {
                $scope.vendorPurchaseItems.freeQty.$setValidity("InvalidSumQty", false);
            } else {
                $scope.vendorPurchaseItems.freeQty.$setValidity("InvalidSumQty", true);
            }
        }
        $scope.vendorPurchaseItems.freeQty.$setValidity("InValidfreeQty", !((parseFloat($scope.vendorPurchaseItem.packageQty) || 0) == 0 && (parseFloat($scope.vendorPurchaseItem.freeQty) || 0) == 0));
        if (!$scope.vendorPurchaseItem.vendorReturn) {
            $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidfreeQty", ((parseFloat($scope.vendorPurchaseItem.packageQty || 0) + parseFloat($scope.vendorPurchaseItem.freeQty || 0)) >= $scope.soldOrReturned));
        }
    };
    function toDate(dateStr) {
        if (dateStr != null) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
    }

    //Added by Sarubala on 24-05-18
    $scope.showSchemeDiscount = function (val) {
        if (val == 'percent') {
            $scope.showSchemeDisc = true;
            $scope.vendorPurchaseItem.isSchemeDiscountValue = false;
        } else {
            $scope.showSchemeDisc = false;
            $scope.vendorPurchaseItem.isSchemeDiscountValue = true;
        }
        //$scope.vendorPurchaseItem.schemeDiscountPerc = '';
        //$scope.vendorPurchaseItem.schemeDiscountValue = '';
    };

    // Added by Sarubala on 11/10/18 - start
    $scope.getInvoiceValueSetting = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getInvoiceValueSettings().then(function (response) {
            $scope.invoiceValueSetting = response.data.invoiceValueSetting == true ? response.data.invoiceValueSetting : false;
            $scope.invoiceValueDifference = response.data.invoiceValueDifference >= 0 ? response.data.invoiceValueDifference : 0;
            $.LoadingOverlay("hide");

        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    };

    $scope.getInvoiceValueSetting();
    // Added by Sarubala on 11/10/18 - end

    $scope.PopupAddDC = function () {
        $scope.AnyPopupOpened = true;
        var m = ModalService.showModal({
            "controller": "addDcCtrl",
            "templateUrl": "AddDC",
            "inputs": {
                "selectedVendor": $scope.vendorPurchase.selectedVendor,
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.AnyPopupOpened = false;
                //$scope.vendorPurchase.selectedVendor = {};
                //$scope.init();
               $scope.vendorPurchase.selectedVendor.id= '';
                var ele = document.getElementById("initialValue");
                ele.focus();
            });
        });
        //return false;
    };

    $scope.PopupProductReturn = function () {
        if (!IsSelectedVendor()) {
            ShowConfirmMsgWindow("Please select Vendor Name and try again.");
            $("#vendor").trigger('chosen:open');
            return;
        }
        $scope.returnPopup = true;
        var m = ModalService.showModal({
            "controller": "purchaseProductReturnCtrl",
            "templateUrl": "productReturn",
            "inputs": {
                "vendorPurchaseItem": $scope.vendorPurchase.vendorPurchaseItem,
                "selectedVendor": $scope.vendorPurchase.selectedVendor,
                //"vendorPurchaseReturn": $scope.selectedProduct.vendorPurchaseReturn,
                "GSTEnabled": $scope.GSTEnabled,
                "taxValuesList": $scope.allTaxValuesList

            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.returnItems = result;
                $scope.returnPopup = false;
                if ($scope.returnItems != null) {
                    $scope.addReturnItems();
                }
                document.getElementById("initialValue").focus();
            });
            //modal.close.then(function (result) {
            //    $scope.returnPopup = false;
            //    var ele = document.getElementById("initialValue");
            //    ele.focus();
            //});
        });
        //return false;
    };

    $scope.addReturnItems = function () {
        if ($scope.returnItems.length > 0) {
            //  $scope.returnDiscountInValid = true;

            for (var x = 0; x < $scope.returnItems.length; x++) {
                if ($scope.returnItems[x].editId == null || $scope.returnItems[x].editId == undefined) {
                    $scope.returnItems[x].editId = $scope.editId++;
                    $scope.vendorPurchase.vendorPurchaseItem.push($scope.returnItems[x]);

                }
            }

            //var returntotal = 0;
            //$scope.vendorPurchase.tax = 0;
            //for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

            //    if ($scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn)
            //    {
            //        returntotal += (parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) * parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice)) - parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].discountValue);

            //        $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;
            //        $scope.vendorPurchase.vendorPurchaseItem[i].ReturnPurchasePrice = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) * parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice);
            //    }
            //}

            //$scope.returnAmount = parseFloat(returntotal) + parseFloat($scope.vendorPurchase.tax);
            //$scope.returnAmount = Math.round($scope.returnAmount);
            //$scope.vendorPurchase.vendorPurchaseItem.ReturnedTotal = parseFloat($scope.returnAmount);

            //setReturnAmt();
            //setVat();
            $scope.ReCalCulateAllValues();

            $scope.EditMode = false;
            $scope.dcItemEdit = false;
            $scope.poItemEdit = false;
            $scope.returnItemEdit = false;
        }
        else {
            //  $scope.returnDiscountInValid = false;
        }
        $scope.returnItems = null;
    };

    $scope.showTempStockScreen = function (productId) {
        $scope.AnyPopupOpened = true;
        var m = ModalService.showModal({
            "controller": "tempPurchaseItemCtrl",
            "templateUrl": "tempPurchaseItem",
            "inputs": {
                "productId": productId,
                "GSTEnabled": $scope.GSTEnabled,
                "purchaseMode": $scope.mode,
                "taxRefNo": $scope.vendorPurchase.taxRefNo,
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.AnyPopupOpened = false;
                if (result.status == "Yes") {
                    $scope.tempStockLoaded = true;
                    tempItemSelection(result.data);
                }
            });
        });
    };
    function tempItemSelection(tempStockValues) {
        fillTempData(tempStockValues);
    }
    $scope.showDCItems = function () {
        var dclist1 = $scope.vendorPurchaseItem.dcList;
        var poList = $scope.vendorPurchaseItem.poList;
        for (var j = 0; j < dclist1.length; j++) {

            if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
                dclist1[j].productStock.vAT = dclist1[j].productStock.vat;
            }



            if ($scope.vendorPurchase.vendorPurchaseItem != null && $scope.vendorPurchase.vendorPurchaseItem.length > 0) {
                for (var k = 0; k < $scope.vendorPurchase.vendorPurchaseItem.length; k++) {
                    if ($scope.vendorPurchase.vendorPurchaseItem[k].fromDcId != undefined && dclist1[j].length > 0) {
                        if ($scope.vendorPurchase.vendorPurchaseItem[k].fromDcId == dclist1[j].id) {
                            dclist1.splice(j, 1);
                        }
                    }
                }
            }
        }
        //
        if (dclist1.length > 0 || poList.length > 0) {
            $scope.AnyPopupOpened = true;
            var m = ModalService.showModal({
                "controller": "showDcItemsCtrl",
                "templateUrl": "showDC",
                "inputs": {
                    "DCPOValue": [{ "dcList1": dclist1 }, { "poList": poList }, { "enableSelling": $scope.enableSelling }],
                    "GSTEnabled": $scope.GSTEnabled
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.AnyPopupOpened = false;
                    if (result.status == "Yes") {
                        getSelectedDcItem(result.data);
                        $scope.disableAddNewDc = true;
                    } else {
                        var prod = document.getElementById("invoiceNo");
                        prod.focus();
                    }
                });
            });
            return false;
        } else {
            $scope.onVendorSelect();
        }
    };
    function getSelectedDcItem(DcPoItem) {
        var dcItem1 = DcPoItem.DCItem;
        var poItem = DcPoItem.POItem;
        if (dcItem1 == undefined || dcItem1 == null || dcItem1 == "") {
            dcItem1 = [];
        }
        if (poItem == undefined || poItem == null || poItem == "") {
            poItem = [];
        }
        for (var j = 0; j < dcItem1.length; j++) {
            if (dcItem1[j].isSelected == true) {


                $scope.vendorPurchaseItem.isPoItem = false;
                $scope.vendorPurchaseItem.productStock = dcItem1[j].productStock;
                $scope.vendorPurchaseItem.productStockId = dcItem1[j].productStockId;
                $scope.vendorPurchaseItem.isEdit = false;
                $scope.vendorPurchase.invoiceNo = null;
                $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;
                $scope.vendorPurchaseItem.packageQty = dcItem1[j].packageQty;
                $scope.vendorPurchaseItem.freeQty = dcItem1[j].freeQty || 0;
                $scope.vendorPurchaseItem.discount = dcItem1[j].discount || 0;
                $scope.vendorPurchaseItem.packageSize = dcItem1[j].packageSize;
                $scope.vendorPurchaseItem.packagePurchasePrice = dcItem1[j].packagePurchasePrice;
                $scope.vendorPurchaseItem.packageSellingPrice = dcItem1[j].packageSellingPrice;
                $scope.vendorPurchaseItem.packageMRP = dcItem1[j].packageMRP;
                $scope.vendorPurchaseItem.fromDcId = dcItem1[j].id;
                $scope.vendorPurchaseItem.discountValue = 0;
                $scope.selectedProduct = dcItem1[j].productStock.product;
                $scope.vendorPurchaseItem.orgDiscount = $scope.vendorPurchaseItem.discount;
                $scope.vendorPurchaseItem.productName = dcItem1[j].productStock.product.name;


                $scope.vendorPurchaseItem.discount = Number($scope.vendorPurchaseItem.discount) + Number($scope.vendorPurchase.discount);


                //if ($scope.vendorPurchaseItem.discount > 0) {
                //    $scope.vendorPurchaseItem.discountValue = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) * ($scope.vendorPurchaseItem.discount / 100);
                //}
                //$scope.vendorPurchaseItem.total = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) - $scope.vendorPurchaseItem.discountValue;

                //if ($scope.GSTEnabled) {
                //    $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.gstTotal / 100);
                //} else {
                //    if ($scope.vendorPurchase.TaxationType == "VAT") {
                //        $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.vAT / 100);
                //    }
                //    if ($scope.vendorPurchase.TaxationType == "CST") {
                //        $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.cST / 100);
                //    }
                //}



                $scope.vendorPurchaseItem.orderedQty = $scope.vendorPurchaseItem.packageQty || 0;


                if ($scope.vendorPurchaseItem.freeQty > 0) {
                    $scope.vendorPurchaseItem.packageQty = Number($scope.vendorPurchaseItem.packageQty) + Number($scope.vendorPurchaseItem.freeQty);
                }

                $scope.vendorPurchaseItem.quantity = $scope.vendorPurchaseItem.packageSize * $scope.vendorPurchaseItem.packageQty;


                //if (!$scope.GSTEnabled) {
                //    var _isCST = false;
                //    if ($scope.vendorPurchase.TaxationType == "VAT") {
                //        $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.vAT;
                //        $scope.vendorPurchaseItem.productStock.cST = 0;
                //        var cst = document.getElementById("cST");
                //        cst.disabled = true;
                //    }
                //    var vat;
                //    if ($scope.vendorPurchase.TaxationType == "CST") {
                //        $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.cST;
                //        $scope.vendorPurchase.TaxationType = "CST";
                //        _isCST = true;
                //    }
                //}

                if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
                    if ($scope.vendorPurchase.TaxationType == "VAT") {
                        $scope.vendorPurchaseItem.productStock.vAT = Number($scope.vendorPurchaseItem.productStock.vAT) || 0;
                        $scope.vendorPurchaseItem.productStock.cST = 0;
                    }
                    else if ($scope.vendorPurchase.TaxationType == "CST") {
                        $scope.vendorPurchaseItem.productStock.vAT = 0;
                        $scope.vendorPurchaseItem.productStock.cST = Number($scope.vendorPurchaseItem.productStock.cST) || 0;
                    }
                }

                $scope.vendorPurchaseItem.selectedProduct = $scope.selectedProduct;
                if (!$scope.EditMode) {
                    $scope.vendorPurchaseItem.previouspurchases = $scope.previouspurchases;
                    $scope.vendorPurchase.vendorPurchaseItem.push($scope.vendorPurchaseItem);
                    //if (window.localStorage.getItem("p_data") == null) {
                    //    window.localStorage.setItem("p_data", JSON.stringify($scope.vendorPurchase));
                    //    $scope.draftCount = 1;
                    //} else {
                    //    $scope.tempvendorPurchase = JSON.parse(window.localStorage.getItem("p_data"));
                    //    $scope.tempvendorPurchase.vendorPurchaseItem.push($scope.vendorPurchaseItem);
                    //    window.localStorage.setItem("p_data", JSON.stringify($scope.tempvendorPurchase));
                    //    $scope.draftCount = 1;
                    //}
                } else {
                    $scope.vendorPurchase.vendorPurchaseItem[$scope.copyedIndex] = $scope.vendorPurchaseItem;
                    $scope.EditMode = false;
                }
                $scope.vendorPurchaseItems.$setPristine();
                $scope.vendorPurchaseItem = { productStock: { product: {} } };
                $scope.selectedProduct = null;
                //setTotal("");
                //setVat();

                //if (!$scope.GSTEnabled) {
                //    (_isCST) ? $scope.vendorPurchaseItem.productStock.cST = 2 : $scope.vendorPurchaseItem.productStock.vAT = 5;
                //}


                if ($scope.vendorPurchase.selectedVendor.discount) {
                    $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
                } else {
                    $scope.vendorPurchaseItem.discount = 0;
                }
                //$scope.initialValue();
                //getIsAllowDecimalSettings();
                var qty = document.getElementById("invoiceNo");
                qty.focus();
            }
        }
        for (var j = 0; j < poItem.length; j++) {
            if (poItem[j].isSelected == true) {
                $scope.vendorPurchaseItem.isPoItem = true;
                $scope.selectedProduct = poItem[j].product;
                $scope.vendorPurchaseItem.productStock = poItem[j].productStock;
                $scope.vendorPurchaseItem.productStock.product = poItem[j].product;
                $scope.vendorPurchaseItem.isEdit = false;
                $scope.vendorPurchase.invoiceNo = null;
                $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id; //poItem[j].vendorId;
                $scope.vendorPurchaseItem.packageSize = poItem[j].packageSize || 0;
                $scope.vendorPurchaseItem.packageQty = poItem[j].packageQty || 0;
                $scope.vendorPurchaseItem.freeQty = poItem[j].freeQty || 0;
                $scope.vendorPurchaseItem.discount = poItem[j].discount || 0;
                $scope.vendorPurchaseItem.discountValue = 0;
                //Following lines commented by Settu on 04/04/2017
                /*  var value = document.getElementById("hdnCustomOrder").value;
                  if (value === "1") {
                      $scope.vendorPurchaseItem.packagePurchasePrice = poItem[j].packageSellingPrice - (poItem[j].packageSellingPrice * 0.05);
                  } else {
                      $scope.vendorPurchaseItem.packagePurchasePrice = poItem[j].purchasePrice || 0;
                  }
          */
                $scope.vendorPurchaseItem.packagePurchasePrice = poItem[j].purchasePrice || 0;
                $scope.vendorPurchaseItem.productStock.vAT = poItem[j].product.vat;
                $scope.vendorPurchaseItem.productStock.vendorId = $scope.vendorPurchase.selectedVendor.id;
                $scope.vendorPurchaseItem.packageSellingPrice = poItem[j].packageSellingPrice;
                $scope.vendorPurchaseItem.packageMRP = poItem[j].packageSellingPrice;
                $scope.vendorPurchaseItem.vendorOrderId = poItem[j].vendorOrderId;
                $scope.vendorPurchaseItem.vendorOrderItemId = poItem[j].id;
                $scope.vendorPurchaseItem.orgDiscount = 0;
                //if ($scope.vendorPurchaseItem.discount > 0)
                //    $scope.vendorPurchaseItem.discountValue = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) * ($scope.vendorPurchaseItem.discount / 100);
                //$scope.vendorPurchaseItem.total = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) - $scope.vendorPurchaseItem.discountValue;

                //if ($scope.GSTEnabled) {
                //    $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.gstTotal / 100);
                //} else {
                //    var _isCST = false;
                //    if ($scope.vendorPurchase.TaxationType == "VAT") {
                //        $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.vAT / 100);
                //        $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.vAT;
                //        $scope.vendorPurchaseItem.productStock.cST = 0;
                //        var cst = document.getElementById("cST");
                //        cst.disabled = true;
                //    }
                //    var vat;
                //    if ($scope.vendorPurchase.TaxationType == "CST") {
                //        $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.cST / 100);

                //        $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.cST;
                //        $scope.vendorPurchase.TaxationType = "CST";
                //        _isCST = true;
                //    }
                //}

                if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
                    if ($scope.vendorPurchase.TaxationType == "VAT") {
                        $scope.vendorPurchaseItem.productStock.vAT = Number($scope.vendorPurchaseItem.productStock.vAT) || 0;
                        $scope.vendorPurchaseItem.productStock.cST = 0;
                    }
                    else if ($scope.vendorPurchase.TaxationType == "CST") {
                        $scope.vendorPurchaseItem.productStock.vAT = 0;
                        $scope.vendorPurchaseItem.productStock.cST = Number($scope.vendorPurchaseItem.productStock.cST) || 0;
                    }
                }

                $scope.vendorPurchaseItem.orderedQty = $scope.vendorPurchaseItem.packageQty || 0;
                if ($scope.vendorPurchaseItem.freeQty > 0) {
                    $scope.vendorPurchaseItem.packageQty = Number($scope.vendorPurchaseItem.packageQty) + Number($scope.vendorPurchaseItem.freeQty);
                }



                $scope.vendorPurchaseItem.selectedProduct = $scope.selectedProduct;
                //if ($scope.SaveTaxSeries == 2) {
                //    $scope.vendorPurchaseItem.productStock.TaxType = $scope.SelectedTaxtype;
                //}
                if ($scope.vendorPurchase.vendorPurchaseItem.length != 0) {
                    $scope.disableAddNewDc = true;
                }
                $scope.vendorPurchaseItem.isValid = false;
                if (!$scope.EditMode) {
                    $scope.vendorPurchaseItem.previouspurchases = $scope.previouspurchases;
                    if (!$scope.vendorPurchaseItem.hasOwnProperty('tempItem')) {
                        $scope.vendorPurchaseItem.tempItem = false;
                    }
                    $scope.vendorPurchase.vendorPurchaseItem.push($scope.vendorPurchaseItem);
                    if ($scope.vendorPurchase.vendorPurchaseItem.length != 0) {
                        $scope.disableAddNewDc = true;
                    }
                    $scope.nonTempItemCount = $filter("filter")($scope.vendorPurchase.vendorPurchaseItem, { "tempItem": false }).length;
                    $scope.TempItemCount = $filter("filter")($scope.vendorPurchase.vendorPurchaseItem, { "tempItem": true }).length;
                } else {
                    $scope.vendorPurchase.vendorPurchaseItem[$scope.copyedIndex] = $scope.vendorPurchaseItem;
                    $scope.EditMode = false;
                    $scope.dcItemEdit = false;
                }
                $scope.vendorPurchaseItems.$setPristine();
                $scope.vendorPurchaseItem = { productStock: { product: {} } };
                $scope.selectedProduct = null;
                //setTotal("");
                //setVat();
                //saveDraft();

                //if (!$scope.GSTEnabled) {
                //    (_isCST) ? $scope.vendorPurchaseItem.productStock.cST = 2 : $scope.vendorPurchaseItem.productStock.vAT = 5;
                //}

                if ($scope.vendorPurchase.selectedVendor.discount) {
                    $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
                } else {
                    $scope.vendorPurchaseItem.discount = 0;
                }
                //$scope.initialValue();
                //getIsAllowDecimalSettings();
                var qty = document.getElementById("invoiceNo");
                qty.focus();
            }
        }
        $.LoadingOverlay("hide");

        $scope.ReCalCulateAllValues();

    }

    $scope.updateUI = function () {


        if (($scope.GSTEnabled && $scope.mode == "NEW") || ($scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {

            var type = $scope.vendorPurchase.selectedVendor.locationType;
            value = $scope.vendorPurchaseItem.productStock.gstTotal || 0;
            $scope.vendorPurchaseItem.productStock.igst = "";
            $scope.vendorPurchaseItem.productStock.cgst = "";
            $scope.vendorPurchaseItem.productStock.sgst = "";
            //Local
            if (type == 0 || type == 1 || type == "" || type == null || type == undefined) {
                $scope.vendorPurchaseItem.productStock.cgst = (value / 2).toFixed(2);
                $scope.vendorPurchaseItem.productStock.sgst = (value / 2).toFixed(2);
            }
            //InterState
            if (type == 2) {
                $scope.vendorPurchaseItem.productStock.igst = value;
                $scope.vendorPurchaseItem.productStock.cgst = "";
                $scope.vendorPurchaseItem.productStock.sgst = "";
            }

            //$scope.vendorPurchaseItem.productStock.igst = "";
            //$scope.vendorPurchaseItem.productStock.cgst = "";
            //$scope.vendorPurchaseItem.productStock.sgst = "";
            //$scope.vendorPurchaseItem.productStock.gstTotal = "";
            //if ($scope.vendorPurchase.selectedVendor.locationType == 0 || $scope.vendorPurchase.selectedVendor.locationType == 1) {
            //    $scope.isCGST = false;
            //    $scope.isSGST = false;
            //    $scope.isIGST = false;
            //    $scope.isGstTotal = true;
            //} else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
            //    $scope.isCGST = false;
            //    $scope.isSGST = false;
            //    $scope.isIGST = false;
            //    $scope.isGstTotal = true;
            //} else {
            //    $scope.isIGST = false;
            //    $scope.isCGST = false;
            //    $scope.isSGST = false;
            //    $scope.isGstTotal = false;
            //}

        } else {

            //var cst = document.getElementById("cST");
            //var vat = document.getElementById("vat");
            $scope.vendorPurchaseItem.productStock.vAT = 0;
            $scope.vendorPurchaseItem.productStock.cST = 0;
            if ($scope.vendorPurchase.selectedVendor.enableCST) {
                //vat.disabled = false;
                //cst.disabled = false;
                $scope.vendorPurchase.TaxationType = "CST";
                //$scope.vendorPurchaseItem.productStock.cST = 2;
            } else {
                //vat.disabled = false;
                //cst.disabled = true;
                $scope.vendorPurchase.TaxationType = "VAT";
                //$scope.vendorPurchaseItem.productStock.vAT = 5;
            }
        }








        if ($scope.vendorPurchase.selectedVendor.discount) {
            $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
        } else {
            $scope.vendorPurchaseItem.discount = 0;
        }
        if ($scope.vendorPurchase.selectedVendor.paymentType != undefined && $scope.vendorPurchase.selectedVendor.paymentType != null) {
            $scope.vendorPurchase.paymentType = $scope.vendorPurchase.selectedVendor.paymentType;
            if ($scope.vendorPurchase.paymentType == "Credit" && $scope.vendorPurchase.selectedVendor.creditNoOfDays != undefined) {
                $scope.vendorPurchase.creditNoOfDays = $scope.vendorPurchase.selectedVendor.creditNoOfDays;
            } else {
                $scope.vendorPurchase.creditNoOfDays = null;
            }
        } else {
            $scope.vendorPurchase.paymentType = "Credit";
        }
        if ($scope.vendorPurchaseItem.discount == 0) {
            $scope.vendorPurchaseItem.discount = "";
        }
        //if ($scope.vendorPurchase.selectedVendor.id != null && !$scope.draftLoaded) {
        if ($scope.vendorPurchase.selectedVendor.id != null) {
            $.LoadingOverlay("show");
            vendorPurchaseService.getDCItems($scope.vendorPurchase.selectedVendor.id).then(function (response) {
                $scope.vendorPurchaseItem.dcList = response.data;
                vendorOrderService.getVendorOrder($scope.vendorPurchase.selectedVendor.id, true, null).then(function (response) {
                    $scope.vendorPurchaseItem.poList = response.data;
                    //Added by Settu
                    var value = document.getElementById("hdnCustomOrder").value;
                    if (value === "1") {
                        for (var k = 0; k < $scope.vendorPurchaseItem.poList.length; k++) {
                            $scope.vendorPurchaseItem.poList[k].packagePurchasePrice = parseFloat($scope.currency($scope.vendorPurchaseItem.poList[k].packageSellingPrice - ($scope.vendorPurchaseItem.poList[k].packageSellingPrice * 0.05)));
                            $scope.vendorPurchaseItem.poList[k].purchasePrice = $scope.vendorPurchaseItem.poList[k].packagePurchasePrice;
                        }
                    }
                    //End here
                    if ($scope.vendorPurchaseItem.dcList.length > 0 || $scope.vendorPurchaseItem.poList.length > 0) {
                        $.LoadingOverlay("hide");
                        $scope.showDCItems();
                    } else {
                        $.LoadingOverlay("hide");
                        $scope.onVendorSelect();
                    }
                }, function (error1) {
                    console.log(error1);
                    toastr.error('Error Occured', 'Error');
                });
            }, function (error) {
                console.log(error);
                $.LoadingOverlay("hide");
                toastr.error("Error Occured", "");
            });
        } else {
            $scope.onVendorSelect();
        }
        if ($scope.mode == "EDIT" && ($scope.vendorPurchase.vendor.vendorPurchaseFormula.id != $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.id || $scope.vendorPurchase.vendor.locationType != $scope.vendorPurchase.selectedVendor.locationType)) {
            for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                    if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                        if ($scope.vendorPurchase.vendorPurchaseItem[i].id == undefined || $scope.vendorPurchase.vendorPurchaseItem[i].id == "" || $scope.vendorPurchase.vendorPurchaseItem[i].id == null) {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "I";
                        }
                        else if ($scope.vendorPurchase.vendorPurchaseItem[i].id != "") {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                        }
                    }
                }
            }
        }
        $scope.ReCalCulateAllValues();
    };
    $scope.EditMode = false;
    $scope.dcItemEdit = false;
    $scope.poItemEdit = false;
    $scope.returnItemEdit = false;
    $scope.Action = "Add";
    $scope.ResetAction = "Reset";
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    function ResetAmounts() {
        $scope.vendorPurchase.noteType = "credit";
        $scope.roundedAmount = 0;
        $scope.purchaseAmount = 0;
        $scope.returnAmount = 0;
        $scope.returnRoundedAmount = 0;
        $scope.vendorPurchase.grossValue = 0;
        $scope.vendorPurchase.discountInRupees = "";
        $scope.vendorPurchase.discount = "";
        $scope.errorNoteAmount = false;
        $scope.errorExcessPercent = false;
        $scope.errorPercentAmount = false;
        $scope.errorExcessMarkupPercent = false;
        $scope.vendorPurchase.discountType = 'percent';
    }
    ResetAmounts();
    if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
        $scope.vendorPurchase.TaxationType = "VAT";
    }

    var d = new Date();
    $scope.vendorPurchase.invoiceDate = d.toISOString();
    $scope.vendorPurchase.total = 0;
    $scope.vendorPurchase.discount = "";
    $scope.vendorPurchase.credit = 0;
    $scope.list = [];
    $scope.editItems = 0;
    $scope.copyedIndex = {};

    function GetTotalPrice() {
        var amount = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    amount += $scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty;
                }
            }
        }
        return amount;
    }


    $scope.calDiscountInRupees = function (type) {


        //$scope.vendorPurchase.noteAmount = "";
        if ($scope.vendorPurchase.discountInRupees != "") {
            if ($scope.vendorPurchase.discountInRupees > $scope.vendorPurchase.tempTotal) {
                $scope.errorPercentAmount = true;
                return false;
            } else {
                $scope.errorPercentAmount = false;
            }
            var discount = $scope.vendorPurchase.discountInRupees * 100 / GetTotalPrice();
        } else {
            $scope.vendorPurchase.discount = 0;
            $scope.errorPercentAmount = false;
            discount = 0;
        }
        //$scope.vendorPurchase.overAllDiscount = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {

                    if ($scope.mode == "EDIT" && $scope.vendorPurchase.vendorPurchaseItem[i].discount != Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number($scope.vendorPurchase.discount)) {
                        if ($scope.vendorPurchase.vendorPurchaseItem[i].id == undefined || $scope.vendorPurchase.vendorPurchaseItem[i].id == "" || $scope.vendorPurchase.vendorPurchaseItem[i].id == null) {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "I";
                        }
                        else if ($scope.vendorPurchase.vendorPurchaseItem[i].id != "") {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                        }
                    }

                    $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number(discount);
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].discount > 99.99) {
                        $scope.errorExcessPercent = true;
                    } else {
                        $scope.errorExcessPercent = false;
                    }


                    //var qty = ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty || 0) - ($scope.vendorPurchase.vendorPurchaseItem[i].freeQty || 0);



                    //$scope.vendorPurchase.vendorPurchaseItem[i].discountValue = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);

                    //$scope.vendorPurchase.overAllDiscount += $scope.vendorPurchase.vendorPurchaseItem[i].discountValue;

                    //$scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountValue;


                    //if (type == "ReCalculation") {

                    //    if ($scope.GSTEnabled) {

                    //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);

                    //    } else {
                    //        if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT > 0) {
                    //            $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                    //        } else {
                    //            $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST / 100);
                    //        }
                    //    }




                    //    } else {
                    //        if ($scope.GSTEnabled) {
                    //            $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);
                    //        } else {
                    //            if ($scope.vendorPurchaseItem.productStock.vAT > 0) {
                    //                $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                    //            } else {
                    //                $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST / 100);
                    //            }
                    //        }
                    //    }
                }
            }

        }

        if ($scope.vendorPurchase.discountInRupees == "") {
            $scope.vendorPurchase.discount = "";
        } else {
            $scope.vendorPurchase.discount = discount;
        }
        //setTotal("");
        //setVat();
        $scope.applyVendorPurchaseFormula();

    };
    $scope.calDiscountOnBill = function (type) {
        // $scope.vendorPurchase.noteAmount = "";
        if ($scope.vendorPurchase.discount == 0 || $scope.vendorPurchase.discount == "0") {
            $scope.vendorPurchase.discountInRupees = "";
        }
        if ($scope.vendorPurchase.discount > 99.99) {
            $scope.errorExcessPercent = true;
            return;
        } else {
            $scope.errorExcessPercent = false;
        }

        //$scope.vendorPurchase.total = 0;
        //$scope.vendorPurchase.overAllDiscount = 0;

        //for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
        //    if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
        //        $scope.vendorPurchase.total += parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice);
        //    }
        //}

        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {

                    if ($scope.mode == "EDIT" && $scope.vendorPurchase.vendorPurchaseItem[i].discount != Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number($scope.vendorPurchase.discount)) {
                        if ($scope.vendorPurchase.vendorPurchaseItem[i].id == undefined || $scope.vendorPurchase.vendorPurchaseItem[i].id == "" || $scope.vendorPurchase.vendorPurchaseItem[i].id == null) {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "I";
                        }
                        else if ($scope.vendorPurchase.vendorPurchaseItem[i].id != "") {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                        }
                    }

                    $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number($scope.vendorPurchase.discount);
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].discount > 99.99) {
                        $scope.errorExcessPercent = true;
                        return false;
                    } else {
                        $scope.errorExcessPercent = false;
                    }
                    //var qty = ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty || 0) - ($scope.vendorPurchase.vendorPurchaseItem[i].freeQty || 0);


                    //$scope.vendorPurchase.vendorPurchaseItem[i].discountValue = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);


                    //$scope.vendorPurchase.overAllDiscount += $scope.vendorPurchase.vendorPurchaseItem[i].discountValue;



                    //$scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountValue;






                    //if (type == "ReCalculation") {


                    //    if ($scope.GSTEnabled) {
                    //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);
                    //    } else {
                    //        if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT > 0) {
                    //            $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                    //        } else {
                    //            $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST / 100);
                    //        }
                    //    }




                    //    } else {
                    //        if ($scope.GSTEnabled) {
                    //            $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);
                    //        } else {
                    //            if ($scope.vendorPurchaseItem.productStock.vAT > 0) {
                    //                $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                    //            } else {
                    //                $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST / 100);
                    //            }
                    //        }
                    //    }
                }
            }
        }
        if ($scope.vendorPurchase.discount == "") {
            $scope.vendorPurchase.discountInRupees = "";
        } else {
            $scope.vendorPurchase.discountInRupees = GetTotalPrice() * $scope.vendorPurchase.discount / 100;
        }


        //setTotal("");
        //setVat();
        $scope.applyVendorPurchaseFormula();

    };

    function setTotal(type) {
        //$scope.vendorPurchase.total = 0;
        //$scope.vendorPurchase.overAllDiscount = 0;
        //for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
        //    if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
        //        $scope.vendorPurchase.total += $scope.vendorPurchase.vendorPurchaseItem[i].total;
        //        $scope.vendorPurchase.overAllDiscount += $scope.vendorPurchase.vendorPurchaseItem[i].discountValue;
        //    }
        //}
        // $scope.purchaseAmount = parseFloat($scope.vendorPurchase.total) - parseFloat($scope.vendorPurchase.overAllDiscount);
        if (type == "NewStock") {
            $scope.vendorPurchase.tempTotal = $scope.vendorPurchase.total;
        }
        //setTotaldiscount();

    }
    //function setTotaldiscount() {
    //    $scope.vendorPurchase.discountOnBill = 0;
    //    for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
    //        //if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
    //            var total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty));
    //            $scope.vendorPurchase.discountOnBill += total - (total * ($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount / 100));
    //        //}
    //    }
    //    $scope.vendorPurchase.discountOnBill = $scope.vendorPurchase.discountOnBill * ($scope.vendorPurchase.discount / 100);
    //}

    function setReturnAmt() {

        var returntotal = 0;
        //$scope.vendorPurchase.tax = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if ($scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    //returntotal += (parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) * parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice)) - parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].discountValue);

                    //$scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;
                    returntotal += $scope.vendorPurchase.vendorPurchaseItem[i].netTotal;
                    $scope.vendorPurchase.vendorPurchaseItem[i].ReturnPurchasePrice = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) * parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice);
                    // Added by Gavaskar 05-09-2017 Discount,Discountvalue, Tax Amount
                    $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount) || 0;
                    $scope.vendorPurchase.vendorPurchaseItem[i].discountValue = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].discountValue) || 0;
                    $scope.vendorPurchase.vendorPurchaseItem[i].gstAmount = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].tax) || 0;
                }
            }
        }

        // $scope.returnAmount = parseFloat(returntotal) + parseFloat($scope.vendorPurchase.tax); // Comment by Gavaskar 05-09-2017
        //$scope.returnAmount = parseFloat(returntotal) + parseFloat($scope.vendorPurchase.tax);
        $scope.returnAmount = parseFloat(returntotal);

        var afterRoundUp = Math.round($scope.returnAmount);
        $scope.returnRoundedAmount = afterRoundUp - $scope.returnAmount;
        $scope.returnAmount = Math.round($scope.returnAmount);
        $scope.vendorPurchase.vendorPurchaseItem.returnedTotal = parseFloat($scope.returnAmount);
        $scope.vendorPurchase.returnRoundedValue = $scope.returnRoundedAmount;

    }

    function setVat() {
        //$scope.vendorPurchase.tax = 0;
        //$scope.vendorPurchase.igst = 0;
        //$scope.vendorPurchase.cgst = 0;
        //$scope.vendorPurchase.sgst = 0;
        //for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
        //    if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
        //        $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;
        //        $scope.vendorPurchase.igst += $scope.vendorPurchase.vendorPurchaseItem[i].igst;
        //        $scope.vendorPurchase.cgst += $scope.vendorPurchase.vendorPurchaseItem[i].cgst;
        //        $scope.vendorPurchase.sgst += $scope.vendorPurchase.vendorPurchaseItem[i].sgst;
        //    }

        //}

        //Footer tax values
        if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == null || $scope.vendorPurchase.selectedVendor.locationType == undefined || $scope.vendorPurchase.selectedVendor.locationType == "") {
            $scope.vendorPurchase.cgst = $scope.vendorPurchase.tax / 2;
            $scope.vendorPurchase.sgst = $scope.vendorPurchase.tax / 2;
            $scope.vendorPurchase.igst = 0;
        }
        else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
            $scope.vendorPurchase.cgst = 0;
            $scope.vendorPurchase.sgst = 0;
            $scope.vendorPurchase.igst = $scope.vendorPurchase.tax;
        }
        else {
            $scope.vendorPurchase.cgst = 0;
            $scope.vendorPurchase.sgst = 0;
            $scope.vendorPurchase.igst = 0;
        }

        if ($scope.returnAmount == undefined || $scope.returnAmount == null || $scope.returnAmount == "") {
            $scope.returnAmount = 0;
        }
        //var noteamount = parseFloat($scope.vendorPurchase.noteAmount) || 0;
        //if ($scope.vendorPurchase.noteType == "credit") {
        //    //$scope.purchaseAmount = parseFloat($scope.vendorPurchase.total) + parseFloat($scope.vendorPurchase.tax) - noteamount;
        //    $scope.purchaseAmount = parseFloat($scope.vendorPurchase.netTotal) - noteamount;
        //    $scope.purchaseAmount = Math.round($scope.purchaseAmount);
        //}
        //if ($scope.vendorPurchase.noteType == "debit") {
        //    //$scope.purchaseAmount = parseFloat($scope.vendorPurchase.total) + parseFloat($scope.vendorPurchase.tax) + noteamount;
        //    $scope.purchaseAmount = parseFloat($scope.vendorPurchase.netTotal) + noteamount;
        //    $scope.purchaseAmount = Math.round($scope.purchaseAmount);
        //}


        //var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax - $scope.returnAmount;
        var total = $scope.vendorPurchase.netTotal - $scope.returnAmount;

        //if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == null) {
        //    $scope.vendorPurchaseItem.productStock.igst = 0;
        //    $scope.vendorPurchase.cgst = $scope.vendorPurchase.tax / 2;
        //    $scope.vendorPurchase.sgst = $scope.vendorPurchase.tax / 2;
        //}
        //else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
        //    $scope.vendorPurchaseItem.productStock.cgst = 0;
        //    $scope.vendorPurchaseItem.productStock.sgst = 0;
        //    $scope.vendorPurchase.igst = $scope.vendorPurchase.tax;
        //}
        //else {
        //    $scope.vendorPurchaseItem.productStock.gstTotal = 0;
        //    $scope.vendorPurchaseItem.productStock.igst = 0;
        //    $scope.vendorPurchaseItem.productStock.cgst = 0;
        //    $scope.vendorPurchaseItem.productStock.sgst = 0;
        //}


        roundingTotal(total);
    }
    function roundingTotal(Total) {
        var noteamount = parseFloat($scope.vendorPurchase.noteAmount) || 0;

        if ($scope.vendorPurchase.noteType == "credit") {
            Total -= noteamount;
            //$scope.purchaseAmount = parseFloat($scope.vendorPurchase.total) + parseFloat($scope.vendorPurchase.tax) - noteamount;
            $scope.purchaseAmount = parseFloat($scope.vendorPurchase.netTotal) - noteamount;
            $scope.purchaseAmount = Math.round($scope.purchaseAmount);
        }
        if ($scope.vendorPurchase.noteType == "debit") {
            Total += noteamount;
            //$scope.purchaseAmount = parseFloat($scope.vendorPurchase.total) + parseFloat($scope.vendorPurchase.tax) + noteamount;
            $scope.purchaseAmount = parseFloat($scope.vendorPurchase.netTotal) + noteamount;
            $scope.purchaseAmount = Math.round($scope.purchaseAmount);
        }

        var afterRoundUp = Math.round(Total);
        $scope.vendorPurchase.grossValue = afterRoundUp;
        $scope.roundedAmount = afterRoundUp - Total;

        if (($scope.returnAmount == 0 || $scope.returnAmount == null || $scope.returnAmount == undefined) && parseFloat($scope.vendorPurchase.netTotal) > 0) {
            if (Math.round(Total) <= 0) {
                $scope.errorNoteAmount = true;
            }
            else {
                $scope.errorNoteAmount = false;
            }
        }

    }

    function getGoodRNO() {
        vendorPurchaseService.getGRNO().then(function (response) {
            $scope.vendorPurchase.goodsRcvNo = response.data.goodsRcvNo;
            if (response.data.billSeries != undefined && response.data.billSeries != null) {
                $scope.vendorPurchase.billSeries = response.data.billSeries;
                $scope.vendorPurchase.goodsRcvNoTemp = $scope.vendorPurchase.billSeries.concat($scope.vendorPurchase.goodsRcvNo);
            }
            else {
                $scope.vendorPurchase.billSeries = null;
                $scope.vendorPurchase.goodsRcvNoTemp = $scope.vendorPurchase.goodsRcvNo;
            }
        });
    }



    $scope.init = function (id, GSTEnabled) {

        if (GSTEnabled == "True") {
            $scope.GSTEnabled = true;
        } else {
            $scope.GSTEnabled = false;
        }

        if (window.localStorage.getItem("p_data") == null) {
            $scope.draftCount = 0;
        } else {
            $scope.tempvendorPurchase = JSON.parse(window.localStorage.getItem("p_data"));
            $scope.draftCount = 1;
        }
        //$scope.loadDraft();
        getIsAllowDecimalSettings();

        //by San 
        $scope.getTaxValues();

        if (id != "") {
            $scope.vendorPurchase.id = id;
            $.LoadingOverlay("show");
            vendorPurchaseService.editVendorPurchaseData($scope.vendorPurchase.id)
                .then(function (response) {

                    $scope.mode = "EDIT";
                    $scope.vendor();
                    $scope.vendorPurchase = response.data;
                    $scope.editModeVendorId = $scope.vendorPurchase.vendorId;

                    $scope.vendorPurchase.selectedVendor = {};
                    $scope.vendorPurchase.selectedVendor.id = $scope.vendorPurchase.vendorId;
                    $scope.vendorPurchase.selectedVendor.discount = $scope.vendorPurchase.vendor.discount;
                    if ($scope.vendorPurchase.vendor.locationType == undefined || $scope.vendorPurchase.vendor.locationType == null || $scope.vendorPurchase.vendor.locationType == "") {
                        $scope.vendorPurchase.selectedVendor.locationType = 1;
                    }
                    else {
                        $scope.vendorPurchase.selectedVendor.locationType = $scope.vendorPurchase.vendor.locationType;
                    }
                    if ($scope.vendorPurchase.vendor.vendorPurchaseFormula == undefined || $scope.vendorPurchase.vendor.vendorPurchaseFormula == null || $scope.vendorPurchase.vendor.vendorPurchaseFormula == "") {
                        $scope.vendorPurchase.vendor.vendorPurchaseFormula = $scope.defaultPurchaseFormula;
                    }
                    $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula = $scope.vendorPurchase.vendor.vendorPurchaseFormula;

                    if ($scope.vendorPurchase.billSeries != undefined && $scope.vendorPurchase.billSeries != null) {
                        $scope.vendorPurchase.goodsRcvNoTemp = $scope.vendorPurchase.billSeries.concat($scope.vendorPurchase.goodsRcvNo);
                    } else {
                        $scope.vendorPurchase.goodsRcvNoTemp = $scope.vendorPurchase.goodsRcvNo;
                    }

                    $scope.vendorPurchase.discount = Number($scope.vendorPurchase.discount) || 0;
                    $scope.vendorPurchase.markupPerc = Number($scope.vendorPurchase.markupPerc) || 0;

                    for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

                        if ($scope.vendorPurchase.vendorPurchaseItem[i].fromDcId == "") {
                            $scope.vendorPurchase.vendorPurchaseItem[i].fromDcId = null;
                        }
                        if ($scope.vendorPurchase.vendorPurchaseItem[i].fromTempId == "") {
                            $scope.vendorPurchase.vendorPurchaseItem[i].fromTempId = null;
                        }

                        $scope.vendorPurchase.vendorPurchaseItem[i].isValid = true;
                        $scope.vendorPurchase.vendorPurchaseItem[i].isEdit = false;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct = {};
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.name = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.eancode = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.eancode;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.id;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.manufacturer = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.manufacturer;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.type = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.type;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.schedule = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.schedule;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.category = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.category;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.packing = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.packing;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.genericName = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.genericName;

                        $scope.vendorPurchase.vendorPurchaseItem[i].freeQty = Number($scope.vendorPurchase.vendorPurchaseItem[i].freeQty) || 0;
                        $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount) || 0;
                        $scope.vendorPurchase.vendorPurchaseItem[i].markupPerc = Number($scope.vendorPurchase.vendorPurchaseItem[i].markupPerc) || 0;

                        $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty;
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageQty1 = Number($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) + Number($scope.vendorPurchase.vendorPurchaseItem[i].freeQty);
                        $scope.vendorPurchase.vendorPurchaseItem[i].quantity = $scope.vendorPurchase.vendorPurchaseItem[i].packageSize * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty1;

                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat;
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst;

                        if ($scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                            $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount);

                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.batchNo = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.batchNo;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.expireDate = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.expireDate;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.gstTotal = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;

                            $scope.vendorPurchase.vendorPurchaseItem[i].name = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name;
                            $scope.vendorPurchase.vendorPurchaseItem[i].batchNo = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.batchNo;
                            $scope.vendorPurchase.vendorPurchaseItem[i].expireDate = $filter('date')(new Date($scope.vendorPurchase.vendorPurchaseItem[i].productStock.expireDate), "MM/yy");
                            $scope.vendorPurchase.vendorPurchaseItem[i].gstTotal = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;
                        }
                        else {
                            $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount) - Number($scope.vendorPurchase.discount);
                            $scope.vendorPurchase.vendorPurchaseItem[i].orgMarkupPerc = Number($scope.vendorPurchase.vendorPurchaseItem[i].markupPerc) - Number($scope.vendorPurchase.markupPerc);
                        }

                        if (!$scope.vendorPurchase.taxRefNo) {
                            if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst > 0) {
                                $scope.vendorPurchase.TaxationType = "CST";
                            }
                            else {
                                $scope.vendorPurchase.TaxationType = "VAT";
                            }
                        }
                    }

                    $scope.returnAmount = $scope.vendorPurchase.returnAmount;
                    //$scope.vendorPurchase.initialValue = null;
                    $scope.editCheque();
                    $scope.ReCalCulateAllValues();

                    $.LoadingOverlay("hide");
                }, function (error) {
                    console.log(error);
                    $.LoadingOverlay("hide");
                    toastr.error('Error Occured', 'Error');
                });
        }
        else {
            getGoodRNO();
            $scope.initialValue();
            $scope.vendor();

            var model = JSON.parse(window.localStorage.getItem("purchaseImportFromExcel"));
            if (model != null) {
                $scope.purchaseImportFromExcel = true;
                $scope.vendorPurchase = model;
                window.localStorage.removeItem("purchaseImportFromExcel");

                $scope.vendorPurchase.selectedVendor = {};
                if (model.vendor != null && model.vendor != undefined && model.vendor.id != null && model.vendor.id != undefined) {
                    $scope.vendorPurchase.selectedVendor = model.vendor;
                }
                $scope.vendorPurchase.discount = Number($scope.vendorPurchase.discount) || 0;
                $scope.vendorPurchase.markupPerc = Number($scope.vendorPurchase.markupPerc) || 0;

                for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
                    $scope.vendorPurchase.vendorPurchaseItem[i].isEdit = false;
                    $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct = {};

                    $scope.vendorPurchase.vendorPurchaseItem[i].productName = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name; //Added by Sarubala on 11-09-17
                    $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.name = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name;
                    $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.id;
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id == undefined || $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id == null || $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id == "") {
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId;
                        $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.accountId = $scope.vendorPurchase.accountId;
                    }

                    $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = Number($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) || 0;
                    $scope.vendorPurchase.vendorPurchaseItem[i].packageQty = Number($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) + (Number($scope.vendorPurchase.vendorPurchaseItem[i].freeQty) || 0);

                    $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.rackNo = $scope.vendorPurchase.vendorPurchaseItem[i].rackNo;

                    $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount) || 0;
                    $scope.vendorPurchase.vendorPurchaseItem[i].markupPerc = Number($scope.vendorPurchase.vendorPurchaseItem[i].markupPerc) || 0;

                    $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount) || 0;
                    $scope.vendorPurchase.vendorPurchaseItem[i].discount = Math.abs(Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + (Number($scope.vendorPurchase.discount) || 0));

                    $scope.vendorPurchase.vendorPurchaseItem[i].orgMarkupPerc = Number($scope.vendorPurchase.vendorPurchaseItem[i].markupPerc) || 0;
                    $scope.vendorPurchase.vendorPurchaseItem[i].markupPerc = Math.abs(Number($scope.vendorPurchase.vendorPurchaseItem[i].orgMarkupPerc) + (Number($scope.vendorPurchase.markupPerc) || 0));

                }
                if (model.vendor.paymentType != undefined && model.vendor.paymentType != null) {
                    $scope.vendorPurchase.paymentType = model.vendor.paymentType;
                    if (model.vendor.creditNoOfDays != undefined && model.vendor.creditNoOfDays != null) {
                        $scope.vendorPurchase.creditNoOfDays = model.vendor.creditNoOfDays;
                    }
                } else {
                    $scope.vendorPurchase.paymentType = "Credit";
                }
                $scope.editCheque();
                $scope.vendorPurchase.initialValue = $scope.vendorPurchase.credit;
                $scope.vendorPurchase.credit = 0;
                $scope.ReCalCulateAllValues();
            }

        }
    };
    function getIsAllowDecimalSettings() {
        vendorPurchaseService.getDecimalSetting()
            .then(function (response) {
                $scope.isAllowDecimal = response.data;
            }, function (error) {
                console.log(error);
                toastr.error('Error Occured', 'Error');
            });
    }
    $scope.getValues = function ($event, selectedProduct1) {
        var key1 = $event.which || $event.keyCode;
        if (key1 == 13)
            if (selectedProduct1 != null) {
                $scope.vendorPurchaseItem.productStock.productId = selectedProduct1.id;
                vendorPurchaseService.getPurchaseValues($scope.vendorPurchaseItem.productStock.productId).then(function (response) {
                    $scope.vendorPurchaseItem = response.data;
                }, function (error) {
                    console.log(error);
                    toastr.error('Error Occured', 'Error');
                });
            }
    };




    $scope.SelectedTaxtype = "";

    $scope.ReCalCulateAllValues = function () {

        if ($scope.vendorPurchase.discount == undefined && $scope.vendorPurchase.discountInRupees == undefined && $scope.vendorPurchase.discountType == undefined) {
            $scope.vendorPurchase.discount = "";
            $scope.vendorPurchase.discountInRupees = "";
            $scope.vendorPurchase.discountType = "percent";
        }
        if ($scope.vendorPurchase.discount != undefined) {
            if ($scope.vendorPurchase.discountType != "rupees") {
                $scope.vendorPurchase.discountType = "percent";
            }
        }
        if ($scope.vendorPurchase.discountType == undefined || $scope.vendorPurchase.discountType == null || $scope.vendorPurchase.discountType == "") {
            $scope.vendorPurchase.discountType = "percent";
        }
        if ($scope.vendorPurchase.noteType == undefined || $scope.vendorPurchase.noteType == null || $scope.vendorPurchase.noteType == "") {
            $scope.vendorPurchase.noteType = "credit";
            $scope.vendorPurchase.noteAmount = "";
        }

        if ($scope.vendorPurchase.discountType == "rupees") {
            $scope.calDiscountInRupees("ReCalculation");
        }
        else if ($scope.vendorPurchase.discountType == "percent") {
            $scope.calDiscountOnBill("ReCalculation");
        }
        setTotal("NewStock");
    };


    $scope.addStock = function () {
        if ($scope.selectedProduct != null) {


            if (typeof $scope.selectedProduct == "string") {
                if (!confirm("This is a new product, do you want to add it ?")) {
                    return;
                }
                $scope.vendorPurchaseItem.productStock.product.name = $scope.selectedProduct;
                $scope.vendorPurchaseItem.productName = $scope.selectedProduct; //Added by Sarubala on 08/09/17
            } else {

                if ($scope.vendorPurchaseItem.id == undefined || $scope.vendorPurchaseItem.id == "" || $scope.vendorPurchaseItem.id == null) {
                    $scope.vendorPurchaseItem.action = "I";
                }
                else if ($scope.vendorPurchaseItem.id != "") {
                    $scope.vendorPurchaseItem.action = "U";
                }

                if ($scope.purchaseImportFromExcel) {
                    if ($scope.selectedProduct.id != undefined) {
                        if (($scope.alterName.id == null || $scope.alterName.id == undefined || $scope.alterName.id == "") && ($scope.alterName != $scope.selectedProduct.name)) {

                            $scope.alternateVendorProduct.vendorId = $scope.vendorPurchase.selectedVendor.id;
                            $scope.alternateVendorProduct.productId = $scope.selectedProduct.id;
                            $scope.alternateVendorProduct.alternateProductName = $scope.alterName.name;
                            $scope.saveAlternate($scope.alternateVendorProduct);
                            if ($scope.alternateVendorProduct.productId != "") {
                                $scope.selectedProduct.id = $scope.alternateVendorProduct.productId;
                                $scope.selectedProduct.accountId = "1";
                            }
                        }
                    }
                }

                $scope.vendorPurchaseItem.productStock.product.name = $scope.selectedProduct.name;
                $scope.vendorPurchaseItem.productName = $scope.selectedProduct.name; //Added by Sarubala on 08/09/17
                $scope.vendorPurchaseItem.productStock.productId = $scope.selectedProduct.id;
                $scope.vendorPurchaseItem.productStock.product.manufacturer = $scope.selectedProduct.manufacturer;
                $scope.vendorPurchaseItem.productStock.product.type = $scope.selectedProduct.type;
                $scope.vendorPurchaseItem.productStock.product.schedule = $scope.selectedProduct.schedule;
                $scope.vendorPurchaseItem.productStock.product.category = $scope.selectedProduct.category;
                $scope.vendorPurchaseItem.productStock.product.packing = $scope.selectedProduct.packing;
                $scope.vendorPurchaseItem.productStock.product.genericName = $scope.selectedProduct.genericName;
                $scope.vendorPurchaseItem.productStock.eancode = $scope.vendorPurchaseItem.productStock.product.eancode;
                if (($scope.selectedProduct.accountId === null || $scope.selectedProduct.accountId === undefined || $scope.selectedProduct.accountId === "") && ($scope.vendorPurchaseItem.productStock.accountId === "" || $scope.vendorPurchaseItem.productStock.accountId === undefined || $scope.vendorPurchaseItem.productStock.accountId === null)) {
                    $scope.vendorPurchaseItem.productStock.product.productMasterID = $scope.selectedProduct.id;
                    $scope.vendorPurchaseItem.productStock.product.id = null;
                } else {
                    $scope.vendorPurchaseItem.productStock.product.productMasterID = null;
                }
            }
            if ($scope.checkEancodeForDuplicate()) {
                return;
            }

            //$scope.excessdiscount = 0;


            $scope.vendorPurchaseItem.isEdit = false;

            $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;
            $scope.vendorPurchaseItem.productStock.vendorId = $scope.vendorPurchase.selectedVendor.id;

            //if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == null) {
            //    $scope.vendorPurchaseItem.productStock.igst = 0;
            //}
            //else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
            //    $scope.vendorPurchaseItem.productStock.cgst = 0;
            //    $scope.vendorPurchaseItem.productStock.sgst = 0;
            //}
            //else {
            //    $scope.vendorPurchaseItem.productStock.gstTotal = 0;
            //    if ($scope.vendorPurchaseItem.vendorReturn) {
            //        $scope.vendorPurchaseItem.selectedProduct.gstTotal = 0;
            //    }
            //    $scope.vendorPurchaseItem.productStock.igst = 0;
            //    $scope.vendorPurchaseItem.productStock.cgst = 0;
            //    $scope.vendorPurchaseItem.productStock.sgst = 0;
            //}
            $scope.vendorPurchaseItem.packageQty = Number($scope.vendorPurchaseItem.packageQty) || 0;
            $scope.vendorPurchaseItem.freeQty = Number($scope.vendorPurchaseItem.freeQty) || 0;
            //$scope.vendorPurchaseItem.discount = parseFloat($scope.vendorPurchaseItem.discount) || 0;

            //var invoiceDiscount = parseFloat($scope.vendorPurchase.discount) || 0;

            //if (($scope.vendorPurchaseItem.discount + invoiceDiscount) > 99.99) {
            //    $scope.excessdiscount++;
            //}
            //$scope.vendorPurchaseItem.discountValue = 0;

            //$scope.vendorPurchaseItem.orgDiscount = $scope.vendorPurchaseItem.discount;
            //if (!$scope.vendorPurchaseItem.vendorReturn)
            //{
            //    $scope.vendorPurchaseItem.discount = Number($scope.vendorPurchaseItem.discount) + invoiceDiscount;
            //}


            //if ($scope.vendorPurchaseItem.discount > 0) {
            //    $scope.vendorPurchaseItem.discountValue = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) * ($scope.vendorPurchaseItem.discount / 100);
            //}

            //$scope.vendorPurchaseItem.total = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) - $scope.vendorPurchaseItem.discountValue;

            $scope.vendorPurchase.markupPerc = Number($scope.vendorPurchase.markupPerc) || 0;
            $scope.vendorPurchaseItem.markupPerc = Number($scope.vendorPurchaseItem.markupPerc) || 0;
            $scope.vendorPurchaseItem.orgMarkupPerc = $scope.vendorPurchaseItem.markupPerc;

            $scope.vendorPurchase.discount = Number($scope.vendorPurchase.discount) || 0;
            $scope.vendorPurchaseItem.discount = Number($scope.vendorPurchaseItem.discount) || 0;
            $scope.vendorPurchaseItem.orgDiscount = $scope.vendorPurchaseItem.discount;

            if (!$scope.vendorPurchaseItem.vendorReturn) {
                $scope.vendorPurchaseItem.markupPerc = $scope.vendorPurchaseItem.orgMarkupPerc + $scope.vendorPurchase.markupPerc;
                if ($scope.vendorPurchaseItem.markupPerc > 99.99) {
                    $scope.errorExcessMarkupPercent = true;
                }
                else {
                    $scope.errorExcessMarkupPercent = false;
                }

                $scope.vendorPurchaseItem.discount = $scope.vendorPurchaseItem.orgDiscount + $scope.vendorPurchase.discount;
                if ($scope.vendorPurchaseItem.discount > 99.99) {
                    $scope.errorExcessPercent = true;
                }
                else {
                    $scope.errorExcessPercent = false;
                }
            }

            //if ($scope.GSTEnabled) {
            //    $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.gstTotal / 100);

            //    if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == null) {
            //        $scope.vendorPurchaseItem.igst = 0;
            //        $scope.vendorPurchaseItem.cgst = $scope.vendorPurchaseItem.tax / 2;
            //        $scope.vendorPurchaseItem.sgst = $scope.vendorPurchaseItem.tax / 2;
            //    }
            //    else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
            //        $scope.vendorPurchaseItem.igst = $scope.vendorPurchaseItem.tax;
            //        $scope.vendorPurchaseItem.cgst = 0;
            //        $scope.vendorPurchaseItem.sgst = 0;
            //    }
            //    else {
            //        $scope.vendorPurchaseItem.igst = 0;
            //        $scope.vendorPurchaseItem.cgst = 0;
            //        $scope.vendorPurchaseItem.sgst = 0;
            //    }

            //} else {
            //    if ($scope.vendorPurchase.TaxationType == "VAT") {
            //        $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.vAT / 100);
            //    }
            //    if ($scope.vendorPurchase.TaxationType == "CST") {
            //        $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.cST / 100);
            //    }
            //}


            $scope.vendorPurchaseItem.orderedQty = $scope.vendorPurchaseItem.packageQty || 0;

            if ($scope.vendorPurchaseItem.freeQty > 0) {
                $scope.vendorPurchaseItem.packageQty = Number($scope.vendorPurchaseItem.packageQty) + Number($scope.vendorPurchaseItem.freeQty);
            }
            $scope.vendorPurchaseItem.quantity = $scope.vendorPurchaseItem.packageSize * $scope.vendorPurchaseItem.packageQty;


            //if (!$scope.GSTEnabled) {
            //    var _isCST = false;
            //    if ($scope.vendorPurchase.TaxationType == "VAT") {
            //        $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.vAT;
            //        $scope.vendorPurchaseItem.productStock.cST = 0;
            //        var cst = document.getElementById("cST");
            //        cst.disabled = true;
            //    }
            //    var vat;
            //    if ($scope.vendorPurchase.TaxationType == "CST") {
            //        $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.cST;
            //        $scope.vendorPurchase.TaxationType = "CST";
            //        _isCST = true;
            //    }
            //}

            if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
                if ($scope.vendorPurchase.TaxationType == "VAT") {
                    $scope.vendorPurchaseItem.productStock.vAT = Number($scope.vendorPurchaseItem.productStock.vAT) || 0;
                    $scope.vendorPurchaseItem.productStock.cST = 0;
                }
                else if ($scope.vendorPurchase.TaxationType == "CST") {
                    $scope.vendorPurchaseItem.productStock.vAT = 0;
                    $scope.vendorPurchaseItem.productStock.cST = Number($scope.vendorPurchaseItem.productStock.cST) || 0;
                }
            }


            $scope.vendorPurchaseItem.selectedProduct = $scope.selectedProduct;
            //if ($scope.SaveTaxSeries == 2) {
            //    $scope.vendorPurchaseItem.productStock.TaxType = $scope.SelectedTaxtype;
            //}




            if (!$scope.EditMode) {
                $scope.vendorPurchaseItem.previouspurchases = $scope.previouspurchases;
                if (!$scope.vendorPurchaseItem.hasOwnProperty('tempItem')) {
                    $scope.vendorPurchaseItem.tempItem = false;
                }
                var status = 0;

                for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

                    //var tempDiscount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount || 0;
                    //if ((tempDiscount + invoiceDiscount) > 99.99) {
                    //    $scope.excessdiscount++;
                    //}

                    //if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    //    $scope.vendorPurchase.vendorPurchaseItem[i].discount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount + invoiceDiscount;

                    //    $scope.vendorPurchase.vendorPurchaseItem[i].discountValue = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);
                    //}

                    // Product Restriction by Gavaskar 14-09-2017 Start 
                    if ($scope.EditMode == false) {
                        for (var k = 0; k < $scope.vendorPurchase.vendorPurchaseItem.length; k++) {
                            if (($scope.vendorPurchase.vendorPurchaseItem[k].productStock.product.id == $scope.selectedProduct.id)) {
                                //Added by Sarubala on 27-09-17
                                if ($scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn != null && $scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn != undefined) {
                                    if ($scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn == true) {
                                        toastr.info("Product already added in Return");
                                        document.getElementById("drugName").focus();
                                        return;
                                    }
                                }
                            }
                        }
                    }

                    // Product Restriction by Gavaskar 14-09-2017 End 

                    //// Added by Gavaskar 14-09-2017
                    //$scope.vendorPurchase.vendorPurchaseItem[i].batchNo == $scope.vendorPurchaseItem.productStock.batchNo

                    //Same ProductExist
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId == $scope.vendorPurchaseItem.productStock.productId &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.batchNo == $scope.vendorPurchaseItem.productStock.batchNo &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat == $scope.vendorPurchaseItem.productStock.vat &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageSize == $scope.vendorPurchaseItem.packageSize &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice == $scope.vendorPurchaseItem.packagePurchasePrice &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageSellingPrice == $scope.vendorPurchaseItem.packageSellingPrice &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.eancode == $scope.vendorPurchaseItem.productStock.eancode &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageMRP == $scope.vendorPurchaseItem.packageMRP &&
                        !$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {

                        if ($scope.mode == "EDIT") {
                            if (!$scope.vendorPurchaseItem.vendorReturn) {
                                $scope.vendorPurchaseItem.markupPerc = $scope.vendorPurchaseItem.orgMarkupPerc;
                                $scope.vendorPurchaseItem.discount = $scope.vendorPurchaseItem.orgDiscount;
                                $scope.vendorPurchaseItem.packageQty = $scope.vendorPurchaseItem.orderedQty;
                            }
                            alert("This Product is already added");
                            return false;
                        }

                        $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) + parseInt($scope.vendorPurchaseItem.orderedQty);
                        $scope.vendorPurchase.vendorPurchaseItem[i].freeQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].freeQty) + parseInt($scope.vendorPurchaseItem.freeQty);
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) + parseInt($scope.vendorPurchaseItem.packageQty);
                        $scope.vendorPurchase.vendorPurchaseItem[i].quantity = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].quantity) + parseInt($scope.vendorPurchaseItem.quantity);

                        //if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                        //    $scope.vendorPurchase.vendorPurchaseItem[i].discount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount;


                        //    $scope.vendorPurchase.vendorPurchaseItem[i].discountValue = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);

                        //}


                        //$scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountValue;


                        //if ($scope.GSTEnabled) {

                        //    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);

                        //} else {
                        //    if ($scope.vendorPurchase.TaxationType == "VAT") {
                        //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                        //    }
                        //    if ($scope.vendorPurchase.TaxationType == "CST") {
                        //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].totalproductStock.cST / 100);
                        //    }
                        //}



                        status = 1;
                        $scope.addStockRowIndex = i;
                    }
                }
                //if ($scope.excessdiscount > 0) {
                //    $scope.errorExcessPercent = true;
                //} else {
                //    $scope.errorExcessPercent = false;
                //}
                if (status == 0) {
                    $scope.vendorPurchase.vendorPurchaseItem.push($scope.vendorPurchaseItem);
                    $scope.addStockRowIndex = $scope.vendorPurchase.vendorPurchaseItem.length - 1;

                }
                //else {
                //setTotal("NewStock");
                //setReturnAmt();
                //setVat();
                //$scope.initialValue();
                //getIsAllowDecimalSettings();
                //var qty = document.getElementById("drugName");
                //qty.focus();
                //}
                if ($scope.vendorPurchase.vendorPurchaseItem.length != 0) {
                    $scope.disableAddNewDc = true;
                }
                $scope.nonTempItemCount = $filter("filter")($scope.vendorPurchase.vendorPurchaseItem, { "tempItem": false }).length;
                $scope.TempItemCount = $filter("filter")($scope.vendorPurchase.vendorPurchaseItem, { "tempItem": true }).length;
            } else {
                var status = 0;

                if (!$scope.purchaseImportFromExcel) {

                    for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

                        //var tempDiscount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount || 0;
                        //if ((tempDiscount + invoiceDiscount) > 99.99) {
                        //    $scope.excessdiscount++;
                        //}
                        //if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                        //    $scope.vendorPurchase.vendorPurchaseItem[i].discount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount + invoiceDiscount;
                        //    $scope.vendorPurchase.vendorPurchaseItem[i].discountValue = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);
                        //}



                        //Same ProductExist
                        if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId == $scope.vendorPurchaseItem.productStock.productId &&
                            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.batchNo == $scope.vendorPurchaseItem.productStock.batchNo &&
                            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT == $scope.vendorPurchaseItem.productStock.vAT &&
                            $scope.vendorPurchase.vendorPurchaseItem[i].packageSize == $scope.vendorPurchaseItem.packageSize &&
                            $scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice == $scope.vendorPurchaseItem.packagePurchasePrice &&
                            $scope.vendorPurchase.vendorPurchaseItem[i].packageSellingPrice == $scope.vendorPurchaseItem.packageSellingPrice &&
                            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.eancode == $scope.vendorPurchaseItem.productStock.eancode &&
                            $scope.vendorPurchase.vendorPurchaseItem[i].packageMRP == $scope.vendorPurchaseItem.packageMRP &&
                            !$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                            if ($scope.Editindex == i) {
                                status = 0;
                            } else {
                                $scope.vendorPurchase.vendorPurchaseItem[i].packageQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) + parseInt($scope.vendorPurchaseItem.packageQty);
                                $scope.vendorPurchase.vendorPurchaseItem[i].freeQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].freeQty) + parseInt($scope.vendorPurchaseItem.freeQty);
                                $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - parseInt($scope.vendorPurchase.vendorPurchaseItem[i].freeQty) - parseInt($scope.vendorPurchaseItem.freeQty);
                                $scope.vendorPurchase.vendorPurchaseItem[i].quantity = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packageSize);

                                //if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                                //$scope.vendorPurchase.vendorPurchaseItem[i].discount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount;

                                //$scope.vendorPurchase.vendorPurchaseItem[i].discountValue = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);
                                // }



                                //$scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountValue;


                                //if ($scope.GSTEnabled) {

                                //    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);

                                //} else {
                                //    if ($scope.vendorPurchase.TaxationType == "VAT") {
                                //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                                //    }
                                //    if ($scope.vendorPurchase.TaxationType == "CST") {
                                //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].totalproductStock.cST / 100);
                                //    }
                                //}




                                status = 1;
                                $scope.addStockRowIndex = i;

                            }
                        }
                    }
                }
                //if ($scope.excessdiscount > 0) {
                //    $scope.errorExcessPercent = true;
                //} else {
                //    $scope.errorExcessPercent = false;
                //}
                if (status == 0) {
                    $scope.vendorPurchaseItem.isValid = true;
                    $scope.vendorPurchase.vendorPurchaseItem[$scope.copyedIndex] = $scope.vendorPurchaseItem;
                    $scope.addStockRowIndex = $scope.copyedIndex;
                } else {
                    $scope.vendorPurchase.vendorPurchaseItem.splice($scope.Editindex, 1);
                    //setTotal("NewStock");
                    //setReturnAmt();
                    //setVat();
                    //$scope.initialValue();
                    //getIsAllowDecimalSettings();
                    //var qty = document.getElementById("drugName");
                    //qty.focus();
                }
                $scope.EditMode = false;
                $scope.poItemEdit = false;
                $scope.dcItemEdit = false;
                $scope.returnItemEdit = false;
            }
            $scope.vendorPurchaseItems.$setPristine();
            $scope.vendorPurchaseItem = { "productStock": { "product": {} } };
            $scope.selectedProduct = null;

            //setTotal("NewStock");
            //setReturnAmt();
            //setVat();
            //$scope.ReCalCulateAllValues();


            //saveDraft();
            //if (!$scope.GSTEnabled) {
            //    (_isCST) ? $scope.vendorPurchaseItem.productStock.cST = 2 : $scope.vendorPurchaseItem.productStock.vAT = 5;
            //}

            if ($scope.vendorPurchase.selectedVendor.discount) {
                $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
            } else {
                $scope.vendorPurchaseItem.discount = 0;
            }
            //$scope.initialValue();
            //getIsAllowDecimalSettings();
            document.getElementById("drugName").disabled = false;
            var qty = document.getElementById("drugName");
            qty.focus();
            $scope.showPriceMessage = "";
            if ($scope.previouspurchases != undefined) {
                if ($scope.previouspurchases.length > 0) {
                    $scope.toggleProductDetail();
                    $scope.previouspurchases = [];
                }
            }
            else {
                $scope.previouspurchases = [];
            }
            $scope.vendorPurchaseItems.$setUntouched();
            $scope.vendorPurchaseItems.$setPristine();

            $scope.IsAddStockRow = true;
            $scope.ReCalCulateAllValues();
            $scope.IsAddStockRow = false;

        }
        $scope.Editindex = -1;
        $scope.remainStock = 0;
        $scope.showSchemeDiscount('percent');
    };

    $scope.applyVendorPurchaseFormula = function () {

        $scope.vendorPurchase.total = 0;
        $scope.vendorPurchase.netTotal = 0;
        $scope.vendorPurchase.tax = 0;
        $scope.vendorPurchase.overAllDiscount = 0;
        $scope.vendorPurchase.markupValue = 0;
        $scope.vendorPurchase.schemeDiscountValue = 0;
        $scope.vendorPurchase.freeQtyTotalTaxValue = 0;
        $scope.IsDcOrTempItemExist = false;

        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

            if (($scope.vendorPurchase.vendorPurchaseItem[i].fromTempId != undefined && $scope.vendorPurchase.vendorPurchaseItem[i].fromTempId != null && $scope.vendorPurchase.vendorPurchaseItem[i].fromTempId != "")) {
                $scope.IsDcOrTempItemExist = true;
            }

            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if ($scope.IsAddStockRow && ($scope.vendorPurchase.discountType == "percent" || (parseFloat($scope.vendorPurchase.discountInRupees) || 0) == 0)) {
                    if ($scope.addStockRowIndex != -1) {
                        applyTaxValues($scope.addStockRowIndex);
                        applyFormulaToSingleItem($scope.addStockRowIndex);
                        $scope.addStockRowIndex = -1;
                    }
                }
                else {
                    applyTaxValues(i);
                    applyFormulaToSingleItem(i);
                }

                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    $scope.vendorPurchase.overAllDiscount += $scope.vendorPurchase.vendorPurchaseItem[i].discountValue;
                    $scope.vendorPurchase.markupValue += $scope.vendorPurchase.vendorPurchaseItem[i].markupValue;
                    $scope.vendorPurchase.schemeDiscountValue += $scope.vendorPurchase.vendorPurchaseItem[i].schemeDiscountValue;
                    $scope.vendorPurchase.total += $scope.vendorPurchase.vendorPurchaseItem[i].total;
                    $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;
                    $scope.vendorPurchase.netTotal += $scope.vendorPurchase.vendorPurchaseItem[i].netTotal;
                    $scope.vendorPurchase.freeQtyTotalTaxValue += $scope.vendorPurchase.vendorPurchaseItem[i].freeQtyTaxValue;
                }
                if (($scope.GSTEnabled && $scope.mode == "NEW") || ($scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
                    $scope.vendorPurchase.vendorPurchaseItem[i].gstValue = $scope.vendorPurchase.vendorPurchaseItem[i].tax;
                }
                else {
                    $scope.vendorPurchase.vendorPurchaseItem[i].vatValue = $scope.vendorPurchase.vendorPurchaseItem[i].tax;
                }
            }
        }

        setReturnAmt();
        setVat();
        $scope.initialValue();
    };

    function applyFormulaToSingleItem(idx) {
        var basicAmount = $scope.vendorPurchase.vendorPurchaseItem[idx].orderedQty * $scope.vendorPurchase.vendorPurchaseItem[idx].packagePurchasePrice;
        var vendorPurchaseItem = JSON.parse(JSON.stringify($scope.vendorPurchase.vendorPurchaseItem[idx]));

        $scope.vendorPurchase.vendorPurchaseItem[idx].discountValue = getFormulaNetValue(basicAmount, vendorPurchaseItem, "discount");
        $scope.vendorPurchase.vendorPurchaseItem[idx].markupValue = getFormulaNetValue(basicAmount, vendorPurchaseItem, "markup");
        $scope.vendorPurchase.vendorPurchaseItem[idx].schemeDiscountValue = getFormulaNetValue(basicAmount, vendorPurchaseItem, "schemediscount");
        if ($scope.vendorPurchase.isApplyTaxFreeQty) {
            var freeQtyBasicAmount = (Number($scope.vendorPurchase.vendorPurchaseItem[idx].freeQty) || 0) * $scope.vendorPurchase.vendorPurchaseItem[idx].packagePurchasePrice;
            $scope.vendorPurchase.vendorPurchaseItem[idx].freeQtyTaxValue = getFormulaNetValue(freeQtyBasicAmount, vendorPurchaseItem, "freetax");
            $scope.vendorPurchase.vendorPurchaseItem[idx].tax = getFormulaNetValue(basicAmount, vendorPurchaseItem, "tax") + $scope.vendorPurchase.vendorPurchaseItem[idx].freeQtyTaxValue;
            $scope.vendorPurchase.vendorPurchaseItem[idx].netTotal = getFormulaNetValue(basicAmount, vendorPurchaseItem, "") + $scope.vendorPurchase.vendorPurchaseItem[idx].freeQtyTaxValue;
        }
        else {
            $scope.vendorPurchase.vendorPurchaseItem[idx].tax = getFormulaNetValue(basicAmount, vendorPurchaseItem, "tax");
            $scope.vendorPurchase.vendorPurchaseItem[idx].netTotal = getFormulaNetValue(basicAmount, vendorPurchaseItem, "");
            $scope.vendorPurchase.vendorPurchaseItem[idx].freeQtyTaxValue = 0;
        }
        $scope.vendorPurchase.vendorPurchaseItem[idx].total = $scope.vendorPurchase.vendorPurchaseItem[idx].netTotal - $scope.vendorPurchase.vendorPurchaseItem[idx].tax;
        $scope.vendorPurchase.vendorPurchaseItem[idx].quantity = $scope.vendorPurchase.vendorPurchaseItem[idx].packageSize * $scope.vendorPurchase.vendorPurchaseItem[idx].packageQty;
        //Assign productStock values
        $scope.vendorPurchase.vendorPurchaseItem[idx].purchasePrice = $scope.vendorPurchase.vendorPurchaseItem[idx].netTotal / $scope.vendorPurchase.vendorPurchaseItem[idx].packageQty / $scope.vendorPurchase.vendorPurchaseItem[idx].packageSize;
        $scope.vendorPurchase.vendorPurchaseItem[idx].productStock.purchasePrice = $scope.vendorPurchase.vendorPurchaseItem[idx].purchasePrice;
        $scope.vendorPurchase.vendorPurchaseItem[idx].productStock.sellingPrice = $scope.vendorPurchase.vendorPurchaseItem[idx].packageSellingPrice / $scope.vendorPurchase.vendorPurchaseItem[idx].packageSize;
        $scope.vendorPurchase.vendorPurchaseItem[idx].productStock.mrp = $scope.vendorPurchase.vendorPurchaseItem[idx].packageMRP / $scope.vendorPurchase.vendorPurchaseItem[idx].packageSize;
        $scope.vendorPurchase.vendorPurchaseItem[idx].productStock.packageSize = $scope.vendorPurchase.vendorPurchaseItem[idx].packageSize;
        $scope.vendorPurchase.vendorPurchaseItem[idx].productStock.packagePurchasePrice = $scope.vendorPurchase.vendorPurchaseItem[idx].packagePurchasePrice;
        $scope.vendorPurchase.vendorPurchaseItem[idx].productStock.vendorId = $scope.vendorPurchase.selectedVendor.id;

        $scope.vendorPurchase.vendorPurchaseItem[idx].hsnCode = $scope.vendorPurchase.vendorPurchaseItem[idx].productStock.hsnCode;
    };

    function getFormulaNetValue(basicAmount, vendorPurchaseItem, formulaTag) {

        if ($scope.vendorPurchase.selectedVendor != null && $scope.vendorPurchase.selectedVendor != undefined && $scope.vendorPurchase.selectedVendor != "") {
            if ($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula == null || $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula == undefined || $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula == "") {
                $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula = $scope.defaultPurchaseFormula;
            }
            if (typeof ($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON) != "object") {
                $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON = JSON.parse($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON);
            }
            var vendorFormula = $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON;

            basicAmount = parseFloat(basicAmount);
            var amount = 0;
            var returnFreeTax = false;
            vendorFormula = $filter('orderBy')(vendorFormula, 'order');
            for (var i = 0; i < vendorFormula.length; i++) {
                var formulaPerc = 0;
                switch (vendorFormula[i].formulaTag) {
                    case "markup": {
                        if (formulaTag == "freetax") {
                            formulaPerc = 0;
                        }
                        else {
                            formulaPerc = vendorPurchaseItem.markupPerc || 0;
                        }
                        break;
                    }
                    case "discount": {
                        if (formulaTag == "freetax") {
                            formulaPerc = 0;
                        }
                        else {
                            formulaPerc = vendorPurchaseItem.discount || 0;
                        }
                        break;
                    }
                    case "schemediscount": {
                        if (formulaTag == "freetax") {
                            formulaPerc = 0;
                        }
                        else {
                            if ($scope.showSchemeDisc == false && parseFloat(vendorPurchaseItem.schemeDiscountValue) >= 0) {
                                $scope.vendorPurchaseItem.individualSchemeValue = parseFloat(vendorPurchaseItem.schemeDiscountValue) / parseFloat(vendorPurchaseItem.packageQty);
                                $scope.vendorPurchaseItem.schemeDiscountPerc = ($scope.vendorPurchaseItem.individualSchemeValue * 100) / parseFloat(basicAmount);
                            }
                            formulaPerc = vendorPurchaseItem.schemeDiscountPerc || 0;
                        }
                        break;
                    }
                    case "tax": {
                        if (($scope.GSTEnabled && $scope.mode == "NEW") || ($scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
                            formulaPerc = vendorPurchaseItem.productStock.gstTotal || 0;
                        }
                        else {
                            if ($scope.vendorPurchase.TaxationType == "VAT") {
                                formulaPerc = vendorPurchaseItem.productStock.vAT || 0;
                            }
                            else if ($scope.vendorPurchase.TaxationType == "CST") {
                                formulaPerc = vendorPurchaseItem.productStock.cST || 0;
                            }
                        }
                        if (formulaTag == "freetax") {
                            returnFreeTax = true;
                        }
                        break;
                    }
                    default: {
                        formulaPerc = 0;
                        break;
                    }
                }
                amount = basicAmount * (formulaPerc / 100);
                if (vendorFormula[i].operation == "+") {
                    basicAmount = basicAmount + amount;
                }
                else if (vendorFormula[i].operation == "-") {
                    basicAmount = basicAmount - amount;
                }
                if (vendorFormula[i].formulaTag == formulaTag || returnFreeTax == true)
                    return amount;
            }
            return basicAmount;
        }
        return 0;
    };

    function applyTaxValues(i) {

        if (($scope.GSTEnabled && $scope.mode == "NEW") || ($scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {

            //VendorPurchaseItem
            $scope.vendorPurchase.vendorPurchaseItem[i].gstTotal = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;
            $scope.vendorPurchase.vendorPurchaseItem[i].vAT = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].vatValue = 0;

            if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == null || $scope.vendorPurchase.selectedVendor.locationType == undefined || $scope.vendorPurchase.selectedVendor.locationType == "") {
                $scope.vendorPurchase.vendorPurchaseItem[i].igst = 0;
                $scope.vendorPurchase.vendorPurchaseItem[i].cgst = $scope.vendorPurchase.vendorPurchaseItem[i].gstTotal / 2;
                $scope.vendorPurchase.vendorPurchaseItem[i].sgst = $scope.vendorPurchase.vendorPurchaseItem[i].gstTotal / 2;
            }
            else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
                $scope.vendorPurchase.vendorPurchaseItem[i].igst = $scope.vendorPurchase.vendorPurchaseItem[i].gstTotal;
                $scope.vendorPurchase.vendorPurchaseItem[i].cgst = 0;
                $scope.vendorPurchase.vendorPurchaseItem[i].sgst = 0;
            }
            else {
                $scope.vendorPurchase.vendorPurchaseItem[i].gstTotal = 0;
                $scope.vendorPurchase.vendorPurchaseItem[i].igst = 0;
                $scope.vendorPurchase.vendorPurchaseItem[i].cgst = 0;
                $scope.vendorPurchase.vendorPurchaseItem[i].sgst = 0;
            }

            //ProductStock
            if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == 2 || $scope.vendorPurchase.selectedVendor.locationType == null || $scope.vendorPurchase.selectedVendor.locationType == undefined || $scope.vendorPurchase.selectedVendor.locationType == "") {
                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.igst = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;
                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cgst = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 2;
                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.sgst = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 2;
            }
            else {
                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal = 0;
                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.igst = 0;
                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cgst = 0;
                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.sgst = 0;

                if ($scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.gstTotal = 0;
                }
            }

        }
        else {
            if ($scope.vendorPurchase.TaxationType == "VAT") {
                $scope.vendorPurchase.vendorPurchaseItem[i].vAT = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT;
                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST = 0;
            }
            else if ($scope.vendorPurchase.TaxationType == "CST") {
                $scope.vendorPurchase.vendorPurchaseItem[i].vAT = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST;
                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT = 0;
            }
            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.igst = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cgst = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.sgst = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal = 0;

            $scope.vendorPurchase.vendorPurchaseItem[i].igst = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].cgst = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].sgst = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].gstTotal = 0;
            $scope.vendorPurchase.vendorPurchaseItem[i].gstValue = 0;

            $scope.vendorPurchase.cgst = 0;
            $scope.vendorPurchase.sgst = 0;
            $scope.vendorPurchase.igst = 0;

        }

    };

    $scope.calculateMarkup = function () {
        if ($scope.vendorPurchase.markupPerc > 99.99) {
            $scope.errorExcessMarkupPercent = true;
            return;
        } else {
            $scope.errorExcessMarkupPercent = false;
        }
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {

                    if ($scope.mode == "EDIT" && $scope.vendorPurchase.vendorPurchaseItem[i].markupPerc != Number($scope.vendorPurchase.vendorPurchaseItem[i].orgMarkupPerc) + Number($scope.vendorPurchase.markupPerc)) {
                        if ($scope.vendorPurchase.vendorPurchaseItem[i].id == undefined || $scope.vendorPurchase.vendorPurchaseItem[i].id == "" || $scope.vendorPurchase.vendorPurchaseItem[i].id == null) {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "I";
                        }
                        else if ($scope.vendorPurchase.vendorPurchaseItem[i].id != "") {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                        }
                    }

                    $scope.vendorPurchase.vendorPurchaseItem[i].markupPerc = (Number($scope.vendorPurchase.vendorPurchaseItem[i].orgMarkupPerc) || 0) + (Number($scope.vendorPurchase.markupPerc) || 0);
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].markupPerc > 99.99) {
                        $scope.errorExcessMarkupPercent = true;
                        return false;
                    } else {
                        $scope.errorExcessMarkupPercent = false;
                    }
                }
            }
        }
        $scope.ReCalCulateAllValues();
    };

    $scope.toggleProductDetail = function () {
        $('#chip-wrapper0').slideToggle();
        $('#chip-wrapper0').show();
        if ($('#chip-btn0').text() === '+') {
            $('#chip-btn0').text('-');
        } else {
            $('#chip-btn0').text('+');
        }
    };
    $scope.removeStock = function (item) {
        //$scope.vendorPurchase.total -= (item.productStock.sellingPrice * item.quantity);
        //var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
        //$scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
        //window.localStorage.setItem("p_data", JSON.stringify($scope.vendorPurchase));
        //setTotal("");
        //setVat();

        var cancel = window.confirm('Are you sure, Do you want to Delete Item?');
        if (cancel) {
            var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
            $scope.vendorPurchase.vendorPurchaseItem[index].isDeleted = true;
            if ($scope.vendorPurchase.vendorPurchaseItem[index].id == undefined || $scope.vendorPurchase.vendorPurchaseItem[index].id == "" || $scope.vendorPurchase.vendorPurchaseItem[index].id == null) {
                $scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
            }
        }
        $scope.ReCalCulateAllValues();

        if ($scope.vendorPurchase.vendorPurchaseItem.length == 0) {
            ClearDiscountFields();
        }
        //saveCurrentDraft();
    };

    $scope.removeReturnStock = function (item) {
        //var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
        //var returntotal = 0;
        //$scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
        //window.localStorage.setItem("p_data", JSON.stringify($scope.vendorPurchase));

        var cancel = window.confirm('Are you sure, Do you want to Delete Item?');
        if (cancel) {
            var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
            $scope.vendorPurchase.vendorPurchaseItem[index].isDeleted = true;
            if ($scope.vendorPurchase.vendorPurchaseItem[index].id == undefined || $scope.vendorPurchase.vendorPurchaseItem[index].id == "" || $scope.vendorPurchase.vendorPurchaseItem[index].id == null) {
                $scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
            }
        }

        //if ($scope.vendorPurchase.vendorPurchaseItem.length > 0)
        //{
        //    $scope.returnAmount = 0;
        //    $scope.vendorPurchase.tax = 0;
        //    $scope.vendorPurchase.vendorPurchaseItem.ReturnedTotal = 0;
        //    for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

        //        if ($scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
        //            returntotal += (parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) * parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice)) - parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].discountValue);

        //            $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;

        //            $scope.vendorPurchase.vendorPurchaseItem[i].ReturnPurchasePrice = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) * parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice);
        //        }
        //    }

        //    $scope.returnAmount = parseFloat(returntotal) + parseFloat($scope.vendorPurchase.tax);
        //    $scope.returnAmount = Math.round($scope.returnAmount);
        //    $scope.vendorPurchase.vendorPurchaseItem.ReturnedTotal = parseFloat($scope.returnAmount);
        //}
        //setReturnAmt();
        //setVat();
        $scope.ReCalCulateAllValues();

    };

    function ClearDiscountFields() {
        $scope.vendorPurchase.discountInRupees = "";
        $scope.vendorPurchase.discount = "";
        $scope.vendorPurchase.noteAmount = "";
        $scope.vendorPurchase.overAllDiscount = 0;
        $scope.vendorPurchase.tax = 0;
        $scope.vendorPurchase.igst = 0;
        $scope.vendorPurchase.cgst = 0;
        $scope.vendorPurchase.sgst = 0;

        $scope.vendorPurchase.total = 0;
        $scope.vendorPurchase.grossValue = 0;
        $scope.vendorPurchase.discountType = "percent";

    }
    $scope.cancelUpdate = function () {
        //if ($scope.Editindex != -1) {
        //    var invoicediscont = parseFloat($scope.vendorPurchase.discount) || 0;
        //    if ($scope.vendorPurchase.discount != "" || $scope.vendorPurchase.discount != null) {
        //        if (!$scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].isDeleted) {
        //            if (!$scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].vendorReturn) {
        //                $scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discount = parseFloat($scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discount) + invoicediscont;
        //                $scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discountValue = ($scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discount / 100);
        //            }
        //        }
        //    }
        //    $scope.Editindex = -1;
        //}
        $scope.EditMode = false;
        $scope.dcItemEdit = false;
        $scope.poItemEdit = false;
        $scope.returnItemEdit = false;
        $scope.vendorPurchaseItems.$setPristine();
        $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", true);
        $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidRoundQty", true);
        $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", true);
        $scope.vendorPurchaseItems.freeQty.$setValidity("InvalidSumQty", true);
        $scope.vendorPurchaseItems.freeQty.$setValidity("InValidfreeQty", true);
        $scope.vendorPurchaseItem = { productStock: { product: {} } };
        $scope.selectedProduct = null;
        if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
            $scope.vendorPurchaseItem.productStock.vAT = vendorPurchaseItem.productStock.vAT;
        }
        $scope.remainStock = 0;
        document.getElementById("drugName").disabled = false;
    };
    $scope.Editindex = -1;
    $scope.editPurchase = function (item, ind) {
        if (item.vendorReturn) {
            $scope.returnItemEdit = true;
        }
        else {
            $scope.returnItemEdit = false;
            if (item.fromDcId != null && item.fromDcId != "") {
                $scope.dcItemEdit = true;
            } else if (item.fromTempId == null && item.isNew == undefined) {
                $scope.poItemEdit = true;
            }
        }
        if ($scope.EditMode) {
            return;
        }

        //item.discount = item.orgDiscount;
        //item.markupPerc = item.orgMarkupPerc;
        //item.packageQty = item.orderedQty;

        if ($scope.mode == "EDIT" || item.vendorReturn) {
            var remainStock = (item.productStock.stock / item.packageSize) || 0;
            $scope.remainStock = 0;
            $scope.soldOrReturned = 0;
            if (remainStock != item.packageQty1) {
                $scope.remainStock = Math.floor(remainStock);
                $scope.soldOrReturned = (parseFloat(item.packageQty1 || 0) - $scope.remainStock);
            }

        }

        //item.discountValue = (item.packagePurchasePrice * item.packageQty) * (item.discount / 100);
        var taxValuesList = [];
        if (item.vendorReturn) {
            taxValuesList = $scope.allTaxValuesList;
        }
        else {
            taxValuesList = $scope.taxValuesList;
        }
        item.productStock.gstTotal = parseFloat(item.productStock.gstTotal);
        var ngst = $filter("filter")(taxValuesList, { "tax": item.productStock.gstTotal }, true);

        if (ngst.length == 0) {
            item.productStock.gstTotal = null;
            item.gstTotal = null;
        }

        $scope.copyedIndex = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
        $scope.vendorPurchaseItem = JSON.parse(JSON.stringify(item));
        $scope.vendorPurchaseItem.discount = $scope.vendorPurchaseItem.orgDiscount;
        $scope.vendorPurchaseItem.markupPerc = $scope.vendorPurchaseItem.orgMarkupPerc;
        $scope.vendorPurchaseItem.packageQty = $scope.vendorPurchaseItem.orderedQty;
        if ($scope.vendorPurchaseItem.productStock.expireDate == "0001-01-01T00:00:00") {
            $scope.vendorPurchaseItem.productStock.expireDate = null;
        }
        if ($scope.vendorPurchaseItem.productStock.batchNo.trim().length == 0) {
            $scope.vendorPurchaseItem.productStock.batchNo = "";
        }
        $scope.alterName = item.selectedProduct;
        $scope.selectedProduct = item.selectedProduct;
        $scope.EditMode = true;
        $scope.Editindex = ind;
        $scope.previouspurchases = item.previouspurchases;
        // Added Gavaskar 12-08-2017 FOCUSING IN PRODUCT ENTRY FIELD 
        if ($scope.returnItemEdit == true) {
            var ele = document.getElementById("packageQuantity");
            ele.focus();
        }
        else {
            if (item.vendorPurchaseId != null && $scope.mode == "EDIT") { // by San 30-11-2017  
                document.getElementById("drugName").disabled = true;
                var ele = document.getElementById("batchNo");
                ele.focus();
            }
            else {
                document.getElementById("drugName").disabled = false;
                var ele = document.getElementById("drugName");
                ele.focus();
            }

        }

        $scope.changeGST(item.productStock.gstTotal);
    };
    $scope.doneEditPurchase = function (item, isValid) {
        if (!isValid) {
            item.isEdit = false;
            $scope.editItems = $scope.editItems - 1;
        }
        //setTotal("");
        //setVat();
        $scope.ReCalCulateAllValues();
    };
    $scope.SelectedFileForUpload = null;
    //File Select event 
    $scope.selectFileforUpload = function (file) {
        $scope.SelectedFileForUpload = file[0];
        document.getElementById("creditNoOfDays").focus();
    };
    $scope.save = function () {

        if ($scope.vendorPurchase.invoiceNo == undefined || $scope.vendorPurchase.invoiceNo == null) {
            var ele = document.getElementById("invoiceNo");
            ele.focus();
            return false;
        }
        if ($scope.vendorPurchase.selectedVendor.id == null || $scope.vendorPurchase.selectedVendor.id == undefined || $scope.vendorPurchase.selectedVendor.id == '') {
            ShowConfirmMsgWindow("Please select Vendor Name and try again.");
            return;
        }

        if ($scope.vendorPurchase.paymentType == "" || $scope.vendorPurchase.paymentType == null || $scope.vendorPurchase.paymentType == undefined) {
            ShowConfirmMsgWindow("Please select Payment Type(Cash/Credit) and try again.");
            return;
        }

        if ($scope.IsDcOrTempItemExist == true && $scope.vendorPurchase.markupPerc > 0) {
            ShowConfirmMsgWindow('"Invoice Markup Perc" is not allowed in DC/Temp stock items convertion. Please clear "Invoice Markup Perc" and try again.');
            return;
        }

        if ($scope.invoiceValueSetting == true) {
            if (parseFloat($scope.vendorPurchase.initialValue) >= 0) {
                if (($scope.vendorPurchase.grossValue < (parseFloat($scope.vendorPurchase.initialValue) - $scope.invoiceValueDifference)) || ($scope.vendorPurchase.grossValue > (parseFloat($scope.vendorPurchase.initialValue) + $scope.invoiceValueDifference))) {
                    ShowConfirmMsgWindow('"Invoice Value" mismatches with "Net Value".Invoice Difference value is Rs.' + Math.abs(parseFloat($scope.vendorPurchase.initialValue) - $scope.vendorPurchase.grossValue) + '. Allowed value is + or -' + $scope.invoiceValueDifference + ' Rs.');
                    return false;
                }
            } else {
                ShowConfirmMsgWindow('Please enter "Invoice Value".');
                return false;
            }
        }

        var str = "";
        var count = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId == null || $scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId == undefined || $scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId == "") {
                str += "<li>" + $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name + "</li>";
                count++;
            }
        }
        if ($scope.purchaseImportFromExcel == true) {
            if ($scope.taxValuesList.length > 0) {
                for (var j = 0; j < $scope.vendorPurchase.vendorPurchaseItem.length; j++) {
                    var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.vendorPurchase.vendorPurchaseItem[j].gstTotal) }, true);
                    if (taxList.length == 0) {
                        toastr.info("Please provide valid GST % and try again." + " Product Name : " + $scope.vendorPurchase.vendorPurchaseItem[j].selectedProduct.name);
                        return;
                    }
                }
            }
        }
        if ($scope.enablePopup == false) {
            if (count > 0) {
                var htmlContent = "<ul>" + str + "</ul>";
                $scope.enablePopup = true;
                $.confirm({
                    title: "Below are new products, do you want to add automatically?",
                    content: htmlContent,
                    closeIcon: function () {
                        $scope.enablePopup = false;
                    },
                    buttons: {
                        yes: {
                            text: 'yes [y]',
                            btnClass: 'primary-button',
                            keys: ['y'],
                            action: function () {
                                prePurchaseCreate();
                            }
                        },
                        no: {
                            text: 'no [n]',
                            btnClass: 'secondary-button',
                            keys: ['n'],
                            action: function () {
                                return;
                            }
                        }
                    }
                });
            }
            else {
                prePurchaseCreate();
            }
        } else {
            $scope.enablePopup = false;
            return false;
        }

    };

    function prePurchaseCreate() {
        $scope.netHighlight = "";
        var checkNetTotal = 0;
        if ($scope.vendorPurchase.initialValue) {
            //checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
            checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.netTotal);
            if (checkNetTotal <= 1 && checkNetTotal >= -1) {
                $scope.netHighlight = "";
                purchaseCreate();
            } else {
                var cancel = window.confirm('Are you sure, Do you want to Update Stock?');
                if (cancel) {
                    purchaseCreate();
                } else {
                    $.LoadingOverlay("hide");
                    $scope.netHighlight = "highlight";
                    $scope.isProcessing = false;
                }
            }
        } else {
            purchaseCreate();
        }
    };

    $scope.SaveProcessing = false;
    function purchaseCreate() {

        //if ($scope.vendorPurchase.selectedVendor.locationType == 3 || $scope.vendorPurchase.selectedVendor.locationType == 4) {
        //    $scope.vendorPurchaseItem.productStock.gstTotal = 0;
        //    $scope.vendorPurchaseItem.productStock.igst = 0;
        //    $scope.vendorPurchaseItem.productStock.cgst = 0;
        //    $scope.vendorPurchaseItem.productStock.sgst = 0;
        //}



        if ($scope.mode == "EDIT") {
            $scope.vendorPurchase.updatePaymentTotal = $scope.vendorPurchase.grossValue;
        }
        if ($scope.vendorPurchase.noteAmount == "" || $scope.vendorPurchase.noteAmount == null) {
            $scope.vendorPurchase.noteAmount = 0;
        }

        $scope.vendorPurchase.totalPurchaseValue = $scope.vendorPurchase.total;
        $scope.vendorPurchase.discountValue = $scope.vendorPurchase.discountInRupees;
        $scope.vendorPurchase.vatValue = $scope.vendorPurchase.tax;
        $scope.vendorPurchase.roundOffValue = $scope.roundedAmount;
        // $scope.vendorPurchase.netValue = $scope.vendorPurchase.grossValue;
        $scope.vendorPurchase.netValue = $scope.purchaseAmount - $scope.returnAmount;
        // Added by Gavaskar 05-09-2017 Total Return Amount
        $scope.vendorPurchase.returnAmount = $scope.returnAmount;
        $scope.vendorPurchase.totalDiscountValue = $scope.vendorPurchase.overAllDiscount


        //if (!$scope.draftLoaded) {
        //    $scope.vendorPurchase.draftName = null;
        //} else {
        //    $scope.vendorPurchase.draftVendorPurchaseId = $scope.vendorPurchase.id;
        //}
        //delete $scope.vendorPurchase.draftVendorPurchaseItem;
        $scope.chkAddStock = false;
        $scope.chkZeroPrice = false;
        $scope.chkSellingPrice = false;
        $scope.chkExpiryDt = false;
        var addStockMsg = "";
        var tmpPurchaseDetail = $scope.vendorPurchase;
        for (var i = 0; i < tmpPurchaseDetail.vendorPurchaseItem.length; i++) {
            if (!tmpPurchaseDetail.vendorPurchaseItem[i].isDeleted) {
                if (tmpPurchaseDetail.vendorPurchaseItem[i].fromDcId == null) {
                    if (tmpPurchaseDetail.vendorPurchaseItem[i].packagePurchasePrice == 0 || tmpPurchaseDetail.vendorPurchaseItem[i].packageSellingPrice == 0) {
                        if ($scope.enableSelling == true) {
                            if (!$scope.chkZeroPrice) {
                                addStockMsg += " Selling and Cost price must be greater than zero.";
                                $scope.chkZeroPrice = true;
                            }
                        } else {
                            if (!$scope.chkZeroPrice) {
                                addStockMsg += " MRP and Cost price must be greater than zero.";
                                $scope.chkZeroPrice = true;
                            }
                        }
                    }
                    if ($scope.enableSelling == true && tmpPurchaseDetail.vendorPurchaseItem[i].packageMRP == 0) {
                        if (!$scope.chkZeroPrice) {
                            addStockMsg += " MRP must be greater than zero.";
                            $scope.chkZeroPrice = true;
                        }
                    }
                    if ($scope.enableSelling == true && parseFloat(tmpPurchaseDetail.vendorPurchaseItem[i].packageMRP) < parseFloat(tmpPurchaseDetail.vendorPurchaseItem[i].packageSellingPrice)) {
                        if (!$scope.chkZeroPrice) {
                            addStockMsg += " MRP must be greater than Selling Price.";
                            $scope.chkZeroPrice = true;
                        }
                    }
                    if (parseFloat(getFormulaNetValue(tmpPurchaseDetail.vendorPurchaseItem[i].packagePurchasePrice, tmpPurchaseDetail.vendorPurchaseItem[i], "")) > parseFloat(tmpPurchaseDetail.vendorPurchaseItem[i].packageSellingPrice)) {
                        if (!$scope.chkSellingPrice) {
                            tmpPurchaseDetail.vendorPurchaseItem[i].isValid = false;
                            $scope.chkSellingPrice = true;
                            if ($scope.enableSelling == true) {
                                addStockMsg += " Selling Price must be greater than Cost Price.";
                            }
                            else {
                                addStockMsg += " MRP must be greater than Cost Price.";
                            }
                        }
                    }

                    // Added by Gavaskar 15-09-2019 Expiry Product validation remove
                    if (!tmpPurchaseDetail.vendorPurchaseItem[i].vendorReturn) {
                        if (tmpPurchaseDetail.vendorPurchaseItem[i].productStock.expireDate != null) {
                            $scope.dayDiff(tmpPurchaseDetail.vendorPurchaseItem[i].productStock.expireDate);
                        }

                        if (!$scope.chkPODate && !$scope.chkExpiryDt) {
                            $scope.chkExpiryDt = true;
                            addStockMsg += " Expire Date must be more than 30 days.";
                        }
                    }
                }
            }
        }
        if ($scope.chkExpiryDt || $scope.chkSellingPrice || $scope.chkZeroPrice) {
            toastr.info(addStockMsg);
            $.LoadingOverlay("hide");
            $scope.isProcessing = false;
        } else {
            if ($scope.SaveProcessing == false) {
                $.LoadingOverlay("show");
                $scope.isProcessing = true;
                $scope.SaveProcessing = true;

                if (typeof ($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON) == "object") {
                    $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON = JSON.stringify($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON);
                }
                $scope.vendorPurchase.vendor = $scope.vendorPurchase.selectedVendor;
                $scope.vendorPurchase.vendorPurchaseFormulaId = $scope.vendorPurchase.vendor.vendorPurchaseFormula.id;
                $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;

                if ($scope.editModeVendorId != $scope.vendorPurchase.vendorId && $scope.mode == "EDIT") {
                    $scope.vendorPurchase.vendorChangedInEdit = true;
                }
                var index = -1;
                for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
                    $scope.vendorPurchase.vendorPurchaseItem[i].vendorPurchaseItemSno = i;
                }

                if ($scope.mode == "NEW") {
                    vendorPurchaseService.create($scope.vendorPurchase)
                        .then(function (response) {
                            $scope.SaveProcessing = false;
                            $scope.list = response.data;
                            $.LoadingOverlay("hide");
                            if ($scope.list.saveFailed) {
                                toastr.error($scope.list.errorLog.errorMessage, 'Error');
                                $scope.isProcessing = false;
                                $scope.SaveProcessing = false;
                            }
                            else if ($scope.scanBarcodeOption.scanBarcode === "1" && $scope.BarcodeGenerationIsEnabled == true) {
                                var data = {
                                    msgTitle: "",
                                    msg: "Do you wish to print barcode stickers?",
                                    showOk: false,
                                    showYes: true,
                                    showNo: true,
                                    showCancel: false,
                                    focusOnNo: false
                                };
                                var m = ModalService.showModal({
                                    "controller": "showConfirmMsgCtrl",
                                    "templateUrl": 'showConfirmMsgModal',
                                    "inputs": { "params": [{ "data": data }] },
                                }).then(function (modal) {
                                    modal.element.modal();
                                    modal.close.then(function (result) {
                                        if (result == "Yes") {
                                            var grnNo = "";
                                            if ($scope.list.prefix != null && $scope.list.prefix != "") {
                                                grnNo = grnNo + $scope.list.prefix;
                                            }
                                            if ($scope.list.billSeries != null && $scope.list.billSeries != "") {
                                                grnNo = grnNo + $scope.list.billSeries;
                                            }
                                            if ($scope.list.goodsRcvNo != null && $scope.list.goodsRcvNo != "") {
                                                grnNo = grnNo + $scope.list.goodsRcvNo;
                                            }
                                            window.localStorage.setItem('loadPurchaseBarcode', grnNo);
                                            window.location.assign('/ProductStock/BarcodeGeneration');
                                        } else {
                                            reset();
                                        }
                                    });
                                });
                            } else {
                                reset();
                            }

                            //$scope.HideDraftEditLabel = true;
                            //$scope.getAlldrafts();
                        }, function (response) {
                            $scope.responses = response;
                            $.LoadingOverlay("hide");
                            var msg = response.data.value.errorDesc;
                            var invoiceNo = "Invoice Number Already Exist";
                            if (msg.indexOf(invoiceNo) != -1)
                                toastr.info(response.data.value.errorDesc, 'Info..');
                            else
                                toastr.error(response.data.value.errorDesc, 'Error');
                            $scope.isProcessing = false;
                            $scope.SaveProcessing = false;
                            var ele = document.getElementById("initialValue");
                            ele.focus();
                        });
                }
                else if ($scope.mode == "EDIT") {
                    vendorPurchaseService.update($scope.vendorPurchase).then(function (response) {
                        $scope.SaveProcessing = false;
                        if (response.data.saveFailed) {
                            toastr.error(response.data.errorLog.errorMessage, 'Error');
                            $scope.isProcessing = false;
                            $scope.SaveProcessing = false;
                        }
                        else if (response.data.errorMessage == "") {
                            toastr.success('Stock updated successfully');
                            window.location.assign('/vendorPurchase/List');
                            $.LoadingOverlay("hide");
                        } else {
                            $.LoadingOverlay("hide");
                            toastr.error(response.data.errorMessage, 'Error');
                            $scope.isProcessing = false;
                        }
                    }, function (error) {
                        $.LoadingOverlay("hide");
                        toastr.error(error.data.errorDesc, 'Error');
                        $scope.isProcessing = false;
                        $scope.SaveProcessing = false;
                    });
                }
            }
        }
    }
    //$scope.DeleteDraft = function (draftVendorPurchaseId) {
    //    vendorPurchaseService.DeleteDraft(draftVendorPurchaseId).then(function (resp) {
    //    }, function (error) { console.log(error); });
    //};
    $scope.initialValue = function () {
        if ($scope.vendorPurchase.initialValue == "")
            $scope.vendorPurchase.initialValue = null;
        //var checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
        var checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.netTotal);
        if (checkNetTotal <= 1 && checkNetTotal >= -1) {
            $scope.netHighlight = "";
        } else {
            if ($scope.vendorPurchase.total + $scope.vendorPurchase.tax > 0 && $scope.vendorPurchase.initialValue != null) {
                $scope.netHighlight = "highlight";
            } else {
                $scope.netHighlight = "";
            }
        }
    };

    function reset() {
        $scope.vendorPurchaseItems.$setPristine();
        toastr.success('Stock added successfully');
        $scope.disableAddNewDc = false;
        $scope.SelectedTaxtype = "";
        //$scope.productStockSearch();
        $scope.vendorPurchase.selectedVendor = {};
        var iDate = new Date();
        $scope.vendorPurchase = { total: 0, discount: 0, vendorPurchaseItem: [], invoiceDate: iDate.toISOString() };
        $scope.minDate = new Date();
        var d = new Date();
        $scope.isProcessing = false;
        if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
            document.getElementById("vat").disabled = false;
            document.getElementById("cST").disabled = true;
            $scope.vendorPurchaseItem.productStock.cST = 0;
        }

        //$scope.draftLoaded = false;
        $.LoadingOverlay("hide");


        if ($scope.draftLoaded) {
            window.localStorage.removeItem('p_data');
            $scope.draftCount = 0;
        }
        getGoodRNO();
        $scope.SelectedFileForUpload = null;
        $scope.roundedAmount = 0;
        $scope.purchaseAmount = 0;
        $scope.returnAmount = 0;
        $scope.returnRoundedAmount = 0;
        $scope.previouspurchases = [];
        $scope.vendorPurchase.paymentType = "Credit";
        $scope.vendorPurchase.noteType = "credit";
        ClearDiscountFields();
        var ele = document.getElementById("initialValue");
        ele.focus();
        $scope.purchaseImportFromExcel = false;
    };

    $scope.vendor = function () {
        if ($scope.mode == "EDIT") {
            $scope.vendorStatus = 2;
        }
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) {
            $scope.vendorList = response.data;
            $scope.vendorList = $filter('orderBy')($scope.vendorList, 'name');
            if ($scope.mode == "EDIT") {
                var temp = $filter("filter")($scope.vendorList, { "id": $scope.editModeVendorId })[0];
                $scope.vendorList = $filter("filter")($scope.vendorList, { "status": 1 });
                $scope.vendorList.push(temp);
            }
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };

    //$scope.getSearchType = function () {
    //    vendorPurchaseService.getSearch().then(function (response1) {
    //        if (response1.data != null && response1.data != '') {
    //            $scope.searchType = response1.data;
    //        }
    //    }, function (error) {
    //        console.log(error);
    //    });
    //};
    //$scope.getSearchType();
    $scope.getProducts = function (val) {
        if ($scope.scanBarcodeOption.scanBarcode === "1") {
            if (isNaN(val))
                $scope.isItScan = false;
            else
                $scope.isItScan = true;
            if (val.length > 3 && !isNaN(val))
                return;
        }
        return productService.nonHiddenProductList(val).then(function (response) {
            //     var flags = [], output = [], l = response.data.length, i;
            //     for (i = 0; i < l; i++) {
            //         if (flags[$filter('uppercase')(response.data[i].name)]) continue;
            //         flags[$filter('uppercase')(response.data[i].name)] = true;
            //         output.push(response.data[i]);
            //     }
            //     console.log(JSON.stringify(output));
            //     var origArr = output;
            //     var newArr = [],
            //origLen = origArr.length,
            //found, x, y;
            //     for (x = 0; x < origLen; x++) {
            //         found = undefined;
            //         for (y = 0; y < newArr.length; y++) {
            //             if (origArr[x].name === newArr[y].name) {
            //                 found = true;
            //                 break;
            //             }
            //         }
            //         if (!found) {
            //             newArr.push(origArr[x]);
            //         }
            //     }
            //     console.log(JSON.stringify(newArr));
            //     return newArr.map(function (item) {
            //         return item;
            //     });
            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                item.name = item.name.trim();
                return item;
            });
        });
    };
    $scope.onVendorSelect = function () {
        window.setTimeout(function () {
            var ele = document.getElementById("invoiceNo");
            ele.focus();
        }, 0);
    };
    function fillTempData(tempPurchaseItem) {
        $.LoadingOverlay("show");
        vendorPurchaseService.getPurchaseValues($scope.vendorPurchaseItem.productStock.product.id, $scope.vendorPurchaseItem.productStock.product.name)
            .then(function (response) {
                $.LoadingOverlay("hide");
                //Assigning TempVendorPurchaseItem to VendorPurchaseItem object of this view
                $scope.vendorPurchaseItem = response.data.vendorPurchaseItem;
                $scope.vendorPurchaseItem.productStock.batchNo = tempPurchaseItem.productStock.batchNo;
                $scope.vendorPurchaseItem.productStock.expireDate = tempPurchaseItem.productStock.expireDate;
                $scope.vendorPurchaseItem.productStock.vAT = tempPurchaseItem.productStock.vAT;
                $scope.vendorPurchaseItem.productStock.hsnCode = tempPurchaseItem.productStock.hsnCode
                $scope.vendorPurchaseItem.productStock.gstTotal = tempPurchaseItem.productStock.gstTotal;

                //GST related fields start here
                //$scope.vendorPurchaseItem.productStock.igst = tempPurchaseItem.productStock.igst;
                //$scope.vendorPurchaseItem.productStock.cgst = tempPurchaseItem.productStock.cgst;
                //$scope.vendorPurchaseItem.productStock.sgst = tempPurchaseItem.productStock.sgst;
                //$scope.vendorPurchaseItem.productStock.gstTotal = tempPurchaseItem.productStock.gstTotal;
                //GST related fields end here

                //if ($scope.vendorPurchase.selectedVendor.locationType == 1) {
                //    $scope.vendorPurchaseItem.productStock.igst = "";
                //} else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
                //    $scope.vendorPurchaseItem.productStock.cgst = "";
                //    $scope.vendorPurchaseItem.productStock.sgst = "";
                //} else {
                //    $scope.vendorPurchaseItem.productStock.igst = "";
                //    $scope.vendorPurchaseItem.productStock.cgst = "";
                //    $scope.vendorPurchaseItem.productStock.sgst = "";
                //}

                //igst = parseFloat($scope.vendorPurchaseItem.productStock.igst) || 0;
                //cgst = parseFloat($scope.vendorPurchaseItem.productStock.cgst) || 0;
                //sgst = parseFloat($scope.vendorPurchaseItem.productStock.sgst) || 0;
                //$scope.vendorPurchaseItem.productStock.gstTotal = igst + cgst + sgst;
                $scope.vendorPurchaseItem.packageSize = tempPurchaseItem.packageSize;
                // Modified by Gavaskar Tempstock sales after 23-08-2017 Start
                //if (tempPurchaseItem.productStock.stock == null || tempPurchaseItem.productStock.stock == 0 || tempPurchaseItem.productStock.stock == undefined)
                //{
                $scope.vendorPurchaseItem.packageQty = tempPurchaseItem.packageQty;
                //}
                //else
                //{
                //    $scope.vendorPurchaseItem.packageQty = tempPurchaseItem.productStock.stock / tempPurchaseItem.packageQty;
                //}
                // Modified by Gavaskar Tempstock sales after 23-08-2017 End
                $scope.vendorPurchaseItem.packagePurchasePrice = tempPurchaseItem.packagePurchasePrice;
                $scope.vendorPurchaseItem.productStock.product.rackNo = tempPurchaseItem.productStock.product.rackNo; //Addedby Poongodi on 22/03/2017
                $scope.vendorPurchaseItem.packageSellingPrice = tempPurchaseItem.packageSellingPrice;
                $scope.vendorPurchaseItem.packageMRP = tempPurchaseItem.packageMRP;
                $scope.vendorPurchaseItem.fromTempId = tempPurchaseItem.fromTempId;
                $scope.vendorPurchaseItem.tempItem = tempPurchaseItem.tempItem;
                $scope.vendorPurchaseItem.freeQty = "";
                var date2 = new Date($scope.vendorPurchaseItem.productStock.expireDate);
                if (date2 < (d.setMonth(d.getMonth() + 1))) {
                    $scope.dayDiff($scope.vendorPurchaseItem.productStock.expireDate);
                } else {
                    $scope.highlight = "";
                    $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", true);
                    $scope.isFormValid = true;
                }
                //if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
                //var cst = document.getElementById("cST");
                //var vat = document.getElementById("vat");
                //$scope.vendorPurchaseItem.productStock.cST = 0;
                //cst.disabled = true;
                //$scope.vendorPurchase.TaxationType = "VAT";
                //}
                $scope.vendorPurchaseItem.discount = "";
                document.getElementById("packageQuantity").focus();
                //$scope.HideDraftEditLabel = true;

                $scope.changeGST($scope.vendorPurchaseItem.productStock.gstTotal);

            }, function (error) {
                $.LoadingOverlay("hide");
                console.log(error);
            });
    }
    $scope.lastPurchasePrice = 0;
    function fillProductData(name) {
        $.LoadingOverlay("show");
        vendorPurchaseService.getPurchaseValues($scope.vendorPurchaseItem.productStock.productId, name)
            .then(function (response) {
                $scope.vendorPurchaseItem = response.data.vendorPurchaseItem;
                if (!$scope.enableSelling)
                    $scope.vendorPurchaseItem.packageMRP = $scope.vendorPurchaseItem.packageSellingPrice;


                if (($scope.GSTEnabled && $scope.mode == "NEW") || ($scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {

                    $scope.vendorPurchaseItem.productStock.gstTotal = parseFloat($scope.vendorPurchaseItem.productStock.gstTotal);
                    var ngst = $filter("filter")($scope.taxValuesList, { "tax": $scope.vendorPurchaseItem.productStock.gstTotal }, true);

                    if (ngst.length == 0) {
                        $scope.vendorPurchaseItem.productStock.gstTotal = null;
                        $scope.vendorPurchaseItem.gstTotal = null;
                    }

                    igst = Number($scope.vendorPurchaseItem.productStock.igst) || 0;
                    cgst = Number($scope.vendorPurchaseItem.productStock.cgst) || 0;
                    sgst = Number($scope.vendorPurchaseItem.productStock.sgst) || 0;

                    if ($scope.vendorPurchase.selectedVendor.locationType == 1) {
                        $scope.vendorPurchaseItem.productStock.igst = "";
                        igst = 0;
                    } else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
                        $scope.vendorPurchaseItem.productStock.igst = $scope.vendorPurchaseItem.productStock.gstTotal;
                        igst = $scope.vendorPurchaseItem.productStock.gstTotal;
                        $scope.vendorPurchaseItem.productStock.cgst = "";
                        cgst = 0;
                        $scope.vendorPurchaseItem.productStock.sgst = "";
                        sgst = 0;


                    } else {
                        //$scope.vendorPurchaseItem.productStock.gstTotalRef = $scope.vendorPurchaseItem.productStock.gstTotal;
                        //$scope.vendorPurchaseItem.productStock.gstTotal = "";

                        $scope.vendorPurchaseItem.productStock.igst = "";
                        igst = 0;
                        $scope.vendorPurchaseItem.productStock.cgst = "";
                        cgst = 0;
                        $scope.vendorPurchaseItem.productStock.sgst = "";
                        sgst = 0;
                    }


                    //$scope.vendorPurchaseItem.productStock.gstTotal = igst + cgst + sgst;



                } else {

                    if (response.data.vendorPurchaseItem.productStock.vat == undefined) {
                        $scope.vendorPurchaseItem.productStock.vAT = "";
                    } else {
                        $scope.vendorPurchaseItem.productStock.vAT = response.data.vendorPurchaseItem.productStock.vat;
                    }
                }


                $scope.vendorPurchaseItem.packageQty = $scope.vendorPurchaseItem.packageQty - $scope.vendorPurchaseItem.freeQty;
                var date2 = new Date($scope.vendorPurchaseItem.productStock.expireDate);
                if (date2 < (d.setMonth(d.getMonth() + 1))) {
                    $scope.dayDiff($scope.vendorPurchaseItem.productStock.expireDate);
                } else {
                    $scope.highlight = "";
                    $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", true);
                    $scope.isFormValid = true;
                }

                if ($scope.vendorPurchaseItem.productStock.product.rackNo == undefined || $scope.vendorPurchaseItem.productStock.product.rackNo == null) {
                    $scope.vendorPurchaseItem.productStock.product.rackNo = "";
                }

                if (!$scope.GSTEnabled || (!$scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
                    //var cst = document.getElementById("cST");
                    //var vat = document.getElementById("vat");
                    $scope.vendorPurchaseItem.productStock.vAT = 0;
                    $scope.vendorPurchaseItem.productStock.cST = 0;
                    if ($scope.vendorPurchase.selectedVendor != undefined && $scope.vendorPurchase.selectedVendor.enableCST) {
                        //vat.disabled = false;
                        //cst.disabled = false;
                        $scope.vendorPurchase.TaxationType = "CST";
                        //$scope.vendorPurchaseItem.productStock.cST = 2;
                    } else {
                        //vat.disabled = false;
                        //cst.disabled = true;
                        $scope.vendorPurchase.TaxationType = "VAT";
                    }
                }



                if ($scope.vendorPurchase.selectedVendor.discount) {
                    $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
                    if ($scope.vendorPurchaseItem.discount == 0) {
                        $scope.vendorPurchaseItem.discount = "";
                    }
                } else {
                    $scope.vendorPurchaseItem.discount = "";
                }
                if ($scope.vendorPurchaseItem.freeQty) {
                    if ($scope.vendorPurchaseItem.freeQty == 0) {
                        $scope.vendorPurchaseItem.freeQty = "";
                    }
                } else {
                    $scope.vendorPurchaseItem.freeQty = "";
                }
                $scope.previouspurchases = response.data.getPreviousPurchase;
                if ($scope.previouspurchases != undefined) {
                    if ($scope.previouspurchases.length > 0) {
                        $scope.toggleProductDetail();
                        $scope.lastPurchasePrice = $scope.previouspurchases[0].packagePurchasePrice;
                    }
                }
                $.LoadingOverlay("hide");
            }, function (error) {
                $.LoadingOverlay("hide");
                toastr.error(error.data.errorDesc, 'Error');
            });
    }
    $scope.onProductSelect = function ($event, selectedProduct1) {
        if (selectedProduct1 != null) {
            $scope.vendorPurchaseItem.productStock.productId = selectedProduct1.id;
            $scope.vendorPurchaseItem.productStock.product = selectedProduct1;

            var productId = selectedProduct1.id;
            // Product Restriction by Gavaskar 14-09-2017 Start 
            for (var k = 0; k < $scope.vendorPurchase.vendorPurchaseItem.length; k++) {
                if (($scope.vendorPurchase.vendorPurchaseItem[k].productStock.product.id == selectedProduct1.id)) { //Added by Sarubala on 27-09-17
                    if ($scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn != null && $scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn != undefined) {
                        if ($scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn == true) {
                            toastr.info("Product already added in Return");
                            document.getElementById("drugName").focus();
                            return;
                        }
                    }
                }
            }
            // Product Restriction by Gavaskar 14-09-2017 End 
            vendorPurchaseService.isTempPurchaseItemAvail(productId)
          .then(function (resp) {
              bReturn = resp.data;
              if (bReturn) {
                  $scope.showTempStockScreen(productId);
              } else {
                  if (!$scope.purchaseImportFromExcel) {
                      fillProductData(selectedProduct1.name);
                  }
              }
          }, function (response) {
              $scope.responses = response;
              console.log(error);
              toastr.error('Error Occured', 'Error');
          });
        }
    };
    $scope.modelOptions = {
        debounce: {
            default: 500,
            blur: 250
        },
        getterSetter: true
    };
    $scope.cancel = function () {
        if ($scope.draftLoaded) {
            var alert = window.confirm("Loaded Draft Will Be Deleted.");
            if (alert) {
                window.localStorage.removeItem('p_data');
                $scope.draftCount = 0;
                window.location.assign("/vendorPurchase/index");
            }
        } else {
            var cancel = window.confirm('Are you sure, Do you want to Cancel?');
            if (cancel) {
                if ($scope.mode == "EDIT") {
                    window.location.assign("/VendorPurchase/list");
                }
                else {
                    window.location.assign("/vendorPurchase/index");
                }
            }
        }
    };
    $scope.editCheque = function () {
        if ($scope.vendorPurchase.paymentType == "Credit") {
            $scope.vendorPurchase.chequeNo = null;
            $scope.vendorPurchase.chequeDate = null;
        } else {
            $scope.vendorPurchase.creditNoOfDays = null;
        }
    };
    $scope.currency = function (N) {
        N = parseFloat(N);
        if (!isNaN(N)) {
            N = N.toFixed(2);
        } else {
            N = '0.00';
        }
        return N;
    };
    $scope.focusVendor = function (event) {
        if (event.which === 13) { // Enter key
            $("#vendor").trigger('chosen:open');
        }
    };
    //$scope.focusVat = function (event) {
    //    if (event.which === 13) { // Enter key
    //        if ($scope.SaveTaxSeries == 1) {
    //            var ele = document.getElementById("vat");
    //            ele.focus();
    //            event.stopPropagation();
    //            return false;
    //        } else {
    //            //var ele = document.getElementById("txtRackNo");
    //            //ele.focus();
    //            //event.preventDefault();
    //        }
    //    }
    //};
    //$scope.keyEnter = function (event, e, previousid, currentid) {
    //    var ele = document.getElementById(e);
    //    //Enterkey
    //    if (event.which === 13) {
    //        if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) // if the adjacent sibling exists set focus
    //        {
    //            ele.focus();
    //            if (ele.nodeName != "BUTTON" && e !== "eancode") {
    //                ele.select();
    //                event.preventDefault();
    //            }
    //        }
    //    }
    //};

    $scope.keyEnter = function (event, nextid, currentid) {
        if (event.which === 13) {

            var ele = document.getElementById(nextid);

            //if (currentid == "discount") {

            //    if (document.getElementById(nextid).disabled) {

            //        if (($scope.GSTEnabled && $scope.mode == "NEW") || ($scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {

            //            ele = document.getElementById("hsnCode");
            //        } else {
            //            ele = document.getElementById("eancode");
            //        }
            //    } else {
            //        ele = document.getElementById(nextid);
            //    }

            //}
            //if (currentid == "gstTotal") {

            //    if ($scope.vendorPurchaseItem.productStock.gstTotal == "" || $scope.vendorPurchaseItem.productStock.gstTotal == undefined) {
            //        return false;
            //    }




            //    if ($scope.scanBarcodeOption.scanBarcode != '1') {
            //        ele = document.getElementById("addStock");
            //    } else {
            //        ele = document.getElementById(nextid);
            //    }
            //}

            //if (currentid == "hsnCode") {
            //    if (!$scope.isIGST) {
            //        ele = document.getElementById("cgst");
            //    } else {
            //        ele = document.getElementById(nextid);
            //    }
            //    if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == 2) {
            //        ele = document.getElementById("gstTotal");
            //    } else {

            //        if ($scope.scanBarcodeOption.scanBarcode != "1") {
            //            ele = document.getElementById("addStock");
            //        } else {
            //            ele = document.getElementById("eancode");
            //        }


            //    }

            //    //if ($scope.vendorPurchase.selectedVendor.locationType == 3 || $scope.vendorPurchase.selectedVendor.locationType == 4) {
            //    //    ele = document.getElementById("eancode");
            //    //}
            //}
            //if (currentid == "igst") {
            //    if ($scope.vendorPurchaseItem.productStock.igst == "" || $scope.vendorPurchaseItem.productStock.igst == "0" || $scope.vendorPurchaseItem.productStock.igst == undefined) {
            //        return false;
            //    }
            //    if (!$scope.isCGST) {
            //        ele = document.getElementById("eancode");
            //    } else {
            //        ele = document.getElementById("cgst");
            //    }
            //}

            //if (currentid == "cgst") {
            //    if ($scope.vendorPurchaseItem.productStock.cgst == "" || $scope.vendorPurchaseItem.productStock.cgst == "0" || $scope.vendorPurchaseItem.productStock.cgst == undefined) {
            //        return false;
            //    }
            //    ele = document.getElementById(nextid);
            //}
            //if (currentid == "sgst") {
            //    if ($scope.vendorPurchaseItem.productStock.sgst == "" || $scope.vendorPurchaseItem.productStock.sgst == "0" || $scope.vendorPurchaseItem.productStock.sgst == undefined) {
            //        return false;
            //    }
            //    ele = document.getElementById(nextid);
            //}




            if (ele.disabled) {
                var id = ele.getAttribute("nextid");
                if (id != null) {
                    $scope.keyEnter(event, id, nextid);
                }
            }
            else {
                ele.focus();
            }
        }
    };
    $scope.draftName = "";
    $scope.vendorPurchaseArray = [];
    $scope.changeDraftName = function () {
        saveDraftModal();
    };
    $scope.getAlldrafts = function () {
        //vendorPurchaseService.getAlldrafts().then(function (resp) {
        vendorPurchaseService.GetDraftsByInstance().then(function (resp) {
            $scope.vendorPurchaseArray = resp.data;
            $scope.draftCount = $scope.vendorPurchaseArray.length;
            for (var i = 0; i < $scope.vendorPurchaseArray.length; i++) {
                $scope.vendorPurchaseArray[i].vendorPurchaseItem = $scope.vendorPurchaseArray[i].draftVendorPurchaseItem;
                $scope.vendorPurchaseArray[i].TaxationType = "VAT";
            }
            //for (var i = 0; i < $scope.vendorPurchaseArray.length; i++) {
            //    $scope.vendorPurchaseArray[i].vendorPurchaseItem = $scope.vendorPurchaseArray[i].draftVendorPurchaseItem;
            //    $scope.vendorPurchaseArray[i].TaxationType = "VAT";
            //    for (var k = 0; k < $scope.vendorPurchaseArray[i].vendorPurchaseItem.length; k++) {
            //        $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].productStock.productId = $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].productId;
            //        //Assign Selected Product
            //        $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].selectedProduct = $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].productStock.product;
            //        //Package Calculation
            //        $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].discountValue = 0;
            //        $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].orgDiscount = $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].discount;
            //        $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].productStock.tax = $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].productStock.vat;
            //        $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].orderedQty = $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].packageQty - $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].freeQty || 0;
            //        if ($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].freeQty > 0) {
            //            $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].packageQty = Number($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].packageQty);
            //        }
            //        if ($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].discount > 0) {
            //            $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].discountValue = ($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].packagePurchasePrice * $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].orderedQty) * ($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].discount / 100);
            //        }
            //        $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].total = ($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].packagePurchasePrice * $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].orderedQty) - $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].discountValue;
            //        if ($scope.vendorPurchaseArray[i].TaxationType == "VAT") {
            //            $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].tax = ($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].total) * ($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].productStock.vat / 100);
            //        }
            //        if ($scope.vendorPurchaseArray[i].TaxationType == "CST") {
            //            $scope.vendorPurchaseArray[i].vendorPurchaseItem[k].tax = ($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].total) * ($scope.vendorPurchaseArray[i].vendorPurchaseItem[k].productStock.cST / 100);
            //        }
            //    }
            //}
        }, function (error) {
            console.log(error);
        });
    };
    //  $scope.getAlldrafts();
    function saveCurrentDraft() {
        if ($scope.vendorPurchase.vendorPurchaseItem.length != 0) {
            $scope.draftvendorPurchase = $scope.vendorPurchase;
            $scope.draftvendorPurchase.draftVendorPurchaseItem = $scope.vendorPurchase.vendorPurchaseItem;
            $scope.draftvendorPurchase.draftAddedOn = new Date();
            if (!$scope.draftLoaded) {
                var currentdatetime = new Date();
                var milliSecond = currentdatetime.getMilliseconds();
                $scope.draftvendorPurchase.draftName = "draft" + milliSecond;
            }
            if ($scope.draftLoaded) {
                $scope.draftvendorPurchase.draftName = $scope.vendorPurchase.draftName;
            }
        }
    }
    //$scope.$on('onBeforeUnload', function (e, confirmation) {
    //    confirmation.message = "Draft Will Be Replaced With New One.";
    //    e.preventDefault();
    //});
    //$scope.$on('onUnload', function (e) {
    //    console.log("leaving"); 
    //});
    window.onbeforeunload = function () {
        saveDraft();
        //delete $scope.draftvendorPurchase.vendorPurchaseItem;
        //if ($scope.draftvendorPurchase.draftVendorPurchaseItem.length > 0) {
        //    //Adding Draft
        //    if ($scope.tempStockLoaded && $scope.nonTempItemCount > 0) {
        //        vendorPurchaseService.savedDraft($scope.draftvendorPurchase).then(function (response) {
        //            $scope.draftvendorPurchase = draftVendorPurchaseModel;
        //        }, function (error) {
        //            console.log(error);
        //        });
        //    } else if (!$scope.draftLoaded && !$scope.tempStockLoaded) {
        //        //Adding Draft
        //        vendorPurchaseService.savedDraft($scope.draftvendorPurchase).then(function (response) {
        //            $scope.draftvendorPurchase = draftVendorPurchaseModel;
        //        }, function (error) {
        //            console.log(error);
        //        });
        //    } else if ($scope.draftLoaded && $scope.draftvendorPurchase.draftVendorPurchaseItem.length != 0) {
        //        //Updating Draft
        //        vendorPurchaseService.updateDraft($scope.draftvendorPurchase).then(function (response) {
        //            $scope.draftvendorPurchase = draftVendorPurchaseModel;
        //        }, function (error) {
        //            console.log(error);
        //        });
        //    }
        //}
    };
    function saveDraftModal() {
        var m = ModalService.showModal({
            "controller": "saveDraftCtrl",
            "templateUrl": "saveDraftTemplate",
            "inputs": {
                "draftName": $scope.draftvendorPurchase.draftName
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result != "Cancel") {
                    $scope.draftvendorPurchase.draftName = result;
                }
            });
        });
    }
    $scope.draftLoaded = false;
    $scope.tempStockLoaded = false;
    function saveDraft() {
        if ($scope.mode == "NEW") {
            if ($scope.vendorPurchase.vendorPurchaseItem.length != 0) {




                window.localStorage.setItem("p_data", JSON.stringify($scope.vendorPurchase));
                $scope.draftCount = 1;
            }
        }
    }
    //$scope.loadDraft = function () {
    //    if (window.localStorage.getItem("p_data") == null) {
    //        $scope.draftCount = 0;
    //        $scope.draftLoaded = false;
    //    } else {
    //        $scope.draftLoaded = true;
    //        $scope.vendorPurchase = JSON.parse(window.localStorage.getItem("p_data"));
    //        $scope.draftCount = 1;
    //    }
    //    $scope.updateUI();
    //    getGoodRNO();
    //    //if (window.localStorage.getItem("p_data") == null)
    //    //    return;
    //    //$scope.draftLoaded = true;
    //    //$scope.vendorPurchase = JSON.parse(window.localStorage.getItem("p_data"));
    //    //$scope.updateUI();
    //    //getGoodRNO();
    //    //var m = ModalService.showModal({
    //    //    "controller": "DraftListCtrl",
    //    //    "templateUrl": "DraftListTemplate",
    //    //    "inputs": {
    //    //        "vendorPurchaseArray": $scope.vendorPurchaseArray,
    //    //        "draftCount": $scope.draftCount
    //    //    }
    //    //}).then(function (modal) {
    //    //    modal.element.modal();
    //    //    modal.close.then(function (result) {
    //    //        if (result.data != null) {
    //    //            $scope.disableAddNewDc = true;
    //    //            $.LoadingOverlay("show");
    //    //            vendorPurchaseService.DraftItemsByDraftId(result.data.id)
    //    //            .then(function (resp) {
    //    //                $scope.tempvendorPurchaseItem = resp.data;
    //    //                for (var k = 0; k < $scope.tempvendorPurchaseItem.length; k++) {
    //    //                    $scope.tempvendorPurchaseItem[k].productStock.productId = $scope.tempvendorPurchaseItem[k].productId;
    //    //                    //Assign Selected Product
    //    //                    $scope.tempvendorPurchaseItem[k].selectedProduct = $scope.tempvendorPurchaseItem[k].productStock.product;
    //    //                    //Package Calculation
    //    //                    $scope.tempvendorPurchaseItem[k].discountValue = 0;
    //    //                    $scope.tempvendorPurchaseItem[k].orgDiscount = $scope.tempvendorPurchaseItem[k].discount;
    //    //                    $scope.tempvendorPurchaseItem[k].productStock.tax = $scope.tempvendorPurchaseItem[k].productStock.vat;
    //    //                    $scope.tempvendorPurchaseItem[k].orderedQty = $scope.tempvendorPurchaseItem[k].packageQty - $scope.tempvendorPurchaseItem[k].freeQty || 0;
    //    //                    if ($scope.tempvendorPurchaseItem[k].freeQty > 0) {
    //    //                        $scope.tempvendorPurchaseItem[k].packageQty = Number($scope.tempvendorPurchaseItem[k].packageQty);
    //    //                    }
    //    //                    if ($scope.tempvendorPurchaseItem[k].discount > 0) {
    //    //                        $scope.tempvendorPurchaseItem[k].discountValue = ($scope.tempvendorPurchaseItem[k].packagePurchasePrice * $scope.tempvendorPurchaseItem[k].orderedQty) * ($scope.tempvendorPurchaseItem[k].discount / 100);
    //    //                    }
    //    //                    $scope.tempvendorPurchaseItem[k].total = ($scope.tempvendorPurchaseItem[k].packagePurchasePrice * $scope.tempvendorPurchaseItem[k].orderedQty) - $scope.tempvendorPurchaseItem[k].discountValue;
    //    //                    if (result.data.TaxationType == "VAT") {
    //    //                        $scope.tempvendorPurchaseItem[k].tax = ($scope.tempvendorPurchaseItem[k].total) * ($scope.tempvendorPurchaseItem[k].productStock.vat / 100);
    //    //                    }
    //    //                    if (result.data.TaxationType == "CST") {
    //    //                        $scope.tempvendorPurchaseItem[k].tax = ($scope.tempvendorPurchaseItem[k].total) * ($scope.tempvendorPurchaseItem[k].productStock.cST / 100);
    //    //                    }
    //    //                }
    //    //                $scope.vendorPurchase = result.data;
    //    //                $scope.vendorPurchase.vendorPurchaseItem = $scope.tempvendorPurchaseItem;
    //    //                $scope.draftLoaded = true;
    //    //                $scope.draftvendorPurchase.draftName = result.data.draftName;
    //    //                $scope.vendorPurchase.selectedVendor = $filter("filter")($scope.vendorList, { "id": result.data.vendorId })[0];
    //    //                setTotal();
    //    //                setVat();
    //    //                $scope.updateUI();
    //    //                getGoodRNO();
    //    //                $.LoadingOverlay("hide");
    //    //            }, function (error) {
    //    //                console.log(error);
    //    //                $.LoadingOverlay("hide");
    //    //            });
    //    //        }
    //    //        $scope.draftCount = result.UpdatedDraftCount;
    //    //    });
    //    //});
    //};
    $scope.loadDraft = function () {
        if (window.localStorage.getItem("p_data") == null)
            return;
        $scope.draftLoaded = true;
        $scope.vendorPurchase = JSON.parse(window.localStorage.getItem("p_data"));
        //$scope.updateUI();
        getGoodRNO();
        // Added by Gavaskar 13-09-2017 Start
        //setReturnAmt();
        //setVat();
        $scope.ReCalCulateAllValues();
        // Added by Gavaskar 13-09-2017 End
    };
    $scope.PopupAddNewProduct = function () {
        $scope.AnyPopupOpened = true;
        var m = ModalService.showModal({
            "controller": "productCreateCtrl",
            "templateUrl": "createProduct",
            "inputs": {
                "mode": "Create",
                "poproductname": $scope.selectedProduct,
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();
            //if ($scope.selectedProduct != null && $scope.selectedProduct != undefined) {
            //    $scope.products.name = $scope.selectedProduct;
            //}
            modal.close.then(function (result) {
                if (result === "No") {
                    var drugName = document.getElementById("drugName");
                    drugName.focus();
                }
                $scope.AnyPopupOpened = false;
            });
        });
    };
    $scope.dayDiff = function (expireDate) {
        //if ($scope.buyForm.expDate.$modelValue == undefined) {
        //    $scope.buyForm.expDate.$setValidity("InValidexpDate", true);
        //    $scope.isFormValid = true;
        //    return;
        //}
        var val = (toDate($scope.vendorPurchaseItems.expDate.$viewValue) > $scope.minDate);
        $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", val);
        $scope.isFormValid = val;
        $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date($scope.formatString($scope.expireDate));
        var date1 = new Date($scope.formatString($scope.today));
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference < 30) {
            $scope.highlight = "highlight";
            $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", false);
            $scope.isFormValid = false;
            $scope.chkPODate = false;
        }
        else {
            $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", true);
            $scope.isFormValid = true;
            $scope.chkPODate = true;
            $scope.highlight = "";
        }
    };
    $scope.formatString = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    };
    $scope.checkMrp = function (vendorPurchaseItem) {
        vendorPurchaseItem = JSON.parse(JSON.stringify(vendorPurchaseItem));
        var netPurchase = getFormulaNetValue(vendorPurchaseItem.packagePurchasePrice, vendorPurchaseItem, "");

        var purchase = vendorPurchaseItem.packagePurchasePrice, sell = vendorPurchaseItem.packageSellingPrice, vat = 0;
        purchase = purchase != undefined ? parseFloat(purchase) : 0;
        sell = sell != undefined ? parseFloat(sell) : 0;
        vat = vat != undefined ? parseFloat(vat) : 0;
        //if ((sell < purchase + (purchase * vat / 100)) || (sell == 0)) {
        if ((sell == 0 && purchase == 0) || (purchase == null && sell == null)) {
            $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", true);
        }
        else {
            //if (sell <= purchase + (purchase * vat / 100)) {
            if (sell <= netPurchase) {
                $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", false);
            } else {
                $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", true);
            }
        }
        if ($scope.previouspurchases != undefined) {
            if ($scope.previouspurchases.length > 0) {
                if (purchase > $scope.lastPurchasePrice) {
                    var difference = $filter('number')((parseFloat(purchase) - parseFloat($scope.lastPurchasePrice)), 2);
                    $scope.showPriceMessage = "Current Price " + difference + " Rs Greater Than Previous Purchase";
                } else {
                    $scope.showPriceMessage = "";
                }
            }
        }
        if (!$scope.enableSelling) {
            $scope.vendorPurchaseItem.packageMRP = $scope.vendorPurchaseItem.packageSellingPrice;
        } else {
            if ($scope.vendorPurchaseItem.packageMRP > 0) {
                $scope.checkSellingMrp($scope.vendorPurchaseItem.packageSellingPrice, $scope.vendorPurchaseItem.packageMRP);
            }
        }
    };





    $scope.checkSellingMrp = function (selling, mrp) {
        if ($scope.enableSelling != true)
            return;
        selling = selling != undefined ? parseFloat(selling) : 0;
        mrp = mrp != undefined ? parseFloat(mrp) : 0;
        if (mrp < selling || (mrp == 0)) {
            $scope.vendorPurchaseItems.mrp.$setValidity("checkMrpError", false);
        } else {
            $scope.vendorPurchaseItems.mrp.$setValidity("checkMrpError", true);
        }
    };
    $("#expDate").keyup(function (e) {
        if ($(this).val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $(this).val($(this).val() + "/");
        }
    });
    $("#expDate1").keyup(function (e) {
        if ($(this).val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $(this).val($(this).val() + "/");
        }
    });
    var prod = productModel;
    $scope.search = prod;
    $scope.search = {
        "id": null,
        "accountId": null,
        "instanceId": null,
        "code": null,
        "name": null,
        "manufacturer": null,
        "kindName": null,
        "strengthName": null,
        "type": null,
        "schedule": null,
        "category": null,
        "genericName": null,
        "commodityCode": null,
        "packing": null,
        "packageSize": null,
        "vAT": null,
        "price": null,
        "status": null,
        "rackNo": null,
        "select": null,
        "page": {
            "pageNo": 1,
            "pageSize": 50
        },
        "getFilter": "Available"
    };
    $scope.ProductStocklist = [];
    $scope.productStockSearch = function () {
        productStockService.list($scope.search).then(function (response) {
            $scope.ProductStocklist = response.data.list;
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
            $.LoadingOverlay("hide");
        });
    };
    // $scope.productStockSearch();
    $scope.errorNoteAmount = false;

    //$scope.changeDiscountInRupees = function () {

    //    $scope.vendorPurchase.noteAmount = "";

    //};
    $scope.changeGrossAmount = function (noteAmount) {
        //var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax - $scope.returnAmount;
        var total = $scope.vendorPurchase.netTotal - $scope.returnAmount;
        roundingTotal(total);
    };
    $scope.changeType = function () {
        //var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax - $scope.returnAmount;
        var total = $scope.vendorPurchase.netTotal - $scope.returnAmount;
        $scope.vendorPurchase.noteAmount = "";
        roundingTotal(total);

    };
    //$scope.discountclick = function (val) {
    //    $scope.discountType = val;
    //};
    $scope.checkAllPurchasefields = function (ind) {
        if (ind > 1) {
            if ($scope.vendorPurchase.selectedVendor == undefined || $scope.vendorPurchase.selectedVendor.id == undefined || Object.keys($scope.vendorPurchase.selectedVendor).length == 0) {
                $("#vendor").trigger('chosen:open');
                event.stopPropagation();
                return false;
            }
        }
        if (ind > 2) {
            if ($scope.vendorPurchase.invoiceNo == undefined || $scope.vendorPurchase.invoiceNo == null) {
                var ele = document.getElementById("invoiceNo");
                ele.focus();
                return false;
            }
        }
        if (ind > 3) {
            if ($scope.vendorPurchase.invoiceDate == undefined || $scope.vendorPurchase.invoiceDate == null || $scope.vendorPurchaseItems.invoiceDate.$error.InValidInvoiceDate == true) {
                var ele = document.getElementById("invoiceDate");
                ele.focus();
                return false;
            }
        }
        if (ind > 4) {
            if ($scope.selectedProduct == undefined || $scope.selectedProduct == null) {
                var ele = document.getElementById("drugName");
                ele.focus();
                return false;
            }
        }
        if (ind > 5) {
            if ($scope.vendorPurchaseItem.productStock.batchNo == undefined || $scope.vendorPurchaseItem.productStock.batchNo == null || $scope.vendorPurchaseItem.productStock.batchNo.length == 0) {
                var ele = document.getElementById("batchNo");
                ele.focus();
                return false;
            }
        }
        if (ind > 6) {
            if ($scope.vendorPurchaseItem.productStock.expireDate == undefined || $scope.vendorPurchaseItem.productStock.expireDate == null || $scope.vendorPurchaseItems.expDate.$error.InValidexpDate == true) {
                var ele = document.getElementById("expDate");
                ele.focus();
                return false;
            }
        }
        if (ind > 7) {
            if ($scope.vendorPurchaseItem.packageSize == undefined || $scope.vendorPurchaseItem.packageSize == null || $scope.vendorPurchaseItem.packageSize == 0) {
                var ele = document.getElementById("packageSize");
                ele.focus();
                return false;
            }
        }
        if (ind > 8) {
            if ($scope.vendorPurchaseItem.packageQty == undefined || $scope.vendorPurchaseItem.packageQty == null || (Number($scope.vendorPurchaseItem.packageQty) || 0) == 0) {
                var ele = document.getElementById("packageQuantity");
                ele.focus();
                return false;
            }
        }
        if (ind > 10) {
            if (($scope.GSTEnabled && $scope.mode == "NEW") || ($scope.vendorPurchase.taxRefNo && $scope.mode == "EDIT")) {
                if ($scope.vendorPurchaseItem.productStock.gstTotal == undefined || $scope.vendorPurchaseItem.productStock.gstTotal == null) {
                    var ele = document.getElementById("gstTotal");
                    ele.focus();
                    return false;
                }
            }
            else {
                if ($scope.vendorPurchase.TaxationType == "VAT") {
                    if ($scope.vendorPurchaseItem.productStock.vAT == undefined || $scope.vendorPurchaseItem.productStock.vAT == null) {
                        var ele = document.getElementById("vat");
                        ele.focus();
                        return false;
                    }
                } else {
                    if ($scope.vendorPurchaseItem.productStock.cST == undefined || $scope.vendorPurchaseItem.productStock.cST == null) {
                        var ele = document.getElementById("cST");
                        ele.focus();
                        return false;
                    }
                }
            }
        }
        if (ind > 11) {
            if ($scope.vendorPurchaseItem.packagePurchasePrice == undefined || $scope.vendorPurchaseItem.packagePurchasePrice == null || $scope.vendorPurchaseItem.packagePurchasePrice == 0) {
                var ele = document.getElementById("purchasePrice");
                ele.focus();
                return false;
            }
        }
        if (ind > 12) {
            if ($scope.vendorPurchaseItem.packageSellingPrice == undefined || $scope.vendorPurchaseItem.packageSellingPrice == null || $scope.vendorPurchaseItem.packageSellingPrice == 0) {
                var ele = document.getElementById("sellingPrice");
                ele.focus();
                return false;
            }
        }
        if (ind > 13) {
            if ($scope.vendorPurchaseItem.packageMRP == undefined || $scope.vendorPurchaseItem.packageMRP == null || $scope.vendorPurchaseItem.packageMRP == 0) {
                var ele = document.getElementById("mrp");
                ele.focus();
                return false;
            }
        }
    };
    $scope.focusInvoice = function (e, nextid, currentid) {
        var text = document.getElementById(currentid).value;
        //Enter
        if (e.keyCode == 13) {
            if (text != "") {
                var ele = document.getElementById(nextid);
                ele.focus();
            }
        }
        //BackSpace
        //if (e.keyCode == 8) {
        //    if (text == "") {
        //        $("#vendor").trigger('chosen:open');
        //        event.stopPropagation();
        //        return false;
        //    }
        //}
    };
    $scope.checkPackageSize = function (size) {
        if (size >= 1) {
            $scope.vendorPurchaseItems.packageSize.$setValidity("checkPackageSizeError", true);
        } else {
            $scope.vendorPurchaseItems.packageSize.$setValidity("checkPackageSizeError", false);
        }
        if (size == undefined) {
            $scope.vendorPurchaseItems.packageSize.$setValidity("checkPackageSizeError", true);
        }
    };
    $scope.chkBatch = function () {
        var blnValue = true;
        if ($scope.vendorPurchaseItem.productStock.batchNo != null && $scope.vendorPurchaseItem.productStock.batchNo.trim().length > 0) {
            blnValue = false;
        }
        return blnValue;
    };
    $scope.getScanBarcodeOption = function () {
        vendorPurchaseService.getScanBarcodeOption().then(function (resp) {
            $scope.scanBarcodeOption = resp.data;
        }, function (error) {
            toastr.error('Error Occured', 'Error');
            console.log(error);
        });
    };
    $scope.getScanBarcodeOption();
    $scope.getProduct = function ($event) {
        if ($scope.scanBarcodeOption.scanBarcode === "1") {
            if ($scope.selectedProduct !== null && $scope.selectedProduct !== "" && ($scope.selectedProduct === undefined || typeof ($scope.selectedProduct) !== "object")) {
                var scannedEancode = $scope.selectedProduct;
                productStockService.getEancodeExistData(scannedEancode).then(function (eanRes) {
                    if (eanRes.data.length !== 0) {
                        productService.nonHiddenProductList(eanRes.data[0].id).then(function (response) {
                            if (response.data.length === 1) {
                                $scope.selectedProduct = response.data[0];
                                $scope.onProductSelect($event, $scope.selectedProduct);
                                var batchNo = document.getElementById("batchNo");
                                batchNo.focus();
                            }
                        }, function (error) {
                            console.log(error);
                            $.LoadingOverlay("hide");
                        });
                    } else {
                        $scope.isItScan = false;
                        addNewProduct();
                    }
                }, function (error) {
                    console.log(error);
                    $.LoadingOverlay("hide");
                });
            } else if (typeof ($scope.selectedProduct) === "object" && $scope.selectedProduct !== null) {
                var batchNo = document.getElementById("batchNo");
                batchNo.focus();
            }
        }
        else {
            addNewProduct();
        }
    };

    function addNewProduct() {
        var isAddProduct = document.getElementById("addNewProduct").value;
        if (isAddProduct != "false") {
            if ($scope.selectedProduct.id == undefined && $scope.selectedProduct.length > 3 && $scope.isItScan == false) {
                $scope.AnyPopupOpened = true;
                $.confirm({
                    title: "Add New Product Confirm",
                    content: "This is a new product, do you want to add it ?",
                    closeIcon: function () {
                        $scope.AnyPopupOpened = false;
                    },
                    buttons: {
                        yes: {
                            text: 'yes [y]',
                            btnClass: 'primary-button',
                            keys: ['y'],
                            action: function () {
                                $scope.AnyPopupOpened = false;
                                $scope.PopupAddNewProduct();
                            }
                        },
                        no: {
                            text: 'no [n]',
                            btnClass: 'secondary-button',
                            keys: ['n'],
                            action: function () {
                                $scope.AnyPopupOpened = false;
                            }
                        }
                    }
                });
            }
        }
    };

    $scope.checkEancodeForDuplicate = function () {
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.eancode != "" && $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.eancode != null && $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.eancode != undefined && $scope.vendorPurchaseItem.productStock.product.eancode != "" && $scope.vendorPurchaseItem.productStock.product.eancode != null & $scope.vendorPurchaseItem.productStock.product.eancode != undefined) {
                if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.eancode === $scope.vendorPurchaseItem.productStock.product.eancode && ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name !== $scope.vendorPurchaseItem.productStock.product.name || $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.id !== $scope.vendorPurchaseItem.productStock.product.id)) {
                    toastr.info("UPC/EAN Code [" + $scope.vendorPurchaseItem.productStock.product.eancode + "] already exist with another product[" + $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name + "]. Please provide different one.");
                    return true;
                }
            }
        }
    };
    //Durga Code Commented on 08/04/2017
    //Durga Code Start
    //$scope.DivWithTax = true;
    //$scope.DivWithOutTax = false;
    //getTaxtype();
    //$scope.SaveTaxSeries = "";
    //function getTaxtype() {
    //    //  console.log("Calling");
    //    vendorPurchaseService.getTaxtype().then(function (response) {
    //        // console.log(JSON.stringify(response.data));
    //        if (response.data != "" && response.data != null) {
    //            $scope.SaveTaxSeries = response.data.taxType;
    //            if ($scope.SaveTaxSeries == "" || $scope.SaveTaxSeries == undefined) {
    //                $scope.SaveTaxSeries = 1;
    //            }
    //            if ($scope.SaveTaxSeries == 1) {
    //                $scope.DivWithTax = true;
    //                $scope.DivWithOutTax = false;
    //            }
    //            if ($scope.SaveTaxSeries == 2) {
    //                $scope.DivWithTax = false;
    //                $scope.DivWithOutTax = true;
    //                getCustomTaxSeries();
    //            }
    //        } else {
    //            $scope.DivWithTax = true;
    //            $scope.DivWithOutTax = false;
    //        }
    //    }, function () {
    //    });
    //}
    //$scope.TaxSeriesItems = [];
    //getCustomTaxSeries = function () {
    //    vendorPurchaseService.getCustomTaxSeriesItems().then(function (response) {
    //        if (response.data != "" && response.data != null) {
    //            $scope.TaxSeriesItems = response.data;
    //            console.log(JSON.stringify($scope.TaxSeriesItems));
    //        }
    //    }, function (error) {
    //        console.log(error);
    //        toastr.error('Error Occured', 'Error');
    //    });
    //};
    //$scope.ValidateCustomTaxSeries = function () {
    //};
    $scope.focusNextId = function (nextid) {
        document.getElementById(nextid).focus();
    };
    /* Commented by settu
    $scope.focusNoteTextBox = function () {
        var ele = document.getElementById("txtNoteAmount");
        ele.focus();
    };
    $scope.focusSaveButton = function () {
        var ele = document.getElementById("subAddStock");
        ele.focus();
    };
    $scope.focusNoteType = function () {
        var ele = document.getElementById("noteType");
        ele.focus();
    };*/
    //$scope.getEnableSelling = function () {
    //    $.LoadingOverlay("show");
    //    vendorPurchaseService.getEnableSelling().then(function (response) {
    //        $.LoadingOverlay("hide");
    //        $scope.enableSelling = response.data;
    //    }, function (error) {
    //        $.LoadingOverlay("hide");
    //        console.log(error);
    //        toastr.error('Error Occured', 'Error');
    //    });
    //};
    //$scope.getEnableSelling();

    $scope.getPurchaseSettings = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getBuyInvoiceDateEditSetting().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.purchaseSettings = response.data;
            if ($scope.purchaseSettings != null && $scope.purchaseSettings != undefined) {
                $scope.enableSelling = $scope.purchaseSettings.enableSelling;
            }
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.getPurchaseSettings();





    $scope.changeGST = function (value) {
        var type = $scope.vendorPurchase.selectedVendor.locationType;
        value = parseFloat(value) || 0;
        //if ($scope.vendorPurchaseItem.packagePurchasePrice != undefined && $scope.vendorPurchaseItem.packageSellingPrice != undefined) {
        //    $scope.checkMrp($scope.vendorPurchaseItem.packagePurchasePrice, $scope.vendorPurchaseItem.packageSellingPrice, value);
        //}
        $scope.vendorPurchaseItem.productStock.igst = "";
        $scope.vendorPurchaseItem.productStock.cgst = "";
        $scope.vendorPurchaseItem.productStock.sgst = "";
        //Local
        if (type == 0 || type == 1 || type == "" || type == null || type == undefined) {
            $scope.vendorPurchaseItem.productStock.cgst = (value / 2).toFixed(2);
            $scope.vendorPurchaseItem.productStock.sgst = (value / 2).toFixed(2);
        }
        //InterState
        if (type == 2) {
            $scope.vendorPurchaseItem.productStock.igst = value;
            $scope.vendorPurchaseItem.productStock.cgst = "";
            $scope.vendorPurchaseItem.productStock.sgst = "";
        }
        $scope.checkMrp($scope.vendorPurchaseItem)
    };



    $scope.checkInvoiceNo = function (val) {
        if (val == "" || val == null)
            return;
        if (IsSelectedVendor())
            $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;
        if (typeof ($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON) == "object") {
            $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON = JSON.stringify($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON);
        }
        if ($scope.vendorPurchase.vendor != undefined) {
            if (typeof ($scope.vendorPurchase.vendor.vendorPurchaseFormula.formulaJSON) == "object") {
                $scope.vendorPurchase.vendor.vendorPurchaseFormula.formulaJSON = JSON.stringify($scope.vendorPurchase.vendor.vendorPurchaseFormula.formulaJSON);
            }
        }
        var vendorPurchase = $scope.vendorPurchase;
        $.LoadingOverlay("show");
        vendorPurchaseService.validateInvoice($scope.vendorPurchase)
            .then(function (response) {
                $.LoadingOverlay("hide");  //toastr.error('Error Occured', 'Error');
            }, function (error) {
                $.LoadingOverlay("hide");
                var ele = document.getElementById("invoiceNo");
                ele.focus();
                toastr.info(error.data.value.errorDesc, 'Info..');
            });
    }

    function IsSelectedVendor() {
        if ($scope.vendorPurchase.selectedVendor.id != null && $scope.vendorPurchase.selectedVendor.id != undefined && $scope.vendorPurchase.selectedVendor.id != "") {
            if ($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula == null || $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula == undefined || $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula == "") {
                $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula = $scope.defaultPurchaseFormula;
            }
            return true;
        }
        return false;
    };

    $scope.setFocus = function (tag) {
        var ele = document.getElementById(tag);
        if (ele != null) {
            ele.focus();
        }
    };

    function ShowConfirmMsgWindow(msg, focusTag) {
        var data = {
            msgTitle: "",
            msg: msg,
            showOk: true,
            showYes: false,
            showNo: false,
            showCancel: false,
            redirectUrl: "",
        };
        var m = ModalService.showModal({
            "controller": "showConfirmMsgCtrl",
            "templateUrl": 'showConfirmMsgModal',
            "inputs": { "params": [{ "data": data }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.setFocus(focusTag);
            });
        });
    };

    $scope.openPurchaseFormula = function () {

        if (IsSelectedVendor()) {
            if (typeof ($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON) == "object") {
                $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON = JSON.stringify($scope.vendorPurchase.selectedVendor.vendorPurchaseFormula.formulaJSON);
            }
        }
        else {
            ShowConfirmMsgWindow("Please select Vendor Name and try again.");
            $("#vendor").trigger('chosen:open');
            return;
        }

        var formula = $scope.vendorPurchase.selectedVendor.vendorPurchaseFormula;
        var formulaValues = {
            basicAmount: Number($scope.vendorPurchaseItem.packagePurchasePrice) || 0,
            discount: Number($scope.vendorPurchaseItem.discount) || 0,
            markup: Number($scope.vendorPurchaseItem.markupPerc) || 0,
            tax: Number($scope.vendorPurchaseItem.productStock.gstTotal) || 0,
            schemediscount: Number($scope.vendorPurchaseItem.schemeDiscountPerc) || 0,
        };
        var m = ModalService.showModal({
            "controller": "purchaseFormulaCreateCtrl",
            "templateUrl": 'purchaseFormulas',
            "inputs": { "params": [{ "data": formula, "formulaValues": formulaValues }] },
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
            });
        });
    };

    $scope.applyTaxFreeQty = function () {
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    if ($scope.mode == "EDIT" && (Number($scope.vendorPurchase.vendorPurchaseItem[i].freeQty) || 0) > 0) {
                        if ($scope.vendorPurchase.vendorPurchaseItem[i].id == undefined || $scope.vendorPurchase.vendorPurchaseItem[i].id == "" || $scope.vendorPurchase.vendorPurchaseItem[i].id == null) {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "I";
                        }
                        else if ($scope.vendorPurchase.vendorPurchaseItem[i].id != "") {
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                        }
                    }
                }
            }
        }
        $scope.ReCalCulateAllValues();
    };

    $scope.alternateVendorProduct = alternateVendorProductModel;
    $scope.alterName = {};
    $scope.saveAlternate = function (val) {
        return productService.saveAlternateName($scope.alternateVendorProduct)
            .then(function (response) {
                for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
                    if ($scope.alternateVendorProduct.productId == $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.id) {
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.id = response.data.productId;
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId = response.data.productId;
                    }
                }
                $scope.alternateVendorProduct = response.data;
            });
    };

    $scope.getTaxValues = function () {
        vendorPurchaseService.getTaxValues().then(function (response) {
            $scope.taxValuesList = response.data;
            if ($scope.taxValuesList.length > 0)
                $scope.vendorPurchaseItem.productStock.gstTotal = Object.keys($scope.taxValuesList)[0];
            getAllTaxValues();
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };

    function getAllTaxValues() {
        vendorPurchaseService.getAllTaxValues().then(function (response) {
            $scope.allTaxValuesList = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };

    function getBarcodeGenerationIsEnabled() {
        vendorPurchaseService.getBarcodeGenerationIsEnabled().then(function (response) {
            $scope.BarcodeGenerationIsEnabled = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    getBarcodeGenerationIsEnabled();

});
app.directive('numbersOnly', function () {
    return {
        "require": '?ngModel',
        "link": function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.filter('unique', function () {
    return function (collection, keyname) {
        var output = [],
        keys = [];
        angular.forEach(collection, function (item) {
            var key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });
        return output;
    };
});

app.directive('focusBox', function () {
    return {
        "link": function (scope, element, attrs) {
            element.bind("keydown", function (event) {
                var valueLength = attrs.$$element[0].value.length;
                var value = parseFloat(attrs.$$element[0].value);
                //Enter 
                if (event.which === 13) {
                    if (attrs.id === "freeQty" || attrs.id === "discount" || attrs.id === "Rackno" || attrs.id === "boxNo" || attrs.id === "hsnCodeDc" || attrs.id === "gstTotalDc" || attrs.id === "hsnCode" || attrs.id === "igst" || attrs.id === "cgst" || attrs.id === "sgst") {
                        ele = document.getElementById(attrs.nextid);
                        ele.focus();
                    } else {
                        if (valueLength !== 0) {
                            if ((angular.isString(scope.selectedProduct)) && (attrs.nextid == 'batchNo')) {
                                ele = document.getElementById("drugName");
                                ele.focus();
                            } else {
                                if (attrs.id !== "vat") {
                                    if (value != 0) {
                                        if (document.getElementById(attrs.nextid).disabled == false) {
                                            ele = document.getElementById(attrs.nextid);
                                            ele.focus();
                                        } else {
                                            if (document.getElementById(attrs.nextid).disabled == true && attrs.nextid == "drugName") {
                                                ele = document.getElementById("batchNo");
                                                ele.focus();
                                            }
                                        }
                                    }
                                } else {
                                    if (valueLength !== 0) {
                                        ele = document.getElementById(attrs.nextid);
                                        ele.focus();
                                    }
                                }
                            }
                        }
                    }
                }
                //BackSpace
                //if (event.which === 8) {
                //    if (valueLength === 0) {
                //        ele = document.getElementById(attrs.previousid);
                //        ele.focus();
                //        event.preventDefault();
                //    }
                //}
            });
        }
    };
});
//app.factory('beforeUnload', function ($rootScope, $window) {
//    $window.onbeforeunload = function (e) {
//        var confirmation = {};
//        var event = $rootScope.$broadcast('onBeforeUnload', confirmation);
//        if (event.defaultPrevented) {
//            return confirmation.message;
//        }
//    };
//    $window.onunload = function () {
//        $rootScope.$broadcast('onUnload');
//    };
//    return {};
//});
//app.run(function (beforeUnload) {
//});
//app.directive('focusTextbox', function ($timeout) {
//    return {
//        link: function (scope, element, attrs) {
//            scope.$watch(attrs.focusTextbox, function (value) {
//                if (value === true) {
//                    element[0].focus();
//                }
//            });
//        }
//    };
//});