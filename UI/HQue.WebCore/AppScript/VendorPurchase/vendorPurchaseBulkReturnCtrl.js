﻿app.controller('vendorPurchaseBulkReturnCtrl', function ($scope, toastr, vendorReturnModel, vendorReturnItemModel, vendorPurchaseService) {
   
    var vendorPurchaseReturn = vendorReturnModel;
    var vendorPurchaseReturnItem = vendorReturnItemModel;

    $scope.total = 0;
    $scope.Math = window.Math;

    $scope.vendorPurchaseReturn = vendorPurchaseReturn;
    $scope.vendorPurchaseReturn.vendorPurchaseReturnItem = vendorPurchaseReturnItem;
    $scope.returnTotal = 0;

    $scope.init = function () {
        $.LoadingOverlay("show");       
        vendorPurchaseService.getBulkReturn().then(function (response) {
            $scope.vendorPurchaseReturn = response.data;
            var flags = [], output = [], l = $scope.vendorPurchaseReturn.vendor.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$scope.vendorPurchaseReturn.vendor[i].id]) {

                    for (j = 0; j < output.length; j++) {
                        if (output[j].id == $scope.vendorPurchaseReturn.vendor[i].id)
                        {
                            output[j].vendorPurchaseReturnItem.push($scope.vendorPurchaseReturn.vendor[i].vendorPurchaseReturnItem[0]);
                        }
                    }                   
                    //$scope.vendorPurchaseReturn.vendor[i].push($scope.vendorPurchaseReturn.vendor[i]);
                    continue;
                }
                else {
                    flags[$scope.vendorPurchaseReturn.vendor[i].id] = true;
                    output.push($scope.vendorPurchaseReturn.vendor[i]);
                    
                }
            }
            $scope.vendorPurchaseReturn1 = output;
            setPrice();
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
        });
    }

    function setPrice() {
        var purchasedTotal = 0;
        for (var i = 0; i < $scope.vendorPurchaseReturn1.length; i++) {
            $scope.vendorPurchaseReturn1[i]["purchasedTotal"] = 0;
            $scope.vendorPurchaseReturn1[i]["returnTotal"] = 0;
            var discount = 0;
            for (var j = 0; j < $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem.length; j++) {
                if ($scope.vendorPurchaseReturn.gstEnabled == true) {
                    $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.total = $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].total + (($scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].total * $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.gstTotal) / 100);
                } else {
                    if ($scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.cst > 0) {
                        $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.total = $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].total + (($scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].total * $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.cst) / 100);
                    }
                    else {
                        $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.total = $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].total + (($scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].total * $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.vat) / 100);
                    }
                }
                
                $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].returnPurchasePrice = $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.total / $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].purchaseQuantity;
                $scope.vendorPurchaseReturn1[i]["purchasedTotal"] += $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.total;

                if (isNaN($scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].returnPurchasePrice))
                {
                    $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].returnPurchasePrice = "0.00";
                }
               

                if ($scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].quantity > $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].purchaseQuantity) {
                    $scope.vendorPurchaseReturn1[i]["returnTotal"] = 0;
                }
                else {
                     if (discount != 0) {
                         $scope.vendorPurchaseReturn1[i]["returnTotal"] += $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].returnPurchasePrice * $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].quantity - (($scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].returnPurchasePrice * $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].quantity) * discount) / 100;
                     }
                    else {
                         $scope.vendorPurchaseReturn1[i]["returnTotal"] += $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].returnPurchasePrice * $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].quantity;
                    }
                }

                //$scope.vendorPurchaseReturn1[i]["returnTotal"] += $scope.vendorPurchaseReturn1[i].vendorPurchaseReturnItem[j].productStock.total;
            }
        }

        //for (var i = 0; i < $scope.vendorPurchaseReturn.vendorPurchaseReturnItem.length; i++) {          
        //    if ($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.cst > 0) {
        //        $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.total = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total + (($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.cst) / 100);
        //    }
        //    else {
        //        $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.total = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total + (($scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].total * $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.vat) / 100);
        //    }
            
        //    $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].returnPurchasePrice = $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.total / $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].purchaseQuantity;
        //    purchasedTotal += $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].productStock.total
        //}
        //$scope.vendorPurchaseReturn.purchasedTotal = purchasedTotal;
    }

    $scope.disableSave = function (data) {
        if (data.purchaseQuantity - data.returnedQuantity - data.quantity <= 0)
            $scope.returnTotal = 0;
    };

    $scope.editItem = function (item) {
        item.returnTotal=0;
        var discount = 0;
        if ($scope.vendorPurchaseReturn.discount)
            discount = $scope.vendorPurchaseReturn.discount;

        for (var i = 0; i < item.vendorPurchaseReturnItem.length; i++) {
            // if entered Qty more than purchase qty show error message
            if (item.vendorPurchaseReturnItem[i].quantity > item.vendorPurchaseReturnItem[i].productStock.stock) {
                item.returnTotal = 0;
                i = item.vendorPurchaseReturnItem.length;
                continue;
            }
            else {
                //$scope.returnTotal += item[i].returnPurchasePrice * item[i].quantity;
                if (discount != 0) {
                    item.returnTotal += item.vendorPurchaseReturnItem[i].returnPurchasePrice * item.vendorPurchaseReturnItem[i].quantity - ((item.vendorPurchaseReturnItem[i].returnPurchasePrice * item.vendorPurchaseReturnItem[i].quantity) * discount) / 100;
                    //$scope.returnTotal += item[i].returnPurchasePrice
                }
                else {
                    item.returnTotal += item.vendorPurchaseReturnItem[i].returnPurchasePrice * item.vendorPurchaseReturnItem[i].quantity;
                }
            }
        }
    }

    $scope.save = function (vendor) {
        $.LoadingOverlay("show");

        //$scope.vendorPurchaseReturn.vendorPurchaseReturnItem.returnedTotal = $scope.returnTotal;        
        vendorPurchaseService.createVendorPurchaseBulkReturn(vendor).then(function (response) {
            $scope.vendorPurchaseReturn = response.data;
            toastr.success('Data Saved Successfully');
            $scope.init();
            //window.location.assign('/vendorPurchase/list');
            $.LoadingOverlay("hide");
        }, function () {
            $.LoadingOverlay("hide");
            toastr.error('Error Occured', 'Error');
        });
    }

    $scope.cancel = function () {
        var cancel = window.confirm('Are you sure, Do you want to cancel?');
        if (cancel) {
            for (var i = 0; i < $scope.vendorPurchaseReturn.vendorPurchaseReturnItem.length; i++) {
                $scope.vendorPurchaseReturn.vendorPurchaseReturnItem[i].quantity = "";
            }
        }
    }


    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#' + row).slideToggle();
        $('#' + row).show();
        if ($('#' + obj.target.id).text() === '+')
            $('#' + obj.target.id).text('-');
        else {
            $('#' + obj.target.id).text('+');
        }
       /* $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        } */
    }     
   
});