﻿app.factory('shortcutHelper', function () {
    var scope;
    return {
        setScope: function (parentScope) {
            scope = parentScope;
        },
        purchaseShortcuts: function (e) {

            //Ctrl + D
            if (e.ctrlKey == true && e.keyCode == 68) {
                event.preventDefault();
                if (scope.disableAddNewDc == false && scope.AnyPopupOpened == false) {
                    scope.PopupAddDC();
                }
            }

            //Ctrl + P
            if (e.ctrlKey == true && e.keyCode == 80) {
                event.preventDefault();
                if (scope.AnyPopupOpened == false) {
                    var isAddProduct = document.getElementById("addNewProduct").value;
                    if (isAddProduct != "false")
                    {
                        scope.PopupAddNewProduct();
                    }
                }
            }



            //Ctrl + R
            if (e.ctrlKey == true && e.keyCode == 82) {
                event.preventDefault();
                if (scope.AnyPopupOpened == false) {
                    document.getElementById("rdCredit").click();
                    angular.element(document.getElementById("creditNoOfDays")).focus();
                }
               
            }


            //Ctrl + L
            if (e.ctrlKey == true && e.keyCode == 76) {
                event.preventDefault();
                if (scope.AnyPopupOpened == false) {
                    scope.loadDraft();
                }
            }



            //Ctrl + C
            if (e.ctrlKey == true && e.keyCode == 67) {
                event.preventDefault();
                if (scope.AnyPopupOpened == false) {
                    scope.cancel();
                }
                   
            }

            //F1
            if (e.keyCode == 112) {
                event.preventDefault();
                scope.showSchemeDiscount('value');
            }

            //F2
            if (e.keyCode == 113) {
                event.preventDefault();
                scope.showSchemeDiscount('percent');
            }


            //ESC
            if (e.keyCode == 27) {
                if (angular.element(document.getElementById("subAddStock"))[0].disabled == false && scope.completePurchaseKeyType == "ESC" && scope.AnyPopupOpened == false) {
                    event.preventDefault();
                    scope.save();
                }
            }



            //END
            if (e.keyCode == 35) {
                if (angular.element(document.getElementById("subAddStock"))[0].disabled == false && scope.completePurchaseKeyType == "END") {
                    event.preventDefault();
                    scope.save();
                }
            }

            //TAB
            if (e.keyCode === 9 && e.target.id == "invoiceNo") {
                var ele = document.getElementById("invoiceNo").value;
                if (ele != undefined && ele != "")
                    scope.checkInvoiceNo(ele);
            }
        },
        purchaseEditShortcuts: function (e) {
            //Ctrl + R
            if (e.ctrlKey == true && e.keyCode == 82) {
                event.preventDefault();
                if (scope.AnyPopupOpened == false) {
                    document.getElementById("rdCredit").click();
                    angular.element(document.getElementById("creditNoOfDays")).focus();
                }
            }





            //Ctrl + P
            if (e.ctrlKey == true && e.keyCode == 80) {
                event.preventDefault();
                if (scope.AnyPopupOpened == false) {
                    var isAddProduct = document.getElementById("addNewProduct").value;
                    if (isAddProduct != "false") {
                        scope.PopupAddNewProduct();
                    }
                }
            }


            //ESC
            if (e.keyCode == 27) {
                if (angular.element(document.getElementById("btnUpdateStock"))[0].disabled == false && scope.completePurchaseKeyType == "ESC" && scope.saveKey == true && scope.AnyPopupOpened == false) {
                    event.preventDefault();
                    scope.save();
                }
            }



            //END
            if (e.keyCode == 35) {
                if (angular.element(document.getElementById("btnUpdateStock"))[0].disabled == false && scope.completePurchaseKeyType == "END" && scope.saveKey == true && scope.AnyPopupOpened == false) {
                    event.preventDefault();
                    scope.save();
                }
            }


            //F1
            if (e.keyCode == 112) {
                event.preventDefault();
                scope.showSchemeDiscount('value');
            }

            //F2
            if (e.keyCode == 113) {
                event.preventDefault();
                scope.showSchemeDiscount('percent');
            }


            //Ctrl + C
            if (e.ctrlKey == true && e.keyCode == 67) {
                event.preventDefault();
                if (scope.AnyPopupOpened == false) {
                    scope.cancelUpdateStock();
                }
            }

            //TAB
            if (e.keyCode === 9 && e.target.id == "invoiceNo") {
                var ele = document.getElementById("invoiceNo").value;
                if (ele != undefined && ele != "")
                    scope.checkInvoiceNo(ele);
            }
        }
    };
});
