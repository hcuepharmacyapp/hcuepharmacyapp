﻿app.controller('purchaseProductReturnCtrl', function ($scope, toastr, $filter, vendorPurchaseService, vendorService, vendorReturnModel, vendorPurchaseModel, productService, close, vendorPurchaseItem, taxValuesList) {

    var vendorPurchaseReturn = vendorReturnModel;

    $scope.search = vendorPurchaseReturn;
    $scope.list = [];
    $scope.addproductDetails = {};
    $scope.addproductDetails.transactionType = "return";
    $scope.addproductDetails.vendorReturn = true;
    $scope.addselectedproduct = {};
    $scope.getaddedproduct = [];
    $scope.tempReturnItem = [];
    $scope.focusInputVendorName = true;
    $scope.isValidQty = false;
    $scope.isBatch = true;
    $scope.editprodItem = false;
    $scope.batchListNew = [];
    $scope.focusReason = false;
    $scope.focusProductName = true;
    $scope.fsbatchNumber = false;
    $scope.returnStatus = false;
    $scope.taxValuesList = taxValuesList;

    $scope.VendorPurchaseReturnItems1 = {
        "Quantity": 0,
        "ProductStockId": 0,
        "ReturnedTotal": 0,
        "ReturnedQuantity": 0,
        "ReturnPurchasePrice": 0,
        "PackageQuantity": 0,
        "ProductStock": {}
    }

    $scope.vendorPurchaseReturn = {
        VendorPurchaseReturnItem: [],
        VendorId: "",
        NetReturn: 0.0
    };

      
    $scope.returnTotal = 0;
    $scope.retTotal = 0;
    $scope.selectedProduct = null;
   
    $scope.reason = "Non Moving Return";

    $scope.toggleProductDetail = function (obj) {
        var row = obj.target.getAttribute("data");

        $('#chip-wrapper' + row).slideToggle();
        $('#chip-wrapper' + row).show();
        if ($('#chip-btn' + row).text() === '+')
            $('#chip-btn' + row).text('-');
        else {
            $('#chip-btn' + row).text('+');
        }
    }

    $scope.getProducts = function (val) {

        return productService.ReturndrugFilter(val).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    }
    
    $scope.minDate = new Date();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];


    $scope.getEnableSelling = function () {
        vendorPurchaseService.getEnableSelling().then(function (response) {
            $scope.enableSelling = response.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getEnableSelling();

   
    $scope.editVendorPurchaseReturn = function (item) {
        item.isEdit = true;
    }

    $scope.PurchaseReturnPopup = function () {
       
        $scope.focusInputVendorName = true;
        $scope.isValidQty = false;
        $scope.returnStatus = false;
        $scope.reason = "Non Moving Return";
        $("#venName").trigger('chosen:open');
    }

    $scope.close = function (result) {
        if (result == 'No') {
            result = null;
        }
        close(result, 500); // close, but give 500ms for bootstrap to animate
        $(".modal-backdrop").hide();
    };

    $scope.clearProductInfo = function () {
        $scope.selectedProduct = null;
        $scope.addselectedproduct.productquantity = null;
        $scope.selectedBatch = null;
        $scope.isValidQty = false;
        $scope.CheckName();
    }


    function checkExist(productStockId, productname) {
        var status = false;
        for (var i = 0; i < $scope.getaddedproduct.length; i++) {
            if ($scope.getaddedproduct[i].name == productname && $scope.getaddedproduct[i].productStockId == productStockId) {
                status = true;
                break;
            }
        }
        return status;
    }

   

    $scope.addProduct = function () {

        $scope.isValidQty = false;

        if ($scope.addselectedproduct.productquantity == null) {
            var elem = document.getElementById("productqtyId");
            elem.focus();
            return;
        }
        else if (document.getElementById("gstTotal1")) {

            var ngst = $filter("filter")($scope.taxValuesList, { "tax": parseFloat($scope.addselectedproduct.productStock.gstTotal) }, true);
            if (ngst.length == 0) {
                var ele = document.getElementById("gstTotal1");
                ele.focus();
                return;
            }            
        }   
       else if ($scope.selectedBatch == null) {
            return;
        }

        

        if ($scope.addselectedproduct != null) {
            $scope.addproductDetails.selectedProduct = $scope.addselectedproduct.productStock.product;
            $scope.addproductDetails.selectedProduct.id = $scope.addselectedproduct.productStock.productId;
            // $scope.addproductDetails.selectedProduct.productId = $scope.addselectedproduct.productStock.id;
            $scope.addproductDetails.selectedProduct.batchNo = $scope.addselectedproduct.productStock.batchNo;
            $scope.addproductDetails.selectedProduct.expireDate = $scope.addselectedproduct.productStock.expireDate;
            $scope.addproductDetails.selectedProduct.eancode = $scope.addselectedproduct.productStock.product.eancode;
            $scope.addproductDetails.selectedProduct.tax = $scope.addselectedproduct.productStock.tax;
            $scope.addproductDetails.selectedProduct.gstTotal = $scope.addselectedproduct.productStock.gstTotal;
            $scope.addproductDetails.selectedProduct.igst = $scope.addselectedproduct.productStock.igst;
            $scope.addproductDetails.selectedProduct.sgst = $scope.addselectedproduct.productStock.sgst;
            $scope.addproductDetails.selectedProduct.cgst = $scope.addselectedproduct.productStock.cgst;
            $scope.addproductDetails.selectedProduct.PurchasePrice = $scope.addselectedproduct.productStock.PurchasePrice;

            $scope.addproductDetails.selectedProduct.manufacturer = $scope.addselectedproduct.productStock.product.manufacturer;
            $scope.addproductDetails.selectedProduct.type = $scope.addselectedproduct.productStock.product.type;
            $scope.addproductDetails.selectedProduct.schedule = $scope.addselectedproduct.productStock.product.schedule;
            $scope.addproductDetails.selectedProduct.category = $scope.addselectedproduct.productStock.product.category;
            $scope.addproductDetails.selectedProduct.packing = $scope.addselectedproduct.productStock.product.packing;
            $scope.addproductDetails.selectedProduct.genericName = $scope.addselectedproduct.productStock.product.genericName;

            $scope.addproductDetails.name = $scope.addselectedproduct.productStock.product.name;
            $scope.addproductDetails.productStockId = $scope.addselectedproduct.productStock.id;
            $scope.addproductDetails.orderedQty = $scope.addselectedproduct.productquantity;

            if (checkExist($scope.addproductDetails.productStockId, $scope.addselectedproduct.productStock.product.name)) {
                var succ = confirm("Same product with same batch already Added");
                if (succ) {
                    document.getElementById("drugName1").focus()
                }
                return;
            }

            for (var k = 0; k < vendorPurchaseItem.length; k++) {

                if ((vendorPurchaseItem[k].selectedProduct.id == $scope.addselectedproduct.productStock.productId && vendorPurchaseItem[k].productStock.batchNo == $scope.addselectedproduct.productStock.batchNo)) {
                    var ele = document.getElementById("drugName1");
                    ele.focus();
                    toastr.info("Product already added in Purchase");
                    return;
                }
            }

            $scope.addproductDetails.stock = $scope.productQty;
            $scope.addproductDetails.freeQty = $scope.addselectedproduct.freeQty;
            $scope.addproductDetails.batchNo = $scope.selectedBatchNum;
            $scope.addproductDetails.expireDate = $filter('date')(new Date($scope.addselectedproduct.productStock.expireDate), "MM/yy");
            $scope.addproductDetails.pricewithvat = $scope.addselectedproduct.productStock.vat;
            // GST related
            $scope.addproductDetails.igst = $scope.addselectedproduct.productStock.igst;
            $scope.addproductDetails.cgst = $scope.addselectedproduct.productStock.cgst;
            $scope.addproductDetails.sgst = $scope.addselectedproduct.productStock.sgst;
            $scope.addproductDetails.gstTotal = $scope.addselectedproduct.productStock.gstTotal;
            $scope.addproductDetails.reason = $scope.reason;

            if ($scope.addselectedproduct.packagePurchasePrice == undefined || $scope.addselectedproduct.packagePurchasePrice == null) { //packagePurchasePrice
                $scope.addproductDetails.packagePurchasePrice = 0;
            } else {
                $scope.addproductDetails.packagePurchasePrice = $scope.addselectedproduct.packagePurchasePrice;
            }

            // if ($scope.addselectedproduct.productStock.sellingPrice == undefined || $scope.addselectedproduct.productStock.sellingPrice == null) {
            if ($scope.addselectedproduct.packageSellingPrice == undefined || $scope.addselectedproduct.packageSellingPrice == null) {
                $scope.addproductDetails.packageSellingPrice = 0;
            } else {
                //$scope.addproductDetails.packageSellingPrice = $scope.addselectedproduct.productStock.sellingPrice * $scope.addselectedproduct.productStock.packageSize;
                $scope.addproductDetails.packageSellingPrice = $scope.addselectedproduct.packageSellingPrice;
            }

            $scope.addproductDetails.packageSize = $scope.addselectedproduct.productStock.packageSize;
            $scope.addproductDetails.packageMRP = $scope.addselectedproduct.productStock.mrp * $scope.addselectedproduct.productStock.packageSize;

            if ($scope.addselectedproduct.discount == undefined || $scope.addselectedproduct.discount == null) {
                $scope.addproductDetails.discount = 0;
                $scope.addproductDetails.orgDiscount = 0;
            } else {
                $scope.addproductDetails.discount = $scope.addselectedproduct.discount;
                $scope.addproductDetails.orgDiscount = $scope.addselectedproduct.discount;
            }
            $scope.addproductDetails.discount = $scope.addselectedproduct.discount;
            $scope.addproductDetails.editprodItem = $scope.editprodItem;
            // $scope.addproductDetails.total = ($scope.addproductDetails.orderedQty) * $scope.addproductDetails.packagePurchasePrice;
            $scope.addproductDetails.totalQuantity = $scope.addselectedproduct.productStock.packageSize * $scope.addproductDetails.orderedQty;
            $scope.addproductDetails.discountVal = ($scope.addselectedproduct.packagePurchasePrice * $scope.addproductDetails.orderedQty) * ($scope.addproductDetails.discount / 100);
            $scope.addproductDetails.tax = (($scope.addproductDetails.orderedQty * $scope.addproductDetails.packagePurchasePrice) - $scope.addproductDetails.discountVal) * ($scope.addselectedproduct.productStock.gstTotal / 100);
            $scope.addproductDetails.packageQty = parseFloat($scope.addproductDetails.orderedQty) || 0;
            $scope.addproductDetails.productStock = $scope.addselectedproduct.productStock;
            $scope.getaddedproduct.push($scope.addproductDetails);


            $scope.retTotal = 0;

            for (var i = 0; i < $scope.getaddedproduct.length; i++) {
                if ($scope.getaddedproduct[i].name == $scope.addselectedproduct.productStock.product.name && $scope.getaddedproduct[i].productStockId == $scope.productStockId) {
                } else {


                    $scope.getaddedproduct[i].total = parseInt($scope.getaddedproduct[i].orderedQty) * $scope.getaddedproduct[i].packagePurchasePrice - $scope.getaddedproduct[i].discountVal;
                    //if ($scope.getaddedproduct[i].vendorReturn != true)
                    //{
                    // $scope.getaddedproduct[i].productStock = $scope.addselectedproduct.productStock;
                    $scope.retTotal += $scope.getaddedproduct[i].total;
                    // }

                }
            }
            // $scope.addproductDetails.selectedProduct = $scope.addselectedproduct;
            $scope.tempReturnItem.push($scope.addproductDetails);

        }
        $scope.emptyValues();
        var elm = document.getElementById("drugName1");
        elm.focus();

    };

    $scope.emptyValues = function () {
        $scope.selectedProduct = null;
        $scope.quantityExceed = 0;

        $scope.addselectedproduct = {};
        $scope.addproductDetails = {};
        $scope.addproductDetails.transactionType = "return";
        $scope.addproductDetails.vendorReturn = true;
        $scope.addselectedproduct.productquantity = null;
        $scope.VendorPurchaseReturnItems1 = {};
        $scope.batchListNew = [];
    }

    $scope.removeProductItem = function (item, index) {
        if (!$scope.returnStatus) {
            $scope.getaddedproduct.splice(index, 1);
            $scope.vendorPurchaseReturn.VendorPurchaseReturnItem.splice(index, 1);
            $scope.retTotal = $scope.retTotal - (item.orderedQty * item.purchasePrice);
            $scope.selectedBatch = '--Select Batch Number--';
            var elem = document.getElementById("drugName1");
            elem.focus();
            $scope.returnStatus = false;
        }
    }

    $scope.editProductItem = function (item, index) {
        if (!$scope.returnStatus) {
            $scope.returnStatus = true;
            $scope.getaddedproduct[index].editprodItem = true;
            $scope.retTotal = $scope.retTotal - (item.orderedQty * item.purchasePrice);
        }
    }

    $scope.addReturnToPurchase = function () {
        $scope.close($scope.tempReturnItem);
    }

    $scope.saveProductItem = function (item, index) {

        if (item.orderedQty > item.stock) {
            $scope.getaddedproduct[index].editprodItem = true;
            return;
        }
        if (item.orderedQty <= 0) {
            $scope.returnStatus = true;
            return;
        }
        $scope.getaddedproduct[index].editprodItem = false;
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].OrderedQty = item.orderedQty;
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].ProductStockId = item.ProductStockId;
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].ReturnedTotal = item.orderedQty * item.purchasePrice;
        $scope.retTotal = $scope.retTotal + (item.orderedQty * item.purchasePrice);
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].ReturnedQuantity = item.orderedQty;
        $scope.vendorPurchaseReturn.VendorPurchaseReturnItem[index].ReturnPurchasePrice = item.purchasePrice;
        $scope.returnStatus = false;
    }

    window.onkeyup = function (event) {
        if (event.keyCode == 27) {
            document.getElementById("popupId").click();
        }
    }

    $scope.batchSelected = function (selectedBatch) {
        if (selectedBatch != null && selectedBatch != undefined && (typeof (selectedBatch) == "object")) {
            $scope.selectedBatch = selectedBatch;
            $scope.selectedBatchNum = $scope.selectedBatch.productStock.batchNo;
            $scope.selectedBatchTotStock = $scope.selectedBatch.productStock.totalStock;
            $scope.productQty = $scope.selectedBatch.productStock.totalStock;
            $scope.packageSellingPrice = $scope.selectedBatch.productStock.sellingPrice;
            if ($scope.selectedBatch.productStock.packageSize == null || $scope.selectedBatch.productStock.packageSize == undefined || $scope.selectedBatch.productStock.packageSize == 0)
            {
                $scope.packageSize = 1;
                $scope.selectedBatch.productStock.packageSize = 1;
            }
            else
            {
                $scope.packageSize = $scope.selectedBatch.productStock.packageSize;
            }
            
            $scope.isValidQty = false;
            $scope.addselectedproduct = $scope.selectedBatch;
            $scope.addselectedproduct.packageSellingPrice = $scope.addselectedproduct.productStock.sellingPrice * $scope.addselectedproduct.productStock.packageSize;
            var ele = document.getElementById("productqtyId");
            ele.focus();
            $scope.addselectedproduct.productquantity = null;
        }
        else {
            $scope.selectedBatchTotStock = undefined;
        }
    }

    $scope.availStrip = function (totalStock, packageSize) {
        var availStrips = totalStock / packageSize;
        if (availStrips < 1) {
            return 0;
        }
        else {
            return parseInt(availStrips);
        }

    }

    $scope.OnProductSelect = function (obj) {
        $scope.addselectedproduct = {};
        $scope.ProductStockId = obj.productStock.id;
        $scope.selectedProduct = obj.productStock.name;
        $scope.productQty = obj.productStock.totalStock;
        $scope.selectedBatchTotStock = obj.productStock.totalStock;
        if (obj.productStock.packageSize == null || obj.productStock.packageSize == undefined  || obj.productStock.packageSize == 0)
        {
            $scope.packageSize = 1;
            obj.productStock.packageSize = 1;
        }
        else
        {
            $scope.packageSize = obj.productStock.packageSize;
        }
        
        $scope.addselectedproduct = obj;
        $scope.addselectedproduct.packageSellingPrice = $scope.addselectedproduct.productStock.sellingPrice * $scope.addselectedproduct.productStock.packageSize;
        var j = 0;
        for (var i = 0; i < $scope.batchList.length; i++) {
            if (obj.productStock.name == $scope.batchList[i].productStock.name) {
                // Added by Gavaskar 12-09-2017 Start
                if ($scope.batchList[i].packageSize == null || $scope.batchList[i].packageSize == undefined || $scope.batchList[i].packageSize == 0)
                {
                    $scope.batchList[i].productStock.packageSize = 1;
                }
                // Added by Gavaskar 12-09-2017 End
                $scope.batchListNew[j] = $scope.batchList[i];
                j++;
            }
        }
        $scope.selectedBatch = $scope.batchListNew[0];
        $scope.selectedBatchNum = $scope.selectedBatch.productStock.batchNo;
        $scope.addselectedproduct.productquantity = null;

        var ele = document.getElementById("productqtyId");
        ele.focus();
        if ($scope.taxValuesList.length > 0) {
            var taxList = $filter("filter")($scope.taxValuesList, { tax: parseFloat($scope.addselectedproduct.productStock.gstTotal) }, true);
            if (taxList.length == 0) {
                return;
            }
        }
    }

    $scope.focusQuantity = function () {
        if ($scope.selectedProduct == null || $scope.selectedProduct == "")
            return;
        $scope.selectedBatch = $scope.batchListNew[0];
        $scope.selectedBatchTotStock = $scope.selectedBatch.productStock.totalStock;
        $scope.selectedBatchNum = $scope.selectedBatch.productStock.batchNo;
        var ele = document.getElementById("productqtyId");
        ele.focus();
    };

    $scope.focusBatchNumber = function () {
        $scope.fsbatchNumber = true;
    }

    $scope.OnselectedVendor = function (data) {
        $scope.VendorId = data.id;
        $scope.vendorName = data.name;
        $scope.isValidVendor = false;
        $scope.focusReason = true;
    }

    $scope.changeReason = function () {
        $scope.focusReason = false;
        document.getElementById("drugName1").focus();
    }

    $scope.focusToProd = function () {
        $scope.focusReason = false;
        document.getElementById("drugName1").focus();
    }

    $scope.focusToReason = function () {
        $scope.focusReason = true;
    }

    $scope.focusAddProduct = function () {

        if ($scope.addselectedproduct.productquantity == undefined) {
            var ele = document.getElementById("productqtyId")
            ele.focus();
            return;
        }

        if (!$scope.isValidQty) {
            var ele = document.getElementById("BtnaddProduct");
            ele.focus();
        }
    }

    

    $scope.IsvalidProductQty = function () {

        var productquantity = $scope.addselectedproduct.productquantity || 0;
        var amount = productquantity * $scope.addselectedproduct.packagePurchasePrice;
        //var amount = ($scope.addselectedproduct.purchasePrice * (productquantity)); //Math.round(value, 2);
       // var amount = ($scope.addselectedproduct.purchasePrice * (productquantity * $scope.packageSize)); //Math.round(value, 2);
        $scope.addselectedproduct.amount = amount.toFixed(2);
        var checkStrips = ($scope.productQty / $scope.packageSize);

        if (productquantity == 0) {
            $scope.isValidQuantity = true;
            return;
        }
        else if (checkStrips != undefined && productquantity > checkStrips) {
            document.getElementById("productqtyId").focus();
            $scope.isValidQty = true;
            $scope.isValidQuantity = false;
            return;
        }
        else {
            $scope.isValidQty = false;
            $scope.isValidQuantity = false;
            return;
        }
    };

    $scope.getreturnProducts = function (val) {
        if ($scope.selectedBatch != null)
            $scope.isValidQty = false;

        $scope.productName = val;
        $scope.isBatch = false;

        return vendorPurchaseService.getPurchaseReturnItem($scope.productName).then(function (response) {
            $scope.batchList = response.data;
            $scope.batchListNew = [];
            var origArr = response.data;
            var newArr = [],
           origLen = origArr.length,
           found, x, y;

            for (x = 0; x < origLen; x++) {
                found = undefined;
                for (y = 0; y < newArr.length; y++) {
                    if ($filter('uppercase')(origArr[x].productStock.productId) === $filter('uppercase')(newArr[y].productStock.productId)) {
                        found = true;
                        newArr[y].productStock.stock = newArr[y].productStock.stock + origArr[x].productStock.stock;
                        break;
                    }
                }
                if (!found) {
                    newArr.push(origArr[x]);
                }
            }

            return newArr.map(function (item) {
                return item;
            });
        });
    };

    //$rootScope.$on("returnPopupEvent", function () {
    //    $scope.close("No");
    //});

    $scope.CheckName = function () {
        if ($scope.selectedProduct == null || $scope.selectedProduct == "") {
            $scope.selectedProduct = null;
            $scope.batchList = [];
            $scope.batchListNew = [];
            $scope.addselectedproduct.productquantity = null;
            $scope.addselectedproduct = { purchasePrice: null };
            return;
        }
    }

    // added by Gavaskar 13-09-2017

    for (var i = 0; i < vendorPurchaseItem.length; i++) {
        if (vendorPurchaseItem[i].vendorReturn) {
            $scope.getaddedproduct.push(vendorPurchaseItem[i]);
        }
    }

   // Purchase Edit return popup issue fixed by Gavaskar 14-09-2017
    ////for (var i = 0; i < $scope.getaddedproduct.length; i++) {
    ////    if ($scope.getaddedproduct[i].vendorReturn) {
    ////        if ($scope.addproductDetails.selectedProduct == null && $scope.addproductDetails.selectedProduct == undefined) {
    ////            $scope.editprodItem = false;
    ////            $scope.vendorReturn = true;
    ////            $scope.pname = $scope.getaddedproduct[i].productStock.product.name;
    ////            $scope.orderedQty = $scope.getaddedproduct[i].orderedQty;
    ////            $scope.batchNo = $scope.getaddedproduct[i].productStock.batchNo;
    ////            $scope.addproductDetails.selectedProduct = $scope.getaddedproduct[i].productStock.product;
    ////            $scope.addproductDetails.selectedProduct.id = $scope.getaddedproduct[i].productStock.productId;
    ////            $scope.expireDate = $filter('date')(new Date($scope.getaddedproduct[i].productStock.expireDate), "MM/yy");
    ////        }
    ////    }
    ////}

});