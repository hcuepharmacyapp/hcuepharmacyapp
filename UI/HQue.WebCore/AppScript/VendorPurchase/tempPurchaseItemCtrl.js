app.controller('tempPurchaseItemCtrl', function ($scope, close, vendorPurchaseService, productId, GSTEnabled, purchaseMode, taxRefNo) {
    $scope.GSTEnabled = GSTEnabled;
    $scope.mode = purchaseMode;
    $scope.vendorPurchase = { taxRefNo: taxRefNo };
    var temp = {};
    $scope.temp = temp;
    $scope.tempItems = null;
    $scope.selectTempItem = function (item) {
        $scope.temp = item;
    };
    var purchaseItem = "";
    $scope.includeTempItem = function () {
        purchaseItem = $scope.temp;
        purchaseItem.fromTempId = purchaseItem.id;
        purchaseItem.tempItem = true;
        purchaseItem.id = null;
        $scope.close('Yes');

    };
    $scope.loadTempStock = function (productId) {
        $.LoadingOverlay("show");
        vendorPurchaseService.loadTempVendorPurchaseItem(productId).then(function (response) {
            $scope.tempItems = response.data;
            //for (var i = 0; i < $scope.tempItems.length; i++) {
            //    $scope.tempItems[i].productStock.vAT = $scope.tempItems[i].productStock.vat;
            //    $scope.tempItems[i].packagePurchasePrice = 0;
            //    $scope.tempItems[i].packagePurchasePrice = $scope.tempItems[i].packageSellingPrice - ($scope.tempItems[i].packageSellingPrice * 20) / 100;
            //}
            $.LoadingOverlay("hide");
            $scope.temp = $scope.tempItems[0];
        }, function () { });
    };
    $scope.loadTempStock(productId);
    $scope.close = function (result) {
        var object = {};
        $(".modal-backdrop").hide();

        if (result == "Yes") {

            object = { "status": result, "data": purchaseItem };
        } else {

            $scope.temp.drugName = null;
            $scope.temp = null;
            object = { "status": result, "data": null };

        }
        close(object, 500);

    };
    $scope.getEnableSelling = function () {
        vendorPurchaseService.getEnableSelling()
            .then(function (response) {
                $scope.enableSelling = response.data;
            }, function (error) {
                console.log(error);
            });
    };
    $scope.getEnableSelling();
});