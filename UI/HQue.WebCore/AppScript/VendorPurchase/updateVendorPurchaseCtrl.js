﻿//app.controller('updatevendorPurchaseCtrl', function ($scope, toastr, vendorPurchaseService, vendorPurchaseModel, vendorPurchaseItemModel, productStockModel, vendorService, productService, productModel, ModalService, $filter, productStockService, shortcutHelper)
//    {
app.controller('updatevendorPurchaseCtrl', function ($scope, toastr, vendorPurchaseService, vendorPurchaseModel, draftVendorPurchaseModel, vendorPurchaseItemModel, productStockModel, vendorService, productService, productModel, ModalService, $filter, productStockService, cacheService, dCVendorPurchaseItemModel, vendorOrderService, vendorOrderItemModel, shortcutHelper, vendorReturnModel, vendorReturnItemModel) {

    $scope.minDate = new Date();
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        "opened": false
    };
    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        "opened": false
    };
    $scope.open3 = function (purchaseItem) {
        purchaseItem.opened = true;
    };
    $scope.popup3 = {
        opened: false
    };
    $scope.open4 = function () {
        $scope.popup4.opened = true;
    };
    $scope.popup4 = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.previouspurchases = [];
    $scope.Enablepurchasebtn = false;
    $scope.EnableSavebtn = false;
    $scope.isAllowDecimal = false;
    $scope.saveKey = true;
    $scope.enableSelling = false;
    $scope.fooAddProd = false;
    $scope.AnyPopupOpened = false;
    $scope.purchaseAmount = 0;
    $scope.returnAmount = 0;
    $scope.returnRoundedAmount = 0;
    $scope.vendorStatus = 2;

    $scope.toDate = function (dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    };
    $scope.enableAddnewStock = function () {
        var prod = document.getElementById("drugName");
        prod.disabled = false;
        prod.focus();
        $scope.AddNewMode = true;
        if ($scope.vendorPurchase.selectedVendor.discount) {
            $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
        } else {
            $scope.vendorPurchaseItem.discount = 0;
        }
    };
    $scope.disableAddnewStock = function () {
        var prod = document.getElementById("drugName");
        prod.disabled = true;
        $scope.AddNewMode = false;
    };
    $scope.isFormValid = true;
    $scope.scanBarcodeOption = null;
    //$scope.validateInvoiceDate = function () {
    //    var val = (toDate($scope.buyForm.invoiceDate.$viewValue) < $scope.minDate);
    //    $scope.buyForm.invoiceDate.$setValidity("InValidInvoiceDate", val);
    //    console.log(val);
    //    $scope.isFormValid = val;
    //};
    $scope.validateInvoiceDate = function (date) {
        $scope.validatepurchase(12);
        if (date == "") {
            $scope.isFormValid = false;
        } else {
            $scope.isFormValid = true;
        }
    };
    $scope.remainStock = 0;
    $scope.soldOrReturned = 0;
    $scope.validateFreeQty = function () {
        $scope.validatepurchase(6);
        if ($scope.vendorPurchaseItem.packageQty != null && $scope.vendorPurchaseItem.packageQty != undefined) {
            if (!$scope.isAllowDecimal) {
                if (parseFloat($scope.vendorPurchaseItem.packageQty) % 1 != 0) {
                    $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidRoundQty", false);
                } else {
                    $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidRoundQty", true);
                }
            }
        }
        if ($scope.vendorPurchaseItem.freeQty != null && $scope.vendorPurchaseItem.freeQty != undefined && $scope.vendorPurchaseItem.freeQty != "") {
            if (!$scope.isAllowDecimal) {
                if (parseFloat($scope.vendorPurchaseItem.freeQty) % 1 != 0) {
                    $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", false);
                } else {
                    $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", true);
                }
            }
        } else {
            $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", true);
        }
        if ($scope.isAllowDecimal) {
            var qty = ($scope.vendorPurchaseItem.packageQty != null && $scope.vendorPurchaseItem.packageQty != undefined && $scope.vendorPurchaseItem.packageQty != "") ? parseFloat($scope.vendorPurchaseItem.packageQty) : 0;
            var freeqty = ($scope.vendorPurchaseItem.freeQty != null && $scope.vendorPurchaseItem.freeQty != undefined && $scope.vendorPurchaseItem.freeQty != "") ? parseFloat($scope.vendorPurchaseItem.freeQty) : 0;
            var tot = (qty + freeqty) % 1;
            if (tot !== 0) {
                $scope.vendorPurchaseItems.freeQty.$setValidity("InvalidSumQty", false);
            } else {
                $scope.vendorPurchaseItems.freeQty.$setValidity("InvalidSumQty", true);
            }
        }
        $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidfreeQty", ((parseFloat($scope.vendorPurchaseItem.packageQty || 0) + parseFloat($scope.vendorPurchaseItem.freeQty || 0)) >= $scope.soldOrReturned));


    };
    $scope.validateFreeQty1 = function () {
        $scope.vendorPurchaseItems.freeQty.$setValidity("InValidfreeQty1", $scope.vendorPurchaseItem.oldFreeQty <= $scope.vendorPurchaseItem.freeQty);
    };
    function toDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
    $scope.updateUI = function (type) {

        if ($scope.GSTEnabled) {

            $scope.vendorPurchaseItem.productStock.igst = "";
            $scope.vendorPurchaseItem.productStock.cgst = "";
            $scope.vendorPurchaseItem.productStock.sgst = "";
            $scope.vendorPurchaseItem.productStock.gstTotal = "";

        } else {

            ////var cst = document.getElementById("cST");
            ////var vat = document.getElementById("vat");
            ////$scope.vendorPurchaseItem.productStock.vAT = 0;
            ////$scope.vendorPurchaseItem.productStock.cST = 0;
            ////if ($scope.vendorPurchase.selectedVendor.enableCST) {
            ////    vat.disabled = false;
            ////    cst.disabled = false;
            ////    $scope.vendorPurchase.TaxationType = "CST";
            ////    $scope.vendorPurchaseItem.productStock.cST = 2;
            ////} else {
            ////    vat.disabled = false;
            ////    cst.disabled = true;
            ////    $scope.vendorPurchase.TaxationType = "VAT";
            ////    $scope.vendorPurchaseItem.productStock.vAT = 5;
            ////}

            var cst = document.getElementById("cst");
            var vat = document.getElementById("vat");
            $scope.vendorPurchaseItem.productStock.vAT = 0;
            $scope.vendorPurchaseItem.productStock.cst = 0;
            if ($scope.vendorPurchase.selectedVendor.enableCST) {
                vat.disabled = false;
                cst.disabled = false;
                $scope.vendorPurchase.TaxationType = "CST";
                $scope.vendorPurchaseItem.productStock.cst = 2;
            } else {
                if ($scope.vendorPurchase.vendor.enableCST && $scope.vendorPurchase.vendorId == $scope.vendorPurchase.selectedVendor.id) {
                    vat.disabled = false;
                    cst.disabled = false;
                    $scope.vendorPurchase.TaxationType = "CST";
                    $scope.vendorPurchaseItem.productStock.cst = 2;
                } else {
                    vat.disabled = false;
                    cst.disabled = true;
                    $scope.vendorPurchase.TaxationType = "VAT";
                    $scope.vendorPurchaseItem.productStock.vAT = 5;
                }
            }
        }

        

        if ($scope.vendorPurchase.selectedVendor.discount) {
            $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
        } else {
            $scope.vendorPurchaseItem.discount = 0;
        }
        if ($scope.vendorPurchase.vendorId != $scope.vendorPurchase.selectedVendor.id) {
            $scope.EnableSavebtn = true;
        }
        $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;


        if (type == "ChangedDropdown") {
            $scope.onVendorSelect();
        }
        $scope.calculateGSTValue(false, false);
    };
    $scope.EditMode = false;
    $scope.AddNewMode = false;
    $scope.returnItemEdit = false;
    $scope.Action = "Add";
    $scope.ResetAction = "Reset";
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    var vendorPurchase = vendorPurchaseModel;
    var vendorPurchaseItem = vendorPurchaseItemModel;
    vendorPurchaseItem.productStock = productStockModel;
    vendorPurchaseItem.productStock.product = productModel;
    $scope.product = productModel;
    $scope.vendorPurchase = vendorPurchase;
    $scope.vendorPurchase.overAllDiscount = 0;
    $scope.vendorPurchaseItem = vendorPurchaseItem;
    $scope.vendorPurchaseItem.productStock.vAT = 5;
    $scope.vendorPurchaseItem.tax = 0;
    $scope.vendorPurchaseItem.total = 0;
    $scope.vendorPurchaseItem.freeQty = 0;
    $scope.vendorPurchaseItem.discount = 0;
    $scope.vendorPurchaseItem.orgDiscount = 0;
    $scope.vendorPurchaseItem.discountVal = 0;
    $scope.vendorPurchaseItem.orderedQty = 0;
    $scope.vendorPurchase.vendorPurchaseItem = [];
    $scope.vendorPurchase.selectedVendor = {};
   
    $scope.selectedProduct = null;
    $scope.Math = window.Math;
    $scope.vendorPurchase.TaxationType = "VAT";
    $scope.vendorPurchase.invoiceDate = new Date();
    $scope.vendorPurchase.total = 0;
    $scope.vendorPurchase.discount = 0;
    $scope.vendorPurchase.credit = 0;
    $scope.list = [];
    $scope.editItems = 0;
    $scope.copyedIndex = {};
    shortcutHelper.setScope($scope);
    $scope.keydown = shortcutHelper.purchaseEditShortcuts;


    function GetTotalPrice() {
        var amount = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    amount += $scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty;
                }
            }
        }
        return amount;
    }


    $scope.calDiscountInRupees = function (type) {
        //  $scope.vendorPurchase.noteAmount = "";

        if ($scope.vendorPurchase.discountInRupees != "") {
            if ($scope.vendorPurchase.discountInRupees > $scope.vendorPurchase.tempTotal) {
                $scope.errorPercentAmount = true;
                return false;
            } else {
                $scope.errorPercentAmount = false;
            }
            var discount = $scope.vendorPurchase.discountInRupees * 100 / GetTotalPrice();
        } else {
            $scope.vendorPurchase.discount = 0;
            $scope.errorPercentAmount = false;
            discount = 0;
        }
        $scope.vendorPurchase.discountVal = 0;


        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {


            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {

                $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number(discount);

                if ($scope.vendorPurchase.vendorPurchaseItem[i].discount > 99.99) {
                    $scope.errorExcessPercent = true;
                } else {
                    $scope.errorExcessPercent = false;
                }


                var qty = ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty || 0) - ($scope.vendorPurchase.vendorPurchaseItem[i].freeQty || 0);



                $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);

                $scope.vendorPurchase.overAllDiscount += $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;

                $scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;


                //if (type == "ReCalculation") {

                if ($scope.vendorPurchase.taxRefNo == 1) {
                    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);
                }
                else {
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT > 0) {
                        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                    } else {
                        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST / 100);
                    }
                }

                //} else {


                //    if ($scope.vendorPurchaseItem.productStock.vAT > 0) {

                //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                //    } else {
                //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST / 100);
                //    }
                //}


                if ($scope.vendorPurchase.vendorPurchaseItem[i].action == undefined) {
                    if (type == "") {
                        $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                    }
                } else {
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].action != "I") {
                        $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                    }
                }

                }
            }
        }







        if ($scope.vendorPurchase.discountInRupees == "") {
            $scope.vendorPurchase.discount = "";
        } else {
            $scope.vendorPurchase.discount = discount;
        }
        $scope.isPageLoad = false;
        setTotal("");
        setVat();

        $scope.EnableSavebtn = true; // Update button Enable by Gavaskar 18-09-2017

        //for (var x = 0; x < $scope.vendorPurchase.vendorPurchaseItem.length; x++) {
        //    $scope.vendorPurchase.vendorPurchaseItem[x].action = "U";
        //}
    };






    $scope.calDiscountOnBill = function (type) {
        //$scope.vendorPurchase.noteAmount = "";

        if ($scope.vendorPurchase.discount == "" || $scope.vendorPurchase.discount == "0") {
            $scope.vendorPurchase.discountInRupees = "";

        }
        if ($scope.vendorPurchase.discount > 100) {
            $scope.errorExcessPercent = true;
            return;
        } else {
            $scope.errorExcessPercent = false;
        }
        $scope.vendorPurchase.total = 0;
        $scope.vendorPurchase.overAllDiscount = 0;

        //for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

        //    if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {

        //        $scope.vendorPurchase.total += parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice);

        //    }
        //}

        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number($scope.vendorPurchase.discount);
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].discount > 99.99) {
                        $scope.errorExcessPercent = true;
                        return false;
                    } else {
                        $scope.errorExcessPercent = false;
                    }
                    var qty = ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty || 0) - ($scope.vendorPurchase.vendorPurchaseItem[i].freeQty || 0);

                $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);


                $scope.vendorPurchase.overAllDiscount += $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;


                $scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;

                $scope.vendorPurchase.total += parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice);


                //if (type == "ReCalculation") {
                if ($scope.vendorPurchase.taxRefNo == 1) {
                    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);
                } else {
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT > 0) {
                        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                    } else {
                        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST / 100);
                    }
                }

                //} else {

                //    if ($scope.vendorPurchaseItem.productStock.vAT > 0) {
                //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                //    } else {
                //        $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cST / 100);
                //    }
                //}

                if ($scope.vendorPurchase.vendorPurchaseItem[i].action == undefined) {
                    if (type == "") {
                        $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                    }
                } else {
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].action != "I") {
                        $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                    }
                }

                }
            }
        }


        if ($scope.vendorPurchase.discount == "") {
            $scope.vendorPurchase.discountInRupees = "";
        } else {
            $scope.vendorPurchase.discountInRupees = GetTotalPrice() * $scope.vendorPurchase.discount / 100;
        }
        $scope.isPageLoad = false;
        setTotal("");
        setVat();

        $scope.EnableSavebtn = true; // Update button Enable by Gavaskar 18-09-2017

        //for (var x = 0; x < $scope.vendorPurchase.vendorPurchaseItem.length; x++) {
        //    $scope.vendorPurchase.vendorPurchaseItem[x].action = "U";
        //}
    };






    function setTotal(type) {
        $scope.vendorPurchase.total = 0;
        $scope.vendorPurchase.overAllDiscount = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    $scope.vendorPurchase.total += $scope.vendorPurchase.vendorPurchaseItem[i].total;
                    $scope.vendorPurchase.overAllDiscount += $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;
                }
            }
        }
        if (type == "NewStock" || $scope.vendorPurchase.discountInRupees == "") {
            $scope.vendorPurchase.tempTotal = $scope.vendorPurchase.total;
        }
        setTotaldiscount();
    }






    function setTotaldiscount() {
        $scope.vendorPurchase.discountOnBill = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            var total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * ($scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty));
            $scope.vendorPurchase.discountOnBill += total - (total * ($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount / 100));
        }
        $scope.vendorPurchase.discountOnBill = $scope.vendorPurchase.discountOnBill * ($scope.vendorPurchase.discount / 100);
    }

    function setReturnAmt() {

        var returntotal = 0;
        $scope.vendorPurchase.tax = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if ($scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                    returntotal += (parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) * parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice)) - parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].discountVal);

                    $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;
                    $scope.vendorPurchase.vendorPurchaseItem[i].returnPurchasePrice = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) * parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice) + parseFloat($scope.vendorPurchase.tax);
                    // Added by Gavaskar 05-09-2017 Discount,Discountvalue, Tax Amount
                    $scope.vendorPurchase.vendorPurchaseItem[i].discount = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].discount);
                    $scope.vendorPurchase.vendorPurchaseItem[i].discountValue = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].discountVal);
                    $scope.vendorPurchase.vendorPurchaseItem[i].gstAmount = parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].tax);
                }
            }
        }

        // $scope.returnAmount = parseFloat(returntotal) + parseFloat($scope.vendorPurchase.tax); // Comment by Gavaskar 05-09-2017
        $scope.returnAmount = parseFloat(returntotal) + parseFloat($scope.vendorPurchase.tax);

        var afterRoundUp = Math.round($scope.returnAmount);
        $scope.returnRoundedAmount = afterRoundUp - $scope.returnAmount;
        $scope.returnAmount = Math.round($scope.returnAmount);
        $scope.vendorPurchase.vendorPurchaseItem.returnedTotal = parseFloat($scope.returnAmount);
        $scope.vendorPurchase.returnRoundedValue = $scope.returnRoundedAmount;

        

    }

    function setVat() {
        $scope.vendorPurchase.tax = 0;
        $scope.vendorPurchase.igst = 0;
        $scope.vendorPurchase.cgst = 0;
        $scope.vendorPurchase.sgst = 0;
        for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {
            if (!$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {
                if (!isNaN(parseFloat($scope.vendorPurchase.vendorPurchaseItem[i].tax))) {
                    if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                        $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;
                        $scope.vendorPurchase.igst += $scope.vendorPurchase.vendorPurchaseItem[i].igst;
                        $scope.vendorPurchase.cgst += $scope.vendorPurchase.vendorPurchaseItem[i].cgst;
                        $scope.vendorPurchase.sgst += $scope.vendorPurchase.vendorPurchaseItem[i].sgst;
                    }
                    

                } else {
                    if ($scope.vendorPurchase.taxRefNo == 1) {
                        $scope.vendorPurchase.vendorPurchaseItem[i].tax = $scope.vendorPurchase.vendorPurchaseItem[i].total * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal) / 100;
                    }
                    else {
                        $scope.vendorPurchase.vendorPurchaseItem[i].tax = $scope.vendorPurchase.vendorPurchaseItem[i].total * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst > 0 ? $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst : $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT) / 100;
                    }
                    if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                        $scope.vendorPurchase.tax += $scope.vendorPurchase.vendorPurchaseItem[i].tax;
                    }
                }
            }
        }

        if ($scope.returnAmount == undefined || $scope.returnAmount == null || $scope.returnAmount == "") {
            $scope.returnAmount = 0;
        }
        var noteamount = parseFloat($scope.vendorPurchase.noteAmount) || 0;
        if ($scope.vendorPurchase.noteType == "credit") {
            $scope.purchaseAmount = parseFloat($scope.vendorPurchase.total) + parseFloat($scope.vendorPurchase.tax) - noteamount;
            $scope.purchaseAmount = Math.round($scope.purchaseAmount);
        }
        if ($scope.vendorPurchase.noteType == "debit") {
            $scope.purchaseAmount = parseFloat($scope.vendorPurchase.total) + parseFloat($scope.vendorPurchase.tax) + noteamount;
            $scope.purchaseAmount = Math.round($scope.purchaseAmount);
        }
        var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax - $scope.returnAmount;

        if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == null) {
                $scope.vendorPurchaseItem.productStock.igst = 0;
                $scope.vendorPurchase.cgst = $scope.vendorPurchase.tax / 2;
                $scope.vendorPurchase.sgst = $scope.vendorPurchase.tax / 2;
           
        }
        else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
                $scope.vendorPurchaseItem.productStock.cgst = 0;
                $scope.vendorPurchaseItem.productStock.sgst = 0;
                $scope.vendorPurchase.igst = $scope.vendorPurchase.tax;
            
        }
        else {
            $scope.vendorPurchaseItem.productStock.gstTotal = 0;
            $scope.vendorPurchaseItem.productStock.igst = 0;
            $scope.vendorPurchaseItem.productStock.cgst = 0;
            $scope.vendorPurchaseItem.productStock.sgst = 0;
        }

        ////$scope.purchaseAmount = parseFloat($scope.vendorPurchase.total);
        ////var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax - $scope.returnAmount;

        ////if ($scope.vendorPurchase.selectedVendor.locationType == 1 || $scope.vendorPurchase.selectedVendor.locationType == null) {
        ////    $scope.vendorPurchaseItem.productStock.igst = 0;
        ////    $scope.vendorPurchase.cgst = $scope.vendorPurchase.tax / 2;
        ////    $scope.vendorPurchase.sgst = $scope.vendorPurchase.tax / 2;
        ////}
        ////else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
        ////    $scope.vendorPurchaseItem.productStock.cgst = 0;
        ////    $scope.vendorPurchaseItem.productStock.sgst = 0;
        ////    $scope.vendorPurchase.igst = $scope.vendorPurchase.tax;
        ////}
        ////else {
        ////    $scope.vendorPurchaseItem.productStock.gstTotal = 0;
        ////    $scope.vendorPurchaseItem.productStock.igst = 0;
        ////    $scope.vendorPurchaseItem.productStock.cgst = 0;
        ////    $scope.vendorPurchaseItem.productStock.sgst = 0;
        ////}
        roundingTotal(total);
    }
    $scope.init = function (id, GSTEnabled) {
        if (GSTEnabled == "True") {
            $scope.GSTEnabled = true;
        } else {
            $scope.GSTEnabled = false;
        }
        $scope.enableAddnewStock();
        $scope.vendorPurchase.id = id;
        getIsAllowDecimalSettings();
        if (id != "") {
            $.LoadingOverlay("show");
            vendorPurchaseService.editVendorPurchaseData($scope.vendorPurchase.id)
                .then(function (response) {

                    $scope.vendorPurchase = response.data;





                    $scope.vendorPurchase.selectedVendor = {};
                    $scope.vendorPurchase.selectedVendor.id = $scope.vendorPurchase.vendorId;
                    $scope.vendorPurchase.selectedVendor.discount = $scope.vendorPurchase.vendor.discount;
                    if ($scope.vendorPurchase.vendor.locationType == null || $scope.vendorPurchase.vendor.locationType == "")
                    {
                        $scope.vendorPurchase.selectedVendor.locationType = 1;
                    }
                    else
                    {
                        $scope.vendorPurchase.selectedVendor.locationType = $scope.vendorPurchase.vendor.locationType;
                    }

                    if ($scope.vendorPurchase.billSeries != undefined && $scope.vendorPurchase.billSeries != null) {
                        $scope.vendorPurchase.goodsRcvNoTemp = $scope.vendorPurchase.billSeries.concat($scope.vendorPurchase.goodsRcvNo);
                    } else {
                        $scope.vendorPurchase.goodsRcvNoTemp = $scope.vendorPurchase.goodsRcvNo;
                    }

                    if ($scope.vendorPurchase.noteType == undefined || $scope.vendorPurchase.noteType == null) {
                        $scope.vendorPurchase.noteType = "credit";
                        $scope.vendorPurchase.noteAmount = "";
                    }



                    $scope.vendorPurchase.overAllDiscount = 0;

                    for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

                        if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {

                            if ($scope.vendorPurchase.vendorPurchaseItem[i].fromDcId == "") {
                                $scope.vendorPurchase.vendorPurchaseItem[i].fromDcId = null;
                            }

                            if ($scope.vendorPurchase.vendorPurchaseItem[i].fromTempId == "") {
                                $scope.vendorPurchase.vendorPurchaseItem[i].fromTempId = null;
                            }

                            $scope.vendorPurchase.vendorPurchaseItem[i].isEdit = false;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct = {};
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.name = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.eancode = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.eancode;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.id;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.manufacturer = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.manufacturer;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.type = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.type;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.schedule = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.schedule;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.category = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.category;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.packing = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.packing;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.genericName = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.genericName;


                            $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty;


                            $scope.vendorPurchase.vendorPurchaseItem[i].packageQty1 = Number($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) + Number($scope.vendorPurchase.vendorPurchaseItem[i].freeQty);


                            $scope.vendorPurchase.vendorPurchaseItem[i].totalQuantity = $scope.vendorPurchase.vendorPurchaseItem[i].packageSize * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty1;


                            $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount) - Number($scope.vendorPurchase.discount);


                            $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number($scope.vendorPurchase.discount);


                            var qty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty;


                            $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);


                            $scope.vendorPurchase.overAllDiscount += $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;



                            $scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;


                            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat;


                            if ($scope.vendorPurchase.taxRefNo == 1) {
                                $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);
                                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;
                            }
                            else {
                                if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst > 0) {

                                    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst / 100);

                                    $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst;
                                    $scope.TaxationType = "CST";

                                } else {

                                    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);

                                    $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT;
                                    $scope.TaxationType = "VAT";

                                }
                            }
                        } // Added Gavaskar Return List
                        else
                        {
                            if ($scope.vendorPurchase.vendorPurchaseItem[i].fromDcId == "") {
                                $scope.vendorPurchase.vendorPurchaseItem[i].fromDcId = null;
                            }

                            if ($scope.vendorPurchase.vendorPurchaseItem[i].fromTempId == "") {
                                $scope.vendorPurchase.vendorPurchaseItem[i].fromTempId = null;
                            }

                            $scope.vendorPurchase.vendorPurchaseItem[i].isEdit = false;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct = {};
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.name = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.batchNo = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.batchNo;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.expireDate = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.expireDate;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.gstTotal = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;

                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.eancode = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.eancode;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.id = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.id;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.manufacturer = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.manufacturer;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.type = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.type;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.schedule = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.schedule;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.category = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.category;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.packing = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.packing;
                            $scope.vendorPurchase.vendorPurchaseItem[i].selectedProduct.genericName = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.genericName;

                            

                            if ($scope.vendorPurchase.vendorPurchaseItem[i].freeQty == undefined || $scope.vendorPurchase.vendorPurchaseItem[i].freeQty == null)
                            {
                                $scope.vendorPurchase.vendorPurchaseItem[i].freeQty = 0;
                            }
                            if ($scope.vendorPurchase.vendorPurchaseItem[i].discount == undefined || $scope.vendorPurchase.vendorPurchaseItem[i].discount == null) {
                                $scope.vendorPurchase.vendorPurchaseItem[i].discount = 0;
                            }
                            else
                            {
                                $scope.vendorPurchase.vendorPurchaseItem[i].discount = $scope.vendorPurchase.vendorPurchaseItem[i].discount;
                            }

                            $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty;


                            $scope.vendorPurchase.vendorPurchaseItem[i].packageQty1 = Number($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) + Number($scope.vendorPurchase.vendorPurchaseItem[i].freeQty);


                            $scope.vendorPurchase.vendorPurchaseItem[i].totalQuantity = $scope.vendorPurchase.vendorPurchaseItem[i].packageSize * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty1;


                            $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount = Number($scope.vendorPurchase.vendorPurchaseItem[i].discount) - Number($scope.vendorPurchase.discount);


                            $scope.vendorPurchase.vendorPurchaseItem[i].discount = Number($scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount) + Number($scope.vendorPurchase.discount);


                            var qty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - $scope.vendorPurchase.vendorPurchaseItem[i].freeQty;


                            $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);


                            $scope.vendorPurchase.overAllDiscount += $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;



                            $scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * qty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;


                            $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vat;


                            if ($scope.vendorPurchase.taxRefNo == 1) {
                                $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);
                                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;
                                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.igst = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;
                                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cgst = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal/2;
                                $scope.vendorPurchase.vendorPurchaseItem[i].productStock.sgst = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal/2;
                            }
                            else {
                                if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst > 0) {

                                    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst / 100);

                                    $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.cst;
                                    $scope.TaxationType = "CST";

                                } else {

                                    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);

                                    $scope.vendorPurchase.vendorPurchaseItem[i].productStock.tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT;
                                    $scope.TaxationType = "VAT";

                                }
                            }

                            // Return Popup Issue
                            $scope.vendorPurchase.vendorPurchaseItem[i].name = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.product.name;
                            $scope.vendorPurchase.vendorPurchaseItem[i].batchNo = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.batchNo;
                            $scope.vendorPurchase.vendorPurchaseItem[i].expireDate = $filter('date')(new Date($scope.vendorPurchase.vendorPurchaseItem[i].productStock.expireDate), "MM/yy");
                            $scope.vendorPurchase.vendorPurchaseItem[i].gstTotal = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;
                           // $scope.vendorPurchase.vendorPurchaseItem[i].tax = $scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal;

                        } // End
                        
                       
                    }


                    if ($scope.vendorPurchase.discount == undefined && $scope.vendorPurchase.discountInRupees == undefined && $scope.vendorPurchase.discountType == undefined) {
                        $scope.vendorPurchase.discount = "";
                        $scope.vendorPurchase.discountInRupees = "";
                        $scope.vendorPurchase.discountType = "percent";
                    }

                    if ($scope.vendorPurchase.discount != undefined) {
                        if ($scope.vendorPurchase.discountType != "rupees") {
                            $scope.vendorPurchase.discountType = "percent";
                        }
                        $scope.vendorPurchase.discountInRupees = GetTotalPrice() * $scope.vendorPurchase.discount / 100;
                    }

                    $scope.returnAmount = $scope.vendorPurchase.returnAmount;
                   

                    $scope.updateUI("");
                    $scope.isPageLoad = true;
                    setTotal("NewStock");
                    setReturnAmt();
                    setVat();
                    $scope.vendorPurchase.initialValue = null;
                    if ($scope.vendorPurchase.paymentType == "Cheque") {
                        $scope.editCheque($scope.vendorPurchase, true, false);
                    } else if ($scope.vendorPurchase.paymentType == "Credit") {
                        $scope.editCheque($scope.vendorPurchase, false, true);
                    }

                    $.LoadingOverlay("hide");
                }, function (error) {
                    console.log(error);
                    $.LoadingOverlay("hide");
                    toastr.error('Error Occured', 'Error');
                });
            $scope.initialValue();
        }
    };
    function getIsAllowDecimalSettings() {
        vendorPurchaseService.getDecimalSetting().then(function (response) {
            $scope.isAllowDecimal = response.data;
        }, function (error) {
            console.log(error);
        });
    }


    $scope.ReCalCulateAllValues = function () {
        if ($scope.vendorPurchase.discountType == "rupees") {
            $scope.calDiscountInRupees("ReCalculation");
        }
        if ($scope.vendorPurchase.discountType == "percent") {
            $scope.calDiscountOnBill("ReCalculation");
        }
    };


    $scope.addStock = function () {
        $scope.EnableSavebtn = true;
        if ($scope.selectedProduct != null) {
            if (typeof $scope.selectedProduct === "string") {
                if (!confirm("This is a new product, do you want to add it ?")) {
                    return;
                }
                $scope.vendorPurchaseItem.productStock.product.name = $scope.selectedProduct;
                $scope.vendorPurchaseItem.productName = $scope.selectedProduct; //Added by Sarubala on 08/09/17
            }
            if ($scope.AddNewMode) {

               

                $scope.vendorPurchaseItem.productStock.product.name = $scope.selectedProduct.name;
                $scope.vendorPurchaseItem.productName = $scope.selectedProduct.name; //Added by Sarubala on 08/09/17
                $scope.vendorPurchaseItem.productStock.productId = $scope.selectedProduct.id;
                $scope.vendorPurchaseItem.productStock.product.manufacturer = $scope.selectedProduct.manufacturer;
                $scope.vendorPurchaseItem.productStock.product.type = $scope.selectedProduct.type;
                $scope.vendorPurchaseItem.productStock.product.schedule = $scope.selectedProduct.schedule;
                $scope.vendorPurchaseItem.productStock.product.category = $scope.selectedProduct.category;
                $scope.vendorPurchaseItem.productStock.product.packing = $scope.selectedProduct.packing;
                $scope.vendorPurchaseItem.productStock.product.genericName = $scope.selectedProduct.genericName;
                $scope.vendorPurchaseItem.action = "I";
                if ($scope.selectedProduct.accountId === null || $scope.selectedProduct.accountId === undefined || $scope.selectedProduct.accountId === "") {
                    $scope.vendorPurchaseItem.productStock.product.productMasterID = $scope.selectedProduct.id;
                } else {
                    $scope.vendorPurchaseItem.productStock.product.productMasterID = null;
                }
            }
            if ($scope.EditMode && $scope.vendorPurchaseItem.isNew != true) {
                $scope.vendorPurchaseItem.action = "U";
            }
            $scope.vendorPurchaseItem.productStock.eancode = $scope.vendorPurchaseItem.productStock.product.eancode;
            $scope.disableAddnewStock();


            $scope.excessdiscount = 0;
            $scope.vendorPurchaseItem.isEdit = false;



            $scope.vendorPurchaseItem.isDeleted = false;


            $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;
            $scope.vendorPurchaseItem.packageQty = $scope.vendorPurchaseItem.packageQty || 0;
            $scope.vendorPurchaseItem.freeQty = $scope.vendorPurchaseItem.freeQty || 0;
            $scope.vendorPurchaseItem.discount = parseFloat($scope.vendorPurchaseItem.discount) || 0;
           
           

            var invoiceDiscount = parseFloat($scope.vendorPurchase.discount) || 0;
            if (($scope.vendorPurchaseItem.discount + invoiceDiscount) > 99.99) {
                $scope.excessdiscount++;
            }
            $scope.vendorPurchaseItem.discountVal = 0;
            $scope.vendorPurchaseItem.orgDiscount = $scope.vendorPurchaseItem.discount;
            if (!$scope.vendorPurchaseItem.vendorReturn)
            {
                $scope.vendorPurchaseItem.discount = Number($scope.vendorPurchaseItem.discount) + invoiceDiscount;
            }
            if ($scope.vendorPurchaseItem.discount > 0) {
                $scope.vendorPurchaseItem.discountVal = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) * ($scope.vendorPurchaseItem.discount / 100);
            }
            $scope.vendorPurchaseItem.total = ($scope.vendorPurchaseItem.packagePurchasePrice * $scope.vendorPurchaseItem.packageQty) - $scope.vendorPurchaseItem.discountVal;


            if ($scope.vendorPurchase.taxRefNo == 1) {
                $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.gstTotal / 100);
            }
            else {
                if ($scope.vendorPurchase.TaxationType == "VAT") {
                    $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.vAT / 100);
                }

                if ($scope.vendorPurchase.TaxationType == "CST") {
                    $scope.vendorPurchaseItem.tax = ($scope.vendorPurchaseItem.total) * ($scope.vendorPurchaseItem.productStock.cst / 100);
                }
            }

            $scope.vendorPurchaseItem.orderedQty = $scope.vendorPurchaseItem.packageQty || 0;


            if ($scope.vendorPurchaseItem.freeQty > 0) {
                $scope.vendorPurchaseItem.packageQty = Number($scope.vendorPurchaseItem.packageQty) + Number($scope.vendorPurchaseItem.freeQty);
            }

            $scope.vendorPurchaseItem.totalQuantity = $scope.vendorPurchaseItem.packageSize * $scope.vendorPurchaseItem.packageQty;
            var _isCST = false;
            if ($scope.vendorPurchase.TaxationType == "VAT") {
                $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.vAT;
                $scope.vendorPurchaseItem.productStock.cst = 0;
                var cst = document.getElementById("cst");
                cst.disabled = true;
            }
            var vat;
            if ($scope.vendorPurchase.TaxationType == "CST") {
                $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.cst;
                $scope.vendorPurchase.TaxationType = "CST";
                _isCST = true;
            }
            if ($scope.vendorPurchase.taxRefNo == 1) {
                $scope.vendorPurchaseItem.productStock.tax = $scope.vendorPurchaseItem.productStock.gstTotal;
            }
            $scope.vendorPurchaseItem.selectedProduct = $scope.selectedProduct;
            $scope.calculateGSTValue(false, true);
            if (!$scope.EditMode) {
                $scope.vendorPurchaseItem.previouspurchases = $scope.previouspurchases;
                var status = 0;
                for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {

                    var tempDiscount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount || 0;
                    if ((tempDiscount + invoiceDiscount) > 99.99) {
                        $scope.excessdiscount++;
                    }
                    if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                        $scope.vendorPurchase.vendorPurchaseItem[i].discount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount + invoiceDiscount;
                        $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);

                    }


                    if ($scope.EditMode == false) {
                        for (var k = 0; k < $scope.vendorPurchase.vendorPurchaseItem.length; k++) {
                            if (($scope.vendorPurchase.vendorPurchaseItem[k].productStock.product.id == $scope.selectedProduct.id)) {  //Added by Sarubala on 27-09-17
                                if ($scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn != null && $scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn != undefined) {
                                    if ($scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn == true) {
                                        toastr.info("Product already added in Return");
                                        document.getElementById("drugName").focus();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    // Product Restriction by Gavaskar 14-09-2017 End 
                    if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId == $scope.vendorPurchaseItem.productStock.productId &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.batchNo == $scope.vendorPurchaseItem.productStock.batchNo &&
                        (($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT == $scope.vendorPurchaseItem.productStock.vAT && !($scope.vendorPurchase.taxRefNo == 1)) 
                        || ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal == $scope.vendorPurchaseItem.productStock.gstTotal && $scope.vendorPurchase.taxRefNo == 1)) &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageSize == $scope.vendorPurchaseItem.packageSize &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice == $scope.vendorPurchaseItem.packagePurchasePrice &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageSellingPrice == $scope.vendorPurchaseItem.packageSellingPrice &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.eancode == $scope.vendorPurchaseItem.productStock.eancode &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageMRP == $scope.vendorPurchaseItem.packageMRP &&
                        !$scope.vendorPurchase.vendorPurchaseItem[i].isDeleted) {


                        alert("This Product Already Added");
                        return false;

                        $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].orderedQty) + parseInt($scope.vendorPurchaseItem.orderedQty);
                        $scope.vendorPurchase.vendorPurchaseItem[i].freeQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].freeQty) + parseInt($scope.vendorPurchaseItem.freeQty);
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) + parseInt($scope.vendorPurchaseItem.packageQty);
                        $scope.vendorPurchase.vendorPurchaseItem[i].totalQuantity = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].totalQuantity) + parseInt($scope.vendorPurchaseItem.totalQuantity);
                        if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                            $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);
                        }
                        
                        $scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;
                        if ($scope.vendorPurchase.taxRefNo == 1) {
                            $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);
                        }
                        else {
                            if ($scope.vendorPurchase.TaxationType == "VAT")
                                $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                            if ($scope.vendorPurchase.TaxationType == "CST")
                                $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].totalproductStock.cST / 100);
                        }
                        status = 1;
                        if ($scope.vendorPurchase.vendorPurchaseItem[i].isNew != true)
                            $scope.vendorPurchase.vendorPurchaseItem[i].action = "U";
                    }
                }
                if ($scope.excessdiscount > 0) {
                    $scope.errorExcessPercent = true;
                } else {
                    $scope.errorExcessPercent = false;
                }
                if (status == 0 || $scope.vendorPurchaseItem.action == "I") {
                    $scope.vendorPurchase.vendorPurchaseItem.push($scope.vendorPurchaseItem);
                } else {
                    $scope.isPageLoad = false;
                    setTotal("");
                    setReturnAmt();
                    setVat();
                    $scope.initialValue();
                    getIsAllowDecimalSettings();
                    var qty = document.getElementById("drugName");
                    qty.focus();
                }
            } else {
                var status = 0;
                for (var i = 0; i < $scope.vendorPurchase.vendorPurchaseItem.length; i++) {


                    var tempDiscount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount || 0;
                    if ((tempDiscount + invoiceDiscount) > 99.99) {
                        $scope.excessdiscount++;
                    }
                    if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                        $scope.vendorPurchase.vendorPurchaseItem[i].discount = $scope.vendorPurchase.vendorPurchaseItem[i].orgDiscount + invoiceDiscount;
                        $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);
                    }



                    if ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.productId == $scope.vendorPurchaseItem.productStock.productId &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.batchNo == $scope.vendorPurchaseItem.productStock.batchNo &&
                       (($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT == $scope.vendorPurchaseItem.productStock.vAT && !($scope.vendorPurchase.taxRefNo == 1)) ||
                        ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal == $scope.vendorPurchaseItem.productStock.gstTotal && $scope.vendorPurchase.taxRefNo == 1)) &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageSize == $scope.vendorPurchaseItem.packageSize &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice == $scope.vendorPurchaseItem.packagePurchasePrice &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageSellingPrice == $scope.vendorPurchaseItem.packageSellingPrice &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].rackNo == $scope.vendorPurchaseItem.rackNo &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].productStock.eancode == $scope.vendorPurchaseItem.productStock.eancode &&
                        $scope.vendorPurchase.vendorPurchaseItem[i].packageMRP == $scope.vendorPurchaseItem.packageMRP) {



                        if ($scope.Editindex == i) {
                            status = 0;
                        } else {
                            $scope.vendorPurchase.vendorPurchaseItem[i].packageQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].packageQty) + parseInt($scope.vendorPurchaseItem.packageQty);
                            $scope.vendorPurchase.vendorPurchaseItem[i].freeQty = parseInt($scope.vendorPurchase.vendorPurchaseItem[i].freeQty) + parseInt($scope.vendorPurchaseItem.freeQty);
                            $scope.vendorPurchase.vendorPurchaseItem[i].orderedQty = $scope.vendorPurchase.vendorPurchaseItem[i].packageQty - parseInt($scope.vendorPurchase.vendorPurchaseItem[i].freeQty) - parseInt($scope.vendorPurchaseItem.freeQty);
                            if (!$scope.vendorPurchase.vendorPurchaseItem[i].vendorReturn) {
                                $scope.vendorPurchase.vendorPurchaseItem[i].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[i].discount / 100);
                            }
                            
                            $scope.vendorPurchase.vendorPurchaseItem[i].total = ($scope.vendorPurchase.vendorPurchaseItem[i].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[i].packageQty) - $scope.vendorPurchase.vendorPurchaseItem[i].discountVal;
                            if ($scope.vendorPurchase.taxRefNo == 1) {
                                $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.gstTotal / 100);
                            }
                            else {
                                if ($scope.vendorPurchase.TaxationType == "VAT")
                                    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].productStock.vAT / 100);
                                if ($scope.vendorPurchase.TaxationType == "CST")
                                    $scope.vendorPurchase.vendorPurchaseItem[i].tax = ($scope.vendorPurchase.vendorPurchaseItem[i].total) * ($scope.vendorPurchase.vendorPurchaseItem[i].totalproductStock.cST / 100);
                            }
                            status = 1;
                        }
                    }
                }
                if ($scope.excessdiscount > 0) {
                    $scope.errorExcessPercent = true;
                } else {
                    $scope.errorExcessPercent = false;
                }
                if (status == 0) {
                    $scope.vendorPurchase.vendorPurchaseItem[$scope.copyedIndex] = $scope.vendorPurchaseItem;
                } else {
                    $scope.vendorPurchase.vendorPurchaseItem.splice($scope.Editindex, 1);
                    $scope.isPageLoad = false;
                    setTotal("NewStock");
                    setReturnAmt();
                    setVat();
                    $scope.initialValue();
                    getIsAllowDecimalSettings();
                    var qty = document.getElementById("drugName");
                    qty.focus();
                }
                $scope.EditMode = false;
                $scope.returnItemEdit = false;
            }            
            $scope.vendorPurchaseItem = { productStock: { product: {} } };
            $scope.selectedProduct = null;
            $scope.isPageLoad = false;
            setTotal("NewStock");
            setReturnAmt();
            setVat();
            $scope.ReCalCulateAllValues();
            (_isCST) ? $scope.vendorPurchaseItem.productStock.cst = 2 : $scope.vendorPurchaseItem.productStock.vAT = 5;
            if ($scope.vendorPurchase.selectedVendor.discount) {
                $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
            } else {
                $scope.vendorPurchaseItem.discount = 0;
            }
            $scope.initialValue();
            getIsAllowDecimalSettings();
            var qty = document.getElementById("drugName");
            qty.focus();
            $scope.enableAddnewStock();// Added by Poongodi on 15/03/2017

            if ($scope.previouspurchases != undefined) {
                if ($scope.previouspurchases.length > 0) {
                    $scope.toggleProductDetail();
                    $scope.previouspurchases = [];
                }
            }
            $scope.remainStock = 0;
            $scope.soldOrReturned = 0;
            $scope.vendorPurchaseItems.$setUntouched();
            $scope.vendorPurchaseItems.$setPristine();
        }
        $scope.Editindex = -1;        
    };
    $scope.removeStock = function (item, vendorId) {
        var cancel = window.confirm('Are you sure, Do you want to Delete Item?');
        if (cancel) {
            $scope.EnableSavebtn = true;
            var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
            $scope.vendorPurchase.vendorPurchaseItem[index].isDeleted = true;
            if ($scope.vendorPurchase.vendorPurchaseItem[index].action == "I") {
                $scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
            }
            var count = $filter("filter")($scope.vendorPurchase.vendorPurchaseItem, { "isDeleted": false }).length;
            if (count == 1) {
                var object = $filter("filter")($scope.vendorPurchase.vendorPurchaseItem, { "isDeleted": false });
                var index = $scope.vendorPurchase.vendorPurchaseItem.map(function (e) { return e.isDeleted; }).indexOf(false);
                $scope.vendorPurchase.vendorPurchaseItem[index].disableDelete = true;
            }
            //if ($scope.vendorPurchase.vendorPurchaseItem[index].isDeleted) {
            //    $scope.vendorPurchase.vendorPurchaseItem[index].action = "D";
            //}
            $scope.isPageLoad = false;
            setTotal("");
            setVat();
            $scope.ReCalCulateAllValues();
            if ($scope.vendorPurchase.vendorPurchaseItem.length == 0) {
                $scope.vendorPurchase.discountInRupees = "";
                $scope.vendorPurchase.discount = "";
                $scope.vendorPurchase.noteAmount = "";
                $scope.vendorPurchase.overAllDiscount = 0;
                $scope.vendorPurchase.tax = 0;
                $scope.vendorPurchase.total = 0;
                $scope.vendorPurchase.grossValue = 0;
            }

            var product = document.getElementById("drugName");
            product.focus();
            // checkNoteAmount();
        }
    };

    $scope.removeReturnStock = function (item, vendorId) {
        //var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
        //var returntotal = 0;
        //$scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
        //window.localStorage.setItem("p_data", JSON.stringify($scope.vendorPurchase));

        //setReturnAmt();
        //setVat();

        var cancel = window.confirm('Are you sure, Do you want to Delete Item?');
        if (cancel) {
            $scope.EnableSavebtn = true;
            var index = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
            $scope.vendorPurchase.vendorPurchaseItem[index].isDeleted = true;
            if ($scope.vendorPurchase.vendorPurchaseItem[index].action == "I")
            {
                $scope.vendorPurchase.vendorPurchaseItem.splice(index, 1);
            }
            var count = $filter("filter")($scope.vendorPurchase.vendorPurchaseItem, { "isDeleted": false }).length;
            if (count == 1) {
                var object = $filter("filter")($scope.vendorPurchase.vendorPurchaseItem, { "isDeleted": false });
                var index = $scope.vendorPurchase.vendorPurchaseItem.map(function (e) { return e.isDeleted; }).indexOf(false);
                $scope.vendorPurchase.vendorPurchaseItem[index].disableDelete = true;
            }

            var rcount = 0;
            $scope.purchaseReturnCount = 0;
            $scope.purchaseCount = 0;
            for (var j = 0; j < $scope.vendorPurchase.vendorPurchaseItem.length; j++) {
                if ($scope.vendorPurchase.vendorPurchaseItem[j].vendorReturn) {
                    rcount++;
                    $scope.purchaseReturnCount++;
                } else {
                    $scope.purchaseCount++;
                }
            }

            setReturnAmt();
            setVat();
            var product = document.getElementById("drugName");
            product.focus();

            //$scope.isPageLoad = false;
            //setTotal("");
            //setReturnAmt();
            //setVat();
            //$scope.ReCalCulateAllValues();
            //if ($scope.vendorPurchase.vendorPurchaseItem.length == 0) {
            //    $scope.vendorPurchase.discountInRupees = "";
            //    $scope.vendorPurchase.discount = "";
            //    $scope.vendorPurchase.noteAmount = "";
            //    $scope.vendorPurchase.overAllDiscount = 0;
            //    $scope.vendorPurchase.tax = 0;
            //    $scope.vendorPurchase.total = 0;
            //    $scope.vendorPurchase.grossValue = 0;
            //}
            // checkNoteAmount();
        }

    };

    function checkNoteAmount() {

        var total = Math.round($scope.vendorPurchase.total + $scope.vendorPurchase.tax);

        if (total < $scope.vendorPurchase.noteAmount) {

            $scope.errorNoteAmount = true;

            $scope.EnableSavebtn = true;

        } else {

            $scope.errorNoteAmount = false;

            $scope.EnableSavebtn = true;
        }
    }
    $scope.focusVendor = function (event) {
        if (event.which === 13) { // Enter key
            $("#vendor").trigger('chosen:open');
        }
    };
    $scope.focusInvoice = function (e, nextid, currentid) {
        var text = document.getElementById(currentid).value;
        //Enter
        if (e.keyCode == 13) {
            if (text != "") {
                var ele = document.getElementById(nextid);
                ele.focus();
            }
        }
        //BackSpace
        //if (e.keyCode == 8) {
        //    if (text == "") {
        //        $("#vendor").trigger('chosen:open');
        //        event.stopPropagation();
        //        return false;
        //    }
        //}
    };
    $scope.cancelUpdate = function () {
        if ($scope.Editindex != -1) {
            var invoicediscont = parseFloat($scope.vendorPurchase.discount) || 0;
            if ($scope.vendorPurchase.discount != "" || $scope.vendorPurchase.discount != null) {
                if (!$scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].isDeleted) {
                    if (!$scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].vendorReturn) {
                        $scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discount = parseFloat($scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discount) + invoicediscont;
                        $scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discountVal = ($scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].packagePurchasePrice * $scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].packageQty) * ($scope.vendorPurchase.vendorPurchaseItem[$scope.Editindex].discount / 100);
                    }
                }
            }
            $scope.Editindex = -1;
        }

        $scope.EditMode = false;
        $scope.returnItemEdit = false;
        $scope.vendorPurchaseItems.$setPristine();
        $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", true);
        $scope.vendorPurchaseItems.packageQuantity.$setValidity("InValidRoundQty", true);
        $scope.vendorPurchaseItems.freeQty.$setValidity("InValidRoundQty", true);
        $scope.vendorPurchaseItems.freeQty.$setValidity("InvalidSumQty", true);
        $scope.vendorPurchaseItem = { productStock: { product: {} } };
        $scope.selectedProduct = null;
        $scope.vendorPurchaseItem.productStock.vAT = vendorPurchaseItem.productStock.vAT;
        if ($scope.vendorPurchase.selectedVendor.discount) {
            $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;
        } else {
            $scope.vendorPurchaseItem.discount = 0;
        }
        $scope.remainStock = 0;
        $scope.soldOrReturned = 0;
        $scope.enableAddnewStock();
    };
    $scope.Editindex = -1;
    $scope.editPurchase = function (item, ind) {
        $scope.Enablepurchasebtn = false;
        var prod = document.getElementById("drugName");
        prod.disabled = true;
        if (item.vendorReturn != true) {
            $scope.returnItemEdit = false;
        }
        else {
            $scope.returnItemEdit = true;
        }
        if ($scope.EditMode) {
            return;
        }
        item.oldFreeQty = item.freeQty;
        if (item.vendorReturn == true)
        {
            item.discount = item.discount;
        }
        else
        {
            item.discount = item.orgDiscount;
        }
        item.packageQty = item.orderedQty;

        var remainStock = (item.productStock.stock / item.packageSize) || 0;
        $scope.remainStock = 0;
        $scope.soldOrReturned = 0;
        if (remainStock != item.packageQty1) {
            $scope.remainStock = Math.floor(remainStock);
            //$scope.remainStock = (item.packageQty + item.freeQty - $scope.remainStock);
            $scope.soldOrReturned = (parseFloat(item.packageQty1 || 0) - $scope.remainStock);
           // $scope.soldOrReturned = (item.packageQty1  - $scope.remainStock);
        }
        $scope.copyedIndex = $scope.vendorPurchase.vendorPurchaseItem.indexOf(item);
        $scope.vendorPurchaseItem = JSON.parse(JSON.stringify(item));
        $scope.selectedProduct = item.selectedProduct;
        $scope.EditMode = true;
        $scope.Editindex = ind;
        var cst = document.getElementById("cst");
        if ($scope.vendorPurchase.selectedVendor.enableCST) {
            cst.disabled = false;
        }
        $scope.previouspurchases = item.previouspurchases;
        $scope.calculateGSTValue(false, false);
        // Added Gavaskar 28-08-2017 FOCUSING IN PRODUCT ENTRY FIELD 

        if ($scope.returnItemEdit == true) {
            var ele = document.getElementById("packageQuantity");
            ele.focus();
        }
        else {
            var ele = document.getElementById("batchNo");
            ele.focus();
        }

    };
    $scope.doneEditPurchase = function (item, isValid) {
        if (!isValid) {
            item.isEdit = false;
            $scope.editItems = $scope.editItems - 1;
        }
        setTotal("");
        setVat();
    };
    $scope.SelectedFileForUpload = null;
    //File Select event 
    $scope.selectFileforUpload = function (file) {
        $scope.SelectedFileForUpload = file[0];
    };
    $scope.save = function () {
        $scope.disableAddnewStock();
        $scope.netHighlight = "";
        var checkNetTotal = 0;
        $scope.saveKey = false;
        if ($scope.vendorPurchase.initialValue) {
            checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
            if (checkNetTotal <= 1 && checkNetTotal >= -1) {
                $scope.netHighlight = "";
                purchaseUpdate();
            } else {
                var cancel = window.confirm('Are you sure, Do you want to Update Stock?');
                if (cancel) {
                    purchaseUpdate();
                } else {
                    $.LoadingOverlay("hide");
                    $scope.netHighlight = "highlight";
                    $scope.isProcessing = false;
                }
            }
        } else {
            purchaseUpdate();
        }
    };
    $scope.SaveProcessing = false;
    function purchaseUpdate() {
        $scope.vendorPurchase.updatePaymentTotal = $scope.vendorPurchase.grossValue;
        if ($scope.vendorPurchase.noteAmount == "" || $scope.vendorPurchase.noteAmount == 0 || $scope.vendorPurchase.noteAmount == null) {
            $scope.vendorPurchase.noteAmount = 0;
        }

        // Added by Gavaskar 06-09-2017 Start
        $scope.vendorPurchase.totalPurchaseValue = $scope.vendorPurchase.total;
        $scope.vendorPurchase.discountValue = $scope.vendorPurchase.discountInRupees;
        $scope.vendorPurchase.vatValue = $scope.vendorPurchase.tax;
        $scope.vendorPurchase.roundOffValue = $scope.roundedAmount;
        // $scope.vendorPurchase.netValue = $scope.vendorPurchase.grossValue;
        $scope.vendorPurchase.netValue = $scope.purchaseAmount - $scope.returnAmount;
        $scope.vendorPurchase.returnAmount = $scope.returnAmount;
        $scope.vendorPurchase.vendorPurchaseItem.ReturnedTotal = $scope.returnAmount;
        $scope.vendorPurchase.totalDiscountValue = $scope.vendorPurchase.overAllDiscount
        // Added by Gavaskar 06-09-2017 End

        if ($scope.SaveProcessing == false) {
            $.LoadingOverlay("show");
            $scope.isProcessing = true;
            $scope.SaveProcessing = true;
            vendorPurchaseService.update($scope.vendorPurchase)
                .then(function (response) {
                    $scope.SaveProcessing = false;
                    if (response.data.errorMessage == "") {
                        toastr.success('Stock updated successfully');
                        window.location.assign('/vendorPurchase/List');
                        $.LoadingOverlay("hide");
                    } else {
                        $.LoadingOverlay("hide");
                        toastr.error(response.data.errorMessage, 'Error');
                        $scope.isProcessing = false;
                    }
                }, function (error) {
                    $.LoadingOverlay("hide");
                    toastr.error(error.data.errorDesc, 'Error');
                    $scope.isProcessing = false;
                    $scope.SaveProcessing = false;
                });
        }
    }
    $scope.vendor = function () {
        vendorService.vendorDataList($scope.vendorStatus).then(function (response) {    //vendorService.vendorAllData().then(function (response) {  //vendorData()
            $scope.vendorList = response.data;
        }, function (error) {
            console.log(error);
            toastr.error('Error Occured', 'Error');
        });
    };
    $scope.vendor();
    $scope.getProducts = function (val) {
        return productService.nonHiddenProductList(val).then(function (response) {
            var flags = [], output = [], l = response.data.length, i;
            for (i = 0; i < l; i++) {
                if (flags[$filter('uppercase')(response.data[i].name)]) continue;
                flags[$filter('uppercase')(response.data[i].name)] = true;
                output.push(response.data[i]);
            }
            return output.map(function (item) {
                return item;
            });
        });
    };

    //added by nandhini for purchase edit product popup 21.6.17
    $scope.getProduct = function ($event) {
        if ($scope.scanBarcodeOption.scanBarcode === "1") {
            if ($scope.selectedProduct !== null && $scope.selectedProduct !== "" && ($scope.selectedProduct === undefined || typeof ($scope.selectedProduct) !== "object")) {
                var scannedEancode = $scope.selectedProduct;
                productStockService.getEancodeExistData(scannedEancode).then(function (eanRes) {
                    if (eanRes.data.length !== 0) {
                        productService.nonHiddenProductList(eanRes.data[0].id).then(function (response) {
                            if (response.data.length === 1) {
                                $scope.selectedProduct = response.data[0];
                                $scope.onProductSelect($event, $scope.selectedProduct);
                                var batchNo = document.getElementById("batchNo");
                                batchNo.focus();
                            }
                        });
                    } else {
                        $scope.isItScan = false;
                    }
                });
            } else if (typeof ($scope.selectedProduct) === "object" && $scope.selectedProduct !== null) {
                var batchNo = document.getElementById("batchNo");
                batchNo.focus();
            }
        }
        var isAddProduct = document.getElementById("addNewProduct").value;
        if (isAddProduct != "false") {
        if ($scope.selectedProduct.id == undefined && $scope.selectedProduct.length > 3) {
            $scope.AnyPopupOpened = true;
            $.confirm({
                title: "Add New Product Confirm",
                content: "This is a new product, do you want to add it ?",
                closeIcon: function () {
                    $scope.AnyPopupOpened = false;
                },
                buttons: {
                    yes: {
                        text: 'yes [y]',
                        btnClass: 'primary-button',
                        keys: ['y'],
                        action: function () {
                            $scope.AnyPopupOpened = false;
                            $scope.PopupAddNewProduct();
                        }
                    },
                    no: {
                        text: 'no [n]',
                        btnClass: 'secondary-button',
                        keys: ['n'],
                        action: function () {
                            $scope.AnyPopupOpened = false;
                        }
                    }
                }
            });
        }
    }

    };

    $scope.onProductSelect = function ($event, selectedProduct1) {
        if (selectedProduct1 != null) {
            $scope.vendorPurchaseItem.productStock.productId = selectedProduct1.id;
            $scope.vendorPurchaseItem.productStock.product = selectedProduct1;
            var productId = selectedProduct1.id;
            // Product Restriction by Gavaskar 14-09-2017 Start 
            for (var k = 0; k < $scope.vendorPurchase.vendorPurchaseItem.length; k++) {
                if (($scope.vendorPurchase.vendorPurchaseItem[k].productStock.product.id == selectedProduct1.id)) {  //Added by Sarubala on 27-09-17
                    if ($scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn != null && $scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn != undefined) {
                        if ($scope.vendorPurchase.vendorPurchaseItem[k].vendorReturn == true) {
                            toastr.info("Product already added in Return");
                            document.getElementById("drugName").focus();
                            return;
                        }
                    }                    
                }
            }
            // Product Restriction by Gavaskar 14-09-2017 End 
            vendorPurchaseService.isTempPurchaseItemAvail(productId)
          .then(function (resp) {
              bReturn = resp.data;
              if (bReturn) {
                  $scope.showTempStockScreen(productId);
              } else {
                  fillProductData(selectedProduct1.name);
              }
          }, function (response) {
              $scope.responses = response;
              console.log(error);
              toastr.error('Error Occured', 'Error');
          });
        }
    };
    //end
    $scope.onVendorSelect = function () {
        window.setTimeout(function () {
            var ele = document.getElementById("invoiceNo");
            ele.focus();
        }, 0);
    };
    $scope.modelOptions = {
        debounce: {
            default: 500,
            blur: 250
        },
        getterSetter: true
    };
    $scope.cancelUpdateStock = function () {
        var cancel = window.confirm('Are you sure, Do you want to Cancel?');
        if (cancel) {
            window.location.assign("/VendorPurchase/list");
        }
    };
    $scope.editCheque = function () {
        if ($scope.vendorPurchase.paymentType == "Credit") {
            $scope.vendorPurchase.chequeNo = null;
            $scope.vendorPurchase.chequeDate = null;
        } else {
            $scope.vendorPurchase.creditNoOfDays = null;
        }
    };
    $scope.currency = function (N) { N = parseFloat(N); if (!isNaN(N)) N = N.toFixed(2); else N = '0.00'; return N; };
    $scope.checkProduct = function () {
        if ($scope.selectedProduct.id == undefined && $scope.selectedProduct.length > 3) {
            $scope.AnyPopupOpened = true;
            $.confirm({
                title: "Add New Product Confirm",
                content: "This is a new product, do you want to add it ?",
                closeIcon: function () {
                    $scope.AnyPopupOpened = false;
                },
                buttons: {
                    yes: {
                        text: 'yes [y]',
                        btnClass: 'primary-button',
                        keys: ['y'],
                        action: function () {
                            $scope.AnyPopupOpened = false;
                            $scope.PopupAddNewProduct();
                        }
                    },
                    no: {
                        text: 'no [n]',
                        btnClass: 'secondary-button',
                        keys: ['n'],
                        action: function () {
                            $scope.AnyPopupOpened = false;
                        }
                    }
                }
            });
        } else {
            $scope.enablePurchaseBtn();
            fillProductData();
            var batchNo = document.getElementById("batchNo");
            batchNo.focus();
        }
    };

    function fillProductData(name) {
        $.LoadingOverlay("show");
        vendorPurchaseService.getPurchaseValues($scope.selectedProduct.id, $scope.selectedProduct.name)
            .then(function (response) {

                $scope.vendorPurchaseItem = response.data.vendorPurchaseItem;
                if (!$scope.enableSelling || $scope.vendorPurchaseItem.packageMRP == 0 || $scope.vendorPurchaseItem.packageMRP == null || $scope.vendorPurchaseItem.packageMRP == "" || $scope.vendorPurchaseItem.packageMRP == undefined)
                    $scope.vendorPurchaseItem.packageMRP = $scope.vendorPurchaseItem.packageSellingPrice;

                if (response.data.vendorPurchaseItem.productStock.vat == undefined) {
                    $scope.vendorPurchaseItem.productStock.vAT = 5;
                } else {
                    $scope.vendorPurchaseItem.productStock.vAT = response.data.vendorPurchaseItem.productStock.vat;
                }

                $scope.vendorPurchaseItem.packageQty = $scope.vendorPurchaseItem.packageQty - $scope.vendorPurchaseItem.freeQty;

                var d = new Date();
                var date2 = new Date($scope.vendorPurchaseItem.productStock.expireDate);
                if (date2 < (d.setMonth(d.getMonth() + 1))) {
                    $scope.dayDiff($scope.vendorPurchaseItem.productStock.expireDate);
                } else {
                    $scope.highlight = "";
                    $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", true);
                    $scope.isFormValid = true;
                }

                var cst = document.getElementById("cst");
                var vat = document.getElementById("vat");

                if ($scope.vendorPurchaseItem.productStock.product.rackNo == undefined || $scope.vendorPurchaseItem.productStock.product.rackNo == null) {
                    $scope.vendorPurchaseItem.productStock.product.rackNo = "";
                }

                $scope.vendorPurchaseItem.productStock.cST = 0;
                if ($scope.vendorPurchase.selectedVendor != undefined && $scope.vendorPurchase.selectedVendor.enableCST) {
                    vat.disabled = false;
                    cst.disabled = false;
                    $scope.vendorPurchase.TaxationType = "CST";
                    $scope.vendorPurchaseItem.productStock.cST = 2;
                } else {
                    vat.disabled = false;
                    cst.disabled = true;
                    $scope.vendorPurchase.TaxationType = "VAT";
                }
                if ($scope.vendorPurchase.selectedVendor.discount) {
                    $scope.vendorPurchaseItem.discount = $scope.vendorPurchase.selectedVendor.discount;

                    if ($scope.vendorPurchaseItem.discount == 0) {
                        $scope.vendorPurchaseItem.discount = "";
                    }
                } else {
                    $scope.vendorPurchaseItem.discount = "";
                }

                if ($scope.vendorPurchaseItem.freeQty) {
                    if ($scope.vendorPurchaseItem.freeQty == 0) {
                        $scope.vendorPurchaseItem.freeQty = "";
                    }
                } else {
                    $scope.vendorPurchaseItem.freeQty = "";
                }

                $scope.previouspurchases = response.data.getPreviousPurchase;

                if ($scope.previouspurchases.length > 0) {
                    $scope.toggleProductDetail();
                    $scope.lastPurchasePrice = $scope.previouspurchases[0].packagePurchasePrice;
                }

                $scope.calculateGSTValue(false, false);
                $.LoadingOverlay("hide");

            }, function (error) {
                $.LoadingOverlay("hide");
                toastr.error(error.data.errorDesc, 'Error');
            });
    }

    $scope.toggleProductDetail = function () {
        $('#chip-wrapper0').slideToggle();
        $('#chip-wrapper0').show();
        if ($('#chip-btn0').text() === '+') {
            $('#chip-btn0').text('-');
        } else {
            $('#chip-btn0').text('+');
        }
    };

    $scope.keyEnter = function (event, nextid, currentid, value) {
        var ele = document.getElementById(nextid);
        if (event.which === 13) {
            if (currentid == "expDate") {
                if ($scope.vendorPurchaseItem.productStock.expireDate == undefined || $scope.vendorPurchaseItem.productStock.expireDate == null || $scope.vendorPurchaseItems.expDate.$error.InValidexpDate == true) {
                    //if (value == 0 || value == null || value < 0) {
                    ele = document.getElementById(currentid);
                    ele.focus();
                    return false;
                } else {
                    ele = document.getElementById(nextid);
                    ele.focus();
                }
            }
            if (currentid == "packageSize" || currentid == "packageQuantity") {
                if (value == 0 || value == null) {
                    ele = document.getElementById(currentid);
                } else {
                    ele = document.getElementById(nextid);
                }
                ele.focus();
            }
            if (currentid == "purchasePrice") {
                if (value == 0 || value == null || value < 0) {
                    ele = document.getElementById(currentid);
                } else {
                    ele = document.getElementById(nextid);
                }
                ele.focus();
            }
            if (currentid == "sellingPrice") {
                if (value == 0 || value == null || value < 0 || $scope.vendorPurchaseItems.sellingPrice.$error.checkMrpError != undefined) {
                    ele = document.getElementById(currentid);
                } else {
                    ele = document.getElementById(nextid);
                }
                ele.focus();
            }
            if (currentid == "vat") {
                if (value == null || value < 0) {
                    ele = document.getElementById(currentid);
                } else {
                    ele = document.getElementById(nextid);
                }
                ele.focus();
            }
            if ((event.currentTarget.required && event.currentTarget.value.length > 0) || !event.currentTarget.required) {
                ele.focus();
                if (ele.nodeName != "BUTTON") {
                    ele.select();
                }
            }
        }
    };
    $scope.getScanBarcodeOption = function () {
        vendorPurchaseService.getScanBarcodeOption().then(function (resp) {
            $scope.scanBarcodeOption = resp.data;
        }, function (error) {
            console.log(error);
        });
    };
    $scope.getScanBarcodeOption();
    $scope.saveDraft = function () {
        window.localStorage.setItem("p_data", JSON.stringify($scope.vendorPurchase));
        toastr.success('Daft saved successfully');
    };
    $scope.loadDraft = function () {
        $scope.vendorPurchase = JSON.parse(window.localStorage.getItem("p_data"));
        $scope.updateUI("");
    };


    //  $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", false);


    $scope.PopupAddNewProduct = function () {
        $scope.AnyPopupOpened = true;
        var m = ModalService.showModal({
            "controller": "productCreateCtrl",
            "templateUrl": "createProduct",
            "inputs": {
                "mode": "Create",
                "poproductname": $scope.selectedProduct,
                "GSTEnabled":$scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                if (result === "No") {
                    var drugName = document.getElementById("drugName");
                    drugName.focus();
                }
                $scope.AnyPopupOpened = false;
            });
        });
    };

    // TempStock Implemented by Gavaskar 09-08-2017 Start 

    
    var d = new Date();
    $scope.tempStockLoaded = false;
    $scope.showTempStockScreen = function (productId) {
        $scope.AnyPopupOpened = true;
        var m = ModalService.showModal({
            "controller": "tempPurchaseItemCtrl",
            "templateUrl": "tempPurchaseItem",
            "inputs": {
                "productId": productId,
                "GSTEnabled": $scope.GSTEnabled
            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.AnyPopupOpened = false;
                if (result.status == "Yes") {
                    $scope.tempStockLoaded = true;
                    tempItemSelection(result.data);
                }
            });
        });
    };
    function tempItemSelection(tempStockValues) {
        fillTempData(tempStockValues);
    }

    function fillTempData(tempPurchaseItem) {
        vendorPurchaseService.getPurchaseValues($scope.vendorPurchaseItem.productStock.product.id, $scope.vendorPurchaseItem.productStock.product.name)
            .then(function (response) {

                //Assigning TempVendorPurchaseItem to VendorPurchaseItem object of this view
                $scope.vendorPurchaseItem = response.data.vendorPurchaseItem;
                $scope.vendorPurchaseItem.productStock.batchNo = tempPurchaseItem.productStock.batchNo;
                $scope.vendorPurchaseItem.productStock.expireDate = tempPurchaseItem.productStock.expireDate;
                //if (tempPurchaseItem.productStock.taxRefNo == 1)
                //{
                //    $scope.vendorPurchaseItem.productStock.vAT = 0;
                //}
                //else
                //{
                //    $scope.vendorPurchaseItem.productStock.vAT = tempPurchaseItem.productStock.vAT;
                //}
                $scope.vendorPurchaseItem.productStock.vAT = tempPurchaseItem.productStock.vAT
                $scope.vendorPurchaseItem.productStock.vAT = 0;
                $scope.vendorPurchaseItem.productStock.hsnCode = tempPurchaseItem.productStock.hsnCode

                //GST related fields start here
                $scope.vendorPurchaseItem.productStock.igst = tempPurchaseItem.productStock.igst;
                $scope.vendorPurchaseItem.productStock.cgst = tempPurchaseItem.productStock.cgst;
                $scope.vendorPurchaseItem.productStock.sgst = tempPurchaseItem.productStock.sgst;
                //$scope.vendorPurchaseItem.productStock.gstTotal = tempPurchaseItem.productStock.gstTotal;
                //GST related fields end here

                if ($scope.vendorPurchase.selectedVendor.locationType == 1) {
                    $scope.vendorPurchaseItem.productStock.igst = "";
                } else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
                    $scope.vendorPurchaseItem.productStock.cgst = "";
                    $scope.vendorPurchaseItem.productStock.sgst = "";
                } else {
                    $scope.vendorPurchaseItem.productStock.igst = "";
                    $scope.vendorPurchaseItem.productStock.cgst = "";
                    $scope.vendorPurchaseItem.productStock.sgst = "";
                }

                igst = parseFloat($scope.vendorPurchaseItem.productStock.igst) || 0;
                cgst = parseFloat($scope.vendorPurchaseItem.productStock.cgst) || 0;
                sgst = parseFloat($scope.vendorPurchaseItem.productStock.sgst) || 0;
                $scope.vendorPurchaseItem.productStock.gstTotal = igst + cgst + sgst;
                $scope.vendorPurchaseItem.packageSize = tempPurchaseItem.packageSize;
                // Modified by Gavaskar Tempstock sales after 23-08-2017 Start
                //if (tempPurchaseItem.productStock.stock == null || tempPurchaseItem.productStock.stock == 0 || tempPurchaseItem.productStock.stock == undefined) {
                    $scope.vendorPurchaseItem.packageQty = tempPurchaseItem.packageQty;
                //}
                //else
                //{
                //    $scope.vendorPurchaseItem.packageQty = tempPurchaseItem.productStock.stock / tempPurchaseItem.packageQty;
                //}
                // Modified by Gavaskar Tempstock sales after 23-08-2017 End
                $scope.vendorPurchaseItem.packagePurchasePrice = tempPurchaseItem.packagePurchasePrice;
                $scope.vendorPurchaseItem.productStock.product.rackNo = tempPurchaseItem.productStock.product.rackNo; 
                $scope.vendorPurchaseItem.packageSellingPrice = tempPurchaseItem.packageSellingPrice;
                $scope.vendorPurchaseItem.packageMRP = tempPurchaseItem.packageMRP;
                $scope.vendorPurchaseItem.fromTempId = tempPurchaseItem.fromTempId;
                $scope.vendorPurchaseItem.tempItem = tempPurchaseItem.tempItem;
                $scope.vendorPurchaseItem.freeQty = "";
                var date2 = new Date($scope.vendorPurchaseItem.productStock.expireDate);
                if (date2 < (d.setMonth(d.getMonth() + 1))) {
                    $scope.dayDiff($scope.vendorPurchaseItem.productStock.expireDate);
                } else {
                    $scope.highlight = "";
                    $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", true);
                    $scope.isFormValid = true;
                }
                if (!$scope.GSTEnabled) {
                    var cst = document.getElementById("cST");
                    var vat = document.getElementById("vat");
                    $scope.vendorPurchaseItem.productStock.cST = 0;
                    cst.disabled = true;
                    $scope.vendorPurchase.TaxationType = "VAT";
                }
                $scope.vendorPurchaseItem.discount = "";
                document.getElementById("packageQuantity").focus();
                //$scope.HideDraftEditLabel = true;
            }, function (error) {
                console.log(error);
            });
    }

    // TempStock Implemented by Gavaskar 09-08-2017 End 


    $scope.PopupProductReturn = function () {
        $scope.returnPopup = true;
        var m = ModalService.showModal({
            "controller": "purchaseProductReturnCtrl",
            "templateUrl": "productReturn",
            "inputs": {
                "vendorPurchaseItem": $scope.vendorPurchase.vendorPurchaseItem,
                "selectedVendor": $scope.vendorPurchase.selectedVendor,
                "GSTEnabled": $scope.GSTEnabled

            }
        }).then(function (modal) {
            modal.element.modal();
            modal.close.then(function (result) {
                $scope.returnItems = result;
                $scope.returnPopup = false;
                if ($scope.returnItems != null) {
                    $scope.addReturnItems();
                }
                document.getElementById("initialValue").focus();
            });
        });
    };

    $scope.addReturnItems = function () {
        if ($scope.returnItems.length > 0) {
            for (var x = 0; x < $scope.returnItems.length; x++) {
                if ($scope.returnItems[x].editId == null || $scope.returnItems[x].editId == undefined) {
                    $scope.returnItems[x].editId = $scope.editId++;
                    $scope.vendorPurchase.vendorPurchaseItem.push($scope.returnItems[x]);

                }
            }

            setReturnAmt();
            setVat();
            $scope.EnableSavebtn = true; // Update button Enable by Gavaskar 14-09-2017
        }
        else {
            //  $scope.returnDiscountInValid = false;
        }
        $scope.returnItems = null;
    };

    $scope.dayDiff = function (expireDate) {
        $scope.validatepurchase(2);
        var val = (toDate($scope.vendorPurchaseItems.expDate.$viewValue) > $scope.minDate);
        $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", val);
        $scope.isFormValid = val;
        $scope.today = $filter('date')(new Date(), 'dd/MM/yyyy');
        $scope.expireDate = $filter('date')(expireDate, 'dd/MM/yyyy');
        var date2 = new Date($scope.formatString($scope.expireDate));
        var date1 = new Date($scope.formatString($scope.today));
        var timeDiff = date2.getTime() - date1.getTime();
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if ($scope.dayDifference < 30) {
            $scope.highlight = "highlight";
            $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", false);
            $scope.isFormValid = false;
        } else {
            $scope.vendorPurchaseItems.expDate.$setValidity("InValidexpDate", true);
            $scope.isFormValid = true;
            $scope.highlight = "";
        }
    };
    $scope.formatString = function (format) {
        var day = parseInt(format.substring(0, 2));
        var month = parseInt(format.substring(3, 5));
        var year = parseInt(format.substring(6, 10));
        var date = new Date(year, month - 1, day);
        return date;
    };
    $scope.initialValue = function () {
        if ($scope.vendorPurchase.initialValue == "")
            $scope.vendorPurchase.initialValue = null;
        var checkNetTotal = $scope.currency($scope.vendorPurchase.initialValue) - $scope.currency($scope.vendorPurchase.total + $scope.vendorPurchase.tax);
        if (checkNetTotal <= 1 && checkNetTotal >= -1) {
            $scope.netHighlight = "";
        } else {
            if ($scope.vendorPurchase.total + $scope.vendorPurchase.tax > 0 && $scope.vendorPurchase.initialValue != null)
                $scope.netHighlight = "highlight";
            else
                $scope.netHighlight = "";
        }
    };
    //$scope.discountclick = function (val) {
    //    $scope.discountType = val;
    //    if (val == "rupees") {
    //        $scope.percentDiscount = true;
    //    }
    //    else {
    //        $scope.percentDiscount = false;
    //    }
    //};
    $scope.focusNextId = function (nextid) {
        document.getElementById(nextid).focus();
    };
    /* Commented by settu
    $scope.focusNoteType = function () {
        var ele = document.getElementById("noteType");
        ele.focus();
    };
    $scope.focusNoteTextBox = function () {
        var ele = document.getElementById("txtNoteAmount");
        ele.focus();
    };*/
    $scope.discountTypePer = function () {
        //$scope.discountType = val;
        //if (val == "percent") {
        //    $scope.percentDiscount = false;
        //    document.getElementById("txtDiscountPercent").focus();
        //}

        angular.element(document.getElementById("txtDiscountPercent")).focus();
    };
    $scope.discountTypeRs = function () {
        //$scope.discountType = val;
        //if (val == "rupees") {
        //    $scope.percentDiscount = true;
        //    document.getElementById("txtDiscountRupees").focus();
        //}

        angular.element(document.getElementById("txtDiscountRupees")).focus();
    };
    $scope.getCompletePurchaseSettings = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getCompletePurchaseSetting().then(function (response) {
            if (response.data.toString() == "1")
                $scope.completePurchaseKeyType = "ESC";
            else if (response.data.toString() == "2")
                $scope.completePurchaseKeyType = "END";
            else
                $scope.completePurchaseKeyType = "ESC";
            $.LoadingOverlay("hide");
        }, function (error) {
            console.log(error);
            $.LoadingOverlay("hide");
        });
    };
    $scope.getCompletePurchaseSettings();
    $scope.changeType = function () {

        var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax - $scope.returnAmount; // Added by Gavaskar 18-09-2017 Return Amount validation
        $scope.vendorPurchase.noteAmount = "";
        roundingTotal(total);


        //var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax;
        //roundingTotal(total);
        //$scope.EnableSavebtn = true;
        //$scope.vendorPurchase.noteAmount = "";
    };
    function roundingTotal(Total) {
        var noteamount = parseFloat($scope.vendorPurchase.noteAmount) || 0;
        if ($scope.vendorPurchase.noteType == "credit") {
            Total -= noteamount;
            $scope.purchaseAmount = parseFloat($scope.vendorPurchase.total) + parseFloat($scope.vendorPurchase.tax) - noteamount;
            $scope.purchaseAmount = Math.round($scope.purchaseAmount);
        }
        if ($scope.vendorPurchase.noteType == "debit") {
            Total += noteamount;
            $scope.purchaseAmount = parseFloat($scope.vendorPurchase.total) + parseFloat($scope.vendorPurchase.tax) + noteamount;
            $scope.purchaseAmount = Math.round($scope.purchaseAmount);
        }
        var afterRoundUp = Math.round(Total);
        $scope.vendorPurchase.grossValue = afterRoundUp;
        $scope.roundedAmount = afterRoundUp - Total;
        if ($scope.returnAmount == 0)
        {
            if (Math.round(Total) <= 0) {
                $scope.errorNoteAmount = true;

                if (!$scope.isPageLoad) {
                    $scope.EnableSavebtn = false;
                }



            } else {
                $scope.errorNoteAmount = false;

                if (!$scope.isPageLoad) {
                    $scope.EnableSavebtn = true;
                }

            }
        }

    }

    $scope.changeGrossAmount = function (noteAmount) {
        $scope.isPageLoad = false;
        var total = $scope.vendorPurchase.total + $scope.vendorPurchase.tax - $scope.returnAmount;
        //setReturnAmt();
        roundingTotal(total);
        $scope.EnableSavebtn = true; // Update button Enable by Gavaskar 18-09-2017
    };
    $scope.validateInvoiceDate = function () {
        var val = (toDate($scope.vendorPurchaseItems.invoiceDate.$viewValue) < $scope.minDate);
        $scope.vendorPurchaseItems.invoiceDate.$setValidity("InValidInvoiceDate", val);
        $scope.isFormValid = val;
        $scope.validatepurchase(12);
    };
    $scope.validatepurchase = function (value) {
        if (value == 1) { // batch
            $scope.enablePurchaseBtn();
        }
        if (value == 2) { //Expirydate
            $scope.enablePurchaseBtn();
        }
        if (value == 3) { //Rackno
            $scope.enablePurchaseBtn();
        }
        if (value == 4) { //Vat
            $scope.enablePurchaseBtn();
        }
        if (value == 5) { // Unit/strip
            $scope.enablePurchaseBtn();
        }
        if (value == 6) { // No.of.Strip 
            $scope.enablePurchaseBtn();
        }
        if (value == 7) { //Price/strip
            $scope.enablePurchaseBtn();
        }
        if (value == 8) { //MRP/Strip
            $scope.enablePurchaseBtn();
        }
        if (value == 9) { //Free Strip
            $scope.enablePurchaseBtn();
        }
        if (value == 10) { //Discount
            $scope.enablePurchaseBtn();
        }
        if (value == 11) { //Invoice no
            $scope.enablePurchaseBtn();
            $scope.EnableSavebtn = true;
        }
        if (value == 12) { //Invoice Date
            $scope.enablePurchaseBtn();
            $scope.EnableSavebtn = true;
        }
        if (value == 13) { //comments
            $scope.EnableSavebtn = true;
        }
        if (value == 14) { //creditNoOfDays
            $scope.EnableSavebtn = true;
        }
        //if (value == 15) { //Vendor Name
        //    $scope.enablePurchaseBtn();
        //    $scope.EnableSavebtn = true;
        //}
        if (value == 16) { //txtDiscountPercent
            $scope.EnableSavebtn = true;
        }
        if (value == 17) { //txtDiscountRupees
            $scope.EnableSavebtn = true;
        }
        if (value == 18) { //igst
            $scope.enablePurchaseBtn();
        }
        if (value == 19) { //cgst
            $scope.enablePurchaseBtn();
        }
        if (value == 20) { //sgst
            $scope.enablePurchaseBtn();
        }
        if (value == 21) { //gstTotal
            $scope.enablePurchaseBtn();
        }
        if (value == 22) { //Barcode
                $scope.enablePurchaseBtn();
        }
    };
    $scope.enablePurchaseBtn = function () {
        $scope.Enablepurchasebtn = true;
    };
    $scope.checkMrp = function (purchase, sell, vat) {
        $scope.validatepurchase(4);
        $scope.validatepurchase(7);
        $scope.validatepurchase(8);
        purchase = purchase != undefined ? parseFloat(purchase) : 0;
        sell = sell != undefined ? parseFloat(sell) : 0;
        vat = vat != undefined ? parseFloat(vat) : 0;
        if ((sell == 0 && purchase == 0) || (purchase == null && sell==null))
        {
            $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", true);
        }
        else {
            if (sell <= purchase + (purchase * vat / 100)) {
                $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", false);
            } else {
                $scope.vendorPurchaseItems.sellingPrice.$setValidity("checkMrpError", true);
            }
        }
       
        if (!$scope.enableSelling) {
            $scope.vendorPurchaseItem.packageMRP = $scope.vendorPurchaseItem.packageSellingPrice;
        } else {
            if ($scope.vendorPurchaseItem.packageMRP > 0) {
                $scope.checkSellingMrp($scope.vendorPurchaseItem.packageSellingPrice, $scope.vendorPurchaseItem.packageMRP);
            }
        }
    };
    $scope.getEnableSelling = function () {
        $.LoadingOverlay("show");
        vendorPurchaseService.getEnableSelling().then(function (response) {
            $.LoadingOverlay("hide");
            $scope.enableSelling = response.data;
        }, function (error) {
            $.LoadingOverlay("hide");
            console.log(error);
        });
    };
    $scope.getEnableSelling();
    $scope.checkSellingMrp = function (selling, mrp) {
        $scope.validatepurchase(9);
        if ($scope.enableSelling != true)
            return;
        selling = selling != undefined ? parseFloat(selling) : 0;
        mrp = mrp != undefined ? parseFloat(mrp) : 0;
        if (mrp < selling || (mrp == 0)) {
            $scope.vendorPurchaseItems.mrp.$setValidity("checkMrpError", false);
        } else {
            $scope.vendorPurchaseItems.mrp.$setValidity("checkMrpError", true);
        }
    };

    $scope.calculateGSTValue = function (GSTFromUI, addStock) {

        if ($scope.vendorPurchase.taxRefNo == 1) {            
            $scope.vendorPurchaseItem.hsnCode = $scope.vendorPurchaseItem.productStock.hsnCode;
            $scope.vendorPurchaseItem.productStock.vAT = 0;
            $scope.vendorPurchaseItem.productStock.vat = 0;
            $scope.vendorPurchaseItem.productStock.cst = 0;
            $scope.vendorPurchaseItem.vAT = 0;
            $scope.vendorPurchaseItem.gstTotal = $scope.vendorPurchaseItem.productStock.gstTotal;
            if (addStock) {
                if ($scope.vendorPurchaseItem.productStock.gstTotal == null || $scope.vendorPurchaseItem.productStock.gstTotal == undefined || $scope.vendorPurchaseItem.productStock.gstTotal == "")
                    $scope.vendorPurchaseItem.productStock.gstTotal = 0;
                $scope.vendorPurchaseItem.productStock.igst = $scope.vendorPurchaseItem.productStock.gstTotal;
                $scope.vendorPurchaseItem.productStock.sgst = parseFloat(($scope.vendorPurchaseItem.productStock.gstTotal / 2).toFixed(2));
                $scope.vendorPurchaseItem.productStock.cgst = parseFloat(($scope.vendorPurchaseItem.productStock.gstTotal / 2).toFixed(2));
                if ($scope.vendorPurchase.selectedVendor.locationType == 1) {
                    $scope.vendorPurchaseItem.igst = 0;
                    $scope.vendorPurchaseItem.sgst = $scope.vendorPurchaseItem.productStock.sgst;
                    $scope.vendorPurchaseItem.cgst = $scope.vendorPurchaseItem.productStock.cgst;
                } else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
                    $scope.vendorPurchaseItem.igst = $scope.vendorPurchaseItem.productStock.igst;
                    $scope.vendorPurchaseItem.sgst = 0;
                    $scope.vendorPurchaseItem.cgst = 0;
                }
            } else {
                $scope.vendorPurchaseItem.igst = $scope.vendorPurchaseItem.productStock.gstTotal;
                $scope.vendorPurchaseItem.sgst = parseFloat(($scope.vendorPurchaseItem.productStock.gstTotal / 2).toFixed(2));
                $scope.vendorPurchaseItem.cgst = parseFloat(($scope.vendorPurchaseItem.productStock.gstTotal / 2).toFixed(2));
                if ($scope.vendorPurchase.selectedVendor.locationType == 1) {
                    $scope.vendorPurchaseItem.productStock.igst = 0;
                    $scope.vendorPurchaseItem.productStock.sgst = $scope.vendorPurchaseItem.sgst;
                    $scope.vendorPurchaseItem.productStock.cgst = $scope.vendorPurchaseItem.cgst;
                } else if ($scope.vendorPurchase.selectedVendor.locationType == 2) {
                    $scope.vendorPurchaseItem.productStock.igst = $scope.vendorPurchaseItem.igst;
                    $scope.vendorPurchaseItem.productStock.sgst = 0;
                    $scope.vendorPurchaseItem.productStock.cgst = 0;
                }
            }
        }

        if (GSTFromUI)
            $scope.checkMrp($scope.vendorPurchaseItem.packagePurchasePrice, $scope.vendorPurchaseItem.packageSellingPrice, $scope.vendorPurchaseItem.productStock.gstTotal)
    }

    $("#expDate").keyup(function (e) {
        if ($(this).val().length == 2) {
            if (!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
                $(this).val($(this).val() + "/");
        }
    });
    $scope.checkAllPurchasefields = function (ind) {
        if (ind > 1) {
            if ($scope.vendorPurchase.selectedVendor == undefined || Object.keys($scope.vendorPurchase.selectedVendor).length == 0) {
                $("#vendor").trigger('chosen:open');
                event.stopPropagation();
                return false;
            }
        }
        if (ind > 2) {
            if ($scope.vendorPurchase.invoiceNo == undefined || $scope.vendorPurchase.invoiceNo == null) {
                var ele = document.getElementById("invoiceNo");
                ele.focus();
                return false;
            }
        }
        if (ind > 3) {
            if ($scope.vendorPurchase.invoiceDate == undefined || $scope.vendorPurchase.invoiceDate == null) {
                var ele = document.getElementById("invoiceDate");
                ele.focus();
                return false;
            }
        }
        if (ind > 4) {
            if ($scope.selectedProduct == undefined || $scope.selectedProduct == null) {
                var ele = document.getElementById("drugName");
                ele.focus();
                return false;
            }
        }
        if (ind > 5) {
            if ($scope.vendorPurchaseItem.productStock.batchNo == undefined || $scope.vendorPurchaseItem.productStock.batchNo == null || $scope.vendorPurchaseItem.productStock.batchNo.length == 0) {
                var ele = document.getElementById("batchNo");
                ele.focus();
                return false;
            }
        }
        if (ind > 6) {
            if ($scope.vendorPurchaseItem.productStock.expireDate == undefined || $scope.vendorPurchaseItem.productStock.expireDate == null || $scope.vendorPurchaseItems.invoiceDate.$error.InValidInvoiceDate == true) {
                var ele = document.getElementById("expDate");
                ele.focus();
                return false;
            }
        }
        if (ind > 7) {
            if ($scope.vendorPurchaseItem.productStock.vAT == undefined || $scope.vendorPurchaseItem.productStock.vAT == null) {
                var ele = document.getElementById("vat");
                ele.focus();
                return false;
            }
        }
        if (ind > 8) {
            if ($scope.vendorPurchaseItem.packageSize == undefined || $scope.vendorPurchaseItem.packageSize == null || $scope.vendorPurchaseItem.packageSize == 0) {
                var ele = document.getElementById("packageSize");
                ele.focus();
                return false;
            }
        }
        if (ind > 9) {
            if ($scope.vendorPurchaseItem.packageQty == undefined || $scope.vendorPurchaseItem.packageQty == null || $scope.vendorPurchaseItem.packageQty == 0) {
                var ele = document.getElementById("packageQuantity");
                ele.focus();
                return false;
            }
        }
        if (ind > 10) {
            if ($scope.vendorPurchaseItem.packagePurchasePrice == undefined || $scope.vendorPurchaseItem.packagePurchasePrice == null || $scope.vendorPurchaseItem.packagePurchasePrice == 0) {
                var ele = document.getElementById("purchasePrice");
                ele.focus();
                return false;
            }
        }
        if (ind > 11) {
            if ($scope.vendorPurchaseItem.packageSellingPrice == undefined || $scope.vendorPurchaseItem.packageSellingPrice == null || $scope.vendorPurchaseItem.packageSellingPrice == 0) {
                var ele = document.getElementById("sellingPrice");
                ele.focus();
                return false;
            }
        }
        if (ind > 12) {
            if ($scope.vendorPurchaseItem.packageMRP == undefined || $scope.vendorPurchaseItem.packageMRP == null || $scope.vendorPurchaseItem.packageMRP == 0) {
                var ele = document.getElementById("mrp");
                ele.focus();
                return false;
            }
        }
    };

    $scope.checkInvoiceNo = function (val) {
        if (val == "" || val == null)
            return;
        if ($scope.vendorPurchase.selectedVendor != null && $scope.vendorPurchase.selectedVendor != "")
            $scope.vendorPurchase.vendorId = $scope.vendorPurchase.selectedVendor.id;
        $scope.isExist = true;        
        var vendorPurchase = $scope.vendorPurchase;
        $.LoadingOverlay("show");
        if ($scope.isExist) {
            vendorPurchaseService.validateInvoice($scope.vendorPurchase)
                .then(function (response) {
                    $.LoadingOverlay("hide");  //toastr.error('Error Occured', 'Error');
                }, function (error) {
                    $.LoadingOverlay("hide");
                    var ele = document.getElementById("invoiceNo");
                    ele.focus();
                    toastr.error(error.data.value.errorDesc, 'Error');
                    $scope.isExist = false;
                });
        }
    }

    /*  Commented by settu
    $scope.focusNoteTextBox = function () {
        var ele = document.getElementById("txtNoteAmount");
        ele.focus();
    };
    $scope.focusSaveButton = function () {
        var ele = document.getElementById("btnUpdateStock");
        ele.focus();
    };*/
});
app.directive('numbersOnly', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    var val = '';
                }
                var clean = val.replace(/[^0-9]+/g, '');
                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });
            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
app.directive('focusBox', function () {
    return {
        "link": function (scope, element, attrs) {
            element.bind("keydown", function (event) {
                var valueLength = attrs.$$element[0].value.length;
                var value = parseFloat(attrs.$$element[0].value);
                //Enter 
                if (event.which === 13) {
                    if (attrs.id === "freeQty" || attrs.id === "discount" || attrs.id === "Rackno" || attrs.id === "boxNo") {
                        if (scope.vendorPurchase.taxRefNo == 1 && attrs.id === "boxNo") {
                            ele = document.getElementById("packageSize");
                            ele.focus();
                        }
                        else {
                            ele = document.getElementById(attrs.nextid);
                            ele.focus();
                        }
                    } else {
                        if (valueLength !== 0) {
                            if ((angular.isString(scope.selectedProduct)) && (attrs.nextid == 'batchNo')) {
                                ele = document.getElementById("drugName");
                                ele.focus();
                            } else {
                                if (attrs.id !== "vat") {
                                    if (value != 0) {
                                        if (document.getElementById(attrs.nextid).disabled == false) {
                                            ele = document.getElementById(attrs.nextid);
                                            ele.focus();
                                        } else {
                                            if (document.getElementById(attrs.nextid).disabled == true && attrs.nextid == "drugName") {
                                                ele = document.getElementById("batchNo");
                                                ele.focus();
                                            }
                                        }
                                    }
                                } else {
                                    if (valueLength !== 0) {
                                        ele = document.getElementById(attrs.nextid);
                                        ele.focus();
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    };
});