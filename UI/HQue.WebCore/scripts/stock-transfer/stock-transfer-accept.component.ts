﻿import { Component, OnInit , Input } from "@angular/core";
import { Http, HTTP_PROVIDERS  } from "@angular/http";
import { StockTransferService  } from "./stock-transfer.service";
import { StockTransfer } from "../model/stockTransfer";
import { ProductStock } from "../model/productStock";
import { Instance } from "../model/instance";

@Component({
    selector: 'stock-transfer-accept',
    templateUrl: '/StockTransfer/AcceptList',
    providers: [StockTransferService, HTTP_PROVIDERS],
})

export class StockTransferAccept implements OnInit {

    public forAccept = true;

    public searchModel = new StockTransfer(); 
    public stockTransferList: StockTransfer[];
    public filterBranchList: Instance[];

    constructor(private stockTransferService: StockTransferService) {
        this.stockTransferList = [];
    }  

    ngOnInit() {
        this.loadData();
        this.getFilterInstance();

    }  

    private loadData() {
        this.stockTransferService.getStockTransfer(this.searchModel, this.forAccept)
            .subscribe(stockTransfers => { this.stockTransferList = stockTransfers; });
    }

    public search() {
        this.loadData();
    }

    accept(transferId: string): void {
        this.stockTransferService.accetpReject(transferId, 3)
            .subscribe(accept => { alert('Accept successful...!'); this.loadData(); });
    }

    reject(transferId: string): void {
        if (confirm("Sure to reject?"))
        this.stockTransferService.accetpReject(transferId, 2)
            .subscribe(accept => { alert('Reject successful...!'); this.loadData(); });
    }

    getProductStock(obj: any): ProductStock {
        return new ProductStock(obj.productStock.purchasePrice, obj.productStock.vat, obj.quantity)
    }

    getStockTransferNetValue(obj: any): number {
        var netValue = 0;
        obj.stockTransferItems.forEach((value) => {
            value.productStockObj = this.getProductStock(value);
            netValue += value.productStockObj.netValue()
        })
        return netValue;
    }

    getFilterInstance() {
        this.stockTransferService.getFilterInstance(this.forAccept)
            .subscribe(filterBranchList => { this.filterBranchList = filterBranchList; });
    }
}

