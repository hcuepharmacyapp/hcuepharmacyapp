﻿import { Component , Input} from "@angular/core";
import { ProductStock } from  "../model/productStock";

@Component({
    selector: 'product-list-item',
    templateUrl: '/StockTransfer/StockTransferProductItem',    
})
export class ProductListItemComponent {
    @Input() addedTransfer: ProductStock[];

    public delete(id: string) {

        if (!confirm('Sure to delete ?')) {
            return;
        }

        var index = this.addedTransfer.findIndex(x => x.id == id);
        this.addedTransfer.splice(index, 1);
    }
}
