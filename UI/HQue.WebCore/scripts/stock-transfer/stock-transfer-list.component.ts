﻿import { Component, OnInit, Input } from "@angular/core";
import { Http, HTTP_PROVIDERS  } from "@angular/http";
import { StockTransferAccept  } from "./stock-transfer-accept.component";
import { StockTransferService  } from "./stock-transfer.service";
import { StockTransfer } from "../model/stockTransfer";
import { ProductStock } from "../model/productStock";

@Component({
    selector: 'stock-transfer-list',
    templateUrl: '/StockTransfer/AcceptList',
    providers: [StockTransferService, HTTP_PROVIDERS],
})

export class StockTransferList extends StockTransferAccept implements OnInit {

    constructor(stockTransferService: StockTransferService) {
        super(stockTransferService);
        this.forAccept = false;
    }  
}

