﻿import { Component, EventEmitter, Input } from "@angular/core";
import {CORE_DIRECTIVES, FormBuilder, ControlGroup, Control} from '@angular/common';
import { Http, HTTP_PROVIDERS  } from "@angular/http";
import {FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import { StockTransferService  } from "./stock-transfer.service";
import { ProductStock } from "../model/ProductStock";
import {TYPEAHEAD_DIRECTIVES} from 'ng2-bootstrap';
import { ProductListItemComponent } from "./product-list-item.component";
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'product',
    templateUrl: '/StockTransfer/StockTransferProduct',
    providers: [StockTransferService, HTTP_PROVIDERS],
    directives: [TYPEAHEAD_DIRECTIVES, CORE_DIRECTIVES, FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, ProductListItemComponent],
    outputs: ['productsUpdated']
})
export class ProductComponent {
    @Input() fromInstanceId: string;

    private typeaheadOptionField: string = "name";
    public dataSource: Observable<ProductStock[]>;
    public asyncSelected: string = '';
    public typeaheadLoading: boolean = false;
    public typeaheadNoResults: boolean = false;

    public transferProduct: ControlGroup;

    public selectedProductCtl: Control;
    public quantityCtl: Control;
    public batchNoCtl: Control;

    public model: ProductStock;
    public batchs: ProductStock[];
    public selectedBatch: ProductStock;

    public addedTransfer: ProductStock[];
    productsUpdated = new EventEmitter<ProductStock[]>();

    public constructor(private stockTransferService: StockTransferService, private formBuilder: FormBuilder) {
        this.dataSource = Observable.create((observer: any) => {
            observer.next(this.asyncSelected);
        }).mergeMap((token: string) => this.stockTransferService.getProductList(token, this.fromInstanceId));
        this.createForm();
        this.model = new ProductStock();
        this.selectedBatch = new ProductStock();
        this.addedTransfer = [];
    }

    private createForm() {
        this.checkQuantity = this.checkQuantity.bind(this);
        this.transferProduct = this.formBuilder.group({
            "selectedProduct": ["", Validators.required],
            "quantity": ["", Validators.required, this.checkQuantity],
            "batchNo": ["", Validators.required],
        });
        this.selectedProductCtl = <Control>this.transferProduct.controls["selectedProduct"];
        this.quantityCtl = <Control>this.transferProduct.controls["quantity"];
        this.batchNoCtl = <Control>this.transferProduct.controls["batchNo"];
    }

    public changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    public changeTypeaheadNoResults(e: boolean): void {
        this.typeaheadNoResults = e;
    }

    public typeaheadOnSelect(e: any): void {
        this.stockTransferService.getBatchList(e.item.product.id).subscribe(batchs => {
            this.batchs = batchs;
            if (this.batchs.length == 1) {
                this.model.id = this.batchs[0].id;
                this.batchSelected(this.model.id);
            }
        });
        this.model.product = e.item.product;
        document.getElementById('quantity').focus();
    }

    public batchSelected(value: string): void {
        this.selectedBatch = this.batchs.filter(item => item.id === value)[0];
        this.model.expireDate = this.selectedBatch.expireDate;
        this.model.vat = this.selectedBatch.vat;
        this.model.batchNo = this.selectedBatch.batchNo;
        this.model.purchasePrice = this.selectedBatch.purchasePrice;
        this.model.sellingPrice = this.selectedBatch.sellingPrice;
        this.quantityCtl.updateValueAndValidity();
    }

    public addProduct() {

        if (!this.transferProduct.valid)
            return;

        var existing = this.addedTransfer.filter(item => item.id === this.model.id);
        if (existing.length == 0) {
            this.addedTransfer.push(this.model);
        }
        else {
            existing[0].quantity = parseInt(existing[0].quantity.toString()) + parseInt(this.model.quantity.toString());
        }

        this.reset();
        this.productsUpdated.emit(this.addedTransfer);
    }

    public componentReset() {
        this.reset();
        this.addedTransfer = [];
    }

    public reset() {
        this.createForm();
        this.model = new ProductStock();
        this.asyncSelected = '';
        document.getElementById('productCtl').focus();
    }

    public qtyKeyDown(keyCode) {
        if (keyCode == 13 && this.quantityCtl.valid) {
            document.getElementById('batchNo').focus();
        }
    }

    public batchNoKeyDown(keyCode) {
        if (keyCode == 13 && this.batchNoCtl.valid) {
            this.addProduct();
        }
    }

    private checkQuantity(control: AbstractControl): boolean {

        if (this.model.quantity > this.selectedBatch.stock) {
            this.quantityCtl.setErrors({ "qtyError": "Quantity cannot be more than" + this.selectedBatch.stock });
            return false;    
        }
        var addedStock = this.getAddedStock(this.model.id);
        if ((parseInt(this.model.quantity.toString()) + parseInt(addedStock.toString())) > this.selectedBatch.stock) {
            this.quantityCtl.setErrors({ "qtyError": "Quantity cannot be more than " + this.selectedBatch.stock + " already added stock is " + addedStock });
            return false;
        }

        if (this.model.quantity == 0) {
            this.quantityCtl.setErrors({ "qtyError": "Quantity cannot be zero" });
            return false;
        }

        this.quantityCtl.setErrors(null)
        return true
    }

    getAvailableStock(productStock: ProductStock): number {
        return productStock.stock - this.getAddedStock(productStock.id)
    }

    getAddedStock(id: string): number {

        var existing = this.addedTransfer.filter(item => item.id === id);
        if (existing.length == 0) {
            return 0;
        }
        return existing[0].quantity;
    }
}
