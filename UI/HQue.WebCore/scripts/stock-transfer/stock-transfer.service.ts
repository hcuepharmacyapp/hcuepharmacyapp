﻿import { Injectable } from "@angular/core";
import { Http, HTTP_PROVIDERS  } from "@angular/http";
import { Observable } from "rxjs/observable";
import "rxjs/Rx";
import "rxjs/add/operator/map";
import {Instance } from "../model/instance";
import { ProductStock } from "../model/productStock";
import { StockTransfer } from "../model/stockTransfer";

@Injectable()
export class StockTransferService {
    private transferInstanceUrl = "/StockTransferData/TransferInstance?instanceId=";
    private stockProductListUrl = '/ProductStockData/InStockProductListForInstance?productName=';
    private batchListUrl = '/ProductStockData/InStockProduct/batch?productId=';
    private transferStockUrl = '/StockTransferData/Index';
    private acceptListStockUrl = '/StockTransferData/AcceptList?forAccept=';
    private getTransfereNoUrl = '/StockTransferData/GetTransfereNo';
    private accetpRejectkUrl = '/StockTransferData/AccetpReject?id=';
    private filterInstanceUrl = '/StockTransferData/GetFilterInstance?forAccept=';

    constructor(private _http: Http) {

    }

    getTransferBranch(instanceId : string): Observable<Instance[]> {
        return this._http.get(this.transferInstanceUrl + instanceId)
            .map(res => res.json());
    }

    getProductList(filter: string, instanceId: string): Observable<ProductStock[]> {
        return this._http.get(this.stockProductListUrl + filter + '&instanceId=' + instanceId)
            .map(res => res.json());
    }

    getBatchList(productId: string): Observable<ProductStock[]> {
        return this._http.get(this.batchListUrl + productId)
            .map(res => <ProductStock[]>res.json());
    }

    stockTransfer(data: StockTransfer): Observable<string> {
        return this._http.post(this.transferStockUrl, data)
            .map(res => <string>res.json());
    }

    getTransfereNo(): Observable<string> {
        return this._http.get(this.getTransfereNoUrl)
            .map(res => <string>res.json());
    }

    getStockTransfer(data: StockTransfer, forAccept: boolean): Observable<StockTransfer[]> {
        return this._http.post(this.acceptListStockUrl + forAccept, data)
            .map(res => <StockTransfer[]>res.json());
    }

    getFilterInstance(forAccept: boolean): Observable<Instance[]> {
        return this._http.get(this.filterInstanceUrl + forAccept)
            .map(res => <Instance[]>res.json());
    }

    accetpReject(transferId: string, status: Number): Observable<boolean> {
        return this._http.post(this.accetpRejectkUrl + transferId + "&status=" + status, {})
            .map(res => <boolean>res.json());
    }
}