﻿import { Component, OnInit ,ViewChild } from "@angular/core";
import {CORE_DIRECTIVES, FormBuilder, ControlGroup, Control} from '@angular/common';
import { Http, HTTP_PROVIDERS  } from "@angular/http";
import {FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, FormGroup, FormControl, AbstractControl, Validators} from '@angular/forms';
import { ProductComponent } from "./product.component";
import { StockTransferService  } from "./stock-transfer.service";
import { Instance } from "../model/instance";
import { ProductStock } from "../model/productStock";
import { StockTransfer } from "../model/stockTransfer";
import { StockTransferItem } from "../model/stockTransferItem";

@Component({
    selector: 'stock-transfer',
    templateUrl: '/StockTransfer/StockTransfer',
    providers: [StockTransferService, HTTP_PROVIDERS],
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, REACTIVE_FORM_DIRECTIVES, ProductComponent],
})

export class StockTransferComponent implements OnInit {

    @ViewChild(ProductComponent) vc: ProductComponent;


    private toBranchs: Instance[];
    private fromBranchs: Instance[];
    public addedProducts: ProductStock[];

    public stockTransfer: ControlGroup;
    public fromBranchCtl: Control;
    public toBranchCtl: Control;

    public model : StockTransfer;

    constructor(private stockTransferService: StockTransferService, private formBuilder: FormBuilder) {
        this.createForm();
        this.model = new StockTransfer();
    }

    createForm() {
        this.stockTransfer = this.formBuilder.group({
            "fromInstanceId": ["", Validators.required],
            "toInstanceId": ["", Validators.required],
        });
        this.fromBranchCtl = <Control>this.stockTransfer.controls["fromInstanceId"];
        this.toBranchCtl = <Control>this.stockTransfer.controls["toInstanceId"];
    }

    ngOnInit() {
        this.stockTransferService.getTransferBranch('')
            .subscribe(branchs => {
                this.fromBranchs = branchs;
                if (this.fromBranchs.length == 1) {
                    this.model.fromInstanceId = this.fromBranchs[0].id;
                    this.getToInstance(this.model.fromInstanceId);
                }
            });

        this.stockTransferService.getTransfereNo()
            .subscribe(x => { this.model.transferNo = x; });
    }

    reset() {
        this.createForm();
        this.model = new StockTransfer();
        this.addedProducts = [];
        this.vc.componentReset();
        document.getElementById('fromBranch').focus();
    }

    productUpdated(products: ProductStock[]) {
        this.addedProducts = products;
    }

    isFormValid(): boolean {
        if (this.addedProducts === undefined)
            return false;
        if (this.addedProducts.length === 0)
            return false;
        return this.stockTransfer.valid;
    }

    transfer(): void {
        this.setTransferItem();
        this.stockTransferService.stockTransfer(this.model).subscribe(x => {
            this.transferSuccess(x);
        });
    }

    transferSuccess(transferNo: string): void {
        alert('Transfer successful..!');
        this.reset();
        this.model.transferNo = transferNo;
    }

    setTransferItem() : void {
        for (var i = 0; i < this.addedProducts.length; i++) {
            var item = new StockTransferItem();
            item.productStockId = this.addedProducts[i].id;
            item.quantity = this.addedProducts[i].quantity;
            this.model.stockTransferItems.push(item);
        }
    }

    getToInstance(value: string): void {
        this.stockTransferService.getTransferBranch(value)
            .subscribe(branchs => {
                this.toBranchs = branchs;
                if (this.toBranchs.length == 1) {
                    this.model.toInstanceId = this.toBranchs[0].id;
                    document.getElementById('productCtl').focus();
                }
            });
    }

    getNetValue(): number {
        if (this.addedProducts === undefined)
            return 0;

        var total = 0;
        this.addedProducts.forEach((value) => { total += value.netValue() });
        return total;
    }
}

