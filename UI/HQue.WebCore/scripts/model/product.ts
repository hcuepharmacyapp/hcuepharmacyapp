﻿export class Product {
   public id: string;
   public accountId: string;
   public instanceId: string;
   public code: string;
   public name: string;
   public manufacturer: string;
   public kindName: string;
   public strengthName: string;
   public type: string;
   public schedule: string;
   public category: string;
   public genericName: string;
   public commodityCode: string;
   public packing: number;
   public packageSize: number;
   public vAT: number;
   public price: number;
   public status: string;
   public rackNo: string;
}