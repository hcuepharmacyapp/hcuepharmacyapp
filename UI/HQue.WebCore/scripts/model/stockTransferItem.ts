﻿export class StockTransferItem {
   public id: string;
   public accountId: string;
   public instanceId: string;
   public transferId: string;
   public productStockId: string;
   public quantity: number;
   public discount: string;
}