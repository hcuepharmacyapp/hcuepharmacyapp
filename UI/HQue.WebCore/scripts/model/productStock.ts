﻿import { Product } from'./product';

export class ProductStock {

    constructor(public purchasePrice?: number, public vat?: number, public quantity?: number) {

    }

    public id: string;
    public accountId: string;
    public instanceId: string;
    public productId: string;
    public vendorId: string;
    public batchNo: string;
    public expireDate: Date;
    //public vat: number;
    public sellingPrice: string;
    public purchaseBarcode: string;
    public stock: number;
    public cST: string;
    public isMovingStock: string;
    public reOrderQty: string;
    public status: string;
    public product: Product;
    //public quantity: number;
    public noOfStrips: number;
    public unitsPerStrip: number;
    public packageSize: number;
    public packagePurchasePrice: number;
    //public purchasePrice: number;

    taxValue(): number {
        if (this.purchasePrice === undefined || this.vat === undefined)
            return 0;

        return this.purchasePrice * (this.vat / 100);
    }

    grossValue(): number {
        if (this.purchasePrice === undefined || this.quantity === undefined)
            return 0;
        return this.quantity * this.purchasePrice;
    }

    totalVatValue(): number {
        if (this.quantity === undefined)
            return 0;
        return this.quantity * this.taxValue();
    }

    netValue(): number {
        return this.grossValue() + this.totalVatValue();
    }
}
