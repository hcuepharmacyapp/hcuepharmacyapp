﻿import { StockTransferItem } from "./StockTransferItem";

export class StockTransfer {

    constructor() {
        this.stockTransferItems = [];
    }

   public id: string;
   public accountId: string;
   public instanceId: string;
   public fromInstanceId: string;
   public toInstanceId: string;
   public transferNo: string;
   public transferDate: Date;
   public fileName: string;
   public comments: string;
   public transferStatus: string;
   public stockTransferItems : StockTransferItem[];
}