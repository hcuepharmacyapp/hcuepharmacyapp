﻿export class Instance {
  public id: string;
  public accountId: string;
  public externalId: string;
  public code: string;
  public name: string;
  public registrationDate: string;
  public phone: string;
  public drugLicenseNo: string;
  public tinNo: string;
  public contactName: string;
  public address: string;
  public area: string;
  public city: string;
  public state: string;
  public pincode: string;
  public landmark: string;
  public status: string;
}