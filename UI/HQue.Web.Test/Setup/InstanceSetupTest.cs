﻿using System;
using HQue.Web.Test.Helper;

namespace HQue.Web.Test.Setup
{
    public class InstanceSetupTest
    {
        [Fact]
        public async void InsertTest()
        {
            var instanceSetup = new InstanceSetupController(ManagerHelper.GetManager<InstanceSetupManager>(),null);

            var code = new Random().Next(int.MinValue, int.MaxValue).ToString();

            var instance = new Instance
            {
                Code = code,
                Name = "My Test Instance",
                CreatedBy = "admin",
                UpdatedBy = "admin"
            };

            await instanceSetup.Index(instance);

            var cc = new CriteriaColumn(InstanceTable.CodeColumn);
            var c = new CustomCriteria(cc);
            var qb = new SqlQueryBuilder(null,null);
            qb.Parameters.Add(InstanceTable.CodeColumn.ColumnName, code);
            qb.ConditionBuilder.And(c);
            var result = await DataAccessHelper.GetDAObject<InstanceSetupDataAccess>().SingleValueAsyc(qb);

            Assert.Equal(1, Convert.ToInt32(result));
        }


        [Fact]
        public void InstanceSetupWithUsers()
        {
            var instanceSetup = new InstanceSetupController(ManagerHelper.GetManager<InstanceSetupManager>(),null);

            var code = new Random().Next(int.MinValue, int.MaxValue).ToString();

            var instance = new Instance
            {
                Code = code,
                Name = "My Latest Test Instance 2",
                CreatedBy = "admin",
                UpdatedBy = "admin"
            };

            /*** Add Instance user ***/
            HQueUser[] instanceUsers =
            {
                new HQueUser
                {
                    UserId = "anilFromUnitTest",
                    Password = "1234",
                    UserType = 1,
                    Status = 1,
                    CreatedBy = "Test",
                    UpdatedBy = "Test"
                },
                new HQueUser
                {
                    UserId = "anilFromUnitTest",
                    Password = "1234",
                    UserType = 1,
                    Status = 1,
                    CreatedBy = "Test2",
                    UpdatedBy = "Test2"
                }
            };

            instance.UserList = instanceUsers;
            var result = instanceSetup.Index(instance);
        }
    }
}
