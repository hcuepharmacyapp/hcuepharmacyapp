﻿using System;
using HQue.Web.Test.Helper;

namespace HQue.Web.Test.Setup
{
    public class AccountSetupTest
    {
        [Fact]
        public async void InsertTest()
        {
            var accountSetup =
                new AccountSetupController(ManagerHelper.GetManager<AccountSetupManager>(),null);

            var code = new Random().Next(int.MinValue, int.MaxValue).ToString();

            var account = new Account
            {
                Code = code,
                Name = "My Test Account",
                InstanceCount = 5,
                CreatedBy = "admin",
                UpdatedBy = "admin",
                User =
                {
                    UserId = Guid.NewGuid().ToString(),
                    Password = "1234",
                    UserType = 1,
                    Status = 1
                },
            };


            await accountSetup.Index(account);

            var cc = new CriteriaColumn(AccountTable.CodeColumn);
            var c = new CustomCriteria(cc);
            var qb = new SqlQueryBuilder(null,null);
            qb.Parameters.Add(AccountTable.CodeColumn.ColumnName, code);
            qb.ConditionBuilder.And(c);
            var result = await DataAccessHelper.GetDAObject<AccountSetupDataAccess>().SingleValueAsyc(qb);

            Assert.Equal(1, Convert.ToInt32(result));
        }
    }
}
