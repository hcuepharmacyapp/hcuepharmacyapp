﻿using HQue.Web.Test.Helper;

namespace HQue.Web.Test.Master
{
    public class PatientTest
    {
        [Fact]
        public void PatientListTest()
        {
            var patientController = new PatientController( ManagerHelper.GetManager<PatientManager>(),null);

            var patient = new Patient
            {
                Name = "Bindu",
                Mobile = "9611785170",
                Code = "1234",
                Email = "bindumurali.m@gmail.com"
            };

           var result = patientController.List(patient);
        }
    }
}
