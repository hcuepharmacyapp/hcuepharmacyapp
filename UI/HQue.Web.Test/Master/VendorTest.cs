﻿using System;
using HQue.Web.Test.Helper;

namespace HQue.Web.Test.Master
{
    public class VendorTest
    {
        [Fact]
        public async void InsertTest()
        {
            var addUser = new VendorDataController(ManagerHelper.GetManager<VendorManager>());

            var code = new Random().Next(int.MinValue, int.MaxValue).ToString();

            var vendor = new Vendor
            {
                Code = code,
                Name = "Test vendor 1",
                CreatedBy = "admin",
                UpdatedBy = "admin"
            };

            await addUser.Index(vendor);

            var cc = new CriteriaColumn(VendorTable.CodeColumn);
            var c = new CustomCriteria(cc);
            var qb = new SqlQueryBuilder(null,null);
            qb.Parameters.Add(VendorTable.CodeColumn.ColumnName, code);
            qb.ConditionBuilder.And(c);
            var result = await DataAccessHelper.GetDAObject<VendorDataAccess>().SingleValueAsyc(qb);

            Assert.Equal(1, Convert.ToInt32(result));
        }
    }
}
