﻿using HQue.Web.Test.Helper;

namespace HQue.Web.Test.HelperTest
{
    public class HelperTest
    {
        [Fact]
        public void DAHelperTest()
        {
            var productDataAccess = DataAccessHelper.GetDAObject<ProductDataAccess>();
            var productStockDataAccess = DataAccessHelper.GetDAObject<ProductStockDataAccess>();
            var vendorPurchaseDataAccess = DataAccessHelper.GetDAObject<VendorPurchaseDataAccess>();
            var vendorDataAccess = DataAccessHelper.GetDAObject<VendorDataAccess>();
            var userDataAccess = DataAccessHelper.GetDAObject<UserDataAccess>();
            var instanceSetupDataAccess = DataAccessHelper.GetDAObject<InstanceSetupDataAccess>();
            var accountSetupDataAccess = DataAccessHelper.GetDAObject<AccountSetupDataAccess>();
            var salesDataAccess = DataAccessHelper.GetDAObject<SalesDataAccess>();
            
        }

        [Fact]
        public void ManagerHelperTest()
        {
            var vendorPurchaseManager = ManagerHelper.GetManager<VendorPurchaseManager>();
            var vendorManager = ManagerHelper.GetManager<VendorManager>();
            var accountSetupManager = ManagerHelper.GetManager<AccountSetupManager>();
            var instanceSetupManager = ManagerHelper.GetManager<InstanceSetupManager>();
            var salesManager = ManagerHelper.GetManager<SalesManager>();
            var patientManager = ManagerHelper.GetManager<PatientManager>();
        }
    }
}
