﻿using System;
using HQue.Web.Test.Helper;

namespace HQue.Web.Test.Inventory
{
    public class SalesTest
    {
        //private readonly ICacheManager _cacheManager;

        [Fact]
        public void InsertTest()
        {
            var controller = new SalesController(ManagerHelper.GetManager<SalesManager>(),null,null);
            Sales salesModel = new Sales { AccountId = Guid.NewGuid().ToString(), InstanceId = Guid.NewGuid().ToString(), InvoiceNo = "12345" , CreatedBy = "Test" , UpdatedBy ="Test"};
           
           /*** Add Sales Items ***/
            SalesItem[] salesItems = { new SalesItem { Quantity = 2, CreatedBy = "Test", UpdatedBy = "Test" },
                                       new SalesItem { Quantity = 5, CreatedBy = "Test2", UpdatedBy = "Test2" } };
            
            salesModel.SalesItem = salesItems;
            var result = controller.Index(salesModel,null);

           // Assert.Equal(expected, query);
        }

    }
}
