﻿using System;
using System.Collections.Generic;
using HQue.Web.Test.Helper;

namespace HQue.Web.Test.Inventory
{
    public class VendorPurchaseTest
    {
        [Fact]
        public async void InsertTest()
        {
            var controller =new VendorPurchaseController( ManagerHelper.GetManager<VendorPurchaseManager>(),null );
            VendorPurchase vendorPurchase = new VendorPurchase
            {
                VendorId = Guid.NewGuid().ToString(),
                InvoiceNo = "Inv01",
                InvoiceDate = DateTime.Now,
                CreatedBy = "Test",
                UpdatedBy = "Test"
            };

            /**** Add Product Stock ****/
            //vendorPurchase.productStock = new ProductStock[] { new ProductStock { AccountId = Guid.NewGuid().ToString(), InstanceId = Guid.NewGuid().ToString(), ProductId = Guid.NewGuid().ToString(), BatchNo = "1234", ExpireDate = DateTime.Now, VAT = 12, SellingPrice = 150, Stock = 3, CreatedBy = "Test2", UpdatedBy = "Test2" },
            //                                   new ProductStock { AccountId = Guid.NewGuid().ToString(), InstanceId = Guid.NewGuid().ToString(), ProductId = Guid.NewGuid().ToString(), BatchNo = "456", ExpireDate = DateTime.Now, VAT = 15, SellingPrice = 200, Stock = 3, CreatedBy = "Test2", UpdatedBy = "Test2" }};


            /**** Add Vendor Purchase Items ****/
            List<VendorPurchaseItem> vendorPurchaseItems = new List<VendorPurchaseItem>();
            //foreach (var item in vendorPurchase.productStock)
            //{
            //    vendorPurchaseItems.Add(new VendorPurchaseItem { VendorPurchaseId = vendorPurchase.Id, ProductStockId = item.Id, Quantity = 3, PurchasePrice = 300, CreatedBy = "Test2", UpdatedBy = "Test2" });
            //    vendorPurchaseItems.Add(new VendorPurchaseItem { VendorPurchaseId = vendorPurchase.Id, ProductStockId = item.Id, Quantity = 5, PurchasePrice = 500, CreatedBy = "Test", UpdatedBy = "Test" });
            //}
            vendorPurchase.VendorPurchaseItem = vendorPurchaseItems;
            await controller.Index(vendorPurchase);
        }
    }
}
