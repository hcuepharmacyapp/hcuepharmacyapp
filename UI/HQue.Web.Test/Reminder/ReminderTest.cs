﻿using System;
using System.Collections.Generic;

namespace HQue.Web.Test.Reminder
{
    public class ReminderTest
    {
        [Fact]
        public void DailyReminderNoUpdate()
        {
            var reminder = new UserReminder
            {
                Id = Guid.NewGuid().ToString(),
                ReminderType = UserReminder.Daily,
            };

            var reminders = new List<UserReminder>
            {
                reminder
            };

            var remarks = new List<ReminderRemark>();

            var listManager = new UserReminderListManager(reminders, remarks);
            var list = listManager.GetList();

            Assert.Equal(3, list.Count);
        }

        [Fact]
        public void DailyReminderToDayUpdated()
        {
            var reminder = new UserReminder
            {
                Id = Guid.NewGuid().ToString(),
                ReminderType = UserReminder.Daily,
            };

            var reminders = new List<UserReminder>
            {
                reminder
            };

            var remark = new ReminderRemark
            {
                Id = Guid.NewGuid().ToString(),
                UserReminderId = reminder.Id,
                RemarksFor = DateTime.Now
            };

            var remarks = new List<ReminderRemark>
            {
                remark
            };


            var listManager = new UserReminderListManager(reminders, remarks);
            var list = listManager.GetList();

            Assert.Equal(2, list.Count);
        }

        [Fact]
        public void WeeklyReminderNoUpdateNoResult()
        {
            var reminder = new UserReminder
            {
                Id = Guid.NewGuid().ToString(),
                ReminderType = UserReminder.Weekly,
                StartDate = DateTime.Now.AddDays(-3)
            };

            var reminders = new List<UserReminder>()
            {
                reminder
            };

            var remarks = new List<ReminderRemark>();

            var listManager = new UserReminderListManager(reminders, remarks);
            var list = listManager.GetList();

            Assert.Equal(0, list.Count);
        }

        [Fact]
        public void WeeklyReminderNoUpdateOneResult()
        {
            var reminder = new UserReminder
            {
                Id = Guid.NewGuid().ToString(),
                ReminderType = UserReminder.Weekly,
                StartDate = DateTime.Now.AddDays(-6)
            };

            var reminders = new List<UserReminder>
            {
                reminder
            };

            var remarks = new List<ReminderRemark>();

            var listManager = new UserReminderListManager(reminders, remarks);
            var list = listManager.GetList();

            Assert.Equal(1, list.Count);
        }
    }
}
