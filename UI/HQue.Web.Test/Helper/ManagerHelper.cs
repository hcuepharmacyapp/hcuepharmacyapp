﻿using System;

namespace HQue.Web.Test.Helper
{
    public class ManagerHelper
    {
        public static T GetManager<T>()
        {
            object m = null;

            switch (typeof (T).Name)
            {
                case "VendorPurchaseManager":
                    m = new VendorPurchaseManager(DataAccessHelper.GetDAObject<VendorPurchaseDataAccess>(),
                        null,null,null);
                    break;
                case "VendorManager":
                    m = new VendorManager(DataAccessHelper.GetDAObject<VendorDataAccess>(), null);
                    break;
                case "AccountSetupManager":
                    m = new AccountSetupManager(DataAccessHelper.GetDAObject<AccountSetupDataAccess>(),
                        null, DataAccessHelper.GetDAObject<UserDataAccess>());
                    break;
                case "InstanceSetupManager":
                    m = new InstanceSetupManager(DataAccessHelper.GetDAObject<InstanceSetupDataAccess>(),
                        DataAccessHelper.GetDAObject<UserDataAccess>(), null, null, null, null);
                    break;
                case "UserReminderManager":
                    m = new UserReminderManager(DataAccessHelper.GetDAObject<UserReminderDataAccess>());
                    break;
                case "SalesManager":
                    m = new SalesManager(DataAccessHelper.GetDAObject<SalesDataAccess>(),
                        DataAccessHelper.GetDAObject<ProductDataAccess>(),
                        GetManager<InstanceSetupManager>(),null,null,null,null,null);
                    break;
                case "PatientManager":
                    m = new PatientManager(DataAccessHelper.GetDAObject<PatientDataAccess>(), new PatientApi(null), new SmsSender(null, null, null));
                    break;
            }

            if (m == null)
                throw new Exception($"No helper for manager {typeof (T).Name}");

            return (T) m;
        }
    }
}
