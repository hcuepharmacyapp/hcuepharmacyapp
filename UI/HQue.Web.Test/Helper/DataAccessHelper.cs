﻿using System;

namespace HQue.Web.Test.Helper
{
    public class DataAccessHelper
    {
        public static T GetDAObject<T>()
        {
            ISqlHelper helper = new SqlServerHelper("");

            object da = null;
            switch (typeof (T).Name)
            {
                case "ProductDataAccess":
                    da = new ProductDataAccess(helper,null,null);
                    break;
                case "ProductStockDataAccess":
                    da = new ProductStockDataAccess(helper, null, null);
                    break;
                case "VendorPurchaseDataAccess":
                    da = new VendorPurchaseDataAccess(helper, GetDAObject<ProductDataAccess>(),
                        GetDAObject<ProductStockDataAccess>(),null);
                    break;
                case "VendorDataAccess":
                    da = new VendorDataAccess(helper,null);
                    break;
                case "UserDataAccess":
                    da = new UserDataAccess(helper, GetDAObject<UserManagementDataAccess>(),null);
                    break;
                case "InstanceSetupDataAccess":
                    da = new InstanceSetupDataAccess(helper,null);
                    break;
                case "AccountSetupDataAccess":
                    da = new AccountSetupDataAccess(helper,null);
                    break;
                case "SalesDataAccess":
                    da = new SalesDataAccess(helper, GetDAObject<ProductStockDataAccess>(),null,null);
                    break;
                case "DoctorDataAccess":
                    da = new DoctorDataAccess(helper,null);
                    break;
                case "PatientDataAccess":
                    da = new PatientDataAccess(helper,null);
                    break;
                case "UserReminderDataAccess":
                    da = new UserReminderDataAccess(helper,null);
                    break;
                case "UserManagementDataAccess":
                    da = new UserManagementDataAccess(helper,null);
                    break;
            }

            if (da == null)
            {
                throw new Exception($"No helper for manager {typeof (T).Name}");
            }
            return (T) da;
        }
    }
}
