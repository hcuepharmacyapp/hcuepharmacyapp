﻿//using System.Collections.Generic;
//using Microsoft.AspNet.Mvc;
//using Utilities.Mvc.HtmlHelper;

//namespace Utilities.Mvc.ViewHelper
//{
//    public abstract class ViewBase<T> : WebViewPage<T>
//    {
//        protected ViewBase()
//        {
//            DDRepository = DependencyResolver.Current.GetService<IDDRepository>();
//        }

//        protected ViewBase(IDDRepository repository)
//        {
//            DDRepository = repository;
//        }

//        public IDDRepository DDRepository { get; private set; }

//        public PaggerParms PaggerParms
//        {
//            get { return ViewContext.ViewBag.PaggerParms as PaggerParms; }
//        }

//        public string GetStatusClass(int? workStatus)
//        {
//            if (!workStatus.HasValue)
//                return string.Empty;
//            switch (workStatus)
//            {
//                case 7:
//                    return "status-red";
//                case 8:
//                    return "status-yellow";
//                case 9:
//                    return "status-green";
//                case 10:
//                    return "status-blue";
//            }
//            return string.Empty;
//        }
//    }

//    public interface IDDRepository
//    {
//        IEnumerable<T> GetDDList<T>() where T : ICacheList<T>;
//    }

//    public class DDRepository : IDDRepository
//    {
//        private readonly IDependencyResolver _resolver;

//        public DDRepository(IDependencyResolver resolver)
//        {
//            _resolver = resolver;
//        }

//        public IEnumerable<T> GetDDList<T>() where T : ICacheList<T>
//        {
//            var x = _resolver.GetService<T>();
//            return x.CacheList().Result;
//        }
//    }
//}
