﻿//using Microsoft.AspNet.Html.Abstractions;
//using Microsoft.AspNet.Mvc.Rendering;

//namespace Utilities.Mvc.HtmlHelper
//{
//    public static class Pagger
//    {
//        public static IHtmlContent Pagenate(this IHtmlHelper htmlHelper,PaggerParms paggerParms)
//        {
//            var pageStartRow = (paggerParms.CurrentPage*paggerParms.PageRows) - (paggerParms.PageRows - 1);
//            var pageLastRow = (pageStartRow + (paggerParms.PageRows - 1)) > paggerParms.TotalRows
//                ? paggerParms.TotalRows
//                : (pageStartRow + (paggerParms.PageRows - 1));


//            var pageMessage = paggerParms.TotalRows == 0
//                ? "No records found"
//                : string.Format("Page {0} ({3} - {4}) of {1} ({2})", paggerParms.CurrentPage, paggerParms.NoOfPages,
//                    paggerParms.TotalRows, pageStartRow, pageLastRow);

//            var divTag = new TagBuilder("div");
//            var pTag = new TagBuilder("p")
//            {
//                InnerHtml = new HtmlString(pageMessage) 
//            };
//            divTag.InnerHtml = pTag;
//            divTag.Attributes.Add("class", "customPagger");
//            var ulTag = new TagBuilder("ul");

//            foreach (var links in paggerParms.Links)
//            {
//                var className = "paggerGetLink";

//                if (paggerParms.IsPostRequest)
//                    className = "paggerPostLink";

//                var liTag = new TagBuilder("li")
//                {
//                    InnerHtml =
//                        htmlHelper.RouteLink(links.Item1, paggerParms.RouteName, new {page = links.Item2} , new { @class = className })
//                };
//                ulTag.InnerHtml = new HtmlString(ulTag.InnerHtml + liTag.ToString()); 
//            }

//            divTag.InnerHtml = new HtmlString(divTag.InnerHtml + ulTag.ToString());
//            return divTag;
//        }
//    }
//}
