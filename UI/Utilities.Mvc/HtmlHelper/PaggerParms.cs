using System;
using System.Collections.Generic;

namespace Utilities.Mvc.HtmlHelper
{
    public class PaggerParms
    {
        public bool WithFirstLast { get; set; }


        public PaggerParms()
        {
            NoOfLinks = 5;
            PageRows = 20;
            WithFirstLast = true;
        }

        public int NoOfLinks { get; set; }

        public int PageRows { get; set; }

        public int TotalRows { get; set; }

        public int CurrentPage { get; set; }

        public string RouteName { get; set; }

        public bool IsPostRequest { get; set; }

        public int NoOfPages
        {
            get { return (int)Math.Ceiling(Convert.ToDecimal(TotalRows) / Convert.ToDecimal(PageRows)); }
        }

        private int LinksToAdd
        {
            get { return NoOfLinks / 2; }
        }

        public IEnumerable<Tuple<string,int>> Links
        {
            get
            {
                if (NoOfLinks == 2)
                {
                    if (CurrentPage > 1)
                        yield return new Tuple<string, int>("Previous", CurrentPage - 1); 
                    if (CurrentPage < NoOfPages)
                        yield return new Tuple<string, int>("Next", CurrentPage + 1); 
                }

                if (NoOfLinks > 2)
                {
                    if (NoOfPages < NoOfLinks)
                    {
                        if(NoOfPages == 1)
                            yield break;

                        for (var i = 1; i <= NoOfPages; i++)
                            yield return new Tuple<string, int>(i.ToString(), i); 
                    }
                    else
                    {
                        var max = CurrentPage + LinksToAdd;
                        var min = CurrentPage - LinksToAdd;

                        if (max > NoOfPages)
                        {
                            min = (NoOfPages - NoOfLinks) + 1;
                            max = NoOfPages;
                        }

                        if (min < 1)
                        {
                            min = 1;
                            max = NoOfLinks;
                        }

                        if (WithFirstLast && CurrentPage > 1)
                            yield return new Tuple<string, int>("First", 1); 

                        for (var i = min; i <= max; i++)
                            yield return new Tuple<string, int>(i.ToString(), i); 

                        if (WithFirstLast && CurrentPage < NoOfPages)
                            yield return new Tuple<string, int>("Last", NoOfPages); 
                    }
                }
            }
        }
    }
}