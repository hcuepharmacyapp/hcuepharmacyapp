﻿using System.Collections.Generic;
using Utilities.Mvc.Message.Interface;
using Utilities.Repository.Interface;

namespace Utilities.Mvc.Message
{
    public class UIMesaageService : IUIMesaageService
    {
        private readonly IMessageRepository _messageRepository;

        public UIMesaageService(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        public string SetUIMessage(int messageId, UIMessageType type, bool isNotification , params string[] messageParams)
        {
            var message = PrepareMessage(messageId, messageParams);
            SetUIMessage(message, type, isNotification);
            return message;
        }

        public string SetUIMessage(int messageId, UIMessageType type, params string[] messageParams)
        {
            return SetUIMessage(messageId, type, false, messageParams);
        }

        public string SetUIMessage(int messageId, UIMessageType type, bool isNotification)
        {
            return SetUIMessage(messageId, type, isNotification, new string[0]);
        }

        public string SetUIMessage(int messageId, UIMessageType type)
        {
            return SetUIMessage(messageId, type, false, new string[0]);
        }

        public Queue<IFlashMessage> TosterQueue { get; }
        public Queue<IFlashMessage> NotificationQueue { get; }

        private string PrepareMessage(int messageId,params string[] messageParams)
        {
            return _messageRepository.GetMessage(messageId, messageParams);
        }

        public void SetUIMessage(string message, UIMessageType type, bool isNotification = false)
        {
            //if (isNotification)
            //    NotificationQueue.Enqueue(new FlashMessage { Message = message, MessageType = type });
            //else
            //    TosterQueue.Enqueue(new FlashMessage { Message = message, MessageType = type });
        }

        //public Queue<IFlashMessage> TosterQueue
        //{
        //    get
        //    {
        //        var m = HttpContext.Current.Session["FlashMessageAlert"] as Queue<IFlashMessage> ??
        //                new Queue<IFlashMessage>();
        //        HttpContext.Current.Session["FlashMessageAlert"] = m;
        //        return m;
        //    }
        //}

        //public Queue<IFlashMessage> NotificationQueue
        //{
        //    get
        //    {
        //        var m = HttpContext.Current.Session["NotificationQueue"] as Queue<IFlashMessage> ??
        //                new Queue<IFlashMessage>();
        //        HttpContext.Current.Session["NotificationQueue"] = m;
        //        return m;
        //    }
        //}
    }
}