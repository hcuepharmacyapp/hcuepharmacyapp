﻿using Utilities.Mvc.Message.Interface;

namespace Utilities.Mvc.Message
{
    internal class FlashMessage : IFlashMessage
    {
        public string  Message { get; set; }
        public UIMessageType MessageType { get; set; }
    }
}
