﻿using System.ComponentModel.DataAnnotations;

namespace Utilities.Mvc.Message
{
    public enum UIMessageType
    {
        [Display(Name = "success")]
        Success,
        [Display(Name = "error")]
        Error,
        [Display(Name = "info")]
        Info,
        [Display(Name = "warning")]
        Warning,
    }
}