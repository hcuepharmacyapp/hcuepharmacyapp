﻿using System.Collections.Generic;

namespace Utilities.Mvc.Message.Interface
{
    public interface IUIMesaageService
    {
        string SetUIMessage(int messageId, UIMessageType type, bool isNotification , params string[] messageParams);
        string SetUIMessage(int messageId, UIMessageType type, params string[] messageParams);
        string SetUIMessage(int messageId, UIMessageType type, bool isNotification );
        string SetUIMessage(int messageId, UIMessageType type);
        Queue<IFlashMessage> TosterQueue { get; }
        Queue<IFlashMessage> NotificationQueue { get; }
    }
}