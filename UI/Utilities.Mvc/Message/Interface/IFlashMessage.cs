﻿namespace Utilities.Mvc.Message.Interface
{
    public interface IFlashMessage
    {
        string Message { get; set; }
        UIMessageType MessageType { get; set; }
    }
}