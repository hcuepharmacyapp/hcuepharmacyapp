﻿using System;

namespace HQue.Contract.Infrastructure.Misc
{
    public class DateRange
    {
        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }
    }
}
