﻿using HQue.Contract.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.Infrastructure.Reports
{
    public class StockMovement: BaseStockMovement
    {
       
        public int TotalInQty { get; set; }
        public int TotalOutQty { get; set; }
        public int ClosingQty { get; set; }
        public int CurrentStock { get; set; }
        public int StockExecuteStatus { get; set; }
        public string  ClosingStockMessge { get; set; }
    }
}
