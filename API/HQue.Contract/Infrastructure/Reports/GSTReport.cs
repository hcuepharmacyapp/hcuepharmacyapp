﻿using HQue.Contract.Base;
using Newtonsoft.Json;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Inventory;
using System;

namespace HQue.Contract.Infrastructure.Reports
{
    public class GSTReport : BaseGSTReport
    {
        public GSTReport()
        {

        }
        public virtual string Code { get; set; }
        public virtual string HSN { get; set; }
        public virtual string BranchName { get; set; }
        public virtual string BranchGSTin { get; set; }
        public virtual string ProductName { get; set; }
        public virtual string BatchNo { get; set; }
        public virtual DateTime ExipryDate { get; set; }
        public virtual string ExipryDate1 { get; set; }
        public virtual string HSNDesc { get; set; }
        public virtual decimal GSTTotal { get; set; }
        public virtual string uom { get; set; }
        public virtual string RecipientName { get; set; }
        public virtual string RecipientGSTin{ get; set; }
        public virtual string PartyType { get; set; }
        public virtual string InvoiceNumber { get; set; }   
        public virtual DateTime InvoiceDate { get; set; }
        public virtual string InvoiceDate1 { get; set; }
        public virtual decimal Quantity { get; set; }
        public virtual decimal InvoiceValue { get; set; }
        public virtual string PlaceOfSupply { get; set; }
        public virtual string IsGoods { get; set; }
             public virtual string IsReverseCharge { get; set; }
        public virtual string SupplyType { get; set; }
        public virtual decimal ValueWithoutTax { get; set; }
  
        public virtual decimal SGST { get; set; }
        public virtual decimal CGST { get; set; }
        public virtual decimal IGST { get; set; }
        public virtual decimal TaxAmount { get; set; }
        public virtual decimal SGSTAmount { get; set; }
        public virtual decimal CGSTAmount { get; set; }
        public virtual decimal IGSTAmount { get; set; }
        public virtual string ImportType { get; set; }
        public virtual decimal Cess { get; set; }

        //Added by Sarubala on 01-09-17 for GSTR-B2CS Report
        public virtual string SupplyLocationType { get { return "Inter"; } set { } } //Inter or Intera
        public virtual string Type { get { return "OE"; } set { } }
        public virtual decimal ReturnCharges { get; set; }
    }
    public class GSTIn
    {
        public virtual string GSTinNumber { get; set; }
    }
    public class ControlReport
    {
        public virtual string PaymentMode { get; set; }
        public virtual decimal TranCount { get; set; }
        public virtual decimal TransTotal { get; set; }
        public virtual decimal Basic { get; set; }
        public virtual decimal TaxAmount { get; set; }
        public virtual decimal DiscountValue { get; set; }
        public virtual decimal RoundOff { get; set; }
        public virtual decimal GroupSum { get; set; }
        public virtual decimal DisplayType { get; set; }

    }
}