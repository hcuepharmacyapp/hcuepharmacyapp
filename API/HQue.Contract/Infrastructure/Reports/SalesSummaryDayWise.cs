﻿using HQue.Contract.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.Infrastructure.Reports
{
    public class SalesSummaryDayWiseReport: BaseCustomReport
    {
        public string InstanceName { get; set; }
        public DateTime SalesDate { get; set; }
        public decimal TotalBills { get; set; }
        public decimal TotalCash { get; set; }
        public decimal TotalCC { get; set; }
        public decimal TotalCredit { get; set; }
        public decimal TotalSales { get; set; }
        public DateTime PrevMonthDate { get; set; }
        public decimal PrevMonthSales { get; set; }
        public decimal AvgSales { get; set; }
    }
}
