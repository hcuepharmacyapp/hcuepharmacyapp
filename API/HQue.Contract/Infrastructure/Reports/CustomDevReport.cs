﻿using HQue.Contract.Base;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Reports
{
    public class CustomDevReport : BaseCustomDevReport
    {
        public CustomDevReport()
        {
            ReportDevData = new ReportDevData();
        }

        public ReportDevData ReportDevData { get; set; }
        public List<ReportFieldA> selectedFields { set; get; }
    }
    

   
    public class ReportDevData
    {
        public List<ReportFieldA> selectedFields { set; get; }
        public string filter { set; get; }
        public string reportName { set; get; }
        public int reportType { set; get; }
    }


    public class ReportFieldA
    {   public string Id { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public bool IsGrouped { get; set; }
        public string Aggregate { get; set; }
        public bool IsShowTotal { get; set; }
    }

}
