﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.Infrastructure.Reports
{
    public class VendorpurchaseReturnReport: BaseProductStock
    {
        public VendorpurchaseReturnReport()
        {                    
        }        
                        
        public string ProductName { get; set; }        

        public decimal? VatAmount { get; set; }
        public decimal? Total { get; set; }
        public decimal? CostPrice { get; set; }
        public int? NoOfItem { get; set; }

    }
}
