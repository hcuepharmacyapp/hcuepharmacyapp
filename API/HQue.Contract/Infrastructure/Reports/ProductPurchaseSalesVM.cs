﻿using HQue.Contract.Infrastructure.Inventory;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Reports
{
    public class ProductPurchaseSalesVM
    {

        public List<Sales> Sales { get; set; }
        //public List<SalesItem> SalesItems { get; set; }
        public List<VendorPurchase> VendorPurchases { get; set; }
        //public List<VendorPurchaseItem> VendorPurchaseItems { get; set; }
        //public ProductStock ProductStock { get; set; }
        
    }
    
}
