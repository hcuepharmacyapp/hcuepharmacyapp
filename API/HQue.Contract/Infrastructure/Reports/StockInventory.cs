﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class StockInventory : BaseProductStock
    {
        public StockInventory()
        {
            Product1 = new BaseProduct();
            SalesItem1 = new BaseSalesItem();
            VendorPurchaseItem1 = new VendorPurchaseItem();
            Sale1 = new Sales();

        }

        public BaseProduct Product1 { get; set; }
        public VendorPurchaseItem VendorPurchaseItem1 { get; set; }

        public BaseSalesItem SalesItem1 { get; set; }

        public Sales Sale1 { get; set; }

        public decimal? InputTax { get; set; }

        public decimal? PurchaseItemTotal
        {
            get
            {
                if (!(VendorPurchaseItem1.Quantity.HasValue && VendorPurchaseItem1.PurchasePrice.HasValue))
                    return 0;
                                                
                if ((VendorPurchaseItem1.PackagePurchasePrice > 0) && (VendorPurchaseItem1.PackageQty > 0))
                {
                    decimal? tmp1 = 0;
                    tmp1 = VendorPurchaseItem1.PackagePurchasePrice * (VendorPurchaseItem1.PackageQty - VendorPurchaseItem1.FreeQty);

                    if (VendorPurchaseItem1.Discount .HasValue)
                        return tmp1 - (tmp1 * VendorPurchaseItem1.Discount / 100);
                    return tmp1;
                }
                else
                {
                    return 0;
                }
            }
            set { }
        }

        public decimal? SellingItemTotal
        {
            get
            {
                decimal? tmp2 = 0;
                if ((SalesItem1.Quantity > 0) && (SalesItem1.SellingPrice > 0))
                {                    
                    tmp2 = SalesItem1.Quantity * SalesItem1.SellingPrice;

                    if(Sale1.Discount.HasValue)
                        return tmp2 - (tmp2 * Sale1.Discount / 100);
                    return tmp2;
                }

                if (!(SalesItem1.Quantity.HasValue && SellingPrice.HasValue))
                { return 0; }

                else
                {
                    tmp2 = SalesItem1.Quantity * SellingPrice;
                    if (Sale1.Discount.HasValue)
                        return tmp2 - (tmp2 * Sale1.Discount / 100);
                    return tmp2;
                }
            }
            set { }
        }

        public decimal? PurchaseAmountWithTax
        {
            get
            {
                if (!(PurchaseItemTotal.HasValue && VAT.HasValue))
                    return 0;

                if ((PurchaseItemTotal > 0) && (CST > 0))
                    return (PurchaseItemTotal + (PurchaseItemTotal * (CST / 100)));
                else if ((PurchaseItemTotal > 0) && (VAT > 0))
                    return (PurchaseItemTotal + (PurchaseItemTotal * (VAT / 100)));
                else
                { return 0; }
            }
            set { }
        }

        public decimal? SellingTax
        {
            get
            {
                if ((SellingItemTotal > 0) && (VAT > 0))
                    return (SellingItemTotal * VAT) / 100;
                else
                { return 0; }
            }
            set { }
        }

        public decimal? PurchaseVatAmount
        {
            get
            {
                if (PurchaseItemTotal.HasValue && PurchaseAmountWithTax.HasValue)
                    return PurchaseAmountWithTax - PurchaseItemTotal;
                else
                    return 0;
            }
            set { }
        }

        public decimal? SellingPriceTotal
        {
            get
            {
                if (SellingItemTotal.HasValue && SellingTax.HasValue)
                    return SellingItemTotal + SellingTax;
                return 0;
            }

            set { }
        }

    }

    public class StockInventoryData
    {
        public string Name { get; set; }

        public decimal? vat { get; set; }

        public decimal? InputTax { get; set; }

        public decimal? PurchaseItemTotal { get; set; }

        public decimal? SellingPrice { get; set; }

        public string CommodityCode { get; set; }

        public StockInventoryData()
        { }


    }
}
