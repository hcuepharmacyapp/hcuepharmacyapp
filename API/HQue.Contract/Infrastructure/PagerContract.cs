﻿using System.Collections.Generic;

namespace HQue.Contract.Infrastructure
{
    public class PagerContract<T>
    {
        public int NoOfRows { get; set; }

        public IEnumerable<T> List { get; set; }
    }
}
