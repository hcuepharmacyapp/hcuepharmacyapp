﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Master
{
    public class AlternateVendorProduct : BaseAlternateVendorProduct
    {
        public bool? isAddNewProduct { get; set; }

        public int itemIndex { get; set; }

        public string ProductName { get; set; }
    }
}
