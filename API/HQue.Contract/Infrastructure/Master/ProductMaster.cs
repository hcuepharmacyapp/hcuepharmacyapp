﻿using System.Collections.Generic;
using System.Linq;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Contract.Infrastructure.Master
{
    public class ProductMaster : BaseProductMaster
    {
        public ProductMaster()
        {
           
        }

        public int? Total { get; set; }
        public int? Completed { get; set; }
        public int? Pending { get; set; }
        public int? Today { get; set; }
    }
}
