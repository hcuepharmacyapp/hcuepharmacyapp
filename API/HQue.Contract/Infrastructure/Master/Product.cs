﻿using System.Collections.Generic;
using System.Linq;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Contract.Infrastructure.Master
{
    public class Product : BaseProduct
    {
        public Product()
        {
            ProductStock = new List<ProductStock>();
            InventoryReOrder = new InventoryReOrder();
            Instance = new Instance();
            ProductInstance = new ProductInstance();
            //MasterProductStock = new ProductStock();
        }
        public IEnumerable<ProductStock> ProductStock { get; set; }
        public InventoryReOrder InventoryReOrder { get; set; }
        public Instance Instance { get; set; }
        public ProductInstance ProductInstance { get; set; }
        //public ProductStock MasterProductStock { get; set; }
        public decimal? Stock
        {
            get
            {
                return ProductStock.Sum(m => m.TotalStock);
            }
            //set { }
        }
        public decimal? Totalpurchaseprice { get { return ProductStock.Sum(m => m.PurchasePrice); } } //Added by Annadurai on 22/06/2017

        public string getFilter { get; set; }
        public decimal? Totalstock { get; set; }
        public decimal? purchaseprice { get; set; } = 0.00M;
       

        //public decimal? Discount { set; get; } = 0.00M;
        //public string Manufacturer { get; set; }
        //public string Schedule { get; set; }
        //public string GenericName { get; set; }
        //public string Packing { get; set; }
        //public string CommodityCode { get; set; }
        //public string Category { get; set; }
        //public string Type { get; set; }

        public string BoxNo { get; set; }// Added by Settu on 19/05/2017
        public bool IsEdit { get; set; } 
        public string DisplayStatus { get; set; } //Added by Poongodi

        public bool IsGstNullSearch { get; set; }
    }


    public class MasterProduct : BaseProduct
    {
        public decimal? Totalstock { get; set; }
        public virtual string BoxNo { get; set; }
    }
    //Added by Poongodi on 06/06/2017
    public class ProductSearch 
    {
    public string Id { get; set; }
    public string AccountId { get; set; }
        public string InstanceId { get; set; }
        public string Name { get; set; }
  
    }
    //Added by Poongodi on 21/08/2017
    public class SyncProduct
    {
        public string fname { get; set; }
        public string AccountId { get; set; }
        public string Userid { get; set; }


    }
}
