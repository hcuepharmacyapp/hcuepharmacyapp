﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.UserAccount;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Master
{
    public class Vendor : BaseVendor
    {
        public Vendor()
        {
            User = new HQueUser();
            Instance = new Instance();
            //by san
            VendorPurchaseReturnItem = new List<VendorPurchaseReturnItem>();
            StateRef = new State();
        }

        public HQueUser User { get; set; }
        public Instance Instance { get; set; }
        //added by nandhini for csv 24.10.17

        public string ReportInvoiceDate { get; set; }
        public List<VendorPurchaseReturnItem> VendorPurchaseReturnItem { get; set; }

        public string tempDrugLicenseNo { get; set; } //Added by Lawrence
       
        public State StateRef { get; set; }
        public virtual string VendorId { get; set; }

        public override string StateId { get; set; }
        public VendorPurchaseFormula VendorPurchaseFormula { get; set; }
        // Newly Added Gavaskar 22-10-2016 Start

        //public bool Mobile1checked { set; get; }
        //public bool Mobile2checked { set; get; }

        // Newly Added Gavaskar 22-10-2016 End

    }
}
