﻿using System.Collections.Generic;
using System.Linq;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Contract.Infrastructure.Master
{
    public class DrugList : BaseProduct
    {
        public DrugList()
        {
            InventoryReOrder = new InventoryReOrder();
            Instance = new Instance();
            ProductStock = new ProductStock();
        }
        public ProductStock ProductStock { get; set; }
        public InventoryReOrder InventoryReOrder { get; set; }
        public Instance Instance { get; set; }
        //public ProductStock MasterProductStock { get; set; }
       
        public string getFilter { get; set; }
    }
}
