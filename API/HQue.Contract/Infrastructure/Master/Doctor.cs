﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Master
{
    public class Doctor : BaseDoctor
    {
        public Doctor()
        {
            Product = new Master.Product();
        }

        public string DayCD { get; set; }
        public string SpecialityID { get; set; }
        public string Gender { get; set; }
        public int Radius { get; set; }
        public string Sort { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
        public string Select { get; set; }
        public string Values { get; set; }
        public int DoctorCount { get; set; }
        public bool isexists { get; set; }

        //Added by Sarubala for Reports on 28-08-17
        public Product Product { get; set; }
        public decimal? Quantity { get; set; }

        public decimal? SalesQty { get; set; }
        public decimal? ReturnQty { get; set; }
        public decimal? NetQuantity { get; set; }
        public decimal? NetValue { get; set; }
        public string BranchName { get; set; }
        public decimal ProfitPerc { get; set; }

    }
}
