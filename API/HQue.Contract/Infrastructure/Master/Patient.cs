﻿using System;
using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Master
{
    public class Patient : BasePatient
    {
        public override int? Age
        {
            get
            {
                if (!DOB.HasValue)
                    return 0;
                DateTime today = DateTime.Today;
                int age = today.Year - DOB.Value.Year;
                return age;
            }

            set { }
        }

        public bool isValidEmail { get; set; }
        // Newly Added Gavaskar 23-10-2016 Start
        public bool isexists { get; set; }
        // Newly Added Gavaskar 23-10-2016 End

        public bool ispatientLocal { get; set; }

        public string strCheckSales { get; set; }
        public int patientSearchType { get; set; }
    

        public string LocationTypeName { get; set; }
    }
   
    public enum LocationType
    {
        Local = 1,
        Interstate = 2,
        Export = 3
    }

    public enum CustStatus
    {
        Active = 1,
        InActive = 2,
        Status = 3,
    }
}
