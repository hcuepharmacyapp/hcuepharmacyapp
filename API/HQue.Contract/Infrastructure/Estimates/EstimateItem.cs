﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Estimates
{
    public class EstimateItem : BaseEstimateItem , IReminderItem , IInvoiceItem
    {
        public decimal? DiscountAmount
        {
            get
            {
                if (!(Total.HasValue && Discount.HasValue))
                    return 0;
                return Discount / 100 * Total;
            }
            set { }
        }

        public EstimateItem()
        {
            ProductStock = new ProductStock();
        }

        public ProductStock ProductStock { get; set; }

        public decimal? Total
        {
            get
            {
                if ((Quantity.HasValue && SellingPrice > 0))
                {
                    return SellingPrice * Quantity;
                }

                if (!(Quantity.HasValue && ProductStock.SellingPrice.HasValue))
                    return 0;
                return ProductStock.SellingPrice * Quantity;
            }
        }

        public decimal? MRPTotal
        {
            get
            {
                if (!(Quantity.HasValue && ProductStock.MRP.HasValue))
                    return 0;
                return ProductStock.MRP * Quantity;
            }
        }

        [JsonIgnore]
        public UserReminderType SetReminder => ReminderFrequency.HasValue ? (UserReminderType)ReminderFrequency.Value : UserReminderType.None;

        public decimal? ActualAmount
        {
            get
            {
                if (!(Total.HasValue && ProductStock.VAT.HasValue))
                    return 0;
                return (Total * 100) / (ProductStock.VAT + 100);
            }
        }

        public decimal? VatAmount
        {
            get
            {
                if (!(Total.HasValue && ActualAmount.HasValue))
                    return 0;
                return Total - ActualAmount;
            }
        }
        public decimal? ReturnedQty { get; }
        //Added by Bikas on 15-05-18
        public decimal? MRP
        {
            get; set;
        }
        public decimal? MRPPerStrip
        {
            get; set;
        }
        public decimal? SellingPricePerStrip
        {
            get; set;
        }
        public decimal? Igst { get; set; }
        public decimal? IgstValue { get; set; }
        public decimal? Cgst { get; set; }
        public decimal? CgstValue { get; set; }
        public decimal? Sgst { get; set; }
        public decimal? SgstValue { get; set; }
        public decimal? GstTotal { get; set; }
        public decimal? GstTotalValue { get; set; }
        public decimal? NetAmount { get; }

         //Added by Sarubala on 10/05/2018 for custom template
        public decimal? MRPAfterDiscount { get; set; }
        public decimal? SellingPriceAfterDiscount { get; set; }
        public decimal? MRPValueAfterDiscount { get; set; }
        public decimal? SellingPriceValueAfterDiscount { get; set; }
    }
}
