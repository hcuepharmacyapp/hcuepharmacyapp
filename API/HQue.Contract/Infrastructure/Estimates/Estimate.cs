﻿
using System;
using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using System.Linq;
using Newtonsoft.Json;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.Contract.Infrastructure.Estimates
{
    public class Estimate : BaseEstimate ,IReminder , IInvoice
    {
        public Estimate()
        {
            EstimateItem = new List<EstimateItem>();
            Instance = new Instance();
            HQueUser = new HQueUser();
            Patient = new Patient();
            SalesReturnItem = new List<SalesReturnItem>();
            SalesPayments = new List<SalesPayment>();
        }

        public List<EstimateItem> EstimateItem { get; set; }
        public string InvoiceNo => EstimateNo;
        public DateTime? InvoiceDate => EstimateDate;
        public string PaymentType { get; set; } //Modified by Sarubala on 08-11-17
        public Instance Instance { get; set; }
        public HQueUser HQueUser { get; }
        public decimal? PurchasedTotal => EstimateItem.Sum(m => m.Total);
        public decimal? MRPTotal => EstimateItem.Sum(m => m.MRPTotal);

        public decimal? DiscountInValue
        {
            get
            {
                if (!Discount.HasValue)
                    return Discount = 0;
                return (Discount / 100) * PurchasedTotal + EstimateItem.Sum(m => m.DiscountAmount);
            }
        }

        public decimal? RoundOff => (decimal?)Math.Round((double)Net);
        //public decimal? RoundoffNetAmount => (decimal?)Math.Round((double)Net); // by  San

        public decimal? Net
        {
            get
            {
                return (SubTotal - (DiscountInValue)) + EstimateItem.Sum(m => m.VatAmount);
            }
        }

        private decimal? SubTotal
        {
            get
            {
                return EstimateItem.Sum(m => m.ActualAmount);
            }
        }

        public Patient Patient { get; set; }
        public int ReferenceFrom => UserReminder.SalesReminder;
        [JsonIgnore]
        public IEnumerable<IInvoiceItem> InvoiceItems => EstimateItem;
        [JsonIgnore]
        public IEnumerable<IReminderItem> ReminderItems => EstimateItem;
        public string InWords { get; set; }
        public decimal? Vat { get; set; }
        public string Prefix { get; set; } /*Added by Poongodi on 16/06/2017*/
        public string InvoiceSeries { get; set; }
        public string UserName { get; set; }

        public string CardNo { get; }
        public  DateTime? CardDate { get; }
        public string ChequeNo { get; }
        public DateTime? ChequeDate { get; }
        public DateTime? SalesCreatedAt { get; set; }
        //Newly added by Mani for Print Footer on 28-02-2017
        public string PrintFooterNote { get; set; }
        public bool IsBold { get; set; }
        public decimal? NetAmount { get; }
        public decimal? RoundoffNetAmount { get; }
        public List<SalesReturnItem> SalesReturnItem { get; set; }
        public SalesReturn SalesReturn { get; set; }
        public bool IsReturnsOnly { get; set; }
        public decimal? GrandGstTotalValue { get; }
        public decimal? IgstTotalValue { get; }
        public decimal? CgstTotalValue { get; }
        public decimal? SgstTotalValue { get; }
        public int? TaxRefNo { get; set; }
        public decimal? TotalDiscount { get; set; }
        //Added by Bikas on 15-05-18
        public decimal? Quantity { get; set; }
        public decimal? TotalMRPValue { get; set; }
        public string AmountinWords { get; set; }
        public  decimal? SavingAmount { get; set; }
        public decimal? ReturnAmount { get; set; }

        //Added by Sarubala on 08-11-17
        public List<SalesPayment> SalesPayments { get; set; }
    }
}
