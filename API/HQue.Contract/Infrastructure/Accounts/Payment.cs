﻿using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using System;

namespace HQue.Contract.Infrastructure.Accounts
{
    public sealed class Payment : BasePayment
    {
        public Payment()
        {
            Credit = 0;
            Debit = 0;
            VendorPurchase = new VendorPurchase();
            PaymentList = new List<Payment>();
            Vendor = new Vendor();
        }

        public List<Payment> PaymentList { get; set; }

        public VendorPurchase VendorPurchase { get; set; }

        public Vendor Vendor { get; set; }
 

        public decimal? Balance { get; set; }
        public decimal? PaymentAmount { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Payabledays {get; set;}
        public DateTime? CreditDate { get; set; }
        public string select { get; set; }
        public string select1 { get; set; }
        public string Mobile { get; set; }
        public DateTime? CreditExpireDate { get; set; }

        public int? Age { get; set; }
    }

    public class FinYearMaster : BaseFinYearMaster
    {

        public FinYearMaster()
        {
            //FinYearResetStatus = new List<FinYearResetStatus>();
            Account1 = new Base.BaseAccount();
        }
           
        //public List<FinYearResetStatus> FinYearResetStatus { get; set; }
        public int? FinYearStatus { get; set; }
        public int? IsBillNumberReset { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public bool SaveFailed { get; set; }
        public string ErrorMessage { get; set; }

        public BaseAccount Account1 { get; set; }   
    }

    public class FinYearResetStatus :BaseFinYearResetStatus
    {

    }

}
