﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.Infrastructure.Accounts
{
    public class Voucher : BaseVoucher
    {
        public string ReturnNo { get; set; }
        public string InvoiceSeries { get; set; }
        public DateTime? ReturnDate { get; set; }
        public decimal? AdjustedAmount { get; set; }
        public string TransactionId { get; set; }
    }
}
