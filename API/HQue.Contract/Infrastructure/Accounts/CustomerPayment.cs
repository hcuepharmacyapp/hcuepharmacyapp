﻿using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using System;

namespace HQue.Contract.Infrastructure.Accounts
{
    public sealed class CustomerPayment : BaseCustomerPayment
    {
        public CustomerPayment()
        {
            Credit = 0;
            Debit = 0;
            Sales = new Sales();
            CustomerPaymentList = new List<CustomerPayment>();            
        }
        public List<CustomerPayment> CustomerPaymentList { get; set; }
        public Sales Sales { get; set; }
        //public decimal? Balance { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public decimal? PaymentAmount { get; set; }
        public string CustomerName { get; set; }
        public string CustomerId { get; set; }
        public string CustomerMobile { get; set; }
        public int CancelStatus { get; set; }
        public decimal? CustomerDebit { get; set; }
        public decimal? CustomerCredit { get; set; }
        public decimal? SalesCredit { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceSeries { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int? Age { get; set; }
        public decimal? BalanceAmount { get; set; }
        public int? CustomerStatus { get; set; }
    }
}
