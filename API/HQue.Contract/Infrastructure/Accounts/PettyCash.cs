﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.Contract.Infrastructure.Accounts
{
    public class PettyCash : BasePettyCash
    {
        public PettyCash()
        {
            HQueUser = new HQueUser();
        }

        public HQueUser HQueUser { get; set; }
    }
}
