﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.Contract.Infrastructure.Accounts
{
    public class PettyCashDtl: BasePettyCashDtl
    {

        public PettyCashDtl()
        {
            HQueUser = new HQueUser();
        }

        public HQueUser HQueUser { get; set; }
    }
}
