﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.Contract.Infrastructure.Accounts
{
    public class PettyCashHdr : BasePettyCashHdr
    {

        public PettyCashHdr()
        {
            HQueUser = new HQueUser();
        }

        public HQueUser HQueUser { get; set; }
    }
}
