﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Inventory;
using System;

namespace HQue.Contract.Infrastructure.Audits
{
    public class VendorPurchaseItemAudit : BaseVendorPurchaseItemAudit
    {
        public VendorPurchaseItemAudit()
        {
            ProductStock = new ProductStock();
        }

        public ProductStock ProductStock { get; set; }

        public string UpdatedByUser { get; set;  }
        //added by nandhini for csv download
        public string ReportCreatedAt { get; set; }
        public string ReportGRNDate { get; set; }
        public string ReportInvoiceDate { get; set; }
        public string ReportExpireDate { get; set; }
        //end
        




        public decimal? Total
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;
                if (!(Discount.HasValue && Discount.HasValue))
                    return 0;

                //return PackagePurchasePrice * PackageQty;
                decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);
                //decimal? total = PurchasePrice * (PackageQty - FreeQty);
                return total - (total * Discount / 100);
                //return PurchasePrice * Quantity;
            }

            //get
            //{
            //    if (!Discount.HasValue)
            //        return 0;

            //    if (!(Quantity.HasValue && PackagePurchasePrice.HasValue))
            //        return 0;
            //    return (PackagePurchasePrice * (Quantity-FreeQty))-((Discount / 100) * (PackagePurchasePrice * Quantity));
            //}
        }

        public decimal? PackagePriceAfterTax
        {
            get
            {
                if (!(Total.HasValue && ProductStock.VAT.HasValue))
                    return 0;

                if (PackageQty > 0)
                {
                    if (ProductStock.CST.HasValue && ProductStock.CST > 0)
                    return (Total + (Total * (ProductStock.CST / 100))) / PackageQty;
                else if (ProductStock.VAT.HasValue && ProductStock.VAT > 0)
                    return (Total + (Total * (ProductStock.VAT / 100))) / PackageQty;
                else
                    // return 0;
                    return (Total) / PackageQty;
                }
                else
                {
                    return Total;
                }
            }
        }

        // Added Gavaskar 01-03-2017
        public string Select { get; set; }
        public string Select1 { get; set; }
        public string Values { get; set; }
        public string SelectValue { get; set; }
        public int PurchaseItemAuditCount { get; set; }
        public DateTime GRNDate { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public string GRNNo { get; set; }
        public string VendorName { get; set; }
        public string ModifiedTime { get; set; }
        public string BillSeries { get; set; }
        public string InstanceName { get; set; }
    }
}
