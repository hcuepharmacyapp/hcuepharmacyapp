﻿using System;

namespace HQue.Contract.Infrastructure.Reminder
{
    [Flags]
    public enum UserReminderType
    {
        None = 0,
        Daily = 1 << 0,
        Weekly = 1 << 1,
        Monthly = 1 << 2,
        Yearly = 1 << 3,
        Once = 1 << 4,
    }
}