﻿using System;
using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Reminder
{
    public interface IReminderItem : IContract
    {
        int? ReminderFrequency { get; set; }

        DateTime? ReminderDate { get; set; }

        UserReminderType SetReminder { get; }

        string ProductStockId { get; }

        string ProductName { get; }

        decimal? Quantity { get; }
        decimal? ReturnedQty { get; }
    }
}