﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Reminder
{
    public class ReminderRemark : BaseReminderRemark
    {
        public ReminderRemark()
        {
            UserReminder = new UserReminder();
        }

        public UserReminder UserReminder { get; set; }
    }
}
