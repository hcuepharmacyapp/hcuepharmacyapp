﻿using System;
using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Reminder
{
    public class ReminderProduct : BaseReminderProduct
    {
        public string ReminderName => ReminderFrequency.HasValue ? ((UserReminderType) ReminderFrequency.Value).ToString() : string.Empty;
    }
}