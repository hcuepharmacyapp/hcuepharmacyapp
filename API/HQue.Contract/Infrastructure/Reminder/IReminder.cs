﻿using System.Collections.Generic;
using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Reminder
{
    public interface IReminder : IContract
    {
        string Mobile { get; set; }
        string Name { get; set; }
        string Gender { get; set; }
        int? Age { get; set; }
        string DoctorName { get; set; }
        IEnumerable<IReminderItem> ReminderItems { get; }
        int ReferenceFrom { get; }
        
    }
}