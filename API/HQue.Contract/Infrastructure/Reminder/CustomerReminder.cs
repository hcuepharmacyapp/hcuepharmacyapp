﻿using System;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Reminder
{
    public sealed class CustomerReminder : UserReminder
    {
        public CustomerReminder()
        {
            ReminderProduct = new List<ReminderProduct>();
            ReminderType = Customer;
        }

        public IEnumerable<ReminderProduct> ReminderProduct { get; set; }

        public int MissedType { get; set; }

        public new CustomerReminder Clone(DateTime reminderDate)
        {
            var clone = new CustomerReminder
            {
                Id = Id,
                ReminderDate = reminderDate,
                HqueUserId = HqueUserId,
                CustomerName = CustomerName,
                ReminderPhone = ReminderPhone,
                CustomerAge = CustomerAge,
                CustomerGender = CustomerGender,
                DoctorName = DoctorName,
                StartDate = StartDate,
                ReminderTime = ReminderTime,
                EndDate = EndDate,
                ReminderFrequency = ReminderFrequency,
                ReminderStatus = ReminderStatus,
                ReminderType = ReminderType,
                Description = Description,
                Remarks = Remarks,
            };

            return clone;
        }
    }
}