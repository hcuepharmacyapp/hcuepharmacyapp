﻿using System;
using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Reminder
{
    public class UserReminder : BaseUserReminder
    {
        public UserReminder()
        {
            ReminderType = Personal;
        }

        public const int Customer = 1;
        private const int Personal = 2;

        public const int SalesReminder = 1;
        public const int EstimateReminder = 2;

        public DateTime ReminderDate { get; set; }

        public string ReminderTimeFormat => ReminderTime.HasValue ? ReminderTime.Value.ToString("##.00") : string.Empty;

        public UserReminderType UserReminderFrequencyType
            => ReminderFrequency.HasValue ? (UserReminderType) ReminderFrequency.Value : UserReminderType.None;

        public UserReminder Clone(DateTime reminderDate)
        {
            var clone = new UserReminder
            {
                Id = Id,
                ReminderDate = reminderDate,
                HqueUserId = HqueUserId,
                CustomerName = CustomerName,
                ReminderPhone = ReminderPhone,
                CustomerAge = CustomerAge,
                CustomerGender = CustomerGender,
                DoctorName = DoctorName,
                StartDate = StartDate,
                ReminderTime = ReminderTime,
                EndDate = EndDate,
                ReminderFrequency = ReminderFrequency,
                ReminderStatus = ReminderStatus,
                ReminderType = ReminderType,
                Description = Description,
                Remarks = Remarks,
            };

            return clone;
        }
    }
}
