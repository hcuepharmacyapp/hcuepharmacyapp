﻿using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Dashboard
{
    public class BarGraph
    {
        public BarGraph()
        {
            Labels = new List<string>();
            Data = new List<decimal>();
        }

        public List<string> Labels { get; set; }

        public List<decimal> Data { get; set; }
    }
}
