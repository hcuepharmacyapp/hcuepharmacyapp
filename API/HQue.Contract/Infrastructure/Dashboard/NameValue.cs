﻿using System;

namespace HQue.Contract.Infrastructure.Dashboard
{
    public class NameValue
    {
        public string Name { get; set; }

        public string Value { get; set; }
        // Newly added Gavaskar 27-10-2016 Start
        public decimal Amount { get; set; }

        public decimal TotalDiscount { get; set; }

        public decimal SalesReturnAmount { get; set; }
        public decimal SalesNetAmount { get; set; }
        public decimal SalesGrossAmount { get; set; }

        public decimal PurchaseReturnAmount { get; set; }
        public decimal PurchaseNetAmount { get; set; }
        public decimal PurchaseGrossAmount { get; set; }

        public decimal CurrentStockAmount { get; set; }

       public decimal AveragePurchase { get; set; }

        public decimal AverageSales { get; set; }
       

        public decimal TotalCash { get; set; }
        public decimal? TotalCard { get; set; }
        public decimal TotalCredit { get; set; }
        public decimal TotalSalesCount { get; set; }
        public decimal TotalSales { get; set; }
        public string PharmacyName { get; set; }
        public string OwnerName { get; set; }
        public string Branch { get; set; }
        public decimal SalesNetMargin { get; set; }
        public decimal PurchaseNetMargin { get; set; }
        public decimal Profit { get; set; }
        public string PatientTypeName { get; set; }
        public string PatientVisitCount { get; set; }
        public decimal BouncedQty { get; set; }

        // Newly added Gavaskar 27-10-2016 End

        // Newly added Gavaskar 05-01-2017 Start
        public string Edge { get; set; }
        public string Paid { get; set; }
        public string Trial { get; set; }
        public string Offline { get; set; }
        public string PatientOverAll { get; set; }
        public string PatientYesterday { get; set; }
        public string PatientAccepted { get; set; }
        public string PatientMissed { get; set; }
        public string DoctorOverAll { get; set; }
        public string DoctorYesterday { get; set; }
        public string DoctorAccepted { get; set; }
        public string DoctorMissed { get; set; }
        public int TotalSaleCount { get; set; }
        public int TotalPurchaseCount { get; set; }
        public string GroupName { get; set; }
        public string PharmaName { get; set; }
        public int TOTALSALES { get; set; }
        public int TOTALPURCHASECOUNT { get; set; }
        public decimal SALESNET { get; set; }
        public decimal PURCHASENET { get; set; }
        public string City { get; set; }
        public string Area { get; set; }

        // Newly added Gavaskar 05-01-2017 End

        //Newly added by Manivannan on 24-02-2017 starts
        public int AvgSold { get; set; }
        public int Stock { get; set; }

        public string LeadDate { get; set; }

        public int Count { get; set; }

        public DateTime ExpireDate { get; set; }
        public decimal FinancialYear { get; set; }
        //Newly added by Manivannan on 24-02-2017 ends
        // Added by Gavaskar 05-10-2017 Daily mail all Pharmacy Start
        public string OwnerId { get; set; }
        public string EmailId { get; set; }

        // Added by Gavaskar 05-10-2017 Daily mail all Pharmacy End

    }

    public class StockNameValue
    {


        public string ProductName { get; set; }
        public decimal AvailableStock { get; set; }
        public int  Reorderdays { get; set; }
        public decimal ReOrderQuantity { get; set; }
        public decimal Totalsold { get; set; }
        public decimal AverageSold { get; set; }
        public string LastSaleDate { get; set; }
        public decimal Age { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual string ExpireDate { get; set; }
        public string GoodsRcvNo { get; set; }
        public virtual string InvoiceDate { get; set; }
        public string VendorName { get; set; }

    }
}
