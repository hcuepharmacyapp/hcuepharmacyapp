﻿using System;

namespace HQue.Contract.Infrastructure.Dashboard
{
    public class getAllPharmaItem
    {
        public string PharmaId { get; set; }
        public string PharmaName { get; set; }
        public string PharmaArea { get; set; }
        public string City { get; set; }
        public int SalesInvoiceCount { get; set; }
        public decimal SalesTotalValue { get; set; }
        public int PurchaseInvoiceCount { get; set; }
        public decimal PurchaseTotalValue { get; set; }
        public int Saleslastmonthcount { get; set; }
        public decimal SaleslastmonthTotalValue { get; set; }
        public int PurchasetotalmonthCount { get; set; }
        public decimal PurchaseTotalmonthValue { get; set; }
        public int RegisterType { get; set; }

        //Edge Details
        public int count { get; set; }
        public string BDOName { get; set; }
        public string RelationshipManagerName { get; set; }

        public string RegisterTypeText { get; set; }

        //public string WhetherRMStillInRolesorNot { get; set; }
        public string CurrentRM { get; set; }
        public string Owner1Name { get; set; }
        public string Owner1number { get; set; }
        public string Owner1Email { get; set; }
        public string Owner2Name { get; set; }
        public string Owner2number { get; set; }
        public string Owner2Email { get; set; }
        //public int TrialorPaidID { get; set; }


        public decimal? InvoiceValue { get; set; }
        public decimal? BillAmount { get; set; }
        //public DateTime? InvoiceDate { get; set; }
        //public DateTime? LastDateLoggedin { get; set; }

        public string InvoiceDate { get; set; }
        public string LastDateLoggedin { get; set; }
        public string FullAmountPaid { get; set; }
        public string Reasonforfullamountnotpaid { get; set; }
        
        //public int NumberofInvoicesfortheday { get; set; }

        public class pharmaDetailsList
        {
            public int count { get; set; }
            public string PharmaId { get; set; }
            public string PharmaName { get; set; }
            public string PharmaArea { get; set; }
            public string City { get; set; }
            public int RegisterType { get; set; }
        }
        public class salesBasedOnDateList
        {

            public string PharmaId { get; set; }
            public string PharmaName { get; set; }
            public int SalesInvoiceCount { get; set; }
            public decimal? salesAmountBeforeDisocunt { set; get; }
            public decimal? salesDisocuntedAmount { set; get; }
            public decimal? SalesTotalValue { get; set; }
        }
        public class purchaseBasedOnDateList
        {

            public string PharmaId { get; set; }
            public string PharmaName { get; set; }
            public int PurchaseInvoiceCount { get; set; }
            public decimal? PurchaseAmountBeforeDiscount { set; get; }
            public decimal? PurchaseDisocuntedAmount { set; get; }
            public decimal? PurchaseTotalValue { get; set; }
        }
        public class salesLastMonthList
        {
            public string PharmaId { get; set; }
            public string PharmaName { get; set; }
            public int Saleslastmonthcount { get; set; }
            public decimal? salesLastmonthAmountBeforeDiscount { set; get; }
            public decimal? salesLastMonthDiscountedAmount { set; get; }
            public decimal? SaleslastmonthTotalValue { get; set; }
        }
        public class purchaseLastMonthList
        {
            public string PharmaId { get; set; }
            public string PharmaName { get; set; }
            public int purchasetotalmonthCount { get; set; }
            public decimal? purchaseLastMonthBeforeDisocunt { set; get; }
            public decimal? pucrhaseLastMonthDiscountedAmount { set; get; }
            public decimal? PurchaseTotalmonthValue { get; set; }
        }

        public class Edge
        {
            public string PharmaId { get; set; }

            public string BdoName { get; set; }

            public string RmName { get; set; }

            public string ContactName { get; set; }

            public string ContactMobile { get; set; }

            public string ContactEmail { get; set; }

            public string SecondaryName { get; set; }

            public string SecondaryMobile { get; set; }

            public string SecondaryEmail { get; set; }

            public int RegisterType { get; set; }

            public string RegisterTypeText
            {
                get
                {
                    if (RegisterType == 1)
                        return "Prospect";
                    else if (RegisterType == 2)
                        return "Live";
                    else if (RegisterType == 3)
                        return "Test";
                    else if (RegisterType == 4)
                        return "Trial";
                    else return "";
                }
            }

            public DateTime PaidDate { get; set; }

            public DateTime LastLoggedOn { get; set; }

            public string PaidDateText {
                get {
                    if (PaidDate == DateTime.MinValue)
                        return "";
                    else
                        return PaidDate.ToString();
                }
            }

            public string LastLoggedOnText
            {
                get
                {
                    if (LastLoggedOn == DateTime.MinValue)
                        return "";
                    else
                        return LastLoggedOn.ToString();
                }
            }

            public decimal? Cost { get; set; }
            public int? Quantity { get; set; }
            public decimal? Discount { get; set; }
            public bool DiscountType { get; set; }
            public decimal? Tax { get; set; }

            public decimal? Paid { get; set; }

            public decimal? InvoiceValue
            {
                get
                {
                    decimal? netAmount = Cost * Quantity;
                    decimal? discountAmt = DiscountType == true ? Discount : netAmount * (Discount / 100);
                    decimal? amount = netAmount - discountAmt;

                    return amount + (amount * (Tax / 100));
                }
            }
            
            //public string IsFullPaid { get; set; }
            public string IsFullPaid
            {
                get
                {
                    if (Paid < InvoiceValue)
                        return "No";
                    else
                        return "Yes";
                }
            }

            public string BalanceReason { get; set; }

            public string BalanceReasonText
            {
                get
                {
                    if (Paid < InvoiceValue)
                        return BalanceReason;
                    else
                        return "-";
                }
            }

        }


    }
}
