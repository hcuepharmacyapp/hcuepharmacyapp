﻿namespace HQue.Contract.Infrastructure.Common
{
    public class Either<T>
    {
        public bool IsSuccess { get; set; }

        public T Data { get; set; }
    }
}
