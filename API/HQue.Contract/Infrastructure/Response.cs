﻿namespace HQue.Contract.Infrastructure
{
    public class Response
    {
        public int errorStatus { get; set; }
        public string errorMessage { get; set; }
    }
}
