﻿using HQue.Contract.Base;
using System;

namespace HQue.Contract.Infrastructure.Replication
{
    public class SyncRequestEntity
    {
        public SyncRequestEntity()
        {
            InstanceClientData = new InstanceClient();
        }

        public string TransactionId { get; set; }

        public string EntityId { get; set; }

        public string TableName { get; set; }

        public int Action { get; set; }

        public DateTime ActionTime { get; set; }

        public bool UseId { get; set; }

        public BaseAccount AccountData { get; set; }

        public BaseDoctor DoctorData { get; set; }

        public BaseHQueUser HQueUserData { get; set; }

        public BaseInstance InstanceData { get; set; }

        public BaseInstanceClient InstanceClientData { get; set; }

        public BaseLeads LeadsData { get; set; }

        public BaseLeadsProduct LeadsProductData { get; set; }

        public BasePatient PatientData { get; set; }

        public BasePayment PaymentData { get; set; }

        public BaseProduct ProductData { get; set; }

        public BaseProductStock ProductStockData { get; set; }

        public ReplicationEntity ReplicationEntityData { get; set; }

        public BaseSales SalesData { get; set; }

        public BaseSalesItem SalesItemData { get; set; }

        public BaseSalesReturn SalesReturnData { get; set; }

        public BaseSalesReturnItem SalesReturnItemData { get; set; }

        public BaseUserAccess UserAccessData { get; set; }

        public BaseVendor VendorData { get; set; }

        public BaseVendorOrder VendorOrderData { get; set; }

        public BaseVendorOrderItem VendorOrderItemData { get; set; }

        public BaseVendorPurchase VendorPurchaseData { get; set; }

        public BaseVendorPurchaseItem VendorPurchaseItemData { get; set; }

        public BaseVendorReturn VendorReturnData { get; set; }

        public BaseVendorReturnItem VendorReturnItemData { get; set; }
    }
}
