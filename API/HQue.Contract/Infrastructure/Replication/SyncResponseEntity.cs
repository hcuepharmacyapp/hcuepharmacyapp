﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Replication
{
    public class SyncResponseEntity
    {
        public BaseAccount[] AccountData { get; set; }

        public BaseDoctor[] DoctorData { get; set; }

        public BaseHQueUser[] HQueUserData { get; set; }

        public BaseInstance[] InstanceData { get; set; }

        public BaseInstanceClient[] InstanceClientData { get; set; }

        public BaseLeads[] LeadsData { get; set; }

        public BaseLeadsProduct[] LeadsProductData { get; set; }

        public BasePatient[] PatientData { get; set; }

        public BasePayment[] PaymentData { get; set; }

        public BaseProduct[] ProductData { get; set; }

        public BaseProductStock[] ProductStockData { get; set; }

        public ReplicationEntity[] ReplicationEntityData { get; set; }

        public BaseSales[] SalesData { get; set; }

        public BaseSalesItem[] SalesItemData { get; set; }

        public BaseSalesReturn[] SalesReturnData { get; set; }

        public BaseSalesReturnItem[] SalesReturnItemData { get; set; }

        public BaseUserAccess[] UserAccessData { get; set; }

        public BaseVendor[] VendorData { get; set; }

        public BaseVendorOrder[] VendorOrderData { get; set; }

        public BaseVendorOrderItem[] VendorOrderItemData { get; set; }

        public BaseVendorPurchase[] VendorPurchaseData { get; set; }

        public BaseVendorPurchaseItem[] VendorPurchaseItemData { get; set; }

        public BaseVendorReturn[] VendorReturnData { get; set; }

        public BaseVendorReturnItem[] VendorReturnItemData { get; set; }
    }
}
