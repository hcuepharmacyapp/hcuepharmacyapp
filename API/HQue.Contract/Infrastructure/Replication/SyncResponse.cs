﻿using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Replication
{
    public class SyncResponse
    {
        public SyncResponse()
        {
            Response =  new List<SyncResponseEntity>();
        }

        public List<SyncResponseEntity> Response { get; set; }
    }
}
