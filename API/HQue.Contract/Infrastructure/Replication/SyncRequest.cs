﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Replication
{

    public class SyncRequest
    {
        public SyncRequestEntity[] Request { get; set; }

        public BaseInstanceClient InstanceClientData { get; set; }
    }
}
