﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Replication
{
    public class ReplicationEntity : BaseReplicationData
    {
        public bool MarkedForDeletion { get; set; }

        public bool ModifiedByClient { get; set; }
    }
}
