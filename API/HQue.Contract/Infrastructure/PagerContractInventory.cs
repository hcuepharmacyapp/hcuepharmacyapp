﻿using System.Collections.Generic;

namespace HQue.Contract.Infrastructure
{
    public class PagerContractInventory<T>
    {
        public int NoOfRows { get; set; }
        public int Available { get; set; }
        public int ZeroQty { get; set; }
        public int Expiry { get; set; }
        public int Inactive { get; set; }
        public int Reorder { get; set; }
        public int OnlyAvailable { get; set; }

        public IEnumerable<T> List { get; set; }
    }
}
