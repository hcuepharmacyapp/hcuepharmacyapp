﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.UserAccount;
using System;

namespace HQue.Contract.Infrastructure.Communication
{
    public class CommunicationLog : BaseCommunicationLog
    {
        public CommunicationLog()
        {
            HQueUser = new HQueUser();
        }
        public HQueUser HQueUser { get; set; }

        public Int32? TotalSms { get; set; }
    }
}
