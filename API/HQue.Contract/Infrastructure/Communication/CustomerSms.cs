﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Setup;


namespace HQue.Contract.Infrastructure.Communication
{
    public class CustomerSms
    {
        public string SmsContent { get; set; }

        public List<Sales> Customers { get; set; }
    }
}
