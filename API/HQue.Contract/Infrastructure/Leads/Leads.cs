﻿using HQue.Contract.Base;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Leads
{
    public class Leads:BaseLeads
    {
        public Leads()
        {
            LeadsProduct = new List<LeadsProduct>();
        }

        public string PharmaExternalId { get; set; }

        public IEnumerable<LeadsProduct> LeadsProduct { get; set; }
    }
}
