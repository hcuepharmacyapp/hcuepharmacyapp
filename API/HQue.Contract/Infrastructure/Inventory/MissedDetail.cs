﻿

using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class MissedDetail
    {
        public MissedDetail()
        {
            ItemList = new List<MissedItemList>();
        }
          
        public List<MissedItemList> ItemList { get; set; }
   
    }

    public class MissedItemList
    {
        public virtual string InvoiceNo { get; set; }
        public virtual string Id { get; set; }
    }
    //Added by Poongodi on 20/09/2017
    public class MissedItems
    {
        public string InstanceId { get; set; }
        public string AccountId { get; set; }
        public string Type { get; set; }


    }
}