﻿using System;
using HQue.Contract.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Master;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SupplierFilter
    {
        public SupplierFilter()
        {
        }
        public string AccountId { get; set; }
        public string InstanceId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public string select { get; set; }
        public string select1 { get; set; }
        public string Mobile { get; set; }
        public string VendorId { get; set; }
        public string supplier { get; set; }
    }
}
