﻿using HQue.Contract.Base;
using Newtonsoft.Json;
using HQue.Contract.Infrastructure.Master;
namespace HQue.Contract.Infrastructure.Inventory
{
    public class VendorPurchaseItem : BaseVendorPurchaseItem
    {
        public VendorPurchaseItem()
        {
            ProductStock = new ProductStock();
            VendorPurchase = new VendorPurchase();
            BuyPurchase = new VendorPurchase();
            Vendor = new Vendor();
        }
        public decimal? PurchaseQuantity { get; set; }
        public decimal? POValue { get; set; }
        public decimal? VATValue { get; set; }
        public string Category { get; set; }
        public string InstanceName { get; set; }
        //added by nandhini for csv 24.10.17

        public string ReportInvoiceDate { get; set; }

        public decimal? Credit { get; set; }
        public decimal? ActualPaymentTotal { get; set; }
        public decimal? TotalPurchaseValue { get; set; }
        //end
        public decimal? Total
        {
            get
            {
                //Calculation changed based on purchase formula
                //if (!(Quantity.HasValue && PurchasePrice.HasValue))
                //    return 0;
                //if (!(Discount.HasValue && Discount.HasValue))
                //    return 0;
                //decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);
                //return total - (total * Discount / 100);
                return (PurchasePrice * Quantity) - (GstValue > 0 ? GstValue : VatValue);
            }
        }
        //The below calculation not used anywhere across application
        //public decimal? TotalWithDiscount
        //{
        //    get
        //    {
        //        if (!(Quantity.HasValue && PurchasePrice.HasValue))
        //            return 0;
        //        if (!(Discount.HasValue && Discount.HasValue))
        //            return 0;
        //        decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);
        //        return (Discount / 100) * total;
        //    }
        //}
        //public decimal? SingleDiscount
        //{
        //    get
        //    {
        //        if (!(Quantity.HasValue && PurchasePrice.HasValue))
        //            return 0;
        //        if (!(Discount.HasValue && Discount.HasValue))
        //            return 0;
        //        decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);
        //        return (total * Discount / 100);
        //    }
        //}
        //public decimal? TotalWithVat
        //{
        //    get
        //    {
        //        if (!(Quantity.HasValue && PurchasePrice.HasValue))
        //            return 0;
        //        decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);
        //        return total + VatPrice;
        //    }
        //}
        public decimal? VatPrice
        {
            get
            {
                //if (!(Total.HasValue && ProductStock.GstTotal.HasValue) && TaxRefNo == 1)
                //    return 0;
                //else if (!(Total.HasValue && ProductStock.VAT.HasValue))
                //    return 0;
                //if (TaxRefNo == 1)
                //    return (Total.HasValue ? Total : 0) * (ProductStock.GstTotal / 100);
                //else if (Total.HasValue && ProductStock.CST > 0)
                //    return Total * (ProductStock.CST / 100);
                //else if (Total.HasValue && ProductStock.VAT > 0)
                //    return Total * (ProductStock.VAT / 100);
                //else
                //    return Total * (ProductStock.VAT / 100);
                if (GstValue > 0)
                    return GstValue;
                else
                    return VatValue;
            }
        }
        //public decimal? PackagePriceAfterTax
        //{
        //    get
        //    {
        //        if (!(Total.HasValue && ProductStock.GstTotal.HasValue) && TaxRefNo == 1)
        //            return 0;
        //        else if (!(Total.HasValue && ProductStock.VAT.HasValue))
        //            return 0;
        //        if (PackageQty > 0)
        //        {
        //            if (TaxRefNo == 1)
        //                return (Total + (Total * (ProductStock.GstTotal / 100))) / PackageQty;
        //            else if (ProductStock.CST.HasValue && ProductStock.CST > 0)
        //                return (Total + (Total * (ProductStock.CST / 100))) / PackageQty;
        //            else if (ProductStock.VAT.HasValue && ProductStock.VAT > 0)
        //                return (Total + (Total * (ProductStock.VAT / 100))) / PackageQty;
        //            else
        //                return (Total) / PackageQty;
        //        }
        //        else
        //        {
        //            return Total;
        //        }
        //    }
        //}
        public ProductStock ProductStock { get; set; }
        [JsonIgnore]
        public VendorPurchase VendorPurchase { get; set; }
        public VendorPurchase BuyPurchase { get; set; }
        public Vendor Vendor { get; set; }
        public decimal? ActualQty { get; set; }
        public decimal? QtyChange => Quantity - ActualQty;
        public decimal? ReportTotal { get; set; }
        public string Action { get; set; }
        public string UOM { get; set; }
        public decimal? MRPValue { get; set; }
        public decimal? MarginValue { get; set; }
        public decimal? MarginPercent { get; set; }
        public string VendorOrderId { get; set; }
        public bool DisableDelete { get; set; } = false;
        public bool isDeleted { get; set; } = false;
        public string UpdatedByUser { get; set; }
        public string RackNo { get; set; }
        public decimal? Debit { get; set; }
        public string vpiReturnid { get; set; }
        public string TransferId { get; set; }
        public string VendorOrderItemId { get; set; }
        public bool? VendorReturn { get; set; }
        public string vendorReturnId { get; set; }
        public string reason { get; set; }
        public int? TaxRefNo { get; set; }
        public string AlternatePackageSize { get; set; }
        public int? ProductCount { get; set; }

        public decimal? IndividualSchemeValue { get; set; }

        public bool? IsSchemeDiscountValue { get; set; }

        /*GST Rleated fields  Added by Poongodi on 04/07/2017*/
        //public decimal? CGSTAmount
        //{
        //    get
        //    {
        //        if (!(Quantity.HasValue && PurchasePrice.HasValue && Cgst.HasValue && GstTotal.HasValue))
        //        {
        //            return 0;
        //        }
        //        return (((PurchasePrice * Quantity) - ((PurchasePrice * Quantity) * GstTotal / (100 + GstTotal))) * Cgst / 100);
        //    }
        //}
        //public decimal? SGSTAmount
        //{
        //    get
        //    {
        //        if (!(Quantity.HasValue && PurchasePrice.HasValue && Sgst.HasValue && GstTotal.HasValue))
        //        {
        //            return 0;
        //        }
        //        return (((PurchasePrice * Quantity) - ((PurchasePrice * Quantity) * GstTotal / (100 + GstTotal))) * Sgst / 100);
        //    }

        //}
        //public decimal? IGSTAmount
        //{
        //    get
        //    {
        //        if (!(Quantity.HasValue && PurchasePrice.HasValue & Igst.HasValue))
        //        {
        //            return 0;
        //        }
        //        return (PurchasePrice * Quantity) - ((PurchasePrice * Quantity) * 100 / (100 + Igst));
        //    }
        //}
        //public decimal? GSTAmount
        //{
        //    get
        //    {
        //        if (!(Quantity.HasValue && PurchasePrice.HasValue & GstTotal.HasValue))
        //        {
        //            return 0;
        //        }
        //        return (PurchasePrice * Quantity) - ((PurchasePrice * Quantity) * 100 / (100 + GstTotal));
        //    }
        //}
    }
}
