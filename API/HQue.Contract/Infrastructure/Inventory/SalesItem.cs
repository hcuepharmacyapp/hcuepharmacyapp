﻿using System;
using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesItem : BaseSalesItem, IReminderItem, IInvoiceItem
    {
        public SalesItem()
        {
            Sales = new Sales();
            ProductStock = new ProductStock();
        }
        public string TinNo { get; set; }
        public string Category { get; set; }
        public decimal? SaleValue { get; set; }
        public decimal? VATValue { get; set; }
        /*The following objects added for SalesConsolidated VAT Report by Poongodi on 02/03/2017*/
        public decimal? VAT5_Value { get; set; }
        public decimal? VAT145_Value { get; set; }
        public decimal? VAT5_SaleValue { get; set; }
        //added by nandhini for csv
        public string ReportExpireDate { get; set; }
        public string ReportCreatedAt { get; set; }
        public decimal? VAT145_SaleValue { get; set; }
        public decimal? VAT5_TotalValue { get; set; }
        public decimal? VAT145_TotalValue { get; set; }
        public decimal? VATExcempted_SaleValue { get; set; }       
        public decimal? GSTValue { get; set; }        
        public ProductStock ProductStock { get; set; }
        public bool? ShowSalesItemHistory { get; set; } //Added by Annadurai on 07032017
        //Added by bala for Brach wise stock display on 05/June/2017
        public List<ProductStock> lstProductStock { get; set; }
        public decimal? Total
        {
            get
            {
                if ((Quantity.HasValue && SellingPrice > 0))
                {
                    return SellingPrice * Quantity;
                }

                if (!(Quantity.HasValue && ProductStock.SellingPrice.HasValue))
                    return 0;
                return ProductStock.SellingPrice * Quantity;

            }
        }

        public decimal? MRPTotal
        {
            get
            {
                if (!(Quantity.HasValue && ProductStock.MRP.HasValue))
                    return 0;
                return ProductStock.MRP * Quantity;

            }
        }

        //public decimal? ActualAmount
        //{
        //    get
        //    {
        //        if (!(Total.HasValue && ProductStock.VAT.HasValue))
        //            return 0;
        //        return (Total * 100) / (ProductStock.VAT + 100);
        //    }
        //}
        //public decimal? VatAmount
        //{
        //    get
        //    {
        //        if (!(Total.HasValue && ActualAmount.HasValue))
        //            return 0;
        //        return Total - ActualAmount;
        //    }
        //}

        //public decimal? DiscountAmount
        //{
        //    get
        //    {
        //        if (!(Total.HasValue && Discount.HasValue))
        //            return 0;
        //        return ((Discount / 100) * Total);
        //    }
        //}

        //public decimal? SalesReportTotal
        //{
        //    get
        //    {
        //        if ((Quantity.HasValue && SellingPrice > 0))
        //        {
        //            if ((Sales.Discount > 0))
        //                return (SellingPrice - (((SellingPrice * Sales.Discount) / 100) + (SellingPrice * Discount) / 100)) * Quantity;

        //            return (SellingPrice * Quantity) - DiscountAmount;
        //        }

        //        if (!(Quantity.HasValue && ProductStock.SellingPrice.HasValue))
        //            return 0;

        //        if ((Quantity.HasValue && ProductStock.SellingPrice.HasValue) && Sales.Discount > 0)
        //            return (ProductStock.SellingPrice - ((((ProductStock.SellingPrice * Sales.Discount) / 100) + (ProductStock.SellingPrice * Discount) / 100))) * Quantity;

        //        return (ProductStock.SellingPrice * Quantity) - DiscountAmount;

        //    }


        //    set { }
        //}
        //public decimal? SalesDiscountValue
        //{
        //    get
        //    {
        //        DiscountAmount = DiscountAmount != null ? DiscountAmount : 0;//Added by Saru on 06/06/2017
        //        if ((Quantity.HasValue && SellingPrice > 0))
        //        {
        //            if ((Sales.Discount > 0))
        //                return (((SellingPrice * Sales.Discount) / 100) * Quantity) + DiscountAmount;
        //            return 0;
        //        }

        //        if (!(Quantity.HasValue && ProductStock.SellingPrice.HasValue))
        //            return 0;

        //        if ((Quantity.HasValue && ProductStock.SellingPrice.HasValue) && Sales.Discount > 0)
        //            return (((ProductStock.SellingPrice * Sales.Discount) / 100) * Quantity) + DiscountAmount;

        //        return 0;
        //    }

        //    set { }
        //}

        //public decimal? ReportActualAmount
        //{
        //    get
        //    {
        //        if (!(SalesReportTotal.HasValue && ProductStock.VAT.HasValue))
        //            return 0;
        //        return (SalesReportTotal * 100) / (ProductStock.VAT + 100);
        //    }
        //}
        //public decimal? ReportVatAmount
        //{
        //    get
        //    {
        //        if (!(SalesReportTotal.HasValue && ReportActualAmount.HasValue))
        //            return 0;
        //        return SalesReportTotal - ReportActualAmount;
        //    }
        //}

        [JsonIgnore]
        public UserReminderType SetReminder => ReminderFrequency.HasValue ? (UserReminderType)ReminderFrequency.Value : UserReminderType.None;

        public string ProductName => ProductStock.Product.Name;

        public Sales Sales { get; set; }

        public decimal? ReportTotal { get; set; }
        public decimal? SalesProfit { get; set; }

        public string Action { get; set; }
        public string productId { get; set; }

        public string BatchNo { get; set; }
        public string ProductMasterID { get; set; }
        public string ProductMasterName { get; set; }
        public string Manufacturer { get; set; }
        public string Schedule { get; set; }
        public string GenericName { get; set; }
        public string Packing { get; set; }
        public bool? salesReturn { get; set; }
        public string salesReturnId { get; set; }

        public static implicit operator SalesItem(List<SalesItem> v)
        {
            throw new NotImplementedException();
        }
        public DateTime? ExpireDate { get; set; }
       
        public decimal? VAT { get; set; }
        public decimal? ReturnedQty { get; set; }

        //For Purchase Sales Report
        public decimal? SI_Selling_Amount { get; set; }
        public decimal? SellingPrice_Profit { get; set; }
        public decimal? MRP_Profit { get; set; }

        //Added by Sarubala on 10/05/2018 for custom template
        public decimal? MRPAfterDiscount { get; set; }

        public decimal? SellingPriceAfterDiscount { get; set; }

        public decimal? MRPValueAfterDiscount { get; set; }

        public decimal? SellingPriceValueAfterDiscount { get; set; }

        //public decimal? SalesItemDiscount // Added By San 08-06-2017
        //{
        //    get
        //    {
        //        if (!(Total.HasValue && Discount.HasValue))
        //            return 0;
        //        return ((Discount / 100) * Total);
        //    }

        //}
        //public decimal? SalesVatAmount
        //{
        //    get
        //    {
        //        if (!(Total.HasValue && ActualAmount.HasValue))
        //            return 0;
        //        return Total - ActualAmount;
        //    }
        //}
        //public decimal? SalesItemVatAmount
        //{
        //    get
        //    {
        //        if (!(Total.HasValue && VAT.HasValue))
        //            return 0;
        //        return ((Total - SalesItemDiscount) - (((Total - SalesItemDiscount) * 100) / (VAT + 100)));
        //    }
        //}

        //public decimal? MRPDicount
        //{
        //    get
        //    {
        //        if ((!(ProductStock.SellingPrice.HasValue) && !(ProductStock.MRP.HasValue))|| ProductStock.MRP == 0)
        //            return 0;
        //        return (((ProductStock.MRP - ProductStock.SellingPrice) / ProductStock.MRP) * 100);
        //    }
        //}

        //public decimal? SalesItemGstAmount
        //{
        //    get
        //    {
        //        if (!(Total.HasValue && GstTotal.HasValue))
        //            return 0;
        //        return ((Total - SalesItemDiscount) - (((Total - SalesItemDiscount) * 100) / (GstTotal + 100)));
        //    }
        //}
        public decimal? SellingPricePerStrip { get; set; }
        public decimal? MRPPerStrip { get; set; }
        public override decimal? Igst { get; set; }
        public decimal? IgstValue {
            get
            {
                //if (!(Total.HasValue && Igst.HasValue))
                //    return 0;
                //return ((Total - SalesItemDiscount) - (((Total - SalesItemDiscount) * 100) / (Igst + 100)));
                return GstAmount;
            }
            set { }
        }
        public override decimal? Cgst { get; set; }
        public decimal? CgstValue {
            get
            {
                //if (!(Total.HasValue && Cgst.HasValue))
                //    return 0;
                //return (((Total - SalesItemDiscount) - (((Total - SalesItemDiscount) * 100) / ((Cgst+ Sgst) + 100))))/2;
                return GstAmount / 2;
            }
            set { }
        }
        public override decimal? Sgst { get; set; }
        public decimal? SgstValue {
            get
            {
                //if (!(Total.HasValue && Sgst.HasValue))
                //    return 0;
                //return (((Total - SalesItemDiscount) - (((Total - SalesItemDiscount) * 100) / ((Sgst+ Cgst) + 100))))/2;
                return GstAmount / 2;
            }
            set { }
        }
        public override decimal? GstTotal { get; set; }
        public decimal? GstTotalValue { get; set; }
        public decimal? SalesDiscount { get; set; } // Added by Sarubala on 11-09-17
        public decimal? NetAmount { get; set; }
        // Added by Gavaskar 27-10-2017 Start 
        public int SalesOrderProductSelected { get; set; }
        public int SalesEstimateProductSelected { get; set; }
        public string  SalesOrderEstimateId { get; set; }
        public string SalesOrderEstimatePatientId { get; set; }
        public string SalesPatientId { get; set; }
        public decimal? OriginalQty { get; set; }

        //Added by Sarubala on 14-09-18
        public decimal? RedeemAmtPerItem { get; set; }        

        // Added by Gavaskar 27-10-2017 End
		
		public bool IsCreated { get; set; } // Added by Manivannan on 16-Nov-2017
    }
}
