﻿using System;
using System.Collections.Generic;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.UserAccount;

namespace HQue.Contract.Infrastructure.Inventory
{
    public interface IInvoice
    {
        string InvoiceNo { get; }
        DateTime? InvoiceDate { get; }
        string Name { get; }
        string Mobile { get; }
        string Address { get; }
        string DoctorName { get; }
        string DoctorMobile { get; }
        string PaymentType { get; set; } //Modified by Sarubala on 08-11-17
        Instance Instance { get; }
        HQueUser HQueUser { get; }
        IEnumerable<IInvoiceItem> InvoiceItems { get; }
        decimal? PurchasedTotal { get; }
        decimal? Quantity { get; set; }
        decimal? DiscountInValue { get; }
        //decimal? RoundOff { get; }
        //decimal? RoundoffNetAmount { get; }
        decimal? Vat { get; }
        decimal? Net { get; }
        Patient Patient { get; set; }
        string Pincode { get; set; }
        string InWords { get; set; }
        string Prefix { get; set; } /*Added by Poongodi on 16/06/2017*/
        string InvoiceSeries { get; set; }
        string UserName { get; }
        string CardNo { get; }
        DateTime? CardDate { get; }
        string ChequeNo { get; }

        DateTime? ChequeDate { get; }

        DateTime? SalesCreatedAt { get; }
        //Newly added by Mani for Print Footer on 28-02-2017
        string PrintFooterNote { get; set; }
        bool IsBold { get; set; }

        decimal? NetAmount { get; }
        decimal? RoundoffNetAmount { get; }
        List<SalesReturnItem> SalesReturnItem { get; set; }
        SalesReturn SalesReturn { get; set; }
       bool IsReturnsOnly { get; set; }
        //decimal? GrandGstTotalValue { get; }
        decimal? IgstTotalValue { get; }
        decimal? CgstTotalValue { get; }
        decimal? SgstTotalValue { get; }
        int? TaxRefNo { get; set; }
        decimal? TotalDiscount { get; set; }
        //Added by Bikas on 15-05-18
        decimal? MRPTotal { get; }
        decimal? TotalMRPValue { get; set; }
        decimal? ReturnAmount { get; set; }
        decimal? SavingAmount { get; set; }
        string AmountinWords { get; set; }
        //Added by Sarubala on 08-11-17
        List<SalesPayment> SalesPayments { get; set; }
    }
}