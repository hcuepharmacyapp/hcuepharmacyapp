﻿using HQue.Contract.Base;
using System.Collections.Generic;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Setup;
using System.Linq;
using HQue.Contract.Infrastructure.Settings;

using HQue.Contract.Infrastructure.Setup;
using System;
using HQue.Contract.Infrastructure.Accounts;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesReturn : BaseSalesReturn
    {
        public SalesReturn()
        {
            SalesReturnItem = new List<SalesReturnItem>();
            Sales = new Sales();
            Patient = new Patient();
            Instance = new Instance();  
            SalesItem = new SalesItem();
            HQueUser = new HQueUser();
            Voucher = new Voucher();
            SalesReturnLoyaltySettings = new LoyaltyPointSettings();
        }

        public SalesReturn(bool isFromSales)
        {
            SalesReturnItem = new List<SalesReturnItem>();
        }
        public List<SalesReturnItem> SalesReturnItem { get; set; }
        public virtual string FromReturnNo { get; set; }      
        
        public virtual string ToReturnNo { get; set; }

        //added by nandhini  for sales cancel no filter search on 11.9.2017
        public int? FromCancelNo { get; set; }
        public int? ToCancelNo { get; set; }

        //end
        //added by nandhini 4/9/17 

        public string searchProductId { get; set; }
        public virtual DateTime? FromReturnDate { get; set; }

        public string ReportReturnDate { get; set; }

        public virtual DateTime? ToReturnDate { get; set; }

        public LoyaltyPointSettings SalesReturnLoyaltySettings { get; set; }  //Added by Sarubala on 20-06-18

        public string PatientName { get; set; }
        
        public string PatientMobile { get; set; }

        public string PatientGender { get; set; }

        public string PatientEmail { get; set; }

        public int? PatientAge { get; set; }

        public decimal? PreviousLoyaltyPts { get; set; }

        public string PatientAddress { get; set; }

        public decimal? Discount { get; set; }

        public string PaymentType { get; set; }
    
        public string DeliveryType { get; set; }

        public bool? BillPrint { get; set; }

        public bool? SendSms { get; set; }

        public bool? SendEmail { get; set; }

        public decimal? PurchasedTotal { get; set; }

        public HQueUser HQueUser { get; set; }

        public Sales Sales { get; set; }
        public Patient Patient { get; set; }
        public SalesItem SalesItem { get; set; }
        public Voucher Voucher { get; set; }
        public decimal? ReturnedPrice { get; set; }
        public decimal? AmountBeforeDiscount { get; set; }
        public decimal? AmountAfterDiscount { get; set; }
        public decimal? discountedAmount { set; get; }
        public decimal? RedeemPts { set; get; }
        public decimal? ReturnTotal { set; get; }

        public Instance Instance { get; set; }

        public decimal? SubTotal
        {
            get
            {
                return SalesReturnItem.Where(m => m.IsDeleted != true).Sum(m => m.FinalPrice);
            }
        }

        public decimal? TotalWithoutDiscount
        {
            get
            {
                return SalesReturnItem.Where(m => m.IsDeleted != true).Sum(m => m.Total);
            }
        }
        public decimal? TotalItemDiscountAmount
        {
            get
            {
                return SalesReturnItem.Where(m => m.IsDeleted != true).Sum(m => m.DiscountAmount);
            }
        }

        public int? ActiveItemsCount
        {
            get
            {
                return SalesReturnItem.Count(m => m.IsDeleted != true);
            }
        }
        //Added by Sarubala on 13-11-17
        public decimal? CancelBillValue
        {
            get
            {
                return SalesReturnItem.Sum(m => m.BillValue);
            }
            set { }
        }

        public DateTime? ReturnDate { get; set; }
        public string ReturnNo { get; set; }

        public decimal? IgstTotalValue { get; set; }
        public decimal? CgstTotalValue { get; set; }
        public decimal? SgstTotalValue { get; set; }
        public decimal? FinalValue { get; set; }
        public string Select1 { get; set; }
        public string SelectedSeries { get; set; }
        public decimal? DebitAmt { get; set; } // Added by Sarubala on 10-07-17
        public bool? IsUpdateNetAmt { get; set; } // Added by Sarubala on 19-07-17
        public bool? isEnableRoundOff { get; set; }
        public bool? isComposite { get; set; }
        public string ReturnTime { get; set; }
    }
}
