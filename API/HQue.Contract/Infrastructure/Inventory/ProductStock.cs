﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using System;
namespace HQue.Contract.Infrastructure.Inventory
{
    public class ProductStock : BaseProductStock
    {
        public ProductStock()
        {
            Product = new Product();
            Vendor = new Vendor();
            VendorPurchase = new VendorPurchase();
            InventoryReOrder = new InventoryReOrder();
        }
        public Product Product { get; set; }
        public Vendor Vendor { get; set; }
        public VendorPurchase VendorPurchase { get; set; }
        public InventoryReOrder InventoryReOrder { get; set; }
        public decimal? VatInPrice => (SellingPrice * 100) / (VAT + 100);
        public decimal? ActualStock { get; set; }
        public decimal? CostPrice { get; set; }
        public decimal? CostPriceGst { get; set; }
        //added by nandhini for csv 
        public string ReportInvoiceDate { get; set; }
        //public decimal? MRP { get; set; }
        public int Age { get; set; }
        public string idchk { get; set; }
        public decimal? TotalStock
        {
            get
            {
                return Stock;
            }
        }
        public string rowHighlight { get; set; }
        public string Name => Product.Name;
        public string RackNo => Product.RackNo;
        public string productName { get; set; }
        public decimal? TotalCostPrice { get; set; }
        public string LastSaleDate { get; set; }
        public decimal? Expiredstock { get; set; }
        //added by nandhini for csv
        public string ReportExpireDate { get; set; }
        public string ReportCreatedAt { get; set; }
        
        public decimal? BalanceAmount { get; set; }
        public decimal? VatAmount { get; set; }
        public TempVendorPurchaseItem TempStock { get; set; }
        //public bool? StockImport { set; get; }
        public decimal? Lessthan30days { get; set; }
        public decimal? Lessthan60days { get; set; }
        public decimal? Lessthan120days { get; set; }
        public decimal? Lessthan180days { get; set; }
        public decimal? Lessthan360days { get; set; }
        public virtual DateTime? LastSalesDate { get; set; }
        public decimal soldqty { get; set; }
        public decimal? TranQty { get; set; }
        public decimal? packageQty { get; set; }
        public decimal? OrderQty { get; set; }
        public decimal? RequiredQty { get; set; }
        public decimal? pendingPOQty { get; set; }
        public bool? isSaleEdit { get; set; }
        public decimal? transQuantity { get; set; }
        public string StatusText { get; set; }
        //For Account wise stock display
        public string InstanceName { get; set; }
        public string Phone { get; set; }

        //For Purchase Sales Report
        public decimal? PS_Purchase_Amount { get; set; }

        public decimal? GSTInPrice => (SellingPrice * 100) / (GstTotal + 100);

        //public decimal? GstTotalRef { get; set; }
        // Added by Gavaskar 11-10-2017 Quantity  Sales Order , Estimate ,Template Convert to Sales Start
        public decimal? PendingTemplateQty { get; set; }
        public decimal? OrderEstimateDiscount { get; set; }
        public string OrderEstimateCutomerName { get; set; }
        public string OrderEstimateCutomerId { get; set; }
        public string OrderEstimateCutomerMobile { get; set; }
        public int OrderEstimateType { get; set; }
        // Added by Gavaskar 11-10-2017 Quantity Sales Order , Estimate ,Template Convert to Sales End


        //Added by Sarubala on 06-11-17
        public decimal? PurchasePriceWithoutTax { get; set; }
        public decimal? ReturnPurchasePriceWithoutTax
        {
            get {
                if (PackagePurchasePrice > 0 && PackageSize > 0)
                    return PackagePurchasePrice / PackageSize;
                return 0;
            }
        }

        //Added by Sarubala on 27-10-17
        public int? SalesItemCount { get; set; }
        public int? SalesItemAuditCount { get; set; }

        public bool IsUpdated { get; set; }
        public string Queryxml { get; set; }

    }

    public class ClosingStock
    {
        public string stockxml { get; set; }
    }
}
