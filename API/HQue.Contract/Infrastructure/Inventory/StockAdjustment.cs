﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Misc;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class StockAdjustment : BaseStockAdjustment
    {
        public StockAdjustment()
        {
            ProductStock = new Inventory.ProductStock();
        }

        public ProductStock ProductStock { get; set; }
        public decimal? PreviousStockValue { get; set; }
        public decimal? Total { get; set; }
        public string AdjustedPerson { get; set; }
        public string select { get; set; }
        public string select1 { get; set; }
        public string selectedValue { get; set; }
        public DateRange Dates { get; set; }
        //added by nandhini for csv
        public string ReportExpireDate { get; set; }
        public string ReportCreatedAt { get; set; }



    }
}
