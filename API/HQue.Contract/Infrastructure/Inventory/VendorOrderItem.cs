﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class VendorOrderItem : BaseVendorOrderItem
    {
        public VendorOrderItem()
        {
            Product = new Product();
            VendorOrder = new VendorOrder();
            ProductStock = new Inventory.ProductStock();
        }

        public Product Product { get; set; }

        public VendorOrder VendorOrder { get; set; }

		public ProductStock ProductStock { get; set; }

        public List<ProductStock> ProductStockList { get; set; }

        public byte Status { get; set; }

        public decimal? Amount { get; set; }

        public decimal? FreeQty { get; set; }

        public decimal? Discount { get; set; }

        public decimal? PackageSellingPrice { get; set; }
        //added by nandhini for csv
        public string OrderId { get; set; }
        public string ReportInvoiceDate { get; set; }

        public decimal? TotalAmount
        {
            get
            {
                if (!(Quantity.HasValue && ProductStock.PackagePurchasePrice.HasValue))
                    return 0;
                decimal? total = Quantity * CalCulatedPrice;
                return total;
            }

        }

        public decimal? CalCulatedPrice
        {
            get
            {
                decimal? price = 0;
                if (!ProductStock.PackagePurchasePrice.HasValue)
                    return 0;
                else
                {
                    if(ProductStock.CST > 0)
                    {
                        price = (ProductStock.PackagePurchasePrice + (ProductStock.PackagePurchasePrice * ProductStock.CST / 100) - (ProductStock.PackagePurchasePrice * Discount / 100));
                    }
                    else
                    {
                        price = (ProductStock.PackagePurchasePrice + (ProductStock.PackagePurchasePrice * ProductStock.VAT / 100) - (ProductStock.PackagePurchasePrice * Discount / 100));
                    }
                }                
                return price;
            }
        }
		
        public string ProductMasterID { get; set; }
        public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public string Schedule { get; set; }
        public string GenericName { get; set; }
        public string Category { get; set; }
        public string Packing { get; set; }
        public bool TransferQtyType { get; set; }
        public string VendorPurchaseItemId { get; set; }
    }
}
