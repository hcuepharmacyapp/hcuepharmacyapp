﻿using System;
using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class PurchaseReturnConsolidatedGst
    {
        //Added by Bikas for GST Report 06/06/2018
        public decimal? GSTValue { get; set; }
        public decimal? gstExcempted_PurchaseReturnValue { get; set; }
         public decimal? Gst0_Value { get; set; }
        public decimal? Gst0_TotalPurchaseReturnValue { get; set; }
        public decimal? Gst0_PurchaseReturnValue { get; set; }
        public decimal? Gst5_Value { get; set; }
        public decimal? Gst5_TotalPurchaseReturnValue { get; set; }
        public decimal? Gst5_PurchaseReturnValue { get; set; }
        public decimal? Gst12_Value { get; set; }
        public decimal? Gst12_TotalPurchaseReturnValue { get; set; }
        public decimal? Gst12_PurchaseReturnValue { get; set; }
        public decimal? Gst18_Value { get; set; }
        public decimal? Gst18_TotalPurchaseReturnValue { get; set; }
        public decimal? Gst18_PurchaseReturnValue { get; set; }
        public decimal? Gst28_Value { get; set; }
        public decimal? Gst28_TotalPurchaseReturnValue { get; set; }
        public decimal? Gst28_PurchaseReturnValue { get; set; }
        public decimal? GstOther_Value { get; set; }
        public decimal? GstOther_TotalPurchaseReturnValue { get; set; }
        public decimal? GstOther_PurchaseReturnValue { get; set; }
        public virtual string ReturnNo { get; set; }
        public virtual DateTime? ReturnDate { get; set; }
        public decimal? FinalValue { get; set; }
      
    }
}
