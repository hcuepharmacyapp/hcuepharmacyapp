﻿using HQue.Contract.Base;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class DiscountRules:BaseDiscountRules
    {
        public DiscountRules()
        {
            DiscountItem = new List<DiscountRules>();
        }

        public IEnumerable<DiscountRules> DiscountItem { get; set; }

    }
}


