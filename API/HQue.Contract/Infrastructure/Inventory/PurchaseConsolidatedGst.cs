﻿using System;
using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class PurchaseConsolidatedGst 
    {
        //Added by Bikas for GST Report 06/06/2018
        public decimal? GSTValue { get; set; }
        public decimal? gstExcempted_PurchaseValue { get; set; }
         public decimal? Gst0_Value { get; set; }
        public decimal? Gst0_TotalPurchaseValue { get; set; }
        public decimal? Gst0_PurchaseValue { get; set; }
        public decimal? Gst5_Value { get; set; }
        public decimal? Gst5_TotalPurchaseValue { get; set; }
        public decimal? Gst5_PurchaseValue { get; set; }
        public decimal? Gst12_Value { get; set; }
        public decimal? Gst12_TotalPurchaseValue { get; set; }
        public decimal? Gst12_PurchaseValue { get; set; }
        public decimal? Gst18_Value { get; set; }
        public decimal? Gst18_TotalPurchaseValue { get; set; }
        public decimal? Gst18_PurchaseValue { get; set; }
        public decimal? Gst28_Value { get; set; }
        public decimal? Gst28_TotalPurchaseValue { get; set; }
        public decimal? Gst28_PurchaseValue { get; set; }
        public decimal? GstOther_Value { get; set; }
        public decimal? GstOther_TotalPurchaseValue { get; set; }
        public decimal? GstOther_PurchaseValue { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public decimal? FinalValue { get; set; }

    }
}
