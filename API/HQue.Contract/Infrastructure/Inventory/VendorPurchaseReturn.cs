﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Reports;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class VendorPurchaseReturn : BaseVendorReturn
    {
        public VendorPurchaseReturn(bool isFromPurchase = false)
        {
            VendorPurchaseReturnItem = new List<VendorPurchaseReturnItem>();
            if (!isFromPurchase)
                VendorPurchase = new VendorPurchase();
            //by san
            Vendor = new List<Vendor>();
            VendorpurchaseReturnReport = new VendorpurchaseReturnReport();
        }

        public List<VendorPurchaseReturnItem> VendorPurchaseReturnItem { get; set; }
        //by san
        public List<Vendor> Vendor { get; set; }

        
        public decimal? SubTotal { get; set; }

        public override string VendorId { get; set; }
        public string VendorName { get; set; }

        public string InvoiceNo { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public decimal? Discount { get; set; }
        //added by nandhini for csv
        public string ReportInvoiceDate { get; set; }
        public string ReportReturnDate { get; set; }
        public string ReportExpireDate { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        //end
        

        public VendorPurchase VendorPurchase { get; set; }
        public VendorpurchaseReturnReport VendorpurchaseReturnReport { get; set; }

        public decimal? NetReturn
        {
            get
            {
                return VendorPurchaseReturnItem.Sum(m => m.ReturnTotal);
            }
        }
        public string BatchNo { get; set; }
        public string GoodsRcvNo { get; set; }
        public string SearchProductId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string BillSeries { get; set; }
        public override decimal? TotalReturnedAmount { get; set; }
        public string InstanceName { get; set; }

        /*GST Rleated fields  Settu by Poongodi on 26/08/2017*/
        public decimal? TotalCGSTAmount
        {
            get
            {
                return VendorPurchaseReturnItem.Sum(s => s.CGSTAmount);
            }
        }
        public decimal? TotalSGSTAmount
        {
            get
            {
                return VendorPurchaseReturnItem.Sum(s => s.SGSTAmount);
            }
        }
        public decimal? TotalIGSTAmount
        {
            get
            {
                return VendorPurchaseReturnItem.Sum(s => s.IGSTAmount);
            }
        }
        public decimal? TotalGSTAmount
        {
            get
            {
                return VendorPurchaseReturnItem.Sum(s => s.GSTAmount1);
            }
        }
        public decimal? TotalVATAmount
        {
            get
            {
                return VendorPurchaseReturnItem.Sum(s => s.VatPrice);
            }
        }
    }
}
