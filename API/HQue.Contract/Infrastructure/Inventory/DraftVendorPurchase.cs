﻿using HQue.Contract.Base;
using System.Collections.Generic;
namespace HQue.Contract.Infrastructure.Inventory
{
    public class DraftVendorPurchase : BaseDraftVendorPurchase
    {

        public DraftVendorPurchase()
        {
            DraftVendorPurchaseItem = new List<DraftVendorPurchaseItem>();
            
        }

        public List<DraftVendorPurchaseItem> DraftVendorPurchaseItem { set; get; }
        public string errorMessage { get; set; } = "OK";

    }
}
