﻿using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using System.Linq;
using System;
using HQue.Contract.Infrastructure.Audits;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.Contract.Infrastructure.Setup;
namespace HQue.Contract.Infrastructure.Inventory
{
    public class VendorPurchase : BaseVendorPurchase
    {
        public VendorPurchase()
        {
          
        VendorPurchaseItem = new List<VendorPurchaseItem>();
            VendorReturn = new VendorPurchaseReturn(true);
            VendorPurchaseReturns = new List<VendorPurchaseReturn>();
            // Added by Gavaskar 30-08-2017 Purchase Return
            VendorPurchaseReturnsHistory = new List<VendorPurchaseReturn>();
            Vendor = new Vendor();
            VendorOrder = new VendorOrder();
            VendorPurchaseItemAudits = new List<VendorPurchaseItemAudit>();
            HQueUser = new HQueUser();
            Instance = new Instance();
        }
        //GrnDate added by nandhini
        public bool SaveFailed { get; set; }
        public bool EditMode { get; set; }
        public BaseErrorLog ErrorLog { get; set; }
        public DateTime GrnDate { get; set; }
        public IEnumerable<VendorPurchaseItem> VendorPurchaseItem { get; set; }
        public Vendor Vendor { get; set; }
        public VendorOrder VendorOrder { get; set; }
        public VendorPurchaseReturn VendorReturn { get; set; }
        public IEnumerable<VendorPurchaseReturn> VendorPurchaseReturns { get; set; }
        public IEnumerable<VendorPurchaseItem> DeletedVendorPurchaseItem { get; set; }
        public IEnumerable<VendorPurchaseItemAudit> VendorPurchaseItemAudits { get; set; }
        // Added by Gavaskar 30-08-2017 Purchase Return
        public IEnumerable<VendorPurchaseReturn> VendorPurchaseReturnsHistory { get; set; }
        //public List<VendorPurchaseItem> VendorPurchaseItemList { get; set; }
        //public List<VendorPurchaseReturnItem> VendorPurchaseReturnItemList { get; set; }
        public decimal? GrossTotal
        {
            get
            {
                return VendorPurchaseItem.Sum(m => m.Total);
            }
            //set { }
        }
        public decimal? NetTotal
        {
            get
            {
                if (Discount.HasValue)
                    return (GrossTotal + Vat).Value + DebitAmount - CreditAmount;
                //return ((GrossTotal + Vat) - ((Discount / 100) * (GrossTotal + Vat))).Value;
                //return Math.Ceiling(((GrossTotal + Vat) - ((Discount / 100) * (GrossTotal + Vat))).Value);
                return GrossTotal + Vat + DebitAmount - CreditAmount;
            }
        }
        //public decimal? NetTotal
        //{
        //    get
        //    {
        //        if (Discount.HasValue)
        //            return (GrossTotal + Vat).Value;
        //        //return ((GrossTotal + Vat) - ((Discount / 100) * (GrossTotal + Vat))).Value;
        //        //return Math.Ceiling(((GrossTotal + Vat) - ((Discount / 100) * (GrossTotal + Vat))).Value);
        //        return GrossTotal + Vat ;
        //    }
        //}
        public decimal? DebitAmount
        {
            get
            {
                if (NoteAmount > 0 && NoteType == "debit")
                    return NoteAmount;
                return 0;
            }
        }
        public decimal? CreditAmount
        {
            get
            {
                if (NoteAmount > 0 && NoteType == "credit")
                    return NoteAmount;
                return 0;
            }
        }
        //The below calculation not used anywhere across application
        //public decimal? GrossTotalWithVat
        //{
        //    get
        //    {
        //        return VendorPurchaseItem.Sum(m => m.TotalWithVat);
        //    }
        //    //set { }
        //}
        //public decimal? TotalOnly
        //{
        //    get
        //    {
        //        //if (Discount.HasValue)
        //        //    return VendorPurchaseItem.Sum(m => m.TotalWithDiscount);
        //        //else
        //        //    return VendorPurchaseItem.Sum(m => m.Total);
        //        return VendorPurchaseItem.Sum(m => m.TotalWithDiscount);
        //    }
        //    //set { }
        //}
        //public decimal? NetTotalWithVat
        //{
        //    get
        //    {
        //        //if (Discount.HasValue)
        //        //    return ((GrossTotalWithVat) - ((Discount / 100) * (TotalOnly))).Value;
        //        //return Math.Ceiling(((GrossTotal + Vat) - ((Discount / 100) * (GrossTotal + Vat))).Value);
        //        return GrossTotal + Vat;
        //    }
        //}

        public bool HasReturn => !string.IsNullOrEmpty(VendorReturn.VendorPurchaseId);
        //public decimal? RoundOff => (decimal?)Math.Ceiling((double)NetTotal);
        public decimal? RoundOff => (decimal?)Math.Round((double)NetTotal, MidpointRounding.AwayFromZero);
        public decimal? Vat
        {
            get
            {
                return VendorPurchaseItem.Sum(m => m.VatPrice);
            }
        }
        public decimal? ActualPaymentTotal
        {
            get; set;
        }
        public decimal? PaymentTotalChange
        {
            get
            {
                return RoundOff - ActualPaymentTotal;
            }
        }
        //public decimal? DiscountInValue
        //{
        //    get
        //    {
        //        if (!Discount.HasValue)
        //            return Discount = 0;
        //        return (Discount / 100) * VendorPurchaseItem.Sum(m => m.Total);
        //    }
        //}
        //public decimal? TotalDisount
        //{
        //    get
        //    {
        //        if (!Discount.HasValue)
        //            return Discount = 0;
        //        //return (Discount / 100) * VendorPurchaseItem.Sum(m => m.TotalWithDiscount);
        //        return VendorPurchaseItem.Sum(m => m.TotalWithDiscount);
        //    }
        //}
        //public decimal? ListTotal => NetTotal - DiscountInValue;
        public decimal? UpdatePaymentTotal
        {
            get; set;
        }     
        public string SearchProductId { get; set; }
        public string BatchNo { get; set; }
        public string InWords { get; set; }
        public HQueUser HQueUser { get; set; }
        public Instance Instance { get; set; }
        public string Select { get; set; }
        public string Select1 { get; set; }
        public string Values { get; set; }
        // Added Gavaskar 01-03-2017
        public string SelectValue { get; set; }
        public int PurchaseCount { get; set; }
        public string DraftName { get; set; }
        public string draftVendorPurchaseId { get; set; }
        public string FormType { get; set; }
        public string SalePurpose { get; set; }
        //The following objects added by Poongodi for POCancel implementation on 07/03/2017
        public int POStatus { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool isPaymentStarted { get; set; } = false;
        public string errorMessage { get; set; } = "";

        //Added by Sarubala for Reports on 21-07-17
        public string ActualInvoice { get; set; }
        public decimal? MRP { get; set; }
        public decimal? Tax { get; set; }
        public decimal? ProfitOnCost { get; set; }
        public decimal? ProfitOnMrp { get; set; }
        public decimal? NetPurchasePrice { get; set; }
        public bool? IsReturn { get; set; }
        public decimal? CreditValue { get; set; } //Added by Sarubala for Reports on 09-08-17
        public decimal? DebitValue { get; set; }
        public DateTime? SearchDateEnd { get; set; }
        public decimal? ReturnRoundedValue { get; set; } // Added by Gavaskar 08-09-2017
        public bool? VendorChangedInEdit { get; set; }

        public string ImportError { get; set; }

        // Added by Gavaskar 01-09-2017 
        //  public decimal ReturnAmount { get; set; }

        /*GST Rleated fields  Added by Poongodi on 04/07/2017*/
        //public decimal? TotalCGSTAmount
        //{
        //    get
        //    {
        //        return VendorPurchaseItem.Sum(s => s.CGSTAmount);
        //    }
        //}
        //public decimal? TotalSGSTAmount
        //{
        //    get
        //    {
        //        return VendorPurchaseItem.Sum(s => s.SGSTAmount);
        //    }
        //}
        //public decimal? TotalIGSTAmount
        //{
        //    get
        //    {
        //        return VendorPurchaseItem.Sum(s => s.IGSTAmount);
        //    }
        //}
        //public decimal? TotalGSTAmount
        //{
        //    get
        //    {
        //        return VendorPurchaseItem.Sum(s => s.GSTAmount);
        //    }
        //}
        //public decimal? NetTotalWithGST
        //{
        //    get
        //    {

        //        return GrossTotal + TotalGSTAmount;
        //    }
        //}
    }
}
