﻿using HQue.Contract.Base;
namespace HQue.Contract.Infrastructure.Inventory
{
    public class DraftVendorPurchaseItem : BaseDraftVendorPurchaseItem
    {

        public DraftVendorPurchaseItem()
        {
            ProductStock = new ProductStock();
        }
        public ProductStock ProductStock { set; get; }
     
    }
}
