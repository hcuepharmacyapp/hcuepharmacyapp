﻿using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using System.Linq;
using System;
using HQue.Contract.Infrastructure.Audits;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class DcVendorPurchaseItem : BaseDCVendorPurchaseItem
    {
        public DcVendorPurchaseItem()
        {
            DcList = new List<DcVendorPurchaseItem>();
            ProductStock = new ProductStock();
            Vendor = new Vendor();            

        }

        public IEnumerable<DcVendorPurchaseItem> DcList { get; set; }        
        public ProductStock ProductStock { get; set; }        

        public Vendor Vendor { get; set; }

        public string SearchProductId { get; set; }
        public string BatchNo { get; set; }
        public decimal? PurchaseQuantity { get; set; }
        //added by nandhini for download csv
        public string ReportDCDate { get; set; }

        public decimal? Total
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;
                if (!(Discount.HasValue && Discount.HasValue))
                    return 0;
                                
                decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);
                return total - (total * Discount / 100);                
            }           
        }

        public decimal? TotalWithDiscount
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;

                if (!(Discount.HasValue && Discount.HasValue))
                    return 0;

                decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);

                return total;
            }

        }

        public decimal? SingleDiscount
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;

                if (!(Discount.HasValue && Discount.HasValue))
                    return 0;

                decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);

                return (total * Discount / 100);
            }

        }


        public decimal? TotalWithVat
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;
                
                decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);                
                return total + VatPrice;

            }
        }

        public decimal? VatPrice
        {
            get
            {
                if (!(Total.HasValue && ProductStock.VAT.HasValue))
                    return 0;               

                if (Total.HasValue && ProductStock.CST > 0)
                    return Total * (ProductStock.CST / 100);
                else if (Total.HasValue && ProductStock.VAT > 0)
                    return Total * (ProductStock.VAT / 100);
                else
                    return Total * (ProductStock.VAT / 100);
            }
        }

        public decimal? PackagePriceAfterTax
        {
            get
            {
                if (!(Total.HasValue && ProductStock.VAT.HasValue))
                    return 0;

                if (PackageQty > 0)
                {
                    if (ProductStock.CST.HasValue && ProductStock.CST > 0)
                        return (Total + (Total * (ProductStock.CST / 100))) / PackageQty;
                    else if (ProductStock.VAT.HasValue && ProductStock.VAT > 0)
                        return (Total + (Total * (ProductStock.VAT / 100))) / PackageQty;
                    else                        
                        return (Total) / PackageQty;
                }
                else
                {
                    return Total;
                }
            }
        }       

        public decimal? ActualQty { get; set; }

        public decimal? QtyChange => Quantity - ActualQty;

        public decimal? ReportTotal { get; set; }
        public decimal? VATValue { get; set; }
        public string Action { get; set; }
        public string InstanceName { get; set; }
        public string Status { get; set; }
    }
}
