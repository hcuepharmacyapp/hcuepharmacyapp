﻿using HQue.Contract.Base;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class TempVendorPurchaseItem : BaseTempVendorPurchaseItem
    {
        public TempVendorPurchaseItem()
        {
            ProductStock = new ProductStock();
            VendorPurchase = new VendorPurchase();
            BuyPurchase = new VendorPurchase();
        }

        public decimal? PurchaseQuantity { get; set; }

        public decimal? Total
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;
                if (!(Discount.HasValue && Discount.HasValue))
                    return 0;

                //return PackagePurchasePrice * PackageQty;
                decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);
                //decimal? total = PurchasePrice * (PackageQty - FreeQty);
                return total - (total * Discount /100);
                //return PurchasePrice * Quantity;
            }

            //get
            //{
            //    if (!Discount.HasValue)
            //        return 0;

            //    if (!(Quantity.HasValue && PackagePurchasePrice.HasValue))
            //        return 0;
            //    return (PackagePurchasePrice * (Quantity-FreeQty))-((Discount / 100) * (PackagePurchasePrice * Quantity));
            //}
        }

        public decimal? TotalWithDiscount
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;

                if (!(Discount.HasValue && Discount.HasValue))
                    return 0;

                decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);

                 return total;                          
            }
            
        }

        public decimal? SingleDiscount
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;

                if (!(Discount.HasValue && Discount.HasValue))
                    return 0;

                decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);

                return (total * Discount / 100);
            }

        }


        public decimal? TotalWithVat
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;
               
                //return PackagePurchasePrice * PackageQty;
                decimal? total = PackagePurchasePrice * (PackageQty - FreeQty);
                //decimal? total = PurchasePrice * (PackageQty - FreeQty);
                return total + VatPrice;
                              
            }
        }

        public decimal? VatPrice
        {
            get
            {
                if (!(Total.HasValue && ProductStock.VAT.HasValue))
                    return 0;

                //if (!(Total.HasValue && ProductStock.CST.HasValue))
                //    return 0;

                //return Total * (ProductStock.CST / 100);

                //if (!(Total.HasValue && ProductStock.VAT.HasValue))
                //    return 0;
                //if (!(Total.HasValue && ProductStock.CST.HasValue))
                //    return 0;

                if (Total.HasValue && ProductStock.CST > 0)
                    return Total * (ProductStock.CST / 100);
                else if (Total.HasValue && ProductStock.VAT > 0)
                    return Total * (ProductStock.VAT / 100);
                else
                    return Total * (ProductStock.VAT / 100);
            }
        }

        public decimal? PackagePriceAfterTax
        {
            get
            {
                if (!(Total.HasValue && ProductStock.VAT.HasValue))
                    return 0;

                if (PackageQty > 0)
                {
                    if (ProductStock.CST.HasValue && ProductStock.CST > 0)
                        return (Total + (Total * (ProductStock.CST / 100))) / PackageQty;
                    else if (ProductStock.VAT.HasValue && ProductStock.VAT > 0)
                        return (Total + (Total * (ProductStock.VAT / 100))) / PackageQty;
                    else
                        // return 0;
                        return (Total) / PackageQty;
                }
                else
                {
                    return Total;
                }
            }
        }

        public ProductStock ProductStock { get; set; }

        [JsonIgnore]
        public VendorPurchase VendorPurchase { get; set; }

        public VendorPurchase BuyPurchase { get; set; }

        public decimal? ActualQty { get; set; }

        public decimal? QtyChange => Quantity - ActualQty;

        public decimal? ReportTotal { get; set; }

        public string Action { get; set; }

        public bool IsItemSaled { get; set; }

        // Added By Gavaskar 31-08-2017 
        public bool IsItemPurchase { get; set; }
        //added by nandhini for csv
        public string ReportExpireDate { get; set; }
    }
}
