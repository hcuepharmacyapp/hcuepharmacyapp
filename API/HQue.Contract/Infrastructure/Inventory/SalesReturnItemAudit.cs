﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesReturnItemAudit : BaseSalesReturnItemAudit
    {


        public SalesReturnItemAudit()
        {
            SalesReturn = new SalesReturn();
            ProductStock = new ProductStock();
        }

        public ProductStock ProductStock { get; set; }

        public SalesReturn SalesReturn { get; set; }
        public string UpdatedByUser { get; set; }

    }
}
