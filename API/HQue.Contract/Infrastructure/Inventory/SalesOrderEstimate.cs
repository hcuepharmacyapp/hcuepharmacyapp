﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesOrderEstimate : BaseSalesOrderEstimate
    {
        public SalesOrderEstimate()
        {
            SalesOrderEstimateItem = new List<SalesOrderEstimateItem>();
            SalesEstimateItem = new List<SalesOrderEstimateItem>();
            Patient = new Patient();
            Instance = new Instance();
        }
        public List<SalesOrderEstimateItem> SalesOrderEstimateItem { get; set; }
        public List<SalesOrderEstimateItem> SalesEstimateItem { get; set; }
        public Patient Patient { get; set; }
        public Instance Instance { get; set; }
        public string FilePath { get; set; }
    }
}
