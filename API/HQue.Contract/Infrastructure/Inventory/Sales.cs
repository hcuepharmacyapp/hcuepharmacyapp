﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using HQue.Contract.Extension;
using HQue.Contract.Infrastructure.Reminder;
using HQue.Contract.Infrastructure.Setup;
using SR = HQue.Contract.Infrastructure.Inventory.SalesReturn;
using HQue.Contract.Infrastructure.UserAccount;
using Newtonsoft.Json;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.Leads;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Settings;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class Sales : BaseSales, IReminder, IInvoice
    {
        public string EmpID;
       

        public Sales()
        {
            SalesItem = new List<SalesItem>();
            Patient = new Patient();
            Instance = new Instance();
      
            SalesReturn = new SR(true);
            HQueUser = new HQueUser();
            SaleType = new SalesType();
            Leads = new HQue.Contract.Infrastructure.Leads.Leads();
            //Leads = new Leads();
            CustomerPayments = new List<CustomerPayment>();
            SalesPayments = new List<SalesPayment>();
            SalesReturnItem = new List<SalesReturnItem>();
            LeadsProduct = new List<LeadsProduct>();
            SalesReturnItemHistory = new List<SalesReturnItem>();
            DomainValueList = new List<DomainValues>(); //Added by Sarubala on 05-10-17
            loyaltySettings = new LoyaltyPointSettings();
        }

        //[JsonIgnore]
        //added by nandhini for csv
        public virtual string ReportInvoiceDate { get; set; }
        public SalesReturn SalesReturn { get; set; }

        public List<SalesItem> SalesItem { get; set; }
        public List<SalesPayment> SalesPayments { get; set; }
        public List<SalesItemAudit> SalesItemAudit { get; set; }
        public List<SalesReturnItem> SalesReturnItem { get; set; }
        public List<SalesReturnItem> SalesReturnItemHistory { get; set; }
        public List<DomainValues> DomainValueList { get; set; } //Added by Sarubala on 05-10-17
        public decimal? ReturnAmount { get; set; }
        public decimal? ReturnAmt { get; set; } 

        public Patient Patient { get; set; }

        public Instance Instance { get; set; }


        public List<LeadsProduct> LeadsProduct { get; set; }


        public HQue.Contract.Infrastructure.Leads.Leads Leads { get; set; }

        // Newly Added 11-11-2016
        public bool isValidEmail { get; set; }

        public bool isEmpID { get; set; }

        //public decimal? SubTotal
        //{
        //    get
        //    {
        //        //return SalesItem.Sum(m => m.ActualAmount);  //Added by Sarubala on 12-09-17 for rounding values in sales table
        //        return Math.Round(Convert.ToDecimal(SalesItem.Sum(m => m.ActualAmount)),2,MidpointRounding.AwayFromZero);
        //    }
        //}

        public decimal? DiscountInValue
        {
            get
            {
                //if (!Discount.HasValue && !DiscountValue.HasValue)
                //    return Discount = 0;
                //else if (DiscountValue.HasValue && DiscountValue != 0)
                //{
                //    return DiscountValue;
                //}
                //else
                //{
                //    //return Math.Round(Convert.ToDecimal((Discount / 100) * SalesItem.Sum(m => m.Total) + SalesItem.Sum(m => m.SalesItemDiscount)), 2, MidpointRounding.AwayFromZero);
                //}
                return TotalDiscountValue as decimal? ?? 0;
            }
        }

        public decimal? Net
        {
            get
            {
                //return (SubTotal - (DiscountInValue)) + Math.Round(Convert.ToDecimal( SalesItem.Sum(m => m.VatAmount)),2);
                //return (SubTotal - (DiscountInValue)) + Convert.ToDecimal(SalesItem.Sum(m => m.VatAmount)); //by San 17-05-2017
                //return (SubTotal - (DiscountInValue)) + Convert.ToDecimal(SalesItem.Sum(m => m.SalesVatAmount)); //by San 17-05-2017
                return (SaleAmount as decimal? ?? 0 - RoundoffSaleAmount as decimal? ?? 0);
            }
        }

        //public decimal? RoundOff => (decimal?)Math.Round((double)Net, MidpointRounding.AwayFromZero);
        //public decimal? RoundOff => RoundoffNetAmount;
        //public decimal? RoundOff
        //{
        //    get
        //    {
        //        //if (RoundoffNetAmount != null && RoundoffNetAmount != 0)
        //        //    return (decimal?)Net + Convert.ToDecimal(RoundoffNetAmount);
        //        //else
        //        //    return (decimal?)Math.Round((double)Net, MidpointRounding.AwayFromZero);

        //        //return Math.Round((decimal)Net, 2, MidpointRounding.AwayFromZero); //by San 17-05-2017
        //    }
        //}

        public int? TotalNoOfDays
        {
            get
            {
                if (InvoiceDate != null)
                {
                    TimeSpan t = (DateTime.Today - Convert.ToDateTime(InvoiceDate).Date);
                    return t.Days;

                }
                else
                {
                    return 0;
                }
            }
        }

        public decimal? Vat
        {
            get
            {
                //return SalesItem.Sum(m => m.SalesVatAmount);
                return VatAmount as decimal? ?? 0;
            }
        }


        public bool HasReturn => !string.IsNullOrEmpty(SalesReturn.SalesId);

        public string InWords { get; set; }

        public decimal? PurchasedTotal => Math.Round(Convert.ToDecimal(SalesItem.Sum(m => m.Total)), 2, MidpointRounding.AwayFromZero);  //Added by Sarubala on 12-09-17 for rounding

        public decimal? MRPTotal => SalesItem.Sum(m => m.MRPTotal);

        public decimal? InvoiceAmount { get; set; }

        public decimal? FinalValue { get; set; }

        public decimal? Profit { get; set; }

        public Int32? TotalSales { get; set; }

        //public decimal? ListTotal => Math.Round(Convert.ToDecimal(PurchasedTotal - DiscountInValue),2,MidpointRounding.AwayFromZero);  //Added by Sarubala on 12-09-17 for rounding

        public HQueUser HQueUser { get; set; }

        public string SalesID { get; set; }

        public string Select { get; set; }

        public string Select1 { get; set; }

        public string Values { get; set; }
        // the following members added by Poongodi for Salehistory on 25/02/2017
        public string SelectValue { get; set; }
        public int SalesCount { get; set; }
        // end here
        public string NameAddress { get; set; }
        public decimal? card { get; set; }
        public decimal? cash { get; set; }
        public decimal? cheque { get; set; }

        //Added by Sarubala on 24-10-17
        public decimal? ewallet { get; set; } 
        public string subPayment { get; set; }

        public SalesType SaleType { get; set; }
        public string ActualInvoice { get; set; }
        public List<ProductStock> AvailableItems { get; set; }
        public int ReferenceFrom => UserReminder.SalesReminder;
        [JsonIgnore]
        public IEnumerable<IInvoiceItem> InvoiceItems => SalesItem;
        [JsonIgnore]
        public IEnumerable<IReminderItem> ReminderItems => SalesItem;

        public List<CustomerPayment> CustomerPayments { get; set; }
        public List<Voucher> VoucherList { get; set; }
        public List<Settlements> SettlementsList { get; set; }
        public string Remarks { get; set; }
        // added by Senthil.S
        public string CustomerId { get; set; }
                 
        public string DoctorAddress { get; set; }
        public string UserName { get; set; }

        public bool autoSavecust { get; set; }

        //Newly added by Mani for Print Footer on 28-02-2017
        public string PrintFooterNote { get; set; }


        public decimal? Amount { get; set; }
        public string Type { get; set; }


        public bool isChequeRecieved { get; set; } = false;

        public DateTime? SalesCreatedAt { get; set; }
        //Newly added for retrieving actual PaymentType as in DB
        public string PaymentTypeOrig { get; set; }
        public bool IsBold { get; set; }

        public bool IsOffline {get; set; }


        public bool leadsProductSelected { get; set; }
        public string leadId { get; set; }

        public string TransactionType { get; set; }
        

        /*The following objects added for ConsolidatedSalesReport by Sabarish on 11/05/2017*/
        public decimal? vaT52_Gross { get; set; }
        public decimal? vaT52 { get; set; }
        public decimal? vaT131_Gross { get; set; }
        public decimal? vaT131 { get; set; }
        public decimal? ConsolidatedRoundOff { get; set; }
        public decimal? SurCharge { get; set; } = 0.00M;
        //The below property added by Poongodi to display the other vat values in consolidate sales report on 30/05/2017
        public decimal? vatOther_Gross { get; set; }
        public decimal? vatOther { get; set; }
        public bool IsReturnsOnly { get; set; }
        public decimal ReturnValue { get; set; }
        public decimal? NetValue { get; set; }

        //Added By Settu for maintaining voucherpayment adjustment status
        public bool? IsReturnCreditAdjusted { get; set; }


        //Added By Sabarish For Pending Amount Paying
        public bool? PendingAmountChecked { get; set; }
        public decimal? PendingAmount { get; set; }
        public decimal? ReturnedPurchasePrice { get; set; }
        //public decimal? GrandGstTotalValue {
        //    get
        //    {
        //        if (!IgstTotalValue.HasValue && !IgstTotalValue.HasValue)  
        //            return Math.Round(Convert.ToDecimal((CgstTotalValue + SgstTotalValue)),2,MidpointRounding.AwayFromZero);
        //        else if (IgstTotalValue.HasValue && IgstTotalValue != 0)
        //            return Math.Round(Convert.ToDecimal(IgstTotalValue),2,MidpointRounding.AwayFromZero);  //Added by Sarubala on 12-09-17 for rounding
        //        else
        //            return 0;
        //    }            
        //}

        //Newly added for report starts
        public decimal? InvoiceAmountNoTaxNoDiscount { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? PurchasePrice { get; set; }
        public decimal? MRP { get; set; }
        public decimal? ProfitOnCost { get; set; }
        public decimal? ProfitOnMrp { get; set; }
        public string SalesProductName { get; set; }
        public string SalesProductId { get; set; }
        public decimal? SalesQty { get; set; }
        public decimal? GrossSaleWithoutTax { get; set; }
        public decimal? GstTotal { get; set; }
        //Newly added for report ends

        public decimal? IgstTotalValue => Math.Round(Convert.ToDecimal(SalesItem.Sum(m => m.IgstValue)),2,MidpointRounding.AwayFromZero);  //Added by Sarubala on 12-09-17 for rounding

        public decimal? CgstTotalValue
        {
            get
            {
                return Math.Round(Convert.ToDecimal(SalesItem.Sum(m => m.CgstValue)),2,MidpointRounding.AwayFromZero); //Added by Sarubala on 12-09-17 for rounding
            }
        }
        public decimal? SgstTotalValue
        {
            get
            {
                return Math.Round(Convert.ToDecimal(SalesItem.Sum(m => m.SgstValue)),2,MidpointRounding.AwayFromZero); //Added by Sarubala on 12-09-17 for rounding
            }
        }

        //
        public decimal? TotalDiscount
        {
            get { return Discount; }
            set { }
        }
        
        public bool IsReturn { get; set; }

        public int? PatientStatus { get; set; }

        public bool IsPartial { get; set; } //Newly added by Manivannan on 16-Nov-2017
        public bool IsCreated { get; set; } //Newly added by Manivannan on 16-Nov-2017
        public decimal? DiscountPercentage { get; set; } //Added by Poongodion 23/11/2017
        public bool SalesEditMode { get; set; } //Added by Sarubala on 23-11-17

        public LoyaltyPointSettings loyaltySettings { get; set; }

        public decimal CGSTTaxAmt { get; set; }
        public decimal SGSTTaxAmt { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? TotalMRPValue { get; set; }
        public decimal? SavingAmount { get; set; }
        public string AmountinWords { get; set; }
        public decimal InvAmountNoTaxNoDiscount { get; set; }
        public bool? isEnableRoundOff { get; set; }
        public bool Gstselect { get; set; }
        public bool? isEnableSalesUser { get; set; }
        public List<SalesItem> _soldItems { get; set; }
        public bool SaveFailed { get; set; }
        public BaseErrorLog ErrorLog { get; set; }
        public List<ProductStock> ValidateStockList { get; set; }
        public List<ProductStock> TransactionStockList { get; set; }
        public decimal? OldLoyaltyPts { get; set; }
        public decimal? OldSalesItemLoyaltyProductPts { get; set; }
    }
}
