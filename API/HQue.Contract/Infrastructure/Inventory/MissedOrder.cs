﻿using HQue.Contract.Base;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class MissedOrder : BaseMissedOrder
    {
        public MissedOrder()
        {
            MissedOrderItem = new List<SalesItem>();
            MissedItem = new SalesItem();
        }

        public IEnumerable<SalesItem> MissedOrderItem { get; set; }
        public SalesItem MissedItem { get; set; }

        public string Select { get; set; }

        public string Select1 { get; set; }

        public string Values { get; set; }

        //public string PatientName { get; set; }   

    }
}


