﻿using HQue.Contract.Base;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesType : BaseSalesType
    {
        public SalesType()
        {
            SalesTypeList = new List<SalesType>();
        }

        public List<SalesType> SalesTypeList;

    }
}


