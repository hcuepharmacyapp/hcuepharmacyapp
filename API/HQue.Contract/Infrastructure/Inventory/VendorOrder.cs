﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Setup;
using System;
using System.Collections.Generic;
using HQue.Contract.Infrastructure.Misc;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class VendorOrder : BaseVendorOrder
    {
        public VendorOrder()
        {
            VendorOrderItem = new List<VendorOrderItem>();
            Vendor = new Vendor();
            Instance = new Instance();
        }

        //public IEnumerable<VendorOrderItem> VendorOrderItem { get; set; }
        public List<VendorOrderItem> VendorOrderItem { get; set; }

        public Vendor Vendor { get; set; }

        public Instance Instance { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public DateRange Dates { get; set; }

        public string FilePath { get; set; }
        public bool IsBold { get; set; }
        public BaseErrorLog ErrorLog { get; set; }
    }
}


























