﻿using System;
using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class JournalReport
    {
        //Added by Bikas for Tally Report 28/06/2018
        public virtual string voucherno { get; set; }
        public virtual DateTime? voucherdate { get; set; }
        public string crledger { get; set; }
        public string drledger { get; set; }
        public string BillType { get; set; }
        public string BillName { get; set; }
        public decimal? Amount { get; set; }
        public string PaymentType { get; set; }
        public string CardNo { get; set; }
        public virtual DateTime? CardDate { get; set; }
        public string Cheque { get; set; }
        public virtual DateTime? ChequeDate { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string Remarks { get; set; }

    }
}
