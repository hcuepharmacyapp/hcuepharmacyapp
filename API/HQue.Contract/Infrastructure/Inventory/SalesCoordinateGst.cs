﻿using System;
using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesCoordinateGst 
    {
        //Added by Bikas for GST Report 06/06/2018
        public decimal? GSTValue { get; set; }
        public decimal? gstExcempted_SaleValue { get; set; }
         public decimal? Gst0_Value { get; set; }
        public decimal? Gst0_TotalSaleValue { get; set; }
        public decimal? Gst0_SaleValue { get; set; }
        public decimal? Gst5_Value { get; set; }
        public decimal? Gst5_TotalSaleValue { get; set; }
        public decimal? Gst5_SaleValue { get; set; }
        public decimal? Gst12_Value { get; set; }
        public decimal? Gst12_TotalSaleValue { get; set; }
        public decimal? Gst12_SaleValue { get; set; }
        public decimal? Gst18_Value { get; set; }
        public decimal? Gst18_TotalSaleValue { get; set; }
        public decimal? Gst18_SaleValue { get; set; }
        public decimal? Gst28_Value { get; set; }
        public decimal? Gst28_TotalSaleValue { get; set; }
        public decimal? Gst28_SaleValue { get; set; }
        public decimal? GstOther_Value { get; set; }
        public decimal? GstOther_TotalSaleValue { get; set; }
        public decimal? GstOther_SaleValue { get; set; }
        public virtual string InvoiceNo { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public decimal? FinalValue { get; set; }
        //public ProductStock ProductStock { get; set; }
        //public Sales Sales { get; set; }
    }
}
