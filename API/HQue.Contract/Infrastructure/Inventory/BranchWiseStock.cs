﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class BranchWiseStock 
    {
        public List<SalesItem> ProductStockList { get; set; }
        public List<VendorPurchaseItem> VendorPurchaseItemList { get; set; }
    }
}
