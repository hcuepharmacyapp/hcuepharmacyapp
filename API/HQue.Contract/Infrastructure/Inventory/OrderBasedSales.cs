﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.Setup;
using System;
using System.Collections.Generic;
using HQue.Contract.Infrastructure.Misc;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class OrderBasedSales : BaseVendorOrder
    {
        public OrderBasedSales()
        {
            QuantityCount = new List<int>();
            Product = new Master.Product();
            Vendor = new Master.Vendor();
        }
        public Product Product { get; set; }        
        public Vendor Vendor { get; set; }
        public decimal? Stock { get; set; }
        public List<int> QuantityCount { get; set; }
        public int PreviousDays { get; set; }
        public decimal OrderedQty { get; set; }
        
    }
}


























