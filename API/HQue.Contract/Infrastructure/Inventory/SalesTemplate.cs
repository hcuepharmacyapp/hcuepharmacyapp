﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesTemplate : BaseSalesTemplate
    {
        public SalesTemplate()
        {
            SalesTemplateItem = new List<SalesTemplateItem>();
            SalesTemplateProductStockList = new List<SalesTemplateItem>();
        }
        public List<SalesTemplateItem> SalesTemplateItem { get; set; }
        public List<SalesTemplateItem> SalesTemplateProductStockList { get; set; }
    }
}
