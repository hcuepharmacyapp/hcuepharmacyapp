﻿using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.UserAccount;
using System.Linq;
using System;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class StockTransfer : BaseStockTransfer
    {
        public const int TransferStatusPending = 1;
        public const int TransferStatusRejected = 2;
        public const int TransferStatusAccepted = 3;

        public StockTransfer()
        {
            StockTransferItems = new List<StockTransferItem>();
            Instance = new Instance();
            HQueUser = new HQueUser();
            ToInstance = new Instance();
        }

        public Instance Instance { get; set; }

        public Instance ToInstance { get; set; }

        public HQueUser HQueUser { get; set; }

        public List<StockTransferItem> StockTransferItems { get; set; }
        public BaseErrorLog ErrorLog { get; set; }
        public string FilePath { get; set; }
        public bool IsBold { get; set; }
        public bool? IsTransferSettings { get; set; }
        public bool SaveFailed { get; set; }       //Added by Settu for stock validation check
        public List<ProductStock> validateStockList { get; set; }

        public decimal? NetAmount
        {
            get
            {
                return StockTransferItems.Sum(s => s.Total);
            }
        }
        public decimal? TotalNoOfStrips
        {
            get
            {
                return StockTransferItems.Sum(s => (s.Quantity/s.PackageSize));
            }
        }
        /*GST Rleated fields  Added by Poongodi on 03/07/2017*/
        public decimal? TotalCGSTAmount
        {
            get
            {
                return StockTransferItems.Sum(s => s.CGSTAmount);
            }
        }
        public decimal? TotalSGSTAmount
        {
            get
            {
                return StockTransferItems.Sum(s => s.SGSTAmount);
            }
        }
        public decimal? TotalIGSTAmount
        {
            get
            {
                return StockTransferItems.Sum(s => s.IGSTAmount);
            }
        }
        public string FromTinNo { get; set; }
        public string ToTinNo { get; set; }
        public string TransferedStatus { get; set; }
		/*Added by Poongodi on 07/07/2017*/
        public long OpeningStock { get; set; }
        public int InwardQty { get; set; }
        public int OutwardQty { get; set; }
        public int AdjQty { get; set; }
        public decimal  TransferAmount { get; set; }
        public decimal AcceptedAmount { get; set; }
        public decimal RejectedAmount { get; set; }
        public decimal PendingAmount { get; set; }
        public DateTime? TransferToDate { get; set; }
        public bool IsToInstanceOfflineInstalled { get; set; }
        public List<ProductStock> TransactionStockList { get; set; }
    }
}
