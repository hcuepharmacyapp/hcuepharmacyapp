﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace HQue.Contract.Infrastructure.Inventory
{

    public class ReturnInvoice
    {
        public ReturnInvoice()
        {
            returnInvoiceList = new List<ReturnInvoiceList>();
            patientDetails = new PatientDetails();   // Added Gavaskar 13-02-2017
        }

        public PatientDetails patientDetails { get; set; }
        public decimal? total { set; get; }
        public decimal? discount { set; get; }
        public decimal? roundOff { set; get; }
        public decimal? netAmount { set; get; }
        public IEnumerable<ReturnInvoiceList> returnInvoiceList { set; get; }
    }
    public class ReturnInvoiceList
    {
        public ReturnInvoiceList()
        {
            salesInvoice = new SalesInvoice();
            salesReturnInvoice = new SalesReturnInvoice();
            salesReturnItemInvoice = new SalesReturnItemInvoice();
            productStockInvoice = new ProductStockInvoice();
            productInvoice = new ProductInvoice();
            instanceInvoice = new InstanceInvoice();
            hqueuserInvoice = new HqueuserInvoice();
            productInstance = new ProductInstanceInvoice();

        }
        public SalesInvoice salesInvoice { set; get; }
        public SalesReturnInvoice salesReturnInvoice { get; set; }
        public SalesReturnItemInvoice salesReturnItemInvoice { get; set; }
        public ProductStockInvoice productStockInvoice { get; set; }
        public ProductInvoice productInvoice { get; set; }
        public ProductInstanceInvoice productInstance { set; get; }
        public InstanceInvoice instanceInvoice { get; set; }
        public HqueuserInvoice hqueuserInvoice { get; set; }

    }
    public class SalesInvoice
    {
        public virtual string Name { get; set; }
        public virtual string Mobile { get; set; }
        public virtual string Email { get; set; }
        public virtual DateTime? DOB { get; set; }

        [Required]
        public virtual string InvoiceNo { get; set; }
        public virtual string InvoiceSeries { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string Address { get; set; }
        public virtual string PaymentType { get; set; }
        public virtual string DoctorName { get; set; }
        public virtual decimal? Discount { get; set; }
        public virtual string Pincode { get; set; }
        public virtual DateTime? CreatedAt { get; set; }
        

    }
    // Added Gavaskar 13-02-2017 Start
    public class PatientDetails
    {
        public virtual string EmpId { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string Pincode { get; set; }
    }
    // Added Gavaskar 13-02-2017 End
    public class SalesReturnInvoice
    {
        [Required]
        public virtual string ReturnNo { get; set; }
        public virtual DateTime? ReturnDate { get; set; }
        public virtual int? TaxRefNo { get; set; }
        public virtual decimal? ReturnCharges { get; set; } // Added for showing 'ReturnCharges' in print (Sales Return List) 
        public virtual DateTime? CreatedDate { get; set; } // Added by Sarubala on 18-07-17 o include created time 
        public virtual decimal? NetAmount { get; set; }
        public virtual decimal? RoundOffNetAmount { get; set; }
        public virtual decimal? TotalDiscountValue { get; set; }
        public virtual decimal? GstAmount { get; set; }
        public virtual decimal? ReturnItemAmount { get; set; }

    }
    public class SalesReturnItemInvoice
    {
        public virtual decimal? Quantity { get; set; }
        public virtual decimal? MrpSellingPrice { get; set; }
        public virtual decimal? Discount { get; set; }
        public virtual decimal? DiscountAmount { get; set; }
        public virtual decimal? MRP { get; set; }
        public virtual decimal? Igst { get; set; }
        public virtual decimal? Cgst { get; set; }
        public virtual decimal? Sgst { get; set; }
        public virtual decimal? GstTotal { get; set; }
    }
    public class ProductStockInvoice
    {
        [Required]
        public virtual string BatchNo { get; set; }
        [Required]
        public virtual DateTime? ExpireDate { get; set; }
        [Required]
        public virtual decimal? VAT { get; set; }
        [Required]
        public virtual decimal? SellingPrice { get; set; }
        public virtual decimal? MRP { get; set; }
    }
    public class ProductInvoice
    {
        [Required]
        public virtual string Name { get; set; }
        public virtual string Manufacturer { get; set; }        
    }
    public class ProductInstanceInvoice
    {
        public virtual string RackNo { get; set; }
        public virtual string BoxNO { get; set; }
    }
    public class InstanceInvoice
    {
        public virtual string Name { get; set; }
        public virtual string Phone { get; set; }
        public virtual string DrugLicenseNo { get; set; }
        public virtual string TinNo { get; set; }
        public virtual string GsTinNo { get; set; }
        public virtual string Address { get; set; }
        public virtual string Area { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Pincode { get; set; }
        [Required]
        public virtual bool? Status { get; set; }
        public virtual bool? isUnionTerritory { get; set; }
        public virtual bool? isGstSelect { get; set; }
    }

    public class HqueuserInvoice
    {
        public virtual string Name { set; get; }
    }


}
