﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Setup;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class StockTransferItem : BaseStockTransferItem
    {
        public StockTransferItem()
        {
            StockTransfer = new StockTransfer();
            ProductStock = new ProductStock();
            Instance = new Instance();
        }
        public virtual decimal? Quantity1 { get; set; }
        public ProductStock ProductStock { get; set; }
        //Added by nandhini for csv
        public long OpeningStock { get; set; }
        public int InwardQty { get; set; }
        public int OutwardQty { get; set; }
        public int AdjQty { get; set; }
        public string ReportTransferDate { get; set; }
        public string ReportExpiryDate { get; set; }
        
        //end
        public StockTransfer StockTransfer { get; set; }

        //public string VendorOrderItemId { get; set; } // Commented by Settu since it is added in table as reflected in base class
        public decimal? NoofStrip { get; set; }
        public bool IsTransferSettings { get; set; }

        public decimal? Total
        {
            get
            {
                if ((Quantity.HasValue && PurchasePrice > 0 && (IsTransferSettings==true)))
                {
                    return PurchasePrice * Quantity;
                }

                if (!(Quantity.HasValue && PurchasePrice.HasValue))
                    return 0;
                return (PurchasePrice * Quantity);
            }
        }
        /*GST Rleated fields  Added by Poongodi on 03/07/2017*/
        public decimal? CGSTAmount {
            get {
                if (!(Quantity.HasValue && PurchasePrice.HasValue && Cgst.HasValue  && GstTotal.HasValue))
                {
                    return 0;
                }
                return (((PurchasePrice * Quantity) - ((PurchasePrice * Quantity) * GstTotal / (100 + GstTotal)))* Cgst /100);
                    }
        }
        public decimal? SGSTAmount
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue && Sgst.HasValue && GstTotal.HasValue))
                {
                    return 0;
                }
                return (((PurchasePrice * Quantity) - ((PurchasePrice * Quantity) * GstTotal / (100 + GstTotal))) * Sgst / 100);
            }
         
        }
        public decimal? IGSTAmount
        {
            get
            {
                if (!(Quantity.HasValue && PurchasePrice.HasValue & Igst.HasValue))
                {
                    return 0;
                }
                return (PurchasePrice * Quantity) - ((PurchasePrice * Quantity) * 100 / (100 + Igst));
            }
        }
 
        public decimal? ActualAmount
        {
            get
            {
                if (!(Total.HasValue && VAT.HasValue))
                    return 0;
                return (Total * 100) / (VAT + 100);
            }
        }
        //public decimal? NoStrip
        //{
        //    get
        //    {
        //        if (!(Quantity.HasValue))
        //            return 0;
        //        return (Quantity/PackageSize);
        //    }
        //}


        public int? TaxRefNo { get; set; }
        public string FromTinNo { get; set; }
        public string ToTinNo { get; set; }
        public bool SaveFailed { get; set; }
        public BaseErrorLog ErrorLog { get; set; }
        public bool IsFromInstanceOfflineInstalled { get; set; }
        public bool IsAccept { get; set; }
        public Instance Instance { get; set; }
    }
}
