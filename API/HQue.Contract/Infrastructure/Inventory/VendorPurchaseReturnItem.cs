﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class VendorPurchaseReturnItem : BaseVendorReturnItem
    {
        public VendorPurchaseReturnItem()
        {
            ProductStock = new ProductStock();
            VendorPurchaseReturn = new VendorPurchaseReturn();
            VendorPurchaseItem = new VendorPurchaseItem();
        }
        public decimal? PurchaseQuantity { get; set; }
        public decimal? PackageQuantity { get; set; }
        public decimal? PackageSize { get; set; }
        public decimal? Total { get; set; }
        //added by nandhini for download csv
       public string ReturnNo { get; set; }
        public string ReportInvoiceDate { get; set; }
        public string TinNo { get; set; }

        //public decimal? Total
        //{
        //    get
        //    {
        //        if (!(Quantity.HasValue && VendorPurchaseItem.PackageSellingPrice.HasValue))
        //            return 0;
        //        return VendorPurchaseItem.PackageSellingPrice * Quantity;
        //    }

        //    set { }
        //}
        public decimal? ReturnTotal
        {
            get
            {
                if (!(Quantity.HasValue && ReturnPurchasePrice.HasValue))
                    return 0;
                return ReturnPurchasePrice * Quantity;
            }
        }

        public decimal? VatPrice
        {
            get
            {
                if (!(ReturnTotal.HasValue && ProductStock.VAT.HasValue))
                    return 0;
                return ReturnTotal * (ProductStock.VAT / 100);
            }
        }

        public decimal? VatValue
        {
            get
            {
                if (!(ReturnTotal.HasValue && ProductStock.VAT.HasValue))
                    return 0;
                return ReturnTotal * (ProductStock.VAT / (100 + ProductStock.VAT));
            }
        }

        public ProductStock ProductStock { get; set; }

        public VendorPurchaseItem VendorPurchaseItem { get; set; }

        public VendorPurchaseReturn VendorPurchaseReturn { get; set; }

        //public decimal? ReturnPurchasePrice { get; set; }
        public decimal? ReturnedQuantity { get; set; }
        public decimal? FreeQty { get; set; }
       
        public string VendorPurchaseId { get; set; }
      ////  public decimal? Discount { get; set; } // Comment by Gavaskar 05-09-2017

        /*GST Rleated fields  Added by Settu on 28/08/2017*/
        public decimal? CGSTAmount
        {
            get
            {
                if (!(Quantity.HasValue && ReturnPurchasePrice.HasValue && Cgst.HasValue && GstTotal.HasValue))
                {
                    return 0;
                }
                return (((ReturnPurchasePrice * Quantity) - ((ReturnPurchasePrice * Quantity) * GstTotal / (100 + GstTotal))) * Cgst / 100);
            }
        }
        public decimal? SGSTAmount
        {
            get
            {
                if (!(Quantity.HasValue && ReturnPurchasePrice.HasValue && Sgst.HasValue && GstTotal.HasValue))
                {
                    return 0;
                }
                return (((ReturnPurchasePrice * Quantity) - ((ReturnPurchasePrice * Quantity) * GstTotal / (100 + GstTotal))) * Sgst / 100);
            }

        }
        public decimal? IGSTAmount
        {
            get
            {
                if (!(Quantity.HasValue && ReturnPurchasePrice.HasValue & Igst.HasValue))
                {
                    return 0;
                }
                return (ReturnPurchasePrice * Quantity) - ((ReturnPurchasePrice * Quantity) * 100 / (100 + Igst));
            }
        }
        public decimal? GSTAmount1
        {
            get
            {
                if (!(Quantity.HasValue && ReturnPurchasePrice.HasValue & GstTotal.HasValue))
                {
                    return 0;
                }
                return (ReturnPurchasePrice * Quantity * GstTotal) / (100 + GstTotal);
            }
        }
    }
}
