﻿namespace HQue.Contract.Infrastructure.Inventory
{
    public interface IInvoiceItem   
    {
        decimal? Quantity { get; set; }
        decimal? SellingPrice { get;  }
        decimal? Discount { get; }
        decimal? DiscountAmount { get; set; }
        decimal? VatAmount { get; }
        ProductStock ProductStock { get; }
        decimal? Total { get; }
        decimal? ReturnedQty { get; }
        decimal? MRP { get; set; }
        decimal? MRPPerStrip { get; set; }
        decimal? SellingPricePerStrip { get; set; }
        decimal? Igst { get; set; }
        decimal? IgstValue { get; set; }
        decimal? Cgst { get; set; }
        decimal? CgstValue { get; set; }
        decimal? Sgst { get; set; }
        decimal? SgstValue { get; set; }
        decimal? GstTotal { get; set; }
        decimal? GstTotalValue { get; set; }
        decimal? NetAmount { get; }
        //Added by Sarubala on 10/05/2018 for custom template
        decimal? MRPAfterDiscount { get; set; }
        decimal? SellingPriceAfterDiscount { get; set; }
        decimal? MRPValueAfterDiscount { get; set; }
        decimal? SellingPriceValueAfterDiscount { get; set; }
    }
}