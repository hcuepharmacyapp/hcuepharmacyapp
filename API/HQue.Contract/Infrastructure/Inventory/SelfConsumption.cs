﻿using HQue.Contract.Base;
using System;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SelfConsumption : BaseSelfConsumption
    {
        public SelfConsumption()
        {
            ProductStock = new ProductStock();
        }
        public ProductStock ProductStock { get; set; }
        public virtual DateTime? ConsumptionDate { get; set; }

        //added by nandhini 
        public string ReportInvoiceDate { get; set; }
        public string ReportExpireDate { get; set; }
        public string BatchNo { get; set; }
        

    }
}
