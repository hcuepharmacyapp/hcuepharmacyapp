﻿using HQue.Contract.Infrastructure.Inventory;

namespace HQue.WebCore.ViewModel
{
    public class InvoiceVM
    {
        public IInvoice Sales { get; set; }
        public VendorPurchase VendorPurchase { get; set; }
        public string FilePath { get; set; }
        public int PrinterType { get; set; }
        public bool IsReturnsOnly { get; set; }
    }
}
