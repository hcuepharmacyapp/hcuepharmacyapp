﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesItemAudit : BaseSalesItemAudit
    {


        public SalesItemAudit()
        {
            Sales = new Sales();
            ProductStock = new ProductStock();
        }

        public ProductStock ProductStock { get; set; }

        public Sales Sales { get; set; }
        public string UpdatedByUser { get; set; }

    }
}
