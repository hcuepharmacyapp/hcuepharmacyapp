﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure.UserAccount;
using System;
namespace HQue.Contract.Infrastructure.Inventory
{
    public class PhysicalStockHistory : BasePhysicalStockHistory
    {
        public PhysicalStockHistory()
        {
            Product = new Product();
        }
        public Product Product { get; set; }
        public HQueUser User { get; set; }
        public decimal? StockDifference { get; set; }
        //added by anndhini for csv
        public string ReportCreateDate { get; set; }
        public string ReportOpenDate { get; set; }
        public string ReportCloseDate { get; set; }
    }
}
