﻿using System;
using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesCoordinateGstReturn
    {
        
        //Added by Bikas for GST Report 06/06/2018
        public decimal? GSTReturnValue { get; set; }
        public decimal? gstExcempted_ReturnValue { get; set; }
        public decimal? Gst0_Value { get; set; }
        public decimal? Gst0_TotalReturnValue { get; set; }
        public decimal? Gst0_ReturnValue { get; set; }
        public decimal? Gst5_Value { get; set; }
        public decimal? Gst5_TotalReturnValue { get; set; }
        public decimal? Gst5_ReturnValue { get; set; }
        public decimal? Gst12_Value { get; set; }
        public decimal? Gst12_TotalReturnValue { get; set; }
        public decimal? Gst12_ReturnValue { get; set; }
        public decimal? Gst18_Value { get; set; }
        public decimal? Gst18_TotalReturnValue { get; set; }
        public decimal? Gst18_ReturnValue { get; set; }
        public decimal? Gst28_Value { get; set; }
        public decimal? Gst28_TotalReturnValue { get; set; }
        public decimal? Gst28_ReturnValue { get; set; }
        public decimal? GstOther_Value { get; set; }
        public decimal? GstOther_TotalReturnValue { get; set; }
        public decimal? GstOther_ReturnValue { get; set; }
        public virtual string ReturnNo { get; set; }
        public virtual DateTime? ReturnDate { get; set; }
        public decimal? FinalValue { get; set; }
    }
}
