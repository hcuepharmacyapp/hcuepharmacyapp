﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
namespace HQue.Contract.Infrastructure.Inventory
{
    public class StockLedger
    {
        public StockLedger()
        {
            SelectedProduct = new Product();
        }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public Product SelectedProduct { get; set; }
        public Transactions TransactionType { get; set; }
        public string TransactionTypeName { get; set; }
        public string TransactionId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string TransactionNo { get; set; }
        public string CustomerName { get; set; }
        public decimal? Quantity { get; set; }
        public string BatchNo { get; set; }
        public DateTime? ExpireDate { get; set; }
        public int? StockType { get; set; }
        public string ToInstance { get; set; }
        public string GoodsRcvNo { get; set; }
        public string BillSeries { get; set; }
        //added by nandhini 13.9.17
        public decimal? Stock { get; set; }
        public decimal? OpeningStock1 { get; set; }

    }
    public enum Transactions
    {
        None = 0,
        Sales = 1,
        SalesReturn = 2,
        SalesCancel = 3,
        VendorPurchase = 4,
        PurchaseReturn = 5,
        TransferIn = 6,
        TransferOut = 7,
        DcStock = 8,
        TempStock = 9,
        SelfConsumption = 10,
        StockAdjustment = 11,
        OpeningStock = 12,
        StockImport = 13,
        TransferPending = 14,
        TransferRejected = 15,
        VendorPurchaseCancel = 16, //Added by Poongodi to display the PO Cancel details
        VendorPurchaseDelete = 17, //Added by Sabarish to display the Purchase Delete
        ReturnDelete = 18,  //Added by Sarubala to display Return Delete items
        ReturnCancel = 19 ,  //Added by Sarubala to display Return Cancel items
        CurrentStock = 20  //Added by nandhini to display current stock value

    }
    public enum StockType1
    {
        Out = 1,
        In = 2
    }
}
