﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesOrderEstimateItem : BaseSalesOrderEstimateItem
    {
        public SalesOrderEstimateItem()
        {
            Product = new Product();
            ProductStock = new Inventory.ProductStock();
            ProductMaster = new ProductMaster();
        }
        public Product Product { get; set; }
        public ProductStock ProductStock { get; set; }
        public ProductMaster ProductMaster { get; set; }
        public string PatientId { get; set; }
        public string ProductName { get; set; }

        // Added by Gavaskar 15-11-2017
        public string ProductMasterID { get; set; }
        public string Manufacturer { get; set; }
        public string Schedule { get; set; }
        public string GenericName { get; set; }
        public string Category { get; set; }
        public string Packing { get; set; }
    }
}
