﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class PurchaseDetails
    {
        public PurchaseDetails()
        {
            vendorPurchaseItem = new VendorPurchaseItem();
            getPreviousPurchase = new List<VendorPurchaseItem>();
        }
        public VendorPurchaseItem vendorPurchaseItem { get; set; }
        public List<VendorPurchaseItem> getPreviousPurchase { get; set; }


    }
}
