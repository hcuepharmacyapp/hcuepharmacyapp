﻿using System;
using System.Collections.Generic;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using Newtonsoft.Json;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesReportTally: BaseVendor
    {
        //Added by Bikas for Tally Report 22/06/2018
        public virtual string InvoiceNo { get; set; }
        public virtual string Vouchertype { get; set; }
        public virtual DateTime? InvoiceDate { get; set; }
        public virtual string Billtype { get; set; }
        public virtual string Gstin { get; set; }
        public virtual string HsnCode { get; set; }
        public string CustomerName { get; set; }
        public string ItemName { get; set; }
        public string Empty { get; set; }
        public decimal? Igst { get; set; }
        public decimal? Cgst { get; set; }
        public decimal? Sgst { get; set; }
        public decimal? Gstpercentage { get; set; }
        public decimal? GstAmount { get; set; }
        public decimal? ItemAmount { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Amount { get; set; }
        public decimal? CGSTAmount { get; set; }
        public decimal? SGSTAmount { get; set; }
        public decimal? DiscountValue { get; set; }
        public decimal? RoundOff { get; set; }
        public decimal? GrandTotal { get; set; }
        public override string StateId { get; set; }
        public override string Name { get; set; }
        public override int? VendorType { get; set; }
        public virtual string VendorTypeDesc { get; set; }
        public virtual string ProductCode { get; set; }
        public virtual string SaleCode { get; set; }
        public virtual string Remark1 { get; set; }
        public virtual string Remark2 { get; set; }
        public virtual string IGSTAmount { get; set; }
    }
}
