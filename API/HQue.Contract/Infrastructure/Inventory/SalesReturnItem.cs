﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Misc;
using System;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Inventory
{
    public class SalesReturnItem : BaseSalesReturnItem
    {
        public SalesReturnItem()
        {
            ProductStock = new ProductStock();
            SalesReturn = new SalesReturn();

            //SalesItem = new List<SalesItem>();


        }
       
        public decimal? GSTReturnValue { get; set; }        
        public decimal? PurchaseQuantity { get; set; }
        public decimal? SoldPrice { get; set; }
        //public SalesItem SalesItem { get; set; }
        public decimal? Total
        {
            get
            {
                if ((Quantity.HasValue && MrpSellingPrice > 0))
                {
                    return MrpSellingPrice * Quantity;
                }
                if (!(Quantity.HasValue && ProductStock.SellingPrice.HasValue))
                    return 0;
                return ProductStock.SellingPrice * Quantity;
            }

            set { }
        }
        public decimal? OnlyReturnAmount
        {
            get
            {
                if ((Quantity.HasValue && SellingPrice > 0))
                {
                    return SellingPrice * Quantity;
                }
                if (!(Quantity.HasValue && ProductStock.SellingPrice.HasValue))
                    return 0;
                return ProductStock.SellingPrice * Quantity;
            }

            set { }
        }
        public decimal? ActualAmount
        {
            get
            {
                if (!(Total.HasValue && ProductStock.VAT.HasValue))
                    return 0;
                return (Total * 100) / (ProductStock.VAT + 100);
            }
        }

        public decimal? VatAmount
        {
            get
            {
                if (!(Total.HasValue && ActualAmount.HasValue))
                    return 0;
                return Total - ActualAmount;
            }
        }
        public decimal? SalesReportTotal
        {
            get
            {
                if ((Quantity.HasValue && MrpSellingPrice > 0))
                {
                    if ((Discount > 0))
                        return (SellingPrice - ((SellingPrice * Discount) / 100)) * Quantity;

                    return MrpSellingPrice * MrpSellingPrice;
                }

                if (!(Quantity.HasValue && ProductStock.SellingPrice.HasValue))
                    return 0;

                if ((Quantity.HasValue && ProductStock.SellingPrice.HasValue) && Discount > 0)
                    return (ProductStock.SellingPrice - ((ProductStock.SellingPrice * Discount) / 100)) * Quantity;

                return ProductStock.SellingPrice * Quantity;

            }


            set { }
        }
        public decimal? ReportActualAmount
        {
            get
            {
                if (!(SalesReportTotal.HasValue && ProductStock.VAT.HasValue))
                    return 0;
                return (SalesReportTotal * 100) / (ProductStock.VAT + 100);
            }
        }
        public decimal? ReportVatAmount
        {
            get
            {
                if (!(SalesReportTotal.HasValue && ReportActualAmount.HasValue))
                    return 0;
                return SalesReportTotal - ReportActualAmount;
            }
        }
        public decimal? SalesDiscountValue
        {
            get
            {
                if ((Quantity.HasValue && MrpSellingPrice > 0))
                {
                    if ((Discount > 0))
                        return (((MrpSellingPrice * Discount) / 100) * Quantity) + DiscountAmount;
                    return 0;
                }

                if (!(Quantity.HasValue && ProductStock.SellingPrice.HasValue))
                    return 0;

                if ((Quantity.HasValue && ProductStock.SellingPrice.HasValue) && Discount > 0)
                    return (((ProductStock.SellingPrice * Discount) / 100) * Quantity) + DiscountAmount;

                return 0;
            }

            set { }
        }

        //public decimal? DiscountAmount
        //{
        //    get
        //    {
        //        if (!(Total.HasValue && Discount.HasValue))
        //            return 0;
        //        return ((Discount / 100) * Total);
        //    }
        //    set { }
        //}

        public decimal? FinalPrice
        {
            get
            {
                if (!(Total.HasValue && SalesReturn.Discount.HasValue && Discount.HasValue))
                    return 0;
                else
                {
                    return (Total - (((SalesReturn.Discount + Discount) / 100) * Total));
                }


            }
        }
        //Added by Sarubala on 13-11-17        
        public decimal? BillValue { get; set; }

        //public decimal? Discount { get; set; }

        public ProductStock ProductStock { get; set; }

        public SalesReturn SalesReturn { get; set; }


        public decimal? ReturnedQuantity { get; set; } = 0;
        public decimal? ReturnAmt { get; set; }
        public decimal? SellingPrice { get; set; }
        public decimal? SalesRoundOff { get; set; }
        public decimal? returndisct { get; set; }
        public decimal? returnsellprice { get; set; }
        public decimal? rettotsellprice { get; set; }
        public decimal? TotalPurchasePrice { get; set; }
        public decimal? TotalMrp { get; set; }
        public int? TotalSalesReturn { get; set; }
        public decimal? ReturnProfit { get; set; }
        public string CancelledBy { get; set; }
        public DateRange Dates { get; set; }
        public string select { get; set; }
        public string selectedValue { get; set; }
        public comparison select1 { get; set; }
        public string CancelledDateTime { get; set; }
        public decimal? vatValue { get; set; }
        public decimal? TotalWOvat { get; set; }
        public string saleid { get; set; }

        public string BatchNo { get; set; }
        public DateTime? ExpireDate { get; set; }
        public decimal? VAT { get; set; }
        public decimal? ReturnedQty { get; set; }
        public decimal? SellingPricePerStrip { get; set; }
        public decimal? MRPPerStrip { get; set; }
        public override decimal? Igst { get; set; }
        public decimal? IgstValue { get; set; }
        public override decimal? Cgst { get; set; }
        public decimal? CgstValue { get; set; }
        public override decimal? Sgst { get; set; }
        public decimal? SgstValue { get; set; }
        public override decimal? GstTotal { get; set; }
        public decimal? GstTotalValue { get; set; }


        public string Action { get; set; }
        public string UpdatedUser { get; set; }
        public string HsnCode { get; set; }
        public decimal? originaldiscount { get; set; }

        // Added by Gavaskar 21-11-2017  
        public string SalesOrderEstimateId { get; set; }
        public int SalesOrderProductSelected { get; set; }
        public int SalesEstimateProductSelected { get; set; }

        //Added by Sarubala on 10/05/2018 for custom template
        public decimal? MRPAfterDiscount { get;set; }
        public decimal? SellingPriceAfterDiscount { get; set; }
        public decimal? MRPValueAfterDiscount { get; set; }
        public decimal? SellingPriceValueAfterDiscount { get; set; }

        public decimal? RedeemAmtPerItem { get; set; } //Added by Sarubala on 14-09-18
    }

    public enum comparison
    {
        Greater = 1,
        Equal = 2,
        Lesser = 3
    }


}
