﻿using HQue.Contract.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.Infrastructure.Settings
{
    public class CustomSettingsDetail : BaseCustomSettingsDetail
    {
        public string CustomSettingsGroupName { get; set; }
    }
}
