﻿using HQue.Contract.Base;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Settings
{
    public class LoyaltyPointSettings : BaseLoyaltyPointSettings
    {
        public LoyaltyPointSettings()
        {
            LoyaltyProductPoints = new List<LoyaltyProductPoints>();
        }
        public List<LoyaltyProductPoints> LoyaltyProductPoints { get; set; }
    }
}


