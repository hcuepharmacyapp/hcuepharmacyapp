﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Settings
{
    public class SalesBatchPopUpSettings : BaseSalesBatchPopUpSettings
    {
        public SalesBatchPopUpSettings()
        {
            Quantity = false;
            Batch = false;
            Expiry = false;
            Vat = false;
            UnitMrp = false;
            StripMrp = false;
            UnitsPerStrip = false;
            Profit = false;
            RackNo = false;
            Generic = false;
            Mfg = false;
            Category = false;
            Supplier = false;
            Age = false;
        }
    }
}
