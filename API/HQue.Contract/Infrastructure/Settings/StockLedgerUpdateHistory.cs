﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Settings
{
    public class StockLedgerUpdateHistory : BaseStockLedgerUpdateHistory
    {
        public StockLedgerUpdateHistory()
        {
            StockLedgerUpdateHistoryItem = new List<StockLedgerUpdateHistoryItem>();
            StockLedgerUpdateHistoryDiffQtyItem = new List<StockLedgerUpdateHistory>();
        }
        public string ProductName { get; set; }
        public string ProductId { get; set; }
        public string Stockid { get; set; }
        public bool IsEdit { get; set; }
        public decimal? TransferInNotAccQty { get; set; }
        public decimal? Stock { get; set; }
        public string BatchNo { get; set; }
        public DateTime ExpireDate { get; set; }
        public decimal? ActualStock { get; set; }
        public decimal? ReturnQty { get; set; }
        public List<StockLedgerUpdateHistoryItem> StockLedgerUpdateHistoryItem { get; set; }
        public List<StockLedgerUpdateHistory> StockLedgerUpdateHistoryDiffQtyItem { get; set; }
        public bool isChecked { get; set; }
    }
}

