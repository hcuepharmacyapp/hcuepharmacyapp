﻿using HQue.Contract.Base;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Settings
{
    public class Requirements : BaseRequirement
    {
        public Requirements()
        {
        }

        public RequirementType RequirementStatus { get; set; }

        public string InstanceName { get; set; }

    }

    public enum RequirementType
    {
        Requirement = 1,
        Issue = 2
    }
}


