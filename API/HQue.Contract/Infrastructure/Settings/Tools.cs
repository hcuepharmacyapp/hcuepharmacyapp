﻿using HQue.Contract.Base;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Settings
{
    public class Tools : BaseTool
    {
        public Tools()
        {
        }

        public string AccountName { get; set; }
        public string InstanceName { get; set; }
        public string ProductName { get; set; }
        public decimal? Stock { get; set; }
        public string Phone { get; set; }
        //added by nandhini for live sales report on 23.10.17
        public string GroupName { get; set; }
        public string PharmaName { get; set; }
        public string City { get; set; }
        public string Area { get; set; }
        public string SALESNET { get; set; }
        public string TOTALSALES { get; set; }
        public string PURCHASENET { get; set; }
        public string TOTALPURCHASECOUNT { get; set; }

        public string Name { get; set; }
        public string RegistrationDate { get; set; }
        public string Mobile { get; set; }
        public string DrugLicenseNo { get; set; }
        public string TinNo { get; set; }
        public string ContactMobile { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string SecondaryName { get; set; }
        public string SecondaryMobile { get; set; }
        public string SecondaryEmail { get; set; }
        public string ContactDesignation { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Pincode { get; set; }
        public string Landmark { get; set; }       
        public string BuildingName { get; set; }
        public string DrugLicenseExpDate { get; set; }
        public string StreetName { get; set; }
        public string Country { get; set; }         
        public string GsTinNo { get; set; }            

        //end
        public bool IsOfflineInstalled { get; set; }

        public int? OfflineVersionNo { get; set; }
    }
}


