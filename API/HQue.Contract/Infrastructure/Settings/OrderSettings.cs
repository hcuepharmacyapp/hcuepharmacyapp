﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Master;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Settings
{
    public class OrderSettings : BaseOrderSettings
    {
        public OrderSettings()
        {
            vendor = new Vendor();
        }

        public string HeadOffice_InstanceName { get; set; }
        public string HeadOffice_InstanceId { get; set; }
        public bool? IsHeadOfficeEnabled { get; set; }

        public Vendor vendor { get; set; }
       
    }
}


