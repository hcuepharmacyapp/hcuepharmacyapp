﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Settings
{
    public class StockLedgerUpdateHistoryItem : BaseStockLedgerUpdateHistoryItem
    {
        public string ProductName { get; set; }
        public string Stockid { get; set; }
    }
   
}
