﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Setup;
using System.Collections.Generic;
using System.Linq;

namespace HQue.Contract.Infrastructure.UserAccount
{
    public class HQueUser : BaseHQueUser
    {
        public HQueUser()
        {
            Instance = new Instance();
            RoleAccess = new List<UserAccess>();
            UserAccess = new UserAccess();
        }
        

        public const int StatusActive = 1;
        public const int StatusInactive = 2;

        public bool HasAccountId => !string.IsNullOrEmpty(AccountId);

        public bool HasInstanceId => !string.IsNullOrEmpty(InstanceId);
       
        public string ConfirmPassword { get; set; }

        public Instance Instance{ get; set; }

        public string CurrentUrl { get; set; }

        public string UserPermission { get
            {
                if (RoleAccess == null)
                    return string.Empty;

                return $"{{{string.Join("}{", RoleAccess.Select(x => x.AccessRight))}}}";

            } }

        public UserAccess UserAccess { get; set; }

        public IEnumerable<UserAccess> RoleAccess { get; set; }

        public string ShortCutKeysType { get; set; }
        public string NewWindowType { get; set; }
        public bool IsGstEnabled { get; set; }  //GSTEnabled Added by Poongodi on 29/06/2017

    }
}
