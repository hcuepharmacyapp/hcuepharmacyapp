﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Setup;
using System.Collections.Generic;
using System.Linq;

namespace HQue.Contract.Infrastructure.UserAccount
{
    public class LoginHistory : BaseLoginHistory
    {
        public virtual string BranchName { get; set; }
        public virtual string ContactEmail { get; set; }

        public virtual string UserName { get; set; }



    }
   }
