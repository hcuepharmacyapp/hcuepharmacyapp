﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.UserAccount
{
    public class UserAccess : BaseUserAccess
    {
        public const int Admin = 1;
        public const int Sell = 2;
        public const int Buy = 3;
        public const int Leads = 4;
        public const int Order = 5;
        public const int Profile = 6;
        public const int Inventory = 7;
        public const int Reports = 8;
        public const int Branch = 9;
        public const int Accounts = 10;
        public const int UserManagement = 11;
        public const int EditMrp = 12;
        public const int PurchaseEdit = 13;
        public const int StockTransfer = 14;
        public const int SalesEdit = 15;
        public const int SalesCancel = 16;
		public const int BillDateEdit = 17;
        public const int Estimate = 18;
        public const int EstimateEdit = 19;
        public const int Faq = 20;
		public const int Dashboard = 25;
        public const int AddNewProduct = 26;
        public const int PurchaseCancel = 27; //Added by Poongodi on 06/03/2017
        public const int SalesTempStock = 28; //by San
        public const int StockAdjustMent = 29; //by San        
        public const int VendorProfile = 30;
        public const int Settings = 31;

        //newly added for Edge functionality on 09/22/2016
        //public const int PharmacyInfo = 15;
        //public const int CreateLicense = 16;
        //public const int Payment = 17;
        //public const int PharmacyListing = 18;
        //public const int PharmacyEdit = 19;
        //public const int DrugListing = 20;
        //public const int DrugEdit = 21;
        //public const int HQueManagement = 22;
        //public const int HQueManagementAdmin = 23;
    }
}
