﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.UserAccount;
using System;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Setup
{
    public class Instance : BaseInstance
    {
        public Instance()
        {
            UserList = new List<HQueUser>();
        }

        public IEnumerable<HQueUser> UserList { get; set; }

        public List<PharmacyTiming> Timings { get; set; }

        public List<PharmacyUploads> Images { get; set; }
        //public string FullAddress => $"{Address} {Area} {Landmark} {City} {State} {Pincode}".Replace("  ", " ");
        //////
        public string FullAddress
        {
            get
            {
                //if (BuildingName != null)
                if (!String.IsNullOrEmpty(Lat))
                {
                    //return $"{BuildingName}, {StreetName}, {Area}, {Landmark}, {City}, {State} {Pincode}".Replace("  ", " ");
                    var fullAddress = "";
                    if (!String.IsNullOrEmpty(Address)) {
                        fullAddress += $"{Address}, ";
                    }
                    if (!String.IsNullOrEmpty(Area))
                    {
                        fullAddress += $"{Area}, ";
                    }
                    if (!String.IsNullOrEmpty(Landmark))
                    {
                        fullAddress += $"{Landmark}, ";
                    }
                    fullAddress += $"{City}, {State} {Pincode}".Replace("  ", " ");
                    return fullAddress;
                }
                else
                {
                    return $"{Address} {Area} {Landmark} {City} {State} {Pincode}".Replace("  ", " ");
                }
            }
        }

        public string Website { get; set; }
        public string FullAddress1 { get; set; }
        public string AccountName { get; set; }
        public bool? IsBillNumberReset { get; set; }
        public int? InstanceCount { get; set; }
        public bool isEmail { get; set; } // Added by Lawrence

        public decimal? StockCount { get; set; }
        public decimal? StockValue { get; set; }
        public decimal? StockValueGst { get; set; }
        public bool IsChecked { get; set; }

    }
}
