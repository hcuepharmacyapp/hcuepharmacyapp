﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Setup
{
    public class PharmacyPayment: BasePharmacyPayment
    {
        public PharmacyPayment()
        {

        }
        
        public decimal? Amount
        {
            get {
                decimal? netAmount = Cost * Quantity;
                decimal? discountAmt = DiscountType == true ? Discount: netAmount * (Discount/100);
                decimal ? amount = netAmount - discountAmt;
                
                return amount + (amount * (Tax/100));
            }
        }

        public decimal? Balance
        {
            get
            {
                return Amount - Paid;
            }
        }
    }
}
