﻿using HQue.Contract.Base;

namespace HQue.Contract.Infrastructure.Setup
{
    public class PharmacyTiming : BasePharmacyTiming
    {
        public PharmacyTiming()
        {

        }

        public Morning morning { get; set; }
        public Noon noon { get; set; }
        public string day { get; set; }
    }
    
    public class Morning
    {
        public string startTime { get; set; }
        public string endTime { get; set; }
    }

    public class Noon
    {
        public string startTime { get; set; }
        public string endTime { get; set; }
    }


}
