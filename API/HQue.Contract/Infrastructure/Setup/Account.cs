﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.UserAccount;
using System;

namespace HQue.Contract.Infrastructure.Setup
{
    public class Account : BaseAccount
    {

        public Account()
        {
            User = new HQueUser();
            FinYearMaster = new FinYearMaster();
        }

        public HQueUser User { get; set; }
        public FinYearMaster FinYearMaster { get; set; }
        public virtual DateTime? FromDate { get; set; }
        public virtual DateTime? ToDate { get; set; }

        public string FullAddress { get { return $"{Address} {Area} {City} {State} {Pincode}".Replace("  ",""); } }

    }
}
