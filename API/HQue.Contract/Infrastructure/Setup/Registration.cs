﻿using HQue.Contract.Base;
using HQue.Contract.Infrastructure.UserAccount;
using System.Collections.Generic;

namespace HQue.Contract.Infrastructure.Setup
{
    public class Registration: BaseContract
    {

        public Registration()
        {
            account = new Account();
            instance = new Instance();
            //PharmacyPayment = new PharmacyPayment();
            payments = new List<PharmacyPayment>();

            //user = new HQueUser();
        }

        public Account account { get; set; }
        public Instance instance { get; set; }
        //public PharmacyPayment PharmacyPayment { get; set; }
        public List<PharmacyPayment> payments { get; set; }
        
        //public HQueUser user { get; set; }
        //public string FullAddress { get { return $"{Address} {Area} {City} {State} {Pincode}".Replace("  ",""); } }

    }
}
