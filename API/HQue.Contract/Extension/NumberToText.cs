﻿using System;

namespace HQue.Contract.Extension
{
    public static class NumberToText
    {
        public static string ConvertNumbertoWords(this long number)
        {
            if (number == 0) return "Zero";
            if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
            var words = "";
            if ((number / 1000000) > 0)
            {
                words += ConvertNumbertoWords(number / 100000) + " Lakhs ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " Thousand ";
                number %= 1000;
            }
            if (number / 100 > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number <= 0)
                return words;

            if (words != "") words += "And ";
            var unitsMap = new[]
                {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
            var tensMap = new[]
                {"Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
            if (number < 20) words += unitsMap[number];
            else
            {
                words += tensMap[number / 10];
                if (number % 10 > 0) words += " " + unitsMap[number % 10];
            }
            return words;
        }
    }
}
