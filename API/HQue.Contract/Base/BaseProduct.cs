
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseProduct : BaseContract
{




public virtual string  Code { get ; set ; }




[Required]
public virtual string  Name { get ; set ; }





public virtual string  Manufacturer { get ; set ; }





public virtual string  KindName { get ; set ; }





public virtual string  StrengthName { get ; set ; }





public virtual string  Type { get ; set ; }





public virtual string  Schedule { get ; set ; }





public virtual string  Category { get ; set ; }





public virtual string  GenericName { get ; set ; }





public virtual string  CommodityCode { get ; set ; }





public virtual string  Packing { get ; set ; }





public virtual string  Ext_RefId { get ; set ; }





public virtual int ? PackageSize { get ; set ; }





public virtual decimal ? VAT { get ; set ; }





public virtual decimal ? Price { get ; set ; }





public virtual int ? Status { get ; set ; }





public virtual string  RackNo { get ; set ; }





public virtual string  ProductMasterID { get ; set ; }





public virtual string  ProductOrgID { get ; set ; }





public virtual decimal ? ReOrderLevel { get ; set ; }





public virtual decimal ? ReOrderQty { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual string  Eancode { get ; set ; }





public virtual bool ?  IsHidden { get ; set ; }





public virtual string  HsnCode { get ; set ; }





public virtual decimal ? Igst { get ; set ; }





public virtual decimal ? Cgst { get ; set ; }





public virtual decimal ? Sgst { get ; set ; }





public virtual decimal ? GstTotal { get ; set ; }





public virtual string  Subcategory { get ; set ; }



}

}
