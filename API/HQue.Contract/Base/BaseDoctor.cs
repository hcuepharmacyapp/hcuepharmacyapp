
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseDoctor : BaseContract
{




public virtual string  Code { get ; set ; }





public virtual string  Name { get ; set ; }





public virtual string  ShortName { get ; set ; }





public virtual string  ClinicName { get ; set ; }





public virtual string  Mobile { get ; set ; }





public virtual string  Phone { get ; set ; }





public virtual string  Email { get ; set ; }





public virtual string  Area { get ; set ; }





public virtual string  City { get ; set ; }





public virtual string  State { get ; set ; }





public virtual string  Pincode { get ; set ; }





public virtual string  GsTin { get ; set ; }





public virtual decimal ? Commission { get ; set ; }



}

}
