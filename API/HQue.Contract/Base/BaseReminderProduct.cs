
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseReminderProduct : BaseContract
{




public virtual string  UserReminderId { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual string  ProductName { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual int ? ReminderFrequency { get ; set ; }



}

}
