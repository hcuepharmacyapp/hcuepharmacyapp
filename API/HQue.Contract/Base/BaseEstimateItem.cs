
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseEstimateItem : BaseContract
{




public virtual string  EstimateId { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual string  ProductId { get ; set ; }





public virtual string  ProductName { get ; set ; }





public virtual int ? ReminderFrequency { get ; set ; }





public virtual DateTime ? ReminderDate { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? SellingPrice { get ; set ; }



}

}
