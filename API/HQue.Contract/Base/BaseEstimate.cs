
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseEstimate : BaseContract
{



[Required]
public virtual string  EstimateNo { get ; set ; }





public virtual DateTime ? EstimateDate { get ; set ; }





public virtual string  PatientId { get ; set ; }





public virtual string  Name { get ; set ; }





public virtual string  Mobile { get ; set ; }





public virtual string  Email { get ; set ; }





public virtual DateTime ? DOB { get ; set ; }





public virtual int ? Age { get ; set ; }





public virtual string  Gender { get ; set ; }





public virtual string  Address { get ; set ; }





public virtual string  Pincode { get ; set ; }





public virtual string  DoctorName { get ; set ; }





public virtual string  DoctorMobile { get ; set ; }





public virtual string  DeliveryType { get ; set ; }





public virtual bool ?  BillPrint { get ; set ; }





public virtual bool ?  SendSms { get ; set ; }





public virtual bool ?  SendEmail { get ; set ; }





public virtual decimal ? Discount { get ; set ; }



}

}
