
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseHQueUser : BaseContract
{



[Required]
public virtual string  UserId { get ; set ; }





public virtual string  Password { get ; set ; }




[Required]
public virtual int ? UserType { get ; set ; }




[Required]
public virtual int ? Status { get ; set ; }





public virtual string  Name { get ; set ; }





public virtual string  Mobile { get ; set ; }





public virtual string  Address { get ; set ; }





public virtual string  Area { get ; set ; }





public virtual string  City { get ; set ; }





public virtual string  State { get ; set ; }





public virtual string  Pincode { get ; set ; }





public virtual string  PasswordResetKey { get ; set ; }





public virtual DateTime ? ResetRequestTime { get ; set ; }



public virtual bool? IsOnlineEnable { get; set; }



}

}
