
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseInstance : BaseContract
{




public virtual string  ExternalId { get ; set ; }





public virtual string  Code { get ; set ; }





public virtual string  Name { get ; set ; }





public virtual DateTime ? RegistrationDate { get ; set ; }





public virtual string  Phone { get ; set ; }





public virtual string  Mobile { get ; set ; }





public virtual string  DrugLicenseNo { get ; set ; }





public virtual string  TinNo { get ; set ; }





public virtual string  ContactName { get ; set ; }





public virtual string  ContactMobile { get ; set ; }





public virtual string  ContactEmail { get ; set ; }





public virtual string  SecondaryName { get ; set ; }





public virtual string  SecondaryMobile { get ; set ; }





public virtual string  SecondaryEmail { get ; set ; }





public virtual string  ContactDesignation { get ; set ; }





public virtual string  BDOId { get ; set ; }





public virtual string  BDODesignation { get ; set ; }





public virtual string  BDOName { get ; set ; }





public virtual string  RMName { get ; set ; }





public virtual string  Address { get ; set ; }





public virtual string  Area { get ; set ; }





public virtual string  City { get ; set ; }





public virtual string  State { get ; set ; }





public virtual string  Pincode { get ; set ; }





public virtual string  Landmark { get ; set ; }




[Required]
public virtual bool ?  Status { get ; set ; }





public virtual DateTime ? DrugLicenseExpDate { get ; set ; }





public virtual string  BuildingName { get ; set ; }





public virtual string  StreetName { get ; set ; }





public virtual string  Country { get ; set ; }





public virtual string  Lat { get ; set ; }





public virtual string  Lon { get ; set ; }





public virtual bool ?  isDoorDelivery { get ; set ; }





public virtual bool ?  isVerified { get ; set ; }





public virtual bool ?  isUnionTerritory { get ; set ; }





public virtual bool ?  isLicensed { get ; set ; }





public virtual bool ?  IsOfflineInstalled { get ; set ; }





public virtual DateTime ? LastLoggedOn { get ; set ; }





public virtual int ? DistanceCover { get ; set ; }





public virtual bool ?  Gstselect { get ; set ; }





public virtual bool ?  isHeadOffice { get ; set ; }





public virtual int ? OfflineVersionNo { get ; set ; }





public virtual string  GsTinNo { get ; set ; }


   

public virtual long? TotalSmsCount { get; set; }




public virtual long? SentSmsCount { get; set; }



public virtual int? InstanceTypeId { get; set; }


public virtual string SmsUserName { get; set; }


public virtual string SmsPassword { get; set; }


public virtual string SmsUrl { get; set; }



    }

}
