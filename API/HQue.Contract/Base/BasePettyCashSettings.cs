
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasePettyCashSettings : BaseContract
{



[Required]
public virtual decimal ? OpeningBalance { get ; set ; }




[Required]
public virtual decimal ? Amount { get ; set ; }




[Required]
public virtual string  CreateHead { get ; set ; }




[Required]
public virtual string  CreditOrDebit { get ; set ; }



}

}
