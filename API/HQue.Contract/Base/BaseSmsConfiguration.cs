
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSmsConfiguration : BaseContract
{
public  DateTime Date { get; set; }

public virtual string Remarks { get ; set ; }


public virtual long ? SmsCount { get ; set ; }

 public virtual bool? IsActive { get; set; }



    }

}
