
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSyncDataSeed : BaseContract
{



[Required]
public virtual int ? SeedIndex { get ; set ; }




[Required]
public virtual DateTime ? LastSynedAt { get ; set ; }



}

}
