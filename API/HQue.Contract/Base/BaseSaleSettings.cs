
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

    public class BaseSaleSettings : BaseContract
    {




        public virtual string CardType { get; set; }





        public virtual string PatientSearchType { get; set; }





        public virtual string DoctorSearchType { get; set; }





        public virtual string DoctorNameMandatoryType { get; set; }





        public virtual string SaleTypeMandatory { get; set; }





        public virtual int? PostCancelBillDays { get; set; }





        public virtual bool? IsCreditInvoiceSeries { get; set; }





        public virtual string DiscountType { get; set; }





        public virtual string CustomSeriesInvoice { get; set; }





        public virtual int? PrinterType { get; set; }





        public virtual string CustomTempJSON { get; set; }





        public virtual bool? ShortCutKeysType { get; set; }





        public virtual bool? NewWindowType { get; set; }





        public virtual bool? AutoTempStockAdd { get; set; }





        public virtual string AutosaveCustomer { get; set; }





        public virtual string MaxDiscountAvail { get; set; }





        public virtual decimal? InstanceMaxDiscount { get; set; }





        public virtual bool? ShortCustomerName { get; set; }





        public virtual bool? ShortDoctorName { get; set; }





        public virtual string CompleteSaleKeyType { get; set; }





        public virtual string ScanBarcode { get; set; }





        public virtual int? AutoScanQty { get; set; }





        public virtual bool? PatientTypeDept { get; set; }





        public virtual bool? ShowSalesItemHistory { get; set; }





        public virtual string PrintLogo { get; set; }





        public virtual string PrintCustomFields { get; set; }





        public virtual bool? IsCancelEditSMS { get; set; }





        public virtual bool? IsEnableRoundOff { get; set; }



        public virtual bool? IsEnableFreeQty { get; set; }




        public virtual bool? IsEnableSalesUser { get; set; }

        public virtual bool? isEnableCustomerSeries { get; set; }

        public virtual string CustomerIDSeries { get; set; }

        public string ActiveStatus { get; set; }

        public virtual string SeriesName { get; set; }

        public virtual bool? IsEnableNewSalesScreen { get; set; }
    }

}
