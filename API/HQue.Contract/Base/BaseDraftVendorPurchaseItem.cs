
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseDraftVendorPurchaseItem : BaseContract
{




public virtual string  DraftVendorPurchaseId { get ; set ; }





public virtual string  ProductStockId { get ; set ; }




[Required]
public virtual string  ProductName { get ; set ; }




[Required]
public virtual string  ProductId { get ; set ; }




[Required]
public virtual string  BatchNo { get ; set ; }




[Required]
public virtual DateTime ? ExpireDate { get ; set ; }





public virtual string  TaxType { get ; set ; }





public virtual decimal ? VAT { get ; set ; }





public virtual decimal ? CST { get ; set ; }





public virtual decimal ? PackageSize { get ; set ; }





public virtual decimal ? PackageQty { get ; set ; }





public virtual decimal ? PackagePurchasePrice { get ; set ; }





public virtual decimal ? PackageSellingPrice { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? PurchasePrice { get ; set ; }





public virtual int ? FreeQty { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual string  fromTempId { get ; set ; }





public virtual string  fromDcId { get ; set ; }



}

}
