
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseStockTransferItem : BaseContract
{




public virtual string  TransferId { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual string  VendorOrderItemId { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual string  ProductId { get ; set ; }





public virtual string  BatchNo { get ; set ; }





public virtual DateTime ? ExpireDate { get ; set ; }





public virtual decimal ? VAT { get ; set ; }





public virtual decimal ? PurchasePrice { get ; set ; }





public virtual decimal ? SellingPrice { get ; set ; }





public virtual decimal ? MRP { get ; set ; }





public virtual decimal ? PackageSize { get ; set ; }





public virtual string  VendorId { get ; set ; }





public virtual string  ToProductStockId { get ; set ; }





public virtual decimal ? AcceptedStrip { get ; set ; }





public virtual decimal ? AcceptedPackageSize { get ; set ; }





public virtual decimal ? AcceptedUnits { get ; set ; }





public virtual string  ToInstanceId { get ; set ; }




[Required]
public virtual bool ?  isStripTransfer { get ; set ; }





public virtual decimal ? Igst { get ; set ; }





public virtual decimal ? Cgst { get ; set ; }





public virtual decimal ? Sgst { get ; set ; }





public virtual decimal ? GstTotal { get ; set ; }





public virtual string  VendorPurchaseItemId { get ; set ; }



}

}
