
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasetempSales : BaseContract
{




public virtual string  PatientId { get ; set ; }




[Required]
public virtual string  InvoiceNo { get ; set ; }





public virtual DateTime ? InvoiceDate { get ; set ; }





public virtual string  Name { get ; set ; }





public virtual string  Mobile { get ; set ; }





public virtual string  Email { get ; set ; }





public virtual DateTime ? DOB { get ; set ; }





public virtual int ? Age { get ; set ; }





public virtual string  Gender { get ; set ; }





public virtual string  Address { get ; set ; }





public virtual string  Pincode { get ; set ; }





public virtual string  DoctorName { get ; set ; }





public virtual string  DoctorMobile { get ; set ; }





public virtual string  PaymentType { get ; set ; }





public virtual string  DeliveryType { get ; set ; }





public virtual bool ?  BillPrint { get ; set ; }





public virtual bool ?  SendSms { get ; set ; }





public virtual bool ?  SendEmail { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? Credit { get ; set ; }





public virtual string  FileName { get ; set ; }





public virtual string  CashType { get ; set ; }





public virtual string  CardNo { get ; set ; }





public virtual DateTime ? CardDate { get ; set ; }





public virtual string  CardDigits { get ; set ; }





public virtual string  CardName { get ; set ; }



}

}
