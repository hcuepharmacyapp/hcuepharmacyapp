
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{ 

public class BasePhysicalStockHistory : BaseContract
{




public virtual int ? PopupNo { get ; set ; }





public virtual string  ProductId { get ; set ; }





public virtual decimal ? CurrentStock { get ; set ; }





public virtual decimal ? PhysicalStock { get ; set ; }





public virtual DateTime ? OpenedAt { get ; set ; }





public virtual DateTime ? ClosedAt { get ; set ; }



}

}
