
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseStockValueReport : BaseContract
{



[Required]
public virtual string  ReportNo { get ; set ; }




[Required]
public virtual string  Category { get ; set ; }





public virtual decimal ? ProductCount { get ; set ; }





public virtual decimal ? Vat0PurchasePrice { get ; set ; }





public virtual decimal ? Vat5PurchasePrice { get ; set ; }





public virtual decimal ? Vat145PurchasePrice { get ; set ; }





public virtual decimal ? VatOtherPurchasePrice { get ; set ; }
        //added by nandhini
public virtual decimal? Gst0PurchasePrice { get; set; }
public virtual decimal? Gst5PurchasePrice { get; set; }
public virtual decimal? Gst12PurchasePrice { get; set; }
public virtual decimal? Gst18PurchasePrice { get; set; }
public virtual decimal? Gst28PurchasePrice { get; set; }
public virtual decimal? GstOtherPurchasePrice { get; set; }

//end


public virtual decimal ? TotalPurchasePrice { get ; set ; }





public virtual decimal ? TotalMrp { get ; set ; }





public virtual decimal ? Profit { get ; set ; }



}

}
