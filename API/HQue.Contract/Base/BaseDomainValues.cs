
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseDomainValues : BaseContract
{



[Required]
public virtual string  DomainId { get ; set ; }




[Required]
public virtual string  DomainValue { get ; set ; }





public virtual string  DisplayText { get ; set ; }


 
public virtual bool? HasSubdomain { get; set; }


 
public virtual int? SubId { get; set; }



}

}
