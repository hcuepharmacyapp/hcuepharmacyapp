
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseStockLedgerUpdateHistory : BaseContract
{



[Required]
public virtual string  ProductStockId { get ; set ; }





public virtual decimal ? ImportQty { get ; set ; }





public virtual decimal ? CurrentStock { get ; set ; }





public virtual decimal ? OpeningStock { get ; set ; }





public virtual decimal ? AdjustedStockIn { get ; set ; }





public virtual decimal ? PurchaseQty { get ; set ; }





public virtual decimal ? SaleReturnQty { get ; set ; }





public virtual decimal ? DcQty { get ; set ; }





public virtual decimal ? TempQty { get ; set ; }





public virtual decimal ? TransferInAccQty { get ; set ; }





public virtual decimal ? AdjustedStockOut { get ; set ; }





public virtual decimal ? SaleQty { get ; set ; }





public virtual decimal ? PurchaseDelQty { get ; set ; }





public virtual decimal ? TransferOutQty { get ; set ; }





public virtual decimal ? ConsumptionQty { get ; set ; }





public virtual decimal ? FinalIn { get ; set ; }





public virtual decimal ? FinalOut { get ; set ; }





public virtual decimal ? DiffQty { get ; set ; }





public virtual decimal ? ClosingStock { get ; set ; }



}

}
