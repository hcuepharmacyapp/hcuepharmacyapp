
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseClosingStock : BaseContract
{




public virtual string  ProductStockId { get ; set ; }





public virtual DateTime ? TransDate { get ; set ; }





public virtual decimal ? ClosingStock { get ; set ; }





public virtual decimal ? StockValue { get ; set ; }





public virtual decimal ? GstTotal { get ; set ; }





public virtual bool ?  IsOffline { get ; set ; }





public virtual bool ?  IsSynctoOnline { get ; set ; }



}

}
