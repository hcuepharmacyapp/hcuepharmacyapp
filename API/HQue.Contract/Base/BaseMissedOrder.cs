
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseMissedOrder : BaseContract
{




public virtual string  PatientId { get ; set ; }





public virtual string  PatientName { get ; set ; }





public virtual string  PatientMobile { get ; set ; }





public virtual int ? PatientAge { get ; set ; }





public virtual string  PatientGender { get ; set ; }




[Required]
public virtual string  ProductId { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? SellingPrice { get ; set ; }





public virtual bool ?  IsActive { get ; set ; }



}

}
