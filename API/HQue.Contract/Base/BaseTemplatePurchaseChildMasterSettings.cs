
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseTemplatePurchaseChildMasterSettings : BaseContract
{



[Required]
public virtual bool ?  HeaderOption { get ; set ; }




[Required]
public virtual int  VendorRowPos { get ; set ; }




[Required]
public virtual int  ProductRowPos { get ; set ; }





public virtual string  DateFormat { get ; set ; }





public virtual string  VendorId { get ; set ; }



}

}
