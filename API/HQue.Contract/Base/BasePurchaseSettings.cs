
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasePurchaseSettings : BaseContract
{




public virtual int ? TaxType { get ; set ; }





public virtual int ? BuyInvoiceDateEdit { get ; set ; }





public virtual bool ?  IsAllowDecimal { get ; set ; }





public virtual string  CompletePurchaseKeyType { get ; set ; }





public virtual int ? BillSeriesType { get ; set ; }





public virtual string  CustomSeriesName { get ; set ; }





public virtual bool ?  EnableSelling { get ; set ; }





public virtual bool ?  IsSortByLowestPrice { get ; set ; }





public virtual bool ?  EnableMarkup { get ; set ; }


public virtual bool? InvoiceValueSetting { get; set; }


public virtual decimal? InvoiceValueDifference { get; set; }



}

}
