
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasePettyCashHdr : BaseContract
{




public virtual string  UserId { get ; set ; }




[Required]
public virtual DateTime ? TransactionDate { get ; set ; }




[Required]
public virtual string  Description { get ; set ; }




[Required]
public virtual decimal ? clbalance { get ; set ; }





public virtual decimal ? FloatingAmount { get ; set ; }





public virtual decimal ? TotalSales { get ; set ; }





public virtual decimal ? TotalExpense { get ; set ; }





public virtual decimal ? VendorPayment { get ; set ; }





public virtual int ? noof2000 { get ; set ; }





public virtual int ? noof1000 { get ; set ; }





public virtual int ? noof500 { get ; set ; }





public virtual int ? noof200 { get ; set ; }





public virtual int ? noof100 { get ; set ; }





public virtual int ? noof50 { get ; set ; }





public virtual int ? noof20 { get ; set ; }





public virtual int ? noof10 { get ; set ; }





public virtual decimal ? Others { get ; set ; }





public virtual decimal ? coins { get ; set ; }





public virtual decimal ? CustomerPayment { get ; set ; }





public virtual int ? CounterNo { get ; set ; }





public virtual bool ?  DeletedStatus { get ; set ; }



}

}
