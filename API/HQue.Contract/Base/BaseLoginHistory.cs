
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseLoginHistory : BaseContract
{
        
public virtual string LogInUserId { get ; set ; }

public virtual string LogOutUserId { get ; set ; }


public virtual DateTime ? LoggedInDate { get ; set ; }


public virtual DateTime? LoggedOutDate { get; set; }


public virtual DateTime? LastTransctionDate { get; set; }


 public virtual bool ? IsSentsms { get ; set ; }

 public virtual bool? IsSentemail { get; set; }



    }

}
