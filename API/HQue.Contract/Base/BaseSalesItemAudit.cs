
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSalesItemAudit : BaseContract
{




public virtual string  SalesId { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual int ? ReminderFrequency { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? SellingPrice { get ; set ; }





public virtual decimal ? MRP { get ; set ; }



public virtual string Action { get; set; }



}

}
