
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSalesOrderEstimateItem : BaseContract
{




public virtual string  SalesOrderEstimateId { get ; set ; }




[Required]
public virtual string  ProductId { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? ReceivedQty { get ; set ; }





public virtual string  BatchNo { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual DateTime ? ExpireDate { get ; set ; }





public virtual decimal ? SellingPrice { get ; set ; }





public virtual bool ?  IsDeleted { get ; set ; }





public virtual bool ?  IsActive { get ; set ; }





public virtual int ? IsOrder { get ; set ; }



}

}
