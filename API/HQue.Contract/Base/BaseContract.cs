﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace HQue.Contract.Base
{
    public interface IContract
    {
        object this[string propertName] { get; set; }
        Page Page { get; set; }
        bool IsNew { get; }
        string Id { get; set; }
        string AccountId { get; set; }
        string InstanceId { get; set; }
        string ClientId { get; set; }
        DateTime CreatedAt { get; set; }
        DateTime UpdatedAt { get; set; }
        string CreatedBy { get; set; }
        string UpdatedBy { get; set; }
        bool OfflineStatus { get; set; }
        bool GSTEnabled { get; set; } //GST flag added by Poongod on 29/06/2017
        bool WriteExecutionQuery { get; set; }
        void InitializeExecutionQuery();
        void AddExecutionQuery(dynamic qb);
        void SetExecutionQuery(List<dynamic> executionQuery, bool writeExecutionQuery);
        List<dynamic> GetExecutionQuery();
    }

    public class Page
    {
        public Page()
        {
            PageNo = 0;
            PageSize = 0;
        }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
    }

    public class BaseContract : IContract
    {
        public BaseContract()
        {
            Page = new Page();
        }

        public Page Page { get; set; }
        public bool IsNew => string.IsNullOrEmpty(Id);
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string InstanceId { get; set; }
        public string ClientId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public bool OfflineStatus { get; set; }
        public bool GSTEnabled { get; set; } //GST flag added by Poongod on 29/06/2017
        private List<dynamic> ExecutionQuery { get; set; }
        public bool WriteExecutionQuery { get; set; }
        public object this[string propertName]
        {
            get
            {
                return GetPropertyValue(propertName);
            }

            set
            {
                SetValue(propertName, value);
            }
        }

        public object GetPropertyValue(string propertName)
        {
            return GetType().GetRuntimeProperty(propertName).GetValue(this);
        }

        public void SetValue(string key, object value)
        {
            PropertyInfo p;
            var o = new object();
            if (key.IndexOf('.') > 0)
            {
                var property = key.Split('.');

                o = GetType().GetProperty(property[0]).GetValue(this);
                p = o.GetType().GetProperty(property[1]);
            }
            else
            {
                p = GetType().GetProperty(key);
            }

            var t = p.PropertyType; // t will be System.String

            if (t.Name.IndexOf("Nullable", StringComparison.Ordinal) >= 0) // if type is nullable
            {
                t = Nullable.GetUnderlyingType(t);
            }

            if (key.IndexOf('.') > 0)
            {
                p.SetValue(o, value != DBNull.Value ? Convert.ChangeType(value, t) : null, null);
            }
            else
            {
                if (t == typeof(int) || t == typeof(decimal) || t == typeof(DateTime))
                {
                    if (string.IsNullOrEmpty(value.ToString()))
                        value = DBNull.Value;
                }
                p.SetValue(this, value != DBNull.Value ? Convert.ChangeType(value, t) : null, null);
            }
        }

        public void InitializeExecutionQuery()
        {
            ExecutionQuery = new List<dynamic>();
            WriteExecutionQuery = true;
        }
        public void AddExecutionQuery(dynamic qb)
        {
            ExecutionQuery.Add(qb);
        }
        public void SetExecutionQuery(List<dynamic> executionQuery, bool writeExecutionQuery)
        {
            ExecutionQuery = executionQuery;
            WriteExecutionQuery = writeExecutionQuery;
        }
        public List<dynamic> GetExecutionQuery()
        {
            return ExecutionQuery;
        }
    }
}
