
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseVendorReturnItem : BaseContract
{




public virtual string  VendorReturnId { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? ReturnPurchasePrice { get ; set ; }





public virtual decimal ? ReturnedTotal { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? DiscountValue { get ; set ; }





public virtual bool ?  IsDeleted { get ; set ; }





public virtual decimal ? Igst { get ; set ; }





public virtual decimal ? Cgst { get ; set ; }





public virtual decimal ? Sgst { get ; set ; }





public virtual decimal ? GstTotal { get ; set ; }





public virtual decimal ? GstAmount { get ; set ; }



}

}
