
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseReplicationData : BaseContract
{



[Required]
public virtual string  ClientId { get ; set ; }




[Required]
public virtual string  TableName { get ; set ; }




[Required]
public virtual int ? Action { get ; set ; }




[Required]
public virtual DateTime  ActionTime { get ; set ; }





public virtual string  TransactionID { get ; set ; }



}

}
