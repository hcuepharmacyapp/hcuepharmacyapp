
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseLeads : BaseContract
{




public virtual string  PatientId { get ; set ; }





public virtual string  ExternalId { get ; set ; }





public virtual DateTime ? LeadDate { get ; set ; }





public virtual string  Name { get ; set ; }





public virtual string  Mobile { get ; set ; }





public virtual string  Email { get ; set ; }





public virtual DateTime ? DOB { get ; set ; }





public virtual int ? Age { get ; set ; }





public virtual string  Gender { get ; set ; }





public virtual string  Address { get ; set ; }





public virtual string  Pincode { get ; set ; }





public virtual string  DoctorId { get ; set ; }





public virtual string  DoctorName { get ; set ; }





public virtual string  DoctorMobile { get ; set ; }





public virtual string  DoctorSpecialization { get ; set ; }





public virtual string  LeadStatus { get ; set ; }





public virtual string  LocalLeadStatus { get ; set ; }





public virtual decimal ? Lat { get ; set ; }





public virtual decimal ? Lng { get ; set ; }





public virtual string  DeliveryMode { get ; set ; }



}

}
