
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

    public class BaseOrderSettings : BaseContract
    {

        public virtual int? NoOfDaysFromDate { get; set; }

        public virtual int? DateSettingType { get; set; }      


    }

}
