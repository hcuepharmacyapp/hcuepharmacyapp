
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseStockLedgerUpdateHistoryItem : BaseContract
{



[Required]
public virtual string  StockLedgerUpdateId { get ; set ; }




[Required]
public virtual string  ProductId { get ; set ; }




public virtual string ProductStockId { get; set; }




[Required]
public virtual string  BatchNo { get ; set ; }




[Required]
public virtual DateTime ? ExpireDate { get ; set ; }





public virtual decimal ? CurrentStock { get ; set ; }





public virtual decimal ? ActualStock { get ; set ; }





public virtual string  TransactionType { get ; set ; }




    }

}
