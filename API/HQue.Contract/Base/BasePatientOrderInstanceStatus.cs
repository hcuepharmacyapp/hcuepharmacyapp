
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasePatientOrderInstanceStatus : BaseContract
{



[Required]
public virtual string  PatientOrderId { get ; set ; }




[Required]
public virtual int ? AcceptanceStatus { get ; set ; }





public virtual int ? Duration { get ; set ; }




[Required]
public virtual string  MessageCode { get ; set ; }




[Required]
public virtual DateTime ? PushSentTime { get ; set ; }




[Required]
public virtual int ? Distance { get ; set ; }



}

}
