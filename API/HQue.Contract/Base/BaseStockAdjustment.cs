
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseStockAdjustment : BaseContract
{




public virtual string  ProductStockId { get ; set ; }





public virtual decimal ? AdjustedStock { get ; set ; }





public virtual string  AdjustedBy { get ; set ; }





public virtual int ? TaxRefNo { get ; set ; }



}

}
