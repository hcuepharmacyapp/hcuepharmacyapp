
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

    public class BaseProductStock : BaseContract
    {



        [Required]
        public virtual string ProductId { get; set; }





        public virtual string VendorId { get; set; }




        [Required]
        public virtual string BatchNo { get; set; }




        [Required]
        public virtual DateTime? ExpireDate { get; set; }




        [Required]
        public virtual decimal? VAT { get; set; }





        public virtual string TaxType { get; set; }




        [Required]
        public virtual decimal? SellingPrice { get; set; }





        public virtual decimal? MRP { get; set; }





        public virtual string PurchaseBarcode { get; set; }



        public virtual string BarcodeProfileId { get; set; }
        [Required]
        public virtual decimal? Stock { get; set; }





        public virtual decimal? PackageSize { get; set; }





        public virtual decimal? PackagePurchasePrice { get; set; }





        public virtual decimal? PurchasePrice { get; set; }





        public virtual decimal? CST { get; set; }





        public virtual bool? IsMovingStock { get; set; }





        public virtual decimal? ReOrderQty { get; set; }





        public virtual int? Status { get; set; }





        public virtual bool? IsMovingStockExpire { get; set; }





        public virtual bool? NewOpenedStock { get; set; }





        public virtual string NewStockInvoiceNo { get; set; }





        public virtual decimal? NewStockQty { get; set; }





        public virtual bool? StockImport { get; set; }





        public virtual decimal? ImportQty { get; set; }





        public virtual string Eancode { get; set; }


        public virtual string HsnCode { get; set; }


        public virtual decimal? Igst { get; set; }





        public virtual decimal? Cgst { get; set; }





        public virtual decimal? Sgst { get; set; }





        public virtual decimal? GstTotal { get; set; }
        //added by nandhini for importdate


        public virtual DateTime? ImportDate { get; set; }


        //Added by Poongodi on 03/08/2017
        public virtual string Ext_RefId { get; set; }

        public virtual int TaxRefNo { get; set; }


        public virtual decimal? PrvStockQty { get; set; } //Added by Sarubala on 01-11-17

        public virtual string TransactionId { get; set; } // Added by Manivanna on 23-Nov-2017



    }

}
