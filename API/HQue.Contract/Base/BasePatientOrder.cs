
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasePatientOrder : BaseContract
{



[Required]
public virtual string  PatientExtId { get ; set ; }




[Required]
public virtual string  OrderId { get ; set ; }




[Required]
public virtual int ? OrderStatus { get ; set ; }




[Required]
public virtual string  Lat { get ; set ; }




[Required]
public virtual string  Lon { get ; set ; }





public virtual string  Prescription { get ; set ; }





public virtual decimal ? Amount { get ; set ; }





public virtual string  DeliveryBoyId { get ; set ; }





public virtual DateTime ? DeliveredTime { get ; set ; }





public virtual int ? WithPharmaId { get ; set ; }



}

}
