
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

    public class BaseAlternateVendorProduct : BaseContract
    {

      

        [Required]
        public virtual string ProductId { get; set; }




        [Required]
        public virtual string VendorId { get; set; }




        [Required]
        public virtual string AlternateProductName { get; set; }


//added by nandhini for purchase import mapping

        [Required]
        public virtual string AlternatePackageSize { get; set; }


        public virtual int? UpdatedPackageSize { get; set; }


    }

}
