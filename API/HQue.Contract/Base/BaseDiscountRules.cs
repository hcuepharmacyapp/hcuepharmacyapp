
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseDiscountRules : BaseContract
{




public virtual decimal ? Amount { get ; set ; }





public virtual decimal ? ToAmount { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual DateTime ? FromDate { get ; set ; }





public virtual DateTime ? ToDate { get ; set ; }





public virtual string  BillAmountType { get ; set ; }



}

}
