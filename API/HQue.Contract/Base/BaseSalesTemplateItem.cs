
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSalesTemplateItem : BaseContract
{



[Required]
public virtual string  TemplateId { get ; set ; }




[Required]
public virtual string  ProductId { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual bool ?  IsDeleted { get ; set ; }



}

}
