
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseImportProcessActivity : BaseContract
{



[Required]
public virtual string  FileName { get ; set ; }




[Required]
public virtual int ? IsCloud { get ; set ; }





public virtual string  ActivityDesc { get ; set ; }





public virtual string  Status { get ; set ; }



}

}
