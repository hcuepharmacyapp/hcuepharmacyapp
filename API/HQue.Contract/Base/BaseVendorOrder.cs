
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseVendorOrder : BaseContract
{




public virtual string  VendorId { get ; set ; }



public virtual string Prefix { get; set; }

public virtual string  OrderId { get ; set ; }





public virtual DateTime ? OrderDate { get ; set ; }





public virtual DateTime ? DueDate { get ; set ; }





public virtual bool ?  PendingAuthorisation { get ; set ; }





public virtual string  AuthorisedBy { get ; set ; }





public virtual string  FinyearId { get ; set ; }





public virtual string  ToInstanceId { get ; set ; }

public virtual int TaxRefNo { get; set; }
        

    }

}
