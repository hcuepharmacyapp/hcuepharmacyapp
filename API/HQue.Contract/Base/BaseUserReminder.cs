
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseUserReminder : BaseContract
{




public virtual string  HqueUserId { get ; set ; }





public virtual string  ReferenceId { get ; set ; }





public virtual int ? ReferenceFrom { get ; set ; }





public virtual string  ReminderPhone { get ; set ; }





public virtual string  CustomerName { get ; set ; }





public virtual string  CustomerAge { get ; set ; }





public virtual string  CustomerGender { get ; set ; }





public virtual string  DoctorName { get ; set ; }





public virtual DateTime ? StartDate { get ; set ; }





public virtual int ? ReminderTime { get ; set ; }





public virtual DateTime ? EndDate { get ; set ; }





public virtual int ? ReminderFrequency { get ; set ; }





public virtual int ? ReminderStatus { get ; set ; }





public virtual int ? ReminderType { get ; set ; }





public virtual string  Description { get ; set ; }





public virtual string  Remarks { get ; set ; }



}

}
