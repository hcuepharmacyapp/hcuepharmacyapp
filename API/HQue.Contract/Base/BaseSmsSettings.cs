
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSmsSettings : BaseContract
{




public virtual bool? IsVendorCreateSms { get ; set ; }





public virtual bool? IsSalesCreateSms { get ; set ; }





public virtual bool? IsSalesEditSms { get ; set ; }





public virtual bool? IsSalesCancelSms { get ; set ; }





public virtual bool ? IsSalesEstimateSms { get ; set ; }





public virtual bool ? IsSalesTemplateCreateSms { get ; set ; }





public virtual bool? IsSalesOrderSms { get ; set ; }





public virtual bool? IsLoginSms { get ; set ; }





public virtual bool ? IsCustomerBulkSms { get ; set ; }





public virtual bool? IsOrderCreateSms { get ; set ; }



public virtual bool? IsUserCreateSms { get; set; }



}

}
