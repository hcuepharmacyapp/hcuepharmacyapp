
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

    public class BasePatient : BaseContract
    {




        public virtual string Code { get; set; }





        public virtual string Name { get; set; }





        public virtual string ShortName { get; set; }





        public virtual string Mobile { get; set; }





        public virtual string Email { get; set; }





        public virtual DateTime? DOB { get; set; }





        public virtual int? Age { get; set; }





        public virtual string Gender { get; set; }





        public virtual string Address { get; set; }





        public virtual string Area { get; set; }





        public virtual string City { get; set; }





        public virtual string State { get; set; }





        public virtual string Pincode { get; set; }





        public virtual string EmpID { get; set; }





        public virtual decimal? Discount { get; set; }





        public virtual int? IsSync { get; set; }





        public virtual string CustomerPaymentType { get; set; }





        public virtual int? PatientType { get; set; }





        public virtual bool? IsAutoSave { get; set; }





        public virtual string Pan { get; set; }





        public virtual string GsTin { get; set; }





        public virtual string DrugLicenseNo { get; set; }





        public virtual int? LocationType { get; set; }





        public virtual decimal? BalanceAmount { get; set; }


        public virtual decimal? CreditBalanceAmount { get; set; }


        public virtual int? Status { get; set; }





        public virtual decimal? LoyaltyPoint { get; set; }



        public virtual decimal? RedeemedPoint { get; set; }

        public virtual string CustomerSeriesId { get; set; }

        public virtual string Prefix { get; set; }

        public virtual bool? isEnableCustomerSeries { get; set; }
    }

}
