
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasePaymentType : BaseContract
{



[Required]
public virtual int ? PaymentInd { get ; set ; }




[Required]
public virtual string  PaymentCategory { get ; set ; }




[Required]
public virtual string  PaymentType { get ; set ; }



}

}
