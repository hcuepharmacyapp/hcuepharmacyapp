
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseLoyaltyPointSettings : BaseContract
{




public virtual decimal ? LoyaltySaleValue { get ; set ; }





public virtual decimal ? LoyaltyPoint { get ; set ; }





public virtual DateTime ? StartDate { get ; set ; }





public virtual DateTime ? EndDate { get ; set ; }





public virtual decimal ? MinimumSalesAmt { get ; set ; }





public virtual decimal ? RedeemPoint { get ; set ; }





public virtual decimal ? RedeemValue { get ; set ; }





public virtual decimal ? MaximumRedeemPoint { get ; set ; }





public virtual bool ?  IsActive { get ; set ; }





public virtual bool ?  LoyaltyOption { get ; set ; }





public virtual bool ?  IsMinimumSale { get ; set ; }





public virtual bool ?  IsMaximumRedeem { get ; set ; }





public virtual bool ?  IsEndDate { get ; set ; }



}

}
