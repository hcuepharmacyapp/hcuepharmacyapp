
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSalesReturn : BaseContract
{




public virtual string  SalesId { get ; set ; }





public virtual string  PatientId { get ; set ; }





public virtual string  InvoiceSeries { get ; set ; }





public virtual string  Prefix { get ; set ; }




[Required]
public virtual string  ReturnNo { get ; set ; }





public virtual DateTime ? ReturnDate { get ; set ; }





public virtual int ? CancelType { get ; set ; }





public virtual string  FinyearId { get ; set ; }





public virtual bool ?  IsAlongWithSale { get ; set ; }





public virtual int ? TaxRefNo { get ; set ; }





public virtual decimal ? ReturnCharges { get ; set ; }





public virtual decimal ? ReturnChargePercent { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? DiscountValue { get ; set ; }





public virtual decimal ? NetAmount { get ; set ; }





public virtual decimal ? RoundOffNetAmount { get ; set ; }





public virtual decimal ? TotalDiscountValue { get ; set ; }





public virtual decimal ? ReturnItemAmount { get ; set ; }





public virtual decimal ? GstAmount { get ; set ; }



public virtual decimal? LoyaltyPts { get; set; }


public virtual string LoyaltyId { get; set; }



public virtual bool ?  IsRoundOff { get ; set ; }


    }

}
