
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseTransferSettings : BaseContract
{




public virtual string  ProductBatchDisplayType { get ; set ; }





public virtual bool ?  TransferQtyType { get ; set ; }





public virtual bool ?  TransferZeroStock { get ; set ; }





public virtual int ? ShowExpiredQty { get ; set ; }



}

}
