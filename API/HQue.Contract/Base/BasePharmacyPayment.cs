
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasePharmacyPayment : BaseContract
{




public virtual DateTime ? PaymentDate { get ; set ; }




[Required]
public virtual int  ProductType { get ; set ; }




[Required]
public virtual int ? Quantity { get ; set ; }




[Required]
public virtual decimal ? Cost { get ; set ; }




[Required]
public virtual bool ?  DiscountType { get ; set ; }




[Required]
public virtual decimal ? Discount { get ; set ; }




[Required]
public virtual decimal ? Tax { get ; set ; }




[Required]
public virtual decimal ? Paid { get ; set ; }





public virtual string  BalanceReason { get ; set ; }




[Required]
public virtual int  SalePersonType { get ; set ; }




[Required]
public virtual string  SalePersonName { get ; set ; }




[Required]
public virtual string  SalePersonMobile { get ; set ; }




[Required]
public virtual bool ?  CommissionType { get ; set ; }




[Required]
public virtual decimal ? Commission { get ; set ; }




[Required]
public virtual int  PaymentType { get ; set ; }





public virtual string  ChequeNo { get ; set ; }





public virtual DateTime ? ChequeDate { get ; set ; }





public virtual string  ChequeBank { get ; set ; }





public virtual string  Last4Digits { get ; set ; }





public virtual string  CardName { get ; set ; }





public virtual string  Comments { get ; set ; }



}

}
