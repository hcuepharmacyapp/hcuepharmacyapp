
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseStockTransfer : BaseContract
{



[Required]
public virtual string  FromInstanceId { get ; set ; }




[Required]
public virtual string  ToInstanceId { get ; set ; }





public virtual string  Prefix { get ; set ; }




[Required]
public virtual string  TransferNo { get ; set ; }




[Required]
public virtual DateTime ? TransferDate { get ; set ; }





public virtual string  FileName { get ; set ; }





public virtual string  Comments { get ; set ; }





public virtual int ? TransferStatus { get ; set ; }





public virtual string  TransferBy { get ; set ; }





public virtual string  FinyearId { get ; set ; }





public virtual int ? TaxRefNo { get ; set ; }





public virtual int ? TransferItemCount { get ; set ; }



}

}
