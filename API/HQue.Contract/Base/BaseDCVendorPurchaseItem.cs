
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseDCVendorPurchaseItem : BaseContract
{




public virtual string  VendorId { get ; set ; }





public virtual DateTime ? DCDate { get ; set ; }





public virtual string  DCNo { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual decimal ? PackageSize { get ; set ; }





public virtual decimal ? PackageQty { get ; set ; }





public virtual decimal ? PackagePurchasePrice { get ; set ; }





public virtual decimal ? PackageSellingPrice { get ; set ; }





public virtual decimal ? PackageMRP { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? PurchasePrice { get ; set ; }





public virtual decimal ? FreeQty { get ; set ; }





public virtual decimal ? Discount { get ; set ; }




[Required]
public virtual bool ?  isActive { get ; set ; }





public virtual bool ?  isPurchased { get ; set ; }





public virtual int ? TaxRefNo { get ; set ; }



}

}
