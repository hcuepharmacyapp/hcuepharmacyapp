
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSalesItem : BaseContract
{




public virtual string  SalesId { get ; set ; }





public virtual string  SaleProductName { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual int ? ReminderFrequency { get ; set ; }





public virtual DateTime ? ReminderDate { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? ItemAmount { get ; set ; }





public virtual decimal ? VAT { get ; set ; }





public virtual decimal ? VatAmount { get ; set ; }





public virtual decimal ? DiscountAmount { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? SellingPrice { get ; set ; }





public virtual decimal ? MRP { get ; set ; }





public virtual decimal ? Igst { get ; set ; }





public virtual decimal ? Cgst { get ; set ; }





public virtual decimal ? Sgst { get ; set ; }





public virtual decimal ? GstTotal { get ; set ; }





public virtual decimal ? GstAmount { get ; set ; }





public virtual decimal ? TotalAmount { get ; set ; }





public virtual string  SalesOrderEstimateId { get ; set ; }





public virtual int ? SalesOrderEstimateType { get ; set ; }





public virtual int? SalesItemSno { get; set; }





public virtual decimal? LoyaltyProductPts { get; set; }



    }

}
