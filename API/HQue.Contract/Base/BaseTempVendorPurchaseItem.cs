
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseTempVendorPurchaseItem : BaseContract
{




public virtual string  ProductStockId { get ; set ; }





public virtual decimal ? PackageSize { get ; set ; }





public virtual decimal ? PackageQty { get ; set ; }





public virtual decimal ? PackagePurchasePrice { get ; set ; }





public virtual decimal ? PackageSellingPrice { get ; set ; }





public virtual decimal ? PackageMRP { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? PurchasePrice { get ; set ; }





public virtual int ? FreeQty { get ; set ; }





public virtual decimal ? Discount { get ; set ; }




[Required]
public virtual bool ?  isActive { get ; set ; }



public virtual int ? TaxRefNo { get; set; }


        public virtual bool? IsDeleted { get; set; }
    }

}
