
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseVendorReturn : BaseContract
{




public virtual string  VendorPurchaseId { get ; set ; }





public virtual string  VendorId { get ; set ; }





public virtual string  Prefix { get ; set ; }




[Required]
public virtual string  ReturnNo { get ; set ; }





public virtual DateTime ? ReturnDate { get ; set ; }





public virtual string  Reason { get ; set ; }





public virtual string  FinyearId { get ; set ; }





public virtual string  PaymentStatus { get ; set ; }





public virtual string  PaymentRemarks { get ; set ; }





public virtual decimal ? TotalReturnedAmount { get ; set ; }





public virtual decimal ? TotalDiscountValue { get ; set ; }





public virtual decimal ? TotalGstValue { get ; set ; }





public virtual decimal ? RoundOffValue { get ; set ; }





public virtual int ? TaxRefNo { get ; set ; }





public virtual bool ?  IsAlongWithPurchase { get ; set ; }



}

}
