
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseCustomerPayment : BaseContract
{




public virtual string  SalesId { get ; set ; }





public virtual DateTime ? TransactionDate { get ; set ; }





public virtual decimal ? Debit { get ; set ; }





public virtual decimal ? Credit { get ; set ; }





public virtual string  PaymentType { get ; set ; }





public virtual string  ChequeNo { get ; set ; }





public virtual DateTime ? ChequeDate { get ; set ; }





public virtual string  CardNo { get ; set ; }





public virtual DateTime ? CardDate { get ; set ; }





public virtual string  Remarks { get ; set ; }





public virtual bool ?  BankDeposited { get ; set ; }





public virtual bool ?  AmountCredited { get ; set ; }



}

}
