
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseVendorPurchaseItemAudit : BaseContract
{




public virtual string  VendorPurchaseId { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual decimal ? PackageSize { get ; set ; }





public virtual decimal ? PackageQty { get ; set ; }





public virtual decimal ? PackagePurchasePrice { get ; set ; }





public virtual decimal ? PackageSellingPrice { get ; set ; }





public virtual decimal ? PackageMRP { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? PurchasePrice { get ; set ; }





public virtual decimal ? FreeQty { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? MarkupPerc { get ; set ; }





public virtual decimal ? SchemeDiscountPerc { get ; set ; }





public virtual string  Action { get ; set ; }



}

}
