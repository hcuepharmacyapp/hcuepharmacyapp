
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSales : BaseContract
{




public virtual string  PatientId { get ; set ; }





public virtual string  InvoiceSeries { get ; set ; }





public virtual string  Prefix { get ; set ; }




[Required]
public virtual string  InvoiceNo { get ; set ; }





public virtual DateTime ? InvoiceDate { get ; set ; }





public virtual string  Name { get ; set ; }





public virtual string  Mobile { get ; set ; }





public virtual string  Email { get ; set ; }





public virtual DateTime ? DOB { get ; set ; }





public virtual int ? Age { get ; set ; }





public virtual string  Gender { get ; set ; }





public virtual string  Address { get ; set ; }





public virtual string  Pincode { get ; set ; }





public virtual string  DoctorName { get ; set ; }





public virtual string  DoctorMobile { get ; set ; }





public virtual string  PaymentType { get ; set ; }





public virtual string  DeliveryType { get ; set ; }





public virtual bool ?  BillPrint { get ; set ; }





public virtual bool ?  SendSms { get ; set ; }





public virtual bool ?  SendEmail { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? DiscountValue { get ; set ; }





public virtual int ? DiscountType { get ; set ; }





public virtual decimal ? TotalDiscountValue { get ; set ; }





public virtual decimal ? RoundoffNetAmount { get ; set ; }





public virtual decimal ? Credit { get ; set ; }





public virtual string  FileName { get ; set ; }





public virtual string  CashType { get ; set ; }





public virtual string  CardNo { get ; set ; }





public virtual DateTime ? CardDate { get ; set ; }





public virtual string  ChequeNo { get ; set ; }





public virtual DateTime ? ChequeDate { get ; set ; }





public virtual string  CardDigits { get ; set ; }





public virtual string  CardName { get ; set ; }





public virtual int ? Cancelstatus { get ; set ; }





public virtual string  SalesType { get ; set ; }





public virtual decimal ? NetAmount { get ; set ; }





public virtual decimal ? SaleAmount { get ; set ; }





public virtual decimal ? RoundoffSaleAmount { get ; set ; }





public virtual bool ?  BankDeposited { get ; set ; }





public virtual bool ?  AmountCredited { get ; set ; }





public virtual string  FinyearId { get ; set ; }





public virtual string  DoctorId { get ; set ; }





public virtual decimal ? VatAmount { get ; set ; }





public virtual decimal ? SalesItemAmount { get ; set ; }





public virtual decimal ? LoyaltyPts { get ; set ; }





public virtual decimal ? RedeemPts { get ; set ; }





public virtual decimal ? RedeemAmt { get ; set ; }





public virtual string  DoorDeliveryStatus { get ; set ; }





public virtual decimal ? GstAmount { get ; set ; }





public virtual bool ?  IsRoundOff { get ; set ; }





public virtual int ? TaxRefNo { get ; set ; }





public virtual string  LoyaltyId { get ; set ; }





public virtual decimal ? RedeemPercent { get ; set ; }



    }

}
