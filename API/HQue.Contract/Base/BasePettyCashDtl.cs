
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasePettyCashDtl : BaseContract
{




public virtual string  PettyHdrId { get ; set ; }




[Required]
public virtual string  PettyItemId { get ; set ; }





public virtual string  UserId { get ; set ; }




[Required]
public virtual DateTime ? TransactionDate { get ; set ; }




[Required]
public virtual string  Description { get ; set ; }





public virtual decimal ? Amount { get ; set ; }





public virtual bool ?  DeletedStatus { get ; set ; }





public virtual bool ?  TalliedStatus { get ; set ; }



}

}
