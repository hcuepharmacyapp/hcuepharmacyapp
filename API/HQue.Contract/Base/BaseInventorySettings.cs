
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

    public class BaseInventorySettings : BaseContract
    {




        public virtual int? NoOfDays { get; set; }





        public virtual int? NoOfMonth { get; set; }





        public virtual DateTime? PopupStartDate { get; set; }





        public virtual DateTime? PopupEndDate { get; set; }





        public virtual string PopupStartTime { get; set; }





        public virtual string PopupEndTime { get; set; }





        public virtual int? PopupInterval { get; set; }





        public virtual bool? PopupEnabled { get; set; }





        public virtual DateTime? LastPopupAt { get; set; }





        public virtual bool? OpenedPopup { get; set; }



    }

}
