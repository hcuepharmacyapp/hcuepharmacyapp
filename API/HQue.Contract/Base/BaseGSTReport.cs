
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

    public class BaseGSTReport : BaseContract
    {


        public virtual string Col0 { get; set; }
        public virtual string Col1 { get; set; }



        public virtual string Col2 { get; set; }


        public virtual string Col3 { get; set; }
        public virtual string Col4 { get; set; }
        public virtual string Col5 { get; set; }
        public virtual string Col6 { get; set; }
        public virtual int IsHeader { get; set; }

    }

}
