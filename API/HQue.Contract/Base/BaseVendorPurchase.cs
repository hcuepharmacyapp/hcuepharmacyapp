
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseVendorPurchase : BaseContract
{




public virtual string  VendorId { get ; set ; }





public virtual string  Prefix { get ; set ; }




[Required]
public virtual string  InvoiceNo { get ; set ; }





public virtual DateTime ? InvoiceDate { get ; set ; }



public virtual decimal? InitialValue { get; set; }





public virtual string  VerndorOrderId { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? Credit { get ; set ; }





public virtual string  PaymentType { get ; set ; }





public virtual string  ChequeNo { get ; set ; }





public virtual DateTime ? ChequeDate { get ; set ; }





public virtual string  BillSeries { get ; set ; }





public virtual string  GoodsRcvNo { get ; set ; }





public virtual string  Comments { get ; set ; }





public virtual string  FileName { get ; set ; }





public virtual int ? CreditNoOfDays { get ; set ; }





public virtual string  NoteType { get ; set ; }





public virtual decimal ? NoteAmount { get ; set ; }





public virtual decimal ? TotalPurchaseValue { get ; set ; }





public virtual decimal ? DiscountValue { get ; set ; }





public virtual decimal ? TotalDiscountValue { get ; set ; }





public virtual decimal ? VatValue { get ; set ; }





public virtual decimal ? RoundOffValue { get ; set ; }





public virtual decimal ? NetValue { get ; set ; }





public virtual decimal ? ReturnAmount { get ; set ; }





public virtual string  Finyear { get ; set ; }





public virtual int ? CancelStatus { get ; set ; }





public virtual string  DiscountType { get ; set ; }





public virtual decimal ? DiscountInRupees { get ; set ; }





public virtual int ? TaxRefNo { get ; set ; }





public virtual string  VendorPurchaseFormulaId { get ; set ; }





public virtual decimal ? MarkupPerc { get ; set ; }





public virtual decimal ? MarkupValue { get ; set ; }





public virtual decimal ? SchemeDiscountValue { get ; set ; }





public virtual bool ?  IsApplyTaxFreeQty { get ; set ; }





public virtual decimal ? FreeQtyTotalTaxValue { get ; set ; }



}

}
