
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseVendorOrderItem : BaseContract
{




public virtual string  VendorOrderId { get ; set ; }





public virtual string  ProductId { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? ReceivedQty { get ; set ; }




[Required]
public virtual decimal ? VAT { get ; set ; }





public virtual decimal ? PackageSize { get ; set ; }





public virtual decimal ? PackageQty { get ; set ; }





public virtual decimal ? PurchasePrice { get ; set ; }





public virtual string  VendorPurchaseId { get ; set ; }





public virtual string  StockTransferId { get ; set ; }





public virtual decimal ? SellingPrice { get ; set ; }





public virtual DateTime ? ExpireDate { get ; set ; }





public virtual string  ToInstanceId { get ; set ; }





public virtual bool ?  IsDeleted { get ; set ; }





public virtual decimal ? Igst { get ; set ; }





public virtual decimal ? Cgst { get ; set ; }





public virtual decimal ? Sgst { get ; set ; }





public virtual decimal ? GstTotal { get ; set ; }


public virtual int? OrderItemSno { get; set; }
}

}
