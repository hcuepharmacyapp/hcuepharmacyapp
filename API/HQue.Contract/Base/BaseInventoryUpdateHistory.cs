
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseInventoryUpdateHistory : BaseContract
{



[Required]
public virtual string  ProductStockId { get ; set ; }




[Required]
public virtual string  BatchNoOld { get ; set ; }




[Required]
public virtual string  BatchNoNew { get ; set ; }




[Required]
public virtual DateTime ? ExpireDateOld { get ; set ; }




[Required]
public virtual DateTime ? ExpireDateNew { get ; set ; }
public virtual decimal ? VATOld { get ; set ; }
public virtual decimal ? VATNew { get ; set ; }
//added by nandhini
 public virtual decimal? GSTOld { get; set; }
 public virtual decimal? GSTNew { get; set; }
 //end
 public virtual decimal ? PackageSizeOld { get ; set ; }





public virtual decimal ? PackageSizeNew { get ; set ; }





public virtual decimal ? PackagePurchasePriceOld { get ; set ; }





public virtual decimal ? PackagePurchasePriceNew { get ; set ; }




[Required]
public virtual decimal ? SellingPriceOld { get ; set ; }




[Required]
public virtual decimal ? SellingPriceNew { get ; set ; }





public virtual decimal ? MRPOld { get ; set ; }





public virtual decimal ? MRPNew { get ; set ; } 



}

}
