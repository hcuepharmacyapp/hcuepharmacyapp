
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class Basetmp_VendorPurchase_error : BaseContract
{




public virtual string  VendorId { get ; set ; }




[Required]
public virtual string  InvoiceNo { get ; set ; }





public virtual DateTime ? InvoiceDate { get ; set ; }





public virtual string  VerndorOrderId { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? Credit { get ; set ; }





public virtual string  PaymentType { get ; set ; }





public virtual string  ChequeNo { get ; set ; }





public virtual DateTime ? ChequeDate { get ; set ; }





public virtual string  GoodsRcvNo { get ; set ; }





public virtual string  Comments { get ; set ; }





public virtual string  FileName { get ; set ; }





public virtual int ? CreditNoOfDays { get ; set ; }



}

}
