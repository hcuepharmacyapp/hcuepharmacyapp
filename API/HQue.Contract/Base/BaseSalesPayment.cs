
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSalesPayment : BaseContract
{



[Required]
public virtual string  SalesId { get ; set ; }




[Required]
public virtual int ? PaymentInd { get ; set ; }



public virtual int? SubPaymentInd { get; set; }




[Required]
public virtual decimal ? Amount { get ; set ; }




[Required]
public virtual bool ?  isActive { get ; set ; }





public virtual string  CardNo { get ; set ; }





public virtual string  CardDigits { get ; set ; }





public virtual string  CardName { get ; set ; }





public virtual DateTime ? CardDate { get ; set ; }





public virtual string  CardTransId { get ; set ; }





public virtual string  ChequeNo { get ; set ; }





public virtual DateTime ? ChequeDate { get ; set ; }





public virtual string  WalletTransId { get ; set ; }



}

}
