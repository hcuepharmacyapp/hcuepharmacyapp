
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSmsLog : BaseContract
{



public virtual DateTime? SmsDate { get; set; }



public virtual string LogType { get ; set ; }



public virtual string ReceiverName { get ; set ; }



public virtual string InvoiceNo { get ; set ; }


public virtual DateTime? InvoiceDate { get; set; }


public virtual string ReferenceId { get; set; }


public virtual int? NoOfSms { get; set; }


public virtual long? OpeningCount { get; set; }



public virtual long? ClosingCount { get; set; }


    }

}
