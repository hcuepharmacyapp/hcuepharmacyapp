
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSalesBatchPopUpSettings : BaseContract
{




public virtual bool ?  Quantity { get ; set ; }





public virtual bool ?  Batch { get ; set ; }





public virtual bool ?  Expiry { get ; set ; }





public virtual bool ?  Vat { get ; set ; }





public virtual bool ?  GST { get ; set ; }





public virtual bool ?  UnitMrp { get ; set ; }





public virtual bool ?  StripMrp { get ; set ; }





public virtual bool ?  UnitsPerStrip { get ; set ; }





public virtual bool ?  Profit { get ; set ; }





public virtual bool ?  RackNo { get ; set ; }





public virtual bool ?  Generic { get ; set ; }




public virtual bool? Mfg { get; set; }





public virtual bool ?  Category { get ; set ; }





public virtual bool ?  Age { get ; set ; }





public virtual bool ?  Supplier { get ; set ; }



}

}
