
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

    public class BaseVendor : BaseContract
    {



        [Required]
        public virtual string Code { get; set; }




        [Required]
        public virtual string Name { get; set; }





        public virtual string Mobile { get; set; }





        public virtual string Phone { get; set; }





        public virtual string Email { get; set; }





        public virtual string Address { get; set; }





        public virtual string Area { get; set; }





        public virtual string City { get; set; }





        public virtual string State { get; set; }





        public virtual string Pincode { get; set; }




        [Required]
        public virtual bool? AllowViewStock { get; set; }





        public virtual string DrugLicenseNo { get; set; }





        public virtual string TinNo { get; set; }




        [Required]
        public virtual int? Status { get; set; }





        public virtual string PaymentType { get; set; }





        public virtual int? CreditNoOfDays { get; set; }





        public virtual bool? EnableCST { get; set; }





        public virtual string ContactPerson { get; set; }





        public virtual string Designation { get; set; }





        public virtual string Landmark { get; set; }



        public virtual string CSTNo { get; set; }



        public virtual decimal? Discount { get; set; }



        public virtual string Mobile1 { get; set; }



        public virtual string Mobile2 { get; set; }



        public virtual bool? Mobile1checked { get; set; }



        public virtual bool? Mobile2checked { get; set; }



        public virtual bool? IsSendMail { get; set; }



        public virtual string TaxType { get; set; }



        public virtual string GsTinNo { get; set; }



        public virtual int? LocationType { get; set; }



        public virtual string StateId { get; set; }



        public virtual string Ext_RefId { get; set; }


        public virtual int? VendorType { get; set; }


        public virtual decimal? BalanceAmount { get; set; }


        public virtual string VendorPurchaseFormulaId { get; set; }

        public virtual bool? IsHeadOfficeVendor { get; set; }



    }

}
