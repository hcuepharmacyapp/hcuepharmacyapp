
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseSalesReturnItemAudit : BaseContract
{




public virtual string  SalesReturnId { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual int ? CancelType { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual decimal ? MrpSellingPrice { get ; set ; }





public virtual decimal ? MRP { get ; set ; }





public virtual bool ?  IsDeleted { get ; set ; }





public virtual decimal ? Igst { get ; set ; }





public virtual decimal ? Cgst { get ; set ; }





public virtual decimal ? Sgst { get ; set ; }





public virtual decimal ? GstTotal { get ; set ; }




public virtual string Action { get; set; }




}

}
