
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseAccount : BaseContract
{




public virtual string  Code { get ; set ; }





public virtual string  Name { get ; set ; }





public virtual DateTime ? RegistrationDate { get ; set ; }




[Required]
public virtual int ? InstanceCount { get ; set ; }





public virtual string  Phone { get ; set ; }





public virtual string  Email { get ; set ; }





public virtual string  Address { get ; set ; }





public virtual string  Area { get ; set ; }





public virtual string  City { get ; set ; }





public virtual string  State { get ; set ; }





public virtual string  Pincode { get ; set ; }





public virtual int  RegisterType { get ; set ; }





public virtual bool ?  isChain { get ; set ; }





public virtual int ? LicensePeriod { get ; set ; }





public virtual DateTime ? LicenseStartDate { get ; set ; }





public virtual DateTime ? LicenseEndDate { get ; set ; }





public virtual int ? LicenseCount { get ; set ; }





public virtual bool ?  IsAMC { get ; set ; }





public virtual bool ?  IsApproved { get ; set ; }





public virtual string  Network { get ; set ; }





public virtual bool ?  IsBillNumberReset { get ; set ; }


public virtual int? InstanceTypeId { get; set; }



}

}
