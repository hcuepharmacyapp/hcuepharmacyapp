
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasebakProductStock : BaseContract
{



[Required]
public virtual string  ProductId { get ; set ; }





public virtual string  VendorId { get ; set ; }




[Required]
public virtual string  BatchNo { get ; set ; }




[Required]
public virtual DateTime ? ExpireDate { get ; set ; }




[Required]
public virtual decimal ? VAT { get ; set ; }




[Required]
public virtual decimal ? SellingPrice { get ; set ; }





public virtual string  PurchaseBarcode { get ; set ; }




[Required]
public virtual decimal ? Stock { get ; set ; }





public virtual decimal ? PackageSize { get ; set ; }





public virtual decimal ? PackagePurchasePrice { get ; set ; }





public virtual decimal ? PurchasePrice { get ; set ; }





public virtual decimal ? CST { get ; set ; }





public virtual bool ?  IsMovingStock { get ; set ; }





public virtual decimal ? ReOrderQty { get ; set ; }





public virtual int ? Status { get ; set ; }





public virtual bool ?  IsMovingStockExpire { get ; set ; }



}

}
