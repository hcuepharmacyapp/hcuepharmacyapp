
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseProductInstance : BaseContract
{




public virtual string  ProductId { get ; set ; }





public virtual decimal ? ReOrderLevel { get ; set ; }





public virtual decimal ? ReOrderQty { get ; set ; }





public virtual bool ?  ReOrderModified { get ; set ; }





public virtual string  RackNo { get ; set ; }





public virtual string  BoxNo { get ; set ; }


        //Added by Poongodi on 03/08/2017
        public virtual string Ext_RefId { get; set; }

    }

}
