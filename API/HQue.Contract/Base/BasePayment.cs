
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BasePayment : BaseContract
{




public virtual DateTime ? TransactionDate { get ; set ; }





public virtual string  VendorId { get ; set ; }





public virtual decimal ? Debit { get ; set ; }





public virtual decimal ? Credit { get ; set ; }





public virtual string  PaymentType { get ; set ; }





public virtual string  PaymentMode { get ; set ; }





public virtual string  BankName { get ; set ; }





public virtual string  BankBranchName { get ; set ; }





public virtual string  IfscCode { get ; set ; }





public virtual string  ChequeNo { get ; set ; }





public virtual DateTime ? ChequeDate { get ; set ; }





public virtual string  VendorPurchaseId { get ; set ; }





public virtual string  Remarks { get ; set ; }





public virtual string  PaymentHistoryStatus { get ; set ; }





public virtual int ? Status { get ; set ; }



}

}
