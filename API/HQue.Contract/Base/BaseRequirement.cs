
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseRequirement : BaseContract
{



[Required]
public virtual string  GatheredBy { get ; set ; }




[Required]
public virtual string  RequirementNo { get ; set ; }




[Required]
public virtual string  Description { get ; set ; }




[Required]
public virtual int  Type { get ; set ; }





public virtual string  SolvedBy { get ; set ; }





public virtual DateTime ? SolvedDate { get ; set ; }





public virtual string  Remarks { get ; set ; }





public virtual string  Status { get ; set ; }



}

}
