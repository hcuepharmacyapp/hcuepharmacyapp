
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseVoucher : BaseContract
{




public virtual string  ReturnId { get ; set ; }





public virtual string  ParticipantId { get ; set ; }





public virtual int ? TransactionType { get ; set ; }





public virtual int ? VoucherType { get ; set ; }





public virtual decimal ? OriginalAmount { get ; set ; }





public virtual int ? Factor { get ; set ; }





public virtual decimal ? Amount { get ; set ; }





public virtual decimal ? ReturnAmount { get ; set ; }





public virtual bool ?  Status { get ; set ; }



}

}
