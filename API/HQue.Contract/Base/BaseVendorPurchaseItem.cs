
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseVendorPurchaseItem : BaseContract
{




public virtual string  VendorPurchaseId { get ; set ; }





public virtual string  ProductName { get ; set ; }





public virtual string  ProductStockId { get ; set ; }





public virtual decimal ? PackageSize { get ; set ; }





public virtual decimal ? PackageQty { get ; set ; }





public virtual decimal ? PackagePurchasePrice { get ; set ; }





public virtual decimal ? PackageSellingPrice { get ; set ; }





public virtual decimal ? PackageMRP { get ; set ; }





public virtual decimal ? Quantity { get ; set ; }





public virtual decimal ? PurchasePrice { get ; set ; }





public virtual decimal ? FreeQty { get ; set ; }





public virtual decimal ? Discount { get ; set ; }





public virtual int ? Status { get ; set ; }





public virtual decimal ? VAT { get ; set ; }





public virtual decimal ? VatValue { get ; set ; }





public virtual decimal ? DiscountValue { get ; set ; }





public virtual string  fromTempId { get ; set ; }





public virtual string  fromDcId { get ; set ; }





public virtual bool ?  IsPoItem { get ; set ; }





public virtual string  HsnCode { get ; set ; }





public virtual decimal ? Igst { get ; set ; }





public virtual decimal ? Cgst { get ; set ; }





public virtual decimal ? Sgst { get ; set ; }





public virtual decimal ? GstTotal { get ; set ; }





public virtual decimal ? GstValue { get ; set ; }





public virtual decimal ? MarkupPerc { get ; set ; }





public virtual decimal ? MarkupValue { get ; set ; }





public virtual decimal ? SchemeDiscountPerc { get ; set ; }





public virtual decimal ? SchemeDiscountValue { get ; set ; }





public virtual decimal ? FreeQtyTaxValue { get ; set ; }


 public virtual int? VendorPurchaseItemSno { get; set; }
    }

}
