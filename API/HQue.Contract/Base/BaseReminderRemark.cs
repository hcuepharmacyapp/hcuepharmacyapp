
using System;
using System.ComponentModel.DataAnnotations;

namespace HQue.Contract.Base
{

public class BaseReminderRemark : BaseContract
{




public virtual string  UserReminderId { get ; set ; }





public virtual DateTime ? RemarksDate { get ; set ; }





public virtual DateTime ? RemarksFor { get ; set ; }





public virtual string  Remarks { get ; set ; }



}

}
