﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.Contract.Base
{
    public class BaseStockMovement : BaseContract
    {
        public virtual string ProductId { get; set; }

        public virtual string ProductName { get; set; }
        public virtual int OpeningQty{ get; set; }
        public virtual int CreatedOpeningStockQty { get; set; } //Stock created through inventort opening stock
        public virtual int ImportQty { get; set; }
        public virtual int PurchaseQty { get; set; }
        public virtual int PurchaseReturnQty { get; set; }
        public virtual int SalesQty { get; set; }
        public virtual int SalesReturnQty { get; set; }
        public virtual int AdjustmentInQty { get; set; }
        public virtual int AdjustmentOutQty { get; set; }
        public virtual int TransferInQty { get; set; }
        public virtual int TransferOutQty { get; set; }
        public virtual int DCQty { get; set; }
        public virtual int TempStockQty { get; set; }
        public virtual int SelfConsumption { get; set; }
        
    }
}
