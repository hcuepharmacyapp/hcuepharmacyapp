﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Accounts;
using HQue.DataAccess.DbModel;
using DataAccess.QueryBuilder;
using DataAccess.ManageData.Interface;
using Utilities.Enum;
using Utilities.Helpers;

namespace HQue.DataAccess.Accounts
{
    public class PettyCashSettingsDataAccess : BaseDataAccess
    {
        public PettyCashSettingsDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory)
            : base(sqlQueryExecuter, queryBuilderFactory)
        {
        }
        public override Task<PettyCashSettings> Save<PettyCashSettings>(PettyCashSettings data)
        {
            return Insert(data, PettyCashSettingsTable.Table);
        }

        public Task<List<PettyCash>> ListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(type, from, to, qb);

            qb.AddColumn(PettyCashTable.DescriptionColumn, PettyCashTable.AmountColumn,
                PettyCashTable.TransactionDateColumn, PettyCashTable.CreatedAtColumn);
            qb.ConditionBuilder.And(PettyCashTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(PettyCashTable.AccountIdColumn, accountId);

            qb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, PettyCashTable.UserIdColumn);
            qb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);

            qb.SelectBuilder.SortBy(PettyCashTable.CreatedAtColumn);

            qb.SelectBuilder.MakeDistinct = true;
            return List<PettyCash>(qb);
        }

        public async Task<int> Count(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(type, from, to, qb);
            qb.ConditionBuilder.And(PettyCashTable.InstanceIdColumn, instanceId);

            return Convert.ToInt32(await SingleValueAsyc(qb));
        }


        private static QueryBuilderBase SqlQueryBuilder(string type, DateTime from, DateTime to, QueryBuilderBase qb)
        {
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(PettyCashTable.TransactionDateColumn, CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from);
                qb.Parameters.Add("FilterToDate", to);
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(PettyCashTable.TransactionDateColumn, CustomDateTime.IST);
                        break;

                    case "WEEK":
                        qb.ConditionBuilder.And(PettyCashTable.TransactionDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int) DateTime.Today.DayOfWeek*-1));
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST);
                        break;

                    case "MONTH":
                        qb.ConditionBuilder.And(PettyCashTable.TransactionDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1));
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST);
                        break;
                }
            }

            return qb;
        }
    }
}
