﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Accounts;
using HQue.DataAccess.DbModel;
using DataAccess.QueryBuilder;
using DataAccess.ManageData.Interface;
using Utilities.Enum;
using Utilities.Helpers;
using DataAccess;
using System.Linq;
namespace HQue.DataAccess.Accounts
{
    public class PettyCashHdrDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        public PettyCashHdrDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper)
            : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }

        public override Task<PettyCashHdr> Save<PettyCashHdr>(PettyCashHdr data)
        {
            return Insert(data, PettyCashHdrTable.Table);
        }

        public Task<List<PettyCashHdr>> ListData(string type, string accountId, string instanceId, DateTime from, DateTime to, string userId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashHdrTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(type, from, to, qb);

            qb.AddColumn(PettyCashHdrTable.IdColumn,PettyCashHdrTable.DescriptionColumn, PettyCashHdrTable.clbalanceColumn,
                PettyCashHdrTable.TotalSalesColumn, PettyCashHdrTable.TotalExpenseColumn,
                PettyCashHdrTable.VendorPaymentColumn, PettyCashHdrTable.CustomerPaymentColumn,
                 PettyCashHdrTable.FloatingAmountColumn,
                PettyCashHdrTable.noof2000Column, PettyCashHdrTable.noof200Column,
                PettyCashHdrTable.noof1000Column, PettyCashHdrTable.noof100Column,
                PettyCashHdrTable.noof10Column, PettyCashHdrTable.noof50Column,
                PettyCashHdrTable.noof20Column, PettyCashHdrTable.OthersColumn,
                PettyCashHdrTable.coinsColumn, PettyCashHdrTable.noof500Column,
                PettyCashHdrTable.TransactionDateColumn, PettyCashHdrTable.CreatedAtColumn);

            qb.ConditionBuilder.And(PettyCashHdrTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(PettyCashHdrTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(PettyCashHdrTable.CreatedByColumn, userId);

            qb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, PettyCashHdrTable.UserIdColumn);
            qb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);

            qb.SelectBuilder.SortBy(PettyCashHdrTable.CreatedAtColumn);

            qb.SelectBuilder.MakeDistinct = true;
            return List<PettyCashHdr>(qb);
        }

        public async Task<int> Count(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashHdrTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(type, from, to, qb);
            qb.ConditionBuilder.And(PettyCashHdrTable.InstanceIdColumn, instanceId);

            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public async Task<string> getLatestupdatedDate(string accountId, string instanceId, string userId)
        {
            string query = $@" select max(transactiondate) from PettyCashHdr Where AccountId='{accountId}' and InstanceId = '{instanceId}' and UserId = '{userId}' ";
            var qdresult = QueryBuilderFactory.GetQueryBuilder(query);
            return Convert.ToString(await QueryExecuter.SingleValueAsyc(qdresult));
        }

        public async Task<PettyCashHdr> UpdateHdr(PettyCashHdr data)
        {
           
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashHdrTable.Table, OperationType.Update);
            qb.AddColumn(PettyCashHdrTable.clbalanceColumn,
                PettyCashHdrTable.TotalSalesColumn, PettyCashHdrTable.TotalExpenseColumn,
                PettyCashHdrTable.VendorPaymentColumn, PettyCashHdrTable.CustomerPaymentColumn,
                PettyCashHdrTable.FloatingAmountColumn,
                PettyCashHdrTable.noof2000Column, PettyCashHdrTable.noof200Column,
                PettyCashHdrTable.noof1000Column, PettyCashHdrTable.noof100Column,
                PettyCashHdrTable.noof10Column, PettyCashHdrTable.noof50Column,
                PettyCashHdrTable.noof20Column, PettyCashHdrTable.OthersColumn,
                PettyCashHdrTable.coinsColumn, PettyCashHdrTable.noof500Column,
                PettyCashHdrTable.TransactionDateColumn, PettyCashHdrTable.UpdatedAtColumn, PettyCashHdrTable.OfflineStatusColumn);
                
            qb.ConditionBuilder.And(PettyCashHdrTable.IdColumn);

            qb.Parameters.Add(PettyCashHdrTable.IdColumn.ColumnName, data.Id);
            qb.Parameters.Add(PettyCashHdrTable.clbalanceColumn.ColumnName, data.clbalance);
            qb.Parameters.Add(PettyCashHdrTable.TotalSalesColumn.ColumnName, data.TotalSales);
            qb.Parameters.Add(PettyCashHdrTable.TotalExpenseColumn.ColumnName, data.TotalExpense);
            qb.Parameters.Add(PettyCashHdrTable.VendorPaymentColumn.ColumnName, data.VendorPayment);
            qb.Parameters.Add(PettyCashHdrTable.CustomerPaymentColumn.ColumnName, data.CustomerPayment);
            qb.Parameters.Add(PettyCashHdrTable.FloatingAmountColumn.ColumnName, data.FloatingAmount);
            qb.Parameters.Add(PettyCashHdrTable.noof2000Column.ColumnName, data.noof2000);
            qb.Parameters.Add(PettyCashHdrTable.noof1000Column.ColumnName, data.noof1000);
            qb.Parameters.Add(PettyCashHdrTable.noof500Column.ColumnName, data.noof500);
            qb.Parameters.Add(PettyCashHdrTable.noof200Column.ColumnName, data.noof200);
            qb.Parameters.Add(PettyCashHdrTable.noof100Column.ColumnName, data.noof100);
            qb.Parameters.Add(PettyCashHdrTable.noof50Column.ColumnName, data.noof50);
            qb.Parameters.Add(PettyCashHdrTable.noof20Column.ColumnName, data.noof20);
            qb.Parameters.Add(PettyCashHdrTable.noof10Column.ColumnName, data.noof10);
            qb.Parameters.Add(PettyCashHdrTable.coinsColumn.ColumnName, data.coins);
            qb.Parameters.Add(PettyCashHdrTable.OthersColumn.ColumnName, data.Others);
            qb.Parameters.Add(PettyCashHdrTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
            qb.Parameters.Add(PettyCashHdrTable.TransactionDateColumn.ColumnName, data.TransactionDate);
            qb.Parameters.Add(ToolTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);

            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }

        public async Task<decimal> GetSalesValue(string AccId, string InsId, DateTime from, DateTime to ,string UserId, bool offlineStatus)
        {
            ///var date1 = from.ToFormat();
          
            bool OfflineStatus = Convert.ToBoolean(offlineStatus);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccId);
            parms.Add("InstanceId", InsId);
            parms.Add("fromdate", from);
            parms.Add("todate", to);
            parms.Add("UserId", UserId);
            parms.Add("offlineStatus", OfflineStatus);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesValueforAccount", parms);

            var salesValue = result.Select(x =>
            {
                return x.Cash as decimal? ?? 0;
            });

            var cashValue = (decimal)salesValue.FirstOrDefault();

            return cashValue;
        }

        public async Task<decimal> GetVendorPaymentValue(string AccId, string InsId, DateTime from, DateTime to,string UserId, bool offlineStatus)
        {
            bool OfflineStatus = Convert.ToBoolean(offlineStatus);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccId);
            parms.Add("InstanceId", InsId);
            parms.Add("fromdate", from);
            parms.Add("todate", to);
            parms.Add("UserId", UserId);
            parms.Add("offlineStatus", OfflineStatus);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetVendorPaymentValue", parms);

            var salesValue = result.Select(x =>
            {
                return x.ReturnValue as decimal? ?? 0;
            });

            var cashValue = (decimal)salesValue.FirstOrDefault();

            return cashValue;
        }

        public async Task<decimal> GetCustomerPaymentValue(string AccId, string InsId, DateTime from, DateTime to, string UserId, bool offlineStatus)
        {
            bool OfflineStatus = Convert.ToBoolean(offlineStatus);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccId);
            parms.Add("InstanceId", InsId);
            parms.Add("fromdate", from);
            parms.Add("todate", to);
            parms.Add("UserId", UserId);
            parms.Add("offlineStatus", OfflineStatus);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetCustomerPaymentValue", parms);

            var salesValue = result.Select(x =>
            {
                return x.ReturnValue as decimal? ?? 0;
            });

            var cashValue = (decimal)salesValue.FirstOrDefault();

            return cashValue;
        }

        private static QueryBuilderBase SqlQueryBuilder(string type, DateTime from, DateTime to, QueryBuilderBase qb)
        {
            var counterNo = 1;
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            if (string.IsNullOrEmpty(type) || type == "null")
            {
                qb.ConditionBuilder.And(PettyCashHdrTable.TransactionDateColumn, CriteriaEquation.Between);
                //  qb.Parameters.Add("FilterFromDate", from.ToFormat() + " 00:00:00");
                // qb.Parameters.Add("FilterToDate", to.ToFormat() + " 23:59:59");

                qb.Parameters.Add("FilterFromDate", from);
                qb.Parameters.Add("FilterToDate", to);

            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(PettyCashHdrTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(PettyCashHdrTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(PettyCashHdrTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
                filterFromDate = filterFromDate + " 00:00:00";
                filterToDate = filterToDate + " 23:59:59";
                qb.Parameters.Add("FilterFromDate", filterFromDate);
                qb.Parameters.Add("FilterToDate", filterToDate);


            }
           // qb.ConditionBuilder.And(PettyCashHdrTable.CounterNoColumn, CriteriaEquation.Equal);
          //  qb.Parameters.Add("CounterNo", counterNo);
            return qb;
        }
    }
}
