﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Accounts;
using HQue.DataAccess.DbModel;
using System;
using DataAccess.QueryBuilder;
using DataAccess.ManageData.Interface;
using HQue.DataAccess.Inventory;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Enum;
using DataAccess.Criteria;
using System.Linq;
using Utilities.Helpers;
using DataAccess;

namespace HQue.DataAccess.Accounts
{
    public class CustomerReceiptDataAccess : BaseDataAccess
    {
        private readonly SalesDataAccess _salesDataAccess;

        SqlDatabaseHelper sqldb;

        private readonly ConfigHelper _configHelper;

        public CustomerReceiptDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, SalesDataAccess salesDataAccess, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _salesDataAccess = salesDataAccess;
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }

        public override Task<CustomerPayment> Save<CustomerPayment>(CustomerPayment data)
        {
            return Insert(data, CustomerPaymentTable.Table);
        }

        public async Task<List<Sales>> CustomerPaymentList(string accountId, string instanceId, string customerId)
        {
            var GetCustomer = "";
            if (customerId != null)
            {
                GetCustomer = "and Sales.Name ='" + customerId + "'";
            }
            var query = $@" SELECT Name,Mobile,sum(Credit) as Credit,Convert(decimal(18,2),sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END)  * salesitem.Quantity - ((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * sales.Discount / 100) ))-sum(Credit) as Balance
   from Sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id WHERE Sales.accountId='{accountId}' and Sales.instanceId ='{instanceId}' and CashType='credit' {GetCustomer} group by Name,Mobile";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<Sales>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var sales = new Sales
                    {
                        Name = reader["Name"].ToString(),
                        Mobile = reader["Mobile"].ToString(),
                        Credit = reader["Balance"] as decimal? ?? 0,
                    };
                    list.Add(sales);
                }
            }
            return list;
        }
        public async Task<Sales> updateSale(string accountId, string instanceId, Sales model)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);

            qb.ConditionBuilder.And(SalesTable.IdColumn, model.Id);

            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);

            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);

            qb.AddColumn(SalesTable.BankDepositedColumn, SalesTable.AmountCreditedColumn);

            qb.Parameters.Add(SalesTable.BankDepositedColumn.ColumnName, model.BankDeposited);

            qb.Parameters.Add(SalesTable.AmountCreditedColumn.ColumnName, model.AmountCredited);

            await QueryExecuter.NonQueryAsyc(qb);
            return model;
        }

        public async Task<Sales> updateCustomerPayment(string accountId, string instanceId, Sales model)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Update);

            qb.ConditionBuilder.And(CustomerPaymentTable.IdColumn, model.Id);

            qb.ConditionBuilder.And(CustomerPaymentTable.InstanceIdColumn, instanceId);

            qb.ConditionBuilder.And(CustomerPaymentTable.AccountIdColumn, accountId);

            qb.AddColumn(CustomerPaymentTable.BankDepositedColumn, CustomerPaymentTable.AmountCreditedColumn);

            qb.Parameters.Add(CustomerPaymentTable.BankDepositedColumn.ColumnName, model.BankDeposited);

            qb.Parameters.Add(CustomerPaymentTable.AmountCreditedColumn.ColumnName, model.AmountCredited);

            await QueryExecuter.NonQueryAsyc(qb);
           
            return model;
        }





        public async Task<List<Sales>> CustomerPaymentDetails(string accountId, string instanceId, string customerMobile, string customerName)
        {
            var query1 = $@" SELECT InvoiceNo,InvoiceDate,sum(Credit) as Credit from Sales WHERE Sales.accountId='{accountId}' and Sales.instanceId ='{instanceId}' and CashType='credit' and Sales.mobile='{customerMobile}' and Sales.Name='{customerName}' group by InvoiceDate,InvoiceNo";

            var query = $@" SELECT InvoiceNo,InvoiceDate, sum(Credit) as Credit, Convert(decimal(18, 2), sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity - ((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * sales.Discount / 100) ))-sum(Credit) as Balance
   from Sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id WHERE Sales.accountId = '{accountId}' and Sales.instanceId = '{instanceId}' and CashType = 'credit' and Sales.mobile='{customerMobile}' and Sales.Name='{customerName}' group by InvoiceDate,InvoiceNo order by InvoiceNo desc";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var list = new List<Sales>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var sales = new Sales
                    {

                        InvoiceNo = reader["InvoiceNo"].ToString(),
                        InvoiceDate = Convert.ToDateTime(reader["InvoiceDate"].ToString()),
                        Credit = reader["Balance"] as decimal? ?? 0,
                    };

                    list.Add(sales);
                }
            }

            return list;

        }

        public async Task<CustomerPayment> GetCustomerBalance(string accountId, string instanceId, string customerMobile, string customerName)
        {


            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.NameColumn, SalesTable.MobileColumn);
            qb.SelectBuilder.GroupBy(SalesTable.NameColumn, SalesTable.MobileColumn);
            qb.JoinBuilder.Join(CustomerPaymentTable.Table, CustomerPaymentTable.SalesIdColumn, SalesTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(CustomerPaymentTable.Table, DbColumn.SumColumn("", CustomerPaymentTable.DebitColumn), DbColumn.SumColumn("", CustomerPaymentTable.CreditColumn));

            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);

            if (!string.IsNullOrEmpty(customerName))
            {
                qb.ConditionBuilder.And(SalesTable.NameColumn, customerName);
            }

            if (!string.IsNullOrEmpty(customerMobile))
            {
                qb.ConditionBuilder.And(SalesTable.MobileColumn, customerMobile);
            }

            // Added Gavaskar 02/02/2017 Start

            var CancelIsNull = new CustomCriteria(SalesTable.CancelstatusColumn, CriteriaCondition.IsNull);
            qb.ConditionBuilder.And(CancelIsNull);

            // Added Gavaskar 02/02/2017 End

            var CustomerPayment = new CustomerPayment();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    CustomerPayment.CustomerName = reader[SalesTable.NameColumn.ColumnName].ToString();
                    CustomerPayment.CustomerMobile = reader[SalesTable.MobileColumn.ColumnName].ToString();
                    CustomerPayment.Credit = reader[CustomerPaymentTable.CreditColumn.FullColumnName] as decimal? ?? 0;
                    CustomerPayment.Debit = reader[CustomerPaymentTable.DebitColumn.FullColumnName] as decimal? ?? 0;
                }
            }

            var cusPay = new CustomerPayment();

            var pqb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            pqb.AddColumn(PatientTable.NameColumn, PatientTable.MobileColumn);
            pqb.SelectBuilder.GroupBy(PatientTable.NameColumn, PatientTable.MobileColumn);
            pqb.JoinBuilder.Join(CustomerPaymentTable.Table, CustomerPaymentTable.CustomerIdColumn, PatientTable.IdColumn);
            pqb.JoinBuilder.AddJoinColumn(CustomerPaymentTable.Table, DbColumn.SumColumn("", CustomerPaymentTable.DebitColumn), DbColumn.SumColumn("", CustomerPaymentTable.CreditColumn));
            pqb.ConditionBuilder.And(PatientTable.InstanceIdColumn, instanceId);
            pqb.ConditionBuilder.And(PatientTable.AccountIdColumn, accountId);


            if (!string.IsNullOrEmpty(customerName))
            {
                pqb.ConditionBuilder.And(PatientTable.NameColumn, customerName);
            }

            if (!string.IsNullOrEmpty(customerMobile))
            {
                pqb.ConditionBuilder.And(PatientTable.MobileColumn, customerMobile);
            }

            using (var reader = await QueryExecuter.QueryAsyc(pqb))
            {
                while (reader.Read())
                {
                    cusPay.CustomerName = reader[PatientTable.NameColumn.ColumnName].ToString();
                    cusPay.CustomerMobile = reader[PatientTable.MobileColumn.ColumnName].ToString();
                    cusPay.Credit = reader[CustomerPaymentTable.CreditColumn.FullColumnName] as decimal? ?? 0;
                    cusPay.Debit = reader[CustomerPaymentTable.DebitColumn.FullColumnName] as decimal? ?? 0;
                }
            }

            if (!string.IsNullOrEmpty(cusPay.CustomerName)){
                if (!string.IsNullOrEmpty(CustomerPayment.CustomerName))
                {
                    CustomerPayment.Credit += cusPay.Credit;
                    CustomerPayment.Debit += cusPay.Debit;
                }else
                {
                    CustomerPayment = cusPay;
                }
            }


            return CustomerPayment;

            //var GetCustomer = "";
            //if (customerName != null)
            //{
            //    GetCustomer = "and Sales.Name ='" + customerName + "' and Sales.Mobile='" + customerMobile + "'";
            //}

            //var query = $@" SELECT Name,Mobile,SUM(Credit) as credit  from Sales WHERE Sales.accountId='{accountId}' and Sales.instanceId ='{instanceId}' and CashType='credit'  and Mobile!='' and Name!=''  {GetCustomer} group by Name,Mobile HAVING SUM(Credit) > 0";

            //var qb = QueryBuilderFactory.GetQueryBuilder(query);

            //var list = new List<Sales>();

            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var sales = new Sales
            //        {
            //            Name = reader["Name"].ToString(),
            //            Mobile = reader["Mobile"].ToString(),
            //            credit = reader["credit"] as decimal? ?? 0,
            //        };

            //        list.Add(sales);
            //    }
            //}

            //return list;

        }
        public async Task<List<Sales>> CustomerList(string accountId, string instanceId)
        {
            var query = $@" SELECT Name,Mobile from Sales WHERE Sales.accountId='{accountId}' and Sales.instanceId ='{instanceId}' and CashType='credit' group by Name,Mobile";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var list = new List<Sales>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var sales = new Sales
                    {
                        //Id = reader["Id"].ToString(),
                        Name = reader["Name"].ToString(),
                        Mobile = reader["Mobile"].ToString(),
                        //Credit = reader["Credit"] as decimal? ?? 0,
                    };
                    list.Add(sales);
                }
            }
            return list;
        }

        public async Task<List<Sales>> searchCustomer(string AccountId, string InstanceId, string keyword, string type)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", InstanceId);
            parms.Add("AccountId", AccountId);
            if (type == "mobile")
            {
                parms.Add("mobile", keyword);
                parms.Add("name", "");
            }
            if (type == "name")
            {
                parms.Add("mobile", "");
                parms.Add("name", keyword);
            }
            parms.Add("PaymentType", "cheque");
            var data = await sqldb.ExecuteProcedureAsync<Sales>("usp_SearchCustomer", parms);
            List<Sales> sales = new List<Sales>();
            var result = data.Select(x =>
            {
                var sale = new Sales
                {
                    Name = x.Name as string ?? "",
                    Mobile = x.Mobile as string ?? "",
                    PatientId = x.PatientId // Added by Violet
                };
                return sale;
            }).ToList();
            return result;

        }

        public async Task<List<Sales>> GetCustomerCheque(string AccountId, string InstanceId, string PatientId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("InstanceId", InstanceId);
            parms.Add("AccountId", AccountId);
            parms.Add("PatientId", PatientId);
            parms.Add("PaymentType", "cheque");

            var data = await sqldb.ExecuteProcedureAsync<Sales>("usp_GetCustomerCheque", parms);

            List<Sales> sales = new List<Sales>();

            var result = data.Select(x =>
            {
                var sale = new Sales
                {
                    Id = x.Id,
                    InvoiceSeries = x.InvoiceSeries,
                    InvoiceNo = x.InvoiceNo,
                    InvoiceDate = x.InvoiceDate,
                    Name = x.Name,
                    Mobile = x.Mobile,
                    PaymentType = x.PaymentType,
                    ChequeNo = x.ChequeNo,
                    ChequeDate = x.ChequeDate,
                    BankDeposited = x.BankDeposited,
                    AmountCredited = x.AmountCredited,
                    Amount = x.InvoiceAmount,
                    Type = x.Type
                };
                return sale;
            }).ToList();
            return result;
        }

        public async Task<Sales> UpdateSaleCredit(CustomerPayment data)
        {
            var List = await GetCustomerPaymentId(data.SalesId);
            var CreditAmount = "";
            if (List[0].Credit >= data.PaymentAmount)
            {
                CreditAmount = (List[0].Credit - data.PaymentAmount).ToString();
            }

            if(!string.IsNullOrEmpty(CreditAmount) && !string.IsNullOrWhiteSpace(CreditAmount)) //Added by Sarubala on 02-11-17
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(SalesTable.IdColumn, data.SalesId);
                qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, data.InstanceId);
                qb.AddColumn(SalesTable.CreditColumn);
                qb.Parameters.Add(SalesTable.CreditColumn.ColumnName, CreditAmount);
                await QueryExecuter.NonQueryAsyc(qb);
            }
            
            return data.Sales;
        }

        public async Task<CustomerPayment> UpdateCustomerPayment(CustomerPayment data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(CustomerPaymentTable.IdColumn, data.Id);
            qb.ConditionBuilder.And(CustomerPaymentTable.InstanceIdColumn, data.InstanceId);
            qb.AddColumn(CustomerPaymentTable.DebitColumn);
            qb.Parameters.Add(CustomerPaymentTable.DebitColumn.ColumnName, data.PaymentAmount);
            await QueryExecuter.NonQueryAsyc(qb);

            var List = await GetCustomerPaymentId(data.SalesId);
            var CreditAmount = (List[0].Credit + (data.Debit - data.PaymentAmount));
            qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(SalesTable.IdColumn, data.SalesId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, data.InstanceId);
            qb.AddColumn(SalesTable.CreditColumn);
            qb.Parameters.Add(SalesTable.CreditColumn.ColumnName, CreditAmount);
            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }

        public async Task<List<Sales>> GetCustomerPaymentId(string SalesId)
        {
            var query = $@"select Credit from sales where Id='{SalesId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return await List<Sales>(qb);
        }
        //Added by Settu to capture return payment adjustments 
        public async Task<Voucher> UpdateVoucherNote(Voucher data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VoucherTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(VoucherTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(VoucherTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(VoucherTable.IdColumn, data.Id);

            qb.AddColumn(VoucherTable.AmountColumn, VoucherTable.ReturnAmountColumn, VoucherTable.StatusColumn, VoucherTable.UpdatedAtColumn, VoucherTable.UpdatedByColumn, VoucherTable.OfflineStatusColumn);
            qb.Parameters.Add(VoucherTable.AmountColumn.ColumnName, data.Amount);
            qb.Parameters.Add(VoucherTable.ReturnAmountColumn.ColumnName, data.ReturnAmount);
            qb.Parameters.Add(VoucherTable.StatusColumn.ColumnName, data.Status);
            qb.Parameters.Add(VoucherTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.Parameters.Add(VoucherTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(VoucherTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }

        public async Task<Settlements> SaveSettlements(Settlements data)
        {
            return await Insert(data, SettlementsTable.Table);
        }
    }
}
