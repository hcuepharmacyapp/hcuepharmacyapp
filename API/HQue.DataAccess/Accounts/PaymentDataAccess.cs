﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Accounts;
using HQue.DataAccess.DbModel;
using System;
using DataAccess.QueryBuilder;
using DataAccess.ManageData.Interface;
using HQue.DataAccess.Inventory;
using HQue.Contract.Infrastructure.Inventory;
using Utilities.Enum;
using System.Linq;
using DataAccess.Criteria;
using DataAccess;
using Utilities.Helpers;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.External;

namespace HQue.DataAccess.Accounts
{
    public class PaymentDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        private readonly SalesDataAccess _salesDataAccess;
        private readonly PatientDataAccess _patientDataAccess;
        public PaymentDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper, SalesDataAccess salesDataAccess, PatientDataAccess patientDataAccess) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _salesDataAccess = salesDataAccess;
            _patientDataAccess = patientDataAccess;
            /*Config helper helps to get the connection string from config file*/
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        public override Task<Payment> Save<Payment>(Payment data)
        {  //added by nandhini for transaction
            if (data.WriteExecutionQuery)
            {
                SetInsertMetaData(data, PaymentTable.Table);
                WriteInsertExecutionQuery(data, PaymentTable.Table);
                return Task.FromResult(data);
            }
            else
            {
                return Insert(data, PaymentTable.Table);
            }
        }
        public async Task UpdatePreviousPaymentRecords(Payment data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PaymentTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(PaymentTable.VendorPurchaseIdColumn, data.VendorPurchaseId);
            qb.ConditionBuilder.And(PaymentTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(PaymentTable.DebitColumn, 0, CriteriaEquation.Equal);
            qb.AddColumn(PaymentTable.StatusColumn);
            qb.Parameters.Add(PaymentTable.StatusColumn.ColumnName, 2);
            if (data.WriteExecutionQuery)
            {
                data.AddExecutionQuery(qb);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qb);
            }

        }
        //public async Task UpdateAllPreviousPaymentStauts(string vendorpurchaseid)
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(PaymentTable.Table, OperationType.Update);
        //    qb.ConditionBuilder.And(PaymentTable.VendorPurchaseIdColumn, vendorpurchaseid);
        //    qb.AddColumn(PaymentTable.IsActiveColumn);
        //    qb.Parameters.Add(PaymentTable.IsActiveColumn.ColumnName, false);
        //    await QueryExecuter.NonQueryAsyc(qb);
        //}
        public async Task<List<Payment>> GetVendorInvoice(Payment pay1, string InstanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", pay1.InstanceId);
            parms.Add("AccountId", pay1.AccountId);
            parms.Add("VendorId", pay1.Vendor.Id);
            string mobile = null, operator1 = null;
            DateTime? date1 = null;
            if (pay1.select == "mobile")
            {
                mobile = pay1.Mobile;
            }
            else
            {
                date1 = pay1.CreditDate;
                operator1 = pay1.select1;
            }
            parms.Add("Mobile", mobile);
            parms.Add("CreditDate", date1);
            parms.Add("Operator", operator1);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetVendorPayments", parms);
            var paymentList = result.Select(x =>
            {
                var payment = new Payment
                {
                    VendorPurchase = new VendorPurchase
                    {
                        InvoiceNo = x.InvoiceNo.ToString(),
                        InvoiceDate = x.InvoiceDate as DateTime? ?? DateTime.MinValue,
                        GoodsRcvNo = x.GoodsRcvNo.ToString(),
                        BillSeries = x.BillSeries.ToString(),
                        PaymentType = x.PaymentType.ToString(),
                        CreditNoOfDays = Convert.ToInt32(x.CreditNoOfDays),
                    },
                    Debit = x.Debit as decimal? ?? 0,
                    Credit = Math.Round(x.Credit) as decimal? ?? 0,

                    VendorPurchaseId = (string.IsNullOrEmpty(x.VendorPurchaseId)) ? null : x.VendorPurchaseId.ToString(),
                    VendorId = x.Id.ToString(),
                    CreditExpireDate = x.CreditDate as DateTime? ?? DateTime.MinValue,
                    Vendor = new Contract.Infrastructure.Master.Vendor
                    {
                        Name = x.Name.ToString(),
                        Id = x.Id.ToString()
                    }
                };
                return payment;
            });
            var payList = paymentList.ToList();
            foreach (Payment pt in payList)
            {
                //var vendorPurchasePaymentItem = await GetPaymentItem(pt.VendorId, pt.VendorPurchaseId); // commented by sabarish for payment history
                var vendorPurchasePaymentItem = await GetPaymentItem(pt.VendorId, pt.VendorPurchaseId, InstanceId);
                foreach (var purchaseItem in vendorPurchasePaymentItem)
                {
                    pt.PaymentList.Add(purchaseItem);
                }
            }
            return payList;
        }
        public Task<FinYearMaster> saveFinancialYear(FinYearMaster data)
        {
            return Insert2(data, FinYearMasterTable.Table);
        }
        public async Task<FinYearMaster> resetFinyearmaster(FinYearMaster data)
        {
            var finyeardata = await getFinyearAvailable(data.AccountId);
            if (finyeardata == null)
            {
                return await Insert2(data, FinYearMasterTable.Table);
            }
            else
            {
                if (!string.IsNullOrEmpty(finyeardata.Id))
                {
                    return finyeardata;
                }
                else
                {
                    return await Insert2(data, FinYearMasterTable.Table);
                }
            }
        }
        public async Task<FinYearMaster> saveFinyearStatus(FinYearMaster data)
        {
            //FinYearResetStatus finyearresert = new FinYearResetStatus();
            //finyearresert.AccountId = data.AccountId;
            //finyearresert.InstanceId = data.InstanceId;
            //finyearresert.FinYearMasterId = data.Id;
            //finyearresert.FinancialYear = data.FinYearEndDate;
            //finyearresert.ResetStatus = data.FinYearStatus;
            //finyearresert.CreatedBy = data.CreatedBy;
            //finyearresert.UpdatedBy = data.UpdatedBy;
            //var result = await InsertFinyearStatus(finyearresert);
            //return result;

            //string query = $"update Account set IsBillNumberReset = {data.IsBillNumberReset}, UpdatedAt = '{CustomDateTime.IST.ToString("dd-MMM-yyyy HH':'mm':'ss.fff")}', UpdatedBy='{data.UpdatedBy}' where Id = '{data.AccountId}'";
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //await QueryExecuter.NonQueryAsyc(qb);
            //return data;

            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(AccountTable.IdColumn, data.AccountId);
            qb.AddColumn(AccountTable.IsBillNumberResetColumn);
            var lastYear = (await List<Account>(qb)).First();
            if (lastYear.IsBillNumberReset == null)
            {
                lastYear.IsBillNumberReset = false;
            }

            if ((lastYear.IsBillNumberReset == false && data.IsBillNumberReset == 1) || (lastYear.IsBillNumberReset == true && data.IsBillNumberReset == 1))
            {
                var query = $"SELECT COUNT(Id) FROM {FinYearMasterTable.TableName} WHERE {FinYearMasterTable.AccountIdColumn} = '{data.AccountId}' and {FinYearMasterTable.FinYearStartDateColumn}='{data.FinYearStartDate?.ToExternalFormat()}' and {FinYearMasterTable.FinYearEndDateColumn}='{data.FinYearEndDate?.ToExternalFormat()}'";
                qb = QueryBuilderFactory.GetQueryBuilder(query);

                var ncount = (await SingleValueAsyc(qb)) as int? ?? 0;

                if (ncount == 0)
                {
                    query = $@"INSERT INTO FinYearMaster(Id,AccountId,FinYearStartDate,FinYearEndDate,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy)
                        VALUES('{Guid.NewGuid().ToString()}','{data.AccountId}','{data.FinYearStartDate?.ToExternalFormat()}','{data.FinYearEndDate?.ToExternalFormat()}','{CustomDateTime.IST.ToString("dd-MMM-yyyy HH':'mm':'ss.fff")}','{CustomDateTime.IST.ToString("dd-MMM-yyyy HH':'mm':'ss.fff")}','{data.CreatedBy}','{data.UpdatedBy}')";
                    qb = QueryBuilderFactory.GetQueryBuilder(query);
                    await QueryExecuter.NonQueryAsyc2(qb);
                    WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "A" });
                }

                query = $"update Account set IsBillNumberReset = {data.IsBillNumberReset}, UpdatedAt = '{CustomDateTime.IST.ToString("dd-MMM-yyyy HH':'mm':'ss.fff")}', UpdatedBy='{data.UpdatedBy}' where Id = '{data.AccountId}'";
                qb = QueryBuilderFactory.GetQueryBuilder(query);
                await QueryExecuter.NonQueryAsyc2(qb);
                WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "A" });
            }
            else if ((lastYear.IsBillNumberReset == true && data.IsBillNumberReset == 0) || (lastYear.IsBillNumberReset == false && data.IsBillNumberReset == 0))
            {
                qb = QueryBuilderFactory.GetQueryBuilder(FinYearMasterTable.Table, OperationType.Select);
                qb.ConditionBuilder.And(FinYearMasterTable.AccountIdColumn, data.AccountId);
                qb.AddColumn(FinYearMasterTable.IdColumn);
                qb.SelectBuilder.SetTop(1);
                qb.SelectBuilder.SortByDesc(FinYearMasterTable.FinYearEndDateColumn);
                var lastFinYear = (await List<FinYearMaster>(qb)).First();

                var query = $@"UPDATE FinYearMaster SET FinYearEndDate = '{data.FinYearEndDate?.ToExternalFormat()}', UpdatedAt = '{CustomDateTime.IST.ToString("dd-MMM-yyyy HH':'mm':'ss.fff")}', UpdatedBy = '{data.UpdatedBy}' WHERE Id = '{lastFinYear.Id}'";
                qb = QueryBuilderFactory.GetQueryBuilder(query);
                await QueryExecuter.NonQueryAsyc2(qb);
                WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "A" });
            }

            return data;
        }
        public async Task<FinYearResetStatus> InsertFinyearStatus(FinYearResetStatus data)
        {
            return await Insert(data, FinYearResetStatusTable.Table);
        }
        public async Task<FinYearMaster> idofFinyear(FinYearMaster data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(FinYearMasterTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(FinYearMasterTable.AccountIdColumn, data.AccountId);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }
        public async Task<FinYearMaster> getFinyearAvailable(string AccountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(FinYearMasterTable.Table, OperationType.Select);
            qb.AddColumn(FinYearMasterTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(AccountTable.Table, FinYearMasterTable.AccountIdColumn, AccountTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(AccountTable.Table, AccountTable.IsBillNumberResetColumn);
            qb.ConditionBuilder.And(FinYearMasterTable.AccountIdColumn, AccountId);
            qb.SelectBuilder.SetTop(1);
            qb.SelectBuilder.SortByDesc(FinYearMasterTable.FinYearEndDateColumn);
            var result = new FinYearMaster();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                if (reader.Read())
                {
                    result.Id = reader[FinYearMasterTable.IdColumn.ColumnName].ToString();
                    result.AccountId = reader[FinYearMasterTable.AccountIdColumn.ColumnName].ToString();
                    result.InstanceId = reader[FinYearMasterTable.InstanceIdColumn.ColumnName].ToString();
                    result.FinYearStartDate = reader[FinYearMasterTable.FinYearStartDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue;
                    result.FinYearEndDate = reader[FinYearMasterTable.FinYearEndDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue;
                    result.Account1.IsBillNumberReset = reader[AccountTable.IsBillNumberResetColumn.FullColumnName] as bool? ?? null;
                }
            }
            if (result.Account1.IsBillNumberReset.HasValue)
            {
                result.IsBillNumberReset = Convert.ToInt16(result.Account1.IsBillNumberReset);
            }
            else
            {
                result.IsBillNumberReset = null;
            }
            // var result = (await List<FinYearMaster>(qb)).FirstOrDefault();
            result.FinYearStartDate = result.FinYearEndDate?.AddYears(-1).AddDays(1);
            return result;
        }
        //
        //history added by nandhini 20.9.17
        public async Task<List<Payment>> GetVendorHistory(Payment history,string InstanceId)
        {
            /*Prefix Added by Poongodi on 15/06/2017*/
            string mobile = "null";
            string id = "null";
            if (history.Mobile != "" && history.Mobile != null)
            {
                mobile = "'"+ history.Mobile + "'";
            }
            if (history.Vendor.Id != "" && history.Vendor.Id != null)
            {
                id = "'" +  history.Vendor.Id + "'";
            }
            var query =
             $@" SELECT b.* From
                (SELECT vendor.PaymentType,vendor.Name,vendor.id,VendorPurchase.InvoiceNo As InvoiceNo,VendorPurchase.GoodsRcvNo As GoodsRcvNo,isnull(VendorPurchase.Prefix,'')+ isnull(VendorPurchase.BillSeries,'') BillSeries ,VendorPurchase.InvoiceDate As InvoiceDate, VendorPurchaseId ,SUM(Isnull(Debit,0)) AS Debit,Isnull(dbo.CreditCalculate(VendorPurchaseId),0) AS Credit FROM Payment 
                INNER JOIN VendorPurchase ON Payment.VendorPurchaseId = VendorPurchase.Id
                LEFT JOIN VENDOR  ON vendor.id=VendorPurchase.VendorId
                WHERE VendorPurchase.VendorId = isnull({id},VendorPurchase.VendorId)
                AND  (vendor.Mobile = isnull({mobile},vendor.Mobile) OR vendor.Mobile is null)
                AND  VendorPurchase.InstanceId = '{InstanceId}'
                GROUP BY VendorPurchase.InvoiceNo,VendorPurchase.GoodsRcvNo,isnull(VendorPurchase.Prefix,'')+ isnull(VendorPurchase.BillSeries,'') ,VendorPurchase.InvoiceDate, VendorPurchaseId,vendor.Name,vendor.id,vendor.PaymentType
                HAVING SUM(Isnull(Debit,0)) > 0 
                UNION
                Select a.* from 
                (SELECT v.PaymentType,v.Name,v.id, '' as InvoiceNo,'' as GoodsRcvNo, '' as BillSeries,
                p.TransactionDate as InvoiceDate,null as 'VendorPurchaseId',SUM(Isnull(Debit,0)) AS Debit,
                SUM(Isnull(credit,0)) AS credit
                From Payment p inner join Vendor V ON p.VendorId = v.Id
                Where  p.VendorId = isnull({id},p.VendorId)
                AND  p.InstanceId = '{InstanceId}' 
                And p.VendorPurchaseId is null
                Group by v.PaymentType,v.Name,v.id, p.TransactionDate ) as a
                Group by PaymentType,Name,id,InvoiceDate,InvoiceNo,GoodsRcvNo,BillSeries,a.credit, a.debit,VendorPurchaseId
                Having (a.Debit) > 0 ) as b 
                Order by b.GoodsRcvNo asc    ";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<Payment>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var payment = new Payment
                    {
                        VendorPurchase =
                        {
                            InvoiceNo = reader["InvoiceNo"].ToString(),
                           //InvoiceDate = Convert.ToDateTime(reader["InvoiceDate"].ToString()),
                            InvoiceDate = string.IsNullOrEmpty(reader["InvoiceDate"].ToString()) ? (DateTime?)null : DateTime.Parse(reader["InvoiceDate"].ToString()),
                            GoodsRcvNo = reader["GoodsRcvNo"].ToString(),
                            BillSeries = reader["BillSeries"].ToString(),
                                                    },                        
                        //Id = reader["Id"].ToString(),
                        Debit = Convert.ToDecimal(reader["Debit"].ToString()),
                        Credit = Math.Round(Convert.ToDecimal(reader["Credit"].ToString()), 0, MidpointRounding.AwayFromZero),
                        VendorPurchaseId = reader["VendorPurchaseId"].ToString(),
                        Vendor = new Contract.Infrastructure.Master.Vendor
                        {
                            Name = reader["Name"].ToString(),
                              Id = reader["Id"].ToString()
                        }
                    };
                    var vendorPurchasePaymentItem = await GetPaymentItem(payment.Vendor.Id,payment.VendorPurchaseId, InstanceId);
                    foreach (var purchaseItem in vendorPurchasePaymentItem)
                    {
                        payment.PaymentList.Add(purchaseItem);
                    }
                    list.Add(payment);
                }
            }
            return list;
        }
        //
        public Task<List<Payment>> GetPaymentItem(string vendorId, string VendorPurchaseId,string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PaymentTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PaymentTable.VendorIdColumn, vendorId);
            if (!string.IsNullOrEmpty(VendorPurchaseId))
            {
                qb.ConditionBuilder.And(PaymentTable.VendorPurchaseIdColumn, VendorPurchaseId);
            }else
            {
                var statusCondition = new CustomCriteria(PaymentTable.VendorPurchaseIdColumn, CriteriaCondition.IsNull);
                var cc1 = new CriteriaColumn(PaymentTable.VendorPurchaseIdColumn, "''", CriteriaEquation.Equal);
                var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);
                qb.ConditionBuilder.And(col1);
            }
            
            qb.ConditionBuilder.And(PaymentTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(PaymentTable.DebitColumn, 0, CriteriaEquation.Greater);
            qb.AddColumn(PaymentTable.IdColumn, PaymentTable.TransactionDateColumn, PaymentTable.PaymentTypeColumn, PaymentTable.DebitColumn, PaymentTable.RemarksColumn,
                PaymentTable.PaymentModeColumn, PaymentTable.BankNameColumn, PaymentTable.ChequeNoColumn, PaymentTable.ChequeDateColumn);
            qb.SelectBuilder.SortByDesc(PaymentTable.CreatedAtColumn);
            return List<Payment>(qb);
        }
        #region
        //the following method renamed and the new method created to convert from direct qry to SP
        public async Task<List<Payment>> GetReportList_old(string accountId, string instanceId)
        {
            var query = $@" SELECT VendorId, Vendor.Name as Name, sum(Credit) - sum(debit) as Balance from payment Inner join Vendor on Vendor.Id = Payment.VendorId 
                        WHERE payment.accountId='{accountId}' and payment.instanceId ='{instanceId}' 
                        group by VendorId, Vendor.Name";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<Payment>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var payment = new Payment
                    {
                        Vendor = {
                            Name = reader["Name"].ToString()
                        },
                        Balance = Convert.ToDecimal(reader["Balance"].ToString()),
                    };
                    list.Add(payment);
                }
            }
            return list;
        }
        public async Task<List<Payment>> GetReportList(string accountId, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetVendorBalance", parms);
            var vendorPayment = result.Select(x =>
            {
                var payment = new Payment
                {
                    Vendor = {
                            Name = Convert.ToString(x.VendorName)
                        },
                    Balance = Convert.ToDecimal(x.Balance)
                };
                return payment;
            });
            return vendorPayment.ToList();
        }
        #endregion
        public async Task<List<CustomerPayment>> CustomerPaymentList(string accountId, string instanceId, string customername, string mobile)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);
            parms.Add("customername", customername);
            parms.Add("mobile", mobile);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_CustomerPaymentList", parms);
            var payment = result.Select(x =>
            {
                var customerPayment = new CustomerPayment
                {
                    Sales =
                    {
                        PatientId = x.PatientId,
                    },
                    CustomerName = x.CustomerName,
                    CustomerMobile = x.CustomerMobile,
                    CustomerCredit = x.CustomerCredit,
                    CustomerDebit = x.CustomerDebit,
                    SalesCredit = x.SalesCredit,
                    BalanceAmount = x.BalanceAmount,
                    CustomerStatus = x.CustomerStatus
                };
                return customerPayment;
            });
            return payment.ToList();
        }
        //Customer receipt shown by PatientId instead of patient mobile & name - Settu
        public async Task<Sales> CustomerPaymentDetails(string accountId, string instanceId, string PatientId)
        {
            var data = new Sales();
            data.CustomerPayments = await GetCustomerReceiptItems(instanceId, PatientId, accountId);
            data.VoucherList = await GetVoucherList(accountId, instanceId, PatientId);

            var patient = await _patientDataAccess.GetPatientById(PatientId);
            data.Name = patient.Name;
            data.Mobile = patient.Mobile;
            data.PatientId = PatientId;
            data.Patient.Status = patient.Status;

            return data;
        }
        // Added by Settu to take balance of sales return credit bills and show in customer receipt
        public async Task<List<Voucher>> GetVoucherList(string accountId, string instanceId, string PatientId)
        {
            var query = $@"SELECT SR.ReturnNo,ISNULL(SR.Prefix,'')+ISNULL(SR.InvoiceSeries,'') AS InvoiceSeries,SR.ReturnDate,V.Id,V.Amount,P.Id AS ParticipantId,V.OriginalAmount,V.ReturnId FROM Voucher V
                        INNER JOIN SalesReturn SR ON V.ReturnId = SR.Id
                        INNER JOIN Patient P ON P.Id = V.ParticipantId
                        WHERE V.AccountId = '{accountId}' AND V.InstanceId = '{instanceId}' AND P.Id = '{PatientId}' AND
                        V.TransactionType = 1 AND V.Amount > 0 ORDER BY V.CreatedAt";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var VoucherList = new List<Voucher>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    Voucher voucher = new Voucher
                    {
                        Id = reader["Id"].ToString(),
                        Amount = (decimal)reader["Amount"],
                        ReturnNo = reader["ReturnNo"].ToString(),
                        InvoiceSeries = reader["InvoiceSeries"].ToString(),
                        ReturnDate = (DateTime)reader["ReturnDate"],
                        ParticipantId = reader["ParticipantId"].ToString(),
                        OriginalAmount = (decimal)reader["OriginalAmount"],
                        ReturnId = reader["ReturnId"].ToString(),
                    };

                    VoucherList.Add(voucher);
                }
            }
            return VoucherList;
        }
        //Customer receipt shown by PatientId instead of patient mobile & name - Settu
        public async Task<List<CustomerPayment>> GetCustomerReceiptItems(string instanceId, string PatientId,string accountId)
        {
            /*Prefix Added by Poongodi on 15/06/2017*/
            /* Query Changed by Senthil.S for Opening Balance inclusion */
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("PatientId", PatientId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetCustomerReceiptItems", parms);            
            //var list = new List<CustomerPayment>();
            
            var list = result.Select(x =>
            {
                var customerPayment = new CustomerPayment
                {
                    SalesId = Convert.ToString(x.Id),
                    CustomerId = Convert.ToString(x.CustomerId),
                    InvoiceAmount = x.InvoiceValue as decimal? ?? 0,
                    InvoiceDate = x.InvoiceDate as DateTime? ?? DateTime.MinValue,
                    InvoiceNo = Convert.ToString(x.InvoiceNo),
                    InvoiceSeries = Convert.ToString(x.InvoiceSeries),
                    PaymentType = Convert.ToString(x.PaymentType),
                    TransactionDate = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    Remarks = Convert.ToString(x.Remarks),
                    Debit = x.Debit as decimal? ?? 0,
                    Credit = x.Credit as decimal? ?? 0,
                    Id = Convert.ToString(x.ReceiptId)
                };
                //list.Add(customerPayment);
                return customerPayment;
            }).ToList();

            var salesList = list.Where(x => !string.IsNullOrEmpty(x.SalesId)).ToList();
            var customerList = list.Where(x => !string.IsNullOrEmpty(x.CustomerId)).ToList();

            var saleResult = salesList.GroupBy(s => new { s.SalesId, s.InvoiceNo, s.InvoiceSeries, s.InvoiceDate, s.InvoiceAmount })
                                  .Select(async (g) =>
                                             new CustomerPayment
                                             {
                                                 SalesId = g.Key.SalesId,
                                                 CustomerId = null,
                                                 InvoiceAmount = g.Key.InvoiceAmount,
                                                 InvoiceDate = g.Key.InvoiceDate,
                                                 InvoiceNo = g.Key.InvoiceNo,
                                                 InvoiceSeries = g.Key.InvoiceSeries,
                                                 TransactionDate = g.Max(x => x.TransactionDate),
                                                 Debit = g.Sum(x => x.Debit),
                                                 Credit = g.Sum(x => x.Credit),
                                                 CustomerPaymentList = await GetCustomerPaymentList(g.Key.SalesId)
                                             }
                                           );
            var finalSaleResult = (await Task.WhenAll(saleResult)).ToList().Where(x => (x.Debit != x.Credit)).ToList();

            var customerResult = customerList.GroupBy(s => new { s.CustomerId, s.InvoiceNo, s.InvoiceSeries, s.InvoiceDate, s.InvoiceAmount })
                                  .Select(async (g) =>
                                             new CustomerPayment
                                             {
                                                 SalesId = null,
                                                 CustomerId = g.Key.CustomerId,
                                                 InvoiceAmount = g.Key.InvoiceAmount,
                                                 InvoiceDate = g.Key.InvoiceDate,
                                                 InvoiceNo = g.Key.InvoiceNo,
                                                 InvoiceSeries = g.Key.InvoiceSeries,
                                                 TransactionDate = g.Max(x => x.TransactionDate),
                                                 Debit = g.Sum(x => x.Debit),
                                                 Credit = g.Sum(x => x.Credit),
                                                 CustomerPaymentList = await GetCustomerPaymentListusingCustomerID(g.Key.CustomerId)
                                             }
                                           );
            var finalCustomerResult = (await Task.WhenAll(customerResult)).ToList().Where(x => (x.Debit != x.Credit)).ToList();
             var concatObjects =  finalSaleResult.Concat(finalCustomerResult).ToList();
            return concatObjects;
        }
        public async Task<List<CustomerPayment>> GetCustomerPaymentList(string salesid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Select);
            qb.AddColumn(CustomerPaymentTable.CreatedAtColumn, CustomerPaymentTable.DebitColumn, CustomerPaymentTable.PaymentTypeColumn, CustomerPaymentTable.RemarksColumn, CustomerPaymentTable.ChequeNoColumn, CustomerPaymentTable.ChequeDateColumn, CustomerPaymentTable.CardNoColumn, 
                CustomerPaymentTable.CardDateColumn, CustomerPaymentTable.IdColumn);
            qb.ConditionBuilder.And(CustomerPaymentTable.SalesIdColumn, salesid);
            var list = new List<CustomerPayment>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new CustomerPayment
                    {
                        Id = reader[CustomerPaymentTable.IdColumn.ColumnName].ToString(),
                        CreatedAt = reader[CustomerPaymentTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        Debit = reader[CustomerPaymentTable.DebitColumn.ColumnName] as decimal? ?? 0,
                        PaymentType = reader[CustomerPaymentTable.PaymentTypeColumn.ColumnName].ToString(),
                        Remarks = reader[CustomerPaymentTable.RemarksColumn.ColumnName].ToString(),
                        ChequeNo = reader[CustomerPaymentTable.ChequeNoColumn.ColumnName].ToString(),
                        ChequeDate = reader[CustomerPaymentTable.ChequeDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        CardDate = reader[CustomerPaymentTable.CardDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        CardNo = reader[CustomerPaymentTable.CardNoColumn.ColumnName].ToString(),
                        SalesId= salesid
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        public async Task<List<CustomerPayment>> GetCustomerPaymentListusingCustomerID(string custId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Select);
            qb.AddColumn(CustomerPaymentTable.CreatedAtColumn, CustomerPaymentTable.DebitColumn, CustomerPaymentTable.PaymentTypeColumn, CustomerPaymentTable.RemarksColumn, CustomerPaymentTable.ChequeNoColumn, CustomerPaymentTable.ChequeDateColumn, CustomerPaymentTable.CardNoColumn, CustomerPaymentTable.CardDateColumn);
            qb.ConditionBuilder.And(CustomerPaymentTable.CustomerIdColumn, custId);
            var list = new List<CustomerPayment>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new CustomerPayment
                    {
                        CreatedAt = reader[CustomerPaymentTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        Debit = reader[CustomerPaymentTable.DebitColumn.ColumnName] as decimal? ?? 0,
                        PaymentType = reader[CustomerPaymentTable.PaymentTypeColumn.ColumnName].ToString(),
                        Remarks = reader[CustomerPaymentTable.RemarksColumn.ColumnName].ToString(),
                        ChequeNo = reader[CustomerPaymentTable.ChequeNoColumn.ColumnName].ToString(),
                        ChequeDate = reader[CustomerPaymentTable.ChequeDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        CardDate = reader[CustomerPaymentTable.CardDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        CardNo = reader[CustomerPaymentTable.CardNoColumn.ColumnName].ToString()
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public async Task<List<CustomerPayment>> CustomerList(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.NameColumn, SalesTable.MobileColumn);
            qb.SelectBuilder.GroupBy(SalesTable.NameColumn, SalesTable.MobileColumn);
            qb.JoinBuilder.Join(CustomerPaymentTable.Table, CustomerPaymentTable.SalesIdColumn, SalesTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(CustomerPaymentTable.Table, DbColumn.SumColumn("", CustomerPaymentTable.DebitColumn), DbColumn.SumColumn("", CustomerPaymentTable.CreditColumn));
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            // Added Gavaskar 02/02/2017 Start
            var statusIsNull = new CustomCriteria(SalesTable.CancelstatusColumn, CriteriaCondition.IsNull);
            qb.ConditionBuilder.And(statusIsNull);
            // Added Gavaskar 02/02/2017 End
            var list = new List<CustomerPayment>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new CustomerPayment
                    {
                        Sales =
                        {
                              Name = reader[SalesTable.NameColumn.ColumnName].ToString(),
                              Mobile = reader[SalesTable.MobileColumn.ColumnName].ToString(),
                        },
                        CustomerCredit = reader[CustomerPaymentTable.CreditColumn.FullColumnName] as decimal? ?? 0,
                        CustomerDebit = reader[CustomerPaymentTable.DebitColumn.FullColumnName] as decimal? ?? 0
                    };
                    list.Add(item);
                }
            }
            var result = list.Select(x => new CustomerPayment
            {
                Sales =
                {
                     Name = x.Sales.Name,
                     Mobile = x.Sales.Mobile
                },
                CustomerDebit = x.CustomerDebit,
                CustomerCredit = x.CustomerCredit
            }).Where(g => (g.CustomerCredit != g.CustomerDebit)).ToList();
            return result;
        }
        public async Task<Payment> Update(Payment data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PaymentTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(PaymentTable.IdColumn, data.Id);
            qb.AddColumn(PaymentTable.DebitColumn, PaymentTable.PaymentHistoryStatusColumn, PaymentTable.UpdatedAtColumn, PaymentTable.TransactionDateColumn);
            qb.Parameters.Add(PaymentTable.DebitColumn.ColumnName, data.Debit);
            qb.Parameters.Add(PaymentTable.PaymentHistoryStatusColumn.ColumnName, data.PaymentHistoryStatus);
            qb.Parameters.Add(PaymentTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(PaymentTable.TransactionDateColumn.ColumnName, data.TransactionDate);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }
        public async Task<Payment> Delete(Payment data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PaymentTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(PaymentTable.IdColumn, data.Id);
            qb.AddColumn(PaymentTable.DebitColumn, PaymentTable.PaymentHistoryStatusColumn, PaymentTable.UpdatedAtColumn, PaymentTable.TransactionDateColumn);
            qb.Parameters.Add(PaymentTable.DebitColumn.ColumnName, data.Debit);
            qb.Parameters.Add(PaymentTable.PaymentHistoryStatusColumn.ColumnName, data.PaymentHistoryStatus);
            qb.Parameters.Add(PaymentTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(PaymentTable.TransactionDateColumn.ColumnName, data.TransactionDate);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }
    }
}
