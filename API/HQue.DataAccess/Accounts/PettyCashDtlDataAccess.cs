﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Accounts;
using HQue.DataAccess.DbModel;
using DataAccess.QueryBuilder;
using DataAccess.ManageData.Interface;
using Utilities.Enum;
using Utilities.Helpers;
using DataAccess.Criteria;

namespace HQue.DataAccess.Accounts
{
    public class PettyCashDtlDataAccess : BaseDataAccess
    {
        
        public PettyCashDtlDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory)
            : base(sqlQueryExecuter, queryBuilderFactory)
        {
        }

        public override Task<PettyCashDtl> Save<PettyCashDtl>(PettyCashDtl data)
        {
            return Insert(data, PettyCashDtlTable.Table);
        }

        public async Task<List<PettyCashDtl>> ListData(string type, string accountId, string instanceId, DateTime from, DateTime to, string userId, bool isoffline)
        {
            
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashDtlTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(type, from, to, qb);

            qb.AddColumn(PettyCashDtlTable.IdColumn, PettyCashDtlTable.DescriptionColumn, PettyCashDtlTable.AccountIdColumn,
                PettyCashDtlTable.PettyHdrIdColumn, PettyCashDtlTable.PettyItemIdColumn, PettyCashDtlTable.AmountColumn,
                PettyCashDtlTable.DeletedStatusColumn,
                PettyCashDtlTable.TransactionDateColumn, PettyCashDtlTable.CreatedAtColumn);


            qb.ConditionBuilder.And(PettyCashDtlTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(PettyCashDtlTable.AccountIdColumn, accountId);
            //qb.ConditionBuilder.And(PettyCashDtlTable.CreatedByColumn, userId); //Comment by Sarubala to fix accounts issue on 27-11-19
            qb.ConditionBuilder.And(PettyCashDtlTable.OfflineStatusColumn, isoffline);
            qb.ConditionBuilder.And(PettyCashDtlTable.DeletedStatusColumn,false);

            if (string.IsNullOrEmpty(type) || type == "null")
            {
               // qb.ConditionBuilder.And(PettyCashDtlTable.DeletedStatusColumn, false); // added by senthil.S
                var statusCondition = new CustomCriteria(PettyCashDtlTable.PettyHdrIdColumn, CriteriaCondition.IsNull);
                var cc1 = new CriteriaColumn(PettyCashDtlTable.PettyHdrIdColumn, "''", CriteriaEquation.Equal);
                var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);
                qb.ConditionBuilder.And(col1);
            }
           

            qb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, PettyCashDtlTable.UserIdColumn);
            qb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);

            qb.SelectBuilder.SortBy(PettyCashDtlTable.CreatedAtColumn);

            qb.SelectBuilder.MakeDistinct = true;
            var result= await List<PettyCashDtl>(qb);
            return result;
        }

        public async Task<int> Count(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashDtlTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(type, from, to, qb);
            qb.ConditionBuilder.And(PettyCashDtlTable.InstanceIdColumn, instanceId);

            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public async Task<PettyCashDtl> UpdateDtl(PettyCashDtl data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashDtlTable.Table, OperationType.Update);
            qb.AddColumn(PettyCashDtlTable.PettyItemIdColumn, PettyCashDtlTable.DescriptionColumn,
                PettyCashDtlTable.DeletedStatusColumn, PettyCashDtlTable.PettyHdrIdColumn,
                PettyCashDtlTable.TransactionDateColumn, PettyCashDtlTable.AmountColumn);

            qb.ConditionBuilder.And(PettyCashDtlTable.IdColumn);

            qb.Parameters.Add(PettyCashDtlTable.IdColumn.ColumnName, data.Id);

            qb.Parameters.Add(PettyCashDtlTable.PettyItemIdColumn.ColumnName, data.PettyItemId);
            qb.Parameters.Add(PettyCashDtlTable.DescriptionColumn.ColumnName, data.Description);
            qb.Parameters.Add(PettyCashDtlTable.AmountColumn.ColumnName, data.Amount);
            qb.Parameters.Add(PettyCashDtlTable.TransactionDateColumn.ColumnName, data.TransactionDate);
            qb.Parameters.Add(PettyCashDtlTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
            qb.Parameters.Add(PettyCashDtlTable.DeletedStatusColumn.ColumnName, data.DeletedStatus);
            qb.Parameters.Add(PettyCashDtlTable.PettyHdrIdColumn.ColumnName, data.PettyHdrId);
            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }

        public async Task<string> getLatestupdatedDate(string accountId, string instanceId , string userId)
        {
            string query = $@" select max(transactiondate) from PettyCashDtl Where AccountId='{accountId}' and InstanceId = '{instanceId}' and UserId = '{userId}' ";
            var qdresult = QueryBuilderFactory.GetQueryBuilder(query);
           return Convert.ToString(await QueryExecuter.SingleValueAsyc(qdresult));
        }
        private static QueryBuilderBase SqlQueryBuilder(string type, DateTime from, DateTime to, QueryBuilderBase qb)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            if (string.IsNullOrEmpty(type) || type == "null")
            {
                qb.ConditionBuilder.And(PettyCashDtlTable.TransactionDateColumn, CriteriaEquation.Between);
                 qb.Parameters.Add("FilterFromDate", from.ToFormat() + " 00:00:00");
                 qb.Parameters.Add("FilterToDate", to.ToFormat() + " 23:59:59");

               // qb.Parameters.Add("FilterFromDate", from);
               // qb.Parameters.Add("FilterToDate", to);
            }
            
            else
            {
                switch (type.ToUpper())
                {
                    case "ALL":
                        qb.ConditionBuilder.And(PettyCashDtlTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = from.ToFormat(); // + " 00:00:00";
                        filterToDate = to.ToFormat(); //+ " 23:59:59";
                        break;
                    case "TODAY":
                        qb.ConditionBuilder.And(PettyCashDtlTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(PettyCashDtlTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(PettyCashDtlTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
                filterFromDate = filterFromDate + " 00:00:00";
                filterToDate = filterToDate + " 23:59:59";
                qb.Parameters.Add("FilterFromDate", filterFromDate);
                qb.Parameters.Add("FilterToDate", filterToDate);
            }

            return qb;
        }

    }
}
