﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Accounts;
using HQue.DataAccess.DbModel;
using DataAccess.QueryBuilder;
using DataAccess.ManageData.Interface;
using Utilities.Enum;
using Utilities.Helpers;

namespace HQue.DataAccess.Accounts
{
    public class PettyCashDataAccess : BaseDataAccess
    {
        public PettyCashDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory)
            : base(sqlQueryExecuter, queryBuilderFactory)
        {
        }

        public override Task<PettyCash> Save<PettyCash>(PettyCash data)
        {
            return Insert(data, PettyCashTable.Table);
        }

       public Task<List<PettyCash>> ListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(type, from, to, qb);

            qb.AddColumn(PettyCashTable.IdColumn,PettyCashTable.DescriptionColumn, PettyCashTable.AmountColumn,
                PettyCashTable.TransactionDateColumn, PettyCashTable.CreatedAtColumn);

            //qb.ConditionBuilder.And(PettyCashTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(PettyCashTable.AccountIdColumn, accountId);

            qb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, PettyCashTable.UserIdColumn);
            qb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);

          //  qb.SelectBuilder.SortBy(PettyCashTable.CreatedAtColumn + " desc");
            qb.SelectBuilder.SortByDesc(PettyCashTable.CreatedAtColumn);
            qb.SelectBuilder.MakeDistinct = true;
            return List<PettyCash>(qb);
        }

        public async Task<int> Count(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PettyCashTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(type, from, to, qb);
            qb.ConditionBuilder.And(PettyCashTable.InstanceIdColumn, instanceId);

            return Convert.ToInt32(await SingleValueAsyc(qb));
        }


        private static QueryBuilderBase SqlQueryBuilder(string type, DateTime from, DateTime to, QueryBuilderBase qb)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(PettyCashTable.TransactionDateColumn, CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from.ToFormat() + " 00:00:00");
                qb.Parameters.Add("FilterToDate", to.ToFormat() + " 23:59:59");
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(PettyCashTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(PettyCashTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(PettyCashTable.TransactionDateColumn, CriteriaEquation.Between);
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
                filterFromDate = filterFromDate + " 00:00:00";
                filterToDate = filterToDate + " 23:59:59";
                qb.Parameters.Add("FilterFromDate", filterFromDate);
                qb.Parameters.Add("FilterToDate", filterToDate);
            }

            return qb;
        }
    }
}
