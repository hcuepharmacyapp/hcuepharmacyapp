﻿using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Helpers;
using DataAccess.Criteria;
using HQue.Contract.Infrastructure.Setup;
using DataAccess;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.ServiceConnector.Connector;
using HQue.Contract.Infrastructure.Common;


namespace HQue.DataAccess.Settings
{
    public class ToolDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;

        public ToolDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        public override Task<Tools> Save<Tools>(Tools data)
        {
            return Insert(data, ToolTable.Table);
        }

        public async Task<Tools> Update(Tools data)
        {
            return await Update(data, ToolTable.Table);
        }

        public async Task<int> Count(Tools data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Count);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public Task<List<Requirements>> List(Requirements data, int type, string status)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(RequirementTable.Table, OperationType.Select);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            if (type != 0)
            {
                qb.ConditionBuilder.And(RequirementTable.TypeColumn, type);
            }
            if (status != null && status != "undefined")
            {
                if (status == "new")
                {
                    var statusCondition = new CustomCriteria(RequirementTable.StatusColumn, CriteriaCondition.IsNull);
                    var cc1 = new CriteriaColumn(RequirementTable.StatusColumn, "'new'", CriteriaEquation.Equal);
                    var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);

                    qb.ConditionBuilder.And(col1);
                }
                else
                {
                    qb.ConditionBuilder.And(RequirementTable.StatusColumn, status);
                }
            }
            else
            {
                var statusCondition = new CustomCriteria(RequirementTable.StatusColumn, CriteriaCondition.IsNull);
                var cc1 = new CriteriaColumn(RequirementTable.StatusColumn, "'closed'", CriteriaEquation.NotEqual);
                var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);

                qb.ConditionBuilder.And(col1);
            }
            qb.SelectBuilder.SortByDesc(RequirementTable.CreatedAtColumn);
            return List<Requirements>(qb);
        }

        public async Task<int> Count()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(RequirementTable.Table, OperationType.Count);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public Task<List<Tools>> List()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ToolTable.Table, OperationType.Select);
            qb.AddColumn(ToolTable.IdColumn, ToolTable.ToolNameColumn, ToolTable.DescriptionColumn, ToolTable.ImageColumn, ToolTable.StatusColumn);
            return List<Tools>(qb);
        }
        public async Task<Tools> updateStatus(Tools data)
        {
            if (data.Status == 1)
            {
                var query = $@"UPDATE TOOL SET Status=0";
                var qbU = QueryBuilderFactory.GetQueryBuilder(query);
            }
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(ToolTable.Table, OperationType.Update);
            qb.AddColumn(ToolTable.StatusColumn);
            qb.ConditionBuilder.And(ToolTable.IdColumn);

            qb.Parameters.Add(ToolTable.StatusColumn.ColumnName, data.Status);
            qb.Parameters.Add(ToolTable.IdColumn.ColumnName, data.Id);
            qb.Parameters.Add(ToolTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);

            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }

        public async Task<string> GetRequirementNo(string instanceId)
        {
            var query = _configHelper.AppConfig.IsSqlServer
                ? $"SELECT ISNULL(MAX(CONVERT(INT, RequirementNo)),0) + 1 AS RequirementNo FROM Requirement WHERE InstanceId = '{instanceId}'"
                : $"SELECT IFNULL(MAX(CAST(RequirementNo as INT)),0) + 1 AS RequirementNo FROM Requirement WHERE InstanceId = '{instanceId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await QueryExecuter.SingleValueAsyc(qb)).ToString();
        }

        public async Task<List<Requirements>> SaveRequirement(List<Requirements> reqList, string accId, string insId, string userId)
        {
            foreach (Requirements item in reqList)
            {
                item.AccountId = accId;
                item.InstanceId = insId;
                item.CreatedAt = DateTime.UtcNow;
                item.UpdatedAt = DateTime.UtcNow;
                item.CreatedBy = userId;
                item.UpdatedBy = userId;
                await Insert(item, RequirementTable.Table);
            }
            return reqList;
        }

        public async Task<List<Tools>> NegativeStockList()
        {
            var query = $@"SELECT A.Name AS AccountName,I.Name AS InstanceName,P.Name AS ProductName,PS.Stock AS Stock FROM ProductStock PS WITH(NOLOCK) INNER JOIN Product P WITH(NOLOCK) ON P.Id = PS.ProductId inner join Instance I with(nolock) on I.id = PS.InstanceId inner join Account A with(nolock) on A.id = I.AccountId WHERE PS.Stock<0 order BY A.Name,I.Name,P.Name";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await List<Tools>(qb)).ToList();
        }

        public async Task<Requirements> updateStatus(Requirements data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.SolvedDate = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(RequirementTable.Table, OperationType.Update);
            qb.AddColumn(RequirementTable.StatusColumn, RequirementTable.RemarksColumn, RequirementTable.UpdatedAtColumn, RequirementTable.SolvedByColumn, RequirementTable.SolvedDateColumn);
            qb.ConditionBuilder.And(RequirementTable.IdColumn);

            qb.Parameters.Add(RequirementTable.StatusColumn.ColumnName, data.Status);
            qb.Parameters.Add(RequirementTable.RemarksColumn.ColumnName, data.Remarks);
            qb.Parameters.Add(RequirementTable.SolvedByColumn.ColumnName, data.SolvedBy);
            qb.Parameters.Add(RequirementTable.SolvedDateColumn.ColumnName, data.SolvedDate);
            qb.Parameters.Add(RequirementTable.IdColumn.ColumnName, data.Id);
            qb.Parameters.Add(RequirementTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);

            await QueryExecuter.NonQueryAsyc(qb);

            return data;

        }

        public async Task<List<Tools>> getBranchList(string accountId, string branchId, string name)
        {
            if (name == "" || name == null)
            {
                var query = $@"Select A.Name as AccountName,I.Name as InstanceName,I.Phone, isnull(OfflineVersionNo,NULL) as OfflineVersionNo, isnull(IsOfflineInstalled,0) as IsOfflineInstalled,isnull(I.OfflineStatus,0) as OfflineStatus, (select isnull(UserId,'') from HQueUser where id = I.UpdatedBy) as UpdatedBy,I.Id as InstanceId,I.UpdatedAt,A.Id AccountId  from Instance I with(nolock) inner join Account A with(nolock) on A.id = I.AccountId";
                if (accountId != null && accountId != "")
                    query = query + " and A.Id='" + accountId + "'";
                if (branchId != null && branchId != "")
                    query = query + " and I.Id='" + branchId + "'";
                query = query + " order by I.UpdatedAt desc";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                return (await List<Tools>(qb)).ToList();
            }
            else
            {
                var query = $@"Select A.Name as AccountName,I.Name as InstanceName,I.Phone,I.Area, isnull(OfflineVersionNo,NULL) as OfflineVersionNo, isnull(IsOfflineInstalled,0) as IsOfflineInstalled,isnull(I.OfflineStatus,0) as OfflineStatus, (select isnull(UserId,'') from HQueUser where id = I.UpdatedBy) as UpdatedBy,I.Id as InstanceId,I.UpdatedAt,A.Id AccountId  from Instance I with(nolock) inner join Account A with(nolock) on A.id = I.AccountId  where A.name ='{name}' ";
                //if (accountId != null && accountId != "")
                //    query = query + " and A.Id='" + accountId + "'";
                if (branchId != null && branchId != "")
                    query = query + " and I.Id='" + branchId + "'";
                query = query + " order by I.UpdatedAt desc";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                return (await List<Tools>(qb)).ToList();
            }

        }
        //added by nandhini
        public async Task<List<Tools>> salesReport(string from)
        {

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("FromDate", from);
            var result = await sqldb.ExecuteProcedureAsync<Tools>("Usp_Sale_Live", parms);

            return result.ToList();

        }
        public async Task<List<Tools>> offlineReport()
        {

            var result = await sqldb.ExecuteQueryAsync<Tools>("usp_GetOfflineVersion");
            return result.ToList();

        }
        //end
        public async Task<List<Tools>> updateList(string accountId, string purchaseLowestPrice, string branchId)
        {

            var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Insert);
            qb.AddColumn(SettingsTable.Pur_SortByLowestPriceColumn, SettingsTable.AccountIdColumn, SettingsTable.IdColumn, SettingsTable.InstanceIdColumn);
            qb.ConditionBuilder.And(SettingsTable.AccountIdColumn);

            qb.Parameters.Add(SettingsTable.Pur_SortByLowestPriceColumn.ColumnName, purchaseLowestPrice);
            qb.Parameters.Add(SettingsTable.AccountIdColumn.ColumnName, accountId);
            qb.Parameters.Add(SettingsTable.InstanceIdColumn.ColumnName, branchId);
            qb.Parameters.Add(SettingsTable.IdColumn.ColumnName, "");

            return (await List<Tools>(qb)).ToList();

        }


        public async Task<Instance> updateOfflineStatus(Instance data)
        {
            data.UpdatedAt = CustomDateTime.IST;

            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Update);
            qb.AddColumn(InstanceTable.OfflineStatusColumn, InstanceTable.UpdatedByColumn, InstanceTable.UpdatedAtColumn);
            qb.ConditionBuilder.And(InstanceTable.IdColumn);

            qb.Parameters.Add(InstanceTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
            qb.Parameters.Add(InstanceTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.Parameters.Add(InstanceTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(InstanceTable.IdColumn.ColumnName, data.Id);

            await QueryExecuter.NonQueryAsyc2(qb);
            return data;
        }

        public async Task<Instance> updateOfflineVersionNo(Instance data)
        {
            data.UpdatedAt = CustomDateTime.IST;

            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Update);
            qb.AddColumn(InstanceTable.OfflineVersionNoColumn, InstanceTable.UpdatedByColumn, InstanceTable.UpdatedAtColumn);
            qb.ConditionBuilder.And(InstanceTable.IdColumn);

            qb.Parameters.Add(InstanceTable.OfflineVersionNoColumn.ColumnName, data.OfflineVersionNo);
            qb.Parameters.Add(InstanceTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.Parameters.Add(InstanceTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(InstanceTable.IdColumn.ColumnName, data.Id);

            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task<List<Account>> getAccountList()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qb.AddColumn(AccountTable.IdColumn, AccountTable.NameColumn);
            return await List<Account>(qb);
        }

        public async Task<List<Instance>> getBranchListData()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.AddColumn(InstanceTable.IdColumn, InstanceTable.NameColumn, InstanceTable.PhoneColumn, InstanceTable.AccountIdColumn, InstanceTable.AreaColumn, InstanceTable.OfflineVersionNoColumn);
            return await List<Instance>(qb);
        }

        //public async Task<List<HQueUser>> getUserIdList()
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
        //    qb.AddColumn(HQueUserTable.IdColumn, HQueUserTable.InstanceIdColumn, HQueUserTable.UserIdColumn, HQueUserTable.AccountIdColumn);
        //    return await List<HQueUser>(qb);
        //}

        public async Task<List<Instance>> UpdateHcueProductMasterSettings(List<Instance> model)
        {

            var instanceList = await GetInstanceSettingsList();

            foreach (var instance in model)
            {
                Setting settings = new Setting();

                settings.AccountId = instance.AccountId;
                settings.InstanceId = instance.InstanceId;
                settings.Enable_GlobalProdut = instance.IsChecked;
                settings.CreatedBy = model[0].CreatedBy;
                settings.UpdatedBy = model[0].CreatedBy;

                if (instanceList.Count() > 0)
                {
                    bool IsExistInstanceId = instanceList.Exists(x => x.InstanceId == instance.InstanceId);
                    if (IsExistInstanceId)
                    {
                        var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Update);
                        qb.AddColumn(SettingsTable.Enable_GlobalProdutColumn, SettingsTable.UpdatedAtColumn, SettingsTable.UpdatedByColumn);
                        qb.Parameters.Add(SettingsTable.Enable_GlobalProdutColumn.ColumnName, instance.IsChecked);
                        qb.Parameters.Add(SettingsTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                        qb.Parameters.Add(SettingsTable.UpdatedByColumn.ColumnName, settings.UpdatedBy);
                        qb.ConditionBuilder.And(SettingsTable.AccountIdColumn, settings.AccountId);
                        qb.ConditionBuilder.And(SettingsTable.InstanceIdColumn, settings.InstanceId);
                        await QueryExecuter.NonQueryAsyc(qb);
                        //await Update(settings, SettingsTable.Table);                   
                    }

                    else
                        await Insert(settings, SettingsTable.Table);
                }
                else
                {
                    await Insert(settings, SettingsTable.Table);
                }
            }
            return model;
        }
        //added by nandhini for PurchasePriceSettings in tools page
        public async Task<List<Instance>> updateHcuePurchasePriceSettings(List<Instance> model)
        {

            var instanceList = await GetInstanceSettingsList1();

            foreach (var instance in model)
            {
                Setting settings = new Setting();

                settings.AccountId = instance.AccountId;
                settings.InstanceId = instance.InstanceId;
                settings.Pur_SortByLowestPrice = instance.IsChecked;
                settings.CreatedBy = model[0].CreatedBy;
                settings.UpdatedBy = model[0].CreatedBy;

                if (instanceList.Count() > 0)
                {
                    bool IsExistInstanceId = instanceList.Exists(x => x.InstanceId == instance.InstanceId);
                    if (IsExistInstanceId)
                    {
                        var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Update);
                        qb.AddColumn(SettingsTable.Pur_SortByLowestPriceColumn, SettingsTable.UpdatedAtColumn, SettingsTable.UpdatedByColumn);
                        qb.Parameters.Add(SettingsTable.Pur_SortByLowestPriceColumn.ColumnName, instance.IsChecked);
                        qb.Parameters.Add(SettingsTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                        qb.Parameters.Add(SettingsTable.UpdatedByColumn.ColumnName, settings.UpdatedBy);
                        qb.ConditionBuilder.And(SettingsTable.AccountIdColumn, settings.AccountId);
                        qb.ConditionBuilder.And(SettingsTable.InstanceIdColumn, settings.InstanceId);
                        await QueryExecuter.NonQueryAsyc(qb);

                    }

                    else
                        await Insert(settings, SettingsTable.Table);
                }
                else
                {
                    await Insert(settings, SettingsTable.Table);
                }
            }
            return model;
        }
        public async Task<List<Instance>> GetInstanceSettingsList1(string accountId, string branchId)
        {
            var query = $@"Select A.Name as AccountName,I.Name,I.Area,I.Id as InstanceId,A.Id AccountId,S.Enable_GlobalProdut as IsChecked from Instance I with(nolock) 
inner join Account A with(nolock) on A.id = I.AccountId";
            if (accountId != null && accountId != "")
                query = query + " and A.Id='" + accountId + "'";
            if (branchId != null && branchId != "")
                query = query + " and I.Id='" + branchId + "'";
            query = query + " Left Join Settings S With(Nolock) on S.InstanceId=I.id group by A.Name,I.Name,I.Area,I.Id,A.Id,S.Pur_SortByLowestPrice";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await List<Instance>(qb)).ToList();
        }

        public async Task<List<Setting>> GetInstanceSettingsList1()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Select);
            qb.AddColumn(SettingsTable.IdColumn, SettingsTable.AccountIdColumn, SettingsTable.InstanceIdColumn, SettingsTable.Pur_SortByLowestPriceColumn);
            return await List<Setting>(qb);
        }
        //end
        //addded by nandhini for csv bill setting 27.11.17 start
        public async Task<List<Instance>> updateSaleBillcsv(List<Instance> model)
        {

            var instanceList = await GetInstanceSettingsForSaleBillcsv();

            foreach (var instance in model)
            {
                Setting settings = new Setting();

                settings.AccountId = instance.AccountId;
                settings.InstanceId = instance.InstanceId;
                settings.SaleBillToCSV = instance.IsChecked;
                settings.CreatedBy = model[0].CreatedBy;
                settings.UpdatedBy = model[0].CreatedBy;

                if (instanceList.Count() > 0)
                {
                    bool IsExistInstanceId = instanceList.Exists(x => x.InstanceId == instance.InstanceId);
                    if (IsExistInstanceId)
                    {
                        var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Update);
                        qb.AddColumn(SettingsTable.SaleBillToCSVColumn, SettingsTable.UpdatedAtColumn, SettingsTable.UpdatedByColumn);
                        qb.Parameters.Add(SettingsTable.SaleBillToCSVColumn.ColumnName, instance.IsChecked);
                        qb.Parameters.Add(SettingsTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                        qb.Parameters.Add(SettingsTable.UpdatedByColumn.ColumnName, settings.UpdatedBy);
                        qb.ConditionBuilder.And(SettingsTable.AccountIdColumn, settings.AccountId);
                        qb.ConditionBuilder.And(SettingsTable.InstanceIdColumn, settings.InstanceId);
                        await QueryExecuter.NonQueryAsyc(qb);
                        //await Update(settings, SettingsTable.Table);                   
                    }

                    else
                        await Insert(settings, SettingsTable.Table);
                }
                else
                {
                    await Insert(settings, SettingsTable.Table);
                }
            }
            return model;
        }
        public async Task<List<Instance>> GetInstanceSettingsForSaleBillcsv(string accountId, string branchId)
        {
            var query = $@"Select A.Name as AccountName,I.Name,I.Area,I.Id as InstanceId,A.Id AccountId,S.SaleBillToCSV as IsChecked from Instance I with(nolock) 
inner join Account A with(nolock) on A.id = I.AccountId";
            if (accountId != null && accountId != "")
                query = query + " and A.Id='" + accountId + "'";
            if (branchId != null && branchId != "")
                query = query + " and I.Id='" + branchId + "'";
            query = query + " Left Join Settings S With(Nolock) on S.InstanceId=I.id group by A.Name,I.Name,I.Area,I.Id,A.Id,S.SaleBillToCSV";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await List<Instance>(qb)).ToList();
        }
        public async Task<List<Setting>> GetInstanceSettingsForSaleBillcsv()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Select);
            qb.AddColumn(SettingsTable.IdColumn, SettingsTable.AccountIdColumn, SettingsTable.InstanceIdColumn, SettingsTable.SaleBillToCSVColumn);
            return await List<Setting>(qb);
        }

        //Added by nandhini on 29-11-17
        public async Task<Setting> getexportToCsvSetting(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SettingsTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(SettingsTable.AccountIdColumn, AccountId);
            var result = await List<Setting>(qb);
            var settings = new Setting();
            if (result != null && result.Count > 0)
            {
                settings = result.FirstOrDefault();
            }

            settings.SaleBillToCSV = settings.SaleBillToCSV == null ? false : settings.SaleBillToCSV;

            return settings;
        }

        public async Task<int> getSaleBillcsv(String insId)
        {
            int setting = 0;
            var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SettingsTable.InstanceIdColumn);
            qb.Parameters.Add(SettingsTable.InstanceIdColumn.ColumnName, insId);
            var result = await List<Setting>(qb);
            if (result != null && result.Count > 0)
            {
                if (result.FirstOrDefault().SaleBillToCSV != null)
                    setting = Convert.ToInt32(result.FirstOrDefault().SaleBillToCSV);
            }
            return setting;
        }

        //addded by nandhini for csv bill setting 27.11.17 end
        public async Task<int> getSalesPriceSetting(String insId)
        {
            int setting = 0;
            var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SettingsTable.InstanceIdColumn);
            qb.Parameters.Add(SettingsTable.InstanceIdColumn.ColumnName, insId);
            var result = await List<Setting>(qb);
            if (result != null && result.Count > 0)
            {
                if (result.FirstOrDefault().SalesPriceSetting != null)
                    setting = Convert.ToInt32(result.FirstOrDefault().SalesPriceSetting);
            }
            return setting;
        }

        public async Task<List<Instance>> updateSalesPriceSettings(List<Instance> model, String SalesPrice)
        {

            var instanceList = await GetInstanceSettingsList2();

            foreach (var instance in model)
            {
                Setting settings = new Setting();

                settings.AccountId = instance.AccountId;
                settings.InstanceId = instance.InstanceId;
                settings.SalesPriceSetting = Convert.ToInt32(SalesPrice);
                settings.CreatedBy = model[0].CreatedBy;
                settings.UpdatedBy = model[0].CreatedBy;

                if (instanceList.Count() > 0)
                {
                    bool IsExistInstanceId = instanceList.Exists(x => x.InstanceId == instance.InstanceId);
                    if (IsExistInstanceId)
                    {
                        var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Update);
                        qb.AddColumn(SettingsTable.SalesPriceSettingColumn, SettingsTable.UpdatedAtColumn, SettingsTable.UpdatedByColumn);
                        qb.Parameters.Add(SettingsTable.SalesPriceSettingColumn.ColumnName, SalesPrice);
                        qb.Parameters.Add(SettingsTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                        qb.Parameters.Add(SettingsTable.UpdatedByColumn.ColumnName, settings.UpdatedBy);
                        qb.ConditionBuilder.And(SettingsTable.AccountIdColumn, settings.AccountId);
                        qb.ConditionBuilder.And(SettingsTable.InstanceIdColumn, settings.InstanceId);
                        await QueryExecuter.NonQueryAsyc(qb);

                    }

                    else
                        await Insert(settings, SettingsTable.Table);
                }
                else
                {
                    await Insert(settings, SettingsTable.Table);
                }
            }
            return model;
        }
        public async Task<List<Instance>> GetInstanceSettingsList2(string accountId, string branchId)
        {
            var query = $@"Select A.Name as AccountName,I.Name,I.Area,I.Id as InstanceId,A.Id AccountId,S.SalesPriceSetting from Instance I with(nolock) 
inner join Account A with(nolock) on A.id = I.AccountId";
            if (accountId != null && accountId != "")
                query = query + " and A.Id='" + accountId + "'";
            if (branchId != null && branchId != "")
                query = query + " and I.Id='" + branchId + "'";
            query = query + " Left Join Settings S With(Nolock) on S.InstanceId=I.id group by A.Name,I.Name,I.Area,I.Id,A.Id,S.SalesPriceSetting";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await List<Instance>(qb)).ToList();
        }
        public async Task<List<Setting>> GetInstanceSettingsList2()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Select);
            qb.AddColumn(SettingsTable.IdColumn, SettingsTable.AccountIdColumn, SettingsTable.InstanceIdColumn, SettingsTable.SalesPriceSettingColumn);
            return await List<Setting>(qb);
        }

        // By San 25-10-2017
        public async Task<List<Instance>> GetInstanceSettingsList(string accountId, string branchId)
        {
            var query = $@"Select A.Name as AccountName,I.Name,I.Area,I.Id as InstanceId,A.Id AccountId,S.Enable_GlobalProdut as IsChecked from Instance I with(nolock) 
inner join Account A with(nolock) on A.id = I.AccountId";
            if (accountId != null && accountId != "")
                query = query + " and A.Id='" + accountId + "'";
            if (branchId != null && branchId != "")
                query = query + " and I.Id='" + branchId + "'";
            query = query + " Left Join Settings S With(Nolock) on S.InstanceId=I.id group by A.Name,I.Name,I.Area,I.Id,A.Id,S.Enable_GlobalProdut";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await List<Instance>(qb)).ToList();
        }
        public async Task<List<Setting>> GetInstanceSettingsList()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Select);
            qb.AddColumn(SettingsTable.IdColumn, SettingsTable.AccountIdColumn, SettingsTable.InstanceIdColumn, SettingsTable.Enable_GlobalProdutColumn);
            return await List<Setting>(qb);
        }

        //Added by Sarubala on 25-11-17 - start
        public async Task<Instance> getSmsCount(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, instanceId);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, accountId);
            var result = await List<Instance>(qb);

            if (result != null && result.Count > 0)
            {
                result.FirstOrDefault().TotalSmsCount = result.FirstOrDefault().TotalSmsCount != null ? result.FirstOrDefault().TotalSmsCount : 0;
                result.FirstOrDefault().SentSmsCount = result.FirstOrDefault().SentSmsCount != null ? result.FirstOrDefault().SentSmsCount : 0;
            }

            return result.FirstOrDefault();
        }

        public async Task<List<SmsConfiguration>> GetSmsConfiguration(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SmsConfigurationTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SmsConfigurationTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(SmsConfigurationTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SmsConfigurationTable.IsActiveColumn, 1);
            qb.SelectBuilder.SortBy(SmsConfigurationTable.CreatedAtColumn);
            var result = await List<SmsConfiguration>(qb);

            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    item.SmsCountBeforeEdit = item.SmsCount;
                }
            }
            if (result == null)
            {
                result = new List<SmsConfiguration>();
            }

            return result;
        }

        //Added by Sarubala on 25-11-17 - end

        //Added by Sarubala on 27-11-17
        public async Task<SmsConfiguration> SaveSmsConfiguration(SmsConfiguration data)
        {
            var Instance1 = getSmsCount(data.AccountId, data.InstanceId).Result;
            if (data.IsNew)
            {
                Instance1.TotalSmsCount = Instance1.TotalSmsCount + data.SmsCount;
            }
            else
            {
                Instance1.TotalSmsCount = Instance1.TotalSmsCount + data.SmsCount - data.SmsCountBeforeEdit;
            }
            await UpdateSmsCount(Instance1);

            if (data.IsNew)
            {
                data.Date = DateTime.Today;
                data.IsActive = true;
                return await Insert(data, SmsConfigurationTable.Table);
            }
            else
            {
                return await Update(data, SmsConfigurationTable.Table);
            }
        }

        public async Task<Instance> UpdateSmsCount(Instance data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Update);
            qb.AddColumn(InstanceTable.TotalSmsCountColumn, InstanceTable.UpdatedAtColumn, InstanceTable.UpdatedByColumn);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, data.Id);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, data.AccountId);
            qb.Parameters.Add(InstanceTable.TotalSmsCountColumn.ColumnName, data.TotalSmsCount);
            qb.Parameters.Add(InstanceTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(InstanceTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            var smsdetail = await QueryExecuter.NonQueryAsyc(qb);
            WriteSyncQueueInsert(data, InstanceTable.Table, "I", data.Id, true);
            return data;


        }
        // Added by Gavaskar Stock Ledger Edit 25-12-2017 Start 
        public async Task<List<StockLedgerUpdateHistory>> StockLedgerEditList(string AccountId, string InstanceId, string ProductName)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            Dictionary<string, object> parmsProductId = new Dictionary<string, object>();
            parms.Add("InstanceId", InstanceId);
            parms.Add("AccountId", AccountId);
            parms.Add("ProductName", ProductName);
            var queryresult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_StockLedgerEdit", parms);
            var result = queryresult.Select(x =>
            {
                var mp = new StockLedgerUpdateHistory
                {
                    ProductName = x.Name,
                    ProductId = x.productId,
                    Stockid = x.stockid,
                    ImportQty = x.ImportQty as decimal? ?? 0,
                    CurrentStock = x.CurrentStock as decimal? ?? 0,
                    OpeningStock = x.OpeningStock as decimal? ?? 0,
                    AdjustedStockIn = x.AdjustedStockIn as decimal? ?? 0,
                    PurchaseQty = x.PurchaseQty as decimal? ?? 0,
                    SaleReturnQty = x.SaleReturnQty as decimal? ?? 0,
                    DcQty = x.DcQty as decimal? ?? 0,
                    TempQty = x.TempQty as decimal? ?? 0,
                    TransferInAccQty = x.TransferInAccQty as decimal? ?? 0,
                    TransferInNotAccQty = x.TransferInNotAccQty as decimal? ?? 0,
                    AdjustedStockOut = x.AdjustedStockOut as decimal? ?? 0,
                    SaleQty = x.SaleQty as decimal? ?? 0,
                    PurchaseDelQty = x.PurchaseDelQty as decimal? ?? 0,
                    TransferOutQty = x.TransferOutQty as decimal? ?? 0,
                    ConsumptionQty = x.ConsumptionQty as decimal? ?? 0,
                    ReturnQty = x.ReturnQty as decimal? ?? 0,
                    FinalIn = x.FinalIn as decimal? ?? 0,
                    FinalOut = x.FinalOut as decimal? ?? 0,
                    DiffQty = x.DiffQty as decimal? ?? 0,

                };
                return mp;
            });

            var StockLedgerProductId = string.Join(",", result.Select(x => x.ProductId).ToList());
            parmsProductId.Add("AccountId", AccountId);
            parmsProductId.Add("InstanceId", InstanceId);
            parmsProductId.Add("ProductId", StockLedgerProductId);
            var resultStockProductId = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_StockLedgerEditItem", parmsProductId);
            var stockLedgerEditTools = resultStockProductId.Select(x =>
            {
                var stockLedgerEditToolsItem = new StockLedgerUpdateHistoryItem
                {
                    ProductName = x.ProductName,
                    Stockid = x.stockid,
                    BatchNo = x.BatchNo,
                    ProductId = x.productId,
                    ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                    CurrentStock = x.Stock as decimal? ?? 0,
                    ActualStock = x.ActualStock as decimal? ?? 0,
                };
                return stockLedgerEditToolsItem;
            });

            var queryresultDiffQty = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_StockLedgerEditDiffQtyList", parmsProductId);
            var resultDiffQty = queryresultDiffQty.Select(x =>
            {
                var diffQty = new StockLedgerUpdateHistory
                {
                    ProductName = x.Name,
                    ProductId = x.productId,
                    Stockid = x.stockid,
                    ImportQty = x.ImportQty as decimal? ?? 0,
                    CurrentStock = x.CurrentStock as decimal? ?? 0,
                    OpeningStock = x.OpeningStock as decimal? ?? 0,
                    AdjustedStockIn = x.AdjustedStockIn as decimal? ?? 0,
                    PurchaseQty = x.PurchaseQty as decimal? ?? 0,
                    SaleReturnQty = x.SaleReturnQty as decimal? ?? 0,
                    DcQty = x.DcQty as decimal? ?? 0,
                    TempQty = x.TempQty as decimal? ?? 0,
                    TransferInAccQty = x.TransferInAccQty as decimal? ?? 0,
                    TransferInNotAccQty = x.TransferInNotAccQty as decimal? ?? 0,
                    AdjustedStockOut = x.AdjustedStockOut as decimal? ?? 0,
                    SaleQty = x.SaleQty as decimal? ?? 0,
                    PurchaseDelQty = x.PurchaseDelQty as decimal? ?? 0,
                    TransferOutQty = x.TransferOutQty as decimal? ?? 0,
                    ConsumptionQty = x.ConsumptionQty as decimal? ?? 0,
                    ReturnQty = x.ReturnQty as decimal? ?? 0,
                    FinalIn = x.FinalIn as decimal? ?? 0,
                    FinalOut = x.FinalOut as decimal? ?? 0,
                    DiffQty = x.DiffQty as decimal? ?? 0,

                };
                return diffQty;
            });

            var StockLedgerList = result.Select(x =>
            {
                x.StockLedgerUpdateHistoryItem = stockLedgerEditTools.Where(y => y.ProductId == x.ProductId).Select(z => z).ToList();
                x.StockLedgerUpdateHistoryDiffQtyItem = resultDiffQty.Where(y => y.ProductId == x.ProductId).Select(z => z).ToList();
                return x;
            });

            return StockLedgerList.ToList();

        }

        public Task<StockLedgerUpdateHistory> SaveStockLedgerUpdateHistory(StockLedgerUpdateHistory data)
        {
            return Insert(data, StockLedgerUpdateHistoryTable.Table);
        }

        public async Task StockLedgerUpdateHistoryItem(IEnumerable<StockLedgerUpdateHistoryItem> itemList, bool internet)
        {
            var InstanceId = "";
            var AccountId = "";
            foreach (var stockLedgerUpdateHistoryItem in itemList)
            {
                if (stockLedgerUpdateHistoryItem.ActualStock >= 0)
                {
                    stockLedgerUpdateHistoryItem.TransactionType = "";
                }
                var AdminDetails = getAdminDetails(stockLedgerUpdateHistoryItem.AccountId).Result;
                var productStockUpdate = getProductStockDetails(stockLedgerUpdateHistoryItem.AccountId, stockLedgerUpdateHistoryItem.InstanceId, stockLedgerUpdateHistoryItem.Stockid).Result;
                var createdAt = productStockUpdate.CreatedAt;
                if (stockLedgerUpdateHistoryItem.ActualStock < 0)
                {
                    if (stockLedgerUpdateHistoryItem.TransactionType == "OpeningStock")
                    {
                        string tempInvoice = GetNewStockInvoiceNo(stockLedgerUpdateHistoryItem.InstanceId).Result;
                        productStockUpdate.CreatedBy = AdminDetails.Id;
                        productStockUpdate.UpdatedBy = AdminDetails.Id;
                        productStockUpdate.NewStockInvoiceNo = tempInvoice;
                        productStockUpdate.ProductId = stockLedgerUpdateHistoryItem.ProductId;
                        productStockUpdate.NewOpenedStock = true;
                        productStockUpdate.Stock = 0;
                        productStockUpdate.NewStockQty = -(stockLedgerUpdateHistoryItem.ActualStock);
                        productStockUpdate.CreatedAt = createdAt;
                        productStockUpdate.UpdatedAt = createdAt;

                        if (internet == true)
                        {
                            var openingStock = await InsertDataCorrection(productStockUpdate, ProductStockTable.Table);
                            stockLedgerUpdateHistoryItem.ProductStockId = openingStock.Id;
                            var openstock = new RestConnector();
                            await openstock.PostAsyc<Either<ProductStock>>(_configHelper.InternalApi.InsertDataCorrectionOpeningStocksUrl, openingStock);
                        }
                        else
                        {
                            var openingStock = await Insert(productStockUpdate, ProductStockTable.Table);
                            stockLedgerUpdateHistoryItem.ProductStockId = openingStock.Id;
                        }
                        await Insert(stockLedgerUpdateHistoryItem, StockLedgerUpdateHistoryItemTable.Table);

                    }
                    else if (stockLedgerUpdateHistoryItem.TransactionType == "StockAdjustment")
                    {
                        StockAdjustment stockAdjustment = new StockAdjustment();
                        stockAdjustment.CreatedBy = AdminDetails.Id;
                        stockAdjustment.UpdatedBy = AdminDetails.Id;
                        stockAdjustment.AdjustedBy = AdminDetails.Id;
                        stockAdjustment.ProductStockId = stockLedgerUpdateHistoryItem.Stockid;
                        stockAdjustment.AccountId = stockLedgerUpdateHistoryItem.AccountId;
                        stockAdjustment.InstanceId = stockLedgerUpdateHistoryItem.InstanceId;
                        stockAdjustment.CreatedAt = createdAt;
                        stockAdjustment.UpdatedAt = createdAt;
                        stockAdjustment.AdjustedStock = -(stockLedgerUpdateHistoryItem.ActualStock);
                        if (internet == true)
                        {
                            var stockAdjustments = await InsertDataCorrection(stockAdjustment, StockAdjustmentTable.Table);
                            stockLedgerUpdateHistoryItem.ProductStockId = stockAdjustments.Id;
                            var stockAdjust = new RestConnector();
                            await stockAdjust.PostAsyc<Either<ProductStock>>(_configHelper.InternalApi.InsertDataCorrectionStockAdjustmentsUrl, stockAdjustments);
                        }
                        else
                        {
                            var stockAdjustments = await Insert(stockAdjustment, StockAdjustmentTable.Table);
                            stockLedgerUpdateHistoryItem.ProductStockId = stockAdjustments.Id;
                        }
                        await Insert(stockLedgerUpdateHistoryItem, StockLedgerUpdateHistoryItemTable.Table);
                    }
                    else if (stockLedgerUpdateHistoryItem.TransactionType == "TempStock")
                    {
                        TempVendorPurchaseItem tempVendorPurchaseItem = new TempVendorPurchaseItem();
                        tempVendorPurchaseItem.PackageSize = productStockUpdate.PackageSize;
                        tempVendorPurchaseItem.PackagePurchasePrice = productStockUpdate.PackagePurchasePrice;
                        tempVendorPurchaseItem.PurchasePrice = productStockUpdate.PurchasePrice;
                        tempVendorPurchaseItem.PackageQty = -(stockLedgerUpdateHistoryItem.ActualStock) / (tempVendorPurchaseItem.PackageSize);
                        tempVendorPurchaseItem.PackageSellingPrice = productStockUpdate.SellingPrice;
                        tempVendorPurchaseItem.PackageMRP = productStockUpdate.MRP;
                        tempVendorPurchaseItem.isActive = true;
                        tempVendorPurchaseItem.FreeQty = 0;
                        tempVendorPurchaseItem.Discount = 0;
                        tempVendorPurchaseItem.AccountId = stockLedgerUpdateHistoryItem.AccountId;
                        tempVendorPurchaseItem.InstanceId = stockLedgerUpdateHistoryItem.InstanceId;
                        tempVendorPurchaseItem.ProductStockId = stockLedgerUpdateHistoryItem.Stockid;
                        tempVendorPurchaseItem.CreatedAt = createdAt;
                        tempVendorPurchaseItem.UpdatedAt = createdAt;
                        tempVendorPurchaseItem.CreatedBy = AdminDetails.Id;
                        tempVendorPurchaseItem.UpdatedBy = AdminDetails.Id;
                        tempVendorPurchaseItem.Quantity = (tempVendorPurchaseItem.PackageSize * tempVendorPurchaseItem.PackageQty);
                        if (internet == true)
                        {
                            var tempStock = await InsertDataCorrection(tempVendorPurchaseItem, TempVendorPurchaseItemTable.Table);
                            stockLedgerUpdateHistoryItem.ProductStockId = tempStock.Id;
                            var tempStocks = new RestConnector();
                            await tempStocks.PostAsyc<Either<ProductStock>>(_configHelper.InternalApi.InsertDataCorrectionTempStocksUrl, tempStock);
                        }
                        else
                        {
                            var tempStock = await InsertDataCorrection(tempVendorPurchaseItem, TempVendorPurchaseItemTable.Table);
                            stockLedgerUpdateHistoryItem.ProductStockId = tempStock.Id;
                        }
                        await Insert(stockLedgerUpdateHistoryItem, StockLedgerUpdateHistoryItemTable.Table);

                    }
                    decimal? stock = 0;
                    //if (stockLedgerUpdateHistoryItem.CurrentStock - (-stockLedgerUpdateHistoryItem.ActualStock) < 0)
                    //{
                    //    stock = 0;
                    //}
                    //else
                    //{
                    //    stock = stockLedgerUpdateHistoryItem.CurrentStock - (-stockLedgerUpdateHistoryItem.ActualStock);
                    //}

                    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                    qb.ConditionBuilder.And(ProductStockTable.IdColumn, stockLedgerUpdateHistoryItem.Stockid);
                    qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, stockLedgerUpdateHistoryItem.InstanceId);
                    qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, stockLedgerUpdateHistoryItem.AccountId);
                    qb.AddColumn(ProductStockTable.StockColumn, ProductStockTable.UpdatedByColumn);
                    //qb.AddColumn(ProductStockTable.UpdatedAtColumn, ProductStockTable.StockColumn, ProductStockTable.UpdatedByColumn);
                    qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, stock);
                    qb.Parameters.Add(ProductStockTable.UpdatedByColumn.ColumnName, AdminDetails.Id);
                    await QueryExecuter.NonQueryAsyc2(qb);

                    stockLedgerUpdateHistoryItem.Id = stockLedgerUpdateHistoryItem.Stockid;
                    stockLedgerUpdateHistoryItem.ActualStock = stock;
                    if (internet == true)
                    {
                        var sc = new RestConnector();
                        await sc.PostAsyc<Either<ProductStock>>(_configHelper.InternalApi.GetDataCorrectionProductStocksUrl, stockLedgerUpdateHistoryItem);
                    }
                }
                else
                {
                    stockLedgerUpdateHistoryItem.ProductStockId = stockLedgerUpdateHistoryItem.Stockid;
                    await Insert(stockLedgerUpdateHistoryItem, StockLedgerUpdateHistoryItemTable.Table);
                    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                    qb.ConditionBuilder.And(ProductStockTable.IdColumn, stockLedgerUpdateHistoryItem.Stockid);
                    qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, stockLedgerUpdateHistoryItem.InstanceId);
                    qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, stockLedgerUpdateHistoryItem.AccountId);
                    qb.AddColumn(ProductStockTable.UpdatedAtColumn, ProductStockTable.StockColumn, ProductStockTable.UpdatedByColumn);
                    qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                    qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, stockLedgerUpdateHistoryItem.ActualStock);
                    qb.Parameters.Add(ProductStockTable.UpdatedByColumn.ColumnName, AdminDetails.Id);
                    await QueryExecuter.NonQueryAsyc2(qb);
                    /*
                    stockLedgerUpdateHistoryItem.Id = stockLedgerUpdateHistoryItem.ProductStockId;
                    stockLedgerUpdateHistoryItem.ActualStock = stockLedgerUpdateHistoryItem.ActualStock;
                    if (internet == true)
                    {
                        var sc = new RestConnector();
                        await sc.PostAsyc<Either<ProductStock>>(_configHelper.InternalApi.GetDataCorrectionProductStocksUrl, stockLedgerUpdateHistoryItem);
                    }
                    */
                }
                InstanceId = stockLedgerUpdateHistoryItem.InstanceId;
                AccountId = stockLedgerUpdateHistoryItem.AccountId;
            }

        }
        private async Task<string> GetNewStockInvoiceNo(string instanceId)
        {
            var query = _configHelper.AppConfig.IsSqlServer
                ? $"SELECT ISNULL(MAX(CONVERT(INT, NewStockInvoiceNo)),0) + 1 AS InvoiceNo FROM ProductStock WHERE InstanceId = '{instanceId}'"
                : $"SELECT IFNULL(MAX(CAST(NewStockInvoiceNo as INT)),0) + 1 AS InvoiceNo FROM ProductStock WHERE InstanceId = '{instanceId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var num = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            return num;
        }
        public async Task<ProductStock> getProductStockDetails(string AccountId, string InstanceId, string Id)
        {
            var query = $@"select AccountId,InstanceId,BatchNo,ExpireDate,VAT,GstTotal,TaxType,ISNULL(SellingPrice,0) as SellingPrice,ISNULL(MRP,0) as MRP,ISNULL(Stock,0) as Stock,ISNULL(PackageSize,0) as PackageSize,ISNULL(PackagePurchasePrice,0) as PackagePurchasePrice,ISNULL(PurchasePrice,0) as PurchasePrice,CreatedAt,UpdatedAt from productstock with(nolock) where AccountId ='{AccountId}' and InstanceId='{InstanceId}' and Id='{Id}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await List<ProductStock>(qb)).FirstOrDefault();
        }
        public async Task<HQueUser> getAdminDetails(string AccountId)
        {
            var query = $@"select  * from HQueUser with(nolock) where Accountid='{AccountId}' and (InstanceId is null or InstanceId='')";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await List<HQueUser>(qb)).FirstOrDefault();
        }
        public async Task<dynamic> BulkUpdateProductStockOfflineToOnline(string AccountId, string InstanceId)
        {
            var query = $@"select id,Stock from productstock with(nolock) where AccountId ='{AccountId}' and InstanceId='{InstanceId}' for xml path('ProductStock'),root  ('insstock')";
            var qb2 = QueryBuilderFactory.GetQueryBuilder(query);
            ProductStock ProductStockInstanceList = new ProductStock();
            string str = "";
            using (var reader = await QueryExecuter.QueryAsyc(qb2))
            {
                while (reader.Read())
                {
                    str += reader[0].ToString().ToUpper();
                }
            }
            ProductStockInstanceList.AccountId = AccountId;
            ProductStockInstanceList.InstanceId = InstanceId;
            ProductStockInstanceList.Queryxml = str;
            var ProductStockInstance = new RestConnector();
            await ProductStockInstance.PostAsyc<Either<ProductStock>>(_configHelper.InternalApi.UpdateProductStockToOnlineUrl, ProductStockInstanceList);
            return ProductStockInstanceList;
        }
        public async Task<List<ProductStock>> BulkUpdateNegativeStockOfflineToOnline(string AccountId, string InstanceId)
        {
            List<ProductStock> GetNegativeStockList = GetNagativeStockList(AccountId, InstanceId).Result;
            foreach (var NegativeStockItem in GetNegativeStockList)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(ProductStockTable.IdColumn, NegativeStockItem.Id);
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, InstanceId);
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, AccountId);
                qb.AddColumn(ProductStockTable.UpdatedAtColumn, ProductStockTable.StockColumn);
                qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, 0);
                await QueryExecuter.NonQueryAsyc2(qb);

                NegativeStockItem.ActualStock = 0;
                NegativeStockItem.UpdatedAt = CustomDateTime.IST;
                var sc = new RestConnector();
                await sc.PostAsyc<Either<ProductStock>>(_configHelper.InternalApi.GetDataCorrectionProductStocksUrl, NegativeStockItem);
            }
            return GetNegativeStockList;
        }
        public async Task<List<ProductStock>> GetNagativeStockList(string AccountId, string InstanceId)
        {
            var query = $@"select * from productstock with(nolock) where AccountId ='{AccountId}' and InstanceId='{InstanceId}' and stock < 0 ";
            var qb1 = QueryBuilderFactory.GetQueryBuilder(query);
            var result = (await List<ProductStock>(qb1)).ToList();
            return result;
        }
        public async Task<string> GetReverseSyncOfflineToOnlineCompare(string AccountId, string InstanceId)
        {
            var query = $@"select SeedIndex from SyncDataSeed with(nolock) where AccountId ='{AccountId}' and InstanceId='{InstanceId}'";
            var qb1 = QueryBuilderFactory.GetQueryBuilder(query);
            var SeedIndex = QueryExecuter.SingleValueAsyc(qb1).Result.ToString();
            if (SeedIndex != null)
            {
                SeedIndex = SeedIndex + "~" + AccountId + "~" + InstanceId;
                var sc = new RestConnector();
                var result = await sc.PostAsyc<string>(_configHelper.InternalApi.GetReverseSyncOfflineToOnlineCompareUrl, SeedIndex);
                if (result != null)
                {
                    return result.ToString();
                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }

        }
        // Added by Gavaskar Stock Ledger Edit 25-12-2017 End 
    }
}
