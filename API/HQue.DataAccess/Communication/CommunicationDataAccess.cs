﻿using System.Threading.Tasks;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Setup;
using System.Linq;
using System;
using Utilities.Helpers;

namespace HQue.DataAccess.Communication
{
    public class CommunicationDataAccess : BaseDataAccess
    {
        public CommunicationDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {
        }

        public override Task<CommunicationLog> Save<CommunicationLog>(CommunicationLog data)
        {
            return Insert(data, CommunicationLogTable.Table);
        }

        //Added by Sarubala on 16/11/17
        public Task<SmsLog> SaveSmsLog(SmsLog data)
        {
            return Insert(data, SmsLogTable.Table);
        }

        //Added by Sarubala on 23-11-17 - start
        public async Task<Instance> GetInstanceSmsCount(string AccId, string Id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, Id);
            var result = await List<Instance>(qb);

            if(result != null && result.Count > 0)
            {
                result.FirstOrDefault().TotalSmsCount = result.FirstOrDefault().TotalSmsCount != null ? result.FirstOrDefault().TotalSmsCount : 0;
                result.FirstOrDefault().SentSmsCount = result.FirstOrDefault().SentSmsCount != null ? result.FirstOrDefault().SentSmsCount : 0;
            }

            return result.FirstOrDefault();
        }

        public async Task<Instance> UpdateSentSmsCount(Instance i, int smsCount) //Updated by Sarubala on 07-12-17
        {
            i.UpdatedAt = CustomDateTime.IST;
            //var query = $@"update Instance set SentSmsCount = isnull(SentSmsCount,0) + {smsCount}, UpdatedBy = '{i.UpdatedBy}', UpdatedAt ='{i.UpdatedAt}' where AccountId = '{i.AccountId}' and Id = '{i.Id}'";
            var query = $@"update Instance set SentSmsCount = isnull(SentSmsCount,0) + @SentSmsCount, UpdatedBy = @UpdatedBy, UpdatedAt =@UpdatedAt where AccountId = '{i.AccountId}' and Id = '{i.Id}'";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.Parameters.Add("SentSmsCount", smsCount);
            qb.Parameters.Add("UpdatedBy", i.UpdatedBy);
            qb.Parameters.Add("UpdatedAt", i.UpdatedAt);
            await QueryExecuter.NonQueryAsyc(qb);

            //var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Update);
            //qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, i.AccountId);
            //qb.ConditionBuilder.And(InstanceTable.IdColumn, i.Id);
            //qb.AddColumn(InstanceTable.SentSmsCountColumn, InstanceTable.UpdatedAtColumn, InstanceTable.UpdatedByColumn);
            //qb.Parameters.Add(InstanceTable.SentSmsCountColumn.ColumnName, i.SentSmsCount);
            //qb.Parameters.Add(InstanceTable.UpdatedByColumn.ColumnName, i.UpdatedBy);
            //qb.Parameters.Add(InstanceTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            //await QueryExecuter.NonQueryAsyc(qb);

            return i;
        }

        //Added by Sarubala on 23-11-17 - end

    }
} 
 