﻿using HQue.Contract.Infrastructure.Leads;
using HQue.DataAccess.DbModel;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using DataAccess.Criteria;
using Utilities.Helpers;
using DataAccess.Criteria.Interface;
using HQue.Contract.Infrastructure.Setup;
using System.Linq;

namespace HQue.DataAccess.Master
{
    public class LeadsDataAccess : BaseDataAccess
    {
        public LeadsDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {
        }

        public override Task<T> Save<T>(T data)
        {
            //  return Insert(data, LeadsTable.Table);
            data.OfflineStatus = IsOfflineInstalled(data.AccountId, data.InstanceId).Result;
            return Insert3(data, LeadsTable.Table);
        }

        public Task<LeadsProduct> SaveItem(LeadsProduct data)
        {
            //  return Insert(data, LeadsProductTable.Table);
            data.OfflineStatus = IsOfflineInstalled(data.AccountId, data.InstanceId).Result;
            return Insert3(data, LeadsProductTable.Table);
        }

     
        private async Task<bool> IsOfflineInstalled(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.AddColumn(InstanceTable.OfflineVersionNoColumn);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, InstanceId);
            var result = (await List<Instance>(qb)).FirstOrDefault();
            if (result != null)
            {
              return (result.OfflineVersionNo != null && Convert.ToInt32(result.OfflineVersionNo)>0) ? true:false;
            }

            return false;
        }

        public async Task<Leads> UpdateStatus(Leads data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus =Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(LeadsTable.Table, OperationType.Update);
            qb.AddColumn(LeadsTable.LeadStatusColumn);
            qb.ConditionBuilder.And(LeadsTable.IdColumn);

            qb.Parameters.Add(LeadsTable.LeadStatusColumn.ColumnName, data.LeadStatus);
            qb.Parameters.Add(LeadsTable.IdColumn.ColumnName, data.Id);

            if (!string.IsNullOrEmpty(data.LocalLeadStatus))
            {
                qb.AddColumn(LeadsTable.LocalLeadStatusColumn);
                qb.Parameters.Add(LeadsTable.LocalLeadStatusColumn.ColumnName, data.LocalLeadStatus);
                
            }

            if (data.WriteExecutionQuery)
            {
                data.AddExecutionQuery(qb);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qb);
            }

            return data;
        }

        public async Task<List<Leads>> ListByStatus(Leads data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(LeadsTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(LeadsTable.InstanceIdColumn, data.InstanceId);
            if (string.IsNullOrEmpty(data.LeadStatus))
                qb.ConditionBuilder.And(new CustomCriteria(LeadsTable.LeadStatusColumn, CriteriaCondition.IsNull));
            else
                qb.ConditionBuilder.And(LeadsTable.LeadStatusColumn, data.LeadStatus);

            qb.SelectBuilder.SortByDesc(LeadsTable.CreatedAtColumn);

            var result = await List<Leads>(qb);
            return await BindLeadsItemData(result) as List<Leads>;
        }

        public async Task<List<Leads>> LeadByExternalId(string instanceId, string externalId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(LeadsTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(LeadsTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(LeadsTable.ExternalIdColumn, externalId);
            
            qb.SelectBuilder.SortByDesc(LeadsTable.CreatedAtColumn);

            var result = await List<Leads>(qb);
            return await BindLeadsItemData(result) as List<Leads>;
        }


        public async Task<Leads> UpdateStatusWithLeadId(Leads data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(LeadsTable.Table, OperationType.Update);
            qb.AddColumn(LeadsTable.LeadStatusColumn);
            qb.ConditionBuilder.And(LeadsTable.ExternalIdColumn);

            qb.Parameters.Add(LeadsTable.LeadStatusColumn.ColumnName, data.LeadStatus);
            qb.Parameters.Add(LeadsTable.ExternalIdColumn.ColumnName, data.ExternalId);

            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }


        public async Task<Leads> UpdateInfoWithLeadId(Leads data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            List<IDbColumn> cols = new System.Collections.Generic.List<IDbColumn>();

            var qb = QueryBuilderFactory.GetQueryBuilder(LeadsTable.Table, OperationType.Update);
            if (!string.IsNullOrEmpty(data.Name)) cols.Add(LeadsTable.NameColumn);
            if (!string.IsNullOrEmpty(data.Mobile)) cols.Add(LeadsTable.MobileColumn);
            if (!string.IsNullOrEmpty(data.Email)) cols.Add(LeadsTable.EmailColumn);
            if (!string.IsNullOrEmpty(data.Address)) cols.Add(LeadsTable.AddressColumn);
            if (!string.IsNullOrEmpty(data.Pincode)) cols.Add(LeadsTable.PincodeColumn);
            if (!string.IsNullOrEmpty(data.DeliveryMode)) cols.Add(LeadsTable.DeliveryModeColumn);
            if (data.Lat != 0) cols.Add(LeadsTable.LatColumn);
            if (data.Lng != 0) cols.Add(LeadsTable.LngColumn);

            qb.AddColumn(cols.ToArray());

            qb.ConditionBuilder.And(LeadsTable.ExternalIdColumn);
            qb.Parameters.Add(LeadsTable.ExternalIdColumn.ColumnName, data.ExternalId);


            if (!string.IsNullOrEmpty(data.Name)) qb.Parameters.Add(LeadsTable.NameColumn.ColumnName, data.Name);
            if (!string.IsNullOrEmpty(data.Mobile)) qb.Parameters.Add(LeadsTable.MobileColumn.ColumnName, data.Mobile);
            if (!string.IsNullOrEmpty(data.Email)) qb.Parameters.Add(LeadsTable.EmailColumn.ColumnName, data.Email);
            if (!string.IsNullOrEmpty(data.Address)) qb.Parameters.Add(LeadsTable.AddressColumn.ColumnName, data.Address);
            if (!string.IsNullOrEmpty(data.Pincode)) qb.Parameters.Add(LeadsTable.PincodeColumn.ColumnName, data.Pincode);
            if (!string.IsNullOrEmpty(data.DeliveryMode)) qb.Parameters.Add(LeadsTable.DeliveryModeColumn.ColumnName, data.DeliveryMode);
            if (data.Lat != 0) qb.Parameters.Add(LeadsTable.LatColumn.ColumnName, data.Lat);
            if (data.Lng != 0) qb.Parameters.Add(LeadsTable.LngColumn.ColumnName, data.Lng);

            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }

        public Task<List<Leads>> List()
        {
            return List<Leads>(LeadsTable.Table);
        }


        public async Task<IEnumerable<Leads>> List(Leads data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(LeadsTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortByDesc(LeadsTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            var result = await List<Leads>(qb);
            return await BindLeadsItemData(result);
        }

        public async Task<IEnumerable<Leads>> BindLeadsItemData(IEnumerable<Leads> data)
        {
            var list = new List<Leads>();

            foreach (var item in data)
            {
                item.LeadsProduct = await LeadsItemList(item);

                list.Add(item);
            }

            return list;
        }

        public Task<List<LeadsProduct>> LeadsItemList(Leads leads)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(LeadsProductTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(LeadsProductTable.LeadsIdColumn);
            qb.Parameters.Add(LeadsProductTable.LeadsIdColumn.ColumnName, leads.Id);
            qb.AddColumn(LeadsProductTable.IdColumn, LeadsProductTable.NameColumn, LeadsProductTable.QuantityColumn,
                LeadsProductTable.DosageColumn, LeadsProductTable.BAColumn, LeadsProductTable.NumberofDaysColumn, LeadsProductTable.TypeColumn);

            return List<LeadsProduct>(qb);
        }

        private static QueryBuilderBase SqlQueryBuilder(Leads data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.LeadStatus))
            {
                qb.ConditionBuilder.And(LeadsTable.LeadStatusColumn, data.LeadStatus);
            }
            else
            {
                var leadStatusIsNull = new CustomCriteria(LeadsTable.LeadStatusColumn, CriteriaCondition.IsNull);
                qb.ConditionBuilder.And(leadStatusIsNull);
            }
            if (!string.IsNullOrEmpty(data.DoctorName))
            {
                qb.ConditionBuilder.And(LeadsTable.DoctorNameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(LeadsTable.DoctorNameColumn.ColumnName, data.DoctorName);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(LeadsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(LeadsTable.InstanceIdColumn, data.InstanceId);
            }

            return qb;
        }

        public async Task<int> Count(Leads data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(LeadsTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<int> getExternalRowCount(Leads data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(LeadsProductTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(LeadsProductTable.LeadsIdColumn, data.Id);
            qb.ConditionBuilder.And(LeadsProductTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(LeadsProductTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
    }
}
