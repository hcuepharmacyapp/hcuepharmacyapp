﻿using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.DbModel;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using HQue.Contract.Infrastructure.Dashboard;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.Helpers.Extension;
using DataAccess;
using Utilities.Helpers;
using HQue.Contract.External;
using HQue.Contract.Infrastructure.Settings;

namespace HQue.DataAccess.Master
{
    public class VendorDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        public VendorDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper)
            : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }

        public override Task<Vendor> Save<Vendor>(Vendor data)
        {
            var venResut = Insert2(data, VendorTable.Table);
           
            return venResut;
        }

        public async Task SaveOpeningBalancePayment(Vendor data)
        {
            Guid id = Guid.NewGuid();
            DateTime updateDate = CustomDateTime.IST;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("VendorId", data.Id);
            parms.Add("credit", data.BalanceAmount);
            parms.Add("offlineStatus", data.OfflineStatus);
            parms.Add("createdBy", data.CreatedBy);
            parms.Add("id", id.ToString());
            parms.Add("updatedDate", updateDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_InsertPaymentOB", parms);
            string status = "0";
            foreach (IDictionary<string, object> row in result)
            {
                foreach (var pair in row)
                {
                    if (pair.Key == "status")
                        status = Convert.ToString(pair.Value);
                }
            }
            if (status == "1")
            {

                Dictionary<string, object> inserParams = new Dictionary<string, object>();

                inserParams.Add("Id", id.ToString());
                inserParams.Add("Accountid", data.AccountId);
                inserParams.Add("InstanceId", data.InstanceId);
                inserParams.Add("VendorId", data.Id);
                inserParams.Add("TransactionDate", updateDate);
                inserParams.Add("Debit", 0);
                inserParams.Add("Credit", data.BalanceAmount);
                inserParams.Add("OfflineStatus", data.OfflineStatus);
                inserParams.Add("CreatedAt", updateDate);
                inserParams.Add("CreatedBy", data.CreatedBy);

                var qb = QueryBuilderFactory.GetQueryBuilder(PaymentTable.Table, OperationType.Insert);
                qb.AddColumn(PaymentTable.IdColumn, PaymentTable.AccountIdColumn, PaymentTable.InstanceIdColumn,
                    PaymentTable.VendorIdColumn, PaymentTable.TransactionDateColumn, PaymentTable.DebitColumn,
                    PaymentTable.CreditColumn, PaymentTable.OfflineStatusColumn,
                    PaymentTable.CreatedAtColumn, PaymentTable.CreatedByColumn);

                WriteToSyncQueue(new SyncObject()
                {
                    Id = id.ToString(),
                    QueryText = qb.GetQuery(),
                    Parameters = inserParams,
                    EventLevel = "A"
                });
            }
        }

        public async Task UpdateOpeningBalancePayment(Vendor data)
        {
            Guid id = Guid.NewGuid();
            DateTime updateDate = CustomDateTime.IST;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("VendorId", data.Id);
            parms.Add("credit", data.BalanceAmount);
            parms.Add("offlineStatus", data.OfflineStatus);
            parms.Add("createdBy", data.CreatedBy);
            parms.Add("id", id.ToString());
            parms.Add("updatedDate", updateDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_UpdatePaymentOB", parms);
            string status = "0"; string updatedId = "";
            foreach (IDictionary<string, object> row in result)
            {
                foreach (var pair in row)
                {
                    if (pair.Key == "status")
                        //Debit = Convert.ToDecimal(pair.Value); 
                        status = Convert.ToString(pair.Value);
                    if (pair.Key == "updatedId")
                        updatedId = Convert.ToString(pair.Value);
                }
            }
            updateSyncValues(id.ToString(), status, updatedId, updateDate,data);
        }
        public void updateSyncValues(string id, string status, string updatedId,DateTime updateDate, Vendor data)
        {
            if (status == "1")
            {
                Dictionary<string, object> inserParams = new Dictionary<string, object>();


                inserParams.Add("Id", id.ToString());
                inserParams.Add("Accountid", data.AccountId);
                inserParams.Add("InstanceId", data.InstanceId);
                inserParams.Add("VendorId", data.Id);
                inserParams.Add("TransactionDate", updateDate);
                inserParams.Add("Debit", 0);
                inserParams.Add("Credit", data.BalanceAmount);
                inserParams.Add("OfflineStatus", data.OfflineStatus);
                inserParams.Add("CreatedAt", updateDate);
                inserParams.Add("CreatedBy", data.CreatedBy);

                var qb = QueryBuilderFactory.GetQueryBuilder(PaymentTable.Table, OperationType.Insert);
                qb.AddColumn(PaymentTable.IdColumn, PaymentTable.AccountIdColumn, PaymentTable.InstanceIdColumn,
                    PaymentTable.VendorIdColumn, PaymentTable.TransactionDateColumn, PaymentTable.DebitColumn,
                    PaymentTable.CreditColumn, PaymentTable.OfflineStatusColumn,
                    PaymentTable.CreatedAtColumn, PaymentTable.CreatedByColumn);

                WriteToSyncQueue(new SyncObject()
                {
                    Id = id.ToString(),
                    QueryText = qb.GetQuery(),
                    Parameters = inserParams,
                    EventLevel = "A"
                });

            }
            else if (status == "2")
            {
                Dictionary<string, object> inserParams = new Dictionary<string, object>();

                inserParams.Add("Id", updatedId.ToString());
                inserParams.Add("Accountid", data.AccountId);
                inserParams.Add("InstanceId", data.InstanceId);
                inserParams.Add("Credit", data.BalanceAmount);
                inserParams.Add("OfflineStatus", data.OfflineStatus);
                inserParams.Add("UpdatedAt", updateDate);
                inserParams.Add("UpdatedBy", data.CreatedBy);
                var qb = QueryBuilderFactory.GetQueryBuilder(PaymentTable.Table, OperationType.Update);
                qb.AddColumn(
                   PaymentTable.CreditColumn, PaymentTable.OfflineStatusColumn,
                   PaymentTable.UpdatedAtColumn, PaymentTable.UpdatedByColumn);
                qb.ConditionBuilder.And(PaymentTable.IdColumn);
                qb.ConditionBuilder.And(PaymentTable.AccountIdColumn);
                qb.ConditionBuilder.And(PaymentTable.InstanceIdColumn);


                WriteToSyncQueue(new SyncObject()
                {
                    Id = updatedId.ToString(),
                    QueryText = qb.GetQuery(),
                    Parameters = inserParams,
                    EventLevel = "A"
                });
            }
        }

        public Task<Vendor> Update(Vendor data)
        {
            
            return Update2(data, VendorTable.Table);
        }

        public async Task<List<Vendor>> VendorList(Vendor vendor)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(vendor, qb);
            qb.SelectBuilder.SortBy(VendorTable.NameColumn); 
            //qb.SelectBuilder.SortByDesc(VendorTable.CreatedAtColumn);            
            qb.SelectBuilder.MakeDistinct = true;

            var data = await List<Vendor>(qb); 
            //data = data.Where(x => x.Status == 1).ToList(); // Active Vendor

            data = data.Select(x =>
            {

                //x.LocationType = x.LocationType == null ? 0 : x.LocationType;

                //Local Vendor
                if((x.EnableCST == null || !(bool)x.EnableCST) && x.LocationType == null)
                {
                    x.LocationType = 1;
                }
                //Inter State Vendor
                if((x.EnableCST == null || (bool)x.EnableCST) && x.LocationType == null)
                {
                    x.LocationType = 2;
                }

                return x;
            }).ToList();

            var vendorFormulaList = await GetPurchaseFormulaList(vendor.AccountId, "");
            var result = data.Select(x =>
            {
                x.VendorPurchaseFormula = vendorFormulaList.Where(y => y.Id == x.VendorPurchaseFormulaId).Select(z => z).FirstOrDefault();
                return x;
            }).ToList();
            return result;
        }

        public async Task<List<Vendor>> VendorDataList(Vendor vendor)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(vendor, qb);
            qb.SelectBuilder.SortByDesc(VendorTable.CreatedAtColumn);
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(VendorTable.NameColumn);
            var data = await List<Vendor>(qb);

            if (vendor.Status == 1)
            {
                data = data.Where(x => x.Status == 1).ToList(); // Active Vendor
            }
            else if (vendor.Status == 2)
            {
                // All Vendors
            }
            else
            {
                data = data.Where(x => x.Status == 1 || x.Status == 3).ToList(); // Active & Hide Vendor
            }

            data = data.Select(x =>
            {
                if ((x.EnableCST == null || !(bool)x.EnableCST) && x.LocationType == null)
                {
                    x.LocationType = 1;
                }
                if ((x.EnableCST == null || (bool)x.EnableCST) && x.LocationType == null)
                {
                    x.LocationType = 2;
                }
                return x;
            }).ToList();

            var vendorFormulaList = await GetPurchaseFormulaList(vendor.AccountId, "");
            var result = data.Select(x =>
            {
                x.VendorPurchaseFormula = vendorFormulaList.Where(y => y.Id == x.VendorPurchaseFormulaId).Select(z => z).FirstOrDefault();
                return x;
            }).ToList();

            return result;
        }
        public Task <List<AlternateVendorProduct>> CheckExisitingProductList(string vendorId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(AlternateVendorProductTable.Table, OperationType.Select);
            qb.AddColumn(AlternateVendorProductTable.AlternatePackageSizeColumn, AlternateVendorProductTable.ProductIdColumn, AlternateVendorProductTable.AlternateProductNameColumn, AlternateVendorProductTable.UpdatedPackageSizeColumn, AlternateVendorProductTable.VendorIdColumn);
            qb.ConditionBuilder.And(AlternateVendorProductTable.VendorIdColumn, vendorId);

            var data = List<AlternateVendorProduct>(qb);           
           
            return data;


        }


        public Task<List<Vendor>> List(Vendor data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortBy(VendorTable.NameColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            return List<Vendor>(qb);
        }
        //Added by Senthil.S
        public async Task<int> CheckOBPaymentPaid(string accountId, string instanceId, string customerId)
        {
            int Debit = 0;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);
            parms.Add("CustomerId", customerId);
            parms.Add("type", "VEN");
            var paymentDone = await sqldb.ExecuteProcedureAsync<dynamic>("usp_CheckCustomerVendorOBPaid", parms);
            foreach (IDictionary<string, object> row in paymentDone)
            {
                foreach (var pair in row)
                {
                    if (pair.Key == "Debit")
                        //Debit = Convert.ToDecimal(pair.Value); 
                        Debit = Convert.ToInt32(pair.Value);

                }
            }

            return Debit;
        }
        private static QueryBuilderBase SqlQueryBuilder(Vendor data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Id))
            {
                qb.ConditionBuilder.And(VendorTable.IdColumn, data.Id);
            }
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(VendorTable.NameColumn, data.Name, CriteriaEquation.Equal);
            }
            if (!string.IsNullOrEmpty(data.Email))
            {
                qb.ConditionBuilder.And(VendorTable.EmailColumn, data.Email);
            }
            if (!string.IsNullOrEmpty(data.Mobile))
            {
                qb.ConditionBuilder.And(VendorTable.MobileColumn, data.Mobile);
            }
            if (!string.IsNullOrEmpty(data.AccountId))
            {
                qb.ConditionBuilder.And(VendorTable.AccountIdColumn, data.AccountId);
            }

            return qb;
        }

        private static QueryBuilderBase SqlQueryBuilderByDrugLicenseNo(Vendor data, QueryBuilderBase qb)
        {
            //if (!string.IsNullOrEmpty(data.Id))
            //{
            //    qb.ConditionBuilder.And(VendorTable.IdColumn, data.Id);
            //}
            if (!string.IsNullOrEmpty(data.Name) && !string.IsNullOrEmpty(data.DrugLicenseNo))
            {
                qb.ConditionBuilder.And(VendorTable.DrugLicenseNoColumn, data.DrugLicenseNo);
            }
            if (!string.IsNullOrEmpty(data.AccountId))
            {
                qb.ConditionBuilder.And(VendorTable.AccountIdColumn, data.AccountId);
            }

            return qb;
        }

        public async Task<Vendor> GetById(Vendor vendor)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb.AddColumn(VendorTable.Table.ColumnList.ToArray());
            qb = SqlQueryBuilder(vendor, qb);

            qb.JoinBuilder.LeftJoin(StateTable.Table, StateTable.IdColumn, VendorTable.StateIdColumn);
            qb.JoinBuilder.AddJoinColumn(StateTable.Table, StateTable.IdColumn, StateTable.StateNameColumn, StateTable.StateCodeColumn);

            var ven = new Vendor();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendor1 = new Vendor().Fill(reader);
                    var stateMas = new State().Fill(reader);
                    vendor1.StateRef = stateMas;
                    vendor1.VendorPurchaseFormula = await GetPurchaseFormulaById(vendor1.VendorPurchaseFormulaId);
                    ven = vendor1;
                }
            }
            return ven; //return (await List<Vendor>(qb)).First();            
        }

        /// <summary>
        /// Vendor status can't be changed if payment is pending 
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="vendorId"></param>
        public async Task<bool> PaymentStatus(string accountId, string instanceId, string vendorId)
        {
            decimal? Debit = 0;
            decimal? Credit = 0;

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);
            parms.Add("VendorId", vendorId);
            var paymentBalance = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_PaymentStatus", parms);
            var rows = paymentBalance;

            foreach (IDictionary<string, object> row in rows)
            {                
                foreach (var pair in row)
                {
                    if (pair.Key == "Debit")
                        Debit = Convert.ToDecimal(pair.Value); //pair.Value as decimal? ?? 0;
                    if (pair.Key == "Credit")
                        Credit = Convert.ToDecimal(pair.Value); //pair.Value as decimal? ?? 0;
                }
            }

            if (Math.Round((decimal)Debit, MidpointRounding.AwayFromZero) == Math.Round((decimal)Credit, MidpointRounding.AwayFromZero))
                return true;
            else
                return false;
        }

        public Task<List<State>> GetState(string stateName = "T")
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(StateTable.Table, OperationType.Select);
            qb.AddColumn(StateTable.IdColumn, StateTable.StateNameColumn, StateTable.StateCodeColumn);
            //qb.JoinBuilder.Join(VendorTable.Table, VendorTable.StateIdColumn, StateTable.IdColumn);

            if (!string.IsNullOrEmpty(stateName))
            {
                qb.ConditionBuilder.And(StateTable.StateNameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(StateTable.StateNameColumn.ColumnName, stateName);
            }
            return List<State>(qb); //var data = List<State>(qb);
        }

        public async Task<int> Count(Vendor data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public Task<List<Vendor>> CheckUniqueVendor(Vendor data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(data, qb);

            return List<Vendor>(qb);
        }

        public Task<List<Vendor>> CheckUniqueVendorByDrugLicenseNo(Vendor data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb = SqlQueryBuilderByDrugLicenseNo(data, qb);

            return List<Vendor>(qb);
        }

        public async Task<Vendor> GetVendorId(Vendor data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(VendorTable.NameColumn, data.Name);
                qb.ConditionBuilder.And(VendorTable.AccountIdColumn, data.AccountId);
                //qb.ConditionBuilder.And(VendorTable.InstanceIdColumn, data.InstanceId);  //Altered by Sarubala on 25-09-17 as vendor is taken by account wise not instance wise
            }

            var result = (await List<Vendor>(qb));

            if (result.Count > 0)
            {
                var vendor = result.First();
                vendor.VendorPurchaseFormula = await GetPurchaseFormulaById(vendor.VendorPurchaseFormulaId);
                return vendor;
            }
            else
                return null;
        }

        // Added Gavaskar 15-02-2017 Start
        public async Task<Vendor> GetVendorName(Vendor data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            //if (!string.IsNullOrEmpty(data.Name))
            //{
            qb.ConditionBuilder.And(VendorTable.IdColumn, data.Id);
            qb.ConditionBuilder.And(VendorTable.AccountIdColumn, data.AccountId);
            //qb.ConditionBuilder.And(VendorTable.InstanceIdColumn, data.InstanceId);
            //}

            var result = (await List<Vendor>(qb));

            if (result.Count > 0)
            {            
                var vendor = result.First();
                vendor.VendorPurchaseFormula = await GetPurchaseFormulaById(vendor.VendorPurchaseFormulaId);
                return vendor;
            }
            else
                return null;
        }
        // Added Gavaskar 15-02-2017 End

        public async Task<List<TemplatePurchaseChildMaster>> GetTemplatePurchaseMaster(TemplatePurchaseChildMaster data, string AccountId, string InstanceId, string vendorId)
        {
            var query = $@"select b.AccountId,b.InstanceId,b.TemplateColumnId,a.VendorHeaderName,b.HeaderIndex,b.Headername from TemplatePurchaseMaster a 
            inner join TemplatePurchaseChildMaster b on  b.TemplateColumnId = a.id
            inner join TemplatePurchaseChildMasterSettings c on c.AccountId=b.AccountId and c.InstanceId=b.InstanceId and b.VendorId=c.VendorId
            Where b.AccountId='{AccountId}' and b.InstanceId='{InstanceId}' and c.VendorId='{vendorId}' order by TemplateColumnId ";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<TemplatePurchaseChildMaster>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var productList = new TemplatePurchaseChildMaster
                    {
                        VendorHeaderName = reader["VendorHeaderName"].ToString(),
                        HeaderIndex = Convert.ToInt32(reader["HeaderIndex"]),
                        HeaderName = reader["Headername"].ToString(),
                    };
                    list.Add(productList);
                }
            }
            return list;
        }

        public async Task<List<TemplatePurchaseChildMaster>> getBuyTemplatePurchaseChildMaster(string AccountId, string InstanceId, string vendorId)
        {
            var query = $@"select a.VendorHeaderName,b.HeaderName from TemplatePurchaseMaster a
            inner join TemplatePurchaseChildMaster b on a.id=b.TemplateColumnId
            inner join TemplatePurchaseChildMasterSettings c on c.AccountId=b.AccountId and c.InstanceId=b.InstanceId and b.VendorId=c.VendorId where b.AccountId='{AccountId}' 
            and b.InstanceId='{InstanceId}' and c.VendorId='{vendorId}' order by a.id ";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<TemplatePurchaseChildMaster>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new TemplatePurchaseChildMaster
                    {
                        VendorHeaderName = reader["VendorHeaderName"].ToString(),
                        HeaderName = reader["HeaderName"].ToString(),
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public async Task<TemplatePurchaseChildMasterSettings> getBuyTemplatePurchaseChildMasterSettings(string AccountId, string InstanceId, string vendorId)
        {
            var query = $@"select distinct c.HeaderOption,c.VendorRowPos,c.ProductRowPos,c.DateFormat,c.VendorId from TemplatePurchaseMaster a
            inner join TemplatePurchaseChildMaster b on a.id=b.TemplateColumnId
            left join TemplatePurchaseChildMasterSettings c on c.AccountId=b.AccountId and c.InstanceId=b.InstanceId and b.VendorId=c.VendorId where c.AccountId='{AccountId}' 
            and c.InstanceId='{InstanceId}' and
             c.VendorId='{vendorId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await List<TemplatePurchaseChildMasterSettings>(qb)).FirstOrDefault();

            //var list = new List<TemplatePurchaseChildMasterSettings>();
            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var item = new TemplatePurchaseChildMasterSettings
            //        {
            //            HeaderOption = reader["HeaderOption"] as bool? ?? false,
            //            VendorRowPos = Convert.ToInt32(reader["VendorRowPos"]),
            //            ProductRowPos = Convert.ToInt32(reader["ProductRowPos"]),
            //            DateFormat = reader["DateFormat"].ToString(),
            //        };
            //        list.Add(item);
            //    }
            //}
            //return list;
        }

        public async Task<List<TemplatePurchaseMaster>> BuyImportSetting(TemplatePurchaseMaster templatePurchaseMaster)
        {
            var query = $@"select max(id) as VendorHeaderId,VendorHeaderName from TemplatePurchaseMaster where VendorHeaderName not in('IsVendorRow','VendorRowPos','ProductRowPos')
                group by VendorHeaderName order by max(id)";  //Altered by Sarubala on 25-09-17
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<TemplatePurchaseMaster>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new TemplatePurchaseMaster
                    {
                        VendorHeaderName = reader["VendorHeaderName"].ToString(),
                        TemplateColumnId = reader["VendorHeaderId"].ToString()
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public Task<VendorPurchaseFormula> SavePurchaseFormula(VendorPurchaseFormula data)
        {
            return Insert(data, VendorPurchaseFormulaTable.Table);
        }

        public async Task<List<VendorPurchaseFormula>> GetPurchaseFormulaList(string AccountId, string formulaName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseFormulaTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VendorPurchaseFormulaTable.AccountIdColumn, AccountId);

            if (!string.IsNullOrEmpty(formulaName))
            {
                qb.ConditionBuilder.And(VendorPurchaseFormulaTable.FormulaNameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorPurchaseFormulaTable.FormulaNameColumn.ColumnName, formulaName);
            }

            var result = (await List<VendorPurchaseFormula>(qb));
            return result;
        }

        public async Task<VendorPurchaseFormula> GetPurchaseFormulaById(string Id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseFormulaTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VendorPurchaseFormulaTable.IdColumn, Id);            

            var result = (await List<VendorPurchaseFormula>(qb));
            return result.FirstOrDefault();
        }

        //Added by Sarubala on 17-11-17
        public async Task<bool> GetVendorCreateSmsSetting(string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SmsSettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SmsSettingsTable.InstanceIdColumn, InstanceId);
            qb.AddColumn(SmsSettingsTable.IsVendorCreateSmsColumn);
            bool sendsms = false;
            var result = await List<SmsSettings>(qb);
            if(result != null && result.Count > 0)
            {
                sendsms = Convert.ToBoolean(result.First().IsVendorCreateSms != null ? result.First().IsVendorCreateSms : false);
            }

            return sendsms;
        }
    }
}
