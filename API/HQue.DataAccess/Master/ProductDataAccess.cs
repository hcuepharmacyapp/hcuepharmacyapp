﻿using HQue.Contract.Infrastructure.Master;
using HQue.Contract.Infrastructure;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Inventory;
using DataAccess.Criteria;
using DataAccess;
using DataAccess.Criteria.Interface;
using System.Text;
using System.IO;
using HQue.Contract.External;
using HQue.Contract.Base;
using HQue.DataAccess;
using HQue.Contract.Infrastructure.Settings;

namespace HQue.DataAccess.Master
{
    public class ProductDataAccess : BaseDataAccess
    {
        private ConfigHelper _configHelper;
        SqlDatabaseHelper sqldb;
        private object list1;

        public ProductDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        public override async Task<Product> Save<Product>(Product data1)
        {
            object data2 = data1;
            var data = (HQue.Contract.Infrastructure.Master.Product)data2;

            if (_configHelper.AppConfig.OfflineMode == false)
            {
                data.Code = await GetProductMaxCode(data.AccountId);
            }
            else
            { //Updated by Sarubala on 30-11-18 for Product Code
               string code1 = await GetProductMaxCode(data.AccountId);
                data.Code = "00" + code1 ;
            }
            data.Name = data.Name.Trim();

            if (data1.WriteExecutionQuery)
            {
                SetInsertMetaData(data, ProductTable.Table);
                WriteInsertExecutionQuery(data, ProductTable.Table);
            }
            else
            {
                var product = await Insert2(data, ProductTable.Table);
                WriteSyncQueueInsert(data, ProductTable.Table, "A", data.InstanceId, true);
            }

            ProductInstance pi = new ProductInstance();
            pi.AccountId = data.AccountId;
            pi.InstanceId = data.InstanceId;
            pi.ProductId = data.Id;
            pi.RackNo = null;
            pi.BoxNo = null;
            pi.ReOrderLevel = null;
            pi.ReOrderQty = null;
            pi.CreatedBy = data.CreatedBy;
            pi.UpdatedBy = data.UpdatedBy;
            pi.CreatedAt = data.CreatedAt;
            var pis = SaveProductInstance(pi);
            return data1;
        }

        //Added by Sarubala on 28/04/18 to fix issue of adding Global products twice
        public async Task<Product> SaveDC<Product>(Product data1)
        {
            object data2 = data1;
            var data = (HQue.Contract.Infrastructure.Master.Product)data2;

            if (_configHelper.AppConfig.OfflineMode == false)
            {
                data.Code = await GetProductMaxCode(data.AccountId);
            }
            else
            {
                data.Code = null;
            }
            data.Name = data.Name.Trim();

            if (data.WriteExecutionQuery)
            {
                SetInsertMetaData(data, ProductTable.Table);
                WriteInsertExecutionQuery(data, ProductTable.Table);
            }
            else
            {
                var product = await Insert2(data, ProductTable.Table);
            }
            return data1;
        }

        // Added by Violet 27-04-17
        public async Task<Product> SaveImportProduct(Product data)
        {
            if (_configHelper.AppConfig.OfflineMode == false)
            {
                data.Code = await GetProductMaxCode(data.AccountId);
            }
            else
            {
                //Updated by Sarubala on 05-01-19 for Product Code
                string code1 = await GetProductMaxCode(data.AccountId);
                data.Code = "00" + code1;
            }
            data.Name = data.Name.Trim();
            //added by nandhini for transaction
            if (data.WriteExecutionQuery)
            {
                SetInsertMetaData(data, ProductTable.Table);
                WriteInsertExecutionQuery(data, ProductTable.Table);
                return await Task.FromResult(data);
            }
            else
            {
                var product = await Insert2(data, ProductTable.Table);
                return product;
            }

        }

        public async Task<string> GetProductMaxCode(string AccountId)
        {
            var query = $"SELECT ISNULL(MAX(CAST(Code AS INT)),0)+1 AS Code FROM Product WHERE AccountId = '{AccountId}' AND ISNUMERIC(Code) = 1";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var num = (await QueryExecuter.SingleValueAsyc(qb));
            return num.ToString();
        }

        public async Task SaveProductInstance(Product data) // for import
        {
            try
            {
                ProductInstance pi = new ProductInstance();
                pi.AccountId = data.AccountId;
                pi.InstanceId = data.InstanceId;
                pi.ProductId = data.Id;
                pi.CreatedBy = data.CreatedBy;
                pi.UpdatedBy = data.UpdatedBy;
                pi.CreatedAt = data.CreatedAt;
                pi.RackNo = data.RackNo;
                pi.BoxNo = data.BoxNo; /*BoxNo included by settu on 19-05-2017*/
                pi.ReOrderLevel = data.ReOrderLevel;
                pi.ReOrderQty = data.ReOrderQty;
                pi.SetExecutionQuery(data.GetExecutionQuery(), data.WriteExecutionQuery);
                var pis = await SaveProductInstance(pi, data.RackNo);
            }
            catch (Exception ed)
            {

            }


        }
        public async Task SaveProductInstanceImport(Product data) // for import data
        {
            try
            {
                ProductInstance pi = new ProductInstance();
                pi.AccountId = data.AccountId;
                pi.InstanceId = data.InstanceId;
                pi.ProductId = data.Id;
                pi.CreatedBy = data.CreatedBy;
                pi.UpdatedBy = data.UpdatedBy;
                pi.CreatedAt = data.CreatedAt;
                pi.RackNo = data.RackNo;
                pi.BoxNo = data.BoxNo; 
                pi.ReOrderLevel = data.ReOrderLevel;
                pi.ReOrderQty = data.ReOrderQty;
                var pis = await SaveProductInstance(pi, data.RackNo);
            }
            catch (Exception ed)
            {
            }
        }
        // New method added to Save Product Instance data
        public async Task<ProductInstance> SaveProductInstance(ProductInstance data)
        {
            if (data.WriteExecutionQuery)
            {
                SetInsertMetaData(data, ProductInstanceTable.Table);
                WriteInsertExecutionQuery(data, ProductInstanceTable.Table);
                return data;
            }

            else
            {
                var ProductInstanceNew =  await Insert(data, ProductInstanceTable.Table);
                WriteSyncQueueInsert(data, ProductInstanceTable.Table, "A", data.InstanceId, true);
                return ProductInstanceNew;
                //Method modified by Poongodi on 07/06/2017 (All Instance Insert removed)
            }
        }
        //Method modified by Poongodi on 07/06/2017 (All Instance Insert removed)
        public async Task<ProductInstance> SaveProductInstance(ProductInstance data, string rackno)
        {  //added by nandhini for transaction
            if (data.WriteExecutionQuery)
            {
                SetInsertMetaData(data, ProductInstanceTable.Table);
                WriteInsertExecutionQuery(data, ProductInstanceTable.Table);
                return await Task.FromResult(data);
            }
            else
            {
                var ProductInstanceNew =  await Insert2(data, ProductInstanceTable.Table);
                WriteSyncQueueInsert(data, ProductInstanceTable.Table, "A", data.InstanceId, true);
                return ProductInstanceNew;
            }

        }



        public async Task<AlternateVendorProduct> saveAlternateName(AlternateVendorProduct data)
        {
            return await Insert(data, AlternateVendorProductTable.Table);
        }

        //Added by Sarubala on 04-05-2018 - start
        public async Task<List<AlternateVendorProduct>> updatePackageSize(List<AlternateVendorProduct> data)
        {
            foreach (var item in data)
            {
                string tempAltName = item.AlternateProductName;
                if(item.isAddNewProduct == true && String.IsNullOrEmpty(item.ProductId))
                {
                    var query = $"SELECT * FROM {ProductTable.TableName} WHERE {ProductTable.NameColumn} = '{item.AlternateProductName.ToString().Replace("'", "''")}' and {ProductTable.AccountIdColumn} = '{item.AccountId}'";
                    var qb1 = QueryBuilderFactory.GetQueryBuilder(query);
                    var resultProduct = await List<Product>(qb1);
                    if (resultProduct.Count == 0)
                    {
                        var result = await InsertNewProduct(item);
                        item.ProductId = result.Id;
                    }
                    else
                    {
                        item.ProductId = resultProduct.FirstOrDefault().Id;
                    }
                }

                var ncount = 0;
                if (!string.IsNullOrEmpty(item.ProductId))
                {
                    var query = $"SELECT COUNT(1) FROM {ProductTable.TableName} WHERE {ProductTable.IdColumn} = '{item.ProductId}' and {ProductTable.AccountIdColumn} = '{item.AccountId}'";
                    var qb1 = QueryBuilderFactory.GetQueryBuilder(query);

                    ncount = Convert.ToInt32(await SingleValueAsyc(qb1));

                    if(ncount == 0)
                    {
                        item.AlternateProductName = item.ProductName;
                        var result = await InsertNewProduct(item);
                        item.ProductId = result.Id;
                        ncount++;
                    }
                }

                string packageSizeDataType = item.AlternatePackageSize.GetType().ToString();

                int altproductCount = await GetAlternateProductCount(item);

                if ((packageSizeDataType.ToLower() == "system.string" || ncount > 0) && altproductCount == 0)
                {
                    string input;
                    if (item.AlternatePackageSize.Contains("'"))
                    {
                        input = item.AlternatePackageSize.Replace("'", " ");
                        item.AlternatePackageSize = input;
                    }
                    if(item.AlternatePackageSize=="-")
                    {
                        item.AlternatePackageSize = "";
                    }

                    var qb = QueryBuilderFactory.GetQueryBuilder(AlternateVendorProductTable.Table, OperationType.Insert);
                    var query = $@"INSERT INTO AlternateVendorProduct(Id,AccountId,InstanceId,ProductId,VendorId,AlternateProductName,OfflineStatus,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy,AlternatePackageSize,UpdatedPackageSize)
                        VALUES('{Guid.NewGuid().ToString()}','{item.AccountId}','{item.InstanceId}','{item.ProductId}','{item.VendorId}','{tempAltName.ToString().Replace("'", "''")}','{item.OfflineStatus}','{CustomDateTime.IST.ToString("dd-MMM-yyyy HH':'mm':'ss.fff")}','{CustomDateTime.IST.ToString("dd-MMM-yyyy HH':'mm':'ss.fff")}','{item.InstanceId}','{item.InstanceId}','{item.AlternatePackageSize}','{item.UpdatedPackageSize}')";
                    qb = QueryBuilderFactory.GetQueryBuilder(query);
                    await QueryExecuter.NonQueryAsyc(qb);
                }

            }

            return data;
        }

        public async Task<Product> InsertNewProduct(AlternateVendorProduct data)
        {
            Product tempProduct = new Product();
            tempProduct.Name = data.AlternateProductName;
            tempProduct.AccountId = data.AccountId;
            tempProduct.InstanceId = data.InstanceId;
            tempProduct.CreatedBy = data.CreatedBy;
            tempProduct.UpdatedBy = data.UpdatedBy;

            SetInsertMetaData(tempProduct, ProductTable.Table);
            var ProductNew =  await Insert2(tempProduct, ProductTable.Table);
            WriteSyncQueueInsert(ProductNew, ProductTable.Table, "A", data.InstanceId, true);
            return ProductNew;
        }

        private async Task<int> GetAlternateProductCount(AlternateVendorProduct altProduct)
        {
            int count = 0;
            var qb = QueryBuilderFactory.GetQueryBuilder(AlternateVendorProductTable.Table, OperationType.Count);

            qb.ConditionBuilder.And(AlternateVendorProductTable.AccountIdColumn, altProduct.AccountId).And(AlternateVendorProductTable.InstanceIdColumn, altProduct.InstanceId);
            qb.ConditionBuilder.And(AlternateVendorProductTable.ProductIdColumn, altProduct.ProductId);
            qb.ConditionBuilder.And(AlternateVendorProductTable.VendorIdColumn, altProduct.VendorId);
            qb.ConditionBuilder.And(AlternateVendorProductTable.AlternateProductNameColumn, altProduct.AlternateProductName);
            qb.ConditionBuilder.And(AlternateVendorProductTable.AlternatePackageSizeColumn, altProduct.AlternatePackageSize);
            qb.ConditionBuilder.And(AlternateVendorProductTable.UpdatedPackageSizeColumn, altProduct.UpdatedPackageSize);

            if(Convert.ToInt32(await SingleValueAsyc(qb)) > 0)
            {
                count = Convert.ToInt32(await SingleValueAsyc(qb));
            }
            return count;
        }

        //Added by Sarubala on 04-05-2018 - end

        public async Task<Product> Update(Product data)
        {
            var product = await Update2(data, ProductTable.Table);
            /*Below Product stock Update method added by Poongodi on 04/07/2017 */
            var bResult = await ProductStockUpdate(product.Id, Convert.ToDecimal(product.GstTotal), product.UpdatedBy, product.AccountId, product.InstanceId,Convert.ToInt32(data.Status));
            return product;
        }

        //New function added by Manivannan on 10-Oct-2017
        public async Task<int> GetGstNullAvailable(string accountId, string instanceId)
        {
            //var query = $"Select Count(1) From {ProductTable.TableName} Where {ProductTable.GstTotalColumn} Is NULL And AccountId = @AccountId";
            //var query = "select count(distinct p.id) count_prd from product p Left Join productStock ps on p.id = ps.productId and ps.GstTotal is not null where p.accountId = @AccountId and p.GstTotal IS NULL and ps.id is null";
            //var query = "select count(distinct p.id) count_prd from (select Id,AccountId,GstTotal from product WITH(nolock) where accountId = @AccountId and  GstTotal IS NULL) p Left Join (select Id,ProductId from productStock WITH(nolock) where accountId = @AccountId and GstTotal is not null) ps on p.id = ps.productId where p.accountId = @AccountId and p.GstTotal IS NULL and ps.id is null";

            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //qb.Parameters.Add(ProductTable.AccountIdColumn.ColumnName, accountId);
            //var count = 0;
            //count = Convert.ToInt32(await SingleValueAsyc(qb));
            //return count;

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);
            //var queryresult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetProductsForOrderTab", parms);
            var queryResult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_GstNullProducts", parms);
            var result = queryResult.FirstOrDefault();
            return (result == null ? 0 : (result.PrdCount as int? ?? 0));
        }

        // New method added to Update Product Instance data
        public async Task<string> UpdateProductInstance(ProductInstance data, string sUpdatetype)
        {
            var query = $"SELECT COUNT(1) FROM {ProductInstanceTable.TableName} WHERE {ProductInstanceTable.ProductIdColumn} = '{data.ProductId}' and {ProductInstanceTable.AccountIdColumn}='{data.AccountId}'  and {ProductInstanceTable.InstanceIdColumn}='{data.InstanceId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var ncount = Convert.ToInt32(await SingleValueAsyc(qb));

            if (ncount > 0)
            {

                qb = QueryBuilderFactory.GetQueryBuilder(ProductInstanceTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(ProductInstanceTable.ProductIdColumn, data.ProductId);
                qb.ConditionBuilder.And(ProductInstanceTable.InstanceIdColumn, data.InstanceId);
                qb.ConditionBuilder.And(ProductInstanceTable.AccountIdColumn, data.AccountId);
                if (Convert.ToString(sUpdatetype).ToLower() == "reorder")
                {
                    qb.AddColumn(ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn, ProductInstanceTable.ReOrderQtyColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn);
                }
                else if (Convert.ToString(sUpdatetype).ToLower() == "rackno")
                {
                    qb.AddColumn(ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn);
                }
                else
                {
                    qb.AddColumn(ProductInstanceTable.ReOrderLevelColumn, ProductInstanceTable.ReOrderQtyColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn, ProductInstanceTable.OfflineStatusColumn, ProductInstanceTable.ReOrderModifiedColumn);
                }
                qb.Parameters.Add(ProductInstanceTable.RackNoColumn.ColumnName, data.RackNo);
                qb.Parameters.Add(ProductInstanceTable.BoxNoColumn.ColumnName, data.BoxNo);
                qb.Parameters.Add(ProductInstanceTable.ReOrderLevelColumn.ColumnName, data.ReOrderLevel);
                qb.Parameters.Add(ProductInstanceTable.ReOrderQtyColumn.ColumnName, data.ReOrderQty);
                qb.Parameters.Add(ProductInstanceTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                qb.Parameters.Add(ProductInstanceTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
                qb.Parameters.Add(ProductInstanceTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
                qb.Parameters.Add(ProductInstanceTable.ReOrderModifiedColumn.ColumnName, data.ReOrderModified);
                var tt = await QueryExecuter.NonQueryAsyc(qb);
                return Convert.ToString(tt);
            }
            else
            {
                var pis = await SaveProductInstance(data);
                return "true";
            }
        }


        //newly added by nandhini 
        public async Task<Product> getGenericDetails(Product product)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.TypeColumn, ProductTable.ScheduleColumn,
                ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);
            qb.ConditionBuilder.And(ProductTable.GenericNameColumn, product.GenericName);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, product.AccountId);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, product.InstanceId);
            var product1 = (await List<Product>(qb)).FirstOrDefault();
            if ((product1 != null) && (!string.IsNullOrEmpty(product1.GenericName)))
            {
                var qb1 = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
                qb1.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn).
                    AddJoinColumn(ProductStockTable.Table, ProductStockTable.StockColumn);
                qb1.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.ManufacturerColumn);
                qb1.ConditionBuilder.And(ProductStockTable.AccountIdColumn, product.AccountId);
                qb1.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, product.InstanceId);
                qb1.ConditionBuilder.And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Greater);
                qb1.ConditionBuilder.And(ProductTable.GenericNameColumn, product1.GenericName);
                //qb1.SelectBuilder.SortBy(ProductTable.NameColumn);
                qb1.SelectBuilder.SortByDesc(ProductStockTable.StockColumn);

                var list1 = new List<ProductStock>();
                using (var reader = await QueryExecuter.QueryAsyc(qb1))
                {
                    while (reader.Read())
                    {
                        var productstock = new ProductStock();
                        productstock.Product.Id = reader[ProductTable.IdColumn.ColumnName].ToString();
                        productstock.Product.Name = reader[ProductTable.NameColumn.ColumnName].ToString();
                        productstock.Product.Manufacturer = reader[ProductTable.ManufacturerColumn.ColumnName].ToString();
                        productstock.Stock = reader[ProductStockTable.StockColumn.FullColumnName] as decimal? ?? 0;
                        list1.Add(productstock);
                    }
                }
                var li1 = list1.GroupBy(p => p.Product.Id).Select(
                                                   pd1 => new
                                                   {
                                                       Id = pd1.Key,
                                                       Name = pd1.First().Product.Name,
                                                       Manufacturer = pd1.First().Product.Manufacturer,
                                                       Stock = pd1.Sum(s => s.Stock),
                                                   });
                var list2 = new List<ProductStock>();
                foreach (var l1 in li1)
                {
                    var productstock1 = new ProductStock();
                    productstock1.Product.Id = l1.Id;
                    productstock1.Product.Name = l1.Name;
                    productstock1.Product.Manufacturer = l1.Manufacturer;
                    productstock1.Stock = l1.Stock;
                    list2.Add(productstock1);
                }
                product1.ProductStock = list2;
                return product1;
            }
            else
            {
                return product;
            }
        }
        /*Method Added by Poongodi on 09/06/2007*/
        /// <summary>
        /// To Get Count from Product instance based on Productid and Instanceid
        /// </summary>
        /// <param name="sAccountid"></param>
        /// <param name="sInstanceId"></param>
        /// <param name="sProductid"></param>
        /// <returns></returns>
        public async Task<int> GetProductInstanceCount(string sAccountid, string sInstanceId, string sProductid)
        {
            var query = $"SELECT COUNT(1) FROM {ProductInstanceTable.TableName} WHERE {ProductInstanceTable.ProductIdColumn} = '{sProductid}' and {ProductInstanceTable.AccountIdColumn}='{sAccountid}'  and {ProductInstanceTable.InstanceIdColumn}='{sInstanceId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var ncount = Convert.ToInt32(await SingleValueAsyc(qb));
            return ncount;
        }
        //newly added by nandhini

        public async Task UpdateProductRackNo(Product data)
        {
            /*For ProductInstance Method modified by Poongodi on 08/06/2017*/
            int ncount = await GetProductInstanceCount(data.AccountId, data.InstanceId, data.Id);

            if (ncount > 0)
            {
                data.RackNo = data.RackNo as string ?? "";
                data.BoxNo = data.BoxNo as string ?? "";
                if (data.RackNo != null && Convert.ToString(data.RackNo.Trim()) != "" || data.BoxNo != null & Convert.ToString(data.BoxNo.Trim()) != "")
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(ProductInstanceTable.Table, OperationType.Update);
                    qb.ConditionBuilder.And(ProductInstanceTable.ProductIdColumn, data.Id);
                    qb.ConditionBuilder.And(ProductInstanceTable.InstanceIdColumn, data.InstanceId);
                    qb.ConditionBuilder.And(ProductInstanceTable.AccountIdColumn, data.AccountId);
                    /*BoxNo included by settu to update in product instance table on 19-05-2017*/
                    if (data.RackNo != null && data.RackNo.Trim() != "" && data.BoxNo != null && data.BoxNo.Trim() != "")
                    {
                        qb.AddColumn(ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn);
                        qb.Parameters.Add(ProductInstanceTable.RackNoColumn.ColumnName, data.RackNo);
                        qb.Parameters.Add(ProductInstanceTable.BoxNoColumn.ColumnName, data.BoxNo);
                    }
                    else
                    {
                        if (data.RackNo != null && data.RackNo.Trim() != "")
                        {
                            qb.AddColumn(ProductInstanceTable.RackNoColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn);
                            qb.Parameters.Add(ProductInstanceTable.RackNoColumn.ColumnName, data.RackNo);
                        }
                        if (data.BoxNo != null && data.BoxNo.Trim() != "")
                        {
                            qb.AddColumn(ProductInstanceTable.BoxNoColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn);
                            qb.Parameters.Add(ProductInstanceTable.BoxNoColumn.ColumnName, data.BoxNo);
                        }
                    }
                    qb.Parameters.Add(ProductInstanceTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                    qb.Parameters.Add(ProductInstanceTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
                    //added by nandhini for transaction
                    if (data.WriteExecutionQuery)
                    {
                        data.AddExecutionQuery(qb);
                    }
                    else
                    {
                        await QueryExecuter.NonQueryAsyc(qb);
                    }

                }
            }
            else
            {
                var PI = SaveProductInstanceImport(data);
            }

        }
        public async Task UpdateProductBarcode(Product data)
        {
            if (data.Eancode != null && data.Eancode != "")
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                qb.AddColumn(ProductTable.EancodeColumn);
                qb.Parameters.Add(ProductTable.EancodeColumn.ColumnName, data.Eancode);
                //added by nandhini for transaction
                if (data.WriteExecutionQuery)
                {
                    data.AddExecutionQuery(qb);
                }
                else
                {
                    await QueryExecuter.NonQueryAsyc(qb);
                }
            }

        }
        public async Task UpdateProductBarcode(List<VendorPurchaseItem> list)
        {
            foreach (var item in list)
            {
                if (item.ProductStock.Product.Eancode != null && item.ProductStock.Product.Eancode != "")
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Update);
                    qb.ConditionBuilder.And(ProductTable.IdColumn, item.ProductStock.Product.Id);
                    qb.AddColumn(ProductTable.EancodeColumn);
                    qb.Parameters.Add(ProductTable.EancodeColumn.ColumnName, item.ProductStock.Product.Eancode);
                    //added by nandhini for transaction
                    if (item.WriteExecutionQuery)
                    {
                        item.AddExecutionQuery(qb);
                    }
                    else
                    {
                        await QueryExecuter.NonQueryAsyc(qb);
                    }
                }
            }
        }
        public async Task<string> CheckUniqueProductId(string ProductMasterId, string accountId)
        {
            string query = $"SELECT Id from Product where ProductMasterId='{ProductMasterId}' and accountId='{accountId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var GetId = (await SingleValueAsyc(qb)).ToString();
            var ProId = "";
            if (GetId.Count() > 0)
            {
                ProId = GetId;
            }
            return ProId;
        }
        public async Task<List<Product>> InstanceList(string productName, string instanceId)
        {
            var filter = "";
            if (productName != null)
            {
                filter += "and p.Name like '" + productName + "%'";
            }
            //status removed by nandhini 19.7.2017
            var query = $"select top 20 result.Id,result.Name from (Select distinct(p.Id),p.name from Product p join ProductStock ps on p.Id = ps.ProductId where ps.InstanceId = '{instanceId}' {filter} ) as result order by result.Name asc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var productList = new Product
                    {
                        Name = reader["Name"].ToString(),
                        Id = reader["Id"].ToString(),
                    };
                    list.Add(productList);
                }
            }
            return list;
        }
        //Return Purchase List
        public async Task<List<Product>> ReturnPurchaseList(string productName, string instanceId)
        {
            var filter = "";
            if (productName != null)
            {
                filter = "and p.Name like '" + productName + "%'";
            }
            var query = $"select top 20 result.Id,result.Name from (Select distinct(p.Id),p.name from Product p join ProductStock ps on p.Id = ps.ProductId join VendorReturnItem vri on ps.Id = vri.ProductStockId where ps.InstanceId = '{instanceId}' {filter} ) as result order by result.Name asc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var productList = new Product
                    {
                        Name = reader["Name"].ToString(),
                        Id = reader["Id"].ToString(),
                    };
                    list.Add(productList);
                }
            }
            return list;
        }
        //Sales History List
        public async Task<List<Product>> SalesHistoryList(string productName, string instanceId)
        {
            var filter = "";
            if (productName != null)
            {
                filter = "and p.Name like '" + productName + "%'";
            }
            var query = $"select top 20 result.Id,result.Name from ( Select distinct(p.Id),p.name from Product p join ProductStock ps on p.Id = ps.ProductId join SalesItem si on ps.Id = si.ProductStockId where ps.InstanceId = '{instanceId}' {filter}) as result order by result.Name asc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var productList = new Product
                    {
                        Name = reader["Name"].ToString(),
                        Id = reader["Id"].ToString(),
                    };
                    list.Add(productList);
                }
            }
            return list;
        }





        public async Task<List<MasterProduct>> List(string productName, string AccountId, string InstanceId)
        {
            //var conditionquery = "";
            //if (productName.Length <= 2)
            //{
            //     conditionquery = "top 20";
            //}
            //var query = $@"select {conditionquery} * from (select p.Name,p.Id,p.AccountId,p.InstanceId,
            //         p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,p.Schedule,
            //         p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,
            //         p.VAT,p.Price,p.Status,p.RackNo, isNull(sum(ps.stock),0) as Totalstock  from product p left join ProductStock ps on p.id = ps.ProductId and (ps.Status is null or ps.Status = 1) and
            //         ps.AccountId='{AccountId}' and ps.InstanceId = '{InstanceId}'
            //         where p.Name like '" + productName.Replace("'", "''") + "%' and p.AccountId='" + AccountId + "' and (p.Status is null or p.Status = 1) group by p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type, p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,p.RackNo  union select p.Name,p.Id,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,p.Schedule,                     p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy, p.VAT,p.Price,p.Status,p.RackNo, isNull(-1,-1) as Totalstock  from ProductMaster p where p.Name like '" + productName.Replace("'", "''") + "%' and (p.Status is null or p.Status = 1) and p.Name not in (select p.Name from product p left join ProductStock ps on p.id = ps.ProductId and ps.AccountId = '" + AccountId + "' and ps.InstanceId = '" + InstanceId + "' where p.Name like '" + productName.Replace("'", "''") + "%' and p.AccountId = '" + AccountId + "' group by p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type, p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,p.RackNo)) a order by a.Name asc,a.accountId,a.Totalstock desc";
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //var result = await List<MasterProduct>(qb);
            //return result;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("productname", productName);
            parms.Add("instanceid", InstanceId);
            parms.Add("accountid", AccountId);
            //var queryresult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetProductsForOrderTab", parms);
            var queryresult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetNonHiddenProductsList", parms);
            var result = queryresult.Select(x =>
            {
                var mp = new MasterProduct
                {
                    Name = x.Name,
                    Id = x.Id,
                    AccountId = x.AccountId,
                    InstanceId = x.InstanceId,
                    Code = x.Code,
                    Manufacturer = x.Manufacturer,
                    KindName = x.KindName,
                    StrengthName = x.StrengthName,
                    Type = x.Type,
                    Schedule = x.Schedule,
                    Category = x.Category,
                    Packing = x.Packing,
                    VAT = x.VAT,
                    Price = x.Price,
                    Status = x.Status,
                    RackNo = x.RackNo,
                    GenericName = x.GenericName,
                    Eancode = x.Eancode,
                    Totalstock = x.Totalstock
                };
                return mp;
            });
            return result.ToList();
        }

        //Added by Bala for Account wise stock
        public async Task<List<MasterProduct>> AccountWiseList(string productName, string AccountId, string InstanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("productname", productName);
            parms.Add("instanceid", InstanceId);
            parms.Add("accountid", AccountId);
            var queryresult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetProductsAccountWise", parms);
            var result = queryresult.Select(x =>
            {
                var mp = new MasterProduct
                {
                    Name = x.Name,
                    Id = x.Id,
                    AccountId = x.AccountId,
                    InstanceId = x.InstanceId,
                    Code = x.Code,
                    Manufacturer = x.Manufacturer,
                    KindName = x.KindName,
                    StrengthName = x.StrengthName,
                    Type = x.Type,
                    Schedule = x.Schedule,
                    Category = x.Category,
                    Packing = x.Packing,
                    VAT = x.VAT,
                    Price = x.Price,
                    Status = x.Status,
                    RackNo = x.RackNo,
                    GenericName = x.GenericName,
                    Eancode = x.Eancode,
                    Totalstock = x.Totalstock
                };
                return mp;
            });
            return result.ToList();
        }
        // Added By Violet Raj 16-03-2017

        public async Task<List<MasterProduct>> NonHiddenProductList(string productName, string AccountId, string InstanceId)
        {

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("productname", productName);
            parms.Add("instanceid", InstanceId);
            parms.Add("accountid", AccountId);
            var queryresult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetNonHiddenProductsList", parms);
            var result = queryresult.Select(x =>
            {
                var mp = new MasterProduct
                {
                    Name = x.Name,
                    Id = x.Id,
                    AccountId = x.AccountId,
                    InstanceId = x.InstanceId,
                    Code = x.Code,
                    Manufacturer = x.Manufacturer,
                    KindName = x.KindName,
                    StrengthName = x.StrengthName,
                    Type = x.Type,
                    Schedule = x.Schedule,
                    Category = x.Category,
                    Packing = x.Packing,
                    VAT = x.VAT,
                    Price = x.Price,
                    Status = x.Status,
                    RackNo = x.RackNo,
                    BoxNo = x.BoxNo,
                    GenericName = x.GenericName,
                    Eancode = x.Eancode,
                    Totalstock = x.Totalstock,
                    HsnCode = x.HsnCode,
                    Igst = x.Igst,
                    Cgst = x.Cgst,
                    Sgst = x.Sgst,
                    GstTotal = x.GstTotal
                };
                return mp;
            });
            return result.ToList();
        }

        public async Task<List<MasterProduct>> StockProductList(string productName, string AccountId, string InstanceId)
        {
            var conditionquery = "";
            if (productName.Length <= 2)
            {
                conditionquery = "top 20";
            }
            /*Product Instance section added by Poongodi on 22/03/2017*/
            var query = $@"select {conditionquery} * from (select p.Name,p.Id,p.AccountId,p.InstanceId,
                     p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,p.Schedule,
                     p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,
                     p.VAT,p.Price,p.Status,isnull(pi.RackNo,'') as RackNo, isNull(sum(ps.stock),0) as Totalstock  from product p left join ProductStock ps on p.id = ps.ProductId and
                     ps.AccountId='{AccountId}' and ps.InstanceId = '{InstanceId}'
                     Left join productinstance PI on PI.productid = p.id and pi.instanceid = '{InstanceId}' 
                     where p.Name like '" + productName.Replace("'", "''") + "%' and p.AccountId='" + AccountId + "' group by p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type, p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,pi.RackNo  union select p.Name,p.Id,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,p.Schedule,                     p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy, p.VAT,p.Price,p.Status,p.RackNo, isNull(-1,-1) as Totalstock  from ProductMaster p where p.Name like '" + productName.Replace("'", "''") + "%' and p.Name not in (select p.Name from product p left join ProductStock ps on p.id = ps.ProductId and ps.AccountId = '" + AccountId + "' and ps.InstanceId = '" + InstanceId + "' where p.Name like '" + productName.Replace("'", "''") + "%' and p.AccountId = '" + AccountId + "' group by p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type, p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,p.RackNo)) a order by a.Name asc,a.accountId,a.Totalstock desc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<MasterProduct>(qb);
            return result;
        }
        // Newly Added Gavaskar 22-02-2017 Start
        public async Task<List<MasterProduct>> InventoryUpdateProductList(string productName, string AccountId, string InstanceId)
        {
            var conditionquery = "";
            if (productName.Length <= 2)
            {
                conditionquery = "top 20";
            }
            /*Rack no taken from productinstance table*/
            var query = $@"select {conditionquery} * from (select p.Name,p.Id,p.AccountId,p.InstanceId,
                     p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,p.Schedule,
                     p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,
                     p.VAT,p.Price,p.Status,p.RackNo,p.GenericName, isNull(sum(ps.stock),0) as Totalstock  from product p left join ProductStock ps on p.id = ps.ProductId and 
                     ps.AccountId='{AccountId}' and ps.InstanceId = '{InstanceId}'
                     and (ps.[Status] is null or ps.[Status] = 1 )
                     where p.Name like '" + productName.Replace("'", "''") + "%' and p.AccountId='" + AccountId + "' and (p.[Status] is null or p.[Status] = 1) group by p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type, p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,p.RackNo,p.GenericName   union select p.Name,p.Id,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,p.Schedule,                     p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy, p.VAT,p.Price,p.Status,p.RackNo,p.GenericName, isNull(-1,-1) as Totalstock  from ProductMaster p where p.Name like '" + productName.Replace("'", "''") + "%' and (p.[Status] is null or p.[Status] = 1) and p.Name not in (select p.Name from product p inner join ProductStock ps on p.id = ps.ProductId and ps.AccountId = '" + AccountId + "' and ps.InstanceId = '" + InstanceId + "' where p.Name like '" + productName.Replace("'", "''") + "%' and p.AccountId = '" + AccountId + "' group by p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type, p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,p.RackNo,p.GenericName)) a where a.Totalstock not in(0,-1) order by a.Name asc,a.accountId,a.Totalstock desc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<MasterProduct>(qb);
            return result;
        }
        // Newly Added Gavaskar 22-02-2017 End
        public async Task<List<ProductMaster>> GetProductMaster(string productName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                qb.ConditionBuilder.And(ProductMasterTable.NameColumn, productName, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.AddColumn(ProductMasterTable.IdColumn, ProductMasterTable.NameColumn, ProductMasterTable.GenericNameColumn, ProductMasterTable.ManufacturerColumn,
                    ProductMasterTable.ScheduleColumn, ProductMasterTable.PackingColumn, ProductMasterTable.TypeColumn, ProductMasterTable.CategoryColumn, ProductMasterTable.CommodityCodeColumn, ProductMasterTable.VATColumn);
            }
            else
            {
                qb.ConditionBuilder.And(ProductMasterTable.NameColumn, $"{productName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductMasterTable.IdColumn, ProductMasterTable.NameColumn, ProductMasterTable.GenericNameColumn, ProductMasterTable.ManufacturerColumn,
                    ProductMasterTable.ScheduleColumn, ProductMasterTable.PackingColumn, ProductMasterTable.TypeColumn, ProductMasterTable.CategoryColumn, ProductMasterTable.CommodityCodeColumn, ProductMasterTable.VATColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(ProductMasterTable.NameColumn);
            qb.SelectBuilder.SetTop(10);
            var list = new List<ProductMaster>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var product = new ProductMaster
                    {
                        Id = reader[ProductMasterTable.IdColumn.ColumnName].ToString(),
                        Name = reader[ProductMasterTable.NameColumn.ColumnName].ToString(),
                        Manufacturer = reader[ProductMasterTable.ManufacturerColumn.ColumnName].ToString(),
                        GenericName = reader[ProductMasterTable.GenericNameColumn.ColumnName].ToString(),
                        Schedule = reader[ProductMasterTable.ScheduleColumn.ColumnName].ToString(),
                        CommodityCode = reader[ProductMasterTable.CommodityCodeColumn.ColumnName].ToString(),
                        Packing = reader[ProductMasterTable.PackingColumn.ColumnName].ToString(),
                        Category = reader[ProductMasterTable.CategoryColumn.ColumnName].ToString(),
                        Type = reader[ProductMasterTable.TypeColumn.ColumnName].ToString(),
                        VAT = reader[ProductMasterTable.VATColumn.ColumnName] as decimal? ?? 0
                    };
                    list.Add(product);
                }
            }
            var genGroupBy = list.GroupBy(l => l.Name).Select(group => group.First());
            return genGroupBy.ToList();
        }

        //Added by Sarubala on 02-01-19 - start
        public async Task<List<Product>> GetProductByName(string productName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, productName, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn,
                    ProductTable.ScheduleColumn, ProductTable.PackingColumn, ProductTable.TypeColumn, ProductTable.CategoryColumn, ProductTable.CommodityCodeColumn, ProductTable.VATColumn);
            }
            else
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, $"{productName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn,ProductTable.ScheduleColumn, ProductTable.PackingColumn, ProductTable.TypeColumn, ProductTable.CategoryColumn, ProductTable.CommodityCodeColumn, ProductTable.VATColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(ProductTable.NameColumn);
            qb.SelectBuilder.SetTop(10);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var product = new Product
                    {
                        Id = reader[ProductTable.IdColumn.ColumnName].ToString(),
                        Name = reader[ProductTable.NameColumn.ColumnName].ToString(),
                        Manufacturer = reader[ProductTable.ManufacturerColumn.ColumnName].ToString(),
                        GenericName = reader[ProductTable.GenericNameColumn.ColumnName].ToString(),
                        Schedule = reader[ProductTable.ScheduleColumn.ColumnName].ToString(),
                        CommodityCode = reader[ProductTable.CommodityCodeColumn.ColumnName].ToString(),
                        Packing = reader[ProductTable.PackingColumn.ColumnName].ToString(),
                        Category = reader[ProductTable.CategoryColumn.ColumnName].ToString(),
                        Type = reader[ProductTable.TypeColumn.ColumnName].ToString(),
                        VAT = reader[ProductTable.VATColumn.ColumnName] as decimal? ?? 0
                    };
                    list.Add(product);
                }
            }
            var genGroupBy = list.GroupBy(l => l.Name).Select(group => group.First());
            return genGroupBy.ToList();
        }
        //Added by Sarubala on 02-01-19 - end

        public async Task<List<MasterProduct>> LocalProductList(string productName, string AccountId, string InstanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("ProdName", productName);
            parms.Add("InstanceId", InstanceId);
            parms.Add("AccountId", AccountId);
            var queryresult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetLocalProductList", parms);
            var result = queryresult.Select(x =>
            {
                var mp = new MasterProduct
                {
                    Name = x.Name,
                    Id = x.Id,
                    AccountId = x.AccountId,
                    InstanceId = x.InstanceId,
                    Code = x.Code,
                    Manufacturer = x.Manufacturer,
                    KindName = x.KindName,
                    StrengthName = x.StrengthName,
                    Type = x.Type,
                    Schedule = x.Schedule,
                    Category = x.Category,
                    Packing = x.Packing,
                    VAT = x.VAT,
                    Price = x.Price,
                    Status = x.Status,
                    RackNo = x.RackNo,
                    Totalstock = x.Totalstock
                };
                return mp;
            });
            return result.ToList();
        }
        //by san
        public async Task<List<Product>> GetproductList(string productName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, productName, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn);
            }
            else
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, $"{productName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn);
            }
            qb.SelectBuilder.SetTop(20).SortBy(ProductTable.NameColumn);
            qb.SelectBuilder.MakeDistinct = true;
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var product = new Product
                    {
                        Name = reader[ProductTable.NameColumn.ColumnName].ToString(),
                    };
                    list.Add(product);
                }
            }
            var genGroupBy = list.GroupBy(l => l.Name).Select(group => group.First());
            return genGroupBy.ToList();
        }
        public async Task<List<Product>> GetGenericList(string genericName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);

            if (_configHelper.AppConfig.IsSqlServer)
            {
                qb.ConditionBuilder.And(ProductTable.GenericNameColumn, genericName, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.GenericNameColumn);
            }
            else
            {
                qb.ConditionBuilder.And(ProductTable.GenericNameColumn, $"{genericName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.GenericNameColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SetTop(10);
            qb.SelectBuilder.SortBy(ProductTable.GenericNameColumn);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var generic = new Product
                    {
                        GenericName = reader[ProductTable.GenericNameColumn.ColumnName].ToString(),
                    };
                    list.Add(generic);
                }
            }
            var genGroupBy = list.GroupBy(l => l.GenericName).Select(group => group.First());
            return genGroupBy.ToList();
        }

        public async Task<List<ProductStock>> GetGenericSearchList(string genericName)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);

            //qb.ConditionBuilder.And(ProductTable.GenericNameColumn, genericName, CriteriaEquation.Like, CriteriaLike.Begin);
            //qb.AddColumn(ProductTable.IdColumn, ProductTable.GenericNameColumn);
            //qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);
            //qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, user.AccountId());
            //qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, user.InstanceId());
            //qb.ConditionBuilder.And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Greater);
            ////qb.ConditionBuilder.And(ProductStockTable.StockColumn, 0, CriteriaEquation.Greater);

            //qb.SelectBuilder.MakeDistinct = true;
            //qb.SelectBuilder.SetTop(10);
            //qb.SelectBuilder.SortBy(ProductTable.GenericNameColumn);
            //var list = new List<Product>();
            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var generic = new Product
            //        {
            //            GenericName = reader[ProductTable.GenericNameColumn.ColumnName].ToString(),
            //        };
            //        list.Add(generic);
            //    }
            //}
            //var genGroupBy = list.GroupBy(l => l.GenericName).Select(group => group.First());
            //return genGroupBy.ToList();

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("GenericName", genericName);
            parms.Add("AccountId", user.AccountId());
            parms.Add("InstanceId", user.InstanceId());
            parms.Add("ExpDate", CustomDateTime.IST.Date.ToFormat());
            //if (user.AccountId() == "2d5abd54-89a9-4b31-aa76-8ec70b9125dd")
            //For Mediplus pharmacy
            if (user.AccountId() == "67067991-0e68-4779-825e-8e1d724cd68b")
            {
                parms.Add("IncludeZeroStock", true);
            }
            else
            {
                parms.Add("IncludeZeroStock", false);
            }

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStock_Generic", parms);

            var ps = result.Select(x =>
            {
                var productstock = new ProductStock
                {
                    Stock = x.Stock,
                    Product = {
                        Name =x.GenericName,
                        GenericName = x.GenericName
                    }
                };
                return productstock;
            });
            return ps.ToList();


        }
        //added by nandhini for sales return list product search
        public async Task<List<Product>> SalesReturnProductList(string productName, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("ProductName", productName);
            parms.Add("InstanceId", instanceId);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_SalesReturnProductList", parms);

            var pl = result.Select(x =>

            {
                var productList = new Product
                {
                    Name = x.Name,
                    Id = x.Id
                };
                return productList;
            });

            return pl.ToList();

        }
        //end

        public async Task<List<Product>> GetManufacturerList(string manufacturercName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                qb.ConditionBuilder.And(ProductTable.ManufacturerColumn, manufacturercName, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.ManufacturerColumn);
            }
            else
            {
                qb.ConditionBuilder.And(ProductTable.ManufacturerColumn, $"{manufacturercName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.ManufacturerColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SetTop(10);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var manufacturerList = new Product
                    {
                        Manufacturer = reader[ProductTable.ManufacturerColumn.ColumnName].ToString(),
                    };
                    list.Add(manufacturerList);
                }
            }
            var manufacturerGroupBy = list.GroupBy(l => l.Manufacturer).Select(group => group.First());
            return manufacturerGroupBy.ToList();
        }
        public async Task<List<Product>> GetScheduledCategoryList(string scheduledCatName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                if (!string.IsNullOrEmpty(scheduledCatName) && scheduledCatName != "null" && scheduledCatName != "undefined")
                {
                    qb.ConditionBuilder.And(ProductTable.ScheduleColumn, scheduledCatName, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.ScheduleColumn);
                }
                else
                {
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.ScheduleColumn);
                }
            }
            else
                qb.ConditionBuilder.And(ProductTable.ScheduleColumn, $"{scheduledCatName}%", CriteriaEquation.Like);
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SetTop(10);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var scheduleList = new Product
                    {
                        Schedule = reader[ProductTable.ScheduleColumn.ColumnName].ToString(),
                    };
                    list.Add(scheduleList);
                }
            }
            var scheduleGroupBy = list.GroupBy(l => l.Schedule).Select(group => group.First());
            return scheduleGroupBy.ToList();
        }
        public async Task<List<Product>> GetTypeList(string typeName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                if (!string.IsNullOrEmpty(typeName) && typeName != "null" && typeName != "undefined")
                {
                    qb.ConditionBuilder.And(ProductTable.TypeColumn, typeName, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.TypeColumn);
                }
                else
                {
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.TypeColumn);
                }
            }
            else
            {
                qb.ConditionBuilder.And(ProductTable.TypeColumn, $"{typeName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.TypeColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SetTop(10);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var typeList = new Product
                    {
                        Type = reader[ProductTable.TypeColumn.ColumnName].ToString(),
                    };
                    list.Add(typeList);
                }
            }
            var typeGroupBy = list.GroupBy(l => l.Type).Select(group => group.First());
            return typeGroupBy.ToList();
        }

        //SubCategoryList
        public async Task<List<Product>> GetCategoryList(string categoryName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                if (!string.IsNullOrEmpty(categoryName) && categoryName != "null" && categoryName != "undefined")
                {
                    qb.ConditionBuilder.And(ProductTable.CategoryColumn, categoryName, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.CategoryColumn);
                }
                else
                {
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.CategoryColumn);
                }
            }
            else
            {
                qb.ConditionBuilder.And(ProductTable.CategoryColumn, $"{categoryName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.CategoryColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SetTop(10);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var categoryList = new Product
                    {
                        Category = reader[ProductTable.CategoryColumn.ColumnName].ToString(),
                    };
                    list.Add(categoryList);
                }
            }
            var categoryGroupBy = list.GroupBy(l => l.Category).Select(group => group.First());
            return categoryGroupBy.ToList();
        }


        //Added by arun for subcategory
        public async Task<List<BaseDomainValues>> SubCategoryList(string categoryName)
        {
            var query = $@" select * from DomainValues where DomainId= 3 ";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<BaseDomainValues>(qb);
            return result;
        }



        //Ended by arun for sub category


        public async Task<List<Product>> GetCommodityCodeList(string commodityName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                if (!string.IsNullOrEmpty(commodityName) && commodityName != "null" && commodityName != "undefined")
                {
                    qb.ConditionBuilder.And(ProductTable.CommodityCodeColumn, commodityName, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.CommodityCodeColumn);
                }
                else
                {
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.CommodityCodeColumn);
                }
            }
            else
            {
                qb.ConditionBuilder.And(ProductTable.CommodityCodeColumn, $"{commodityName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.CommodityCodeColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SetTop(10);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var commodityList = new Product
                    {
                        CommodityCode = reader[ProductTable.CommodityCodeColumn.ColumnName].ToString(),
                    };
                    list.Add(commodityList);
                }
            }
            var commodityGroupBy = list.GroupBy(l => l.CommodityCode).Select(group => group.First());
            return commodityGroupBy.ToList();
        }
        public async Task<List<Product>> GetKindNameList(string kindName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                if (!string.IsNullOrEmpty(kindName) && kindName != "null" && kindName != "undefined")
                {
                    qb.ConditionBuilder.And(ProductTable.KindNameColumn, kindName, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.KindNameColumn);
                }
                else
                {
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.KindNameColumn);
                }
            }
            else
            {
                qb.ConditionBuilder.And(ProductTable.KindNameColumn, $"{kindName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductTable.IdColumn, ProductTable.KindNameColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var kindList = new Product
                    {
                        KindName = reader[ProductTable.KindNameColumn.ColumnName].ToString(),
                    };
                    list.Add(kindList);
                }
            }
            var kindGroupBy = list.GroupBy(l => l.KindName).Select(group => group.First());
            return kindGroupBy.ToList();
        }
        public async Task<Product> GetProductById(string productId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductTable.IdColumn);
            qb.Parameters.Add(ProductTable.IdColumn.ColumnName, productId);
            qb.SelectBuilder.MakeDistinct = true;
            return (await List<Product>(qb)).FirstOrDefault();
        }
		public async Task<List<LoyaltyProductPoints>> GetLoyaltyPointProductList(string LoyaltyId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(LoyaltyProductPointsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(LoyaltyProductPointsTable.LoyaltyIdColumn);
            qb.Parameters.Add(LoyaltyProductPointsTable.LoyaltyIdColumn.ColumnName, LoyaltyId);
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(LoyaltyProductPointsTable.KindNameColumn);
            return (await List<LoyaltyProductPoints>(qb));
        }

        //Added by Bala for Account wise stock
        public async Task<List<VendorPurchaseItem>> getPreviousPurchaseAccountWise(string accountId, string instanceId, string name, bool? bLowPrice = false)
        {
            string sortfilter = "vpi.CreatedAt desc";
            if (bLowPrice == true)
            {
                sortfilter = "ps.PurchasePrice asc";
            }

            /*Prefix Added by Poongodi on 15/06/2017*/
            name = name.Replace("'", "''");
            string query = $@"select top 5 vpi.VendorPurchaseId,
                                           vpi.Quantity,
                                           vpi.ProductStockId,
										   vpi.FreeQty,
										   vpi.Discount,
										   vpi.PackageQty,
                                           vpi.PackageSize,
										   vpi.PackagePurchasePrice,
										   vpi.PackageSellingPrice,
                                           vpi.PackageMRP,
                                           vpi.CreatedAt,
                                           ps.ProductId,
                                           ps.VendorId,
                                           ps.BatchNo,
                                           ps.VAT,
                                           ps.PurchasePrice,										   
                                           ps.cst,
                                           ps.ExpireDate,
                                           p.Name as productname,
										   p.Manufacturer,
                                           v.Name as vendorname,
                                           v.Code as vendorcode,
                                           v.Mobile as vendormobile,
										   vp.GoodsRcvNo,
                                            isnull(vp.prefix,'') + vp.BillSeries [BillSeries]
                                               from VendorPurchaseItem as vpi inner join ProductStock as ps on vpi.ProductStockId = ps.Id  
                                                          inner join Product as p on ps.ProductId = p.Id 
		                                                  inner join VendorPurchase as vp on vpi.VendorPurchaseId = vp.id
                                                          left join Vendor as v on vp.VendorId = v.Id
                                                             where vpi.AccountId='{accountId}' and vpi.InstanceId = '{instanceId}' and p.Name = '{name}' and (vp.cancelstatus is null or vp.cancelstatus = 0) and (ps.Status is null or ps.Status = 1) order by " + sortfilter;
            var list = new List<VendorPurchaseItem>();
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var VendorPurchaseItem = new VendorPurchaseItem
                    {
                        BuyPurchase = {
                            GoodsRcvNo = reader[VendorPurchaseTable.GoodsRcvNoColumn.ColumnName].ToString(),
                            BillSeries = reader[VendorPurchaseTable.BillSeriesColumn.ColumnName].ToString(),
                        },
                        VendorPurchaseId = reader[VendorPurchaseItemTable.VendorPurchaseIdColumn.ColumnName].ToString(),
                        Quantity = reader[VendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0,
                        PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        PackagePurchasePrice = reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        PackageMRP = reader[VendorPurchaseItemTable.PackageMRPColumn.ColumnName] as decimal? ?? 0,
                        CreatedAt = reader[VendorPurchaseItemTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        ProductStock = {
                            ProductId = reader[ProductStockTable.ProductIdColumn.ColumnName].ToString(),
                            VendorId = reader[ProductStockTable.VendorIdColumn.ColumnName].ToString(),
                            BatchNo =  reader[ProductStockTable.BatchNoColumn.ColumnName].ToString(),
                            VAT = reader[ProductStockTable.VATColumn.ColumnName] as decimal? ?? 0,
                            PurchasePrice = reader[ProductStockTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                            CST = reader[ProductStockTable.CSTColumn.ColumnName] as decimal? ?? 0,
                            ExpireDate = reader[ProductStockTable.ExpireDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                            Product = {
                                Name = reader["productname"].ToString(),
                                Manufacturer = reader[ProductTable.ManufacturerColumn.ColumnName].ToString()
                            },
                            Vendor = {
                                Name = reader["vendorname"].ToString(),
                                Code=  reader["vendorcode"].ToString(),
                                Mobile = reader["vendormobile"].ToString()
                            }
                        }
                    };
                    list.Add(VendorPurchaseItem);
                }
                return list;
            }
        }

        public async Task<Product> GetProductByProductStockId(string productStockIdId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn, productStockIdId);
            qb.SelectBuilder.MakeDistinct = true;
            return (await List<Product>(qb)).First();
        }
        public async Task<int> Count(string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(instanceId, from, to, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        private static QueryBuilderBase SqlQueryBuilder(string instanceId, DateTime from, DateTime to, QueryBuilderBase qb)
        {
            qb.ConditionBuilder.And(ProductTable.UpdatedAtColumn, CriteriaEquation.Between);
            qb.Parameters.Add("FilterFromDate", from);
            qb.Parameters.Add("FilterToDate", to);
            return qb;
        }
        public async Task<int> Count(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<int> TotalProductCount(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Count);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        private static QueryBuilderBase SqlQueryBuilder(Product data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
            }
            if (!string.IsNullOrEmpty(data.Manufacturer))
            {
                qb.ConditionBuilder.And(ProductTable.ManufacturerColumn, data.Manufacturer);
            }
            if (data.AccountId == "18204879-99ff-4efd-b076-f85b4a0da0a3")
            {
                if (!string.IsNullOrEmpty(data.AccountId))
                {
                    qb.ConditionBuilder.And(ProductTable.AccountIdColumn, data.AccountId);
                }
            }
            return qb;
        }
        private static QueryBuilderBase SqlQueryBuilder1(DrugList data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
            }
            if (!string.IsNullOrEmpty(data.Manufacturer))
            {
                qb.ConditionBuilder.And(ProductTable.ManufacturerColumn, data.Manufacturer);
            }
            if (data.AccountId == "18204879-99ff-4efd-b076-f85b4a0da0a3")
            {
                if (!string.IsNullOrEmpty(data.AccountId))
                {
                    qb.ConditionBuilder.And(ProductTable.AccountIdColumn, data.AccountId);
                }
            }
            return qb;
        }
        // Modified Gavaskar 20-02-2017 
        public Task<List<Product>> List(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.AddColumn(ProductTable.NameColumn, ProductTable.UpdatedAtColumn, ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn, ProductTable.TypeColumn, ProductTable.CategoryColumn,
                ProductTable.PackageSizeColumn, ProductTable.VATColumn, ProductTable.PriceColumn, ProductTable.KindNameColumn, ProductTable.ScheduleColumn, ProductTable.StatusColumn,
                ProductTable.CommodityCodeColumn, ProductTable.IdColumn, ProductTable.InstanceIdColumn, ProductInstanceTable.ReOrderLevelColumn, ProductInstanceTable.ReOrderQtyColumn, ProductTable.DiscountColumn);
            /*Reorder level and qty taken from ProductInstanceTable by Poongodi on 15/03/2017*/
            if (data.AccountId == "18204879-99ff-4efd-b076-f85b4a0da0a3")
            {
                qb.JoinBuilder.LeftJoin(InstanceTable.Table, InstanceTable.IdColumn,
                    ProductTable.InstanceIdColumn);
                qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.IdColumn, InstanceTable.NameColumn);
            }
            qb.JoinBuilder.LeftJoin(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductTable.IdColumn); // by Poongodi on 15/03/2017
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortBy(ProductTable.NameColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            return List<Product>(qb);
        }
        public async Task<IEnumerable<Product>> ProductList(Product data)
        {
            var filter = "";
            if (data.Name != null)
            {
                filter = "and p.Name like '" + data.Name + "%'";
            }
            var query =
                $"Select Distinct(p.name),p.*,i.name as Instance from Product p  left join ProductStock ps on p.Id = ps.ProductId left join Instance i on i.Id = p.InstanceId where ps.AccountId = '{data.AccountId}' {filter} order by p.Name";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            var list = new List<Product>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var productList = new Product
                    {
                        Instance =
                        {
                            Name = reader["Instance"].ToString(),
                        },
                    };
                    productList.Id = reader["Id"].ToString();
                    productList.Name = reader["Name"].ToString();
                    productList.Manufacturer = reader["Manufacturer"].ToString();
                    productList.GenericName = reader["GenericName"].ToString();
                    productList.Code = reader["Code"].ToString();
                    productList.StrengthName = reader["StrengthName"].ToString();
                    productList.Type = reader["Type"].ToString();
                    productList.Schedule = reader["Schedule"].ToString();
                    productList.Category = reader["Category"].ToString();
                    productList.CommodityCode = reader["CommodityCode"].ToString();
                    productList.PackageSize = reader["PackageSize"] as int? ?? 0;
                    productList.VAT = reader["VAT"] as decimal? ?? 0;
                    productList.Price = reader["Price"] as decimal? ?? 0;
                    productList.Status = reader["Status"] as byte? ?? 0;
                    productList.RackNo = reader["RackNo"].ToString();
                    list.Add(productList);
                }
            }
            return list;
        }
        public Task<List<Product>> Listdata(string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.AddColumn(ProductTable.NameColumn, ProductTable.UpdatedAtColumn, ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn, ProductTable.TypeColumn, ProductTable.CategoryColumn,
                ProductTable.PackageSizeColumn, ProductTable.VATColumn, ProductTable.PriceColumn, ProductTable.KindNameColumn, ProductTable.ScheduleColumn, ProductTable.StatusColumn,
                ProductTable.CommodityCodeColumn, ProductTable.IdColumn);
            qb = SqlQueryBuilder(instanceId, from, to, qb);
            qb.SelectBuilder.SortBy(ProductTable.NameColumn);
            return List<Product>(qb);
        }
        public Task<List<Product>> ExportList(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortBy(ProductTable.NameColumn);
            return List<Product>(qb);
        }
        public async Task<bool> CheckUniqueProduct(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name).And(ProductTable.AccountIdColumn, data.AccountId);
            if (!string.IsNullOrEmpty(data.Id))
            {
                qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id, CriteriaEquation.NotEqual);
            }
            int count = Convert.ToInt32(await SingleValueAsyc(qb));
            return count == 0;
        }
        public async Task<Product> GetByProductId(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name);
            }
            var result = (await List<Product>(qb));
            if (result.Count > 0)
                return result.First();
            else
                return data;
        }
        //By San
        public async Task<Product> GetImportProductById(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name).And(ProductTable.AccountIdColumn, data.AccountId);
            }
            var result = (await List<Product>(qb));
            if (result.Count > 0)
                return result.First();
            else
                return data;
        }
        public async Task<Product> GetPurchaseImportProductById(Product data)
        {
            string errorMessage = "";
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data.Name) &&!string.IsNullOrWhiteSpace(data.Name))
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name).And(ProductTable.AccountIdColumn, data.AccountId);
            }
            else
            {
                errorMessage = "Product Name is Mandatory";
                throw new Exception(errorMessage);
            }
            
            var result = (await List<Product>(qb));
            if (result.Count > 0)
                return result.First();
            else
            {
                var qbPm = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Select);
                if (!string.IsNullOrEmpty(data.Name))
                {
                    qbPm.ConditionBuilder.And(ProductMasterTable.NameColumn, data.Name);
                }
                var getMasterDetails = (await List<ProductMaster>(qbPm));
                if (getMasterDetails.Count > 0)
                {
                    data.Name = getMasterDetails.First().Name;
                    ProductMaster pm = new ProductMaster();
                    //var count = await TotalProductCount(data);
                    data.Name = data.Name.ToUpper();
                    //var getChar = data.Name.Substring(0, 2);
                    //data.Code = (getChar.ToUpper() + (count + 1)).ToString();
                    pm.Name = data.Name;
                    var getProductMasterId = await GetProductMasterById(pm);
                    if (!string.IsNullOrEmpty(getProductMasterId.Id))
                        data.ProductMasterID = getProductMasterId.Id;
                    var obj = await Save(data);
                    return obj;
                }
                else
                {
                    return data;
                }
            }
        }
        public async Task<ProductMaster> GetProductMasterById(ProductMaster data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(ProductMasterTable.NameColumn, data.Name);
            }
            var result = (await List<ProductMaster>(qb));
            if (result.Count > 0)
                return result.First();
            else
                return data;
        }
        public async Task<Product> GetExistProductMasterById(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data.ProductMasterID))
            {
                qb.ConditionBuilder.And(ProductTable.ProductMasterIDColumn, data.ProductMasterID);
                qb.ConditionBuilder.And(ProductTable.AccountIdColumn, data.AccountId);
            }
            var result = (await List<Product>(qb));
            if (result.Count > 0)
                return result.FirstOrDefault();
            else
                return data;
        }
        public async Task<ProductMaster> GetProductByMasterId(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(id))
            {
                qb.ConditionBuilder.And(ProductMasterTable.IdColumn, id);
            }
            var result = (await List<ProductMaster>(qb));
            return result.FirstOrDefault();
        }
        //public async Task<int> MasterCount(string accountid, string instanceid, Product data)
        //{
        //    var namequery = "";
        //    var manufacturerquery = "";
        //    var Accountid = "";
        //    var Instanceid = "";
        //    if (!string.IsNullOrEmpty(data.Name))
        //    {
        //        namequery = $" and Product.Name='{data.Name}'";
        //    }
        //    if (!string.IsNullOrEmpty(data.Manufacturer))
        //    {
        //        manufacturerquery = $" and Product.Name='{data.Manufacturer}'";
        //    }
        //    if (!string.IsNullOrEmpty(accountid))
        //    {
        //        Accountid = accountid;
        //    }
        //    if (!string.IsNullOrEmpty(instanceid))
        //    {
        //        Instanceid = instanceid;
        //    }
        //    var query = $@"SELECT count(*) from ( 
        //                                  SELECT DISTINCT
        //                                  Product.Name,Product.UpdatedAt,Product.GenericName,Product.Manufacturer,
        //                                  Product.Type,Product.Category,Product.PackageSize,Product.VAT,Product.Price,
        //                                  Product.KindName,Product.Schedule,Product.Status,Product.CommodityCode,
        //                                  Product.Id,Product.InstanceId 
        //                                  FROM Product 
        //                                  INNER JOIN ProductStock ON ProductStock.ProductId = Product.Id 
        //                                  WHERE ProductStock.AccountId  =  '{Accountid}'AND ProductStock.InstanceId  =  '{Instanceid}' 
        //                                  {namequery} {manufacturerquery}
        //                                  GROUP BY 
        //                                  Product.Name,Product.UpdatedAt,Product.GenericName,Product.Manufacturer,
        //                                  Product.Type,Product.Category,Product.PackageSize,Product.VAT,Product.Price,
        //                                  Product.KindName,Product.Schedule,Product.Status,Product.CommodityCode,
        //                                  Product.Id,Product.InstanceId 
        //                                ) a";
        //    var qb = QueryBuilderFactory.GetQueryBuilder(query);
        //    return Convert.ToInt32(await SingleValueAsyc(qb));
        //}

        public async Task<int> MasterCount(string accountid, string instanceid, Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Count);
            //qb.JoinBuilder.LeftJoin(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductTable.IdColumn);
            //Instance join changed by Poongodi on 04/12/2017
            qb.JoinBuilder.LeftJoins2(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductTable.IdColumn, ProductInstanceTable.InstanceIdColumn, instanceid);
            //Commented By San 30-01-2017
            //qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.ProductIdColumn,ProductTable.IdColumn);
            qb = SqlQueryBuilder2(accountid, instanceid, data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        //Modified Gavaskar 20-02-2017
        public Task<List<Product>> MasterList(string accountid, string instanceid, Product data)
        {
            try
            {
                /*Ext_RefIdColumn added by Poongodi on 04/08/2017*/
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
                qb.AddColumn(ProductTable.NameColumn, ProductTable.UpdatedAtColumn, ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn,
                    ProductTable.TypeColumn, ProductTable.CategoryColumn, ProductTable.SubcategoryColumn, ProductTable.PackageSizeColumn, ProductTable.VATColumn, ProductTable.PriceColumn, ProductTable.KindNameColumn,
                    ProductTable.ScheduleColumn, ProductTable.StatusColumn, ProductTable.CommodityCodeColumn, ProductTable.IdColumn, ProductTable.InstanceIdColumn,
                    ProductTable.HsnCodeColumn, ProductTable.IgstColumn, ProductTable.CgstColumn, ProductTable.SgstColumn, ProductTable.GstTotalColumn,
                    ProductInstanceTable.ReOrderQtyColumn, ProductInstanceTable.ReOrderLevelColumn, ProductTable.DiscountColumn, ProductTable.IsHiddenColumn, ProductTable.HsnCodeColumn, ProductTable.GstTotalColumn,

                    //added by nandhini for product code 31.8.17
                    ProductTable.CodeColumn,
                     ProductTable.Ext_RefIdColumn);
                //Commented By San 30-01-2017
                // qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);

                qb.JoinBuilder.LeftJoins2(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductTable.IdColumn, ProductInstanceTable.InstanceIdColumn, instanceid);

                qb = SqlQueryBuilder2(accountid, instanceid, data, qb);
                qb.SelectBuilder.SortBy(ProductTable.NameColumn);
                qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);

                return List<Product>(qb);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //Newly created by Manivannan on 31-Oct-2017
        public async Task<int> GstNullMasterCount(string AccountId, string InstanceId, Product data)
        {
            var namepart = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                namepart = $"And Product.Name like '{data.Name}%'";
            }

            var query = $"select count(distinct Product.id) from Product Left Join ProductStock on Product.id = ProductStock.productId and ProductStock.GstTotal is not null where Product.accountId = @AccountId {namepart} and Product.GstTotal IS NULL and ProductStock.id is null";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            qb.Parameters.Add(ProductTable.AccountIdColumn.ColumnName, AccountId);

            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        //Newly created by Manivannan on 31-Oct-2017
        public async Task<List<Product>> GstNullMasterList(string AccountId, string InstanceId, Product data)
        {
            var namepart = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                namepart = $"And Product.Name like '{data.Name}%'";
            }

            var query = $"Select Distinct Product.* from Product Left Join ProductStock on Product.id = ProductStock.productId and ProductStock.GstTotal is not null where Product.accountId = @AccountId {namepart} And Product.GstTotal IS NULL and ProductStock.id is null ";
            query += " Order by Product.Name offset " + ((data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize) + " rows FETCH next " + data.Page.PageSize + " rows only"; // Pagination with 30 records

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            qb.Parameters.Add(ProductTable.AccountIdColumn.ColumnName, AccountId);
            qb.Parameters.Add(ProductTable.InstanceIdColumn.ColumnName, InstanceId);

            return (await List<Product>(qb));

        }

        //public Task<List<Product>> MasterList(string accountid, string instanceid, Product data)
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
        //    qb.AddColumn(ProductTable.NameColumn, ProductTable.UpdatedAtColumn, ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn, ProductTable.TypeColumn, ProductTable.CategoryColumn, ProductTable.PackageSizeColumn, ProductTable.VATColumn, ProductTable.PriceColumn, ProductTable.KindNameColumn, ProductTable.ScheduleColumn, ProductTable.StatusColumn, ProductTable.CommodityCodeColumn, ProductTable.IdColumn, ProductTable.InstanceIdColumn);
        //    qb.SelectBuilder.GroupBy(ProductTable.NameColumn, ProductTable.UpdatedAtColumn, ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn, ProductTable.TypeColumn, ProductTable.CategoryColumn, ProductTable.PackageSizeColumn, ProductTable.VATColumn, ProductTable.PriceColumn, ProductTable.KindNameColumn, ProductTable.ScheduleColumn, ProductTable.StatusColumn, ProductTable.CommodityCodeColumn, ProductTable.IdColumn, ProductTable.InstanceIdColumn);
        //    qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);
        //    qb = SqlQueryBuilder2(accountid, instanceid, data, qb);
        //    qb.SelectBuilder.SortBy(ProductTable.NameColumn);
        //    qb.SelectBuilder.MakeDistinct = true;
        //    qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
        //    return List<Product>(qb);
        //}
        public Task<List<Product>> EntireMasterList(string accountid, string instanceid, Product data)
        {
            /*Productstock table join removed by Poongodi on 27/07/2017*/
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.AddColumn(ProductTable.NameColumn, ProductTable.UpdatedAtColumn, ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn, ProductTable.TypeColumn, ProductTable.CategoryColumn, ProductTable.PackageSizeColumn, ProductTable.VATColumn, ProductTable.PriceColumn, ProductTable.KindNameColumn, ProductTable.ScheduleColumn, ProductTable.StatusColumn, ProductTable.CommodityCodeColumn, ProductTable.IdColumn, ProductTable.InstanceIdColumn);
            //qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);
            // qb = SqlQueryBuilder2(accountid, instanceid, data, qb);
            if (!string.IsNullOrEmpty(accountid))
            {
                qb.ConditionBuilder.And(ProductTable.AccountIdColumn, accountid);
            }
            //if (!string.IsNullOrEmpty(instanceid))
            //{
            //    qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceid);
            //}
            qb.SelectBuilder.SortBy(ProductTable.NameColumn);
            return List<Product>(qb);
        }
        private static QueryBuilderBase SqlQueryBuilder2(string accountid, string instanceid, Product data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
            }
            if (!string.IsNullOrEmpty(data.Manufacturer))
            {
                qb.ConditionBuilder.And(ProductTable.ManufacturerColumn, data.Manufacturer);
            }

            if (data.IsGstNullSearch)
            {
                var gstTotalCondition = new CustomCriteria(ProductTable.GstTotalColumn, CriteriaCondition.IsNull);
                string condition1 = "0";
                var cc1 = new CriteriaColumn(ProductTable.GstTotalColumn, condition1, CriteriaEquation.Equal);
                var col1 = new CustomCriteria(cc1, gstTotalCondition, CriteriaCondition.Or);
                qb.ConditionBuilder.And(col1);
            }

            if (!string.IsNullOrEmpty(accountid))
            {
                qb.ConditionBuilder.And(ProductTable.AccountIdColumn, accountid);
                //qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, accountid);
            }

            //Added by Poongodi on 22/03/2017
            //if (!string.IsNullOrEmpty(instanceid))
            //{
            //    qb.ConditionBuilder.And(ProductInstanceTable.InstanceIdColumn, instanceid);
            //}
            return qb;
        }
        public async Task<bool> ProductExist(string productId, string accountid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.AddColumn(ProductTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(ProductTable.IdColumn, productId);
            qb.ConditionBuilder.And(ProductTable.AccountIdColumn, accountid);
            var result = await List<Product>(qb);
            if (result.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<IEnumerable<ProductMaster>> HcueProductMasterList(ProductMaster data)
        {
            var filter = "";
            var query = "";
            if (data.Name != null)
            {
                filter = " and Name like '" + data.Name + "%'";
            }

            if (data.Code == "completed")
                query = $"Select * from ProductMaster Where GenericName is Not Null {filter} order by Name";
            else if (data.Code == "all")
                query = $"Select * from ProductMaster Where (GenericName is Not Null Or GenericName is Null) {filter} order by Name";
            else
                query = $"Select * from ProductMaster Where GenericName is Null {filter} order by Name";


            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            var list = new List<ProductMaster>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var productList = new ProductMaster
                    {

                    };
                    productList.Id = reader["Id"].ToString();
                    productList.Name = reader["Name"].ToString();
                    productList.Manufacturer = reader["Manufacturer"].ToString();
                    productList.GenericName = reader["GenericName"].ToString();
                    productList.Schedule = reader["Schedule"].ToString();
                    list.Add(productList);
                }
            }
            return list;
        }

        public async Task<ProductMaster> UpdateHcueProductMaster(ProductMaster data)
        {
            string generic = data.GenericName;
            generic = generic.Replace("(", " ").Replace(")", " ").Replace(",", "+");
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(ProductMasterTable.IdColumn, data.Id);
            qb.AddColumn(ProductMasterTable.GenericNameColumn, ProductMasterTable.ScheduleColumn, ProductMasterTable.UpdatedAtColumn, ProductMasterTable.UpdatedByColumn);
            qb.Parameters.Add(ProductMasterTable.GenericNameColumn.ColumnName, generic.ToUpper());
            qb.Parameters.Add(ProductMasterTable.ScheduleColumn.ColumnName, data.Schedule.ToUpper());
            qb.Parameters.Add(ProductMasterTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
            qb.Parameters.Add(ProductMasterTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            var tt = await QueryExecuter.NonQueryAsyc2(qb);
            return data;

        }

        //Newly added for GST ProductMaster update while updating Account Product table
        public async Task<ProductMaster> UpdateHcueProductMasterGST(ProductMaster data, bool isHsn, bool isIgst, bool isCgst, bool isSgst, bool isGstTotal)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(ProductMasterTable.IdColumn, data.Id);


            List<IDbColumn> sColumns = new List<IDbColumn>();
            //qb.AddColumn(ProductMasterTable.UpdatedAtColumn, ProductMasterTable.UpdatedByColumn);
            sColumns.Add(ProductMasterTable.UpdatedAtColumn);
            sColumns.Add(ProductMasterTable.UpdatedByColumn);

            qb.Parameters.Add(ProductMasterTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
            qb.Parameters.Add(ProductMasterTable.UpdatedByColumn.ColumnName, CustomDateTime.IST);

            //var sCmd = "Update ProductMaster set {0} where Id = {1}";

            if (isHsn)
            {
                sColumns.Add(ProductMasterTable.HsnCodeColumn);
                //qb.AddColumn(ProductMasterTable.HsnCodeColumn);
                qb.Parameters.Add(ProductMasterTable.HsnCodeColumn.ColumnName, data.HsnCode);
            }
            if (isIgst)
            {
                sColumns.Add(ProductMasterTable.IgstColumn);
                //qb.AddColumn(ProductMasterTable.IgstColumn);
                qb.Parameters.Add(ProductMasterTable.IgstColumn.ColumnName, data.Igst);
            }
            if (isCgst)
            {
                sColumns.Add(ProductMasterTable.CgstColumn);
                //qb.AddColumn(ProductMasterTable.CgstColumn);
                qb.Parameters.Add(ProductMasterTable.CgstColumn.ColumnName, data.Cgst);
            }
            if (isSgst)
            {
                sColumns.Add(ProductMasterTable.SgstColumn);
                //qb.AddColumn(ProductMasterTable.SgstColumn);
                qb.Parameters.Add(ProductMasterTable.SgstColumn.ColumnName, data.Sgst);
            }
            if (isGstTotal)
            {
                sColumns.Add(ProductMasterTable.GstTotalColumn);
                //qb.AddColumn(ProductMasterTable.GstTotalColumn);
                qb.Parameters.Add(ProductMasterTable.GstTotalColumn.ColumnName, data.GstTotal);
            }

            qb.AddColumn(sColumns.ToArray());

            var tt = await QueryExecuter.NonQueryAsyc2(qb);
            return data;
        }

        //Newly added for GST Product update while updating Product Stock table
        public async Task<Product> UpdateProductGSTFromStock(Product data, bool isHsn, bool isIgst, bool isCgst, bool isSgst, bool isGstTotal)
        {


            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);

            List<IDbColumn> sColumns = new List<IDbColumn>();
            sColumns.Add(ProductTable.UpdatedAtColumn);
            sColumns.Add(ProductTable.UpdatedByColumn);

            qb.Parameters.Add(ProductTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
            if (data.UpdatedBy == null)
            {

                qb.Parameters.Add(ProductTable.UpdatedByColumn.ColumnName, "");
            }
            else
            {
                qb.Parameters.Add(ProductTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            }

            if (isHsn)
            {
                sColumns.Add(ProductTable.HsnCodeColumn);
                qb.Parameters.Add(ProductTable.HsnCodeColumn.ColumnName, data.HsnCode);
            }
            if (isIgst)
            {
                sColumns.Add(ProductTable.IgstColumn);
                qb.Parameters.Add(ProductTable.IgstColumn.ColumnName, data.Igst);
            }
            if (isCgst)
            {
                sColumns.Add(ProductTable.CgstColumn);
                qb.Parameters.Add(ProductTable.CgstColumn.ColumnName, data.Cgst);
            }
            if (isSgst)
            {
                sColumns.Add(ProductTable.SgstColumn);
                qb.Parameters.Add(ProductTable.SgstColumn.ColumnName, data.Sgst);
            }
            if (isGstTotal)
            {
                sColumns.Add(ProductTable.GstTotalColumn);
                qb.Parameters.Add(ProductTable.GstTotalColumn.ColumnName, data.GstTotal);
            }

            qb.AddColumn(sColumns.ToArray());

            if (data.WriteExecutionQuery)
            {
                data.AddExecutionQuery(qb);
            }
            else
            {
                var tt = await QueryExecuter.NonQueryAsyc(qb);
            }

            return data;
        }
        /// <summary>
        /// Product stock - GST details updated for the  Account  based on Product
        /// </summary>
        /// <param name="sProductid"></param>
        /// <param name="dGstTotal"></param>
        /// <param name="sUpdatedby"></param>
        /// <param name="sAccountId"></param>
        /// <returns></returns>
        /// ******************* COMMENTED FOR HANDLING DEAD LOCK  by Poongodi on 12/08/2017***************************
        /// Uncommented for GST Null popup bulk update on 31-Oct-2017
        /// Added by Poogodi on 04/07/2017
        public async Task<bool> ProductStockUpdate(string sProductid, decimal dGstTotal, string sUpdatedby, string sAccountId)
        {
            ////string query = $@"update ProductStock set GstTotal =  {dGstTotal} ,Igst = {dGstTotal}, Cgst = {dGstTotal}/2, Sgst = {dGstTotal}/2,UpdatedAt = @UpdatedAt, UpdatedBy = '{sUpdatedby}' where productid ='{sProductid}' and isnull(GstTotal,0) =0 and AccountId ='{sAccountId}'";
            //string query = "Update ProductStock Set GstTotal =  @GstTotal, Igst = @Igst, Cgst = @Cgst, Sgst = @Sgst, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy Where ProductId = @ProductId And GstTotal is NULL And AccountId = @AccountId";

            //var qb = QueryBuilderFactory.GetQueryBuilder(query);

            //qb.Parameters.Add("GstTotal", dGstTotal);
            //qb.Parameters.Add("Igst", dGstTotal);
            //qb.Parameters.Add("Cgst", dGstTotal/2);
            //qb.Parameters.Add("Sgst", dGstTotal/2);
            //qb.Parameters.Add("UpdatedBy", sUpdatedby);
            //qb.Parameters.Add("ProductId", sProductid);
            //qb.Parameters.Add("AccountId", sAccountId);

            //qb.Parameters.Add("UpdatedAt", CustomDateTime.IST);
            //bool result = await QueryExecuter.NonQueryAsyc2(qb);
            ////qb.Parameters.Add("AccountId", sAccountId);

            //WriteToSyncQueue(new SyncObject() { Id = sProductid, QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "A" });
            //return result;
            return true;
        }

        public async Task<bool> ProductStockUpdate(string sProductid, decimal dGstTotal, string sUpdatedby, string sAccountId, string sInstanceId,int Status)
        {
            var sUpdatedAt = CustomDateTime.IST;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("GstTotal", dGstTotal);
            param.Add("Igst", dGstTotal);
            param.Add("Cgst", dGstTotal / 2);
            param.Add("Sgst", dGstTotal / 2);
            param.Add("UpdatedBy", sUpdatedby);
            param.Add("ProductId", sProductid);
            param.Add("AccountId", sAccountId);
            param.Add("InstanceId", sInstanceId);
            param.Add("UpdatedAt", sUpdatedAt);
            param.Add("Status", Status);  //Added By Sumathi On 20-Feb-2019

            string procName = string.Empty;
            var resultSP = await sqldb.ExecuteProcedureAsync<dynamic>("usp_ProductStock_UpdateByProductId", param);

            string query = "Update ProductStock Set GstTotal =  @GstTotal, Igst = @Igst, Cgst = @Cgst, Sgst = @Sgst, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy Where ProductId = @ProductId And GstTotal is NULL And AccountId = @AccountId";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            //qb.Parameters.Add("GstTotal", dGstTotal);
            //qb.Parameters.Add("Igst", dGstTotal);
            //qb.Parameters.Add("Igst", dGstTotal / 2);
            //qb.Parameters.Add("Sgst", dGstTotal / 2);
            //qb.Parameters.Add("UpdatedBy", sUpdatedby);
            //qb.Parameters.Add("ProductId", sProductid);
            //qb.Parameters.Add("AccountId", sAccountId);
            //qb.Parameters.Add("UpdatedAt", sUpdatedAt);
            //bool result = await QueryExecuter.NonQueryAsyc2(qb);

            WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = param, EventLevel = "A" });


            query = "Update ProductStock Set Status = @Status Where ProductId = @ProductId And AccountId = @AccountId";
            qb = QueryBuilderFactory.GetQueryBuilder(query);
            WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = param, EventLevel = "A" });

            return true;

        }
        public async Task<List<ProductMaster>> GetProductMasterGeneric(string genericName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Select);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                qb.ConditionBuilder.And(ProductMasterTable.GenericNameColumn, genericName, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.AddColumn(ProductMasterTable.IdColumn, ProductMasterTable.GenericNameColumn);
            }
            else
            {
                qb.ConditionBuilder.And(ProductMasterTable.GenericNameColumn, $"{genericName}%", CriteriaEquation.Like);
                qb.AddColumn(ProductMasterTable.IdColumn, ProductMasterTable.GenericNameColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SetTop(10);
            var list = new List<ProductMaster>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var generic = new ProductMaster
                    {
                        GenericName = reader[ProductMasterTable.GenericNameColumn.ColumnName].ToString(),
                    };
                    list.Add(generic);
                }
            }
            var genGroupBy = list.GroupBy(l => l.GenericName).Select(group => group.First());
            return genGroupBy.ToList();
        }

        public async Task<ProductMaster> ProductMasterUpdateCount()
        {
            var query = $"Select (select count(1) from ProductMaster) as Total,(select count(1) from ProductMaster Where GenericName is Null) as Pending,(select count(1) from ProductMaster Where GenericName is not Null) as Completed,(select count(1) from ProductMaster Where Convert(date,UpdatedAt)=Convert(date,GETDATE())) as Today";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = await List<ProductMaster>(qb);
            return list.FirstOrDefault();
            //var qb = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Count);
            //qb.ConditionBuilder.And(ProductMasterTable.UpdatedAtColumn, CriteriaEquation.Equal);
            //qb.Parameters.Add(ProductMasterTable.UpdatedAtColumn.ColumnName,DateTime.Now.ToString("yyyy-MM-dd"));                    
            //return Convert.ToInt32(await SingleValueAsyc(qb));           
        }
        // Added by Settu on 06-06-2017 for minmax reorder
        public async Task<bool> ReorderMinMaxBulkUpdate_Old(ProductInstance data, DateTime dfromDate, DateTime dtoDate, int minReorderFactor, int maxReorderFactor)
        {
            string fromDate = dfromDate.ToString("yyyy-MM-dd");
            string toDate = dtoDate.ToString("yyyy-MM-dd");
            string query = $@"
            UPDATE PINS SET PINS.ReOrderLevel = ROUND(R.AvgSoldQty * {minReorderFactor}, 0),
	        PINS.ReOrderQty = ROUND(CASE WHEN ISNULL(R.AvgSoldQty,0) <> 0 THEN R.AvgSoldQty * {maxReorderFactor}
	        ELSE ISNULL(RR.PackageSize,ISNULL(R.PackageSize,0)) END, 0),
            PINS.OfflineStatus = {(data.OfflineStatus ? 1 : 0)},
            PINS.UpdatedAt = '{CustomDateTime.IST}',
            PINS.UpdatedBy = '{data.UpdatedBy}'
	        FROM 
	        (
		        SELECT P.Id,P.Name,P.PackageSize,
		        ISNULL(R1.SoldQty,0) AS SoldQty,
		        CAST((ISNULL(R1.SoldQty,0)/(SELECT DATEDIFF(DAY, CAST('{fromDate}' AS DATE), CAST('{toDate}' AS DATE)) + 1)) AS MONEY) AS AvgSoldQty
		        FROM Product P 
		        LEFT JOIN 
		        (
			        SELECT PS.ProductId,SUM(SI.Quantity-ISNULL(SR.ReturnQty,0)) AS SoldQty FROM ProductStock PS
			        INNER JOIN SalesItem SI ON SI.ProductStockId = PS.Id
			        INNER JOIN Sales S ON S.Id = SI.SalesId
			        LEFT JOIN 
			        (
				        SELECT SR.SalesId,SRI.ProductStockId,SUM(SRI.Quantity) AS ReturnQty FROM SalesReturn SR 
				        INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
				        WHERE SR.AccountId = '{data.AccountId}' AND SR.InstanceId = '{data.InstanceId}'
				        AND SR.ReturnDate BETWEEN CAST('{fromDate}' AS DATE) AND CAST('{toDate}' AS DATE)
				        GROUP BY SR.SalesId,SRI.ProductStockId 
			        ) SR 
			        ON SR.SalesId = SI.SalesId AND SR.ProductStockId = SI.ProductStockId		
			        WHERE PS.AccountId = '{data.AccountId}' AND PS.InstanceId = '{data.InstanceId}'
			        AND S.InvoiceDate BETWEEN CAST('{fromDate}' AS DATE) AND CAST('{toDate}' AS DATE)
			        AND S.Cancelstatus IS NULL
			        GROUP BY PS.ProductId
		        ) R1 ON R1.ProductId = P.Id
		        GROUP BY P.Id,P.Name,P.PackageSize,ISNULL(R1.SoldQty,0)
	        ) R 
	        INNER JOIN ProductInstance PINS ON PINS.ProductId = R.Id
            LEFT JOIN
            (	
		        SELECT PS1.ProductId,PS1.PackageSize 		
		        FROM ProductStock PS1 
		        INNER JOIN 
		        (
			        SELECT PS2.ProductId,MAX(PS2.CreatedAt) AS CreatedAt FROM ProductStock PS2
			        WHERE PS2.AccountId = '{data.AccountId}' AND PS2.InstanceId = '{data.InstanceId}'
			        GROUP BY PS2.ProductId
		        ) R2 ON R2.ProductId = PS1.ProductId AND R2.CreatedAt = PS1.CreatedAt
		        WHERE PS1.AccountId = '{data.AccountId}' AND PS1.InstanceId = '{data.InstanceId}'
	        ) RR ON RR.ProductId = PINS.ProductId
	        WHERE PINS.AccountId = '{data.AccountId}' AND PINS.InstanceId = '{data.InstanceId}'
	        AND ISNULL(PINS.ReOrderModified,0) = 0 ";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            bool result = await QueryExecuter.NonQueryAsyc(qb);
            return result;
        }

        public async Task<bool> ReorderMinMaxBulkUpdate(ProductInstance data, DateTime dfromDate, DateTime dtoDate, int minReorderFactor, int maxReorderFactor)
        {
            string fromDate = dfromDate.ToString("yyyy-MM-dd");
            string toDate = dtoDate.ToString("yyyy-MM-dd");
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", data.AccountId);
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("fromDate", fromDate);
            parms.Add("toDate", toDate);
            parms.Add("MinReorderFactor", minReorderFactor);
            parms.Add("MaxReorderFactor", maxReorderFactor);
            parms.Add("OfflineStatus", data.OfflineStatus);
            parms.Add("updatedBy", data.UpdatedBy);
            var queryresult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_UpdateMinMaxReorderQtyBySales", parms);
            bool result = queryresult.Select(x =>
            {
                return x.Result;
            }).FirstOrDefault();
            return result;
        }

        public async Task<string> ExportProductMaster(string accountId)
        {
            StringBuilder sb = new StringBuilder();
            string fileName = null;
            string filePath = null;
            FileInfo info = null;
            try
            {
                /*All columns included by Poongodi on 21/08/2017*/
                var query = $" SELECT ROW_NUMBER() OVER(ORDER BY name ASC) AS Row#, Id, replace(Code,',','') [Code],replace( Name,',','') [Name],Replace(Manufacturer,',',' ') as Manufacturer,Schedule,Replace(Category,',',' ') as Category,"
                            + "ISNULL(Price,0) as Purchaseprice,ISNULL(PackageSize,0) as PurchaseUnit,ISNULL(VAT,0) as PurchaseTax,HsnCode,Sgst,Cgst,Igst,gstTotal,InstanceId,Replace(isnull(KindName,''),',','') [KindName],Replace(isnull(StrengthName,''),',','') [StrengthName],"
                            + "Type,Replace(isnull(GenericName,''),',','') [GenericName],"
                            + "CommodityCode,Status,ProductMasterID,Discount, Eancode,IsHidden FROM Product WHERE AccountId=@AccountId";

                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                qb.Parameters.Add("@AccountId", accountId);

                using (var reader = await QueryExecuter.QueryAsyc(qb))
                {
                    fileName = string.Format("{0}.csv", accountId);
                    filePath = string.Format("./wwwroot/temp/ProductMaster_{0}.csv", accountId);
                    info = new FileInfo(filePath);
                    if (info.Exists)
                        info.Delete();

                    using (StreamWriter file = info.AppendText())
                    {
                        file.WriteLine("RowREF,Id,ProductCode,ProductName,Manufacturer,ScheduleCode,Category,Purchaseprice,PurchaseUnit,PurchaseTax,HsnCode,SGST,CGST,IGST,gstTotal,InstanceId,KindName,StrengthName,Type,GenericName,CommodityCode,Status,ProductMasterID,Discount,Eancode,IsHidden");
                        bool bResult = true;
                        while (bResult)
                        {
                            while (reader.Read())
                            {
                                //sb.Append(reader.GetString(0));
                                file.Write(reader.GetValue(0).ToString());
                                //var splitter = "\t";
                                file.Write(string.Format(",{0}", reader.GetValue(1).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(2).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(3).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(4).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(5).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(6).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(7).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(8).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(9).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(10).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(11).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(12).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(13).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(14).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(15).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(16).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(17).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(18).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(19).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(20).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(21).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(22).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(23).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(24).ToString()));
                                file.Write(string.Format(",{0}", reader.GetValue(25).ToString()));

                                //file.Write(string.Format(",{0}", reader.GetValue(13).ToString()));
                                file.WriteLine();
                            }

                            bResult = reader.NextResult();
                        }
                    }
                }

            }
            catch (Exception e)
            {
                return e.Message;
            }

            return info.FullName;
        }

        public async Task<List<Product>> GetProductCodes(List<Product> data)
        {
            if (data != null)
            {
                Dictionary<string, object> parms = new Dictionary<string, object>();
                var ProductIds = string.Join(",", data.Select(x => x.Id));
                parms.Add("AccountId", data[0].AccountId);
                parms.Add("ProductIds", ProductIds);
                var result = await sqldb.ExecuteProcedureAsync<Product>("usp_UpdateProductCodes", parms);

                return result.ToList();
            }
            else
            {
                return data;
            }
        }

        public async Task<List<Product>> UpdateProductCodesToLocal(List<Product> data)
        {
            if (data != null)
            {
                Dictionary<string, object> parms = new Dictionary<string, object>();
                string ProductIdsVsCodes = string.Join(",", from item in data select item.Id + "~" + item.Code);
                parms.Add("ProductIdsVsCodes", ProductIdsVsCodes);
                var result = await sqldb.ExecuteProcedureAsync<Product>("usp_UpdateProductCodesToLocal", parms);

                return result.ToList();
            }
            else
            {
                return data;
            }
        }

        public async Task<int> CheckProductExist(string AccountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Count);
            if (!string.IsNullOrEmpty(AccountId))
            {
                qb.ConditionBuilder.And(ProductTable.AccountIdColumn, AccountId);
            }
            qb.AddColumn(ProductTable.IdColumn);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        // DATA Correction Start
        public async Task<Tuple<bool, ProductStock>> GetDataCorrectionProductStocks(ProductStock data)
        {
            if (data != null)
            {
                Dictionary<string, object> parms = new Dictionary<string, object>();
                parms.Add("AccountId", data.AccountId);
                parms.Add("InstanceId", data.InstanceId);
                parms.Add("ProductStockId", data.Id);
                parms.Add("Stock", data.ActualStock);
                parms.Add("UpdatedAt", data.UpdatedAt);
                var result = await sqldb.ExecuteProcedureAsync<ProductStock>("usp_DataCorrection_UpdateProductStock", parms);
                return Tuple.Create(false, new ProductStock());
            }
            else
            {
                return Tuple.Create(false, new ProductStock());
            }
        }
        public async Task<Tuple<bool, ProductStock>> InsertDataCorrectionOpeningStocks(ProductStock data)
        {
            if (data != null)
            {
                await InsertAPIDataCorrection(data, ProductStockTable.Table);
                return Tuple.Create(false, new ProductStock());
            }
            else
            {
                return Tuple.Create(false, new ProductStock());
            }
        }
        public async Task<Tuple<bool, StockAdjustment>> InsertDataCorrectionStockAdjustments(StockAdjustment data)
        {
            if (data != null)
            {
                await InsertAPIDataCorrection(data, StockAdjustmentTable.Table);
                return Tuple.Create(false, new StockAdjustment());
            }
            else
            {
                return Tuple.Create(false, new StockAdjustment());
            }
        }
        public async Task<Tuple<bool, TempVendorPurchaseItem>> InsertDataCorrectionTempStocks(TempVendorPurchaseItem data)
        {
            if (data != null)
            {
                await InsertAPIDataCorrection(data, TempVendorPurchaseItemTable.Table);
                return Tuple.Create(false, new TempVendorPurchaseItem());
            }
            else
            {
                return Tuple.Create(false, new TempVendorPurchaseItem());
            }
        }

        public async Task<Tuple<bool, ProductStock>> UpdateProductStockToOnline(ProductStock data)
        {
            if (data != null)
            {
                Dictionary<string, object> parms = new Dictionary<string, object>();
                //string ProductStockIdsVsStock = string.Join(",", from item in data select item.Id + "~" + item.Stock);
                parms.Add("AccountId", data.AccountId);
                parms.Add("InstanceId", data.InstanceId);
                parms.Add("ProductStockIdsVsStock", data.Queryxml);
                var result = await sqldb.ExecuteProcedureAsync<ProductStock>("usp_UpdateProductStockToOnline", parms);
                return Tuple.Create(false, new ProductStock());
            }
            else
            {
                return Tuple.Create(false, new ProductStock());
            }
        }
        public async Task<string> GetReverseSyncOfflineToOnlineCompare(string SeedIndex)
        {
            var SyncDataId = "0";
            if (SeedIndex != null)
            {
                string[] SyncData = SeedIndex.Split('~');
                var query = $@"select  count(*) as CountSyncData from SyncData with(nolock) where AccountId ='{SyncData[1]}' and InstanceId='{SyncData[2]}' and id > '{SyncData[0]}' and (data like '%UPDATE PRODUCTSTOCK%' or data like '%INSERT INTO PRODUCTSTOCK%')";
                var qb1 = QueryBuilderFactory.GetQueryBuilder(query);
                SyncDataId = QueryExecuter.SingleValueAsyc(qb1).Result.ToString();
                if (SeedIndex != null)
                {
                    return SyncDataId;
                }
                else
                {
                    return "0";
                }
                    
            }
            else
            {
                return SyncDataId;
            }
           
        }
        // DATA Correction End
    }
}
