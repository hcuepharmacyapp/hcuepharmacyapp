﻿using HQue.Contract.External.Common;
using HQue.Contract.External.Patient;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using DataAccess.Criteria;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Helpers;
using System.Linq;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.External;

namespace HQue.DataAccess.Master
{
    public class PatientDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb; //Added to convert qry to SP on 15022017 by Poongodi
        private readonly ConfigHelper _configHelper;
        public PatientDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }


        public override Task<Patient> Save<Patient>(Patient data)
        {
            return Insert2(data, PatientTable.Table);
        }




        public async Task<string> AutoSave(Patient data)
        {

            if (string.IsNullOrEmpty(data.EmpID))
            {
                var AutoCustid = await AutoGenerateCustid(data);

                if (string.IsNullOrEmpty(AutoCustid))
                {
                    data.EmpID = "X1";
                }
                else
                {
                    data.EmpID = AutoCustid;
                    data.IsAutoSave = true;

                }
            }
            else
            {
                bool EmpExists = await EmpIdExists(data);
                if (EmpExists == true)
                {
                    return "false";
                }
                data.IsAutoSave = false;
            }


            //data.EmpID = AutoCustid;
            data.PatientType = 1;

            //Checking Existing patient by Violet - Start
            if (!string.IsNullOrEmpty(data.Mobile) || !string.IsNullOrEmpty(data.Name) || !string.IsNullOrEmpty(data.Id))
            {
                data.Id = await GetExistPatientatautosave(data.Mobile, data.Name, data.ShortName, data.Id, data.AccountId, data.InstanceId);
            }
            if (string.IsNullOrEmpty(data.Id) && (!string.IsNullOrEmpty(data.Mobile) || !string.IsNullOrEmpty(data.Name) || !string.IsNullOrEmpty(data.ShortName)))
            {
                if (data.WriteExecutionQuery)
                {
                    SetInsertMetaData(data, PatientTable.Table);
                    WriteInsertExecutionQuery(data, PatientTable.Table);
                }
                else
                {
                    var patient = await Insert2(data, PatientTable.Table);
                    data.Id = patient.Id;
                }
            }
            if (!string.IsNullOrEmpty(data.Id))
            {
               await SaveOpeningBalanceCustomerPayment(data);
            }
            return data.Id;


        }

        public async Task<string> AutoGenerateCustid(Patient data)
        {


            var query = "";
            query = $" select CONCAT('X', max(Convert(int, SUBSTRING(EmpID,2, (LEN(EmpID) -1))))+1) from Patient where AccountId ='{data.AccountId}' and IsAutoSave = 1 and EmpID like 'X%'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);


            var result = await QueryExecuter.SingleValueAsyc(qb);
            if (result == null)
            {
                return "";
            }
            else
            {
                return result.ToString();
            }


        }

        // Newly Added Gavaskar 23-10-2016 Start
        public async Task<bool> EmpIdExists(Patient data)
        {
            var query = $"SELECT COUNT(*) FROM {PatientTable.TableName} WHERE {PatientTable.EmpIDColumn} = '{data.EmpID}' and {PatientTable.AccountIdColumn}='{data.AccountId}' /*and {PatientTable.InstanceIdColumn}='{data.InstanceId}'*/ and {PatientTable.IdColumn}!='{data.Id}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var count = Convert.ToInt32(await SingleValueAsyc(qb));

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public async Task<bool> ShortNameExists(Patient data)
        {
            if (data.ShortName == "" || data.ShortName == null)
            {
                return false;
            }
            else
            {

                var query = $"SELECT COUNT(*) FROM {PatientTable.TableName} WHERE {PatientTable.ShortNameColumn} = '{data.ShortName}' and {PatientTable.AccountIdColumn}='{data.AccountId}' /*and {PatientTable.InstanceIdColumn}='{data.InstanceId}'*/ and {PatientTable.IdColumn}!='{data.Id}'";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);

                var count = Convert.ToInt32(await SingleValueAsyc(qb));

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }


        }

        public async Task<bool> DepartmentNameExists(Patient data)
        {
            if (data.Name == "" || data.Name == null)
            {
                return false;
            }
            else
            {

                var query = $"SELECT COUNT(*) FROM {PatientTable.TableName} WHERE {PatientTable.NameColumn} = '{data.Name}' and {PatientTable.AccountIdColumn}='{data.AccountId}' and {PatientTable.InstanceIdColumn}='{data.InstanceId}' and {PatientTable.IdColumn}!='{data.Id}' and {PatientTable.PatientTypeColumn}='{data.PatientType}'";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);

                var count = Convert.ToInt32(await SingleValueAsyc(qb));

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //Start saving a customer code Newly Added by DURGA on 18-04-2017 
        public async Task<Patient> PatientSave(Patient data)
        {


            if (!string.IsNullOrEmpty(data.AccountId))
            {
                if (!string.IsNullOrEmpty(data.Mobile))
                {
                    data.Mobile = data.Mobile.TrimStart('0');
                }

                bool EmpExists = false;
                if (!string.IsNullOrEmpty(data.EmpID))
                {

                    //EmpId checking 
                    EmpExists = await EmpIdExists(data);

                    if (EmpExists == true)
                    {
                        data.isexists = true;
                    }
                    else
                    {
                        await continuePatientSave(data);
                    }


                }
                else
                {
                    await continuePatientSave(data);
                }



                return data;
            }
            else
            {
                return data;
            }


        }

        public async Task SaveOpeningBalanceCustomerPayment(Patient data)
        {
            Guid id = Guid.NewGuid();
            DateTime updateDate = CustomDateTime.IST;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("CustomerId", data.Id);
            parms.Add("credit", data.BalanceAmount);
            parms.Add("offlineStatus", data.OfflineStatus);
            parms.Add("createdBy", data.CreatedBy);
            parms.Add("id", id.ToString());
            parms.Add("updatedDate", updateDate);
            var result =  await sqldb.ExecuteProcedureAsync<dynamic>("usp_InsertCustomerPaymentOB", parms);
            string status = "0";
            foreach (IDictionary<string, object> row in result)
            {
                foreach (var pair in row)
                {
                    if (pair.Key == "status")
                        //Debit = Convert.ToDecimal(pair.Value); 
                        status = Convert.ToString(pair.Value);
                }
            }
    
            if (status == "1")
            {

                Dictionary<string, object> inserParams = new Dictionary<string, object>();

                inserParams.Add("Id", id.ToString());
                inserParams.Add("Accountid", data.AccountId);
                inserParams.Add("InstanceId", data.InstanceId);
                inserParams.Add("CustomerId", data.Id);
                inserParams.Add("TransactionDate", updateDate);
                inserParams.Add("Debit", 0);
                inserParams.Add("Credit", data.BalanceAmount);
                inserParams.Add("OfflineStatus", data.OfflineStatus);
                inserParams.Add("CreatedAt", updateDate);
                inserParams.Add("CreatedBy", data.CreatedBy);

                var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Insert);
                qb.AddColumn(CustomerPaymentTable.IdColumn, CustomerPaymentTable.AccountIdColumn, CustomerPaymentTable.InstanceIdColumn,
                    CustomerPaymentTable.CustomerIdColumn, CustomerPaymentTable.TransactionDateColumn, CustomerPaymentTable.DebitColumn,
                    CustomerPaymentTable.CreditColumn, CustomerPaymentTable.OfflineStatusColumn,
                    CustomerPaymentTable.CreatedAtColumn, CustomerPaymentTable.CreatedByColumn);

                WriteToSyncQueue(new SyncObject()
                {
                    Id = id.ToString(),
                    QueryText = qb.GetQuery(),
                    Parameters = inserParams,
                    EventLevel = "A"
                });
            }
        }

        public async Task UpdateOpeningBalanceCustomerayment(Patient data)
        {
            Guid id = Guid.NewGuid();
            DateTime updateDate = CustomDateTime.IST;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("CustomerId", data.Id);
            parms.Add("credit", data.BalanceAmount);
            parms.Add("offlineStatus", data.OfflineStatus);
            parms.Add("createdBy", data.CreatedBy);
            parms.Add("id", id.ToString());
            parms.Add("updatedDate", updateDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_UpdateCustomerPaymentOB", parms);
            string status = "0"; string updatedId = "";
            foreach (IDictionary<string, object> row in result)
            {
                foreach (var pair in row)
                {
                    if (pair.Key == "status")
                        //Debit = Convert.ToDecimal(pair.Value); 
                        status = Convert.ToString(pair.Value);
                    if (pair.Key == "updatedId")
                        updatedId = Convert.ToString(pair.Value);
                }
            }

            updateSyncValues(id.ToString(),status, updatedId, updateDate, data);

           

        }

        public void updateSyncValues(string id, string status, string updatedId, DateTime updatedDate, Patient data)
        {
            if (status == "1")
            {
                Dictionary<string, object> inserParams = new Dictionary<string, object>();

                inserParams.Add("Id", id.ToString());
                inserParams.Add("Accountid", data.AccountId);
                inserParams.Add("InstanceId", data.InstanceId);
                inserParams.Add("CustomerId", data.Id);
                inserParams.Add("TransactionDate", updatedDate);
                inserParams.Add("Debit", 0);
                inserParams.Add("Credit", data.BalanceAmount);
                inserParams.Add("OfflineStatus", data.OfflineStatus);
                inserParams.Add("CreatedAt", updatedDate);
                inserParams.Add("CreatedBy", data.CreatedBy);

                var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Insert);
                qb.AddColumn(CustomerPaymentTable.IdColumn, CustomerPaymentTable.AccountIdColumn, CustomerPaymentTable.InstanceIdColumn,
                    CustomerPaymentTable.CustomerIdColumn, CustomerPaymentTable.TransactionDateColumn, CustomerPaymentTable.DebitColumn,
                    CustomerPaymentTable.CreditColumn, CustomerPaymentTable.OfflineStatusColumn,
                    CustomerPaymentTable.CreatedAtColumn, CustomerPaymentTable.CreatedByColumn);

                WriteToSyncQueue(new SyncObject()
                {
                    Id = id.ToString(),
                    QueryText = qb.GetQuery(),
                    Parameters = inserParams,
                    EventLevel = "A"
                });

            }
            else if (status == "2")
            {
                Dictionary<string, object> inserParams = new Dictionary<string, object>();

                inserParams.Add("Id", updatedId.ToString());
                inserParams.Add("Accountid", data.AccountId);
                inserParams.Add("InstanceId", data.InstanceId);
                inserParams.Add("Credit", data.BalanceAmount);
                inserParams.Add("OfflineStatus", data.OfflineStatus);
                inserParams.Add("UpdatedAt", updatedDate);
                inserParams.Add("UpdatedBy", data.CreatedBy);
                var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Update);
                qb.AddColumn(
                   CustomerPaymentTable.CreditColumn, CustomerPaymentTable.OfflineStatusColumn,
                   CustomerPaymentTable.UpdatedAtColumn, CustomerPaymentTable.UpdatedByColumn);
                qb.ConditionBuilder.And(CustomerPaymentTable.IdColumn);
                qb.ConditionBuilder.And(CustomerPaymentTable.AccountIdColumn);
                qb.ConditionBuilder.And(CustomerPaymentTable.InstanceIdColumn);


                WriteToSyncQueue(new SyncObject()
                {
                    Id = updatedId.ToString(),
                    QueryText = qb.GetQuery(),
                    Parameters = inserParams,
                    EventLevel = "A"
                });
            }
        }

        public async Task<Patient> DepartmentPatientSave(Patient data)
        {


            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                //Checking ShortName
                bool DepartmentName = await DepartmentNameExists(data);
                if (DepartmentName == true)
                {
                    data.Name = "Already DepartmentName Exists";
                    return data;
                }
                else
                {
                    if (data.Discount == null)
                        data.Discount = 0;
                    data.Mobile = "NA";
                    data.Gender = "";
                    //data.CustomerPaymentType = "";
                    var pdata = await Insert(data, PatientTable.Table);
                    await SaveOpeningBalanceCustomerPayment(pdata);
                    return pdata;
                }
            }
            else
            {
                return data;
            }

        }

        public async Task<Patient> continuePatientSave(Patient data)
        {
            //Checking ShortName
            bool ShortName = await ShortNameExists(data);
            if (ShortName == true)
            {
                data.ShortName = "Already ShortName Exists";

                return data;
            }

            //Checking Patient AlreadyExists or not
            var count = await GetExistPatient(data.Mobile, data.Name, data.ShortName, data.Id, data.AccountId, data.InstanceId);
            if (count > 0)
            {

                //if (data.Id == null || data.Id == "")
                //{
                data.Name = "Already Name Exists";
                data.Mobile = "Already Mobile Exists";
                return data;
                //}
                //else
                //{
                //    var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
                //    qb.ConditionBuilder.And(PatientTable.IdColumn, data.Id);
                //    qb.AddColumn(PatientTable.NameColumn, PatientTable.MobileColumn, PatientTable.ShortNameColumn, PatientTable.EmailColumn,
                //    PatientTable.DOBColumn, PatientTable.GenderColumn, PatientTable.CustomerPaymentTypeColumn,
                //    PatientTable.AddressColumn, PatientTable.CityColumn, PatientTable.PincodeColumn, PatientTable.DiscountColumn, PatientTable.EmpIDColumn, PatientTable.IsAutoSaveColumn);
                //    qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data.Name);
                //    qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, data.Mobile);
                //    qb.Parameters.Add(PatientTable.ShortNameColumn.ColumnName, data.ShortName);
                //    qb.Parameters.Add(PatientTable.EmailColumn.ColumnName, data.Email);
                //    qb.Parameters.Add(PatientTable.DOBColumn.ColumnName, data.DOB);
                //    qb.Parameters.Add(PatientTable.GenderColumn.ColumnName, data.Gender);
                //    qb.Parameters.Add(PatientTable.EmpIDColumn.ColumnName, data.EmpID);
                //    qb.Parameters.Add(PatientTable.AddressColumn.ColumnName, data.Address);
                //    qb.Parameters.Add(PatientTable.CityColumn.ColumnName, data.City);
                //    qb.Parameters.Add(PatientTable.PincodeColumn.ColumnName, data.Pincode);
                //    qb.Parameters.Add(PatientTable.DiscountColumn.ColumnName, data.Discount);
                //    qb.Parameters.Add(PatientTable.CustomerPaymentTypeColumn.ColumnName, data.CustomerPaymentType);
                //    qb.Parameters.Add(PatientTable.IsAutoSaveColumn.ColumnName, data.IsAutoSave);

                //    await QueryExecuter.NonQueryAsyc(qb);

                //    return data;
                //}

            }

            else
            {
                if (data.Discount == null)
                    data.Discount = 0;
                //For Offline Modified by  Poongodi on 06/06/2017
                if (data.PatientType.ToString() == "2")
                {
                    var pdata = await Insert(data, PatientTable.Table);
                    await SaveOpeningBalanceCustomerPayment(pdata);
                    return pdata;
                }
                else
                {
                    var pdata = await Insert2(data, PatientTable.Table);
                    await SaveOpeningBalanceCustomerPayment(pdata);
                    return pdata;
                }
                    
            }


            //return data;


        }

        //End saving a customer code Newly Added by DURGA on 18-04-2017 


        //Updating a customer code Newly Added by DURGA on 19-04-2017 
        //Updating a customer Status Newly Added on 27-10-2017 
        public async Task<Patient> continueUpdatePatient(Patient data)
        {
            //Checking ShortName
            bool ShortName = await ShortNameExists(data);
            if (ShortName == true)
            {
                data.ShortName = "Already ShortName Exists";
                return data;
            }
            else
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(PatientTable.IdColumn, data.Id);
                qb.AddColumn(PatientTable.NameColumn, PatientTable.MobileColumn, PatientTable.ShortNameColumn, PatientTable.EmailColumn,
                PatientTable.DOBColumn, PatientTable.GenderColumn, PatientTable.CustomerPaymentTypeColumn,PatientTable.BalanceAmountColumn,
                PatientTable.AddressColumn, PatientTable.CityColumn, PatientTable.PincodeColumn, PatientTable.DiscountColumn, PatientTable.EmpIDColumn, PatientTable.IsAutoSaveColumn, PatientTable.PanColumn, PatientTable.GsTinColumn, PatientTable.LocationTypeColumn, PatientTable.StatusColumn);
                qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data.Name);
                qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, data.Mobile);
                qb.Parameters.Add(PatientTable.ShortNameColumn.ColumnName, data.ShortName);
                qb.Parameters.Add(PatientTable.EmailColumn.ColumnName, data.Email);
                qb.Parameters.Add(PatientTable.DOBColumn.ColumnName, data.DOB);
                qb.Parameters.Add(PatientTable.GenderColumn.ColumnName, data.Gender);
                qb.Parameters.Add(PatientTable.EmpIDColumn.ColumnName, data.EmpID);
                qb.Parameters.Add(PatientTable.AddressColumn.ColumnName, data.Address);
                qb.Parameters.Add(PatientTable.CityColumn.ColumnName, data.City);
                qb.Parameters.Add(PatientTable.PincodeColumn.ColumnName, data.Pincode);
                qb.Parameters.Add(PatientTable.DiscountColumn.ColumnName, data.Discount);
                qb.Parameters.Add(PatientTable.CustomerPaymentTypeColumn.ColumnName, data.CustomerPaymentType);
                qb.Parameters.Add(PatientTable.BalanceAmountColumn.ColumnName, data.BalanceAmount);
                qb.Parameters.Add(PatientTable.IsAutoSaveColumn.ColumnName, data.IsAutoSave);
                qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, user.InstanceId());
                qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, data.AccountId);


                qb.Parameters.Add(PatientTable.PanColumn.ColumnName, data.Pan);
                qb.Parameters.Add(PatientTable.GsTinColumn.ColumnName, data.GsTin);
                qb.Parameters.Add(PatientTable.LocationTypeColumn.ColumnName, Convert.ToInt32(Enum.Parse(typeof(LocationType), data.LocationTypeName)));
                qb.Parameters.Add(PatientTable.StatusColumn.ColumnName, Convert.ToInt32(data.Status));




                /*Modified for offline by Poongodi on 06/06/2017 */
                await QueryExecuter.NonQueryAsyc2(qb);
                WriteToSyncQueue(new SyncObject()
                {
                    Id = data.Id,
                    QueryText = qb.GetQuery(),
                    Parameters = qb.Parameters,
                    EventLevel = Convert.ToString(data.PatientType) == "2" ? "I" : "A"
                });
                Guid id = Guid.NewGuid();
                DateTime updateDate = CustomDateTime.IST;
                Dictionary<string, object> parms = new Dictionary<string, object>();
                parms.Add("InstanceId", data.InstanceId);
                parms.Add("AccountId", data.AccountId);
                parms.Add("CustomerId", data.Id);
                parms.Add("credit", data.BalanceAmount);
                parms.Add("offlineStatus", data.OfflineStatus);
                parms.Add("createdBy", data.CreatedBy);
                parms.Add("id", id.ToString());
                parms.Add("updatedDate", updateDate);
                var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_UpdateCustomerPaymentOB", parms);
                string status = "0"; string updatedId = "";
                foreach (IDictionary<string, object> row in result)
                {
                    foreach (var pair in row)
                    {
                        if (pair.Key == "status")
                            //Debit = Convert.ToDecimal(pair.Value); 
                            status = Convert.ToString(pair.Value);
                        if (pair.Key == "updatedId")
                            updatedId = Convert.ToString(pair.Value);
                    }
                }

                updateSyncValues(id.ToString(), status, updatedId, updateDate, data);
                return data;
            }
        }

        public async Task UpdatePatient(Patient data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(PatientTable.IdColumn, data.Id);
            qb.AddColumn(PatientTable.StatusColumn);
            
            qb.Parameters.Add(PatientTable.StatusColumn.ColumnName, Convert.ToInt32(data.Status));
            await QueryExecuter.NonQueryAsyc2(qb);
        }

        public async Task<Patient> UpdateDepartmentPatient(Patient data)
        {
            //Checking ShortName
            bool DepartmentName = await DepartmentNameExists(data);
            if (DepartmentName == true)
            {
                data.Name = "Already DepartmentName Exists";
                return data;
            }
            else
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
                //  qb.ConditionBuilder.And(PatientTable.MobileColumn, data.Mobile);
                qb.ConditionBuilder.And(PatientTable.IdColumn, data.Id);
                qb.AddColumn(PatientTable.NameColumn, PatientTable.ShortNameColumn, PatientTable.EmailColumn,
                PatientTable.DOBColumn, PatientTable.GenderColumn, PatientTable.CustomerPaymentTypeColumn,PatientTable.BalanceAmountColumn,
                PatientTable.AddressColumn, PatientTable.CityColumn, PatientTable.PincodeColumn, PatientTable.DiscountColumn, PatientTable.EmpIDColumn, PatientTable.GsTinColumn, PatientTable.PanColumn, PatientTable.LocationTypeColumn, PatientTable.StatusColumn);
                qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data.Name);
                qb.Parameters.Add(PatientTable.ShortNameColumn.ColumnName, data.ShortName);
                qb.Parameters.Add(PatientTable.EmailColumn.ColumnName, data.Email);
                qb.Parameters.Add(PatientTable.DOBColumn.ColumnName, data.DOB);
                qb.Parameters.Add(PatientTable.GenderColumn.ColumnName, "");
                qb.Parameters.Add(PatientTable.EmpIDColumn.ColumnName, data.EmpID);
                qb.Parameters.Add(PatientTable.AddressColumn.ColumnName, data.Address);
                qb.Parameters.Add(PatientTable.CityColumn.ColumnName, data.City);
                qb.Parameters.Add(PatientTable.PincodeColumn.ColumnName, data.Pincode);
                qb.Parameters.Add(PatientTable.DiscountColumn.ColumnName, data.Discount);
                qb.Parameters.Add(PatientTable.CustomerPaymentTypeColumn.ColumnName, data.CustomerPaymentType);
                qb.Parameters.Add(PatientTable.BalanceAmountColumn.ColumnName,data.BalanceAmount);
                qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, user.InstanceId());
                qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, data.AccountId);
                qb.Parameters.Add(PatientTable.PanColumn.ColumnName, data.Pan);
                qb.Parameters.Add(PatientTable.GsTinColumn.ColumnName, data.GsTin);
                qb.Parameters.Add(PatientTable.LocationTypeColumn.ColumnName, Convert.ToInt32(Enum.Parse(typeof(LocationType), data.LocationTypeName)));
                qb.Parameters.Add(PatientTable.StatusColumn.ColumnName, Convert.ToInt32(data.Status));

                /*Modified for offline by Poongodi on 06/06/2017 */
                await QueryExecuter.NonQueryAsyc2(qb);
                WriteToSyncQueue(new SyncObject()
                {
                    Id = data.Id,
                    QueryText = qb.GetQuery(),
                    Parameters = qb.Parameters,
                    EventLevel = Convert.ToString(data.PatientType) == "2" ? "I" : "A"
                });
                Guid id = Guid.NewGuid();
                DateTime updateDate = CustomDateTime.IST;
                Dictionary<string, object> parms = new Dictionary<string, object>();
                parms.Add("InstanceId", data.InstanceId);
                parms.Add("AccountId", data.AccountId);
                parms.Add("CustomerId", data.Id);
                parms.Add("credit", data.BalanceAmount);
                parms.Add("offlineStatus", data.OfflineStatus);
                parms.Add("createdBy", data.CreatedBy);
                parms.Add("id", id.ToString());
                parms.Add("updatedDate", updateDate);
                var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_UpdateCustomerPaymentOB", parms);
                string status = "0"; string updatedId = "";
                foreach (IDictionary<string, object> row in result)
                {
                    foreach (var pair in row)
                    {
                        if (pair.Key == "status")
                            //Debit = Convert.ToDecimal(pair.Value); 
                            status = Convert.ToString(pair.Value);
                        if (pair.Key == "updatedId")
                            updatedId = Convert.ToString(pair.Value);
                    }
                }

                updateSyncValues(id.ToString(), status, updatedId, updateDate, data);
                return data;
            }
        }

        //Updating a customer code Newly Added by DURGA on 19-04-2017 
        public async Task<Patient> PatientUpdate(Patient data)
        {

            bool EmpExists = false;
            if (!string.IsNullOrEmpty(data.EmpID))
            {
                //EmpId checking 
                EmpExists = await EmpIdExists(data);

                if (EmpExists == true)
                {
                    data.isexists = true;
                }
                else
                {
                    await continueUpdatePatient(data);
                }
            }
            else
            {
                await continueUpdatePatient(data);
            }
            return data;

        }

        public async Task<Patient> DepartmentPatientUpdate(Patient data)
        {


            await UpdateDepartmentPatient(data);

            return data;

        }

        //Modified GetExistPatient Removed Shortname condition by DURGA on 18-04-2017
        public async Task<int> GetExistPatient(string mobile, string name, string shortName, string id, string Accountid, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, Accountid);
            //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            //qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, InstanceId);
            if (!string.IsNullOrEmpty(id))
            {
                qb.ConditionBuilder.Or(PatientTable.IdColumn);
                qb.Parameters.Add(PatientTable.IdColumn.ColumnName, id);
            }
            else
            {
                qb.ConditionBuilder.And(PatientTable.MobileColumn, CriteriaEquation.Equal);
                qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, mobile);
                qb.Parameters.Add(PatientTable.NameColumn.ColumnName, name);
            }

            qb.SelectBuilder.MakeDistinct = true;
            var count = (await List<Patient>(qb)).Count();
            return count;

        }

        public async Task<string> GetExistPatientatautosave(string mobile, string name, string shortName, string id, string Accountid, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, Accountid);
            //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            //qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, InstanceId);
            if (!string.IsNullOrEmpty(id))
            {
                qb.ConditionBuilder.And(PatientTable.IdColumn);
                qb.Parameters.Add(PatientTable.IdColumn.ColumnName, id);
            }
            else
            {
                qb.ConditionBuilder.And(PatientTable.MobileColumn, CriteriaEquation.Equal);
                qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, mobile);
                qb.Parameters.Add(PatientTable.NameColumn.ColumnName, name);
            }

            qb.SelectBuilder.MakeDistinct = true;
            using (var reader = QueryExecuter.QueryAsyc(qb).Result)
            {
                if (reader.Read())
                {
                    return reader[PatientTable.IdColumn.ColumnName].ToString();
                }
            }

            return "";

        }

        public string PatientMobile(Patient data, string Accountid, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PatientTable.EmpIDColumn, CriteriaEquation.Equal);
            qb.Parameters.Add(PatientTable.EmpIDColumn.ColumnName, data.EmpID);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, Accountid);
            //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            // qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, InstanceId);

            qb.SelectBuilder.MakeDistinct = true;

            using (var reader = QueryExecuter.QueryAsyc(qb).Result)
            {
                if (reader.Read())
                {
                    return reader[PatientTable.MobileColumn.ColumnName].ToString();
                }
            }

            return "";
        }

        /// <summary>
        /// Check sale against the patient
        /// </summary>
        /// <param name="data"></param>
        /// <param name="Accountid"></param>
        /// <param name="InstanceId"></param>
        /// <returns></returns>
        public string ChkPatientInSales(Patient data, string Accountid, string InstanceId)
        {
            var patId = data.Id;
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PatientTable.IdColumn, CriteriaEquation.Equal);
            qb.Parameters.Add(PatientTable.IdColumn.ColumnName, data.Id);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, Accountid);
            qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, InstanceId);

            qb.SelectBuilder.MakeDistinct = true;

            using (var reader = QueryExecuter.QueryAsyc(qb).Result)
            {
                if (reader.Read())
                {
                    patId = reader[PatientTable.IdColumn.ColumnName].ToString();
                    return patId;
                }
            }

            return patId;
        }

        public string PatientEmpId(Patient data, string Accountid, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PatientTable.IdColumn, CriteriaEquation.Equal);
            qb.Parameters.Add(PatientTable.IdColumn.ColumnName, data.Id);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, Accountid);
            qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, InstanceId);

            qb.SelectBuilder.MakeDistinct = true;

            using (var reader = QueryExecuter.QueryAsyc(qb).Result)
            {
                if (reader.Read())
                {
                    return reader[PatientTable.EmpIDColumn.ColumnName].ToString();
                }
            }

            return "";
        }


        //Newly addes by DURGA on 19-04-2017 to get localpatients details for external call
        public async Task<List<Patient>> GetExternalLocalPatientList(Patient data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PatientTable.MobileColumn, CriteriaEquation.Equal);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, data.Mobile);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, data.AccountId);
            qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, data.InstanceId);

            return await List<Patient>(qb);
        }

        public async Task<decimal?> GetCustomerDiscount(Patient customer)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            decimal? disc = 0;
            disc = customer.Discount == null ? 0 : customer.Discount;
            if (!string.IsNullOrEmpty(customer.EmpID))
            {
                qb.ConditionBuilder.And(PatientTable.AccountIdColumn, customer.AccountId);
                qb.ConditionBuilder.And(PatientTable.EmpIDColumn, CriteriaEquation.Equal);                
                qb.Parameters.Add(PatientTable.EmpIDColumn.ColumnName, customer.EmpID);               

                qb.SelectBuilder.MakeDistinct = true;

                using (var reader = QueryExecuter.QueryAsyc(qb).Result)
                {
                    if (reader.Read())
                    {
                        disc = reader[PatientTable.DiscountColumn.ColumnName] as decimal? ?? 0;
                        return disc;
                    }
                }
            }

            else if (!string.IsNullOrEmpty(customer.Mobile) && !string.IsNullOrEmpty(customer.Name))
            {
                string query = $@"SELECT * FROM PATIENT WHERE name = '{customer.Name}' and SUBSTRING(Mobile,PATINDEX('%[^0]%',Mobile+'.'),LEN(Mobile)) = '{customer.Mobile}'";

                qb.SelectBuilder.MakeDistinct = true;
                qb = QueryBuilderFactory.GetQueryBuilder(query);

                using (var reader = QueryExecuter.QueryAsyc(qb).Result)
                {
                    if (reader.Read())
                    {
                        disc = reader[PatientTable.DiscountColumn.ColumnName] as decimal? ?? 0;
                        return disc;
                    }
                }
            }


            return disc;

        }

        public Task<List<Patient>> PatientList(Patient data)
        {

            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data.Name);
            }
            if (!string.IsNullOrEmpty(data.Mobile))
            {
                qb.ConditionBuilder.And(PatientTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, data.Mobile);
            }
            if (!string.IsNullOrEmpty(data.Code))
            {
                qb.ConditionBuilder.And(PatientTable.CodeColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.CodeColumn.ColumnName, data.Code);
            }
            if (!string.IsNullOrEmpty(data.Email))
            {
                qb.ConditionBuilder.And(PatientTable.EmailColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.EmailColumn.ColumnName, data.Email);
            }

            return List<Patient>(qb);
        }

        public static IEnumerable<Patient> GetPatientList(SearchResultRows[] searchResultRows)
        {
            var patientList = new List<Patient>();
            foreach (var patient in searchResultRows)
            {
                var obj = new Patient();
                SetPatientInfo(obj, patient.Patient, patient.PatientEmail, patient.PatientAddress, patient.PatientPhone);
                patientList.Add(obj);
            }

            return patientList;
        }

        public async Task<IEnumerable<Patient>> GetPatientListLocal(string data, string accountid, string instanceid, int department, int isActive)
        {

            //Added by violet
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Name", data);
            parms.Add("AccountId", accountid);
            parms.Add("InstanceId", instanceid);
            parms.Add("Dept", department);
            parms.Add("IsActive", isActive); 
             var result = await sqldb.ExecuteProcedureAsync<Patient>("usp_get_patient_details", parms);

            //Ends here

            //var result =await List<Patient>(qb);



            return result;

        }

        public async Task<IEnumerable<Patient>> GetPatientSalesListLocal(string data, string accountid, string instanceid, int department)
        {

            //Added by violet
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Name", data);
            parms.Add("AccountId", accountid);
            parms.Add("InstanceId", instanceid);
            parms.Add("Dept", department);
            var result = await sqldb.ExecuteProcedureAsync<Patient>("usp_get_Salespatient_details", parms);

            //Ends here

            //var result =await List<Patient>(qb);



            return result;

        }

        public async Task<IEnumerable<Patient>> GetPatientMobileList(string data, string accountid, string instanceid, int department)
        {

            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data))
            {
                qb.ConditionBuilder.And(PatientTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, data);
            }

            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, accountid);
            qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, instanceid);

            if (department == 0)
            {
                qb.ConditionBuilder.And(PatientTable.PatientTypeColumn, CriteriaEquation.NotEqual);
                //var PatientTypeNull = new CustomCriteria(PatientTable.PatientTypeColumn, CriteriaCondition.IsNull);
                //qb.ConditionBuilder.And(PatientTypeNull);
                qb.Parameters.Add(PatientTable.PatientTypeColumn.ColumnName, 2);
            }

            qb.SelectBuilder.SortBy(PatientTable.MobileColumn);

            var result = await List<Patient>(qb);
            return result;

        }

        public async Task<IEnumerable<Patient>> GetPatientListLocalId(string data, string accountid, string instanceid)
        {

            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data))
            {
                qb.ConditionBuilder.And(PatientTable.EmpIDColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.EmpIDColumn.ColumnName, data);
            }

            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, accountid);
            //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn); // commented by violet 15/05/2017
            //qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, instanceid);

            var result = await List<Patient>(qb);
            return result;

        }
        public async Task<List<Patient>> GetOfflinePatientList(Patient data, int department)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            // qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);

            // Id added by violet
            if (!string.IsNullOrEmpty(data.Id))
            {
                qb.ConditionBuilder.And(PatientTable.IdColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(PatientTable.IdColumn.ColumnName, data.Id);
            }
            else
            {
                if (!string.IsNullOrEmpty(data.Mobile))
                {
                    qb.ConditionBuilder.And(PatientTable.MobileColumn, CriteriaEquation.Equal);
                    qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, data.Mobile);

                }

                if (!string.IsNullOrEmpty(data.Name))
                {
                    qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Equal);
                    qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data.Name);
                }
                //Added the below code by Annadurai
                if (!string.IsNullOrEmpty(data.EmpID))
                {
                    qb.ConditionBuilder.And(PatientTable.EmpIDColumn, CriteriaEquation.Equal);
                    qb.Parameters.Add(PatientTable.EmpIDColumn.ColumnName, data.EmpID);
                }
                //Ends
            }

            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, data.AccountId);
            if (data.Status != null)
            {
                qb.ConditionBuilder.And(PatientTable.StatusColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(PatientTable.StatusColumn.ColumnName, data.Status);
            }
            //qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, data.InstanceId);

            if (department == 0)
            {
                //qb.ConditionBuilder.And(PatientTable.PatientTypeColumn, CriteriaEquation.NotEqual);
                //qb.Parameters.Add(PatientTable.PatientTypeColumn.ColumnName, 2);
                //var PatientTypeNull = new CustomCriteria(PatientTable.PatientTypeColumn, CriteriaCondition.IsNull);
                //qb.ConditionBuilder.Or(PatientTypeNull);

                var PatientTypeCondition = new CustomCriteria(PatientTable.PatientTypeColumn, CriteriaCondition.IsNull);
                string PatientType = "2";
                var cc1 = new CriteriaColumn(PatientTable.PatientTypeColumn, PatientType, CriteriaEquation.NotEqual);
                var col1 = new CustomCriteria(cc1, PatientTypeCondition, CriteriaCondition.Or);
                qb.ConditionBuilder.And(col1);
            }

            var res = await List<Patient>(qb);
            var result = res.Select(x =>
            {
                x.Name = x.Name == null ? "" : x.Name;
                x.ShortName = x.ShortName == null ? "" : x.ShortName;
                x.Mobile = x.Mobile == null ? "" : x.Mobile;
                x.DOB = x.DOB as DateTime? ?? x.DOB;
                x.EmpID = x.EmpID == null ? "" : x.EmpID;
                x.Id = x.Id == null ? "" : x.Id;
                x.City = x.City == null ? "" : x.City;
                x.Address = x.Address == null ? "" : x.Address;
                x.CustomerPaymentType = x.CustomerPaymentType == null ? "" : x.CustomerPaymentType;
                x.BalanceAmount = x.BalanceAmount == null ? 0 : x.BalanceAmount;
                x.LocationTypeName = x.LocationType == null ? Enum.GetName(typeof(LocationType), 0) : Enum.GetName(typeof(LocationType), x.LocationType);
                x.LoyaltyPoint = x.LoyaltyPoint == null ? 0 : x.LoyaltyPoint;
                return x;
            }).ToList();
            return result;
        }

        public static void SetPatientInfo(Patient patient, ExtPatient[] extPatients, Email[] extPatientEmail, Address[] extPatientAddress, Phone[] extPatientPhone)
        {
            if (extPatients == null
            || extPatients.Length == 0)
                return;

            var objPatients = extPatients[0];
            if (extPatientEmail.Length != 0)
            {
                var objPatientEmail = extPatientEmail[0];
                if (objPatientEmail.EmailID == "N/A")
                {
                    patient.Email = "";
                }
                else
                {
                    patient.Email = objPatientEmail.EmailID;
                    patient.isValidEmail = objPatientEmail.ValidIND == "Y" ? true : false;
                }
            }
            if (extPatientAddress.Length != 0)
            {
                var objPatientAddress = extPatientAddress[0];

                // Api dont accept null value to address so hardcoded N/A. 
                if (objPatientAddress.Address1 == "N/A")
                {
                    patient.Address = string.Empty;
                }
                else
                {
                    patient.Address = objPatientAddress.Address1;
                }
            }

            // var objPatientPhone = extPatientPhone[0];

            patient.Name = objPatients.FullName;
            patient.Gender = objPatients.Gender;
            //patient.Mobile = Convert.ToString(objPatientPhone.PhNumber);
            if (extPatientPhone.Count() > 0)
            {
                var objPatientPhone = extPatientPhone[0];
                patient.Mobile = Convert.ToString(objPatientPhone.PhNumber);
            }
            else
            {
                patient.Mobile = "0";
            }
            double dbdob = Convert.ToDouble(objPatients.DOB);
            patient.DOB = dbdob.UnixTimeStampToDateTime();
            patient.Id = objPatients.PatientID.ToString();
            patient.EmpID = Convert.ToString(objPatients.EmpID);
            patient.Age = objPatients.Age.HasValue ? (int)objPatients.Age.Value : 0;

        }

        public async Task<IEnumerable<Patient>> PatientSearchShortListLocal(string data, string accountid, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data))
            {
                qb.ConditionBuilder.And(PatientTable.ShortNameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.ShortNameColumn.ColumnName, data);
            }

            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, accountid);
            qb.ConditionBuilder.And(PatientTable.StatusColumn);         // Active customers only for Short Name Search
            qb.Parameters.Add(PatientTable.StatusColumn.ColumnName, 1);
            //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            //qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, instanceid);
            qb.SelectBuilder.SortBy(PatientTable.ShortNameColumn);

            var result = await List<Patient>(qb);
            return result;

        }
        //To get patient details by Id - Settu
        public async Task<Patient> GetPatientById(string PatientId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PatientTable.IdColumn, PatientId);

            var result = (await List<Patient>(qb)).FirstOrDefault();
            return result;
        }

        public async Task<bool> CustPaymentStatus(string accountId, string instanceId, string CustName, string CustMobile)
        {
            decimal? Debit = 0;
            decimal? Credit = 0;

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);
            parms.Add("Mobile", CustMobile);
            parms.Add("Name", CustName);            
            var paymentBalance = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_CustPaymentStatus", parms);
            var rows = paymentBalance;

            foreach (IDictionary<string, object> row in rows)
            {
                foreach (var pair in row)
                {
                    if (pair.Key == "Debit")
                        Debit = Convert.ToDecimal(pair.Value); //pair.Value as decimal? ?? 0;
                    if (pair.Key == "Credit")
                        Credit = Convert.ToDecimal(pair.Value); //pair.Value as decimal? ?? 0;
                }
            }

            if (Math.Round((decimal)Debit, MidpointRounding.AwayFromZero) == Math.Round((decimal)Credit, MidpointRounding.AwayFromZero))
                return true;
            else
                return false;
        }
    }
}
