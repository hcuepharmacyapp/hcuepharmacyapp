﻿using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.DbModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.External.Common;
using HQue.Contract.External.Doctor;
using HQue.ServiceConnector.Connector;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using DataAccess;
using System;
using System.Linq;

using Utilities.Helpers;
using HQue.Contract.Infrastructure.Settings;

namespace HQue.DataAccess.Master
{
    public class DoctorDataAccess : BaseDataAccess
    {
        public DoctorDataAccess(ISqlHelper sqlQueryExecuter,QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter,queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }

        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;

        public override Task<Doctor> Save<Doctor>(Doctor data)
        {
            return Insert(data, DoctorTable.Table);
        }

        public async Task<Doctor> CreateDoctor(Doctor data)
        {
            int count1 = await GetDoctorCount(data);
            if(count1 == 0)
            {
                return await Insert(data, DoctorTable.Table);
            }
            else
            {
                return null;
                
            }
        }

        public async Task<int> GetDoctorCount(Doctor data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(DoctorTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(DoctorTable.InstanceIdColumn, data.InstanceId);
            //qb.ConditionBuilder.And(DoctorTable.MobileColumn, data.Mobile);
            qb.ConditionBuilder.And(DoctorTable.ShortNameColumn, data.ShortName);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public Task<List<Doctor>> List()
        {
            return List<Doctor>(DoctorTable.Table);
        }

        //public async Task<IEnumerable<Doctor>> GetDoctorExternal(Doctor data)
        //{
        //    var sc = new RestConnector();
        //    var extData = new SearchRequest { DayCD = data.DayCD,SpecialityID=data.SpecialityID,Sort="FEES",Latitude="13.019954",Gender=data.Gender,Count=0,Longitude="80.218136"};
        //    var result = await sc.PostAsyc<SearchResponse>("http://d1lmwj8jm5d3bc.cloudfront.net/patients/getDoctors", extData);
        //    return GetDoctorList(result.rows);
        //}

        public async Task<IEnumerable<Doctor>> DoctorListExternal(Doctor data)
        {
            var sc = new RestConnector();
            var extData = new SearchRequest { DoctorName = data.Name, PageNumber = data.Page.PageNo, Radius=data.Radius, Count = 0, Sort = data.Sort, Latitude = data.Latitude, Longitude = data.Longitude };
            var result =
                await sc.PostAsyc<SearchResponse>("http://d1lmwj8jm5d3bc.cloudfront.net/patients/getDoctors", extData);
            return GetDoctorList(result.rows);
        }

        private IEnumerable<Doctor> GetDoctorList(SearchResultRows[] searchResultRows)
        {
            var doctorList = new List<Doctor>();
            foreach (var doctor in searchResultRows)
            {
                var obj = new Doctor();
                SetDoctorInfo(obj, doctor.Doctor);
                doctorList.Add(obj);
            }

            return doctorList;
        }

        private void SetDoctorInfo(Doctor doctor, ExtDoctor[] extDoctors)
        {
            if (extDoctors == null
            || extDoctors.Length == 0)
                return;

            var obj = extDoctors[0];

            doctor.Name = obj.FullName;
        }

        public Task<List<Doctor>> DoctorList(string doctorName)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(DoctorTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
            qb.Parameters.Add(DoctorTable.NameColumn.ColumnName, doctorName);
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(DoctorTable.NameColumn);
            return List<Doctor>(qb);
        }        


        public async Task<List<Doctor>> GetDoctorList(Doctor doc)
        {
            var Pageno = (doc.Page.PageSize * doc.Page.PageNo) - doc.Page.PageSize;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", doc.AccountId);
            parms.Add("InstanceId", doc.InstanceId);

            string mobile = null, doctorId = null;
            if(doc.Select != null)
            {
                if (doc.Select == "name")
                {
                    doctorId = doc.Values;
                }
                else if(doc.Select == "mobile")
                {
                    mobile = doc.Values;
                }
            }
            parms.Add("Id", doctorId);
            parms.Add("Mobile", mobile);
            parms.Add("PageNo", Pageno);
            parms.Add("PageSize", doc.Page.PageSize);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetDoctorList", parms);            
            var DoctorList = result.Select(x =>
            {
                var Doctor1 = new Doctor
                {
                    Id = x.Id.ToString(),
                    Name = x.Name.ToString(),
                    ShortName = x.ShortName.ToString(),
                    Mobile = Convert.ToString(x.Mobile),
                    Area = Convert.ToString(x.Area),
                    City = Convert.ToString(x.City),
                    State = Convert.ToString(x.State),
                    Pincode = Convert.ToString(x.Pincode),
                    Commission = x.Commission as decimal? ?? 0,
                    DoctorCount = Convert.ToInt16(x.DoctorCount) ?? 0,
                    GsTin = x.GsTin == null ? "": x.GsTin.ToString()
                };

                Doctor1.Address = string.Concat(Doctor1.Area, "\t", Doctor1.City, "\t", Doctor1.State);
                return Doctor1;
            });

            return DoctorList.ToList();

        }
        
        public async Task<Doctor> GetDoctorById(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(DoctorTable.IdColumn, id);
            var doctor = await List<Doctor>(qb);
            return doctor.FirstOrDefault();
        }

        public async Task<Doctor> UpdateDoctor(Doctor doc1)
        {

            bool Count = await GetDoctorUpdateCount(doc1);
            if (Count == false)
            {
                //doc1.isexists = true;
                //return doc1;

                var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(DoctorTable.IdColumn, doc1.Id);
                qb.ConditionBuilder.And(DoctorTable.AccountIdColumn, doc1.AccountId);
                qb.AddColumn(DoctorTable.NameColumn, DoctorTable.ShortNameColumn, DoctorTable.MobileColumn, DoctorTable.AreaColumn, DoctorTable.CityColumn, DoctorTable.StateColumn, DoctorTable.PincodeColumn, DoctorTable.CommissionColumn, DoctorTable.UpdatedAtColumn, DoctorTable.UpdatedByColumn,DoctorTable.GsTinColumn);
                qb.Parameters.Add(DoctorTable.NameColumn.ColumnName, doc1.Name);
                qb.Parameters.Add(DoctorTable.ShortNameColumn.ColumnName, doc1.ShortName);
                qb.Parameters.Add(DoctorTable.MobileColumn.ColumnName, doc1.Mobile);
                qb.Parameters.Add(DoctorTable.AreaColumn.ColumnName, doc1.Area);
                qb.Parameters.Add(DoctorTable.CityColumn.ColumnName, doc1.City);
                qb.Parameters.Add(DoctorTable.StateColumn.ColumnName, doc1.State);
                qb.Parameters.Add(DoctorTable.PincodeColumn.ColumnName, doc1.Pincode);
                qb.Parameters.Add(DoctorTable.CommissionColumn.ColumnName, doc1.Commission);
                qb.Parameters.Add(DoctorTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(DoctorTable.UpdatedByColumn.ColumnName, doc1.UpdatedBy);
                qb.Parameters.Add(DoctorTable.GsTinColumn.ColumnName, doc1.GsTin);
                await QueryExecuter.NonQueryAsyc(qb);

                return doc1;
            }
            else
            {
                return null;
            }
             
           
        }

        public async Task<bool> GetDoctorUpdateCount(Doctor data)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Count);
            //qb.ConditionBuilder.And(DoctorTable.AccountIdColumn, data.AccountId);
            //qb.ConditionBuilder.And(DoctorTable.InstanceIdColumn, data.InstanceId);
            ////qb.ConditionBuilder.And(DoctorTable.MobileColumn, data.Mobile);
            //qb.ConditionBuilder.And(DoctorTable.ShortNameColumn, data.ShortName);
            //return Convert.ToInt32(await SingleValueAsyc(qb));

            var query = $"SELECT COUNT(*) FROM {DoctorTable.TableName} WHERE {DoctorTable.IdColumn} != '{data.Id}' and {DoctorTable.ShortNameColumn} = '{data.ShortName}' and {DoctorTable.AccountIdColumn}='{data.AccountId}' and {DoctorTable.InstanceIdColumn}='{data.InstanceId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var count = Convert.ToInt32(await SingleValueAsyc(qb));
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public async Task<Doctor> SaveDefaultDoctor(Doctor doc)
        {
            int defaultDoctorCount = await CountDefaultDoctor(doc);

            if(defaultDoctorCount > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(DefaultDoctorTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(DefaultDoctorTable.InstanceIdColumn, doc.InstanceId);
                qb.ConditionBuilder.And(DefaultDoctorTable.AccountIdColumn, doc.AccountId);
                qb.AddColumn(DefaultDoctorTable.DoctorIdColumn, DefaultDoctorTable.UpdatedAtColumn, DefaultDoctorTable.UpdatedByColumn);
                qb.Parameters.Add(DefaultDoctorTable.DoctorIdColumn.ColumnName, doc.Id);
                qb.Parameters.Add(DefaultDoctorTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(DefaultDoctorTable.UpdatedByColumn.ColumnName, doc.UpdatedBy);
                await QueryExecuter.NonQueryAsyc(qb);
            }
            else
            {
                DefaultDoctor DD = new DefaultDoctor();
                DD.AccountId = doc.AccountId;
                DD.InstanceId = doc.InstanceId;
                DD.DoctorId = doc.Id;
                DD.CreatedAt = DateTime.UtcNow;
                DD.UpdatedAt = DateTime.UtcNow;
                DD.CreatedBy = doc.CreatedBy;
                DD.UpdatedBy = doc.UpdatedBy;

                await Insert(DD, DefaultDoctorTable.Table);
            }
            return doc;
        }

        private async Task<int> CountDefaultDoctor(Doctor doc)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DefaultDoctorTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(DefaultDoctorTable.AccountIdColumn, doc.AccountId);
            qb.ConditionBuilder.And(DefaultDoctorTable.InstanceIdColumn, doc.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public async Task<Doctor> GetDefaultDoctor(string AccId, string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Select);

            qb.AddColumn(DoctorTable.IdColumn, DoctorTable.NameColumn, DoctorTable.ShortNameColumn, DoctorTable.MobileColumn);
            qb.JoinBuilder.Join(DefaultDoctorTable.Table, DefaultDoctorTable.DoctorIdColumn, DoctorTable.IdColumn);
            
            qb.ConditionBuilder.And(DefaultDoctorTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(DefaultDoctorTable.InstanceIdColumn, InsId);

            var result = await List<Doctor>(qb);

            return result.FirstOrDefault();
        }

        public async Task<Doctor> ResetDefaultDoctor(Doctor doc)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DefaultDoctorTable.Table, OperationType.Delete);
            qb.ConditionBuilder.And(DefaultDoctorTable.AccountIdColumn, doc.AccountId);
            qb.ConditionBuilder.And(DefaultDoctorTable.InstanceIdColumn, doc.InstanceId);
            qb.ConditionBuilder.And(DefaultDoctorTable.DoctorIdColumn, doc.Id);
            await QueryExecuter.NonQueryAsyc(qb);
            return doc;
        }

       public Task<List<Doctor>> LocalDoctorList(string doctorName,string accountid,string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(DoctorTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
            qb.Parameters.Add(DoctorTable.NameColumn.ColumnName, doctorName);
            qb.ConditionBuilder.And(DoctorTable.AccountIdColumn);
            qb.Parameters.Add(DoctorTable.AccountIdColumn.ColumnName, accountid);
            qb.ConditionBuilder.And(DoctorTable.InstanceIdColumn);
            qb.Parameters.Add(DoctorTable.InstanceIdColumn.ColumnName, instanceid);
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(DoctorTable.NameColumn);
            return List<Doctor>(qb);
        }

        public Task<List<Doctor>> DoctorListProfile(string doctorName, string accountid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(DoctorTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
            qb.Parameters.Add(DoctorTable.NameColumn.ColumnName, doctorName);
            qb.ConditionBuilder.And(DoctorTable.AccountIdColumn);
            qb.Parameters.Add(DoctorTable.AccountIdColumn.ColumnName, accountid);            
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(DoctorTable.NameColumn);
            return List<Doctor>(qb);
        }


        public Task<List<Doctor>> CheckUniqueDoctor(Doctor data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Select);

            if (!string.IsNullOrEmpty(data.Mobile))
            {
                qb.ConditionBuilder.And(DoctorTable.MobileColumn);
                qb.Parameters.Add(DoctorTable.MobileColumn.ColumnName, data.Mobile);
                qb.ConditionBuilder.And(DoctorTable.NameColumn);
                qb.Parameters.Add(DoctorTable.NameColumn.ColumnName, data.Name);
                qb.ConditionBuilder.And(DoctorTable.InstanceIdColumn);
                qb.Parameters.Add(DoctorTable.InstanceIdColumn.ColumnName, data.InstanceId);
            }
            else
            {
                qb.ConditionBuilder.And(DoctorTable.NameColumn);
                qb.Parameters.Add(DoctorTable.NameColumn.ColumnName, data.Name);
                qb.ConditionBuilder.And(DoctorTable.InstanceIdColumn);
                qb.Parameters.Add(DoctorTable.InstanceIdColumn.ColumnName, data.InstanceId);
            }

            return List<Doctor>(qb);
        }


        public Task<List<Doctor>> LocalShortDoctorList(string ShortDoctorName, string accountid, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DoctorTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(DoctorTable.ShortNameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
            qb.Parameters.Add(DoctorTable.ShortNameColumn.ColumnName, ShortDoctorName);
            qb.ConditionBuilder.And(DoctorTable.AccountIdColumn);
            qb.Parameters.Add(DoctorTable.AccountIdColumn.ColumnName, accountid);
            qb.ConditionBuilder.And(DoctorTable.InstanceIdColumn);
            qb.Parameters.Add(DoctorTable.InstanceIdColumn.ColumnName, instanceid);
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(DoctorTable.ShortNameColumn);
            return List<Doctor>(qb);
        }

        
    }
}
