﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Dashboard;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Helpers;
using DataAccess.Criteria;
using Utilities.Enum;
// Done 07-01-2017
using Dapper;
using DataAccess;
using System.Linq;

namespace HQue.DataAccess.Dashboard
{
    public class DashboardDataAccess : BaseDataAccess
    {
        // Done 07-01-2017
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;

        public DashboardDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory,ConfigHelper configHelper)
            : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            // Done 07-01-2017
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }

        public override Task<T> Save<T>(T data)
        {
            throw new NotImplementedException();
        }

        //public async Task<BarGraph> GetPendingLeads(string accountId, string instanceId)
        //{

        //    var qb = QueryBuilderFactory.GetQueryBuilder(LeadsTable.Table, OperationType.Select);

        //    var cc = new CustomCriteria(LeadsTable.LeadStatusColumn, CriteriaCondition.IsNull);

        //    qb.AddColumn(DbColumn.CustomColumn("REPLACE(CONVERT(VARCHAR(32),LeadDate,106),' ','-') AS LeadDate"),
        //        DbColumn.CountColumn("Count"));
        //    qb.ConditionBuilder.And(LeadsTable.InstanceIdColumn, instanceId).And(cc);
        //    qb.SelectBuilder.GroupBy(DbColumn.CustomColumn("REPLACE(CONVERT(VARCHAR(32),LeadDate,106),' ','-')"));

        //    var list = new BarGraph();

        //    using (var reader = await QueryExecuter.QueryAsyc(qb))
        //    {
        //        while (reader.Read())
        //        {
        //            list.Labels.Add(reader["LeadDate"].ToString());
        //            list.Data.Add(Convert.ToDecimal(reader["Count"].ToString()));
        //        }
        //    }

        //    return list;

        
        //}

        public async Task<BarGraph> GetPendingLeads(string accountId, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);

            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardPendingLeads", parms);

            var list = new BarGraph();

            result.ToList().ForEach(x=>{
                list.Labels.Add(x.LeadDate);
                list.Data.Add(Convert.ToDecimal(x.Count.ToString()));
            });
            
            return list;
        }

        // Newly Added Gavaskar 27-10-2016 Start 
        //public async Task<IEnumerable<NameValue>> Sales(object instanceId, object accountId)
        //{
        //           string query =
        //               $@"select Name, sum(Amount)Amount, sum(ReturnedAmount)ReturnedAmount from (

        //               select 'Today' Name,sum(Amount) Amount,'0.00' as ReturnedAmount from (
        //               select 'Today' As Name,
        //                round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) 
        //			* salesitem.Quantity - 
        //((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * sales.Discount / 100))-sum(salesitem.Quantity * 
        //(case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount, 
        //               '0.00' as ReturnedAmount
        //               from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id
        //               --left join vendorpurchaseitem on productstock.id=vendorpurchaseitem.productstockid 
        //               WHERE sales.InstanceId = '{instanceId}' and sales.AccountId ='{accountId}'
        //               AND Convert(date,sales.invoicedate) =dateadd(day,datediff(day,0,GETDATE()),0) and sales.Cancelstatus is NULL
        //               group by sales.InvoiceNo ) a group by a.name

        //               union 
        //               select 'Today' As Name, '0.00' as Amount, '0.00' as ReturnedAmount
        //               union 

        //               select Name,'0.00'  As Amount,sum(ReturnedAmount)ReturnedAmount from (

        //               select 'Today' As Name,
        //               '0.00'  As Amount,
        //               round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
        //               else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity else 
        //               (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) -( sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity else 
        //               ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end)))),0) as ReturnedAmount
        //               from SalesReturn as sr inner JOIN Sales as s ON s.Id = sr.SalesId
        //               inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
        //               inner join ProductStock as ps on ps.id = sri.ProductStockId
        //               inner join Product as p on p.Id = ps.ProductId
        //               WHERE sr.InstanceId = '{instanceId}' and sr.AccountId ='{accountId}'
        //               AND Convert(date,sr.ReturnDate) =dateadd(day,datediff(day,0,GETDATE()),0) and s.Cancelstatus is NULL
        //               group by s.InvoiceNo ) e group by e.name
        //               ) Today group by Name


        //               union all

        //               select Name, sum(Amount)Amount, sum(ReturnedAmount)ReturnedAmount from (

        //               select 'Yesterday' Name,sum(Amount) Amount,'0.00' as ReturnedAmount from (
        //               select 'Yesterday' As Name,
        //                round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) 
        //			* salesitem.Quantity - 
        //((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * sales.Discount / 100))-sum(salesitem.Quantity * 
        //(case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount,
        //               '0.00' as ReturnedAmount
        //               from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id 
        //               --left join vendorpurchaseitem on productstock.id=vendorpurchaseitem.productstockid 
        //               WHERE sales.InstanceId = '{instanceId}' and sales.AccountId ='{accountId}'
        //               AND Convert(date,sales.invoicedate) =dateadd(day,datediff(day,1,GETDATE()),0) and sales.Cancelstatus is NULL
        //               group by sales.InvoiceNo ) b group by b.name

        //               union 
        //               select 'Yesterday' As Name, '0.00' as Amount, '0.00' as ReturnedAmount

        //               union 

        //               select 'Yesterday' As Name,'0.00'  As Amount , sum(ReturnedAmount) from (
        //               select 'Yesterday' As Name,
        //               '0.00'  As Amount,
        //               round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
        //               else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity else 
        //               (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) -( sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity else 
        //               ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end)))),0) as ReturnedAmount
        //               from SalesReturn as sr inner JOIN Sales as s ON s.Id = sr.SalesId
        //               inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
        //               inner join ProductStock as ps on ps.id = sri.ProductStockId
        //               inner join Product as p on p.Id = ps.ProductId
        //               WHERE sr.InstanceId = '{instanceId}' and sr.AccountId ='{accountId}'
        //               AND Convert(date,sr.ReturnDate) =dateadd(day,datediff(day,1,GETDATE()),0) and s.Cancelstatus is NULL
        //               group by s.InvoiceNo ) f group by f.name
        //               ) Yesterday group by Name

        //               union all

        //               select Name, sum(Amount)Amount, sum(ReturnedAmount)ReturnedAmount from (
        //               select  'Last Month' As Name,sum(Amount) Amount,'0.00' as ReturnedAmount from (
        //               select 'Last Month' As Name,
        //                round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) 
        //			* salesitem.Quantity - 
        //((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * sales.Discount / 100))-sum(salesitem.Quantity * 
        //(case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount,
        //               '0.00' as ReturnedAmount
        //               from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id 
        //               --left join vendorpurchaseitem on productstock.id=vendorpurchaseitem.productstockid 
        //               WHERE sales.InstanceId = '{instanceId}' and sales.AccountId ='{accountId}'
        //               AND Convert(date,sales.invoicedate) between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0))) and sales.Cancelstatus is NULL
        //               --between  CONVERT(varchar,dateadd(d,-(day(dateadd(m,-1,getdate()-2))),dateadd(m,-1,getdate()-1)),106) and CONVERT(varchar,dateadd(d,-(day(getdate())),getdate()),106) 
        //               group by sales.InvoiceNo) c group by c.name
        //               union 
        //               select 'Last Month' As Name, '0.00' as Amount, '0.00' as ReturnedAmount
        //               union 

        //               select 'Last Month' As Name,'0.00'  As Amount,sum(ReturnedAmount) from (
        //               select 'Last Month' As Name,
        //               '0.00'  As Amount,
        //               round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
        //               else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity else 
        //               (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) -(sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity else 
        //               ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end)))),0) as ReturnedAmount
        //               from SalesReturn as sr inner JOIN Sales as s ON s.Id = sr.SalesId
        //               inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
        //               inner join ProductStock as ps on ps.id = sri.ProductStockId
        //               inner join Product as p on p.Id = ps.ProductId
        //               WHERE sr.InstanceId = '{instanceId}' and sr.AccountId ='{accountId}'
        //               AND Convert(date,sr.ReturnDate) between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))  and s.Cancelstatus is NULL
        //               --between  CONVERT(varchar,dateadd(d,-(day(dateadd(m,-1,getdate()-2))),dateadd(m,-1,getdate()-1)),106) and CONVERT(varchar,dateadd(d,-(day(getdate())),getdate()),106)
        //               group by s.InvoiceNo ) g group by g.name
        //               ) LastMonth group by Name

        //               union all

        //               select Name, sum(Amount)Amount, sum(ReturnedAmount)ReturnedAmount from (
        //               select Name,sum(Amount)Amount,'0.00' ReturnedAmount from (
        //               select 'Financial Year' As Name,
        //                round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) 
        //			* salesitem.Quantity - 
        //((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * sales.Discount / 100))-sum(salesitem.Quantity * 
        //(case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount,
        //               '0.00' as ReturnedAmount
        //               from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id 
        //               --left join vendorpurchaseitem on productstock.id=vendorpurchaseitem.productstockid 
        //               WHERE sales.InstanceId = '{instanceId}' and sales.AccountId ='{accountId}'
        //               AND Convert(date,sales.invoicedate) between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))
        //               and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ))))) and sales.Cancelstatus is NULL
        //               group by sales.InvoiceNo ) d group by d.name

        //               union 
        //               select 'Financial Year' As Name, '0.00' as Amount, '0.00' as ReturnedAmount 
        //               union 

        //               select Name,'0.00' Amount,sum(ReturnedAmount)ReturnedAmount from (
        //               select 'Financial Year' As Name,
        //               '0.00'  As Amount,
        //               round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
        //               else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity else 
        //               (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) - (sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity else 
        //               ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end)))),0) as ReturnedAmount
        //               from SalesReturn as sr inner JOIN Sales as s ON s.Id = sr.SalesId
        //               inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
        //               inner join ProductStock as ps on ps.id = sri.ProductStockId
        //               inner join Product as p on p.Id = ps.ProductId
        //               WHERE sr.InstanceId = '{instanceId}' and sr.AccountId ='{accountId}'
        //               AND Convert(date,sr.ReturnDate) between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))
        //               and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ))))) and s.Cancelstatus is NULL
        //               group by s.InvoiceNo ) h group by h.name
        //               ) Financial group by Name
        //              ";


        //var qb = QueryBuilderFactory.GetQueryBuilder(query);
        //    var list = new List<NameValue>();
        //    using (var reader = await QueryExecuter.QueryAsyc(qb))
        //    {
        //        while (reader.Read())
        //        {
        //            var item = new NameValue
        //            {
        //                Name = reader["Name"].ToString(),
        //                SalesReturnAmount = reader["ReturnedAmount"] == DBNull.Value ? 0 : Math.Round((decimal)reader["ReturnedAmount"], 0, MidpointRounding.AwayFromZero),
        //                Amount = reader["Amount"] == DBNull.Value ? 0 : Math.Round((decimal)reader["Amount"], 0, MidpointRounding.AwayFromZero),
        //                // TotalDiscount= reader["TotalDiscount"]== DBNull.Value ? 0 : Math.Round((decimal)reader["TotalDiscount"],0,MidpointRounding.AwayFromZero),
        //                SalesNetAmount = reader["Amount"] == DBNull.Value ? 0 : Math.Round((decimal)reader["Amount"], 0, MidpointRounding.AwayFromZero) - (reader["ReturnedAmount"] == DBNull.Value ? 0 : Math.Round((decimal)reader["ReturnedAmount"], 0, MidpointRounding.AwayFromZero)),
        //                SalesGrossAmount = reader["Amount"] == DBNull.Value ? 0 : Math.Round((decimal)reader["Amount"], 0, MidpointRounding.AwayFromZero)
        //            };
        //            list.Add(item);
        //        }
        //    }
        //    decimal netamount = 0.00M;
        //    var l = list.GetRange(3, 1);

        //    foreach (var item in l)
        //    {
        //        if (item.Name == "Avg")
        //        {

        //            decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays(instanceId, accountId));
        //            item.AverageSales = item.AverageSales / noOfDays;
        //        }

        //        else
        //        {


        //            netamount += item.SalesNetAmount;

        //            decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays(instanceId, accountId));

        //            if (noOfDays == 0)
        //            {
        //                item.AverageSales = noOfDays;
        //            }
        //            else
        //            {
        //                item.AverageSales = netamount / noOfDays;
        //            }

        //        }
        //        list.Add(item);
        //    }
        //
        //return List;
        //}

        // Newly Added Manivannan 23-02-2017 
        public async Task<IEnumerable<NameValue>> Sales(object instanceId, object accountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);

            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardSales", parms);

            var iList = result.Select(x =>
            {
                var item = new NameValue
                {
                    Name = x.Name,
                    SalesReturnAmount = x.SalesReturnAmount as decimal? ?? 0, //Math.Round((decimal)x.SalesReturnAmount, 0, MidpointRounding.AwayFromZero),
                    Amount = x.Amount as decimal? ?? 0, //Math.Round((decimal)x.Amount, 0, MidpointRounding.AwayFromZero),
                    // TotalDiscount= reader["TotalDiscount"]== DBNull.Value ? 0 : Math.Round((decimal)reader["TotalDiscount"],0,MidpointRounding.AwayFromZero),
                    SalesNetAmount = (x.Amount as decimal? ?? 0) - (x.SalesReturnAmount as decimal? ?? 0),//Math.Round((decimal)x.Amount, 0, MidpointRounding.AwayFromZero) - (Math.Round((decimal)x.SalesReturnAmount, 0, MidpointRounding.AwayFromZero)),
                    SalesGrossAmount = x.Amount as decimal? ?? 0, //Math.Round((decimal)x.Amount, 0, MidpointRounding.AwayFromZero)
                };
                return item;
            });

            var list = iList.ToList();

            decimal netamount = 0.00M;
            var l = list.GetRange(3, 1);

            foreach (var item in l)
            {
                decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays(instanceId, accountId));

                if (item.Name == "Avg")
                {
                    //decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays(instanceId, accountId));
                    item.AverageSales = item.AverageSales / noOfDays;
                }
                else
                {
                    netamount += item.SalesNetAmount;

                    //decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays(instanceId, accountId));

                    if (noOfDays == 0)
                    {
                        item.AverageSales = noOfDays;
                    }
                    else
                    {
                        item.AverageSales = netamount / noOfDays;
                    }
                }
                list.Add(item);
            }

            return list.ToList();
        }

        //public async Task<decimal> GetFinancialYearDays(object instanceid, object accountid)
        //{
        //    string query = $@" select sum(FinancialYear) as FinancialYear from (
        //    SELECT distinct DATEDIFF(day, (SELECT top 1 CONVERT(date, createdat)
        //    as date from Sales where convert(date, createdat) >=
        //    ( select CONVERT(date, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())
        //    - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))
        //    and convert(date, createdat)<=
        //    (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
        //    getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))
        //    order by CreatedAt), (select top 1 case when(select convert(date, GETDATE())) <= (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(
        //    dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
        //    getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))
        //    then (select  convert(date, GETDATE())) end from Sales)) as FinancialYear
        //    from Sales where instanceid = '{instanceid}' and accountid ='{accountid}'

        //    UNION ALL

        //    SELECT '0' as FinancialYear ) as Financial
        //    ";

        //    var qb = QueryBuilderFactory.GetQueryBuilder(query);
        //    return Convert.ToDecimal(await QueryExecuter.SingleValueAsyc(qb));
        //}

        public async Task<decimal> GetFinancialYearDays(object instanceid, object accountid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", accountid);
            parms.Add("InstanceId", instanceid);

            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardFinYearSales", parms);
            return result.First().FinancialYear;
        }


        //        public async Task<IEnumerable<NameValue>> Purchase(string instanceId, string accountId)
        //        {
        //            var filterFromDate = CustomDateTime.IST.ToFormat();
        //            var filterToDate = CustomDateTime.IST.ToFormat();

        //            string query =
        //                $@"select Name, sum(Amount)Amount, sum(ReturnAmount)ReturnAmount from (
        //                SELECT Name,Sum(Amount) as Amount,0 as ReturnAmount from (SELECT 'Today' as Name,Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
        //                -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
        //                +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
        //                ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
        //                * (CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END))) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
        //                '0.00' as ReturnAmount
        //                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId inner join ProductStock ps
        //                on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
        //                where v.InstanceId='{instanceId}' and v.AccountId ='{accountId}'
        //                AND Convert(date,v.CreatedAt)  = dateadd(day,datediff(day,0,GETDATE()),0) group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a group by a.name

        //                union

        //                SELECT 'Today' as Name,'0.00' as Amount,
        //                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) * VendorReturnItem.Quantity)))),0)  as ReturnAmount
        //                FROM VendorReturnItem
        //INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
        //INNER JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
        //INNER JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
        //INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
        //INNER JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
        //INNER JOIN Product ON Product.Id = ProductStock.ProductId
        //                where VendorPurchase.InstanceId='{instanceId}' and VendorPurchase.AccountId ='{accountId}'
        //                AND Convert(date,VendorReturn.ReturnDate)  = dateadd(day,datediff(day,0,GETDATE()),0) 
        //                ) Today group by Name    

        //                union all

        //                select Name, sum(Amount)Amount, sum(ReturnAmount)ReturnAmount from (
        //                SELECT Name,Sum(Amount) as Amount,0 as ReturnAmount from (SELECT 'Yesterday' as Name,Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
        //                -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
        //                +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
        //                ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
        //                * (CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END)))+ (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
        //                '0.00' as ReturnAmount
        //                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId inner join ProductStock ps
        //                on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
        //                where v.InstanceId='{instanceId}' and v.AccountId ='{accountId}'
        //                AND Convert(date,v.CreatedAt)  = dateadd(day,datediff(day,1,GETDATE()),0) group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a group by a.name

        //                union

        //                SELECT 'Yesterday' as Name,'0.00' as Amount,
        //                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) * VendorReturnItem.Quantity)))),0)  as ReturnAmount
        //               FROM VendorReturnItem
        //INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
        //INNER JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
        //INNER JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
        //INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
        //INNER JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
        //INNER JOIN Product ON Product.Id = ProductStock.ProductId
        //                where VendorPurchase.InstanceId='{instanceId}' and VendorPurchase.AccountId ='{accountId}'
        //                AND Convert(date,VendorReturn.ReturnDate)  = dateadd(day,datediff(day,1,GETDATE()),0) 
        //                ) Yesterday group by Name  

        //                union all

        //                select Name, sum(Amount)Amount, sum(ReturnAmount)ReturnAmount from (
        //                SELECT Name,Sum(Amount) as Amount,0 as ReturnAmount from (SELECT 'Last Month' as Name,Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
        //                -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
        //                +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
        //                ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
        //                * (CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END)))+ (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
        //                '0.00' as ReturnAmount
        //                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId inner join ProductStock ps
        //                on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
        //                where v.InstanceId='{instanceId}' and v.AccountId ='{accountId}'
        //                AND Convert(date,v.CreatedAt)  between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0))) group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a group by a.name

        //                union

        //                SELECT 'Last Month' as Name,'0.00' as Amount,
        //               Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) * VendorReturnItem.Quantity)))),0)  as ReturnAmount
        //                FROM VendorReturnItem
        //INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
        //INNER JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
        //INNER JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
        //INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
        //INNER JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
        //INNER JOIN Product ON Product.Id = ProductStock.ProductId
        //                where VendorPurchase.InstanceId='{instanceId}' and VendorPurchase.AccountId ='{accountId}'
        //                AND Convert(date,VendorReturn.ReturnDate)   between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))
        //                ) LastMonth group by Name  

        //                union all

        //                select Name, sum(Amount)Amount, sum(ReturnAmount)ReturnAmount from (
        //                SELECT Name,Sum(Amount) as Amount,0 as ReturnAmount from (SELECT 'Financial Year' as Name,Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
        //                -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
        //                +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
        //                ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
        //                * (CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END)))+ (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
        //                '0.00' as ReturnAmount
        //                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId inner join ProductStock ps
        //                on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
        //                where v.InstanceId='{instanceId}' and v.AccountId ='{accountId}'
        //                AND Convert(date,v.CreatedAt)  between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))
        //                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ))))) group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a group by a.name

        //                union

        //                SELECT 'Financial Year' as Name,'0.00' as Amount,
        //                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) * VendorReturnItem.Quantity)))),0)  as ReturnAmount
        //                FROM VendorReturnItem
        //INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
        //INNER JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
        //INNER JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
        //INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
        //INNER JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
        //INNER JOIN Product ON Product.Id = ProductStock.ProductId
        //                where VendorPurchase.InstanceId='{instanceId}' and VendorPurchase.AccountId ='{accountId}'
        //                AND Convert(date,VendorReturn.ReturnDate)  between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))
        //                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))))
        //                ) FinancialYear group by Name";

        //            var qb = QueryBuilderFactory.GetQueryBuilder(query);

        //            var list = new List<NameValue>();

        //            using (var reader = await QueryExecuter.QueryAsyc(qb))
        //            {
        //                while (reader.Read())
        //                {
        //                    var item = new NameValue
        //                    {
        //                        Name = reader["Name"].ToString(),
        //                        Amount = reader["Amount"] == DBNull.Value ? 0 : (decimal)reader["Amount"],
        //                        PurchaseReturnAmount = reader["ReturnAmount"] == DBNull.Value ? 0 : (decimal)reader["ReturnAmount"],
        //                        PurchaseNetAmount = (reader["Amount"] == DBNull.Value ? 0 : (decimal)reader["Amount"]) - (reader["ReturnAmount"] == DBNull.Value ? 0 : (decimal)reader["ReturnAmount"]),
        //                        PurchaseGrossAmount = reader["Amount"] == DBNull.Value ? 0 : (decimal)reader["Amount"],

        //                    };
        //                    // decimal? Value = reader["Amount"] as decimal? ?? 0;
        //                    list.Add(item);
        //                }
        //            }


        //            decimal netamount = 0.00M;
        //            var l = list.GetRange(3, 1);

        //            foreach (var item in l)
        //            {
        //                if (item.Name == "Avg")
        //                {

        //                    decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays1(instanceId, accountId));
        //                    item.AveragePurchase = item.AveragePurchase / noOfDays;
        //                }

        //                else
        //                {


        //                    netamount += item.PurchaseNetAmount;

        //                    decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays1(instanceId, accountId));

        //                    if (noOfDays == 0)
        //                    {
        //                        item.AveragePurchase = noOfDays;
        //                    }
        //                    else
        //                    {
        //                        item.AveragePurchase = netamount / noOfDays;
        //                    }


        //                }
        //                list.Add(item);
        //            }



        //            return list;

        //        }

        public async Task<IEnumerable<NameValue>> Purchase(string instanceId, string accountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);

            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardPurchase", parms);

            var iList = result.Select(x =>
            {
                var item = new NameValue
                {
                    Name = x.Name.ToString(),
                    Amount = (decimal)x.Amount,
                    PurchaseReturnAmount = (decimal)x.PurchaseReturnAmount,
                    PurchaseNetAmount = ((decimal)x.Amount) - ((decimal)x.PurchaseReturnAmount),
                    PurchaseGrossAmount = (decimal)x.Amount
                };
                return item;
            });

            var list = iList.ToList();
            
            decimal netamount = 0.00M;
            var l = list.GetRange(3, 1);

            foreach (var item in l)
            {
                decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays1(instanceId, accountId));

                if (item.Name == "Avg")
                {
                    //decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays1(instanceId, accountId));
                    item.AveragePurchase = item.AveragePurchase / noOfDays;
                }
                else
                {
                    netamount += item.PurchaseNetAmount;

                    //decimal noOfDays = Convert.ToDecimal(await GetFinancialYearDays1(instanceId, accountId));

                    if (noOfDays == 0)
                    {
                        item.AveragePurchase = noOfDays;
                    }
                    else
                    {
                        item.AveragePurchase = netamount / noOfDays;
                    }
                }
                list.Add(item);
            }



            return list;

        }

        //public async Task<decimal> GetFinancialYearDays1(object instanceid, object accountid)
        //{
        //    //           string query = $@"SELECT distinct DATEDIFF(day, (SELECT top 1 CONVERT(date, createdat)
        //    //                 as date from VendorPurchase where convert(date, createdat) >=
        //    //               ( select CONVERT(date, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())
        //    //                - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))
        //    //               and convert(date, createdat)<=
        //    //               (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
        //    //                getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))
        //    //                order by CreatedAt), (select top 1 case when(select convert(date, GETDATE())) <= (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(
        //    //                dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
        //    //                 getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))

        //    //		  then
        //    //                (select  convert(date, GETDATE())) end from VendorPurchase))

        //    //                from VendorPurchase where instanceid = '{instanceid}' and accountid ='{accountid}'

        //    //";

        //    string query = $@"select sum(FinancialYear) as FinancialYear from (
        //    SELECT distinct DATEDIFF(day, (SELECT top 1 CONVERT(date, createdat)
        //    as date from VendorPurchase where convert(date, createdat) >=
        //    ( select CONVERT(date, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())
        //    - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))
        //    and convert(date, createdat)<=
        //    (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
        //    getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))
        //    order by CreatedAt), (select top 1 case when(select convert(date, GETDATE())) <= (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(
        //    dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
        //    getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))
        //    then (select  convert(date, GETDATE())) end from VendorPurchase)) as FinancialYear
        //    from VendorPurchase where instanceid = '{instanceid}' and accountid ='{accountid}'

        //    UNION ALL

        //    SELECT '0' as FinancialYear ) as Financial

        //    ";

        //    var qb = QueryBuilderFactory.GetQueryBuilder(query);
        //    return Convert.ToDecimal(await QueryExecuter.SingleValueAsyc(qb));
        //}

        public async Task<decimal> GetFinancialYearDays1(object instanceid, object accountid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", accountid);
            parms.Add("InstanceId", instanceid);

            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardFinYearPurchase", parms);
            return result.First().FinancialYear;
        }

        // Newly Added Gavaskar 27-10-2016 End 

        // Newly Added Gavaskar 15-11-2016 Start

        //        public async Task<IEnumerable<NameValue>> CurrentStock(string accountId, string instanceId)
        //        {
        //            string query =$@"select 'WITH TAX' as Name, Convert(decimal(18,2),sum((ISNULL(vpi.PurchasePrice,ProductStock.PurchasePrice) * ProductStock.Stock)),0) as CurrentStockAmount
        //from ProductStock Inner JOIN Product ON Product.Id = ProductStock.ProductId  
        //left outer JOIN (select distinct productstockid, PurchasePrice from vendorpurchaseitem where InstanceId = '{instanceId}') vpi ON (ProductStock.Id = vpi.productstockid)  LEFT JOIN Vendor ON Vendor.Id = ProductStock.VendorId 
        //WHERE ProductStock.InstanceId = '{instanceId}' and ProductStock.AccountId = '{accountId}'

        //UNION ALL

        //select 'WITHOUT TAX' as Name, Convert(decimal(18,2),sum((ISNULL(vpi.PurchasePrice,ProductStock.PurchasePrice) * ProductStock.Stock) - ((ISNULL(vpi.PurchasePrice,ProductStock.PurchasePrice) * ProductStock.Stock) 
        //* (ProductStock.VAT/(100 + ProductStock.VAT)))),0) as CurrentStockAmount from ProductStock Inner JOIN Product ON Product.Id = ProductStock.ProductId  
        //left outer JOIN (select distinct productstockid, PurchasePrice from vendorpurchaseitem where InstanceId = '{instanceId}') vpi ON (ProductStock.Id = vpi.productstockid) 
        //LEFT JOIN Vendor ON Vendor.Id = ProductStock.VendorId 
        //WHERE ProductStock.InstanceId = '{instanceId}' and ProductStock.AccountId = '{accountId}'

        //";

        //            var qb = QueryBuilderFactory.GetQueryBuilder(query);

        //            var list = new List<NameValue>();

        //            using (var reader = await QueryExecuter.QueryAsyc(qb))
        //            {
        //                while (reader.Read())
        //                {
        //                    var item = new NameValue
        //                    {
        //                        Name = reader["Name"].ToString(),
        //                        CurrentStockAmount = reader["CurrentStockAmount"] == DBNull.Value ? 0 : (decimal)reader["CurrentStockAmount"]
        //                    };
        //                    list.Add(item);
        //                }
        //            }
        //            return list;

        //        }

        public async Task<IEnumerable<NameValue>> CurrentStock(string accountId, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);

            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardCurrentStock", parms);

            var list = result.ToList();

            return list;

        }

        // Newly Added Gavaskar 15-11-2016 End

        //public async Task<IEnumerable<NameValue>> GetAboutToExpireProduct(object instanceId)
        //{

        //    int getMonth = await GetMonth(instanceId.ToString());
        //    int month = 0;          
        //    if (getMonth > 0)
        //    {
        //        month = getMonth;
        //    }
        //    else
        //    {
        //        month = 1;
        //    }

        //    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
        //    qb.SelectBuilder.SetTop(10).GroupBy(ProductTable.NameColumn,ProductStockTable.ExpireDateColumn);
        //    qb.SelectBuilder.SortBy(ProductStockTable.ExpireDateColumn);

        //    qb.AddColumn(ProductTable.NameColumn, ProductStockTable.ExpireDateColumn)
        //        .JoinBuilder
        //        .Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);

        //    qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId)
        //        .And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.AddMonths(month).ToFormat(), CriteriaEquation.Greater);
            
        //    var list = new List<NameValue>();

        //    using (var reader = await QueryExecuter.QueryAsyc(qb))
        //    {
        //        while (reader.Read())
        //        {
        //            var item = new NameValue
        //            {
        //                Name = reader["Name"].ToString(),
        //                Value = Convert.ToDateTime(reader["ExpireDate"]).ToString("dd/MM/yyyy")
        //            };
        //            list.Add(item);
        //        }
        //    }

        //    return list;
        //}

        public async Task<IEnumerable<NameValue>> GetAboutToExpireProduct(string accountId, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("FromExpDate", CustomDateTime.IST.AddMonths(1).ToFormat());
            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);

            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardAboutToExpire", parms);

            var list = result.ToList();

            return list;
        }

        //public async Task<IEnumerable<NameValue>> GetTopTrendingSales(string instanceId)
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);

        //    qb.SelectBuilder.SetTop(10).GroupBy(ProductTable.NameColumn);
        //    qb.SelectBuilder.SortBy(DbColumn.SumColumn("", SalesItemTable.QuantityColumn));

        //    qb.AddColumn(ProductTable.NameColumn, DbColumn.SumColumn("Quantity", SalesItemTable.QuantityColumn), DbColumn.SumColumn("ReturnQuantity", SalesReturnItemTable.QuantityColumn))
        //        .JoinBuilder
        //        .Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn)
        //        .Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn)
        //        .Join(SalesTable.Table, SalesTable.IdColumn, SalesItemTable.SalesIdColumn)
        //        //.Join(SalesTable.Table, SalesTable.IdColumn,SalesReturnTable.SalesIdColumn)
        //        .LeftJoin(SalesReturnTable.Table, SalesReturnTable.SalesIdColumn, SalesTable.IdColumn)
        //        .LeftJoin(SalesReturnItemTable.Table, SalesReturnItemTable.SalesReturnIdColumn, SalesReturnTable.IdColumn);

        //    qb.ConditionBuilder.And(SalesItemTable.InstanceIdColumn, instanceId);
        //    if (_configHelper.AppConfig.IsSqlServer)
        //        qb.ConditionBuilder.And(DbColumn.CustomColumn($"Convert(date,{SalesTable.InvoiceDateColumn.FullColumnName})"), CriteriaEquation.Between);
        //    else
        //        qb.ConditionBuilder.And(DbColumn.CustomColumn($"CAST({SalesTable.InvoiceDateColumn.FullColumnName} AS date)"), CriteriaEquation.Between);

        //    qb.Parameters.Add("FilterFromDate", CustomDateTime.IST.AddDays(-10).ToFormat());
        //    qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());

        //    var list = new List<NameValue>();

        //    using (var reader = await QueryExecuter.QueryAsyc(qb))
        //    {
        //        while (reader.Read())
        //        {
        //            var item = new NameValue
        //            {
        //                //Name = reader["Name"].ToString(),
        //                //Value = reader["Quantity"].ToString()                       
        //            };
        //            decimal totalQty = (reader["Quantity"] as decimal? ?? 0) - (reader["ReturnQuantity"] as decimal? ?? 0);

        //            item.Name = reader["Name"].ToString();
        //            item.Value = totalQty.ToString();
        //            list.Add(item);
        //        }
        //    }

        //    return list;
        //}

        public async Task<IEnumerable<NameValue>> GetTopTrendingSales(string accountId, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);

            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardTrendingSales", parms);

            var list = result.ToList();

            return list;
        }

        //public async Task<IEnumerable<NameValue>> GetTopLowStockItem(string instanceId)
        //{
        //    int getMonth = await GetMonth(instanceId);
        //    int month = 0;           
        //    if(getMonth > 0)
        //    {
        //        month = getMonth * 30;
        //    }
        //    else
        //    {
        //        month = 30;
        //    }
        //    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
        //    qb.SelectBuilder.SetTop(10)
        //        .GroupBy(ProductTable.NameColumn);

        //    qb.AddColumn(ProductTable.NameColumn, DbColumn.SumColumn("Stock",ProductStockTable.StockColumn))
        //                .JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);

        //    qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId)
        //        .HavingAnd(DbColumn.SumColumn("", ProductStockTable.StockColumn), month, CriteriaEquation.Lesser);


        //    var list = new List<NameValue>();

        //    using (var reader = await QueryExecuter.QueryAsyc(qb))
        //    {
        //        while (reader.Read())
        //        {
        //            var item = new NameValue
        //            {
        //                Name = reader["Name"].ToString(),
        //                Value = reader["Stock"].ToString()
        //            };
        //            list.Add(item);
        //        }
        //    }

        //    return list;
        //}

        public async Task<IEnumerable<NameValue>> GetTopLowStockItem(string accountId, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);

            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardTopLowStock", parms);
            
            var list = result.ToList();
            
            return list;
        }

        public async Task<int> GetMonth(string instanceId)
        {
            var query = $@"SELECT NoOfMonth from InventorySettings WHERE instanceId = '{instanceId}'";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var getNoOfMonth=(await QueryExecuter.SingleValueAsyc(qb)).ToString();
            if (getNoOfMonth == "" || getNoOfMonth == null)
                    getNoOfMonth = "0";
            return Convert.ToInt32(getNoOfMonth);
        }

        public async Task<IEnumerable<NameValue>> DashboarDailyReport_CardDetails(object accountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardDailyCardDetailsList", parms);

            var Cardlist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Branch = x.Branch,
                    PharmacyName = x.PharmacyName,
                    OwnerName = x.OwnerName,
                    TotalCash = x.TotalCash as decimal? ?? 0,
                    TotalCard = x.TotalCard as decimal? ?? 0,
                    TotalCredit = x.TotalCredit as decimal? ?? 0,
                    TotalSales = x.TotalCash as decimal? ?? 0 + x.TotalCard as decimal? ?? 0 + x.TotalCredit as decimal? ?? 0,
                };
                return item;
            });

            return Cardlist.ToList();
        }

        public async Task<IEnumerable<NameValue>> PlusPharmacyReport_CardDetails(object accountId, object EmailId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("EmailId", EmailId);
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardDailyPlusPharmacyCardDetailsList", parms);

            var Cardlist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Branch = x.Branch,
                    PharmacyName = x.PharmacyName,
                    OwnerName = x.OwnerName,
                    City = x.City,
                    Area = x.Area,
                    TotalCash = x.TotalCash as decimal? ?? 0,
                    TotalCard = x.TotalCard as decimal? ?? 0,
                    TotalCredit = x.TotalCredit as decimal? ?? 0,
                    TotalSales = x.TotalCash as decimal? ?? 0 + x.TotalCard as decimal? ?? 0 + x.TotalCredit as decimal? ?? 0,
                };
                return item;
            });

            return Cardlist.ToList();
        }

        public async Task<IEnumerable<NameValue>> PlusPharmacyReport_GrossMargin(object accountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardDailyPlusPharmacyGrossMarginList", parms);

            var GrossMarginlist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Branch = x.Branch,
                    City = x.City,
                    Area = x.Area,
                    SalesNetMargin = x.SALESNET as decimal? ?? 0,
                    PurchaseNetMargin = x.PURCHASENET as decimal? ?? 0,
                    Profit = x.Profit as decimal? ?? 0,
                };
                return item;
            });

            return GrossMarginlist.ToList();



        }

        public async Task<IEnumerable<NameValue>> DashboarDailyStockReport(object instanceid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Instanceid", instanceid);
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardDailyStockList", parms);

            var Cardlist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Branch = x.Branch,
                    PharmacyName = x.PharmacyName,
                    OwnerName = x.OwnerName,

                };
                return item;
            });

            return Cardlist.ToList();



        }

        public async Task<IEnumerable<StockNameValue>> DashboarDailyNonMovingQuantity(object instanceid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Instanceid", instanceid);
            var result = await sqldb.ExecuteProcedureAsync<StockNameValue>("usp_GetNonMovingStockReportForMailer", parms);

            var Cardlist = result.Select(x =>
            {
                var item = new StockNameValue
                {

                    ProductName = x.ProductName,
                    AvailableStock = x.AvailableStock,
                    LastSaleDate = x.LastSaleDate,
                    InvoiceNo = x.InvoiceNo,
                    Age = x.Age,


                };
                return item;
            });

            return Cardlist.ToList();



        }

        public async Task<IEnumerable<NameValue>> DashboarDailyReport_GrossMargin(object accountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardDailyGrossMarginList", parms);

            var GrossMarginlist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Branch = x.Branch,
                    SalesNetMargin = x.SALESNET as decimal? ?? 0,
                    PurchaseNetMargin = x.PURCHASENET as decimal? ?? 0,
                    Profit = x.Profit as decimal? ?? 0,
                };
                return item;
            });

            return GrossMarginlist.ToList();

            

        }



        public async Task<IEnumerable<StockNameValue>> DashboarDailyStockReportOfReorderQuantity(string instanceid)
        {


            string query = $@"select top 15  ps.ProductId as productid,  p.name as ProductName , sum(ps.Stock) as AvailableStock,
			                	 ((SQA.SoldQuantity/90)*Ins.NoOfDays)- sum(ps.Stock) as ReOrderQuantity,
								SQA.SoldQuantity as Totalsold,
						     	   SQA.SoldQuantity/90 as Averagesold
                                from ProductStock ps
							    inner join Product p on p.Id=ps.ProductId
	                            inner join InventorySettings Ins on ins.instanceid=ps.instanceid
                                inner join
                                (
                                    select  p.Id,sum(si.Quantity) as SoldQuantity from ProductStock 
							        inner join SalesItem si on si.ProductStockId=ProductStock.Id
									 inner join Product p on p.id=ProductStock.ProductId
									 and 
									si.CreatedAt >= DATEADD(MONTH,-3,GETDATE()) 
								    where SI.InstanceId='{instanceid}'  
                                    group by p.Id
								 
				                ) SQA on p.Id=SQA.Id
				                and ps.InstanceId='{instanceid}'
								 
				                group by ps.ProductId,SQA.SoldQuantity,
								p.Name,Ins.NoOfDays having ((SQA.SoldQuantity/90)*Ins.NoOfDays)- sum(ps.Stock) > 0
                                ORDER BY ReOrderQuantity desc
                                ";


            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var list = new List<StockNameValue>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new StockNameValue
                    {
                        ProductName = reader["ProductName"].ToString(),
                        AvailableStock = reader["AvailableStock"] == DBNull.Value ? 0 : (decimal)reader["AvailableStock"],
                        ReOrderQuantity = reader["ReOrderQuantity"] == DBNull.Value ? 0 : (decimal)reader["ReOrderQuantity"],
                        Totalsold = reader["Totalsold"] == DBNull.Value ? 0 : (decimal)reader["Totalsold"],
                        AverageSold = reader["AverageSold"] == DBNull.Value ? 0 : (decimal)reader["AverageSold"],
                    };
                    list.Add(item);
                }
            }
            return list;

        }

        public async Task<IEnumerable<StockNameValue>> DashboarDailyStockReportOfZeroQuantity(string instanceid)
        {


            string query = $@"select top 15 p.name as ProductName,
                                  (sum(SI.Quantity)/90)*Ins.NoOfDays as ReOrderQuantity,sum(SI.Quantity)/90 as AverageTotalsold 
                                   from SalesItem SI
                                 inner join ProductStock ps on ps.id=SI.ProductStockId
                                 --inner join  ProductInstance pis on pis.ProductId=ps.Id
                                 inner join Product p on p.Id=ps.ProductId
                                 inner join InventorySettings Ins on ins.instanceid=ps.instanceid
                                 where ps.InstanceId='{instanceid}'
                                 and SI.CreatedAt >= DATEADD(MONTH,-3,GETDATE()) 
                                 group by p.name,p.Id, Ins.NoOfDays having sum(ps.stock) = 0   and  Sum(SI.Quantity) > 0
                                   order by ReOrderQuantity desc
                                ";


            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var list = new List<StockNameValue>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new StockNameValue
                    {
                        ProductName = reader["ProductName"].ToString(),
                        AverageSold = reader["AverageTotalsold"] == DBNull.Value ? 0 : (decimal)reader["AverageTotalsold"],
                        ReOrderQuantity = reader["ReOrderQuantity"] == DBNull.Value ? 0 : (decimal)reader["ReOrderQuantity"],

                    };
                    list.Add(item);
                }
            }
            return list;

        }

        public async Task<IEnumerable<StockNameValue>> DashboarDailyTrendindSalesQuantity(string instanceid)
        {
            string query = $@"select top 15 p.name as ProductName , sum(ps.Stock) as AvailableStock,
								SQA.SoldTotalQuantity as Totalsold,
						     	   SQA.SoldTotalQuantity/90 as Averagesold
                                from ProductStock ps
							    inner join Product p on p.Id=ps.ProductId
	                            inner join InventorySettings Ins on ins.instanceid=ps.instanceid
                                inner join
                                (
                                    select p.Id,Sum(Quantity) as SoldTotalQuantity from SalesItem as si 
		                            INNER JOIN Sales ON Sales.Id = si.SalesId 
		                            left join ProductStock as ps on si.ProductStockId = ps.Id
                                    left join Product as p on p.Id = ps.ProductId 
								    where si.InstanceId = '{instanceid}'  AND Convert(date,Sales.InvoiceDate) > DATEADD(MONTH,-3,GETDATE()) 
									  group by p.Id 
								 
				                ) SQA on p.Id=SQA.Id
				                and ps.InstanceId='{instanceid}'
				                group by ps.ProductId,SQA.SoldTotalQuantity,
								p.Name,Ins.NoOfDays
                                ORDER BY Totalsold desc
                                ";


            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var list = new List<StockNameValue>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new StockNameValue
                    {
                        ProductName = reader["ProductName"].ToString(),
                        AvailableStock = reader["AvailableStock"] == DBNull.Value ? 0 : (decimal)reader["AvailableStock"],
                        Totalsold = reader["TotalSold"] == DBNull.Value ? 0 : (decimal)reader["TotalSold"],
                        AverageSold = reader["AverageSold"] == DBNull.Value ? 0 : (decimal)reader["AverageSold"],

                    };
                    list.Add(item);
                }
            }
            return list;

        }




        public async Task<IEnumerable<StockNameValue>> DashboarDailyAboutToExpireReport(string instanceid)
        {
            string query = $@" select top 15 
                                 p.Name as ProductName,
                                 ps.ExpireDate,Sum(ps.Stock) as AvailableStock
                                 ,vp.GoodsRcvNo,
                                    vp.InvoiceDate as Invoicedate
                                 ,v.Name as VendorName 
                                 from ProductStock ps
                                 inner JOIN Product p ON p.Id = ps.ProductId  
                                 left join VendorPurchaseItem vpi on vpi.ProductStockId =ps.Id
                                 left join VendorPurchase vp on vp.id=vpi.VendorPurchaseId
                                 left join Vendor v on v.Id=vp.VendorId
                                 where  ps.InstanceId='{instanceid}' and ps.ExpireDate >  getdate()
                                 group by p.Name,ps.ExpireDate, vp.GoodsRcvNo,vp.InvoiceNo,vp.InvoiceDate,v.Name having Sum(ps.Stock) > 0
                                 order by ps.ExpireDate asc   
                                ";


            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var list = new List<StockNameValue>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {

                    var expdate = "";
                    if (reader["ExpireDate"] == DBNull.Value)
                    {
                        expdate = "";
                    }
                    else
                    {
                        expdate = ((DateTime)reader["ExpireDate"]).ToString("dd-MM-yyyy");
                    }
                    var indate = "";
                    if (reader["Invoicedate"] == DBNull.Value)
                    {
                        indate = "";
                    }
                    else
                    {
                        indate = ((DateTime)reader["Invoicedate"]).ToString("dd-MM-yyyy");
                    }
                    var item = new StockNameValue
                    {
                        ProductName = reader["ProductName"].ToString(),

                        ExpireDate = expdate,
                        AvailableStock = reader["AvailableStock"] == DBNull.Value ? 0 : (decimal)reader["AvailableStock"],
                        GoodsRcvNo = reader["GoodsRcvNo"].ToString(),
                        VendorName = reader["VendorName"].ToString(),
                        InvoiceDate = indate

                    };
                    list.Add(item);
                }
            }
            return list;

        }

        

        public async Task<IEnumerable<StockNameValue>> DashboarDailyExpiredResultReport(string instanceid)
        {
            string query = $@" select top 15 
                                 p.Name as ProductName,
                                 ps.ExpireDate,Sum(ps.Stock) as AvailableStock
                                 ,vp.GoodsRcvNo,
                                    vp.InvoiceDate as Invoicedate
                                 ,v.Name as VendorName 
                                 from ProductStock ps
                                 inner JOIN Product p ON p.Id = ps.ProductId  
                                 left join VendorPurchaseItem vpi on vpi.ProductStockId =ps.Id
                                 left join VendorPurchase vp on vp.id=vpi.VendorPurchaseId
                                 left join Vendor v on v.Id=vp.VendorId
                                 where  ps.InstanceId='{instanceid}' and ps.ExpireDate <  getdate()
                                 group by p.Name,ps.ExpireDate, vp.GoodsRcvNo,vp.InvoiceNo,vp.InvoiceDate,v.Name having Sum(ps.Stock) > 0
                                 order by ps.ExpireDate desc   
                                ";


            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var list = new List<StockNameValue>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {

                    var expdate = "";
                    if (reader["ExpireDate"] == DBNull.Value)
                    {
                        expdate = "";
                    }
                    else
                    {
                        expdate = ((DateTime)reader["ExpireDate"]).ToString("dd-MM-yyyy");
                    }
                    var indate = "";
                    if (reader["Invoicedate"] == DBNull.Value)
                    {
                        indate = "";
                    }
                    else
                    {
                        indate = ((DateTime)reader["Invoicedate"]).ToString("dd-MM-yyyy");
                    }
                    var item = new StockNameValue
                    {
                        ProductName = reader["ProductName"].ToString(),

                        ExpireDate = expdate,
                        AvailableStock = reader["AvailableStock"] == DBNull.Value ? 0 : (decimal)reader["AvailableStock"],
                        GoodsRcvNo = reader["GoodsRcvNo"].ToString(),
                        VendorName = reader["VendorName"].ToString(),
                        InvoiceDate = indate

                    };
                    list.Add(item);
                }
            }
            return list;

        }
        public async Task<IEnumerable<NameValue>> DashboarDailyReport_PatientVisit(object accountId)
        {

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardDailyPatientVisitList", parms);

            var Patientlist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Branch = x.Branch,
                    PatientTypeName = x.PatientTypeName.Trim(),
                    PatientVisitCount = x.PatientVisitCount,

                };
                return item;
            });

            return Patientlist.ToList();

            //string query = $@" select Branch,PatientTypeName, PatientVisitCount from (select Instance.Name AS Branch,LOWER(SalesType.Name) as PatientTypeName,count(Sales.SalesType)as PatientVisitCount
            //from Account
            //INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            //INNER JOIN Instance on Instance.AccountId = HQueUser.AccountId AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''
            //INNER join SalesType SalesType on SalesType.AccountId = HQueUser.Accountid and SalesType.InstanceId =Instance.id
            //INNER JOIN Sales Sales ON Sales.SalesType = SalesType.ID  and sales.AccountId=HQueUser.AccountId
            //WHERE Convert(date,InvoiceDate)  = dateadd(day,datediff(day,1,GETDATE()),0) and sales.Cancelstatus is NULL
            //and Account.id ='{accountId}' and HQueUser.UserId='saravanan@goclinix.com'
            //group by Instance.Name,SalesType.Name
            //UNION
            //select Instance.Name AS Branch,'-' As PatientTypeName,'-' as PatientVisitCount
            //from Account
            //INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            //INNER JOIN Instance on Instance.AccountId = HQueUser.AccountId AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''            
            //WHERE Account.id ='{accountId}' and HQueUser.UserId='saravanan@goclinix.com'
            //group by Instance.Name ) as a group by a.Branch,a.PatientTypeName,a.PatientVisitCount order by a.Branch,a.PatientTypeName desc";


            //var qb = QueryBuilderFactory.GetQueryBuilder(query);

            //var list = new List<NameValue>();

            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var item = new NameValue
            //        {
            //            Branch= reader["Branch"].ToString(),
            //            PatientTypeName = reader["PatientTypeName"].ToString().Trim(),
            //            PatientVisitCount = reader["PatientVisitCount"].ToString(),
            //        };
            //        list.Add(item);
            //    }
            //}
            //return list;

        }

        public async Task<IEnumerable<NameValue>> DashboarDailyReport_BouncedItems(object accountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetDashboardDailyBouncedList", parms);

            var Bouncedlist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Branch = x.Branch,
                    TotalSalesCount = x.BouncedQty as decimal? ?? 0,

                };
                return item;
            });

            return Bouncedlist.ToList();

            //string query = $@" SELECT BRANCH,SUM(QTY) AS QTY FROM (
            //select Instance.Name AS BRANCH,MissedOrder.Quantity as QTY
            //from Account
            //INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            //INNER JOIN Instance on Instance.AccountId = HQueUser.AccountId AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''
            //INNER JOIN MissedOrder on  MissedOrder.AccountId = HQueUser.AccountId AND MissedOrder.InstanceId=INSTANCE.ID  
            //INNER JOIN Product on Product.id = MissedOrder.ProductId --AND Product.InstanceId=INSTANCE.ID 
            //WHERE HQueUser.AccountId ='{accountId}' and HQueUser.UserId='saravanan@goclinix.com' and Convert(date,MissedOrder.CreatedAt)  = dateadd(day,datediff(day,1,GETDATE()),0) 
            //group by Instance.Name,MissedOrder.Quantity
            //UNION
            //select Instance.Name AS Branch,'0.00' as QTY
            //from Account
            //INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            //INNER JOIN Instance on Instance.AccountId = HQueUser.AccountId AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''           
            //WHERE Account.id ='{accountId}' and HQueUser.UserId='saravanan@goclinix.com'
            //group by Instance.Name 
            //) AS SS GROUP BY SS.BRANCH
            //";

            //var qb = QueryBuilderFactory.GetQueryBuilder(query);

            //var list = new List<NameValue>();

            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var item = new NameValue
            //        {
            //            Branch = reader["BRANCH"].ToString(),
            //            TotalSalesCount = reader["QTY"] == DBNull.Value ? 0 : (decimal)reader["QTY"],
            //        };
            //        list.Add(item);
            //    }
            //}
            //return list;

        }

        public async Task<IEnumerable<NameValue>> DashboarManagementReport_Details()
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetManagementPharmacyList", parms);
           
            var Pharmalist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Edge=x.Edge,
                    Paid = x.Paid,
                    Trial = x.Trial,
                    Offline = x.Offline,
                    PatientOverAll = x.PatientOverAll,
                    PatientYesterday = x.PatientYesterday,
                    PatientAccepted = x.PatientAccepted ,
                    PatientMissed = x.PatientMissed,
                    DoctorOverAll = x.DoctorOverAll,
                    DoctorYesterday = x.DoctorYesterday,
                    DoctorAccepted = x.DoctorAccepted,
                    DoctorMissed = x.DoctorMissed,
                };
                return item;
            });

            return Pharmalist.ToList();
        }

        public async Task<IEnumerable<NameValue>> DashboarManagementReport_PharmacyLiveList()
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetManagementPharmacyLiveList", parms);

            var Pharmalist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Branch = x.PharmaName,
                    SalesNetMargin = x.SALESNET as decimal? ?? 0,
                    TotalSaleCount = x.TOTALSALES as int? ?? 0,
                    TotalPurchaseCount = x.TOTALPURCHASECOUNT as int? ?? 0,
                    PurchaseNetMargin = x.PURCHASENET as decimal? ?? 0,
                    City = x.City,
                    Area = x.Area,
                };
                return item;
            });

            return Pharmalist.ToList();
        }

        public async Task<IEnumerable<NameValue>> DashboarManagementReport_PharmacyTrialList()
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetManagementPharmacyTrialList", parms);

            var Pharmalist = result.Select(x =>
            {
                var item = new NameValue
                {
                    // Branch = x.GroupName + " - " + x.PharmaName,
                    Branch = x.PharmaName,
                    SalesNetMargin = x.SALESNET as decimal? ?? 0,
                    TotalSaleCount = x.TOTALSALES as int? ?? 0,
                    TotalPurchaseCount = x.TOTALPURCHASECOUNT as int? ?? 0,
                    PurchaseNetMargin = x.PURCHASENET as decimal? ?? 0,
                    City = x.City,
                    Area = x.Area,
                };
                return item;
            });

            return Pharmalist.ToList();
        }

        public async Task<IEnumerable<NameValue>> DashboarManagementReport_PharmacyEdgeList()
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_GetManagementPharmacyEdgeList", parms);

            var Pharmalist = result.Select(x =>
            {
                var item = new NameValue
                {
                    Branch =x.PharmaName,
                    SalesNetMargin = x.SALESNET as decimal? ?? 0,
                    TotalSaleCount = x.TOTALSALES as int? ?? 0,
                    TotalPurchaseCount = x.TOTALPURCHASECOUNT as int? ?? 0,
                    PurchaseNetMargin = x.PURCHASENET as decimal? ?? 0,
                    City = x.City,
                    Area = x.Area,
                };
                return item;
            });

            return Pharmalist.ToList();
        }

        // Added by Gavaskar 05-10-2017 Daily mail all Pharmacy Start
        public async Task<IEnumerable<NameValue>> DashboardGetAdminDetails()
        {

            //string query = $@" select AccountId,UserId from hqueuser(nolock) where instanceid is null and usertype=2 and isnull(userid,'') Not in ('' ,'NULL')";
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //var list = new List<NameValue>();
            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var item = new NameValue
            //        {
            //            OwnerId = reader["AccountId"].ToString(),
            //            EmailId = reader["UserId"].ToString()
            //        };
            //        list.Add(item);
            //    }
            //}
            //return list;

            Dictionary<string, object> parms = new Dictionary<string, object>();
            var result = await sqldb.ExecuteProcedureAsync<NameValue>("usp_Get_DashboardGetAdminDetails", parms);

            var Pharmalist = result.Select(x =>
            {
                var item = new NameValue
                {
                    OwnerId = x.OwnerId,
                    EmailId = x.EmailId,
                };
                return item;
            });

            return Pharmalist.ToList();

        }
        // Added by Gavaskar 05-10-2017 Daily mail all Pharmacy End
    }
}
