﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.External;
using Newtonsoft.Json;
using Utilities.Helpers;
using DataAccess;
using System.Xml;
using HQue.Contract.External.Sync;
using HQue.Contract.Infrastructure.Settings;
using HQue.DataAccess.DbModel;
using HQue.Contract.Infrastructure.UserAccount;
using Utilities.Helpers;
using DataAccess.Criteria;

namespace HQue.DataAccess.Sync
{
    public class SyncDataAccess : BaseDataAccess
    {
        private readonly ConfigHelper _configHelper;

        SqlDatabaseHelper sqldb;
        public SyncDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;

            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }

        public override Task<T> Save<T>(T data)
        {
            throw new NotImplementedException();
        }
        public async Task<List<CustomSettingsDetail>> GetCustomSettings(string AccountId, string InstanceId, int customSettingsGroupId)
        {
            var parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("CustomSettingsGroupId", customSettingsGroupId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_get_customSettingsforSyncData", parms);
            var result1 = result.Select(x =>
            {
                var item = new CustomSettingsDetail
                {
                    Id = x.Id as string ?? "",
                    CustomSettingsGroupId = x.GroupId,
                    CustomSettingsGroupName = x.GroupName,
                    AccountId = x.AccountId as string ?? AccountId,
                    InstanceId = x.InstanceId as string ?? InstanceId,
                    IsActive = x.IsActive as bool? ?? false,
                };
                return item;
            });
            return result1.ToList();
        }
        public async Task<bool> WriteToDatabase(SyncObject data)
        {
            bool isExecSp = false;
            bool isSPReturn = true;
            string spname = "";
            try
            {
                WriteOfflineDBLog(data);
                if (data.FileType == "xml")
                {
                    return await WriteXmlToDatabase(data);
                }
                //if (data.QueryText.ToLower().Contains("productstock") && data.EventLevel == "A")
                //    return true;

                /* if (data.QueryText.StartsWith("Exec"))
                 {
                     string spname = data.QueryText.Replace("Exec", "").Trim();
                     Dictionary<string, object> param = new Dictionary<string, object>();
                     foreach (var param1 in data.Parameters)
                     {
                         param.Add(param1.Key, param1.Value);
                     }
                     var result = await sqldb.ExecuteProcedureAsync<dynamic>(spname, param);
                     return true;
                 }*/



                var qb = QueryBuilderFactory.GetQueryBuilder(data.QueryText);
                var sSql = qb.GetQuery().ToLower();
                ////if ((sSql.Contains("update ") && sSql.Contains(" productstock ") && !data.Parameters.Keys.Contains("Id"))
                ////    || (sSql.Contains("update ") && sSql.Contains(" product ") && !data.Parameters.Keys.Contains("Id")))
                //if ((sSql.Contains(" productstock set gsttotal ="))
                //    || (sSql.Contains("update ") && sSql.Contains(" product ") && !data.Parameters.Keys.Contains("Id")))
                //    return true;

                //Added by Poongodi on 11/09/2017

                if (data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,UpdatedAt = @UpdatedAt,ProductId = @ProductId,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode WHERE ProductStock.Id  =  @Id AND ProductStock.AccountId  =  @AccountId AND ProductStock.InstanceId  =  @InstanceId")
                    || data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,UpdatedAt = @UpdatedAt,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode")
                    || data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,ProductId = @ProductId,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode WHERE ProductStock.Id  =  @Id AND ProductStock.AccountId  =  @AccountId AND ProductStock.InstanceId  =  @InstanceId")
                    || data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,UpdatedAt = @UpdatedAt,ProductId = @ProductId,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode"))
                {
                    // data.QueryText = "UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,ProductId = @ProductId,BatchNo = @BatchNo,ExpireDate = @ExpireDate,VAT = @VAT,TaxType = @TaxType,SellingPrice = @SellingPrice,MRP = @MRP,PurchaseBarcode = @PurchaseBarcode,PackageSize = @PackageSize,PurchasePrice = @PurchasePrice,OfflineStatus = @OfflineStatus,UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,CST = @CST,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal,Eancode = @Eancode WHERE ProductStock.Id  =  @Id AND ProductStock.AccountId  =  @AccountId AND ProductStock.InstanceId  =  @InstanceId";
                    isExecSp = true;
                    spname = "Usp_purchase_productstock_update";

                }
                else if (data.QueryText.Contains("UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,UpdatedAt = @UpdatedAt,IsMovingStock = @IsMovingStock,SellingPrice = @SellingPrice,Eancode = @Eancode,OfflineStatus = @OfflineStatus,MRP = @MRP WHERE ProductStock.Id  =  @Id AND ProductStock.AccountId  =  @AccountId AND ProductStock.InstanceId  =  @InstanceId"))
                {

                    isExecSp = true;
                    spname = "usp_Productstock_Update";
                }
                else if (data.QueryText.Contains("Update ProductStock Set GstTotal =  @GstTotal, Igst = @Igst, Cgst = @Cgst, Sgst = @Sgst, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy Where ProductId = @ProductId And GstTotal is NULL And AccountId = @AccountId"))
                {
                    isExecSp = true;
                    isSPReturn = false;
                    spname = "usp_ProductStock_UpdateByProductId";
                }
                else if (data.QueryText.Contains("UPDATE Product SET Manufacturer = @Manufacturer,Schedule = @Schedule,GenericName = @GenericName,Category = @Category,Packing = @Packing,CommodityCode = @CommodityCode,Type = @Type,RackNo = @RackNo WHERE Product.Id  =  @Id"))
                {
                    isExecSp = true;
                    spname = "USP_Product_Update";
                }
                else if (data.QueryText.Contains("UPDATE Product SET UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal WHERE Product.Id  =  @Id") ||
                    data.QueryText.Contains("UPDATE Product SET UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,HsnCode = @HsnCode WHERE Product.Id  =  @Id"))
                {
                    isExecSp = true;
                    spname = "usp_product_HSN_update";
                }
                else if (data.QueryText.Contains("INSERT INTO ProductStock (Id,AccountId,InstanceId,ProductId,VendorId,BatchNo,ExpireDate,VAT,TaxType,SellingPrice,MRP,PurchaseBarcode,Stock,PackageSize,PackagePurchasePrice,PurchasePrice,OfflineStatus,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy,CST,IsMovingStock,ReOrderQty,Status,IsMovingStockExpire,NewOpenedStock,NewStockInvoiceNo,NewStockQty,StockImport,ImportQty,Eancode,HsnCode,Igst,Cgst,Sgst,GstTotal,ImportDate,Ext_RefId,TaxRefNo)"))
                {
                    isExecSp = true;
                    spname = "usp_productStock_Insert";
                }

                else if (data.QueryText.Contains(", UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy, TransactionId = @TransactionId where AccountId = @AccountId And InstanceId = @InstanceId And  id = @Id") && data.QueryText.Contains("Update ProductStock Set Stock = Stock"))
                {
                    isExecSp = true;
                    spname = "usp_Sales_Productstock_Update";
                }
                else if (data.QueryText.Contains("UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy where AccountId = @AccountId And InstanceId = @InstanceId And  id = @Id") && data.QueryText.Contains("Update ProductStock Set Stock = Stock -"))
                {
                    isExecSp = true;
                    spname = "usp_Sales_Productstock_Update";
                }
                else if (data.QueryText.Contains("Update ProductStock Set Stock = @Stock, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy, TransactionId = @TransactionId where AccountId = @AccountId And InstanceId = @InstanceId And  id = @Id"))
                {
                    isExecSp = true;
                    spname = "usp_Sales_Productstock_Updates";
                }
                else if (data.QueryText.Contains("DELETE FROM CustomerPayment WHERE CustomerPayment.AccountId  =  @AccountId AND CustomerPayment.InstanceId  =  @InstanceId AND CustomerPayment.SalesId  =  @SalesId"))
                {
                    isExecSp = true;
                    spname = "usp_CustomerPayment_Updates";
                }
                //else if(data.QueryText.Contains("SYNC DATA DELETE SCHEDULER"))  //Added by Sumathi on 29-11-18 -   Removed as per Poongodi's Suggesstion
                //{
                //    isExecSp = true;
                //    spname = "usp_Reverse_SyncData_Seed_Insert";
                //}

                //Hot fix added by Sumathi on 09-Mar-2019 
                else if (data.QueryText.Contains("INSERT INTO Patient"))
                {

                    if (data.QueryText.Contains("@CreatedBy") && (data.Parameters["CreatedBy"] == null))
                    {
                        data.Parameters["CreatedBy"] = GetAdminUser(data.Parameters["AccountId"].ToString());
                    }

                    if (data.QueryText.Contains("@UpdatedBy") && (!data.Parameters.ContainsKey("UpdatedBy")))
                    {
                        data.Parameters.Add("UpdatedBy", data.Parameters["CreatedBy"]);
                    }
                    if (data.QueryText.Contains("@EmpID") && (!data.Parameters.ContainsKey("EmpID")))
                    {
                        data.Parameters.Add("EmpID", null);
                    }
                    if (data.QueryText.Contains("@Discount") && (!data.Parameters.ContainsKey("Discount")))
                    {
                        data.Parameters.Add("Discount", null);
                    }
                    if (data.QueryText.Contains("@IsSync") && (!data.Parameters.ContainsKey("IsSync")))
                    {
                        data.Parameters.Add("IsSync", null);
                    }
                    if (data.QueryText.Contains("@CustomerPaymentType") && (!data.Parameters.ContainsKey("CustomerPaymentType")))
                    {
                        data.Parameters.Add("CustomerPaymentType", null);
                    }
                    if (data.QueryText.Contains("@PatientType") && (!data.Parameters.ContainsKey("PatientType")))
                    {
                        data.Parameters.Add("PatientType", null);
                    }
                    if (data.QueryText.Contains("@IsAutoSave") && (!data.Parameters.ContainsKey("IsAutoSave")))
                    {
                        data.Parameters.Add("IsAutoSave", null);
                    }

                    if (data.QueryText.Contains("@Pan") && (!data.Parameters.ContainsKey("Pan")))
                    {
                        data.Parameters.Add("Pan", null);
                    }
                    if (data.QueryText.Contains("@GsTin") && (!data.Parameters.ContainsKey("GsTin")))
                    {
                        data.Parameters.Add("GsTin", null);
                    }
                    if (data.QueryText.Contains("@DrugLicenseNo") && (!data.Parameters.ContainsKey("DrugLicenseNo")))
                    {
                        data.Parameters.Add("DrugLicenseNo", null);
                    }
                    if (data.QueryText.Contains("@LocationType") && (!data.Parameters.ContainsKey("LocationType")))
                    {
                        data.Parameters.Add("LocationType", null);
                    }
                    if (data.QueryText.Contains("@BalanceAmount") && (!data.Parameters.ContainsKey("BalanceAmount")))
                    {
                        data.Parameters.Add("BalanceAmount", null);
                    }
                    if (data.QueryText.Contains("@Status") && (!data.Parameters.ContainsKey("Status")))
                    {
                        data.Parameters.Add("Status", 1);
                    }
                }

                if (isExecSp)
                {

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    foreach (var param1 in data.Parameters)
                    {
                        if (param1.Key.ToString().ToLower() != "transquantity")
                            param.Add(param1.Key, param1.Value);
                    }
                    var result = await sqldb.ExecuteProcedureAsync<dynamic>(spname, param);
                    if (isSPReturn)
                    {
                        return true;
                    }
                }

                foreach (var param in data.Parameters)
                {
                    if (param.Key.ToString().ToLower() == "updatedby" && param.Value != null && param.Value.ToString().Contains("@"))
                    {
                        qb.Parameters.Add(param.Key, "");
                    }
                    else
                    {
                        qb.Parameters.Add(param.Key, param.Value);
                    }
                }
                if (isSPReturn)
                {
                    await QueryExecuter.NonQueryAsyc(qb);
                }
                //else
                //{
                //    if (data.Parameters.Where(x => x.Key == "InstanceId").Count() == 0)
                //        data.Parameters.Add("InstanceId", "");
                //}
                if (qb.Parameters.Keys.Contains("ToInstanceId") || data.EventLevel == "A" || data.EventLevel == "G")
                {
                    //WriteToOnlineSyncTargetQueue(new SyncObject() { Id = data.Id, QueryText = qb.GetQuery(), Parameters = qb.Parameters });
                    WriteToOnlineSyncTargetQueue(data); //Added by Martin on 23/05/2017
                }
            }
            catch (Exception e)
            {
                WriteOfflineErrorLog(data, spname + e.Message);

                // do nothing if PasswordResetKey has empty object
                if (data.QueryText.Contains("PasswordResetKey"))
                    return true;

                // do nothing for PRIMARY KEY VIOLATION
                if (((System.Data.SqlClient.SqlException)e).Number == 2627)
                    return true;

                // do nothing for deadlocked just return false to retry
                if (e.Message.Contains("deadlocked"))
                    return false;

                return false;
            }

            return true;
        }
        /// <summary>
        /// Bulk insert Added by Poongodi on 23/10/2017
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<bool> WriteXmlToDatabase(SyncObject data)
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                //XmlDocument xmldoc = new XmlDocument();
                //xmldoc.LoadXml(data.QueryText);

                foreach (var param1 in data.Parameters)
                {
                    param.Add(param1.Key, param1.Value);
                }
                param.Add("xmlData", data.QueryText);
                var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_ClosingStockToOnline", param);

            }
            catch (Exception e)
            {
                WriteOfflineErrorLog(data, "Usp_ClosingStockToOnline" + e.Message);
                // do nothing for PRIMARY KEY VIOLATION
                if (((System.Data.SqlClient.SqlException)e).Number == 2627)
                    return true;

                return false;
            }

            return true;

        }
        private void WriteOfflineDBLog(SyncObject data)
        {
            if (data == null)
                return;

            try
            {
                string query = $"INSERT INTO syncdata_Offline (AccountID,InstanceId,Action,Data) VALUES (@AccountId, @InstanceId, @Action, @Data)";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                qb.Parameters.Add("AccountId", data.Parameters.Keys.Contains("AccountId") ? data.Parameters["AccountId"] : DBNull.Value);
                qb.Parameters.Add("InstanceId", data.Parameters.Keys.Contains("InstanceId") ? data.Parameters["InstanceId"] : DBNull.Value);
                qb.Parameters.Add("Action", data.EventLevel);
                qb.Parameters.Add("Data", JsonConvert.SerializeObject(data));

                QueryExecuter.NonQueryAsyc3(qb);

            }
            catch (Exception ex)
            {

                //do nothing
            }

        }


        private void WriteOfflineErrorLog(SyncObject data, string errorMessage)
        {
            if (data == null)
                return;

            try
            {
                string query = $"INSERT INTO syncdata_Offline_error (AccountID,InstanceId,Action,Data, ErrorMessage) VALUES (@AccountId, @InstanceId, @Action, @Data, @ErrorMessage)";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                qb.Parameters.Add("AccountId", data.Parameters.Keys.Contains("AccountId") ? data.Parameters["AccountId"] : DBNull.Value);
                qb.Parameters.Add("InstanceId", data.Parameters.Keys.Contains("InstanceId") ? data.Parameters["InstanceId"] : DBNull.Value);
                qb.Parameters.Add("Action", data.EventLevel);
                qb.Parameters.Add("Data", JsonConvert.SerializeObject(data));
                qb.Parameters.Add("ErrorMessage", errorMessage);

                QueryExecuter.NonQueryAsyc3(qb);

            }
            catch (Exception ex)
            {

                //do nothing
            }

        }

        public Task<bool> SaveSyncPendingQuery(List<SyncObject> data)
        {
            string query = $"INSERT INTO SyncPendingQuery (Id, AccountID, InstanceId, SyncQuery) VALUES (@Id, @AccountId, @InstanceId, @SyncQuery)";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.Parameters.Add("Id", Guid.NewGuid().ToString());
            qb.Parameters.Add("AccountId", data[0].AccountId);
            qb.Parameters.Add("InstanceId", data[0].InstanceId);
            qb.Parameters.Add("SyncQuery", JsonConvert.SerializeObject(data));
            return QueryExecuter.NonQueryAsyc2(qb);
        }

        public Task<bool> UpdateSyncPendingQuery(List<SyncObject> data, string AccountId, string InstanceId, string Id)
        {
            string query = $"UPDATE SyncPendingQuery SET SyncQuery = @SyncQuery WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = @Id";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.Parameters.Add("Id", Id);
            qb.Parameters.Add("AccountId", AccountId);
            qb.Parameters.Add("InstanceId", InstanceId);
            qb.Parameters.Add("SyncQuery", JsonConvert.SerializeObject(data));
            return QueryExecuter.NonQueryAsyc2(qb);
        }

        public Task<bool> DeleteSyncPendingQuery(string AccountId, string InstanceId, string Id)
        {
            string query = $"DELETE FROM SyncPendingQuery WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = @Id";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.Parameters.Add("Id", Id);
            qb.Parameters.Add("AccountId", AccountId);
            qb.Parameters.Add("InstanceId", InstanceId);
            return QueryExecuter.NonQueryAsyc2(qb);
        }

        public Task<bool> ExecuteSyncPendingQuery(string AccountId, string InstanceId)
        {
            if (string.IsNullOrEmpty(AccountId) || string.IsNullOrEmpty(InstanceId))
            {
                if (!string.IsNullOrEmpty(InstanceId))
                {
                    string query1 = $"SELECT AccountId FROM Instance WHERE Id = '{InstanceId}'";
                    var qb1 = QueryBuilderFactory.GetQueryBuilder(query1);
                    AccountId = QueryExecuter.SingleValueAsyc(qb1).Result.ToString();
                }
                else
                {
                    return Task.FromResult(true);
                }
            }
            string query = $"SELECT Id, SyncQuery FROM SyncPendingQuery WHERE AccountId = '{AccountId}' AND InstanceId = '{InstanceId}' ORDER BY CreatedAt ASC";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            using (var reader = QueryExecuter.QueryAsyc(qb).Result)
            {
                while (reader.Read())
                {
                    var syncObject = new SyncPendingQuery
                    {
                        Id = reader["Id"].ToString(),
                        SyncQuery = reader["SyncQuery"].ToString(),
                    };
                    var list = JsonConvert.DeserializeObject<List<SyncObject>>(syncObject.SyncQuery);
                    for (var i = 0; i < list.Count; i++)
                    {
                        var syncStatus = true;
                        syncStatus = WriteToDatabase(list[i]).Result;
                        if (syncStatus == false)
                        {
                            var syncObjectList = new List<SyncObject>();
                            for (var j = i; j < list.Count; j++)
                            {
                                syncObjectList.Add(list[j]);
                            }

                            UpdateSyncPendingQuery(syncObjectList, AccountId, InstanceId, syncObject.Id);
                            return Task.FromResult(false);
                        }
                    }
                    DeleteSyncPendingQuery(AccountId, InstanceId, syncObject.Id);
                }
            }

            return Task.FromResult(true);
        }

        //Added by Sumathi on 29-Nov-18
        public async Task<bool> SyncDataDeleteScheduler(string AccountId, string InstanceId, string sUserId)
        {
            bool bOffline = _configHelper.AppConfig.OfflineMode;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("Offline", bOffline);
            parms.Add("TransDate", DateTime.Now);
            parms.Add("CreatedBy", sUserId);
            var result = await sqldb.ExecuteProcedureAsync<int>("usp_SyncDataRemove_Scheduler", parms);

            if (bOffline && result != null)
            {
                int SeedIndex = Convert.ToInt32(result.FirstOrDefault());
                if (SeedIndex > 0)
                {
                    parms.Remove("Offline");
                    parms.Remove("TransDate");
                    parms.Remove("CreatedBy");

                    parms.Add("CreatedAt", CustomDateTime.IST);
                    parms.Add("SeedIndex", SeedIndex);

                    WriteToOfflineSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = "SYNC DATA DELETE SCHEDULER", Parameters = parms, EventLevel = "I" }, "");
                }
            }
            return true;

        }

        private string GetAdminUser(string AccountId)
        {
            try
            {

                var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
                qb.AddColumn(HQueUserTable.IdColumn);

                qb.ConditionBuilder
                    .And(HQueUserTable.AccountIdColumn, AccountId);


                var col1 = new CustomCriteria(HQueUserTable.InstanceIdColumn, Utilities.Enum.CriteriaCondition.IsNull);
                qb.ConditionBuilder.And(col1);

                var user = List<HQueUser>(qb).Result.FirstOrDefault();
                if (user != null)
                {
                    return user.Id;
                }

                return "";

            }
            catch (Exception ex)
            {
                return "";
            }
        }


        public Task<bool> LogScheduler(string Id, int EventType, string Data, DateTime StartTime)
        {
            string query = $"INSERT INTO syncdata_scheduler (id,eventtype,starttime,data) VALUES (@Id, @EventType,@StartTime,@Data)";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);           
            qb.Parameters.Add("Id", Id);
            qb.Parameters.Add("EventType", EventType);
            qb.Parameters.Add("StartTime", StartTime);
            qb.Parameters.Add("Data", Data);

            return QueryExecuter.NonQueryAsyc3(qb);
        }

        public Task<bool> LogSchedulerUpdate(string Id, int EventType, string Data, DateTime EndTime)
        {
            string query = $"UPDATE syncdata_scheduler SET endtime=@EndTime,data=data+ ' - '+ @Data WHERE id=@Id AND eventtype=@EventType";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            qb.Parameters.Add("Id", Id);
            qb.Parameters.Add("EventType", EventType);
            qb.Parameters.Add("EndTime", EndTime);
            qb.Parameters.Add("Data", Data);

            return QueryExecuter.NonQueryAsyc3(qb);
        }
    }

}