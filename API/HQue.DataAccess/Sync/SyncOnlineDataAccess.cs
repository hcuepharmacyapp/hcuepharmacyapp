﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.External;
using System.Text;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Data.Common;
using Newtonsoft.Json;

namespace HQue.DataAccess.Sync
{
    public class SyncOnlineDataAccess : BaseDataAccess
    {
        private readonly IHostingEnvironment _environment;
            public SyncOnlineDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, IHostingEnvironment environment) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _environment = environment;   
        }

        public override Task<T> Save<T>(T data)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetOnlineInstanceData(string instanceid)
        {
            StringBuilder sb = new StringBuilder();
            string fileName = null;
            string filePath = null;
            FileInfo info = null;
            try
            {
                var qb = QueryBuilderFactory.GetQueryBuilder("sp_syncinstancedata");
                qb.Parameters.Add("@InstanceID", instanceid);

                using (var reader = await QueryExecuter.ExecuteProcAsyc(qb))
                {
                    //fileName = Path.Combine(_environment.WebRootPath, "temp", string.Format("hq_db_{0}.txt", instanceid));
                    fileName = string.Format("hq_db_{0}.txt", instanceid);
                    filePath = Path.Combine("./temp", fileName);
                    info = new FileInfo(filePath);

                    if (info.Exists)
                        info.Delete();

                    using (StreamWriter file = info.AppendText())
                    {
                        file.Write("<SyncData>");
                        file.Write(File.ReadAllText("./temp/productmaster"));
                        bool bResult = true;
                        while (bResult)
                        {
                            while (reader.Read())
                            {
                                //sb.Append(reader.GetString(0));
                                file.Write(reader.GetString(0));
                            }

                           bResult = reader.NextResult();
                        }

                        file.Write("</SyncData>");
                    }
                }

            }
            catch (Exception e)
            {
                return e.Message;
            }

            return info.FullName;
        }


        public async Task<string> GetInstanceSeedData(string accountId, string instanceId, int seedIndex)
        {
            //Modified by Martin on 07/06/2017 -- nolock added by Poongodi on 30112018
            var qb = QueryBuilderFactory.GetQueryBuilder("SELECT  TOP 100 id, data FROM SyncData (nolock) WHERE (instanceId=@InstanceID AND Id > @SeedIndex) OR (accountId=@AccountID AND Action='A' AND Id > @SeedIndex)");
            qb.Parameters.Add("@InstanceID", instanceId);
            qb.Parameters.Add("@SeedIndex", seedIndex);
            qb.Parameters.Add("@AccountID", accountId);

            //var reader = await QueryExecuter.QueryAsyc(qb);
            //var rows = this.ConvertToDictionary(reader);

            //return JsonConvert.SerializeObject(rows, Formatting.Indented);

            //Using Block added by Sumathi on 06-JAN-2019 to solve Dev API-e2e Restart Issue -- max pool size was reached.
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                var rows = this.ConvertToDictionary(reader);

                return JsonConvert.SerializeObject(rows, Formatting.Indented);
            }

        }


        private IEnumerable<Dictionary<string, object>> ConvertToDictionary(DbDataReader reader)
        {
            var columns = new List<string>();
            var rows = new List<Dictionary<string, object>>();

            for (var i = 0; i < reader.FieldCount; i++)
            {
                columns.Add(reader.GetName(i));
            }

            while (reader.Read())
            {
                rows.Add(columns.ToDictionary(column => column, column => reader[column]));
            }

            return rows;
        }


        //Sumathi on 29-Oct-18
        public async Task<bool> RemoveSyncData(string AccountID, string InstanceID, int SeedIndex)
        {
            try
            {
                var qb = QueryBuilderFactory.GetQueryBuilder("usp_move_syncDataToTemp");
                qb.Parameters.Add("@AccountID", AccountID);
                qb.Parameters.Add("@InstanceID", InstanceID);
                qb.Parameters.Add("@SeedIndex", SeedIndex);

               
                using (var reader = await QueryExecuter.ExecuteProcAsyc(qb))
                {
                    return true;
                }
                    

            }
            catch(Exception ex)
            {
                return false;
            }
        }

       
   }
}
