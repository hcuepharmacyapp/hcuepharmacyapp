﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Reminder;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using DataAccess.Criteria.Interface;
using Utilities.Helpers;
using DataAccess.Criteria;
using System.Data.Common;
using HQue.Contract.External;
using System.IO;
using Newtonsoft.Json;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.Data;
using HQue.Contract.Infrastructure.Settings;
using System.Data.SqlClient;
using HQue.DataAccess.Sync;
using DataAccess;

namespace HQue.DataAccess
{
    public abstract class BaseDataAccess
    {
        protected readonly ISqlHelper QueryExecuter;
        protected readonly QueryBuilderFactory QueryBuilderFactory;
        public ClaimsPrincipal user;
        protected BaseDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory)
        {
            QueryExecuter = sqlQueryExecuter;
            QueryBuilderFactory = queryBuilderFactory;
            sqlQueryExecuter.OnSyncQueueOperation = OnWriteToSyncQueue;
            user = queryBuilderFactory.GetHttpContext().HttpContext.User;
        }
        public void Dispose()
        {
            QueryExecuter.OnSyncQueueOperation = null;
        }
        public async Task<bool> ConnectionTest()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder("SELECT 1 FROM HQueUser");
            try
            {
                await QueryExecuter.SingleValueAsyc(qb);
                return true;
            }
            catch (Exception)
            {

            }
            return false;
        }
        private void OnWriteToSyncQueue(QueryBuilderBase qb)
        {
            if (qb == null)
                return;
            WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = qb.Parameters });
        }
        protected void WriteToSyncQueue(SyncObject data)
        {
            if (user.InstanceTypeId() != 1)
            {
                if (user.OfflineStatus() == "True")
                    WriteToOfflineSyncQueue(data);
                else
                {
                    if (user.OfflineInstalled() == "True")
                        WriteToOnlineSyncQueue(data);
                }
            }

        }
        protected void WriteToSyncQueue(List<SyncObject> syncObjectList)
        {
            if (user.InstanceTypeId() != 1)
            {
                if (user.OfflineStatus() == "True")
                    WriteToOfflineSyncQueue(syncObjectList);
                else
                {
                    if (user.OfflineInstalled() == "True")
                        foreach (var data in syncObjectList)
                        {
                            WriteToOnlineSyncQueue(data, true);
                        }
                    //if (user.OfflineInstalled() == "True")
                    //{
                    //    if(IsSyncEnabled(syncObjectList[0].AccountId, syncObjectList[0].InstanceId))
                    //    {
                    //        foreach (var data in syncObjectList)
                    //        {
                    //            WriteToOnlineSyncQueue(data, true);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        foreach (var data in syncObjectList)
                    //        {
                    //            WriteToOnlineSyncPending(data);
                    //        }
                    //    }
                    //}
                }
            }
        }
        protected void WriteToOnlineSyncQueue(SyncObject data, string sInstanceId = "")
        {
            //if (IsSyncEnabled(data.AccountId, data.InstanceId)) 
            //    WriteToOnlineSyncQueue(data, true, sInstanceId);
            //else
            //    WriteToOnlineSyncPending(data, sInstanceId);

            if (user.InstanceTypeId() != 1)
            {
                WriteToOnlineSyncQueue(data, true, sInstanceId);
            }
        }

        protected void WriteToOnlineSyncTargetQueue(SyncObject data)
        {
            if (user.InstanceTypeId() != 1)
            {
                //if (IsSyncEnabled(data.AccountId, data.InstanceId)) 
                //    WriteToOnlineSyncTargetQueue(data, true);
                //else
                //    WriteToOnlineSyncTargetPending(data);

                WriteToOnlineSyncTargetQueue(data, true);
            }
        }
        protected void WriteXMLToSyncQueue(SyncObject data, string sFileName = "")
        {
            data.AccountId = user.AccountId();
            data.InstanceId = user.InstanceId();
            WriteToOfflineSyncQueue(data, sFileName);

        }
        protected void WriteToOfflineSyncQueue(SyncObject data, string sFileName = "")
        {
            if (user.InstanceTypeId() != 1)
            {
                //for offline flat file
                data.AccountId = user.AccountId();
                data.InstanceId = user.InstanceId();
                var logFile = System.IO.File.Create(Directory.GetCurrentDirectory() + "/../syncdata/" + string.Format("{0}_{1}{2}", data.Id, DateTime.Now.ToString("yyyyMMddHHmmss"), sFileName));
                var logWriter = new System.IO.StreamWriter(logFile);
                logWriter.WriteLine(JsonConvert.SerializeObject(data));
                logWriter.Dispose();
            }

            //string query = $"INSERT INTO SyncData (AccountID,InstanceId,Action,Data) VALUES (@AccountId, @InstanceId, @Action, @Data)";
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //qb.Parameters.Add("AccountId", data.Parameters.Keys.Contains("AccountId") ? data.Parameters["AccountId"] : user.AccountId());
            //qb.Parameters.Add("InstanceId", data.Parameters.Keys.Contains("InstanceId") ? data.Parameters["InstanceId"] : user.InstanceId());
            //qb.Parameters.Add("Action", string.IsNullOrEmpty(data.EventLevel) ? "O": data.EventLevel); //Direct Hardcoded value removed and parameter value assigned by Martin on 19/05/2017
            //qb.Parameters.Add("Data", JsonConvert.SerializeObject(data));
            //QueryExecuter.NonQueryAsyc2(qb);
        }

        protected void WriteToOfflineSyncQueue(List<SyncObject> syncObjectList, string sFileName = "")
        {
            if (user.InstanceTypeId() != 1)
            {
                syncObjectList.ForEach(x => { x.AccountId = user.AccountId(); x.InstanceId = user.InstanceId(); });
                //for offline flat file
                var logFile = System.IO.File.Create(Directory.GetCurrentDirectory() + "/../syncdata/" + string.Format("{0}_{1}{2}", Guid.NewGuid().ToString(), DateTime.Now.ToString("yyyyMMddHHmmss"), sFileName));
                var logWriter = new System.IO.StreamWriter(logFile);
                logWriter.WriteLine(JsonConvert.SerializeObject(syncObjectList));
                logWriter.Dispose();
            }

        }
        protected async void WriteToOnlineSyncQueue(SyncObject data, bool sync, string sInstanceId = "")
        {
            try
            {
                //for from ONLINE to OFFLINE SYNC
                data.AccountId = user.AccountId();
                data.InstanceId = user.InstanceId();
                string query = $"INSERT INTO SyncData (AccountID,InstanceId,Action,Data) VALUES (@AccountId, @InstanceId, @Action, @Data)";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                qb.Parameters.Add("AccountId", data.Parameters.Keys.Contains("AccountId") ? data.Parameters["AccountId"] : user.AccountId());
                if (Convert.ToString(sInstanceId) != "")
                {
                    qb.Parameters.Add("InstanceId", sInstanceId);
                }
                else
                {
                    qb.Parameters.Add("InstanceId", data.Parameters.Keys.Contains("InstanceId") ? data.Parameters["InstanceId"] : user.InstanceId());
                }
                qb.Parameters.Add("Action", data.EventLevel);
                qb.Parameters.Add("Data", JsonConvert.SerializeObject(data));
                //qb.Parameters.Add("CreatedAt", DateTime.Now.ToString());
                await QueryExecuter.NonQueryAsyc2(qb);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }
        protected async void WriteToOnlineSyncTargetQueue(SyncObject data, bool sync)
        {
            try
            {
                //for from ONLINE to OFFLINE SYNC
                data.AccountId = user.AccountId();
                data.InstanceId = user.InstanceId();
                string query = $"INSERT INTO SyncData (AccountID,InstanceId,Action,Data) VALUES (@AccountId, @InstanceId, @Action, @Data)";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                qb.Parameters.Add("AccountId", data.Parameters.Keys.Contains("AccountId") ? data.Parameters["AccountId"] : user.AccountId());
                qb.Parameters.Add("InstanceId", data.Parameters.Keys.Contains("ToInstanceId") ? data.Parameters["ToInstanceId"] : data.Parameters["InstanceId"]);
                qb.Parameters.Add("Action", data.EventLevel);
                qb.Parameters.Add("Data", JsonConvert.SerializeObject(data));
                //qb.Parameters.Add("CreatedAt", DateTime.Now.ToString());
                await QueryExecuter.NonQueryAsyc2(qb);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }
        public async Task<T> Insert<T>(T data, IDbTable tableName) where T : IContract
        {
            data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            await ExecuteInsert(data, tableName);
            return data;
        }
        public T SetInsertMetaData<T>(T data, IDbTable tableName) where T : IContract
        {
            data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            return data;
        }
        public QueryBuilderBase WriteInsertExecutionQuery<T>(T data, IDbTable tableName) where T : IContract
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Insert);
            foreach (var dbColumnName in tableName.ColumnList)
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            data.AddExecutionQuery(qb);
            return qb;
        }
        public async Task<T> InsertCustomerPayment<T>(T data, IDbTable tablename) where T : IContract
        {
            data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            await ExecuteInsert(data, tablename);
            return data;
        }
        protected async Task<T> ExecuteInsert<T>(T data, IDbTable tableName) where T : IContract
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Insert);
            foreach (var dbColumnName in tableName.ColumnList)
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            await QueryExecuter.NonQueryAsyc(qb);
            //WriteToSyncQueue(new SyncObject() { Id = data.Id, QueryText= qb.GetQuery(), Parameters=qb.Parameters});  //Need to remove

            return data;
        }
        public async Task<T> Insert2<T>(T data, IDbTable tableName) where T : IContract
        {
            data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            await ExecuteInsert2(data, tableName);
            return data;
        }
        protected async Task<T> ExecuteInsert2<T>(T data, IDbTable tableName) where T : IContract
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Insert);
            foreach (var dbColumnName in tableName.ColumnList)
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            await QueryExecuter.NonQueryAsyc2(qb);
            WriteToSyncQueue(new SyncObject() { Id = data.Id, QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "A" });
            return data;
        }

        /* - Added By Sumathi on 26-Feb-2019 -   */
        public async Task<T> Insert3<T>(T data, IDbTable tableName) where T : IContract
        {
            data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;          
            await ExecuteInsert3(data, tableName);

            return data;
        }
        protected async Task<T> ExecuteInsert3<T>(T data, IDbTable tableName) where T : IContract
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Insert);
            foreach (var dbColumnName in tableName.ColumnList)
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            await QueryExecuter.NonQueryAsyc(qb);
            if (data.OfflineStatus)
            {
                WriteToOnlineSyncQueue(new SyncObject() { Id = data.Id, QueryText = qb.GetQuery(), Parameters = qb.Parameters },true,data.InstanceId);
            }
            return data;
        }


        // NO need to call WriteToSyncQueue method. by San
        protected async Task<T> OfflineInstanceInsert<T>(T data, IDbTable tableName) where T : IContract
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Insert);
            foreach (var dbColumnName in tableName.ColumnList)
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            await QueryExecuter.NonQueryAsyc2(qb);
            return data;
        }
        protected async Task<T> Update<T>(T data, IDbTable tableName) where T : IContract
        {
            return await ExecuteUpdate(data, tableName);
        }
        protected async Task<T> ExecuteUpdate<T>(T data, IDbTable tableName) where T : IContract
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Update);
            foreach (var dbColumnName in tableName.ColumnList.Where(QueryBuilderBase.UpdateSkipColumn))
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            qb.Parameters.Add(GetIdDbColumn(tableName).ColumnName, data["Id"]);
            qb.ConditionBuilder.And(GetIdDbColumn(tableName));
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public QueryBuilderBase WriteUpdateExecutionQuery<T>(T data, IDbTable tableName) where T : IContract
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Update);
            foreach (var dbColumnName in tableName.ColumnList.Where(QueryBuilderBase.UpdateSkipColumn))
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            qb.Parameters.Add(GetIdDbColumn(tableName).ColumnName, data["Id"]);
            qb.ConditionBuilder.And(GetIdDbColumn(tableName));
            data.AddExecutionQuery(qb);
            return qb;
        }

        protected async Task<T> Update2<T>(T data, IDbTable tableName) where T : IContract
        {
            return await ExecuteUpdate2(data, tableName);
        }

        protected async Task<T> ExecuteUpdate2<T>(T data, IDbTable tableName) where T : IContract
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Update);
            foreach (var dbColumnName in tableName.ColumnList.Where(QueryBuilderBase.UpdateSkipColumn))
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            qb.Parameters.Add(GetIdDbColumn(tableName).ColumnName, data["Id"]);
            qb.Parameters.Add("AccountId", data["AccountId"]);
            qb.Parameters.Add("InstanceId", data["InstanceId"]);
            qb.ConditionBuilder.And(GetIdDbColumn(tableName));
            await QueryExecuter.NonQueryAsyc2(qb);
            WriteToSyncQueue(new SyncObject() { Id = data.Id, QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "A" });
            return data;
        }
        public async Task<bool> Delete<T>(T data, IDbTable tableName) where T : IContract
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Delete);
            qb.Parameters.Add(GetIdDbColumn(tableName).ColumnName,
                data[GetIdDbColumn(tableName).ColumnName]);
            var criteria = new CustomCriteria(new CriteriaColumn(GetIdDbColumn(tableName)));
            qb.ConditionBuilder.And(criteria);
            await QueryExecuter.NonQueryAsyc(qb);
            return true;
        }
        public QueryBuilderBase WriteDeleteExecutionQuery<T>(T data, IDbTable tableName) where T : IContract
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Delete);
            qb.Parameters.Add(GetIdDbColumn(tableName).ColumnName,
                data[GetIdDbColumn(tableName).ColumnName]);
            var criteria = new CustomCriteria(new CriteriaColumn(GetIdDbColumn(tableName)));
            qb.ConditionBuilder.And(criteria);
            data.AddExecutionQuery(qb);
            return qb;
        }
        public async Task<bool> DeleteRecords<T>(T data, IDbTable tableName, IDbColumn columnName, object columnValue) where T : IContract
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Delete);
            qb.ConditionBuilder.And(columnName, columnValue);
            await QueryExecuter.NonQueryAsyc(qb);
            return true;
        }
        public abstract Task<T> Save<T>(T data) where T : IContract;
        public async Task<T> SaveCustomerPayment<T>(T data) where T : IContract
        {
            data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            await ExecuteInsert(data, CustomerPaymentTable.Table);
            return data;
        }
        protected Task<List<T>> List<T>(IDbTable tableName) where T : IContract, new()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Select);
            return List<T>(qb);
        }
        protected async Task<List<T>> List<T>(QueryBuilderBase query) where T : IContract, new()
        {
            var list = new List<T>();
            using (var reader = await QueryExecuter.QueryAsyc(query))
            {
                CreateListFromDataReader(list, reader);
            }
            return list;
        }
        protected async Task<List<T>> List<T>(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction) where T : IContract, new()
        {
            var list = new List<T>();
            using (var reader = await QueryExecuter.QueryAsyc(query, con, transaction))
            {
                CreateListFromDataReader(list, reader);
            }
            return list;
        }
        protected QueryBuilderBase CreateQueryBuilder<T>(IDbTable table, T data) where T : IContract
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(table, OperationType.Insert);
            foreach (var dbColumnName in table.ColumnList)
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            return qb;
        }
        public async Task ExecuteQueryBuilderForSave(IEnumerable<QueryBuilderBase> queries)
        {
            //Execute queries under transaction
            //await QueryExecuter.ExecuteNonQueryAsyc(queries);  //Commented by Poongodi on 12/08/2017

        }
        private void CreateListFromDataReader<T>(List<T> list, DbDataReader reader) where T : IContract, new()
        {
            while (reader.Read())
            {
                var obj = new T();
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    //var name = reader.GetName(i);
                    //var value = reader.GetValue(i);
                    //obj[name] = value;
                    //System.Diagnostics.Debug.WriteLine($"name : {name} and value : {value}");
                    obj[reader.GetName(i)] = reader.GetValue(i);
                }
                list.Add(obj);
            }
        }
        protected async Task<object> SingleValueAsyc(QueryBuilderBase query)
        {
            return await QueryExecuter.SingleValueAsyc(query);
        }
        protected async Task<object> SingleValueAsyc(QueryBuilderBase query, SqlConnection con, SqlTransaction transaction)
        {
            return await QueryExecuter.SingleValueAsyc(query, con, transaction);
        }
        protected static IDbColumn GetIdDbColumn(IDbTable tableName)
        {
            return tableName.ColumnList.First(x => x.ToString() == "Id");
        }
        protected static IDbColumn GetIDbColumn(IDbTable tableName, string columnName)
        {
            return tableName.ColumnList.First(x => x.ToString() == columnName);
        }
        protected async Task<bool> DoesRecordExist(IDbTable tableName, string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Count);
            var column = tableName.ColumnList.First(col => col.ColumnName == "Id");
            var criteriaColumn = new CriteriaColumn(column);
            var customCriteria = new CustomCriteria(criteriaColumn);
            qb.Parameters.Add(column.ColumnName, id);
            qb.ConditionBuilder.And(customCriteria);
            var result = await QueryExecuter.SingleValueAsyc(qb);
            return Convert.ToInt16(result) > 0;
        }
        protected QueryBuilderBase AddColumn(QueryBuilderBase qb, IDbColumn column, object value)
        {
            qb.ConditionBuilder.And(column, Utilities.Enum.CriteriaEquation.Greater);
            qb.Parameters.Add(column.ColumnName, value);
            return qb;
        }
        public async Task<bool> SaveRemark(ReminderRemark data)
        {
            await Insert(data, ReminderRemarkTable.Table);
            return true;
        }
        /// <summary>
        /// Get offline status from instance table
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public async Task<Boolean> getOfflineStatusValue(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.AddColumn(InstanceTable.OfflineStatusColumn);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, instanceId);
            var offlineStatusValue = await SingleValueAsyc(qb);
            if (offlineStatusValue == DBNull.Value)
            {
                offlineStatusValue = 0;
            }
            return Convert.ToBoolean(offlineStatusValue);
        }
        public async Task<Boolean> getLastSalesPrintType(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.BillPrintColumn);
            qb.SelectBuilder.SetTop(1);
            qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            var offlineStatusValue = await SingleValueAsyc(qb);
            if (offlineStatusValue == null)
            {
                offlineStatusValue = 0;
            }
            return Convert.ToBoolean(offlineStatusValue);
        }
        public async Task<Boolean> getLastSalesSmsType(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.SendSmsColumn);
            qb.SelectBuilder.SetTop(1);
            qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            var offlineStatusValue = await SingleValueAsyc(qb);
            if (offlineStatusValue == null)
            {
                offlineStatusValue = 0;
            }
            return Convert.ToBoolean(offlineStatusValue);
        }
        public async Task<Boolean> getLastSalesEmailType(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.SendEmailColumn);
            qb.SelectBuilder.SetTop(1);
            qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            var offlineStatusValue = await SingleValueAsyc(qb);
            if (offlineStatusValue == null)
            {
                offlineStatusValue = 0;
            }
            return Convert.ToBoolean(offlineStatusValue);
        }

        public async Task<string> PrepareInsert<T>(T data, IDbTable tableName) where T : IContract
        {

            data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            //loop
            string str = "@c1";
            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Insert);
            string qbstr = qb.GetQuery().Replace("@", str);
            foreach (var dbColumnName in tableName.ColumnList)
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }

            return qb.GetQuery();//.Replace('@','@1')

        }

        /// <summary>
        /// Insert Sync Queue method added by Poongodi on 20/09/2017
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="tableName"></param>
        public void WriteSyncQueueInsert<T>(T data, IDbTable tableName, string sEventLevel = "S", string sInstanceId = "", bool bQueue = false) where T : IContract
        {

            var qb = QueryBuilderFactory.GetQueryBuilder(tableName, OperationType.Insert);
            foreach (var dbColumnName in tableName.ColumnList)
            {
                qb.Parameters.Add(dbColumnName.ColumnName, data[dbColumnName.ColumnName]);
            }
            if (bQueue)
                WriteToOnlineSyncQueue(new SyncObject() { Id = data.Id, QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = sEventLevel }, sInstanceId);
            else
            {
                WriteToSyncQueue(new SyncObject() { Id = data.Id, QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = sEventLevel });
            }

        }

        public async Task<bool> GetFeatureAccess(int GroupId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomSettingsDetailTable.Table, OperationType.Select);
            qb.AddColumn(CustomSettingsDetailTable.IsActiveColumn);
            qb.ConditionBuilder.And(CustomSettingsDetailTable.AccountIdColumn, user.AccountId());
            qb.ConditionBuilder.And(CustomSettingsDetailTable.InstanceIdColumn, user.InstanceId());
            qb.ConditionBuilder.And(CustomSettingsDetailTable.CustomSettingsGroupIdColumn, GroupId);
            var result = await SingleValueAsyc(qb);
            if (result == null)
            {
                result = false;
            }
            return Convert.ToBoolean(result);
        }

        public async Task<bool> GetFeatureAccess(int GroupId, string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomSettingsDetailTable.Table, OperationType.Select);
            qb.AddColumn(CustomSettingsDetailTable.IsActiveColumn);
            qb.ConditionBuilder.And(CustomSettingsDetailTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(CustomSettingsDetailTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(CustomSettingsDetailTable.CustomSettingsGroupIdColumn, GroupId);
            var result = await SingleValueAsyc(qb);
            if (result == null)
            {
                result = false;
            }
            return Convert.ToBoolean(result);
        }

        //ExecuteTransactionOrder is to avoid deadlock issue
        public async Task<bool> ExecuteTransactionOrder(string tableList, SqlConnection con, SqlTransaction transaction)
        {
            var query = $@"SELECT * FROM TransactionOrder WHERE TableName IN({tableList}) ORDER BY Id";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var result = reader["LockQuery"].ToString() ?? "";
                    if (!string.IsNullOrEmpty(result))
                    {
                        var qbLockQuery = QueryBuilderFactory.GetQueryBuilder(result);
                        qbLockQuery.Parameters.Add("AccountId", user.AccountId());
                        qbLockQuery.Parameters.Add("InstanceId", user.InstanceId());
                        await QueryExecuter.NonQueryAsyc(qbLockQuery, con, transaction);
                    }
                }
            }
            return true;
        }

        // Data Correction Insert Query

        public async Task<T> InsertDataCorrection<T>(T data, IDbTable tableName) where T : IContract
        {
            data.Id = Guid.NewGuid().ToString();
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            await ExecuteInsert2(data, tableName);
            return data;
        }
        public async Task<T> InsertAPIDataCorrection<T>(T data, IDbTable tableName) where T : IContract
        {
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            await ExecuteInsert2(data, tableName);
            return data;
        }
        protected async void WriteToOnlineSyncPending(SyncObject data, string sInstanceId = "")
        {
            try
            {
                //for from ONLINE to OFFLINE SYNC
                data.AccountId = user.AccountId();
                data.InstanceId = user.InstanceId();
                //SyncData_Reverse_Pending
                string query = $"INSERT INTO SyncData (AccountID,InstanceId,Action,Data) VALUES (@AccountId, @InstanceId, @Action, @Data)";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                qb.Parameters.Add("AccountId", data.Parameters.Keys.Contains("AccountId") ? data.Parameters["AccountId"] : user.AccountId());
                if (Convert.ToString(sInstanceId) != "")
                {
                    qb.Parameters.Add("InstanceId", sInstanceId);
                }
                else
                {
                    qb.Parameters.Add("InstanceId", data.Parameters.Keys.Contains("InstanceId") ? data.Parameters["InstanceId"] : user.InstanceId());
                }
                qb.Parameters.Add("Action", data.EventLevel);
                qb.Parameters.Add("Data", JsonConvert.SerializeObject(data));
                //qb.Parameters.Add("CreatedAt", DateTime.Now.ToString());
                await QueryExecuter.NonQueryAsyc2(qb);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }

        protected async void WriteToOnlineSyncTargetPending(SyncObject data)
        {
            try
            {
                //for from ONLINE to OFFLINE SYNC
                data.AccountId = user.AccountId();
                data.InstanceId = user.InstanceId();
                //SyncData_Reverse_Pending
                string query = $"INSERT INTO SyncData(AccountID,InstanceId,Action,Data) VALUES (@AccountId, @InstanceId, @Action, @Data)";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                qb.Parameters.Add("AccountId", data.Parameters.Keys.Contains("AccountId") ? data.Parameters["AccountId"] : user.AccountId());
                qb.Parameters.Add("InstanceId", data.Parameters.Keys.Contains("ToInstanceId") ? data.Parameters["ToInstanceId"] : data.Parameters["InstanceId"]);
                qb.Parameters.Add("Action", data.EventLevel);
                qb.Parameters.Add("Data", JsonConvert.SerializeObject(data));
                //qb.Parameters.Add("CreatedAt", DateTime.Now.ToString());
                await QueryExecuter.NonQueryAsyc2(qb);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }

        public bool IsSyncEnabled(string AccountId, string InstanceId)
        {
            //   List<CustomSettingsDetail> lst = await this.GetCustomSettings(AccountId, InstanceId, 5);
            //  CustomSettingsDetail cs = lst.FirstOrDefault<CustomSettingsDetail>();
            return this.GetCustomSettings(AccountId, InstanceId, 5).Result;

        }

        private async Task<bool> GetCustomSettings(string AccountId, string InstanceId, int customSettingsGroupId)
        {

            var qb = QueryBuilderFactory.GetQueryBuilder("usp_get_customSettingsforSyncData");
            qb.Parameters.Add("@AccountId", AccountId);
            qb.Parameters.Add("@InstanceId", InstanceId);
            qb.Parameters.Add("@CustomSettingsGroupId", customSettingsGroupId);
            bool result = false;
            using (var reader = await QueryExecuter.ExecuteProcAsyc(qb))
            {
                while (reader.Read())
                {
                    result = reader["IsActive"] != DBNull.Value ? (bool)reader["IsActive"] : false;
                }
            }
            return result;

        }
    }
}
