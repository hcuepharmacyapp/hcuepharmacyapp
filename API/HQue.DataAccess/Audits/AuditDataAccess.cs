﻿using System.Collections.Generic;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using System;
using System.Threading.Tasks;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
namespace HQue.DataAccess.Audits
{
    public class AuditDataAccess : BaseDataAccess
    {
        public AuditDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {
        }
        public override Task<VendorPurchaseItemAudit> Save<VendorPurchaseItemAudit>(VendorPurchaseItemAudit data)
        {
            throw new NotImplementedException();
        }
        public async Task<IEnumerable<VendorPurchaseItem>> SaveAudit(IEnumerable<VendorPurchaseItem> itemList)
        {
            foreach (var vendorPurchaseItem in itemList)
            {
                VendorPurchaseItem auditData;
                if (vendorPurchaseItem.Action == "I" && vendorPurchaseItem.WriteExecutionQuery)
                {
                    auditData = vendorPurchaseItem;
                }
                else
                {
                    auditData = GetActiveRecord(vendorPurchaseItem.Id).Result[0];
                }
                auditData.Action = vendorPurchaseItem.Action;
                //added by nandhini for transaction
                auditData.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);
                if (auditData.WriteExecutionQuery)
                {
                    SetInsertMetaData(auditData, VendorPurchaseItemAuditTable.Table);
                    WriteInsertExecutionQuery(auditData, VendorPurchaseItemAuditTable.Table);
                }
                else
                {
                    await Insert(auditData, VendorPurchaseItemAuditTable.Table);
                }

            }
            return itemList;
        }
        public Task<List<VendorPurchaseItem>> GetActiveRecord(string itemId)
        {
            var query =
                $@"SELECT * FROM VendorPurchaseItem WHERE Id = '{itemId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return List<VendorPurchaseItem>(qb);
        }
    }
}
