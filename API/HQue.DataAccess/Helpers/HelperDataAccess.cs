﻿using DataAccess;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Helpers;

namespace HQue.DataAccess.Helpers
{
    public class HelperDataAccess 
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        // private SalesReturnItem salesReturnItem;

        //public int TotalMrp { get; private set; }
        //public int TotalPurchasePrice { get; private set; }
        //public decimal ReturnProfit { get; private set; }
        public HelperDataAccess(  ConfigHelper configHelper)  
        {
            // Done 23-12-2016
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        public async Task<string> GetNumber(TableName tableName, DateTime Date, string sInstanceId, string sAccountid, string sInvoiceSeries, string sPrefix = "", SqlConnection con = null, SqlTransaction transaction = null)
        {
            string sbill = "";

            /*Prefix added by Poongodi on 10/10/2017*/
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", sInstanceId);
            parms.Add("AccountId", sAccountid);
            parms.Add("BillDate", Date);
            parms.Add("TableName", tableName.ToString());
            parms.Add("InvoiceSeries", sInvoiceSeries.ToString());
            parms.Add("Prefix", Convert.ToString(sPrefix));


            List<Bill> result = null;
            if (con != null)
            {
                result = (await sqldb.ExecuteProcedureAsync<Bill>("usp_GetBillNumber", parms, con, transaction)).ToList();
            }
            else
            {
                result = (await sqldb.ExecuteProcedureAsync<Bill>("usp_GetBillNumber", parms)).ToList();
            }
            if (result != null)
            {
                sbill = result.FirstOrDefault().BillNo;
            }
            return sbill;
        }
        public async Task<string> GetFinyear( DateTime Date, string sInstanceId, string sAccountid)
        {
            string sFinYearId = "";


            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", sInstanceId);
            parms.Add("AccountId", sAccountid);
            parms.Add("BillDate", Date);

           var result = await sqldb.ExecuteProcedureAsync<Bill>("usp_Get_Finyear", parms);
            if (result != null)
            {
                sFinYearId = result.FirstOrDefault().FinYearId;
            }
            return sFinYearId;

        }
        public enum TableName
        {
            VendorPurchase = 0,
            Sales = 1,
            PurchaseReturn = 2,
            SalesReturn = 3,
            Transfer = 4,
            Order = 5,
            VendorPurchaseReturns = 6,
            SalesCancel = 7,
            SalesTemplate = 8, // Added by Gavaskar 09-10-2017
            SalesOrderEstimate = 9, // Added by Gavaskar 16-10-2017
            Patient = 10 //Added by Bikas 02/07/2018
        }
        // Added by Settu regarding voucher/settlements transaction type
        public enum TransactionType
        {
            SalesReturn = 1,
            VendorReturn = 2,
        }

        public enum VoucherType
        {
            DebitNote = 1,
            CreditNote = 2,            
        }

        public enum VoucherFactor
        {
            SalesReturn = -1,
            VendorReturn = 1,
        }

        public enum SettlementsTransactionType
        {
            Sales = 1,
            Purchase = 2,
        }
    }
    public class Bill
    {
        public string BillNo { get; set; }
        public string FinYearId { get; set; }
    }
}
