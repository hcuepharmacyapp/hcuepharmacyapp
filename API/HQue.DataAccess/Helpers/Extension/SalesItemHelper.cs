using System.Data.Common;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class SalesItemHelper
    {
        public static SalesItem Fill(this SalesItem salesItem, DbDataReader reader)
        {
            salesItem.Id = reader.ToString(SalesItemTable.IdColumn.ColumnName);
            salesItem.SaleProductName = reader.ToString(SalesItemTable.SaleProductNameColumn.ColumnName);
            salesItem.ProductStockId = reader.ToString(SalesItemTable.ProductStockIdColumn.ColumnName);
            salesItem.Quantity = reader.ToDecimal(SalesItemTable.QuantityColumn.ColumnName);
            salesItem.SellingPrice = reader.ToDecimalNullable(SalesItemTable.SellingPriceColumn.ColumnName, 0);
            salesItem.SalesId = reader.ToString(SalesItemTable.SalesIdColumn.ColumnName);
            salesItem.Discount = reader.ToDecimalNullable(SalesItemTable.DiscountColumn.ColumnName);
            salesItem.DiscountAmount = reader.ToDecimalNullable(SalesItemTable.DiscountAmountColumn.ColumnName);
            salesItem.Quantity = reader.ToDecimalNullable(SalesItemTable.QuantityColumn.ColumnName, 0);
            salesItem.ReminderFrequency = reader.ToIntNullable(SalesItemTable.ReminderFrequencyColumn.ColumnName, 0);
            salesItem.ReminderDate = reader.ToDateTime(SalesItemTable.ReminderDateColumn.ColumnName, null);
            salesItem.MRP = reader.ToDecimalNullable(SalesItemTable.MRPColumn.ColumnName, 0);
            salesItem.Igst = reader[SalesItemTable.IgstColumn.ColumnName] as decimal? ?? 0;
            salesItem.Cgst = reader[SalesItemTable.CgstColumn.ColumnName] as decimal? ?? 0;
            salesItem.Sgst = reader[SalesItemTable.SgstColumn.ColumnName] as decimal? ?? 0;
            salesItem.GstTotal = reader[SalesItemTable.GstTotalColumn.ColumnName] as decimal? ?? 0;
            salesItem.SalesOrderEstimateType = reader.ToIntNullable(SalesItemTable.SalesOrderEstimateTypeColumn.ColumnName, 0);
            salesItem.SalesOrderEstimateId = reader.ToString(SalesItemTable.SalesOrderEstimateIdColumn.ColumnName);
            salesItem.GstAmount = reader[SalesItemTable.GstAmountColumn.ColumnName] as decimal? ?? 0;
            salesItem.LoyaltyProductPts = reader[SalesItemTable.LoyaltyProductPtsColumn.ColumnName] as decimal? ?? 0;
            //salesItem.Igst = reader.ToDecimalNullable(SalesItemTable.IgstColumn.ColumnName, 0);
            //salesItem.Cgst = reader.ToDecimalNullable(SalesItemTable.CgstColumn.ColumnName, 0);
            //salesItem.Sgst = reader.ToDecimalNullable(SalesItemTable.SgstColumn.ColumnName, 0);
            //salesItem.GstTotal = reader.ToDecimalNullable(SalesItemTable.GstTotalColumn.ColumnName, 0);
            return salesItem;
        }
    }
}