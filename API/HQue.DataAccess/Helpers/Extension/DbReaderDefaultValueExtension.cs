using System;
using System.Data.Common;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class DbReaderDefaultValueExtension
    {
        public static int? ToIntNullable(this DbDataReader reader, string columnName, int? defaultValue)
        {
            return HandelConvertionException(() => reader.ToIntNullable(columnName), defaultValue);
        }

        public static decimal? ToDecimalNullable(this DbDataReader reader, string columnName, decimal? defaultValue)
        {
            return HandelConvertionException(() => reader.ToDecimalNullable(columnName), defaultValue);
        }

        public static DateTime? ToDateTime(this DbDataReader reader, string columnName, DateTime? defaultValue)
        {
            return HandelConvertionException(() => reader.ToDateTime(columnName), defaultValue);
        }

        public static string ToString(this DbDataReader reader, string columnName, string defaultValue)
        {
            return HandelConvertionException(() => reader.ToString(columnName), defaultValue);
        }

        public static decimal ToDecimal(this DbDataReader reader, string columnName, decimal defaultValue)
        {
            return HandelConvertionException(() => reader.ToDecimal(columnName), defaultValue);
        }

        public static bool? ToBoolNullable(this DbDataReader reader, string columnName, bool? defaultValue)
        {
            return HandelConvertionException(() => reader.ToBoolNullable(columnName), defaultValue);
        }

        private static T HandelConvertionException<T>(Func<T> action, T defaultValue)
        {
            try
            {
                return action();
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}