using System.Data.Common;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class SalesItemAuditHelper
    {
        public static SalesItemAudit Fill(this SalesItemAudit SalesItemAudit, DbDataReader reader)
        {
            SalesItemAudit.Id = reader.ToString(SalesItemAuditTable.IdColumn.ColumnName);
            SalesItemAudit.ProductStockId = reader.ToString(SalesItemAuditTable.ProductStockIdColumn.ColumnName);
            SalesItemAudit.Quantity = reader.ToDecimal(SalesItemAuditTable.QuantityColumn.ColumnName);
            SalesItemAudit.SellingPrice = reader.ToDecimalNullable(SalesItemAuditTable.SellingPriceColumn.ColumnName, 0);
            SalesItemAudit.SalesId = reader.ToString(SalesItemAuditTable.SalesIdColumn.ColumnName);
            SalesItemAudit.Discount = reader.ToDecimalNullable(SalesItemAuditTable.DiscountColumn.ColumnName);
            SalesItemAudit.Quantity = reader.ToDecimalNullable(SalesItemAuditTable.QuantityColumn.ColumnName, 0);
            SalesItemAudit.ReminderFrequency = reader.ToIntNullable(SalesItemAuditTable.ReminderFrequencyColumn.ColumnName, 0);
            SalesItemAudit.CreatedAt = reader.ToDateTime(SalesItemAuditTable.CreatedAtColumn.ColumnName);

            return SalesItemAudit;
        }
    }
}