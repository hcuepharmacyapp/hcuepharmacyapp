using System.Data.Common;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class ProductStockHelper
    {
        public static ProductStock Fill(this ProductStock productStock,DbDataReader reader)
        {
            productStock.Id = reader.ToString(ProductStockTable.IdColumn.FullColumnName);
            productStock.VendorId = reader.ToString(ProductStockTable.VendorIdColumn.FullColumnName);
            productStock.BatchNo = reader.ToString(ProductStockTable.BatchNoColumn.FullColumnName);
            productStock.PurchaseBarcode = reader.ToString(ProductStockTable.PurchaseBarcodeColumn.FullColumnName);
            productStock.BarcodeProfileId = reader.ToString(ProductStockTable.BarcodeProfileIdColumn.FullColumnName);
            productStock.Stock = reader.ToDecimalNullable(ProductStockTable.StockColumn.FullColumnName);
            productStock.CST = reader.ToDecimalNullable(ProductStockTable.CSTColumn.FullColumnName);
            productStock.IsMovingStock = reader.ToBoolNullable(ProductStockTable.IsMovingStockColumn.FullColumnName,false);
            productStock.ReOrderQty = reader.ToDecimalNullable(ProductStockTable.ReOrderQtyColumn.FullColumnName);
            productStock.Status = reader.ToIntNullable(ProductStockTable.StatusColumn.FullColumnName,null);
            productStock.ProductId = reader.ToString(ProductStockTable.ProductIdColumn.FullColumnName);
            productStock.SellingPrice = reader.ToDecimalNullable(ProductStockTable.SellingPriceColumn.FullColumnName);
            productStock.MRP = reader.ToDecimalNullable(ProductStockTable.MRPColumn.FullColumnName);
            productStock.VAT = reader.ToDecimalNullable(ProductStockTable.VATColumn.FullColumnName);
            productStock.GstTotal = reader[ProductStockTable.GstTotalColumn.FullColumnName] as decimal? ?? 0;
            productStock.ExpireDate = reader.ToDateTimeNullable(ProductStockTable.ExpireDateColumn.FullColumnName);
            productStock.PackageSize = reader.ToDecimalNullable(ProductStockTable.PackageSizeColumn.FullColumnName);
            productStock.PackagePurchasePrice = reader.ToDecimalNullable(ProductStockTable.PackagePurchasePriceColumn.FullColumnName);
            productStock.PurchasePrice = reader.ToDecimalNullable(ProductStockTable.PurchasePriceColumn.FullColumnName);
            productStock.HsnCode = reader.ToString(ProductStockTable.HsnCodeColumn.FullColumnName);
            return productStock;
        }
    }
}