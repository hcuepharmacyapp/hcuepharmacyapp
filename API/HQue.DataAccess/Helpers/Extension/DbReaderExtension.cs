using System;
using System.Data.Common;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class DbReaderExtension
    {
        public static int? ToIntNullable(this DbDataReader reader, string columnName)
        {
            try
            {
                return Convert.ToInt32(reader[columnName]);
            }
            catch
            {
                throw new Exception(Message(columnName, "int"));
            }
        }

        public static decimal? ToDecimalNullable(this DbDataReader reader, string columnName)
        {
            try
            {
                return reader[columnName] as decimal?;
            }
            catch
            {
                throw new Exception(Message(columnName, "decimal"));
            }
        }

        public static DateTime ToDateTime(this DbDataReader reader, string columnName)
        {
            try
            {
                return (DateTime)reader[columnName];
            }
            catch
            {
                throw new Exception(Message(columnName, "datetime"));
            }
        }

        public static DateTime ToDateTimeNullable(this DbDataReader reader, string columnName)
        {
            try
            {
                if (reader[columnName] is DBNull)
                {
                    return Convert.ToDateTime("01-01-0001");
                }
                return (DateTime)reader[columnName];
            }
            catch
            {
                throw new Exception(Message(columnName, "datetime"));
            }
        }

        public static string ToString(this DbDataReader reader, string columnName)
        {
            try
            {
                return reader[columnName].ToString();
            }
            catch
            {
                throw new Exception(Message(columnName, "string"));
            }
        }

        public static decimal ToDecimal(this DbDataReader reader, string columnName)
        {
            try
            {
                return Convert.ToDecimal(reader[columnName]);
            }
            catch
            {
                throw new Exception(Message(columnName, "decimal"));
            }
        }

        public static bool? ToBoolNullable(this DbDataReader reader, string columnName)
        {
            try
            {
                return Convert.ToBoolean(reader[columnName]);
            }
            catch
            {
                throw new Exception(Message(columnName, "bool"));
            }
        }

        private static string Message(string columnName, string type)
        {
            return $"Unable to convert value to {type} for column {columnName}, if you expect error while converting use the overload with default value";
        }
    }
}