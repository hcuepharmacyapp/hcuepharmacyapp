using System;
using System.Data.Common;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.DbModel;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class SalesHelper
    {
        public static Sales Fill(this Sales sales, DbDataReader reader)
        {
            /*Prefix Added by Poongodi on 15/06/2017*/
            if (sales == null)
                sales = new Sales();
            sales.Id = reader.ToString(SalesTable.IdColumn.FullColumnName);
            sales.InvoiceSeries = reader.ToString(SalesTable.InvoiceSeriesColumn.FullColumnName);
            sales.InvoiceNo = reader.ToString(SalesTable.InvoiceNoColumn.FullColumnName);
            sales.InvoiceDate = reader.ToDateTime(SalesTable.InvoiceDateColumn.FullColumnName);
            sales.DoctorName = reader.ToString(SalesTable.DoctorNameColumn.FullColumnName);
            sales.DoctorMobile = reader.ToString(SalesTable.DoctorMobileColumn.FullColumnName);
            sales.DoctorId = reader.ToString(SalesTable.DoctorIdColumn.FullColumnName);
            //sales.DoctorAddress = reader.ToString(SalesTable.DoctorIdColumn.FullColumnName);
            sales.BillPrint = reader.ToBoolNullable(SalesTable.BillPrintColumn.FullColumnName);
            sales.SendSms = reader.ToBoolNullable(SalesTable.SendSmsColumn.FullColumnName);
            sales.SendEmail = reader.ToBoolNullable(SalesTable.SendEmailColumn.FullColumnName);
            //sales.Discount = reader.ToDecimalNullable(SalesTable.DiscountColumn.FullColumnName);
            sales.Discount = (reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0) - (reader.ToDecimalNullable(SalesTable.RedeemPercentColumn.FullColumnName));  // Modified by Gavaskar Loyalty Points 12-12-2017 
            sales.Prefix = reader.ToString(SalesTable.PrefixColumn.FullColumnName);
            sales.DiscountValue = reader.ToDecimalNullable(SalesTable.DiscountValueColumn.FullColumnName) - reader.ToDecimalNullable(SalesTable.RedeemAmtColumn.FullColumnName);// Modified by Gavaskar Loyalty Points 12-12-2017 
            sales.DiscountType = reader.ToString(SalesTable.DiscountTypeColumn.FullColumnName) != "" ? reader.ToIntNullable(SalesTable.DiscountTypeColumn.FullColumnName) : 1;
            sales.RoundoffNetAmount = reader.ToDecimalNullable(SalesTable.RoundoffNetAmountColumn.FullColumnName);

            sales.PaymentType = reader.ToString(SalesTable.PaymentTypeColumn.FullColumnName);
            sales.DeliveryType = reader.ToString(SalesTable.DeliveryTypeColumn.FullColumnName);
            sales.Credit = reader.ToDecimalNullable(SalesTable.CreditColumn.FullColumnName);
            sales.FileName = reader.ToString(SalesTable.FileNameColumn.FullColumnName);
            sales.CashType = reader.ToString(SalesTable.CashTypeColumn.FullColumnName);
            sales.CardNo = reader.ToString(SalesTable.CardNoColumn.FullColumnName);
            sales.CardDate = reader.ToDateTime(SalesTable.CardDateColumn.FullColumnName, null);
            sales.CardDigits = reader.ToString(SalesTable.CardDigitsColumn.FullColumnName);
            sales.CardName = reader.ToString(SalesTable.CardNameColumn.FullColumnName);
            sales.SalesType = reader.ToString(SalesTable.SalesTypeColumn.FullColumnName);

            sales.Age = reader.ToIntNullable(SalesTable.AgeColumn.FullColumnName, 0);
            sales.Pincode = reader.ToString(SalesTable.PincodeColumn.FullColumnName);
            sales.Address = reader.ToString(SalesTable.AddressColumn.FullColumnName);
            sales.Gender = reader.ToString(SalesTable.GenderColumn.FullColumnName);
            sales.Name = reader.ToString(SalesTable.NameColumn.FullColumnName);
            sales.Email = reader.ToString(SalesTable.EmailColumn.FullColumnName);
            sales.Mobile = reader.ToString(SalesTable.MobileColumn.FullColumnName);
            sales.PatientId = reader.ToString(SalesTable.PatientIdColumn.FullColumnName);
            sales.DOB = reader.ToDateTime(SalesTable.DOBColumn.FullColumnName, null);

            //Newly created for PrintFooter by Mani on 28-02-2017
            sales.AccountId = reader.ToString(SalesTable.AccountIdColumn.FullColumnName);
            sales.InstanceId = reader.ToString(SalesTable.InstanceIdColumn.FullColumnName);
            //PrintFooter change ends

            sales.SalesCreatedAt = reader.ToDateTime(SalesTable.CreatedAtColumn.FullColumnName);
            sales.ChequeNo  = reader.ToString(SalesTable.ChequeNoColumn.FullColumnName);
            sales.ChequeDate = reader.ToDateTime(SalesTable.ChequeDateColumn.FullColumnName, null);
            sales.BankDeposited = reader.ToBoolNullable(SalesTable.BankDepositedColumn.FullColumnName, false);
            sales.AmountCredited = reader.ToBoolNullable(SalesTable.AmountCreditedColumn.FullColumnName, false);

            sales.DoorDeliveryStatus = reader.ToString(SalesTable.DoorDeliveryStatusColumn.FullColumnName);
            sales.NetAmount = reader.ToDecimalNullable(SalesTable.NetAmountColumn.FullColumnName);
            sales.Patient = new Patient
            {
                Name = reader.ToString(SalesTable.NameColumn.FullColumnName)
            };

            var taxRefNo = reader.ToString(SalesTable.TaxRefNoColumn.FullColumnName);
            sales.TaxRefNo = taxRefNo == "1" ? 1 : 0;
            sales.DiscountPercentage = reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0; //Added by POongodi on 23/11/2017

            sales.GstAmount = reader[SalesTable.GstAmountColumn.FullColumnName] as decimal? ?? 0;
            sales.VatAmount = reader[SalesTable.VatAmountColumn.FullColumnName] as decimal? ?? 0;
            sales.TotalDiscountValue = reader[SalesTable.TotalDiscountValueColumn.FullColumnName] as decimal? ?? 0;
            sales.SaleAmount = reader[SalesTable.SaleAmountColumn.FullColumnName] as decimal? ?? 0;
            sales.RoundoffSaleAmount = reader[SalesTable.RoundoffSaleAmountColumn.FullColumnName] as decimal? ?? 0;
            sales.SalesItemAmount = reader[SalesTable.SalesItemAmountColumn.FullColumnName] as decimal? ?? 0;
            
			 // Added by Gavaskar Loyalty Points 12-12-2017 Start
            sales.RedeemPercent = reader.ToDecimalNullable(SalesTable.RedeemPercentColumn.FullColumnName);
            sales.RedeemPts = reader.ToDecimalNullable(SalesTable.RedeemPtsColumn.FullColumnName);
            sales.RedeemAmt = reader.ToDecimalNullable(SalesTable.RedeemAmtColumn.FullColumnName);
            sales.LoyaltyId = reader.ToString(SalesTable.LoyaltyIdColumn.FullColumnName);
            sales.LoyaltyPts = reader.ToDecimalNullable(SalesTable.LoyaltyPtsColumn.FullColumnName);
            sales.OldLoyaltyPts = reader.ToDecimalNullable(SalesTable.LoyaltyPtsColumn.FullColumnName);            
            // Added by Gavaskar Loyalty Points 12-12-2017 End
            return sales;
        }
    }
}