﻿using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.DbModel;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class StateHelper
    {
        public static State Fill(this State StateRef, DbDataReader reader)
        {
            if (StateRef == null)
                StateRef = new State();

            StateRef.StateCode = reader[StateTable.StateCodeColumn.FullColumnName].ToString() ?? "";
            StateRef.StateName = reader[StateTable.StateNameColumn.FullColumnName].ToString() ?? "";
            StateRef.Id = reader[StateTable.IdColumn.FullColumnName].ToString() ?? "";

            return StateRef;
        }
    }
}
