﻿using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using System;
using System.Data.Common;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class VendorOrderItemHelper
    {
        public static VendorOrderItem Fill(this VendorOrderItem VendorOrderItem, DbDataReader reader)
        {
            VendorOrderItem.Id = reader.ToString(VendorOrderItemTable.IdColumn.ColumnName);
            VendorOrderItem.AccountId = reader.ToString(VendorOrderItemTable.AccountIdColumn.ColumnName);
            VendorOrderItem.InstanceId = reader.ToString(VendorOrderItemTable.InstanceIdColumn.ColumnName);
            VendorOrderItem.VendorOrderId = reader.ToString(VendorOrderItemTable.VendorOrderIdColumn.ColumnName);
            VendorOrderItem.ProductId = reader.ToString(VendorOrderItemTable.ProductIdColumn.ColumnName);       
            //VendorOrderItem.Quantity = reader.ToDecimal(VendorOrderItemTable.QuantityColumn.ColumnName) as decimal? ?? 0;

            VendorOrderItem.Quantity = reader[VendorOrderItemTable.QuantityColumn.ColumnName] as decimal? ?? 0;



            //VendorOrderItem.CreatedAt = reader.ToDateTime(VendorOrderItemTable.CreatedAtColumn.ColumnName) as DateTime? ?? DateTime.MinValue;
            //VendorOrderItem.UpdatedAt = reader.ToDateTime(VendorOrderItemTable.UpdatedAtColumn.ColumnName) as DateTime? ?? DateTime.MinValue;
            VendorOrderItem.CreatedAt = reader[VendorOrderItemTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue;
            VendorOrderItem.UpdatedAt = reader[VendorOrderItemTable.UpdatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue;

            VendorOrderItem.CreatedBy = reader.ToString(VendorOrderItemTable.CreatedByColumn.ColumnName);
            VendorOrderItem.UpdatedBy = reader.ToString(VendorOrderItemTable.UpdatedByColumn.ColumnName);
            VendorOrderItem.VAT = reader[VendorOrderItemTable.VATColumn.ColumnName] as decimal? ?? 0;
            VendorOrderItem.PackageQty = reader[VendorOrderItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0;
            VendorOrderItem.PackageSize = reader[VendorOrderItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0;

            VendorOrderItem.SellingPrice = reader[VendorOrderItemTable.SellingPriceColumn.ColumnName] as decimal? ?? 0;
            VendorOrderItem.PurchasePrice = reader[VendorOrderItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0;
            VendorOrderItem.ExpireDate = reader[VendorOrderItemTable.ExpireDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue;
            VendorOrderItem.IsDeleted = reader[VendorOrderItemTable.IsDeletedColumn.ColumnName] as Boolean? ?? false;
            VendorOrderItem.VendorPurchaseId = reader.ToString(VendorOrderItemTable.VendorPurchaseIdColumn.ColumnName);
            VendorOrderItem.StockTransferId = reader.ToString(VendorOrderItemTable.StockTransferIdColumn.ColumnName);
            VendorOrderItem.OrderItemSno = reader.ToIntNullable(VendorOrderItemTable.OrderItemSnoColumn.ColumnName, 0);
            VendorOrderItem.GstTotal = reader.ToIntNullable(VendorOrderItemTable.GstTotalColumn.ColumnName, 0);
            VendorOrderItem.FreeQty = reader.ToIntNullable(VendorOrderItemTable.FreeQtyColumn.ColumnName, 0);
            return VendorOrderItem;
        }
    }
}
