using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class DbHelperExtension
    {
        public static IDbColumn ToDateColumn(this IDbColumn dateTimeColumn)
        {
            return DbColumn.CustomColumn($"Convert(date,{dateTimeColumn.FullColumnName})");
        }
    }
}