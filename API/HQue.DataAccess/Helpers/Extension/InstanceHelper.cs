using System.Data.Common;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.DbModel;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class InstanceHelper
    {
        public static Instance Fill(this Instance instance, DbDataReader reader)
        {
            instance.Name = reader .ToString(InstanceTable.NameColumn.FullColumnName);
            instance.Address = reader.ToString(InstanceTable.AddressColumn.FullColumnName);
            instance.Area = reader.ToString(InstanceTable.AreaColumn.FullColumnName);
            instance.State = reader.ToString(InstanceTable.StateColumn.FullColumnName);
            instance.City = reader.ToString(InstanceTable.CityColumn.FullColumnName);
            instance.Pincode = reader.ToString(InstanceTable.PincodeColumn.FullColumnName);
            instance.TinNo = reader.ToString(InstanceTable.TinNoColumn.FullColumnName);
            instance.DrugLicenseNo = reader.ToString(InstanceTable.DrugLicenseNoColumn.FullColumnName);
            instance.Phone = reader.ToString(InstanceTable.PhoneColumn.FullColumnName);
            instance.Mobile = reader.ToString(InstanceTable.MobileColumn.FullColumnName);
            instance.BuildingName = reader.ToString(InstanceTable.BuildingNameColumn.FullColumnName);
            instance.StreetName = reader.ToString(InstanceTable.StreetNameColumn.FullColumnName);
            instance.GsTinNo = reader.ToString(InstanceTable.GsTinNoColumn.FullColumnName);
            instance.isUnionTerritory = reader.ToBoolNullable(InstanceTable.isUnionTerritoryColumn.FullColumnName, false);
            instance.Gstselect = reader.ToBoolNullable(InstanceTable.GstselectColumn.FullColumnName, false);
            instance.ContactEmail = reader.ToString(InstanceTable.ContactEmailColumn.FullColumnName);
            return instance;
        }
    }
}