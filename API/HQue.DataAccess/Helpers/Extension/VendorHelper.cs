﻿using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.DbModel;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class VendorHelper
    {
        public static Vendor Fill(this Vendor vendor, DbDataReader reader)
        {
            if (vendor == null)
                vendor = new Vendor();

            vendor.Id = reader[VendorTable.IdColumn.ColumnName].ToString() ?? "";
            vendor.Code = reader[VendorTable.CodeColumn.ColumnName].ToString() ?? "";
            vendor.Name = reader[VendorTable.NameColumn.ColumnName].ToString() ?? "";
            vendor.Mobile = reader[VendorTable.MobileColumn.ColumnName].ToString() ?? "";
            vendor.Phone = reader[VendorTable.PhoneColumn.ColumnName].ToString() ?? "";
            vendor.Email = reader[VendorTable.EmailColumn.ColumnName].ToString() ?? "";
            vendor.Address = reader[VendorTable.AddressColumn.ColumnName].ToString() ?? "";
            vendor.Area = reader[VendorTable.AreaColumn.ColumnName].ToString() ?? "";
            vendor.City = reader[VendorTable.CityColumn.ColumnName].ToString() ?? "";
            vendor.State = reader[VendorTable.StateColumn.ColumnName].ToString() ?? "";
            vendor.Pincode = reader[VendorTable.PincodeColumn.ColumnName].ToString() ?? "";
            vendor.AllowViewStock = reader[VendorTable.AllowViewStockColumn.ColumnName] as bool? ?? false;
            vendor.DrugLicenseNo = reader[VendorTable.DrugLicenseNoColumn.ColumnName].ToString() ?? "";
            vendor.TinNo = reader[VendorTable.TinNoColumn.ColumnName].ToString() ?? "";
            vendor.Status = Convert.ToInt32(reader[VendorTable.StatusColumn.ColumnName]) as int? ?? -1;
            vendor.PaymentType = reader[VendorTable.PaymentTypeColumn.ColumnName].ToString() ?? "";
            vendor.CreditNoOfDays = reader[VendorTable.CreditNoOfDaysColumn.ColumnName] as int? ?? 0;
            vendor.EnableCST = reader[VendorTable.EnableCSTColumn.ColumnName] as bool? ?? false;
            vendor.ContactPerson = reader[VendorTable.ContactPersonColumn.ColumnName].ToString() ?? "";
            vendor.Designation = reader[VendorTable.DesignationColumn.ColumnName].ToString() ?? "";
            vendor.Landmark = reader[VendorTable.LandmarkColumn.ColumnName].ToString() ?? "";
            vendor.CSTNo = reader[VendorTable.CSTNoColumn.ColumnName].ToString() ?? "";
            vendor.Discount = reader[VendorTable.DiscountColumn.ColumnName] as decimal? ?? 0;
            vendor.BalanceAmount = reader[VendorTable.BalanceAmountColumn.ColumnName] as decimal? ?? 0;
            vendor.Mobile1 = reader[VendorTable.Mobile1Column.ColumnName].ToString() ?? "";
            vendor.Mobile2 = reader[VendorTable.Mobile2Column.ColumnName].ToString() ?? "";
            vendor.Mobile1checked = reader[VendorTable.Mobile1checkedColumn.ColumnName] as bool? ?? false;
            vendor.Mobile2checked = reader[VendorTable.Mobile2checkedColumn.ColumnName] as bool? ?? false;
            vendor.IsSendMail = reader[VendorTable.IsSendMailColumn.ColumnName] as bool? ?? false;
            vendor.TaxType = reader[VendorTable.TaxTypeColumn.ColumnName].ToString() ?? "";
            vendor.GsTinNo = reader[VendorTable.GsTinNoColumn.ColumnName].ToString() ?? "";
            vendor.LocationType = reader[VendorTable.LocationTypeColumn.ColumnName] as int?;
            vendor.StateId = reader[VendorTable.StateIdColumn.ColumnName].ToString() ?? "";
            vendor.Ext_RefId = reader[VendorTable.Ext_RefIdColumn.ColumnName].ToString();
            vendor.VendorType = reader[VendorTable.VendorTypeColumn.ColumnName] as int?;
            vendor.VendorPurchaseFormulaId = reader[VendorTable.VendorPurchaseFormulaIdColumn.ColumnName].ToString() ?? "";
            return vendor;
        }
    }
}
