using System;
using System.Data.Common;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.DbModel;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class ProductHelper
    {
        public static Product Fill(this Product product, DbDataReader reader)
        {
            product.Id = reader.ToString(ProductTable.IdColumn.FullColumnName);
            product.Code  = reader.ToString(ProductTable.CodeColumn.FullColumnName);
            product.Name = reader.ToString(ProductTable.NameColumn.FullColumnName);
            product.Manufacturer = reader.ToString(ProductTable.ManufacturerColumn.FullColumnName);
            product.KindName = reader.ToString(ProductTable.KindNameColumn.FullColumnName);
            product.StrengthName = reader.ToString(ProductTable.StrengthNameColumn.FullColumnName);
            product.Type = reader.ToString(ProductTable.TypeColumn.FullColumnName);
            product.Schedule = reader.ToString(ProductTable.ScheduleColumn.FullColumnName);
            product.Category = reader.ToString(ProductTable.CategoryColumn.FullColumnName);
            product.GenericName = reader.ToString(ProductTable.GenericNameColumn.FullColumnName);
            product.CommodityCode = reader.ToString(ProductTable.CommodityCodeColumn.FullColumnName);
            product.Packing = reader.ToString(ProductTable.PackingColumn.FullColumnName);
            product.VAT = reader.ToDecimalNullable(ProductTable.VATColumn.FullColumnName);
            product.Price = reader.ToDecimalNullable(ProductTable.PriceColumn.FullColumnName);
            product.PackageSize = reader.ToIntNullable(ProductTable.PackageSizeColumn.FullColumnName,0);
            product.Status = reader.ToIntNullable(ProductTable.StatusColumn.FullColumnName,0);
            product.RackNo = reader.ToString(ProductTable.RackNoColumn.FullColumnName);
            product.HsnCode = reader.ToString(ProductTable.HsnCodeColumn.FullColumnName);
            product.Discount = reader[ProductTable.DiscountColumn.FullColumnName] as decimal? ?? 0;
            return product;
        }
    }
}