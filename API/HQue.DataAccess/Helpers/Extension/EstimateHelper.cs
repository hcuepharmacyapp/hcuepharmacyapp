using System.Data.Common;
using HQue.Contract.Infrastructure.Estimates;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.DbModel;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class EstimateHelper
    {
        public static Estimate Fill(this Estimate estimate, DbDataReader reader)
        {
            if(estimate == null)
                estimate = new Estimate();
            estimate.Id = reader.ToString(EstimateTable.IdColumn.FullColumnName);
            estimate.EstimateNo = reader.ToString(EstimateTable.EstimateNoColumn.FullColumnName);
            estimate.EstimateDate = reader.ToDateTime(EstimateTable.EstimateDateColumn.FullColumnName);
            estimate.DoctorName = reader.ToString(EstimateTable.DoctorNameColumn.FullColumnName);
            estimate.BillPrint = reader.ToBoolNullable(EstimateTable.BillPrintColumn.FullColumnName);
            estimate.SendSms = reader.ToBoolNullable(EstimateTable.SendSmsColumn.FullColumnName);
            estimate.SendEmail = reader.ToBoolNullable(EstimateTable.SendEmailColumn.FullColumnName);
            estimate.Discount = reader.ToDecimalNullable(EstimateTable.DiscountColumn.FullColumnName);
            estimate.DeliveryType= reader.ToString(EstimateTable.DeliveryTypeColumn.FullColumnName);
            estimate.Age = reader.ToIntNullable(EstimateTable.AgeColumn.FullColumnName,0);
            estimate.Pincode = reader.ToString(EstimateTable.PincodeColumn.FullColumnName);
            estimate.Address = reader.ToString(EstimateTable.AddressColumn.FullColumnName);
            estimate.Gender = reader.ToString(EstimateTable.GenderColumn.FullColumnName);
            estimate.Name = reader.ToString(EstimateTable.NameColumn.FullColumnName);
            estimate.Email = reader.ToString(EstimateTable.EmailColumn.FullColumnName);
            estimate.Mobile = reader.ToString(EstimateTable.MobileColumn.FullColumnName);
            estimate.PatientId = reader.ToString(EstimateTable.PatientIdColumn.FullColumnName);
            estimate.DOB = reader.ToDateTime(EstimateTable.DOBColumn.FullColumnName, null);
            estimate.Patient = new Patient
            {
                Name = reader.ToString(EstimateTable.NameColumn.FullColumnName)
            };

            return estimate;
        }
    }
}