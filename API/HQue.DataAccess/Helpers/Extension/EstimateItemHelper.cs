using System.Data.Common;
using HQue.Contract.Infrastructure.Estimates;
using HQue.DataAccess.DbModel;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class EstimateItemHelper
    {
        public static EstimateItem Fill(this EstimateItem estimateItem, DbDataReader reader)
        {
            estimateItem.Id = reader.ToString(EstimateItemTable.IdColumn.ColumnName);
            estimateItem.ProductStockId = reader.ToString(EstimateItemTable.ProductStockIdColumn.ColumnName);
            estimateItem.Quantity = reader.ToDecimal(EstimateItemTable.QuantityColumn.ColumnName);
            estimateItem.SellingPrice = reader.ToDecimalNullable(EstimateItemTable.SellingPriceColumn.ColumnName, 0);
            estimateItem.EstimateId = reader.ToString(EstimateItemTable.EstimateIdColumn.ColumnName);
            estimateItem.Discount = reader.ToDecimalNullable(EstimateItemTable.DiscountColumn.ColumnName);
            estimateItem.Quantity = reader.ToDecimalNullable(EstimateItemTable.QuantityColumn.ColumnName, 0);
            estimateItem.ReminderFrequency = reader.ToIntNullable(EstimateItemTable.ReminderFrequencyColumn.ColumnName, 0);
            estimateItem.ReminderDate = reader.ToDateTime(EstimateItemTable.ReminderDateColumn.ColumnName, null);
            return estimateItem;
        }
    }
}