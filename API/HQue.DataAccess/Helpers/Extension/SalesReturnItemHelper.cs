using System.Data.Common;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;

namespace HQue.DataAccess.Helpers.Extension
{
    public static class SalesReturnItemHelper
    {
        public static SalesReturnItem Fill(this SalesReturnItem salesReturnItem, DbDataReader reader)
        {
            salesReturnItem.Id = reader.ToString(SalesReturnItemTable.IdColumn.ColumnName);
            salesReturnItem.AccountId = reader.ToString(SalesReturnItemTable.AccountIdColumn.ColumnName);
            salesReturnItem.InstanceId = reader.ToString(SalesReturnItemTable.InstanceIdColumn.ColumnName);
            salesReturnItem.SalesReturnId = reader.ToString(SalesReturnItemTable.SalesReturnIdColumn.ColumnName);
            salesReturnItem.ProductStockId = reader.ToString(SalesReturnItemTable.ProductStockIdColumn.ColumnName);
            salesReturnItem.Quantity = reader.ToDecimalNullable(SalesReturnItemTable.QuantityColumn.ColumnName, 0);
            salesReturnItem.CancelType = reader.ToIntNullable(SalesReturnItemTable.CancelTypeColumn.ColumnName, 0);
            salesReturnItem.Discount = reader.ToDecimalNullable(SalesReturnItemTable.DiscountColumn.ColumnName, 0);
            salesReturnItem.DiscountAmount = reader.ToDecimalNullable(SalesReturnItemTable.DiscountAmountColumn.ColumnName, 0);
            salesReturnItem.MrpSellingPrice = reader.ToDecimalNullable(SalesReturnItemTable.MrpSellingPriceColumn.ColumnName, 0);
            salesReturnItem.MRP = reader.ToDecimalNullable(SalesReturnItemTable.MRPColumn.ColumnName, 0);
            salesReturnItem.IsDeleted = reader.ToBoolNullable(SalesReturnItemTable.IsDeletedColumn.ColumnName, false);
            salesReturnItem.Igst = reader.ToDecimalNullable(SalesReturnItemTable.IgstColumn.ColumnName, 0);
            salesReturnItem.Cgst = reader.ToDecimalNullable(SalesReturnItemTable.CgstColumn.ColumnName, 0);
            salesReturnItem.Sgst = reader.ToDecimalNullable(SalesReturnItemTable.SgstColumn.ColumnName, 0);
            salesReturnItem.GstTotal = reader.ToDecimalNullable(SalesReturnItemTable.GstTotalColumn.ColumnName, 0);
            salesReturnItem.GstAmount = reader.ToDecimalNullable(SalesReturnItemTable.GstAmountColumn.ColumnName, 0);
            salesReturnItem.TotalAmount = reader.ToDecimalNullable(SalesReturnItemTable.TotalAmountColumn.ColumnName, 0);
            salesReturnItem.LoyaltyProductPts = reader.ToDecimalNullable(SalesReturnItemTable.LoyaltyProductPtsColumn.ColumnName, 0);

            return salesReturnItem;
        }
    }
}