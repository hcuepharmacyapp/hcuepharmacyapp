﻿using System.Collections.Generic;
using DataAccess.ManageData.Interface;
using HQue.DataAccess.DbModel;
using HQue.Contract.Infrastructure.Setup;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using System;
using System.Linq;
using System.Threading.Tasks;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.UserAccount;
using System.IO;
using Utilities.Helpers;
using System.Globalization;
using DataAccess.Criteria;
using HQue.Contract.Infrastructure.Master;

namespace HQue.DataAccess.Setup
{
    public class RegistrationDataAccess : BaseDataAccess
    {
        InstanceSetupDataAccess _instanceSetupDataAccess;
        AccountSetupDataAccess _accountSetupDataAccess;
        public RegistrationDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, InstanceSetupDataAccess instanceSetupDataAccess, AccountSetupDataAccess accountSetupDataAccess) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _instanceSetupDataAccess = instanceSetupDataAccess;
            _accountSetupDataAccess = accountSetupDataAccess;
        }

        //public Account SavePayment(Account data)
        //{
        //    var payments = data.Payments;
        //    var result = Save<PharmacyPayment>(payment);

        //    /*foreach(PharmacyPayment payment in payments)
        //    {
        //        payment.AccountId = data.AccountId;
        //        payment.SetLoggedUserDetails(User);
        //        var result = Save<PharmacyPayment>(payment);
        //    }*/
        //    return data;
        //}

        public async Task DeleteTimings(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PharmacyTimingTable.Table, OperationType.Delete);
            qb.ConditionBuilder.And(PharmacyTimingTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(PharmacyTimingTable.InstanceIdColumn, instanceId);

            await QueryExecuter.NonQueryAsyc(qb);
        }

        public async Task DeletePayments(string accountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PharmacyPaymentTable.Table, OperationType.Delete);
            qb.ConditionBuilder.And(PharmacyPaymentTable.AccountIdColumn, accountId);

            await QueryExecuter.NonQueryAsyc(qb);
        }

        public Task<PharmacyTiming[]> SaveTimings(IEnumerable<PharmacyTiming> timings)
        {
            var list = timings.Select(timing => { return SaveTiming(timing); });
            return Task.WhenAll(list);
        }

        private async Task<PharmacyTiming> SaveTiming(PharmacyTiming data)
        {
            return await Insert(data, PharmacyTimingTable.Table);
        }

        public Task<PharmacyPayment[]> SavePayment(List<PharmacyPayment> payments)
        {
            //IEnumerable<Task<PharmacyPayment>> list = payments.Select(payment => { return Insert(payment, PharmacyPaymentTable.Table); });
            //var list = payments.Select(payment => { return Save(payment); });
            var list = payments.Select(payment => { return Create(payment); });
            return Task.WhenAll(list);

        }

        //public Task<PharmacyPayment[]> UpdatePayment(List<PharmacyPayment> payments)
        //{
        //    //IEnumerable<Task<PharmacyPayment>> list = payments.Select(payment => { return Insert(payment, PharmacyPaymentTable.Table); });
        //    var list = payments.Select(payment => { payment.Id!="" ? Update(payment) : Save(payment);
        //});
        //    return Task.WhenAll(list);

        //}

        public async Task<int> CreateLicense(Account account)
        {
            var licenseCount = account.LicenseCount.GetValueOrDefault();
            bool isApproved = (licenseCount > 0) ? true : false;

            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(AccountTable.IdColumn, account.Id);
            qb.AddColumn(AccountTable.LicenseCountColumn, AccountTable.IsApprovedColumn, AccountTable.LicensePeriodColumn, AccountTable.LicenseEndDateColumn);
            qb.Parameters.Add(AccountTable.LicenseCountColumn.ColumnName, licenseCount);
            qb.Parameters.Add(AccountTable.IsApprovedColumn.ColumnName, isApproved);
            qb.Parameters.Add(AccountTable.LicensePeriodColumn.ColumnName, account.LicensePeriod);
            qb.Parameters.Add(AccountTable.LicenseEndDateColumn.ColumnName, account.LicenseEndDate);
            await QueryExecuter.NonQueryAsyc(qb);

            return licenseCount;
        }

        public async Task<int> AddLicense(string accountId, int addnLicense)
        {
            var qbCurrent = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qbCurrent.AddColumn(AccountTable.LicenseCountColumn);
            qbCurrent.ConditionBuilder.And(AccountTable.IdColumn, accountId);

            var currentLicenseCount = Convert.ToInt32(await SingleValueAsyc(qbCurrent));
            var licenseCount = currentLicenseCount + addnLicense;

            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(AccountTable.IdColumn, accountId);
            qb.AddColumn(AccountTable.LicenseCountColumn);
            qb.Parameters.Add(AccountTable.LicenseCountColumn.ColumnName, licenseCount);
            await QueryExecuter.NonQueryAsyc(qb);

            return licenseCount;
        }

        public async Task<List<Vendor>> List(string accountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);            
            qb.ConditionBuilder.And(VendorTable.AccountIdColumn);
            qb.AddColumn(VendorTable.NameColumn, VendorTable.EmailColumn);  
            qb.SelectBuilder.GroupBy(VendorTable.NameColumn, VendorTable.EmailColumn);
            qb.Parameters.Add(VendorTable.AccountIdColumn.ColumnName, accountId);
            return await List<Vendor>(qb);
        }

        public override Task<PharmacyPayment> Save<PharmacyPayment>(PharmacyPayment data)
        {
            if (data.Id == null)
                return Insert(data, PharmacyPaymentTable.Table);
            else
                return Update(data, PharmacyPaymentTable.Table);
        }

        public async Task<PharmacyPayment> Create(PharmacyPayment data)
        {
            return await Insert(data, PharmacyPaymentTable.Table);
        }

        public async Task<PharmacyPayment> Update(PharmacyPayment data)
        {
            return await Update(data, PharmacyPaymentTable.Table);
        }

        public async Task<PharmacyUploads> SaveUploads(PharmacyUploads data)
        {

            int FileCount = await Count(data);

            if (Convert.ToInt32(FileCount) == 0)
            {
                return await Insert(data, PharmacyUploadsTable.Table);
            }
            else
            {

                var qb = QueryBuilderFactory.GetQueryBuilder(PharmacyUploadsTable.Table, OperationType.Update);
                qb.AddColumn(PharmacyUploadsTable.FileNameColumn);
                qb.Parameters.Add(PharmacyUploadsTable.FileNameColumn.ColumnName, data.FileName);

                qb.ConditionBuilder.And(PharmacyUploadsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(PharmacyUploadsTable.InstanceIdColumn, data.InstanceId);
                qb.ConditionBuilder.And(PharmacyUploadsTable.FileNumberColumn, data.FileNumber);

                await QueryExecuter.NonQueryAsyc(qb);
                return data;

            }
        }

        public async Task<int> Count(PharmacyUploads data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PharmacyUploadsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(PharmacyUploadsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(PharmacyUploadsTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(PharmacyUploadsTable.FileNumberColumn, data.FileNumber);
            var fileCount = Convert.ToInt32(await SingleValueAsyc(qb));
            return fileCount;
        }

        public async Task<Registration> GetBranchData(string instanceId)
        {
            //var instanceList = await _instanceSetupDataAccess.List(id);
            var instance = await _instanceSetupDataAccess.GetById(instanceId);

            Registration registration = new Registration();
            
            registration.instance = instance;

            if(registration.instance.InstanceTypeId == null)
            {
                Account account = await _accountSetupDataAccess.GetGroupDetails(registration.instance.AccountId);
                registration.instance.InstanceTypeId = account.InstanceTypeId;
            }
            var qbt = QueryBuilderFactory.GetQueryBuilder(PharmacyTimingTable.Table, OperationType.Select);
            qbt.ConditionBuilder.And(PharmacyTimingTable.AccountIdColumn, registration.instance.AccountId);
            qbt.ConditionBuilder.And(PharmacyTimingTable.InstanceIdColumn, registration.instance.Id);                     

            var timingList = await List<PharmacyTiming>(qbt);
            if (timingList.Count > 0)
            {
                registration.instance.Timings = timingList.OrderBy(timing => timing.DayOfWeek).Select(timing => timing).ToList();
            }
            else
            {
                PharmacyTiming[] objTimings = new PharmacyTiming[7];
                for (int i = 0; i < 7; i++)
                {
                    PharmacyTiming obj = new PharmacyTiming { Id = "", AccountId = "", InstanceId = "", DayOfWeek = i + 1, Timing = "" };
                    objTimings[i] = obj;
                }
                registration.instance.Timings = objTimings.ToList();
            }
            registration.instance.Timings.Select(timing =>
            {
                timing.morning = new Morning { startTime = "", endTime = "" };

                timing.noon = new Noon { startTime = "", endTime = "" };
                return timing;
            }).ToList();
            //string query = $@"select b.Id, b.FileName, a.Number as FileNumber from master..spt_values a left join PharmacyUploads b on a.number=b.FileNumber 
            //       and b.AccountId='{id}' and b.InstanceId = '{registration.instance.Id}' where a.type='P' and a.number<5 order by a.number";
            string query = $@"select b.Id, b.FileName, b.FileNumber from PharmacyUploads b where b.AccountId='{registration.instance.AccountId}' and b.InstanceId = '{registration.instance.Id}' order by createdAt";
            var qbimg = QueryBuilderFactory.GetQueryBuilder(query);

            var FilesList = await List<PharmacyUploads>(qbimg);

            var filePath = Path.Combine("..//uploads//Registration");

            FilesList = FilesList.Select(img =>
            {
                if (!string.IsNullOrEmpty(img.FileName))
                    img.FileName = filePath + "//" + img.FileName;
                return img;
            }).ToList();
            if (FilesList.Count < 5)
            {
                for (int i = FilesList.Count; i < 5; i++)
                {
                    FilesList.Add(new PharmacyUploads { FileName = "" });
                }
            }
            registration.instance.Images = FilesList;
            
            return registration;
        }

        public async Task<List<PharmacyUploads>> GetBranchImages(string accountId, string instanceId)
        {
            string query = $@"select Id, FileName, FileNumber from PharmacyUploads where AccountId='{accountId}' and InstanceId = '{instanceId}' order by FileNumber";
            var qbimg = QueryBuilderFactory.GetQueryBuilder(query);

            return await List<PharmacyUploads>(qbimg);
        }

        public async Task DeleteBranchImage(string imgId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PharmacyUploadsTable.Table, OperationType.Delete);
            qb.ConditionBuilder.And(PharmacyUploadsTable.IdColumn, imgId);

            await QueryExecuter.NonQueryAsyc(qb);
        }

        //public async Task<Registration> GetRegistrationSingleData(string accountId, string instanceId)
        //{
        //    var qba = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
        //    //qba.AddColumn(AccountTable.IdColumn, AccountTable.RegisterTypeColumn, AccountTable.NameColumn, AccountTable.isChainColumn, AccountTable.InstanceCountColumn, AccountTable.LicensePeriodColumn, AccountTable.LicenseEndDateColumn, AccountTable.LicenseCountColumn, AccountTable.IsAMCColumn, AccountTable.isApprovedColumn);
        //    qba.ConditionBuilder.And(AccountTable.IdColumn, accountId);

        //    var account = (await List<Account>(qba)).First();

        //    //var qbi = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
        //    //qbi.AddColumn(InstanceTable.IdColumn, InstanceTable.DrugLicenseNoColumn, InstanceTable.DrugLicenseExpDateColumn, InstanceTable.BuildingNameColumn, InstanceTable.StreetNameColumn, InstanceTable.AreaColumn, InstanceTable.LandmarkColumn, InstanceTable.CityColumn, InstanceTable.PincodeColumn, InstanceTable.BDODesignationColumn, InstanceTable.StateColumn, InstanceTable.CountryColumn, InstanceTable.LatColumn, InstanceTable.LonColumn, InstanceTable.ContactNameColumn, InstanceTable.ContactMobileColumn, InstanceTable.ContactEmailColumn, InstanceTable.isVerifiedColumn);
        //    //qbi.ConditionBuilder.And(InstanceTable.AccountIdColumn, id);

        //    //var instance = (await List<Instance>(qbi)).First();

        //    //var instanceList = await _instanceSetupDataAccess.List(id);
        //    var instance = await _instanceSetupDataAccess.GetById(instanceId);

        //    Registration registration = new Registration();
        //    registration.account = account;
        //    //if (instanceList.Count > 0)
        //    //registration.instance = instanceList.First();
        //    registration.instance = instance;

        //    //var qbu = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
        //    //qbu.AddColumn(HQueUserTable.IdColumn, HQueUserTable.NameColumn, HQueUserTable.MobileColumn, HQueUserTable.UserIdColumn);
        //    //qbu.ConditionBuilder.And(HQueUserTable.AccountIdColumn, id);
        //    //qbu.ConditionBuilder.And(HQueUserTable.InstanceIdColumn, registration.instance.Id);

        //    var sqlUser = $@"Select Id, Name, Mobile, UserId from HQueUser where AccountId = '{accountId}' and InstanceId is null";
        //    var qbu = QueryBuilderFactory.GetQueryBuilder(sqlUser);

        //    var userList = await List<HQueUser>(qbu);
        //    if (userList.Count > 0)
        //    {
        //        registration.account.User = userList.First();
        //    }

        //    var qbt = QueryBuilderFactory.GetQueryBuilder(PharmacyTimingTable.Table, OperationType.Select);
        //    qbt.ConditionBuilder.And(PharmacyTimingTable.AccountIdColumn, accountId);
        //    qbt.ConditionBuilder.And(PharmacyTimingTable.InstanceIdColumn, registration.instance.Id);

        //    var timingList = await List<PharmacyTiming>(qbt);
        //    if (timingList.Count > 0)
        //    {
        //        registration.instance.Timings = timingList.OrderBy(timing => timing.DayOfWeek).Select(timing => timing).ToList();
        //    }
        //    else
        //    {
        //        PharmacyTiming[] objTimings = new PharmacyTiming[7];
        //        for (int i = 0; i < 7; i++)
        //        {
        //            PharmacyTiming obj = new PharmacyTiming { Id = "", AccountId = "", InstanceId = "", DayOfWeek = i + 1, Timing = "" };
        //            objTimings[i] = obj;
        //        }
        //        registration.instance.Timings = objTimings.ToList();
        //    }
        //    registration.instance.Timings.Select(timing =>
        //    {
        //        timing.morning = new Morning { startTime = "", endTime = "" };

        //        timing.noon = new Noon { startTime = "", endTime = "" };
        //        return timing;
        //    }).ToList();
        //    //string query = $@"select b.Id, b.FileName, a.Number as FileNumber from master..spt_values a left join PharmacyUploads b on a.number=b.FileNumber 
        //    //       and b.AccountId='{id}' and b.InstanceId = '{registration.instance.Id}' where a.type='P' and a.number<5 order by a.number";
        //    string query = $@"select b.Id, b.FileName, b.FileNumber from PharmacyUploads b where b.AccountId='{accountId}' and b.InstanceId = '{registration.instance.Id}'";
        //    var qbimg = QueryBuilderFactory.GetQueryBuilder(query);

        //    var FilesList = await List<PharmacyUploads>(qbimg);

        //    var filePath = Path.Combine("..//uploads//Registration");

        //    FilesList = FilesList.Select(img =>
        //    {
        //        if (!string.IsNullOrEmpty(img.FileName))
        //            img.FileName = filePath + "//" + img.FileName;
        //        return img;
        //    }).ToList();
        //    if (FilesList.Count < 5)
        //    {
        //        for (int i = FilesList.Count; i < 5; i++)
        //        {
        //            FilesList.Add(new PharmacyUploads { FileName = "" });
        //        }
        //    }
        //    registration.instance.Images = FilesList;

        //    var qbp = QueryBuilderFactory.GetQueryBuilder(PharmacyPaymentTable.Table, OperationType.Select);
        //    // qbp.AddColumn(PharmacyPaymentTable.IdColumn, PharmacyPaymentTable.ProductTypeColumn, PharmacyPaymentTable.QuantityColumn, PharmacyPaymentTable.CostColumn, PharmacyPaymentTable.DiscountColumn, PharmacyPaymentTable.DiscountTypeColumn, PharmacyPaymentTable.TaxColumn, PharmacyPaymentTable.PaidColumn, PharmacyPaymentTable.SalePersonTypeColumn, PharmacyPaymentTable.SalePersonNameColumn, PharmacyPaymentTable.SalePersonMobileColumn);
        //    qbp.ConditionBuilder.And(PharmacyPaymentTable.AccountIdColumn, accountId);
        //    qbp.SelectBuilder.SortByDesc(PharmacyPaymentTable.CreatedAtColumn);

        //    var paymentList = await List<PharmacyPayment>(qbp);
        //    if (paymentList.Count > 0)
        //    {
        //        //registration.account.Payments.Add(paymentList.First());
        //        registration.account.Payments = paymentList;
        //    }
        //    else
        //    {
        //        //registration.account.Payments.Add(new PharmacyPayment() { ProductType = 1, Quantity = 0, Cost = 0, Discount = 0, DiscountType = 1, Tax = 0, Paid = 0, SalePersonType = 1, SalePersonMobile = "", SalePersonName = "" });
        //    }

        //    return registration;
        //}

        public async Task<bool> contactMobileExists(string mobileno)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.ContactMobileColumn, mobileno);

            return (await List<Instance>(qb)).Count() > 0;
        }



        //Newly Added Gavaskar 04-10-2016 Start
        public async Task SaveOTP(BaseRegistrationOTP registrationOTP)
        {
            int CountOTP = await Count(registrationOTP);
            if (Convert.ToInt32(CountOTP) == 0)
            {
                await Insert(registrationOTP, RegistrationOTPTable.Table);
            }
            else
            {
                string OTPId = await GetOTPId(registrationOTP);
                //registrationOTP.Id = OTPId;
                //registrationOTP.CreatedAt = CustomDateTime.IST;

                var query = String.Format("Update RegistrationOTP set OTPNumber = {0}, CreatedAt = '{1}' where Id = '{2}'", registrationOTP.OTPNumber, CustomDateTime.IST.ToString("yyyy-MM-dd HH:mm:ss.FFF", CultureInfo.InvariantCulture), OTPId);
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                await QueryExecuter.NonQueryAsyc(qb);
                //await Update(registrationOTP, RegistrationOTPTable.Table);
            }
        }

        public async Task<bool> ConfirmOTP(BaseRegistrationOTP registrationOTP)
        {
            int CountOTPNumber = await GetOTPNumber(registrationOTP);
            if (Convert.ToInt32(CountOTPNumber) == 0)
            {
                return false;
                //await Insert(registrationOTP, RegistrationOTPTable.Table);
            }
            else
            {
                return true;
            }

        }

        public async Task<int> Count(BaseRegistrationOTP registrationOTP)
        {
            string query = $@"SELECT count(*)as countOTP from RegistrationOTP WHERE AccountId='{registrationOTP.AccountId}' AND UserId='{registrationOTP.UserId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            int count = 0;
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    count = Convert.ToInt32(reader["countOTP"]);
                }
            }
            return count;

        }

        public async Task AssignBDO(string instanceId, string bdoId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Update);
            qb.AddColumn(InstanceTable.BDOIdColumn);
            qb.ConditionBuilder.And(InstanceTable.IdColumn);
            qb.Parameters.Add(InstanceTable.BDOIdColumn.ColumnName, bdoId);
            qb.Parameters.Add(InstanceTable.IdColumn.ColumnName, instanceId);

            await QueryExecuter.NonQueryAsyc(qb);
        }

        //Newly Added 11/21/2016
        //public Task<List<HQueUser>> BDOList()
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
        //    qb.AddColumn(HQueUserTable.IdColumn, HQueUserTable.NameColumn);
        //    qb.JoinBuilder.Join(UserAccessTable.Table, HQueUserTable.IdColumn, UserAccessTable.UserIdColumn);
        //    qb.ConditionBuilder.And(HQueUserTable.UserTypeColumn, UserType.RegistrationUser);
        //    qb.ConditionBuilder.And(UserAccessTable.AccessRightColumn, CriteriaEquation.Equal);
        //    qb.Parameters.Add(UserAccessTable.AccessRightColumn.ColumnName, UserAccess.PharmacyEdit);
        //    qb.SelectBuilder.SortBy(HQueUserTable.NameColumn);

        //    qb.SelectBuilder.MakeDistinct = true;
        //    return List<HQueUser>(qb);
        //}

        //Newly Added Gavaskar 07/10/2016 Start

        public async Task<List<Account>> List(int registrationType, string bdoId = "")
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qb.AddColumn(AccountTable.IdColumn, AccountTable.NameColumn, AccountTable.InstanceCountColumn, InstanceTable.IdColumn, InstanceTable.NameColumn, InstanceTable.DrugLicenseNoColumn, InstanceTable.CreatedAtColumn, InstanceTable.BDOIdColumn, InstanceTable.BDONameColumn, InstanceTable.BDODesignationColumn);
            qb.JoinBuilder.Join(InstanceTable.Table, AccountTable.IdColumn, InstanceTable.AccountIdColumn);
            qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.IdColumn);

            if (bdoId != "")
            {
                //qb.ConditionBuilder.And(AccountTable.CreatedByColumn, bdoId);
                qb.ConditionBuilder.And(InstanceTable.BDOIdColumn, bdoId);
            }
            qb = SqlQueryBuilderAccount(registrationType, qb, bdoId);
            qb.SelectBuilder.SortByDesc(AccountTable.CreatedAtColumn);

            var list = new List<Account>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var data = new Account()
                    {
                        Id = reader[AccountTable.IdColumn.ColumnName].ToString(),
                        Name = reader[AccountTable.NameColumn.ColumnName].ToString(),
                        InstanceCount = reader[AccountTable.InstanceCountColumn.ColumnName] as int? ?? 0,
                        CreatedAt = Convert.ToDateTime(reader[AccountTable.CreatedAtColumn.ColumnName]),

                        User =
                       {
                            Instance={
                                Id = reader[InstanceTable.IdColumn.FullColumnName].ToString(),
                                DrugLicenseNo = reader[InstanceTable.DrugLicenseNoColumn.ColumnName].ToString(),
                                Name = reader[InstanceTable.NameColumn.ColumnName].ToString(),
                                BDOId = reader[InstanceTable.BDOIdColumn.ColumnName].ToString(),
                                BDOName = reader[InstanceTable.BDONameColumn.ColumnName].ToString(),
                                BDODesignation = reader[InstanceTable.BDODesignationColumn.ColumnName].ToString()
                            }
                       }
                    };

                    list.Add(data);
                }
            }
            return list;

            //var result = List<Account>(qb);
            ////qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            //return List<Account>(qb);
        }

        private static QueryBuilderBase SqlQueryBuilderAccount(int registrationType, QueryBuilderBase qb, string bdoId)
        {
            if (registrationType == 1 || registrationType == 2 || registrationType == 4)
            {
                qb.ConditionBuilder.And(AccountTable.RegisterTypeColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(AccountTable.RegisterTypeColumn.ColumnName, registrationType);
            }
            else
            {
                qb.ConditionBuilder.And(AccountTable.RegisterTypeColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(AccountTable.RegisterTypeColumn.ColumnName, 0);
                if (bdoId == "")
                {
                    var bdoIsNull = new CustomCriteria(InstanceTable.BDOIdColumn, CriteriaCondition.IsNull);
                    qb.ConditionBuilder.Or(bdoIsNull);
                }
            }

            return qb;
        }

        public async Task<string> GetOTPId(BaseRegistrationOTP registrationOTP)
        {
            string query = $@"SELECT Id from RegistrationOTP WHERE AccountId='{registrationOTP.AccountId}' AND UserId='{registrationOTP.UserId}' AND OTPType='{registrationOTP.OTPType}' ";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var OTPId = "";
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    OTPId = Convert.ToString(reader["Id"]);
                }
            }
            return OTPId;
        }

        public async Task<int> GetOTPNumber(BaseRegistrationOTP registrationOTP)
        {
            if (registrationOTP.OTPNumber.StartsWith("0"))
                registrationOTP.OTPNumber = registrationOTP.OTPNumber.Substring(1);

            string query = $@"SELECT count(*) as CountOTPNumber  from RegistrationOTP WHERE AccountId='{registrationOTP.AccountId}' AND UserId='{registrationOTP.UserId}' AND OTPType='{registrationOTP.OTPType}' AND OTPNumber='{registrationOTP.OTPNumber}' AND datediff(MINUTE, CreatedAt, getDate()) <= 3";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            int CountOTPNumber = 0;

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    CountOTPNumber = Convert.ToInt32(reader["CountOTPNumber"]);
                }
            }
            return CountOTPNumber;
        }

        private static QueryBuilderBase SqlQueryBuilder(Instance data, QueryBuilderBase qb)
        {
            qb.AddColumn(InstanceTable.CodeColumn, InstanceTable.IdColumn, InstanceTable.NameColumn, InstanceTable.PhoneColumn, InstanceTable.AddressColumn, InstanceTable.LandmarkColumn, InstanceTable.AreaColumn, InstanceTable.CityColumn, InstanceTable.LandmarkColumn, InstanceTable.StateColumn, InstanceTable.PincodeColumn, InstanceTable.RegistrationDateColumn);
            if (!string.IsNullOrEmpty(data.Code))
            {
                qb.ConditionBuilder.And(InstanceTable.CodeColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(InstanceTable.CodeColumn.ColumnName, data.Code);
            }
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(InstanceTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(InstanceTable.NameColumn.ColumnName, data.Name);
            }
            if (!string.IsNullOrEmpty(data.Phone))
            {
                qb.ConditionBuilder.And(InstanceTable.PhoneColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(InstanceTable.PhoneColumn.ColumnName, data.Phone);
            }
            if (!string.IsNullOrEmpty(data.AccountId))
            {
                qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, data.AccountId);
            }
            return qb;
        }
        // Added by Gavaskar 01-12-2017 Start
        public async Task<string> GetGstSelectedCustomer(string AccountId)
        {
            var query = $@"select count(*) as LocationTypeCount from Patient (nolock) where AccountId='{AccountId}' and (locationtype !=1  or locationtype is null)";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var count = await SingleValueAsyc(qb);
            return count.ToString();
        }
        public async Task<string> GetGstSelectedVendor(string AccountId)
        {
            var query = $@"select count(*) as LocationTypeCount from Vendor (nolock) where AccountId='{AccountId}' and (locationtype !=1  or locationtype is null) and (vendortype = 1  or vendortype is null)";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var count = await SingleValueAsyc(qb);
            return count.ToString();
        }
        // Added by Gavaskar 01-12-2017 End
    }
}
