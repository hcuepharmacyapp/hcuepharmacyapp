﻿using System.Collections.Generic;
using HQue.DataAccess.DbModel;
using HQue.Contract.Infrastructure.Setup;
using System;
using System.Threading.Tasks;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using HQue.Contract.Infrastructure.Settings;
using DataAccess;
using Utilities.Helpers;
using System.Linq;
using HQue.Contract.Infrastructure.Inventory;

namespace HQue.DataAccess.Setup
{
    public class AccountSetupDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;

        public AccountSetupDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }

        public override Task<Account> Save<Account>(Account data)
        {
            return Insert(data, AccountTable.Table);
        }

        public Task<List<Account>> List(Account data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortByDesc(AccountTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            return List<Account>(qb);
        }

        public async Task<Account> UpdateGroupDetails(Account data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(AccountTable.IdColumn, data.Id);
            qb.AddColumn(AccountTable.NameColumn, AccountTable.InstanceCountColumn, AccountTable.AddressColumn,
                AccountTable.PhoneColumn, AccountTable.RegistrationDateColumn, AccountTable.RegisterTypeColumn, 
                AccountTable.LicenseStartDateColumn, AccountTable.LicenseEndDateColumn, AccountTable.InstanceTypeIdColumn);
            qb.Parameters.Add(AccountTable.NameColumn.ColumnName, data.Name);
            qb.Parameters.Add(AccountTable.InstanceCountColumn.ColumnName, data.InstanceCount);
            qb.Parameters.Add(AccountTable.AddressColumn.ColumnName, data.FullAddress);
            qb.Parameters.Add(AccountTable.PhoneColumn.ColumnName, data.Phone);
            qb.Parameters.Add(AccountTable.RegistrationDateColumn.ColumnName, data.RegistrationDate);
            qb.Parameters.Add(AccountTable.RegisterTypeColumn.ColumnName, data.RegisterType);
            qb.Parameters.Add(AccountTable.LicenseStartDateColumn.ColumnName, data.LicenseStartDate);
            qb.Parameters.Add(AccountTable.LicenseEndDateColumn.ColumnName, data.LicenseEndDate);
            qb.Parameters.Add(AccountTable.InstanceTypeIdColumn.ColumnName, data.InstanceTypeId);
                        
            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }

        public async Task<Account> GetGroupDetails(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qb.AddColumn(AccountTable.NameColumn, AccountTable.InstanceCountColumn, AccountTable.AddressColumn,
                AccountTable.PhoneColumn, AccountTable.RegistrationDateColumn, AccountTable.RegisterTypeColumn,
                AccountTable.LicenseStartDateColumn, AccountTable.LicenseEndDateColumn, AccountTable.InstanceTypeIdColumn);
            qb.ConditionBuilder.And(AccountTable.IdColumn, id);

            var result = new Account();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                if (reader.Read())
                {
                    result.Id = id;
                    result.Name = reader[AccountTable.NameColumn.ColumnName].ToString();
                    result.InstanceCount = reader[AccountTable.InstanceCountColumn.ColumnName] as int? ?? 0;
                    result.Address = reader[AccountTable.AddressColumn.ColumnName].ToString();
                    result.Phone = reader[AccountTable.PhoneColumn.ColumnName].ToString();
                    result.RegistrationDate = reader[AccountTable.RegistrationDateColumn.ColumnName] as DateTime? ?? DateTime.Today;
                    result.RegisterType = reader[AccountTable.RegisterTypeColumn.ColumnName] as Int16? ?? 0;
                    result.LicenseStartDate = reader[AccountTable.LicenseStartDateColumn.ColumnName] as DateTime? ?? DateTime.Today;
                    result.LicenseEndDate = reader[AccountTable.LicenseEndDateColumn.ColumnName] as DateTime? ?? DateTime.Today;
                    result.InstanceTypeId = reader[AccountTable.InstanceTypeIdColumn.ColumnName] as Int32? ?? 0;
                }
            }
            return result;
        }

        private static QueryBuilderBase SqlQueryBuilder(Account data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(AccountTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(AccountTable.NameColumn.ColumnName, data.Name);
            }
            if (!string.IsNullOrEmpty(data.Phone))
            {
                qb.ConditionBuilder.And(AccountTable.PhoneColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(AccountTable.PhoneColumn.ColumnName, data.Phone);
            }
            return qb;
        }

        public async Task<int> Count(Account data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public async Task<IEnumerable<Account>> CheckUniqueAccount(Account data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(AccountTable.PhoneColumn);
            qb.Parameters.Add(AccountTable.PhoneColumn.ColumnName, data.Phone);
            //qb.ConditionBuilder.Or(AccountTable.NameColumn);
            //qb.Parameters.Add(AccountTable.NameColumn.ColumnName, data.Name);

            return await List<Account>(qb);
        }

        public async Task<List<CustomSettingsDetail>> GetCustomSettings(string AccountId, string InstanceId)
        {
            var parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_get_customSettings", parms);
            var result1 = result.Select(x =>
            {
                var item = new CustomSettingsDetail
                {
                    Id = x.Id as string ?? "",
                    CustomSettingsGroupId = x.GroupId,
                    CustomSettingsGroupName = x.GroupName,
                    AccountId = x.AccountId as string ?? AccountId,
                    InstanceId = x.InstanceId as string ?? InstanceId,
                    IsActive = x.IsActive as bool? ?? false,
                };
                return item;
            });
            return result1.ToList();
        }

        public async Task<List<CustomSettingsDetail>> SaveCustomSettings(List<CustomSettingsDetail> list)
        {
            var accountId = "";
            var instanceId = "";
            if (list.Count() > 0)
            {
                accountId = list[0].AccountId;
                instanceId = list[0].InstanceId;
            }
            foreach (var data in list)
            {
                data.SetLoggedUserDetails(user);
                data.AccountId = accountId;
                data.InstanceId = instanceId;
                if (string.IsNullOrEmpty(data.Id))
                {
                    await Insert2(data, CustomSettingsDetailTable.Table);
                }
                else
                {
                    await Update2(data, CustomSettingsDetailTable.Table);
                }
            }
            return list;
        }

        //Added by Sarubala on 14-12-2018
        public async Task<List<DomainValues>> GetInstanceType()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DomainValuesTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(DomainValuesTable.DomainIdColumn, 6);
            qb.AddColumn(DomainValuesTable.DomainValueColumn, DomainValuesTable.DisplayTextColumn);

            var result = await List<DomainValues>(qb);
            return result;
        }
    }
}
