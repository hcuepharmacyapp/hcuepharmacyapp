﻿using System.Collections.Generic;
using DataAccess.ManageData.Interface;
using HQue.DataAccess.DbModel;
using HQue.Contract.Infrastructure.Setup;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using System;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Criteria.Interface;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.UserAccount;
using DataAccess.Criteria;
using HQue.Contract.External;

namespace HQue.DataAccess.Setup
{
    public class InstanceSetupDataAccess : BaseDataAccess
    {
        public InstanceSetupDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {
        }

        public override async Task<Instance> Save<Instance>(Instance data1)
        {
            object data2 = data1;
            var data = (HQue.Contract.Infrastructure.Setup.Instance)data2;
            data.Code = await GetInstanceMaxCode(data.AccountId);
            var result = await Insert(data, InstanceTable.Table);
            await ConfigureDefaultSettings(result);
            return data1;
        }

        public async Task<string> GetInstanceMaxCode(string AccountId)
        {
            var query = $"SELECT ISNULL(MAX(CAST(Code AS INT)),0)+1 AS Code FROM Instance WHERE AccountId = '{AccountId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var num = (await QueryExecuter.SingleValueAsyc(qb));
            return num.ToString();
        }

        public Task<Instance> SaveToLocal(Instance data)
        {
            return InsertToLocal(data, InstanceTable.Table);
        }
        public Task<Account> SaveAccountToLocal(Account data)
        {
            return InsertAccountToLocal(data, AccountTable.Table);
        }
        /// <summary>
        //// Offline version number method added by POongodi on 05/07/2017
        /// </summary>
        /// <param name="data"></param>
        /// <param name="sVersion"></param>
        /// <param name="sUpdatedby"></param>
        /// <returns></returns>
        public async Task<bool> UpdateOfflineVersion(Instance data,string sVersion,string sUpdatedby)
        {
            string query = $@"update Instance set  OfflineVersionNo=  '{sVersion}', UpdatedBy = '{sUpdatedby}' where Id ='{data.Id}'  and AccountId ='{data.AccountId}'";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.Parameters.Add("AccountId", data.AccountId);
            qb.Parameters.Add("InstanceId", data.Id);
            bool result = await QueryExecuter.NonQueryAsyc2(qb);
            WriteToOfflineSyncQueue(new SyncObject()
            {
                Id = data.Id,
                QueryText = qb.GetQuery(),
                Parameters = qb.Parameters,
                EventLevel ="I"
            });


            return result;
        }
        private async Task<Instance> InsertToLocal(Instance data, IDbTable table)
        {
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            return await OfflineInstanceInsert(data, table);
        }
        private async Task<Account> InsertAccountToLocal(Account data, IDbTable table)
        {
            //data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            return await ExecuteInsert(data, table);
        }

        public async Task<Instance> GetById(string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.IdColumn);
            qb.Parameters.Add(InstanceTable.IdColumn.ColumnName, instanceId);
            return (await List<Instance>(qb)).First();
        }
        public async Task<List<Instance>> GetInstancesByAccountId(string accountid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, accountid);
            qb.AddColumn(InstanceTable.Table.ColumnList.ToArray());
            return (await List<Instance>(qb));

        }

        public async Task<List<Instance>> List(string accountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn);
            qb.Parameters.Add(InstanceTable.AccountIdColumn.ColumnName, accountId);
            return await List<Instance>(qb);
        }

        public async Task<SaleSettings> GetShortCutKeySetting(string AccountId, string InstanceId) // 
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.ShortCutKeysTypeColumn, SaleSettingsTable.NewWindowTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }

        public async Task<IEnumerable<Instance>> List(Instance data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortByDesc(InstanceTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            return await List<Instance>(qb);
        }

        private static QueryBuilderBase SqlQueryBuilder(Instance data, QueryBuilderBase qb)
        {
            qb.AddColumn(InstanceTable.CodeColumn, InstanceTable.IdColumn, InstanceTable.NameColumn, InstanceTable.BuildingNameColumn, InstanceTable.StreetNameColumn, InstanceTable.PhoneColumn, InstanceTable.MobileColumn, InstanceTable.LatColumn, InstanceTable.LonColumn, InstanceTable.AddressColumn, InstanceTable.LandmarkColumn, InstanceTable.AreaColumn, InstanceTable.CityColumn, InstanceTable.LandmarkColumn, InstanceTable.StateColumn, InstanceTable.PincodeColumn, InstanceTable.RegistrationDateColumn, InstanceTable.isHeadOfficeColumn, InstanceTable.GsTinNoColumn);
            if (!string.IsNullOrEmpty(data.Code))
            {
                qb.ConditionBuilder.And(InstanceTable.CodeColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(InstanceTable.CodeColumn.ColumnName, data.Code);
            }
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(InstanceTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(InstanceTable.NameColumn.ColumnName, data.Name);
            }
            //if (!string.IsNullOrEmpty(data.Phone))
            //{
            //    qb.ConditionBuilder.And(InstanceTable.PhoneColumn, CriteriaEquation.Like, CriteriaLike.Begin);
            //    qb.Parameters.Add(InstanceTable.PhoneColumn.ColumnName, data.Phone);
            //}
            if (!string.IsNullOrEmpty(data.Mobile))
            {
                qb.ConditionBuilder.And(InstanceTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(InstanceTable.MobileColumn.ColumnName, data.Mobile);
            }
            if (!string.IsNullOrEmpty(data.AccountId))
            {
                qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, data.AccountId);
            }
            return qb;
        }

        public async Task<int> Count(Instance data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public async Task<int> GetInstanceCount(string accountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, accountId);
            var createdInstance = Convert.ToInt32(await SingleValueAsyc(qb));

            qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(AccountTable.IdColumn, accountId);
            qb.AddColumn(AccountTable.InstanceCountColumn);
            qb.SelectBuilder.MakeDistinct = true;
            var allowedInstance = Convert.ToInt32(await SingleValueAsyc(qb));

            return allowedInstance - createdInstance;
        }

        public async Task<int> GetTotalInstanceCount(string accountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, accountId);
            var createdInstance = Convert.ToInt32(await SingleValueAsyc(qb));

            qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(AccountTable.IdColumn, accountId);
            qb.AddColumn(AccountTable.InstanceCountColumn);
            qb.SelectBuilder.MakeDistinct = true;
            return Convert.ToInt32(await SingleValueAsyc(qb));           
        }

        public async Task<Instance> GetByExternalPharmaId(Int64 instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.ExternalIdColumn);
            qb.Parameters.Add(InstanceTable.ExternalIdColumn.ColumnName, instanceId);
            return (await List<Instance>(qb)).First();
        }
        
        public async Task<IEnumerable<Instance>> GetByTinNo(string tinNo)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.TinNoColumn,tinNo);
            return await List<Instance>(qb);
        }

        public Task<Instance> Update(Instance data)
        {
            return Update(data, InstanceTable.Table);
        }


        public async Task<List<Instance>> GetPharmacyList()
        {
            var query = $@"SELECT Instance.Name,Instance.Id,HQueUser.mobile,Instance.Area,HQueUser.Mobile AS [HQueUser.Mobile], HQueUser.userid, HQueUser.password 
                            FROM Instance INNER JOIN HQueUser ON HQueUser.AccountId = Instance.AccountId  
                            WHERE Instance.Accountid  IN ('18204879-99ff-4efd-b076-f85b4a0da0a3','756efa5e-66e1-46ff-a3b2-3f282d746d93', '4edd19c2-3fe3-4ba2-8083-83a06ccd6c88')
                            AND HQueUser.InstanceId IS NULL ORDER BY 1 ";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            List<Instance> results = new System.Collections.Generic.List<Instance>();
            
            //var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            //qb.AddColumn(InstanceTable.NameColumn, InstanceTable.IdColumn, InstanceTable.PhoneColumn, InstanceTable.AreaColumn);

            //qb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.AccountIdColumn, InstanceTable.AccountIdColumn);
            //qb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.MobileColumn);

            //qb.ConditionBuilder.And(InstanceTable.IdColumn, "85d3fdc9-1426-4d3e-8a96-16c78088eaa8");
            //qb.ConditionBuilder.And(HQueUserTable.InstanceIdColumn, DBNull.Value);

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    Instance obj = new Instance();

                    obj.Name = reader[InstanceTable.NameColumn.ColumnName].ToString();
                    obj.Id = reader[InstanceTable.IdColumn.ColumnName].ToString();
                    obj.InstanceId = reader[InstanceTable.IdColumn.ColumnName].ToString();
                    obj.Area = reader[InstanceTable.AreaColumn.ColumnName].ToString();
                    obj.Phone = reader[HQueUserTable.MobileColumn.FullColumnName].ToString();

                    results.Add(obj);
                }
            }
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return results;
        }

        public async Task<HQueUser> GetHqueUserId(string AccountId,string LoginUserId) // 
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            qb.AddColumn(HQueUserTable.IdColumn);
            qb.ConditionBuilder.And(HQueUserTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(HQueUserTable.UserIdColumn, LoginUserId);
            var instanceIdIsNull = new CustomCriteria(HQueUserTable.InstanceIdColumn, CriteriaCondition.IsNull);
            qb.ConditionBuilder.And(instanceIdIsNull);

            var result = (await List<HQueUser>(qb)).FirstOrDefault();
            return result;
        }

        public async Task<List<UserAccess>> GetUserAccess(string AccountId, string Id) // 
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(UserAccessTable.Table, OperationType.Select);
            qb.AddColumn(UserAccessTable.AccessRightColumn);
            qb.ConditionBuilder.And(UserAccessTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(UserAccessTable.UserIdColumn, Id);
            var result = (await List<UserAccess>(qb));
            return result;
        }
        
        public async Task<string> UpdateOfflineStatus(string instance,string offlineVersionNo)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Update);
            qb.AddColumn(InstanceTable.IsOfflineInstalledColumn, InstanceTable.OfflineVersionNoColumn);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, instance);
            qb.Parameters.Add(InstanceTable.IsOfflineInstalledColumn.ColumnName, true);
            qb.Parameters.Add(InstanceTable.OfflineVersionNoColumn.ColumnName, int.Parse(offlineVersionNo));
            await QueryExecuter.NonQueryAsyc(qb);
            return instance;
        }
        public async Task<Account> AccountList(string accountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.AddColumn(AccountTable.IdColumn, AccountTable.NameColumn, AccountTable.IsBillNumberResetColumn, AccountTable.InstanceCountColumn);
            qb.ConditionBuilder.And(AccountTable.IdColumn);
            qb.Parameters.Add(AccountTable.IdColumn.ColumnName, accountId);
            return (await List<Account>(qb)).FirstOrDefault();
        }

        public async Task<Instance> ConfigureDefaultSettings(Instance data)
        {
            await ConfigureSmsSettings(data);
            return data;
        }

        public async Task<Instance> ConfigureSmsSettings(Instance data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DomainValuesTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(DomainValuesTable.DomainIdColumn, 4);
            qb.ConditionBuilder.And(DomainValuesTable.IdColumn, 12);
            qb.AddColumn(DomainValuesTable.DomainValueColumn);
            var totalSmsCount = Convert.ToInt32((await QueryExecuter.SingleValueAsyc(qb)));

            data.TotalSmsCount = totalSmsCount;
            await Update(data, InstanceTable.Table);
            var smsConfiguration = new SmsConfiguration()
            {
                AccountId = data.AccountId,
                InstanceId = data.Id,
                Date = DateTime.Today,
                SmsCount = data.TotalSmsCount,
                IsActive = true,
                UpdatedBy = data.UpdatedBy,
                CreatedBy = data.CreatedBy,
            };
            await Insert(smsConfiguration, SmsConfigurationTable.Table);
            return data;
        }
    }
}
