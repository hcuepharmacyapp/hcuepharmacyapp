﻿using System;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using HQue.DataAccess.Inventory;
using Utilities.Helpers;
using System.Collections.Generic;
using HQue.Contract.Infrastructure.Inventory;
using System.Linq;
using HQue.DataAccess.DbModel;
using HQue.Contract.External;
using Microsoft.SqlServer.Server;

namespace HQue.DataAccess.ETL
{
    public class ETLDataAccess : BaseDataAccess
    {
        private SqlDatabaseHelper sqlRepository;
        private readonly ConfigHelper _configHelper;

        public ETLDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper, SalesDataAccess salesDataAccess) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqlRepository = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);

        }

        public override Task<T> Save<T>(T data)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> ImportHistoricalPurchases(string accountid, string instanceid, string filename, int iscloud, string userId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountID", accountid);
            parms.Add("InstanceID", instanceid);
            parms.Add("FilePath", filename);
            parms.Add("IsCloud", iscloud);
            parms.Add("Createdby", userId);//Added by Poongodi on 02/08/2017
            var result = await sqlRepository.ExecuteProcedureAsync<dynamic>("sp_importpurchasehistory", parms);

            return await Task.FromResult(true);
        }

        public async Task<bool> ImportHistoricalSales(string accountid, string instanceid, string filename, int iscloud, string userId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountID", accountid);
            parms.Add("InstanceID", instanceid);
            parms.Add("FilePath", filename);
            parms.Add("IsCloud", iscloud);
            parms.Add("Createdby", userId);//Added by Poongodi on 02/08/2017
            var result = await sqlRepository.ExecuteProcedureAsync<dynamic>("sp_importsaleshistory", parms);

            return await Task.FromResult(true);
        }

        public async Task<bool> ImportVendorMaster(string accountid, string instanceid, string filename, int iscloud, string userId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountID", accountid);
            parms.Add("InstanceID", instanceid);
            parms.Add("FilePath", filename);
            parms.Add("IsCloud", iscloud);
            parms.Add("Createdby", userId); //Added by Poongodi on 02/08/2017
            var result = await sqlRepository.ExecuteProcedureAsync<dynamic>("sp_importVendorMaster", parms);

            return await Task.FromResult(true);
        }

        public async Task<bool> ImportProductMaster(string accountid, string instanceid, string filename, int iscloud)
        {
            string query = @" INSERT INTO[dbo].[ImportProcessActivity] "
                           + " ([AccountId],[InstanceId],[FileName],[IsCloud]) "
                           + " VALUES (@AccountID, @InstanceID, @FileName, @IsCloud)";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            qb.Parameters.Add("AccountID", accountid);
            qb.Parameters.Add("InstanceID", instanceid);
            qb.Parameters.Add("FileName", filename);
            qb.Parameters.Add("IsCloud", 1);

            bool result = await QueryExecuter.NonQueryAsyc2(qb);
            return result;
        }
        /// <summary>
        /// Stock Import Added by Poongodi on 02/08/2017
        /// </summary>
        /// <param name="accountid"></param>
        /// <param name="instanceid"></param>
        /// <param name="filename"></param>
        /// <param name="iscloud"></param>
        /// <returns></returns>
        public async Task<bool> ImportProductStock(string accountid, string instanceid, string filename, int iscloud, string userId, List<SqlDataRecord> stockData = null)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountID", accountid);
            parms.Add("InstanceID", instanceid);
            parms.Add("FilePath", filename);
            parms.Add("IsCloud", iscloud);
            parms.Add("Createdby", userId);
            parms.Add("StockData", stockData);

            var result = await sqlRepository.ExecuteProcedureAsync<dynamic>("usp_importproductstock", parms);

            return await Task.FromResult(true);
        }
        /// <summary>
        /// Stock Import Added by Poongodi on 21/08/2017
        /// </summary>
        /// <param name="accountid"></param>

        /// <param name="filename"></param>
        /// <param name="iscloud"></param>
        /// <returns></returns>
        public async Task<bool> SyncProductToOnline(string accountid, string filename, int iscloud, string userId)
        {
            try
            {
                Dictionary<string, object> parms = new Dictionary<string, object>();
                parms.Add("AccountID", accountid);
                parms.Add("FilePath", filename);
                parms.Add("IsCloud", iscloud);
                parms.Add("Createdby", userId);

                var result = await sqlRepository.ExecuteProcedureAsync<dynamic>("usp_syncproduct", parms);

                //return await Task.FromResult(true);
                return false;
            }
            catch (Exception ed)
            {
                return false;
            }
        }
        /// <summary>
        /// Get Missed item form Online
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<MissedDetail> GetMissedSalesItem(string accountid, string instanceid,string sType)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountID", accountid);
            parms.Add("InstanceID", instanceid);
            parms.Add("Type", sType);
            var MissedItem = new MissedDetail();
            var result = await sqlRepository.ExecuteProcedureAsync<dynamic>("usp_GetMissedList", parms);

            var MissedItemTemp = result.Select(x =>
           {
               var ItemList = new MissedItemList
               {
                   InvoiceNo = x.InvoiceNo,
                   Id = x.Id,
               };
               return ItemList;
           });

            MissedItem.ItemList = MissedItemTemp.ToList();

            return MissedItem;

        }
        public async Task<bool> WriteMissedItemToSyncQue(MissedItemList item, string sType)
        {
            if (sType == "sales")
            await    WriteMissedSalesItemToSyncQue(item, sType);
            else
                await WriteMissedPurchaseItemToSyncQue(item, sType);

            return true;
        }
        /// <summary>
        /// Missed item get form offline and forcefully sync file write into the sync folder
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// 
        public async Task<bool> WriteMissedSalesItemToSyncQue(MissedItemList item,string sType)
        {
            //foreach (var item in itemList)
            //{
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Id", item.Id);
            parms.Add("Type", sType);
            parms.Add("AccountId", user.AccountId());
            parms.Add("InstanceId", user.InstanceId());

            Sales ss = new Sales();
            var result = await sqlRepository.ExecuteProcedureAsync<SalesItem>("Usp_Get_MissedItemDetails", parms);
            if (result.Count() > 0)
            {
          
                ss.SalesItem = result.ToList();
                foreach (var saleitem in ss.SalesItem)
                {
                    WriteSyncQueueInsert(saleitem, SalesItemTable.Table);
                   
                }
            }
            //}

            return true;
        }
        /// <summary>
        /// Missed item get form offline and forcefully sync file write into the sync folder
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// 
        public async Task<bool> WriteMissedPurchaseItemToSyncQue(MissedItemList item, string sType)
        {
            //foreach (var item in itemList)
            //{
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Id", item.Id);
            parms.Add("Type", sType);
            parms.Add("AccountId", user.AccountId());
            parms.Add("InstanceId", user.InstanceId());

            VendorPurchase vp = new VendorPurchase();
            var result = await sqlRepository.ExecuteProcedureAsync<VendorPurchaseItem>("Usp_Get_MissedItemDetails", parms);
            if (result.Count() > 0)
            {

                vp.VendorPurchaseItem = result.ToList();
                foreach (var saleitem in vp.VendorPurchaseItem)
                {
                    WriteSyncQueueInsert(saleitem, VendorPurchaseItemTable.Table);
                    parms.Remove("Id");
                    parms.Remove("Type");
                    //parms.Remove("AccountId");
                    //parms.Remove("InstanceId");
                    parms.Add("Id", saleitem.Id);
                    parms.Add("Type", "Purchasestock");
                    //parms.Add("AccountId", saleitem.AccountId);
                    //parms.Add("InstanceId", saleitem.InstanceId);
                    var result1 = await sqlRepository.ExecuteProcedureAsync<ProductStock>("Usp_Get_MissedItemDetails", parms);
                    if (result1.Count() > 0)
                    {
                        Sales sale = new Sales();
                        sale.AvailableItems = result1.ToList();
                        foreach (var productstock in sale.AvailableItems)
                        {
                            WriteSyncQueueInsert(productstock, ProductStockTable.Table);
                        }
                    }
                }
            }
            //}

            return true;
        }

    }
}


