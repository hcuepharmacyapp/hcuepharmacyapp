using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Base;
using DataAccess.QueryBuilder;

namespace HQue.DataAccess.Replication
{
    public interface ISyncDataAccess<out T> where T : IContract, new()
    {
        string AccountId { get; set; }
        string InstanceId { get; set; }

        Task<IEnumerable<T>> GetList<T>() where T : IContract, new();

        //T GetData(string id);

        Task<T> GetData<T>(string id) where T : IContract, new();

        Task<T> SaveData<T>(T data) where T : IContract;

        QueryBuilderBase GetQueryBuilderForSave<T>(T data) where T : IContract;

        QueryBuilderBase GetQueryBuilderToUpdateProductStock<T>(string productStockId, int stock) where T : IContract;

        //void ExecuteQueryBuilderForSave(IEnumerable<QueryBuilderBase> query);

        Task DeleteData(string id);

        Task<T> UpdateData<T>(T data) where T : IContract;

        Task<IEnumerable<BaseInstanceClient>> GetInstanceClients(string instanceId);

        Task<BaseProductStock> UpdateProductStock(BaseProductStock productStock);
    }
}