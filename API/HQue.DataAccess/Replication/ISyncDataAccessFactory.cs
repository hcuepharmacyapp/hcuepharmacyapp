using HQue.Contract.Base;

namespace HQue.DataAccess.Replication
{
    public interface ISyncDataAccessFactory
    {
        ISyncDataAccess<BaseContract> GetSyncDataAccess(string tableName,string instance,string accountId);
        //ISyncDataAccess<BaseContract> GetOnlineDataAccess(string tableName, string accountId, string instanceId, string clientId);
        ISyncDataAccess<BaseContract> GetBaseDataAccess(string tableName);
    }
}