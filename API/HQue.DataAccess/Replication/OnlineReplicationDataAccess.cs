﻿using HQue.Contract.Infrastructure.Replication;
using System.Threading.Tasks;
using HQue.ServiceConnector.Connector;
using Utilities.Helpers;

namespace HQue.DataAccess.Replication
{
    public class OnlineReplicationDataAccess
    {
        private readonly ConfigHelper _configHelper;
        public OnlineReplicationDataAccess(ConfigHelper configHelper)
        {
            _configHelper = configHelper;
        }

        public async Task<SyncResponse> ProcessRequest(SyncRequest entity)
        {
            var sc = new RestConnector();
            var result = await sc.PostAsyc<SyncResponse>(_configHelper.InternalApi.SyncUrl, entity);
            return result;
        }
    }
}
