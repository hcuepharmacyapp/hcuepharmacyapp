﻿using HQue.Contract.Base;
using HQue.DataAccess.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using DataAccess.Criteria.Interface;
using Utilities.Helpers;

namespace HQue.DataAccess.Replication
{
    public class SyncDataAccess<T> : BaseDataAccess, ISyncDataAccess<T> where T : IContract, new()
    {
        readonly string _tableName;

        public SyncDataAccess(ISqlHelper sqlQueryExecuter, string tableName, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _tableName = tableName;
            QueryExecuter.IsItFromReplication = true;
        }

        public string AccountId { get; set; }
        public string InstanceId { get; set; }

        public async Task<IEnumerable<T>> GetList<T>() where T : IContract, new()
        {
            var table = TableFactory.GetTable(_tableName);

            var qb = QueryBuilderFactory.GetQueryBuilder(table, OperationType.Select);

            if (_tableName == InstanceTable.TableName)
            {
                qb.ConditionBuilder.And(InstanceTable.IdColumn,InstanceId);
            }
            if (_tableName == HQueUserTable.TableName)
            {
                qb.ConditionBuilder.And(HQueUserTable.AccountIdColumn, AccountId);
                var list = await List<T>(qb);
                foreach (var item in list)
                {
                    item.InstanceId = InstanceId;
                }
                return list;
            }
            if (_tableName == UserAccessTable.TableName)
            {
                qb.ConditionBuilder.And(UserAccessTable.AccountIdColumn, AccountId)
                    .Or(UserAccessTable.InstanceIdColumn, InstanceId);

                return await List<T>(qb);
            }
            if (table.ColumnList.Any(x => x.ColumnName == "InstanceId") && !string.IsNullOrEmpty(InstanceId))
            {
                qb.ConditionBuilder.And(table.ColumnList.FirstOrDefault(x => x.ColumnName == "InstanceId"));
                qb.Parameters.Add("InstanceId", InstanceId);
            }

            return await List<T>(qb);
        }

        public async Task<T> GetData<T>(string id) where T : IContract, new()
        {
            var table = TableFactory.GetTable(_tableName);
            return (await List<T>(BuildQuery(table, id))).FirstOrDefault();
        }

        public async Task<T> SaveData<T>(T data) where T : IContract
        {
            var table = TableFactory.GetTable(_tableName);
            if (!await DoesRecordExist(table, data.Id))
                await ExecuteInsert<T>(data, table);
            else
                await UpdateData<T>(data);
            return data;
        }

        public override Task<T> Save<T>(T data) { return SaveData(data); }

        private QueryBuilderBase BuildQuery(IDbTable table, string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(table, OperationType.Select);
            var column = GetIdDbColumn(table);
            qb.ConditionBuilder.And(column);
            qb.Parameters.Add(column.ColumnName, id);
            return qb;
        }

        private async Task<DateTime> GetLastSyncedTime()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceClientTable.Table, OperationType.Select);
            qb.AddColumn(GetIDbColumn(InstanceClientTable.Table, InstanceClientTable.LastSyncedAtColumn.ColumnName));
            var result = await QueryExecuter.SingleValueAsyc(qb);
            return (result == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(result);
        }

        public async Task DeleteData(string id)
        {
            var table = TableFactory.GetTable(_tableName);
            var qb = QueryBuilderFactory.GetQueryBuilder(table, OperationType.Delete);
            var idColumn = GetIdDbColumn(table);
            qb.ConditionBuilder.And(idColumn);
            qb.Parameters.Add(idColumn.ColumnName, id);
            await QueryExecuter.NonQueryAsyc(qb);
        }

        public async Task<T> UpdateData<T>(T data) where T : IContract
        {
            var table = TableFactory.GetTable(_tableName);
            return await ExecuteUpdate<T>(data, table);
        }

        public async Task<IEnumerable<BaseInstanceClient>> GetInstanceClients(string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceClientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceClientTable.InstanceIdColumn);
            qb.Parameters.Add(InstanceClientTable.InstanceIdColumn.ColumnName, instanceId);
            return await List<BaseInstanceClient>(qb);
        }

        public async Task<BaseProductStock> UpdateProductStock(BaseProductStock data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.StockColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn);

            qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, data.Stock);
            qb.Parameters.Add(ProductStockTable.IdColumn.ColumnName, data.Id);

            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }

        public QueryBuilderBase GetQueryBuilderForSave<T>(T data) where T : IContract
        {
            var table = TableFactory.GetTable(_tableName);
            var qb = CreateQueryBuilder(table, data);
            return qb;
        }

        public QueryBuilderBase GetQueryBuilderToUpdateProductStock<T>(string productStockId, int stock) where T : IContract
        {
            var query = $" update {ProductStockTable.TableName} set {ProductStockTable.StockColumn.ColumnName} = {ProductStockTable.StockColumn.ColumnName} + {stock}  where id = '{productStockId}'";
            return QueryBuilderFactory.GetQueryBuilder(query);
        }
    }
}
