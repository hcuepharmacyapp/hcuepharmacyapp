﻿using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.Infrastructure.Replication;
using HQue.DataAccess.DbModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.DataAccess.Replication
{
    public class InstanceClientDataAccess : BaseDataAccess
    {
        public InstanceClientDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            QueryExecuter.IsItFromReplication = true;
        }

        public async Task<List<InstanceClient>> GetInstanceClients()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceClientTable.Table, OperationType.Select);
            return await List<InstanceClient>(qb);
        }

        public override async Task<T> Save<T>(T data)
        {
            var table = InstanceClientTable.Table;
            if (!await DoesRecordExist(table, data.Id))
                await ExecuteInsert(data, table);
            else
                await ExecuteUpdate(data, table);
            return data;
        }
    }
}
