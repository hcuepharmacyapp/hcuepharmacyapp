﻿using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.Base;
using HQue.Contract.Infrastructure.Replication;
using HQue.DataAccess.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HQue.DataAccess.Replication
{
    public class ReplicationDataAccess : BaseDataAccess
    {
        public ReplicationDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            QueryExecuter.IsItFromReplication = true;
        }

        public async Task<bool> CheckIfTransactionIdExisting(string transactionId)
        {
            var query = $"SELECT COUNT(*) FROM {ReplicationDataTable.TableName} WHERE {ReplicationDataTable.TransactionIDColumn} = '{transactionId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return Convert.ToBoolean(await QueryExecuter.SingleValueAsyc(qb));
        }

        public async Task<int> GetReplicationTransactionId()
        {
            //var query = "SELECT ISNULL(MAX(CONVERT(INT, Version)),0) + 1 AS Version FROM " + ReplicationDataTable.Table.TableName;
            var query = "SELECT Max(IfNULL(Version, 0)) + 1 AS Version  FROM ReplicationData";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
        }

        public async Task<List<ReplicationEntity>> GetReplicationData()
        {
            var data = (await List<ReplicationEntity>(ReplicationDataTable.Table)).ToList();
            if (data != null && data.Count > 0)
                await UpdateTransactionIdForReplication(data);
            return data;
        }

        public async Task UpdateTransactionIdForReplication(List<ReplicationEntity> replicationData)
        {
            //Get transaction ID to set for the records which are having null id
            var transactionID = Guid.NewGuid().ToString();

            var dataWithNoTransactionId = replicationData.Where(r => (string.IsNullOrEmpty(r.TransactionID))).ToList();

            if (dataWithNoTransactionId != null && dataWithNoTransactionId.Count == 0)
                transactionID = replicationData[0].TransactionID;

            //Update the records which we just pulled
            foreach (var item in dataWithNoTransactionId)
            {
                item.TransactionID = transactionID;
                await Update(item, ReplicationDataTable.Table);
            }
        }

        public Task<List<ReplicationEntity>> GetReplicationData(SyncRequestEntity entity, BaseInstanceClient instanceClient)
        {
            var table = TableFactory.GetTable(entity.TableName);

            var qb = QueryBuilderFactory.GetQueryBuilder(table, OperationType.Select);
            var instanceIdColumn = GetIDbColumn(table, ReplicationDataTable.InstanceIdColumn.ColumnName);
            qb.ConditionBuilder.And(instanceIdColumn);
            qb.Parameters.Add(instanceIdColumn.ColumnName, instanceClient.InstanceId);

            var clientIdColumn = GetIDbColumn(table, ReplicationDataTable.ClientIdColumn.ColumnName);
            qb.ConditionBuilder.And(clientIdColumn, Utilities.Enum.CriteriaEquation.NotEqual);
            qb.Parameters.Add(clientIdColumn.ColumnName, instanceClient.ClientId);

            //var accountIdColumn = GetIDbColumn(table, ReplicationDataTable.AccountIdColumn.GetColumnName());
            //qb.ConditionBuilder.And(accountIdColumn);
            //qb.Parameters.Add(accountIdColumn.GetColumnName(), entity.InstanceClientData.AccountId);

            if (instanceClient.LastSyncedAt != DateTime.MinValue)
            {
                var lastSyncColumn = ReplicationDataTable.ActionTimeColumn;
                qb.ConditionBuilder.And(lastSyncColumn, Utilities.Enum.CriteriaEquation.Greater | Utilities.Enum.CriteriaEquation.Equal);
                qb.Parameters.Add(lastSyncColumn.ColumnName, instanceClient.LastSyncedAt);
            }

            return List<ReplicationEntity>(qb);
        }

        public override async Task<T> Save<T>(T data)
        {
            //Commented by Poongodi to fix deadlock issue as suggested by Martin on 11/08/2017
           /* var table = ReplicationDataTable.Table;
            if (!await DoesRecordExist(table, data.Id))
                await ExecuteInsert<T>(data, table);
            else
                await ExecuteUpdate<T>(data, table);*/
            return data;
        }
    }
}
