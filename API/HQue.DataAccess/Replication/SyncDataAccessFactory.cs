using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.Base;
using System;

namespace HQue.DataAccess.Replication
{
    public class SyncDataAccessFactory : ISyncDataAccessFactory
    {
        readonly ISqlHelper _sqlHelper;
		private readonly QueryBuilderFactory _queryBuilderFactory;

        public SyncDataAccessFactory(ISqlHelper sqlHelper,QueryBuilderFactory queryBuilderFactory)
        {
            _sqlHelper = sqlHelper;
            _queryBuilderFactory = queryBuilderFactory;
        }

        public ISyncDataAccess<BaseContract> GetSyncDataAccess(string tableName,string instanceId)
        {
            tableName = tableName.Trim();
			ISyncDataAccess<BaseContract> da;
            switch (tableName)
            {
				case "Account":
                    da =  new SyncDataAccess<BaseAccount>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "CommunicationLog":
                    da =  new SyncDataAccess<BaseCommunicationLog>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "Doctor":
                    da =  new SyncDataAccess<BaseDoctor>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "HQueUser":
                    da =  new SyncDataAccess<BaseHQueUser>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "Instance":
                    da =  new SyncDataAccess<BaseInstance>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "InstanceClient":
                    da =  new SyncDataAccess<BaseInstanceClient>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "Leads":
                    da =  new SyncDataAccess<BaseLeads>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "LeadsProduct":
                    da =  new SyncDataAccess<BaseLeadsProduct>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "Patient":
                    da =  new SyncDataAccess<BasePatient>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "Payment":
                    da =  new SyncDataAccess<BasePayment>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "PettyCash":
                    da =  new SyncDataAccess<BasePettyCash>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "Product":
                    da =  new SyncDataAccess<BaseProduct>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "ProductStock":
                    da =  new SyncDataAccess<BaseProductStock>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "ReminderRemark":
                    da =  new SyncDataAccess<BaseReminderRemark>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "ReplicationData":
                    da =  new SyncDataAccess<BaseReplicationData>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "Sales":
                    da =  new SyncDataAccess<BaseSales>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "SalesItem":
                    da =  new SyncDataAccess<BaseSalesItem>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "SalesReturn":
                    da =  new SyncDataAccess<BaseSalesReturn>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "SalesReturnItem":
                    da =  new SyncDataAccess<BaseSalesReturnItem>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "StockAdjustment":
                    da =  new SyncDataAccess<BaseStockAdjustment>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "UserAccess":
                    da =  new SyncDataAccess<BaseUserAccess>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "UserReminder":
                    da =  new SyncDataAccess<BaseUserReminder>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "Vendor":
                    da =  new SyncDataAccess<BaseVendor>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "VendorOrder":
                    da =  new SyncDataAccess<BaseVendorOrder>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "VendorOrderItem":
                    da =  new SyncDataAccess<BaseVendorOrderItem>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "VendorPurchaseItem":
                    da =  new SyncDataAccess<BaseVendorPurchaseItem>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "VendorReturn":
                    da =  new SyncDataAccess<BaseVendorReturn>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "VendorReturnItem":
                    da =  new SyncDataAccess<BaseVendorReturnItem>(_sqlHelper, tableName,_queryBuilderFactory);break;
				case "VendorPurchase":
                    da =  new SyncDataAccess<BaseVendorPurchase>(_sqlHelper, tableName,_queryBuilderFactory);break;
				default:
                    return null;
            }
			da.InstanceId = instanceId;
            return da;
        }
		        
        public ISyncDataAccess<BaseContract> GetBaseDataAccess(string tableName)
        {
            return new SyncDataAccess<BaseContract>(_sqlHelper, tableName,_queryBuilderFactory);
        }

        public ISyncDataAccess<BaseContract> GetSyncDataAccess(string tableName, string instance, string accountId)
        {
            throw new NotImplementedException();
        }
    }
}
