














using DataAccess.Criteria.Interface;
using HQue.DataAccess.DbModel;


namespace HQue.DataAccess.Replication
{
    public class TableFactory
    {
        public static IDbTable GetTable(string tableName)
        {
            switch (tableName)
            {

				case SalesOrderEstimateItemTable.TableName:
                    return SalesOrderEstimateItemTable.Table;

				case DomainMasterTable.TableName:
                    return DomainMasterTable.Table;

				case VendorPurchaseFormulaTable.TableName:
                    return VendorPurchaseFormulaTable.Table;

				case DomainValuesTable.TableName:
                    return DomainValuesTable.Table;

				case PettyCashDtlTable.TableName:
                    return PettyCashDtlTable.Table;

				case PettyCashHdrTable.TableName:
                    return PettyCashHdrTable.Table;

				case AccountTable.TableName:
                    return AccountTable.Table;

				case AlternateVendorProductTable.TableName:
                    return AlternateVendorProductTable.Table;

				case SettingsTable.TableName:
                    return SettingsTable.Table;

				case BatchListSettingsTable.TableName:
                    return BatchListSettingsTable.Table;

				case StockAdjustmentTable.TableName:
                    return StockAdjustmentTable.Table;

				case BillPrintSettingsTable.TableName:
                    return BillPrintSettingsTable.Table;

				case CardTypeSettingsTable.TableName:
                    return CardTypeSettingsTable.Table;

				case CommunicationLogTable.TableName:
                    return CommunicationLogTable.Table;

				case CustomDevReportTable.TableName:
                    return CustomDevReportTable.Table;

				case CustomReportTable.TableName:
                    return CustomReportTable.Table;

				case DCVendorPurchaseItemTable.TableName:
                    return DCVendorPurchaseItemTable.Table;

				case DefaultDoctorTable.TableName:
                    return DefaultDoctorTable.Table;

				case DeliveryBoyTable.TableName:
                    return DeliveryBoyTable.Table;

				case DiscountRulesTable.TableName:
                    return DiscountRulesTable.Table;

				case DoctorTable.TableName:
                    return DoctorTable.Table;

				case DraftVendorPurchaseTable.TableName:
                    return DraftVendorPurchaseTable.Table;

				case DraftVendorPurchaseItemTable.TableName:
                    return DraftVendorPurchaseItemTable.Table;

				case EstimateTable.TableName:
                    return EstimateTable.Table;

				case EstimateItemTable.TableName:
                    return EstimateItemTable.Table;

				case FinYearMasterTable.TableName:
                    return FinYearMasterTable.Table;

				case FinYearResetStatusTable.TableName:
                    return FinYearResetStatusTable.Table;

				case HQueUserTable.TableName:
                    return HQueUserTable.Table;

				case InstanceTable.TableName:
                    return InstanceTable.Table;

				case InstanceClientTable.TableName:
                    return InstanceClientTable.Table;

				case VendorReturnItemTable.TableName:
                    return VendorReturnItemTable.Table;

				case InventoryReOrderTable.TableName:
                    return InventoryReOrderTable.Table;

				case InventorySettingsTable.TableName:
                    return InventorySettingsTable.Table;

				case InventorySmsSettingsTable.TableName:
                    return InventorySmsSettingsTable.Table;

				case InvoiceSeriesTable.TableName:
                    return InvoiceSeriesTable.Table;

				case InvoiceSeriesItemTable.TableName:
                    return InvoiceSeriesItemTable.Table;

				case LeadsTable.TableName:
                    return LeadsTable.Table;

				case LeadsProductTable.TableName:
                    return LeadsProductTable.Table;

				case SyncSettingsTable.TableName:
                    return SyncSettingsTable.Table;

				case MissedOrderTable.TableName:
                    return MissedOrderTable.Table;

				case StockTransferTable.TableName:
                    return StockTransferTable.Table;

				case StockTransferItemTable.TableName:
                    return StockTransferItemTable.Table;

				//case NetworkTable.TableName:
    //                return NetworkTable.Table;

				case OrderSettingsTable.TableName:
                    return OrderSettingsTable.Table;

				case PatientOrderTable.TableName:
                    return PatientOrderTable.Table;

				case PatientOrderInstanceStatusTable.TableName:
                    return PatientOrderInstanceStatusTable.Table;

				case PaymentTable.TableName:
                    return PaymentTable.Table;

				case PettyCashSettingsTable.TableName:
                    return PettyCashSettingsTable.Table;

				case PharmacyPaymentTable.TableName:
                    return PharmacyPaymentTable.Table;

				case PharmacyTimingTable.TableName:
                    return PharmacyTimingTable.Table;

				case CustomerPaymentTable.TableName:
                    return CustomerPaymentTable.Table;

				case PharmacyUploadsTable.TableName:
                    return PharmacyUploadsTable.Table;

				case PhysicalStockHistoryTable.TableName:
                    return PhysicalStockHistoryTable.Table;

				case ProductTable.TableName:
                    return ProductTable.Table;

				case ProductInstanceTable.TableName:
                    return ProductInstanceTable.Table;

				case ProductMasterTable.TableName:
                    return ProductMasterTable.Table;

				case ProductSearchSettingsTable.TableName:
                    return ProductSearchSettingsTable.Table;

				case ProductStockTable.TableName:
                    return ProductStockTable.Table;

				case VendorTable.TableName:
                    return VendorTable.Table;

				case RegistrationOTPTable.TableName:
                    return RegistrationOTPTable.Table;

				case ReminderProductTable.TableName:
                    return ReminderProductTable.Table;

				case ReminderRemarkTable.TableName:
                    return ReminderRemarkTable.Table;

				case ReplicationDataTable.TableName:
                    return ReplicationDataTable.Table;

				case RequirementTable.TableName:
                    return RequirementTable.Table;

				case SalesTable.TableName:
                    return SalesTable.Table;

				case SalesBatchPopUpSettingsTable.TableName:
                    return SalesBatchPopUpSettingsTable.Table;

				case SaleSettingsTable.TableName:
                    return SaleSettingsTable.Table;

				case SalesItemAuditTable.TableName:
                    return SalesItemAuditTable.Table;

				case SalesPaymentTable.TableName:
                    return SalesPaymentTable.Table;

				case VendorReturnTable.TableName:
                    return VendorReturnTable.Table;

				case SalesReturnTable.TableName:
                    return SalesReturnTable.Table;

				case SalesTemplateItemTable.TableName:
                    return SalesTemplateItemTable.Table;

				case SalesReturnItemTable.TableName:
                    return SalesReturnItemTable.Table;

				case SalesReturnItemAuditTable.TableName:
                    return SalesReturnItemAuditTable.Table;

				case SalesTypeTable.TableName:
                    return SalesTypeTable.Table;

				case SelfConsumptionTable.TableName:
                    return SelfConsumptionTable.Table;

				case SettlementsTable.TableName:
                    return SettlementsTable.Table;

				case StateTable.TableName:
                    return StateTable.Table;

				//case StockTransferTable.TableName:
    //                return StockTransferTable.Table;

				//case StockTransferItemTable.TableName:
    //                return StockTransferItemTable.Table;

				case SalesItemTable.TableName:
                    return SalesItemTable.Table;

				case SyncDataSeedTable.TableName:
                    return SyncDataSeedTable.Table;

				case SalesTemplateTable.TableName:
                    return SalesTemplateTable.Table;

				//case SyncPreDataCorrectionTable.TableName:
    //                return SyncPreDataCorrectionTable.Table;

				case TaxSeriesItemTable.TableName:
                    return TaxSeriesItemTable.Table;

				case TemplatePurchaseChildMasterTable.TableName:
                    return TemplatePurchaseChildMasterTable.Table;

				case TemplatePurchaseChildMasterSettingsTable.TableName:
                    return TemplatePurchaseChildMasterSettingsTable.Table;

				case TemplatePurchaseMasterTable.TableName:
                    return TemplatePurchaseMasterTable.Table;

				case TempVendorPurchaseItemTable.TableName:
                    return TempVendorPurchaseItemTable.Table;

				case ToolTable.TableName:
                    return ToolTable.Table;

				case VendorPurchaseItemTable.TableName:
                    return VendorPurchaseItemTable.Table;

				case TransferSettingsTable.TableName:
                    return TransferSettingsTable.Table;

				case UserAccessTable.TableName:
                    return UserAccessTable.Table;

				case UserReminderTable.TableName:
                    return UserReminderTable.Table;

				case VendorOrderTable.TableName:
                    return VendorOrderTable.Table;

				case VendorOrderItemTable.TableName:
                    return VendorOrderItemTable.Table;

				case SalesOrderEstimateTable.TableName:
                    return SalesOrderEstimateTable.Table;

				case VoucherTable.TableName:
                    return VoucherTable.Table;

				case ImportProcessActivityTable.TableName:
                    return ImportProcessActivityTable.Table;

				case PatientTable.TableName:
                    return PatientTable.Table;

				case InventoryUpdateHistoryTable.TableName:
                    return InventoryUpdateHistoryTable.Table;

				case SyncDataTable.TableName:
                    return SyncDataTable.Table;

				case VendorPurchaseTable.TableName:
                    return VendorPurchaseTable.Table;

				case StockValueReportTable.TableName:
                    return StockValueReportTable.Table;

				case PettyCashTable.TableName:
                    return PettyCashTable.Table;

				case VendorPurchaseItemAuditTable.TableName:
                    return VendorPurchaseItemAuditTable.Table;

				case PurchaseSettingsTable.TableName:
                    return PurchaseSettingsTable.Table;

				default:
					return null;
            }
        }
    }
}


