﻿using System.Threading.Tasks;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.DataAccess.DbModel;
using System.Collections.Generic;
using System.Linq;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using DataAccess.Criteria.Interface;
using Utilities.Helpers;
using DataAccess.Criteria;
using System;
using HQue.Contract.Infrastructure.Settings;

namespace HQue.DataAccess.UserAccount
{
    public class UserManagementDataAccess : BaseDataAccess
    {
        public UserManagementDataAccess(ISqlHelper sqlQueryExecuter,QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter,queryBuilderFactory)
        {
        }

        public override Task<UserAccess> Save<UserAccess>(UserAccess data)
        {
            return Insert(data, UserAccessTable.Table);
        }

        //By San - insert same UserAccessId for Offline  11-01-2017
        public Task<UserAccess> SaveToLocal(UserAccess data)
        {
            return InsertToLocal(data, UserAccessTable.Table);
        }

        //By San - insert same UserAccessId for Offline  11-01-2017
        private async Task<UserAccess> InsertToLocal(UserAccess data, IDbTable table)
        {
            //data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            return await ExecuteInsert(data, table);
        }

        public async Task<List<HQueUser>> GetUserDetails(HQueUser data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            qb.AddColumn(HQueUserTable.IdColumn, HQueUserTable.NameColumn, HQueUserTable.StatusColumn, HQueUserTable.MobileColumn, HQueUserTable.UserIdColumn, HQueUserTable.UserTypeColumn, HQueUserTable.InstanceIdColumn);
            qb.JoinBuilder.LeftJoin(UserAccessTable.Table, UserAccessTable.UserIdColumn, HQueUserTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(UserAccessTable.Table, UserAccessTable.AccessRightColumn);

            //qb.ConditionBuilder.And(HQueUserTable.IdColumn, CriteriaEquation.NotEqual);
            //qb.Parameters.Add(HQueUserTable.IdColumn.ColumnName, data.Id);

            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(HQueUserTable.AccountIdColumn, data.AccountId);
                //qb.ConditionBuilder.And(HQueUserTable.InstanceIdColumn, data.InstanceId);

                var cc = new CustomCriteria(HQueUserTable.InstanceIdColumn, CriteriaCondition.IsNull);
                var cc1 = new CustomCriteria(cc, new CustomCriteria(new CriteriaColumn(HQueUserTable.InstanceIdColumn, $"{"\'"+ data.InstanceId + "\'" }")), CriteriaCondition.Or);

                qb.ConditionBuilder.And(cc1);
            }
            qb.ConditionBuilder.And(HQueUserTable.UserTypeColumn,6, CriteriaEquation.NotEqual);
            

            qb.SelectBuilder.SortBy(HQueUserTable.InstanceIdColumn);

            var result = await List<HQueUser>(qb);

            var userAccess = result.GroupBy(x => x.Id).Select(y =>
            {
                var hq = new HQueUser();
                hq = result.First(w => w.Id == y.Key);
                hq.RoleAccess = y.ToList().Where(z => z.UserAccess.AccessRight.HasValue).Select(z => z.UserAccess);
                return hq;
            }).ToList();


            return userAccess;
        }
       
        public Task<List<UserAccess>> GetUserAccessList(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(UserAccessTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(UserAccessTable.UserIdColumn, id);

            qb.SelectBuilder.SortBy(UserAccessTable.AccessRightColumn);
            return List<UserAccess>(qb);

        }
        public async Task<bool> Delete(UserAccess data)
        {
            return await DeleteRecords(data, UserAccessTable.Table, UserAccessTable.UserIdColumn, data.UserId);
        }

        //Added by Sarubala on 20-11-17
        public async Task<bool> GetIsUserCreateSms(string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SmsSettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SmsSettingsTable.InstanceIdColumn, InstanceId);
            bool setting1 = false;
            var result = await List<SmsSettings>(qb);
            if(result != null && result.Count > 0)
            {
                setting1 = Convert.ToBoolean(result.First().IsUserCreateSms != null ? result.First().IsUserCreateSms : false);
            }
            return setting1;
        }


        public async Task<List<HQueUser>> getSalesPerson(string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(HQueUserTable.InstanceIdColumn, instanceid);

            //qb.SelectBuilder.SortBy(HQueUserTable.);
            return await List<HQueUser>(qb);
        }

    }
}
