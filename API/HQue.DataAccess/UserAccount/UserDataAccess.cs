﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.DataAccess.DbModel;
using HQue.Contract.Infrastructure.UserAccount;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Helpers;
using Utilities.Enum;
using DataAccess.Criteria.Interface;
using DataAccess;
using System.IO;
using System.Globalization;
using System.Text;
using Utilities.Logger;

namespace HQue.DataAccess.UserAccount
{
    public class UserDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        private readonly UserManagementDataAccess _userManagementDataAccess;
        private ConfigHelper configHelper;
        private string file;
        private int templateId;

        public UserDataAccess(ISqlHelper sqlQueryExecuter, UserManagementDataAccess userManagementDataAccess,QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter,queryBuilderFactory)
        {
            _userManagementDataAccess = userManagementDataAccess;
            _configHelper = configHelper;
           sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);

        }

        public override Task<HQueUser> Save<HQueUser>(HQueUser data)
        {
            return Insert(data, HQueUserTable.Table);
        }


        //By San - insert same userid for Offline 11-01-2017
        public Task<HQueUser> SaveToLocal(HQueUser data)
        {
            return InsertToLocal(data, HQueUserTable.Table);
        }

        //By San - insert same userid for Offline 11-01-2017
        private async Task<HQueUser> InsertToLocal(HQueUser data, IDbTable table)
        {
            //data.Id = Guid.NewGuid().ToString();
            data.CreatedAt = CustomDateTime.IST;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
            return await ExecuteInsert(data, table);
        }

        public async Task<IEnumerable<HQueUser>> Save(IEnumerable<HQueUser> userList)
        {
            foreach (var user in userList)
            {
                user.PasswordResetKey = Guid.NewGuid().ToString();
                await Insert(user, HQueUserTable.Table);
            }
            return userList;
        }

        public async Task<IEnumerable<HQueUser>> List()
        {
            return await List<HQueUser>(HQueUserTable.Table);
        }

        public async Task<Tuple<bool, HQueUser>> ValidateUser(HQueUser data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            qb.AddColumn(HQueUserTable.IdColumn, HQueUserTable.InstanceIdColumn, HQueUserTable.UserIdColumn,
                HQueUserTable.UserTypeColumn, HQueUserTable.MobileColumn, HQueUserTable.NameColumn,
                HQueUserTable.AccountIdColumn);
            qb.JoinBuilder.LeftJoin(InstanceTable.Table, InstanceTable.IdColumn, HQueUserTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.IdColumn, InstanceTable.NameColumn,
                InstanceTable.PhoneColumn, InstanceTable.AddressColumn, InstanceTable.OfflineStatusColumn, InstanceTable.IsOfflineInstalledColumn, InstanceTable.GstselectColumn, InstanceTable.InstanceTypeIdColumn);

            qb.ConditionBuilder
                .And(HQueUserTable.UserIdColumn, data.UserId)
                .And(HQueUserTable.PasswordColumn, data.Password)
                .And(HQueUserTable.StatusColumn, true);

            var user = (await List<HQueUser>(qb)).FirstOrDefault();
            if (user != null)
            {
                user.RoleAccess = await _userManagementDataAccess.GetUserAccessList(user.Id);
                return Tuple.Create(true, user);
            }
            return Tuple.Create(false, new HQueUser());
        }

        public async Task<Tuple<bool, HQueUser>> ValidateEmail(HQueUser data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(HQueUserTable.UserIdColumn, data.UserId);

            var user = (await List<HQueUser>(qb)).FirstOrDefault();
            if (user != null)
            {
                user = await UpdatePasswordResetKey(user);
                return Tuple.Create(true, user);
            }

            return Tuple.Create(false, new HQueUser());
        }
        //added by nandhini on 13.11.17
        public async Task<IEnumerable<LoginHistory>> LoginHistory(string AccountId, string InstanceId, string UserId)
        {

            LoginHistory LoginHistory = new LoginHistory();

            var loginList = await GetloginList(InstanceId);

            if (loginList.Count == 0)
            {
                LoginHistory.AccountId = AccountId;
                LoginHistory.InstanceId = InstanceId;
                LoginHistory.CreatedAt = CustomDateTime.IST;
                LoginHistory.UpdatedAt = CustomDateTime.IST;
                LoginHistory.CreatedBy = InstanceId;
                LoginHistory.UpdatedBy = InstanceId;
                LoginHistory.LogInUserId = UserId;
                LoginHistory.LogOutUserId = null;
                LoginHistory.LoggedInDate = CustomDateTime.IST;
                LoginHistory.LoggedOutDate = null;
                if (AccountId == "c503ca6e-f418-4807-a847-b6886378cf0b" || AccountId == "b322f4ad-c054-441a-8a43-c950dd205e50"|| AccountId == "ae13cb7f-991b-42d6-8dec-8de153e4526a")
                {                     
                    LoginHistory.IsSentsms = true;
                LoginHistory.IsSentemail = true;
                };
                LoginHistory.LastTransctionDate = null;
                if (_configHelper.AppConfig.OfflineMode)
                    await Insert(LoginHistory, LoginHistoryTable.Table);
                else
                {
                    LoginHistory.Id = Guid.NewGuid().ToString();
                    LoginHistory.CreatedAt = CustomDateTime.IST;
                    LoginHistory.UpdatedAt = CustomDateTime.IST;
                    LoginHistory.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());

                    await OfflineInstanceInsert(LoginHistory, LoginHistoryTable.Table);
                }
                var today = CustomDateTime.IST;


                var userId = UserId;
                Dictionary<string, object> parms = new Dictionary<string, object>();
                parms.Add("LoggedInDate", today);
                parms.Add("AccountId", AccountId);
                parms.Add("InstanceId", InstanceId);
                parms.Add("LoginUserId", userId);

                var result = await sqldb.ExecuteProcedureAsync<LoginHistory>("usp_get_LoginHistory_details", parms);

               

                return result.ToList();
            }
            else
            {
                List<LoginHistory> temp = new List<LoginHistory>();
                return temp;
            }
            
            }
     
        public async Task<List<LoginHistory>> GetloginList(string InstanceId)
        {
                   
            var check = DateTime.UtcNow;
            var query = "select cast(LoggedInDate as date) LoggedInDate from LoginHistory (nolock) as l where l.InstanceId = @InstanceId and cast(l.LoggedInDate as date)= cast(@check as date)";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.Parameters.Add("check", check);
            qb.Parameters.Add(LoginHistoryTable.InstanceIdColumn.ColumnName, InstanceId);
         
            return await List<LoginHistory>(qb);
        }
        public async Task<LoginHistory> logOutHistory(string sAccountId, string sInstanceId, string sUserId,bool OfflineStatus)
        {

            var instanceList = await GetInstanceSettingsList1(sAccountId,sInstanceId, OfflineStatus);
            LoginHistory logOutHistory = new LoginHistory();
            logOutHistory.LoggedOutDate = CustomDateTime.IST;
            logOutHistory.LogOutUserId = sUserId;


            if (instanceList.Count() > 0)
            {
                bool IsExistInstanceId = instanceList.Exists(x => x.InstanceId == sInstanceId);
                if (IsExistInstanceId)
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(LoginHistoryTable.Table, OperationType.Update);
                    qb.AddColumn(LoginHistoryTable.LoggedOutDateColumn,LoginHistoryTable.LogOutUserIdColumn);
                    qb.Parameters.Add(LoginHistoryTable.LoggedOutDateColumn.ColumnName, logOutHistory.LoggedOutDate);
                    qb.Parameters.Add(LoginHistoryTable.LogOutUserIdColumn.ColumnName, sUserId);
                    qb.ConditionBuilder.And(LoginHistoryTable.InstanceIdColumn, sInstanceId);
                    qb.ConditionBuilder.And(LoginHistoryTable.AccountIdColumn, sAccountId);
                    qb.ConditionBuilder.And(LoginHistoryTable.IdColumn, instanceList[0].Id);
                    if (_configHelper.AppConfig.OfflineMode)
                        await QueryExecuter.NonQueryAsyc(qb);
                    else
                        await QueryExecuter.NonQueryAsyc2(qb);
                }

            }
           
            return logOutHistory;
        }

        public async Task<List<LoginHistory>> GetInstanceSettingsList1(string sAccountId, string sInstanceId,bool OfflineStatus)
        {
            var check = DateTime.UtcNow;
           
            var query = "select cast(LoggedInDate as date) LoggedInDate,AccountId,InstanceId,Id from LoginHistory (nolock) as l where l.AccountId = @AccountId and l.InstanceId = @InstanceId and cast(l.LoggedInDate as date)= cast(@check as date) and l.OfflineStatus=@OfflineStatus";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
           qb.Parameters.Add("check", check);
            qb.Parameters.Add(LoginHistoryTable.InstanceIdColumn.ColumnName, sInstanceId);
            qb.Parameters.Add(LoginHistoryTable.AccountIdColumn.ColumnName, sAccountId);
            qb.Parameters.Add(LoginHistoryTable.OfflineStatusColumn.ColumnName, OfflineStatus);
            return await List<LoginHistory>(qb);
        }
        //end 
        public async Task<HQueUser> UpdateResetPassword(HQueUser data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Update);

            qb.AddColumn(HQueUserTable.PasswordColumn, HQueUserTable.ResetRequestTimeColumn);
            qb.Parameters.Add(HQueUserTable.PasswordColumn.ColumnName, data.Password);
            qb.Parameters.Add(HQueUserTable.ResetRequestTimeColumn.ColumnName, DBNull.Value);

            qb.ConditionBuilder.And(HQueUserTable.PasswordResetKeyColumn);
            qb.Parameters.Add(HQueUserTable.PasswordResetKeyColumn.ColumnName, data.PasswordResetKey);
            

            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }
        public async Task<HQueUser> ChangePassword(HQueUser data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(HQueUserTable.UserIdColumn);
            qb.Parameters.Add(HQueUserTable.UserIdColumn.ColumnName, data.UserId);
            qb.Parameters.Add(HQueUserTable.PasswordColumn.ColumnName, data.Password);
            qb.AddColumn(HQueUserTable.PasswordColumn);

           await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task<HQueUser> UpdatePasswordResetKey(HQueUser data)
        {
            data.PasswordResetKey = Guid.NewGuid().ToString();

            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Update);
            qb.AddColumn(HQueUserTable.PasswordResetKeyColumn, HQueUserTable.ResetRequestTimeColumn);

            qb.Parameters.Add(HQueUserTable.PasswordResetKeyColumn.ColumnName, data.PasswordResetKey);
            qb.Parameters.Add(HQueUserTable.ResetRequestTimeColumn.ColumnName, CustomDateTime.IST);

            qb.ConditionBuilder.And(HQueUserTable.IdColumn, data.Id);

            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task<List<HQueUser>> CheckUniqueUserId(HQueUser data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(HQueUserTable.UserIdColumn, data.UserId);
            //qb.ConditionBuilder.Or(HQueUserTable.MobileColumn, data.Mobile);

            return await List<HQueUser>(qb);
        }

        public Task<HQueUser> Update(HQueUser data)
        {
            return Update(data, HQueUserTable.Table);
        }

        public async Task<bool> ValidateResetKey(string key)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(HQueUserTable.PasswordResetKeyColumn , key);
            qb.ConditionBuilder.And(HQueUserTable.ResetRequestTimeColumn, CustomDateTime.IST.AddDays(-1),
                CriteriaEquation.Greater);

            var list = (await List<HQueUser>(qb)).FirstOrDefault();

            return list != null;
        }

        public async Task<HQueUser> UpdateStatus(HQueUser data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(HQueUserTable.IdColumn, data.Id);
            qb.Parameters.Add(HQueUserTable.StatusColumn.ColumnName, data.Status);
            qb.AddColumn(HQueUserTable.StatusColumn);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task<int> getOfflineUserCount()
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Count);           
            int count = Convert.ToInt32(await SingleValueAsyc(qb));
            return count;
        }

        public async Task<HQueUser> AddSalesPerson(HQueUser data)
        {
            if (data.IsNew == true)
            {
                data.UserType = 6;
                data.UserId = data.UserId + "1";
                data.Status = data.Status;
                data.PasswordResetKey = Guid.NewGuid().ToString();
                data.ResetRequestTime = CustomDateTime.IST;
                return await Insert(data, HQueUserTable.Table);
            }
            else
            {  
                data.UserId = data.UserId + "1";
                return await Update(data, HQueUserTable.Table);
            }
                     
        }
       
        
        public async Task<bool> SalesPersonExists(HQueUser data)
        {
            var query = $"SELECT COUNT(1) FROM {HQueUserTable.TableName} WHERE {HQueUserTable.AccountIdColumn}='{data.AccountId}' and {HQueUserTable.InstanceIdColumn}='{data.InstanceId}' and {HQueUserTable.PasswordColumn}='{data.Password}' and {HQueUserTable.IdColumn}!='{data.Id}' and UserType=6";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var count = Convert.ToInt32(await SingleValueAsyc(qb));

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }          
        }
        public async Task<List<HQueUser>> SalesPersonList(HQueUser data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);            
            qb.ConditionBuilder.And(HQueUserTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(HQueUserTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(HQueUserTable.UserTypeColumn, data.UserType);            
            return await List<HQueUser>(qb);
        }

        public async Task<HQueUser> validateExistUser(HQueUser data)
        {
            data.UserType = 6;
            data.Status = 0;
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(HQueUserTable.AccountIdColumn, data.AccountId);
            if (data.InstanceId != null)
            {
                qb.ConditionBuilder.And(HQueUserTable.InstanceIdColumn, data.InstanceId);
            }
            qb.ConditionBuilder.And(HQueUserTable.PasswordColumn, data.Password);
            qb.ConditionBuilder.And(HQueUserTable.UserTypeColumn, data.UserType);
            qb.ConditionBuilder.And(HQueUserTable.StatusColumn, data.Status);
            qb.AddColumn(HQueUserTable.IdColumn);
            var list = (await List<HQueUser>(qb)).FirstOrDefault();

            //HQueUser getSaleUser = new HQueUser();
            //getSaleUser.Id = list.Id;            

            return list;
        }
    }
}
