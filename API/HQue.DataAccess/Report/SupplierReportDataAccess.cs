﻿using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Leads;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.DbModel;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.DataAccess.Helpers.Extension;
using HQue.Contract.Infrastructure.Communication;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Helpers;
using Utilities.Enum;
using DataAccess.Criteria;

using Dapper;
using DataAccess;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.Master;

namespace HQue.DataAccess.Report
{
    
    public class SupplierReportParamDt
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class SupplierReportDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
       
        public SupplierReportDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        
        private SupplierReportParamDt GetParameter(string type, DateTime? from, DateTime? to)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();

            if (string.IsNullOrEmpty(type))
            {
                filterFromDate = from.ToFormat();
                filterToDate = to.ToFormat();
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;

                    case "WEEK":
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;

                    case "MONTH":
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
            }
            return new SupplierReportParamDt()
            {
                FromDate = filterFromDate,
                ToDate = filterToDate
            };
        }

        public override Task<T> Save<T>(T data)
        {
            throw new NotImplementedException();
        }
        
        public async Task<List<Payment>> SupplierWiseBalanceList(string accountId, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSupplierWiseBalanceList", parms);
            var getSupplierList = result.Select(x =>
            {
                var supplierWiseBalace = new Payment();

                supplierWiseBalace.Vendor.Name = x.Name;
                supplierWiseBalace.Vendor.Mobile = x.Mobile;
                supplierWiseBalace.Balance= (decimal)x.TotalDue;            
                return supplierWiseBalace;
            });
            return getSupplierList.ToList();
        }

        public async Task<List<Payment>> SupplierDetailsList(Payment data)
        {
            var type = "";
            var report = GetParameter(type, data.FromDate, data.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);            
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("VendorId", data.VendorId);
            //parms.Add("Name", data.Vendor.Name);
            //parms.Add("Mobile", data.Vendor.Mobile);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSupplierWiseBalanceDetails", parms);
            var supplierDetails = result.Select(x =>
            {
                    var supplierWiseBalace = new Payment();

                supplierWiseBalace.VendorPurchase.InvoiceNo = x.InvoiceNo;
                supplierWiseBalace.VendorPurchase.InvoiceDate = x.InvoiceDate;
                supplierWiseBalace.VendorPurchase.GoodsRcvNo = x.GoodsRcvNo;
                supplierWiseBalace.VendorPurchase.BillSeries = Convert.ToString(x.BillSeries);
                supplierWiseBalace.Payabledays = x.PayableDays as int? ?? 0;
                supplierWiseBalace.CreditDate = x.CreditDate;
                supplierWiseBalace.Age = x.Age as int? ?? 0;
                supplierWiseBalace.PaymentType = x.PaymentType;
                supplierWiseBalace.Debit = (decimal)x.Debit;
                supplierWiseBalace.Credit = (decimal)x.Credit;
                supplierWiseBalace.Balance = (decimal)x.TotalDue;
                supplierWiseBalace.Vendor.Name = x.VendorName;
                return supplierWiseBalace;            
            });
            return supplierDetails.ToList();
        }

        public async Task<List<Vendor>> GetSupplierName(string supplier, string mobile, string accountid, string instanceid)
        {            
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceid);
            parms.Add("AccountId", accountid);
            
            parms.Add("Name", supplier);
            parms.Add("Mobile", mobile);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSupplier", parms);
            var vendorDetails = result.Select(x =>
            {
                var vendorList = new Vendor();

                vendorList.Name = x.Name;
                vendorList.Mobile = x.Mobile;
                vendorList.Id = x.Id;
                return vendorList;
            });
            return vendorDetails.ToList();
        }
        public async Task<List<Vendor>> GetSupplierByInstance(string accountid, string instanceid)
                {            
                    Dictionary<string, object> parms = new Dictionary<string, object>();
                    parms.Add("InstanceId", instanceid);
                    parms.Add("AccountId", accountid);            
                    var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSupplierByInstance", parms);
                    var vendorDetails = result.Select(x =>
                    {
                        var vendorList = new Vendor();

                        vendorList.Name = x.Name;
                        vendorList.Mobile = x.Mobile;
                        vendorList.Id = x.Id;
                        return vendorList;
                    });
                    return vendorDetails.ToList();
                }

        public async Task<List<VendorPurchaseReturn>> SupplierWisePurchaseReturnDetail(SupplierFilter data, bool IsInvoiceDate)
        {
            var type = "";
            var report = GetParameter(type, data.FromDate, data.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("VendorId", data.VendorId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("IsInvoiceDate", IsInvoiceDate); 

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSupplierWisePurchaseReturn", parms);
            var supplierDetails = result.Select(x =>
            {
                var supplierPurchaseReturn = new VendorPurchaseReturn();
                //added by nandhini for csv download
                supplierPurchaseReturn.ReportInvoiceDate = " " + x.InvoiceDate.ToString("dd-MM-yyyy");
                supplierPurchaseReturn.ReportReturnDate = " " + x.ReturnDate.ToString("dd-MM-yyyy");
                supplierPurchaseReturn.InvoiceNo= x.InvoiceNo;
                supplierPurchaseReturn.GoodsRcvNo = x.GoodsRcvNo;
                //end
                supplierPurchaseReturn.VendorPurchase.InvoiceNo = x.InvoiceNo;
                supplierPurchaseReturn.VendorPurchase.InvoiceDate = x.InvoiceDate;
                supplierPurchaseReturn.VendorPurchase.GoodsRcvNo = x.GoodsRcvNo;
                supplierPurchaseReturn.VendorPurchase.BillSeries = x.BillSeries;
                supplierPurchaseReturn.ReturnNo = x.ReturnNo;
                supplierPurchaseReturn.ReturnDate = x.ReturnDate;
                supplierPurchaseReturn.Reason = x.Reason;
               
                supplierPurchaseReturn.VendorpurchaseReturnReport.ProductName = x.Name;
                supplierPurchaseReturn.VendorpurchaseReturnReport.VAT = x.VAT;
                supplierPurchaseReturn.VendorpurchaseReturnReport.Stock = x.Quantity;
                supplierPurchaseReturn.VendorpurchaseReturnReport.VatAmount = x.VatAmount;
                supplierPurchaseReturn.VendorpurchaseReturnReport.CostPrice = x.PurchasePrice;
                supplierPurchaseReturn.VendorpurchaseReturnReport.Total = x.Total;
                supplierPurchaseReturn.VendorpurchaseReturnReport.GstTotal = x.GstTotal as decimal? ?? 0;  //Added by Sarubala to include GST 06-07-2017 - start
                supplierPurchaseReturn.VendorpurchaseReturnReport.Cgst = x.Cgst as decimal? ?? 0;
                supplierPurchaseReturn.VendorpurchaseReturnReport.Sgst = x.Sgst as decimal? ?? 0;
                supplierPurchaseReturn.VendorpurchaseReturnReport.Igst = x.Igst as decimal? ?? 0;

                supplierPurchaseReturn.TaxRefNo = x.TaxRefNo;
                supplierPurchaseReturn.InstanceName = Convert.ToString(x.InstanceName);

                if (supplierPurchaseReturn.TaxRefNo == 1)
                {
                    supplierPurchaseReturn.VendorpurchaseReturnReport.VAT = supplierPurchaseReturn.VendorpurchaseReturnReport.GstTotal;
                }
                //Added by Sarubala to include GST 06-07-2017 - end


                return supplierPurchaseReturn;
            });
            return supplierDetails.ToList();
        }

        public async Task<List<Payment>> SupplierPaymentDetails(Payment data)
        {
            var type = "";
            var report = GetParameter(type, data.FromDate, data.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("VendorId", data.VendorId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSupplierPayment", parms);
            var supplierDetails = result.Select(x =>
            {
                var supplierPayment = new Payment();

                supplierPayment.CreatedAt = x.CreatedAt;
                supplierPayment.Vendor.Name = x.Name;
                supplierPayment.VendorPurchase.InvoiceNo = x.InvoiceNo;
                supplierPayment.VendorPurchase.InvoiceDate = x.InvoiceDate;
                supplierPayment.VendorPurchase.GoodsRcvNo = x.GoodsRcvNo;
                supplierPayment.VendorPurchase.BillSeries = x.BillSeries;
                supplierPayment.VendorPurchase.ActualPaymentTotal = x.InvoiceAmount;
                supplierPayment.PaymentMode = x.PaymentMode;
                supplierPayment.ChequeNo = x.ChequeNo;
                supplierPayment.ChequeDate = x.ChequeDate;
                supplierPayment.Debit = x.Debit;
                supplierPayment.Credit = x.Balance;
                supplierPayment.BankName = x.BankName;
                supplierPayment.BankBranchName = x.BankBranchName;
                supplierPayment.IfscCode = x.IfscCode;

                //supplierPurchaseReturn.ReturnNo = x.ReturnNo;
                //supplierPurchaseReturn.ReturnDate = x.ReturnDate;
                //supplierPurchaseReturn.Reason = x.Reason;

                //supplierPurchaseReturn.VendorpurchaseReturnReport.ProductName = x.Name;
                //supplierPurchaseReturn.VendorpurchaseReturnReport.VAT = x.VAT;
                //supplierPurchaseReturn.VendorpurchaseReturnReport.Stock = x.Quantity;
                //supplierPurchaseReturn.VendorpurchaseReturnReport.VatAmount = x.VatAmount;
                //supplierPurchaseReturn.VendorpurchaseReturnReport.CostPrice = x.PurchasePrice;
                //supplierPurchaseReturn.VendorpurchaseReturnReport.Total = x.Total;

                return supplierPayment;
            });
            return supplierDetails.ToList();
        }
        

    public async Task<List<Payment>> SupplierLedgerDetails(Payment data)
        {
            var type = "";
            var report = GetParameter(type, data.FromDate, data.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("VendorId", data.VendorId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);            
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSupplierLedger", parms);
            var supplierLedger = result.Select(x =>
            {
                var supplierWiseLedger = new Payment();

                supplierWiseLedger.VendorPurchase.InvoiceNo = x.InvoiceNo;
                supplierWiseLedger.VendorPurchase.InvoiceDate = x.InvoiceDate;
                supplierWiseLedger.VendorPurchase.GoodsRcvNo = x.GRN;
                supplierWiseLedger.VendorPurchase.BillSeries = x.BillSeries;
                supplierWiseLedger.VendorPurchase.CreatedAt = x.GRNDate;
                supplierWiseLedger.PaymentType = x.TransactionType;
                supplierWiseLedger.Vendor.Name = x.Name;
                supplierWiseLedger.PaymentAmount = x.InvoiceAmount as decimal? ?? 0;
                supplierWiseLedger.Payabledays = x.Payabledays;
                supplierWiseLedger.PaymentMode = x.PaymentMode;
                supplierWiseLedger.ChequeNo = x.ChequeNo;
                supplierWiseLedger.ChequeDate = x.ChequeDate;
                supplierWiseLedger.Debit = x.PaidAmount as decimal? ?? 0;
                supplierWiseLedger.Credit = x.ReturnAmount as decimal? ?? 0;
                supplierWiseLedger.Balance = x.Balance as decimal? ?? 0;
                supplierWiseLedger.BankName = x.BankName as System.String ?? "";
                supplierWiseLedger.BankBranchName = x.BankBranchName as System.String ?? "";
                supplierWiseLedger.IfscCode = x.IfscCode as System.String ?? "";
                return supplierWiseLedger;
            });
            return supplierLedger.ToList();
        }

    public async Task<List<VendorPurchaseReturn>> SupplierWiseExpiryReturnDetail(SupplierFilter data)
    {
            var type = "";
            var report = GetParameter(type, data.FromDate, data.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("VendorId", data.VendorId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetExpiryReturnBySupplier", parms);
            var expiryDetails = result.Select(x =>
            {
                var supplierExpiryReturn = new VendorPurchaseReturn();

                supplierExpiryReturn.ReturnNo = x.ReturnNo;
                supplierExpiryReturn.ReturnDate = x.ReturnDate;
                supplierExpiryReturn.Reason = x.Reason;

                supplierExpiryReturn.VendorpurchaseReturnReport.ProductName = x.Name;
                supplierExpiryReturn.VendorpurchaseReturnReport.BatchNo = x.BatchNo;
                supplierExpiryReturn.VendorpurchaseReturnReport.VAT = x.VAT as decimal? ?? 0;
                supplierExpiryReturn.VendorpurchaseReturnReport.Stock = x.Quantity as decimal? ?? 0;
                supplierExpiryReturn.VendorpurchaseReturnReport.VatAmount = x.VatAmount as decimal? ?? 0;
                supplierExpiryReturn.VendorpurchaseReturnReport.CostPrice = x.PurchasePrice as decimal? ?? 0;
                supplierExpiryReturn.VendorpurchaseReturnReport.SellingPrice = x.SellingPrice as decimal? ?? 0;
                supplierExpiryReturn.VendorpurchaseReturnReport.Total = x.Total as decimal? ?? 0;

                return supplierExpiryReturn;
            });
            return expiryDetails.ToList();
    }

        public async Task<List<Payment>> supplierWisePaymentReceiptCancellationList(Payment data)
        {
            var type = "";
            var report = GetParameter(type, data.FromDate, data.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("VendorId", data.VendorId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSupplierWisePaymentReceiptCancellationList", parms);
            var supplierDetails = result.Select(x =>
            {
                var supplierWiseCancellation = new Payment();

                supplierWiseCancellation.TransactionDate = x.PaymentDate ;
                supplierWiseCancellation.VendorPurchase.GoodsRcvNo = x.GRNNo as System.String ?? "";
                supplierWiseCancellation.VendorPurchase.BillSeries = x.BillSeries as System.String ?? "";
                supplierWiseCancellation.VendorPurchase.InvoiceNo = x.InvoiceNo as System.String ?? "";
                supplierWiseCancellation.VendorPurchase.InvoiceDate = x.InvoiceDate ;
                supplierWiseCancellation.Credit = x.InvoiceAmount as decimal? ?? 0;
                supplierWiseCancellation.Debit = x.PaidAmount as decimal? ?? 0;
                supplierWiseCancellation.Balance = x.Balance as decimal? ?? 0;
                supplierWiseCancellation.PaymentMode = x.PaymentMode as System.String ?? "";
                supplierWiseCancellation.ChequeNo = x.ChequeNo as System.String ?? "";
                supplierWiseCancellation.ChequeDate = x.ChequeDate ;
                supplierWiseCancellation.PaymentType = x.PaymentType as System.String ?? "";
                supplierWiseCancellation.BankName = x.BankName as System.String ?? "";
                supplierWiseCancellation.BankBranchName = x.BankBranchName as System.String ?? "";
                supplierWiseCancellation.IfscCode = x.IfscCode as System.String ?? "";
                return supplierWiseCancellation;
            });
            return supplierDetails.ToList();
        }


    }
}
