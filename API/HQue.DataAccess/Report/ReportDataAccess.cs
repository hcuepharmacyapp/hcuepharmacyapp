﻿using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Leads;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.DbModel;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.DataAccess.Helpers.Extension;
using HQue.Contract.Infrastructure.Communication;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Helpers;
using Utilities.Enum;
using DataAccess.Criteria;
using HQue.Contract.Infrastructure.Dashboard;
using static HQue.Contract.Infrastructure.Dashboard.getAllPharmaItem;
// Done 23-12-2016
using Dapper;
using DataAccess;
using HQue.Contract.Infrastructure.Reports;
using HQue.Contract.Infrastructure.Audits;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.UserAccount;
using HQue.Contract.Infrastructure.Master;
using HQue.WebCore.ViewModel;

namespace HQue.DataAccess.Report
{
    // Done 23-12-2016
    public class ReportParam
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class ReportDataAccess : BaseDataAccess
    {
        // Done 23-12-2016
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        // private SalesReturnItem salesReturnItem;
        //public int TotalMrp { get; private set; }
        //public int TotalPurchasePrice { get; private set; }``
        //public decimal ReturnProfit { get; private set; }
        public ReportDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            // Done 23-12-2016
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        // Done 23-12-2016
        private ReportParam GetParameter(string type, DateTime from, DateTime to)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            if (string.IsNullOrEmpty(type))
            {
                filterFromDate = from.ToFormat();
                filterToDate = to.ToFormat();
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "WEEK":
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "MONTH":
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
            }
            return new ReportParam()
            {
                FromDate = filterFromDate,
                ToDate = filterToDate
            };
        }
        public override Task<T> Save<T>(T data)
        {
            throw new NotImplementedException();
        }
        public async Task<List<SalesItem>> SalesReportList1(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesItemTable.ProductStockIdColumn, SalesItemTable.QuantityColumn, SalesItemTable.SellingPriceColumn, SalesItemTable.DiscountColumn);
            qb.JoinBuilder.Join(SalesTable.Table, SalesTable.IdColumn, SalesItemTable.SalesIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.IdColumn, SalesTable.InvoiceNoColumn, SalesTable.InvoiceDateColumn, SalesTable.NameColumn, SalesTable.EmailColumn, SalesTable.MobileColumn, SalesTable.DiscountColumn, SalesTable.DoctorNameColumn, SalesTable.AddressColumn, SalesTable.PaymentTypeColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.PurchasePriceColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.ScheduleColumn, ProductTable.TypeColumn, ProductTable.ManufacturerColumn);
            // For Cost Price
            qb.JoinBuilder.LeftJoin(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.VendorPurchaseIdColumn);
            //qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorPurchaseItemTable.VendorPurchaseIdColumn);
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from.ToFormat());
                qb.Parameters.Add("FilterToDate", to.ToFormat());
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", CustomDateTime.IST.ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                }
            }
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            qb.SelectBuilder.SortByDesc(SalesTable.InvoiceDateColumn);
            var list = new List<SalesItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesItem
                    {
                        ProductStockId = reader[SalesItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = (decimal)reader[SalesItemTable.QuantityColumn.ColumnName],
                        SellingPrice = reader[SalesItemTable.SellingPriceColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[SalesItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        Sales =
                        {
                            Id = reader[SalesTable.IdColumn.FullColumnName].ToString(),
                            InvoiceNo = reader[SalesTable.InvoiceNoColumn.FullColumnName].ToString(),
                            InvoiceDate = Convert.ToDateTime(reader[SalesTable.InvoiceDateColumn.FullColumnName]),
                            Name = reader[SalesTable.NameColumn.FullColumnName].ToString(),
                            DoctorName = reader[SalesTable.DoctorNameColumn.FullColumnName].ToString(),
                            Discount = reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
                            Address = reader[SalesTable.AddressColumn.FullColumnName].ToString(),
                            PaymentType = reader[SalesTable.PaymentTypeColumn.FullColumnName].ToString()
                        }
                    };
                    if (reader[SalesTable.EmailColumn.FullColumnName].ToString() != "N/A")
                    {
                        salesItem.Sales.Email = reader[SalesTable.EmailColumn.FullColumnName].ToString();
                    }
                    salesItem.Sales.Mobile = reader[SalesTable.MobileColumn.FullColumnName].ToString();
                    salesItem.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;
                    salesItem.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    salesItem.ProductStock.PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0;
                    if (salesItem.ProductStock.PurchasePrice == 0)
                    {
                        salesItem.ProductStock.PurchasePrice = reader[ProductStockTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0;
                    }
                    if (salesItem.SellingPrice > 0)
                    {
                        salesItem.ProductStock.SellingPrice = salesItem.SellingPrice;
                        salesItem.ReportTotal = (salesItem.SellingPrice * salesItem.Quantity) -
                                            ((salesItem.SellingPrice * salesItem.Quantity * (salesItem.Sales.Discount / 100))
                                            + (salesItem.SellingPrice * salesItem.Quantity * salesItem.Discount / 100));
                    }
                    else
                    {
                        salesItem.ProductStock.SellingPrice = (decimal)reader[ProductStockTable.SellingPriceColumn.FullColumnName];
                        salesItem.ReportTotal = (salesItem.ProductStock.SellingPrice * salesItem.Quantity) -
                                            ((salesItem.ProductStock.SellingPrice * salesItem.Quantity * (salesItem.Sales.Discount / 100))
                                            + (salesItem.ProductStock.SellingPrice * salesItem.Quantity * salesItem.Discount / 100));
                    }
                    //salesItem.ProductStock.TotalCostPrice = salesItem.ProductStock.PurchasePrice * salesItem.Quantity;
                    salesItem.ProductStock.TotalCostPrice = decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.PurchasePrice * salesItem.Quantity));
                    if (salesItem.ProductStock.TotalCostPrice > 0)
                        salesItem.SalesProfit = ((salesItem.ReportTotal - salesItem.ProductStock.TotalCostPrice) / salesItem.ProductStock.TotalCostPrice) * 100;
                    else
                        salesItem.SalesProfit = 0;
                    //salesItem.ReportTotal = Math.Round((decimal)salesItem.ReportTotal);
                    salesItem.ReportTotal = (decimal)salesItem.ReportTotal;
                    salesItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    salesItem.ProductStock.ExpireDate = (DateTime)reader[ProductStockTable.ExpireDateColumn.FullColumnName];
                    //Product details
                    salesItem.ProductStock.Product.Manufacturer = reader[ProductTable.ManufacturerColumn.FullColumnName].ToString();
                    salesItem.ProductStock.Product.Schedule = reader[ProductTable.ScheduleColumn.FullColumnName].ToString();
                    salesItem.ProductStock.Product.Type = reader[ProductTable.TypeColumn.FullColumnName].ToString();
                    list.Add(salesItem);
                }
            }
            return list;
        }

        //Created by Manivannan on 18-Jul-2017
        public async Task<List<Sales>> SalesReportDetailMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, Sales data)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceIds", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("Level", level);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            //parms.Add("PageNo", Pageno);
            //parms.Add("PageSize", data.Page.PageSize);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesDetailMultiLevelReport", parms);
            var sales = result.Select(x =>
            {
                var sale = new HQue.Contract.Infrastructure.Inventory.Sales
                {

                    InvoiceNo = x.InvoiceNo,
                    InvoiceSeries = x.InvoiceSeries,
                    ActualInvoice = x.InvoiceSeries + " " + x.InvoiceNo,
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    Name = x.PatientName,
                    //InvoiceAmount = Math.Round(x.InvoiceAmount as decimal? ?? 0, MidpointRounding.AwayFromZero),
                    InvoiceAmount = x.InvoiceAmount as decimal? ?? 0,
                    InvoiceAmountNoTaxNoDiscount = x.InvoiceAmountNoTaxNoDiscount as decimal? ?? 0,
                    //Discount = x.Discount,
                    DiscountValue = x.DiscountValue as decimal? ?? 0,
                    TaxAmount = x.TaxAmount as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    MRP = x.MRPAmount as decimal? ?? 0,
                    RoundoffNetAmount = x.RoundOffNetAmount as decimal? ?? 0,

                    SalesCount = x.SalesCount as int? ?? 0,

                    Instance =
                                    {
                                        Id  = x.InstanceId,
                                        Name  = x.InstanceName
                                    }

                };

                if (level == 3)
                {
                    sale.IsReturn = Convert.ToBoolean(x.IsReturn);
                }
                else
                {
                    sale.IsReturn = false;
                }

                //if (!sale.IsReturn)
                //{
                //sale.InvoiceAmount = Math.Round(Convert.ToDecimal(sale.InvoiceAmount), MidpointRounding.AwayFromZero);
                //sale.RoundoffNetAmount = sale.InvoiceAmount - (sale.InvoiceAmountNoTaxNoDiscount + sale.TaxAmount);
                //}
                //else
                //{
                //    sale.RoundoffNetAmount = 0;
                //}

                sale.ProfitOnCost = 0;
                sale.ProfitOnMrp = 0;

                if (sale.PurchasePrice != 0)
                {
                    sale.ProfitOnCost = (sale.InvoiceAmount - sale.PurchasePrice) / sale.PurchasePrice * 100;
                }

                if (sale.InvoiceAmount != 0)
                {
                    sale.ProfitOnMrp = (sale.InvoiceAmount - sale.PurchasePrice) / sale.InvoiceAmount * 100;
                }

                return sale;
            });

            var returnList = sales.ToList();

            if (data.Select == "profitOnCost")
            {
                decimal profitOnCostFilter;  //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnCostFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnCost.HasValue ? x.ProfitOnCost.Value : 0, 2) == profitOnCostFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnCost > profitOnCostFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnCost < profitOnCostFilter).ToList();
                            break;
                    }

                }
            }
            else if (data.Select == "profitOnMrp")
            {
                decimal profitOnMrpFilter;  //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnMrpFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnMrp.HasValue ? x.ProfitOnMrp.Value : 0, 2) == profitOnMrpFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnMrp > profitOnMrpFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnMrp < profitOnMrpFilter).ToList();
                            break;
                    }

                }
            }

            return returnList;
        }

        //Created by Bikas on 18-06-18
        public async Task<List<Sales>> BranchWiseSalesReportDetailMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, Sales data)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceIds", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("Level", level);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            //parms.Add("PageNo", Pageno);
            //parms.Add("PageSize", data.Page.PageSize);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_BranchWiseSalesDetailMultiLevelReport", parms);
            var sales = result.Select(x =>
            {
                var sale = new HQue.Contract.Infrastructure.Inventory.Sales
                {

                    InvoiceNo = x.InvoiceNo,
                    InvoiceSeries = x.InvoiceSeries,
                    ActualInvoice = x.InvoiceSeries + " " + x.InvoiceNo,
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    Name = x.PatientName,
                    //InvoiceAmount = Math.Round(x.InvoiceAmount as decimal? ?? 0, MidpointRounding.AwayFromZero),
                    InvoiceAmount = x.InvoiceAmount as decimal? ?? 0,
                    InvoiceAmountNoTaxNoDiscount = x.InvoiceAmountNoTaxNoDiscount as decimal? ?? 0,
                    //Discount = x.Discount,
                    DiscountValue = x.DiscountValue as decimal? ?? 0,
                    TaxAmount = x.TaxAmount as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    MRP = x.MRPAmount as decimal? ?? 0,
                    RoundoffNetAmount = x.RoundOffNetAmount as decimal? ?? 0,

                    SalesCount = x.SalesCount as int? ?? 0,

                    Instance =
                                    {
                                        Id  = x.InstanceId,
                                        Name  = x.InstanceName
                                    }

                };

                if (level == 3)
                {
                    sale.IsReturn = Convert.ToBoolean(x.IsReturn);
                }
                else
                {
                    sale.IsReturn = false;
                }

                //if (!sale.IsReturn)
                //{
                //sale.InvoiceAmount = Math.Round(Convert.ToDecimal(sale.InvoiceAmount), MidpointRounding.AwayFromZero);
                //sale.RoundoffNetAmount = sale.InvoiceAmount - (sale.InvoiceAmountNoTaxNoDiscount + sale.TaxAmount);
                //}
                //else
                //{
                //    sale.RoundoffNetAmount = 0;
                //}

                sale.ProfitOnCost = 0;
                sale.ProfitOnMrp = 0;

                if (sale.PurchasePrice != 0)
                {
                    sale.ProfitOnCost = (sale.InvoiceAmount - sale.PurchasePrice) / sale.PurchasePrice * 100;
                }

                if (sale.InvoiceAmount != 0)
                {
                    sale.ProfitOnMrp = (sale.InvoiceAmount - sale.PurchasePrice) / sale.InvoiceAmount * 100;
                }

                return sale;
            });

            var returnList = sales.ToList();

            if (data.Select == "profitOnCost")
            {
                decimal profitOnCostFilter;  //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnCostFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnCost.HasValue ? x.ProfitOnCost.Value : 0, 2) == profitOnCostFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnCost > profitOnCostFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnCost < profitOnCostFilter).ToList();
                            break;
                    }

                }
            }
            else if (data.Select == "profitOnMrp")
            {
                decimal profitOnMrpFilter;  //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnMrpFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnMrp.HasValue ? x.ProfitOnMrp.Value : 0, 2) == profitOnMrpFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnMrp > profitOnMrpFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnMrp < profitOnMrpFilter).ToList();
                            break;
                    }

                }
            }

            return returnList;
        }
        //Created by Manivannan on 18-Jul-2017
        public async Task<List<Sales>> SalesReportTaxGroupMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, Sales data, double gstTotal)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceIds", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("Level", level);

            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("GstTotal", gstTotal);

            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            //parms.Add("PageNo", Pageno);
            //parms.Add("PageSize", data.Page.PageSize);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesTaxGroupMultiLevelReport", parms);
            var sales = result.Select(x =>
            {
                var sale = new HQue.Contract.Infrastructure.Inventory.Sales
                {
                    GstTotal = x.GstTotal as decimal? ?? 0,
                    InvoiceNo = x.InvoiceNo as string ?? "",
                    InvoiceSeries = x.InvoiceSeries as string ?? "",
                    ActualInvoice = (x.InvoiceSeries as string ?? "") + " " + (x.InvoiceNo as string ?? ""),
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate as DateTime? ?? DateTime.UtcNow),
                    Name = x.PatientName as string ?? "",

                    //InvoiceAmount = Math.Round(x.InvoiceAmount as decimal? ?? 0, MidpointRounding.AwayFromZero),
                    InvoiceAmount = x.InvoiceAmount as decimal? ?? 0,
                    InvoiceAmountNoTaxNoDiscount = x.InvoiceAmountNoTaxNoDiscount as decimal? ?? 0,
                    //Discount = x.Discount,
                    DiscountValue = x.DiscountValue as decimal? ?? 0,
                    TaxAmount = x.TaxAmount as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    MRP = x.MRPAmount as decimal? ?? 0,

                    SalesCount = x.SalesCount as int? ?? 0,

                    Instance =
                                    {
                                        Id  = x.InstanceId as string ?? "",
                                        Name  = x.InstanceName as string ?? ""
                                    }

                };

                if (level == 3)
                {
                    sale.IsReturn = Convert.ToBoolean(x.IsReturn as bool? ?? false);
                }
                else
                {
                    sale.IsReturn = false;
                }

                //if (!sale.IsReturn)
                //{
                //sale.InvoiceAmount = Math.Round(Convert.ToDecimal(sale.InvoiceAmount), MidpointRounding.AwayFromZero);
                //sale.RoundoffNetAmount = sale.InvoiceAmount - (sale.InvoiceAmountNoTaxNoDiscount + sale.TaxAmount);
                sale.RoundoffNetAmount = 0;
                //}
                //else
                //{
                //    sale.RoundoffNetAmount = 0;
                //}

                sale.ProfitOnCost = 0;
                sale.ProfitOnMrp = 0;

                if (sale.PurchasePrice != 0)
                {
                    sale.ProfitOnCost = (sale.InvoiceAmount - sale.PurchasePrice) / sale.PurchasePrice * 100;
                }

                if (sale.InvoiceAmount != 0)
                {
                    sale.ProfitOnMrp = (sale.InvoiceAmount - sale.PurchasePrice) / sale.InvoiceAmount * 100;
                }

                return sale;
            });

            var returnList = sales.ToList();

            if (data.Select == "profitOnCost")
            {
                decimal profitOnCostFilter; //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnCostFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnCost.HasValue ? x.ProfitOnCost.Value : 0, 2) == profitOnCostFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnCost > profitOnCostFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnCost < profitOnCostFilter).ToList();
                            break;
                    }

                }
            }
            else if (data.Select == "profitOnMrp")
            {
                decimal profitOnMrpFilter; //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnMrpFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnMrp.HasValue ? x.ProfitOnMrp.Value : 0, 2) == profitOnMrpFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnMrp > profitOnMrpFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnMrp < profitOnMrpFilter).ToList();
                            break;
                    }

                }
            }

            return returnList;
        }

        //Created by Sarubala on 26-Jul-2017
        public async Task<Tuple<List<Sales>, List<Sales>>> ItemWiseSalesReportMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, Sales data, string productId)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceIds", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("Level", level);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            parms.Add("ProdId", productId);
            //parms.Add("PageNo", Pageno);
            //parms.Add("PageSize", data.Page.PageSize);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetItemWiseSaleMultiLevelReport", parms);
            var sales = result.Select(x =>
            {
                var sale = new HQue.Contract.Infrastructure.Inventory.Sales
                {

                    InvoiceNo = Convert.ToString(x.InvoiceNo as string ?? ""),
                    InvoiceSeries = Convert.ToString(x.InvoiceSeries as string ?? ""),
                    ActualInvoice = (x.InvoiceSeries as string ?? "") + " " + (x.InvoiceNo as string ?? ""),
                    InvoiceDate = (level == 3 ? Convert.ToDateTime(x.InvoiceDate as DateTime? ?? DateTime.UtcNow) : DateTime.UtcNow),
                    Name = Convert.ToString(x.PatientName as string ?? ""),

                    SalesProductId = Convert.ToString(x.ProductId as string ?? ""),
                    SalesProductName = Convert.ToString(x.Name as string ?? ""),
                    SalesQty = x.Quantity as decimal? ?? 0,
                    InvoiceAmount = x.TotalNetAmount as decimal? ?? 0,
                    GrossSaleWithoutTax = x.GrossSaleWithoutTax as decimal? ?? 0,
                    InvoiceAmountNoTaxNoDiscount = x.InvoiceAmountNoTaxNoDiscount as decimal? ?? 0,
                    Discount = x.SalesDiscount as decimal? ?? 0,
                    DiscountValue = x.DiscountAmount as decimal? ?? 0,
                    TaxAmount = x.TaxAmount as decimal? ?? 0,
                    PurchasePrice = x.TotalPurchasePrice as decimal? ?? 0,
                    MRP = x.MRP as decimal? ?? 0,
                    IsReturn = Convert.ToByte(x.ReturnFlag as int? ?? 0) == 1 ? true : false,

                    Instance =
                                    {
                                        Id  = Convert.ToString(x.InstanceId as string ?? ""),
                                        Name  = Convert.ToString(x.InstanceName as string ?? "")
                                    }

                };

                sale.ProfitOnCost = 0;
                sale.ProfitOnMrp = 0;

                if (sale.PurchasePrice != 0)
                {
                    sale.ProfitOnCost = (sale.InvoiceAmount - sale.PurchasePrice) / sale.PurchasePrice * 100;
                }

                if (sale.InvoiceAmount != 0)
                {
                    sale.ProfitOnMrp = (sale.InvoiceAmount - sale.PurchasePrice) / sale.InvoiceAmount * 100;
                }

                return sale;
            });

            var returnList = sales.ToList();

            if (data.Select == "profitOnCost")
            {
                decimal profitOnCostFilter; //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnCostFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnCost.HasValue ? x.ProfitOnCost.Value : 0, 2) == profitOnCostFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnCost > profitOnCostFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnCost < profitOnCostFilter).ToList();
                            break;
                    }

                }
            }
            else if (data.Select == "profitOnMrp")
            {
                decimal profitOnMrpFilter; //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnMrpFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnMrp.HasValue ? x.ProfitOnMrp.Value : 0, 2) == profitOnMrpFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnMrp > profitOnMrpFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnMrp < profitOnMrpFilter).ToList();
                            break;
                    }

                }
            }

            var TempSale = returnList.Where(x => x.IsReturn == false).ToList();
            var TempSale1 = returnList.Where(x => x.IsReturn == true).ToList();

            Tuple<List<Sales>, List<Sales>> tuple = new Tuple<List<Sales>, List<Sales>>(TempSale, TempSale1);

            return tuple;
        }

        // Done Gavaskar 23-12-2016
        public async Task<List<SalesItem>> SalesReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, string kind)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("Kind", kind);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesReportList", parms);
            /*Instance Name added by Poongodi on 14/09/2017*/
            var salesItems = result.Select(x =>
            {
                var salesItem = new HQue.Contract.Infrastructure.Inventory.SalesItem
                {
                    Sales =
                             {
                                 InvoiceNo = x.InvoiceNo,
                                 InvoiceSeries = x.InvoiceSeries,
                                 ActualInvoice = x.InvoiceSeries +" "+ x.InvoiceNo,
                                 InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                                 Name = x.Name,
                                 DoctorName = x.DoctorName,
                                 Address = x.Address,
                                 Instance =
                                {
                            Name =Convert.ToString( x.Branch)
                        },
                             },
                    ProductStock =
                             {
                                  BatchNo = x.BatchNo,
                                  ExpireDate = Convert.ToDateTime(x.ExpireDate),
                                  Stock= x.Stock as decimal? ?? 0,
                                  VAT=x.VAT as decimal? ?? 0,
                                  PurchasePrice=x.PurchasePrice as decimal? ?? 0,
                                  //CostPrice=x.CostPrice as decimal? ?? 0,
                                  CreatedAt = x.InwardDate,


                    Product =
                             {
                                  Name=x.ProductName,
                                  Manufacturer=x.Manufacturer,
                                  Schedule=x.Schedule,
                                  Type=x.Type,
                                  HsnCode=Convert.ToString(x.HsnCode),
                                  KindName=x.KindName,
                             }
                    }
                };
                salesItem.SellingPrice = x.SellingPrice as decimal? ?? 0;
                salesItem.Quantity = x.Quantity as decimal? ?? 0;
                salesItem.Discount = x.Discount as decimal? ?? 0;
                //salesItem.Sales.Discount = (decimal)x.SalesDiscount;
                salesItem.DiscountAmount = x.DiscountSum as decimal? ?? 0; //Added by Poongodi on 07/07/2017
                //decimal? discountSum = x.DiscountSum as decimal? ?? 0;
                //salesItem.Sales.InvoiceAmount = x.InvoiceAmount as decimal? ?? 0;
                //salesItem.ProductStock.TotalCostPrice = salesItem.SellingPrice * salesItem.Quantity;
                //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
                //salesItem.ReportTotal = salesItem.Sales.FinalValue = Math.Round((decimal)(salesItem.Sales.FinalValue), 0, MidpointRounding.AwayFromZero);
                //salesItem.ReportTotal = salesItem.Sales.FinalValue = (decimal)(salesItem.Sales.FinalValue);
                salesItem.TotalAmount = x.TotalAmount as decimal? ?? 0;
                salesItem.GstAmount = x.TaxAmount as decimal? ?? 0;
                salesItem.ItemAmount = x.AmountWOTTax as decimal? ?? 0;
                //salesItem.ProductStock.TotalCostPrice = salesItem.ProductStock.PurchasePrice * salesItem.Quantity;
                //salesItem.ProductStock.TotalCostPrice = decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.PurchasePrice * salesItem.Quantity));
                salesItem.ProductStock.TotalCostPrice = x.TotalCostPrice as decimal? ?? 0;
                //if (salesItem.ProductStock.TotalCostPrice > 0)
                //{
                //    salesItem.SalesProfit = ((decimal.Parse(String.Format("{0:0.##}", salesItem.ReportTotal)) - decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) / decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) * 100;
                //}
                //else
                //    salesItem.SalesProfit = 0;
                salesItem.SalesProfit = x.SalesProfit as decimal? ?? 0;
                return salesItem;
            });
            return salesItems.ToList();
        }
        // Prefix Added in Sales report by Violet on 22/06/2017
        public async Task<List<SalesItem>> SalesReportCustomerDetailList(string name, string Mobile, string accountId, string instanceId, DateTime from, DateTime to, string ProductId, string UserName, string UserId)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            filterFromDate = from.ToFormat();
            filterToDate = to.ToFormat();
            /*GST calculation added by Poongodi on 06/07/2017*/

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", filterFromDate);
            parms.Add("EndDate", filterToDate);
            parms.Add("PatientName", name);
            parms.Add("PatientMobile", Mobile);
            parms.Add("ProductId", ProductId);
            parms.Add("UserName", UserName);
            parms.Add("UserId", UserId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesDetails", parms);
            var list = result.Select(x =>
            {
                var salesItem = new SalesItem
                {
                    Sales =
                        {
                            InvoiceNo = x.InvoiceNo as string ?? "",
                            InvoiceSeries = x.InvoiceSeries as string ?? "",
                            ActualInvoice = (x.InvoiceSeries  as string ?? "")+" "+ (x.InvoiceNo as string ?? ""),
                            InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                            Name = x.Name as string ?? "",
                            DoctorName = x.DoctorName as string ?? "",
                            Address = x.Address as string ?? "",
                            Instance = { Name = x.InstanceName as string ?? "", }
                        },
                    ProductStock =
                        {
                             BatchNo = x.BatchNo as string ?? "",
                             ExpireDate = Convert.ToDateTime(x.ExpireDate),
                             Stock = x.Stock as decimal? ?? 0,
                             VAT = x.VAT as decimal? ?? 0,
                             PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                             //CostPrice = x.CostPrice as decimal? ?? 0,
                            Product =
                            {
                                Name = x.ProductName as string ?? "",
                                Manufacturer = x.Manufacturer as string ?? "",
                                Schedule = x.Schedule as string ?? "",
                                Type = x.Type as string ?? "",
                            }
                        }
                };
                salesItem.SellingPrice = x.SellingPrice as decimal? ?? 0;
                salesItem.Quantity = x.quantity as decimal? ?? 0;
                //salesItem.Discount = x.Discount as decimal? ?? 0;
                //salesItem.Sales.Discount = x.salesDiscount as decimal? ?? 0;
                //decimal? discountSum = x.DiscountSum as decimal? ?? 0;
                salesItem.DiscountAmount = x.DiscountAmount as decimal? ?? 0;
                //salesItem.Sales.InvoiceAmount = x.InvoiceAmount as decimal? ?? 0;
                //salesItem.ProductStock.TotalCostPrice = salesItem.SellingPrice * salesItem.Quantity;
                //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
                //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - discountSum;
                //salesItem.ReportTotal = salesItem.Sales.FinalValue = Math.Round((decimal)(salesItem.Sales.FinalValue), 0, MidpointRounding.AwayFromZero);
                salesItem.ReportTotal = x.TotalAmount as decimal? ?? 0;
                //salesItem.ProductStock.TotalCostPrice = salesItem.ProductStock.PurchasePrice * salesItem.Quantity;
                //salesItem.ProductStock.TotalCostPrice = decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.PurchasePrice * salesItem.Quantity));
                salesItem.ProductStock.TotalCostPrice = x.TotalCostPrice as decimal? ?? 0;
                //if (salesItem.ProductStock.TotalCostPrice > 0)
                //{
                //    // string Tval=String.Format("{0:0.##}", salesItem.ReportTotal);
                //    //string TCost = String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice);
                //    salesItem.SalesProfit = ((decimal.Parse(String.Format("{0:0.##}", salesItem.ReportTotal)) - decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) / decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) * 100;
                //}
                ////salesItem.SalesProfit = ((salesItem.ReportTotal - salesItem.ProductStock.TotalCostPrice) / salesItem.ProductStock.TotalCostPrice) * 100;
                //else
                //    salesItem.SalesProfit = 0;
                salesItem.SalesProfit = x.SalesProfit as decimal? ?? 0;
                salesItem.GstAmount = x.TaxAmount as decimal? ?? 0;
                salesItem.ItemAmount = x.AmountWOTTax as decimal? ?? 0;
                salesItem.Sales.HQueUser.Name = x.UserName as string ?? "";
                salesItem.Sales.Credit = x.Credit;
                return salesItem;
            }).ToList();
            return list;
        }
        // Prefix Added in Sales report by Violet on 22/06/2017
        public async Task<List<SalesItem>> SalesProductwiseListData(string id, string Name, string Mobile, string accountId, string instanceId, DateTime from, DateTime to)
        {
            return await SalesReportCustomerDetailList(Name, Mobile, accountId, instanceId, from, to, id, null, null);

            //        var filterFromDate = CustomDateTime.IST.ToFormat();
            //        var filterToDate = CustomDateTime.IST.ToFormat();
            //        filterFromDate = from.ToFormat();
            //        filterToDate = to.ToFormat();
            //        var filter = "";
            //        if (Name != null && Mobile != null)
            //        {
            //            filter = "and sales.Name ='" + Name + "' and sales.Mobile='" + Mobile + "'";
            //        }
            //        /*GST calculation added by Poongodi on 06/07/2017*/
            //        string query =
            //            $@"select sales.invoicedate AS InvoiceDate,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries,'') +' '+ isnull(sales.InvoiceNo,'')) as InvoiceNo, isNull(sales.Discount, 0) as  salesDiscount, isNull(sales.DiscountValue, 0) as  salesDiscountValue,
            //                sales.Name,sales.DoctorName,sales.Address,salesitem.quantity,convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END)) As SellingPrice,salesitem.Discount,ps.BatchNo,ps.ExpireDate,case isnull(sales.TaxRefNo,0) when 
            //1 then isnull(salesitem.GstTotal, isnull(ps.GstTotal,0)) else isnull(ps.VAT,0) end as VAT,ps.Stock,ps.PurchasePrice,p.Name as ProductName, p.Manufacturer, p.Schedule,p.Type,
            //                convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) *salesitem.quantity) As InvoiceAmount
            //                , isnull(sales.Discount,0) as Discount,ISNULL(ISNULL(vpi.PurchasePrice,ps.PurchasePrice) * (ps.Stock),0)  As CostPrice
            //                ,(salesitem.Quantity * (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) * isnull(salesitem.Discount,0) / 100) as DiscountSum, Inst.Name AS InstanceName
            //                from sales sales 
            //                join salesitem salesitem on sales.id= salesitem.salesid
            //                join productstock ps on ps.id=salesitem.productstockid
            //                inner join Product p on p.Id=ps.ProductId
            //	left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem group by productstockId) vpi on ps.id=vpi.productstockid 	
            //                join Instance Inst on Inst.Id = case when '{instanceId}'='' then sales.InstanceId else '{instanceId}' end
            //                WHERE sales.InstanceId = case when '{instanceId}'='' then sales.InstanceId else '{instanceId}' end AND sales.AccountId = '{accountId}' and sales.Cancelstatus is NULL {filter} AND p.Id='{id}'
            //	AND Convert(date,sales.invoicedate) BETWEEN '{filterFromDate}' AND '{filterToDate}'
            //                ORDER BY sales.CreatedAt desc";
            //        var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //        var list = new List<SalesItem>();
            //        using (var reader = await QueryExecuter.QueryAsyc(qb))
            //        {
            //            while (reader.Read())
            //            {
            //                var salesItem = new SalesItem
            //                {
            //                    Sales =
            //                    {
            //                        InvoiceNo = reader["InvoiceNo"].ToString(),
            //                        InvoiceDate = Convert.ToDateTime(reader["InvoiceDate"]),
            //                        Name = reader["Name"].ToString(),
            //                        DoctorName = reader["DoctorName"].ToString(),
            //                        Address = reader["Address"].ToString(),
            //                    },
            //                    ProductStock =
            //                    {
            //                         BatchNo = reader["BatchNo"].ToString(),
            //                         ExpireDate = Convert.ToDateTime(reader["ExpireDate"]),
            //                         Stock=reader["Stock"] as decimal? ?? 0,
            //                         VAT=reader["VAT"] as decimal? ?? 0,
            //                         PurchasePrice=reader["PurchasePrice"] as decimal? ?? 0,
            //                         CostPrice=reader["CostPrice"] as decimal? ?? 0,
            //                        Product=
            //                        {
            //                            Name=reader["ProductName"].ToString(),
            //                            Manufacturer=reader["Manufacturer"].ToString(),
            //                            Schedule=reader["Schedule"].ToString(),
            //                            Type=reader["Type"].ToString(),
            //                        },
            //                        InstanceName = reader["InstanceName"].ToString(),
            //                    }
            //                };
            //                salesItem.SellingPrice = reader["SellingPrice"] as decimal? ?? 0;
            //                salesItem.Quantity = reader["quantity"] as decimal? ?? 0;
            //                salesItem.Discount = reader["Discount"] as decimal? ?? 0;
            //                salesItem.Sales.Discount = (decimal)reader["salesDiscount"];
            //                salesItem.Sales.DiscountValue = (decimal)reader["salesDiscountValue"];
            //                //var discountValuePercent = salesItem.Sales.DiscountValue / salesItem.Sales.InvoiceAmount * 100;
            //                decimal? discountSum = reader["DiscountSum"] as decimal? ?? 0;
            //                salesItem.Sales.InvoiceAmount = reader["InvoiceAmount"] as decimal? ?? 0;
            //                salesItem.ProductStock.TotalCostPrice = salesItem.SellingPrice * salesItem.Quantity;
            //                //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
            //                salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
            //                //salesItem.Sales.FinalValue = Math.Round((decimal)salesItem.Sales.FinalValue);
            //                //salesItem.ReportTotal = (decimal)salesItem.Sales.FinalValue;
            //                salesItem.ReportTotal = salesItem.Sales.FinalValue = Math.Round((decimal)(salesItem.Sales.FinalValue), 0, MidpointRounding.AwayFromZero);
            //                //salesItem.ProductStock.TotalCostPrice = salesItem.ProductStock.PurchasePrice * salesItem.Quantity;
            //                salesItem.ProductStock.TotalCostPrice = decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.PurchasePrice * salesItem.Quantity));
            //                if (salesItem.ProductStock.TotalCostPrice > 0)
            //                {
            //                    // string Tval=String.Format("{0:0.##}", salesItem.ReportTotal);
            //                    //string TCost = String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice);
            //                    salesItem.SalesProfit = ((decimal.Parse(String.Format("{0:0.##}", salesItem.ReportTotal)) - decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) / decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) * 100;
            //                }
            //                //salesItem.SalesProfit = ((salesItem.ReportTotal - salesItem.ProductStock.TotalCostPrice) / salesItem.ProductStock.TotalCostPrice) * 100;
            //                else
            //                    salesItem.SalesProfit = 0;
            //                list.Add(salesItem);
            //            }
            //        }
            //        return list;
        }

        public async Task<List<Sales>> PatientListData(string accountId, string instanceId, DateTime from, DateTime to)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("From", from);
            parms.Add("To", to);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_NonSaleCustomer", parms);
            var saleslist = result.Select(x =>
            {
                var sales = new Sales
                {
                    Patient = {
                    Name = x.Name,
                    ShortName = x.ShortName,
                    Area = x.Area,
                    City = x.City,
                    Address = x.Address,
                    Email = x.Email,
                    Mobile=x.Mobile
                    },
                    InvoiceNo = x.InvoiceNo,
                    //NetAmount = Math.Round(decimal.Parse(String.Format("{0:0.##}", x.InvoiceAmount - x.Discount - x.DiscountSum)), MidpointRounding.AwayFromZero),
                    NetAmount = x.SaleAmount as decimal? ?? 0,
                    InvoiceDate = x.CreatedAt,
                    Instance =
                    {
                        Name = Convert.ToString(x.InstanceName)
                    }
                };
                return sales;
            });
            return saleslist.ToList();
        }

        // Done Gavaskar 24-12-2016
        public async Task<List<Sales>> SalesSummaryList(string type, object accountId, string instanceId, DateTime from, DateTime to, string InvoiceSeries, int fromInvoice, int toInvoice, int paymentType)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            if (report.FromDate == "01-Jan-0001")
            {
                report.FromDate = null;
                report.ToDate = null;
            }
            else
            {
                report.FromDate = report.FromDate + " 00:00:00";
                report.ToDate = report.ToDate + " 23:59:59";
            }
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("InvoiceSeries", InvoiceSeries);
            parms.Add("FromInvoice", fromInvoice);
            parms.Add("ToInvoice", toInvoice);
            parms.Add("paymentMode", paymentType);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesSummary", parms); //usp_GetSalesSummary_Test
            var salesItems = result.Select(x =>
            {
                var Sales = new Sales
                {
                    InvoiceDate = Convert.ToDateTime(x.invoicedate),
                    ReportInvoiceDate = " " + x.invoicedate.ToString("dd-MM-yyyy"),
                    //Credit = Math.Round((decimal)(x.credit), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                    //cash = Math.Round((decimal)(x.cash), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                    //card = Math.Round((decimal)(x.card), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                    //cheque = Math.Round((decimal)(x.cheque), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                    //ewallet = Math.Round((decimal)(x.ewallet), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                    //InvoiceAmount = Math.Round((decimal)(x.sellingprice), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                    Credit = x.credit as decimal? ?? 0,
                    cash = x.cash as decimal? ?? 0,
                    card = x.card as decimal? ?? 0,
                    cheque = x.cheque as decimal? ?? 0,
                    ewallet = x.ewallet as decimal? ?? 0,
                    InvoiceAmount = x.sellingprice as decimal? ?? 0,
                    FinalValue = x.purchaseprice as decimal? ?? 0,
                    ReturnValue = x.returnedPrice as decimal? ?? 0,
                    TotalSales = x.salescount as int? ?? 0,
                    ReturnedPurchasePrice = x.ReturnPurchasePrice as decimal? ?? 0,
                    Instance =
                    {
                        Name = Convert.ToString(x.InstanceName)
                    },
                };
                /*Return Value added in Sales Profit Calculation by Poongodi on 10/07/2017*/
                //Sales.Profit = Math.Round((decimal)(Sales.InvoiceAmount - Sales.FinalValue - (Sales.ReturnValue - Sales.ReturnedPurchasePrice)), 2, MidpointRounding.AwayFromZero);
                Sales.Profit = x.ProfitAmount as decimal? ?? 0;
                //Sales.Amount = 0;
                //Sales.NetValue = Math.Round((decimal)(Sales.InvoiceAmount - Sales.ReturnValue), 2, MidpointRounding.AwayFromZero) as decimal? ?? 0;
                Sales.NetValue = x.NetValue as decimal? ?? 0;
                //if (Sales.Profit > 0) //Profit percentage Added by Poongodi on 26/04/2017
                //{
                //    Sales.Amount = Math.Round(((decimal)Sales.Profit / (decimal)(Sales.InvoiceAmount - Sales.ReturnValue)) * 100, 2, MidpointRounding.AwayFromZero);
                //}
                Sales.Amount = x.ProfitPerc as decimal? ?? 0;
                return Sales;
            });
            return salesItems.ToList();
        }
        public async Task<List<SalesItem>> SalesSummaryCustomerwiseList(string PatientName, string PatientMobile, string accountId, string instanceId, DateTime from, DateTime to, string PatientId, string UserName, string UserId, string type)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            filterFromDate = from.ToFormat();
            filterToDate = to.ToFormat();

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", filterFromDate);
            parms.Add("EndDate", filterToDate);
            parms.Add("PatientName", PatientName);
            parms.Add("PatientMobile", PatientMobile);
            parms.Add("PatientId", PatientId);
            parms.Add("UserName", UserName);
            parms.Add("UserId", UserId);
            parms.Add("Type", type);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesSummaryWise", parms);
            var list = result.Select(x =>
            {
                var salesItem = new SalesItem
                {
                    Sales =
                        {
                            InvoiceDate = Convert.ToDateTime(x.SalesDate),
                            Instance = { Name = x.InstanceName as string ?? "" }
                        }
                };
                salesItem.Sales.Name = x.PatientName as string ?? "";
                salesItem.Sales.HQueUser.Name = x.UserName as string ?? "";
                salesItem.Sales.TotalSales = x.TotalSales as int? ?? 0;
                salesItem.Sales.InvoiceAmount = x.TotalPurchase as decimal? ?? 0;
                salesItem.Sales.FinalValue = x.TotalMRP as decimal? ?? 0;
                //decimal? totalDiscountForItems = x.TotalDiscount as decimal? ?? 0;
                //salesItem.Sales.FinalValue = Math.Round((decimal)(salesItem.Sales.FinalValue - totalDiscountForItems), 0, MidpointRounding.AwayFromZero);
                //salesItem.Sales.Profit = salesItem.Sales.FinalValue - salesItem.Sales.InvoiceAmount;
                //salesItem.Sales.Profit = Math.Round((decimal)salesItem.Sales.Profit, MidpointRounding.AwayFromZero);
                salesItem.Sales.Profit = x.Profit as decimal? ?? 0;
                return salesItem;
            }).ToList();
            return list;
        }
        public async Task<List<SalesReturnItem>> SalesSummaryReturnList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            if (string.IsNullOrEmpty(type))
            {
                filterFromDate = from.ToFormat();
                filterToDate = to.ToFormat();
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "WEEK":
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "MONTH":
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
            }
            string query =
                $@"select
                convert(date, SalesReturn.ReturnDate) AS SalesReturnDate,
                count(distinct SalesReturn.id) AS TotalSalesReturn,
                convert(decimal(18,2),
                sum((productstock.PurchasePrice - DBO.GetInclusiveVatAmount(isnull(productstock.PurchasePrice,0),ProductStock.VAT)) * SalesReturnItem.Quantity)) AS TotalPurchasePrice,
                convert(decimal(18,2),SUM( 
                (CASE when isnull(SalesReturnItem.MrpSellingPrice,0)=0 then ProductStock.SellingPrice * SalesReturnItem.Quantity else (SalesReturnItem.MrpSellingPrice * SalesReturnItem.Quantity ) end)
                ))as TotalMrp
                from SalesReturnItem
                INNER JOIN SalesReturn ON  SalesReturnItem.SalesReturnId = SalesReturn.Id
                INNER JOIN Sales ON Sales.Id = SalesReturn.SalesId
                INNER JOIN salesitem on sales.id = salesitem.salesid
                INNER JOIN ProductStock ON ProductStock.Id = SalesReturnItem.ProductStockId
                INNER JOIN Product ON Product.Id = ProductStock.ProductId where
                Salesreturn.AccountId = '{accountId}' and SalesReturnItem.InstanceId = '{instanceId}' and 
                Sales.Cancelstatus is NULL
                convert(date, SalesReturn.ReturnDate) 
                BETWEEN  '{filterFromDate}' AND '{filterToDate}' group by SalesReturn.ReturnDate Order by SalesReturn.ReturnDate desc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<SalesReturnItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesReturnItem = new SalesReturnItem
                    {
                        SalesReturn =
                                    {
                                        ReturnDate = reader["SalesReturnDate"] as DateTime? ?? DateTime.MinValue
                                    },
                        TotalSalesReturn = reader["TotalSalesReturn"] as Int32? ?? 0,
                        TotalPurchasePrice = reader["TotalPurchasePrice"] as decimal? ?? 0,
                        TotalMrp = reader["TotalMrp"] as decimal? ?? 0
                    };
                    decimal? TotalMrp = Math.Round((decimal)salesReturnItem.TotalMrp, MidpointRounding.AwayFromZero);
                    decimal? TotalPurchasePrice = Math.Round((decimal)salesReturnItem.TotalPurchasePrice, MidpointRounding.AwayFromZero);
                    salesReturnItem.ReturnProfit = TotalMrp - TotalPurchasePrice;
                    list.Add(salesReturnItem);
                }
            }
            return list;
        }
        public async Task<List<SalesReturnItem>> BillCancelListData(string accountId, string instanceId, SalesReturnItem sr, string type)
        {
            var report = GetParameter(type, sr.Dates.FromDate, sr.Dates.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            report.FromDate = null;
            report.ToDate = null;
            string cancelNo = null, customerName = null, mobile = null, productId = null, invoiceNo = null, batch = null;
            if (sr.select == "CancelNo")
            {
                cancelNo = sr.selectedValue;
            }
            else if (sr.select == "Name")
            {
                customerName = sr.selectedValue;
            }
            else if (sr.select == "Mobile")
            {
                mobile = sr.selectedValue;
            }
            else if (sr.select == "Product")
            {
                productId = sr.selectedValue;
            }
            else if (sr.select == "InvoiceNo")
            {
                invoiceNo = sr.selectedValue;
            }
            else if (sr.select == "Batch")
            {
                batch = sr.selectedValue;
            }
            else
            {
                if ((int)sr.select1 == 0)
                {
                    report = GetParameter(type, sr.Dates.FromDate, sr.Dates.ToDate);
                    report.FromDate = report.FromDate + " 00:00:00";
                    report.ToDate = report.ToDate + " 23:59:59";
                }
            }
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("CancelNo", cancelNo);
            parms.Add("Name", customerName);
            parms.Add("Mobile", mobile);
            parms.Add("ProdId", productId);
            parms.Add("InvoiceNo", invoiceNo);
            parms.Add("Batch", batch);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_BillCancelList", parms);
            var salesreturnlist = result.Select(x =>
            {
                var srItem = new SalesReturnItem
                {
                    Quantity = x.Quantity as decimal? ?? 0,
                    MrpSellingPrice = x.MrpSellingPrice as decimal? ?? 0,
                    CancelledBy = Convert.ToString(x.Name),
                    CancelledDateTime = Convert.ToString(x.CancelDtTime), //Modified by Poongodi to fix Bill cancel report issue on 18/03/2017
                    SalesReturn = new SalesReturn
                    {
                        Id = x.Id,
                        ReturnDate = x.ReturnDate as DateTime? ?? DateTime.MinValue,
                        ReturnNo = Convert.ToString(x.ReturnNo),
                        CreatedAt = Convert.ToDateTime(x.CreatedAt),
                        Sales = new Sales
                        {
                            InvoiceDate = x.InvoiceDate,
                            InvoiceNo = x.InvoiceNo,
                            Name = x.CustomerName,
                            Mobile = x.Mobile,
                        },
                    },
                    ProductStock = new ProductStock
                    {
                        Product = new Contract.Infrastructure.Master.Product
                        {
                            Name = x.ProductName,
                        },
                        BatchNo = Convert.ToString(x.BatchNo),
                        ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        InstanceName = Convert.ToString(x.InstanceName),
                    },
                    TotalAmount = x.TotalAmount as decimal? ?? 0,
                };
                //srItem.TotalMrp = (srItem.Total - srItem.DiscountAmount) as decimal? ?? 0;

                //if (srItem.SalesReturn.Sales.Discount > 0) // Added by Sarubala to include overall discount on 10/07/17
                //{
                //    srItem.TotalMrp = (srItem.Total - (srItem.Total * srItem.SalesReturn.Sales.Discount / 100)) as decimal? ?? 0;
                //}
                return srItem;
            });
            if (sr.select == "Amount")
            {
                decimal ele1 = Convert.ToDecimal(sr.selectedValue);
                if ((int)sr.select1 == 1)
                {
                    var list1 = from item in salesreturnlist where item.TotalAmount > ele1 select item;
                    salesreturnlist = list1.ToList();
                }
                else if ((int)sr.select1 == 2)
                {
                    var list1 = from item in salesreturnlist where item.TotalAmount == ele1 select item;
                    salesreturnlist = list1.ToList();
                }
                else
                {
                    var list1 = from item in salesreturnlist where item.TotalAmount < ele1 select item;
                    salesreturnlist = list1.ToList();
                }
            }
            return salesreturnlist.ToList();
        }
        // Done Gavaskar 23-12-2016
        public async Task<List<SalesItem>> SalesList(string type, object accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string invoiceSearchType, int fromInvoice, int toInvoice)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("SearchOption", invoiceType);
            parms.Add("SearchValue", invoiceSearchType);
            parms.Add("FromInvoice", fromInvoice);
            parms.Add("ToInvoice", toInvoice);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesList", parms);
            /*Instance Name added by Poongodi on 14/09/2017*/
            var salesitems = result.Select(x =>
            {
                var salesItem = new HQue.Contract.Infrastructure.Inventory.SalesItem
                {
                    Sales =
                        {
                            InvoiceNo = x.InvoiceNo,
                            InvoiceSeries= x.InvoiceSeries,
                            ActualInvoice= x.InvoiceSeries +" "+ x.InvoiceNo,
                            InvoiceDate =  x.InvoiceDate as DateTime? ?? DateTime.MinValue,
                             ReportInvoiceDate = " "+x.InvoiceDate.ToString("dd-MM-yyyy"),
                            SalesType =  Convert.ToString(x.SalesTypeName),
                            Name =Convert.ToString(x.Name),
                           Select1 = Convert.ToString(x.Branch),
                            Instance = { Name =x.Branch}
                        },


                };
                //salesItem.Sales.Discount = (decimal)x.Discount;
                salesItem.Sales.Discount = x.TotalDiscountValue as decimal? ?? 0;
                //decimal? discountSum = x.DiscountSum as decimal? ?? 0;
                salesItem.Sales.InvoiceAmount = (decimal)x.InvoiceAmount;
                //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
                //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - salesItem.Sales.Discount - discountSum;
                salesItem.Sales.FinalValue = (decimal)x.SaleAmount;
                //salesItem.Sales.Discount = (decimal)x.Discount + (decimal)discountSum; //Item discount added to the sales discount to display the total discount value by Poongodi on 19/05/2017
                //salesItem.Sales.FinalValue = Math.Round(decimal.Parse(String.Format("{0:0.##}", salesItem.Sales.FinalValue)), MidpointRounding.AwayFromZero);
                //salesItem.Sales.RoundoffNetAmount = (decimal)(salesItem.Sales.FinalValue - (salesItem.Sales.InvoiceAmount - salesItem.Sales.Discount)); //Rounded off added by Poongodi on 19/05/2017
                salesItem.Sales.RoundoffNetAmount = (decimal)x.RoundoffSaleAmount;
                salesItem.Sales.IsRoundOff = x.IsRoundOff as bool? ?? false;
                return salesItem;
            });
            var salesItemsList = salesitems.ToList();
            return salesItemsList;
        }
        // Prefix Added in Sales report by Violet on 22/06/2017
        public async Task<List<SalesItem>> SalesCustomerWiseList(string Name, string Mobile, object accountId, string instanceId, DateTime from, DateTime to, string UserName, string UserId, string type)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            filterFromDate = from.ToFormat();
            filterToDate = to.ToFormat();

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", filterFromDate);
            parms.Add("EndDate", filterToDate);
            parms.Add("PatientName", Name);
            parms.Add("UserName", UserName);
            parms.Add("UserId", UserId);
            parms.Add("Type", type);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesCustomerInvoiceWise", parms);
            var list = result.Select(x =>
            {
                var salesItem = new SalesItem
                {
                    Sales =
                        {
                            InvoiceNo = x.InvoiceNo as string?? "",
                            InvoiceSeries = x.InvoiceSeries as string?? "",
                            ActualInvoice = (x.InvoiceSeries as string?? "")+" "+(x.InvoiceNo as string?? ""),
                            InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                        }
                };
                salesItem.Sales.Instance.Name = x.InstanceName as string ?? "";
                salesItem.Sales.Discount = x.TotalDiscountValue as decimal? ?? 0;
                salesItem.Sales.InvoiceAmount = x.InvoiceAmount as decimal? ?? 0;
                //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
                //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - discountSum;
                //salesItem.Sales.FinalValue = Math.Round(decimal.Parse(String.Format("{0:0.##}", salesItem.Sales.FinalValue)), MidpointRounding.AwayFromZero);
                salesItem.Sales.FinalValue = x.SaleAmount as decimal? ?? 0;
                salesItem.Sales.RoundoffSaleAmount = x.RoundoffSaleAmount as decimal? ?? 0;
                salesItem.Sales.Name = x.PatientName as string ?? "";
                salesItem.Sales.HQueUser.Name = x.UserName as string ?? "";
                return salesItem;
            }).ToList();
            return list;
        }
        // Done Gavaskar 24-12-2016
        public async Task<List<Sales>> HomeDeliveryList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string invoiceSearchType, int fromInvoice, int toInvoice)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("SearchOption", invoiceType);// added invoice series filter by Violet on 19/06/2017
            parms.Add("SearchValue", invoiceSearchType);
            parms.Add("FromInvoice", fromInvoice);
            parms.Add("ToInvoice", toInvoice);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetHomeDeliveryList", parms);
            /*Instance Name added by Poongodi on 14/09/2017*/
            var homedelivery = result.Select(x =>
            {
                //decimal InvoiceAmount = Math.Round((decimal)(x.SellingPrice), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0;
                var Sales = new HQue.Contract.Infrastructure.Inventory.Sales
                {
                    Id = x.SalesId,
                    Name = x.Name,
                    Mobile = x.Mobile,
                    Email = x.Email,
                    InvoiceNo = x.InvoiceNo,
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    ReportInvoiceDate = " " + x.InvoiceDate.ToString("dd-MM-yyyy"),
                    DeliveryType = x.DeliveryType,
                    //InvoiceAmount = Math.Round((decimal)(x.SellingPrice), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                    InvoiceAmount = x.SellingPrice as decimal? ?? 0,
                    RoundoffSaleAmount = x.RoundoffSaleAmount as decimal? ?? 0,
                    cash = x.cash as decimal? ?? 0, // Modified by Sarubala on 24 - 10 - 17
                    Credit = x.credit as decimal? ?? 0,
                    card = x.card as decimal? ?? 0,
                    cheque = x.cheque as decimal? ?? 0,
                    ewallet = x.ewallet as decimal? ?? 0,
                    subPayment = Convert.ToString(x.subPayment),
                    CreatedBy = x.CreatedBy,
                    FinalValue = 0.00M,
                    ActualInvoice = x.InvoiceSeries + " " + x.InvoiceNo,
                    Select1 = Convert.ToString(x.Branch),
                    Instance =
                    {
                        Name =  Convert.ToString( x.Branch),
                    }
                };
                var paymentType = x.PaymentType;
                //Modified by Sarubala on 24-10-17
                //if (paymentType == "Cash")
                //{
                //    Sales.cash = Sales.InvoiceAmount;
                //    Sales.FinalValue = Sales.cash;
                //}
                //if (paymentType == "Card")
                //{
                //    Sales.card = Sales.InvoiceAmount;
                //    Sales.FinalValue = Sales.card;
                //}
                //if (paymentType == "Credit")
                //{
                //    Sales.cash = Sales.InvoiceAmount - Sales.Credit;
                //    Sales.FinalValue = Sales.InvoiceAmount;
                //}
                //if (paymentType == "Cheque")
                //{
                //    Sales.cheque = Sales.InvoiceAmount;
                //    Sales.FinalValue = Sales.cheque;
                //}
                if (paymentType == "Credit")
                {
                    Sales.cash = Sales.InvoiceAmount - Sales.Credit;
                }
                Sales.FinalValue = Sales.InvoiceAmount;

                return Sales;
            });
            return homedelivery.ToList();
        }

        //Added by Sarubala on 28-08-17 - start
        public async Task<List<Doctor>> DoctorwiseReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, string filterType, string doctorId, string productId)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("DoctorId", doctorId);
            parms.Add("ProductId", productId);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetDoctorwiseReport", parms);
            var doctorlist = result.Select(x =>
            {
                var doctor = new HQue.Contract.Infrastructure.Master.Doctor
                {
                    BranchName = x.BranchName,
                    Name = x.DoctorName,
                    SalesQty = x.SalesQty as decimal? ?? 0,
                    ReturnQty = x.ReturnQty as decimal? ?? 0,
                    NetQuantity = x.NetQuantity as decimal? ?? 0,
                    NetValue = x.Netvalue as decimal? ?? 0,
                    ProfitPerc = x.ProfitPerc as decimal? ?? 0, 
                    Product = new Product
                    {
                        Name = x.ProductName
                    }
                };

                return doctor;
            });

            if (filterType == "2")
            {
                doctorlist = doctorlist.OrderBy(x => x.Product.Name).ToList();
            }
            else
            {
                doctorlist = doctorlist.OrderBy(x => x.Name).ToList();
            }

            return doctorlist.ToList();
        }
        //Added by Sarubala on 28-08-17 - end


        //add by nandhini for door delivery report
        public async Task<List<Sales>> doorDeliveryListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetDoorDeliveryList", parms);
            var homedelivery = result.Select(x =>

            {
                //decimal InvoiceAmount = Math.Round((decimal)(x.SellingPrice), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0;
                var Sales = new HQue.Contract.Infrastructure.Inventory.Sales
                {
                    Id = x.SalesId,
                    Name = x.Name,
                    Mobile = x.Mobile,
                    Email = x.Email,
                    InvoiceNo = x.InvoiceNo,
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    DeliveryType = x.DeliveryType,
                    PaymentType = x.PaymentType,
                    DoorDeliveryStatus = x.DoorDeliveryStatus,
                    //InvoiceAmount = Math.Round((decimal)(x.SellingPrice), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                    InvoiceAmount = x.SaleAmount as decimal? ?? 0,
                    RoundoffSaleAmount = x.RoundoffSaleAmount as decimal? ?? 0,
                    cash = x.cash as decimal? ?? 0, // Modified by Sarubala on 24 - 10 - 17
                    Credit = x.credit as decimal? ?? 0,
                    card = x.card as decimal? ?? 0,
                    cheque = x.cheque as decimal? ?? 0,
                    ewallet = x.ewallet as decimal? ?? 0,
                    subPayment = Convert.ToString(x.subPayment),
                    CreatedBy = x.CreatedBy,
                    FinalValue = 0.00M,
                    ActualInvoice = x.InvoiceSeries + " " + x.InvoiceNo,
                    Instance =
                        {
                            Name = x.InstanceName
                        },
                };
                var paymentType = x.PaymentType;
                // Modified by Sarubala on 24 - 10 - 17
                //if (paymentType == "Cash")
                //{
                //    Sales.cash = Sales.InvoiceAmount;
                //    Sales.FinalValue = Sales.cash;
                //}
                //if (paymentType == "Card")
                //{
                //    Sales.card = Sales.InvoiceAmount;
                //    Sales.FinalValue = Sales.card;
                //}
                //if (paymentType == "Credit")
                //{
                //    Sales.cash = Sales.InvoiceAmount - Sales.Credit;
                //    Sales.FinalValue = Sales.InvoiceAmount;
                //}
                //if (paymentType == "Cheque")
                //{
                //    Sales.cheque = Sales.InvoiceAmount;
                //    Sales.FinalValue = Sales.cheque;
                //}
                if (paymentType == "Credit")
                {
                    Sales.cash = Sales.InvoiceAmount - Sales.Credit;
                }
                Sales.FinalValue = Sales.InvoiceAmount;
                return Sales;
            });
            //var res1 = homedelivery.ToList();
            //res1.RemoveAll(x => x.DeliveryType == "Counter" || x.PaymentType == "Credit");

            //return res1.ToList();
            return homedelivery.ToList();

        }
        //end
        // Done Gavaskar 24-12-2016
        // Buy Summary Report Start
        /// <summary>
        /// IsInvoiceDate boolean field added for invoicedate filter in purchase report
        /// </summary>
        /// <param name="IsInvoiceDate"></param>
        public async Task<List<VendorPurchaseItem>> BuySummaryList(string type, string accountId, string instanceId, DateTime from, DateTime to, bool IsInvoiceDate)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("IsInvoiceDate", IsInvoiceDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetBuySummaryList", parms);
            var vendorpurchase = result.Select(x =>
            {
                var vendorPurchaseItem = new HQue.Contract.Infrastructure.Inventory.VendorPurchaseItem
                {
                    //added by nandhini for csv download 24.10.17                      
                    ReportInvoiceDate = " " + x.InvoiceDate.ToString("dd-MM-yyyy"),
                    Credit = x.NoteValue as decimal? ?? null,
                    ActualPaymentTotal = x.DebitNoteValue as decimal? ?? null,
                    TotalPurchaseValue = Math.Round((decimal)(x.FinalValue), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                    BuyPurchase = new HQue.Contract.Infrastructure.Inventory.VendorPurchase
                    {
                        InvoiceNo = x.InvoiceNo,
                        InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                        GoodsRcvNo = x.GoodsRcvNo,
                        BillSeries = x.BillSeries,
                        Credit = x.NoteValue as decimal? ?? null,
                        ActualPaymentTotal = x.DebitNoteValue as decimal? ?? null,
                        TotalPurchaseValue = Math.Round((decimal)(x.FinalValue), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0,
                        Vendor =
                            {
                                 Name =x.Name,
                            },
                        Instance =
                        {
                            Name = x.InstanceName
                        }
                    },

                };
                vendorPurchaseItem.ReportTotal = Math.Round((decimal)(x.InvoiceValue), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0;

                //vendorPurchaseItem.ReportTotal = Math.Round(decimal.Parse(String.Format("{0:0.##}", x.InvoiceValue)), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0;
                return vendorPurchaseItem;
            });
            return vendorpurchase.ToList();
        }
        //Buy Summary Report End
        // Done Sarubala 07-02-2017
        // Buy Vendor Order Report Start
        public async Task<List<VendorOrderItem>> PurchaseOrderData(string type, VendorOrder order1)
        {
            var report = GetParameter(type, order1.Dates.FromDate, order1.Dates.ToDate);
            string SpName = "";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", order1.InstanceId);
            parms.Add("AccountId", order1.AccountId);
            if (type == "OrderNo")
            {
                parms.Add("OrderId", order1.OrderId);
                SpName = "usp_GetBuyOrderUnique";
            }
            else
            {
                parms.Add("StartDate", report.FromDate);
                parms.Add("EndDate", report.ToDate);
                SpName = "usp_GetBuyOrderList";
            }
            var result = await sqldb.ExecuteProcedureAsync<dynamic>(SpName, parms);
            var vendorOrdrItm = result.Select(x =>
            {
                var vendorItem = new VendorOrderItem
                {
                    //added by nandhini for csv download 24.10.17
                    OrderId = x.OrderId.ToString(),
                    ReportInvoiceDate = " " + x.OrderDate.ToString("dd-MM-yyyy"),
                    VendorOrder = new VendorOrder
                    {
                        Id = x.Id.ToString(),
                        VendorId = x.VendorId.ToString(),
                        OrderId = x.OrderId.ToString(),
                        OrderDate = x.OrderDate as DateTime? ?? DateTime.MinValue,
                        TaxRefNo = x.TaxRefNo as int? ?? 0, //Added by Sarubala to include GST 06-07-2017
                        Instance = {
                                Name = x.InstanceName.ToString()
                            },
                        Vendor =
                            {
                                Name = x.Name.ToString(),
                                Address = x.Address.ToString(),
                                Area = x.Area.ToString(),
                                City = x.City.ToString(),
                                DrugLicenseNo = Convert.ToString(x.DrugLicenseNo),
                                TinNo = Convert.ToString(x.TinNo),
                                Mobile = Convert.ToString(x.Mobile),
                                Email = Convert.ToString(x.Email),
                            },
                    },
                    ProductId = Convert.ToString(x.ProductId),
                    Quantity = x.Quantity as decimal? ?? 0,
                    Status = Convert.ToByte(x.Status),
                    FreeQty = x.FreeQty as decimal? ?? null,
                    Discount = x.Discount as decimal? ?? null,
                    PackageSellingPrice = x.PackageSellingPrice as decimal? ?? null,
                    Product = new Contract.Infrastructure.Master.Product
                    {
                        Id = Convert.ToString(x.ProductId),
                        Name = Convert.ToString(x.ProductName),
                    },
                    ProductStock = new ProductStock
                    {
                        VAT = x.VAT as decimal? ?? null,
                        PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? null,
                        PackageSize = x.PackageSize as decimal? ?? null,
                        CST = x.CST as decimal? ?? null,
                        GstTotal = x.GstTotal as decimal? ?? null,
                        Cgst = x.Cgst as decimal? ?? null,
                        Sgst = x.Sgst as decimal? ?? null,
                        Igst = x.Igst as decimal? ?? null,
                    },

                };
                if (vendorItem.VendorOrder.TaxRefNo == 1)
                {
                    vendorItem.ProductStock.VAT = vendorItem.ProductStock.GstTotal;
                }
                return vendorItem;
            });
            return vendorOrdrItm.ToList();
        }
        // Buy Vendor Order Report End
        // Buy Return Summary Report Start - By San -22-02-2017
        /// <summary>
        /// IsInvoiceDate boolean field added for returndate filter in purchase report
        /// </summary>
        /// <param name="IsInvoiceDate"></param>
        public async Task<List<VendorPurchaseReturn>> ReturnSummaryListData(string type, string accountId, string instanceId, DateTime from, DateTime to, bool IsInvoiceDate)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("IsInvoiceDate", IsInvoiceDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetBuyReturnSummaryList", parms);
            var vendorpurchase = result.Select(x =>
            {
                var vendorPurchaseReturn = new VendorPurchaseReturn
                {
                    //added by nandhini for csv download 24.10.17                   
                    ReportInvoiceDate = " " + Convert.ToString(x.InvoiceDate),
                    ReportReturnDate = " " + Convert.ToString(x.ReturnDate),
                    ReturnNo = x.ReturnNo,
                    ReturnDate = x.ReturnDate,
                    InvoiceNo = x.InvoiceNo,
                    InvoiceDate = x.InvoiceDate,
                    VendorName = x.Name,
                    InstanceName = x.InstanceName,
                    VendorpurchaseReturnReport = new Contract.Infrastructure.Reports.VendorpurchaseReturnReport
                    {
                        NoOfItem = x.NOOfItem,
                        Total = x.Total
                    }
                };
                //vendorPurchaseItem.ReportTotal = Math.Round((decimal)(x.InvoiceValue), 0, MidpointRounding.AwayFromZero) as decimal? ?? 0;
                return vendorPurchaseReturn;
            });
            return vendorpurchase.ToList();
        }
        //Buy Return Summary Report End
        #region

        /// <summary>
        /// Purchase ORder Details
        ///  IsInvoiceDate boolean field added for invoicedate filter in purchase report
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="IsInvoiceDate"></param>
        /// <returns></returns>
        //VendorId, Productid added as paramater (the same method consume by Purchase Report
        public async Task<List<VendorPurchaseItem>> BuyReportList(string type, string accountId, string instanceId, string sProductId, string sVendorId, DateTime from, DateTime to, bool IsInvoiceDate)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("VendorId", sVendorId);
            parms.Add("ProductId", sProductId);
            parms.Add("IsInvoiceDate", IsInvoiceDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPOList", parms);
            var POItems = result.Select(x =>
            {
                var vendorPurchaseItem = new VendorPurchaseItem
                {
                    ProductStockId = Convert.ToString(x.ProductStockId),
                    Quantity = x.Quantity as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    PackagePurchasePrice =
                           x.PackagePurchasePrice as decimal? ?? 0,
                    PackageQty = x.PackageQty as decimal? ?? 0,
                    PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    FreeQty = x.FreeQty as decimal? ?? 0,
                    ActualQty = x.ActualQty,
                    ReportTotal = x.ReportTotal,
                    VATValue = x.VatValue as decimal? ?? 0,
                    TaxRefNo = x.TaxRefNo as int? ?? 0,  //Added by Sarubala to include GST 06-07-2017
                    InstanceName = Convert.ToString(x.InstanceName), // Added by Lawrence for all branch
                    // Added by Gavaskar 12-10-2017 Markup Related Start
                    MarkupPerc = x.MarkupPerc as decimal? ?? 0,
                    SchemeDiscountPerc = x.SchemeDiscountPerc as decimal? ?? 0,
                    FreeQtyTaxValue = x.FreeQtyTaxValue as decimal? ?? 0,
                    //added by nandhini for csv download 24.10.17
                    reason = Convert.ToString(x.VendorName),
                    ReportInvoiceDate = " " + x.InvoiceDate.ToString("dd-MM-yyyy"),
                    Category = Convert.ToString(x.VendorPONo),
                    // Added by Gavaskar 12-10-2017 Markup Related End

                    BuyPurchase =
                        {
                        Id = Convert.ToString(x.VendorPurchaseId),
                        InvoiceNo = Convert.ToString(x.VendorPONo),
                        InvoiceDate =
                                Convert.ToDateTime(x.InvoiceDate),
                        Discount = x.PODiscount as decimal? ?? 0
                    },
                    Vendor =
                        {
                        Name = Convert.ToString(x.VendorName),
                        Mobile = Convert.ToString(x.VendorMobile),
                        Email = Convert.ToString(x.VendorEmail),
                        Address = Convert.ToString(x.VendorAddress),
                        TinNo = Convert.ToString(x.VendorTinNo)
                    },
                    ProductStock =
                        {
                        SellingPrice = x.ProductStockSellingPrice as decimal? ?? 0,
                        VAT = x.VAT as decimal? ?? 0,
                        CST = x.CST as decimal? ?? 0,
                        GstTotal = x.GSTTotal as decimal? ?? 0,
                        Cgst = x.Cgst as decimal? ?? 0,
                        Sgst = x.Sgst as decimal? ?? 0,
                        Igst = x.Igst as decimal? ?? 0,
                        Product = { Name = Convert.ToString(x.ProductName),
                                    HsnCode = Convert.ToString(x.HsnCode)
                                  }
                },
                    VendorPurchase =
                    {
                        TaxRefNo = x.TaxRefNo as int? ?? 0
                    }
                    //vendorPurchaseItem.ReportTotal = (((vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty) * vendorPurchaseItem.PackagePurchasePrice) -
                    //                                 ((vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty)) *
                    //                                  (vendorPurchaseItem.Discount / 100))) + vendorPurchaseItem.VatPrice;
                };

                if (vendorPurchaseItem.VendorPurchase.TaxRefNo == 1)
                {
                    vendorPurchaseItem.ProductStock.VAT = vendorPurchaseItem.ProductStock.GstTotal;
                }
                return vendorPurchaseItem;
            });
            return POItems.ToList();
        }

        /// <summary>
        /// Purchase ORder Details
        /// Newly added by Manivannan
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        //VendorId, Productid added as paramater(the same method consume by Purchase Report
        public async Task<List<HQue.Contract.Infrastructure.Reports.ProductPurchaseSalesVM>> BuySalesList(string grnNo, string accountId, string instanceId,
                   DateTime from, DateTime to)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);
            parms.Add("GRNNo", "87");

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetProduct_PurchaseSalesList", parms);
            var BuySalesEnum = result.Select(x =>
            {

                var VendorPurchaseItem = new VendorPurchaseItem
                {
                    Quantity = x.PurchaseQuantity as decimal? ?? 0,
                    FreeQty = x.FreeQty as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    ProductStock = {
                    Id = Convert.ToString(x.ProductStockId),
                    SellingPrice = x.PS_SellingPrice as decimal? ?? 0,
                    MRP = x.PS_MRP as decimal? ?? 0,
                    Stock = x.PS_Stock as decimal? ?? 0,
                    Product =
                        {
                            Name = Convert.ToString(x.ProductName)
                        }
                }
                };
                var SalesItem = new SalesItem
                {
                    Quantity = x.SI_Quantity as decimal? ?? 0,
                    SellingPrice = x.SI_SellingPrice as decimal? ?? 0,
                    MRP = x.SI_MRP as decimal? ?? 0
                };

                var ProductPurchaseSalesVM = new ProductPurchaseSalesVM
                {
                    //VendorPurchase = {
                    //    VendorPurchaseItem = VendorPurchaseItem as IEnumerable<VendorPurchaseItem>
                    //},
                    //SalesExt =
                    //{
                    //    Sales =
                    //    {   
                    //        InvoiceDate = x.S_InvoiceDate,
                    //        InvoiceNo = Convert.ToString(x.S_InvoiceNo),
                    //        Name = Convert.ToString(x.CustomerName)
                    //    },
                    //    SalesItemExt= {
                    //        SalesItem = (SalesItem as IEnumerable<SalesItem>).ToList(),
                    //        SI_Selling_Amount = x.SI_Selling_Amount as decimal? ?? 0,
                    //        SellingPrice_Profit = x.SellingPrice_Profit as decimal? ?? 0,
                    //        MRP_Profit = x.MRP_Profit as decimal? ?? 0
                    //    }
                    //},
                    //ProductStock =
                    //{
                    //    //ProductStock =
                    //    //{
                    //    //    Id = Convert.ToString(x.ProductStockId),
                    //    //    SellingPrice = x.PS_SellingPrice as decimal? ?? 0,
                    //    //    MRP = x.PS_MRP as decimal? ?? 0,
                    //    //    Stock = x.PS_Stock as decimal? ?? 0
                    //    //},
                    //    PS_Purchase_Amount = x.PS_Purchase_Amount as decimal? ?? 0
                    //}


                };
                return ProductPurchaseSalesVM;
            });
            var BuySalesList = BuySalesEnum.ToList();
            return BuySalesList;
        }

        #endregion
        #region Purchase Margin Report created by Poongodi on 20/07/2017
        /// <summary>
        /// This method helps to get the purchase details with margin
        /// IsInvoiceDate boolean field added for invoicedate filter in purchase report
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="IsInvoiceDate"></param>
        /// <returns></returns>
        public async Task<List<VendorPurchaseItem>> BuyMarginList(string type, string accountId, string instanceId,
                   DateTime from, DateTime to, bool IsInvoiceDate)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("IsInvoiceDate", IsInvoiceDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPOMarginList", parms);
            var POItems = result.Select(x =>
            {
                var vendorPurchaseItem = new VendorPurchaseItem
                {
                    ProductStockId = Convert.ToString(x.ProductStockId),
                    Quantity = x.Quantity as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    PackagePurchasePrice =
                           x.PackagePurchasePrice as decimal? ?? 0,
                    PackageQty = x.PackageQty as decimal? ?? 0,
                    PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    FreeQtyTaxValue = x.FreeQtyTaxValue as decimal? ?? 0,
                    SchemeDiscountPerc = x.SchDiscount as decimal? ?? 0,
                    MarkupPerc = x.MarkupDiscount as decimal? ?? 0,
                    FreeQty = x.FreeQty as decimal? ?? 0,
                    ActualQty = x.ActualQty,
                    ReportTotal = x.ReportTotal,
                    VATValue = x.VatValue as decimal? ?? 0,
                    MRPValue = x.MRPValue as decimal? ?? 0,
                    MarginValue = x.MarginAmount as decimal? ?? 0,
                    MarginPercent = x.MarginPer as decimal? ?? 0,
                    TaxRefNo = x.TaxRefNo as int? ?? 0,
                    InstanceName = Convert.ToString(x.InstanceName),
                    //added by nandhini for csv download 24.10.17
                    reason = Convert.ToString(x.VendorName),
                    ReportInvoiceDate = " " + x.InvoiceDate.ToString("dd-MM-yyyy"),
                    Category = Convert.ToString(x.VendorPONo),
                    BuyPurchase =
                        {
                        Id = Convert.ToString(x.VendorPurchaseId),
                        InvoiceNo = Convert.ToString(x.VendorPONo),
                        InvoiceDate =
                                Convert.ToDateTime(x.InvoiceDate),
                        Discount = x.PODiscount as decimal? ?? 0
                    },
                    Vendor =
                        {
                        Name = Convert.ToString(x.VendorName),
                        Mobile = Convert.ToString(x.VendorMobile),
                        Email = Convert.ToString(x.VendorEmail),
                        Address = Convert.ToString(x.VendorAddress),
                        TinNo = Convert.ToString(x.VendorTinNo)
                    },
                    ProductStock =
                        {
                        SellingPrice = x.ProductStockSellingPrice as decimal? ?? 0,
                        VAT = x.VAT as decimal? ?? 0,
                        CST = x.CST as decimal? ?? 0,
                        GstTotal = x.GSTTotal as decimal? ?? 0,  //Added by Sarubala to include GST 06-07-2017 - start
                        Cgst = x.Cgst as decimal? ?? 0,
                        Sgst = x.Sgst as decimal? ?? 0,
                        Igst = x.Igst as decimal? ?? 0,
                        Product = { Name = Convert.ToString(x.ProductName)}
                },
                    VendorPurchase =
                    {
                        TaxRefNo = x.TaxRefNo as int? ?? 0
                    }
                    //vendorPurchaseItem.ReportTotal = (((vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty) * vendorPurchaseItem.PackagePurchasePrice) -
                    //                                 ((vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty)) *
                    //                                  (vendorPurchaseItem.Discount / 100))) + vendorPurchaseItem.VatPrice;
                };
                if (vendorPurchaseItem.VendorPurchase.TaxRefNo == 1)
                {
                    vendorPurchaseItem.ProductStock.VAT = vendorPurchaseItem.ProductStock.GstTotal;
                }
                //Added by Sarubala to include GST 06-07-2017 - end
                return vendorPurchaseItem;
            });
            return POItems.ToList();
        }
        #endregion
        public async Task<List<VendorPurchaseItem>> BuyReporProductwisetList(string id, string accountId, string instanceId,
          DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.QuantityColumn,
                VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.VatValueColumn, VendorPurchaseItemTable.GstValueColumn, VendorPurchaseItemTable.PackageSizeColumn);
            qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn,
                VendorPurchaseItemTable.VendorPurchaseIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn,
                VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.InvoiceDateColumn,
                VendorPurchaseTable.DiscountColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.IdColumn, VendorTable.NameColumn,
                VendorTable.MobileColumn, VendorTable.EmailColumn, VendorTable.AddressColumn, VendorTable.TinNoColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn,
                VendorPurchaseItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn,
                ProductStockTable.VATColumn, ProductStockTable.CSTColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceDateColumn, CriteriaEquation.Between);
            qb.Parameters.Add("FilterFromDate", from.ToFormat());
            qb.Parameters.Add("FilterToDate", to.ToFormat());
            qb.ConditionBuilder.And(VendorPurchaseTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(VendorPurchaseTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, id);
            qb.SelectBuilder.SortByDesc(VendorPurchaseTable.InvoiceDateColumn);
            qb.SelectBuilder.SortByDesc(VendorPurchaseTable.CreatedAtColumn);
            var list = new List<VendorPurchaseItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseItem = new VendorPurchaseItem
                    {
                        ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = reader[VendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackagePurchasePrice =
                           reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0,
                        PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] as int? ?? 0,
                        GstValue = reader[VendorPurchaseItemTable.GstValueColumn.ColumnName] as decimal? ?? 0,
                        VatValue = reader[VendorPurchaseItemTable.VatValueColumn.ColumnName] as decimal? ?? 0,
                        BuyPurchase = new VendorPurchase
                        {
                            Id = reader[VendorPurchaseTable.IdColumn.FullColumnName].ToString(),
                            InvoiceNo = reader[VendorPurchaseTable.InvoiceNoColumn.FullColumnName].ToString(),
                            InvoiceDate =
                                Convert.ToDateTime(reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName])
                        }
                    };
                    vendorPurchaseItem.BuyPurchase.Discount =
                        reader[VendorPurchaseTable.DiscountColumn.FullColumnName] != DBNull.Value
                            ? Convert.ToDecimal(reader[VendorPurchaseTable.DiscountColumn.FullColumnName])
                            : 0;
                    vendorPurchaseItem.BuyPurchase.Vendor = new Contract.Infrastructure.Master.Vendor
                    {
                        Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
                        Mobile = reader[VendorTable.MobileColumn.FullColumnName].ToString(),
                        Email = reader[VendorTable.EmailColumn.FullColumnName].ToString(),
                        Address = reader[VendorTable.AddressColumn.FullColumnName].ToString(),
                        TinNo = reader[VendorTable.TinNoColumn.FullColumnName].ToString()
                    };
                    vendorPurchaseItem.ProductStock = new ProductStock
                    {
                        SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                        VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                        CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0,
                        Product = { Name = reader[ProductTable.NameColumn.FullColumnName].ToString() }
                    };
                    vendorPurchaseItem.ReportTotal = (((vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty) * vendorPurchaseItem.PackagePurchasePrice) -
                                                     ((vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty)) *
                                                      (vendorPurchaseItem.Discount / 100))) + vendorPurchaseItem.VatPrice;
                    vendorPurchaseItem.ActualQty = vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty;
                    list.Add(vendorPurchaseItem);
                }
            }
            return list;
        }
        public async Task<List<VendorPurchaseItem>> BuyReportVendorwiseList(string id, string accountId, string instanceId,
          DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.QuantityColumn,
                VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.VatValueColumn, VendorPurchaseItemTable.GstValueColumn, VendorPurchaseItemTable.PackageSizeColumn);
            qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn,
                VendorPurchaseItemTable.VendorPurchaseIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn,
                VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.InvoiceDateColumn,
                VendorPurchaseTable.DiscountColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.IdColumn, VendorTable.NameColumn,
                VendorTable.MobileColumn, VendorTable.EmailColumn, VendorTable.AddressColumn, VendorTable.TinNoColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn,
                VendorPurchaseItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn,
                ProductStockTable.VATColumn, ProductStockTable.CSTColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceDateColumn, CriteriaEquation.Between);
            qb.Parameters.Add("FilterFromDate", from.ToFormat());
            qb.Parameters.Add("FilterToDate", to.ToFormat());
            qb.ConditionBuilder.And(VendorPurchaseTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(VendorPurchaseTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(VendorPurchaseTable.VendorIdColumn, id);
            qb.SelectBuilder.SortByDesc(VendorPurchaseTable.InvoiceDateColumn);
            qb.SelectBuilder.SortByDesc(VendorPurchaseTable.CreatedAtColumn);
            var list = new List<VendorPurchaseItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseItem = new VendorPurchaseItem
                    {
                        ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = reader[VendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackagePurchasePrice =
                           reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0,
                        PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] as int? ?? 0,
                        GstValue = reader[VendorPurchaseItemTable.GstValueColumn.ColumnName] as decimal? ?? 0,
                        VatValue = reader[VendorPurchaseItemTable.VatValueColumn.ColumnName] as decimal? ?? 0,
                        BuyPurchase = new VendorPurchase
                        {
                            Id = reader[VendorPurchaseTable.IdColumn.FullColumnName].ToString(),
                            InvoiceNo = reader[VendorPurchaseTable.InvoiceNoColumn.FullColumnName].ToString(),
                            InvoiceDate =
                                Convert.ToDateTime(reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName])
                        }
                    };
                    vendorPurchaseItem.BuyPurchase.Discount =
                        reader[VendorPurchaseTable.DiscountColumn.FullColumnName] != DBNull.Value
                            ? Convert.ToDecimal(reader[VendorPurchaseTable.DiscountColumn.FullColumnName])
                            : 0;
                    vendorPurchaseItem.BuyPurchase.Vendor = new Contract.Infrastructure.Master.Vendor
                    {
                        Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
                        Mobile = reader[VendorTable.MobileColumn.FullColumnName].ToString(),
                        Email = reader[VendorTable.EmailColumn.FullColumnName].ToString(),
                        Address = reader[VendorTable.AddressColumn.FullColumnName].ToString(),
                        TinNo = reader[VendorTable.TinNoColumn.FullColumnName].ToString()
                    };
                    vendorPurchaseItem.ProductStock = new ProductStock
                    {
                        SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                        VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                        CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0,
                        Product = { Name = reader[ProductTable.NameColumn.FullColumnName].ToString() }
                    };
                    vendorPurchaseItem.ReportTotal = (((vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty) * vendorPurchaseItem.PackagePurchasePrice) -
                                                     ((vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty)) *
                                                      (vendorPurchaseItem.Discount / 100))) + vendorPurchaseItem.VatPrice;
                    vendorPurchaseItem.ActualQty = vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty;
                    list.Add(vendorPurchaseItem);
                }
            }
            return list;
        }

        // Done Gavaskar 24-12-2016
        public async Task<List<ProductStock>> LowProductStockReportList(string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            var productStocks = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetLowProductStockInfo", parms);
            var productStockslist = productStocks.Select(x =>
            {
                var productStock = new ProductStock()
                {
                    Product = { Name = x.Productname, Id = x.Productid, Manufacturer = x.ManufacturerName, },
                    Stock = x.Stock as decimal? ?? 0,
                    Vendor = { Id = x.VendorId },
                    ReOrderQty = Convert.ToDecimal(x.OrderQty),
                    PurchasePrice = x.purchasePrice,
                    VAT = x.vat,
                    BatchNo = x.BatchNo
                };
                return productStock;
            });
            return productStockslist.ToList();
        }

        // Done Gavaskar 24-12-2016
        public async Task<List<Instance>> DistributorBranchList(params object[] parameters)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("EmailId", parameters[0]);
            var result = await sqldb.ExecuteProcedureAsync<Instance>("usp_GetDistributorBranchList", parms);
            var instancelist = result.Select(x =>
            {
                var instance = new Instance
                {
                    Id = x.Id,
                    Name = x.Name,
                    Phone = x.Phone,
                    Address = x.Address,
                };
                return instance;
            });
            return instancelist.ToList();
        }
        // Done Gavaskar 24-12-2016 N
        public async Task<int> DistributorBranchCount(params object[] parameters)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("EmailId", parameters[0]);
            var result = await sqldb.ExecuteProcedureAsync<int>("usp_GetDistributorBranchCount", parms);
            var count = result.FirstOrDefault();
            return count;
        }
        // Done Gavaskar 24-12-2016
        public async Task<List<ProductStock>> DistributorLowProductStockList(params object[] parameters)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", parameters[1]);
            parms.Add("EmailId", parameters[0]);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetDistributorLowProductStockList", parms);
            var productStockslist = result.Select(x =>
            {
                var productStock = new ProductStock()
                {
                    Product = {
                            Name = x.Product
                        },
                    Stock = x.Stock as decimal? ?? 0,
                };
                return productStock;
            });
            return productStockslist.ToList();
        }
        // Done Gavaskar 24-12-2016
        //date parameter added by nandhini 13-9-17
        public async Task<List<ProductStock>> ExpireProductList(string instanceId, DateTime from, DateTime to)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            var products = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetProductExpiryList", parms);
            var productlist = products.Select(x =>
            {
                var productStock = new ProductStock()
                {
                    Product = {
                            Name = x.Product
                        },
                    Vendor = {
                            Name = x.Vendor
                        },
                    ReportInvoiceDate = " " + x.ExpireDate.ToString("MM-yy"),
                    Stock = x.Stock as decimal? ?? 0,
                    ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                    InstanceId = instanceId,
                    Id = x.Id,
                    idchk = x.Id
                };
                return productStock;
            });
            return productlist.ToList();
        }
        #region
        //The below method renamed and the newmethod created to convert from direct query to SP - by poongodi
        public async Task<List<VendorPurchaseReturnItem>> BuyReturnReportList_Old(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            if (string.IsNullOrEmpty(type))
            {
                filterFromDate = from.ToFormat();
                filterToDate = to.ToFormat();
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "WEEK":
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "MONTH":
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
            }
            filterFromDate = filterFromDate + " 00:00:00";
            filterToDate = filterToDate + " 23:59:59";
            var sql = $@"select vr.Id,
                               vr.ReturnDate,
                               vr.ReturnNo,
	                           vri.Quantity,
	                           vri.ProductStockId,
	                           vp.InvoiceNo,
	                           vp.Discount,
	                           vp.InvoiceNo,
	                           v.Name as vendorName,
	                           v.Mobile,
	                           v.Email,
	                           v.Address,
	                           v.TinNo,
	                           p.Name as productName,
	                           vpi.PurchasePrice,
	                           ps.SellingPrice,
	                           ps.VAT
	             from VendorReturnItem as vri left join VendorReturn as vr on vri.VendorReturnId = vr.Id
				       left join VendorPurchase as vp on vp.Id = vr.VendorPurchaseId
					   left join Vendor as v on v.Id = vp.VendorId
					   left join ProductStock as ps on ps.Id = vri.ProductStockId
					   left join Product as p on p.Id = ps.ProductId
					   left join (select ProductStockId, max(PurchasePrice) as PurchasePrice from VendorPurchaseItem  group by ProductStockId) as vpi on vpi.ProductStockId = ps.Id   
	                   where vri.InstanceId='{instanceId}'  
	                   and vri.AccountId='{accountId}'
	                   and vri.CreatedAt between '{filterFromDate}' and '{filterToDate}' order by vri.CreatedAt desc";
            var list = new List<VendorPurchaseReturnItem>();
            var qb = QueryBuilderFactory.GetQueryBuilder(sql);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                    {
                        ProductStockId = reader["ProductStockId"].ToString(),
                        Quantity = reader["Quantity"] as decimal? ?? 0,
                        VendorPurchaseReturn =
                        {
                            Id = reader["Id"].ToString(),
                            ReturnNo = reader["ReturnNo"].ToString(),
                            ReturnDate = Convert.ToDateTime(reader["ReturnDate"]),
                            VendorPurchase =
                            {
                                Vendor =
                                {
                                    Name = reader["vendorName"].ToString(),
                                    Mobile = reader["Mobile"].ToString(),
                                    Email = reader["Email"].ToString(),
                                    Address = reader["Address"].ToString(),
                                    TinNo = reader["TinNo"].ToString()
                                },
                                Discount = reader["Discount"] as decimal? ?? 0,
                                InvoiceNo = reader["InvoiceNo"].ToString(),
                            }
                        },
                        ProductStock =
                        {
                            SellingPrice = reader["SellingPrice"] as decimal? ?? 0,
                            VAT = reader["VAT"] as decimal? ?? 0,
                            Product = {Name = reader["productName"].ToString()},
                        },
                        ReturnPurchasePrice = reader["PurchasePrice"] as decimal? ?? 0,
                    };
                    vendorPurchaseReturnItem.Total = ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity));
                    list.Add(vendorPurchaseReturnItem);
                }
            }
            return list;
            //var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            //qb.AddColumn(VendorReturnItemTable.ProductStockIdColumn, VendorReturnItemTable.QuantityColumn);
            //qb.JoinBuilder.Join(VendorReturnTable.Table, VendorReturnTable.IdColumn, VendorReturnItemTable.VendorReturnIdColumn);
            //qb.JoinBuilder.AddJoinColumn(VendorReturnTable.Table, VendorReturnTable.IdColumn, VendorReturnTable.ReturnNoColumn, VendorReturnTable.ReturnDateColumn);
            //qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorReturnTable.VendorPurchaseIdColumn);
            //qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorPurchaseTable.VendorIdColumn, VendorPurchaseTable.DiscountColumn, VendorPurchaseTable.InvoiceNoColumn);
            //qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            //qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.MobileColumn, VendorTable.EmailColumn, VendorTable.AddressColumn, VendorTable.TinNoColumn);
            //qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            //qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn);
            //qb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            //qb.JoinBuilder.AddJoinColumn(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.IdColumn, VendorPurchaseItemTable.PurchasePriceColumn);
            //qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            //qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            //if (string.IsNullOrEmpty(type))
            //{
            //    qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Between);
            //    qb.Parameters.Add("FilterFromDate", from.ToFormat());
            //    qb.Parameters.Add("FilterToDate", to.ToFormat());
            //}
            //else
            //{
            //    switch (type.ToUpper())
            //    {
            //        case "TODAY":
            //            qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CustomDateTime.IST.ToFormat());
            //            break;
            //        case "WEEK":
            //            qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Between);
            //            qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
            //            qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
            //            break;
            //        case "MONTH":
            //            qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Between);
            //            qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
            //            qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
            //            break;
            //    }
            //}
            //qb.ConditionBuilder.And(VendorReturnTable.AccountIdColumn, accountId);
            //qb.ConditionBuilder.And(VendorReturnTable.InstanceIdColumn, instanceId);
            //qb.SelectBuilder.SortByDesc(VendorReturnTable.CreatedAtColumn);
            //var list = new List<VendorPurchaseReturnItem>();
            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
            //        {
            //            ProductStockId = reader[VendorReturnItemTable.ProductStockIdColumn.ColumnName].ToString(),
            //            Quantity = reader[VendorReturnItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
            //            VendorPurchaseReturn =
            //            {
            //                Id = reader[VendorReturnTable.IdColumn.FullColumnName].ToString(),
            //                ReturnNo = reader[VendorReturnTable.ReturnNoColumn.FullColumnName].ToString(),
            //                ReturnDate = Convert.ToDateTime(reader[VendorReturnTable.ReturnDateColumn.FullColumnName]),
            //                VendorPurchase =
            //                {
            //                    Vendor =
            //                    {
            //                        Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
            //                        Mobile = reader[VendorTable.MobileColumn.FullColumnName].ToString(),
            //                        Email = reader[VendorTable.EmailColumn.FullColumnName].ToString(),
            //                        Address = reader[VendorTable.AddressColumn.FullColumnName].ToString(),
            //                        TinNo = reader[VendorTable.TinNoColumn.FullColumnName].ToString()
            //                    },
            //                    Discount = reader[VendorPurchaseTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
            //                    InvoiceNo = reader[VendorPurchaseTable.InvoiceNoColumn.FullColumnName].ToString(),
            //                }
            //            },
            //            ProductStock =
            //            {
            //                SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
            //                VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
            //                Product = {Name = reader[ProductTable.NameColumn.FullColumnName].ToString()},
            //            },
            //            ReturnPurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0,
            //        };
            //        //vendorPurchaseReturnItem.Total = ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity) - ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity) * (vendorPurchaseReturnItem.VendorPurchaseReturn.VendorPurchase.Discount / 100))) + vendorPurchaseReturnItem.VatPrice;
            //        vendorPurchaseReturnItem.Total = ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity));
            //        list.Add(vendorPurchaseReturnItem);
            //    }
            //}
            //return list;
        }
        /// <summary>
        /// IsInvoiceDate boolean field added for returndate filter in purchase report
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="IsInvoiceDate">newly added</param>
        /// <returns></returns>
        public async Task<List<VendorPurchaseReturnItem>> BuyReturnReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, bool IsInvoiceDate)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("IsInvoiceDate", IsInvoiceDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_getporeturn_rpt", parms);
            var vendorPurchaseReturn = result.Select(x =>
            {
                var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                {
                    ProductStockId = x.Productstockid,
                    Quantity = x.Quantity as decimal? ?? 0,
                    ReturnNo = x.ReturnNo.ToString(),
                    ReportInvoiceDate = " " + x.ReturnDate.ToString("dd-MM-yyyy"),
                    TinNo = Convert.ToString(x.VendorTinNo),
                    VendorPurchaseReturn =
                        {
                            Id =x.VendorReturnId.ToString(),
                            ReturnNo = x.ReturnNo.ToString(),
                            ReturnDate = Convert.ToDateTime(x.ReturnDate),
                            TaxRefNo = x.TaxRefNo as int? ?? 0,

                            VendorPurchase =
                            {
                                Vendor =
                                {
                                    Name = Convert.ToString(x.VendorName),
                                    Mobile = Convert.ToString(x.VendorMobile),
                                    Email = Convert.ToString(x.VendorEmail),
                                    Address = Convert.ToString(x.VendorAddress),
                                    TinNo =Convert.ToString(x.VendorTinNo)
                                },
                                Discount = x.VendorPurchaseDiscount as decimal? ?? 0,
                                InvoiceNo = Convert.ToString(x.InvoiceNo),
                                Instance =
                                {
                                    Name = Convert.ToString(x.InstanceName)
                                }
                            }
                        },
                    ProductStock =
                        {
                            SellingPrice = x.SellingPrice as decimal? ?? 0,
                            VAT = x.VAT as decimal? ?? 0,
                            GstTotal = x.GstTotal as decimal? ?? 0, //Added by Sarubala to include GST 06-07-2017
                            Cgst = x.Cgst as decimal? ?? 0,
                            Sgst = x.Sgst as decimal? ?? 0,
                            Igst = x.Igst as decimal? ?? 0,
                            Product = {Name = x.ProductName.ToString()}
                        },
                    ReturnPurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    Total = x.ReturnTotal as decimal? ?? 0
                };
                //vendorPurchaseReturnItem.Total = ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity) - ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity) * (vendorPurchaseReturnItem.VendorPurchaseReturn.VendorPurchase.Discount / 100))) + vendorPurchaseReturnItem.VatPrice;
                if (vendorPurchaseReturnItem.VendorPurchaseReturn.TaxRefNo == 1)
                {
                    vendorPurchaseReturnItem.ProductStock.VAT = vendorPurchaseReturnItem.ProductStock.GstTotal;
                }

                return vendorPurchaseReturnItem;
            });
            return vendorPurchaseReturn.ToList();
        }
        #endregion
        public async Task<List<TempVendorPurchaseItem>> TempStockListData(string type, string accountId, string instanceId, string productId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("ProductId", productId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetTempStockList", parms);
            var tempStockDetails = result.Select(x =>
            {
                var tempStockItem = new TempVendorPurchaseItem();

                tempStockItem.ProductStock.productName = x.Name;
                tempStockItem.ProductStock.BatchNo = x.BatchNo;
                tempStockItem.ProductStock.ExpireDate = x.ExpireDate;
                tempStockItem.ProductStock.VAT = x.VAT;
                tempStockItem.PackageSize = x.PackageSize;
                tempStockItem.PackageQty = x.PackageQty;
                tempStockItem.PackagePurchasePrice = x.PackagePurchasePrice;
                tempStockItem.PackageSellingPrice = x.PackageSellingPrice;
                tempStockItem.Action = x.Status;
                //added by nandhini
                tempStockItem.ReportExpireDate = " " + x.ExpireDate.ToString("MM-yy");
                // GST Implemented by Gavaskar 10-08-2017 Start
                tempStockItem.TaxRefNo = x.TaxRefNo;
                tempStockItem.ProductStock.GstTotal = x.GstTotal;
                if (tempStockItem.TaxRefNo == 1)
                {
                    tempStockItem.ProductStock.VAT = tempStockItem.ProductStock.GstTotal;
                }
                // GST Implemented by Gavaskar 10-08-2017 End

                return tempStockItem;
            });
            return tempStockDetails.ToList();
        }

        //added by nandhini for product list report
        public async Task<List<Product>> productListData(string type, string accountId, string instanceId)
        {

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("type", type);
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetProductList", parms);
            var tempStockDetails = result.Select(x =>
            {
                var tempStockItem = new Product();

                tempStockItem.Name = x.Name;
                tempStockItem.Manufacturer = x.Manufacturer;
                tempStockItem.GenericName = x.GenericName;
                tempStockItem.Category = x.Category;
                tempStockItem.PackageSize = Convert.ToInt32(x.PackageSize);

                return tempStockItem;
            });
            var returnVal = tempStockDetails.ToList();
            return returnVal;
        }

        //end

        public async Task<List<SalesReturnItem>> SalesReturnReportList_Old(string type, string accountId, string instanceId, DateTime from, DateTime to, int nIssummary)
        {
            ////var report = GetParameter(type, from, to);
            ////report.FromDate = report.FromDate + " 00:00:00";
            ////report.ToDate = report.ToDate + " 23:59:59";
            ////Dictionary<string, object> parms = new Dictionary<string, object>();
            ////parms.Add("InstanceId", instanceId);
            ////parms.Add("AccountId", accountId);
            ////parms.Add("StartDate", report.FromDate);
            ////parms.Add("EndDate", report.ToDate);
            ////var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesReturn", parms);
            ////var salesreturn = result.Select(x =>
            ////{
            ////    var salesReturnItem = new SalesReturnItem
            ////    {
            ////        ProductStockId = x.ProductStockId,
            ////        Quantity = (decimal)x.Quantity,
            ////        MrpSellingPrice = x.MrpSellingPrice as decimal? ?? 0,
            ////        Discount = x.Discount as decimal? ?? 0,
            ////        SalesReturn =
            ////        {
            ////            Id = x.SalesReturn.Id,
            ////            ReturnNo = x.SalesReturn.ReturnNo,
            ////            ReturnDate = Convert.ToDateTime(x.SalesReturn.ReturnDate),
            ////            Sales =
            ////            {
            ////                Name = x.Sales.Name,
            ////                Discount = x.Sales.Discount as decimal? ?? 0
            ////            },
            ////        }
            ////    };
            ////    if (x.Sales.Email != "N/A")
            ////    {
            ////        salesReturnItem.SalesReturn.Sales.Email = x.Sales.Email;
            ////    }
            ////    salesReturnItem.SalesReturn.Sales.Mobile = x.Sales.Mobile;
            ////    salesReturnItem.ProductStock.VAT = x.ProductStock.VAT as decimal? ?? 0;
            ////    salesReturnItem.ProductStock.BatchNo = x.ProductStock.BatchNo;
            ////    salesReturnItem.ProductStock.ExpireDate = Convert.ToDateTime(x.ProductStock.ExpireDate);
            ////    if (salesReturnItem.MrpSellingPrice > 0)
            ////    {
            ////        salesReturnItem.ProductStock.SellingPrice = salesReturnItem.MrpSellingPrice;
            ////    }
            ////    else
            ////    {
            ////        salesReturnItem.ProductStock.SellingPrice = x.ProductStock.SellingPrice as decimal? ?? 0;
            ////    }
            ////    salesReturnItem.ProductStock.Product.Name = x.Product.Name;
            ////    salesReturnItem.SalesRoundOff = salesReturnItem.Total - ((salesReturnItem.Total * (salesReturnItem.SalesReturn.Sales.Discount / 100)) + (salesReturnItem.Total * (salesReturnItem.Discount / 100)));
            ////    return salesReturnItem;
            ////});
            ////return salesreturn.ToList();
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesReturnItemTable.ProductStockIdColumn, SalesReturnItemTable.QuantityColumn, SalesReturnItemTable.MrpSellingPriceColumn, SalesReturnItemTable.DiscountColumn);
            qb.JoinBuilder.Join(SalesReturnTable.Table, SalesReturnTable.IdColumn, SalesReturnItemTable.SalesReturnIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesReturnTable.Table, SalesReturnTable.IdColumn, SalesReturnTable.ReturnNoColumn, SalesReturnTable.ReturnDateColumn);
            qb.JoinBuilder.Join(SalesTable.Table, SalesTable.IdColumn, SalesReturnTable.SalesIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.NameColumn, SalesTable.EmailColumn, SalesTable.MobileColumn, SalesTable.DiscountColumn);
            //qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
            //qb.JoinBuilder.AddJoinColumn(SalesItemTable.Table, SalesItemTable.SellingPriceColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(SalesReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from.ToFormat());
                qb.Parameters.Add("FilterToDate", to.ToFormat());
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(SalesReturnTable.ReturnDateColumn, CustomDateTime.IST.ToFormat());
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(SalesReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(SalesReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                }
            }
            qb.ConditionBuilder.And(SalesReturnTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesReturnTable.InstanceIdColumn, instanceId);
            //var CancelIsNull = new CustomCriteria(SalesTable.CancelstatusColumn, CriteriaCondition.IsNull);
            //qb.ConditionBuilder.And(CancelIsNull);
            qb.SelectBuilder.SortByDesc(SalesReturnTable.ReturnDateColumn);
            var list = new List<SalesReturnItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesReturnItem = new SalesReturnItem
                    {
                        ProductStockId = reader[SalesReturnItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = (decimal)reader[SalesReturnItemTable.QuantityColumn.ColumnName],
                        MrpSellingPrice = reader[SalesReturnItemTable.MrpSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[SalesReturnItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        SalesReturn =
                        {
                            Id = reader[SalesReturnTable.IdColumn.FullColumnName].ToString(),
                            ReturnNo = reader[SalesReturnTable.ReturnNoColumn.FullColumnName].ToString(),
                            ReturnDate = Convert.ToDateTime(reader[SalesReturnTable.ReturnDateColumn.FullColumnName]),
                            Sales = { Name = reader[SalesTable.NameColumn.FullColumnName].ToString(),
                            Discount = reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0 },
                            //SalesItem= {SellingPrice= reader[SalesItemTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0 }
                        }
                    };
                    if (reader[SalesTable.EmailColumn.FullColumnName].ToString() != "N/A")
                    {
                        salesReturnItem.SalesReturn.Sales.Email = reader[SalesTable.EmailColumn.FullColumnName].ToString();
                    }
                    //if (salesReturnItem.SalesReturn.SalesItem.SellingPrice > 0)
                    //{
                    //    salesItem.ProductStock.SellingPrice = salesItem.SellingPrice;
                    //    salesItem.ReportTotal = (salesItem.SellingPrice * salesItem.Quantity) -
                    //                        (salesItem.SellingPrice * salesItem.Quantity *
                    //                         (salesItem.Sales.Discount / 100));
                    //}
                    //else
                    //{
                    //    salesItem.ProductStock.SellingPrice = (decimal)reader[ProductStockTable.SellingPriceColumn.FullColumnName];
                    //    salesItem.ReportTotal = (salesItem.ProductStock.SellingPrice * salesItem.Quantity) -
                    //                        (salesItem.ProductStock.SellingPrice * salesItem.Quantity *
                    //                         (salesItem.Sales.Discount / 100));
                    //}
                    //salesReturnItem.Discount = reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0;
                    salesReturnItem.SalesReturn.Sales.Mobile = reader[SalesTable.MobileColumn.FullColumnName].ToString();
                    salesReturnItem.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;
                    salesReturnItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    salesReturnItem.ProductStock.ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName]);
                    if (salesReturnItem.MrpSellingPrice > 0)
                    {
                        salesReturnItem.ProductStock.SellingPrice = salesReturnItem.MrpSellingPrice;
                    }
                    else
                    {
                        salesReturnItem.ProductStock.SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0;
                    }
                    salesReturnItem.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    salesReturnItem.SalesRoundOff = salesReturnItem.Total - ((salesReturnItem.Total * (salesReturnItem.SalesReturn.Sales.Discount / 100)) + (salesReturnItem.Total * (salesReturnItem.Discount / 100)));
                    //salesReturnItem.SalesRoundOff = Math.Round(decimal.Parse(String.Format("{0:0.##}", salesReturnItem.SalesRoundOff)), MidpointRounding.AwayFromZero);
                    //salesReturnItem.SalesRoundOff = Math.Round((decimal)salesReturnItem.SalesRoundOff);
                    list.Add(salesReturnItem);
                }
            }
            return list;
        }
        // Direct query removed and modified to get the data from SP by poongodi
        public async Task<List<SalesReturnItem>> SalesReturnReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, int nIssummary, int fromvalues, int tovalues, int SearchType, string kind)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("IsSummary", nIssummary);
            parms.Add("FromReturnNo", fromvalues); // ReturnNo filter added by Violet - 15-06-2017
            parms.Add("ToReturnNo", tovalues);
            parms.Add("SearchType", SearchType);
            parms.Add("Kind", kind);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesReturn", parms);
            /*Instance Name added by Poongodi on 14/09/2017*/
            var salesreturn = result.Select(x =>
            {
                var salesReturnItem = new SalesReturnItem
                {
                    ProductStockId = x.ProductStockId,
                    Quantity = x.Quantity as decimal? ?? 0,
                    MrpSellingPrice = x.MrpSellingPrice as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    TotalWOvat = x.WithoutVAT,
                    vatValue = x.VatValue,
                    DiscountAmount = x.DiscountAmount as decimal? ?? 0,

                    SalesReturn =
                    {
                        ReturnCharges = x.ReturnCharges as decimal? ?? 0,
                        Id = Convert.ToString(x.SalesReturnId),
                        ReturnNo =  Convert.ToString(x.ReturnNo),
                        ReturnDate = Convert.ToDateTime(x.ReturnDate),
                        ReportReturnDate = " "+ x.ReturnDate.ToString("dd-MM-yyyy"),
                         Select1 = Convert.ToString(x.Branch),
                         ReturnTime= x.ReturnTime.ToString("hh:mm tt"),

                        Sales =
                        {
                            Name =  Convert.ToString(x.SalesName),
                            Discount = x.SalesDiscount as decimal? ?? 0,
                           InvoiceNo = x.BillNo,
                           InvoiceDate=Convert.ToDateTime(x.InvoiceDate),
                        },
                        Instance =
                        {
                            Name =x.Branch,
                        },
                        HQueUser=
                        {
                            Name=x.UserName,
                        }
                    }
                };
                if (Convert.ToString(x.SalesEmail) != "N/A")
                {
                    salesReturnItem.SalesReturn.Sales.Email = Convert.ToString(x.SalesEmail);
                }
                salesReturnItem.SalesReturn.Sales.Mobile = Convert.ToString(x.SalesMobile);
                //salesReturnItem.SalesReturn.ReturnCharges = x.ReturnCharge as decimal? ?? 0; // Added by Sarubala on 13-10-17
                salesReturnItem.ProductStock.VAT = x.VAT as decimal? ?? 0;
                salesReturnItem.ProductStock.BatchNo = Convert.ToString(x.BatchNo);
                salesReturnItem.ProductStock.ExpireDate = Convert.ToDateTime(x.ExpireDate);
                //if (salesReturnItem.MrpSellingPrice > 0)
                //{
                //    salesReturnItem.ProductStock.SellingPrice = salesReturnItem.MrpSellingPrice;
                //}
                //else
                //{
                //    salesReturnItem.ProductStock.SellingPrice = x.ProductStockSellingPrice as decimal? ?? 0;
                //}
                salesReturnItem.ProductStock.SellingPrice = salesReturnItem.MrpSellingPrice;
                salesReturnItem.ProductStock.Product.Name = Convert.ToString(x.ProductName);
                salesReturnItem.ProductStock.productName = Convert.ToString(x.ProductName);
                salesReturnItem.ProductStock.Product.HsnCode = Convert.ToString(x.HsnCode);
                salesReturnItem.ProductStock.Product.KindName = Convert.ToString(x.KindName);
                //salesReturnItem.SalesRoundOff =  salesReturnItem.Total - ((salesReturnItem.Total * (salesReturnItem.SalesReturn.Sales.Discount / 100)) + (salesReturnItem.Total * (salesReturnItem.Discount / 100)));
                salesReturnItem.SalesRoundOff = x.ReturnValue as decimal? ?? 0;
                return salesReturnItem;
            });
            return salesreturn.ToList();
        }
        public async Task<List<Sales>> HomeDeliveryList1(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.IdColumn, SalesTable.InvoiceNoColumn, SalesTable.InvoiceDateColumn,
                SalesTable.NameColumn, SalesTable.EmailColumn, SalesTable.MobileColumn, SalesTable.DeliveryTypeColumn, SalesTable.PaymentTypeColumn);
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from.ToFormat());
                qb.Parameters.Add("FilterToDate", to.ToFormat());
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", CustomDateTime.IST.ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                }
            }
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);
            return await List<Sales>(qb);
        }
        // Done Gavaskar 24-12-2016
        public async Task<List<Leads>> LeadsReportList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var reportparam = GetParameter(type, from, to);
            var sql = @"SELECT LeadDate,Name,DoctorName,Mobile,LeadStatus FROM Leads WITH(NOLOCK) 
                        WHERE InstanceId = @InstanceID
                        AND AccountId = @AccountID and  LeadDate BETWEEN @StartDate and @EndDate
                        ORDER BY CreatedAt DESC";
            var result = await sqldb.ExecuteQueryAsync<Leads>(sql, new { InstanceID = instanceId, AccountID = accountId, StartDate = reportparam.FromDate, EndDate = reportparam.ToDate });
            return result.ToList();
        }
        // Done Gavaskar 24-12-2016
        // BoxNo, RackNo, VendorName & Current Stock column added by Settu on 27-05-2017
        public async Task<List<SalesItem>> DrugWiseList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceSeries, int fromInvoice, int toInvoice, string productVal)
        {
            var reportparam = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);
            parms.Add("StartDate", reportparam.FromDate);
            parms.Add("EndDate", reportparam.ToDate);
            parms.Add("InvoiceSeries", invoiceSeries);
            parms.Add("FromInvoice", fromInvoice);
            parms.Add("ToInvoice", toInvoice);
            parms.Add("productVal", productVal);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_DrugWiseReportList", parms);
            var saleItems = result.Select(x =>
            {
                var item = new SalesItem
                {
                    ProductStock = {
                        Product = {
                            Name = x.Name,
                            RackNo = x.RackNo,
                            BoxNo = x.BoxNo,
                        },
                        Vendor =
                        {
                            Name = x.VendorName,
                        },
                        Stock = x.Stock,
                        InstanceName = x.InstanceName,
                    },
                    Quantity = (decimal)x.Quantity
                };
                return item;
            });
            return saleItems.ToList();
        }

        // Done Gavaskar 24-12-2016
        //Stock Type parameter added by Poongodi on 13/03/2017
        //Modified
        public async Task<List<ProductStock>> StockReportList(string instanceId, string accountId, string sStockType, bool isProductWise, string category, string manufacturer, string sRackNo, string sBoxNo, string alphabet1, string alphabet2, DateTime from, DateTime to)
        {
            //Accountid, Box and RAck number parameter added by Poongodi on 21/07/2017
            string fromDate = "";
            string toDate = "";
            if (from.ToFormat() == "01-Jan-0001" && to.ToFormat() == "01-Jan-0001")
            {
                fromDate = "";
                toDate = "";
            }
            else
            {
                // Need to check here at run time
                fromDate = from.ToString();
                toDate = to.ToString();
            }
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("accountId", accountId);

            parms.Add("StockType", sStockType);
            parms.Add("IsProductWise", isProductWise);
            parms.Add("Category", category);
            parms.Add("Manufacturer", manufacturer);
            parms.Add("RackNo", sRackNo);
            parms.Add("Boxno", sBoxNo);
            parms.Add("Alphabet1", alphabet1);
            parms.Add("Alphabet2", alphabet2);
            parms.Add("fromDate", fromDate);
            parms.Add("toDate", toDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStockReportList", parms);
            /*Gsttotal,Rack Number and Box Number added by Poongodi on 07/07/2017*/
            var products = result.Select(x =>
            {
                var productStock = new ProductStock
                {

                    Product = {
                        Name = x.Name,
                        GenericName = x.GenericName,
                        Category = x.Category,
                        Manufacturer = x.Manufacturer,
                        Schedule = x.Schedule,
                        Type = x.Type,
                        DisplayStatus=x.Status,
                        RackNo =x.RackNo,
                        BoxNo =x.BoxNo,

                    },


                    //ProductInstance =
                    //{
                    //    RackNo=x.RackNo,
                    //    BoxNo=x.BoxNo,
                    //},
                    Vendor = { Name = x.Vendor },
                    BatchNo = x.BatchNo,
                    Stock = Convert.ToDecimal(x.Quantity),
                    ExpireDate = Convert.ToDateTime(x.ExpireDate),
                    VAT = Convert.ToDecimal(x.VAT),
                    CostPrice = Convert.ToDecimal(x.CostPrice),
                    CostPriceGst = Convert.ToDecimal(x.CostPriceGst),
                    MRP = Convert.ToDecimal(x.MRP),
                    GstTotal = Convert.ToDecimal(x.GST),
                    //added by nandhini for csv 
                    StatusText = x.Status,
                    ReportExpireDate = " " + Convert.ToString(x.ExpireDate),
                    //end

                };
                //decimal? costPriceWithoutVAT = 0;
                //if (productStock.VAT != null)
                //{
                //    costPriceWithoutVAT = productStock.CostPrice / (1 + (productStock.VAT / 100));
                //}
                //if (productStock.GstTotal != null)
                //{
                //    productStock.CostPriceGst = costPriceWithoutVAT * (1 + (productStock.GstTotal / 100));
                //}

                return productStock;
            });
            return products.ToList();
        }

        //Created by Manivannan on 11-July-2017
        public async Task<List<Instance>> StockAllBranchConsolidatedList(string accountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStockAllBranchReportList", parms);

            var instances = result.Select(x =>
            {
                var instance = new Instance
                {
                    Name = x.Name,
                    StockCount = Convert.ToDecimal(x.StockCount),
                    StockValue = Convert.ToDecimal(x.StockValue),
                    StockValueGst = Convert.ToDecimal(x.StockValueGst),

                };
                instance.Name = instance.Name.Trim();
                if (instance.Name.EndsWith(","))
                    instance.Name = instance.Name.Substring(0, instance.Name.Length - 1);
                return instance;
            });
            var returnValue = instances.ToList();
            return returnValue;
        }

        // Done Gavaskar 24-12-2016
        public async Task<List<ProductStock>> StockReportCategoryList(string instanceId, string sStockType, string category)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            //temp 
            var date = string.Empty;
            parms.Add("InstanceId", instanceId);
            parms.Add("StockType", sStockType);
            parms.Add("Category", category);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStockReportCategoryList", parms);
            var products = result.Select(x =>
            {
                var productStock = new ProductStock
                {
                    Product = {
                        //Name = x.Name,
                        //GenericName = x.GenericName,
                        Category = x.Category
                        //,
                        //Schedule = x.Schedule,
                        //Type = x.Type,
                        //RackNo =x.Status,
                    }
                };
                return productStock;
            });
            return products.ToList();
        }

        public async Task<List<ProductStock>> StockReportManufacturerList(string instanceId, string sStockType, string manufacturer)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            var date = string.Empty;
            parms.Add("InstanceId", instanceId);
            parms.Add("StockType", sStockType);
            parms.Add("Manufacturer", manufacturer);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStockReportManufacturerList", parms);
            var products = result.Select(x =>
            {
                var productStock = new ProductStock
                {
                    Product = {
                        Manufacturer = x.Manufacturer
                    }
                };
                return productStock;
            });
            return products.ToList();
        }

        public async Task<List<ProductStock>> StockProductListByLocation(string instanceId, string sStockType, string sSearchText, string sSearchType)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            var date = string.Empty;
            parms.Add("InstanceId", instanceId);
            parms.Add("StockType", sStockType);
            parms.Add("SearchType", sSearchType);
            parms.Add("SearchText", sSearchText);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetProductStock_Location_List", parms);
            var products = result.Select(x =>
            {
                var productStock = new ProductStock
                {
                    Product = {
                        BoxNo = x.BoxNo,
                        RackNo =x.RackNo
                    }
                };
                return productStock;
            });
            return products.ToList();
        }


        public async Task<List<ProductStock>> NonMovingStockReportList(string instanceId, DateTime from, DateTime to) //int stockMonth 
        {
            bool gstEnabled = _configHelper.AppConfig.IsGstEnabled;
            string fromDate = "";
            string toDate = "";
            if (from.ToFormat() == "01-Jan-0001" && to.ToFormat() == "01-Jan-0001")
            {
                fromDate = "";
                toDate = "";
            }
            else
            {
                fromDate = from.ToString("yyyy-MM-dd");
                toDate = to.ToString("yyyy-MM-dd");
            }
            //int getStockMonth = 0;
            //if (stockMonth == 0)
            //{
            //    getStockMonth = 6;
            //}
            //else
            //{
            //    getStockMonth = stockMonth;
            //}
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("from", fromDate);
            parms.Add("to", toDate);
            //parms.Add("Month", getStockMonth);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetNonMovingStockReportListNotBuySell", parms); //usp_GetNonMovingStockReportList
            var products = result.Select(x =>
            {
                var productStock = new ProductStock()
                {
                    Product = {
                            Name = x.ProductName ,
                            Category = x.Category as System.String ?? "",
                            Schedule =  x.Schedule,
                            Type = x.Type,
                        },
                    Vendor = {
                            Name = x.VendorName
                        },
                    BatchNo = x.BatchNo,
                    Stock = x.Quantity as decimal? ?? 0,
                    ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                    //added by nandhini for csv
                    ReportExpireDate = " " + x.ExpireDate.ToString("MM-yy"),
                    ReportInvoiceDate = " " + x.PurchaseDate.ToString("dd-MM-yyyy"),
                    VendorPurchase = {
                            InvoiceDate = x.PurchaseDate  as DateTime? ?? DateTime.MinValue
                        },
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    VAT = x.VAT as decimal? ?? 0,
                    GstTotal = x.GstTotal as decimal? ?? 0,
                    Cgst = x.Cgst as decimal? ?? 0,
                    Sgst = x.Sgst as decimal? ?? 0,
                    Igst = x.Igst as decimal? ?? 0,
                    CostPrice = x.CostPrice as decimal? ?? 0,
                    MRP = x.MRP as decimal? ?? 0,
                    InstanceId = instanceId,
                    Id = x.Id,
                    idchk = x.Id,
                    Age = x.Age as int? ?? 0,
                    LastSaleDate = x.LastSaleDate,
                    GSTEnabled = gstEnabled
                };
                if (gstEnabled == true)
                {
                    productStock.VAT = productStock.GstTotal;
                }
                return productStock;
            });
            return products.ToList();
        }
        public async Task<List<ProductStock>> UpdateIsMovingStock(List<ProductStock> productStocks)
        {
            //UpdateIsMovingStockEmpty();
            // var qbupdate = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            //// qbupdate.ConditionBuilder.And(ProductStockTable.IsMovingStockColumn, 1);
            // qbupdate.AddColumn(ProductStockTable.IsMovingStockColumn);
            // qbupdate.Parameters.Add(ProductStockTable.IsMovingStockColumn.ColumnName, false);
            // await QueryExecuter.NonQueryAsyc(qbupdate);
            var query = $@"Update ProductStock set IsMovingStock=0 WHERE IsMovingStock=1 and  instanceId = '{productStocks.First().InstanceId}'";
            var qb1 = QueryBuilderFactory.GetQueryBuilder(query);
            await QueryExecuter.NonQueryAsyc(qb1);
            foreach (var productStock in productStocks)
            {
                if (productStock.IsMovingStock != null)
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                    qb.ConditionBuilder.And(ProductStockTable.IdColumn, productStock.idchk);
                    qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, productStock.InstanceId);
                    qb.AddColumn(ProductStockTable.IsMovingStockColumn, ProductStockTable.UpdatedAtColumn);
                    qb.Parameters.Add(ProductStockTable.IsMovingStockColumn.ColumnName, productStock.IsMovingStock);
                    qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, productStock.UpdatedAt = CustomDateTime.IST);
                    await QueryExecuter.NonQueryAsyc(qb);
                }
            }
            return productStocks;
        }
        //Newly Added Gavaskar 25-10-2016 Start
        //public void UpdateIsMovingStockEmpty()
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
        //    qb.AddColumn(ProductStockTable.IsMovingStockColumn);
        //    qb.Parameters.Add(ProductStockTable.IsMovingStockColumn.ColumnName, false);
        //    QueryExecuter.NonQueryAsyc(qb);
        //}
        //public void UpdateIsMovingStockExpireEmpty()
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
        //    qb.AddColumn(ProductStockTable.IsMovingStockExpireColumn);
        //    qb.Parameters.Add(ProductStockTable.IsMovingStockExpireColumn.ColumnName, false);
        //    QueryExecuter.NonQueryAsyc(qb);
        //}
        public Task<ProductStock[]> vendorExpireUpdate(List<ProductStock> productStocks)
        {
            var stocks = productStocks.Select(productStock => { return updateExpireVendor(productStock); });
            return Task.WhenAll(stocks);
        }
        private async Task<ProductStock> updateExpireVendor(ProductStock productStock)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.VendorIdColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn, productStock.idchk);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, productStock.InstanceId);
            qb.ConditionBuilder.And(ProductStockTable.IsMovingStockExpireColumn, true);
            qb.Parameters.Add(ProductStockTable.VendorIdColumn.ColumnName, productStock.VendorId);
            await QueryExecuter.NonQueryAsyc(qb);
            return productStock;
        }
        public Task<ProductStock[]> vendorNonMovingUpdate(List<ProductStock> productStocks)
        {
            var stocks = productStocks.Select(productStock => { return updateNonMovingVendor(productStock); });
            return Task.WhenAll(stocks);
        }
        private async Task<ProductStock> updateNonMovingVendor(ProductStock productStock)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.VendorIdColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn, productStock.idchk);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, productStock.InstanceId);
            qb.ConditionBuilder.And(ProductStockTable.IsMovingStockColumn, true);
            qb.Parameters.Add(ProductStockTable.VendorIdColumn.ColumnName, productStock.VendorId);
            await QueryExecuter.NonQueryAsyc(qb);
            return productStock;
        }
        public async Task<List<ProductStock>> UpdateIsMovingStockExpire(List<ProductStock> productStocks)
        {
            //UpdateIsMovingStockExpireEmpty();
            //foreach (var productStockListUpdate in productStocks)
            //{
            //    var qbupdate = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            //    qbupdate.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, productStockListUpdate.InstanceId);
            //    qbupdate.ConditionBuilder.And(ProductStockTable.AccountIdColumn, productStockListUpdate.AccountId);
            //    // qbupdate.ConditionBuilder.And(ProductStockTable.IsMovingStockExpireColumn, 1);
            //    qbupdate.AddColumn(ProductStockTable.IsMovingStockExpireColumn);
            //    qbupdate.Parameters.Add(ProductStockTable.IsMovingStockExpireColumn.ColumnName, false);
            //    await QueryExecuter.NonQueryAsyc(qbupdate);
            //}
            var query = $@"Update ProductStock set IsMovingStockExpire=0 WHERE IsMovingStockExpire=1 and  instanceId = '{productStocks.First().InstanceId}'";
            var qb1 = QueryBuilderFactory.GetQueryBuilder(query);
            await QueryExecuter.NonQueryAsyc(qb1);
            foreach (var productStock in productStocks)
            {
                if (productStock.IsMovingStockExpire != null)
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                    qb.ConditionBuilder.And(ProductStockTable.IdColumn, productStock.idchk);
                    qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, productStock.InstanceId);
                    qb.AddColumn(ProductStockTable.IsMovingStockExpireColumn, ProductStockTable.UpdatedAtColumn);
                    qb.Parameters.Add(ProductStockTable.IsMovingStockExpireColumn.ColumnName, productStock.IsMovingStockExpire);
                    qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, productStock.UpdatedAt = CustomDateTime.IST);
                    await QueryExecuter.NonQueryAsyc(qb);
                }
            }
            return productStocks;
        }
        //Newly Added Gavaskar 25-10-2016 End
        // Done Gavaskar 24-12-2016
        public async Task<List<SalesItem>> SalesUserList(SalesItem data, string accountId, string instanceId)
        {
            var type = "USERWISE";
            var UserName = string.Empty;
            var UserId = string.Empty;
            var fromDate = Convert.ToDateTime(data.Sales.Select);
            var toDate = Convert.ToDateTime(data.Sales.Select1);

            if (data.Sales.SelectValue == null)
            {
                data.Sales.SelectValue = string.Empty;
            }
            if (data.Sales.SelectValue.ToLower() == "username")
            {
                UserName = data.Sales.Name;
            }
            else if (data.Sales.SelectValue.ToLower() == "userid")
            {
                UserId = data.Sales.Name;
            }
            if (data.Sales.Values == "Summary")
            {
                return await SalesSummaryCustomerwiseList(null, null, accountId, instanceId, fromDate, toDate, null, UserName, UserId, type);
            }
            else if (data.Sales.Values == "Invoice")
            {
                return await SalesCustomerWiseList(null, null, accountId, instanceId, fromDate, toDate, UserName, UserId, type);
            }
            else if (data.Sales.Values == "Detail")
            {
                return await SalesReportCustomerDetailList(null, null, accountId, instanceId, fromDate, toDate, null, UserName, UserId);
            }
            else
            {
                return null;
            }

            //Dictionary<string, object> parms = new Dictionary<string, object>();
            //parms.Add("InstanceId", instanceId);
            //parms.Add("AccountId", accountId);
            //parms.Add("StartDate", data.Sales.Select);
            //parms.Add("EndDate", data.Sales.Select1);
            //parms.Add("SearchOption", data.Sales.SelectValue);
            //parms.Add("SearchValue", data.Sales.Name);
            //parms.Add("Mobile", data.Sales.Mobile);
            //parms.Add("FilterValue", data.Sales.Values);
            ////var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesUserList", parms);
            //var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesUserWiseList", parms);
            //var salesItems = result.Select(x =>
            //{
            //    var salesItem = new SalesItem();
            //    if (data.Sales.Values == "Summary")
            //    {
            //        salesItem.Sales.InvoiceDate = x.SalesDate;
            //        salesItem.Sales.HQueUser.Name = x.Name;
            //        salesItem.Sales.TotalSales = (int)x.TotalSales;
            //        salesItem.Sales.InvoiceAmount = (decimal)x.TotalPurchase;
            //        salesItem.Sales.FinalValue = (decimal)x.TotalMRP;
            //        decimal? totalDiscount = x.TotalDiscount as decimal? ?? 0;
            //        salesItem.Sales.FinalValue = Math.Round((decimal)(salesItem.Sales.FinalValue - totalDiscount), 0, MidpointRounding.AwayFromZero);
            //        salesItem.Sales.Profit = salesItem.Sales.FinalValue - salesItem.Sales.InvoiceAmount;
            //        salesItem.Sales.Profit = Math.Round((decimal)salesItem.Sales.Profit, 0, MidpointRounding.AwayFromZero);
            //        salesItem.Sales.Instance.Name = x.InstanceName;
            //    }
            //    else if (data.Sales.Values == "Invoice")
            //    {
            //        salesItem.Sales.InvoiceNo = x.InvoiceNo;
            //        salesItem.Sales.HQueUser.Name = x.Name;
            //        salesItem.Sales.Name = x.PatientName;
            //        salesItem.Sales.InvoiceDate = x.InvoiceDate;
            //        salesItem.Sales.InvoiceSeries = x.InvoiceSeries;
            //        salesItem.Sales.ActualInvoice = x.InvoiceSeries + " " + x.InvoiceNo;
            //        salesItem.Sales.Discount = x.Discount;
            //        //item and wise discount issue fixed
            //        //decimal? discountSum = 0; //x.DiscountSum;
            //        salesItem.Sales.InvoiceAmount = x.InvoiceAmount;
            //        salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)); //- discountSum;
            //        salesItem.Sales.FinalValue = Math.Round(decimal.Parse(String.Format("{0:0.##}", salesItem.Sales.FinalValue)), MidpointRounding.AwayFromZero);
            //        salesItem.Sales.Instance.Name = x.InstanceName;
            //    }
            //    else if (data.Sales.Values == "Detail")
            //    {
            //        salesItem.Sales.InvoiceNo = x.InvoiceNo;
            //        salesItem.Sales.HQueUser.Name = x.Name;
            //        salesItem.Sales.Name = x.PatientName;
            //        salesItem.Sales.InvoiceDate = x.InvoiceDate;
            //        salesItem.Sales.InvoiceSeries = x.InvoiceSeries;
            //        salesItem.Sales.ActualInvoice = x.InvoiceSeries + " " + x.InvoiceNo;
            //        salesItem.Sales.DoctorName = string.IsNullOrEmpty(x.DoctorName) ? "" : x.DoctorName.ToString();
            //        salesItem.Sales.Address = string.IsNullOrEmpty(x.Address) ? "" : x.Address.ToString();
            //        salesItem.ProductStock.BatchNo = x.BatchNo;
            //        salesItem.ProductStock.ExpireDate = x.ExpireDate;
            //        salesItem.ProductStock.Stock = x.Stock;
            //        salesItem.ProductStock.VAT = x.VAT;
            //        salesItem.ProductStock.PurchasePrice = x.PurchasePrice;
            //        salesItem.ProductStock.CostPrice = x.CostPrice;
            //        salesItem.ProductStock.Product.Name = string.IsNullOrEmpty(x.ProductName) ? "" : x.ProductName.ToString();
            //        salesItem.ProductStock.Product.Manufacturer = string.IsNullOrEmpty(x.Manufacturer) ? "" : x.Manufacturer.ToString();
            //        salesItem.ProductStock.Product.Schedule = string.IsNullOrEmpty(x.Schedule) ? "" : x.Schedule.ToString();
            //        salesItem.ProductStock.Product.Type = string.IsNullOrEmpty(x.Type) ? "" : x.Type.ToString();
            //        salesItem.SellingPrice = x.SellingPrice;
            //        salesItem.Quantity = x.Quantity;
            //        salesItem.Discount = x.Discount;
            //        salesItem.Sales.Discount = x.SalesDiscount;
            //        salesItem.Sales.Credit = x.Credit;
            //        decimal? discountSum = x.DiscountSum;
            //        salesItem.Sales.InvoiceAmount = x.InvoiceAmount;
            //        salesItem.ProductStock.TotalCostPrice = salesItem.SellingPrice * salesItem.Quantity;
            //        //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
            //        salesItem.Sales.FinalValue = x.FinalValue;
            //        salesItem.Sales.TaxAmount = x.Tax;
            //        salesItem.Sales.GrossSaleWithoutTax = (decimal)x.AmountwithoutTax;
            //        salesItem.ReportTotal = salesItem.Sales.FinalValue = Math.Round((decimal)(salesItem.Sales.FinalValue), 0, MidpointRounding.AwayFromZero);
            //        //salesItem.ProductStock.TotalCostPrice = salesItem.ProductStock.PurchasePrice * salesItem.Quantity;
            //        salesItem.ProductStock.TotalCostPrice = decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.PurchasePrice * salesItem.Quantity));
            //        salesItem.Sales.Instance.Name = x.InstanceName;
            //        salesItem.Discount = x.DiscountSum; //User Wise bill discount not showing issue fixed 
            //        if (salesItem.ProductStock.TotalCostPrice > 0)
            //        {
            //            salesItem.SalesProfit = ((decimal.Parse(String.Format("{0:0.##}", salesItem.ReportTotal)) - decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) / decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) * 100;
            //        }
            //        else
            //        {
            //            salesItem.SalesProfit = 0;
            //        }
            //    }
            //    return salesItem;
            //});
            //return salesItems.ToList();
        }
        // Done Gavaskar 24-12-2016
        public async Task<List<CommunicationLog>> SmsUserList(string accountId, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSmsUserList", parms);
            var communicationList = result.Select(x =>
            {
                var communicationLog = new CommunicationLog();
                communicationLog.HQueUser.Name = x.Name;
                communicationLog.TotalSms = (int)x.TotalSms;
                return communicationLog;
            });
            return communicationList.ToList();
        }
        // Done Gavaskar 24-12-2016
        public async Task<List<CommunicationLog>> SmsBranchList(string type, object accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSmsBranchList", parms);
            var communicationList = result.Select(x =>
            {
                var communicationLog = new CommunicationLog();
                communicationLog.CreatedAt = (DateTime)x.SmsDate;
                communicationLog.TotalSms = (int)x.TotalSms;
                return communicationLog;
            });
            return communicationList.ToList();
        }
        public async Task<List<StockLedger>> GetStockLedger(StockLedger data, string AccId, string InsId)
        {
            var filterFromDate = data.FromDate.ToFormat();
            var filterToDate = data.ToDate.ToFormat();
            StockLedger ledger = new StockLedger();
            Dictionary<string, object> SaleParms = new Dictionary<string, object>();
            SaleParms.Add("InstanceId", InsId);
            SaleParms.Add("AccountId", AccId);
            SaleParms.Add("StartDate", filterFromDate);
            SaleParms.Add("EndDate", filterToDate);
            SaleParms.Add("ProductId", data.SelectedProduct.Id);
            var stockResult = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStockLedger", SaleParms);
            var stockLedgerList = stockResult.Select(x =>
            {
                var stockLedger = new StockLedger
                {
                    TransactionType = (Transactions)x.TransactionType,
                    TransactionTypeName = ((Transactions)x.TransactionType).ToString(),
                    TransactionId = string.IsNullOrEmpty(x.TransactionId) ? "" : x.TransactionId.ToString(),
                    TransactionDate = x.TransactionDate as DateTime? ?? DateTime.MinValue,
                    TransactionNo = string.IsNullOrEmpty(x.TransactionNo) ? "" : x.TransactionNo.ToString(),
                    CustomerName = string.IsNullOrEmpty(x.Name) ? "" : x.Name.ToString(),
                    Quantity = x.Quantity as decimal? ?? 0,
                    BatchNo = string.IsNullOrEmpty(x.BatchNo) ? "" : x.BatchNo.ToString(),
                    ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                    ToInstance = string.IsNullOrEmpty(x.ToInstance) ? "" : x.ToInstance.ToString(),
                    GoodsRcvNo = string.IsNullOrEmpty(x.GoodsRcvNo) ? "" : x.GoodsRcvNo.ToString(),
                    BillSeries = string.IsNullOrEmpty(x.BillSeries) ? "" : x.BillSeries.ToString(),
                    StockType = x.StockType as int? ?? 0,
                    Stock = x.Stock as decimal? ?? 0,
                    OpeningStock1 = x.OpeningStock1 as decimal? ?? 0

                };
                return stockLedger;
            });
            return stockLedgerList.ToList();
        }
        public async Task<List<StockAdjustment>> GetStockAdjustmentReport(StockAdjustment sa, string type)
        {
            var report = GetParameter(type, sa.Dates.FromDate, sa.Dates.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", sa.InstanceId);
            parms.Add("AccountId", sa.AccountId);
            /*Null assignment removed by Poongodi on 19/06/2017*/
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            string searchon = null, productId = null, batch = null, val = null;

            //if (sa.select == null && sa.select1 == null)
            //{
            //    report = GetParameter(type, sa.Dates.FromDate, sa.Dates.ToDate);
            //    report.FromDate = report.FromDate + " 00:00:00";
            //    report.ToDate = report.ToDate + " 23:59:59";
            //}
            if (sa.select1 != null)
            {
                searchon = sa.select;
                val = sa.selectedValue;
            }
            else
            {
                if (sa.select == "Product")
                {
                    productId = sa.selectedValue;
                }
                else if (sa.select == "Batch")
                {
                    batch = sa.selectedValue;
                }
            }
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("ProdId", productId);
            parms.Add("Batch", batch);
            parms.Add("SearchOn", searchon);
            parms.Add("Value", val);
            parms.Add("Operator", sa.select1);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_StockAdjustmentReport", parms);
            var stockAdjust = result.Select(x =>
            {
                var stockAdjust1 = new StockAdjustment
                {
                    Id = x.Id.ToString(),
                    AdjustedStock = x.AdjustedStock as decimal? ?? 0,
                    PreviousStockValue = x.StockBeforeAdjust as decimal? ?? 0,
                    AdjustedPerson = x.AdjustedName.ToString(),
                    CreatedAt = Convert.ToDateTime(x.CreatedAt),
                    //added by nandhini for csv
                    ReportExpireDate = " " + x.ExpireDate.ToString("dd-MM-yyyy"),
                    ReportCreatedAt = " " + x.CreatedAt.ToString("dd-MM-yyyy"),
                    select = x.BatchNo,
                    Total = x.Total as decimal? ?? 0,
                    ProductStock = new ProductStock
                    {
                        BatchNo = x.BatchNo,
                        ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        // VAT = x.VAT as decimal? ?? 0, // Commented by Gavaskar 11-08-2017 VAT
                        VAT = x.GSTVAT as decimal? ?? 0, //GST Implemented by Gavaskar 11-08-2017 
                        PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                        Stock = x.Stock as decimal? ?? 0,
                        productName = x.Name.ToString()
                    }
                };
                if (stockAdjust1.AdjustedStock < 0)
                {
                    stockAdjust1.Total = stockAdjust1.Total * (-1);
                }
                return stockAdjust1;
            });
            return stockAdjust.ToList();
        }
        public async Task<List<StockValueReport>> GetStockValueReport(string accId, string insId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", insId);
            parms.Add("AccountId", accId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_StockValueReport", parms);
            var stockValueList = result.Select(x =>
            {
                var stockValue = new StockValueReport
                {
                    Category = x.Category.ToString(),
                    ProductCount = x.ProductCount as decimal? ?? 0,
                    TotalPurchasePrice = x.PriceTotal as decimal? ?? 0,
                    TotalMrp = x.MrpTotal as decimal? ?? 0,
                    Profit = x.ProfitPercentage as decimal? ?? 0,
                    Vat0PurchasePrice = x.Vat0_PriceTotal as decimal? ?? 0,
                    Vat145PurchasePrice = x.Vat145_PriceTotal as decimal? ?? 0,
                    Vat5PurchasePrice = x.Vat5_PriceTotal as decimal? ?? 0,
                    VatOtherPurchasePrice = x.VatOther_PriceTotal as decimal? ?? 0,
                    Gst0PurchasePrice = x.Gst0_PriceTotal as decimal? ?? 0,
                    Gst5PurchasePrice = x.Gst5_PriceTotal as decimal? ?? 0,
                    Gst12PurchasePrice = x.Gst12_PriceTotal as decimal? ?? 0,
                    Gst18PurchasePrice = x.Gst18_PriceTotal as decimal? ?? 0,
                    Gst28PurchasePrice = x.Gst28_PriceTotal as decimal? ?? 0,
                    GstOtherPurchasePrice = x.GstOther_PriceTotal as decimal? ?? 0

                };
                return stockValue;
            });
            return stockValueList.ToList();
        }
        public async Task<List<StockValueReport>> GetSavedReport(StockValueReport svr)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", svr.InstanceId);
            parms.Add("AccountId", svr.AccountId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStockValueReportList", parms);
            var stockValueList = result.Select(x =>
            {
                var stockValue = new StockValueReport
                {
                    ReportNo = x.ReportNo.ToString(),
                    CreatedAt = Convert.ToDateTime(x.CreatedAt)
                };
                return stockValue;
            });
            return stockValueList.ToList();
        }
        public async Task<List<StockValueReport>> SaveStockValueReport(List<StockValueReport> svlist)
        {
            string reportNo = await GetStockValueReportNo(svlist.First().InstanceId);
            foreach (StockValueReport svr in svlist)
            {
                svr.InstanceId = svlist.First().InstanceId;
                svr.AccountId = svlist.First().AccountId;
                svr.ReportNo = reportNo;
                svr.CreatedAt = DateTime.UtcNow;
                svr.UpdatedAt = DateTime.UtcNow;
                svr.CreatedBy = svlist.First().CreatedBy;
                svr.UpdatedBy = svlist.First().UpdatedBy;
                await Insert(svr, StockValueReportTable.Table);
            }
            return svlist;
        }
        private async Task<string> GetStockValueReportNo(string instanceId)
        {
            var query = _configHelper.AppConfig.IsSqlServer
                ? $"SELECT ISNULL(MAX(CONVERT(INT, ReportNo)),0) + 1 AS ReportNo FROM StockValueReport WHERE InstanceId = '{instanceId}'"
                : $"SELECT IFNULL(MAX(CAST(ReportNo as INT)),0) + 1 AS ReportNo FROM StockValueReport WHERE InstanceId = '{instanceId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await QueryExecuter.SingleValueAsyc(qb)).ToString();
        }
        public async Task<List<StockValueReport>> GetUniqueSavedReport(string reportNo, string insId, string accId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", insId);
            parms.Add("AccountId", accId);
            parms.Add("ReportNo", reportNo);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetUniqueStockValueReport", parms);
            var stockValueList = result.Select(x =>
            {
                var stockValue = new StockValueReport
                {
                    Category = x.Category.ToString(),
                    ProductCount = x.ProductCount as decimal? ?? 0,
                    TotalPurchasePrice = x.TotalPurchasePrice as decimal? ?? 0,
                    TotalMrp = x.TotalMrp as decimal? ?? 0,
                    Profit = x.Profit as decimal? ?? 0,
                    Vat0PurchasePrice = x.Vat0PurchasePrice as decimal? ?? 0,
                    Vat5PurchasePrice = x.Vat5PurchasePrice as decimal? ?? 0,
                    Vat145PurchasePrice = x.Vat145PurchasePrice as decimal? ?? 0,
                    VatOtherPurchasePrice = x.VatOtherPurchasePrice as decimal? ?? 0,
                    Gst0PurchasePrice = x.Gst0PurchasePrice as decimal? ?? 0,
                    Gst5PurchasePrice = x.Gst5PurchasePrice as decimal? ?? 0,
                    Gst12PurchasePrice = x.Gst12PurchasePrice as decimal? ?? 0,
                    Gst18PurchasePrice = x.Gst18PurchasePrice as decimal? ?? 0,
                    Gst28PurchasePrice = x.Gst28PurchasePrice as decimal? ?? 0,
                    GstOtherPurchasePrice = x.GstOtherPurchasePrice as decimal? ?? 0

                };
                return stockValue;
            });
            return stockValueList.ToList();
        }
        public async Task<List<SalesItem>> ScheduleReportList(string type, string accountId, string instanceId, string schedule, DateTime from, DateTime to, string search, string frombillno, string tobillno)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            var scheduleName = "";
            if (!string.IsNullOrEmpty(schedule) && schedule != "undefined")
            {

                if (schedule == "H2")
                {
                    scheduleName = "and product.Schedule in ('H','H1' )";
                }
                else if (schedule == "NON-SCHEDULED (NS) ")
                {
                    scheduleName = "and product.Schedule in ('NS','NON-SCHEDULED' )";
                }
                else
                {
                    scheduleName = "and product.Schedule = '" + schedule + "'";
                }
            }
            var frmBillno = "0";
            var toBillno = "0";
            var billNo = "";
            var invoiceSeries = "";
            var InvoiceDate = "";
            string filterDate = string.Empty;

            if (search == "Default")
            {

                invoiceSeries = "and isnull(sales.invoiceSeries,'') = ''";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else if (search == "MAN")
            {
                invoiceSeries = "and isnull(sales.invoiceSeries,'') = 'MAN'";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else if (search != null && search != "All" && search != "Custom" && search != "undefined" && search != "Total")
            {
                invoiceSeries = "and invoiceSeries = '" + search + "'";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else if (search == "Custom")
            {
                invoiceSeries = "and ISNULL(sales.InvoiceSeries,'') != '' and ISNULL(sales.InvoiceSeries,'') != 'MAN'";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else if (search == "Total")
            {
                invoiceSeries = "";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else
            {
                invoiceSeries = "";
            }

            if (search != null && search != "undefined")
            {
                DateTime dt1 = DateTime.Parse("01-01-0001 00:00:00");
                if (from != dt1 && to != dt1)
                {
                    filterDate = "AND Convert(date, sales.invoicedate) BETWEEN '" + filterFromDate + "' AND '" + filterToDate + "'";
                }
                else
                {
                    InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
                }
            }

            //else if (search == "All")
            //{
            //    invoiceSeries = "and isnull(sales.invoiceSeries,'') = ''";
            //    InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            //}

            //if ((!string.IsNullOrEmpty(search)) && search!="undefined")
            //{
            //    invoiceSeries = "and invoiceSeries = '" + search + "'";
            //}
            //else
            //{   
            //    if(type== "Today" && type == "Week" && type == "Month")
            //    invoiceSeries = "and isnull(sales.invoiceSeries,'') = ''";
            //}



            if ((!string.IsNullOrEmpty(frombillno) && !string.IsNullOrEmpty(tobillno)) && frombillno != "undefined" && tobillno != "undefined")
            {
                frmBillno = frombillno;
                toBillno = tobillno;
                billNo = "and convert(int,sales.InvoiceNo) BETWEEN '" + frmBillno + "' and '" + toBillno + "'";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            if (string.IsNullOrEmpty(type))
            {
                filterFromDate = from.ToFormat();
                filterToDate = to.ToFormat();
                if (filterFromDate == "01-Jan-0001" && filterToDate == "01-Jan-0001")
                {
                    filterFromDate = CustomDateTime.IST.ToFormat();
                    filterToDate = CustomDateTime.IST.ToFormat();
                    filterDate = "";
                }
                else
                {
                    filterDate = "AND Convert(date, sales.invoicedate) BETWEEN '" + filterFromDate + "' AND '" + filterToDate + "'";
                }
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "WEEK":
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "MONTH":
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
                filterDate = "AND Convert(date, sales.invoicedate) BETWEEN '" + filterFromDate + "' AND '" + filterToDate + "'";
            }
            //string query =
            //    $@"select sales.Id,sales.InvoiceNo,sales.InvoiceDate,sales.Name,sales.Address,sales.DoctorName,salesitem.ProductStockId,salesitem.Quantity,productstock.BatchNo,productstock.ExpireDate,product.Name as ProductName,product.Schedule,product.Manufacturer from Sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id left join product on product.id=productstock.productId where sales.InstanceId = '{instanceId}' AND sales.AccountId = '{accountId}' {filterDate} {scheduleName} {billNo} order by sales.InvoiceNo asc";




            string query =
             $@"select sales.Id,ltrim(isnull(sales.prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,sales.InvoiceNo,sales.InvoiceDate,sales.Name,sales.Address,sales.DoctorName,salesitem.ProductStockId,salesitem.Quantity,productstock.BatchNo,productstock.ExpireDate,product.Name as ProductName,product.Schedule,product.Manufacturer from Sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id left join product on product.id=productstock.productId where sales.InstanceId = '{instanceId}' AND sales.AccountId = '{accountId}' {filterDate} {scheduleName} {billNo}{invoiceSeries} {InvoiceDate} and Cancelstatus is null order by isNull(sales.InvoiceSeries, ''),cast(sales.InvoiceNo as varchar) asc";



            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<SalesItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesItem
                    {
                        ProductStockId = reader[SalesItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = reader[SalesItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,

                        //added by nandhini
                        ReportExpireDate = " " + Convert.ToDateTime(reader["ExpireDate"]).ToString("MM-yy"),
                        Packing = reader["InvoiceSeries"].ToString() + reader["InvoiceNo"].ToString(),
                        ReportCreatedAt = " " + Convert.ToDateTime(reader["InvoiceDate"]).ToString("dd-MM-yyyy"),



                        Sales =
                        {
                           // Id = reader["Id"].ToString(),
                           // InvoiceNo = reader["InvoiceNo"].ToString(),
                           // InvoiceDate = Convert.ToDateTime(reader["InvoiceDate"]),
                           // //Name = reader["Name"].ToString(),
                           // //Address = reader["Address"].ToString(),
                           //// NameAddress=string.Concat(reader["Name"].ToString() + Environment.NewLine + reader["Address"].ToString()),
                           //  NameAddress=reader["Name"].ToString()+Environment.NewLine + "-"+ reader["Address"].ToString(),
                           // DoctorName = reader["DoctorName"].ToString()
                           

                             Id = reader["Id"].ToString(),
                            //added by nandhini 22.4
                            InvoiceSeries=reader["InvoiceSeries"].ToString(),
                            InvoiceNo = reader["InvoiceNo"].ToString(),
                            InvoiceDate = Convert.ToDateTime(reader["InvoiceDate"]),
                            //Name = reader["Name"].ToString(),
                            //Address = reader["Address"].ToString(),
                           // NameAddress=string.Concat(reader["Name"].ToString() + Environment.NewLine + reader["Address"].ToString()),
                             NameAddress=reader["Name"].ToString()+Environment.NewLine + "-"+ reader["Address"].ToString(),
                            DoctorName = reader["DoctorName"].ToString(),
                        }
                    };
                    salesItem.ProductStock.Product.Name = reader["ProductName"].ToString();
                    salesItem.ProductStock.BatchNo = reader["BatchNo"].ToString();
                    salesItem.ProductStock.ExpireDate = Convert.ToDateTime(reader["ExpireDate"]);

                    //Product details
                    salesItem.ProductStock.Product.Manufacturer = reader["Manufacturer"].ToString();
                    salesItem.ProductStock.Product.Schedule = reader["Schedule"].ToString();
                    list.Add(salesItem);
                }
            }
            return list;
        }

        public async Task<List<SalesItem>> ScheduleReportListforPrint(string type, string accountId, string instanceId, string schedule, DateTime? from, DateTime? to, string frombillno, string tobillno, string search)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            var scheduleName = "";
            if (!string.IsNullOrEmpty(schedule) && schedule != "undefined")
            {
                if (schedule == "H2")
                {
                    scheduleName = "and product.Schedule in ('H','H1' )";
                }
                else if (schedule == "NON-SCHEDULED (NS) ")
                {
                    scheduleName = "and product.Schedule in ('NS','NON-SCHEDULED' )";
                }
                else
                {
                    scheduleName = "and product.Schedule = '" + schedule + "'";
                }
            }
            var frmBillno = "0";
            var toBillno = "0";
            var billNo = "";
            var invoiceSeries = "";
            var InvoiceDate = "";
            string filterDate = string.Empty;




            if (search == "Default")
            {

                invoiceSeries = "and isnull(sales.invoiceSeries,'') = ''";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else if (search == "MAN")
            {
                invoiceSeries = "and isnull(sales.invoiceSeries,'') = 'MAN'";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else if (search != null && search != "All" && search != "Custom" && search != "undefined" && search != "Total")
            {
                invoiceSeries = "and invoiceSeries = '" + search + "'";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else if (search == "Custom")
            {
                invoiceSeries = "and ISNULL(sales.InvoiceSeries,'') != '' and ISNULL(sales.InvoiceSeries,'') != 'MAN'";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else if (search == "Total")
            {
                invoiceSeries = "";
                //InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            else
            {
                invoiceSeries = "";
            }

            if (search != null && search != "undefined")
            {
                DateTime dt1 = DateTime.Parse("01-01-0001 00:00:00");
                if (from != dt1 && to != dt1)
                {
                    filterDate = "AND Convert(date, sales.invoicedate) BETWEEN '" + filterFromDate + "' AND '" + filterToDate + "'";
                }
                else
                {
                    InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
                }
            }



            if ((!string.IsNullOrEmpty(frombillno) && !string.IsNullOrEmpty(tobillno)) && frombillno != "undefined" && tobillno != "undefined")
            {
                frmBillno = frombillno;
                toBillno = tobillno;
                billNo = "and convert(int,sales.InvoiceNo) BETWEEN '" + frmBillno + "' and '" + toBillno + "'";
                // InvoiceDate = "AND cast(sales.InvoiceDate as date) > = Concat(Year(Getdate()), '-04', '-01')";
            }
            if (string.IsNullOrEmpty(type))
            {
                filterFromDate = from.ToFormat();
                filterToDate = to.ToFormat();
                if ((filterFromDate == "01-Jan-0001" && filterToDate == "01-Jan-0001") || (filterFromDate == "" && filterToDate == ""))
                {
                    filterFromDate = CustomDateTime.IST.ToFormat();
                    filterToDate = CustomDateTime.IST.ToFormat();
                    filterDate = "";
                }
                else
                {
                    filterDate = "AND Convert(date, sales.invoicedate) BETWEEN '" + filterFromDate + "' AND '" + filterToDate + "'";
                }
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "WEEK":
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "MONTH":
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
                filterDate = "AND Convert(date, sales.invoicedate) BETWEEN '" + filterFromDate + "' AND '" + filterToDate + "'";
            }
            //      string query =
            //          $@"
            //           select   
            //                  sales.Id,
            //                  sales.InvoiceNo,
            //                  sales.InvoiceDate,
            //                  sales.Name,
            //                  sales.Address,
            //                  sales.DoctorName,
            //                  salesitem.ProductStockId,
            //                  salesitem.Quantity,
            //                  productstock.BatchNo,
            //                  productstock.ExpireDate,
            //                  product.Name as ProductName,
            //                  product.Schedule,
            //                  product.Manufacturer,
            //                  product.Type,
            //            	Instance.Name as [Instance.Name],
            //Instance.DrugLicenseNo,
            //Instance.TinNo,
            //                  Instance.Address as [Instance.Address]
            //                  from Sales inner join salesitem on sales.id = salesitem.salesid 
            //                         inner join Instance  on Instance.id = Sales.InstanceId
            //                          inner join productstock on salesitem.productstockid = productstock.id 
            //                          left join product on product.id=productstock.productId 
            //                          where sales.InstanceId = '{instanceId}' AND sales.AccountId = '{accountId}' {filterDate} {scheduleName} {billNo} order by CAST(sales.InvoiceNo AS INT) asc";




            string query =
               $@"

                 select   
                        sales.Id,
                        sales.InvoiceNo,
                        ltrim(isnull(sales.prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,
                        sales.InvoiceDate,
                        sales.Name,
                        sales.Address,
                        sales.DoctorName,
                        salesitem.ProductStockId,
                        salesitem.Quantity,
                        productstock.BatchNo,
                        productstock.ExpireDate,
                        Replace(Replace(product.Name,'@',' '),'*',' ') as ProductName,
                        product.Schedule,
                        product.Manufacturer,
                        product.Type,
                        Doctor.Area,
	                 	Instance.Name as [Instance.Name],
						Instance.DrugLicenseNo,
						Instance.TinNo,
                        Instance.GsTinNo,
                        Instance.Address as [Instance.Address]

                        from Sales inner join salesitem on sales.id = salesitem.salesid 

                               inner join Instance  on Instance.id = Sales.InstanceId
                                left join Doctor on Doctor.id = Sales.DoctorId
                                inner join productstock on salesitem.productstockid = productstock.id 
                                left join product on product.id=productstock.productId 
                                where sales.InstanceId = '{instanceId}' AND sales.AccountId = '{accountId}' {filterDate} {scheduleName} {billNo}   { invoiceSeries} {InvoiceDate} AND Sales.Cancelstatus is null order by isNull(sales.InvoiceSeries, ''),cast(sales.InvoiceNo as int) asc";



            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<SalesItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesItem
                    {
                        ProductStockId = reader[SalesItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = reader[SalesItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        Sales =
                        {
                            //Id = reader["Id"].ToString(),
                            //InvoiceNo = reader["InvoiceNo"].ToString(),
                            //InvoiceDate = Convert.ToDateTime(reader["InvoiceDate"]),
                            // NameAddress=reader["Name"].ToString(),
                            //DoctorName = reader["DoctorName"].ToString(),
                            //Instance = {
                            //    Name = reader[InstanceTable.NameColumn.FullColumnName].ToString(),
                            //    DrugLicenseNo = reader[InstanceTable.DrugLicenseNoColumn.ColumnName].ToString(),
                            //    TinNo= reader[InstanceTable.TinNoColumn.ColumnName].ToString(),
                            //    FullAddress1 = reader[InstanceTable.AddressColumn.FullColumnName].ToString()
                            //}



                             Id = reader["Id"].ToString(),
                             InvoiceNo = reader["InvoiceNo"].ToString(),
                             InvoiceSeries=reader["InvoiceSeries"].ToString(),
                             InvoiceDate = Convert.ToDateTime(reader["InvoiceDate"]),
                             NameAddress=reader["Name"].ToString(),
                             Address=reader["Address"].ToString(),
                             DoctorName = reader["DoctorName"].ToString(),
                             DoctorAddress=reader["Area"].ToString(),
                             Instance = {
                                Name = reader[InstanceTable.NameColumn.FullColumnName].ToString(),
                                DrugLicenseNo = reader[InstanceTable.DrugLicenseNoColumn.ColumnName].ToString(),
                                TinNo= reader[InstanceTable.TinNoColumn.ColumnName].ToString(),
                                GsTinNo= reader[InstanceTable.GsTinNoColumn.ColumnName].ToString(),
                                FullAddress1 = reader[InstanceTable.AddressColumn.FullColumnName].ToString()
                            }
                        }
                    };
                    salesItem.ProductStock.Product.Name = reader["ProductName"].ToString().Replace("{", "(").Replace("}", ")"); //Replace added by POongodi on 31/07/2017
                    salesItem.ProductStock.BatchNo = reader["BatchNo"].ToString();
                    salesItem.ProductStock.ExpireDate = Convert.ToDateTime(reader["ExpireDate"]);
                    //Product details
                    salesItem.ProductStock.Product.Manufacturer = reader["Manufacturer"].ToString();
                    salesItem.ProductStock.Product.Schedule = reader["Schedule"].ToString();
                    salesItem.ProductStock.Product.Type = reader["Type"].ToString();
                    list.Add(salesItem);
                }
            }
            return list;
        }
        // VAT
        /*  public async Task<List<VendorPurchaseItem>> PurchaseReportList(string type, string accountId, string instanceId,
              DateTime from, DateTime to, int filter)
          {
              var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
              qb.AddColumn(VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.QuantityColumn,
                  VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                  VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn);
              qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn,
                  VendorPurchaseItemTable.VendorPurchaseIdColumn);
              qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn,
                  VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.InvoiceDateColumn,
                  VendorPurchaseTable.DiscountColumn);
              qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
              qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.IdColumn, VendorTable.NameColumn, VendorTable.TinNoColumn);
              qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn,
                  VendorPurchaseItemTable.ProductStockIdColumn);
              qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn,
                  ProductStockTable.VATColumn);
              qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
              qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.CommodityCodeColumn);
              if (string.IsNullOrEmpty(type))
              {
                  qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceDateColumn, CriteriaEquation.Between);
                  qb.Parameters.Add("FilterFromDate", from.ToFormat());
                  qb.Parameters.Add("FilterToDate", to.ToFormat());
              }
              else
              {
                  switch (type.ToUpper())
                  {
                      case "TODAY":
                          qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceDateColumn, CustomDateTime.IST.ToFormat());
                          break;
                      case "WEEK":
                          qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceDateColumn, CriteriaEquation.Between);
                          qb.Parameters.Add("FilterFromDate",
                              DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                          qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                          break;
                      case "MONTH":
                          qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceDateColumn, CriteriaEquation.Between);
                          qb.Parameters.Add("FilterFromDate",
                              new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                          qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                          break;
                  }
              }
              qb.ConditionBuilder.And(VendorPurchaseTable.AccountIdColumn, accountId);
              qb.ConditionBuilder.And(VendorPurchaseTable.InstanceIdColumn, instanceId);
              if (filter == 0)
              {
                  qb.ConditionBuilder.And(ProductStockTable.VATColumn, 0, CriteriaEquation.Equal);
              }
              else
              {
                  qb.ConditionBuilder.And(ProductStockTable.VATColumn, 0, CriteriaEquation.Greater);
              }
              qb.SelectBuilder.SortByDesc(VendorPurchaseTable.InvoiceDateColumn);
              qb.SelectBuilder.SortByDesc(VendorPurchaseTable.CreatedAtColumn);
              var list = new List<VendorPurchaseItem>();
              using (var reader = await QueryExecuter.QueryAsyc(qb))
              {
                  while (reader.Read())
                  {
                      var vendorPurchaseItem = new VendorPurchaseItem
                      {
                          ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                          Quantity = reader[VendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                          PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                          PackagePurchasePrice =
                             reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0,
                          PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0,
                          PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0,
                          Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                          FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] as int? ?? 0,
                          BuyPurchase = new VendorPurchase
                          {
                              Id = reader[VendorPurchaseTable.IdColumn.FullColumnName].ToString(),
                              InvoiceNo = reader[VendorPurchaseTable.InvoiceNoColumn.FullColumnName].ToString(),
                              InvoiceDate =
                                  Convert.ToDateTime(reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName])
                          }
                      };
                      vendorPurchaseItem.BuyPurchase.Discount =
                          reader[VendorPurchaseTable.DiscountColumn.FullColumnName] != DBNull.Value
                              ? Convert.ToDecimal(reader[VendorPurchaseTable.DiscountColumn.FullColumnName])
                              : 0;
                      vendorPurchaseItem.BuyPurchase.Vendor = new Contract.Infrastructure.Master.Vendor
                      {
                          Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
                          TinNo = reader[VendorTable.TinNoColumn.FullColumnName].ToString(),
                      };
                      vendorPurchaseItem.ProductStock = new ProductStock
                      {
                          SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                          VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                          //CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0,
                          Product = { CommodityCode = reader[ProductTable.CommodityCodeColumn.FullColumnName].ToString() }
                      };
                      vendorPurchaseItem.PackageQty = vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty;
                      if (filter == 0)
                      {
                          vendorPurchaseItem.ReportTotal = vendorPurchaseItem.Total;
                      }
                      else
                      {
                          vendorPurchaseItem.ReportTotal = (((vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty) * vendorPurchaseItem.PackagePurchasePrice) -
                                                                               ((vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty)) *
                                                                                (vendorPurchaseItem.Discount / 100))) + vendorPurchaseItem.VatPrice;
                      }
                      list.Add(vendorPurchaseItem);
                  }
              }
              return list;
          }
        */  //
        public async Task<List<VendorPurchaseItem>> PurchaseReportList(string type, string accountId, string instanceId,
               DateTime from, DateTime to, int filter)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("vatPer", filter);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_VAT_Annx1_Rpt", parms);
            var list = new List<VendorPurchaseItem>();
            var vendorpurchaseItems = result.Select(x =>
            {
                var vendorPurchaseItem = new VendorPurchaseItem
                {
                    POValue = x.POValue as decimal? ?? 0,
                    VATValue = x.VatValue as decimal? ?? 0,
                    Category = Convert.ToString(x.Category),
                    ReportTotal = x.InvoiceValue as decimal? ?? 0,
                    ProductStock =
                {
                              VAT = x.VAT as decimal? ?? 0,
                              Product = { CommodityCode = x.Commoditycode.ToString() }
                },
                    BuyPurchase = {
                              InvoiceNo =Convert.ToString( x.InvoiceNo),
                          InvoiceDate =
                              Convert.ToDateTime(x.InvoiceDate)
                              },
                    Vendor ={
                              Name = Convert.ToString(x.VendorName),
                              TinNo = Convert.ToString(x.VendorTinNo)
                        }
                };
                return vendorPurchaseItem;
            });
            return vendorpurchaseItems.ToList();
        }
        /* public async Task<List<VendorPurchaseReturnItem>> PurchaseReturnReportList(string type, string accountId, string instanceId, DateTime from, DateTime to)
         {
             var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorReturnItemTable.ProductStockIdColumn, VendorReturnItemTable.QuantityColumn);
            qb.JoinBuilder.Join(VendorReturnTable.Table, VendorReturnTable.IdColumn, VendorReturnItemTable.VendorReturnIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorReturnTable.Table, VendorReturnTable.IdColumn, VendorReturnTable.ReturnNoColumn, VendorReturnTable.ReturnDateColumn);
            qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorReturnTable.VendorPurchaseIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorPurchaseTable.VendorIdColumn, VendorPurchaseTable.InvoiceDateColumn, VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.DiscountColumn);
            qb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.VendorPurchaseIdColumn, VendorPurchaseTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.IdColumn, VendorPurchaseItemTable.QuantityColumn,
                VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.MobileColumn, VendorTable.EmailColumn, VendorTable.AddressColumn, VendorTable.TinNoColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.CSTColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.CommodityCodeColumn);
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from.ToFormat());
                qb.Parameters.Add("FilterToDate", to.ToFormat());
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CustomDateTime.IST.ToFormat());
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                }
            }
             qb.ConditionBuilder.And(VendorReturnTable.AccountIdColumn, accountId);
             qb.ConditionBuilder.And(VendorReturnTable.InstanceIdColumn, instanceId);
             qb.SelectBuilder.SortByDesc(VendorReturnTable.CreatedAtColumn);
             var list = new List<VendorPurchaseReturnItem>();
             using (var reader = await QueryExecuter.QueryAsyc(qb))
             {
                 while (reader.Read())
                 {
                     var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                     {
                         ProductStockId = reader[VendorReturnItemTable.ProductStockIdColumn.ColumnName].ToString(),
                         Quantity = reader[VendorReturnItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                         VendorPurchaseReturn =
                         {
                             Id = reader[VendorReturnTable.IdColumn.FullColumnName].ToString(),
                             ReturnNo = reader[VendorReturnTable.ReturnNoColumn.FullColumnName].ToString(),
                             ReturnDate = Convert.ToDateTime(reader[VendorReturnTable.ReturnDateColumn.FullColumnName]),
                             VendorPurchase =
                             {
                                 Vendor =
                                 {
                                     Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
                                     Mobile = reader[VendorTable.MobileColumn.FullColumnName].ToString(),
                                     Email = reader[VendorTable.EmailColumn.FullColumnName].ToString(),
                                     Address = reader[VendorTable.AddressColumn.FullColumnName].ToString(),
                                     TinNo = reader[VendorTable.TinNoColumn.FullColumnName].ToString()
                                 },
                                 Discount = reader[VendorPurchaseTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
                                 InvoiceNo = reader[VendorPurchaseTable.InvoiceNoColumn.FullColumnName].ToString(),
                                 InvoiceDate = Convert.ToDateTime(reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName]),
                             }
                         },
                         ProductStock =
                         {
                             SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                     VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                             Product = {CommodityCode = reader[ProductTable.CommodityCodeColumn.FullColumnName].ToString()},
                         },
                         VendorPurchaseItem =
                         {
                             Quantity = reader[VendorPurchaseItemTable.QuantityColumn.FullColumnName] as decimal? ?? 0,
                             PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0,
                             PackagePurchasePrice =
                             reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.FullColumnName] as decimal? ?? 0,
                             PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.FullColumnName] as decimal? ?? 0,
                             PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.FullColumnName] as decimal? ?? 0,
                             Discount = reader[VendorPurchaseItemTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
                             FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.FullColumnName] as int? ?? 0,
                         },
                         ReturnPurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0,
                     };
                     vendorPurchaseReturnItem.VendorPurchaseItem.ReportTotal = (vendorPurchaseReturnItem.VendorPurchaseItem.Total * vendorPurchaseReturnItem.ProductStock.VAT) / 100;
                     vendorPurchaseReturnItem.VendorPurchaseReturn.SubTotal = vendorPurchaseReturnItem.VendorPurchaseItem.Total + vendorPurchaseReturnItem.VendorPurchaseItem.ReportTotal;
                     vendorPurchaseReturnItem.Total = ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity) - ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity) * (vendorPurchaseReturnItem.VendorPurchaseReturn.VendorPurchase.Discount / 100))) + vendorPurchaseReturnItem.VatPrice;
                     list.Add(vendorPurchaseReturnItem);
                 }
             }
             return list;
         }
         //
         */
        public async Task<List<VendorPurchaseReturnItem>> PurchaseReturnReportList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_VAT_POReturn_Annx5_rpt", parms);
            var vendorPurchaseReturnItems = result.Select(x =>
            {
                var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                {
                    // ReportTotal = x.ReturnVat,
                    Total = x.ReturnVat as decimal? ?? 0,
                    VendorPurchaseReturn =
                        {
                            ReturnNo = Convert.ToString(x.ReturnNo),
                            ReturnDate = Convert.ToDateTime(x.ReturnDate),
                            VendorPurchase =
                            {
                                Vendor =
                                {
                                    Name =Convert.ToString( x.VendorName),
                                    TinNo =Convert.ToString( x.TinNo)
                                },
                                InvoiceNo = Convert.ToString(x.InvoiceNo),
                                InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                            },
                         //ReturnPurchasePrice =  x.ReturnValue as decimal? ?? 0,
                SubTotal = x.ReturnValue 
                //vendorPurchaseReturnItem.Total = x.ReturnValue
            },
                    ProductStock =
                {
                              VAT = x.VAT as decimal? ?? 0,
                              Product = { CommodityCode = x.Commoditycode.ToString() }
                },
                    VendorPurchaseItem =
                        {
                            PurchasePrice =  x.POValue as decimal? ?? 0,
                            VATValue= x.POVat,
                            ReportTotal =x.POValue + x.POVat,
                            POValue =x.POValue
                        }
                };
                return vendorPurchaseReturnItem;
            });
            return vendorPurchaseReturnItems.ToList();
        }
        //
        /*The following method name changed and the same method created to convert direct qry to sp by Poongodi*/
        public async Task<List<SalesItem>> SalesReportListData_Old(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesItemTable.ProductStockIdColumn, SalesItemTable.QuantityColumn);
            qb.JoinBuilder.Join(SalesTable.Table, SalesTable.IdColumn, SalesItemTable.SalesIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.IdColumn, SalesTable.InvoiceNoColumn, SalesTable.InvoiceDateColumn, SalesTable.NameColumn, SalesTable.EmailColumn, SalesTable.MobileColumn, SalesTable.DiscountColumn, SalesTable.DoctorNameColumn, SalesTable.AddressColumn, SalesTable.PaymentTypeColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.CommodityCodeColumn, ProductTable.ScheduleColumn, ProductTable.TypeColumn, ProductTable.ManufacturerColumn);
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from.ToFormat());
                qb.Parameters.Add("FilterToDate", to.ToFormat());
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", CustomDateTime.IST.ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                }
            }
            qb.ConditionBuilder.And(ProductStockTable.VATColumn, 0, CriteriaEquation.Equal);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            qb.SelectBuilder.SortByDesc(SalesTable.InvoiceDateColumn);
            var list = new List<SalesItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesItem
                    {
                        ProductStockId = reader[SalesItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = reader[SalesItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        Sales =
                        {
                            Id = reader[SalesTable.IdColumn.FullColumnName].ToString(),
                            InvoiceNo = reader[SalesTable.InvoiceNoColumn.FullColumnName].ToString(),
                            InvoiceDate = Convert.ToDateTime(reader[SalesTable.InvoiceDateColumn.FullColumnName]),
                            Name = reader[SalesTable.NameColumn.FullColumnName].ToString(),
                            DoctorName = reader[SalesTable.DoctorNameColumn.FullColumnName].ToString(),
                            Address = reader[SalesTable.AddressColumn.FullColumnName].ToString(),
                            PaymentType = reader[SalesTable.PaymentTypeColumn.FullColumnName].ToString()
                        }
                    };
                    if (reader[SalesTable.EmailColumn.FullColumnName].ToString() != "N/A")
                    {
                        salesItem.Sales.Email = reader[SalesTable.EmailColumn.FullColumnName].ToString();
                    }
                    salesItem.Sales.Discount = reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0;
                    salesItem.Sales.Mobile = reader[SalesTable.MobileColumn.FullColumnName].ToString();
                    salesItem.ProductStock.SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0;
                    salesItem.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;
                    salesItem.ProductStock.Product.CommodityCode = reader[ProductTable.CommodityCodeColumn.FullColumnName].ToString();
                    salesItem.ReportTotal = (salesItem.ProductStock.SellingPrice * salesItem.Quantity) -
                                            (salesItem.ProductStock.SellingPrice * salesItem.Quantity *
                                             (salesItem.Sales.Discount / 100));
                    salesItem.ReportTotal = Math.Round((decimal)salesItem.ReportTotal, MidpointRounding.AwayFromZero);
                    salesItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    salesItem.ProductStock.ExpireDate = (DateTime)reader[ProductStockTable.ExpireDateColumn.FullColumnName];
                    //Product details
                    salesItem.ProductStock.Product.Manufacturer = reader[ProductTable.ManufacturerColumn.FullColumnName].ToString();
                    salesItem.ProductStock.Product.Schedule = reader[ProductTable.ScheduleColumn.FullColumnName].ToString();
                    salesItem.ProductStock.Product.Type = reader[ProductTable.TypeColumn.FullColumnName].ToString();
                    list.Add(salesItem);
                }
            }
            return list;
        }
        //
        public async Task<List<SalesItem>> SalesReportListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_vat_Sales_annx21_rpt", parms);
            var salesItems = result.Select(x =>
            {
                var SalesItem = new SalesItem
                {
                    TinNo = Convert.ToString(x.BuyerTin),
                    Category = Convert.ToString(x.Category),
                    VATValue = x.VatValue as decimal? ?? 0,
                    Sales =
                        {
                            InvoiceNo =Convert.ToString(x.InvoiceNo),
                            InvoiceDate = Convert.ToDateTime(x.Invoicedate),
                            Name = Convert.ToString(x.BuyerName),
                            FinalValue = (x.VatValue + x.SalesValue ) as decimal? ?? 0,
                            Select = Convert.ToString(x.TranType),
                            Remarks=Convert.ToString(x.ExemptType)
                         },
                    ProductStock =
                {
                            Product = { CommodityCode = x.Commoditycode.ToString() },
                },
                };
                return SalesItem;
            });
            return salesItems.ToList();
        }
        //
        /*The following method name changed and the same method created to convert direct qry to sp by Poongodi*/
        public async Task<List<SalesReturnItem>> SalesReturnList_Old(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesReturnItemTable.ProductStockIdColumn, SalesReturnItemTable.QuantityColumn, SalesReturnItemTable.MrpSellingPriceColumn);
            qb.JoinBuilder.Join(SalesReturnTable.Table, SalesReturnTable.IdColumn, SalesReturnItemTable.SalesReturnIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesReturnTable.Table, SalesReturnTable.IdColumn, SalesReturnTable.ReturnNoColumn, SalesReturnTable.ReturnDateColumn);
            qb.JoinBuilder.Join(SalesTable.Table, SalesTable.IdColumn, SalesReturnTable.SalesIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.NameColumn, SalesTable.EmailColumn, SalesTable.MobileColumn, SalesTable.DiscountColumn, SalesTable.InvoiceNoColumn, SalesTable.InvoiceDateColumn, SalesTable.AddressColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.CSTColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.CommodityCodeColumn, ProductTable.ScheduleColumn, ProductTable.TypeColumn);
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(SalesReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from.ToFormat());
                qb.Parameters.Add("FilterToDate", to.ToFormat());
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(SalesReturnTable.ReturnDateColumn, CustomDateTime.IST.ToFormat());
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(SalesReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(SalesReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                }
            }
            qb.ConditionBuilder.And(SalesReturnTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesReturnTable.InstanceIdColumn, instanceId);
            qb.SelectBuilder.SortByDesc(SalesReturnTable.ReturnDateColumn);
            var list = new List<SalesReturnItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesReturnItem = new SalesReturnItem
                    {
                        ProductStockId = reader[SalesReturnItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = reader[SalesReturnItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        MrpSellingPrice = reader[SalesReturnItemTable.MrpSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        SalesReturn =
                        {
                            Id = reader[SalesReturnTable.IdColumn.FullColumnName].ToString(),
                            ReturnNo = reader[SalesReturnTable.ReturnNoColumn.FullColumnName].ToString(),
                            ReturnDate = Convert.ToDateTime(reader[SalesReturnTable.ReturnDateColumn.FullColumnName]),
                            Sales = { Name = reader[SalesTable.NameColumn.FullColumnName].ToString(),
                            InvoiceNo = reader[SalesTable.InvoiceNoColumn.FullColumnName].ToString(),
                            InvoiceDate = Convert.ToDateTime(reader[SalesTable.InvoiceDateColumn.FullColumnName])
                            }
                        }
                    };
                    if (reader[SalesTable.EmailColumn.FullColumnName].ToString() != "N/A")
                    {
                        salesReturnItem.SalesReturn.Sales.Email = reader[SalesTable.EmailColumn.FullColumnName].ToString();
                    }
                    salesReturnItem.Discount = reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0;
                    salesReturnItem.SalesReturn.Sales.Mobile = reader[SalesTable.MobileColumn.FullColumnName].ToString();
                    salesReturnItem.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;
                    salesReturnItem.ProductStock.CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0;
                    salesReturnItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    salesReturnItem.ProductStock.ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName]);
                    salesReturnItem.ProductStock.Product.Name = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    salesReturnItem.ProductStock.SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0;
                    salesReturnItem.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    salesReturnItem.ProductStock.Product.CommodityCode = reader[ProductTable.CommodityCodeColumn.FullColumnName].ToString();
                    salesReturnItem.ProductStock.Product.Schedule = reader[ProductTable.ScheduleColumn.FullColumnName].ToString();
                    salesReturnItem.ProductStock.Product.Type = reader[ProductTable.TypeColumn.FullColumnName].ToString();
                    // salesReturnItem.Total = salesReturnItem.ProductStock.SellingPrice - (salesReturnItem.ProductStock.SellingPrice * (salesReturnItem.Discount / 100));
                    salesReturnItem.SalesRoundOff = salesReturnItem.Total - (salesReturnItem.Total * (salesReturnItem.Discount / 100));
                    salesReturnItem.SalesRoundOff = Math.Round((decimal)salesReturnItem.SalesRoundOff, MidpointRounding.AwayFromZero);
                    //salesReturnItem.Total = Math.Round((decimal)salesReturnItem.Total);
                    list.Add(salesReturnItem);
                }
            }
            return list;
        }
        public async Task<List<SalesReturnItem>> SalesReturnList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_vat_SalesReturn_annx19_rpt", parms);
            var salesReturn = result.Select(x =>
            {
                var salesReturnItem = new SalesReturnItem
                {
                    SalesReturn =
                        {
                            ReturnNo =x.ReturnNo.ToString(),
                            ReturnDate = Convert.ToDateTime(x.ReturnDate),
                            ReturnedPrice =x.ReturnValue as decimal? ??0,
                            Sales =
                            {
                            Name = x.BuyerName.ToString(),
                              InvoiceNo = x.InvoiceNo.ToString(),
                            InvoiceDate = Convert.ToDateTime(x.Invoicedate),
                            FinalValue = (x.VatValue + x.SalesValue ) as decimal? ?? 0,
                            InvoiceAmount = x.SalesValue as decimal? ?? 0
                               },
                            SalesItem =
                            {
                                 TinNo = Convert.ToString(x.BuyerTin),
                                   VATValue = x.VatValue as decimal? ?? 0,
                            }
                    },
                    ProductStock =
                {
                              VAT = x.VAT as decimal? ?? 0,
                              Product = { CommodityCode = x.Commoditycode.ToString() },
                        TotalCostPrice = x.SalesValue as decimal? ?? 0
                },
                    SalesRoundOff = x.RVatValue
                };
                return salesReturnItem;
            });
            return salesReturn.ToList();
        }
        //
        public async Task<List<StockInventoryData>> StockInventoryList(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.AddColumn(ProductStockTable.ProductIdColumn, ProductStockTable.VATColumn, ProductStockTable.PurchasePriceColumn,
                            ProductStockTable.SellingPriceColumn, ProductStockTable.CSTColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.CommodityCodeColumn);
            qb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.QuantityColumn, VendorPurchaseItemTable.DiscountColumn,
                              VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn, VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PurchasePriceColumn);
            qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesItemTable.Table, SalesItemTable.QuantityColumn, SalesItemTable.SellingPriceColumn);
            qb.JoinBuilder.Join(SalesTable.Table, SalesTable.IdColumn, SalesItemTable.SalesIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.IdColumn, SalesTable.InvoiceDateColumn, SalesTable.DiscountColumn);
            //if(string.IsNullOrEmpty(type))
            //{
            //    qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn, CriteriaEquation.Between);
            //    qb.Parameters.Add("FilterFromDate", from.ToFormat());
            //    qb.Parameters.Add("FilterToDate", to.ToFormat());
            //}
            //else
            //{
            //    switch (type.ToUpper())
            //    {
            //        case "TODAY":
            //            qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn, CustomDateTime.IST.ToFormat());
            //            break;
            //        case "WEEK":
            //            qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn, CriteriaEquation.Between);
            //            qb.Parameters.Add("FilterFromDate",
            //                DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
            //            qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
            //            break;
            //        case "MONTH":
            //            qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn, CriteriaEquation.Between);
            //            qb.Parameters.Add("FilterFromDate",
            //                new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
            //            qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
            //            break;
            //    }
            //}
            DateTime fromdt = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).AddMonths(-1);
            DateTime todt = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).AddDays(-1);
            qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn, CriteriaEquation.Between);
            qb.Parameters.Add("FilterFromDate", fromdt.ToFormat());
            qb.Parameters.Add("FilterToDate", todt.ToFormat());
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            qb.SelectBuilder.SortByDesc(SalesTable.InvoiceDateColumn);
            var list = new List<StockInventory>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var stockInventory = new StockInventory
                    {
                        ProductId = reader[ProductStockTable.ProductIdColumn.ColumnName].ToString(),
                        VAT = reader[ProductStockTable.VATColumn.ColumnName] as decimal? ?? 0,
                        PurchasePrice = reader[ProductStockTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        SellingPrice = reader[ProductStockTable.SellingPriceColumn.ColumnName] as decimal? ?? 0,
                        CST = reader[ProductStockTable.CSTColumn.ColumnName] as decimal? ?? 0,
                        VendorPurchaseItem1 =
                        {
                               Quantity = reader[VendorPurchaseItemTable.QuantityColumn.FullColumnName] as decimal? ?? 0,
                               Discount = reader[VendorPurchaseItemTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
                               FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.FullColumnName] as int? ?? 0,
                               PackagePurchasePrice = reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.FullColumnName] as decimal? ?? 0,
                               PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.FullColumnName] as decimal? ?? 0,
                               PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0
                        },
                        SalesItem1 =
                        {
                            SellingPrice = reader[SalesItemTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                            Quantity = reader[SalesItemTable.QuantityColumn.FullColumnName] as decimal? ?? 0
                        },
                        Product1 =
                        {
                            Id = reader[ProductTable.IdColumn.FullColumnName].ToString(),
                            Name = reader[ProductTable.NameColumn.FullColumnName].ToString(),
                            CommodityCode = reader[ProductTable.CommodityCodeColumn.FullColumnName].ToString(),
                        },
                        Sale1 = { Discount = reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0 }
                    };
                    list.Add(stockInventory);
                }
            }
            var tmp = from x in list group x by x.Product1.Name;
            var result1 = from z in tmp
                          select new
                          {
                              Name1 = z.Key,
                              SellingVatAmountTotal1 = z.Sum(x => x.SellingTax)
                          };
            //var finalresult = from r in result1
            //                  join li in list
            //                  on r.Name1 equals li.Product1.Name
            //                  select new
            //                  {
            //                      InputTax = r.InputTax1,
            //                      Name = r.Name1,
            //                      PurchaseItemTotal = r.PurchaseItemTotal1,
            //                      CommodityCode = li.Product1.CommodityCode,
            //                      vat = li.VAT,
            //                      SellingPrice = li.SellingPrice
            //                  };
            var finalresult1 = from p in result1
                               join c in list
                               on p.Name1 equals c.Product1.Name into g
                               from l in g.DefaultIfEmpty()
                               select new
                               {
                                   InputTax = l.PurchaseVatAmount - p.SellingVatAmountTotal1,
                                   Name = p.Name1,
                                   PurchaseItemTotal = l.PurchaseAmountWithTax,
                                   CommodityCode = l.Product1.CommodityCode,
                                   vat = l.VAT,
                                   SellingPrice = l.SellingPrice
                               };
            var rs2 = from v in finalresult1
                      group v by v.Name into gps
                      select gps.OrderByDescending(p => p.vat).First();
            var stockdata = new List<StockInventoryData>();
            foreach (var x1 in rs2)
            {
                var sd = new StockInventoryData();
                sd.Name = x1.Name.ToString();
                sd.InputTax = x1.InputTax;
                sd.PurchaseItemTotal = (decimal)x1.PurchaseItemTotal;
                sd.vat = (decimal)x1.vat;
                sd.CommodityCode = x1.CommodityCode.ToString();
                sd.SellingPrice = (decimal)x1.SellingPrice;
                stockdata.Add(sd);
            }
            return stockdata;
        }
        //Cst
        /*The below method name changed  and the same method created to convert from direct qry to sp  by Poongodi*/

        public async Task<List<VendorPurchaseItem>> PurchaseReportListCst(string type, string accountId, string instanceId,
               DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_vat_po_annx8_rpt", parms);
            var vendorPurchaseItem = result.Select(x =>
            {
                var vendorPOItem = new VendorPurchaseItem
                {
                    // ProductStockId = x.ProductStockId.ToString(),
                    Quantity = x.Quantity as decimal? ?? 0,
                    POValue = x.POValue as decimal? ?? 0,
                    VATValue = x.CSTValue as decimal? ?? 0,
                    //Category = Convert.ToString(x.Category),
                    UOM = x.UOM,
                    ReportTotal = x.TotalPO as decimal? ?? 0,
                    BuyPurchase = new VendorPurchase
                    {
                        //Id = x.VendorPurchaseId.ToString(),
                        InvoiceNo = x.InvoiceNo.ToString(),
                        InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                        Discount = x.VendorPODiscount as decimal? ?? 0,
                        GoodsRcvNo = Convert.ToString(x.GRNNo),
                        CreatedAt = Convert.ToDateTime(x.PoDate),
                        Select = Convert.ToString(x.AggreementOrderNo),
                        Select1 = Convert.ToString(x.AggreementOrderDt),
                        Comments = Convert.ToString(x.Transport),
                        FormType = Convert.ToString(x.FormType),
                        SalePurpose = Convert.ToString(x.SalePurpose),
                        Vendor =
                        {
                        Name = Convert.ToString( x.VendorName),
                        TinNo = Convert.ToString(x.VendorTinNo),
                        Address = Convert.ToString(x.VendorAddress),
                        Area = Convert.ToString(x.VendorArea),
                        State = Convert.ToString(x.VendorState),
                    }
                    },
                    ProductStock = new ProductStock
                    {
                        SellingPrice = x.SellingPrice as decimal? ?? 0,
                        //VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                        CST = x.CST as decimal? ?? 0,
                        Product = { CommodityCode = x.Commoditycode.ToString() }
                    }
                };
                /*vendorPurchaseItem.ReportTotal = (((vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty) * vendorPurchaseItem.PackagePurchasePrice) -
                                                                              ((vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty)) *
                                                                               (vendorPurchaseItem.Discount / 100))) + vendorPurchaseItem.VatPrice;
                 */
                return vendorPOItem;
            });
            return vendorPurchaseItem.ToList();
        }
        /*The below method name changed  and the same method created to convert from direct qry to sp  by Poongodi*/
        public async Task<List<VendorPurchaseReturnItem>> PurchaseReturnReportListCst_old(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorReturnItemTable.ProductStockIdColumn, VendorReturnItemTable.QuantityColumn);
            qb.JoinBuilder.Join(VendorReturnTable.Table, VendorReturnTable.IdColumn, VendorReturnItemTable.VendorReturnIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorReturnTable.Table, VendorReturnTable.IdColumn, VendorReturnTable.ReturnNoColumn, VendorReturnTable.ReturnDateColumn);
            qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorReturnTable.VendorPurchaseIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorPurchaseTable.VendorIdColumn, VendorPurchaseTable.InvoiceDateColumn, VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.DiscountColumn);
            qb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.VendorPurchaseIdColumn, VendorPurchaseTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.IdColumn, VendorPurchaseItemTable.QuantityColumn,
                VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.MobileColumn, VendorTable.EmailColumn, VendorTable.AddressColumn, VendorTable.AreaColumn, VendorTable.TinNoColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.CSTColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.CommodityCodeColumn);
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from.ToFormat());
                qb.Parameters.Add("FilterToDate", to.ToFormat());
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CustomDateTime.IST.ToFormat());
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                }
            }
            qb.ConditionBuilder.And(VendorReturnTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(VendorReturnTable.InstanceIdColumn, instanceId);
            qb.SelectBuilder.SortByDesc(VendorReturnTable.CreatedAtColumn);
            var list = new List<VendorPurchaseReturnItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                    {
                        ProductStockId = reader[VendorReturnItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = (decimal)reader[VendorReturnItemTable.QuantityColumn.ColumnName],
                        VendorPurchaseReturn =
                        {
                            Id = reader[VendorReturnTable.IdColumn.FullColumnName].ToString(),
                            ReturnNo = reader[VendorReturnTable.ReturnNoColumn.FullColumnName].ToString(),
                            ReturnDate = Convert.ToDateTime(reader[VendorReturnTable.ReturnDateColumn.FullColumnName]),
                            VendorPurchase =
                            {
                                Vendor =
                                {
                                    Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
                                    Mobile = reader[VendorTable.MobileColumn.FullColumnName].ToString(),
                                    Email = reader[VendorTable.EmailColumn.FullColumnName].ToString(),
                                    Address = reader[VendorTable.AddressColumn.FullColumnName].ToString(),
                                    Area = reader[VendorTable.AreaColumn.FullColumnName].ToString(),
                                    TinNo = reader[VendorTable.TinNoColumn.FullColumnName].ToString()
                                },
                                Discount = (decimal) reader[VendorPurchaseTable.DiscountColumn.FullColumnName],
                                InvoiceNo = reader[VendorPurchaseTable.InvoiceNoColumn.FullColumnName].ToString(),
                                InvoiceDate = Convert.ToDateTime(reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName]),
                            }
                        },
                        ProductStock =
                        {
                            SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                    CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0,
                            Product = {CommodityCode = reader[ProductTable.CommodityCodeColumn.FullColumnName].ToString()},
                        },
                        VendorPurchaseItem =
                        {
                            Quantity = reader[VendorPurchaseItemTable.QuantityColumn.FullColumnName] as decimal? ?? 0,
                            PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0,
                            PackagePurchasePrice =
                            reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.FullColumnName] as decimal? ?? 0,
                            PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.FullColumnName] as decimal? ?? 0,
                            PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.FullColumnName] as decimal? ?? 0,
                            Discount = reader[VendorPurchaseItemTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
                            FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.FullColumnName] as int? ?? 0,
                        },
                        ReturnPurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0,
                    };
                    vendorPurchaseReturnItem.VendorPurchaseItem.ReportTotal = (vendorPurchaseReturnItem.VendorPurchaseItem.Total * vendorPurchaseReturnItem.ProductStock.CST) / 100;
                    vendorPurchaseReturnItem.VendorPurchaseReturn.SubTotal = vendorPurchaseReturnItem.VendorPurchaseItem.Total + vendorPurchaseReturnItem.VendorPurchaseItem.ReportTotal;
                    vendorPurchaseReturnItem.Total = ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity) - ((vendorPurchaseReturnItem.ReturnPurchasePrice * vendorPurchaseReturnItem.Quantity) * (vendorPurchaseReturnItem.VendorPurchaseReturn.VendorPurchase.Discount / 100))) + vendorPurchaseReturnItem.VatPrice;
                    list.Add(vendorPurchaseReturnItem);
                }
            }
            return list;
        }
        public async Task<List<VendorPurchaseReturnItem>> PurchaseReturnReportListCst(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_vat_poreturn_annx9_rpt", parms);
            var vendorPurchaseReturnItems = result.Select(x =>
            {
                var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                {
                    // ReportTotal = x.ReturnVat,
                    Total = x.ReturnVat as decimal? ?? 0,
                    VendorPurchaseReturn =
                        {
                            ReturnNo = Convert.ToString(x.ReturnNo),
                            ReturnDate = Convert.ToDateTime(x.ReturnDate),
                            VendorPurchase =
                            {
                                Vendor =
                                {
                                    Name =Convert.ToString( x.VendorName),
                                    TinNo =Convert.ToString( x.TinNo),
                                    Address = x.VendorAddress,
                                    State = x.VendorState
                                },
                                InvoiceNo = Convert.ToString(x.InvoiceNo),
                                InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                                 FormType = Convert.ToString(x.FormType),
                                 Comments =  Convert.ToString(x.FormNo),
                            },
                         //ReturnPurchasePrice =  x.ReturnValue as decimal? ?? 0,
                SubTotal = x.ReturnValue 
                //vendorPurchaseReturnItem.Total = x.ReturnValue
            },
                    ProductStock =
                {
                              CST  =x.CST as decimal? ??0,
                              Product = { CommodityCode = x.Commoditycode.ToString() }
                },
                    VendorPurchaseItem =
                        {
                            PurchasePrice =  x.POValue as decimal? ?? 0,
                            VATValue= x.ReturnCST ,
                            ReportTotal =x.POValue + x.POCST,
                            POValue =x.POValue
                        }
                };
                return vendorPurchaseReturnItem;
            });
            return vendorPurchaseReturnItems.ToList();
        }
        //
        #region
        //The below method commented and the newmethod created to convert from direct query to SP - by poongodi
        /*
        public async Task<List<SalesItem>> LocalSalesReportListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            /* var qb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);
             qb.AddColumn(SalesItemTable.ProductStockIdColumn, SalesItemTable.QuantityColumn);
             qb.JoinBuilder.Join(SalesTable.Table, SalesTable.IdColumn, SalesItemTable.SalesIdColumn);
             qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.IdColumn, SalesTable.InvoiceNoColumn, SalesTable.InvoiceDateColumn, SalesTable.NameColumn, SalesTable.EmailColumn, SalesTable.MobileColumn, SalesTable.DiscountColumn, SalesTable.DoctorNameColumn, SalesTable.AddressColumn, SalesTable.PaymentTypeColumn);
             qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesItemTable.ProductStockIdColumn);
             qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn);
             qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
             qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.CommodityCodeColumn, ProductTable.ScheduleColumn, ProductTable.TypeColumn, ProductTable.ManufacturerColumn);
             if (string.IsNullOrEmpty(type))
             {
                 qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                 qb.Parameters.Add("FilterFromDate", from.ToFormat());
                 qb.Parameters.Add("FilterToDate", to.ToFormat());
             }
             else
             {
                 switch (type.ToUpper())
                 {
                     case "TODAY":
                         qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                         qb.Parameters.Add("FilterFromDate", CustomDateTime.IST.ToFormat());
                         qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                         break;
                     case "WEEK":
                         qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                         qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                         qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                         break;
                     case "MONTH":
                         qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                         qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                         qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                         break;
                 }
             }
             qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
             qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
             qb.ConditionBuilder.And(ProductStockTable.VATColumn, 0, CriteriaEquation.Greater);
             qb.SelectBuilder.SortByDesc(SalesTable.InvoiceDateColumn);
             var list = new List<SalesItem>();
             using (var reader = await QueryExecuter.QueryAsyc(qb))
             {
                 while (reader.Read())
                 {
                     var salesItem = new SalesItem
                     {
                         ProductStockId = reader[SalesItemTable.ProductStockIdColumn.ColumnName].ToString(),
                         Quantity = reader[SalesItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                         Sales =
                         {
                             Id = reader[SalesTable.IdColumn.FullColumnName].ToString(),
                             InvoiceNo = reader[SalesTable.InvoiceNoColumn.FullColumnName].ToString(),
                             InvoiceDate = Convert.ToDateTime(reader[SalesTable.InvoiceDateColumn.FullColumnName]),
                             Name = reader[SalesTable.NameColumn.FullColumnName].ToString(),
                             DoctorName = reader[SalesTable.DoctorNameColumn.FullColumnName].ToString(),
                             Address = reader[SalesTable.AddressColumn.FullColumnName].ToString(),
                             PaymentType = reader[SalesTable.PaymentTypeColumn.FullColumnName].ToString()
                         }
                     };
                     if (reader[SalesTable.EmailColumn.FullColumnName].ToString() != "N/A")
                     {
                         salesItem.Sales.Email = reader[SalesTable.EmailColumn.FullColumnName].ToString();
                     }
                     salesItem.Sales.Discount = reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0;
                     salesItem.Sales.Mobile = reader[SalesTable.MobileColumn.FullColumnName].ToString();
                     salesItem.ProductStock.SellingPrice =
                        reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0;
                     salesItem.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;
                     salesItem.ProductStock.Product.CommodityCode = reader[ProductTable.CommodityCodeColumn.FullColumnName].ToString();
                     salesItem.ReportTotal = (salesItem.ProductStock.SellingPrice * salesItem.Quantity) -
                                             (salesItem.ProductStock.SellingPrice * salesItem.Quantity *
                                              (salesItem.Sales.Discount / 100));
                     salesItem.ReportTotal = Math.Round((decimal)salesItem.ReportTotal);
                     salesItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                     salesItem.ProductStock.ExpireDate = (DateTime)reader[ProductStockTable.ExpireDateColumn.FullColumnName];
                     //Product details
                     salesItem.ProductStock.Product.Manufacturer = reader[ProductTable.ManufacturerColumn.FullColumnName].ToString();
                     salesItem.ProductStock.Product.Schedule = reader[ProductTable.ScheduleColumn.FullColumnName].ToString();
                     salesItem.ProductStock.Product.Type = reader[ProductTable.TypeColumn.FullColumnName].ToString();
                     list.Add(salesItem);
                 }
             }  */
        /* ----------- 
        var filterFromDate = CustomDateTime.IST.ToFormat();
        var filterToDate = CustomDateTime.IST.ToFormat();
        if (string.IsNullOrEmpty(type))
        {
            filterFromDate = from.ToFormat();
            filterToDate = to.ToFormat();
        }
        else
        {
            switch (type.ToUpper())
            {
                case "TODAY":
                    filterFromDate = CustomDateTime.IST.ToFormat();
                    filterToDate = CustomDateTime.IST.ToFormat();
                    break;
                case "WEEK":
                    filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                    filterToDate = CustomDateTime.IST.ToFormat();
                    break;
                case "MONTH":
                    filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                    filterToDate = CustomDateTime.IST.ToFormat();
                    break;
            }
        }
        string query =
            $@"select sales.invoicedate AS InvoiceDate,sales.invoiceno as InvoiceNo,sales.Discount as  salesDiscount,sales.Name,sales.DoctorName,sales.Address,salesitem.quantity,convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END)) As SellingPrice,salesitem.Discount,ps.BatchNo,ps.ExpireDate,ps.VAT,ps.Stock,ps.PurchasePrice,p.Name as ProductName, p.Manufacturer, p.Schedule,p.CommodityCode,
convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) *salesitem.quantity) As InvoiceAmount
                , sales.Discount as Discount,ISNULL(ISNULL(vpi.PurchasePrice,ps.PurchasePrice) * (ps.Stock),0)  As CostPrice
                ,(salesitem.Quantity * (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) * isnull(salesitem.Discount,0) / 100) as DiscountSum
                from sales sales 
                join salesitem salesitem on sales.id= salesitem.salesid
                join productstock ps on ps.id=salesitem.productstockid
                inner join Product p on p.Id=ps.ProductId
                left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem group by productstockId) vpi on ps.id=vpi.productstockid 					
                WHERE sales.InstanceId = '{instanceId}' AND sales.AccountId = '{accountId}' and ps.VAT>0
                AND Convert(date,sales.invoicedate) BETWEEN '{filterFromDate}' AND '{filterToDate}'
                ORDER BY sales.CreatedAt desc";
        var qb = QueryBuilderFactory.GetQueryBuilder(query);
        var list = new List<SalesItem>();
        using (var reader = await QueryExecuter.QueryAsyc(qb))
        {
            while (reader.Read())
            {
                var salesItem = new SalesItem
                {
                    Sales =
                    {
                        InvoiceNo = reader["InvoiceNo"].ToString(),
                        InvoiceDate = Convert.ToDateTime(reader["InvoiceDate"]),
                        Name = reader["Name"].ToString(),
                        DoctorName = reader["DoctorName"].ToString(),
                        Address = reader["Address"].ToString(),
                    },
                    ProductStock =
                    {
                         BatchNo = reader["BatchNo"].ToString(),
                         ExpireDate = Convert.ToDateTime(reader["ExpireDate"]),
                         Stock=reader["Stock"] as decimal? ?? 0,
                         VAT=reader["VAT"] as decimal? ?? 0,
                         PurchasePrice=reader["PurchasePrice"] as decimal? ?? 0,
                         CostPrice=reader["CostPrice"] as decimal? ?? 0,
                        Product=
                        {
                            Name=reader["ProductName"].ToString(),
                            Manufacturer=reader["Manufacturer"].ToString(),
                            Schedule=reader["Schedule"].ToString(),
                            CommodityCode=reader["CommodityCode"].ToString(),
                        }
                    }
                };
                salesItem.SellingPrice = reader["SellingPrice"] as decimal? ?? 0;
                salesItem.Quantity = reader["quantity"] as decimal? ?? 0;
                salesItem.Discount = reader["Discount"] as decimal? ?? 0;
                salesItem.Sales.Discount = (decimal)reader["salesDiscount"];
                decimal? discountSum = reader["DiscountSum"] as decimal? ?? 0;
                salesItem.Sales.InvoiceAmount = reader["InvoiceAmount"] as decimal? ?? 0;
                salesItem.ProductStock.TotalCostPrice = salesItem.SellingPrice * salesItem.Quantity;
                salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
                //salesItem.Sales.FinalValue = Math.Round((decimal)salesItem.Sales.FinalValue);
                //salesItem.ReportTotal = (decimal)salesItem.Sales.FinalValue;
                salesItem.ReportTotal = salesItem.Sales.FinalValue = Math.Round((decimal)(salesItem.Sales.FinalValue), 0, MidpointRounding.AwayFromZero);
                salesItem.ProductStock.TotalCostPrice = salesItem.ProductStock.PurchasePrice * salesItem.Quantity;
                if (salesItem.ProductStock.TotalCostPrice > 0)
                {
                    // string Tval=String.Format("{0:0.##}", salesItem.ReportTotal);
                    //string TCost = String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice);
                    salesItem.SalesProfit = ((decimal.Parse(String.Format("{0:0.##}", salesItem.ReportTotal)) - decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) / decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) * 100;
                }
                //salesItem.SalesProfit = ((salesItem.ReportTotal - salesItem.ProductStock.TotalCostPrice) / salesItem.ProductStock.TotalCostPrice) * 100;
                else
                    salesItem.SalesProfit = 0;
                list.Add(salesItem);
            }
        }
        return list;
    }
    */
        #endregion
        //Manivannan for New Multi-instance reports
        public async Task<List<SalesSummaryDayWiseReport>> GetSalesSummaryListData(string accountId, string type, bool allInstances, List<string> instanceIds, DateTime fromDate, DateTime toDate)
        {
            //var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            if (allInstances)
            {
                parms.Add("@InstanceInd", 0);
            }
            else
            {
                parms.Add("@InstanceInd", 1);
                parms.Add("InstanceIds", instanceIds);
            }
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", fromDate);
            parms.Add("EndDate", toDate);
            var result = await sqldb.ExecuteProcedureAsync<SalesSummaryDayWiseReport>("usp_GetSales_DayWise_LocationWise", parms);
            return result.ToList();
        }
        public async Task<List<SalesItem>> LocalSalesReportListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_vat_Sales_annx15_rpt", parms);
            var salesItems = result.Select(x =>
            {
                var SalesItem = new SalesItem
                {
                    TinNo = Convert.ToString(x.BuyerTin),
                    Category = Convert.ToString(x.Category),
                    VATValue = x.VatValue as decimal? ?? 0,
                    Sales =
                        {
                            InvoiceNo =Convert.ToString(x.InvoiceNo),
                            InvoiceDate = Convert.ToDateTime(x.Invoicedate),
                             Name = Convert.ToString(x.BuyerName),
                            FinalValue = (x.VatValue + x.SalesValue ) as decimal? ?? 0
                         },
                    ProductStock =
                {
                              VAT = x.VAT as decimal? ?? 0,
                              Product = { CommodityCode = x.Commoditycode.ToString() },
                        TotalCostPrice = x.SalesValue as decimal? ?? 0
                },
                };
                return SalesItem;
            });
            return salesItems.ToList();
        }
        //
        public async Task<List<SalesItem>> GetDoctorSalesList(string AccId, string DoctorId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccId);
            parms.Add("DoctorId", DoctorId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetDoctorSalesReport", parms);
            //decimal? discountSum = 0;
            var salesItemList = result.Select(x =>
            {
                var salesItem = new SalesItem
                {
                    Sales = new Sales
                    {
                        InvoiceDate = x.InvoiceDate as DateTime? ?? DateTime.UtcNow,
                        InvoiceNo = x.InvoiceNo as string ?? "",
                        DoctorName = x.DoctorName as string ?? "",
                        DoctorMobile = x.DoctorMobile as string ?? "",
                        PaymentType = x.PaymentType as string ?? "",
                        Name = x.Name as string ?? "",
                        Mobile = x.Mobile as string ?? "",
                        DeliveryType = x.DeliveryType as string ?? "",
                        FinalValue = x.InvoiceAmount as decimal? ?? 0,
                        TaxRefNo = Convert.ToInt32(x.TaxRefNo)  //x.TaxRefNo as int? ?? 0,
                    },
                    Quantity = x.Quantity as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    SellingPrice = x.SellingPrice as decimal? ?? 0,
                    BatchNo = x.BatchNo as string ?? "",
                    //SalesDiscountValue = x.DiscountSum as decimal? ?? 0,
                    ProductStock = new ProductStock
                    {
                        ExpireDate = x.ExpireDate as DateTime? ?? DateTime.UtcNow,
                        VAT = x.VAT as decimal? ?? 0,
                        GstTotal = x.GstTotal as decimal? ?? 0,
                        PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                        SellingPrice = x.SellingPrice as decimal? ?? 0,
                        VatAmount = x.TaxAmount as decimal? ?? 0,
                        productName = x.ProductName as string ?? "",
                        Product = new Contract.Infrastructure.Master.Product
                        {
                            Name = x.ProductName as string ?? "",
                            Manufacturer = x.Manufacturer as string ?? "",
                            Schedule = x.Schedule as string ?? "",
                            Type = x.Type as string ?? ""
                        }
                    }
                };
                //salesItem.ProductStock.VatAmount = (salesItem.SellingPrice * salesItem.ProductStock.VAT / 100) * salesItem.Quantity;
                //discountSum = x.DiscountSum as decimal? ?? 0;
                //if (salesItem.Sales.TaxRefNo == 1) //salesItem.ProductStock.GstTotal > 0 &&   // Removed for GST BillNo
                //{
                //    salesItem.ProductStock.VAT = salesItem.ProductStock.GstTotal;
                //    salesItem.ProductStock.VatAmount = ((salesItem.Sales.FinalValue - discountSum) - (((salesItem.Sales.FinalValue - discountSum) * 100) / (salesItem.ProductStock.GstTotal + 100)));
                //}
                //else
                //    salesItem.ProductStock.VatAmount = ((salesItem.Sales.FinalValue - discountSum) * salesItem.ProductStock.VAT / 100);

                //salesItem.Sales.FinalValue = Math.Round((decimal)salesItem.Sales.FinalValue - (decimal)discountSum);
                salesItem.Sales.FinalValue = x.TotalAmount as decimal? ?? 0;
                return salesItem;
            });
            return salesItemList.ToList();
        }
        public async Task<List<SalesItem>> LocalSalesReportListData1(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesItemTable.ProductStockIdColumn, SalesItemTable.QuantityColumn);
            qb.JoinBuilder.Join(SalesTable.Table, SalesTable.IdColumn, SalesItemTable.SalesIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.IdColumn, SalesTable.InvoiceNoColumn, SalesTable.InvoiceDateColumn, SalesTable.NameColumn, SalesTable.EmailColumn, SalesTable.MobileColumn, SalesTable.DiscountColumn, SalesTable.DoctorNameColumn, SalesTable.AddressColumn, SalesTable.PaymentTypeColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.CommodityCodeColumn, ProductTable.ScheduleColumn, ProductTable.TypeColumn, ProductTable.ManufacturerColumn);
            if (string.IsNullOrEmpty(type))
            {
                qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", from.ToFormat());
                qb.Parameters.Add("FilterToDate", to.ToFormat());
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", CustomDateTime.IST.ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "WEEK":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                    case "MONTH":
                        qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn.ToDateColumn(), CriteriaEquation.Between);
                        qb.Parameters.Add("FilterFromDate", new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat());
                        qb.Parameters.Add("FilterToDate", CustomDateTime.IST.ToFormat());
                        break;
                }
            }
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(ProductStockTable.VATColumn, 0, CriteriaEquation.Greater);
            qb.SelectBuilder.SortByDesc(SalesTable.InvoiceDateColumn);
            var list = new List<SalesItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesItem
                    {
                        ProductStockId = reader[SalesItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = (decimal)reader[SalesItemTable.QuantityColumn.ColumnName],
                        Sales =
                        {
                            Id = reader[SalesTable.IdColumn.FullColumnName].ToString(),
                            InvoiceNo = reader[SalesTable.InvoiceNoColumn.FullColumnName].ToString(),
                            InvoiceDate = Convert.ToDateTime(reader[SalesTable.InvoiceDateColumn.FullColumnName]),
                            Name = reader[SalesTable.NameColumn.FullColumnName].ToString(),
                            DoctorName = reader[SalesTable.DoctorNameColumn.FullColumnName].ToString(),
                            Address = reader[SalesTable.AddressColumn.FullColumnName].ToString(),
                            PaymentType = reader[SalesTable.PaymentTypeColumn.FullColumnName].ToString()
                        }
                    };
                    if (reader[SalesTable.EmailColumn.FullColumnName].ToString() != "N/A")
                    {
                        salesItem.Sales.Email = reader[SalesTable.EmailColumn.FullColumnName].ToString();
                    }
                    salesItem.Sales.Discount = (decimal)reader[SalesTable.DiscountColumn.FullColumnName];
                    salesItem.Sales.Mobile = reader[SalesTable.MobileColumn.FullColumnName].ToString();
                    salesItem.ProductStock.SellingPrice =
                        (decimal)reader[ProductStockTable.SellingPriceColumn.FullColumnName];
                    salesItem.ProductStock.VAT = (decimal)reader[ProductStockTable.VATColumn.FullColumnName];
                    salesItem.ProductStock.Product.CommodityCode = reader[ProductTable.CommodityCodeColumn.FullColumnName].ToString();
                    salesItem.ReportTotal = (salesItem.ProductStock.SellingPrice * salesItem.Quantity) -
                                            (salesItem.ProductStock.SellingPrice * salesItem.Quantity *
                                             (salesItem.Sales.Discount / 100));
                    salesItem.ReportTotal = Math.Round((decimal)salesItem.ReportTotal, MidpointRounding.AwayFromZero);
                    salesItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    salesItem.ProductStock.ExpireDate = (DateTime)reader[ProductStockTable.ExpireDateColumn.FullColumnName];
                    //Product details
                    salesItem.ProductStock.Product.Manufacturer = reader[ProductTable.ManufacturerColumn.FullColumnName].ToString();
                    salesItem.ProductStock.Product.Schedule = reader[ProductTable.ScheduleColumn.FullColumnName].ToString();
                    salesItem.ProductStock.Product.Type = reader[ProductTable.TypeColumn.FullColumnName].ToString();
                    list.Add(salesItem);
                }
            }
            return list;
        }
        public async Task<List<SalesItem>> LocalSaleConsolidatedVATListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_Get_Consolidated_Sales_VAT", parms);
            var salesItems = result.Select(x =>
            {
                var SalesItem = new SalesItem
                {
                    VATValue = x.VatValue as decimal? ?? 0,
                    VAT5_SaleValue = x.Vat5_SalesValue,
                    VAT5_Value = x.Vat5_Value,
                    VAT5_TotalValue = x.Vat5_TotalValue,
                    VAT145_SaleValue = x.Vat145_SalesValue,
                    VAT145_Value = x.Vat145_Value,
                    VAT145_TotalValue = x.Vat145_TotalValue,
                    VATExcempted_SaleValue = x.Vat0_SalesValue,
                    Sales =
                        {
                            InvoiceNo =Convert.ToString(x.InvoiceNo),
                            InvoiceDate = Convert.ToDateTime(x.Invoicedate),
                            FinalValue = ( x.SalesValue ) as decimal? ?? 0
                         },
                };
                return SalesItem;
            });
            return salesItems.ToList();
        }
        //Added by Bikas for GST Report 06/06/18
        public async Task<List<SalesCoordinateGst>> LocalSalesConsolidatedGSTListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("salesGstReport", parms);
            var salesCoordinateGst = result.Select(x =>
            {
                var SalesCoordinateGst = new SalesCoordinateGst
                {
                    GSTValue = x.GstAmount as decimal? ?? 0,
                    Gst0_SaleValue = x.Gst0_SaleValue,
                    Gst0_Value = x.Gst0_Value,
                    Gst0_TotalSaleValue = x.Gst0_TotalSaleValue,
                    Gst5_SaleValue = x.Gst5_SaleValue,
                    Gst5_Value = x.Gst5_Value,
                    Gst5_TotalSaleValue = x.Gst5_TotalSaleValue,
                    Gst12_SaleValue = x.Gst12_SaleValue,
                    Gst12_Value = x.Gst12_Value,
                    Gst12_TotalSaleValue = x.Gst12_TotalSaleValue,
                    Gst18_SaleValue = x.Gst18_SaleValue,
                    Gst18_Value = x.Gst18_Value,
                    Gst18_TotalSaleValue = x.Gst18_TotalSaleValue,
                    Gst28_SaleValue = x.Gst28_SaleValue,
                    Gst28_Value = x.Gst28_Value,
                    Gst28_TotalSaleValue = x.Gst28_TotalSaleValue,
                    GstOther_SaleValue = x.GstOther_SaleValue,
                    GstOther_Value = x.GstOther_Value,
                    GstOther_TotalSaleValue = x.GstOther_TotalSaleValue,
                    gstExcempted_SaleValue = x.Gst0_SaleValue,
                    InvoiceNo =Convert.ToString(x.InvoiceNo),
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    FinalValue = ( x.SalesAmount ) as decimal? ?? 0
                };
                return SalesCoordinateGst;
            });
            return salesCoordinateGst.ToList();
        }

        //Added by Bikas for GST Retutn Report 07/06/18
        public async Task<List<SalesCoordinateGstReturn>> LocalSalesConsolidatedGSTReturnListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("salesReturnGstReport", parms);
            var salesCoordinateGstReturn = result.Select(x =>
            {
                var SalesCoordinateGstReturn = new SalesCoordinateGstReturn
                {
                    GSTReturnValue = x.GstAmount as decimal? ?? 0,
                    Gst0_ReturnValue = x.Gst0_ReturnValue,
                    Gst0_Value = x.Gst0_Value,
                    Gst0_TotalReturnValue = x.Gst0_TotalReturnValue,
                    Gst5_ReturnValue = x.Gst5_ReturnValue,
                    Gst5_Value = x.Gst5_Value,
                    Gst5_TotalReturnValue = x.Gst5_TotalReturnValue,
                    Gst12_ReturnValue = x.Gst12_ReturnValue,
                    Gst12_Value = x.Gst12_Value,
                    Gst12_TotalReturnValue = x.Gst12_TotalReturnValue,
                    Gst18_ReturnValue = x.Gst18_ReturnValue,
                    Gst18_Value = x.Gst18_Value,
                    Gst18_TotalReturnValue = x.Gst18_TotalReturnValue,
                    Gst28_ReturnValue = x.Gst28_ReturnValue,
                    Gst28_Value = x.Gst28_Value,
                    Gst28_TotalReturnValue = x.Gst28_TotalReturnValue,
                    GstOther_ReturnValue = x.GstOther_ReturnValue,
                    GstOther_Value = x.GstOther_Value,
                    GstOther_TotalReturnValue = x.GstOther_TotalReturnValue,
                    gstExcempted_ReturnValue = x.Gst0_ReturnValue,
                    ReturnNo =Convert.ToString(x.ReturnNo),
                    ReturnDate = Convert.ToDateTime(x.ReturnDate),
                    FinalValue = ( x.ReturnAmount) as decimal? ?? 0
                };
                return SalesCoordinateGstReturn;
            });
            return salesCoordinateGstReturn.ToList();
        }

        //Added by Bikas for GST Report 20/06/18
        public async Task<List<PurchaseConsolidatedGst>> LocalPurchaseConsolidatedGSTListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("purchaseGstReport", parms);
            var purchaseConsolidatedGst = result.Select(x =>
            {
                var PurchaseConsolidatedGst = new PurchaseConsolidatedGst
                {
                    GSTValue = x.GstAmount as decimal? ?? 0,
                    Gst0_PurchaseValue = x.Gst0_PurchaseValue,
                    Gst0_Value = x.Gst0_Value,
                    Gst0_TotalPurchaseValue = x.Gst0_TotalPurchaseValue,
                    Gst5_PurchaseValue = x.Gst5_PurchaseValue,
                    Gst5_Value = x.Gst5_Value,
                    Gst5_TotalPurchaseValue = x.Gst5_TotalPurchaseValue,
                    Gst12_PurchaseValue = x.Gst12_PurchaseValue,
                    Gst12_Value = x.Gst12_Value,
                    Gst12_TotalPurchaseValue = x.Gst12_TotalPurchaseValue,
                    Gst18_PurchaseValue = x.Gst18_PurchaseValue,
                    Gst18_Value = x.Gst18_Value,
                    Gst18_TotalPurchaseValue = x.Gst18_TotalPurchaseValue,
                    Gst28_PurchaseValue = x.Gst28_PurchaseValue,
                    Gst28_Value = x.Gst28_Value,
                    Gst28_TotalPurchaseValue = x.Gst28_TotalPurchaseValue,
                    GstOther_PurchaseValue = x.GstOther_PurchaseValue,
                    GstOther_Value = x.GstOther_Value,
                    GstOther_TotalPurchaseValue = x.GstOther_TotalPurchaseValue,
                    gstExcempted_PurchaseValue = x.Gst0_PurchaseValue,
                    InvoiceNo = Convert.ToString(x.InvoiceNo),
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    FinalValue = (x.SalesAmount) as decimal? ?? 0
                };
                return PurchaseConsolidatedGst;
            });
            return purchaseConsolidatedGst.ToList();
        }

        //Added by Bikas for GST Report 20/06/18
        public async Task<List<PurchaseReturnConsolidatedGst>> LocalPurchaseReturnConsolidatedGSTListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("purchaseReturnGstReport", parms);
            var purchaseReturnConsolidatedGst = result.Select(x =>
            {
                var PurchaseReturnConsolidatedGst = new PurchaseReturnConsolidatedGst
                {
                    GSTValue = x.GstAmount as decimal? ?? 0,
                    Gst0_PurchaseReturnValue = x.Gst0_PurchaseReturnValue,
                    Gst0_Value = x.Gst0_Value,
                    Gst0_TotalPurchaseReturnValue = x.Gst0_TotalPurchaseReturnValue,
                    Gst5_PurchaseReturnValue = x.Gst5_PurchaseReturnValue,
                    Gst5_Value = x.Gst5_Value,
                    Gst5_TotalPurchaseReturnValue = x.Gst5_TotalPurchaseReturnValue,
                    Gst12_PurchaseReturnValue = x.Gst12_PurchaseReturnValue,
                    Gst12_Value = x.Gst12_Value,
                    Gst12_TotalPurchaseReturnValue = x.Gst12_TotalPurchaseReturnValue,
                    Gst18_PurchaseReturnValue = x.Gst18_PurchaseReturnValue,
                    Gst18_Value = x.Gst18_Value,
                    Gst18_TotalPurchaseReturnValue = x.Gst18_TotalPurchaseReturnValue,
                    Gst28_PurchaseReturnValue = x.Gst28_PurchaseReturnValue,
                    Gst28_Value = x.Gst28_Value,
                    Gst28_TotalPurchaseReturnValue = x.Gst28_TotalPurchaseReturnValue,
                    GstOther_PurchaseReturnValue = x.GstOther_PurchaseReturnValue,
                    GstOther_Value = x.GstOther_Value,
                    GstOther_TotalPurchaseReturnValue = x.GstOther_TotalPurchaseReturnValue,
                    gstExcempted_PurchaseReturnValue = x.Gst0_PurchaseReturnValue,
                    ReturnNo = Convert.ToString(x.ReturnNo),
                    ReturnDate = Convert.ToDateTime(x.ReturnDate),
                    FinalValue = (x.ReturnAmount) as decimal? ?? 0
                };
                return PurchaseReturnConsolidatedGst;
            });
            return purchaseReturnConsolidatedGst.ToList();
        }


        //Added by Bikas for GST Report 22/06/18
        public async Task<List<SalesReportTally>> LocalSalesReportTallyListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("TallySaleReport", parms);
            var salesReportTally = result.Select(x =>
            {
                var SalesReportTally = new SalesReportTally
                {
                    InvoiceNo = Convert.ToString(x.InvoiceNo),
                    Vouchertype = "Sale",
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    Billtype = x.CustomerPaymentType,
                    CustomerName = x.CustomerName,
                    Empty = "",
                    ItemName = x.SaleProductName,
                    Gstin = x.GsTin,
                    HsnCode = x.HsnCode,
                    GstAmount = x.GstAmount,
                    ProductCode = x.Code,
                    Igst = x.Igst,
                    Cgst = x.Cgst,
                    Sgst = x.Sgst,
                    Gstpercentage = x.GstTotal,
                    ItemAmount = x.SellingPrice,
                    Quantity = x.Quantity,
                    Amount = x.Amount,
                    CGSTAmount = x.CGSTAmount,
                    SGSTAmount = x.SGSTAmount,
                    IGSTAmount = "",
                    DiscountValue = x.DiscountValue,
                    RoundOff = x.RoundOff,
                    GrandTotal = (x.TotalAmount) as decimal? ?? 0,
                    StateId = x.StateId,
                    Name = x.Name,
                    VendorType = x.VendorType,
                    VendorTypeDesc = x.VendorTypeDesc,
                    SaleCode = x.salecode,
                    Remark1 = "",
                    Remark2 = ""
                };
                return SalesReportTally;
            });
            return salesReportTally.ToList();
        }
        //Added by Bikas for Tally Report 25/06/18
        public async Task<List<SalesreturnReportTally>> LocalSalesReturnReportTallyListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("TallySaleReturnReport", parms);
            var salesReturnReportTally = result.Select(x =>
            {
                var SalesreturnReportTally = new SalesreturnReportTally
                {
                    InvoiceNo = Convert.ToString(x.InvoiceNo),
                    Vouchertype = "Sales Return",
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    Billtype = x.CustomerPaymentType,
                    CustomerName = x.CustomerName,
                    ItemName = x.SaleProductName,
                    Gstin = x.GsTin,
                    HsnCode = x.HsnCode,
                    GstAmount = x.GstAmount,
                    ProductCode = x.Code,
                    Igst = x.Igst,
                    Cgst = x.Cgst,
                    Sgst = x.Sgst,
                    Gstpercentage = x.GstTotal,
                    ItemAmount = x.MrpSellingPrice,
                    Quantity = x.Quantity,
                    Amount = x.Amount,
                    CGSTAmount = x.CGSTAmount,
                    SGSTAmount = x.SGSTAmount,
                    IGSTAmount = "",
                    DiscountValue = x.DiscountValue,
                    RoundOff = x.RoundOff,
                    GrandTotal = (x.TotalAmount) as decimal? ?? 0,
                    StateId = x.StateId,
                    Name = x.Name,
                    VendorType = x.VendorType,
                    VendorTypeDesc = x.VendorTypeDesc,
                    SaleReturnCode = x.salereturncode,
                    Remark1 = "",
                    Remark2 = ""
                };
                return SalesreturnReportTally;
            });
            return salesReturnReportTally.ToList();
        }

        //Added by Bikas for Tally Report 26/06/18
        public async Task<List<PurchaseReportTally>> LocalPurchaseReportTallyListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("TallypurchaseReport", parms);
            var purchaseReportTally = result.Select(x =>
            {
                var PurchaseReportTally = new PurchaseReportTally
                {
                    InvoiceNo = Convert.ToString(x.InvoiceNo),
                    Vouchertype = "Purchase",
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    Billtype = x.CustomerPaymentType,
                    CustomerName = x.Name,
                    Empty = "",
                    ItemName = x.ProductName,
                    Gstin = x.GsTin,
                    HsnCode = x.HsnCode,
                    GstAmount = x.GstAmount,
                    ProductCode = x.Code,
                    Igst = x.Igst,
                    Cgst = x.Cgst,
                    Sgst = x.Sgst,
                    Gstpercentage = x.GstTotal,
                    ItemAmount = x.PackagePurchasePrice,
                    Quantity = x.Quantity,
                    Amount = x.Amount,
                    CGSTAmount = x.CGSTAmount,
                    SGSTAmount = x.SGSTAmount,
                    IGSTAmount = "",
                    DiscountValue = x.DiscountValue,
                    RoundOff = x.RoundOff,
                    GrandTotal = (x.NetValue) as decimal? ?? 0,
                    StateId = x.StateId,
                    VendorType = x.VendorType,
                    VendorTypeDesc = x.VendorTypeDesc,
                    PurchaseCode = x.purchaseCode,
                    Remark1 = "",
                    Remark2 = ""
                };
                return PurchaseReportTally;
            });
            return purchaseReportTally.ToList();
        }

        //Added by Bikas for Tally Report 27/06/18
        public async Task<List<PurchaseReturnReportTally>> LocalPurchaseReturnReportTallyListData(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("TallypurchaseReturnReport", parms);
            var purchaseReturnReportTally = result.Select(x =>
            {
                var PurchaseReturnReportTally = new PurchaseReturnReportTally
                {
                    ReturnNo = Convert.ToString(x.ReturnNo),
                    ReturnDate = Convert.ToDateTime(x.ReturnDate),
                    Vouchertype = "Purchase Return",
                    CustomerName = x.Name,
                    Empty = "",
                    ItemName = x.ProductName,
                    Gstin = x.GsTin,
                    HsnCode = x.HsnCode,
                    GstAmount = x.GstAmount,
                    ProductCode = x.Code,
                    Igst = x.Igst,
                    Cgst = x.Cgst,
                    Sgst = x.Sgst,
                    Gstpercentage = x.GstTotal,
                    ItemAmount = x.ReturnPurchasePrice,
                    Quantity = x.Quantity,
                    Amount = x.Amount,
                    CGSTAmount = x.CGSTAmount,
                    SGSTAmount = x.SGSTAmount,
                    IGSTAmount = "",
                    DiscountValue = x.DiscountValue,
                    RoundOff = x.RoundOff,
                    GrandTotal = (x.ReturnTotal) as decimal? ?? 0,
                    StateId = x.StateId,
                    VendorType = x.VendorType,
                    VendorTypeDesc = x.VendorTypeDesc,
                    PurchaseReturnCode = x.purchasereturnCode,
                    Remark1 = "",
                    Remark2 = ""
                };
                return PurchaseReturnReportTally;
            });
            return purchaseReturnReportTally.ToList();
        }

        //Added by Bikas for Tally Report 27/06/18
        public async Task<List<JournalReport>> LocalJournalListData(string filter,string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter("", from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Filter", filter);
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetJournalReport", parms);
            var journalReport2 = new List<JournalReport>();
            if (filter == "CustomerReceipts")
            {
                var journalReport = result.Select(x =>
                {
                    var JournalReport = new JournalReport
                    {
                        voucherno = Convert.ToString(x.InvoiceNo),
                        voucherdate = Convert.ToDateTime(x.InvoiceDate),
                        crledger = x.InstanceName,
                        drledger = x.Name,
                        BillType = "Sales Recepit",
                        BillName = x.Name,
                        Amount = x.InvoiceValue,
                        PaymentType = x.PaymentType,
                        Cheque = x.ChequeNo,
                        ChequeDate = x.ChequeDate,
                        CardNo = x.CardNo,
                        CardDate = x.CardDate,
                        Remarks = x.Remarks
                    };

                    return JournalReport;
                });
                journalReport2 = journalReport.ToList();
            }
            else
            {
                var journalReport = result.Select(x =>
                {
                    var JournalReport1 = new JournalReport
                    {
                        voucherno = Convert.ToString(x.InvoiceNo),
                        voucherdate = Convert.ToDateTime(x.InvoiceDate),
                        crledger = x.Name,
                        drledger = x.InstanceName,
                        BillType = "Sales Recepit",
                        BillName = x.Name,
                        Amount = x.InvoiceAmount,
                        PaymentType = x.PaymentMode,
                        Cheque = x.ChequeNo,
                        ChequeDate = x.ChequeDate,
                        Remarks = x.Remarks
                    };
                    return JournalReport1;
                });
                journalReport2 = journalReport.ToList();
            }

            return journalReport2.ToList();
        }

        ///Vendor View
        ///
        public async Task<List<VendorPurchaseItem>> VendorView(string accountId, string instanceId, string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.QuantityColumn,
                    VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                    VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.VatValueColumn, VendorPurchaseItemTable.GstValueColumn, VendorPurchaseItemTable.PackageSizeColumn,
                    VendorPurchaseItemTable.MarkupPercColumn, VendorPurchaseItemTable.SchemeDiscountPercColumn);
            qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn,
                VendorPurchaseItemTable.VendorPurchaseIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn,
                VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.InvoiceDateColumn,
                VendorPurchaseTable.DiscountColumn, VendorPurchaseTable.TaxRefNoColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.IdColumn, VendorTable.NameColumn,
                VendorTable.MobileColumn, VendorTable.EmailColumn, VendorTable.AddressColumn, VendorTable.TinNoColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn,
                VendorPurchaseItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn,
                ProductStockTable.VATColumn, ProductStockTable.CSTColumn, ProductStockTable.GstTotalColumn,
                ProductStockTable.CgstColumn, ProductStockTable.IgstColumn, ProductStockTable.SgstColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            qb.ConditionBuilder.And(VendorPurchaseTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(VendorPurchaseTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(VendorTable.IdColumn, id);

            //Added by Sarubala to remove cancelled purchase on 18-07-17 - start
            var isCancelledCondition = new CustomCriteria(VendorPurchaseTable.CancelStatusColumn, CriteriaCondition.IsNull);
            string isCancelled = "1";
            var cc1 = new CriteriaColumn(VendorPurchaseTable.CancelStatusColumn, isCancelled, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, isCancelledCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);

            //Added by Sarubala to remove cancelled purchase on 18-07-17 - end

            qb.SelectBuilder.SortByDesc(VendorPurchaseTable.InvoiceDateColumn);
            qb.SelectBuilder.SortByDesc(VendorPurchaseTable.CreatedAtColumn);
            var list = new List<VendorPurchaseItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseItem = new VendorPurchaseItem
                    {
                        ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = reader[VendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackagePurchasePrice =
                           reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0,
                        PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] as decimal? ?? 0,
                        GstValue = reader[VendorPurchaseItemTable.GstValueColumn.ColumnName] as decimal? ?? 0,
                        VatValue = reader[VendorPurchaseItemTable.VatValueColumn.ColumnName] as decimal? ?? 0,
                        MarkupPerc = reader[VendorPurchaseItemTable.MarkupPercColumn.ColumnName] as decimal? ?? 0,
                        SchemeDiscountPerc = reader[VendorPurchaseItemTable.SchemeDiscountPercColumn.ColumnName] as decimal? ?? 0,
                        BuyPurchase = new VendorPurchase
                        {
                            Id = reader[VendorPurchaseTable.IdColumn.FullColumnName].ToString(),
                            InvoiceNo = reader[VendorPurchaseTable.InvoiceNoColumn.FullColumnName].ToString(),
                            InvoiceDate =
                                Convert.ToDateTime(reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName])
                        }
                    };
                    vendorPurchaseItem.BuyPurchase.Discount =
                                            reader[VendorPurchaseTable.DiscountColumn.FullColumnName] != DBNull.Value
                                                ? Convert.ToDecimal(reader[VendorPurchaseTable.DiscountColumn.FullColumnName])
                                                : 0;
                    vendorPurchaseItem.BuyPurchase.TaxRefNo =
                                           reader[VendorPurchaseTable.TaxRefNoColumn.FullColumnName] != DBNull.Value
                                               ? Convert.ToInt32(reader[VendorPurchaseTable.TaxRefNoColumn.FullColumnName])
                                               : 0;
                    vendorPurchaseItem.BuyPurchase.Vendor = new Contract.Infrastructure.Master.Vendor
                    {
                        Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
                        Mobile = reader[VendorTable.MobileColumn.FullColumnName].ToString(),
                        Email = reader[VendorTable.EmailColumn.FullColumnName].ToString(),
                        Address = reader[VendorTable.AddressColumn.FullColumnName].ToString(),
                        TinNo = reader[VendorTable.TinNoColumn.FullColumnName].ToString()
                    };
                    vendorPurchaseItem.ProductStock = new ProductStock
                    {
                        SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                        VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                        CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0,
                        GstTotal = reader[ProductStockTable.GstTotalColumn.FullColumnName] as decimal? ?? 0,
                        Cgst = reader[ProductStockTable.CgstColumn.FullColumnName] as decimal? ?? 0,
                        Sgst = reader[ProductStockTable.SgstColumn.FullColumnName] as decimal? ?? 0,
                        Igst = reader[ProductStockTable.IgstColumn.FullColumnName] as decimal? ?? 0,
                        Product = { Name = reader[ProductTable.NameColumn.FullColumnName].ToString() }
                    };
                    //vendorPurchaseItem.ReportTotal = ((vendorPurchaseItem.PurchasePrice*vendorPurchaseItem.Quantity) -
                    //                                  ((vendorPurchaseItem.PurchasePrice*vendorPurchaseItem.Quantity)*
                    //                                   (vendorPurchaseItem.BuyPurchase.Discount/100))) +
                    //                                 vendorPurchaseItem.VatPrice;
                    //vendorPurchaseItem.ReportTotal = (vendorPurchaseItem.PurchasePrice * vendorPurchaseItem.Quantity);
                    //vendorPurchaseItem.ReportTotal = ((vendorPurchaseItem.PackageQty * vendorPurchaseItem.PackagePurchasePrice) -
                    //                                  ((vendorPurchaseItem.PackagePurchasePrice * vendorPurchaseItem.PackageQty) *
                    //                                   (vendorPurchaseItem.Discount / 100))) + vendorPurchaseItem.VatPrice;

                    if (vendorPurchaseItem.BuyPurchase.TaxRefNo == 1) // Added by Sarubala on 13-07-17 to include GST
                    {
                        vendorPurchaseItem.ProductStock.VAT = vendorPurchaseItem.ProductStock.GstTotal;
                    }

                    //vendorPurchaseItem.ReportTotal = (((vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty) * vendorPurchaseItem.PackagePurchasePrice) -
                    //                                 ((vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty)) *
                    //                                 vendorPurchaseItem.ReportTotal = vendorPurchaseItem.Total + vendorPurchaseItem.VatPrice;
                    //(vendorPurchaseItem.Discount / 100))) + vendorPurchaseItem.VatPrice;
                    vendorPurchaseItem.ReportTotal = vendorPurchaseItem.PurchasePrice * vendorPurchaseItem.Quantity;
                    vendorPurchaseItem.PackageQty = vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty;
                    list.Add(vendorPurchaseItem);
                }
            }
            return list;
        }
        // Done Gavaskar 24-12-2016
        public async Task<List<SalesReturn>> GetReturnsByDate(string type, string accountid, string instanceid, DateTime from, DateTime to, string invoiceType, string invoiceSearchType, int fromInvoice, int toInvoice)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceid);
            parms.Add("AccountId", accountid);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("SearchOption", invoiceType);// added invoice series filter by Violet on 19/06/2017
            parms.Add("SearchValue", invoiceSearchType);
            parms.Add("FromInvoice", fromInvoice);
            parms.Add("ToInvoice", toInvoice);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetReturnsByDate", parms);
            /*Instance Name added by Poongodi on 14/09/2017*/
            var salesreturn = result.Select(x =>
            {
                var SalesReturn = new SalesReturn
                {
                    ReturnDate = x.ReturnDate as DateTime? ?? DateTime.MinValue,
                    ReturnNo = x.ReturnNo,
                    discountedAmount = x.DiscountedAmount as decimal? ?? 0,
                    AmountBeforeDiscount = x.AmountBeforeDiscount as decimal? ?? 0,
                    //AmountAfterDiscount = Math.Round(decimal.Parse(String.Format("{0:0.##}", x.AmountBeforeDiscount - x.DiscountedAmount)), MidpointRounding.AwayFromZero),
                    AmountAfterDiscount = x.ReturnedPrice as decimal? ?? 0,
                    //ReturnedPrice = Math.Round(decimal.Parse(String.Format("{0:0.##}", x.ReturnedPrice - x.ReturnedItemDiscount - x.ReturnCharges)), MidpointRounding.AwayFromZero), // Added by Sarubala on 13-10-17
                    ReturnedPrice = x.NetValue as decimal? ?? 0,
                    ReturnCharges = x.ReturnCharges as decimal? ?? 0, // Added by Sarubala on 13-10-17
                    RoundOffNetAmount = x.RoundOffNetAmount as decimal? ?? 0,
                    Sales = {
                            Name = x.Name,
                            Email = x.Email,
                            InvoiceNo = x.InvoiceNo,
                            ActualInvoice = x.InvoiceSeries +" "+ x.InvoiceNo,
                            Mobile = x.Mobile,
                            CreatedBy = x.CreatedBy,
                            //cash = Math.Round(decimal.Parse(String.Format("{0:0.##}", x.ReturnedPrice- x.ReturnedItemDiscount)), MidpointRounding.AwayFromZero),
                            cash = x.ReturnedPrice as decimal? ?? 0,
                            Credit = 0.00M,
                            PaymentType = "cash",
                            Discount = x.Discount as decimal? ?? 0
                        },
                    Instance =
                    { Name = x.Branch}
                };
                return SalesReturn;
            });
            return salesreturn.ToList();
        }
        // Done Gavaskar 24-12-2016
        public async Task<List<SalesReturn>> GetSalesDetailReturnsByDate(string type, string accountid, string instanceid, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceid);
            parms.Add("AccountId", accountid);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesDetailReturnsByDate", parms);
            var salesItems = result.Select(x =>
            {
                var SalesReturn = new SalesReturn
                {
                    ReturnDate = x.ReturnDate as DateTime? ?? DateTime.MinValue,
                    ReturnNo = x.ReturnNo,
                    ReturnedPrice = x.ReturnedPrice as decimal? ?? 0,
                    Sales = {
                             Name = x.Name,
                             InvoiceNo = x.InvoiceNo,
                             cash = Math.Round((decimal)(x.ReturnedPrice),0,MidpointRounding.AwayFromZero),
                             Discount = x.Discount as decimal? ?? 0,
                         }
                };
                return SalesReturn;
            });
            return salesItems.ToList();
        }
        public async Task<List<getAllPharmaItem>> getAllPharmaList(string type, DateTime from, DateTime to, int registerType)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();
            if (string.IsNullOrEmpty(type))
            {
                filterFromDate = from.ToFormat();
                filterToDate = to.ToFormat();
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "WEEK":
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                    case "MONTH":
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
            }
            var FROMDATE = filterFromDate + " 00:00:00";
            var TO = filterToDate + " 23:59:59";
            var today = DateTime.Today;
            var month = new DateTime(today.Year, today.Month, 1);
            var first = month.AddMonths(-1);
            var last = month.AddDays(-1);
            var filterfirstDate = first.ToFormat();
            var filterlastDate = last.ToFormat();
            var firstdate = filterfirstDate + " 00:00:00";
            var lastdate = filterlastDate + " 23:59:59";
            string wherecodition = string.Empty;
            if (registerType != 0)
            {
                wherecodition = "where a.RegisterType = " + registerType + "";
            }
            var list = new List<getAllPharmaItem>();
            var pharmaList = await GetPharmaDetails(wherecodition);
            var salesBasedOnDateList = await GetSalesBasedOnDateList(FROMDATE, TO);
            var purchaseBasedOnDateList = await GetPucrhaseOnDateList(FROMDATE, TO);
            var salesLastMonthList = await GetSalesLastMonthList(firstdate, lastdate);
            var purchaseLastMonthList = await GetPurchaseLastMonthList(firstdate, lastdate);
            var edgeList = await GetEdgeDetails();
            var result = from item1 in pharmaList
                         join item2 in salesBasedOnDateList on item1.PharmaId equals item2.PharmaId
                         select new
                         {
                             item1.count,
                             item1.PharmaId,
                             item1.PharmaName,
                             item1.RegisterType,
                             item1.PharmaArea,
                             item1.City,
                             item2.salesAmountBeforeDisocunt,
                             item2.salesDisocuntedAmount,
                             item2.SalesInvoiceCount,
                             item2.SalesTotalValue
                         } into intermediate1
                         join item3 in purchaseBasedOnDateList on intermediate1.PharmaId equals item3.PharmaId
                         select new
                         {
                             intermediate1.count,
                             intermediate1.PharmaId,
                             intermediate1.PharmaName,
                             intermediate1.RegisterType,
                             intermediate1.PharmaArea,
                             intermediate1.City,
                             intermediate1.salesAmountBeforeDisocunt,
                             intermediate1.salesDisocuntedAmount,
                             intermediate1.SalesInvoiceCount,
                             intermediate1.SalesTotalValue,
                             item3.PurchaseAmountBeforeDiscount,
                             item3.PurchaseDisocuntedAmount,
                             item3.PurchaseInvoiceCount,
                             item3.PurchaseTotalValue
                         } into intermediate2
                         join item4 in salesLastMonthList on intermediate2.PharmaId equals item4.PharmaId
                         select new
                         {
                             intermediate2.count,
                             intermediate2.PharmaId,
                             intermediate2.PharmaName,
                             intermediate2.RegisterType,
                             intermediate2.PharmaArea,
                             intermediate2.City,
                             intermediate2.salesAmountBeforeDisocunt,
                             intermediate2.salesDisocuntedAmount,
                             intermediate2.SalesInvoiceCount,
                             intermediate2.SalesTotalValue,
                             intermediate2.PurchaseAmountBeforeDiscount,
                             intermediate2.PurchaseDisocuntedAmount,
                             intermediate2.PurchaseInvoiceCount,
                             intermediate2.PurchaseTotalValue,
                             item4.salesLastmonthAmountBeforeDiscount,
                             item4.Saleslastmonthcount,
                             item4.salesLastMonthDiscountedAmount,
                             item4.SaleslastmonthTotalValue
                         } into intermediate3
                         join item5 in purchaseLastMonthList on intermediate3.PharmaId equals item5.PharmaId
                         select new
                         {
                             intermediate3.count,
                             intermediate3.PharmaId,
                             intermediate3.PharmaName,
                             intermediate3.RegisterType,
                             intermediate3.PharmaArea,
                             intermediate3.City,
                             intermediate3.salesAmountBeforeDisocunt,
                             intermediate3.salesDisocuntedAmount,
                             intermediate3.SalesInvoiceCount,
                             intermediate3.SalesTotalValue,
                             intermediate3.PurchaseAmountBeforeDiscount,
                             intermediate3.PurchaseDisocuntedAmount,
                             intermediate3.PurchaseInvoiceCount,
                             intermediate3.PurchaseTotalValue,
                             intermediate3.salesLastmonthAmountBeforeDiscount,
                             intermediate3.Saleslastmonthcount,
                             intermediate3.salesLastMonthDiscountedAmount,
                             intermediate3.SaleslastmonthTotalValue,
                             item5.pucrhaseLastMonthDiscountedAmount,
                             item5.purchaseLastMonthBeforeDisocunt,
                             item5.purchasetotalmonthCount,
                             item5.PurchaseTotalmonthValue
                         } into intermediate4
                         join item6 in edgeList on intermediate4.PharmaId equals item6.PharmaId
                         select new
                         {
                             intermediate4.count,
                             intermediate4.PharmaId,
                             intermediate4.PharmaName,
                             intermediate4.RegisterType,
                             intermediate4.PharmaArea,
                             intermediate4.City,
                             intermediate4.salesAmountBeforeDisocunt,
                             intermediate4.salesDisocuntedAmount,
                             intermediate4.SalesInvoiceCount,
                             intermediate4.SalesTotalValue,
                             intermediate4.PurchaseAmountBeforeDiscount,
                             intermediate4.PurchaseDisocuntedAmount,
                             intermediate4.PurchaseInvoiceCount,
                             intermediate4.PurchaseTotalValue,
                             intermediate4.salesLastmonthAmountBeforeDiscount,
                             intermediate4.Saleslastmonthcount,
                             intermediate4.salesLastMonthDiscountedAmount,
                             intermediate4.SaleslastmonthTotalValue,
                             intermediate4.pucrhaseLastMonthDiscountedAmount,
                             intermediate4.purchaseLastMonthBeforeDisocunt,
                             intermediate4.purchasetotalmonthCount,
                             intermediate4.PurchaseTotalmonthValue,
                             item6.BdoName,
                             item6.RmName,
                             item6.ContactName,
                             item6.ContactMobile,
                             item6.ContactEmail,
                             item6.SecondaryName,
                             item6.SecondaryMobile,
                             item6.SecondaryEmail,
                             item6.RegisterTypeText,
                             item6.LastLoggedOnText,
                             item6.PaidDateText,
                             item6.InvoiceValue,
                             item6.IsFullPaid,
                             item6.BalanceReasonText
                         };
            var list1 = result.Select(x => new getAllPharmaItem
            {
                PharmaName = x.PharmaName,
                PharmaArea = x.PharmaArea,
                City = x.City,
                RegisterType = x.RegisterType,
                PharmaId = x.PharmaId,
                SalesInvoiceCount = x.SalesInvoiceCount,
                SalesTotalValue = x.SalesTotalValue as decimal? ?? 0,
                PurchaseInvoiceCount = x.PurchaseInvoiceCount,
                PurchaseTotalValue = x.PurchaseTotalValue as decimal? ?? 0,
                Saleslastmonthcount = x.Saleslastmonthcount,
                SaleslastmonthTotalValue = x.SaleslastmonthTotalValue as decimal? ?? 0,
                PurchasetotalmonthCount = x.purchasetotalmonthCount,
                PurchaseTotalmonthValue = x.PurchaseTotalmonthValue as decimal? ?? 0,
                count = x.count,
                BDOName = x.BdoName,
                RelationshipManagerName = x.RmName,
                CurrentRM = x.RmName,
                Owner1Name = x.ContactName,
                Owner1Email = x.ContactEmail,
                Owner1number = x.ContactMobile,
                Owner2Name = x.SecondaryName,
                Owner2Email = x.SecondaryEmail,
                Owner2number = x.SecondaryMobile,
                InvoiceValue = x.InvoiceValue,
                InvoiceDate = x.PaidDateText,
                FullAmountPaid = x.IsFullPaid,
                Reasonforfullamountnotpaid = x.BalanceReasonText,
                LastDateLoggedin = x.LastLoggedOnText
            }).ToList();
            return list1;
        }
        public async Task<List<pharmaDetailsList>> GetPharmaDetails(string wherecodition)
        {
            string query = $@"select DISTINCT i.Id as PharmaId,i.Name as PharmaName,i.City,i.Area as PharmaArea,a.RegisterType from 
                   Instance as i inner join Account as a on a.Id = i.AccountId {wherecodition} 
                       group by i.Name,i.City,i.Id,i.Area,a.RegisterType order by i.Name asc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var pharmaDetailsList = new List<pharmaDetailsList>();
            int count = 0;
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    count++;
                    var item = new pharmaDetailsList
                    {
                        count = count,
                        City = reader["city"].ToString(),
                        PharmaId = reader["PharmaId"].ToString(),
                        PharmaArea = reader["PharmaArea"].ToString(),
                        PharmaName = reader["PharmaName"].ToString(),
                        RegisterType = reader["RegisterType"] as Int32? ?? 0
                    };
                    pharmaDetailsList.Add(item);
                }
            }
            return pharmaDetailsList;
        }
        public async Task<List<salesBasedOnDateList>> GetSalesBasedOnDateList(string fromdate, string todate)
        {
            string query = $@"select   DISTINCT
                      i.Id as PharmaId,
					 i.Name as PharmaName,
					  count(si.salescount) as SalesInvoiceCount,                    
					  sum(si.salesamountBeforeDiscount) as SalesAmountBeforeDisocunt,
					  sum(si.salesdiscountedAmount) as SalesDisocuntedAmount              
               from 
                   Instance as i  
                 left join (select sii.InstanceId,count(s.InvoiceNo) as salescount,
			   sum(  case 
                                when isNull(sii.SellingPrice,0)=0 
	                            then (productstock.SellingPrice)*sii.Quantity
	                            else (sii.SellingPrice)*sii.Quantity
	                            end
                               ) as salesamountBeforeDiscount,
			       sum(case
									    when  ISNULL(s.Discount,0) <> 0 then 
										      case 
											        when sii.SellingPrice is not null 
													then ((sii.SellingPrice*s.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*s.Discount)/100)*sii.Quantity
											  end
                                      when isnull(sii.Discount,0) <> 0 then
									          case
											         when sii.SellingPrice is not null 
													then ((sii.SellingPrice*sii.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*sii.Discount)/100)*sii.Quantity
											  end
                                       else								         
											         0											 
									 end								     
								) as salesdiscountedAmount  
                from Sales as  s inner join SalesItem as  sii on s.Id = sii.SalesId
				                 inner join productstock as productstock on productstock.id=sii.productstockid
				                 where s.CreatedAt  between '{fromdate}' and '{todate}'
				                 group by sii.SalesId,sii.InstanceId
                 ) as si on i.id = si.InstanceId
				  inner join Account as a on a.Id = i.AccountId
                       group by i.Id,i.Name";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var salesBasedOnDateList = new List<salesBasedOnDateList>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    decimal? SalesAmountBeforeDisocunt = reader["SalesAmountBeforeDisocunt"] as decimal? ?? 0;
                    decimal? SalesDisocuntedAmount = reader["SalesDisocuntedAmount"] as decimal? ?? 0;
                    var item = new salesBasedOnDateList
                    {
                        PharmaId = reader["PharmaId"].ToString(),
                        PharmaName = reader["PharmaName"].ToString(),
                        SalesInvoiceCount = reader["SalesInvoiceCount"] as Int32? ?? 0,
                        SalesTotalValue = SalesAmountBeforeDisocunt - SalesDisocuntedAmount
                    };
                    salesBasedOnDateList.Add(item);
                }
            }
            return salesBasedOnDateList;
        }
        public async Task<List<purchaseBasedOnDateList>> GetPucrhaseOnDateList(string fromdate, string todate)
        {
            string query = $@"select   DISTINCT
                      i.Id as PharmaId,
					   i.Name as PharmaName,
					  count(vpi.purchaseCount) as PurchaseInvoiceCount,                  
					  sum(vpi.PurchaseAmount) as PurchaseAmount
               from 
                   Instance as i
                 left join (select count(vpp.InvoiceNo) as purchaseCount,vpii.InstanceId,   
                                ROUND(sum(vpii.PurchasePrice * vpii.Quantity),0) as PurchaseAmount 
           from VendorPurchase as vpp inner join VendorPurchaseItem as vpii on vpp.Id = vpii.VendorPurchaseId 
		   where vpp.CreatedAt  between '{fromdate}' and '{todate}'
		   group by vpii.VendorPurchaseId,vpii.InstanceId 
                 )as vpi on i.id = vpi.InstanceId
				  inner join Account as a on a.Id = i.AccountId
                       group by i.Id,i.Name";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var purchaseBasedOnDateList = new List<purchaseBasedOnDateList>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new purchaseBasedOnDateList
                    {
                        PharmaId = reader["PharmaId"].ToString(),
                        PharmaName = reader["PharmaName"].ToString(),
                        PurchaseInvoiceCount = reader["PurchaseInvoiceCount"] as Int32? ?? 0,
                        PurchaseTotalValue = reader["PurchaseAmount"] as decimal? ?? 0
                    };
                    purchaseBasedOnDateList.Add(item);
                }
            }
            return purchaseBasedOnDateList;
        }
        public async Task<List<salesLastMonthList>> GetSalesLastMonthList(string fromdate, string todate)
        {
            string query = $@"select   DISTINCT
                      i.Id as PharmaId,
					  i.Name as PharmaName,
					   count(sis.saleslastmonthcount) as saleslastmonthcount,					
					  sum(sis.saleslastmonthamountBeforeDiscount) as salesLastmonthAmountBeforeDiscount,
					  sum(sis.saleslastmonthdiscountedAmount) as salesLastMonthDiscountedAmount
               from 
                   Instance as i  
				   left join (select sii.InstanceId,count(s.InvoiceNo) as saleslastmonthcount,			
			   sum(  case 
                                when isNull(sii.SellingPrice,0)=0 
	                            then (productstock.SellingPrice)*sii.Quantity
	                            else (sii.SellingPrice)*sii.Quantity
	                            end
                               ) as saleslastmonthamountBeforeDiscount,
			       sum(case
									    when  ISNULL(s.Discount,0) <> 0 then 
										      case 
											        when sii.SellingPrice is not null 
													then ((sii.SellingPrice*s.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*s.Discount)/100)*sii.Quantity
											  end
                                      when isnull(sii.Discount,0) <> 0 then
									          case
											         when sii.SellingPrice is not null 
													then ((sii.SellingPrice*sii.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*sii.Discount)/100)*sii.Quantity
											  end
                                       else     							         
											         0											 
									 end								     
								) as saleslastmonthdiscountedAmount  
                from Sales as  s inner join SalesItem as  sii on s.Id = sii.SalesId
								 inner join productstock as productstock on productstock.id=sii.productstockid
				                 where s.CreatedAt between '{fromdate}' and '{todate}'
				                 group by sii.SalesId,sii.InstanceId				
                 ) as sis on i.id = sis.InstanceId
				  inner join Account as a on a.Id = i.AccountId
                       group by i.Id,i.Name";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var salesLastMonthList = new List<salesLastMonthList>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    decimal? salesLastmonthAmountBeforeDiscount = reader["salesLastmonthAmountBeforeDiscount"] as decimal? ?? 0;
                    decimal? salesLastMonthDiscountedAmount = reader["salesLastMonthDiscountedAmount"] as decimal? ?? 0;
                    var item = new salesLastMonthList
                    {
                        PharmaId = reader["PharmaId"].ToString(),
                        PharmaName = reader["PharmaName"].ToString(),
                        Saleslastmonthcount = reader["saleslastmonthcount"] as Int32? ?? 0,
                        SaleslastmonthTotalValue = salesLastmonthAmountBeforeDiscount - salesLastMonthDiscountedAmount
                    };
                    salesLastMonthList.Add(item);
                }
            }
            return salesLastMonthList;
        }
        public async Task<List<purchaseLastMonthList>> GetPurchaseLastMonthList(string fromdate, string todate)
        {
            string query = $@"select   DISTINCT
                      i.Id as PharmaId,
					 i.Name as PharmaName,
					     count(vpis.purchasetotalmonthCount) as purchasetotalmonthCount,                     
					  sum(vpis.purchaselastmonthbeforedisocunt) as purchaseLastMonthBeforeDisocunt,
					  sum(vpis.pucrhaselastmonthdiscountedAmount) as pucrhaseLastMonthDiscountedAmount
               from 
                   Instance as i  
				     left join (select count(vpp.InvoiceNo) as purchasetotalmonthCount,vpii.InstanceId,           
                       sum(case
									    when  ISNULL(vpp.Discount,0) <> 0 then 										     
											       ((vpii.PurchasePrice * vpp.Discount)/100)*vpii.Quantity
												   else (vpii.PurchasePrice * vpii.Discount/100)* vpii.Quantity                    	 
									 end								     
								) as pucrhaselastmonthdiscountedAmount,
								sum(vpii.PurchasePrice * vpii.Quantity) as purchaselastmonthbeforedisocunt 
           from VendorPurchase as vpp inner join VendorPurchaseItem as vpii on vpp.Id = vpii.VendorPurchaseId 
		   where vpp.CreatedAt between '{fromdate}' and '{todate}'
		   group by vpii.VendorPurchaseId,vpii.InstanceId                            
                 ) as vpis on i.id = vpis.InstanceId
				  inner join Account as a on a.Id = i.AccountId
                       group by i.Id,i.Name";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var purchaseLastMonthList = new List<purchaseLastMonthList>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    decimal? purchaseLastMonthBeforeDisocunt = reader["purchaseLastMonthBeforeDisocunt"] as decimal? ?? 0;
                    decimal? pucrhaseLastMonthDiscountedAmount = reader["pucrhaseLastMonthDiscountedAmount"] as decimal? ?? 0;
                    var item = new purchaseLastMonthList
                    {
                        PharmaId = reader["PharmaId"].ToString(),
                        PharmaName = reader["PharmaName"].ToString(),
                        purchasetotalmonthCount = reader["purchasetotalmonthCount"] as Int32? ?? 0,
                        PurchaseTotalmonthValue = purchaseLastMonthBeforeDisocunt - pucrhaseLastMonthDiscountedAmount
                    };
                    purchaseLastMonthList.Add(item);
                }
            }
            return purchaseLastMonthList;
        }
        public async Task<List<Edge>> GetEdgeDetails()
        {
            string query = $@"select distinct i.id PharmaId, i.bdoName, i.rmName, i.contactName, i.ContactMobile, i.contactEmail, i.secondaryName, i.secondaryMobile, i.secondaryEmail, a.RegisterType, i.lastLoggedOn, pay.paymentDate paiddate, pay.cost, pay.quantity, pay.discount, pay.discountType, pay.tax, maxpay.paidAmt, pay.BalanceReason 
                from account a join instance i on a.id = i.accountId left join pharmacyPayment pay on pay.accountId = a.id 
                       left join
                       ( select phPay.accountId, min(phpay.paymentDate) minCreatedDate, max(paid) as paidamt from pharmacyPayment phpay group by accountId
                           ) maxPay on pay.paymentDate = maxPay.minCreatedDate and pay.accountId = maxPay.accountId
                         where ((pay.accountId is null) or (pay.accountId is not null and maxpay.accountId is not null))";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var edgeList = new List<Edge>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new Edge
                    {
                        PharmaId = reader["PharmaId"].ToString(),
                        BdoName = reader["bdoName"].ToString(),
                        RmName = reader["rmName"].ToString(),
                        ContactName = reader["contactName"].ToString(),
                        ContactMobile = reader["ContactMobile"].ToString(),
                        ContactEmail = reader["contactEmail"].ToString(),
                        SecondaryName = reader["secondaryName"].ToString(),
                        SecondaryMobile = reader["secondaryMobile"].ToString(),
                        SecondaryEmail = reader["secondaryEmail"].ToString(),
                        RegisterType = reader["RegisterType"] as int? ?? 0,
                        LastLoggedOn = (reader["lastLoggedOn"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(reader["lastLoggedOn"]),
                        PaidDate = (reader["paiddate"] == DBNull.Value) ? DateTime.MinValue : Convert.ToDateTime(reader["paiddate"]),
                        Cost = reader["cost"] as decimal? ?? 0,
                        Quantity = reader["quantity"] as int? ?? 0,
                        Discount = reader["discount"] as decimal? ?? 0,
                        DiscountType = reader["discountType"] as bool? ?? false,
                        Tax = reader["tax"] as decimal? ?? 0,
                        Paid = reader["paidAmt"] as decimal? ?? 0,
                        BalanceReason = reader["BalanceReason"].ToString()
                    };
                    edgeList.Add(item);
                }
            }
            return edgeList;
        }
        //Added By San
        public async Task<List<DcVendorPurchaseItem>> DcListData(string type, string accountId, string instanceId, string sProductId, string sVendorId,
                DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("VendorId", sVendorId);
            parms.Add("ProductId", sProductId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetDCList", parms);
            var dcItems = result.Select(x =>
            {
                var dcVendorPurchaseItem = new DcVendorPurchaseItem
                {
                    ReportDCDate = " " + x.DCDate.ToString("dd-MM-yyyy"), //added by nandhini for download csv
                    DCDate = x.DCDate,
                    DCNo = Convert.ToString(x.DCNo),
                    ProductStockId = Convert.ToString(x.ProductStockId),
                    Quantity = x.Quantity as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    PackagePurchasePrice =
                           x.PackagePurchasePrice as decimal? ?? 0,
                    PackageQty = x.PackageQty as decimal? ?? 0,
                    PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    FreeQty = x.FreeQty as decimal? ?? 0,
                    ActualQty = x.ActualQty,
                    ReportTotal = x.ReportTotal,
                    VATValue = x.VatValue as decimal? ?? 0,
                    TaxRefNo = x.TaxRefNo as int? ?? 0,
                    Status = x.Status,                  
                    InstanceName = Convert.ToString(x.InstanceName),
                    Vendor =
                        {
                        Name = Convert.ToString(x.VendorName),
                        Mobile = Convert.ToString(x.VendorMobile),
                        Email = Convert.ToString(x.VendorEmail),
                        Address = Convert.ToString(x.VendorAddress),
                        TinNo = Convert.ToString(x.VendorTinNo),
                        EnableCST = Convert.ToBoolean(x.VendorEnableCST)
                    },
                    ProductStock =
                        {
                        SellingPrice = x.ProductStockSellingPrice as decimal? ?? 0,
                        VAT = x.VAT as decimal? ?? 0,
                        CST = x.CST as decimal? ?? 0,
                        GstTotal = x.GstTotal as decimal? ?? 0,
                        Cgst = x.Cgst as decimal? ?? 0,
                        Sgst = x.Sgst as decimal? ?? 0,
                        Igst = x.Igst as decimal? ?? 0,
                        Product = { Name = Convert.ToString(x.ProductName) }
                }
                };

                if (dcVendorPurchaseItem.Vendor.EnableCST == true) //Added by Sarubala on 26-07-17
                {
                    dcVendorPurchaseItem.ProductStock.VAT = 0;
                    dcVendorPurchaseItem.ProductStock.GstTotal = 0;
                }

                if (dcVendorPurchaseItem.TaxRefNo == 1)
                {
                    dcVendorPurchaseItem.ProductStock.VAT = dcVendorPurchaseItem.ProductStock.GstTotal;
                    dcVendorPurchaseItem.ProductStock.CST = dcVendorPurchaseItem.ProductStock.Igst;
                }
                return dcVendorPurchaseItem;
            });
            return dcItems.ToList();
        }
        public async Task<List<ProductStock>> ProductAgeAnalyseListData(string type, string accountId, string instanceId, string productId, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("ProductId", productId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetProductAgeAnalysis", parms);
            var productAgeList = result.Select(x =>
            {
                var productAgeItem = new ProductStock();
                //added by nandhini for csv download
                productAgeItem.ReportExpireDate = " " + x.ExpireDate.ToString("MM-yy");
                //productAgeItem.ReportInvoiceDate = " " + x.LastSaleDate.ToString("dd-MM-yyyy");
                productAgeItem.ReportInvoiceDate = " " + x.LastSaleDate;
                productAgeItem.productName = x.Name;
                productAgeItem.BatchNo = x.BatchNo;
                productAgeItem.ExpireDate = x.ExpireDate;
                productAgeItem.Age = x.Age as int? ?? 0;
                productAgeItem.Lessthan30days = x.Lessthan30days as decimal? ?? 0;
                productAgeItem.Lessthan60days = x.Lessthan60days as decimal? ?? 0;
                productAgeItem.Lessthan120days = x.Lessthan120days as decimal? ?? 0;
                productAgeItem.Lessthan180days = x.Lessthan180days as decimal? ?? 0;
                productAgeItem.Lessthan360days = x.Lessthan360days as decimal? ?? 0;
                productAgeItem.LastSalesDate = x.LastSaleDate;
                return productAgeItem;
            });

            return productAgeList.ToList();
        }
        public async Task<List<SelfConsumption>> selfConsumptionListData(string type, string accountId, string instanceId, string productId, string invoiceNo, DateTime from, DateTime to)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            var searchtype = "1";
            var searchvalue = "";
            if (!string.IsNullOrEmpty(productId))
            {
                searchtype = "2";
                searchvalue = productId;
            }
            else if (!string.IsNullOrEmpty(invoiceNo))
            {
                searchtype = "3";
                searchvalue = invoiceNo;
            }
            else
            {
                searchtype = "1";
                searchvalue = "";
            }
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("SearchOption", searchtype);
            parms.Add("SearchValue", searchvalue);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Getselfconsumptionlist", parms);
            var selfconsumptionitems = result.Select(x =>
            {
                var SelfConsumption = new HQue.Contract.Infrastructure.Inventory.SelfConsumption
                {

                    ProductStock =
                    {
                        SellingPrice =x.SellingPrice as decimal? ?? 0,
                        VAT = x.VAT as decimal? ?? 0,
                        BatchNo= x.BatchNo as System.String ?? "",
                        ExpireDate =x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        PurchasePrice=x.PurchasePrice as decimal? ?? 0,
                        Stock= x.Stock as decimal? ?? 0,
                        PackageSize= x.PackageSize as decimal? ?? 0,

                        Product =
                        {
                            Name=x.ProductName as System.String ?? "",

                        }
                    }
                };
                SelfConsumption.Consumption = x.Consumption as decimal? ?? 0;
                SelfConsumption.ConsumptionNotes = x.ConsumptionNotes as System.String ?? "";
                SelfConsumption.ConsumptionDate = x.ConsumptionDate;
                SelfConsumption.ConsumedBy = x.ConsumedBy as System.String ?? "";
                SelfConsumption.ReportInvoiceDate = " " + x.ConsumptionDate.ToString("dd-MM-yyyy");
                SelfConsumption.ReportExpireDate = " " + x.ExpireDate.ToString("MM-yy");
                SelfConsumption.BatchNo = x.BatchNo.ToString();
                return SelfConsumption;
            });
            var selfconsumptionList = selfconsumptionitems.ToList();
            return selfconsumptionList;
        }
        public async Task<List<SalesItem>> SalesMarginProductWiseList(string filter, string accountId, string instanceId, DateTime from, DateTime to)
        {
            var report = GetParameter("", from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Filter", filter);
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesMarginList", parms);
            IEnumerable<SalesItem> salesItems;
            if (filter == "productWise")
            {
                salesItems = result.Select(x =>
                {
                    var salesItem = new HQue.Contract.Infrastructure.Inventory.SalesItem
                    {
                        Sales =
                             {
                                 InvoiceNo = x.InvoiceNo,
                                 InvoiceSeries = x.InvoiceSeries,
                                 ActualInvoice = x.InvoiceSeries +" "+ x.InvoiceNo,
                                 InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                                 Name = x.Name,

                             },
                        ProductStock =
                             {
                                  BatchNo = x.BatchNo,
                                  ExpireDate = Convert.ToDateTime(x.ExpireDate),
                                  Stock= x.Stock as decimal? ?? 0,
                                  VAT=x.VAT as decimal? ?? 0,
                                  PurchasePrice=x.PurchasePrice as decimal? ?? 0,
                                  CostPrice=x.CostPrice as decimal? ?? 0,
                                  InstanceName = Convert.ToString(x.InstanceName),
                    Product =
                             {
                                  Name=x.ProductName,
                                  Schedule=x.Schedule,
                             }
                    }
                    };
                    salesItem.ProductStock.TotalCostPrice = x.CostPrice as decimal? ?? 0;
                    salesItem.Sales.FinalValue = x.TotalAmount as decimal? ?? 0;
                    salesItem.SalesProfit = x.SalesProfit as decimal? ?? 0;
                    salesItem.ReportTotal = x.ProfitPerc as decimal? ?? 0;
                    //salesItem.SellingPrice = x.SellingPrice as decimal? ?? 0;
                    //salesItem.Quantity = x.Quantity as decimal? ?? 0;
                    //salesItem.Discount = x.Discount as decimal? ?? 0;
                    //salesItem.Sales.Discount = x.SalesDiscount as decimal? ?? 0;
                    //decimal? discountSum = x.DiscountSum as decimal? ?? 0;
                    //salesItem.Sales.InvoiceAmount = x.InvoiceAmount as decimal? ?? 0;
                    //salesItem.ProductStock.TotalCostPrice = salesItem.SellingPrice * salesItem.Quantity;
                    //salesItem.Sales.FinalValue = salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
                    //salesItem.ReportTotal = salesItem.Sales.FinalValue = (decimal)(salesItem.Sales.FinalValue);
                    //salesItem.ProductStock.TotalCostPrice = salesItem.ProductStock.PurchasePrice * salesItem.Quantity;
                    //salesItem.ProductStock.TotalCostPrice = decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.PurchasePrice * salesItem.Quantity));
                    //if (salesItem.ProductStock.TotalCostPrice > 0)
                    //{
                    //    //salesItem.SalesProfit = ((decimal.Parse(String.Format("{0:0.##}", salesItem.ReportTotal)) - decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) / decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) * 100;
                    //    salesItem.SalesProfit = salesItem.ReportTotal - salesItem.ProductStock.TotalCostPrice;
                    //    if (salesItem.SalesProfit > 0)
                    //        salesItem.ReportTotal = ((salesItem.Sales.FinalValue - salesItem.ProductStock.TotalCostPrice) / salesItem.Sales.FinalValue) * 100;
                    //    else
                    //        salesItem.ReportTotal = 0;
                    //    salesItem.SaleValue = (salesItem.SalesProfit / salesItem.ProductStock.TotalCostPrice) * 100;
                    //}
                    //else
                    //{
                    //    salesItem.SalesProfit = 0;
                    //    salesItem.ReportTotal = 0;
                    //    salesItem.SaleValue = 0;
                    //}
                    return salesItem;
                });
            }
            else if (filter == "invoiceWise")
            {
                salesItems = result.Select(x =>
                {
                    var salesItem = new SalesItem
                    {
                        Sales =
                             {
                                 InvoiceNo = x.InvoiceNo,
                                 InvoiceSeries = x.InvoiceSeries,
                                 ActualInvoice = x.InvoiceSeries +" "+ x.InvoiceNo,
                                 InvoiceDate = x.InvoiceDate,
                                 Name = x.Name,

                             },
                    };
                    salesItem.ProductStock.TotalCostPrice = x.CostPrice as decimal? ?? 0;
                    salesItem.ProductStock.InstanceName = Convert.ToString(x.InstanceName);
                    salesItem.Sales.FinalValue = x.MRP as decimal? ?? 0;
                    salesItem.SalesProfit = x.SalesProfit as decimal? ?? 0;
                    salesItem.ReportTotal = x.ProfitPerc as decimal? ?? 0;
                    //salesItem.ReportTotal = salesItem.Sales.FinalValue = (decimal)(salesItem.Sales.FinalValue);
                    //if (salesItem.ProductStock.TotalCostPrice > 0)
                    //{
                    //    //salesItem.SalesProfit = ((decimal.Parse(String.Format("{0:0.##}", salesItem.ReportTotal)) - decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) / decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) * 100;
                    //    salesItem.SalesProfit = salesItem.ReportTotal - salesItem.ProductStock.TotalCostPrice;
                    //    if (salesItem.SalesProfit > 0)
                    //        salesItem.ReportTotal = ((salesItem.Sales.FinalValue - salesItem.ProductStock.TotalCostPrice) / salesItem.Sales.FinalValue) * 100;
                    //    else
                    //        salesItem.ReportTotal = 0;
                    //    salesItem.SaleValue = (salesItem.SalesProfit / salesItem.ProductStock.TotalCostPrice) * 100;
                    //}
                    //else
                    //{
                    //    salesItem.SalesProfit = 0;
                    //    salesItem.ReportTotal = 0;
                    //    salesItem.SaleValue = 0;
                    //}
                    return salesItem;
                });
            }
            else
            {
                salesItems = result.Select(x =>
                {
                    var salesItem = new SalesItem
                    {
                        Sales =
                             {
                                 InvoiceDate = x.InvoiceDate,

                             },
                    };
                    salesItem.ProductStock.TotalCostPrice = x.CostPrice as decimal? ?? 0;
                    salesItem.ProductStock.InstanceName = Convert.ToString(x.InstanceName);
                    salesItem.Sales.FinalValue = x.MRP as decimal? ?? 0;
                    salesItem.SalesProfit = x.SalesProfit as decimal? ?? 0;
                    salesItem.ReportTotal = x.ProfitPerc as decimal? ?? 0;
                    //salesItem.Sales.InvoiceAmount = x.InvoiceAmount as decimal? ?? 0;
                    //salesItem.ProductStock.TotalCostPrice = x.CostPrice;
                    //salesItem.ProductStock.InstanceName = Convert.ToString(x.InstanceName);
                    //salesItem.Sales.FinalValue = x.MRP;
                    //salesItem.ReportTotal = salesItem.Sales.FinalValue = (decimal)(salesItem.Sales.FinalValue);
                    //if (salesItem.ProductStock.TotalCostPrice > 0)
                    //{
                    //    //salesItem.SalesProfit = ((decimal.Parse(String.Format("{0:0.##}", salesItem.ReportTotal)) - decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) / decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) * 100;
                    //    salesItem.SalesProfit = salesItem.ReportTotal - salesItem.ProductStock.TotalCostPrice;
                    //    if (salesItem.SalesProfit > 0)
                    //        salesItem.ReportTotal = ((salesItem.Sales.FinalValue - salesItem.ProductStock.TotalCostPrice) / salesItem.Sales.FinalValue) * 100;
                    //    else
                    //        salesItem.ReportTotal = 0;
                    //    salesItem.SaleValue = (salesItem.SalesProfit / salesItem.ProductStock.TotalCostPrice) * 100;
                    //}
                    //else
                    //{
                    //    salesItem.SalesProfit = 0;
                    //    salesItem.ReportTotal = 0;
                    //    salesItem.SaleValue = 0;
                    //}
                    return salesItem;
                });
                //return salesItems.ToList();
            }
            return salesItems.ToList();
        }

        //Created by Sarubala on 21-Jul-2017
        public async Task<Tuple<List<VendorPurchase>, List<VendorPurchase>>> PurchaseReportDetailMultiLevel(string type, string accountId, string instanceId, int level, DateTime from, DateTime to, VendorPurchase data)
        {
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceIds", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("Level", level);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            parms.Add("InvoiceDate", data.InvoiceDate); // Modified by Sarubala on 11-08-17
            //parms.Add("PageNo", Pageno);
            //parms.Add("PageSize", data.Page.PageSize);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseDetailMultiLevelReport", parms);
            var vendorPurchase = result.Select(x =>
            {
                var purchase = new HQue.Contract.Infrastructure.Inventory.VendorPurchase
                {

                    InvoiceNo = Convert.ToString(x.InvoiceNo),
                    ActualInvoice = Convert.ToString(x.Prefix) + " " + Convert.ToString(x.InvoiceNo),
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    Vendor =
                                {
                                  Name = Convert.ToString(x.VendorName)
                                },

                    VatValue = x.TaxAmount as decimal? ?? 0,
                    DiscountValue = x.PurchaseDiscountAmount as decimal? ?? 0,
                    TotalPurchaseValue = x.CostPriceWithoutTax as decimal? ?? 0,
                    MRP = x.SellingPrice as decimal? ?? 0,
                    RoundOffValue = x.RoundOffNetAmount as decimal? ?? 0,
                    NetPurchasePrice = x.NetPurchasePrice as decimal? ?? 0,
                    IsReturn = Convert.ToByte(x.ReturnFlag) == 1 ? true : false,
                    CreditValue = x.Credit as decimal? ?? 0, // Added by Sarubala on 09/08/17 to show credit/debit
                    DebitValue = x.Debit as decimal? ?? 0,

                    Instance =
                                    {
                                        Id  = x.InstanceId,
                                        Name  = x.InstanceName
                                    }

                };

                var temp1 = purchase.NetPurchasePrice;
                purchase.NetPurchasePrice = Math.Round(Convert.ToDecimal(purchase.NetPurchasePrice), MidpointRounding.AwayFromZero);
                if (purchase.TotalPurchaseValue > 0)
                {
                    purchase.RoundOffValue = purchase.NetPurchasePrice + purchase.CreditValue - (purchase.TotalPurchaseValue + purchase.VatValue + purchase.DebitValue);
                }
                else
                {
                    purchase.RoundOffValue = purchase.NetPurchasePrice - temp1;
                }

                if (purchase.NetPurchasePrice != 0)
                {
                    purchase.ProfitOnCost = (purchase.MRP - purchase.NetPurchasePrice) / purchase.NetPurchasePrice * 100;
                }

                if (purchase.MRP != 0)
                {
                    purchase.ProfitOnMrp = (purchase.MRP - (purchase.NetPurchasePrice + purchase.CreditValue - purchase.DebitValue)) / purchase.MRP * 100;
                }

                return purchase;
            });

            var returnList = vendorPurchase.ToList();

            if (data.Select == "profitOnCost")
            {
                decimal profitOnCostFilter; //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnCostFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnCost.HasValue ? x.ProfitOnCost.Value : 0, 2) == profitOnCostFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnCost > profitOnCostFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnCost < profitOnCostFilter).ToList();
                            break;
                    }

                }
            }
            else if (data.Select == "profitOnMrp")
            {
                decimal profitOnMrpFilter; //Data type Altered by Sarubala on 13-09-17
                if (decimal.TryParse(data.Values, out profitOnMrpFilter))
                {
                    switch (data.Select1)
                    {
                        case "equal":
                            returnList = returnList.Where(x => Math.Round(x.ProfitOnMrp.HasValue ? x.ProfitOnMrp.Value : 0, 2) == profitOnMrpFilter).ToList();
                            break;
                        case "greater":
                            returnList = returnList.Where(x => x.ProfitOnMrp > profitOnMrpFilter).ToList();
                            break;
                        case "less":
                            returnList = returnList.Where(x => x.ProfitOnMrp < profitOnMrpFilter).ToList();
                            break;
                    }

                }
            }

            var TempPurchase = returnList.Where(x => x.IsReturn == false).ToList();
            var TempPurchaseReturn = returnList.Where(x => x.IsReturn == true).ToList();
            Tuple<List<VendorPurchase>, List<VendorPurchase>> tuple = new Tuple<List<VendorPurchase>, List<VendorPurchase>>(TempPurchase, TempPurchaseReturn);

            return tuple;
        }


        // Added Gavaskar 06-03-2017
        public async Task<List<VendorPurchaseItemAudit>> PurchaseModifiedList(VendorPurchaseItemAudit data, string AccountId, string sInstanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            Dictionary<string, object> parmsAudit = new Dictionary<string, object>();
            var Pageno = (data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize;
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", sInstanceId);
            parms.Add("PageNo", Pageno);
            parms.Add("PageSize", "");
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            parms.Add("fromDate", data.Values);
            parms.Add("ToDate", data.SelectValue);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseModifiedList", parms);
            var purchaseItemsAudit = result.Select(x =>
            {
                var vendorPurchaseItemAudit = new VendorPurchaseItemAudit
                {
                    //added by nandhini for csv download
                    ReportCreatedAt = " " + x.ModifiedDate.ToString("dd-MM-yyyy"),
                    ReportGRNDate = " " + x.GRNDate.ToString("dd-MM-yyyy"),
                    ReportInvoiceDate = " " + x.InvoiceDate.ToString("dd-MM-yyyy"),
                    ReportExpireDate = " " + x.ExpireDate.ToString("MM-yy"),
                    ProductStockId = x.ProductStockId as System.String ?? "",
                    VendorPurchaseId = x.VendorPurchaseId as System.String ?? "",
                    PackageSize = x.PackageSize as decimal? ?? 0,
                    PackageQty = x.PackageQty as decimal? ?? 0,
                    PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? 0,
                    PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0,
                    Quantity = x.Quantity as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    FreeQty = x.FreeQty as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    MarkupPerc = x.MarkupPerc as decimal? ?? 0,
                    SchemeDiscountPerc = x.SchemeDiscountPerc as decimal? ?? 0,
                    UpdatedByUser = x.UpdatedBy as System.String ?? "",
                    CreatedAt = x.ModifiedDate as DateTime? ?? DateTime.MinValue,
                    ModifiedTime = x.ModifiedTime as System.String ?? "",
                    Action = x.Action as System.String ?? "",
                    GRNDate = x.GRNDate as DateTime? ?? DateTime.MinValue,
                    InvoiceDate = x.InvoiceDate as DateTime? ?? DateTime.MinValue,
                    InvoiceNo = x.InvoiceNo as System.String ?? "",
                    GRNNo = x.GRNNo as System.String ?? "",
                    BillSeries = x.BillSeries as String ?? "",
                    VendorName = x.VendorName as System.String ?? "",
                    InstanceName = Convert.ToString(x.InstanceName),
                    ProductStock =
                    {
                        SellingPrice =x.SellingPrice as decimal? ?? 0,
                        VAT =x.VAT as decimal? ?? 0,
                        CST= x.CST as decimal? ?? 0,
                        GstTotal =x.GstTotal as decimal? ?? 0,
                        Cgst =x.Cgst as decimal? ?? 0,
                        Sgst =x.Sgst as decimal? ?? 0,
                        Igst =x.Igst as decimal? ?? 0,
                        BatchNo= x.BatchNo as System.String ?? "",
                        ExpireDate =x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        Product =
                        {
                            Name=x.ProductName as System.String ?? "",
                        }
                    }
                };
                int? taxRefNo = x.TaxRefNo as int? ?? 0;

                if (taxRefNo == 1)
                {
                    vendorPurchaseItemAudit.ProductStock.VAT = vendorPurchaseItemAudit.ProductStock.GstTotal;
                }

                if (vendorPurchaseItemAudit.Action != "D")
                {
                    vendorPurchaseItemAudit.ProductStock.StatusText = "Modified";
                }
                else
                {
                    vendorPurchaseItemAudit.ProductStock.StatusText = "Deleted";
                }
                return vendorPurchaseItemAudit;
            });
            return purchaseItemsAudit.ToList();
        }
        // Added Gavaskar 04-03-2017
        public async Task<List<InvoiceSeriesItem>> getInvoiceSeriesItems(InvoiceSeriesItem IS, bool flag)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InvoiceSeriesItemTable.Table, OperationType.Select);
            qb.AddColumn(InvoiceSeriesItemTable.SeriesNameColumn, InvoiceSeriesItemTable.ActiveStatusColumn); //InvoiceSeriesItemTable.IdColumn, 
            qb.ConditionBuilder.And(InvoiceSeriesItemTable.AccountIdColumn, IS.AccountId);
            if (flag == false)
                qb.ConditionBuilder.And(InvoiceSeriesItemTable.InstanceIdColumn, IS.InstanceId);
            qb.ConditionBuilder.And(InvoiceSeriesItemTable.InvoiceseriesTypeColumn, IS.InvoiceseriesType);
            qb.SelectBuilder.SortBy(InvoiceSeriesItemTable.SeriesNameColumn); //CreatedAtColumn
            qb.SelectBuilder.MakeDistinct = true; // to Distinct the 'Invoice Series' added by Lawrence
            return await List<InvoiceSeriesItem>(qb);
        }
        public async Task<List<SalesReturn>> GetReturnsByDateSalesList(string type, string accountid, string instanceid, DateTime from, DateTime to, string invoiceType, string invoiceSearchType)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceid);
            parms.Add("AccountId", accountid);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("SearchOption", invoiceType);
            parms.Add("SearchValue", invoiceSearchType);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetReturnsByDateSalesList", parms);
            /*Instance Name added by Poongodi on 14/09/2017*/
            var salesreturn = result.Select(x =>
            {
                var SalesReturn = new SalesReturn
                {
                    ReturnDate = x.ReturnDate as DateTime? ?? DateTime.MinValue,
                    ReturnNo = x.ReturnNo,
                    discountedAmount = x.DiscountedAmount as decimal? ?? 0,
                    AmountBeforeDiscount = x.AmountBeforeDiscount as decimal? ?? 0,
                    //AmountAfterDiscount = Math.Round(decimal.Parse(String.Format("{0:0.##}", x.AmountBeforeDiscount - x.DiscountedAmount)), MidpointRounding.AwayFromZero),
                    AmountAfterDiscount = x.ReturnedPrice as decimal? ?? 0,
                    //ReturnedPrice = Math.Round(decimal.Parse(String.Format("{0:0.##}", x.ReturnedPrice - x.ReturnedItemDiscount - x.ReturnCharges)), MidpointRounding.AwayFromZero), // Added by Sarubala on 13-10-17
                    ReturnedPrice = x.NetValue as decimal? ?? 0,
                    ReturnCharges = x.ReturnCharges as decimal? ?? 0, // Added by Sarubala on 13-10-17
                    RoundOffNetAmount = x.RoundOffNetAmount as decimal? ?? 0,
                    IsRoundOff = x.IsRoundOff as bool? ?? false,
                    Sales = {
                            Name = x.Name,
                            Email = x.Email,
                            InvoiceNo = x.InvoiceNo,
                            ActualInvoice = x.InvoiceNo,
                            //x.InvoiceSeries +" "+ x.InvoiceNo,
                            Mobile = x.Mobile,
                            CreatedBy = x.CreatedBy,
                            //cash = Math.Round(decimal.Parse(String.Format("{0:0.##}", x.ReturnedPrice- x.ReturnedItemDiscount)), MidpointRounding.AwayFromZero),
                            cash = x.ReturnedPrice as decimal? ?? 0,
                            Credit = 0.00M,
                            PaymentType = "cash",
                            Discount = x.Discount as decimal? ?? 0
                        },
                    Instance =
                    {
                        Name = x.Branch
                    }
                };
                return SalesReturn;
            });
            return salesreturn.ToList();
        }
        public async Task<List<HQueUser>> GetUserName(string data, string accountid, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data))
            {
                qb.ConditionBuilder.And(HQueUserTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(HQueUserTable.NameColumn.ColumnName, data);
            }
            qb.ConditionBuilder.And(HQueUserTable.AccountIdColumn);
            qb.Parameters.Add(HQueUserTable.AccountIdColumn.ColumnName, accountid);
            qb.ConditionBuilder.And(HQueUserTable.InstanceIdColumn);
            qb.Parameters.Add(HQueUserTable.InstanceIdColumn.ColumnName, instanceid);
            var list = new List<HQueUser>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var HQueUser = new HQueUser
                    {
                        Name = reader[HQueUserTable.NameColumn.ColumnName].ToString().ToUpper(),
                        Mobile = reader[HQueUserTable.MobileColumn.ColumnName].ToString(),
                    };
                    list.Add(HQueUser);
                }
            }
            var genGroupBy = list.GroupBy(x => new { x.Mobile, x.Name }).Select(group => group.First());
            return genGroupBy.ToList();
        }
        public async Task<List<HQueUser>> GetUserId(string data, string accountid, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data))
            {
                qb.ConditionBuilder.And(HQueUserTable.UserIdColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(HQueUserTable.UserIdColumn.ColumnName, data);
            }
            qb.ConditionBuilder.And(HQueUserTable.AccountIdColumn);
            qb.Parameters.Add(HQueUserTable.AccountIdColumn.ColumnName, accountid);
            qb.ConditionBuilder.And(HQueUserTable.InstanceIdColumn);
            qb.Parameters.Add(HQueUserTable.InstanceIdColumn.ColumnName, instanceid);
            qb.ConditionBuilder.And(HQueUserTable.UserTypeColumn, 6, CriteriaEquation.NotEqual);
            var list = new List<HQueUser>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var HQueUser = new HQueUser
                    {
                        UserId = reader[HQueUserTable.UserIdColumn.ColumnName].ToString().ToUpper(),
                        Mobile = reader[HQueUserTable.MobileColumn.ColumnName].ToString(),
                    };
                    list.Add(HQueUser);
                }
            }
            var genGroupBy = list.GroupBy(x => new { x.Mobile, x.UserId }).Select(group => group.First());
            return genGroupBy.ToList();
            //var result = await List<HQueUser>(qb);
            //return result;
        }


        public async Task<List<Sales>> consolidatedsalesReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string cashType)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("SearchOption", invoiceType);
            parms.Add("CashType", cashType);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_Get_Consolidated_Sales_Report", parms);
            var salesItems = result.Select(x =>
            {
                var Sales = new Sales
                {
                    vaT52_Gross = x.Gross52Amt as decimal? ?? 0,
                    vaT52 = x.vat52Amount as decimal? ?? 0,
                    vaT131_Gross = x.Gross131Amt as decimal? ?? 0,
                    vaT131 = x.vat131Amount as decimal? ?? 0,
                    /*Other Vat and Gross added by Poongodi on 30/05/2017*/
                    vatOther_Gross = x.OtherGrossAmt as decimal? ?? 0,
                    vatOther = x.OtherVATAmt as decimal? ?? 0,

                    InvoiceNo = Convert.ToString(x.invoiceNo),
                    InvoiceDate = Convert.ToDateTime(x.InvoiceDate),
                    Name = x.Name,
                    Instance =
                      {
                        Name =x.Branch
                    },
                };
                //decimal? total = Sales.vaT52_Gross + Sales.vaT52 + Sales.vaT131_Gross + Sales.vaT131 + Sales.vatOther_Gross + Sales.vatOther;
                //Sales.ConsolidatedRoundOff = Math.Round((decimal)total, 0, MidpointRounding.AwayFromZero) - total;
                //Sales.FinalValue = Math.Round((decimal)total, 0, MidpointRounding.AwayFromZero);
                Sales.ConsolidatedRoundOff = x.RoundoffSaleAmount as decimal? ?? 0;
                Sales.FinalValue = x.SaleAmount as decimal? ?? 0;
                return Sales;
            });
            return salesItems.ToList();
        }
        //add by nandhini for consolidated sales return report 9/6/17

        public async Task<List<SalesReturn>> consolidatedsalesReturnReportList(string type, string accountId, string instanceId, DateTime from, DateTime to, string invoiceType, string cashType)
        {
            var report = GetParameter(type, from, to);
            report.FromDate = report.FromDate + " 00:00:00";
            report.ToDate = report.ToDate + " 23:59:59";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("SearchOption", invoiceType);
            parms.Add("CashType", cashType);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_Get_Consolidated_Sales_Return_Report", parms);
            var salesreturn = result.Select(x =>
            {
                var SalesReturn = new SalesReturn
                {

                    ReturnNo = Convert.ToString(x.ReturnNo),
                    ReturnDate = Convert.ToDateTime(x.ReturnDate),
                    Sales =
                    {

                            vaT52_Gross = x.Gross52Amt as decimal? ?? 0,
                    vaT52 = x.vat52Amount as decimal? ?? 0,
                    vaT131_Gross = x.Gross131Amt as decimal? ?? 0,
                    vaT131 = x.vat131Amount as decimal? ?? 0,

                    vatOther_Gross = x.OtherGrossAmt as decimal? ?? 0,
                    vatOther = x.OtherVATAmt as decimal? ?? 0,


                    Name = x.Name,
                     Instance =
                                {
                            Name =x.Branch
                        },

                    }



                };
                //decimal? total = SalesReturn.Sales.vaT52_Gross + SalesReturn.Sales.vaT52 + SalesReturn.Sales.vaT131_Gross + SalesReturn.Sales.vaT131 + SalesReturn.Sales.vatOther_Gross + SalesReturn.Sales.vatOther;
                //SalesReturn.Sales.ConsolidatedRoundOff = Math.Round((decimal)total, 0, MidpointRounding.AwayFromZero) - total;
                //SalesReturn.Sales.FinalValue = Math.Round((decimal)total, 0, MidpointRounding.AwayFromZero);
                SalesReturn.Sales.ConsolidatedRoundOff = x.RoundOffNetAmount as decimal? ?? 0;
                SalesReturn.Sales.FinalValue = x.NetAmount as decimal? ?? 0;


                return SalesReturn;
            });
            return salesreturn.ToList();

        }


        public async Task<List<StockTransferItem>> StockTransferList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {
            /*Opening Stock and Closing Stock added and Batch details commented by Poongodi on 06/07/2017*/
            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            //  parms.Add("ProductId", productId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStockTransferList_Rpt", parms);
            var stockTransferList = result.Select(x =>
            {
                var stockTransferItem = new HQue.Contract.Infrastructure.Inventory.StockTransferItem
                {
                    StockTransfer =
                    {
                        TransferNo=x.TransferNo,
                        TransferDate=x.TransferDate,
                        TransferedStatus=x.TransferStatus,
                        TransferBy=x.TransferBy,
                        OpeningStock = x.OpeningStock,
                        InwardQty = x.InwardQty,
                        OutwardQty = x.OutwardQty,
                        AdjQty = x.AdjQty,
                        //ClosingStock = x.ClosingStock,
                        Instance =
                        {
                            Name = x.Name,
                        },

                    },

                    ProductStock =
                             {
                                  Stock = x.CurrentStock as decimal? ?? 0,
                    Product =
                             {
                                  Name = x.ProductName,
                                  Manufacturer=x.Mfg,
                             },
                    Vendor =
                            {
                                  Name = x.VendorName,
                            }
                    }

                };

                stockTransferItem.Quantity = x.TransferQty;
                stockTransferItem.Quantity1 = x.PurchaseQty;
                //Added by nandhini for csv
                stockTransferItem.OpeningStock = x.OpeningStock;
                stockTransferItem.InwardQty = x.InwardQty;
                stockTransferItem.AdjQty = x.AdjQty;
                stockTransferItem.OutwardQty = x.OutwardQty;
                //end
                //stockTransferItem.BatchNo = x.BatchNo;
                //stockTransferItem.ExpireDate = x.ExpireDate;
                //stockTransferItem.PurchasePrice = x.PurchasePrice;
                stockTransferItem.MRP = x.MRP;
                stockTransferItem.SellingPrice = x.CurrentStock;


                return stockTransferItem;
            });



            return stockTransferList.ToList();
        }

        public async Task<List<PhysicalStockHistory>> physicalStockHistoryList(DateTime FromDate, DateTime ToDate, string type, string AccountId, string InstanceId, string ProductId, string UserName, string UserId)
        {
            var report = GetParameter(type, FromDate, ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("FromDate ", report.FromDate);
            parms.Add("ToDate", report.ToDate);
            parms.Add("ProductId", ProductId);
            parms.Add("UserName", UserName);
            parms.Add("UserId", UserId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_PhysicalStockHistory", parms);

            var resultItems = result.Select(x =>
            {
                var data = new PhysicalStockHistory
                {
                    Product =
                    {
                        Id = x.ProductId,
                        Name = x.ProductName,
                    },
                    PhysicalStock = x.PhysicalStock,
                    CurrentStock = x.CurrentStock,
                    StockDifference = x.StockDifference,
                    CreatedAt = x.CreatedAt,
                    OpenedAt = x.OpenedAt,
                    ClosedAt = x.ClosedAt,
                    //added by anndhini for csv
                    ReportCreateDate = " " + x.CreatedAt.ToString("dd-MM-yyyy"),
                    ReportOpenDate = " " + x.OpenedAt.ToString("MM/dd/yyyy hh:mm tt"),
                    ReportCloseDate = " " + x.ClosedAt.ToString("MM/dd/yyyy hh:mm tt"),

                    User = new HQueUser
                    {
                        Name = x.UserName,
                        UserId = x.UserId,
                    },
                };
                return data;
            });

            return resultItems.ToList();
        }
        //Added by Poongodi on 14/11/2017
        public async Task<string> ClosingStockLog(string accountId, string instanceId)
        {


            var qb = QueryBuilderFactory.GetQueryBuilder("usp_ClosingStockLog");
            qb.Parameters.Add("InstanceId", instanceId);
            qb.Parameters.Add("AccountId", accountId);
            string sMessage = "";
            using (var reader = await QueryExecuter.ExecuteProcAsyc(qb))
            {
                while (reader.Read())
                {
                    sMessage = reader.GetString(0);
                }
            }
            return sMessage;
        }

        /// <summary>
        /// Get Product wise StockMovement List
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        /// Added by Poongodi on 10/07/2017
        public async Task<List<StockMovement>> StockMovementList(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {

            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("FromDate", report.FromDate);
            parms.Add("ToDate", report.ToDate);
            parms.Add("SysDate", CustomDateTime.IST);

            var result = await sqldb.ExecuteProcedureAsync<StockMovement>("usp_GetStockMovement_Rpt", parms);


            return result.ToList();
        }
        /// <summary>
        /// Stock Transfer Report
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public async Task<List<StockTransferItem>> StockTransferDetail(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {

            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("FromDate", report.FromDate);
            parms.Add("ToDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_transfer_rpt", parms);
            var stockTransfer = result.Select(x =>
            {
                var data = new StockTransferItem
                {
                    //added by nandhini for csv
                    ReportTransferDate = " " + x.TransferDate.ToString("dd-MM-yyyy"),
                    ToProductStockId = x.TransferTo,
                    SellingPrice = x.TransferAmount,
                    StockTransfer =
                     {
                     TransferAmount= x.TransferAmount,
                     TransferBy =x.TransferBy,
                     TransferNo= x.TransferNo,
                     TransferDate = x.TransferDate,
                    TransferedStatus= x.TransferStatus,
                     Instance = new Instance()
                     {
                         Name = x.TranferFrom,
                     },
                    ToInstance =
                    {
                        Name = x.TransferTo,
                    }
                  },
                };
                return data;
            });
            return stockTransfer.ToList();



        }
        public async Task<List<StockTransferItem>> StockTransfeSummary(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {

            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("FromDate", report.FromDate);
            parms.Add("ToDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_transfer_Summary_rpt", parms);
            var stockTransfer = result.Select(x =>
            {
                var data = new StockTransferItem
                {
                    //added by nandhini for csv
                    ReportTransferDate = " " + x.TransferDate.ToString("dd-MM-yyyy"),
                    StockTransfer =
                       {
                        TransferAmount= x.TransferValue,
                        AcceptedAmount =x.AcceptedValue,
                        PendingAmount= x.PendingValue,
                        TransferDate = x.TransferDate,
                        RejectedAmount= x.RejectedValue,
                    },
                };
                return data;
            });
            return stockTransfer.ToList();


        }
        public async Task<List<StockTransferItem>> StockTransferProductDetail(string type, string accountId, string instanceId, DateTime from, DateTime to)
        {

            var report = GetParameter(type, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("FromDate", report.FromDate);
            parms.Add("ToDate", report.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_transfer_Detail_rpt", parms);
            var stockTransfer = result.Select(x =>
            {
                var data = new StockTransferItem
                {
                    //added by nandhini for csv
                    ReportTransferDate = " " + x.TransferDate.ToString("dd-MM-yyyy"),
                    ToProductStockId = x.TransferTo,
                    ReportExpiryDate = " " + x.ExpireDate.ToString("dd-MM-yyyy"),
                    StockTransfer =
                      {
                       TransferAmount= x.TransferAmount,
                       TransferBy =x.TransferBy,
                       TransferNo= x.TransferNo,
                       TransferDate = x.TransferDate,
                       TransferedStatus= x.TransferStatus,
                       Instance = new Instance()
                       {
                           Name = x.TranferFrom,
                       },
                      ToInstance =
                      {
                          Name = x.TransferTo,
                      },
                   },
                    BatchNo = x.BatchNo,
                    ExpireDate = x.ExpireDate,
                    MRP = x.MRP,
                    PurchasePrice = x.PurchasePrice,
                    Quantity = x.Quantity,
                    AcceptedUnits = x.AcceptedUnits,
                    ProductStock =
                      {
                          Product =
                          {
                              Name = x.ProductName,
                          },

                      }
                };
                return data;
            });
            return stockTransfer.ToList();


        }
        /// <summary>
        /// GST 3B Report added by Poongodi on 13/08/2017
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<GSTReport>> GST3BReportList(string type, string accountId, string instanceId,
          DateTime from, DateTime to, int filte, string sSearchType, string sGsttinNo, int nInvoiceDt)
        {
            /* var report = GetParameter(type, from, to);
             report.FromDate = report.FromDate + " 00:00:00";
             report.ToDate = report.ToDate + " 23:59:59"; */
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            parms.Add("Searchtype", sSearchType);
            parms.Add("Gstin", sGsttinNo);
            parms.Add("IsInvDtSearch", nInvoiceDt);
            var result = await sqldb.ExecuteProcedureAsync<GSTReport>("usp_GST_3b_Rpt", parms);

            return result.ToList();
        }
        /// <summary>
        /// hsnReportList  added for GST REPORTS by Nandhini on 29/08/2017
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="sSearchType"></param>
        /// <param name="sGsttinNo"></param>
        /// <returns></returns>
        public async Task<List<GSTReport>> hsnReportList(string accountId)
        {

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            var result = await sqldb.ExecuteProcedureAsync<GSTReport>("Usp_Get_HSN", parms);

            return result.ToList();
        }
        /// <summary>
        ///  added by Nandhini on 29/08/2017 for controlreport 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<List<ControlReport>> controlReportList(string accountId, string instanceId,
          DateTime from, DateTime to, int filter)
        {
            /* var report = GetParameter(type, from, to);
             report.FromDate = report.FromDate + " 00:00:00";
             report.ToDate = report.ToDate + " 23:59:59"; */
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);

            var result = await sqldb.ExecuteProcedureAsync<ControlReport>("Usp_ControlReport", parms);

            return result.ToList();
        }
        /// <summary>
        /// GSTR 1 Purchase Details
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="filte"></param>
        /// <param name="sSearchType"></param>
        /// <param name="sGsttinNo"></param>
        /// <returns></returns>
        public async Task<List<GSTReport>> GSTR1PurchaseList(string type, string accountId, string instanceId,
         DateTime from, DateTime to, int filte, string sSearchType, string sGsttinNo, int nSummary, int nRegister, int nInvoiceDt)
        {
            /* var report = GetParameter(type, from, to);
             report.FromDate = report.FromDate + " 00:00:00";
             report.ToDate = report.ToDate + " 23:59:59"; */
            string sSpname = "Usp_GSTR1_Purchase";
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            parms.Add("FilterType", sSearchType);
            parms.Add("Gstinno", sGsttinNo);
            parms.Add("IsRegister", nRegister);
            parms.Add("IsInvDtSearch", nInvoiceDt);
            if (nSummary == 1)
            {
                sSpname = "Usp_gstr2_purchaseSummary";
            }
            var result = await sqldb.ExecuteProcedureAsync<GSTReport>(sSpname, parms);

            return result.ToList();
        }
        /// <summary>
        /// GSTR 2 Purchase Summary
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="filte"></param>
        /// <param name="sSearchType"></param>
        /// <param name="sGsttinNo"></param>
        /// <returns></returns>
        public async Task<List<GSTReport>> GSTR2PurchaseReturn(string type, string accountId, string instanceId,
         DateTime from, DateTime to, int filte, string sSearchType, string sGsttinNo, int nRegister, int nInvoiceDt)
        {
            /* var report = GetParameter(type, from, to);
             report.FromDate = report.FromDate + " 00:00:00";
             report.ToDate = report.ToDate + " 23:59:59"; */
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            parms.Add("FilterType", sSearchType);
            parms.Add("Gstinno", sGsttinNo);
            parms.Add("IsRegister", nRegister);
            parms.Add("IsInvDtSearch", nInvoiceDt);

            var result = await sqldb.ExecuteProcedureAsync<GSTReport>("Usp_GSTR2_PurchaseReturn", parms);

            return result.ToList();
        }

        public async Task<List<GSTReport>> GSTR3BPurchaseReturnList(string type, string accountId, string instanceId,
     DateTime from, DateTime to, int filte, string sSearchType, string sGsttinNo, int nRegister, int nInvoiceDt)
        {
            /* var report = GetParameter(type, from, to);
             report.FromDate = report.FromDate + " 00:00:00";
             report.ToDate = report.ToDate + " 23:59:59"; */
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            parms.Add("FilterType", sSearchType);
            parms.Add("Gstinno", sGsttinNo);
            parms.Add("IsRegister", nRegister);
            parms.Add("IsInvDtSearch", nInvoiceDt);

            var result = await sqldb.ExecuteProcedureAsync<GSTReport>("Usp_GSTR2_PurchaseReturn", parms);

            return result.ToList();
        }
        public async Task<List<GSTReport>> GSTR3BDetailsList(string type, string accountId, string instanceId,
 DateTime from, DateTime to, int filte, string sSearchType, string sGsttinNo, int nRegister, int nInvoiceDt)
        {
            /* var report = GetParameter(type, from, to);
             report.FromDate = report.FromDate + " 00:00:00";
             report.ToDate = report.ToDate + " 23:59:59"; */
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            parms.Add("FilterType", sSearchType);
            parms.Add("Gstinno", sGsttinNo);
            parms.Add("IsRegister", nRegister);
            parms.Add("IsInvDtSearch", nInvoiceDt);
            var result = await sqldb.ExecuteProcedureAsync<GSTReport>("Usp_GSTR1_Purchase", parms);

            return result.ToList();
        }

        /// <summary>
        /// GSTR 1 Sales Details
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="filte"></param>
        /// <param name="sSearchType"></param>
        /// <param name="sGsttinNo"></param>
        /// <returns></returns>
        public async Task<List<GSTReport>> GSTR1SalesList(string type, string accountId, string instanceId,
       DateTime from, DateTime to, int filte, string sSearchType, string sGsttinNo, int IsSummary)
        {
            /* var report = GetParameter(type, from, to);
             report.FromDate = report.FromDate + " 00:00:00";
             report.ToDate = report.ToDate + " 23:59:59"; */
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            parms.Add("FilterType", sSearchType);
            parms.Add("Gstinno", sGsttinNo);
            parms.Add("IsSummary", IsSummary); // Added by Sarubala on 04-09-17
            var result = await sqldb.ExecuteProcedureAsync<GSTReport>("Usp_GSTR1_Sales", parms);

            return result.ToList();
        }

        /// <summary>
        /// GSTR 1 Sales Details
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="filte"></param>
        /// <param name="sSearchType"></param>
        /// <param name="sGsttinNo"></param>
        /// <returns></returns>
        public async Task<List<GSTReport>> GSTR3BSalesDetailsList(string type, string accountId, string instanceId,
       DateTime from, DateTime to, int filte, string sSearchType, string sGsttinNo, int nSummary)
        {
            /* var report = GetParameter(type, from, to);
             report.FromDate = report.FromDate + " 00:00:00";
             report.ToDate = report.ToDate + " 23:59:59"; */
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            parms.Add("FilterType", sSearchType);
            parms.Add("Gstinno", sGsttinNo);
            parms.Add("IsSummary", nSummary);
            var result = await sqldb.ExecuteProcedureAsync<GSTReport>("Usp_GSTR1_Sales", parms);

            return result.ToList();
        }
        /// <summary>
        /// GSTR1 Sales Return Summary report
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="filte"></param>
        /// <param name="sSearchType"></param>
        /// <param name="sGsttinNo"></param>
        /// <param name="nSummary"></param>
        /// <returns></returns>
        public async Task<List<GSTReport>> GSTR1SalesReturn(string type, string accountId, string instanceId,
            DateTime from, DateTime to, int filte, string sSearchType, string sGsttinNo, int nSummary)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            parms.Add("FilterType", sSearchType);
            parms.Add("Gstinno", sGsttinNo);
            parms.Add("IsSummary", nSummary);
            var result = await sqldb.ExecuteProcedureAsync<GSTReport>("Usp_gstr1_salesReturn", parms);

            return result.ToList();
        }
        /// <summary>
        /// 3B Sales Return report
        /// </summary>
        /// <param name="type"></param>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="filte"></param>
        /// <param name="sSearchType"></param>
        /// <param name="sGsttinNo"></param>
        /// <param name="nSummary"></param>
        /// <returns></returns>
        public async Task<List<GSTReport>> GSTR3BSalesReturnList(string type, string accountId, string instanceId,
            DateTime from, DateTime to, int filte, string sSearchType, string sGsttinNo, int nSummary)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("StartDate", from);
            parms.Add("EndDate", to);
            parms.Add("FilterType", sSearchType);
            parms.Add("Gstinno", sGsttinNo);
            parms.Add("IsSummary", nSummary);
            var result = await sqldb.ExecuteProcedureAsync<GSTReport>("Usp_gstr1_salesReturn", parms);

            return result.ToList();
        }
        /// <summary>
        /// GEt account GSTin number
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<List<GSTIn>> GetGstinData(string accountId)
        {

            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", accountId);


            var result = await sqldb.ExecuteProcedureAsync<GSTIn>("usp_Get_GSTin", parms);

            return result.ToList();
        }



        public async Task<string[]> ValidateGSTR3B(string accountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, accountId);
            var result = await List<Instance>(qb);

            string[] msg = new string[2];
            msg[0] = "Ok";
            if (result.Where(x => x.GsTinNo == null || x.GsTinNo == "").Count() > 0)
            {
                msg[0] = "Please update \"GSTIN No.\" for all branches & try again.";
                if (user.HasPermission(UserAccess.Branch))
                {
                    msg[1] = "/InstanceSetup/List";
                }
                else
                {
                    msg[0] = msg[0] + " Please contact administrator since you don't have rights.";
                }
                return msg;
            }

            qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VendorTable.AccountIdColumn, accountId);
            var result1 = await List<Vendor>(qb);
            if (result1.Where(x => x.LocationType == null || x.LocationType == 0 || x.VendorType == null || x.VendorType == 0).Count() > 0)
            {
                msg[0] = "Please update \"Location Type & Vendor Type\" for all vendors & try again.";
                if (user.HasPermission(UserAccess.Profile))
                {
                    msg[1] = "/Vendor/VendorMultiEdit";
                }
                else
                {
                    msg[0] = msg[0] + " Please contact administrator since you don't have rights.";
                }
                return msg;
            }

            return msg;
        }


        /// <summary>
        /// Stock Report List for Print 
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductStock>> StockListDataForPrint(string instanceId, string accountId, string sStockType, bool isProductWise, string category, string manufacturer, string sRackNo, string sBoxNo, string alphabet1, string alphabet2, DateTime? from, DateTime? to)
        {
            string fromDate = "";
            string toDate = "";
            if (from.ToFormat() == "01-Jan-0001" && to.ToFormat() == "01-Jan-0001")
            {
                fromDate = "";
                toDate = "";
            }
            else
            {
                fromDate = from.ToString();
                toDate = to.ToString();
            }
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("accountId", accountId);
            parms.Add("StockType", sStockType);
            parms.Add("IsProductWise", isProductWise);
            parms.Add("Category", category);
            parms.Add("Manufacturer", manufacturer);
            parms.Add("RackNo", sRackNo);
            parms.Add("Boxno", sBoxNo);
            parms.Add("Alphabet1", alphabet1);
            parms.Add("Alphabet2", alphabet2);
            parms.Add("fromDate", fromDate);
            parms.Add("toDate", toDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStockRptLstForPrint", parms);

            var products = result.Select(x =>
            {
                var productStock = new ProductStock
                {
                    Product = {
                        Name = x.Name,
                        Category = x.Category,
                        Manufacturer = x.Manufacturer,  //Schedule = x.Schedule,
                        RackNo = x.RackNo,
                        BoxNo = x.BoxNo,
                        Instance =
                        {
                            Name = x.InstName,
                            DrugLicenseNo = x.DrugLicenseNo,
                            GsTinNo = x.GsTinNo,
                            FullAddress1 = x.InstAddress, //I.Name as [I.Name], I.DrugLicenseNo, I.TinNo, I.GsTinNo, I.Address as [I.Address]
                        }
                    },
                    BatchNo = x.BatchNo,
                    Stock = Convert.ToDecimal(x.Quantity),
                    ExpireDate = Convert.ToDateTime(x.ExpireDate),
                    PackageSize = x.PackageSize as decimal? ?? 0,
                    CostPriceGst = Convert.ToDecimal(x.CostPriceGst),
                    MRP = Convert.ToDecimal(x.MRP),
                    SellingPrice = Convert.ToDecimal(x.SellingPrice)
                };

                return productStock;
            });
            return products.ToList();
        }

        public async Task<List<Sales>> gstr4TaxRateWiseTurnOver(string accountId, string instanceId, DateTime from, DateTime to, string sGsttinNo)
        {
            var report = GetParameter(null, from, to);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("GstinNo", sGsttinNo);
            parms.Add("fromDate", report.FromDate);
            parms.Add("toDate", report.ToDate);
            //parms.Add("SearchColName", data.Select);
            //parms.Add("SearchOption", data.Select1);
            //parms.Add("SearchValue", data.Values);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_gstr4TaxRateWiseTurnOver", parms);
            var sales = result.Select(x =>
            {
                var sale = new HQue.Contract.Infrastructure.Inventory.Sales
                {
                    GstTotal = x.GstTotal as decimal? ?? 0,
                    InvoiceAmountNoTaxNoDiscount = x.InvoiceAmountNoTaxNoDiscount as decimal? ?? 0,
                    CGSTTaxAmt = x.CGSTTaxAmt as decimal? ?? 0,
                    SGSTTaxAmt = x.SGSTTaxAmt as decimal? ?? 0,
                    InvAmountNoTaxNoDiscount = x.InvAmountNoTaxNoDiscount as decimal? ?? 0
                };

                return sale;
            });

            var returnList = sales.ToList();

            return returnList;
        }

    }
}

