﻿using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Leads;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.DbModel;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.DataAccess.Helpers.Extension;
using HQue.Contract.Infrastructure.Communication;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Helpers;
using Utilities.Enum;
using DataAccess.Criteria;

using Dapper;
using DataAccess;
using HQue.Contract.Infrastructure.Accounts;

namespace HQue.DataAccess.Report
{
    
    public class ReportParamDt
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class CustomerReportDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
       
        public CustomerReportDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        
        private ReportParamDt GetParameter(string type, DateTime? from, DateTime? to)
        {
            var filterFromDate = CustomDateTime.IST.ToFormat();
            var filterToDate = CustomDateTime.IST.ToFormat();

            if (string.IsNullOrEmpty(type))
            {
                filterFromDate = from.ToFormat();
                filterToDate = to.ToFormat();
            }
            else
            {
                switch (type.ToUpper())
                {
                    case "TODAY":
                        filterFromDate = CustomDateTime.IST.ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;

                    case "WEEK":
                        filterFromDate = DateTime.Today.AddDays((int)DateTime.Today.DayOfWeek * -1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;

                    case "MONTH":
                        filterFromDate = new DateTime(CustomDateTime.IST.Year, CustomDateTime.IST.Month, 1).ToFormat();
                        filterToDate = CustomDateTime.IST.ToFormat();
                        break;
                }
            }
            return new ReportParamDt()
            {
                FromDate = filterFromDate,
                ToDate = filterToDate
            };
        }

        public override Task<T> Save<T>(T data)
        {
            throw new NotImplementedException();
        }
        
        public async Task<List<CustomerPayment>> CustomerWiseBalanceList(string accountId, string instanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetCustomerWiseBalanceList", parms);
            var getCustomerList = result.Select(x =>
            {
                var customerWiseBalace = new CustomerPayment();

                customerWiseBalace.CustomerName = x.Name;
                customerWiseBalace.CustomerMobile = x.Mobile;
                customerWiseBalace.CustomerDebit= (decimal)x.TotalDue;              
                return customerWiseBalace;
            });
            return getCustomerList.ToList();
        }

        public async Task<List<CustomerPayment>> customerDetailsList(CustomerPayment data)
        {
            var type = "";
            var report = GetParameter(type, data.FromDate, data.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);

            //report.FromDate = report.FromDate + " 00:00:00";
            //report.ToDate = report.ToDate + " 23:59:59";
            
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("Name", data.CustomerName);
            parms.Add("Mobile", data.CustomerMobile);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetCustomerWiseBalanceDetails", parms);
            var CustomerDetails = result.Select(x =>
            {
                    var customerWiseBalace = new CustomerPayment();
               
                    customerWiseBalace.InvoiceNo = x.InvoiceNo;
                    customerWiseBalace.InvoiceDate = x.InvoiceDate;
                    //customerWiseBalace.InvoiceAmount = Math.Round((decimal)x.InvoiceValue, 0, MidpointRounding.AwayFromZero);
                    customerWiseBalace.InvoiceAmount = x.InvoiceValue as decimal? ?? 0;
                    customerWiseBalace.Age = x.Age;
                    //customerWiseBalace.Debit = Math.Round((decimal)x.Debit, 0, MidpointRounding.AwayFromZero);
                    customerWiseBalace.Debit = x.Debit as decimal? ?? 0;
                    customerWiseBalace.Credit = (decimal)x.Credit;

                return customerWiseBalace;            
            });
            return CustomerDetails.ToList();
        }

        public async Task<List<CustomerPayment>> GetCustomerList(string customerName, string mobile, string accountid, string instanceid)
        {            
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceid);
            parms.Add("AccountId", accountid);
            
            parms.Add("Name", customerName);
            parms.Add("Mobile", mobile);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetCreditCustomer", parms);
            var CustomerDetails = result.Select(x =>
            {
                var customerList = new CustomerPayment();

                customerList.CustomerName = x.Name;
                customerList.CustomerMobile = x.Mobile;                

                return customerList;
            });
            return CustomerDetails.ToList();
        }

        public async Task<List<PettyCashHdr>> GetPettycashDates(string AccountId, string InstanceId, string UserId, DateTime selectedDate)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", InstanceId);
            parms.Add("AccountId", AccountId);
            parms.Add("UserId", UserId);
            parms.Add("tDate", selectedDate.ToString("yyyy-MM-dd"));
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPettyCashDates", parms);
            var pettyHdrDtls = result.Select(x =>
            {
                var pettyHdrDtl = new PettyCashHdr();
                pettyHdrDtl.Id = x.Id;
                pettyHdrDtl.TransactionDate = x.TransactionDate;
               
                return pettyHdrDtl;

            });

            return pettyHdrDtls.ToList();

        }

        public async Task<List<dynamic>> GetPettycashDetails(string pettyId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("pettyCashId", pettyId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPettyCashDetails", parms);


            return result.ToList();


        }
        public async Task<List<CustomerPayment>> customerWisePaymentReceiptCancellationList(CustomerPayment data)
        {
            var type = "";
            var report = GetParameter(type, data.FromDate, data.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("Name", data.CustomerName);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetCustomerWisePaymentReceiptCancellationList", parms);
            var getCustomerList = result.Select(x =>
            {
                var customerWiseBalace = new CustomerPayment();

                customerWiseBalace.PaymentDate = x.PaymentDate;
                customerWiseBalace.InvoiceNo = x.BillNo;
                customerWiseBalace.InvoiceDate = x.BillDate;
                customerWiseBalace.InvoiceAmount = x.BillAmount;
                customerWiseBalace.PaymentAmount = x.PaidAmount;
                customerWiseBalace.PaymentType = x.PaymentType;
                customerWiseBalace.ChequeNo = x.ChequeNo;
                customerWiseBalace.ChequeDate = x.ChequeDate;
                customerWiseBalace.CardNo = x.CardNo;
                customerWiseBalace.CardDate = x.CardDate;
                return customerWiseBalace;
            });
            return getCustomerList.ToList();
        }

        public async Task<List<CustomerPayment>> customerPaymentDetails(CustomerPayment data)
        {
            var type = "";
            var report = GetParameter(type, data.FromDate, data.ToDate);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("StartDate", report.FromDate);
            parms.Add("EndDate", report.ToDate);
            parms.Add("Name", data.CustomerName);
            parms.Add("Mobile", data.CustomerMobile);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetCustomerPayment", parms);
            var supplierDetails = result.Select(x =>
            {
                var customerWiseBalace = new CustomerPayment();

                customerWiseBalace.CustomerName = x.Name;
                customerWiseBalace.CustomerMobile = x.Mobile;
                customerWiseBalace.PaymentDate = x.PaymentDate;
                customerWiseBalace.InvoiceNo = x.BillNo;
                customerWiseBalace.InvoiceDate = x.BillDate;
                customerWiseBalace.InvoiceAmount = x.BillAmount;
                customerWiseBalace.PaymentAmount = x.PaidAmount;
                customerWiseBalace.CustomerDebit = x.BalanceAmount;
                customerWiseBalace.PaymentType = x.PaymentType;
                customerWiseBalace.ChequeNo = x.ChequeNo;
                customerWiseBalace.ChequeDate = x.ChequeDate;
                customerWiseBalace.CardNo = x.CardNo;
                customerWiseBalace.CardDate = x.CardDate;

                return customerWiseBalace;
            });
            return supplierDetails.ToList();
        }
    }
}
