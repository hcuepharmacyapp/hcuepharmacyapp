﻿using HQue.Contract.Infrastructure.Reports;
using HQue.DataAccess.DbModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using Newtonsoft.Json.Linq;
using System;

namespace HQue.DataAccess.Report
{
    public class CustomReportDataAccess : BaseDataAccess
    {
        public CustomReportDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {

        }

        public override Task<CustomReport> Save<CustomReport>(CustomReport data)
        {
            return Insert(data, CustomReportTable.Table);
        }

        public async Task<CustomDevReport> SaveDev(CustomDevReport data)
        {
            return await Insert(data, CustomDevReportTable.Table);
        }

        public async Task<CustomDevReport> UpdateReport(CustomDevReport data)
        {
            return await Update(data, CustomDevReportTable.Table);
        }

        public Task<List<CustomReport>> List(string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomReportTable.Table, OperationType.Select);
            qb.AddColumn(CustomReportTable.IdColumn, CustomReportTable.ReportNameColumn,
                CustomReportTable.ReportTypeColumn, CustomReportTable.CreatedAtColumn);

            qb.ConditionBuilder.And(CustomReportTable.InstanceIdColumn, instanceId, CriteriaEquation.Equal);

            qb.SelectBuilder.SortByDesc(CustomReportTable.CreatedAtColumn);

            qb.SelectBuilder.MakeDistinct = true;
            return List<CustomReport>(qb);
        }

        public CustomReport GetCustomReport(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomReportTable.Table, OperationType.Select);

            qb.AddColumn(CustomReportTable.IdColumn, CustomReportTable.ReportNameColumn,
                CustomReportTable.ConfigJSONColumn, CustomReportTable.QueryColumn,
                CustomReportTable.ReportTypeColumn, CustomReportTable.CreatedAtColumn);

            qb.ConditionBuilder.And(CustomReportTable.IdColumn, id, CriteriaEquation.Like);

            return List<CustomReport>(qb).Result.FirstOrDefault();
        }

        public async Task<CustomDevReport> EditReportData(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomDevReportTable.Table, OperationType.Select);

            qb.AddColumn(CustomDevReportTable.IdColumn, CustomDevReportTable.ReportNameColumn,
                CustomDevReportTable.ConfigJSONColumn, CustomDevReportTable.QueryColumn, CustomDevReportTable.FilterJSONColumn,
                CustomDevReportTable.ReportTypeColumn, CustomDevReportTable.CreatedAtColumn);

            qb.ConditionBuilder.And(CustomDevReportTable.IdColumn, id);

            return (await List<CustomDevReport>(qb)).FirstOrDefault();            
        }

        public async Task<JArray> GetReportData(string query)
        {

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            //var data = new List<Dictionary<string, dynamic>>();

            JArray data = new JArray();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    //var row = new Dictionary<string, dynamic>();
                    JObject row = new JObject();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        //row.Add(reader.GetName(i), reader[i]);
                        var value = (reader[i] is DBNull) ? "" : reader[i];
                        row.Add(new JProperty(reader.GetName(i).Replace(".","_"), value));
                    }

                    data.Add(row);
                }

            }
            
            return data;
        }

        public async Task<JArray> GetDevReportData(string query)
        {

            var qb = QueryBuilderFactory.GetQueryBuilder(query.Replace("''", "null"));

            //var data = new List<Dictionary<string, dynamic>>();

            JArray data = new JArray();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    //var row = new Dictionary<string, dynamic>();
                    JObject row = new JObject();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        //row.Add(reader.GetName(i), reader[i]);
                        var value = (reader[i] is DBNull) ? "" : reader[i];
                        row.Add(new JProperty(reader.GetName(i).Replace(" ", ""), value));
                    }

                    data.Add(row);
                }

            }

            return data;
        }


        public Task<List<CustomDevReport>> GetDevReportList(string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomDevReportTable.Table, OperationType.Select);
            qb.AddColumn(CustomDevReportTable.IdColumn, CustomDevReportTable.ReportNameColumn,
                CustomDevReportTable.ReportTypeColumn, CustomDevReportTable.CreatedAtColumn);

            qb.SelectBuilder.SortByDesc(CustomDevReportTable.CreatedAtColumn);

            qb.SelectBuilder.MakeDistinct = true;
            return List<CustomDevReport>(qb);
        }

        public CustomDevReport GetDevCustomReport(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomDevReportTable.Table, OperationType.Select);

            qb.AddColumn(CustomDevReportTable.IdColumn, CustomDevReportTable.ReportNameColumn,
                CustomDevReportTable.ConfigJSONColumn, CustomDevReportTable.QueryColumn, CustomDevReportTable.FilterJSONColumn,
                CustomDevReportTable.ReportTypeColumn, CustomDevReportTable.CreatedAtColumn);

            qb.ConditionBuilder.And(CustomDevReportTable.IdColumn, id, CriteriaEquation.Like);

            return List<CustomDevReport>(qb).Result.FirstOrDefault(); ;
        }
    }
}
