﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HQue.Contract.Infrastructure.Reminder;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using DataAccess.Criteria;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Inventory;
using HQue.Contract.Infrastructure.Master;
using System.Collections;

namespace HQue.DataAccess.Reminder
{
    public class UserReminderDataAccess : BaseDataAccess
    {
        public UserReminderDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {
        }

        public override Task<UserReminder> Save<UserReminder>(UserReminder data)
        {
            return Insert(data, UserReminderTable.Table);
        }

        public Task<List<ReminderRemark>> GetRecentlyReminderRemark(string userId, string accountId, string instanceId)
        {
            var query =
                $@"SELECT ReminderRemark.* FROM ReminderRemark 
                INNER JOIN UserReminder ON ReminderRemark.UserReminderId = UserReminder.Id
                WHERE (EndDate IS NULL OR EndDate > CONVERT(VARCHAR(32),GETDATE(),106))
                AND HqueUserId = '{userId}' AND ReminderRemark.AccountId = '{accountId}' AND ReminderRemark.InstanceId = '{instanceId}'
                AND RemarksFor BETWEEN DATEADD(DAY,-3,GETDATE()) AND DATEADD(DAY,3,GETDATE())";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            return List<ReminderRemark>(qb);
        }

        public async Task<List<CustomerReminder>> GetMissedOrders(CustomerReminder customerReminderReq)
        {
            var query = $@"SELECT M.Id, M.CreatedAt, M.PatientName as CustomerName, M.PatientAge as CustomerAge, M.PatientGender as CustomerGender, M.PatientMobile as ReminderPhone, '' As DoctorName, P.Name as ProductName, M.Quantity 
                            FROM MissedOrder M JOIN Product P on P.Id = M.ProductId 
                            WHERE M.accountId = '{customerReminderReq.AccountId}' AND M.instanceId = '{customerReminderReq.InstanceId}'";

            //if (!string.IsNullOrEmpty(customerReminderReq.CustomerName))
            //    query += $@" And PA.Name = '{customerReminderReq.CustomerName}'";
            //if (!string.IsNullOrEmpty(customerReminderReq.ReminderPhone))
            //    query += $@" And PA.Name = '{customerReminderReq.ReminderPhone}'";
            //if (!string.IsNullOrEmpty(customerReminderReq.DoctorName))
            //    query += $@" And PA.Name = '{customerReminderReq.DoctorName}'";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            var list = new List<CustomerReminder>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var reminderProduct = new ReminderProduct()
                    {
                        ProductName = reader[ReminderProductTable.ProductNameColumn.ColumnName].ToString(),
                        Quantity = Convert.ToInt32(reader[ReminderProductTable.QuantityColumn.ColumnName])
                    };
                    var listProduct = new List<ReminderProduct>();
                    listProduct.Add(reminderProduct);
                    var customerReminder = new CustomerReminder()
                    {
                        ReminderProduct = listProduct,
                        Id = reader["Id"].ToString(),
                        CustomerName = reader["CustomerName"].ToString(),
                        CustomerAge = reader["CustomerAge"].ToString(),
                        CustomerGender = reader["CustomerGender"].ToString(),
                        ReminderPhone = reader["ReminderPhone"].ToString(),
                        DoctorName = "",
                        ReminderDate = Convert.ToDateTime(reader[ReminderProductTable.CreatedAtColumn.ColumnName]),
                        MissedType = 2
                    };
                    list.Add(customerReminder);
                }
            }

            return list;
        }

        public Task<List<ReminderProduct>> GetReminderItem(List<CustomerReminder> list)
        {
            var query = QueryBuilderFactory.GetQueryBuilder(ReminderProductTable.Table, OperationType.Select);

            var i = 1;
            foreach (var listItem in list)
            {
                var paramName = $"p{i++}";
                var caol = new CriteriaColumn(ReminderProductTable.UserReminderIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(caol);
                query.ConditionBuilder.Or(cc);
                query.Parameters.Add(paramName, listItem.Id);
            }

            return List<ReminderProduct>(query);
        }

        public async Task<int> GetReminderCount(UserReminder userReminder)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(UserReminderTable.Table, OperationType.Count);
            GetReminderBaseQuery(userReminder, qb);
            var count = await QueryExecuter.SingleValueAsyc(qb);
            return Convert.ToInt32(count);
        }

        public async Task<int> GetMissedOrderCount(UserReminder userReminder)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(MissedOrderTable.Table, OperationType.Count);
            GetMissedOrderBaseQuery(userReminder, qb);
            var count = await QueryExecuter.SingleValueAsyc(qb);
            return Convert.ToInt32(count);
        }

        public Task<List<UserReminder>> GetActivePersonalReminder(UserReminder userReminder)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(UserReminderTable.Table, OperationType.Select);
            GetReminderBaseQuery(userReminder, qb);
            qb.SelectBuilder.SetPage(userReminder.Page.PageNo, userReminder.Page.PageSize);
            qb.SelectBuilder.SortByDesc(UserReminderTable.StartDateColumn);

            return List<UserReminder>(qb);
        }

        public Task<List<CustomerReminder>> GetActiveCustomerReminder(CustomerReminder customerReminder)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(UserReminderTable.Table, OperationType.Select);
            GetReminderBaseQuery(customerReminder, qb);
            qb.SelectBuilder.SetPage(customerReminder.Page.PageNo, customerReminder.Page.PageSize);
            qb.SelectBuilder.SortByDesc(UserReminderTable.StartDateColumn);

            return List<CustomerReminder>(qb);
        }

        private void GetReminderBaseQuery(UserReminder customerReminder, QueryBuilderBase qb)
        {
            var cc = new CustomCriteria(UserReminderTable.EndDateColumn, CriteriaCondition.IsNull);
            var cc1 = new CustomCriteria(cc, new CustomCriteria(new CriteriaColumn(UserReminderTable.EndDateColumn, CriteriaEquation.Greater)), CriteriaCondition.Or);

            qb.ConditionBuilder.And(cc1)
                .And(UserReminderTable.AccountIdColumn, customerReminder.AccountId)
                .And(UserReminderTable.InstanceIdColumn, customerReminder.InstanceId)
                .And(UserReminderTable.ReminderTypeColumn, customerReminder.ReminderType);

            if (!string.IsNullOrEmpty(customerReminder.CustomerName))
            {
                qb.ConditionBuilder.And(UserReminderTable.CustomerNameColumn, customerReminder.CustomerName);
            }

            if (!string.IsNullOrEmpty(customerReminder.ReminderPhone))
            {
                qb.ConditionBuilder.And(UserReminderTable.ReminderPhoneColumn, customerReminder.ReminderPhone);
            }

            if (!string.IsNullOrEmpty(customerReminder.DoctorName))
            {
                qb.ConditionBuilder.And(UserReminderTable.DoctorNameColumn, customerReminder.DoctorName);
            }
            if (!string.IsNullOrEmpty(customerReminder.HqueUserId))
            {
                qb.ConditionBuilder.And(UserReminderTable.HqueUserIdColumn, customerReminder.HqueUserId);
            }
            qb.Parameters.Add(UserReminderTable.EndDateColumn.ColumnVariable, CustomDateTime.IST.ToFormat());
        }

        private void GetMissedOrderBaseQuery(UserReminder customerReminder, QueryBuilderBase qb)
        {
            qb.ConditionBuilder
                .And(MissedOrderTable.AccountIdColumn, customerReminder.AccountId)
                .And(MissedOrderTable.InstanceIdColumn, customerReminder.InstanceId);

            if (!string.IsNullOrEmpty(customerReminder.CustomerName))
            {
                qb.ConditionBuilder.And(MissedOrderTable.PatientNameColumn, customerReminder.CustomerName);
            }

            if (!string.IsNullOrEmpty(customerReminder.ReminderPhone))
            {
                qb.ConditionBuilder.And(MissedOrderTable.PatientMobileColumn, customerReminder.ReminderPhone);
            }
        }

        public Task<List<ReminderRemark>> GetRecentlyCustomerReminderRemark(string accountId, string instanceId)
        {
            var query =
                $@"SELECT ReminderRemark.* FROM ReminderRemark 
                INNER JOIN UserReminder ON ReminderRemark.UserReminderId = UserReminder.Id
                WHERE (EndDate IS NULL OR EndDate > CONVERT(VARCHAR(32),GETDATE(),106))
                AND ReminderPhone IS NOT NULL AND ReminderRemark.AccountId = '{accountId}' AND ReminderRemark.InstanceId = '{instanceId}'
                AND RemarksFor BETWEEN DATEADD(DAY,-3,GETDATE()) AND DATEADD(DAY,3,GETDATE())";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            return List<ReminderRemark>(qb);

        }

        public async Task<bool> CancelReminder(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(UserReminderTable.Table, OperationType.Update);
            qb.AddColumn(UserReminderTable.EndDateColumn);
            qb.Parameters.Add(UserReminderTable.EndDateColumn.ColumnVariable, CustomDateTime.IST);
            qb.ConditionBuilder.And(UserReminderTable.IdColumn, id);

            await QueryExecuter.NonQueryAsyc(qb);

            return true;
        }

        public async Task<bool> CancelReminder(UserReminder userReminder)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(UserReminderTable.Table, OperationType.Update);
            qb.AddColumn(UserReminderTable.EndDateColumn);
            qb.Parameters.Add(UserReminderTable.EndDateColumn.ColumnVariable, CustomDateTime.IST);
            qb.ConditionBuilder.And(UserReminderTable.IdColumn, userReminder.Id);

            if (userReminder.WriteExecutionQuery)
            {
                userReminder.AddExecutionQuery(qb);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qb);
            }

            return true;
        }

        public async Task<List<UserReminder>> List(UserReminder userReminder)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(UserReminderTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(userReminder.ReferenceId))
                qb.ConditionBuilder.And(UserReminderTable.ReferenceIdColumn, userReminder.ReferenceId);

            return await List<UserReminder>(qb);
        }

        public async Task CreateReminderProduct(IEnumerable<ReminderProduct> data)
        {
            foreach (var item in data)
            {
                if (item.WriteExecutionQuery)
                {
                    SetInsertMetaData(item, ReminderProductTable.Table);
                    WriteInsertExecutionQuery(item, ReminderProductTable.Table);
                }
                else
                {
                    await Insert(item, ReminderProductTable.Table);
                }
            }
        }

        public async Task<List<ReminderRemark>> GetReminderRemarks(IEnumerable<UserReminder> list)
        {
            var query = QueryBuilderFactory.GetQueryBuilder(ReminderRemarkTable.Table, OperationType.Select);

            var i = 0;
            foreach (var listItem in list)
            {
                var idParm = $"id{i++}";
                var dateParm = $"date{i++}";
                var caol = new CriteriaColumn(ReminderRemarkTable.UserReminderIdColumn, $"@{idParm}");
                var dateCol = new CriteriaColumn(ReminderRemarkTable.RemarksForColumn, $"@{dateParm}", CriteriaEquation.GreaterEqual);
                var cc = new CustomCriteria(caol, dateCol, CriteriaCondition.And);
                query.ConditionBuilder.Or(cc);
                query.Parameters.Add(idParm, listItem.Id);
                query.Parameters.Add(dateParm, listItem.ReminderDate);
            }

            return await List<ReminderRemark>(query);
        }
    }
}
