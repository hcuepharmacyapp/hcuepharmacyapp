
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class tempSalesTable
{

	public const string TableName = "tempSales";
public static readonly IDbTable Table = new DbTable("tempSales", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn PatientIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientId");


public static readonly IDbColumn InvoiceNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceNo");


public static readonly IDbColumn InvoiceDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceDate");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn MobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Mobile");


public static readonly IDbColumn EmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Email");


public static readonly IDbColumn DOBColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DOB");


public static readonly IDbColumn AgeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Age");


public static readonly IDbColumn GenderColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gender");


public static readonly IDbColumn AddressColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Address");


public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Pincode");


public static readonly IDbColumn DoctorNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorName");


public static readonly IDbColumn DoctorMobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorMobile");


public static readonly IDbColumn PaymentTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentType");


public static readonly IDbColumn DeliveryTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DeliveryType");


public static readonly IDbColumn BillPrintColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BillPrint");


public static readonly IDbColumn SendSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SendSms");


public static readonly IDbColumn SendEmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SendEmail");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn CreditColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Credit");


public static readonly IDbColumn FileNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FileName");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn CashTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CashType");


public static readonly IDbColumn CardNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardNo");


public static readonly IDbColumn CardDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardDate");


public static readonly IDbColumn CardDigitsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardDigits");


public static readonly IDbColumn CardNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardName");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return PatientIdColumn;


yield return InvoiceNoColumn;


yield return InvoiceDateColumn;


yield return NameColumn;


yield return MobileColumn;


yield return EmailColumn;


yield return DOBColumn;


yield return AgeColumn;


yield return GenderColumn;


yield return AddressColumn;


yield return PincodeColumn;


yield return DoctorNameColumn;


yield return DoctorMobileColumn;


yield return PaymentTypeColumn;


yield return DeliveryTypeColumn;


yield return BillPrintColumn;


yield return SendSmsColumn;


yield return SendEmailColumn;


yield return DiscountColumn;


yield return CreditColumn;


yield return FileNameColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return CashTypeColumn;


yield return CardNoColumn;


yield return CardDateColumn;


yield return CardDigitsColumn;


yield return CardNameColumn;


            
        }

}

}
