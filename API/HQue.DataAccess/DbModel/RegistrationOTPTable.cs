
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class RegistrationOTPTable
{

	public const string TableName = "RegistrationOTP";
public static readonly IDbTable Table = new DbTable("RegistrationOTP", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn UserIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UserId");


public static readonly IDbColumn OTPTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OTPType");


public static readonly IDbColumn OTPNumberColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OTPNumber");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return UserIdColumn;


yield return OTPTypeColumn;


yield return OTPNumberColumn;


yield return CreatedAtColumn;


            
        }

}

}
