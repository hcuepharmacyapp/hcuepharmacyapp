
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class VoucherTable
{

	public const string TableName = "Voucher";
public static readonly IDbTable Table = new DbTable("Voucher", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ReturnIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnId");


public static readonly IDbColumn ParticipantIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ParticipantId");


public static readonly IDbColumn TransactionTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionType");


public static readonly IDbColumn VoucherTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VoucherType");


public static readonly IDbColumn OriginalAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OriginalAmount");


public static readonly IDbColumn FactorColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Factor");


public static readonly IDbColumn AmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Amount");


public static readonly IDbColumn ReturnAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnAmount");


public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Status");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ReturnIdColumn;


yield return ParticipantIdColumn;


yield return TransactionTypeColumn;


yield return VoucherTypeColumn;


yield return OriginalAmountColumn;


yield return FactorColumn;


yield return AmountColumn;


yield return ReturnAmountColumn;


yield return StatusColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
