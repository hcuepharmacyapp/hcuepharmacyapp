
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class LeadsTable
{

	public const string TableName = "Leads";
public static readonly IDbTable Table = new DbTable("Leads", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn PatientIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientId");


public static readonly IDbColumn ExternalIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExternalId");


public static readonly IDbColumn LeadDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LeadDate");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn MobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Mobile");


public static readonly IDbColumn EmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Email");


public static readonly IDbColumn DOBColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DOB");


public static readonly IDbColumn AgeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Age");


public static readonly IDbColumn GenderColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gender");


public static readonly IDbColumn AddressColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Address");


public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Pincode");


public static readonly IDbColumn DoctorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorId");


public static readonly IDbColumn DoctorNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorName");


public static readonly IDbColumn DoctorMobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorMobile");


public static readonly IDbColumn DoctorSpecializationColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorSpecialization");


public static readonly IDbColumn LeadStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LeadStatus");


public static readonly IDbColumn LocalLeadStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LocalLeadStatus");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn LatColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Lat");


public static readonly IDbColumn LngColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Lng");


public static readonly IDbColumn DeliveryModeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DeliveryMode");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return PatientIdColumn;


yield return ExternalIdColumn;


yield return LeadDateColumn;


yield return NameColumn;


yield return MobileColumn;


yield return EmailColumn;


yield return DOBColumn;


yield return AgeColumn;


yield return GenderColumn;


yield return AddressColumn;


yield return PincodeColumn;


yield return DoctorIdColumn;


yield return DoctorNameColumn;


yield return DoctorMobileColumn;


yield return DoctorSpecializationColumn;


yield return LeadStatusColumn;


yield return LocalLeadStatusColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return LatColumn;


yield return LngColumn;


yield return DeliveryModeColumn;


            
        }

}

}
