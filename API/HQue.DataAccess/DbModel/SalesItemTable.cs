
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SalesItemTable
{

	public const string TableName = "SalesItem";
public static readonly IDbTable Table = new DbTable("SalesItem", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SalesIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesId");


public static readonly IDbColumn SaleProductNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SaleProductName");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn ReminderFrequencyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReminderFrequency");


public static readonly IDbColumn ReminderDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReminderDate");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn ItemAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ItemAmount");


public static readonly IDbColumn VATColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VAT");


public static readonly IDbColumn VatAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VatAmount");


public static readonly IDbColumn DiscountAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountAmount");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn SellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SellingPrice");


public static readonly IDbColumn MRPColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MRP");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn IgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Igst");


public static readonly IDbColumn CgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Cgst");


public static readonly IDbColumn SgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Sgst");


public static readonly IDbColumn GstTotalColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstTotal");


public static readonly IDbColumn GstAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstAmount");


public static readonly IDbColumn TotalAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalAmount");


public static readonly IDbColumn SalesOrderEstimateIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesOrderEstimateId");


public static readonly IDbColumn SalesOrderEstimateTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesOrderEstimateType");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn SalesItemSnoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "SalesItemSno");


public static readonly IDbColumn LoyaltyProductPtsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LoyaltyProductPts");

private static IEnumerable<IDbColumn> GetColumn()
{
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SalesIdColumn;


yield return SaleProductNameColumn;


yield return ProductStockIdColumn;


yield return ReminderFrequencyColumn;


yield return ReminderDateColumn;


yield return QuantityColumn;


yield return ItemAmountColumn;


yield return VATColumn;


yield return VatAmountColumn;


yield return DiscountAmountColumn;


yield return DiscountColumn;


yield return SellingPriceColumn;


yield return MRPColumn;


yield return OfflineStatusColumn;


yield return IgstColumn;


yield return CgstColumn;


yield return SgstColumn;


yield return GstTotalColumn;


yield return GstAmountColumn;


yield return TotalAmountColumn;


yield return SalesOrderEstimateIdColumn;


yield return SalesOrderEstimateTypeColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return SalesItemSnoColumn;


yield return LoyaltyProductPtsColumn;

        }

    }

}
