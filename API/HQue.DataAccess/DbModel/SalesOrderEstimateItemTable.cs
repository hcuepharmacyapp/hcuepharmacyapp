















using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SalesOrderEstimateItemTable
{

	public const string TableName = "SalesOrderEstimateItem";
public static readonly IDbTable Table = new DbTable("SalesOrderEstimateItem", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SalesOrderEstimateIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesOrderEstimateId");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn ReceivedQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ReceivedQty");


public static readonly IDbColumn BatchNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BatchNo");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn ExpireDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExpireDate");


public static readonly IDbColumn SellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SellingPrice");


public static readonly IDbColumn IsDeletedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsDeleted");


public static readonly IDbColumn IsActiveColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsActive");


public static readonly IDbColumn IsOrderColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsOrder");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SalesOrderEstimateIdColumn;


yield return ProductIdColumn;


yield return ProductStockIdColumn;


yield return QuantityColumn;


yield return ReceivedQtyColumn;


yield return BatchNoColumn;


yield return DiscountColumn;


yield return ExpireDateColumn;


yield return SellingPriceColumn;


yield return IsDeletedColumn;


yield return IsActiveColumn;


yield return IsOrderColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
