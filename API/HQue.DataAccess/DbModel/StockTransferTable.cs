
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class StockTransferTable
{

	public const string TableName = "StockTransfer";
public static readonly IDbTable Table = new DbTable("StockTransfer", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn FromInstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FromInstanceId");


public static readonly IDbColumn ToInstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ToInstanceId");


public static readonly IDbColumn PrefixColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Prefix");


public static readonly IDbColumn TransferNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferNo");


public static readonly IDbColumn TransferDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferDate");


public static readonly IDbColumn FileNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FileName");


public static readonly IDbColumn CommentsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Comments");


public static readonly IDbColumn TransferStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferStatus");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn TransferByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferBy");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn FinyearIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FinyearId");


public static readonly IDbColumn TaxRefNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TaxRefNo");


public static readonly IDbColumn TransferItemCountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferItemCount");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return FromInstanceIdColumn;


yield return ToInstanceIdColumn;


yield return PrefixColumn;


yield return TransferNoColumn;


yield return TransferDateColumn;


yield return FileNameColumn;


yield return CommentsColumn;


yield return TransferStatusColumn;


yield return OfflineStatusColumn;


yield return TransferByColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return FinyearIdColumn;


yield return TaxRefNoColumn;


yield return TransferItemCountColumn;


            
        }

}

}
