
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SalesOrderEstimateTable
{

	public const string TableName = "SalesOrderEstimate";
public static readonly IDbTable Table = new DbTable("SalesOrderEstimate", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn PatientIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientId");


public static readonly IDbColumn PrefixColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Prefix");


public static readonly IDbColumn OrderEstimateIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OrderEstimateId");


public static readonly IDbColumn OrderEstimateDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OrderEstimateDate");


public static readonly IDbColumn SalesOrderEstimateTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesOrderEstimateType");


public static readonly IDbColumn IsEstimateActiveColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsEstimateActive");


public static readonly IDbColumn FinyearIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FinyearId");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return PatientIdColumn;


yield return PrefixColumn;


yield return OrderEstimateIdColumn;


yield return OrderEstimateDateColumn;


yield return SalesOrderEstimateTypeColumn;


yield return IsEstimateActiveColumn;


yield return FinyearIdColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
