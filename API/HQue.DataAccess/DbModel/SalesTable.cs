
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SalesTable
{

	public const string TableName = "Sales";
public static readonly IDbTable Table = new DbTable("Sales", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn PatientIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientId");


public static readonly IDbColumn InvoiceSeriesColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceSeries");


public static readonly IDbColumn PrefixColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Prefix");


public static readonly IDbColumn InvoiceNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceNo");


public static readonly IDbColumn InvoiceDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceDate");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn MobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Mobile");


public static readonly IDbColumn EmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Email");


public static readonly IDbColumn DOBColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DOB");


public static readonly IDbColumn AgeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Age");


public static readonly IDbColumn GenderColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gender");


public static readonly IDbColumn AddressColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Address");


public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Pincode");


public static readonly IDbColumn DoctorNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorName");


public static readonly IDbColumn DoctorMobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorMobile");


public static readonly IDbColumn PaymentTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentType");


public static readonly IDbColumn DeliveryTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DeliveryType");


public static readonly IDbColumn BillPrintColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BillPrint");


public static readonly IDbColumn SendSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SendSms");


public static readonly IDbColumn SendEmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SendEmail");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn DiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountValue");


public static readonly IDbColumn DiscountTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountType");


public static readonly IDbColumn TotalDiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalDiscountValue");


public static readonly IDbColumn RoundoffNetAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RoundoffNetAmount");


public static readonly IDbColumn CreditColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Credit");


public static readonly IDbColumn FileNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FileName");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn CashTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CashType");


public static readonly IDbColumn CardNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardNo");


public static readonly IDbColumn CardDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardDate");


public static readonly IDbColumn ChequeNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeNo");


public static readonly IDbColumn ChequeDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeDate");


public static readonly IDbColumn CardDigitsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardDigits");


public static readonly IDbColumn CardNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardName");


public static readonly IDbColumn CancelstatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Cancelstatus");


public static readonly IDbColumn SalesTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesType");


public static readonly IDbColumn NetAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NetAmount");


public static readonly IDbColumn SaleAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SaleAmount");


public static readonly IDbColumn RoundoffSaleAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RoundoffSaleAmount");


public static readonly IDbColumn BankDepositedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BankDeposited");


public static readonly IDbColumn AmountCreditedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AmountCredited");


public static readonly IDbColumn FinyearIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FinyearId");


public static readonly IDbColumn DoctorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorId");


public static readonly IDbColumn VatAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VatAmount");


public static readonly IDbColumn SalesItemAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesItemAmount");


public static readonly IDbColumn LoyaltyPtsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LoyaltyPts");


public static readonly IDbColumn RedeemPtsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RedeemPts");


public static readonly IDbColumn RedeemAmtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RedeemAmt");


public static readonly IDbColumn DoorDeliveryStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoorDeliveryStatus");


public static readonly IDbColumn GstAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstAmount");


public static readonly IDbColumn IsRoundOffColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsRoundOff");


public static readonly IDbColumn TaxRefNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TaxRefNo");


public static readonly IDbColumn LoyaltyIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LoyaltyId");


public static readonly IDbColumn RedeemPercentColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RedeemPercent");


private static IEnumerable<IDbColumn> GetColumn()
{
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return PatientIdColumn;


yield return InvoiceSeriesColumn;


yield return PrefixColumn;


yield return InvoiceNoColumn;


yield return InvoiceDateColumn;


yield return NameColumn;


yield return MobileColumn;


yield return EmailColumn;


yield return DOBColumn;


yield return AgeColumn;


yield return GenderColumn;


yield return AddressColumn;


yield return PincodeColumn;


yield return DoctorNameColumn;


yield return DoctorMobileColumn;


yield return PaymentTypeColumn;


yield return DeliveryTypeColumn;


yield return BillPrintColumn;


yield return SendSmsColumn;


yield return SendEmailColumn;


yield return DiscountColumn;


yield return DiscountValueColumn;


yield return DiscountTypeColumn;


yield return TotalDiscountValueColumn;


yield return RoundoffNetAmountColumn;


yield return CreditColumn;


yield return FileNameColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return CashTypeColumn;


yield return CardNoColumn;


yield return CardDateColumn;


yield return ChequeNoColumn;


yield return ChequeDateColumn;


yield return CardDigitsColumn;


yield return CardNameColumn;


yield return CancelstatusColumn;


yield return SalesTypeColumn;


yield return NetAmountColumn;


yield return SaleAmountColumn;


yield return RoundoffSaleAmountColumn;


yield return BankDepositedColumn;


yield return AmountCreditedColumn;


yield return FinyearIdColumn;


yield return DoctorIdColumn;


yield return VatAmountColumn;


yield return SalesItemAmountColumn;


yield return LoyaltyPtsColumn;


yield return RedeemPtsColumn;


yield return RedeemAmtColumn;


yield return DoorDeliveryStatusColumn;


yield return GstAmountColumn;


yield return IsRoundOffColumn;


yield return TaxRefNoColumn;


yield return LoyaltyIdColumn;


yield return RedeemPercentColumn;


        }

}

}
