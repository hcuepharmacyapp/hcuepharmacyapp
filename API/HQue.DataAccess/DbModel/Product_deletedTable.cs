
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class Product_deletedTable
{

	public const string TableName = "Product_deleted";
public static readonly IDbTable Table = new DbTable("Product_deleted", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn CodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Code");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn ManufacturerColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Manufacturer");


public static readonly IDbColumn KindNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"KindName");


public static readonly IDbColumn StrengthNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StrengthName");


public static readonly IDbColumn TypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Type");


public static readonly IDbColumn ScheduleColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Schedule");


public static readonly IDbColumn CategoryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Category");


public static readonly IDbColumn GenericNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GenericName");


public static readonly IDbColumn CommodityCodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CommodityCode");


public static readonly IDbColumn PackingColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Packing");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn PackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSize");


public static readonly IDbColumn VATColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VAT");


public static readonly IDbColumn PriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Price");


public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Status");


public static readonly IDbColumn RackNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RackNo");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return CodeColumn;


yield return NameColumn;


yield return ManufacturerColumn;


yield return KindNameColumn;


yield return StrengthNameColumn;


yield return TypeColumn;


yield return ScheduleColumn;


yield return CategoryColumn;


yield return GenericNameColumn;


yield return CommodityCodeColumn;


yield return PackingColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return PackageSizeColumn;


yield return VATColumn;


yield return PriceColumn;


yield return StatusColumn;


yield return RackNoColumn;


            
        }

}

}
