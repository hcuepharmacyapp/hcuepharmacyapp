
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class ReplicationDataTable
{

	public const string TableName = "ReplicationData";
public static readonly IDbTable Table = new DbTable("ReplicationData", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ClientIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ClientId");


public static readonly IDbColumn TableNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TableName");


public static readonly IDbColumn ActionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Action");


public static readonly IDbColumn ActionTimeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ActionTime");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn TransactionIDColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionID");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ClientIdColumn;


yield return TableNameColumn;


yield return ActionColumn;


yield return ActionTimeColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return TransactionIDColumn;


            
        }

}

}
