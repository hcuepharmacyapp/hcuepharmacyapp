
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class PettyCashTable
{

	public const string TableName = "PettyCash";
public static readonly IDbTable Table = new DbTable("PettyCash", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn UserIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UserId");


public static readonly IDbColumn TitleColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Title");


public static readonly IDbColumn CashTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CashType");


public static readonly IDbColumn DescriptionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Description");


public static readonly IDbColumn AmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Amount");


public static readonly IDbColumn TransactionDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionDate");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return UserIdColumn;


yield return TitleColumn;


yield return CashTypeColumn;


yield return DescriptionColumn;


yield return AmountColumn;


yield return TransactionDateColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
