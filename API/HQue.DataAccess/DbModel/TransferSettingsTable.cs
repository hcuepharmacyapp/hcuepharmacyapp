
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class TransferSettingsTable
{

	public const string TableName = "TransferSettings";
public static readonly IDbTable Table = new DbTable("TransferSettings", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ProductBatchDisplayTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductBatchDisplayType");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn TransferQtyTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferQtyType");


public static readonly IDbColumn TransferZeroStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferZeroStock");


public static readonly IDbColumn ShowExpiredQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ShowExpiredQty");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ProductBatchDisplayTypeColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return TransferQtyTypeColumn;


yield return TransferZeroStockColumn;


yield return ShowExpiredQtyColumn;


            
        }

}

}
