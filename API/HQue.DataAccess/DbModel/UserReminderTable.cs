
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class UserReminderTable
{

	public const string TableName = "UserReminder";
public static readonly IDbTable Table = new DbTable("UserReminder", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn HqueUserIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"HqueUserId");


public static readonly IDbColumn ReferenceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReferenceId");


public static readonly IDbColumn ReferenceFromColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReferenceFrom");


public static readonly IDbColumn ReminderPhoneColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReminderPhone");


public static readonly IDbColumn CustomerNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CustomerName");


public static readonly IDbColumn CustomerAgeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CustomerAge");


public static readonly IDbColumn CustomerGenderColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CustomerGender");


public static readonly IDbColumn DoctorNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorName");


public static readonly IDbColumn StartDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StartDate");


public static readonly IDbColumn ReminderTimeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReminderTime");


public static readonly IDbColumn EndDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"EndDate");


public static readonly IDbColumn ReminderFrequencyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReminderFrequency");


public static readonly IDbColumn ReminderStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReminderStatus");


public static readonly IDbColumn ReminderTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReminderType");


public static readonly IDbColumn DescriptionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Description");


public static readonly IDbColumn RemarksColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Remarks");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return HqueUserIdColumn;


yield return ReferenceIdColumn;


yield return ReferenceFromColumn;


yield return ReminderPhoneColumn;


yield return CustomerNameColumn;


yield return CustomerAgeColumn;


yield return CustomerGenderColumn;


yield return DoctorNameColumn;


yield return StartDateColumn;


yield return ReminderTimeColumn;


yield return EndDateColumn;


yield return ReminderFrequencyColumn;


yield return ReminderStatusColumn;


yield return ReminderTypeColumn;


yield return DescriptionColumn;


yield return RemarksColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
