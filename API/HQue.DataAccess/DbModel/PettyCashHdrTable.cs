
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class PettyCashHdrTable
{

	public const string TableName = "PettyCashHdr";
public static readonly IDbTable Table = new DbTable("PettyCashHdr", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn UserIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UserId");


public static readonly IDbColumn TransactionDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionDate");


public static readonly IDbColumn DescriptionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Description");


public static readonly IDbColumn clbalanceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"clbalance");


public static readonly IDbColumn FloatingAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FloatingAmount");


public static readonly IDbColumn TotalSalesColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalSales");


public static readonly IDbColumn TotalExpenseColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalExpense");


public static readonly IDbColumn VendorPaymentColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorPayment");


public static readonly IDbColumn noof2000Column = new global::DataAccess.Criteria.DbColumn(TableName,"noof2000");


public static readonly IDbColumn noof1000Column = new global::DataAccess.Criteria.DbColumn(TableName,"noof1000");


public static readonly IDbColumn noof500Column = new global::DataAccess.Criteria.DbColumn(TableName,"noof500");


public static readonly IDbColumn noof200Column = new global::DataAccess.Criteria.DbColumn(TableName,"noof200");


public static readonly IDbColumn noof100Column = new global::DataAccess.Criteria.DbColumn(TableName,"noof100");


public static readonly IDbColumn noof50Column = new global::DataAccess.Criteria.DbColumn(TableName,"noof50");


public static readonly IDbColumn noof20Column = new global::DataAccess.Criteria.DbColumn(TableName,"noof20");


public static readonly IDbColumn noof10Column = new global::DataAccess.Criteria.DbColumn(TableName,"noof10");


public static readonly IDbColumn OthersColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Others");


public static readonly IDbColumn coinsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"coins");


public static readonly IDbColumn CustomerPaymentColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CustomerPayment");


public static readonly IDbColumn CounterNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CounterNo");


public static readonly IDbColumn DeletedStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DeletedStatus");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return UserIdColumn;


yield return TransactionDateColumn;


yield return DescriptionColumn;


yield return clbalanceColumn;


yield return FloatingAmountColumn;


yield return TotalSalesColumn;


yield return TotalExpenseColumn;


yield return VendorPaymentColumn;


yield return noof2000Column;


yield return noof1000Column;


yield return noof500Column;


yield return noof200Column;


yield return noof100Column;


yield return noof50Column;


yield return noof20Column;


yield return noof10Column;


yield return OthersColumn;


yield return coinsColumn;


yield return CustomerPaymentColumn;


yield return CounterNoColumn;


yield return DeletedStatusColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
