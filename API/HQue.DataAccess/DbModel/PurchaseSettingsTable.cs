
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

    public class PurchaseSettingsTable
    {

        public const string TableName = "PurchaseSettings";
        public static readonly IDbTable Table = new DbTable("PurchaseSettings", GetColumn);


        public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Id");


        public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "AccountId");


        public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InstanceId");


        public static readonly IDbColumn TaxTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "TaxType");


        public static readonly IDbColumn BuyInvoiceDateEditColumn = new global::DataAccess.Criteria.DbColumn(TableName, "BuyInvoiceDateEdit");


        public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OfflineStatus");


        public static readonly IDbColumn IsAllowDecimalColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsAllowDecimal");


        public static readonly IDbColumn CompletePurchaseKeyTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CompletePurchaseKeyType");


        public static readonly IDbColumn BillSeriesTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "BillSeriesType");


        public static readonly IDbColumn CustomSeriesNameColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CustomSeriesName");


        public static readonly IDbColumn EnableSellingColumn = new global::DataAccess.Criteria.DbColumn(TableName, "EnableSelling");


        public static readonly IDbColumn IsSortByLowestPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSortByLowestPrice");


        public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedAt");


        public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedAt");


        public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedBy");


        public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedBy");


        public static readonly IDbColumn EnableMarkupColumn = new global::DataAccess.Criteria.DbColumn(TableName, "EnableMarkup");


        public static readonly IDbColumn InvoiceValueSettingColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InvoiceValueSetting");


        public static readonly IDbColumn InvoiceValueDifferenceColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InvoiceValueDifference");



        private static IEnumerable<IDbColumn> GetColumn()
        {

            yield return IdColumn;


            yield return AccountIdColumn;


            yield return InstanceIdColumn;


            yield return TaxTypeColumn;


            yield return BuyInvoiceDateEditColumn;


            yield return OfflineStatusColumn;


            yield return IsAllowDecimalColumn;


            yield return CompletePurchaseKeyTypeColumn;


            yield return BillSeriesTypeColumn;


            yield return CustomSeriesNameColumn;


            yield return EnableSellingColumn;


            yield return IsSortByLowestPriceColumn;


            yield return CreatedAtColumn;


            yield return UpdatedAtColumn;


            yield return CreatedByColumn;


            yield return UpdatedByColumn;


            yield return EnableMarkupColumn;


            yield return InvoiceValueSettingColumn;


            yield return InvoiceValueDifferenceColumn;


        }

    }

}
