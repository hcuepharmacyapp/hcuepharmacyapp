
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class VendorOrderItemTable
{

	public const string TableName = "VendorOrderItem";
public static readonly IDbTable Table = new DbTable("VendorOrderItem", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn VendorOrderIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorOrderId");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn ReceivedQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReceivedQty");


public static readonly IDbColumn VATColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VAT");


public static readonly IDbColumn PackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSize");


public static readonly IDbColumn PackageQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageQty");


public static readonly IDbColumn PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PurchasePrice");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn VendorPurchaseIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorPurchaseId");


public static readonly IDbColumn StockTransferIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StockTransferId");


public static readonly IDbColumn SellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SellingPrice");


public static readonly IDbColumn ExpireDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExpireDate");


public static readonly IDbColumn ToInstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ToInstanceId");


public static readonly IDbColumn IsDeletedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsDeleted");


public static readonly IDbColumn IgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Igst");


public static readonly IDbColumn CgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Cgst");


public static readonly IDbColumn SgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Sgst");


public static readonly IDbColumn GstTotalColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstTotal");

public static readonly IDbColumn OrderItemSnoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OrderItemSno");

public static readonly IDbColumn FreeQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName, "FreeQty");

        private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return VendorOrderIdColumn;


yield return ProductIdColumn;


yield return QuantityColumn;


yield return ReceivedQtyColumn;


yield return VATColumn;


yield return PackageSizeColumn;


yield return PackageQtyColumn;


yield return PurchasePriceColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return VendorPurchaseIdColumn;


yield return StockTransferIdColumn;


yield return SellingPriceColumn;


yield return ExpireDateColumn;


yield return ToInstanceIdColumn;


yield return IsDeletedColumn;


yield return IgstColumn;


yield return CgstColumn;


yield return SgstColumn;


yield return GstTotalColumn;


yield return OrderItemSnoColumn;


yield return FreeQtyColumn;

        }

}

}
