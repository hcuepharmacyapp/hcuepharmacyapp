
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class ErrorLogTable
{

	public const string TableName = "ErrorLog";
public static readonly IDbTable Table = new DbTable("ErrorLog", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ErrorMessageColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ErrorMessage");


public static readonly IDbColumn ErrorStackTraceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ErrorStackTrace");


public static readonly IDbColumn UnSavedDataColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UnSavedData");


public static readonly IDbColumn DataColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Data");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ErrorMessageColumn;


yield return ErrorStackTraceColumn;


yield return UnSavedDataColumn;


yield return DataColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
