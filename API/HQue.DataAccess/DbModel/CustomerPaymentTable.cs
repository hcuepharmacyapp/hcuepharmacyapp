
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class CustomerPaymentTable
{

	public const string TableName = "CustomerPayment";
public static readonly IDbTable Table = new DbTable("CustomerPayment", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SalesIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesId");


public static readonly IDbColumn CustomerIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CustomerId");


public static readonly IDbColumn TransactionDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionDate");


public static readonly IDbColumn DebitColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Debit");


public static readonly IDbColumn CreditColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Credit");


public static readonly IDbColumn PaymentTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentType");


public static readonly IDbColumn ChequeNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeNo");


public static readonly IDbColumn ChequeDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeDate");


public static readonly IDbColumn CardNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardNo");


public static readonly IDbColumn CardDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardDate");


public static readonly IDbColumn RemarksColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Remarks");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn BankDepositedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BankDeposited");


public static readonly IDbColumn AmountCreditedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AmountCredited");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SalesIdColumn;


yield return CustomerIdColumn;


yield return TransactionDateColumn;


yield return DebitColumn;


yield return CreditColumn;


yield return PaymentTypeColumn;


yield return ChequeNoColumn;


yield return ChequeDateColumn;


yield return CardNoColumn;


yield return CardDateColumn;


yield return RemarksColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return BankDepositedColumn;


yield return AmountCreditedColumn;


            
        }

}

}
