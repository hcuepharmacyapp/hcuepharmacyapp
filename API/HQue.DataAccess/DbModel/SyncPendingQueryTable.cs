
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SyncPendingQueryTable 
{

	public const string TableName = "SyncPendingQuery";
public static readonly IDbTable Table = new DbTable("SyncPendingQuery", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SyncQueryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SyncQuery");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SyncQueryColumn;


yield return CreatedAtColumn;


            
        }

}

}
