
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class DiscountRulesTable
{

	public const string TableName = "DiscountRules";
public static readonly IDbTable Table = new DbTable("DiscountRules", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn AmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Amount");


public static readonly IDbColumn ToAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ToAmount");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn FromDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FromDate");


public static readonly IDbColumn ToDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ToDate");


public static readonly IDbColumn BillAmountTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BillAmountType");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return AmountColumn;


yield return ToAmountColumn;


yield return DiscountColumn;


yield return FromDateColumn;


yield return ToDateColumn;


yield return BillAmountTypeColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
