
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

    public class VendorTable
    {

        public const string TableName = "Vendor";
        public static readonly IDbTable Table = new DbTable("Vendor", GetColumn);


        public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Id");


        public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "AccountId");


        public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InstanceId");


        public static readonly IDbColumn CodeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Code");


        public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Name");


        public static readonly IDbColumn MobileColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Mobile");


        public static readonly IDbColumn PhoneColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Phone");


        public static readonly IDbColumn EmailColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Email");


        public static readonly IDbColumn AddressColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Address");


        public static readonly IDbColumn AreaColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Area");


        public static readonly IDbColumn CityColumn = new global::DataAccess.Criteria.DbColumn(TableName, "City");


        public static readonly IDbColumn StateColumn = new global::DataAccess.Criteria.DbColumn(TableName, "State");


        public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Pincode");


        public static readonly IDbColumn AllowViewStockColumn = new global::DataAccess.Criteria.DbColumn(TableName, "AllowViewStock");


        public static readonly IDbColumn DrugLicenseNoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "DrugLicenseNo");


        public static readonly IDbColumn TinNoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "TinNo");


        public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Status");


        public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OfflineStatus");


        public static readonly IDbColumn PaymentTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "PaymentType");


        public static readonly IDbColumn CreditNoOfDaysColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreditNoOfDays");


        public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedAt");


        public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedAt");


        public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedBy");


        public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedBy");


        public static readonly IDbColumn EnableCSTColumn = new global::DataAccess.Criteria.DbColumn(TableName, "EnableCST");


        public static readonly IDbColumn ContactPersonColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ContactPerson");


        public static readonly IDbColumn DesignationColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Designation");


        public static readonly IDbColumn LandmarkColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Landmark");


        public static readonly IDbColumn CSTNoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CSTNo");


        public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Discount");


        public static readonly IDbColumn Mobile1Column = new global::DataAccess.Criteria.DbColumn(TableName, "Mobile1");


        public static readonly IDbColumn Mobile2Column = new global::DataAccess.Criteria.DbColumn(TableName, "Mobile2");


        public static readonly IDbColumn Mobile1checkedColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Mobile1checked");


        public static readonly IDbColumn Mobile2checkedColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Mobile2checked");


        public static readonly IDbColumn IsSendMailColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSendMail");


        public static readonly IDbColumn TaxTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "TaxType");


        public static readonly IDbColumn GsTinNoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "GsTinNo");


        public static readonly IDbColumn LocationTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LocationType");


        public static readonly IDbColumn StateIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "StateId");


        public static readonly IDbColumn Ext_RefIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Ext_RefId");


        public static readonly IDbColumn VendorTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "VendorType");


        public static readonly IDbColumn BalanceAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName, "BalanceAmount");


        public static readonly IDbColumn VendorPurchaseFormulaIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "VendorPurchaseFormulaId");


        public static readonly IDbColumn IsHeadOfficeVendorColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsHeadOfficeVendor");


        private static IEnumerable<IDbColumn> GetColumn()
        {

            yield return IdColumn;


            yield return AccountIdColumn;


            yield return InstanceIdColumn;


            yield return CodeColumn;


            yield return NameColumn;


            yield return MobileColumn;


            yield return PhoneColumn;


            yield return EmailColumn;


            yield return AddressColumn;


            yield return AreaColumn;


            yield return CityColumn;


            yield return StateColumn;


            yield return PincodeColumn;


            yield return AllowViewStockColumn;


            yield return DrugLicenseNoColumn;


            yield return TinNoColumn;


            yield return StatusColumn;


            yield return OfflineStatusColumn;


            yield return PaymentTypeColumn;


            yield return CreditNoOfDaysColumn;


            yield return CreatedAtColumn;


            yield return UpdatedAtColumn;


            yield return CreatedByColumn;


            yield return UpdatedByColumn;


            yield return EnableCSTColumn;


            yield return ContactPersonColumn;


            yield return DesignationColumn;


            yield return LandmarkColumn;


            yield return CSTNoColumn;


            yield return DiscountColumn;


            yield return Mobile1Column;


            yield return Mobile2Column;


            yield return Mobile1checkedColumn;


            yield return Mobile2checkedColumn;


            yield return IsSendMailColumn;


            yield return TaxTypeColumn;


            yield return GsTinNoColumn;


            yield return LocationTypeColumn;


            yield return StateIdColumn;


            yield return Ext_RefIdColumn;


            yield return VendorTypeColumn;


            yield return BalanceAmountColumn;


            yield return VendorPurchaseFormulaIdColumn;


            yield return IsHeadOfficeVendorColumn;



        }

    }

}
