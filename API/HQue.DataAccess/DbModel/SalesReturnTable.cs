
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SalesReturnTable
{

	public const string TableName = "SalesReturn";
public static readonly IDbTable Table = new DbTable("SalesReturn", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SalesIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesId");


public static readonly IDbColumn PatientIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientId");


public static readonly IDbColumn InvoiceSeriesColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceSeries");


public static readonly IDbColumn PrefixColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Prefix");


public static readonly IDbColumn ReturnNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnNo");


public static readonly IDbColumn ReturnDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnDate");


public static readonly IDbColumn CancelTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CancelType");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn FinyearIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FinyearId");


public static readonly IDbColumn IsAlongWithSaleColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsAlongWithSale");


public static readonly IDbColumn TaxRefNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TaxRefNo");


public static readonly IDbColumn ReturnChargesColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnCharges");


public static readonly IDbColumn ReturnChargePercentColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnChargePercent");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn DiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountValue");


public static readonly IDbColumn NetAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NetAmount");


public static readonly IDbColumn RoundOffNetAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RoundOffNetAmount");


public static readonly IDbColumn TotalDiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalDiscountValue");


public static readonly IDbColumn ReturnItemAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnItemAmount");


public static readonly IDbColumn GstAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstAmount");


public static readonly IDbColumn IsRoundOffColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsRoundOff");


public static readonly IDbColumn LoyaltyPtsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LoyaltyPts");



public static readonly IDbColumn LoyaltyIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LoyaltyId");


private static IEnumerable<IDbColumn> GetColumn()
{
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SalesIdColumn;


yield return PatientIdColumn;


yield return InvoiceSeriesColumn;


yield return PrefixColumn;


yield return ReturnNoColumn;


yield return ReturnDateColumn;


yield return CancelTypeColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return FinyearIdColumn;


yield return IsAlongWithSaleColumn;


yield return TaxRefNoColumn;


yield return ReturnChargesColumn;


yield return ReturnChargePercentColumn;


yield return DiscountColumn;


yield return DiscountValueColumn;


yield return NetAmountColumn;


yield return RoundOffNetAmountColumn;


yield return TotalDiscountValueColumn;


yield return ReturnItemAmountColumn;


yield return GstAmountColumn;


yield return IsRoundOffColumn;


yield return LoyaltyIdColumn;


yield return LoyaltyPtsColumn;


            
}

}

}
