
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class DomainValuesTable
{

	public const string TableName = "DomainValues";
public static readonly IDbTable Table = new DbTable("DomainValues", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn DomainIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DomainId");


public static readonly IDbColumn DomainValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DomainValue");


public static readonly IDbColumn DisplayTextColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DisplayText");


public static readonly IDbColumn HasSubdomainColumn = new global::DataAccess.Criteria.DbColumn(TableName,"HasSubdomain");


public static readonly IDbColumn SubIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SubId");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return DomainIdColumn;


yield return DomainValueColumn;


yield return DisplayTextColumn;


yield return HasSubdomainColumn;


yield return SubIdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
