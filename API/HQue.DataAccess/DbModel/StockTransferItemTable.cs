
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class StockTransferItemTable
{

	public const string TableName = "StockTransferItem";
public static readonly IDbTable Table = new DbTable("StockTransferItem", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn TransferIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn VendorOrderItemIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorOrderItemId");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn BatchNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BatchNo");


public static readonly IDbColumn ExpireDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExpireDate");


public static readonly IDbColumn VATColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VAT");


public static readonly IDbColumn PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PurchasePrice");


public static readonly IDbColumn SellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SellingPrice");


public static readonly IDbColumn MRPColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MRP");


public static readonly IDbColumn PackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSize");


public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorId");


public static readonly IDbColumn ToProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ToProductStockId");


public static readonly IDbColumn AcceptedStripColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AcceptedStrip");


public static readonly IDbColumn AcceptedPackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AcceptedPackageSize");


public static readonly IDbColumn AcceptedUnitsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AcceptedUnits");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn ToInstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ToInstanceId");


public static readonly IDbColumn isStripTransferColumn = new global::DataAccess.Criteria.DbColumn(TableName,"isStripTransfer");


public static readonly IDbColumn IgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Igst");


public static readonly IDbColumn CgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Cgst");


public static readonly IDbColumn SgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Sgst");


public static readonly IDbColumn GstTotalColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstTotal");


public static readonly IDbColumn VendorPurchaseItemIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorPurchaseItemId");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return TransferIdColumn;


yield return ProductStockIdColumn;


yield return VendorOrderItemIdColumn;


yield return QuantityColumn;


yield return DiscountColumn;


yield return ProductIdColumn;


yield return BatchNoColumn;


yield return ExpireDateColumn;


yield return VATColumn;


yield return PurchasePriceColumn;


yield return SellingPriceColumn;


yield return MRPColumn;


yield return PackageSizeColumn;


yield return VendorIdColumn;


yield return ToProductStockIdColumn;


yield return AcceptedStripColumn;


yield return AcceptedPackageSizeColumn;


yield return AcceptedUnitsColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return ToInstanceIdColumn;


yield return isStripTransferColumn;


yield return IgstColumn;


yield return CgstColumn;


yield return SgstColumn;


yield return GstTotalColumn;


yield return VendorPurchaseItemIdColumn;


            
        }

}

}
