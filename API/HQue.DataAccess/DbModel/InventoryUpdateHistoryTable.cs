
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class InventoryUpdateHistoryTable
{

	public const string TableName = "InventoryUpdateHistory";
public static readonly IDbTable Table = new DbTable("InventoryUpdateHistory", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn BatchNoOldColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BatchNoOld");


public static readonly IDbColumn BatchNoNewColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BatchNoNew");


public static readonly IDbColumn ExpireDateOldColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExpireDateOld");


public static readonly IDbColumn ExpireDateNewColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExpireDateNew");


public static readonly IDbColumn VATOldColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VATOld");


public static readonly IDbColumn VATNewColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VATNew");


public static readonly IDbColumn PackageSizeOldColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSizeOld");


public static readonly IDbColumn PackageSizeNewColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSizeNew");


public static readonly IDbColumn PackagePurchasePriceOldColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackagePurchasePriceOld");


public static readonly IDbColumn PackagePurchasePriceNewColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackagePurchasePriceNew");


public static readonly IDbColumn SellingPriceOldColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SellingPriceOld");


public static readonly IDbColumn SellingPriceNewColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SellingPriceNew");


public static readonly IDbColumn MRPOldColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MRPOld");


public static readonly IDbColumn MRPNewColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MRPNew");


public static readonly IDbColumn GSTOldColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GSTOld");


public static readonly IDbColumn GSTNewColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GSTNew");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ProductStockIdColumn;


yield return BatchNoOldColumn;


yield return BatchNoNewColumn;


yield return ExpireDateOldColumn;


yield return ExpireDateNewColumn;


yield return VATOldColumn;


yield return VATNewColumn;


yield return PackageSizeOldColumn;


yield return PackageSizeNewColumn;


yield return PackagePurchasePriceOldColumn;


yield return PackagePurchasePriceNewColumn;


yield return SellingPriceOldColumn;


yield return SellingPriceNewColumn;


yield return MRPOldColumn;


yield return MRPNewColumn;


yield return GSTOldColumn;


yield return GSTNewColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
