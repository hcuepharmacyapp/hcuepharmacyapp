
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class ClosingStockTable
{

	public const string TableName = "ClosingStock";
public static readonly IDbTable Table = new DbTable("ClosingStock", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn TransDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransDate");


public static readonly IDbColumn ClosingStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ClosingStock");


public static readonly IDbColumn StockValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StockValue");


public static readonly IDbColumn GstTotalColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstTotal");


public static readonly IDbColumn IsOfflineColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsOffline");


public static readonly IDbColumn IsSynctoOnlineColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsSynctoOnline");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ProductStockIdColumn;


yield return TransDateColumn;


yield return ClosingStockColumn;


yield return StockValueColumn;


yield return GstTotalColumn;


yield return IsOfflineColumn;


yield return IsSynctoOnlineColumn;


yield return CreatedAtColumn;


yield return CreatedByColumn;


            
        }

}

}
