
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class LoyaltyProductPointsTable
{

	public const string TableName = "LoyaltyProductPoints";
public static readonly IDbTable Table = new DbTable("LoyaltyProductPoints", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn LoyaltyIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LoyaltyId");


public static readonly IDbColumn KindNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"KindName");


public static readonly IDbColumn KindOfProductPtsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"KindOfProductPts");


public static readonly IDbColumn IsActiveColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsActive");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return LoyaltyIdColumn;


yield return KindNameColumn;


yield return KindOfProductPtsColumn;


yield return IsActiveColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
