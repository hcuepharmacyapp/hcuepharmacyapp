
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class LoyaltyPointSettingsTable
{

	public const string TableName = "LoyaltyPointSettings";
public static readonly IDbTable Table = new DbTable("LoyaltyPointSettings", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn LoyaltySaleValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LoyaltySaleValue");


public static readonly IDbColumn LoyaltyPointColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LoyaltyPoint");


public static readonly IDbColumn StartDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StartDate");


public static readonly IDbColumn EndDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"EndDate");


public static readonly IDbColumn MinimumSalesAmtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MinimumSalesAmt");


public static readonly IDbColumn RedeemPointColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RedeemPoint");


public static readonly IDbColumn RedeemValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RedeemValue");


public static readonly IDbColumn MaximumRedeemPointColumn = new global::DataAccess.Criteria.DbColumn(TableName, "MaximumRedeemPoint");


public static readonly IDbColumn IsActiveColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsActive");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn LoyaltyOptionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LoyaltyOption");


public static readonly IDbColumn IsMinimumSaleColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsMinimumSale");


public static readonly IDbColumn IsMaximumRedeemColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsMaximumRedeem");


public static readonly IDbColumn IsEndDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsEndDate");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return LoyaltySaleValueColumn;


yield return LoyaltyPointColumn;


yield return StartDateColumn;


yield return EndDateColumn;


yield return MinimumSalesAmtColumn;


yield return RedeemPointColumn;


yield return RedeemValueColumn;


yield return MaximumRedeemPointColumn;


yield return IsActiveColumn;


yield return OfflineStatusColumn;


yield return LoyaltyOptionColumn;


yield return IsMinimumSaleColumn;


yield return IsMaximumRedeemColumn;


yield return IsEndDateColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
