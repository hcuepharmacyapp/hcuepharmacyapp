
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class PhysicalStockHistoryTable
{

	public const string TableName = "PhysicalStockHistory";
public static readonly IDbTable Table = new DbTable("PhysicalStockHistory", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn PopupNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PopupNo");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn CurrentStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CurrentStock");


public static readonly IDbColumn PhysicalStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PhysicalStock");


public static readonly IDbColumn OpenedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OpenedAt");


public static readonly IDbColumn ClosedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ClosedAt");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return PopupNoColumn;


yield return ProductIdColumn;


yield return CurrentStockColumn;


yield return PhysicalStockColumn;


yield return OpenedAtColumn;


yield return ClosedAtColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
