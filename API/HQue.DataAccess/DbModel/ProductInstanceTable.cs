
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class ProductInstanceTable
{

	public const string TableName = "ProductInstance";
public static readonly IDbTable Table = new DbTable("ProductInstance", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn ReOrderLevelColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReOrderLevel");


public static readonly IDbColumn ReOrderQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReOrderQty");


public static readonly IDbColumn ReOrderModifiedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReOrderModified");


public static readonly IDbColumn RackNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RackNo");


public static readonly IDbColumn BoxNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BoxNo");


public static readonly IDbColumn Ext_RefIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Ext_RefId");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ProductIdColumn;


yield return ReOrderLevelColumn;


yield return ReOrderQtyColumn;


yield return ReOrderModifiedColumn;


yield return RackNoColumn;


yield return BoxNoColumn;


yield return Ext_RefIdColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
