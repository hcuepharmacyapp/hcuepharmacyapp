
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class VendorPurchaseItemAuditTable
{

	public const string TableName = "VendorPurchaseItemAudit";
public static readonly IDbTable Table = new DbTable("VendorPurchaseItemAudit", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn VendorPurchaseIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorPurchaseId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn PackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSize");


public static readonly IDbColumn PackageQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageQty");


public static readonly IDbColumn PackagePurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackagePurchasePrice");


public static readonly IDbColumn PackageSellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSellingPrice");


public static readonly IDbColumn PackageMRPColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageMRP");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PurchasePrice");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn FreeQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FreeQty");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn MarkupPercColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MarkupPerc");


public static readonly IDbColumn SchemeDiscountPercColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SchemeDiscountPerc");


public static readonly IDbColumn ActionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Action");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return VendorPurchaseIdColumn;


yield return ProductStockIdColumn;


yield return PackageSizeColumn;


yield return PackageQtyColumn;


yield return PackagePurchasePriceColumn;


yield return PackageSellingPriceColumn;


yield return PackageMRPColumn;


yield return QuantityColumn;


yield return PurchasePriceColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return FreeQtyColumn;


yield return DiscountColumn;


yield return MarkupPercColumn;


yield return SchemeDiscountPercColumn;


yield return ActionColumn;


            
        }

}

}
