
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SchedulerLogTable
{

	public const string TableName = "SchedulerLog";
public static readonly IDbTable Table = new DbTable("SchedulerLog", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn TaskColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Task");


public static readonly IDbColumn ExecutedDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExecutedDate");


public static readonly IDbColumn IsOfflineColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsOffline");


public static readonly IDbColumn IsSynctoOnlineColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsSynctoOnline");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return TaskColumn;


yield return ExecutedDateColumn;


yield return IsOfflineColumn;


yield return IsSynctoOnlineColumn;


yield return CreatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedAtColumn;


yield return UpdatedByColumn;


            
        }

}

}
