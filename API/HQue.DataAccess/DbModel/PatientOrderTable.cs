
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class PatientOrderTable
{

	public const string TableName = "PatientOrder";
public static readonly IDbTable Table = new DbTable("PatientOrder", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn PatientExtIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientExtId");


public static readonly IDbColumn OrderIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OrderId");


public static readonly IDbColumn OrderStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OrderStatus");


public static readonly IDbColumn LatColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Lat");


public static readonly IDbColumn LonColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Lon");


public static readonly IDbColumn PrescriptionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Prescription");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn AmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Amount");


public static readonly IDbColumn DeliveryBoyIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DeliveryBoyId");


public static readonly IDbColumn DeliveredTimeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DeliveredTime");


public static readonly IDbColumn WithPharmaIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"WithPharmaId");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return PatientExtIdColumn;


yield return OrderIdColumn;


yield return OrderStatusColumn;


yield return LatColumn;


yield return LonColumn;


yield return PrescriptionColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return AmountColumn;


yield return DeliveryBoyIdColumn;


yield return DeliveredTimeColumn;


yield return WithPharmaIdColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
