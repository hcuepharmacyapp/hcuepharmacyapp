
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class StockLedgerUpdateHistoryItemTable
{

	public const string TableName = "StockLedgerUpdateHistoryItem";
public static readonly IDbTable Table = new DbTable("StockLedgerUpdateHistoryItem", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn StockLedgerUpdateIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StockLedgerUpdateId");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ProductStockId");


public static readonly IDbColumn BatchNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BatchNo");


public static readonly IDbColumn ExpireDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExpireDate");


public static readonly IDbColumn CurrentStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CurrentStock");


public static readonly IDbColumn ActualStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ActualStock");


public static readonly IDbColumn TransactionTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionType");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return StockLedgerUpdateIdColumn;


yield return ProductIdColumn;


yield return ProductStockIdColumn;


yield return BatchNoColumn;


yield return ExpireDateColumn;


yield return CurrentStockColumn;


yield return ActualStockColumn;


yield return TransactionTypeColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
