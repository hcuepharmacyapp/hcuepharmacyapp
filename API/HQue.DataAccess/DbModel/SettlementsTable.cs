
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SettlementsTable
{

	public const string TableName = "Settlements";
public static readonly IDbTable Table = new DbTable("Settlements", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn VoucherIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VoucherId");


public static readonly IDbColumn TransactionTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionType");


public static readonly IDbColumn TransactionIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionId");


public static readonly IDbColumn AdjustedAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AdjustedAmount");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return VoucherIdColumn;


yield return TransactionTypeColumn;


yield return TransactionIdColumn;


yield return AdjustedAmountColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
