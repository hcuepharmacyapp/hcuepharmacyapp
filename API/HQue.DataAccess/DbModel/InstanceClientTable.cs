
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class InstanceClientTable
{

	public const string TableName = "InstanceClient";
public static readonly IDbTable Table = new DbTable("InstanceClient", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ClientIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ClientId");


public static readonly IDbColumn LastSyncedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LastSyncedAt");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return InstanceIdColumn;


yield return ClientIdColumn;


yield return LastSyncedAtColumn;


yield return OfflineStatusColumn;


            
        }

}

}
