
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class AccountTable
{

	public const string TableName = "Account";
public static readonly IDbTable Table = new DbTable("Account", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn CodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Code");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn RegistrationDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RegistrationDate");


public static readonly IDbColumn InstanceCountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceCount");


public static readonly IDbColumn PhoneColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Phone");


public static readonly IDbColumn EmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Email");


public static readonly IDbColumn AddressColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Address");


public static readonly IDbColumn AreaColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Area");


public static readonly IDbColumn CityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"City");


public static readonly IDbColumn StateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"State");


public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Pincode");


public static readonly IDbColumn RegisterTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RegisterType");


public static readonly IDbColumn isChainColumn = new global::DataAccess.Criteria.DbColumn(TableName,"isChain");


public static readonly IDbColumn LicensePeriodColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LicensePeriod");


public static readonly IDbColumn LicenseStartDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LicenseStartDate");


public static readonly IDbColumn LicenseEndDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LicenseEndDate");


public static readonly IDbColumn LicenseCountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LicenseCount");


public static readonly IDbColumn IsAMCColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsAMC");


public static readonly IDbColumn IsApprovedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsApproved");


public static readonly IDbColumn NetworkColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Network");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn IsBillNumberResetColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsBillNumberReset");


public static readonly IDbColumn InstanceTypeIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InstanceTypeId");



private static IEnumerable<IDbColumn> GetColumn()
{
		 
yield return IdColumn;


yield return CodeColumn;


yield return NameColumn;


yield return RegistrationDateColumn;


yield return InstanceCountColumn;


yield return PhoneColumn;


yield return EmailColumn;


yield return AddressColumn;


yield return AreaColumn;


yield return CityColumn;


yield return StateColumn;


yield return PincodeColumn;


yield return RegisterTypeColumn;


yield return isChainColumn;


yield return LicensePeriodColumn;


yield return LicenseStartDateColumn;


yield return LicenseEndDateColumn;


yield return LicenseCountColumn;


yield return IsAMCColumn;


yield return IsApprovedColumn;


yield return NetworkColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return IsBillNumberResetColumn;


yield return InstanceTypeIdColumn;



}

}

}
