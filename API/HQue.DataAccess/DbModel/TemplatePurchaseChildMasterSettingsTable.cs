
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class TemplatePurchaseChildMasterSettingsTable
{

	public const string TableName = "TemplatePurchaseChildMasterSettings";
public static readonly IDbTable Table = new DbTable("TemplatePurchaseChildMasterSettings", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn HeaderOptionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"HeaderOption");


public static readonly IDbColumn VendorRowPosColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorRowPos");


public static readonly IDbColumn ProductRowPosColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductRowPos");


public static readonly IDbColumn DateFormatColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DateFormat");


public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorId");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return HeaderOptionColumn;


yield return VendorRowPosColumn;


yield return ProductRowPosColumn;


yield return DateFormatColumn;


yield return VendorIdColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
