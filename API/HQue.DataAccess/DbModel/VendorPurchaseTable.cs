
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class VendorPurchaseTable
{

	public const string TableName = "VendorPurchase";
public static readonly IDbTable Table = new DbTable("VendorPurchase", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorId");


public static readonly IDbColumn PrefixColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Prefix");


public static readonly IDbColumn InvoiceNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceNo");


public static readonly IDbColumn InvoiceDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceDate");



public static readonly IDbColumn InitialValueColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InitialValue");



public static readonly IDbColumn VerndorOrderIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VerndorOrderId");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn CreditColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Credit");


public static readonly IDbColumn PaymentTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentType");


public static readonly IDbColumn ChequeNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeNo");


public static readonly IDbColumn ChequeDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeDate");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn BillSeriesColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BillSeries");


public static readonly IDbColumn GoodsRcvNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GoodsRcvNo");


public static readonly IDbColumn CommentsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Comments");


public static readonly IDbColumn FileNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FileName");


public static readonly IDbColumn CreditNoOfDaysColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreditNoOfDays");


public static readonly IDbColumn NoteTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NoteType");


public static readonly IDbColumn NoteAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NoteAmount");


public static readonly IDbColumn TotalPurchaseValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalPurchaseValue");


public static readonly IDbColumn DiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountValue");


public static readonly IDbColumn TotalDiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalDiscountValue");


public static readonly IDbColumn VatValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VatValue");


public static readonly IDbColumn RoundOffValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RoundOffValue");


public static readonly IDbColumn NetValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NetValue");


public static readonly IDbColumn ReturnAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnAmount");


public static readonly IDbColumn FinyearColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Finyear");


public static readonly IDbColumn CancelStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CancelStatus");


public static readonly IDbColumn DiscountTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountType");


public static readonly IDbColumn DiscountInRupeesColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountInRupees");


public static readonly IDbColumn TaxRefNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TaxRefNo");


public static readonly IDbColumn VendorPurchaseFormulaIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorPurchaseFormulaId");


public static readonly IDbColumn MarkupPercColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MarkupPerc");


public static readonly IDbColumn MarkupValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MarkupValue");


public static readonly IDbColumn SchemeDiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SchemeDiscountValue");


public static readonly IDbColumn IsApplyTaxFreeQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsApplyTaxFreeQty");


public static readonly IDbColumn FreeQtyTotalTaxValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FreeQtyTotalTaxValue");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return VendorIdColumn;


yield return PrefixColumn;


yield return InvoiceNoColumn;


yield return InvoiceDateColumn;


yield return InitialValueColumn;


yield return VerndorOrderIdColumn;


yield return DiscountColumn;


yield return CreditColumn;


yield return PaymentTypeColumn;


yield return ChequeNoColumn;


yield return ChequeDateColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return BillSeriesColumn;


yield return GoodsRcvNoColumn;


yield return CommentsColumn;


yield return FileNameColumn;


yield return CreditNoOfDaysColumn;


yield return NoteTypeColumn;


yield return NoteAmountColumn;


yield return TotalPurchaseValueColumn;


yield return DiscountValueColumn;


yield return TotalDiscountValueColumn;


yield return VatValueColumn;


yield return RoundOffValueColumn;


yield return NetValueColumn;


yield return ReturnAmountColumn;


yield return FinyearColumn;


yield return CancelStatusColumn;


yield return DiscountTypeColumn;


yield return DiscountInRupeesColumn;


yield return TaxRefNoColumn;


yield return VendorPurchaseFormulaIdColumn;


yield return MarkupPercColumn;


yield return MarkupValueColumn;


yield return SchemeDiscountValueColumn;


yield return IsApplyTaxFreeQtyColumn;


yield return FreeQtyTotalTaxValueColumn;


            
        }

}

}
