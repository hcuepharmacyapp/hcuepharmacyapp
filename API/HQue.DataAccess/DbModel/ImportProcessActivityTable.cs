
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class ImportProcessActivityTable
{

	public const string TableName = "ImportProcessActivity";
public static readonly IDbTable Table = new DbTable("ImportProcessActivity", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn FileNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FileName");


public static readonly IDbColumn IsCloudColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsCloud");


public static readonly IDbColumn ActivityDescColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ActivityDesc");


public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Status");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return FileNameColumn;


yield return IsCloudColumn;


yield return ActivityDescColumn;


yield return StatusColumn;


yield return CreatedAtColumn;


yield return CreatedByColumn;


            
        }

}

}
