
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class PharmacyPaymentTable
{

	public const string TableName = "PharmacyPayment";
public static readonly IDbTable Table = new DbTable("PharmacyPayment", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn PaymentDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentDate");


public static readonly IDbColumn ProductTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductType");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn CostColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Cost");


public static readonly IDbColumn DiscountTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountType");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn TaxColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Tax");


public static readonly IDbColumn PaidColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Paid");


public static readonly IDbColumn BalanceReasonColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BalanceReason");


public static readonly IDbColumn SalePersonTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalePersonType");


public static readonly IDbColumn SalePersonNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalePersonName");


public static readonly IDbColumn SalePersonMobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalePersonMobile");


public static readonly IDbColumn CommissionTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CommissionType");


public static readonly IDbColumn CommissionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Commission");


public static readonly IDbColumn PaymentTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentType");


public static readonly IDbColumn ChequeNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeNo");


public static readonly IDbColumn ChequeDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeDate");


public static readonly IDbColumn ChequeBankColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeBank");


public static readonly IDbColumn Last4DigitsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Last4Digits");


public static readonly IDbColumn CardNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardName");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn CommentsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Comments");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return PaymentDateColumn;


yield return ProductTypeColumn;


yield return QuantityColumn;


yield return CostColumn;


yield return DiscountTypeColumn;


yield return DiscountColumn;


yield return TaxColumn;


yield return PaidColumn;


yield return BalanceReasonColumn;


yield return SalePersonTypeColumn;


yield return SalePersonNameColumn;


yield return SalePersonMobileColumn;


yield return CommissionTypeColumn;


yield return CommissionColumn;


yield return PaymentTypeColumn;


yield return ChequeNoColumn;


yield return ChequeDateColumn;


yield return ChequeBankColumn;


yield return Last4DigitsColumn;


yield return CardNameColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return CommentsColumn;


            
        }

}

}
