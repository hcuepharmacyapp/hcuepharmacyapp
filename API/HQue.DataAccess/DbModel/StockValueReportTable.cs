
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class StockValueReportTable
{

	public const string TableName = "StockValueReport";
public static readonly IDbTable Table = new DbTable("StockValueReport", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ReportNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReportNo");


public static readonly IDbColumn CategoryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Category");


public static readonly IDbColumn ProductCountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductCount");


public static readonly IDbColumn Vat0PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Vat0PurchasePrice");


public static readonly IDbColumn Vat5PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Vat5PurchasePrice");


public static readonly IDbColumn Vat145PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Vat145PurchasePrice");


public static readonly IDbColumn VatOtherPurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VatOtherPurchasePrice");


public static readonly IDbColumn TotalPurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalPurchasePrice");


public static readonly IDbColumn Gst0PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gst0PurchasePrice");


public static readonly IDbColumn Gst5PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gst5PurchasePrice");


public static readonly IDbColumn Gst12PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gst12PurchasePrice");


public static readonly IDbColumn Gst18PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gst18PurchasePrice");


public static readonly IDbColumn Gst28PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gst28PurchasePrice");


public static readonly IDbColumn GstOtherPurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstOtherPurchasePrice");


public static readonly IDbColumn TotalMrpColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalMrp");


public static readonly IDbColumn ProfitColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Profit");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ReportNoColumn;


yield return CategoryColumn;


yield return ProductCountColumn;


yield return Vat0PurchasePriceColumn;


yield return Vat5PurchasePriceColumn;


yield return Vat145PurchasePriceColumn;


yield return VatOtherPurchasePriceColumn;


yield return TotalPurchasePriceColumn;


yield return Gst0PurchasePriceColumn;


yield return Gst5PurchasePriceColumn;


yield return Gst12PurchasePriceColumn;


yield return Gst18PurchasePriceColumn;


yield return Gst28PurchasePriceColumn;


yield return GstOtherPurchasePriceColumn;


yield return TotalMrpColumn;


yield return ProfitColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
