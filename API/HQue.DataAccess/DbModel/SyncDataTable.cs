















using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SyncDataTable
{

	public const string TableName = "SyncData";
public static readonly IDbTable Table = new DbTable("SyncData", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ActionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Action");


public static readonly IDbColumn DataColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Data");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ActionColumn;


yield return DataColumn;


yield return CreatedAtColumn;


            
        }

}

}
