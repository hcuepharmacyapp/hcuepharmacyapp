
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SelfConsumptionTable
{

	public const string TableName = "SelfConsumption";
public static readonly IDbTable Table = new DbTable("SelfConsumption", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn InvoiceNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceNo");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn ConsumptionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Consumption");


public static readonly IDbColumn ConsumptionNotesColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ConsumptionNotes");


public static readonly IDbColumn ConsumedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ConsumedBy");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return InvoiceNoColumn;


yield return ProductStockIdColumn;


yield return ConsumptionColumn;


yield return ConsumptionNotesColumn;


yield return ConsumedByColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
