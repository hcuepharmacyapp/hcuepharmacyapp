
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

    public class ProductStockTable
    {

        public const string TableName = "ProductStock";
        public static readonly IDbTable Table = new DbTable("ProductStock", GetColumn);


        public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Id");


        public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "AccountId");


        public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InstanceId");


        public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ProductId");


        public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "VendorId");


        public static readonly IDbColumn BatchNoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "BatchNo");


        public static readonly IDbColumn ExpireDateColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ExpireDate");


        public static readonly IDbColumn VATColumn = new global::DataAccess.Criteria.DbColumn(TableName, "VAT");


        public static readonly IDbColumn TaxTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "TaxType");


        public static readonly IDbColumn SellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName, "SellingPrice");


        public static readonly IDbColumn MRPColumn = new global::DataAccess.Criteria.DbColumn(TableName, "MRP");


        public static readonly IDbColumn PurchaseBarcodeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "PurchaseBarcode");


        public static readonly IDbColumn BarcodeProfileIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "BarcodeProfileId");


        public static readonly IDbColumn StockColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Stock");


        public static readonly IDbColumn PackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "PackageSize");


        public static readonly IDbColumn PackagePurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName, "PackagePurchasePrice");


        public static readonly IDbColumn PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName, "PurchasePrice");


        public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OfflineStatus");


        public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedAt");


        public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedAt");


        public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedBy");


        public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedBy");


        public static readonly IDbColumn CSTColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CST");


        public static readonly IDbColumn IsMovingStockColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsMovingStock");


        public static readonly IDbColumn ReOrderQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ReOrderQty");


        public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Status");


        public static readonly IDbColumn IsMovingStockExpireColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsMovingStockExpire");


        public static readonly IDbColumn NewOpenedStockColumn = new global::DataAccess.Criteria.DbColumn(TableName, "NewOpenedStock");


        public static readonly IDbColumn NewStockInvoiceNoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "NewStockInvoiceNo");


        public static readonly IDbColumn NewStockQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName, "NewStockQty");


        public static readonly IDbColumn StockImportColumn = new global::DataAccess.Criteria.DbColumn(TableName, "StockImport");


        public static readonly IDbColumn ImportQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ImportQty");


        public static readonly IDbColumn EancodeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Eancode");


        public static readonly IDbColumn HsnCodeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "HsnCode");


        public static readonly IDbColumn IgstColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Igst");


        public static readonly IDbColumn CgstColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Cgst");


        public static readonly IDbColumn SgstColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Sgst");


        public static readonly IDbColumn GstTotalColumn = new global::DataAccess.Criteria.DbColumn(TableName, "GstTotal");


        public static readonly IDbColumn ImportDateColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ImportDate");


        public static readonly IDbColumn Ext_RefIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Ext_RefId");


        public static readonly IDbColumn TaxRefNoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "TaxRefNo");


        public static readonly IDbColumn PrvStockQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName, "PrvStockQty");

        public static readonly IDbColumn TransactionIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "TransactionId");
        

        private static IEnumerable<IDbColumn> GetColumn()
        {

            yield return IdColumn;


            yield return AccountIdColumn;


            yield return InstanceIdColumn;


            yield return ProductIdColumn;


            yield return VendorIdColumn;


            yield return BatchNoColumn;


            yield return ExpireDateColumn;


            yield return VATColumn;


            yield return TaxTypeColumn;


            yield return SellingPriceColumn;


            yield return MRPColumn;


            yield return PurchaseBarcodeColumn;

            yield return BarcodeProfileIdColumn;
            yield return StockColumn;


            yield return PackageSizeColumn;


            yield return PackagePurchasePriceColumn;


            yield return PurchasePriceColumn;


            yield return OfflineStatusColumn;


            yield return CreatedAtColumn;


            yield return UpdatedAtColumn;


            yield return CreatedByColumn;


            yield return UpdatedByColumn;


            yield return CSTColumn;


            yield return IsMovingStockColumn;


            yield return ReOrderQtyColumn;


            yield return StatusColumn;


            yield return IsMovingStockExpireColumn;


            yield return NewOpenedStockColumn;


            yield return NewStockInvoiceNoColumn;


            yield return NewStockQtyColumn;


            yield return StockImportColumn;


            yield return ImportQtyColumn;


            yield return EancodeColumn;


            yield return HsnCodeColumn;


            yield return IgstColumn;


            yield return CgstColumn;


            yield return SgstColumn;


            yield return GstTotalColumn;


            yield return ImportDateColumn;


            yield return Ext_RefIdColumn;


            yield return TaxRefNoColumn;


            yield return PrvStockQtyColumn;

            yield return TransactionIdColumn;




        }

    }

}
