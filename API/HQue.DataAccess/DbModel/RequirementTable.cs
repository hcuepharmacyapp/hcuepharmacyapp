
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class RequirementTable
{

	public const string TableName = "Requirement";
public static readonly IDbTable Table = new DbTable("Requirement", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn GatheredByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GatheredBy");


public static readonly IDbColumn RequirementNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RequirementNo");


public static readonly IDbColumn DescriptionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Description");


public static readonly IDbColumn TypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Type");


public static readonly IDbColumn SolvedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SolvedBy");


public static readonly IDbColumn SolvedDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SolvedDate");


public static readonly IDbColumn RemarksColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Remarks");


public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Status");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return GatheredByColumn;


yield return RequirementNoColumn;


yield return DescriptionColumn;


yield return TypeColumn;


yield return SolvedByColumn;


yield return SolvedDateColumn;


yield return RemarksColumn;


yield return StatusColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
