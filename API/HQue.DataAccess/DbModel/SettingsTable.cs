
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SettingsTable
{

	public const string TableName = "Settings";
public static readonly IDbTable Table = new DbTable("Settings", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn Enable_GlobalProdutColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Enable_GlobalProdut");


public static readonly IDbColumn Pur_SortByLowestPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Pur_SortByLowestPrice");


public static readonly IDbColumn SalesPriceSettingColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesPriceSetting");

public static readonly IDbColumn SaleBillToCSVColumn = new global::DataAccess.Criteria.DbColumn(TableName, "SaleBillToCSV");

 public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return Enable_GlobalProdutColumn;


yield return Pur_SortByLowestPriceColumn;


yield return SalesPriceSettingColumn;

yield return SaleBillToCSVColumn;

yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
