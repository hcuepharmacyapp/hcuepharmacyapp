
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class EstimateTable
{

	public const string TableName = "Estimate";
public static readonly IDbTable Table = new DbTable("Estimate", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn EstimateNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"EstimateNo");


public static readonly IDbColumn EstimateDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"EstimateDate");


public static readonly IDbColumn PatientIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientId");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn MobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Mobile");


public static readonly IDbColumn EmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Email");


public static readonly IDbColumn DOBColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DOB");


public static readonly IDbColumn AgeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Age");


public static readonly IDbColumn GenderColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gender");


public static readonly IDbColumn AddressColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Address");


public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Pincode");


public static readonly IDbColumn DoctorNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorName");


public static readonly IDbColumn DoctorMobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorMobile");


public static readonly IDbColumn DeliveryTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DeliveryType");


public static readonly IDbColumn BillPrintColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BillPrint");


public static readonly IDbColumn SendSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SendSms");


public static readonly IDbColumn SendEmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SendEmail");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return EstimateNoColumn;


yield return EstimateDateColumn;


yield return PatientIdColumn;


yield return NameColumn;


yield return MobileColumn;


yield return EmailColumn;


yield return DOBColumn;


yield return AgeColumn;


yield return GenderColumn;


yield return AddressColumn;


yield return PincodeColumn;


yield return DoctorNameColumn;


yield return DoctorMobileColumn;


yield return DeliveryTypeColumn;


yield return BillPrintColumn;


yield return SendSmsColumn;


yield return SendEmailColumn;


yield return DiscountColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
