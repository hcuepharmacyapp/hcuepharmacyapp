
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class DraftVendorPurchaseItemTable
{

	public const string TableName = "DraftVendorPurchaseItem";
public static readonly IDbTable Table = new DbTable("DraftVendorPurchaseItem", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn DraftVendorPurchaseIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DraftVendorPurchaseId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn ProductNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductName");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn BatchNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BatchNo");


public static readonly IDbColumn ExpireDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExpireDate");


public static readonly IDbColumn TaxTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TaxType");


public static readonly IDbColumn VATColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VAT");


public static readonly IDbColumn CSTColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CST");


public static readonly IDbColumn PackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSize");


public static readonly IDbColumn PackageQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageQty");


public static readonly IDbColumn PackagePurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackagePurchasePrice");


public static readonly IDbColumn PackageSellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSellingPrice");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PurchasePrice");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn FreeQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FreeQty");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn fromTempIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"fromTempId");


public static readonly IDbColumn fromDcIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"fromDcId");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return DraftVendorPurchaseIdColumn;


yield return ProductStockIdColumn;


yield return ProductNameColumn;


yield return ProductIdColumn;


yield return BatchNoColumn;


yield return ExpireDateColumn;


yield return TaxTypeColumn;


yield return VATColumn;


yield return CSTColumn;


yield return PackageSizeColumn;


yield return PackageQtyColumn;


yield return PackagePurchasePriceColumn;


yield return PackageSellingPriceColumn;


yield return QuantityColumn;


yield return PurchasePriceColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return FreeQtyColumn;


yield return DiscountColumn;


yield return fromTempIdColumn;


yield return fromDcIdColumn;


            
        }

}

}
