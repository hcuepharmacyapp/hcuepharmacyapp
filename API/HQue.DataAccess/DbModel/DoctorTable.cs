
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class DoctorTable
{

	public const string TableName = "Doctor";
public static readonly IDbTable Table = new DbTable("Doctor", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn CodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Code");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn ShortNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ShortName");


public static readonly IDbColumn ClinicNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ClinicName");


public static readonly IDbColumn MobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Mobile");


public static readonly IDbColumn PhoneColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Phone");


public static readonly IDbColumn EmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Email");


public static readonly IDbColumn AreaColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Area");


public static readonly IDbColumn CityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"City");


public static readonly IDbColumn StateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"State");


public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Pincode");


public static readonly IDbColumn GsTinColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GsTin");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CommissionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Commission");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return CodeColumn;


yield return NameColumn;


yield return ShortNameColumn;


yield return ClinicNameColumn;


yield return MobileColumn;


yield return PhoneColumn;


yield return EmailColumn;


yield return AreaColumn;


yield return CityColumn;


yield return StateColumn;


yield return PincodeColumn;


yield return GsTinColumn;


yield return OfflineStatusColumn;


yield return CommissionColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
