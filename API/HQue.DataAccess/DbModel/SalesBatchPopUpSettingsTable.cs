
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SalesBatchPopUpSettingsTable
{

	public const string TableName = "SalesBatchPopUpSettings";
public static readonly IDbTable Table = new DbTable("SalesBatchPopUpSettings", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn BatchColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Batch");


public static readonly IDbColumn ExpiryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Expiry");


public static readonly IDbColumn VatColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Vat");


public static readonly IDbColumn GSTColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GST");


public static readonly IDbColumn UnitMrpColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UnitMrp");


public static readonly IDbColumn StripMrpColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StripMrp");


public static readonly IDbColumn UnitsPerStripColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UnitsPerStrip");


public static readonly IDbColumn ProfitColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Profit");


public static readonly IDbColumn RackNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RackNo");


public static readonly IDbColumn GenericColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Generic");


public static readonly IDbColumn MfgColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Mfg");


public static readonly IDbColumn CategoryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Category");


public static readonly IDbColumn AgeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Age");


public static readonly IDbColumn SupplierColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Supplier");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return QuantityColumn;


yield return BatchColumn;


yield return ExpiryColumn;


yield return VatColumn;


yield return GSTColumn;


yield return UnitMrpColumn;


yield return StripMrpColumn;


yield return UnitsPerStripColumn;


yield return ProfitColumn;


yield return RackNoColumn;


yield return GenericColumn;


yield return MfgColumn;


yield return CategoryColumn;


yield return AgeColumn;


yield return SupplierColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
