
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class MissedOrderTable
{

	public const string TableName = "MissedOrder";
public static readonly IDbTable Table = new DbTable("MissedOrder", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn PatientIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientId");


public static readonly IDbColumn PatientNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientName");


public static readonly IDbColumn PatientMobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientMobile");


public static readonly IDbColumn PatientAgeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientAge");


public static readonly IDbColumn PatientGenderColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientGender");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn SellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SellingPrice");


public static readonly IDbColumn IsActiveColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsActive");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return PatientIdColumn;


yield return PatientNameColumn;


yield return PatientMobileColumn;


yield return PatientAgeColumn;


yield return PatientGenderColumn;


yield return ProductIdColumn;


yield return QuantityColumn;


yield return SellingPriceColumn;


yield return IsActiveColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
