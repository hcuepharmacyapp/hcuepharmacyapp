
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class InstanceTable
{

	public const string TableName = "Instance";
public static readonly IDbTable Table = new DbTable("Instance", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn ExternalIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExternalId");


public static readonly IDbColumn CodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Code");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn RegistrationDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RegistrationDate");


public static readonly IDbColumn PhoneColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Phone");


public static readonly IDbColumn MobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Mobile");


public static readonly IDbColumn DrugLicenseNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DrugLicenseNo");


public static readonly IDbColumn TinNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TinNo");


public static readonly IDbColumn ContactNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ContactName");


public static readonly IDbColumn ContactMobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ContactMobile");


public static readonly IDbColumn ContactEmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ContactEmail");


public static readonly IDbColumn SecondaryNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SecondaryName");


public static readonly IDbColumn SecondaryMobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SecondaryMobile");


public static readonly IDbColumn SecondaryEmailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SecondaryEmail");


public static readonly IDbColumn ContactDesignationColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ContactDesignation");


public static readonly IDbColumn BDOIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BDOId");


public static readonly IDbColumn BDODesignationColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BDODesignation");


public static readonly IDbColumn BDONameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BDOName");


public static readonly IDbColumn RMNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RMName");


public static readonly IDbColumn AddressColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Address");


public static readonly IDbColumn AreaColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Area");


public static readonly IDbColumn CityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"City");


public static readonly IDbColumn StateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"State");


public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Pincode");


public static readonly IDbColumn LandmarkColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Landmark");


public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Status");


public static readonly IDbColumn DrugLicenseExpDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DrugLicenseExpDate");


public static readonly IDbColumn BuildingNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BuildingName");


public static readonly IDbColumn StreetNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StreetName");


public static readonly IDbColumn CountryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Country");


public static readonly IDbColumn LatColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Lat");


public static readonly IDbColumn LonColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Lon");


public static readonly IDbColumn isDoorDeliveryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"isDoorDelivery");


public static readonly IDbColumn isVerifiedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"isVerified");


public static readonly IDbColumn isUnionTerritoryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"isUnionTerritory");


public static readonly IDbColumn isLicensedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"isLicensed");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn IsOfflineInstalledColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsOfflineInstalled");


public static readonly IDbColumn LastLoggedOnColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LastLoggedOn");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn DistanceCoverColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DistanceCover");


public static readonly IDbColumn GstselectColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Gstselect");


public static readonly IDbColumn isHeadOfficeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"isHeadOffice");


public static readonly IDbColumn OfflineVersionNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineVersionNo");


public static readonly IDbColumn GsTinNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GsTinNo");


public static readonly IDbColumn TotalSmsCountColumn = new global::DataAccess.Criteria.DbColumn(TableName, "TotalSmsCount");


public static readonly IDbColumn SentSmsCountColumn = new global::DataAccess.Criteria.DbColumn(TableName, "SentSmsCount");


public static readonly IDbColumn InstanceTypeIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InstanceTypeId");


public static readonly IDbColumn SmsUserNameColumn = new global::DataAccess.Criteria.DbColumn(TableName, "SmsUserName");


public static readonly IDbColumn SmsPasswordColumn = new global::DataAccess.Criteria.DbColumn(TableName, "SmsPassword");


public static readonly IDbColumn SmsUrlColumn = new global::DataAccess.Criteria.DbColumn(TableName, "SmsUrl");



private static IEnumerable<IDbColumn> GetColumn()
{
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return ExternalIdColumn;


yield return CodeColumn;


yield return NameColumn;


yield return RegistrationDateColumn;


yield return PhoneColumn;


yield return MobileColumn;


yield return DrugLicenseNoColumn;


yield return TinNoColumn;


yield return ContactNameColumn;


yield return ContactMobileColumn;


yield return ContactEmailColumn;


yield return SecondaryNameColumn;


yield return SecondaryMobileColumn;


yield return SecondaryEmailColumn;


yield return ContactDesignationColumn;


yield return BDOIdColumn;


yield return BDODesignationColumn;


yield return BDONameColumn;


yield return RMNameColumn;


yield return AddressColumn;


yield return AreaColumn;


yield return CityColumn;


yield return StateColumn;


yield return PincodeColumn;


yield return LandmarkColumn;


yield return StatusColumn;


yield return DrugLicenseExpDateColumn;


yield return BuildingNameColumn;


yield return StreetNameColumn;


yield return CountryColumn;


yield return LatColumn;


yield return LonColumn;


yield return isDoorDeliveryColumn;


yield return isVerifiedColumn;


yield return isUnionTerritoryColumn;


yield return isLicensedColumn;


yield return OfflineStatusColumn;


yield return IsOfflineInstalledColumn;


yield return LastLoggedOnColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return DistanceCoverColumn;


yield return GstselectColumn;


yield return isHeadOfficeColumn;


yield return OfflineVersionNoColumn;


yield return GsTinNoColumn;


yield return TotalSmsCountColumn;


yield return SentSmsCountColumn;


yield return InstanceTypeIdColumn;


yield return SmsUserNameColumn;


yield return SmsPasswordColumn;


yield return SmsUrlColumn;


}

}

}
