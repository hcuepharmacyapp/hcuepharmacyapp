
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SalesReturnItemTable
{

	public const string TableName = "SalesReturnItem";
public static readonly IDbTable Table = new DbTable("SalesReturnItem", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SalesReturnIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesReturnId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn CancelTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CancelType");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn DiscountAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountAmount");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn MrpSellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MrpSellingPrice");


public static readonly IDbColumn MRPColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MRP");


public static readonly IDbColumn IsDeletedColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsDeleted");


public static readonly IDbColumn IgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Igst");


public static readonly IDbColumn CgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Cgst");


public static readonly IDbColumn SgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Sgst");


public static readonly IDbColumn GstTotalColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstTotal");


public static readonly IDbColumn GstAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstAmount");


public static readonly IDbColumn TotalAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalAmount");


public static readonly IDbColumn LoyaltyProductPtsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LoyaltyProductPts");




private static IEnumerable<IDbColumn> GetColumn()
{
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SalesReturnIdColumn;


yield return ProductStockIdColumn;


yield return QuantityColumn;


yield return CancelTypeColumn;


yield return DiscountColumn;


yield return DiscountAmountColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return MrpSellingPriceColumn;


yield return MRPColumn;


yield return IsDeletedColumn;


yield return IgstColumn;


yield return CgstColumn;


yield return SgstColumn;


yield return GstTotalColumn;


yield return GstAmountColumn;


yield return TotalAmountColumn;


yield return LoyaltyProductPtsColumn;


            
}

}

}
