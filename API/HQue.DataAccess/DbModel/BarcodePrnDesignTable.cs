
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class BarcodePrnDesignTable
{

	public const string TableName = "BarcodePrnDesign";
public static readonly IDbTable Table = new DbTable("BarcodePrnDesign", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn PrnNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PrnName");


public static readonly IDbColumn PrnFileDataColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PrnFileData");


public static readonly IDbColumn PrnFileNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PrnFileName");


public static readonly IDbColumn PrnFieldJsonColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PrnFieldJson");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return PrnNameColumn;


yield return PrnFileDataColumn;


yield return PrnFileNameColumn;


yield return PrnFieldJsonColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
