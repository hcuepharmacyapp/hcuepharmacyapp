
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class PatientOrderInstanceStatusTable
{

	public const string TableName = "PatientOrderInstanceStatus";
public static readonly IDbTable Table = new DbTable("PatientOrderInstanceStatus", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn PatientOrderIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientOrderId");


public static readonly IDbColumn AcceptanceStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AcceptanceStatus");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn DurationColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Duration");


public static readonly IDbColumn MessageCodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MessageCode");


public static readonly IDbColumn PushSentTimeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PushSentTime");


public static readonly IDbColumn DistanceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Distance");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return PatientOrderIdColumn;


yield return AcceptanceStatusColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return DurationColumn;


yield return MessageCodeColumn;


yield return PushSentTimeColumn;


yield return DistanceColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
