
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class CustomDevReportTable
{

	public const string TableName = "CustomDevReport";
public static readonly IDbTable Table = new DbTable("CustomDevReport", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ReportNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReportName");


public static readonly IDbColumn ReportTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReportType");


public static readonly IDbColumn QueryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Query");


public static readonly IDbColumn FilterJSONColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FilterJSON");


public static readonly IDbColumn ConfigJSONColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ConfigJSON");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ReportNameColumn;


yield return ReportTypeColumn;


yield return QueryColumn;


yield return FilterJSONColumn;


yield return ConfigJSONColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
