
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SaleSettingsTable
{

	public const string TableName = "SaleSettings";
public static readonly IDbTable Table = new DbTable("SaleSettings", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn CardTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardType");


public static readonly IDbColumn PatientSearchTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientSearchType");


public static readonly IDbColumn DoctorSearchTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorSearchType");


public static readonly IDbColumn DoctorNameMandatoryTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DoctorNameMandatoryType");


public static readonly IDbColumn SaleTypeMandatoryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SaleTypeMandatory");


public static readonly IDbColumn PostCancelBillDaysColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PostCancelBillDays");


public static readonly IDbColumn IsCreditInvoiceSeriesColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsCreditInvoiceSeries");


public static readonly IDbColumn DiscountTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountType");


public static readonly IDbColumn CustomSeriesInvoiceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CustomSeriesInvoice");


public static readonly IDbColumn PrinterTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PrinterType");


public static readonly IDbColumn CustomTempJSONColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CustomTempJSON");


public static readonly IDbColumn ShortCutKeysTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ShortCutKeysType");


public static readonly IDbColumn NewWindowTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NewWindowType");


public static readonly IDbColumn AutoTempStockAddColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AutoTempStockAdd");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn AutosaveCustomerColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AutosaveCustomer");


public static readonly IDbColumn MaxDiscountAvailColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MaxDiscountAvail");


public static readonly IDbColumn InstanceMaxDiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceMaxDiscount");


public static readonly IDbColumn ShortCustomerNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ShortCustomerName");


public static readonly IDbColumn ShortDoctorNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ShortDoctorName");


public static readonly IDbColumn CompleteSaleKeyTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CompleteSaleKeyType");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn ScanBarcodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ScanBarcode");


public static readonly IDbColumn AutoScanQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AutoScanQty");


public static readonly IDbColumn PatientTypeDeptColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PatientTypeDept");


public static readonly IDbColumn ShowSalesItemHistoryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ShowSalesItemHistory");


public static readonly IDbColumn PrintLogoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PrintLogo");


public static readonly IDbColumn PrintCustomFieldsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PrintCustomFields");


public static readonly IDbColumn IsCancelEditSMSColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsCancelEditSMS");


public static readonly IDbColumn IsEnableRoundOffColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsEnableRoundOff");
        public static readonly IDbColumn IsEnableFreeQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsEnableFreeQty");

        public static readonly IDbColumn isEnableCustomerSeriesColumn = new global::DataAccess.Criteria.DbColumn(TableName, "isEnableCustomerSeries");

        public static readonly IDbColumn IsEnableSalesUserColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsEnableSalesUser");

        public static readonly IDbColumn CustomerIDSeriesColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CustomerIDSeries");


        public static readonly IDbColumn IsEnableNewSalesScreenColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsEnableNewSalesScreen");

        

        private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return CardTypeColumn;


yield return PatientSearchTypeColumn;


yield return DoctorSearchTypeColumn;


yield return DoctorNameMandatoryTypeColumn;


yield return SaleTypeMandatoryColumn;


yield return PostCancelBillDaysColumn;


yield return IsCreditInvoiceSeriesColumn;


yield return DiscountTypeColumn;


yield return CustomSeriesInvoiceColumn;


yield return PrinterTypeColumn;


yield return CustomTempJSONColumn;


yield return ShortCutKeysTypeColumn;


yield return NewWindowTypeColumn;


yield return AutoTempStockAddColumn;


yield return OfflineStatusColumn;


yield return AutosaveCustomerColumn;


yield return MaxDiscountAvailColumn;


yield return InstanceMaxDiscountColumn;


yield return ShortCustomerNameColumn;


yield return ShortDoctorNameColumn;


yield return CompleteSaleKeyTypeColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return ScanBarcodeColumn;


yield return AutoScanQtyColumn;


yield return PatientTypeDeptColumn;


yield return ShowSalesItemHistoryColumn;


yield return PrintLogoColumn;


yield return PrintCustomFieldsColumn;


yield return IsCancelEditSMSColumn;


yield return IsEnableRoundOffColumn;

yield return IsEnableFreeQtyColumn;

yield return isEnableCustomerSeriesColumn;

yield return IsEnableSalesUserColumn;

yield return CustomerIDSeriesColumn;


yield return IsEnableNewSalesScreenColumn;


        }

}

}
