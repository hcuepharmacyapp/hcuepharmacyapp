
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class InventorySettingsTable
{

	public const string TableName = "InventorySettings";
public static readonly IDbTable Table = new DbTable("InventorySettings", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn NoOfDaysColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NoOfDays");


public static readonly IDbColumn NoOfMonthColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NoOfMonth");


public static readonly IDbColumn PopupStartDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PopupStartDate");


public static readonly IDbColumn PopupEndDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PopupEndDate");


public static readonly IDbColumn PopupStartTimeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PopupStartTime");


public static readonly IDbColumn PopupEndTimeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PopupEndTime");


public static readonly IDbColumn PopupIntervalColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PopupInterval");


public static readonly IDbColumn PopupEnabledColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PopupEnabled");


public static readonly IDbColumn LastPopupAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LastPopupAt");


public static readonly IDbColumn OpenedPopupColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OpenedPopup");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return NoOfDaysColumn;


yield return NoOfMonthColumn;


yield return PopupStartDateColumn;


yield return PopupEndDateColumn;


yield return PopupStartTimeColumn;


yield return PopupEndTimeColumn;


yield return PopupIntervalColumn;


yield return PopupEnabledColumn;


yield return LastPopupAtColumn;


yield return OpenedPopupColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
