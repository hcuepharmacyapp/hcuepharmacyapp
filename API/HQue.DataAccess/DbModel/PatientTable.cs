
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

    public class PatientTable
    {

        public const string TableName = "Patient";
        public static readonly IDbTable Table = new DbTable("Patient", GetColumn);


        public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Id");


        public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "AccountId");


        public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InstanceId");


        public static readonly IDbColumn CodeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Code");


        public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Name");


        public static readonly IDbColumn ShortNameColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ShortName");


        public static readonly IDbColumn MobileColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Mobile");


        public static readonly IDbColumn EmailColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Email");


        public static readonly IDbColumn DOBColumn = new global::DataAccess.Criteria.DbColumn(TableName, "DOB");


        public static readonly IDbColumn AgeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Age");


        public static readonly IDbColumn GenderColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Gender");


        public static readonly IDbColumn AddressColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Address");


        public static readonly IDbColumn AreaColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Area");


        public static readonly IDbColumn CityColumn = new global::DataAccess.Criteria.DbColumn(TableName, "City");


        public static readonly IDbColumn StateColumn = new global::DataAccess.Criteria.DbColumn(TableName, "State");


        public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Pincode");


        public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OfflineStatus");


        public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedAt");


        public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedAt");


        public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedBy");


        public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedBy");


        public static readonly IDbColumn EmpIDColumn = new global::DataAccess.Criteria.DbColumn(TableName, "EmpID");


        public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Discount");


        public static readonly IDbColumn IsSyncColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSync");


        public static readonly IDbColumn CustomerPaymentTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CustomerPaymentType");


        public static readonly IDbColumn PatientTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "PatientType");


        public static readonly IDbColumn IsAutoSaveColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsAutoSave");


        public static readonly IDbColumn PanColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Pan");


        public static readonly IDbColumn GsTinColumn = new global::DataAccess.Criteria.DbColumn(TableName, "GsTin");


        public static readonly IDbColumn DrugLicenseNoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "DrugLicenseNo");


        public static readonly IDbColumn LocationTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LocationType");


        public static readonly IDbColumn BalanceAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName, "BalanceAmount");


        public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Status");


        public static readonly IDbColumn LoyaltyPointColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LoyaltyPoint");


        public static readonly IDbColumn RedeemedPointColumn = new global::DataAccess.Criteria.DbColumn(TableName, "RedeemedPoint");
        public static readonly IDbColumn CustomerSeriesIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CustomerSeriesId");
        public static readonly IDbColumn PrefixColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Prefix");



        private static IEnumerable<IDbColumn> GetColumn()
        {

            yield return IdColumn;


            yield return AccountIdColumn;


            yield return InstanceIdColumn;


            yield return CodeColumn;


            yield return NameColumn;


            yield return ShortNameColumn;


            yield return MobileColumn;


            yield return EmailColumn;


            yield return DOBColumn;


            yield return AgeColumn;


            yield return GenderColumn;


            yield return AddressColumn;


            yield return AreaColumn;


            yield return CityColumn;


            yield return StateColumn;


            yield return PincodeColumn;


            yield return OfflineStatusColumn;


            yield return CreatedAtColumn;


            yield return UpdatedAtColumn;


            yield return CreatedByColumn;


            yield return UpdatedByColumn;


            yield return EmpIDColumn;


            yield return DiscountColumn;


            yield return IsSyncColumn;


            yield return CustomerPaymentTypeColumn;


            yield return PatientTypeColumn;


            yield return IsAutoSaveColumn;


            yield return PanColumn;


            yield return GsTinColumn;


            yield return DrugLicenseNoColumn;


            yield return LocationTypeColumn;


            yield return BalanceAmountColumn;


            yield return StatusColumn;


            yield return LoyaltyPointColumn;


            yield return RedeemedPointColumn;

            yield return CustomerSeriesIdColumn;

            yield return PrefixColumn;
        }

    }

}
