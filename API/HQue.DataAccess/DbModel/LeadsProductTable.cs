
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class LeadsProductTable
{

	public const string TableName = "LeadsProduct";
public static readonly IDbTable Table = new DbTable("LeadsProduct", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn LeadsIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LeadsId");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn TypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Type");


public static readonly IDbColumn DosageColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Dosage");


public static readonly IDbColumn BAColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BA");


public static readonly IDbColumn NumberofDaysColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NumberofDays");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return LeadsIdColumn;


yield return ProductIdColumn;


yield return NameColumn;


yield return TypeColumn;


yield return DosageColumn;


yield return BAColumn;


yield return NumberofDaysColumn;


yield return QuantityColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
