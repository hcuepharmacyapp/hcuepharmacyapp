
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class HQueUserTable
{

	public const string TableName = "HQueUser";
public static readonly IDbTable Table = new DbTable("HQueUser", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn UserIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UserId");


public static readonly IDbColumn PasswordColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Password");


public static readonly IDbColumn UserTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UserType");


public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Status");


public static readonly IDbColumn NameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Name");


public static readonly IDbColumn MobileColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Mobile");


public static readonly IDbColumn AddressColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Address");


public static readonly IDbColumn AreaColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Area");


public static readonly IDbColumn CityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"City");


public static readonly IDbColumn StateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"State");


public static readonly IDbColumn PincodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Pincode");


public static readonly IDbColumn PasswordResetKeyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PasswordResetKey");


public static readonly IDbColumn ResetRequestTimeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ResetRequestTime");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");

        
public static readonly IDbColumn IsOnlineEnableColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsOnlineEnable");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return UserIdColumn;


yield return PasswordColumn;


yield return UserTypeColumn;


yield return StatusColumn;


yield return NameColumn;


yield return MobileColumn;


yield return AddressColumn;


yield return AreaColumn;


yield return CityColumn;


yield return StateColumn;


yield return PincodeColumn;


yield return PasswordResetKeyColumn;


yield return ResetRequestTimeColumn;


yield return OfflineStatusColumn;


yield return IsOnlineEnableColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
