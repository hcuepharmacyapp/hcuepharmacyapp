
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class PaymentTable
{

	public const string TableName = "Payment";
public static readonly IDbTable Table = new DbTable("Payment", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn TransactionDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionDate");


public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorId");


public static readonly IDbColumn DebitColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Debit");


public static readonly IDbColumn CreditColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Credit");


public static readonly IDbColumn PaymentTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentType");


public static readonly IDbColumn PaymentModeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentMode");


public static readonly IDbColumn BankNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BankName");


public static readonly IDbColumn BankBranchNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BankBranchName");


public static readonly IDbColumn IfscCodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IfscCode");


public static readonly IDbColumn ChequeNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeNo");


public static readonly IDbColumn ChequeDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeDate");


public static readonly IDbColumn VendorPurchaseIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorPurchaseId");


public static readonly IDbColumn RemarksColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Remarks");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn PaymentHistoryStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentHistoryStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Status");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return TransactionDateColumn;


yield return VendorIdColumn;


yield return DebitColumn;


yield return CreditColumn;


yield return PaymentTypeColumn;


yield return PaymentModeColumn;


yield return BankNameColumn;


yield return BankBranchNameColumn;


yield return IfscCodeColumn;


yield return ChequeNoColumn;


yield return ChequeDateColumn;


yield return VendorPurchaseIdColumn;


yield return RemarksColumn;


yield return OfflineStatusColumn;


yield return PaymentHistoryStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return StatusColumn;


            
        }

}

}
