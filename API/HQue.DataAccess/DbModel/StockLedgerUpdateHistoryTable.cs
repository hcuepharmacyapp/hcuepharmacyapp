
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class StockLedgerUpdateHistoryTable
{

	public const string TableName = "StockLedgerUpdateHistory";
public static readonly IDbTable Table = new DbTable("StockLedgerUpdateHistory", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn ImportQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ImportQty");


public static readonly IDbColumn CurrentStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CurrentStock");


public static readonly IDbColumn OpeningStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OpeningStock");


public static readonly IDbColumn AdjustedStockInColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AdjustedStockIn");


public static readonly IDbColumn PurchaseQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PurchaseQty");


public static readonly IDbColumn SaleReturnQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SaleReturnQty");


public static readonly IDbColumn DcQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DcQty");


public static readonly IDbColumn TempQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TempQty");


public static readonly IDbColumn TransferInAccQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferInAccQty");


public static readonly IDbColumn AdjustedStockOutColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AdjustedStockOut");


public static readonly IDbColumn SaleQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SaleQty");


public static readonly IDbColumn PurchaseDelQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PurchaseDelQty");


public static readonly IDbColumn TransferOutQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransferOutQty");


public static readonly IDbColumn ConsumptionQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ConsumptionQty");


public static readonly IDbColumn FinalInColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FinalIn");


public static readonly IDbColumn FinalOutColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FinalOut");


public static readonly IDbColumn DiffQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiffQty");


public static readonly IDbColumn ClosingStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ClosingStock");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ProductStockIdColumn;


yield return ImportQtyColumn;


yield return CurrentStockColumn;


yield return OpeningStockColumn;


yield return AdjustedStockInColumn;


yield return PurchaseQtyColumn;


yield return SaleReturnQtyColumn;


yield return DcQtyColumn;


yield return TempQtyColumn;


yield return TransferInAccQtyColumn;


yield return AdjustedStockOutColumn;


yield return SaleQtyColumn;


yield return PurchaseDelQtyColumn;


yield return TransferOutQtyColumn;


yield return ConsumptionQtyColumn;


yield return FinalInColumn;


yield return FinalOutColumn;


yield return DiffQtyColumn;


yield return ClosingStockColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
