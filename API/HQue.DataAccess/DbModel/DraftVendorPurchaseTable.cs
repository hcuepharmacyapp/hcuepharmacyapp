
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class DraftVendorPurchaseTable
{

	public const string TableName = "DraftVendorPurchase";
public static readonly IDbTable Table = new DbTable("DraftVendorPurchase", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorId");


public static readonly IDbColumn InvoiceNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceNo");


public static readonly IDbColumn InvoiceDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InvoiceDate");


public static readonly IDbColumn VerndorOrderIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VerndorOrderId");


public static readonly IDbColumn DraftNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DraftName");


public static readonly IDbColumn DraftAddedOnColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DraftAddedOn");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn CreditColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Credit");


public static readonly IDbColumn PaymentTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentType");


public static readonly IDbColumn ChequeNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeNo");


public static readonly IDbColumn ChequeDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeDate");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn GoodsRcvNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GoodsRcvNo");


public static readonly IDbColumn CommentsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Comments");


public static readonly IDbColumn FileNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FileName");


public static readonly IDbColumn CreditNoOfDaysColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreditNoOfDays");


public static readonly IDbColumn NoteTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NoteType");


public static readonly IDbColumn NoteAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"NoteAmount");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return VendorIdColumn;


yield return InvoiceNoColumn;


yield return InvoiceDateColumn;


yield return VerndorOrderIdColumn;


yield return DraftNameColumn;


yield return DraftAddedOnColumn;


yield return DiscountColumn;


yield return CreditColumn;


yield return PaymentTypeColumn;


yield return ChequeNoColumn;


yield return ChequeDateColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return GoodsRcvNoColumn;


yield return CommentsColumn;


yield return FileNameColumn;


yield return CreditNoOfDaysColumn;


yield return NoteTypeColumn;


yield return NoteAmountColumn;


            
        }

}

}
