
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class tempProductStockTable
{

	public const string TableName = "tempProductStock";
public static readonly IDbTable Table = new DbTable("tempProductStock", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductId");


public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorId");


public static readonly IDbColumn BatchNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"BatchNo");


public static readonly IDbColumn ExpireDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ExpireDate");


public static readonly IDbColumn VATColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VAT");


public static readonly IDbColumn SellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SellingPrice");


public static readonly IDbColumn PurchaseBarcodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PurchaseBarcode");


public static readonly IDbColumn StockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Stock");


public static readonly IDbColumn PackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSize");


public static readonly IDbColumn PackagePurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackagePurchasePrice");


public static readonly IDbColumn PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PurchasePrice");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn CSTColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CST");


public static readonly IDbColumn IsMovingStockColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsMovingStock");


public static readonly IDbColumn ReOrderQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReOrderQty");


public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Status");


public static readonly IDbColumn IsMovingStockExpireColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsMovingStockExpire");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return ProductIdColumn;


yield return VendorIdColumn;


yield return BatchNoColumn;


yield return ExpireDateColumn;


yield return VATColumn;


yield return SellingPriceColumn;


yield return PurchaseBarcodeColumn;


yield return StockColumn;


yield return PackageSizeColumn;


yield return PackagePurchasePriceColumn;


yield return PurchasePriceColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return CSTColumn;


yield return IsMovingStockColumn;


yield return ReOrderQtyColumn;


yield return StatusColumn;


yield return IsMovingStockExpireColumn;


            
        }

}

}
