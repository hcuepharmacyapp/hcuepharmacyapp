
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SmsSettingsTable
    {

	public const string TableName = "SmsSettings";
public static readonly IDbTable Table = new DbTable("SmsSettings", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn IsVendorCreateSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsVendorCreateSms");


public static readonly IDbColumn IsSalesCreateSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSalesCreateSms");


public static readonly IDbColumn IsSalesEditSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSalesEditSms");


public static readonly IDbColumn IsSalesCancelSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSalesCancelSms");


public static readonly IDbColumn IsSalesEstimateSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSalesEstimateSms");


public static readonly IDbColumn IsSalesTemplateCreateSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSalesTemplateCreateSms");


public static readonly IDbColumn IsSalesOrderSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSalesOrderSms");


public static readonly IDbColumn IsLoginSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsLoginSms");


public static readonly IDbColumn IsCustomerBulkSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsCustomerBulkSms");


public static readonly IDbColumn IsOrderCreateSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsOrderCreateSms");


public static readonly IDbColumn IsUserCreateSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsUserCreateSms");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return IsVendorCreateSmsColumn;
            

yield return IsSalesCreateSmsColumn;


yield return IsSalesEditSmsColumn;


yield return IsSalesCancelSmsColumn;


yield return IsSalesEstimateSmsColumn;


yield return IsSalesTemplateCreateSmsColumn;


yield return IsSalesOrderSmsColumn;


yield return IsLoginSmsColumn;


yield return IsCustomerBulkSmsColumn;


yield return IsOrderCreateSmsColumn;


yield return IsUserCreateSmsColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;



            
        }

}

}
