
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class VendorOrderTable
{

	public const string TableName = "VendorOrder";
public static readonly IDbTable Table = new DbTable("VendorOrder", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorId");


public static readonly IDbColumn PrefixColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Prefix");


public static readonly IDbColumn OrderIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OrderId");


public static readonly IDbColumn OrderDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OrderDate");


public static readonly IDbColumn DueDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DueDate");


public static readonly IDbColumn PendingAuthorisationColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PendingAuthorisation");


public static readonly IDbColumn AuthorisedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AuthorisedBy");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn FinyearIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FinyearId");


public static readonly IDbColumn ToInstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ToInstanceId");


public static readonly IDbColumn TaxRefNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TaxRefNo");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return VendorIdColumn;


yield return PrefixColumn;


yield return OrderIdColumn;


yield return OrderDateColumn;


yield return DueDateColumn;


yield return PendingAuthorisationColumn;


yield return AuthorisedByColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return FinyearIdColumn;


yield return ToInstanceIdColumn;


yield return TaxRefNoColumn;


            
        }

}

}
