
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class PettyCashDtlTable
{

	public const string TableName = "PettyCashDtl";
public static readonly IDbTable Table = new DbTable("PettyCashDtl", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn PettyHdrIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PettyHdrId");


public static readonly IDbColumn PettyItemIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PettyItemId");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn UserIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UserId");


public static readonly IDbColumn TransactionDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TransactionDate");


public static readonly IDbColumn DescriptionColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Description");


public static readonly IDbColumn AmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Amount");


public static readonly IDbColumn DeletedStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DeletedStatus");


public static readonly IDbColumn TalliedStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TalliedStatus");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return PettyHdrIdColumn;


yield return PettyItemIdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return UserIdColumn;


yield return TransactionDateColumn;


yield return DescriptionColumn;


yield return AmountColumn;


yield return DeletedStatusColumn;


yield return TalliedStatusColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
