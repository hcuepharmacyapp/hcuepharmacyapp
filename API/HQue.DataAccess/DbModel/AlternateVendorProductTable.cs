
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

    public class AlternateVendorProductTable
    {

        public const string TableName = "AlternateVendorProduct";
        public static readonly IDbTable Table = new DbTable("AlternateVendorProduct", GetColumn);


        public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Id");


        public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "AccountId");


        public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InstanceId");


        public static readonly IDbColumn ProductIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ProductId");


        public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "VendorId");


        public static readonly IDbColumn AlternateProductNameColumn = new global::DataAccess.Criteria.DbColumn(TableName, "AlternateProductName");
        public static readonly IDbColumn UpdatedPackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedPackageSize");

        public static readonly IDbColumn AlternatePackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "AlternatePackageSize");


        public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OfflineStatus");


        public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedAt");


        public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedAt");


        public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedBy");


        public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedBy");



        private static IEnumerable<IDbColumn> GetColumn()
        {

            yield return IdColumn;


            yield return AccountIdColumn;


            yield return InstanceIdColumn;


            yield return ProductIdColumn;


            yield return VendorIdColumn;


            yield return AlternateProductNameColumn;

            yield return AlternatePackageSizeColumn;

            yield return UpdatedPackageSizeColumn;

            yield return OfflineStatusColumn;


            yield return CreatedAtColumn;


            yield return UpdatedAtColumn;


            yield return CreatedByColumn;


            yield return UpdatedByColumn;



        }

    }

}
