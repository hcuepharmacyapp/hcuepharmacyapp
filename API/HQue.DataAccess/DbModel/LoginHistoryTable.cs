using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;
using DataAccess.QueryBuilder;

namespace HQue.DataAccess.DbModel
{
    public class LoginHistoryTable
    {
        public const string TableName = "LoginHistory";
        public static readonly IDbTable Table = new DbTable("LoginHistory", GetColumn);
        public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "Id");
        public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "AccountId");
        public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InstanceId");
        public static readonly IDbColumn LogInUserIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LogInUserId");
        public static readonly IDbColumn LogOutUserIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LogOutUserId");
        public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OfflineStatus");
        public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,
"CreatedAt");
        public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,
"UpdatedAt");
        public static readonly IDbColumn LoggedInDateColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LoggedInDate");
        public static readonly IDbColumn LoggedOutDateColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LoggedOutDate");
        public static readonly IDbColumn LastTransctionDateColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LastTransctionDate");
        public static readonly IDbColumn IsSentsmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSentsms");
        public static readonly IDbColumn IsSentemailColumn = new global::DataAccess.Criteria.DbColumn(TableName, "IsSentemail");
        public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "CreatedBy");
        public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName, "UpdatedBy");

        public static OperationType Update { get; internal set; }

        private static IEnumerable<IDbColumn> GetColumn()
        {
            yield return IdColumn;
            yield return AccountIdColumn;
            yield return InstanceIdColumn;
            yield return LogInUserIdColumn;
            yield return LogOutUserIdColumn;
            yield return OfflineStatusColumn;
            yield return CreatedAtColumn;
            yield return UpdatedAtColumn;
            yield return LoggedInDateColumn;
            yield return LoggedOutDateColumn;
            yield return LastTransctionDateColumn;
            yield return IsSentsmsColumn;
            yield return IsSentemailColumn;
            yield return CreatedByColumn;
            yield return UpdatedByColumn;

        }
        }
}
