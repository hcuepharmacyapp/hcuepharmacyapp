
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class tempSalesItemTable
{

	public const string TableName = "tempSalesItem";
public static readonly IDbTable Table = new DbTable("tempSalesItem", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SalesIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesId");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn ReminderFrequencyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReminderFrequency");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn SellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SellingPrice");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SalesIdColumn;


yield return ProductStockIdColumn;


yield return ReminderFrequencyColumn;


yield return QuantityColumn;


yield return DiscountColumn;


yield return SellingPriceColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
