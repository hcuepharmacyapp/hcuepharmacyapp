
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class StateTable
{

	public const string TableName = "State";
public static readonly IDbTable Table = new DbTable("State", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn StateNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StateName");


public static readonly IDbColumn StateCodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"StateCode");


public static readonly IDbColumn CountryIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CountryId");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn isUnionTerritoryColumn = new global::DataAccess.Criteria.DbColumn(TableName,"isUnionTerritory");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return StateNameColumn;


yield return StateCodeColumn;


yield return CountryIdColumn;


yield return OfflineStatusColumn;


yield return isUnionTerritoryColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
