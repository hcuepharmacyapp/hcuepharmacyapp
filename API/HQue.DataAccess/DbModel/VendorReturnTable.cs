
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class VendorReturnTable
{

	public const string TableName = "VendorReturn";
public static readonly IDbTable Table = new DbTable("VendorReturn", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn VendorPurchaseIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorPurchaseId");


public static readonly IDbColumn VendorIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorId");


public static readonly IDbColumn PrefixColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Prefix");


public static readonly IDbColumn ReturnNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnNo");


public static readonly IDbColumn ReturnDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ReturnDate");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn ReasonColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Reason");


public static readonly IDbColumn FinyearIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FinyearId");


public static readonly IDbColumn PaymentStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentStatus");


public static readonly IDbColumn PaymentRemarksColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentRemarks");


public static readonly IDbColumn TotalReturnedAmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalReturnedAmount");


public static readonly IDbColumn TotalDiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalDiscountValue");


public static readonly IDbColumn TotalGstValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TotalGstValue");


public static readonly IDbColumn RoundOffValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"RoundOffValue");


public static readonly IDbColumn TaxRefNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"TaxRefNo");


public static readonly IDbColumn IsAlongWithPurchaseColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsAlongWithPurchase");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return VendorPurchaseIdColumn;


yield return VendorIdColumn;


yield return PrefixColumn;


yield return ReturnNoColumn;


yield return ReturnDateColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return ReasonColumn;


yield return FinyearIdColumn;


yield return PaymentStatusColumn;


yield return PaymentRemarksColumn;


yield return TotalReturnedAmountColumn;


yield return TotalDiscountValueColumn;


yield return TotalGstValueColumn;


yield return RoundOffValueColumn;


yield return TaxRefNoColumn;


yield return IsAlongWithPurchaseColumn;


            
        }

}

}
