
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SyncDataSeedTable
{

	public const string TableName = "SyncDataSeed";
public static readonly IDbTable Table = new DbTable("SyncDataSeed", GetColumn);

        
public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SeedIndexColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SeedIndex");


public static readonly IDbColumn LastSynedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"LastSynedAt");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SeedIndexColumn;


yield return LastSynedAtColumn;


            
        }

}

}
