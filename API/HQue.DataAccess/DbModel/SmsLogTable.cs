
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SmsLogTable
    {

	public const string TableName = "SmsLog";
public static readonly IDbTable Table = new DbTable("SmsLog", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SmsDateColumn = new global::DataAccess.Criteria.DbColumn(TableName, "SmsDate");


public static readonly IDbColumn LogTypeColumn = new global::DataAccess.Criteria.DbColumn(TableName, "LogType");


public static readonly IDbColumn ReceiverNameColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ReceiverName");


public static readonly IDbColumn InvoiceNoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InvoiceNo");


public static readonly IDbColumn InvoiceDateColumn = new global::DataAccess.Criteria.DbColumn(TableName, "InvoiceDate");


public static readonly IDbColumn ReferenceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ReferenceId");


public static readonly IDbColumn NoOfSmsColumn = new global::DataAccess.Criteria.DbColumn(TableName, "NoOfSms");


public static readonly IDbColumn OpeningCountColumn = new global::DataAccess.Criteria.DbColumn(TableName, "OpeningCount");


public static readonly IDbColumn ClosingCountColumn = new global::DataAccess.Criteria.DbColumn(TableName, "ClosingCount");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SmsDateColumn;


yield return LogTypeColumn;


yield return ReceiverNameColumn;


yield return InvoiceNoColumn;


yield return InvoiceDateColumn;


yield return ReferenceIdColumn;


yield return NoOfSmsColumn;


yield return OpeningCountColumn;


yield return ClosingCountColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


            
        }

}

}
