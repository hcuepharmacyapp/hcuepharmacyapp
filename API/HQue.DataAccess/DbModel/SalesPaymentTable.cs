
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class SalesPaymentTable
{

	public const string TableName = "SalesPayment";
public static readonly IDbTable Table = new DbTable("SalesPayment", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn SalesIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SalesId");


public static readonly IDbColumn PaymentIndColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PaymentInd");


public static readonly IDbColumn SubPaymentIndColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SubPaymentInd");


public static readonly IDbColumn AmountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Amount");


public static readonly IDbColumn isActiveColumn = new global::DataAccess.Criteria.DbColumn(TableName,"isActive");


public static readonly IDbColumn CardNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardNo");


public static readonly IDbColumn CardDigitsColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardDigits");


public static readonly IDbColumn CardNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardName");


public static readonly IDbColumn CardDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardDate");


public static readonly IDbColumn CardTransIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CardTransId");


public static readonly IDbColumn ChequeNoColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeNo");


public static readonly IDbColumn ChequeDateColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ChequeDate");


public static readonly IDbColumn WalletTransIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"WalletTransId");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");



private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return SalesIdColumn;


yield return PaymentIndColumn;


yield return SubPaymentIndColumn;


yield return AmountColumn;


yield return isActiveColumn;


yield return CardNoColumn;


yield return CardDigitsColumn;


yield return CardNameColumn;


yield return CardDateColumn;


yield return CardTransIdColumn;


yield return ChequeNoColumn;


yield return ChequeDateColumn;


yield return WalletTransIdColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return OfflineStatusColumn;


            
        }

}

}
