
using System.Collections.Generic;
using DataAccess.Criteria;
using DataAccess.Criteria.Interface;

namespace HQue.DataAccess.DbModel
{

public class VendorPurchaseItemTable
{

	public const string TableName = "VendorPurchaseItem";
public static readonly IDbTable Table = new DbTable("VendorPurchaseItem", GetColumn);

        
public static readonly IDbColumn IdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Id");


public static readonly IDbColumn AccountIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"AccountId");


public static readonly IDbColumn InstanceIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"InstanceId");


public static readonly IDbColumn VendorPurchaseIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VendorPurchaseId");


public static readonly IDbColumn ProductNameColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductName");


public static readonly IDbColumn ProductStockIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"ProductStockId");


public static readonly IDbColumn PackageSizeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSize");


public static readonly IDbColumn PackageQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageQty");


public static readonly IDbColumn PackagePurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackagePurchasePrice");


public static readonly IDbColumn PackageSellingPriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageSellingPrice");


public static readonly IDbColumn PackageMRPColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PackageMRP");


public static readonly IDbColumn QuantityColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Quantity");


public static readonly IDbColumn PurchasePriceColumn = new global::DataAccess.Criteria.DbColumn(TableName,"PurchasePrice");


public static readonly IDbColumn OfflineStatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"OfflineStatus");


public static readonly IDbColumn CreatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedAt");


public static readonly IDbColumn UpdatedAtColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedAt");


public static readonly IDbColumn CreatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"CreatedBy");


public static readonly IDbColumn UpdatedByColumn = new global::DataAccess.Criteria.DbColumn(TableName,"UpdatedBy");


public static readonly IDbColumn FreeQtyColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FreeQty");


public static readonly IDbColumn DiscountColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Discount");


public static readonly IDbColumn StatusColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Status");


public static readonly IDbColumn VATColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VAT");


public static readonly IDbColumn VatValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"VatValue");


public static readonly IDbColumn DiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"DiscountValue");


public static readonly IDbColumn fromTempIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"fromTempId");


public static readonly IDbColumn fromDcIdColumn = new global::DataAccess.Criteria.DbColumn(TableName,"fromDcId");


public static readonly IDbColumn IsPoItemColumn = new global::DataAccess.Criteria.DbColumn(TableName,"IsPoItem");


public static readonly IDbColumn HsnCodeColumn = new global::DataAccess.Criteria.DbColumn(TableName,"HsnCode");


public static readonly IDbColumn IgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Igst");


public static readonly IDbColumn CgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Cgst");


public static readonly IDbColumn SgstColumn = new global::DataAccess.Criteria.DbColumn(TableName,"Sgst");


public static readonly IDbColumn GstTotalColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstTotal");


public static readonly IDbColumn GstValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"GstValue");


public static readonly IDbColumn MarkupPercColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MarkupPerc");


public static readonly IDbColumn MarkupValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"MarkupValue");


public static readonly IDbColumn SchemeDiscountPercColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SchemeDiscountPerc");


public static readonly IDbColumn SchemeDiscountValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"SchemeDiscountValue");


public static readonly IDbColumn FreeQtyTaxValueColumn = new global::DataAccess.Criteria.DbColumn(TableName,"FreeQtyTaxValue");

        public static readonly IDbColumn VendorPurchaseItemSnoColumn = new global::DataAccess.Criteria.DbColumn(TableName, "VendorPurchaseItemSno");

        private static IEnumerable<IDbColumn> GetColumn()
        {
		 
yield return IdColumn;


yield return AccountIdColumn;


yield return InstanceIdColumn;


yield return VendorPurchaseIdColumn;


yield return ProductNameColumn;


yield return ProductStockIdColumn;


yield return PackageSizeColumn;


yield return PackageQtyColumn;


yield return PackagePurchasePriceColumn;


yield return PackageSellingPriceColumn;


yield return PackageMRPColumn;


yield return QuantityColumn;


yield return PurchasePriceColumn;


yield return OfflineStatusColumn;


yield return CreatedAtColumn;


yield return UpdatedAtColumn;


yield return CreatedByColumn;


yield return UpdatedByColumn;


yield return FreeQtyColumn;


yield return DiscountColumn;


yield return StatusColumn;


yield return VATColumn;


yield return VatValueColumn;


yield return DiscountValueColumn;


yield return fromTempIdColumn;


yield return fromDcIdColumn;


yield return IsPoItemColumn;


yield return HsnCodeColumn;


yield return IgstColumn;


yield return CgstColumn;


yield return SgstColumn;


yield return GstTotalColumn;


yield return GstValueColumn;


yield return MarkupPercColumn;


yield return MarkupValueColumn;


yield return SchemeDiscountPercColumn;


yield return SchemeDiscountValueColumn;


yield return FreeQtyTaxValueColumn;

  yield return VendorPurchaseItemSnoColumn;

        }

}

}
