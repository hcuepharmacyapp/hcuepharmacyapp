﻿using DataAccess.Criteria.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Helpers.Extension;
using Utilities.Enum;

namespace HQue.DataAccess.Inventory
{
    public class PurchaseExtendedQuery
    {
        

        public static void BuildExtendedPurchaseQuery(VendorPurchase data, QueryBuilderBase qb)
        {
            
            switch (data.Select)
            {
                case "batchNo":
                    qb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.VendorPurchaseIdColumn, VendorPurchaseTable.IdColumn);
                    qb.JoinBuilder.Join(ProductStockTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    qb.ConditionBuilder.And(ProductStockTable.BatchNoColumn);
                    qb.Parameters.Add(ProductStockTable.BatchNoColumn.ColumnName, data.Values);
                    //qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    //qb.JoinBuilder.Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    //qb.ConditionBuilder.And(ProductStockTable.BatchNoColumn, data.Values);
                    break;
                case "product":
                    qb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.VendorPurchaseIdColumn, VendorPurchaseTable.IdColumn);
                    qb.JoinBuilder.Join(ProductStockTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, data.Values);
                    break;
                case "invoiceNo":
                    qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(VendorPurchaseTable.InvoiceNoColumn.ColumnName, data.Values);
                    //qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    //qb.JoinBuilder.Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    //qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, data.Values);
                    break;
                case "grNo":
                    qb.ConditionBuilder.And(VendorPurchaseTable.GoodsRcvNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(VendorPurchaseTable.GoodsRcvNoColumn.ColumnName, data.Values);
                    //qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    //SetComparerQuery(SalesItemTable.QuantityColumn, qb, data.Values, data.Select1);
                    break;
                case "vendorName":
                    qb.ConditionBuilder.And(VendorTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(VendorTable.NameColumn.ColumnName, data.Values);
                    //qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    //qb.JoinBuilder.Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    //SetComparerQuery(ProductStockTable.SellingPriceColumn, qb, data.Values, data.Select1);
                    break;
                //case "discount":
                //    SetComparerQuery(SalesTable.DiscountColumn, qb, data.Values, data.Select1);
                //    break;
                //case "vat":
                //    qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                //    qb.JoinBuilder.Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                //    SetComparerQuery(ProductStockTable.VATColumn, qb, data.Values, data.Select1);
                //    break;
                //case "billNo":
                //    qb.ConditionBuilder.And(SalesTable.InvoiceNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                //    qb.Parameters.Add(SalesTable.InvoiceNoColumn.ColumnName, data.Values);
                //    break;
                case "email":
                    qb.ConditionBuilder.And(VendorTable.EmailColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(VendorTable.EmailColumn.ColumnName, data.Values);
                    break;
                case "mobile":
                    qb.ConditionBuilder.And(VendorTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(VendorTable.MobileColumn.ColumnName, data.Values);
                    break;
                //case "doctorName":
                //    qb.ConditionBuilder.And(SalesTable.DoctorNameColumn, CriteriaEquation.Like);
                //    qb.Parameters.Add(SalesTable.DoctorNameColumn.ColumnName, data.Values);
                //    break;
                //case "doctorMobile":
                //    qb.ConditionBuilder.And(SalesTable.DoctorMobileColumn);
                //    qb.Parameters.Add(SalesTable.DoctorMobileColumn.ColumnName, data.Values);
                //    break;
                case "billDate":
                    //qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceDateColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    //qb.Parameters.Add(VendorPurchaseTable.InvoiceDateColumn.ColumnName, data.Values != null ? data.Values : null, data.Select1);
                    SetComparerQuery(VendorPurchaseTable.InvoiceDateColumn, qb, data.Values, data.Select1);
                    break;
                case "expiry":
                    //qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    qb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.VendorPurchaseIdColumn, VendorPurchaseTable.IdColumn);
                    qb.JoinBuilder.Join(ProductStockTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    SetComparerQuery(ProductStockTable.ExpireDateColumn, qb, data.Values, data.Select1);
                    break;
            }
        }

        private static void SetComparerQuery(IDbColumn column, QueryBuilderBase qb, string value, string condition)
        {
            const string compareGreater = "greater";
            const string compareLesser = "less";

            switch (condition)
            {
                case compareGreater:
                    qb.ConditionBuilder.And(column, CriteriaEquation.Greater);
                    break;
                case compareLesser:
                    qb.ConditionBuilder.And(column, CriteriaEquation.Lesser);
                    break;
                default:
                    qb.ConditionBuilder.And(column);
                    break;
            }

            qb.Parameters.Add(column.ColumnName, value);
        }
    }
}