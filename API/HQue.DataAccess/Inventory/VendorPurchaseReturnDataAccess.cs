﻿using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using DataAccess.Criteria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HQue.DataAccess.Helpers;
using Utilities.Helpers;
using DataAccess;

namespace HQue.DataAccess.Inventory
{
    public class VendorPurchaseReturnDataAccess : BaseDataAccess
    {
        private readonly ProductStockDataAccess _productStockDataAccess;
        private readonly HelperDataAccess _HelperDataAccess;
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        public VendorPurchaseReturnDataAccess(ISqlHelper sqlQueryExecuter, ProductStockDataAccess productStockDataAccess, HelperDataAccess helperDataAccess, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter,queryBuilderFactory)
        {
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
            _productStockDataAccess = productStockDataAccess;
            _HelperDataAccess = helperDataAccess; //Added by Poongodi on 26/03/2017
        }

        public override Task<VendorPurchaseReturn> Save<VendorPurchaseReturn>(VendorPurchaseReturn data)
        {
            return Insert(data, VendorReturnTable.Table);
        }

        public async Task<VendorPurchaseReturn> saveReturn(VendorPurchaseReturn data)
        {
            return await Insert(data, VendorReturnTable.Table);
        }

        public async Task<string> GetReturnNo( VendorPurchaseReturn v)
        {
            /*Prefix added by Poongodi on 10/10/2017*/
            DateTime returndate = v.ReturnDate as DateTime? ?? CustomDateTime.IST;
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.PurchaseReturn, returndate, v.InstanceId, v.AccountId,"",v.Prefix);
            return sBillNo.ToString();
        }
        //finyear method added by Poongodi on 26/03/2017
        public async Task<string> GetFinYear(VendorPurchaseReturn v)
        {
            DateTime returndate = v.ReturnDate as DateTime? ?? CustomDateTime.IST;
            string sFinyr = await _HelperDataAccess.GetFinyear(returndate, v.InstanceId, v.AccountId);
            return Convert.ToString(sFinyr);

        }

        public async Task<IEnumerable<VendorPurchaseReturnItem>> Save(IEnumerable<VendorPurchaseReturnItem> itemList)
        {
            foreach (var vendorPurchaseReturnItem in itemList)
            {
                if (vendorPurchaseReturnItem.Quantity != null && vendorPurchaseReturnItem.Quantity > 0)
                {
                    // Update Product Stock 
                    var productStock = await _productStockDataAccess.GetStockValue(vendorPurchaseReturnItem.ProductStockId);
                    productStock.Stock = productStock.Stock - vendorPurchaseReturnItem.Quantity;
                    if(productStock.Stock==0)
                    {
                        productStock.IsMovingStock = null;
                    }
                    await _productStockDataAccess.Update(productStock);
                    await Insert(vendorPurchaseReturnItem, VendorReturnItemTable.Table);
                }
            }
            return itemList;
        }

        //Added by Lawrence
        public async Task<IEnumerable<VendorPurchaseReturnItem>> SaveItems(IEnumerable<VendorPurchaseReturnItem> itemList)  
        {
            foreach (var vendorPurchaseReturnItem in itemList)
            {
			// Added Gavaskar 30-08-2017 
                if (vendorPurchaseReturnItem.PurchaseQuantity!=null)
                {
                    vendorPurchaseReturnItem.Quantity = vendorPurchaseReturnItem.PurchaseQuantity;
                }
              
                if (vendorPurchaseReturnItem.Quantity != null && vendorPurchaseReturnItem.Quantity > 0)
                {
                    // Update Product Stock 
                    var productStock = await _productStockDataAccess.GetStockValue(vendorPurchaseReturnItem.ProductStockId);
                    productStock.Stock = productStock.Stock - vendorPurchaseReturnItem.Quantity;
                    if (productStock.Stock == 0)
                    {
                        productStock.IsMovingStock = null;
                    }
                    await _productStockDataAccess.Update(productStock);
                    await Insert(vendorPurchaseReturnItem, VendorReturnItemTable.Table);
                }
            }
            return itemList;
        }

        public async Task<int> Count(VendorPurchaseReturn data, bool IsInvoiceDate)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnTable.Table, OperationType.Count);
            qb.JoinBuilder.LeftJoin(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorReturnTable.VendorPurchaseIdColumn); // Modified by Gavaskar
            qb.JoinBuilder.LeftJoin(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn); // Modified by Gavaskar
            qb = SqlQueryBuilder(data, qb, (IsInvoiceDate == false) ? false : IsInvoiceDate);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        private static QueryBuilderBase SqlQueryBuilder(VendorPurchaseReturn data, QueryBuilderBase qb, bool IsInvoiceDate)
        {
            if (!string.IsNullOrEmpty(data.VendorPurchase.Vendor.Name))
            {
                qb.ConditionBuilder.And(VendorTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorTable.NameColumn.ColumnName, data.VendorPurchase.Vendor.Name);
            }
            //Added by Annadurai 06142017 starts here
            if (!string.IsNullOrEmpty(data.PaymentStatus) && data.PaymentStatus != "All")
            {
                if (data.PaymentStatus == "None")
                {
                    var c1 = new CustomCriteria(VendorReturnTable.PaymentStatusColumn, CriteriaCondition.IsNull);
                    var c2 = new CriteriaColumn(VendorReturnTable.PaymentStatusColumn, "''", CriteriaEquation.Equal);
                    var condition3 = new CustomCriteria(c2, c1, CriteriaCondition.Or);
                    qb.ConditionBuilder.And(condition3);
                }
                else
                {
                    qb.ConditionBuilder.And(VendorReturnTable.PaymentStatusColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(VendorReturnTable.PaymentStatusColumn.ColumnName, data.PaymentStatus);
                }

            }
            if (!string.IsNullOrEmpty(data.ReturnNo))
            {
                qb.ConditionBuilder.And(VendorReturnTable.ReturnNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorReturnTable.ReturnNoColumn.ColumnName, data.ReturnNo);
            }
            if (data.FromDate != null && data.ToDate != null)
            {
                //ReturnDateColumn >= data.FromDate and ReturnDateColumn < data.ToDate
                var cc1 = new CriteriaColumn(VendorReturnTable.ReturnDateColumn, "'" + data.FromDate.Value.ToString("yyyy-MM-dd") + "'", CriteriaEquation.GreaterEqual);
                var cc2 = new CriteriaColumn(VendorReturnTable.ReturnDateColumn, "'" + data.ToDate.Value.ToString("yyyy-MM-dd") + "'", CriteriaEquation.LesserEqual);
                var condition4 = new CustomCriteria(cc1, cc2, CriteriaCondition.And);
                qb.ConditionBuilder.And(condition4);
            }
            //Added by Annadurai 06142017 Ends here
            if (!string.IsNullOrEmpty(data.VendorPurchase.Vendor.Mobile))
            {
                qb.ConditionBuilder.And(VendorTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorTable.MobileColumn.ColumnName, data.VendorPurchase.Vendor.Mobile);
            }
            if (!string.IsNullOrEmpty(data.InvoiceNo))
            {
                qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorPurchaseTable.InvoiceNoColumn.ColumnName, data.InvoiceNo);
            }
            if (!string.IsNullOrEmpty(data.VendorPurchase.GoodsRcvNo))
            {
                /*Prefix filter added by Poongodi on 23/06/2017 */
                string letters = null, numbers = null, prefix = null;
                foreach (char c in data.VendorPurchase.GoodsRcvNo)
                {
                    if (Char.IsLetter(c))
                    {
                        if (c.ToString().ToUpper() == "O")
                            prefix = c.ToString();
                        else
                            letters += c;
                    }
                    if (Char.IsNumber(c))
                    {
                        numbers += c;
                    }
                }
                if (!string.IsNullOrEmpty(prefix))
                {
                    qb.ConditionBuilder.And(VendorPurchaseTable.PrefixColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(VendorPurchaseTable.PrefixColumn.ColumnName, prefix);
                }
                if (!string.IsNullOrEmpty(letters))
                {
                    qb.ConditionBuilder.And(VendorPurchaseTable.BillSeriesColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(VendorPurchaseTable.BillSeriesColumn.ColumnName, letters);
                }
                if (!string.IsNullOrEmpty(numbers))
                {
                    qb.ConditionBuilder.And(VendorPurchaseTable.GoodsRcvNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(VendorPurchaseTable.GoodsRcvNoColumn.ColumnName, numbers);
                }

            }
            // Added for "PURCHASE RETURN AND EXPIRY STATUS" report filter by "Invoice Date"
            var filterFromDate = data.FromDate;
            var filterToDate = data.ToDate;
            if (IsInvoiceDate)
            {
                qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceDateColumn, CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", filterFromDate);
                qb.Parameters.Add("FilterToDate", filterToDate);
            }

            
            if (!string.IsNullOrEmpty(data.SearchProductId))
            {
                qb.JoinBuilder.Join(VendorReturnItemTable.Table, VendorReturnItemTable.VendorReturnIdColumn, VendorReturnTable.IdColumn);
                qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
                qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
                qb.ConditionBuilder.And(ProductTable.IdColumn, data.SearchProductId);
            }
            if (!string.IsNullOrEmpty(data.BatchNo) && !string.IsNullOrEmpty(data.SearchProductId))
            {
                qb.ConditionBuilder.And(ProductStockTable.BatchNoColumn);
                qb.Parameters.Add(ProductStockTable.BatchNoColumn.ColumnName, data.BatchNo);
            }
            else if (!string.IsNullOrEmpty(data.BatchNo) && string.IsNullOrEmpty(data.SearchProductId))
            {
                qb.JoinBuilder.Join(VendorReturnItemTable.Table, VendorReturnItemTable.VendorReturnIdColumn, VendorReturnTable.IdColumn);
                qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
                qb.ConditionBuilder.And(ProductStockTable.BatchNoColumn);
                qb.Parameters.Add(ProductStockTable.BatchNoColumn.ColumnName, data.BatchNo);
            }
            if (data.InvoiceDate.HasValue)
            {

                qb.ConditionBuilder.And(VendorReturnTable.ReturnDateColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorReturnTable.ReturnDateColumn.ColumnName, data.InvoiceDate != null ? data.InvoiceDate.Value.ToString("yyyy-MM-dd") : null);
            }
            //Added by Annadurai 06142017
            //if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId) && string.IsNullOrEmpty(data.Id))
            {
                qb.ConditionBuilder.And(VendorReturnTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(VendorReturnTable.InstanceIdColumn, data.InstanceId);
            }
            if (!string.IsNullOrEmpty(data.Id))
            {
                qb.ConditionBuilder.And(VendorReturnTable.IdColumn, data.Id);
            }
           

            return qb;
        }

        public async Task<IEnumerable<VendorPurchaseReturn>> List(VendorPurchaseReturn data, bool IsInvoiceDate, bool isFromReport)
        {
            // Column added by Settu to implement payment status & remarks
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnTable.Table, OperationType.Select);
            /*Prefix Column added by Poongodi on 14/06/2017*/
            qb.AddColumn(VendorReturnTable.IdColumn, VendorReturnTable.ReturnNoColumn, VendorReturnTable.PrefixColumn, VendorReturnTable.ReturnDateColumn, VendorReturnTable.ReasonColumn, VendorReturnTable.PaymentStatusColumn, VendorReturnTable.PaymentRemarksColumn, VendorReturnTable.TaxRefNoColumn);

            qb = SqlQueryBuilder(data, qb, IsInvoiceDate);

            // Added by Lawrence for Non Purchased Products returned by Vendor --   Start            
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorReturnTable.VendorIdColumn); //qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn); 
            //-- Vendor Return End
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.IdColumn, VendorTable.NameColumn, VendorTable.EmailColumn, VendorTable.MobileColumn,
                VendorTable.AddressColumn, VendorTable.CityColumn, VendorTable.AreaColumn, VendorTable.PincodeColumn, VendorTable.DrugLicenseNoColumn, VendorTable.GsTinNoColumn, VendorTable.TinNoColumn, VendorTable.LocationTypeColumn);

            qb.JoinBuilder.Join(InstanceTable.Table, InstanceTable.IdColumn, VendorReturnTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.IdColumn, InstanceTable.NameColumn,
                InstanceTable.DrugLicenseNoColumn, InstanceTable.GsTinNoColumn, InstanceTable.TinNoColumn,
                InstanceTable.AddressColumn, InstanceTable.AreaColumn, InstanceTable.CityColumn, InstanceTable.PincodeColumn, InstanceTable.isUnionTerritoryColumn);

            qb.JoinBuilder.LeftJoin(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorReturnTable.VendorPurchaseIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorPurchaseTable.VendorIdColumn, VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.GoodsRcvNoColumn, VendorPurchaseTable.InvoiceDateColumn, VendorPurchaseTable.BillSeriesColumn);

            qb.SelectBuilder.SortByDesc(VendorReturnTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);

            var list = new List<VendorPurchaseReturn>();
            // int TaxRefNo = 0;
            /*Prefix Added by Poongodi on 15/06/2017*/
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseReturn = new VendorPurchaseReturn
                    {
                        ReturnNo = Convert.ToString(reader[VendorReturnTable.PrefixColumn.ColumnName]) + reader[VendorReturnTable.ReturnNoColumn.ColumnName].ToString(),
                        ReturnDate = (reader[VendorReturnTable.ReturnDateColumn.ColumnName]) == DBNull.Value ? null : ((DateTime?)reader[VendorReturnTable.ReturnDateColumn.ColumnName]),
                        Id = reader[VendorReturnTable.IdColumn.ColumnName].ToString(),
                        Reason = reader[VendorReturnTable.ReasonColumn.ColumnName].ToString(),
                        PaymentStatus = reader[VendorReturnTable.PaymentStatusColumn.ColumnName].ToString(),
                        PaymentRemarks = reader[VendorReturnTable.PaymentRemarksColumn.ColumnName].ToString(),
                        TaxRefNo = (reader[VendorReturnTable.TaxRefNoColumn.ColumnName]) == DBNull.Value ? 0 : (Convert.ToInt32(reader[VendorReturnTable.TaxRefNoColumn.ColumnName])),
                       
                        VendorPurchase =
                        {
                            VendorId = reader[VendorPurchaseTable.VendorIdColumn.FullColumnName].ToString(),
                            InvoiceNo = reader[VendorPurchaseTable.InvoiceNoColumn.FullColumnName].ToString(),
                            GoodsRcvNo = reader[VendorPurchaseTable.GoodsRcvNoColumn.FullColumnName].ToString(),
                            BillSeries = Convert.ToString( reader[VendorPurchaseTable.PrefixColumn.ColumnName].ToString())+ reader[VendorPurchaseTable.BillSeriesColumn.FullColumnName].ToString(),
                              //InvoiceDate = reader[VendorReturnTable.ReturnDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                              
                            Vendor =
                            {
                                Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
                                Email = reader[VendorTable.EmailColumn.FullColumnName].ToString(),
                                Mobile = reader[VendorTable.MobileColumn.FullColumnName].ToString(),
                                Id = reader[VendorTable.IdColumn.FullColumnName].ToString(),
                                Address = reader[VendorTable.AddressColumn.FullColumnName].ToString(),
                                Area = reader[VendorTable.AreaColumn.FullColumnName].ToString(),
                                City = reader[VendorTable.CityColumn.FullColumnName].ToString(),
                                Pincode = reader[VendorTable.PincodeColumn.FullColumnName].ToString(),
                                DrugLicenseNo = reader[VendorTable.DrugLicenseNoColumn.FullColumnName].ToString(),
                                GsTinNo = reader[VendorTable.GsTinNoColumn.FullColumnName].ToString(),
                                TinNo = reader[VendorTable.TinNoColumn.FullColumnName].ToString(),
                                LocationType = reader[VendorTable.LocationTypeColumn.FullColumnName] as int? ?? 1,
                            },
                            Instance =
                            {
                                Id = reader[InstanceTable.IdColumn.FullColumnName].ToString(),
                                Name = reader[InstanceTable.NameColumn.FullColumnName].ToString(),
                                DrugLicenseNo = reader[InstanceTable.DrugLicenseNoColumn.FullColumnName].ToString(),
                                GsTinNo = reader[InstanceTable.GsTinNoColumn.FullColumnName].ToString(),
                                TinNo = reader[InstanceTable.TinNoColumn.FullColumnName].ToString(),
                                Address = reader[InstanceTable.AddressColumn.FullColumnName].ToString(),
                                Area = reader[InstanceTable.AreaColumn.FullColumnName].ToString(),
                                City = reader[InstanceTable.CityColumn.FullColumnName].ToString(),
                                Pincode = reader[InstanceTable.PincodeColumn.FullColumnName].ToString(),
                                isUnionTerritory = reader[InstanceTable.isUnionTerritoryColumn.FullColumnName] as bool? ?? false,
                            }
                        }
                    };
                    if (Convert.ToString( reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName] )!= "")
                    {

                        vendorPurchaseReturn.InvoiceDate = (DateTime)reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName];
                        vendorPurchaseReturn.VendorPurchase.InvoiceDate = (DateTime)reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName];
                    }
                    else
                    {
                        vendorPurchaseReturn.InvoiceDate = (DateTime)reader[VendorReturnTable.ReturnDateColumn.ColumnName];
                        vendorPurchaseReturn.VendorPurchase.InvoiceDate = (DateTime)reader[VendorReturnTable.ReturnDateColumn.ColumnName];
                    }
                    list.Add(vendorPurchaseReturn);
                }
            }

            var vp = await SqlQueryList(list, isFromReport);

            var vendorReturn = list.Select(x =>
            {
                x.VendorPurchaseReturnItem = vp.Where(y => y.VendorReturnId == x.Id).Select(z => z).ToList();
                return x;
            });

            return vendorReturn;
        }

        public async Task<List<VendorPurchaseReturnItem>> SqlQueryList(IEnumerable<VendorPurchaseReturn> data, bool isFromReport)
        {
            var vriqb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            vriqb.AddColumn(VendorReturnItemTable.ProductStockIdColumn, VendorReturnItemTable.QuantityColumn, VendorReturnItemTable.VendorReturnIdColumn
                , VendorReturnItemTable.ReturnedTotalColumn, VendorReturnItemTable.ReturnPurchasePriceColumn, VendorReturnItemTable.GstTotalColumn,
                VendorReturnItemTable.SgstColumn, VendorReturnItemTable.CgstColumn, VendorReturnItemTable.IgstColumn, VendorReturnItemTable.IsDeletedColumn);            


            //changed join condition for accommadating temp stock on 04/11/2016
            vriqb.JoinBuilder.Join(VendorReturnTable.Table, VendorReturnTable.IdColumn, VendorReturnItemTable.VendorReturnIdColumn);
            vriqb.JoinBuilder.LeftJoin(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorReturnTable.VendorPurchaseIdColumn);

            vriqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            vriqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.MRPColumn, ProductStockTable.VATColumn, ProductStockTable.BatchNoColumn, ProductStockTable.CSTColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.PackageSizeColumn, ProductStockTable.PackagePurchasePriceColumn, ProductStockTable.PurchasePriceColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.GstTotalColumn);
            //Purchaseid added in joins to avoid item duplicate - by Poongodi on 18/04/2017
            vriqb.JoinBuilder.LeftJoins(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn, VendorPurchaseItemTable.VendorPurchaseIdColumn, VendorReturnTable.VendorPurchaseIdColumn);
            vriqb.JoinBuilder.AddJoinColumn(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.QuantityColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.PackageSizeColumn, VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.PackageMRPColumn);

            vriqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            vriqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
 

            var i = 1;
            foreach (var vendorReturn in data)
            {
                var paramName = $"s{i++}";
                var caol = new CriteriaColumn(VendorReturnItemTable.VendorReturnIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(caol);
                vriqb.ConditionBuilder.Or(cc);
                vriqb.Parameters.Add(paramName, vendorReturn.Id);

                //Added by Sarubala on 18-01-18
                if (isFromReport)
                {
                    var isDeletedCondition = new CustomCriteria(VendorReturnItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
                    var cc1 = new CriteriaColumn(VendorReturnItemTable.IsDeletedColumn, "0", CriteriaEquation.Equal);
                    var col = new CustomCriteria(cc1, isDeletedCondition, CriteriaCondition.Or);
                    vriqb.ConditionBuilder.And(col);
                }
            }

            var list = new List<VendorPurchaseReturnItem>();

                using (var reader = await QueryExecuter.QueryAsyc(vriqb))
                {
                    while (reader.Read())
                    {
                        var vendorReturnItem = new VendorPurchaseReturnItem
                        {
                            ProductStockId = reader[VendorReturnItemTable.ProductStockIdColumn.ColumnName].ToString(),
                            VendorReturnId = reader[VendorReturnItemTable.VendorReturnIdColumn.ColumnName].ToString(),
                            Quantity = reader[VendorReturnItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                            ReturnedTotal = reader[VendorReturnItemTable.ReturnedTotalColumn.ColumnName] as decimal? ?? 0,
                            ReturnPurchasePrice = reader[VendorReturnItemTable.ReturnPurchasePriceColumn.ColumnName] as decimal? ?? 0,
                            GstTotal = reader[VendorReturnItemTable.GstTotalColumn.ColumnName] as decimal? ?? 0,
                            Sgst = reader[VendorReturnItemTable.SgstColumn.ColumnName] as decimal? ?? 0,
                            Cgst = reader[VendorReturnItemTable.CgstColumn.ColumnName] as decimal? ?? 0,
                            Igst = reader[VendorReturnItemTable.IgstColumn.ColumnName] as decimal? ?? 0,
                            IsDeleted = reader[VendorReturnItemTable.IsDeletedColumn.ColumnName] as bool? ?? false,
                            ProductStock =
                        {
                            SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ??0,
                            MRP = reader[ProductStockTable.MRPColumn.FullColumnName] as decimal? ??0,
                            PackagePurchasePrice = reader[ProductStockTable.PackagePurchasePriceColumn.FullColumnName] as decimal? ??0,
                            PurchasePrice = reader[ProductStockTable.PurchasePriceColumn.FullColumnName] as decimal? ??0,
                            Product = {Name = reader[ProductTable.NameColumn.FullColumnName].ToString()},
                            VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ??0,
                            CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ??0,
                             //GST related fields
                            Igst = reader[ProductStockTable.IgstColumn.FullColumnName] as decimal? ??0,
                            Cgst = reader[ProductStockTable.CgstColumn.FullColumnName] as decimal? ??0,
                            Sgst = reader[ProductStockTable.SgstColumn.FullColumnName] as decimal? ??0,
                            GstTotal = reader[ProductStockTable.GstTotalColumn.FullColumnName] as decimal? ??0,
                            PackageSize = reader[ProductStockTable.PackageSizeColumn.FullColumnName] as decimal? ??0,
                            BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString(),
                            ExpireDate = (DateTime)reader[ProductStockTable.ExpireDateColumn.FullColumnName]
                        },

                            VendorPurchaseItem = {
                            PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.FullColumnName] as decimal? ??0,
                            Quantity = reader[VendorPurchaseItemTable.QuantityColumn.FullColumnName] as decimal? ??0,
                            FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.FullColumnName] as decimal? ??0,
                            Discount = reader[VendorPurchaseItemTable.DiscountColumn.FullColumnName] as decimal? ??0,
                            PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.FullColumnName] as decimal? ??0,
                            PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.FullColumnName] as decimal? ??0,
                            PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.FullColumnName] as decimal? ??0,
                            PackageMRP = reader[VendorPurchaseItemTable.PackageMRPColumn.FullColumnName] as decimal? ??0,
                        }
                        };

                        list.Add(vendorReturnItem);
                    }
                }
            return list;
        }
        // Added by Violet
        public async Task<List<VendorPurchaseItem>> GetVendorPurchaseReturnItem(string productName, string instanceId) //, string VendorId
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", user.AccountId());
            parms.Add("InstanceId", instanceId);
            parms.Add("Name", productName);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetVendorPurchaseReturnItem", parms);
            var data = result.Select(item =>
            {
                var VendorPurchaseItem = new VendorPurchaseItem()
                {
                    Quantity = item.Quantity as decimal? ?? 0,
                    ProductStockId = item.ProductStockId as System.String ?? "",
                    PurchasePrice = item.PurchasePrice as decimal? ?? 0,
                    PackageSellingPrice = item.PackageSellingPrice as decimal? ?? 0,
                    //PackagePurchasePrice = item.PackagePurchasePrice as decimal? ?? 0,
                    PackagePurchasePrice = item.pkgPurPrice as decimal? ?? 0,
                    PackageMRP = item.PackageMRP as decimal? ?? 0,
                    PackageQty = item.PackageQty as decimal? ?? 0,
                    PackageSize = item.PackageSize as decimal? ?? 0,
                    FreeQty = item.FreeQty as decimal? ?? 0,
                    Discount = item.Discount as decimal? ?? 0,
                    ProductStock = {
                        Product =
                        {
                            Name = item.Name as System.String ?? ""
                        },
                        Id =  item.productstockid as System.String ?? "",
                        ProductId  =  item.ProductId as System.String ?? "",
                        BatchNo  =  item.BatchNo as System.String ?? "",
                        ExpireDate  =  item.ExpireDate as DateTime? ?? DateTime.MinValue,
                        VAT = item.VAT as decimal? ?? 0,
                        // GST Related
                        Igst = item.Igst as decimal? ?? 0,
                        Cgst = item.Cgst as decimal? ?? 0,
                        Sgst = item.Sgst as decimal? ?? 0,
                        GstTotal = item.GstTotal as decimal? ?? 0,
                        SellingPrice  = item.SellingPrice as decimal? ?? 0,
                        MRP = item.MRP as decimal? ?? 0,
                        Stock =  item.Stock as decimal? ?? 0,
                        CST = item.CST as decimal? ?? 0,
                        PackageSize = item.PackageSize as decimal? ?? 0
                    }
                };
                return VendorPurchaseItem;
            }).ToList();
            return data;
        }
        // Added by Settu to implement payment status & remarks
        public async Task<bool> updateVendorPurchaseReturn(VendorPurchaseReturn data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnTable.Table, OperationType.Update);
            qb.AddColumn(VendorReturnTable.PaymentStatusColumn, VendorReturnTable.PaymentRemarksColumn, VendorReturnTable.UpdatedAtColumn, VendorReturnTable.OfflineStatusColumn);
            qb.ConditionBuilder.And(VendorReturnTable.IdColumn);
            qb.Parameters.Add(VendorReturnTable.PaymentStatusColumn.ColumnName, data.PaymentStatus);
            qb.Parameters.Add(VendorReturnTable.PaymentRemarksColumn.ColumnName, data.PaymentRemarks);
            qb.Parameters.Add(VendorReturnTable.IdColumn.ColumnName, data.Id);
            qb.Parameters.Add(VendorReturnTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(VendorReturnTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
            await QueryExecuter.NonQueryAsyc(qb);
            return true;
        }

        // Added by Gavaskar Purchase With Return
        public async Task<VendorPurchaseReturn> updateVendorPurchaseReturnIsAlongWithPurchase(VendorPurchaseReturn data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnTable.Table, OperationType.Update);
            qb.AddColumn(VendorReturnTable.TotalReturnedAmountColumn, VendorReturnTable.RoundOffValueColumn, VendorReturnTable.TotalDiscountValueColumn, VendorReturnTable.TotalGstValueColumn);
            qb.ConditionBuilder.And(VendorReturnTable.IdColumn);
            qb.Parameters.Add(VendorReturnTable.TotalReturnedAmountColumn.ColumnName, data.TotalReturnedAmount);
            qb.Parameters.Add(VendorReturnTable.RoundOffValueColumn.ColumnName, data.RoundOffValue);
            qb.Parameters.Add(VendorReturnTable.TotalDiscountValueColumn.ColumnName, data.TotalDiscountValue);
            qb.Parameters.Add(VendorReturnTable.TotalGstValueColumn.ColumnName, data.TotalGstValue);
            qb.Parameters.Add(VendorReturnTable.IdColumn.ColumnName, data.Id);
            qb.Parameters.Add(VendorReturnTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;


        }

        // Added by Gavaskar Purchase With Return
        public async Task<IEnumerable<VendorPurchaseReturnItem>> SaveIsAlongWithPurchaseList(IEnumerable<VendorPurchaseReturnItem> itemList)
        {
            foreach (var vendorPurchaseReturnItem in itemList)
            {
              
                if (vendorPurchaseReturnItem.PurchaseQuantity != null)
                {
                    vendorPurchaseReturnItem.Quantity = vendorPurchaseReturnItem.PurchaseQuantity;
                }

                if (vendorPurchaseReturnItem.Quantity != null && vendorPurchaseReturnItem.Quantity > 0)
                {
                    var productStock = await _productStockDataAccess.GetStockValue(vendorPurchaseReturnItem.ProductStockId);
                    if (vendorPurchaseReturnItem.Id!=null)
                    {
                        if (vendorPurchaseReturnItem.IsDeleted == true)
                        {
                            productStock.Stock = productStock.Stock + vendorPurchaseReturnItem.Quantity;
                            await _productStockDataAccess.Update(productStock);
                            await Update(vendorPurchaseReturnItem, VendorReturnItemTable.Table);
                        }
                        else
                        {
                            var vendorPurchaseReturnItemStock = await GetVendorPurchaseReturnStockValue(vendorPurchaseReturnItem.Id);
                            if (vendorPurchaseReturnItemStock.Quantity > vendorPurchaseReturnItem.Quantity)
                            {
                                productStock.Stock = productStock.Stock + (vendorPurchaseReturnItemStock.Quantity - vendorPurchaseReturnItem.Quantity);
                            }
                            else
                            {
                                productStock.Stock = productStock.Stock - (vendorPurchaseReturnItem.Quantity - vendorPurchaseReturnItemStock.Quantity);
                            }
                            await _productStockDataAccess.Update(productStock);
                            await Update(vendorPurchaseReturnItem, VendorReturnItemTable.Table);
                        }
                        
                    }
                    else
                    {
                        productStock.Stock = productStock.Stock - vendorPurchaseReturnItem.Quantity;
                        if (productStock.Stock == 0)
                        {
                            productStock.IsMovingStock = null;
                        }
                        await _productStockDataAccess.Update(productStock);
                        await Insert(vendorPurchaseReturnItem, VendorReturnItemTable.Table);
                    }
                   
                }
            }
            return itemList;
        }
        // Added by Gavaskar Purchase With Return
        public async Task<VendorPurchaseReturnItem> GetVendorPurchaseReturnStockValue(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VendorReturnItemTable.IdColumn, id);
            return (await List<VendorPurchaseReturnItem>(qb)).First();
        }
        // Added by Gavaskar Purchase With Return
        public async Task<ProductStock> updateIsAlongWithPurchaseStock(ProductStock data)
        {
            //data.UpdatedAt = CustomDateTime.IST;
            //var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            //qb.AddColumn(ProductStockTable.StockColumn);
            //qb.ConditionBuilder.And(ProductStockTable.IdColumn);
            //qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, data.Stock);
            //qb.Parameters.Add(ProductStockTable.IdColumn.ColumnName, data.Id);
            //qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            //await QueryExecuter.NonQueryAsyc(qb);
            //return data;

            data.UpdatedAt = CustomDateTime.IST;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("InstanceId", data.InstanceId);
            param.Add("Accountid", data.AccountId);
            param.Add("Id", data.Id);
            param.Add("Stock", data.Stock);
            param.Add("UpdatedAt", data.UpdatedAt);

            string procName = string.Empty;
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Productstock_Update_IsAlongWithPurchase", param);
            return data;
        }

    }
}
