﻿using System.Collections.Generic;
using HQue.Contract.Infrastructure.Inventory;
using System.Linq;
using HQue.DataAccess.DbModel;
using HQue.Contract.Infrastructure.Master;
using System;
using System.Threading.Tasks;
using HQue.Contract.External;
using HQue.Contract.Infrastructure.Settings;
using DataAccess.QueryBuilder;
using DataAccess.ManageData.Interface;
using Utilities.Helpers;
using Utilities.Enum;
using DataAccess.Criteria;
using Dapper;
using DataAccess;
using HQue.DataAccess.Master;
using Newtonsoft.Json;
using System.Collections;
using HQue.Contract.Infrastructure.Setup;
using System.Data.SqlClient;
using System.Data;
using Microsoft.SqlServer.Server;
using HQue.DataAccess.ETL;

namespace HQue.DataAccess.Inventory
{
    public class ProductStockDataAccess : BaseDataAccess
    {
        private readonly ConfigHelper _configHelper;
        private readonly ProductDataAccess _productDataAccess;

        SqlDatabaseHelper sqldb;
        public ProductStockDataAccess(ISqlHelper sqlQueryExecuter, ProductDataAccess productDataAccess,
            QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            _productDataAccess = productDataAccess;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        //public async Task<List<ProductStock>> List(Product data)
        //{
        //    int NameType = 1;
        //    string Name = "";
        //    var Pageno = (data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize;
        //    if (!string.IsNullOrEmpty(data.Name))
        //    {
        //        Name = data.Name;
        //    }
        //    if (!string.IsNullOrEmpty(data.GenericName))
        //    {
        //        Name = data.GenericName;
        //        NameType = 2;
        //    }
        //    var result = await GetInventoryDetails(data.getFilter, data.InstanceId, data.AccountId, NameType, Name, Pageno, data.Page.PageSize);
        //    return result.ToList();
        //}
        private async Task<IEnumerable<ProductStock>> GetInventoryDetails(string type, string InstanceId, string AccountId, int NameType, string Name, int PageNo, int? PageSize)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", InstanceId);
            parms.Add("AccountId", AccountId);
            parms.Add("NameType", NameType);
            parms.Add("Name", Name);
            parms.Add("PageNo", PageNo);
            parms.Add("PageSize", PageSize);
            string procName = string.Empty;
            switch (type)
            {
                //All
                default:
                case "All":
                    procName = "usp_GetInventoryAllInfo";
                    break;
                case "Available":
                    procName = "usp_GetInventoryAvailableInfo";
                    break;
                case "Zero":
                    procName = "usp_GetInventoryZeroQtyInfo";
                    break;
                case "Expiry":
                    procName = "usp_GetInventoryExpiryInfo";
                    break;
                case "Reorder":
                    procName = "usp_GetInventoryReOrderInfo";
                    break;
                case "Inactive":
                    procName = "usp_GetInventoryInactiveInfo";
                    break;
            }
            var result = await sqldb.ExecuteProcedureAsync<dynamic>(procName, parms);
            //int TotalRecordCount = result.FirstOrDefault().TotalRecordCount;
            var productStocks = result.Select(x =>
            {
                var productStock = new ProductStock()
                {
                    InstanceId = InstanceId,
                    ProductId = x.ProductId,
                    Stock = x.Stock,
                    ExpireDate = x.ExpireDate,
                    productName = x.ProductName,
                    Id = Convert.ToString(x.ProductId),
                    TotalCostPrice = x.Totalpurchaseprice, //added by Annadurai
                    Product = new Contract.Infrastructure.Master.Product()
                    {
                        Name = x.ProductName,
                        GenericName = x.GenericName,
                        Code = x.ProductCode,
                        Manufacturer = x.Manufacturer,
                        RackNo = x.RackNo,
                        BoxNo = x.BoxNo, // Updated by Settu to include boxno on 19-05-2017
                        Status = x.Status,
                        Id = Convert.ToString(x.ProductId)
                    },
                    Vendor = new Contract.Infrastructure.Master.Vendor()
                    {
                        Name = x.VendorName
                    },
                    InventoryReOrder = new InventoryReOrder()
                    {
                        Id = x.ReorderId,
                        ReOrder = x.ReOrder,
                        Status = x.ReOrderStatus
                    }
                    //x.GenericName
                };
                return productStock;
            });
            //------------Already existing code refactored. The below code add/updates the values in InventoryReorder for the above productids
            //---sqlUpdateCalculation2
            productStocks = productStocks.ToList();
            //return productStocks;
            /*  var ProductIds = productStocks.Select(x => x.ProductId).ToList();
              var sqlids = "('" + string.Join("'),('", ProductIds) + "')";
              if (type == "Reorder")
              {
                  string query = $@"DECLARE @NoofDay int;
                          DECLARE @tblIds table(ProductId varchar(36));
                          DECLARE @tblProduct table(ProductId varchar(36), ReorderQty decimal(18,2),Stock decimal(18,2));
                          INSERT INTO @tblIds(ProductId) VALUES {sqlids} ;
                          SELECT TOP 1 @NoofDay=ISNULL(NoOfDays ,0)
                          FROM InventorySettings WHERE instanceId = @instanceId 
                          ORDER BY CreatedAt Desc ;
                          INSERT INTO @tblProduct(ProductId,ReorderQty,Stock)
                          SELECT PS.ProductId,SUM(SI.Quantity)/90* @NoofDay  ReorderQty,
                                 SUM(DISTINCT PS.Stock) TotalStock  
                          FROM Sales S 
                          INNER JOIN SalesItem SI on S.Id=SI.SalesId 
                          INNER JOIN ProductStock PS on PS.Id=SI.ProductStockId 
                          INNER JOIN @tblIds IDS ON PS.ProductId=IDS.ProductId
                          WHERE s.InvoiceDate>=DATEADD(month,-3,GETDATE()) 
                          GROUP BY PS.ProductId
                         -- HAVING FLOOR(SUM(si.Quantity)/90* @NoofDay) > 0;
                          UPDATE R SET UpdatedAt=GetDate(), UpdatedBy=CreatedBy ,ReOrder=P.ReorderQty
                          FROm InventoryReOrder R 
                          INNER JOIN @tblProduct P ON P.ProductId= R.ProductId AND R.Status=2
                          WHERE R.instanceId = @instanceId AND R.AccountId=@AccountId AND FLOOR(P.ReorderQty) > 0;
                          INSERT INTO InventoryReOrder(id,AccountId,InstanceId,ProductId,ReOrder,CreatedAt,CreatedBy,Status)
                          SELECT lower(NewID()) id,@AccountId ,@instanceId,P.ProductId,P.ReorderQty,getdate(),null,2
                          FROM @tblProduct P 
                          WHERE FLOOR(P.ReorderQty) > 0
                          NOT EXISTS( SELECT 1 FROM InventoryReOrder R WHERE R.instanceId = @instanceId 
                          AND R.AccountId=@AccountId
                          AND P.ProductId= R.ProductId AND R.Status=2);
                          ";
                  var updatResult = await sqldb.ExecuteQueryAsync<dynamic>(query, new { instanceId = InstanceId, AccountId = AccountId });
              }*/
            return productStocks;
        }
        public override Task<ProductStock> Save<ProductStock>(ProductStock data)
        {
            /*Added by Poongodi on 10/08/2017*/
            bool bGstEnabled = _configHelper.AppConfig.IsGstEnabled;
            if (bGstEnabled)
            {
                data["TaxRefNo"] = 1;
            }
            //added by nandhini for transaction
            if (data.WriteExecutionQuery)
            {
                SetInsertMetaData(data, ProductStockTable.Table);
                WriteInsertExecutionQuery(data, ProductStockTable.Table);
                return Task.FromResult(data);
            }
            else
            {
                return Insert(data, ProductStockTable.Table);
            }
        }
        public async Task<List<ProductStock>> getBathcDetails(Product product)
        {
            string query = $@"
            select  max(ps.Id) as Id, 
                    ps.AccountId,
            		ps.InstanceId,
            	    ps.ProductId,		
            		ps.ExpireDate,
            		ps.VAT,
            		ps.TaxType,
            		max(ps.SellingPrice) as SellingPrice,
            		ps.PurchaseBarcode,
            		ps.PackageSize,
            		max(ps.PackagePurchasePrice) as PackagePurchasePrice,
            	    max(ps.PurchasePrice) as purchaseprice,
            		max(ps.CreatedAt) as CreatedAt,
            		max(ps.UpdatedAt) as UpdatedAt,
            		max(ps.CreatedBy) as CreatedBy,
            		max(ps.UpdatedBy) as UpdatedBy,
            		ps.CST,
            		ps.IsMovingStock,
            		ps.ReOrderQty,
            		ps.Status,
            		ps.IsMovingStockExpire,
            		ps.NewOpenedStock,
            		ps.NewStockInvoiceNo,
            		ps.NewStockQty,
            		sum(ps.Stock) as stock,		
            		ps.BatchNo,
                    p.name,
p.Schedule,
                    p.Id as productid
            		   from ProductStock as ps inner join product as p on ps.ProductId = p.Id
                                           where ps.InstanceId = '{product.InstanceId}'
                                          and ps.AccountId='{product.AccountId}'
                                          and ps.Stock > 0 
                                          and ps.ExpireDate > getdate()  and p.Id='{product.Id}'
                                          group by   ps.AccountId,
            		                                 ps.InstanceId,
            	                                     ps.ProductId,	                                   
            		                                 ps.BatchNo,										
            										 ps.ExpireDate,
            										 ps.VAT,
            										 ps.TaxType,
            										 ps.PurchaseBarcode,
            										 ps.PackageSize,
            										 ps.CST,
            										 ps.IsMovingStock,
            										 ps.ReOrderQty,
            										 ps.Status,
            										 ps.IsMovingStockExpire,
            										 ps.NewOpenedStock,
            										 ps.NewStockInvoiceNo,
            										 ps.NewStockQty,
                                                     p.name,
p.Schedule,
                                                     p.Id";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var stockList = new List<ProductStock>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new ProductStock
                    {
                        Id = reader["Id"].ToString(),
                        AccountId = reader["AccountId"].ToString(),
                        InstanceId = reader["InstanceId"].ToString(),
                        ProductId = reader["ProductId"].ToString(),
                        ExpireDate = reader["ExpireDate"] as DateTime? ?? DateTime.MinValue,
                        VAT = reader["VAT"] as decimal? ?? 0,
                        TaxType = reader["TaxType"].ToString(),
                        SellingPrice = reader["SellingPrice"] as decimal? ?? 0,
                        PurchaseBarcode = reader["PurchaseBarcode"].ToString(),
                        PackageSize = reader["PackageSize"] as decimal? ?? 0,
                        PackagePurchasePrice = reader["PackagePurchasePrice"] as decimal? ?? 0,
                        PurchasePrice = reader["purchaseprice"] as decimal? ?? 0,
                        CreatedAt = reader["CreatedAt"] as DateTime? ?? DateTime.MinValue,
                        UpdatedAt = reader["UpdatedAt"] as DateTime? ?? DateTime.MinValue,
                        CreatedBy = reader["CreatedBy"].ToString(),
                        UpdatedBy = reader["UpdatedBy"].ToString(),
                        CST = reader["CST"] as decimal? ?? 0,
                        IsMovingStock = reader["IsMovingStock"] as bool? ?? false,
                        ReOrderQty = reader["ReOrderQty"] as decimal? ?? 0,
                        Status = reader["Status"] as int? ?? 0,
                        IsMovingStockExpire = reader["IsMovingStockExpire"] as bool? ?? false,
                        NewOpenedStock = reader["NewOpenedStock"] as bool? ?? false,
                        NewStockInvoiceNo = reader["NewStockInvoiceNo"].ToString(),
                        NewStockQty = reader["NewStockQty"] as decimal? ?? 0,
                        Stock = reader["stock"] as decimal? ?? 0,
                        BatchNo = reader["BatchNo"].ToString(),
                        Product = {
                            Name = reader["name"].ToString(),
                            Id = reader["productid"].ToString(),
                            Schedule=reader["Schedule"].ToString()
                        }
                    };
                    stockList.Add(item);
                }
            }
            stockList = await GetTempStockDetails(stockList);
            return stockList;
        }
        /// <summary>
        /// Direct qry changed to Stored proc by Arun on 27/09/2017
        /// </summary>
        /// <param name="product"></param>
        /// <param name="showExpDateQty"></param>
        /// <param name="blnQtyType"></param>
        /// <param name="ProductStockId"></param>
        /// <returns></returns>
        public async Task<List<ProductStock>> GetInStockItem(Product product, int showExpDateQty = 0, bool blnQtyType = true, string ProductStockId = "")
        {

            var stockList = new List<ProductStock>();

            string sCondition = "";

            //Dictionary<string, object> parms = new Dictionary<string, object>();

            //parms.Add("Accountid", product.AccountId);
            //parms.Add("InstanceId", product.InstanceId);
            //parms.Add("productId", product.Id);
            //parms.Add("ShowExpiredQty", showExpDateQty);


            //var sQry = await sqldb.ExecuteProcedureAsync<ProductStock>("Usp_Get_StockItemExpiry", parms);
            //stockList = sQry.ToList(); //await List<ProductStock>(sQry);

            if (!string.IsNullOrEmpty(product.Id))
            {
                sCondition = "And  Product.Id ='" + product.Id + "'";
            }
            if (!string.IsNullOrEmpty(ProductStockId))
            {
                sCondition = sCondition + " And  ProductStock.Id ='" + ProductStockId + "'";
            }/*Left join added For Product Instance */
            //BoxNo included by settu on 19-05-2017
            /*HSNcode taken from Product By Poongodi on 12/09/2017*/
            string showExpDateQtyCondition = ""; //Added by Sarubala on 04-10-17
            if (showExpDateQty == 0)
            {
                showExpDateQtyCondition = "cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date)";
            }
            else
            {
                showExpDateQtyCondition = "cast(ProductStock.ExpireDate  as date)<=cast(getdate() as date)";
            }

            string sQry = $@" SELECT 
ProductStock.Id,ProductStock.AccountId,ProductStock.InstanceId,ProductStock.ProductId,ProductStock.VendorId,ProductStock.BatchNo,ProductStock.ExpireDate,
ProductStock.VAT,isnull(ProductStock.Igst,0) as Igst,isnull(ProductStock.Cgst,0) as Cgst,isnull(ProductStock.Sgst,0) as Sgst,isnull(ProductStock.GstTotal,0) as GstTotal,Isnull(Product.HsnCode,'') [HsnCode],ProductStock.TaxType,ProductStock.SellingPrice,ProductStock.MRP,ProductStock.PurchaseBarcode,ProductStock.Stock,isnull(ProductStock.PackageSize,Product.PackageSize) AS PackageSize,ProductStock.PackagePurchasePrice,ProductStock.PurchasePrice,ProductStock.OfflineStatus,ProductStock.CreatedAt,ProductStock.UpdatedAt,ProductStock.CreatedBy,ProductStock.UpdatedBy,ProductStock.CST,ProductStock.IsMovingStock,ProductStock.ReOrderQty,ProductStock.Status,ProductStock.IsMovingStockExpire,ProductStock.NewOpenedStock,ProductStock.NewStockInvoiceNo,ProductStock.NewStockQty,ProductStock.StockImport,ProductStock.Eancode, ProductStock.BarcodeProfileId, Product.Id 
AS [Product.Id],Product.AccountId AS [Product.AccountId],Product.InstanceId AS [Product.InstanceId],Product.Code AS [Product.Code],Product.Name AS 
[Product.Name],Product.Manufacturer AS [Product.Manufacturer],Product.KindName AS [Product.KindName],Product.StrengthName AS [Product.StrengthName],Product.Type AS [Product.Type],
Product.Schedule AS [Product.Schedule],Product.Category AS [Product.Category],Product.GenericName AS [Product.GenericName],Product.CommodityCode AS [Product.CommodityCode],
Product.Packing AS [Product.Packing],Product.OfflineStatus AS [Product.OfflineStatus],Product.CreatedAt AS [Product.CreatedAt],Product.UpdatedAt AS [Product.UpdatedAt],
Product.CreatedBy AS [Product.CreatedBy],Product.UpdatedBy AS [Product.UpdatedBy],Product.PackageSize AS [Product.PackageSize],Product.VAT AS [Product.VAT],
Product.Price AS [Product.Price],Product.Status AS [Product.Status],ProductInstance.RackNo AS [Product.RackNo],ProductInstance.BoxNo AS [Product.BoxNo],Product.ProductMasterID AS [Product.ProductMasterID],
Product.ProductOrgID AS [Product.ProductOrgID],ProductInstance.ReOrderLevel AS [Product.ReOrderLevel],ProductInstance.ReOrderQty AS [Product.ReOrderQty],
isnull(Product.Igst,0) as [Product.Igst],isnull(Product.Cgst,0) as [Product.Cgst],isnull(Product.Sgst,0) as [Product.Sgst],isnull(Product.GstTotal,0) as [Product.GstTotal],
Product.Discount AS [Product.Discount],Vendor.Name AS [Vendor.Name],cast(ISNULL(ProductStock.PackagePurchasePrice,0)/CASE WHEN ProductStock.PackageSize = 0 THEN 1 ELSE ISNULL(ProductStock.PackageSize,1)    END as decimal(18,6)) [PurchasePriceWithoutTax] FROM ProductStock (nolock) INNER JOIN Product (nolock) ON Product.Id = ProductStock.ProductId LEFT JOIN Vendor ON ProductStock.VendorId = Vendor.Id LEFT JOIN ProductInstance ON ProductInstance.ProductId = ProductStock.ProductId
  And ProductInstance.InstanceId = ProductStock.InstanceId  WHERE ProductStock.Stock  >  0 AND (ProductStock.Status  =  1 Or (ProductStock.Status IS NULL))  {sCondition}  AND ProductStock.AccountId  =  '{product.AccountId}' 
  AND ProductStock.InstanceId  =  '{product.InstanceId}' and ProductStock.Stock  >  0 AND " + showExpDateQtyCondition + " ORDER BY ProductStock.ExpireDate asc ";
            var qb = QueryBuilderFactory.GetQueryBuilder(sQry);
            stockList = await List<ProductStock>(qb);
            stockList = await GetTempStockDetails(stockList);

            stockList = stockList.OrderByDescending(item => item.CreatedAt).ToList();

            return stockList;
            /*

            var products = sQry.Select(x =>
            {
                var productstock = new ProductStock
                {

                    ProductId= x.ProductId,
                    VendorId = x.VendorId,
                    BatchNo = x.BatchNo,
                    Stock = x.Stock,
                    ExpireDate = x.ExpireDate,
                    VAT = x.VAT,
                    Igst = x.Igst,
                    Cgst = x.Cgst,
                    Sgst = x.Sgst,
                    GstTotal = x.GstTotal,
                  //  HsnCode = x.HsnCode,
                  //  TaxType = x.TaxType,
                    SellingPrice = x.SellingPrice,
                    MRP = x.MRP,
                  //  PurchaseBarcode = x.PurchaseBarcode,
                    ActualStock = x.Stock,
                    PackageSize = x.PackageSize,
                    PackagePurchasePrice = x.PackagePurchasePrice,
                    PurchasePrice = x.PurchasePrice,
                    OfflineStatus = x.OfflineStatus,
                    CreatedAt = x.CreatedAt,
                    UpdatedAt = x.UpdatedAt,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CST = x.CST,
                    IsMovingStock = x.IsMovingStock,
                //    ReOrderQty = x.ReOrderQty,
                    //Status = x.Status,
                    IsMovingStockExpire = x.IsMovingStockExpire,
                  //  NewOpenedStock = x.NewOpenedStock,
                 //   NewStockInvoiceNo = x.NewStockInvoiceNo,
                   // NewStockQty = x.NewStockQty,
                    StockImport = x.StockImport,
                 //   Eancode = x.Eancode,
                    
                    Product = {Id =x.Product.Id ,
                    AccountId = x.Product.AccountId,
                   // InstanceId = x.Product.InstanceId,
                 //   Code =x.Product.Code,
                    Name = x.Product.Name,
                  //  Manufacturer = x.Product.Manufacturer,
                   // KindName = x.Product.KindName,
                    //StrengthName = x.Product.StrengthName,
                    //Type = x.Product.Type,
                    Schedule=x.Product.Schedule,
                   // Category = x.Product.Category,
                   // GenericName = x.Product.GenericName,
                   // CommodityCode = x.Product.CommodityCode,
                   // Packing = x.Product.Packing,
                    OfflineStatus = x.Product.OfflineStatus,
                    CreatedAt = x.Product.CreatedAt,
                    UpdatedAt = x.Product.UpdatedAt,
                    CreatedBy = x.Product.CreatedBy,
                    UpdatedBy = x.Product.UpdatedBy,
                 //     PackageSize = x.Product.PackageSize,
                 //    VAT = x.Product.VAT,
                 //    Price = x.Product.Price,
                  //   Status = x.Product.Status,
                  //   RackNo = x.Product.RackNo,
                  //   BoxNo = x.Product.BoxNo,
                  //   ProductMasterID = x.Product.ProductMasterID,

                      ProductOrgID = x.Product.ProductOrgID,
                  //   ReOrderLevel = x.Product.ReOrderLevel,
                 //    ReOrderQty = x.Product.ReOrderQty,
                    Igst = x.Product.Igst,
                    Cgst = x.Product.Cgst,
                    Sgst = x.Product.Sgst,
                    GstTotal = x.Product.GstTotal,
                 //    Discount = x.Product.Discount,
                    
                    },
                    Vendor = {Name = x.Vendor.Name}
                    
                };
                return productstock;
            });
         
                return products.ToList(); */
        }


        //        public async Task<List<ProductStock>> GetInTransferStockItem(Product product, int showExpDateQty, bool blnQtyType = true, string ProductStockId = "")
        //        {
        //            var stockList = new List<ProductStock>();
        //            //       string query = $@"
        //            //       select  max(ps.Id) as Id, 
        //            //               ps.AccountId,
        //            //       		ps.InstanceId,
        //            //       	    ps.ProductId,		
        //            //       		ps.ExpireDate,
        //            //       		ps.VAT,
        //            //       		ps.TaxType,
        //            //       		max(ps.SellingPrice) as SellingPrice,
        //            //       		ps.PurchaseBarcode,
        //            //       		ps.PackageSize,
        //            //       		max(ps.PackagePurchasePrice) as PackagePurchasePrice,
        //            //       	    max(ps.PurchasePrice) as purchaseprice,
        //            //       		max(ps.CreatedAt) as CreatedAt,
        //            //       		max(ps.UpdatedAt) as UpdatedAt,
        //            //       		max(ps.CreatedBy) as CreatedBy,
        //            //       		max(ps.UpdatedBy) as UpdatedBy,
        //            //       		ps.CST,
        //            //       		ps.IsMovingStock,
        //            //       		ps.ReOrderQty,
        //            //       		ps.Status,
        //            //       		ps.IsMovingStockExpire,
        //            //       		ps.NewOpenedStock,
        //            //       		ps.NewStockInvoiceNo,
        //            //       		ps.NewStockQty,
        //            //       		sum(ps.Stock) as stock,		
        //            //       		ps.BatchNo,
        //            //               p.name,
        //            //               p.Schedule,
        //            //               p.Id as productid,
        //            //               v.name as vendorname,
        //            //datediff(day, ps.CreatedAt, getDate()) as age                
        //            //       		   from ProductStock as ps inner join product as p on ps.ProductId = p.Id
        //            //                                          left join Vendor as v on v.Id = ps.VendorId
        //            //                                      where ps.InstanceId = '{product.InstanceId}'
        //            //                                     and ps.AccountId='{product.AccountId}'
        //            //                                     and ps.Stock > 0 
        //            //                                     and ps.ExpireDate > getdate()  and p.Id='{product.Id}'
        //            //                                     group by   ps.AccountId,
        //            //       		                                 ps.InstanceId,
        //            //       	                                     ps.ProductId,	                                   
        //            //       		                                 ps.BatchNo,										
        //            //       										 ps.ExpireDate,
        //            //       										 ps.VAT,
        //            //       										 ps.TaxType,
        //            //       										 ps.PurchaseBarcode,
        //            //       										 ps.PackageSize,
        //            //       										 ps.CST,
        //            //       										 ps.IsMovingStock,
        //            //       										 ps.ReOrderQty,
        //            //       										 ps.Status,
        //            //       										 ps.IsMovingStockExpire,
        //            //       										 ps.NewOpenedStock,
        //            //       										 ps.NewStockInvoiceNo,
        //            //       										 ps.NewStockQty,
        //            //                                                p.name,
        //            //                                                p.Schedule,
        //            //                                                p.Id,
        //            //                                                v.name,
        //            //                                                ps.CreatedAt";
        //            //       var qb = QueryBuilderFactory.GetQueryBuilder(query);
        //            //       var stockList = new List<ProductStock>();
        //            //       using (var reader = await QueryExecuter.QueryAsyc(qb))
        //            //       {
        //            //           while (reader.Read())
        //            //           {
        //            //               var item = new ProductStock
        //            //               {
        //            //                   Id = reader["Id"].ToString(),
        //            //                   AccountId = reader["AccountId"].ToString(),
        //            //                   InstanceId = reader["InstanceId"].ToString(),
        //            //                   ProductId = reader["ProductId"].ToString(),
        //            //                   ExpireDate = reader["ExpireDate"] as DateTime? ?? DateTime.MinValue,
        //            //                   VAT = reader["VAT"] as decimal? ?? 0,
        //            //                   TaxType = reader["TaxType"].ToString(),
        //            //                   SellingPrice = reader["SellingPrice"] as decimal? ?? 0,
        //            //                   PurchaseBarcode = reader["PurchaseBarcode"].ToString(),
        //            //                   PackageSize = reader["PackageSize"] as decimal? ?? 0,
        //            //                   PackagePurchasePrice = reader["PackagePurchasePrice"] as decimal? ?? 0,
        //            //                   PurchasePrice = reader["purchaseprice"] as decimal? ?? 0,
        //            //                   CreatedAt = reader["CreatedAt"] as DateTime? ?? DateTime.MinValue,
        //            //                   UpdatedAt = reader["UpdatedAt"] as DateTime? ?? DateTime.MinValue,
        //            //                   CreatedBy = reader["CreatedBy"].ToString(),
        //            //                   UpdatedBy = reader["UpdatedBy"].ToString(),
        //            //                   CST = reader["CST"] as decimal? ?? 0,
        //            //                   IsMovingStock = reader["IsMovingStock"] as bool? ?? false,
        //            //                   ReOrderQty = reader["ReOrderQty"] as decimal? ?? 0,
        //            //                   Status = reader["Status"] as int? ?? 0,
        //            //                   IsMovingStockExpire = reader["IsMovingStockExpire"] as bool? ?? false,
        //            //                   NewOpenedStock = reader["NewOpenedStock"] as bool? ?? false,
        //            //                   NewStockInvoiceNo = reader["NewStockInvoiceNo"].ToString(),
        //            //                   NewStockQty = reader["NewStockQty"] as decimal? ?? 0,
        //            //                   Stock = reader["stock"] as decimal? ?? 0,
        //            //                   BatchNo = reader["BatchNo"].ToString(),
        //            //                   Product = {
        //            //                       Name = reader["name"].ToString(),
        //            //                       Id = reader["productid"].ToString(),
        //            //                       Schedule=reader["Schedule"].ToString()
        //            //                   },
        //            //                   Vendor = { Name = reader["vendorname"].ToString() },
        //            //                   Age = reader["age"] as int? ?? 0
        //            //               };
        //            //               stockList.Add(item);
        //            //           }
        //            //       }
        //            //       stockList = await GetTempStockDetails(stockList);
        //            //       return stockList;

        //            /* var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select)
        //             .AddColumn(ProductStockTable.Table.ColumnList.ToArray());
        //             qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn)
        //                .AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
        //             qb.JoinBuilder.LeftJoin(VendorTable.Table, ProductStockTable.VendorIdColumn, VendorTable.IdColumn);
        //             qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn);
        //             qb.JoinBuilder.LeftJoins(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductStockTable.ProductIdColumn, ProductInstanceTable.InstanceIdColumn, ProductStockTable.InstanceIdColumn);
        //             qb.JoinBuilder.AddJoinColumn(ProductInstanceTable.Table, ProductInstanceTable.RackNoColumn);
        //             qb.ConditionBuilder.And(ProductStockTable.StockColumn, CriteriaEquation.Greater).And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Greater);
        //             qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, 0);
        //             var statusCondition = new CustomCriteria(ProductStockTable.StatusColumn, CriteriaCondition.IsNull);
        //             string condition1 = "1";
        //             var cc1 = new CriteriaColumn(ProductStockTable.StatusColumn, condition1, CriteriaEquation.Equal);
        //             var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);
        //             qb.ConditionBuilder.And(col1);
        //             if (!string.IsNullOrEmpty(product.Id))
        //             {
        //                 qb.ConditionBuilder.And(ProductTable.IdColumn);
        //                 qb.Parameters.Add(ProductTable.IdColumn.ColumnName, product.Id);
        //             }
        //             qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, product.AccountId);
        //             qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, product.InstanceId);
        //             qb.SelectBuilder.SortBy(ProductStockTable.ExpireDateColumn);
        //             */
        //            string sCondition = "";
        //            if (!string.IsNullOrEmpty(product.Id))
        //            {
        //                sCondition = "And  Product.Id ='" + product.Id + "'";
        //            }
        //            if (!string.IsNullOrEmpty(ProductStockId))
        //            {
        //                sCondition = sCondition + " And  ProductStock.Id ='" + ProductStockId + "'";
        //            }/*Left join added For Product Instance */
        //            //BoxNo included by settu on 19-05-2017
        //            /*HSNcode taken from Product By Poongodi on 12/09/2017*/
        //            string sQry = "";
        //            if (showExpDateQty != 1)
        //            {
        //                sCondition = sCondition + " And  ProductStock.ExpireDate  >'" + CustomDateTime.IST.Date.ToFormat() + "'";
        //                //AND ProductStock.ExpireDate  > '{CustomDateTime.IST.Date.ToFormat()}'
        //            }


        //            sQry = $@" SELECT 
        //ProductStock.Id,ProductStock.AccountId,ProductStock.InstanceId,ProductStock.ProductId,ProductStock.VendorId,ProductStock.BatchNo,ProductStock.ExpireDate,
        //ProductStock.VAT,isnull(ProductStock.Igst,0) as Igst,isnull(ProductStock.Cgst,0) as Cgst,isnull(ProductStock.Sgst,0) as Sgst,isnull(ProductStock.GstTotal,0) as GstTotal,Isnull(Product.HsnCode,'') [HsnCode],ProductStock.TaxType,ProductStock.SellingPrice,ProductStock.MRP,ProductStock.PurchaseBarcode,ProductStock.Stock,ISNULL(ProductStock.PackageSize, 1) AS PackageSize,ProductStock.PackagePurchasePrice,ProductStock.PurchasePrice,ProductStock.OfflineStatus,ProductStock.CreatedAt,ProductStock.UpdatedAt,ProductStock.CreatedBy,ProductStock.UpdatedBy,ProductStock.CST,ProductStock.IsMovingStock,ProductStock.ReOrderQty,ProductStock.Status,ProductStock.IsMovingStockExpire,ProductStock.NewOpenedStock,ProductStock.NewStockInvoiceNo,ProductStock.NewStockQty,ProductStock.StockImport,ProductStock.Eancode,Product.Id 
        //AS [Product.Id],Product.AccountId AS [Product.AccountId],Product.InstanceId AS [Product.InstanceId],Product.Code AS [Product.Code],Product.Name AS 
        //[Product.Name],Product.Manufacturer AS [Product.Manufacturer],Product.KindName AS [Product.KindName],Product.StrengthName AS [Product.StrengthName],Product.Type AS [Product.Type],
        //Product.Schedule AS [Product.Schedule],Product.Category AS [Product.Category],Product.GenericName AS [Product.GenericName],Product.CommodityCode AS [Product.CommodityCode],
        //Product.Packing AS [Product.Packing],Product.OfflineStatus AS [Product.OfflineStatus],Product.CreatedAt AS [Product.CreatedAt],Product.UpdatedAt AS [Product.UpdatedAt],
        //Product.CreatedBy AS [Product.CreatedBy],Product.UpdatedBy AS [Product.UpdatedBy],Product.PackageSize AS [Product.PackageSize],Product.VAT AS [Product.VAT],
        //Product.Price AS [Product.Price],Product.Status AS [Product.Status],ProductInstance.RackNo AS [Product.RackNo],ProductInstance.BoxNo AS [Product.BoxNo],Product.ProductMasterID AS [Product.ProductMasterID],
        //Product.ProductOrgID AS [Product.ProductOrgID],ProductInstance.ReOrderLevel AS [Product.ReOrderLevel],ProductInstance.ReOrderQty AS [Product.ReOrderQty],
        //isnull(Product.Igst,0) as [Product.Igst],isnull(Product.Cgst,0) as [Product.Cgst],isnull(Product.Sgst,0) as [Product.Sgst],isnull(Product.GstTotal,0) as [Product.GstTotal],
        //Product.Discount AS [Product.Discount],Vendor.Name AS [Vendor.Name] FROM ProductStock (nolock) INNER JOIN Product (nolock) ON Product.Id = ProductStock.ProductId
        // LEFT JOIN Vendor ON ProductStock.VendorId = Vendor.Id LEFT JOIN ProductInstance ON ProductInstance.ProductId = ProductStock.ProductId
        //  And ProductInstance.InstanceId = ProductStock.InstanceId  WHERE ProductStock.Stock  >  0 
        //  AND (ProductStock.Status  =  1 Or (ProductStock.Status IS NULL))  {sCondition}  AND ProductStock.AccountId  =  '{product.AccountId}' 
        //  AND ProductStock.InstanceId  =  '{product.InstanceId}' ORDER BY ProductStock.ExpireDate asc ";
        //            var qb = QueryBuilderFactory.GetQueryBuilder(sQry);
        //            stockList = await List<ProductStock>(qb);
        //            stockList = await GetTempStockDetails(stockList);

        //            stockList = stockList.OrderByDescending(item => item.CreatedAt).ToList();

        //            return stockList;
        //        }



        //Added by bala for Branch wise stock display on 01/June/2017
        public async Task<BranchWiseStock> GetBranchWiseStock(ProductSearch product, bool? bLowPrice = false)
        {
            //var stockList = new List<ProductStock>();
            //Direct Qry removed and Converted to SP by Poongodi on 06/06/2017

            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", product.AccountId);
            parms.Add("ProductId", product.Id);

            var result = await sqldb.ExecuteProcedureAsync<ProductStock>("usp_GetBranchwiseStock", parms);
            //stockList = await GetTempStockDetails(result);

            var li1 = result.GroupBy(p => p.InstanceName).Select(
                                                  pd1 => new
                                                  {
                                                      InstanceName = pd1.Select(s => s.InstanceName),
                                                      Phone = pd1.Select(s => s.Phone),
                                                      SumStock = pd1.Sum(s => s.Stock),
                                                      Qty = pd1.Select(s => s.Stock),
                                                      Expiry = pd1.Select(s => s.ExpireDate),
                                                      MRP = pd1.Select(s => s.MRP),
                                                      BranchNo = pd1.Select(s => s.BatchNo),

                                                  });
            var lstSaleItem = new List<SalesItem>();

            foreach (var l1 in li1)
            {
                var saleItem = new SalesItem();
                var lstPdtStock = new List<ProductStock>();

                saleItem.ProductStock.VendorPurchase = null;
                lstPdtStock.Select(p =>
                {
                    var t = new ProductStock();
                    t.VendorPurchase = null;
                    return t;
                });
                var n = 0;

                var lstQty = l1.Qty.ToList();
                var lstBatch = l1.BranchNo.ToList();
                var lstMRP = l1.MRP.ToList();
                var lstExpiry = l1.Expiry.ToList();

                saleItem.InstanceId = l1.InstanceName.First().ToString();
                saleItem.Quantity = l1.SumStock;
                saleItem.Sales.Mobile = l1.Phone.First().ToString();
                for (int i = 0; i < l1.InstanceName.ToList().Count; i++)
                {
                    var ProductStock = new ProductStock
                    {
                        BatchNo = lstBatch[i].ToString(),
                        ExpireDate = lstExpiry[i].Value,
                        MRP = lstMRP[i].Value,
                        Stock = lstQty[i].Value,
                        VendorPurchase = null
                    };
                    lstPdtStock.Add(ProductStock);
                }
                n++;

                saleItem.lstProductStock = lstPdtStock;
                lstSaleItem.Add(saleItem);

            }

            BranchWiseStock obj = new BranchWiseStock();
            obj.ProductStockList = lstSaleItem;

            obj.VendorPurchaseItemList = await _productDataAccess.getPreviousPurchaseAccountWise(product.AccountId, product.InstanceId, product.Name, bLowPrice);

            return obj;
        }

        public async Task<List<ProductStock>> GetAllProductBatch(Product product)
        {
            var stockList = new List<ProductStock>();

            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select)
                .AddColumn(ProductStockTable.Table.ColumnList.ToArray());
            //qb.JoinBuilder.LeftJoin(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn)
            qb.JoinBuilder.LeftJoins2(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn, ProductStockTable.InstanceIdColumn, product.InstanceId)
                .AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            //.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());

            if (!string.IsNullOrEmpty(product.Id))
            {
                qb.ConditionBuilder.And(ProductTable.IdColumn);
                qb.Parameters.Add(ProductTable.IdColumn.ColumnName, product.Id);
            }
            qb.SelectBuilder.SortBy(ProductStockTable.ExpireDateColumn);
            stockList = await List<ProductStock>(qb);

            var qbProduct = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select)
                .AddColumn(ProductTable.Table.ColumnList.ToArray());
            if (!string.IsNullOrEmpty(product.Id))
            {
                qbProduct.ConditionBuilder.And(ProductTable.IdColumn, product.Id);
            }
            qbProduct.ConditionBuilder.And(ProductTable.AccountIdColumn, product.AccountId);
            var products = await List<Product>(qbProduct);

            if (stockList.Count() > 0 && products.Count() > 0)
            {
                if (String.IsNullOrEmpty(stockList.First().Id))
                {
                    stockList.First().HsnCode = products.First().HsnCode;
                    stockList.First().GstTotal = products.First().GstTotal;
                    stockList.First().Igst = products.First().Igst;
                    stockList.First().Cgst = products.First().Cgst;
                    stockList.First().Sgst = products.First().Sgst;
                }

                if (!String.IsNullOrEmpty(stockList.First().Id))
                    stockList = await GetTempStockDetails(stockList);
            }
            return stockList;
        }
        public async Task<List<ProductStock>> GetProductBatchForReturn(Product product)
        {
            var stockList = new List<ProductStock>();
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select)
            .AddColumn(ProductStockTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn)
                .AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Greater);
            var statusCondition = new CustomCriteria(ProductStockTable.StatusColumn, CriteriaCondition.IsNull);
            string condition1 = "1";
            var cc1 = new CriteriaColumn(ProductStockTable.StatusColumn, condition1, CriteriaEquation.Equal);
            var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);
            if (!string.IsNullOrEmpty(product.Id))
            {
                qb.ConditionBuilder.And(ProductTable.IdColumn);
                qb.Parameters.Add(ProductTable.IdColumn.ColumnName, product.Id);
            }
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, product.AccountId);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, product.InstanceId);
            qb.SelectBuilder.SortBy(ProductStockTable.ExpireDateColumn);
            stockList = await List<ProductStock>(qb);
            stockList = await GetTempStockDetails(stockList);
            return stockList;
        }
        public async Task<List<ProductStock>> GetActiveProductBatch(Product product)
        {
            var stockList = new List<ProductStock>();
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select)
            .AddColumn(ProductStockTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn)
                .AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            var statusCondition = new CustomCriteria(ProductStockTable.StatusColumn, CriteriaCondition.IsNull);
            string condition1 = "1";
            var cc1 = new CriteriaColumn(ProductStockTable.StatusColumn, condition1, CriteriaEquation.Equal);
            var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);
            if (!string.IsNullOrEmpty(product.Id))
            {
                qb.ConditionBuilder.And(ProductTable.IdColumn);
                qb.Parameters.Add(ProductTable.IdColumn.ColumnName, product.Id);
            }
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, product.AccountId);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, product.InstanceId);
            qb.SelectBuilder.SortBy(ProductStockTable.ExpireDateColumn);
            stockList = await List<ProductStock>(qb);
            stockList = await GetTempStockDetails(stockList);
            return stockList;
        }

        private async Task<List<ProductStock>> GetTempStockDetails(List<ProductStock> ps)
        {
            for (var x = 0; x < ps.Count(); x++)
            {
                TimeSpan difference = DateTime.Now - ps[x].CreatedAt;
                ps[x].Age = Convert.ToInt32(difference.TotalDays);
                var qb = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Select);
                qb.ConditionBuilder.And(TempVendorPurchaseItemTable.ProductStockIdColumn, ps[x].Id);
                qb.ConditionBuilder.And(TempVendorPurchaseItemTable.isActiveColumn, 1);
                qb.ConditionBuilder.And(TempVendorPurchaseItemTable.AccountIdColumn, ps[x].AccountId);
                qb.ConditionBuilder.And(TempVendorPurchaseItemTable.InstanceIdColumn, ps[x].InstanceId);
                qb.AddColumn(TempVendorPurchaseItemTable.IdColumn, TempVendorPurchaseItemTable.QuantityColumn);
                using (var reader = await QueryExecuter.QueryAsyc(qb))
                {
                    if (reader.Read())
                    {
                        TempVendorPurchaseItem temp = new TempVendorPurchaseItem();
                        temp.Id = reader[TempVendorPurchaseItemTable.IdColumn.ColumnName].ToString();
                        temp.Quantity = reader[TempVendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0;
                        ps[x].TempStock = temp;
                    }
                }
            }
            return ps;
        }


        public async Task<Product> GetProductDetails(Product product)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.TypeColumn, ProductTable.ScheduleColumn,
                ProductTable.GenericNameColumn, ProductTable.ManufacturerColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);
            qb.ConditionBuilder.And(ProductTable.IdColumn, product.Id);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, product.AccountId);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, product.InstanceId);
            var product1 = (await List<Product>(qb)).FirstOrDefault();
            if ((product1 != null) && (!string.IsNullOrEmpty(product1.GenericName)))
            {
                var qb1 = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
                qb1.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn).
                    AddJoinColumn(ProductStockTable.Table, ProductStockTable.StockColumn);
                qb1.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.ManufacturerColumn);
                qb1.ConditionBuilder.And(ProductStockTable.AccountIdColumn, product.AccountId);
                qb1.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, product.InstanceId);
                qb1.ConditionBuilder.And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Greater);
                qb1.ConditionBuilder.And(ProductTable.GenericNameColumn, product1.GenericName);
                qb1.SelectBuilder.SortBy(ProductTable.NameColumn);
                var list1 = new List<ProductStock>();
                using (var reader = await QueryExecuter.QueryAsyc(qb1))
                {
                    while (reader.Read())
                    {
                        var productstock = new ProductStock();
                        productstock.Product.Id = reader[ProductTable.IdColumn.ColumnName].ToString();
                        productstock.Product.Name = reader[ProductTable.NameColumn.ColumnName].ToString();
                        productstock.Product.Manufacturer = reader[ProductTable.ManufacturerColumn.ColumnName].ToString();
                        productstock.Stock = reader[ProductStockTable.StockColumn.FullColumnName] as decimal? ?? 0;
                        list1.Add(productstock);
                    }
                }
                list1.RemoveAll(x => x.Product.Name == product1.Name);
                var li1 = list1.GroupBy(p => p.Product.Id).Select(
                                                   pd1 => new
                                                   {
                                                       Id = pd1.Key,
                                                       Name = pd1.First().Product.Name,
                                                       Manufacturer = pd1.First().Product.Manufacturer,
                                                       Stock = pd1.Sum(s => s.Stock),
                                                   }).Where(item => item.Stock > -1);

                var list2 = new List<ProductStock>();
                foreach (var l1 in li1)
                {
                    var productstock1 = new ProductStock();
                    productstock1.Product.Id = l1.Id;
                    productstock1.Product.Name = l1.Name;
                    productstock1.Product.Manufacturer = l1.Manufacturer;
                    productstock1.Stock = l1.Stock;
                    list2.Add(productstock1);
                }
                product1.ProductStock = list2;
                return product1;
            }
            else
            {
                return product;
            }
        }
        public async Task<List<ProductStock>> GetAllProductsinProductStock(string productName, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn)
               .AddJoinColumn(ProductTable.Table, ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.StatusColumn, ProductTable.ScheduleColumn);
            qb.SelectBuilder.GroupBy(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.StatusColumn, ProductTable.ScheduleColumn);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, productName, CriteriaEquation.Like, CriteriaLike.Begin);
            }
            else
                qb.ConditionBuilder.And(ProductTable.NameColumn, $"{productName}%", CriteriaEquation.Like);
            if (!string.IsNullOrEmpty(instanceId))
            {
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId);
                var instanceIdIsNull = new CustomCriteria(ProductStockTable.InstanceIdColumn, CriteriaCondition.IsNull);
                qb.ConditionBuilder.Or(instanceIdIsNull);
            }
            if (productName.Length <= 2)
            {
                qb.SelectBuilder.SetTop(20).SortBy(ProductTable.NameColumn);
            }
            else
            {
                qb.SelectBuilder.SortBy(ProductTable.NameColumn);
            }
            qb.SelectBuilder.MakeDistinct = true;
            var result = await List<ProductStock>(qb);
            return result;
        }
        /// <summary>
        /// Method Created by Poongodi  on 09/05/2017 to performance optimize  of the product 
        /// </summary>
        /// <param name="productName"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public async Task<List<ProductStock>> GetInStockItemList(string accountId, string instanceId, string productName, int showExpDateQty)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("InstanceId", instanceId);
            parms.Add("ProductName", productName);
            parms.Add("Accountid", accountId); //Added by Poongodi on 21/07/2017

            //if (accountId == "2d5abd54-89a9-4b31-aa76-8ec70b9125dd") //Local hcuepharmacy1 id
            if (accountId == "67067991-0e68-4779-825e-8e1d724cd68b") //For Mediplus pharmacy
            {
                parms.Add("IncludeZeroStock", true);
            }
            else
            {
                parms.Add("IncludeZeroStock", false);
            }


            parms.Add("ShowExpiredQty", showExpDateQty);/*Parameter added by Arun on 28/09/2017*/

            string procName = string.Empty;
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetStock_Product", parms);
            var product = result.Select(x =>
            {
                var productstock = new ProductStock
                {
                    AccountId = accountId,
                    ProductId = x.ProductId,
                    Stock = x.Stock,
                    Product = {Name =x.ProductName ,
                    Id =x.ProductId,
                    Code =x.ProductCode, //Added by Poongodi on 21/07/2017
                    Schedule=x.ProductSchedule}

                };
                if (productstock.AccountId == "67067991-0e68-4779-825e-8e1d724cd68b" || productstock.AccountId == "ae13cb7f-991b-42d6-8dec-8de153e4526a" || productstock.AccountId == "63ED272D-1122-4E04-AFE8-C5A0F13B4F0E") //For Mediplus pharmacy
                {
                    productstock.Product.Code = productstock.Product.Code + " -    ";
                }
                else
                {
                    productstock.Product.Code = "";
                }

                return productstock;
            });
            return product.ToList();

        }
        /// <summary>
        /// Get Product List for sales page 
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="instanceId"></param>
        /// <param name="productName"></param>
        /// <returns></returns>
        /// Sepearate method created by Poongodi on 26/07/2017
        public async Task<List<ProductStock>> GetInStockProductForSales(string accountId, string instanceId, string productName)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("InstanceId", instanceId);
            parms.Add("ProductName", productName);
            parms.Add("Accountid", accountId);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_ProductForSales", parms);
            var product = result.Select(x =>
            {
                var productstock = new ProductStock
                {
                    AccountId = accountId,
                    ProductId = x.ProductId,
                    Stock = x.Stock,
                    Product = {Name =x.ProductName ,
                    Id =x.ProductId,
                    Code =x.ProductCode,
                    Schedule=x.ProductSchedule}

                };
                if (productstock.AccountId == "67067991-0e68-4779-825e-8e1d724cd68b" || productstock.AccountId == "ae13cb7f-991b-42d6-8dec-8de153e4526a" || productstock.AccountId == "63ED272D-1122-4E04-AFE8-C5A0F13B4F0E") //For Mediplus pharmacy
                {
                    productstock.Product.Code = productstock.Product.Code + " -    ";
                }
                else
                {
                    productstock.Product.Code = "";
                }

                return productstock;
            });
            return product.ToList();

        }
        /// <summary>
        /// Method Rename by Poongodi  on 09/05/2017  to performance optimize  of the product 
        /// </summary>
        /// <param name="productName"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public async Task<List<ProductStock>> GetInStockItemList_Old(string productName, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn)
               .AddJoinColumn(ProductTable.Table, ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.StatusColumn, ProductTable.ScheduleColumn);
            qb.SelectBuilder.GroupBy(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.StatusColumn, ProductTable.ScheduleColumn);
            qb.AddColumn(DbColumn.SumColumn("Stock", ProductStockTable.StockColumn));
            qb.ConditionBuilder.And(ProductStockTable.StockColumn, 0, CriteriaEquation.Greater).And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Greater);
            var statusCondition = new CustomCriteria(ProductStockTable.StatusColumn, CriteriaCondition.IsNull);
            string condition1 = "1";
            var cc1 = new CriteriaColumn(ProductStockTable.StatusColumn, condition1, CriteriaEquation.Equal);
            var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);
            //var statusIsNull = new CustomCriteria(ProductStockTable.StatusColumn, CriteriaCondition.IsNull);
            //qb.ConditionBuilder.And(statusIsNull);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                var statusCondition3 = new CriteriaColumn(ProductTable.NameColumn, "'" + productName.Replace("'", "''") + "%'", CriteriaEquation.Like);
                var cc3 = new CriteriaColumn(ProductTable.IdColumn, "'" + productName.Replace("'", "''") + "'", CriteriaEquation.Equal);
                var col3 = new CustomCriteria(cc3, statusCondition3, CriteriaCondition.Or);
                qb.ConditionBuilder.And(col3);
            }
            else
                qb.ConditionBuilder.And(ProductTable.NameColumn, $"{productName}%", CriteriaEquation.Like);
            var statusCondition2 = new CustomCriteria(ProductTable.StatusColumn, CriteriaCondition.IsNull);
            string condition2 = "1";
            var cc2 = new CriteriaColumn(ProductTable.StatusColumn, condition2, CriteriaEquation.Equal);
            var col2 = new CustomCriteria(cc2, statusCondition2, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col2);
            if (!string.IsNullOrEmpty(instanceId))
            {
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId);
                var instanceIdIsNull = new CustomCriteria(ProductStockTable.InstanceIdColumn, CriteriaCondition.IsNull);
                qb.ConditionBuilder.Or(instanceIdIsNull);
            }
            //if (productName.Length <= 2)
            //{
            //    qb.SelectBuilder.SetTop(20).SortBy(ProductTable.NameColumn);
            //}
            //else
            //{
            //    qb.SelectBuilder.SortBy(ProductTable.NameColumn);
            //}
            qb.SelectBuilder.SetTop(10).SortBy(ProductTable.NameColumn);
            qb.SelectBuilder.MakeDistinct = true;
            var result = await List<ProductStock>(qb);
            return result;
        }
        public async Task<List<ProductStock>> GetInAllStockItemList(string productName, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn)
               .AddJoinColumn(ProductTable.Table, ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.StatusColumn, ProductTable.ScheduleColumn);
            qb.SelectBuilder.GroupBy(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.StatusColumn, ProductTable.ScheduleColumn);
            qb.AddColumn(DbColumn.SumColumn("Stock", ProductStockTable.StockColumn));
            var statusCondition = new CustomCriteria(ProductStockTable.StatusColumn, CriteriaCondition.IsNull);
            string condition1 = "1";
            var cc1 = new CriteriaColumn(ProductStockTable.StatusColumn, condition1, CriteriaEquation.Equal);
            var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                var statusCondition3 = new CriteriaColumn(ProductTable.NameColumn, "'" + productName.Replace("'", "''") + "%'", CriteriaEquation.Like);
                var cc3 = new CriteriaColumn(ProductTable.IdColumn, "'" + productName.Replace("'", "''") + "'", CriteriaEquation.Equal);
                var col3 = new CustomCriteria(cc3, statusCondition3, CriteriaCondition.Or);
                qb.ConditionBuilder.And(col3);
            }
            else
                qb.ConditionBuilder.And(ProductTable.NameColumn, $"{productName}%", CriteriaEquation.Like);
            var statusCondition2 = new CustomCriteria(ProductTable.StatusColumn, CriteriaCondition.IsNull);
            string condition2 = "1";
            var cc2 = new CriteriaColumn(ProductTable.StatusColumn, condition2, CriteriaEquation.Equal);
            var col2 = new CustomCriteria(cc2, statusCondition2, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col2);
            if (!string.IsNullOrEmpty(instanceId))
            {
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId);
                var instanceIdIsNull = new CustomCriteria(ProductStockTable.InstanceIdColumn, CriteriaCondition.IsNull);
                qb.ConditionBuilder.Or(instanceIdIsNull);
            }
            qb.SelectBuilder.SetTop(10).SortBy(ProductTable.NameColumn);
            qb.SelectBuilder.MakeDistinct = true;
            var result = await List<ProductStock>(qb);
            return result;
        }

        //added for return in sales page

        public async Task<List<ProductStock>> GetInAllStockItemListForReturn(string productName, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn)
               .AddJoinColumn(ProductTable.Table, ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.StatusColumn, ProductTable.ScheduleColumn);
            qb.SelectBuilder.GroupBy(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.StatusColumn, ProductTable.ScheduleColumn);
            qb.AddColumn(DbColumn.SumColumn("Stock", ProductStockTable.StockColumn));
            var statusCondition = new CustomCriteria(ProductStockTable.StatusColumn, CriteriaCondition.IsNull);
            string condition1 = "1";
            var cc1 = new CriteriaColumn(ProductStockTable.StatusColumn, condition1, CriteriaEquation.Equal);
            var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                var statusCondition3 = new CriteriaColumn(ProductTable.NameColumn, "'" + productName.Replace("'", "''") + "%'", CriteriaEquation.Like);
                var cc3 = new CriteriaColumn(ProductTable.IdColumn, "'" + productName.Replace("'", "''") + "'", CriteriaEquation.Equal);
                var col3 = new CustomCriteria(cc3, statusCondition3, CriteriaCondition.Or);
                qb.ConditionBuilder.And(col3);
            }
            else
                qb.ConditionBuilder.And(ProductTable.NameColumn, $"{productName}%", CriteriaEquation.Like);
            var statusCondition2 = new CustomCriteria(ProductTable.StatusColumn, CriteriaCondition.IsNull);
            string condition2 = "1";
            var cc2 = new CriteriaColumn(ProductTable.StatusColumn, condition2, CriteriaEquation.Equal);
            var col2 = new CustomCriteria(cc2, statusCondition2, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col2);

            // Added Gavaskar Product IsHidden 25-05-2017
            var IsHiddenCondition = new CustomCriteria(ProductTable.IsHiddenColumn, CriteriaCondition.IsNull);
            string IsHidden = "0";
            var IsHiddencc = new CriteriaColumn(ProductTable.IsHiddenColumn, IsHidden, CriteriaEquation.Equal);
            var IsHiddencol = new CustomCriteria(IsHiddencc, IsHiddenCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(IsHiddencol);

            qb.ConditionBuilder.And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Greater);
            if (!string.IsNullOrEmpty(instanceId))
            {
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId);
                var instanceIdIsNull = new CustomCriteria(ProductStockTable.InstanceIdColumn, CriteriaCondition.IsNull);
                qb.ConditionBuilder.Or(instanceIdIsNull);
            }
            qb.SelectBuilder.SetTop(10).SortBy(ProductTable.NameColumn);
            qb.SelectBuilder.MakeDistinct = true;
            var result = await List<ProductStock>(qb);
            return result;
        }
        //
        public Task<List<ProductStock>> GetAllStockItemList(string productName, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn)
               .AddJoinColumn(ProductTable.Table, ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.StatusColumn);
            qb.ConditionBuilder.And(ProductStockTable.StockColumn, 0, CriteriaEquation.Greater).And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Greater);
            if (_configHelper.AppConfig.IsSqlServer)
            {
                qb.ConditionBuilder.And(ProductTable.NameColumn, productName, CriteriaEquation.Like, CriteriaLike.Begin);
            }
            else
                qb.ConditionBuilder.And(ProductTable.NameColumn, $"{productName}%", CriteriaEquation.Like);
            if (!string.IsNullOrEmpty(instanceId))
            {
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId);
                var instanceIdIsNull = new CustomCriteria(ProductStockTable.InstanceIdColumn, CriteriaCondition.IsNull);
                qb.ConditionBuilder.Or(instanceIdIsNull);
            }
            qb.SelectBuilder.MakeDistinct = true;
            return List<ProductStock>(qb);
        }
        public async Task<List<ProductStockResult>> GetProductStockByExternalID(string externalID, string searchText)
        {
            //string query =
            //    $@" SELECT  p.name, i.ExternalId, ps.BatchNo, 
            //                ps.ExpireDate, ps.Stock, p.Type, 
            //                p.Category, p.Packing, ps.SellingPrice,
            //                ISNULL(p.GenericName,''), ISNULL(p.Kindname,''), ISNULL(p.Strengthname,'') 
            //        FROM    ProductStock ps 
            //        INNER JOIN product p on ps.ProductId = p.Id 
            //        INNER JOIN Instance i ON ps.InstanceId = i.Id
            //        WHERE i.ExternalId = {externalID} AND p.Name Like '{searchText}%'";
            //string query = $@" SELECT   p.name As Medicine, 
            //       ISNULL(p.GenericName,'') As GeneralName,
            //       ISNULL(p.Strengthname,'') As Strength,
            //       SUBSTRING(ISNULL(p.Category,''),1,3) As MedicineType, 
            //       ISNULL(p.Manufacturer,'') As Manufacturer,
            //       ISNULL(p.Packing,'0') As Container, 
            //       ISNULL(ps.Stock,'0') As Stock, 
            //       ISNULL(ps.SellingPrice,'0') AS MRP
            //                    FROM    ProductStock ps 
            //                    INNER JOIN product p on ps.ProductId = p.Id 
            //                    INNER JOIN Instance i ON ps.InstanceId = i.Id
            //                    WHERE i.ExternalId = {externalID} AND p.Name Like '{searchText}%'";
            /*Query Optimized by POongodi on 09/10/2017*/
            string query = $@"SELECT  p.name As Medicine, p.GenericName, sum(ISNULL(ps.Stock,'0'))  As Stock
                                FROM   (Select * from Instance (nolock)  WHERE  ExternalId = {externalID} ) i 
                                 INNER JOIN  ProductStock (nolock) ps  ON ps.InstanceId = i.Id
                                INNER JOIN (Select * from product (nolock) where Name Like '{searchText}%') p on ps.ProductId = p.Id 
                                WHERE i.ExternalId = {externalID} AND p.Name Like '{searchText}%' group by p.Name, p.GenericName";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<ProductStockResult>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new ProductStockResult
                    {
                        Medicine = reader["Medicine"].ToString(),
                        GeneralName = reader["GenericName"].ToString(),
                        //Strength = reader.GetString(2),
                        //MedicineType = reader.GetString(3),
                        //Manufacturer = reader.GetString(4),
                        //Container = reader.GetString(5),
                        Stock = Convert.ToDecimal(reader["Stock"]),
                        //MRP = reader.GetDecimal(7)
                        Strength = "",
                        MedicineType = "",
                        Manufacturer = "",
                        Container = "",
                        MRP = 0
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        //Newly added on 16-Aug-2017 by Manivannan
        public async Task<ProductStockResult> GetProductStockByExternalIDExactMatch(string externalID, string searchText)
        {
            string query = $@"SELECT  p.name As Medicine, p.GenericName, sum(ISNULL(ps.Stock,'0'))  As Stock
                                FROM    ProductStock ps 
                                INNER JOIN product p on ps.ProductId = p.Id 
                                INNER JOIN Instance i ON ps.InstanceId = i.Id
                                WHERE i.ExternalId = {externalID} AND p.Name = '{searchText}' group by p.Name, p.GenericName";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<ProductStockResult>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new ProductStockResult
                    {
                        Medicine = reader["Medicine"].ToString(),
                        GeneralName = reader["GenericName"].ToString(),
                        //Strength = reader.GetString(2),
                        //MedicineType = reader.GetString(3),
                        //Manufacturer = reader.GetString(4),
                        //Container = reader.GetString(5),
                        Stock = Convert.ToDecimal(reader["Stock"]),
                        //MRP = reader.GetDecimal(7)
                        Strength = "",
                        MedicineType = "",
                        Manufacturer = "",
                        Container = "",
                        MRP = 0
                    };
                    list.Add(item);
                }
            }
            return list.FirstOrDefault();
        }
        public async Task<IList<ProductStock>> GetTempProductStock(ProductStock data, string tempVendorPurchaseItemId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId)
                .And(ProductStockTable.InstanceIdColumn, data.InstanceId)
                .And(ProductStockTable.ProductIdColumn, data.ProductId)
                .And(ProductStockTable.BatchNoColumn, data.BatchNo);
            //.And(ProductStockTable.ExpireDateColumn, data.ExpireDate)
            //.And(ProductStockTable.VATColumn, data.VAT);
            qb.JoinBuilder.Join(TempVendorPurchaseItemTable.Table, TempVendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.IdColumn, tempVendorPurchaseItemId);
            return await List<ProductStock>(qb);
        }
        public async Task<IList<ProductStock>> GetDcProductStock(ProductStock data, string dcVendorPurchaseItemId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId)
                .And(ProductStockTable.InstanceIdColumn, data.InstanceId)
                .And(ProductStockTable.ProductIdColumn, data.ProductId)
                .And(ProductStockTable.BatchNoColumn, data.BatchNo)
                //.And(ProductStockTable.ExpireDateColumn, data.ExpireDate)
                .And(ProductStockTable.VATColumn, data.VAT);
            qb.JoinBuilder.Join(DCVendorPurchaseItemTable.Table, DCVendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.IdColumn, dcVendorPurchaseItemId);
            return await List<ProductStock>(qb);
        }
        public async Task<IList<ProductStock>> GetProductStock(ProductStock data)
        {
            /*Packagesize validation added by Poongodi on 17/06/2016*/
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId)
                .And(ProductStockTable.InstanceIdColumn, data.InstanceId)
                .And(ProductStockTable.ProductIdColumn, data.ProductId)
                .And(ProductStockTable.BatchNoColumn, data.BatchNo)
                .And(ProductStockTable.ExpireDateColumn, data.ExpireDate)
                //.And(ProductStockTable.VATColumn, data.VAT) //Commented by Sarubala on 07-Aug-17
                .And(ProductStockTable.GstTotalColumn, data.GstTotal)
                .And(ProductStockTable.PurchasePriceColumn, data.PurchasePrice)
                .And(ProductStockTable.SellingPriceColumn, data.SellingPrice)
            .And(ProductStockTable.PackageSizeColumn, data.PackageSize);
            if (data.MRP != null)
            {
                qb.ConditionBuilder.And(ProductStockTable.MRPColumn, data.MRP);
            }
            //new condition on 10/25/2016
            if (data.PurchaseBarcode != null)
                qb.ConditionBuilder.And(ProductStockTable.PurchaseBarcodeColumn, data.PurchaseBarcode);
            if (data.Eancode != null && data.Eancode != "")
                qb.ConditionBuilder.And(ProductStockTable.EancodeColumn, data.Eancode);
            if (data.VendorId != null)
                qb.ConditionBuilder.And(ProductStockTable.VendorIdColumn, data.VendorId);
            return await List<ProductStock>(qb);
        }
        
        public async Task<IList<ProductStock>> GetProductStockById(ProductStock data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId)
                .And(ProductStockTable.InstanceIdColumn, data.InstanceId)
               .And(ProductStockTable.IdColumn, data.Id);
            return await List<ProductStock>(qb);
        }
        //Added by Manivannan on 17-Dec-2017
        public async Task<ProductStock> GetProductStockById(string productStockId, string transactionId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn, productStockId);
            qb.ConditionBuilder.And(ProductStockTable.TransactionIdColumn, transactionId);

            return (await List<ProductStock>(qb)).FirstOrDefault();
        }
        //Added by Manivannan on 17-Dec-2017
        public async Task<ProductStock> GetProductStockById(string productStockId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn, productStockId);
            
            return (await List<ProductStock>(qb)).FirstOrDefault();
        }
        //To get a product list based on product - by Poongodi on 17-05-2017
        public async Task<IList<ProductStock>> GetProductStockList(string ProductStockId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, InstanceId)
                .And(ProductStockTable.IdColumn, ProductStockId);


            return await List<ProductStock>(qb);
        }
        /// <summary>
        /// update method For handling deadlock
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ProductStock> Update(ProductStock data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("InstanceId", data.InstanceId);
            param.Add("Accountid", data.AccountId);
            param.Add("Id", data.Id);
            param.Add("Stock", data.Stock);
            param.Add("VendorId", data.VendorId);
            param.Add("IsMovingStock", data.IsMovingStock);
            param.Add("SellingPrice", data.SellingPrice);
            param.Add("Eancode", data.Eancode);
            param.Add("OfflineStatus", data.OfflineStatus);
            param.Add("MRP", data.MRP);
            param.Add("UpdatedAt", data.UpdatedAt);
                     
            string procName = string.Empty;
            if (!data.WriteExecutionQuery)
            {
                var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Productstock_Update", param);
            }

            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.StockColumn, ProductStockTable.VendorIdColumn, ProductStockTable.UpdatedAtColumn,
                ProductStockTable.IsMovingStockColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.EancodeColumn,
                ProductStockTable.OfflineStatusColumn, ProductStockTable.MRPColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn);
            param.Add("transQuantity", data.transQuantity);
            //added by nandhini for transaction
            if (data.WriteExecutionQuery)
            {
                qb.SetCommandType(CommandType.StoredProcedure);
                qb.SetProcName("usp_Productstock_Update");
                param.ToList().ForEach(x => qb.Parameters.Add(x.Key, x.Value)); 
                data.AddExecutionQuery(qb);
            }
            else
            {
                WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = param, EventLevel = "I" });
            }
            //WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = "Exec usp_Productstock_Update", Parameters = param, EventLevel = "I" });

            return data;
        }
        //Added by Bikas 10.11.2018 to Update PackageSize (Start)
        public async Task<bool> UpdateTransferProductInventory(ProductStock data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Update);
            qb.AddColumn(ProductTable.PackageSizeColumn, ProductTable.UpdatedAtColumn);
            qb.ConditionBuilder.And(ProductTable.AccountIdColumn, data.AccountId);
            //Commented by Sumathi on 26-Nov-18 
         //   qb.ConditionBuilder.And(ProductTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(ProductTable.IdColumn, data.ProductId);
            var packageSizeIsNull = new CustomCriteria(ProductTable.PackageSizeColumn, CriteriaCondition.IsNull);
            var cc1 = new CriteriaColumn (ProductTable.PackageSizeColumn,"0", CriteriaEquation.Equal);
            var col1 = new CustomCriteria(cc1, packageSizeIsNull, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);
            qb.Parameters.Add(ProductTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(ProductTable.PackageSizeColumn.ColumnName, data.PackageSize);
            await QueryExecuter.NonQueryAsyc(qb);

            return true;
        }
        public async Task<ProductStock> UpdateTransferProductStockInventory(ProductStock data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.PackageSizeColumn, ProductStockTable.UpdatedAtColumn);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, data.ProductId);
            var packageSizeIsNull = new CustomCriteria(ProductStockTable.PackageSizeColumn, CriteriaCondition.IsNull);
            var cc1 = new CriteriaColumn(ProductStockTable.PackageSizeColumn, "0", CriteriaEquation.Equal);
            var col1 = new CustomCriteria(cc1, packageSizeIsNull, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);
            qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(ProductStockTable.PackageSizeColumn.ColumnName, data.PackageSize);
            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }
        //End
        //added by nandhini on 23.12.17
        public async Task<ProductStock> UpdateGst(ProductStock data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.GstTotalColumn, ProductStockTable.HsnCodeColumn, ProductStockTable.UpdatedAtColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn,data.AccountId);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn,data.InstanceId);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn,data.Id);
            qb.Parameters.Add(ProductStockTable.GstTotalColumn.ColumnName, data.GstTotal);
            qb.Parameters.Add(ProductStockTable.HsnCodeColumn.ColumnName, data.HsnCode);
            qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(ProductStockTable.IgstColumn.ColumnName, data.Igst);
            qb.Parameters.Add(ProductStockTable.CgstColumn.ColumnName, data.Cgst);
            qb.Parameters.Add(ProductStockTable.SgstColumn.ColumnName, data.Sgst);
            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }
        //end 
        //Newly created by Manivannan on 20-Nov-2017
        public async Task<bool> UpdateProductStock(SalesItem salesItem)
        {
            string productStockId = salesItem.ProductStockId;
            decimal? quantity = salesItem.Quantity;

            var isUpdated = false;

            var psExisting = await GetProductStockById(productStockId);

            var transactionId = Guid.NewGuid().ToString();

            if (psExisting != null)
            {
                try
                {


                    var sUpdatedAt = CustomDateTime.IST;
                    var sUpdatedBy = salesItem.UpdatedBy;

                    Dictionary<string, object> param = new Dictionary<string, object>();

                    param.Add("InstanceId", psExisting.InstanceId);
                    param.Add("Accountid", psExisting.AccountId);
                    param.Add("Id", productStockId);
                    param.Add("UpdatedAt", sUpdatedAt);
                    param.Add("UpdatedBy", sUpdatedBy);
                    param.Add("Quantity", quantity);
                    param.Add("TransactionId", transactionId);

                    //var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Productstock_UpdateStock", param);
                    var result = (await sqldb.ExecuteProcedureAsync<int>("usp_Productstock_UpdateStock", param)).FirstOrDefault();
                    if (result > 0)
                    {
                        var psAfterUpdate = await GetProductStockById(productStockId);
                        var stockAfterUpdate = psAfterUpdate.Stock;

                        var query = $@"Update ProductStock Set Stock = @Stock, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy, TransactionId = @TransactionId where AccountId = @AccountId And InstanceId = @InstanceId And  id = @Id";
                        var qb = QueryBuilderFactory.GetQueryBuilder(query);

                        param.Add("Stock", stockAfterUpdate);

                        qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, sUpdatedAt);
                        qb.Parameters.Add(ProductStockTable.UpdatedByColumn.ColumnName, sUpdatedBy);
                        qb.Parameters.Add(ProductStockTable.IdColumn.ColumnName, productStockId);
                        qb.Parameters.Add(ProductStockTable.AccountIdColumn.ColumnName, psExisting.AccountId);
                        qb.Parameters.Add(ProductStockTable.InstanceIdColumn.ColumnName, psExisting.InstanceId);
                        qb.Parameters.Add(ProductStockTable.TransactionIdColumn.ColumnName, transactionId);
                        qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, stockAfterUpdate);

                        param.Add("transQuantity", quantity);
                        WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = param, EventLevel = "I" });
                        isUpdated = true;
                    }
                    else
                    {
                        isUpdated = false;
                    }

                }
                catch(Exception ex)
                {
                    isUpdated = false;
                }
                //var ps = await GetProductStockById(productStockId, transactionId);
                //if (ps != null)
                //    isUpdated = true;
                //else
                //    isUpdated = false;

                return isUpdated;
            }
            return false;
        }

        public async Task<List<ProductStock>> BulkUpdateProductStock(List<ProductStock> StockList, SqlConnection con, SqlTransaction transaction, List<SyncObject> syncObjectList)
        {
            var StockListWithQty = string.Join(",", from item in StockList select item.Id + "~" + item.transQuantity);
            if (!string.IsNullOrEmpty(StockListWithQty))
            {
                var qb = QueryBuilderFactory.GetQueryBuilder("Usp_Bulk_Update_ProductStock");
                qb.Parameters.Add(ProductStockTable.AccountIdColumn.ColumnName, user.AccountId());
                qb.Parameters.Add(ProductStockTable.InstanceIdColumn.ColumnName, user.InstanceId());
                qb.Parameters.Add("StockListWithQty", StockListWithQty);
                qb.Parameters.Add(ProductStockTable.UpdatedByColumn.ColumnName, user.Identity.Id());
                var psList = await sqldb.ExecuteProcedureAsync<dynamic>(qb, con, transaction);

                foreach (var ps in psList)
                {
                    var query = $@"Update ProductStock Set Stock = @Stock, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy, TransactionId = @TransactionId where AccountId = @AccountId And InstanceId = @InstanceId And  id = @Id";
                    var qbPs = QueryBuilderFactory.GetQueryBuilder(query);

                    Dictionary<string, object> param = new Dictionary<string, object>();
                    param.Add("Accountid", user.AccountId());
                    param.Add("InstanceId", user.InstanceId());
                    param.Add("Id", ps.ProductStockId);
                    param.Add("UpdatedAt", ps.UpdatedAt);
                    param.Add("UpdatedBy", user.Identity.Id());
                    param.Add("Quantity", ps.UpdatedStock);
                    param.Add("TransactionId", ps.TransactionId);
                    param.Add("Stock", ps.Stock);
                    param.Add("transQuantity", ps.UpdatedStock);
                    var syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbPs.GetQuery(), Parameters = param, EventLevel = "I" };
                    syncObjectList.Add(syncObject);
                }
            }
            return StockList;
        }

        public async Task<ProductStock> UpdateOld(ProductStock data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.StockColumn, ProductStockTable.VendorIdColumn, ProductStockTable.UpdatedAtColumn, ProductStockTable.IsMovingStockColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.EancodeColumn, ProductStockTable.OfflineStatusColumn, ProductStockTable.MRPColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn);

            qb.Parameters.Add(ProductStockTable.AccountIdColumn.ColumnName, data.AccountId);
            qb.Parameters.Add(ProductStockTable.InstanceIdColumn.ColumnName, data.InstanceId);
            qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, data.Stock);
            qb.Parameters.Add(ProductStockTable.VendorIdColumn.ColumnName, data.VendorId);
            qb.Parameters.Add(ProductStockTable.IdColumn.ColumnName, data.Id);
            qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(ProductStockTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
            qb.Parameters.Add(ProductStockTable.IsMovingStockColumn.ColumnName, data.IsMovingStock);
            qb.Parameters.Add(ProductStockTable.SellingPriceColumn.ColumnName, data.SellingPrice);
            qb.Parameters.Add(ProductStockTable.MRPColumn.ColumnName, data.MRP);
            qb.Parameters.Add(ProductStockTable.EancodeColumn.ColumnName, data.Eancode);
            qb.Parameters.Add("transQuantity", data.transQuantity);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task UpdateBarcode(ProductStock data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.VendorIdColumn, ProductStockTable.EancodeColumn, ProductStockTable.UpdatedAtColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn);
            qb.Parameters.Add(ProductStockTable.IdColumn.ColumnName, data.Id);
            qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(ProductStockTable.VendorIdColumn.ColumnName, data.VendorId);
            qb.Parameters.Add(ProductStockTable.EancodeColumn.ColumnName, data.Eancode);
            //added by nandhini for transaction
            if (data.WriteExecutionQuery)
            {
                data.AddExecutionQuery(qb);
            }
            else
            { 
            await QueryExecuter.NonQueryAsyc(qb);
            }
        }
        public async Task<List<ProductStock>> SaveNewStock(List<ProductStock> stock, string accId, string insId, string userid, bool blnIndent = false)
        {
            /*bGstEnabled Added by Poongodi on 10/08/2017*/
            bool bGstEnabled = _configHelper.AppConfig.IsGstEnabled;
            if (stock.Count() > 0)
            {               
                string tempInvoice = GetNewStockInvoiceNo(insId).Result;
                /*Product GST% and HSN code updated by Poongodi on 25/07/2017*/
                foreach (ProductStock item in stock)
                {
                    item.Product.SetExecutionQuery(item.GetExecutionQuery(), item.WriteExecutionQuery);
                    /*HSNcode taken from Product By Poongodi on 12/09/2017*/
                    item.HsnCode = item.Product.HsnCode;
                    //item.Product.HsnCode = item.Product.HsnCode;
                    item.Product.GstTotal = item.GstTotal;
                    item.Product.Sgst = item.GstTotal / 2;
                    item.Product.Cgst = item.GstTotal / 2;
                    item.Product.Igst = item.GstTotal;
                    if (!string.IsNullOrEmpty(item.Product.ProductMasterID))
                    {
                        var product = new Product
                        {
                            Name = item.Product.Name,
                            ProductMasterID = item.Product.ProductMasterID,
                            Manufacturer = item.Product.Manufacturer,
                            Schedule = item.Product.Schedule,
                            GenericName = item.Product.GenericName,
                            Packing = item.Product.Packing,
                            Category = item.Product.Category,
                            AccountId = accId,
                            InstanceId = insId,
                            CreatedBy = userid,
                            UpdatedBy = userid,
                            HsnCode = item.Product.HsnCode,
                            GstTotal = item.GstTotal,
                            Sgst = item.GstTotal / 2,
                            Cgst = item.GstTotal / 2,
                            Igst = item.GstTotal,
                        };
                        var getExistData = await _productDataAccess.GetExistProductMasterById(product);
                        if (getExistData.Id != null)
                        {
                            item.Product.Id = getExistData.Id;
                            var result = await _productDataAccess.UpdateProductGSTFromStock(product, true, true, true, true, true);//Added by Poongodi on 25/07/2017
                        }
                        else
                        {
                            //int NoOfRows = await _productDataAccess.TotalProductCount(product);
                            //var getChar = product.Name.Substring(0, 2);
                            //product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();

                            product = await _productDataAccess.Save(product);
                            item.Product.Id = product.Id;
                            ///*Product Info added to Product Instance table*/
                            //ProductInstance pi = new ProductInstance();
                            //pi.AccountId = product.AccountId;
                            //pi.InstanceId = insId;
                            //pi.ProductId = product.Id;
                            //pi.CreatedBy = userid;
                            //pi.UpdatedBy = userid;
                            //var pis = await _productDataAccess.SaveProductInstance(pi);
                        }
                    }
                    else //Added by Poongodi on 25/07/2017
                    {
                        var result = await _productDataAccess.UpdateProductGSTFromStock(item.Product, true, true, true, true, true);
                    }
                    if (blnIndent == false)
                    {
                        item.NewOpenedStock = true;
                        item.NewStockInvoiceNo = tempInvoice;
                        //added by nandhini
                        item.ImportQty = null;
                        item.StockImport = null;
                        //end
                        item.CreatedBy = userid;
                        item.UpdatedBy = userid;
                    }
                    item.AccountId = accId;
                    item.InstanceId = insId;
                    item.ProductId = item.Product.Id;

                    if (bGstEnabled)
                    {
                        item.TaxRefNo = 1;
                    }
                    if (item.WriteExecutionQuery)
                    {
                        SetInsertMetaData(item, ProductStockTable.Table);
                        WriteInsertExecutionQuery(item, ProductStockTable.Table);
                    }
                    else
                    {
                        await Insert(item, ProductStockTable.Table);
                    }
                   
                }
            }
            return stock;
        }
        // Added Gavaskar inventoryDataUpdate 16-02-2017 Start 
        public async Task<ProductStock> getProductStockDetails(string AccountId, string InstanceId, string Id)
        {
            var query = $@"select BatchNo,ExpireDate,VAT,GstTotal,TaxType,SellingPrice,MRP,Stock,PackageSize,PackagePurchasePrice,PurchasePrice from productstock where 
                           AccountId ='{AccountId}' and InstanceId='{InstanceId}' and Id='{Id}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await List<ProductStock>(qb)).FirstOrDefault();
        }
        public async Task<List<ProductStock>> inventoryDataUpdate_Old(List<ProductStock> Stock, string AccountId, string InstanceId, string userid)
        {
            //if (Stock.Count() > 0)
            //{
            foreach (ProductStock item in Stock)
            {
                // Get Data  ProductStock
                var productStockUpdate = getProductStockDetails(AccountId, InstanceId, item.Id).Result;
                // Update  ProductStock
                item.UpdatedAt = CustomDateTime.IST;
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(ProductStockTable.IdColumn, item.Id);
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, item.AccountId);
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, item.InstanceId);
                //MRP Implementation by Settu
                qb.AddColumn(ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.VATColumn, ProductStockTable.GstTotalColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.GstTotalColumn, ProductStockTable.VendorIdColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.MRPColumn, ProductStockTable.PackageSizeColumn, ProductStockTable.PackagePurchasePriceColumn, ProductStockTable.PurchasePriceColumn, ProductStockTable.UpdatedAtColumn);
                qb.Parameters.Add(ProductStockTable.BatchNoColumn.ColumnName, item.BatchNo);
                qb.Parameters.Add(ProductStockTable.ExpireDateColumn.ColumnName, item.ExpireDate);
                qb.Parameters.Add(ProductStockTable.VATColumn.ColumnName, item.VAT);
                qb.Parameters.Add(ProductStockTable.GstTotalColumn.ColumnName, item.GstTotal);
                //GST fields starts here
                qb.Parameters.Add(ProductStockTable.IgstColumn.ColumnName, item.Igst);
                qb.Parameters.Add(ProductStockTable.CgstColumn.ColumnName, item.Cgst);
                qb.Parameters.Add(ProductStockTable.SgstColumn.ColumnName, item.Sgst);
                qb.Parameters.Add(ProductStockTable.GstTotalColumn.ColumnName, item.GstTotal);
                //GST fields ends here

                qb.Parameters.Add(ProductStockTable.VendorIdColumn.ColumnName, item.VendorId);
                qb.Parameters.Add(ProductStockTable.SellingPriceColumn.ColumnName, item.SellingPrice);
                qb.Parameters.Add(ProductStockTable.MRPColumn.ColumnName, item.MRP);
                qb.Parameters.Add(ProductStockTable.PackageSizeColumn.ColumnName, item.PackageSize);
                qb.Parameters.Add(ProductStockTable.PackagePurchasePriceColumn.ColumnName, item.PackagePurchasePrice);
                qb.Parameters.Add(ProductStockTable.PurchasePriceColumn.ColumnName, item.PurchasePrice);
                qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, item.UpdatedAt);
                await QueryExecuter.NonQueryAsyc(qb);
                // Insert InventoryUpdateHistory
                InventoryUpdateHistory inventoryUpdateHistory = new InventoryUpdateHistory();
                inventoryUpdateHistory.AccountId = item.AccountId;
                inventoryUpdateHistory.InstanceId = item.InstanceId;
                inventoryUpdateHistory.ProductStockId = item.Id;
                inventoryUpdateHistory.BatchNoOld = productStockUpdate.BatchNo;
                inventoryUpdateHistory.BatchNoNew = item.BatchNo;
                inventoryUpdateHistory.ExpireDateOld = productStockUpdate.ExpireDate;
                inventoryUpdateHistory.ExpireDateNew = item.ExpireDate;
                inventoryUpdateHistory.VATOld = productStockUpdate.VAT;
                inventoryUpdateHistory.VATNew = item.VAT;
                inventoryUpdateHistory.GSTOld = productStockUpdate.GstTotal;
                inventoryUpdateHistory.GSTNew = item.GstTotal;
                inventoryUpdateHistory.PackageSizeOld = productStockUpdate.PackageSize;
                inventoryUpdateHistory.PackageSizeNew = item.PackageSize;
                inventoryUpdateHistory.PackagePurchasePriceOld = Math.Round(Convert.ToDecimal(productStockUpdate.PackagePurchasePrice), 2);
                inventoryUpdateHistory.PackagePurchasePriceNew = Math.Round(Convert.ToDecimal(item.PackagePurchasePrice), 2);
                inventoryUpdateHistory.SellingPriceOld = Math.Round(Convert.ToDecimal(productStockUpdate.SellingPrice), 2);
                inventoryUpdateHistory.SellingPriceNew = Math.Round(Convert.ToDecimal(item.SellingPrice), 2);
                inventoryUpdateHistory.MRPOld = Math.Round(Convert.ToDecimal(productStockUpdate.MRP), 5);
                inventoryUpdateHistory.MRPNew = Math.Round(Convert.ToDecimal(item.MRP), 5);
                inventoryUpdateHistory.CreatedAt = CustomDateTime.IST;
                inventoryUpdateHistory.UpdatedAt = CustomDateTime.IST;
                inventoryUpdateHistory.CreatedBy = userid;
                inventoryUpdateHistory.UpdatedBy = userid;
                await Insert(inventoryUpdateHistory, InventoryUpdateHistoryTable.Table);
            }
            //}
            return Stock;
        }
        // Added Gavaskar inventoryDataUpdate 16-02-2017 End 
        /// <summary>
        /// Previous Method commented 
        /// Update Productstock changed from direct qry to sp for handling deadlock 
        /// </summary>
        /// <param name="Stock"></param>
        /// <param name="AccountId"></param>
        /// <param name="InstanceId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public async Task<List<ProductStock>> inventoryDataUpdate(List<ProductStock> Stock, string AccountId, string InstanceId, string userid)
        {
            //if (Stock.Count() > 0)
            //{
            foreach (ProductStock item in Stock)
            {
                // Get Data  ProductStock
                var productStockUpdate = getProductStockDetails(AccountId, InstanceId, item.Id).Result;
                // Update  ProductStock
                item.UpdatedAt = CustomDateTime.IST;
                item.OfflineStatus = _configHelper.AppConfig.OfflineMode;
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("Id", item.Id);
                param.Add("AccountId", item.AccountId);
                param.Add("InstanceId", item.InstanceId);
                param.Add("BatchNo", item.BatchNo);
                param.Add("ExpireDate", item.ExpireDate);
                param.Add("SellingPrice", item.SellingPrice);
                param.Add("MRP", item.MRP);
                param.Add("PackageSize", item.PackageSize);
                param.Add("PackagePurchasePrice", item.PackagePurchasePrice);
                param.Add("PurchasePrice", item.PurchasePrice);
                param.Add("UpdatedAt", item.UpdatedAt);
                param.Add("UpdatedBy", userid);
                param.Add("Igst", item.Igst);
                param.Add("Cgst", item.Cgst);
                param.Add("Sgst", item.Sgst);
                param.Add("GstTotal", item.GstTotal);
                param.Add("Hsncode", item.Product.HsnCode); //Added  by Poongodi on 08/09/2017
                string procName = string.Empty;
                var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_Inventory_StockUpdate", param);

                /*Update Given HSNCode and GST% to product table by Poongodi on 12/09/2017*/
                var result1 = await _productDataAccess.UpdateProductGSTFromStock(item.Product, true, true, true, true, true);//Added by Poongodi on 12/09/2017

                var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(ProductStockTable.IdColumn, item.Id);
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, item.AccountId);
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, item.InstanceId);
                qb.AddColumn(ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.GstTotalColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.MRPColumn, ProductStockTable.PackageSizeColumn, ProductStockTable.PackagePurchasePriceColumn, ProductStockTable.PurchasePriceColumn, ProductStockTable.UpdatedAtColumn, ProductStockTable.UpdatedByColumn);
                WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = param, EventLevel = "I" });

                // Insert InventoryUpdateHistory
                InventoryUpdateHistory inventoryUpdateHistory = new InventoryUpdateHistory();
                inventoryUpdateHistory.AccountId = item.AccountId;
                inventoryUpdateHistory.InstanceId = item.InstanceId;
                inventoryUpdateHistory.ProductStockId = item.Id;
                inventoryUpdateHistory.BatchNoOld = productStockUpdate.BatchNo;
                inventoryUpdateHistory.BatchNoNew = item.BatchNo;
                inventoryUpdateHistory.ExpireDateOld = productStockUpdate.ExpireDate;
                inventoryUpdateHistory.ExpireDateNew = item.ExpireDate;
                inventoryUpdateHistory.VATOld = productStockUpdate.VAT;
                inventoryUpdateHistory.VATNew = item.VAT;
                inventoryUpdateHistory.GSTOld = productStockUpdate.GstTotal;
                inventoryUpdateHistory.GSTNew = item.GstTotal;
                inventoryUpdateHistory.PackageSizeOld = productStockUpdate.PackageSize;
                inventoryUpdateHistory.PackageSizeNew = item.PackageSize;
                inventoryUpdateHistory.PackagePurchasePriceOld = Math.Round(Convert.ToDecimal(productStockUpdate.PackagePurchasePrice), 2);
                inventoryUpdateHistory.PackagePurchasePriceNew = Math.Round(Convert.ToDecimal(item.PackagePurchasePrice), 2);
                inventoryUpdateHistory.SellingPriceOld = Math.Round(Convert.ToDecimal(productStockUpdate.SellingPrice), 2);
                inventoryUpdateHistory.SellingPriceNew = Math.Round(Convert.ToDecimal(item.SellingPrice), 2);
                inventoryUpdateHistory.MRPOld = Math.Round(Convert.ToDecimal(productStockUpdate.MRP), 5);
                inventoryUpdateHistory.MRPNew = Math.Round(Convert.ToDecimal(item.MRP), 5);
                inventoryUpdateHistory.CreatedAt = CustomDateTime.IST;
                inventoryUpdateHistory.UpdatedAt = CustomDateTime.IST;
                inventoryUpdateHistory.CreatedBy = userid;
                inventoryUpdateHistory.UpdatedBy = userid;
                await Insert(inventoryUpdateHistory, InventoryUpdateHistoryTable.Table);
            }
            //}
            return Stock;
        }

        private async Task<string> GetNewStockInvoiceNo(string instanceId)
        {
            var query = _configHelper.AppConfig.IsSqlServer
                ? $"SELECT ISNULL(MAX(CONVERT(INT, NewStockInvoiceNo)),0) + 1 AS InvoiceNo FROM ProductStock WHERE InstanceId = '{instanceId}'"
                : $"SELECT IFNULL(MAX(CAST(NewStockInvoiceNo as INT)),0) + 1 AS InvoiceNo FROM ProductStock WHERE InstanceId = '{instanceId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var num = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            return num;
        }
        public async Task<List<SelfConsumption>> SaveStockConsumption(List<SelfConsumption> pstock, string accId, string insId, string userid)
        {
            if (pstock.Count() > 0)
            {
                foreach (SelfConsumption item in pstock)
                {
                    item.InvoiceNo = GetConsumptionInvoiceNo(insId).Result;
                    item.AccountId = accId;
                    item.InstanceId = insId;
                    item.ConsumedBy = userid;
                    item.CreatedBy = userid;
                    item.UpdatedBy = userid;
                    await Insert(item, SelfConsumptionTable.Table);
                    //updateProductStock(item.ProductStock);
                    await Update(item.ProductStock); //Previous line commented and added by Poongodi on 29/08/2017
                    //if (item.ProductStock.Id != null)
                    //{
                    //    item.ProductStock.UpdatedAt = CustomDateTime.IST;
                    //    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                    //    qb.ConditionBuilder.And(ProductStockTable.IdColumn, item.ProductStock.Id);
                    //    qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, item.ProductStock.InstanceId);
                    //    qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, item.ProductStock.AccountId);
                    //    qb.AddColumn(ProductStockTable.UpdatedAtColumn, ProductStockTable.StockColumn);
                    //    qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, item.ProductStock.UpdatedAt);                        
                    //    qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, item.ProductStock.Stock);
                    //    await QueryExecuter.NonQueryAsyc(qb);
                    //}
                }
            }
            return pstock;
        }
        private async Task<string> GetConsumptionInvoiceNo(string instanceId)
        {
            var query = _configHelper.AppConfig.IsSqlServer
                ? $"SELECT ISNULL(MAX(CONVERT(INT, InvoiceNo)),0) + 1 AS InvoiceNo FROM SelfConsumption WHERE InstanceId = '{instanceId}'"
                : $"SELECT IFNULL(MAX(CAST(InvoiceNo as INT)),0) + 1 AS InvoiceNo FROM SelfConsumption WHERE InstanceId = '{instanceId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var num = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            return num;
        }
        private async void updateProductStock(ProductStock item)
        {
            if (item.Id != null)
            {
                item.UpdatedAt = CustomDateTime.IST;
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(ProductStockTable.IdColumn, item.Id);
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, item.InstanceId);
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, item.AccountId);
                qb.AddColumn(ProductStockTable.UpdatedAtColumn, ProductStockTable.StockColumn, ProductStockTable.VendorIdColumn);
                qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, item.UpdatedAt);
                qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, item.Stock);
                qb.Parameters.Add(ProductStockTable.VendorIdColumn.ColumnName, item.VendorId);
                await QueryExecuter.NonQueryAsyc(qb);
            }
        }
        public async Task<List<ProductStock>> GetNewOpenedInventory(ProductStock ps, string prodId, string InvoiceNo)
        {
            var pslist = new List<ProductStock>();
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, ps.InstanceId);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, ps.AccountId);
            qb.ConditionBuilder.And(ProductStockTable.NewOpenedStockColumn, 1);
            if (prodId != "null" && prodId != null && prodId != "undefined")
            {
                qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, prodId);
            }
            if (InvoiceNo != "null" && InvoiceNo != null && InvoiceNo != "undefined")
            {
                qb.ConditionBuilder.And(ProductStockTable.NewStockInvoiceNoColumn, InvoiceNo);
            }
            qb.AddColumn(ProductStockTable.IdColumn, ProductStockTable.ProductIdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn,
                //MRP Implementation by Settu
                ProductStockTable.VATColumn, ProductStockTable.GstTotalColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.MRPColumn, ProductStockTable.StockColumn, ProductStockTable.PackageSizeColumn,
                ProductStockTable.PackagePurchasePriceColumn, ProductStockTable.PurchasePriceColumn, ProductStockTable.NewStockInvoiceNoColumn,
                ProductStockTable.NewStockQtyColumn, ProductStockTable.CreatedAtColumn, ProductStockTable.PurchaseBarcodeColumn);
            qb.JoinBuilder.LeftJoin(ProductTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.CodeColumn);
            qb.SelectBuilder.SortByDesc(ProductStockTable.CreatedAtColumn);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var pstock = new ProductStock();
                    pstock.Id = reader[ProductStockTable.IdColumn.ColumnName].ToString();
                    pstock.ProductId = reader[ProductStockTable.ProductIdColumn.ColumnName].ToString();
                    pstock.BatchNo = reader[ProductStockTable.BatchNoColumn.ColumnName].ToString();
                    pstock.ExpireDate = reader[ProductStockTable.ExpireDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue;
                    pstock.VAT = reader[ProductStockTable.VATColumn.ColumnName] as decimal? ?? 0;
                    pstock.GstTotal = reader[ProductStockTable.GstTotalColumn.ColumnName] as decimal? ?? 0;
                    pstock.SellingPrice = reader[ProductStockTable.SellingPriceColumn.ColumnName] as decimal? ?? 0;
                    pstock.MRP = reader[ProductStockTable.MRPColumn.ColumnName] as decimal? ?? 0;
                    pstock.Stock = reader[ProductStockTable.StockColumn.ColumnName] as decimal? ?? 0;
                    pstock.PackageSize = reader[ProductStockTable.PackageSizeColumn.ColumnName] as decimal? ?? 0;
                    pstock.PackagePurchasePrice = reader[ProductStockTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0;
                    pstock.PurchasePrice = reader[ProductStockTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0;
                    pstock.NewStockInvoiceNo = reader[ProductStockTable.NewStockInvoiceNoColumn.ColumnName].ToString();
                    pstock.NewStockQty = reader[ProductStockTable.NewStockQtyColumn.ColumnName] as decimal? ?? 0;
                    pstock.Product.Id = reader[ProductTable.IdColumn.FullColumnName].ToString();
                    pstock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    pstock.Product.Code = reader[ProductTable.CodeColumn.FullColumnName].ToString();
                    pstock.CreatedAt = reader[ProductStockTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.Now;
                    pstock.PurchaseBarcode = reader[ProductStockTable.PurchaseBarcodeColumn.ColumnName].ToString();
                    pslist.Add(pstock);
                }
            }
            return pslist;
        }
        public async Task<InventorySettings> SaveSetting(InventorySettings data)
        {
            if (data.Id != null)
            {
                data.UpdatedAt = CustomDateTime.IST;
                data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
                var qb = QueryBuilderFactory.GetQueryBuilder(InventorySettingsTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(InventorySettingsTable.IdColumn, data.Id);
                qb.ConditionBuilder.And(InventorySettingsTable.InstanceIdColumn, data.InstanceId);
                qb.AddColumn(InventorySettingsTable.NoOfDaysColumn, InventorySettingsTable.NoOfMonthColumn, InventorySettingsTable.PopupStartDateColumn,
                     InventorySettingsTable.PopupEndDateColumn, InventorySettingsTable.PopupStartTimeColumn, InventorySettingsTable.PopupEndTimeColumn,
                     InventorySettingsTable.PopupIntervalColumn, InventorySettingsTable.PopupEnabledColumn,
                     InventorySettingsTable.UpdatedAtColumn, InventorySettingsTable.OfflineStatusColumn);
                qb.Parameters.Add(InventorySettingsTable.NoOfDaysColumn.ColumnName, data.NoOfDays);
                qb.Parameters.Add(InventorySettingsTable.NoOfMonthColumn.ColumnName, data.NoOfMonth);
                qb.Parameters.Add(InventorySettingsTable.PopupStartDateColumn.ColumnName, data.PopupStartDate);
                qb.Parameters.Add(InventorySettingsTable.PopupEndDateColumn.ColumnName, data.PopupEndDate);
                qb.Parameters.Add(InventorySettingsTable.PopupStartTimeColumn.ColumnName, data.PopupStartTime);
                qb.Parameters.Add(InventorySettingsTable.PopupEndTimeColumn.ColumnName, data.PopupEndTime);
                qb.Parameters.Add(InventorySettingsTable.PopupIntervalColumn.ColumnName, data.PopupInterval);
                qb.Parameters.Add(InventorySettingsTable.PopupEnabledColumn.ColumnName, data.PopupEnabled);
                qb.Parameters.Add(InventorySettingsTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
                qb.Parameters.Add(InventorySettingsTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
                await QueryExecuter.NonQueryAsyc(qb);
                //var vp = await sqlUpdateCalculation(data);
                return data;
            }
            else
            {
                return await Insert(data, InventorySettingsTable.Table);
            }
        }
        public async Task<ProductStock> GetStockValue(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn, id);
            return (await List<ProductStock>(qb)).First();
        }
        // Comment Gavaskar 24-12-2016
        //Method name renamed and the new method created in the same name by Poongodi on 21/02/2017
        public async Task<List<ProductStock>> List_Old(Product data)
        {
            var query = "";
            var NameQuery = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                NameQuery = " and p.Name like  '" + data.Name + "%' ";
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                NameQuery = " and p.GenericName like  '" + data.GenericName + "%' ";
            }
            var Pageno = (data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize;
            if (data.getFilter == "All")
            {
                query = $@" select ps.ProductId,p.Id AS [Product.Id], ps.Stock as [Stock],ps.ExpireDate,
                        p.Name AS [Product.Name],p.GenericName AS [Product.GenericName],p.Code AS [Product.Code],
                        p.Manufacturer AS [Product.Manufacturer],p.RackNo AS [Product.RackNo],p.Status AS [Product.Status]
                        ,v.Name AS [Vendor.Name],I.ReOrder AS [InventoryReOrder.ReOrder],I.Id AS [InventoryReOrder.Id],
                        I.Status AS [InventoryReOrder.Status] 
                        from 
                        ProductStock ps 
                        inner join Product p on p.id = ps.ProductId 
                        left join Vendor v on v.Id = ps.VendorId 
                        left join InventoryReOrder I ON I.ProductId = p.Id 
                        where ps.ProductId in(
                        select  ProductStock.productId from ProductStock inner join Product on Product.Id=ProductStock.ProductId 
                        where ProductStock.InstanceId='{data.InstanceId}' 
                        group by ProductStock.productId
                        ) and ps.InstanceId='{data.InstanceId}' and ps.AccountId='{data.AccountId}' {NameQuery}
                        ORDER BY p.Name asc OFFSET {Pageno} ROWS FETCH NEXT {data.Page.PageSize} ROWS ONLY
                ";
            }
            if (data.getFilter == "Available")
            {
                query = $@" select ps.ProductId,p.Id AS [Product.Id], ps.Stock as [Stock],ps.ExpireDate,
                        p.Name AS [Product.Name],p.GenericName AS [Product.GenericName],p.Code AS [Product.Code],
                        p.Manufacturer AS [Product.Manufacturer],p.RackNo AS [Product.RackNo],p.Status AS [Product.Status]
                        ,v.Name AS [Vendor.Name],I.ReOrder AS [InventoryReOrder.ReOrder],I.Id AS [InventoryReOrder.Id],
                        I.Status AS [InventoryReOrder.Status] 
                        from 
                        ProductStock ps 
                        inner join Product p on p.id = ps.ProductId 
                        left join Vendor v on v.Id = ps.VendorId 
                        left join InventoryReOrder I ON I.ProductId = p.Id 
                        where ps.ProductId in(
                        select  ProductStock.productId from ProductStock inner join Product on Product.Id=ProductStock.ProductId 
                        where ProductStock.InstanceId='{data.InstanceId}' 
                        group by ProductStock.productId having sum(stock) > 0 
                        ) and ps.InstanceId='{data.InstanceId}' and ps.AccountId='{data.AccountId}'  and ps.stock > 0 {NameQuery}
                        ORDER BY p.Name asc OFFSET {Pageno} ROWS FETCH NEXT {data.Page.PageSize} ROWS ONLY
                ";
            }
            if (data.getFilter == "Zero")
            {
                query = $@" select ps.ProductId,p.Id AS [Product.Id], ps.Stock as [Stock],ps.ExpireDate,
                        p.Name AS [Product.Name],p.GenericName AS [Product.GenericName],p.Code AS [Product.Code],
                        p.Manufacturer AS [Product.Manufacturer],p.RackNo AS [Product.RackNo],p.Status AS [Product.Status]
                        ,v.Name AS [Vendor.Name],I.ReOrder AS [InventoryReOrder.ReOrder],I.Id AS [InventoryReOrder.Id],
                        I.Status AS [InventoryReOrder.Status] 
                        from 
                        ProductStock ps 
                        inner join Product p on p.id = ps.ProductId 
                        left join Vendor v on v.Id = ps.VendorId 
                        left join InventoryReOrder I ON I.ProductId = p.Id 
                        where ps.ProductId in(
                        select  ProductStock.productId from ProductStock inner join Product on Product.Id=ProductStock.ProductId 
                        where ProductStock.InstanceId='{data.InstanceId}' 
                        group by ProductStock.productId having sum(stock) = 0 
                        ) and ps.InstanceId='{data.InstanceId}' and ps.AccountId='{data.AccountId}' and ps.ExpireDate >  getdate()  {NameQuery}
                        ORDER BY p.Name asc OFFSET {Pageno} ROWS FETCH NEXT {data.Page.PageSize} ROWS ONLY
                ";
            }
            if (data.getFilter == "Expiry")
            {
                query = $@" select ps.ProductId,p.Id AS [Product.Id], ps.Stock as [Stock],ps.ExpireDate,
                        p.Name AS [Product.Name],p.GenericName AS [Product.GenericName],p.Code AS [Product.Code],
                        p.Manufacturer AS [Product.Manufacturer],p.RackNo AS [Product.RackNo],p.Status AS [Product.Status]
                        ,v.Name AS [Vendor.Name],I.ReOrder AS [InventoryReOrder.ReOrder],I.Id AS [InventoryReOrder.Id],
                        I.Status AS [InventoryReOrder.Status] 
                        from 
                        ProductStock ps 
                        inner join Product p on p.id = ps.ProductId 
                        left join Vendor v on v.Id = ps.VendorId 
                        left join InventoryReOrder I ON I.ProductId = p.Id 
                        where ps.ProductId in(
                        select  ProductStock.productId from ProductStock inner join Product on Product.Id=ProductStock.ProductId 
                        where ProductStock.InstanceId='{data.InstanceId}' 
                        group by ProductStock.productId having sum(stock) > 0 
                        ) and ps.InstanceId='{data.InstanceId}' and ps.AccountId='{data.AccountId}'  and ps.ExpireDate <  getdate() {NameQuery} 
                        ORDER BY p.Name asc OFFSET {Pageno} ROWS FETCH NEXT {data.Page.PageSize} ROWS ONLY
                ";
            }
            if (data.getFilter == "Reorder")
            {
                query = $@" select ps.ProductId,p.Id AS [Product.Id], ps.Stock as [Stock],ps.ExpireDate,
                        p.Name AS [Product.Name],p.GenericName AS [Product.GenericName],p.Code AS [Product.Code],
                        p.Manufacturer AS [Product.Manufacturer],p.RackNo AS [Product.RackNo],p.Status AS [Product.Status]
                        ,v.Name AS [Vendor.Name],I.ReOrder AS [InventoryReOrder.ReOrder],I.Id AS [InventoryReOrder.Id],
                        I.Status AS [InventoryReOrder.Status] 
                        from 
                        ProductStock ps 
                        inner join Product p on p.id = ps.ProductId 
                        left join Vendor v on v.Id = ps.VendorId 
                        left join InventoryReOrder I ON I.ProductId = p.Id and  I.InstanceId='{data.InstanceId}'
                        where ps.ProductId in(
                        select  ProductStock.productId from ProductStock inner join Product on Product.Id=ProductStock.ProductId 
                        where ProductStock.InstanceId='{data.InstanceId}' 
                        group by ProductStock.productId having sum(stock) > 0 
                        ) and ps.InstanceId='{data.InstanceId}' and ps.AccountId='{data.AccountId}'  and I.ReOrder > 0 {NameQuery} 
                        ORDER BY p.Name asc OFFSET {Pageno} ROWS FETCH NEXT {data.Page.PageSize} ROWS ONLY
                ";
            }
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<ProductStock>(qb);
            if (data.getFilter == "Reorder")
            {
                var result1 = await List<ProductStock>(qb);
                await sqlUpdateCalculation2(result, data.InstanceId);
            }
            if (result.Count > 0)
            {
                var vp = await SqlQueryList1(result, data.InstanceId);
                var product = result.Select(x =>
                {
                    if (data.getFilter == "Available")
                        x.Product.ProductStock = vp.Where(y => y.ProductId == x.ProductId && y.InstanceId == data.InstanceId && y.Stock > 0).Select(z => z);
                    else if (data.getFilter == "Zero")
                    {
                        x.Product.ProductStock = vp.Where(y => y.ProductId == x.ProductId && y.InstanceId == data.InstanceId && y.Stock == 0).Select(z => z);
                    }
                    else if (data.getFilter == "Expiry")
                    {
                        x.Product.ProductStock = vp.Where(y => y.ProductId == x.ProductId && y.InstanceId == data.InstanceId && y.ExpireDate < DateTime.Now).Select(z => z);
                    }
                    else
                    {
                        x.Product.ProductStock = vp.Where(y => y.ProductId == x.ProductId && y.InstanceId == data.InstanceId).Select(z => z);
                    }
                    //x.Product.ProductStock = vp.Where(y => y.ProductId == x.ProductId && y.InstanceId == data.InstanceId && y.Stock > 0).Select(z => z);
                    //x.Product.ProductStock = vp.Where(y => y.ProductId == x.ProductId && y.InstanceId == data.InstanceId).Select(z => z);
                    return x;
                });
                var products = product.GroupBy(x => x.ProductId).Select(y => y.First());
                return products.ToList();
            }
            else
            {
                return result;
            }
        }
        public async Task<List<ProductStock>> List(Product data)
        {
            int NameType = 1;
            string Name = "";
            var Pageno = (data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize;
            if (!string.IsNullOrEmpty(data.Name))
            {
                Name = data.Name;
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                Name = data.GenericName;
                NameType = 2;
            }
            var result = await GetInventoryDetails(data.getFilter, data.InstanceId, data.AccountId, NameType, Name, Pageno, data.Page.PageSize);
            if (data.getFilter == "Reorder")
            {
                await sqlUpdateCalculation2(result, data.InstanceId);
            }
            if (result.Count() > 0  )
            {
                var vp = await GetInventory_Details(result, data.InstanceId, data.getFilter); //Filter Parameter added by Poongodi on 27/05/2017
                var product = result.Select(x =>
                {
                    if (data.getFilter == "Available")
                    {
                        x.Product.ProductStock = vp.Where(y => y.InstanceId == data.InstanceId && y.ProductId == x.ProductId && y.Stock > 0).Select(z => z);
                    }
                    else if (data.getFilter == "Zero")
                    {
                        x.Product.ProductStock = vp.Where(y => y.InstanceId == data.InstanceId && y.ProductId == x.ProductId && y.Stock == 0).Select(z => z);
                    }
                    else if (data.getFilter == "Expiry")
                    {
                        x.Product.ProductStock = vp.Where(y => y.InstanceId == data.InstanceId && y.ProductId == x.ProductId && y.ExpireDate < DateTime.Now).Select(z => z);
                    }
                    else
                    {
                        x.Product.ProductStock = vp.Where(y => y.InstanceId == data.InstanceId && y.ProductId == x.ProductId).Select(z => z);
                    }
                    //x.Product.ProductStock = vp.Where(y => y.ProductId == x.ProductId && y.InstanceId == data.InstanceId && y.Stock > 0).Select(z => z);
                    //x.Product.ProductStock = vp.Where(y => y.ProductId == x.ProductId && y.InstanceId == data.InstanceId).Select(z => z);
                    return x;
                });
                var products = product.GroupBy(x => x.ProductId).Select(y => y.First());
                return products.ToList();
            }
            else
            {
                return result.ToList();
            }
        }
        public async Task<List<ProductStock>> SqlQueryList1(IEnumerable<ProductStock> data, string instanceId)
        {
            var viqb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            viqb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId);
            viqb.AddColumn(ProductStockTable.IdColumn, ProductStockTable.ProductIdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.StockColumn, ProductStockTable.ReOrderQtyColumn, ProductStockTable.StatusColumn, ProductStockTable.InstanceIdColumn);
            viqb.JoinBuilder.LeftJoin(VendorTable.Table, VendorTable.IdColumn, ProductStockTable.VendorIdColumn);
            viqb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.IdColumn);
            var i = 1;
            foreach (var product in data)
            {
                var paramName = $"s{i++}";
                var col = new CriteriaColumn(ProductStockTable.ProductIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(col);
                viqb.ConditionBuilder.Or(cc);
                viqb.Parameters.Add(paramName, product.ProductId);
            }
            var list = await List<ProductStock>(viqb);
            return list;
        }
        //New method added to get the Inventory details by Poongodi  on 28/02/2017

        public async Task<List<ProductStock>> GetInventory_Details(IEnumerable<ProductStock> data, string instanceId, string type)
        {
            //to be work
            string sProductId = string.Join(",", data.Select(x => x.Id).ToList());
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("ProductId", sProductId);
            parms.Add("Type", type);    //type Filter added by Poongodi on 27/05/2017
            string procName = string.Empty;
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetInventory_Details", parms);
            var product = result.Select(x =>
           {
               var productstock = new ProductStock
               {
                   BatchNo = x.BatchNo,
                   ExpireDate = x.ExpireDate,
                   Stock = x.Stock,
                   ProductId = x.ProductId,
                   InstanceId = x.InstanceId,
                   Id = x.Id,
                   Status = x.Status,
                   Vendor = { Name = x.VendorName },
                   PurchasePrice = x.purchasePrice //Added by Annadurai 20062017
               };
               return productstock;
           });
            return product.ToList();
        }
        private QueryBuilderBase SqlQueryBuilder1(Product data, QueryBuilderBase qb)
        {
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.GenericNameColumn, ProductTable.CodeColumn, ProductTable.ManufacturerColumn, ProductTable.RackNoColumn, ProductTable.StatusColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, ProductStockTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn);
            qb.JoinBuilder.LeftJoin(InventoryReOrderTable.Table, InventoryReOrderTable.ProductIdColumn, ProductTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(InventoryReOrderTable.Table, InventoryReOrderTable.ReOrderColumn, InventoryReOrderTable.IdColumn, InventoryReOrderTable.StatusColumn);
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (!string.IsNullOrEmpty(data.Id))
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                    else
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                }
                else
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                    else
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                }
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                if (_configHelper.AppConfig.IsSqlServer)
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
                else
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                if (data.getFilter == "Available")
                {
                    qb.ConditionBuilder.And(ProductStockTable.StockColumn, 0, CriteriaEquation.Greater);
                }
                else if (data.getFilter == "Zero")
                {
                    qb.ConditionBuilder.And(ProductStockTable.StockColumn, 0, CriteriaEquation.Equal);
                }
                else if (data.getFilter == "Expiry")
                {
                    qb.ConditionBuilder.And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Lesser);
                }
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, data.InstanceId);
            }
            return qb;
        }
        private QueryBuilderBase SqlQueryBuilder(Product data, QueryBuilderBase qb)
        {
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (!string.IsNullOrEmpty(data.Id))
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                    else
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                }
                else
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                    else
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                }
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                if (_configHelper.AppConfig.IsSqlServer)
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
                else
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, data.InstanceId);
            }
            return qb;
        }
        private QueryBuilderBase SqlQueryBuilderAvailable(Product data, QueryBuilderBase qb)
        {
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (!string.IsNullOrEmpty(data.Id))
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                    else
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                }
                else
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                    else
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                }
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                if (_configHelper.AppConfig.IsSqlServer)
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
                else
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(ProductStockTable.StockColumn, 0, CriteriaEquation.Greater);
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, data.InstanceId);
            }
            return qb;
        }
        private QueryBuilderBase SqlQueryBuilderZero(Product data, QueryBuilderBase qb)
        {
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (!string.IsNullOrEmpty(data.Id))
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                    else
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                }
                else
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                    else
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                }
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                if (_configHelper.AppConfig.IsSqlServer)
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
                else
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(ProductStockTable.StockColumn, 0, CriteriaEquation.Equal);
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, data.InstanceId);
            }
            return qb;
        }
        private QueryBuilderBase SqlQueryBuilderExpiry(Product data, QueryBuilderBase qb)
        {
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (!string.IsNullOrEmpty(data.Id))
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                    else
                        qb.ConditionBuilder.And(ProductTable.IdColumn, data.Id);
                }
                else
                {
                    if (_configHelper.AppConfig.IsSqlServer)
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                    else
                        qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
                }
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                if (_configHelper.AppConfig.IsSqlServer)
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
                else
                    qb.ConditionBuilder.And(ProductTable.GenericNameColumn, data.GenericName);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(ProductStockTable.ExpireDateColumn, CustomDateTime.IST.Date.ToFormat(), CriteriaEquation.Lesser);
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, data.InstanceId);
            }
            return qb;
        }
        public async Task<int> Count(Product data)
        {
            var NameQuery = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (data.Name.ToString().Length == 1)
                    NameQuery = " and Product.Name like  '" + data.Name.ToString().Replace("'", "''") + "%' ";
                else
                    NameQuery = " and Product.Name =  '" + data.Name.ToString().Replace("'", "''") + "'";
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                NameQuery = "and Product.GenericName like '" + data.GenericName.ToString().Replace("'", "''") + "%'";
            }
            var query = $@"select COUNT(DISTINCT ProductStock.productId) from ProductStock WITH(NOLOCK) 
                            inner join Product WITH(NOLOCK) on Product.Id=ProductStock.ProductId 
                            where ProductStock.AccountId='{data.AccountId}' 
                            AND ProductStock.InstanceId='{data.InstanceId}' 
                            AND (ISNULL(ProductStock.Status,1)=1 OR ProductStock.ProductId IN(SELECT ProductId FROM ProductStock
                            WHERE AccountId='{data.AccountId}' AND InstanceId='{data.InstanceId}' AND Stock != 0 AND ISNULL(Status,1)!=1))
                            {NameQuery}";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            int count = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
            return count;
        }
        public async Task<int> AvailableCount(Product data)
        {
            var NameQuery = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (data.Name.ToString().Length == 1)
                    NameQuery = " and Product.Name like  '" + data.Name.ToString().Replace("'", "''") + "%' ";
                else
                    NameQuery = " and Product.Name =  '" + data.Name.ToString().Replace("'", "''") + "'";
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                NameQuery = "and Product.GenericName like '" + data.GenericName.ToString().Replace("'", "''") + "%'";
            }
            var query = $@"select COUNT(DISTINCT ProductStock.productId) from ProductStock WITH(NOLOCK) 
                            inner join Product WITH(NOLOCK) on Product.Id=ProductStock.ProductId 
                            where ProductStock.AccountId='{data.AccountId}' 
                            AND ProductStock.InstanceId='{data.InstanceId}' 
                            AND ProductStock.Stock != 0 and (ProductStock.Status is null or ProductStock.Status = 1)
                            {NameQuery}";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            int count = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
            return count;
        }
        public async Task<int> OnlyAvailableCount(Product data)
        {
            var NameQuery = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (data.Name.ToString().Length == 1)
                    NameQuery = " and Product.Name like  '" + data.Name.ToString().Replace("'", "''") + "%' ";
                else
                    NameQuery = " and Product.Name =  '" + data.Name.ToString().Replace("'", "''") + "'";
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                NameQuery = "and Product.GenericName like '" + data.GenericName.ToString().Replace("'", "''") + "%'";
            }
            var query = $@"select COUNT(DISTINCT ProductStock.productId) from ProductStock WITH(NOLOCK) 
                            inner join Product WITH(NOLOCK) on Product.Id=ProductStock.ProductId 
                            where ProductStock.AccountId='{data.AccountId}' 
                            AND ProductStock.InstanceId='{data.InstanceId}' 
                            AND ProductStock.ExpireDate > GETDATE()
                            AND ProductStock.Stock != 0 and (ProductStock.Status is null or ProductStock.Status = 1)
                            {NameQuery}";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            int count = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
            return count;
        }
        public async Task<int> InactiveCount(Product data)
        {
            var NameQuery = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (data.Name.ToString().Length == 1)
                    NameQuery = " and Product.Name like  '" + data.Name.ToString().Replace("'", "''") + "%' ";
                else
                    NameQuery = " and Product.Name =  '" + data.Name.ToString().Replace("'", "''") + "'";
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                NameQuery = "and Product.GenericName like '" + data.GenericName.ToString().Replace("'", "''") + "%'";
            }
            var query = $@"select COUNT(DISTINCT ProductStock.productId) from ProductStock WITH(NOLOCK) 
                            inner join Product WITH(NOLOCK) on Product.Id=ProductStock.ProductId 
                            where ProductStock.AccountId='{data.AccountId}' 
                            AND ProductStock.InstanceId='{data.InstanceId}' 
                            AND ProductStock.ProductId NOT IN(SELECT ProductId FROM ProductStock WHERE ProductStock.AccountId='{data.AccountId}' 
                            AND ProductStock.InstanceId='{data.InstanceId}' and ISNULL(ProductStock.Status,1) = 1)
                            AND ProductStock.Stock != 0 and ISNULL(ProductStock.Status,1) != 1
                            {NameQuery}";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            int count = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
            return count;
        }
        public async Task<int> ReorderItems(Product data)
        {
            var NameQuery = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (data.Name.ToString().Length == 1)
                    NameQuery = " and Product.Name like  '" + data.Name.ToString().Replace("'", "''") + "%' ";
                else
                    NameQuery = " and Product.Name =  '" + data.Name.ToString().Replace("'", "''") + "'";
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                NameQuery = "and Product.GenericName like '" + data.GenericName.ToString().Replace("'", "''") + "%'";
            }
            var query = $@"select COUNT(DISTINCT ProductStock.productId) from ProductStock WITH(NOLOCK) 
                            inner join Product WITH(NOLOCK) on Product.Id=ProductStock.ProductId 
                            left join InventoryReOrder I WITH(NOLOCK) ON I.ProductId = Product.Id
                            where ProductStock.AccountId='{data.AccountId}' AND ProductStock.InstanceId='{data.InstanceId}' 
                            AND ProductStock.Stock != 0 AND I.ReOrder > 0 and (ProductStock.Status is null or ProductStock.Status = 1)
                            {NameQuery}";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            int count = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
            return count;
        }
        public async Task<int> ZeroQty(Product data)
        {
            var NameQuery = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (data.Name.ToString().Length == 1)
                    NameQuery = " and Product.Name like  '" + data.Name.ToString().Replace("'", "''") + "%' ";
                else
                    NameQuery = " and Product.Name =  '" + data.Name.ToString().Replace("'", "''") + "'"; //NameQuery = " and p.Name like  '" + data.Name.ToString().Replace("'", "''") + "%' ";
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                NameQuery = " and Product.GenericName like  '" + data.GenericName.ToString().Replace("'", "''") + "%' ";
            }
            var query = $@"select count(distinct productid) from (
                            select ProductStock.productId from ProductStock WITH(NOLOCK) 
                            inner join Product WITH(NOLOCK) on Product.Id=ProductStock.ProductId 
                            where ProductStock.AccountId='{data.AccountId}' 
                            AND ProductStock.InstanceId='{data.InstanceId}' {NameQuery} 
                            and (ProductStock.Status is null or ProductStock.Status = 1)
                            group by ProductStock.productId having sum(ProductStock.Stock) = 0
                            ) a";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            int count = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
            return count;
        }
        public async Task<int> ExpiryStock(Product data)
        {
            var NameQuery = "";
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (data.Name.ToString().Length == 1)
                    NameQuery = " and Product.Name like  '" + data.Name.ToString().Replace("'", "''") + "%' ";
                else
                    NameQuery = " and Product.Name =  '" + data.Name.ToString().Replace("'", "''") + "'";
            }
            if (!string.IsNullOrEmpty(data.GenericName))
            {
                NameQuery = " and Product.GenericName like '" + data.GenericName.ToString().Replace("'", "''") + "%'";
            }
            var query = $@"select COUNT(DISTINCT ProductStock.productId) from ProductStock WITH(NOLOCK) 
                            inner join Product WITH(NOLOCK) on Product.Id=ProductStock.ProductId 
                            where ProductStock.AccountId='{data.AccountId}' 
                            AND ProductStock.InstanceId='{data.InstanceId}' 
                            and ProductStock.ExpireDate <=  getdate() and 
                            ProductStock.Stock != 0 AND (ProductStock.Status is null or ProductStock.Status = 1)
                            {NameQuery} ";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            int count = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
            return count;
        }
        public async Task<List<ProductStock>> GetStockData(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.AddColumn(ProductStockTable.IdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.GstTotalColumn, ProductStockTable.StockColumn, ProductStockTable.VendorIdColumn, ProductStockTable.MRPColumn,
                DbColumn.CustomColumn("Stock as ActualStock"));
            var statusCondition = new CustomCriteria(ProductStockTable.StatusColumn, CriteriaCondition.IsNull);
            string condition1 = "1";
            var cc1 = new CriteriaColumn(ProductStockTable.StatusColumn, condition1, CriteriaEquation.Equal);
            var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortBy(ProductTable.NameColumn);
            return await List<ProductStock>(qb);
        }
        public async Task<StockAdjustment> SaveStockAdjustment(StockAdjustment data)
        {
            data.TaxRefNo = (user.GSTEnabled() == "True" ? 1 : 0);
            return await Insert(data, StockAdjustmentTable.Table);
        }
        public async Task<ProductStock> UpdateProductStock_Old(ProductStock data)
        {
            return await Update(data, ProductStockTable.Table);
        }
        /// <summary>
        /// Update Productstock changed from direct qry to sp for handling deadlock 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ProductStock> UpdateProductStock(ProductStock data)
        {
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("Id", data.Id);
            param.Add("AccountId", data.AccountId);
            param.Add("InstanceId", data.InstanceId);
            param.Add("ProductId", data.ProductId);
            param.Add("VendorId", data.VendorId);
            param.Add("BatchNo", data.BatchNo);
            param.Add("ExpireDate", data.ExpireDate);
            param.Add("VAT", data.VAT);
            param.Add("TaxType", data.TaxType);
            param.Add("SellingPrice", data.SellingPrice);
            param.Add("MRP", data.MRP);
            param.Add("PurchaseBarcode", data.PurchaseBarcode);
            param.Add("Stock", data.Stock);
            param.Add("PackageSize", data.PackageSize);
            param.Add("PackagePurchasePrice", data.PackagePurchasePrice);
            param.Add("PurchasePrice", data.PurchasePrice);
            param.Add("OfflineStatus", data.OfflineStatus);
            param.Add("UpdatedAt", data.UpdatedAt);
            param.Add("UpdatedBy", data.UpdatedBy);
            param.Add("CST", data.CST);
            param.Add("Eancode", data.Eancode);
            param.Add("HsnCode", data.HsnCode);
            param.Add("Igst", data.Igst);
            param.Add("Cgst", data.Cgst);
            param.Add("Sgst", data.Sgst);
            param.Add("GstTotal", data.GstTotal);

            string procName = string.Empty;
            if (!data.WriteExecutionQuery)
            {
                var result = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_purchase_productstock_update", param);
            }

            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.StockColumn, ProductStockTable.VendorIdColumn, ProductStockTable.UpdatedAtColumn,
                ProductStockTable.ProductIdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.VATColumn,
                ProductStockTable.TaxTypeColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.MRPColumn, ProductStockTable.PurchaseBarcodeColumn,
                ProductStockTable.PackageSizeColumn, ProductStockTable.PurchasePriceColumn, ProductStockTable.OfflineStatusColumn,
                ProductStockTable.UpdatedByColumn, ProductStockTable.CSTColumn, ProductStockTable.HsnCodeColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn,
                ProductStockTable.SgstColumn, ProductStockTable.GstTotalColumn, ProductStockTable.EancodeColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn);
            param.Add("transQuantity", data.transQuantity);
            //added by nandhini for transaction
            if (data.WriteExecutionQuery)
            {
                qb.SetCommandType(CommandType.StoredProcedure);
                qb.SetProcName("Usp_purchase_productstock_update");
                param.ToList().ForEach(x => qb.Parameters.Add(x.Key, x.Value));
                data.AddExecutionQuery(qb);
            }
            else
            {

                WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = param, EventLevel = "I" });
            }
            //WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = "Exec Usp_purchase_productstock_update", Parameters = param, EventLevel = "I" });
            return data;
        }
        
        public async Task<IEnumerable<Product>> ListInventory(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductTable.InstanceIdColumn, data.InstanceId);
            qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.CodeColumn, ProductTable.ManufacturerColumn, ProductTable.RackNoColumn, ProductTable.StatusColumn);
            qb.JoinBuilder.LeftJoin(InventoryReOrderTable.Table, InventoryReOrderTable.ProductIdColumn, ProductTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(InventoryReOrderTable.Table, InventoryReOrderTable.ReOrderColumn, InventoryReOrderTable.IdColumn, InventoryReOrderTable.StatusColumn);
            qb = InventorySqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortBy(ProductTable.NameColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            var result1 = await List<Product>(qb);
            //var Is = await sqlUpdateCalculation(result1, data.InstanceId);
            var result = await List<Product>(qb);
            var vp = await SqlQueryList(result);
            var product = result.Select(x =>
            {
                x.ProductStock = vp.Where(y => y.ProductId == x.Id).Select(z => z);
                return x;
            });
            return product;
        }
        public async Task<List<ProductStock>> SqlQueryList(IEnumerable<Product> data)
        {
            var viqb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            viqb.AddColumn(ProductStockTable.IdColumn, ProductStockTable.ProductIdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.StockColumn, ProductStockTable.ReOrderQtyColumn, ProductStockTable.StatusColumn);
            viqb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, ProductStockTable.VendorIdColumn);
            viqb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.IdColumn);
            var i = 1;
            foreach (var product in data)
            {
                var paramName = $"s{i++}";
                var col = new CriteriaColumn(ProductStockTable.ProductIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(col);
                viqb.ConditionBuilder.Or(cc);
                viqb.Parameters.Add(paramName, product.Id);
            }
            var list = await List<ProductStock>(viqb);
            return list;
        }
        private QueryBuilderBase InventorySqlQueryBuilder(Product data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Name))
            {
                if (_configHelper.AppConfig.IsSqlServer)
                    qb.ConditionBuilder.And(ProductTable.NameColumn, data.Name, CriteriaEquation.Like,
                        CriteriaLike.Begin);
                else
                    qb.ConditionBuilder.And(ProductTable.NameColumn, $"{data.Name}%", CriteriaEquation.Like);
            }
            return qb;
        }
        public async Task<InventorySettings> EditInventorySetting(InventorySettings inventorySettings)
        {
            var query = $@"SELECT top 1 * FROM InventorySettings WHERE instanceId = '{inventorySettings.InstanceId}' order by CreatedAt Desc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = (await List<InventorySettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<IEnumerable<ProductStock>> sqlUpdateCalculation2(IEnumerable<ProductStock> data, string instanceId)
        {
            var query = $@"SELECT top 1 * FROM InventorySettings WHERE instanceId = '{instanceId}' order by CreatedAt Desc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = (await List<InventorySettings>(qb)).FirstOrDefault();
            if (result != null && result.NoOfDays != null)
            {
                foreach (var product in data)
                {
                    if (product.InventoryReOrder.Status != 1)
                    {
                        var getquery = $@"Select ps.ProductId as Id,(sum(si.Quantity)/90)*'{result.NoOfDays}' as ReorderQty,sum(distinct ps.Stock) as TotalStock from Sales s left join SalesItem si on s.Id=si.SalesId left join ProductStock ps on ps.Id=si.ProductStockId left join Product p on p.id=ps.ProductId where ps.ProductId='{product.Product.Id}' and s.InvoiceDate>=DATEADD(month,-3,GETDATE()) group by ps.ProductId";
                        var qbf = QueryBuilderFactory.GetQueryBuilder(getquery);
                        using (var reader = await QueryExecuter.QueryAsyc(qbf))
                        {
                            while (reader.Read())
                            {
                                var productStock = new ProductStock
                                {
                                    ReOrderQty = Convert.ToDecimal(reader["ReorderQty"].ToString()),
                                    Stock = Convert.ToDecimal(reader["TotalStock"].ToString()),
                                };
                                InventoryReOrder IRO = new InventoryReOrder();
                                IRO.InstanceId = result.InstanceId;
                                IRO.AccountId = result.AccountId;
                                IRO.ProductId = product.Product.Id;
                                IRO.ReOrder = decimal.Floor(productStock.ReOrderQty.Value);
                                IRO.ReOrder = IRO.ReOrder - productStock.Stock;
                                IRO.UpdatedBy = result.UpdatedBy;
                                IRO.CreatedBy = result.CreatedBy;
                                IRO.Status = 2;
                                if (IRO.ReOrder > 0)
                                {
                                    var qbcount = QueryBuilderFactory.GetQueryBuilder(InventoryReOrderTable.Table, OperationType.Count);
                                    qbcount.ConditionBuilder.And(InventoryReOrderTable.InstanceIdColumn, IRO.InstanceId);
                                    qbcount.ConditionBuilder.And(InventoryReOrderTable.ProductIdColumn, product.Product.Id);
                                    qbcount.ConditionBuilder.And(InventoryReOrderTable.StatusColumn, 2);
                                    int count = Convert.ToInt32(await SingleValueAsyc(qbcount));
                                    if (count == 0)
                                    {
                                        await Insert(IRO, InventoryReOrderTable.Table);
                                    }
                                    else
                                    {
                                        IRO.UpdatedAt = CustomDateTime.IST;
                                        var qbu = QueryBuilderFactory.GetQueryBuilder(InventoryReOrderTable.Table, OperationType.Update);
                                        qbu.ConditionBuilder.And(InventoryReOrderTable.InstanceIdColumn, IRO.InstanceId);
                                        qbu.ConditionBuilder.And(InventoryReOrderTable.ProductIdColumn, product.Product.Id);
                                        //qbu.AddColumn(InventoryReOrderTable.ReOrderColumn);
                                        qbu.Parameters.Add(InventoryReOrderTable.UpdatedAtColumn.ColumnName, IRO.UpdatedAt);
                                        qbu.Parameters.Add(InventoryReOrderTable.ReOrderColumn.ColumnName, IRO.ReOrder);
                                        qbu.Parameters.Add(InventoryReOrderTable.UpdatedByColumn.ColumnName, IRO.UpdatedBy);
                                        qbu.Parameters.Add(InventoryReOrderTable.StatusColumn.ColumnName, IRO.Status);
                                        qbu.Parameters.Add(InventoryReOrderTable.OfflineStatusColumn.ColumnName, result.OfflineStatus); // Offline status added by Santhiyagu
                                        // Added Gavaskar 11-04-2017
                                        await QueryExecuter.NonQueryAsyc(qbu);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return data;
        }
        public async Task<int> CountProduct(Product data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InventoryReOrderTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(InventoryReOrderTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(InventoryReOrderTable.IdColumn, data.Id);
            int count = Convert.ToInt32(await SingleValueAsyc(qb));
            return count;
        }
        public async Task<ProductStock> UpdateProductInventory(ProductStock data, string filtertype)
        {
            if (data.Product.ProductStock.Count() > 0)
            {
                data.UpdatedAt = CustomDateTime.IST;
                data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Update);
                qb.AddColumn(ProductTable.StatusColumn); /**Rack No update Removed*/
                qb.ConditionBuilder.And(ProductTable.IdColumn);
                qb.Parameters.Add(ProductTable.StatusColumn.ColumnName, data.Product.Status);
                qb.Parameters.Add(ProductTable.IdColumn.ColumnName, data.Product.Id);
                //qb.Parameters.Add(ProductTable.RackNoColumn.ColumnName, data.Product.RackNo);
                // Added Gavaskar 11-04-2017
                await QueryExecuter.NonQueryAsyc(qb);
                /*RackNo Upadate in Product Instance table*/
                /*BoxNo included by settu to update in Product Instance table*/
                /*Product Instance changes modified by Poongodi on 13/06/2017*/
                var pis = new ProductInstance
                {
                    AccountId = data.AccountId,
                    InstanceId = data.InstanceId,
                    ProductId = data.Product.Id,
                    RackNo = data.Product.RackNo,
                    BoxNo = data.Product.BoxNo,
                    ReOrderQty = data.InventoryReOrder.ReOrder,
                    UpdatedAt = CustomDateTime.IST,
                    UpdatedBy = data.UpdatedBy
                };
                var sreturn = await _productDataAccess.UpdateProductInstance(pis, Convert.ToString(filtertype).ToLower() == "reorder"
                              ? "reorder" : "rackno");

                /* var qbins = QueryBuilderFactory.GetQueryBuilder(ProductInstanceTable.Table, OperationType.Update);
                 if (Convert.ToString(filtertype).ToLower() == "reorder")
                 {
                     qbins.AddColumn(ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn, ProductInstanceTable.ReOrderQtyColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn);
                     qbins.Parameters.Add(ProductInstanceTable.ReOrderQtyColumn.ColumnName, data.InventoryReOrder.ReOrder);
                 }
                 else
                 {
                     qbins.AddColumn(ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn);
                 }
                 qbins.ConditionBuilder.And(ProductInstanceTable.ProductIdColumn);
                 qbins.ConditionBuilder.And(ProductInstanceTable.InstanceIdColumn);
                 qbins.ConditionBuilder.And(ProductInstanceTable.AccountIdColumn);
                 qbins.Parameters.Add(ProductInstanceTable.ProductIdColumn.ColumnName, data.Product.Id);
                 qbins.Parameters.Add(ProductInstanceTable.InstanceIdColumn.ColumnName, data.InstanceId);
                 qbins.Parameters.Add(ProductInstanceTable.AccountIdColumn.ColumnName, data.AccountId);
                 qbins.Parameters.Add(ProductInstanceTable.RackNoColumn.ColumnName, data.Product.RackNo);
                 qbins.Parameters.Add(ProductInstanceTable.BoxNoColumn.ColumnName, data.Product.BoxNo);
                 qbins.Parameters.Add(ProductInstanceTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                 qbins.Parameters.Add(ProductInstanceTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
                 //
                 // Added Gavaskar 11-04-2017
                 await QueryExecuter.NonQueryAsyc(qbins); */
                if (Convert.ToString(filtertype).ToLower() == "reorder")
                {
                    var qbio = QueryBuilderFactory.GetQueryBuilder(InventoryReOrderTable.Table, OperationType.Update);
                    qbio.AddColumn(InventoryReOrderTable.ReOrderColumn, InventoryReOrderTable.StatusColumn);
                    qbio.ConditionBuilder.And(InventoryReOrderTable.ProductIdColumn);
                    qbio.Parameters.Add(InventoryReOrderTable.ReOrderColumn.ColumnName, data.InventoryReOrder.ReOrder);
                    qbio.Parameters.Add(InventoryReOrderTable.ProductIdColumn.ColumnName, data.Product.Id);
                    qb.AddColumn(ProductInstanceTable.UpdatedAtColumn);
                    qb.Parameters.Add(ProductInstanceTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                    qb.AddColumn(ProductInstanceTable.UpdatedByColumn);
                    qb.Parameters.Add(ProductInstanceTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
                    if (data.InventoryReOrder.ReOrder > 0)
                    {
                        qbio.Parameters.Add(InventoryReOrderTable.StatusColumn.ColumnName, 1);
                    }
                    else
                    {
                        qbio.Parameters.Add(InventoryReOrderTable.StatusColumn.ColumnName, 2);
                    }
                    // Added Gavaskar 11-04-2017
                    await QueryExecuter.NonQueryAsyc(qbio);
                }
                foreach (var product in data.Product.ProductStock)
                {
                    if (data.Product.Status == 1)
                        data.Product.Status = null;
                    data.UpdatedAt = CustomDateTime.IST;
                    data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
                    var qbps = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                    qbps.AddColumn(ProductStockTable.StatusColumn);
                    qbps.ConditionBuilder.And(ProductStockTable.IdColumn);
                    qbps.Parameters.Add(ProductStockTable.StatusColumn.ColumnName, data.Product.Status);
                    qbps.Parameters.Add(ProductStockTable.IdColumn.ColumnName, product.Id);
                    // Added Gavaskar 11-04-2017
                    await QueryExecuter.NonQueryAsyc(qbps);
                }
            }
            else
            {
                data.UpdatedAt = CustomDateTime.IST;
                data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Update);
                qb.AddColumn(ProductTable.StatusColumn);
                qb.ConditionBuilder.And(ProductTable.IdColumn);
                qb.Parameters.Add(ProductTable.StatusColumn.ColumnName, data.Product.Status);
                qb.Parameters.Add(ProductTable.IdColumn.ColumnName, data.Product.Id);
                //qb.Parameters.Add(ProductTable.RackNoColumn.ColumnName, data.Product.RackNo);
                // Added Gavaskar 11-04-2017
                await QueryExecuter.NonQueryAsyc(qb);
                /*RackNo Upadate in Product Instance table*/
                /*BoxNo included by settu to update in Product Instance table*/
                /*Product Instance changes modified by Poongodi on 13/06/2017*/
                var pis = new ProductInstance
                {
                    AccountId = data.AccountId,
                    InstanceId = data.InstanceId,
                    ProductId = data.Product.Id,
                    RackNo = data.Product.RackNo,
                    BoxNo = data.Product.BoxNo,
                    ReOrderQty = data.InventoryReOrder.ReOrder,
                    UpdatedAt = CustomDateTime.IST,
                    UpdatedBy = data.UpdatedBy
                };
                var sreturn = await _productDataAccess.UpdateProductInstance(pis, Convert.ToString(filtertype).ToLower() == "reorder"
                              ? "reorder" : "rackno");
                /* var qbins = QueryBuilderFactory.GetQueryBuilder(ProductInstanceTable.Table, OperationType.Update);
                 if (Convert.ToString(filtertype).ToLower() == "reorder")
                 {
                     qbins.AddColumn(ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn, ProductInstanceTable.ReOrderQtyColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn);
                     qbins.Parameters.Add(ProductInstanceTable.ReOrderQtyColumn.ColumnName, data.InventoryReOrder.ReOrder);
                 }
                 else
                 {
                     qbins.AddColumn(ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn, ProductInstanceTable.UpdatedAtColumn, ProductInstanceTable.UpdatedByColumn);
                 }
                 qbins.ConditionBuilder.And(ProductInstanceTable.ProductIdColumn);
                 qbins.ConditionBuilder.And(ProductInstanceTable.InstanceIdColumn);
                 qbins.ConditionBuilder.And(ProductInstanceTable.AccountIdColumn);
                 qbins.Parameters.Add(ProductInstanceTable.ProductIdColumn.ColumnName, data.Product.Id);
                 qbins.Parameters.Add(ProductInstanceTable.InstanceIdColumn.ColumnName, data.InstanceId);
                 qbins.Parameters.Add(ProductInstanceTable.AccountIdColumn.ColumnName, data.AccountId);
                 qbins.Parameters.Add(ProductInstanceTable.RackNoColumn.ColumnName, data.Product.RackNo);
                 qbins.Parameters.Add(ProductInstanceTable.BoxNoColumn.ColumnName, data.Product.BoxNo);
                 qbins.Parameters.Add(ProductInstanceTable.ReOrderQtyColumn.ColumnName, data.InventoryReOrder.ReOrder);
                 qbins.Parameters.Add(ProductInstanceTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                 qbins.Parameters.Add(ProductInstanceTable.UpdatedByColumn.ColumnName, data.UpdatedBy);*/

                if (Convert.ToString(filtertype).ToLower() == "reorder")
                {
                    var qbio = QueryBuilderFactory.GetQueryBuilder(InventoryReOrderTable.Table, OperationType.Update);
                    qbio.AddColumn(InventoryReOrderTable.ReOrderColumn, InventoryReOrderTable.StatusColumn);
                    qbio.ConditionBuilder.And(InventoryReOrderTable.ProductIdColumn);
                    qbio.Parameters.Add(InventoryReOrderTable.ReOrderColumn.ColumnName, data.InventoryReOrder.ReOrder);
                    qbio.Parameters.Add(InventoryReOrderTable.ProductIdColumn.ColumnName, data.Product.Id);
                    if (data.InventoryReOrder.ReOrder > 0)
                    {
                        qbio.Parameters.Add(InventoryReOrderTable.StatusColumn.ColumnName, 1);
                    }
                    else
                    {
                        qbio.Parameters.Add(InventoryReOrderTable.StatusColumn.ColumnName, 2);
                    }
                    // Added Gavaskar 11-04-2017
                    await QueryExecuter.NonQueryAsyc(qbio);
                }
                if (data.Product.Status == 1)
                    data.Product.Status = null;
                data.UpdatedAt = CustomDateTime.IST;
                data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
                var qbps = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                qbps.AddColumn(ProductStockTable.StatusColumn);
                qbps.ConditionBuilder.And(ProductStockTable.ProductIdColumn);
                qbps.Parameters.Add(ProductStockTable.StatusColumn.ColumnName, data.Product.Status);
                qbps.Parameters.Add(ProductStockTable.ProductIdColumn.ColumnName, data.Product.Id);
                // Added Gavaskar 11-04-2017
                await QueryExecuter.NonQueryAsyc(qbps);
            }
            return data;
        }
        public async Task<ProductStock> UpdateSingleProductStock(ProductStock data)
        {
            if (data.Status == 1)
                data.Status = null;
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.StatusColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn);
            qb.Parameters.Add(ProductStockTable.StatusColumn.ColumnName, data.Status);
            qb.Parameters.Add(ProductStockTable.IdColumn.ColumnName, data.Id);
            // Added Gavaskar 11-04-2017
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }
        public async Task<List<Product>> TempStockProductList(string productName, string instanceId, string accountId)
        {
            var conditionquery = "";
            if (productName.Length <= 2)
            {
                conditionquery = "top 20 ";
            }
            var query = $@"Select distinct {conditionquery} p.Name,p.Id from TempVendorPurchaseItem T Inner join ProductStock ps ON T.ProductStockId=ps.id 
	 Inner join Product p ON p.id=ps.ProductId Where T.InstanceId='{instanceId}' AND T.AccountId  ='{accountId}' and p.Name like '{productName}%'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<Product>(qb);
            return result;
        }
        public async Task<List<Product>> GetEancodeExistData(string eancode, string instanceId, string accountId)
        {
            var result = await GetBarcodeExistData(eancode, instanceId, accountId);
            if (result.Count() > 0)
            {
                return result;
            }

            eancode = "'" + eancode.Replace("'", "''") + "'";
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.JoinBuilder.LeftJoin(ProductStockTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.ConditionBuilder.And(ProductTable.AccountIdColumn, accountId);
            var statusCondition = new CustomCriteria(ProductTable.StatusColumn, CriteriaCondition.IsNull);
            var cc = new CriteriaColumn(ProductTable.StatusColumn, "1", CriteriaEquation.Equal);
            var col = new CustomCriteria(cc, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col);
            var statusCondition1 = new CriteriaColumn(ProductStockTable.EancodeColumn, eancode, CriteriaEquation.Equal);
            var cc1 = new CriteriaColumn(ProductTable.EancodeColumn, eancode, CriteriaEquation.Equal);
            var col1 = new CustomCriteria(cc1, statusCondition1, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);
            qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.EancodeColumn, ProductStockTable.EancodeColumn);
            qb.SelectBuilder.MakeDistinct = true;
            result = (await List<Product>(qb)).ToList();
            return result;
        }
        //Added by Settu on 04/07/17 for physical stock verification popup implementation
        public async Task<List<PhysicalStockHistory>> getPhysicalStockProducts(string AccountId, string InstanceId)
        {
            InventorySettings popup = new InventorySettings();
            popup.InstanceId = InstanceId;
            popup = await EditInventorySetting(popup);

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_PhysicalStockProducts", parms);
            var product = result.Select(x =>
            {
                var physicalStock = new PhysicalStockHistory
                {
                    ProductId = x.Id,
                    CurrentStock = x.Stock,
                    Product = {
                        Name = x.Name ,
                        Id = x.Id,
                        RackNo = x.RackNo,
                        BoxNo = x.BoxNo,
                    },
                    OpenedAt = popup.OpenedPopup == true ? popup.LastPopupAt : CustomDateTime.IST,
                };
                return physicalStock;
            });
            return product.ToList();
        }

        public async Task<bool> updatePhysicalStockHistory(List<PhysicalStockHistory> list)
        {
            InventorySettings popup = new InventorySettings();
            popup.InstanceId = list[0].InstanceId;
            popup = await EditInventorySetting(popup);
            if (popup.OpenedPopup == true)
            {
                popup.OpenedPopup = false;
                await UpdatePhysicalStockPopupSettings(popup);
                var popupNo = await GetPhysicalStockHistoryPopupNo(list[0].AccountId, list[0].InstanceId);
                foreach (var data in list)
                {
                    data.ClosedAt = CustomDateTime.IST;
                    data.PopupNo = popupNo;
                    await Insert(data, PhysicalStockHistoryTable.Table);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<InventorySettings> UpdatePhysicalStockPopupSettings(InventorySettings data)
        {
            if (data.Id != null)
            {
                data.UpdatedAt = CustomDateTime.IST;
                data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
                var qb = QueryBuilderFactory.GetQueryBuilder(InventorySettingsTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(InventorySettingsTable.IdColumn, data.Id);
                qb.ConditionBuilder.And(InventorySettingsTable.InstanceIdColumn, data.InstanceId);
                qb.AddColumn(InventorySettingsTable.LastPopupAtColumn, InventorySettingsTable.OpenedPopupColumn, InventorySettingsTable.UpdatedAtColumn, InventorySettingsTable.OfflineStatusColumn);
                qb.Parameters.Add(InventorySettingsTable.LastPopupAtColumn.ColumnName, data.LastPopupAt);
                qb.Parameters.Add(InventorySettingsTable.OpenedPopupColumn.ColumnName, data.OpenedPopup);
                qb.Parameters.Add(InventorySettingsTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
                qb.Parameters.Add(InventorySettingsTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
                await QueryExecuter.NonQueryAsyc(qb);
            }
            return data;
        }

        private async Task<int> GetPhysicalStockHistoryPopupNo(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PhysicalStockHistoryTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PhysicalStockHistoryTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(PhysicalStockHistoryTable.InstanceIdColumn, InstanceId);
            qb.AddColumn(PhysicalStockHistoryTable.PopupNoColumn);
            qb.SelectBuilder.SortByDesc(PhysicalStockHistoryTable.PopupNoColumn);
            qb.SelectBuilder.SetTop(1);
            var num = (await QueryExecuter.SingleValueAsyc(qb));
            return (int)(num == null ? 1 : num) + 1;
        }

        public async Task<List<ProductStock>> ValidateStock(string AccountId, string InstanceId, string ProductStockIdsWithRequestedStock)
        {
            //Example: ProductStockIdsWithRequestedStock = "000012a9-e5f1-4ca2-9e87-e61e5ddb03a6~10,0000520f-d8a8-4978-bfd7-5f76953de527~1";

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("StockIds", ProductStockIdsWithRequestedStock);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_ValidateStock", parms);
            var product = result.Select(x =>
            {
                var productStock = new ProductStock
                {
                    Id = x.ProductStockId,
                    ProductId = x.ProductId,
                    Product = {
                        Name = x.Name ,
                        Id = x.ProductId,
                    },
                    Stock = x.Stock,
                    soldqty = x.RequiredStock,
                    BatchNo = x.BatchNo,
                };
                return productStock;
            });
            return product.ToList();
        }
        /// <summary>
        /// Closing stock Scheduler added by Poongodi on 20/10/2017
        /// </summary>
        /// <param name="AccountId"></param>
        /// <param name="InstanceId"></param>
        /// <returns></returns>
        public async Task<bool> ClosingStockScheduler(string AccountId, string InstanceId,string sUserId )
        {
           
                bool bOffline = _configHelper.AppConfig.OfflineMode;
                Dictionary<string, object> parms = new Dictionary<string, object>();
                parms.Add("AccountId", AccountId);
                parms.Add("InstanceId", InstanceId);
                parms.Add("Offline", bOffline);
                parms.Add("TransDate", DateTime.Now);
                parms.Add("CreatedBy", sUserId);
                var result = await sqldb.ExecuteProcedureAsync<ClosingStock>("usp_closingStock_Scheduler", parms);
                if (bOffline && result != null && Convert.ToString(result.FirstOrDefault().stockxml) != "")
                {
                    parms.Remove("Offline");
                    parms.Remove("TransDate");
                    parms.Remove("CreatedBy");
                    WriteXMLToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = result.FirstOrDefault().stockxml.ToString(), Parameters = parms, EventLevel = "I", FileType = "xml" }, "_xml");
                }


            return true;
        }

        public async Task<bool> ImportStockOnline(List<ProductStock> productStock, string instanceId, string accountId, string userId, int isCloud, string filename)
        {
            try
            {
                List<SqlDataRecord> dataTable = new List<SqlDataRecord>();
                List<SqlMetaData> sqlMetaData = new List<SqlMetaData>();
                sqlMetaData.Add(new SqlMetaData("ProductName", SqlDbType.VarChar, 1000));
                sqlMetaData.Add(new SqlMetaData("HsnCode", SqlDbType.VarChar, 50));
                sqlMetaData.Add(new SqlMetaData("GstTotal", SqlDbType.Decimal));
                sqlMetaData.Add(new SqlMetaData("ExpireDate(dd - MMM - yyyy)", SqlDbType.Date));
                sqlMetaData.Add(new SqlMetaData("BatchNo", SqlDbType.VarChar, 1000));
                sqlMetaData.Add(new SqlMetaData("Units/Strip", SqlDbType.Decimal));
                sqlMetaData.Add(new SqlMetaData("Price/Strip", SqlDbType.Decimal));
                sqlMetaData.Add(new SqlMetaData("MRP/Strip", SqlDbType.Decimal));
                sqlMetaData.Add(new SqlMetaData("Barcode", SqlDbType.VarChar, 50));
                sqlMetaData.Add(new SqlMetaData("stock", SqlDbType.Decimal));
                sqlMetaData.Add(new SqlMetaData("FreeQty", SqlDbType.Int));
                sqlMetaData.Add(new SqlMetaData("Vendor Name", SqlDbType.VarChar, 100));
                sqlMetaData.Add(new SqlMetaData("Category", SqlDbType.VarChar, 250));
                sqlMetaData.Add(new SqlMetaData("Generic Name", SqlDbType.VarChar, 250));
                sqlMetaData.Add(new SqlMetaData("Schedule", SqlDbType.VarChar, 250));
                sqlMetaData.Add(new SqlMetaData("Type", SqlDbType.VarChar, 250));
                sqlMetaData.Add(new SqlMetaData("Manufacturer", SqlDbType.VarChar, 300));
                sqlMetaData.Add(new SqlMetaData("RackNo", SqlDbType.VarChar, 50));
                sqlMetaData.Add(new SqlMetaData("BoxNo", SqlDbType.VarChar, 15));

                foreach (var ps in productStock)
                {
                    SqlDataRecord row = new SqlDataRecord(sqlMetaData.ToArray());
                    row.SetValues(new object[] { ps.Product.Name,
                                                ps.Product.HsnCode,
                                                ps.Product.GstTotal,
                                                ps.ExpireDate,
                                                ps.BatchNo,
                                                ps.PackageSize,
                                                ps.PackagePurchasePrice,
                                                ps.MRP,
                                                ps.Eancode,
                                                ps.Stock,
                                                0, //FreeQty
                                                ps.Vendor.Name,
                                                ps.Product.Category,
                                                ps.Product.GenericName,
                                                ps.Product.Schedule,
                                                ps.Product.Type,
                                                ps.Product.Manufacturer,
                                                ps.Product.RackNo,
                                                ps.Product.BoxNo
                                            });
                    dataTable.Add(row);

                }

                Dictionary<string, object> parms = new Dictionary<string, object>();
                parms.Add("AccountID", accountId);
                parms.Add("InstanceID", instanceId);
                parms.Add("FilePath", filename);
                parms.Add("IsCloud", isCloud);
                parms.Add("Createdby", userId);
                parms.Add("StockData", dataTable);             

                var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_importproductstock", parms);
                // var result = await _etlDataAccess.ImportProductStock(accountId, instanceId, "", isCloud, userId, dataTable);  
                return await Task.FromResult(true);          
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> updateBarcodeProfile(BarcodeProfile data)
        {
            if (data.Id == null)
            {
                await Insert(data, BarcodeProfileTable.Table);
                return true;
            }
            else
            {
                await Update(data, BarcodeProfileTable.Table);
                return true;
            }
        }

        public async Task<List<BarcodeProfile>> getBarcodeProfileList(string name)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(BarcodeProfileTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(BarcodeProfileTable.AccountIdColumn, user.AccountId());
            if (!string.IsNullOrEmpty(name))
                qb.ConditionBuilder.And(BarcodeProfileTable.ProfileNameColumn, name, CriteriaEquation.Like, CriteriaLike.Begin);
            var result = (await List<BarcodeProfile>(qb)).ToList();
            foreach (var item in result)
            {
                qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
                qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, user.AccountId());
                qb.ConditionBuilder.And(ProductStockTable.BarcodeProfileIdColumn, item.Id);
                qb.AddColumn(ProductStockTable.BarcodeProfileIdColumn);
                qb.SelectBuilder.SetTop(1);
                var result1 = (await List<ProductStock>(qb)).FirstOrDefault();
                if (result1 != null)
                {
                    item.IsTransactedProfile = true;
                }
            }
            return result;
        }

        public async Task<BarcodeProfile> getBarcodeProfileById(string Id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(BarcodeProfileTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(BarcodeProfileTable.IdColumn, Id);
            var result = await List<BarcodeProfile>(qb);
            return result.First();
        }

        public async Task<bool> updateBarcodePrnDesign(BarcodePrnDesign data)
        {
            if (data.Id == null)
            {
                await Insert(data, BarcodePrnDesignTable.Table);
                return true;
            }
            else
            {
                await Update(data, BarcodePrnDesignTable.Table);
                return true;
            }
        }

        public async Task<List<BarcodePrnDesign>> getBarcodePrnDesignList(string name)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(BarcodePrnDesignTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(BarcodePrnDesignTable.AccountIdColumn, user.AccountId());
            if (!string.IsNullOrEmpty(name))
                qb.ConditionBuilder.And(BarcodePrnDesignTable.PrnNameColumn, name, CriteriaEquation.Like, CriteriaLike.Begin);
            var result = (await List<BarcodePrnDesign>(qb)).ToList();
            return result;
        }

        public async Task<List<ProductStock>> generateBarcode(List<ProductStock> productStock, string BarcodeProfileId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, user.AccountId());
            qb.ConditionBuilder.And(InstanceTable.IdColumn, user.InstanceId());
            qb.AddColumn(InstanceTable.CodeColumn);
            var result1 = (await List<Instance>(qb)).FirstOrDefault();
            var InstanceCode = "";
            if (result1 != null)
            {
                InstanceCode = result1.Code;
            }

            var profile = await getBarcodeProfileById(BarcodeProfileId);
            var settings = JsonConvert.DeserializeObject<List<BarcodeProfileParticulars>>(profile.ProfileJson);
            settings = settings.Where(x => x.SetYesNo == "Y").OrderBy(x => x.Order).ToList();

            var productWise = settings.Where(x => x.SetYesNo == "Y" && x.Tag == "productCode").OrderBy(x => x.Order).ToList();
            var productWiseGeneration = false;
            if (settings.Count() == productWise.Count())
            {
                productWiseGeneration = true;
            }

            foreach (var item in productStock)
            {
                if (string.IsNullOrEmpty(item.PurchaseBarcode))
                {
                    var barcodeValue = "";
                    if (productWiseGeneration == false)
                    {
                        if (_configHelper.AppConfig.OfflineMode)
                        {
                            barcodeValue = "1";
                        }
                        else
                        {
                            barcodeValue = "0";
                        }
                    }
                    foreach (var fld in settings)
                    {
                        if (fld.Tag == "productCode")
                        {
                            barcodeValue = barcodeValue + item.Product.Code.PadLeft(fld.MaxLength as int? ?? 0, '0').Substring(0, fld.MaxLength as int? ?? 0);
                        }
                        else if (fld.Tag == "batchNo")
                        {
                            var batchNo = item.BatchNo.PadLeft(fld.MaxLength as int? ?? 0, '0');
                            barcodeValue = barcodeValue + batchNo.Substring((batchNo.Length - fld.MaxLength as int? ?? 0), fld.MaxLength as int? ?? 0);
                        }
                        else if (fld.Tag == "expireDate")
                        {
                            barcodeValue = barcodeValue + (item.ExpireDate as DateTime? ?? DateTime.Now).ToString("MMyy").PadLeft(fld.MaxLength as int? ?? 0, '0').Substring(0, fld.MaxLength as int? ?? 0);
                        }
                        else if (fld.Tag == "transactionDate")
                        {
                            barcodeValue = barcodeValue + item.CreatedAt.ToString("ddMMyy").PadLeft(fld.MaxLength as int? ?? 0, '0').Substring(0, fld.MaxLength as int? ?? 0);
                        }
                        else if (fld.Tag == "monthYearTransactionDate")
                        {
                            barcodeValue = barcodeValue + item.CreatedAt.ToString("MMyy").PadLeft(fld.MaxLength as int? ?? 0, '0').Substring(0, fld.MaxLength as int? ?? 0);
                        }
                    }

                    if (productWiseGeneration == false)
                    {
                        barcodeValue = barcodeValue + InstanceCode;
                        var num = await getBarcodeSequence(barcodeValue, profile);
                        barcodeValue = barcodeValue + num;
                    }
                    item.PurchaseBarcode = barcodeValue;
                    item.BarcodeProfileId = BarcodeProfileId;
                    await UpdateCustomBarcode(item);
                }
            }
            return productStock;
        }

        public async Task<string> getBarcodeSequence(string barcodeValue, BarcodeProfile profile)
        {
            var query = $"SELECT ISNULL(MAX(CAST(REPLACE(PurchaseBarcode, '{barcodeValue}', '') AS INT)), 0)+1 FROM ProductStock (NOLOCK) WHERE AccountId = '{user.AccountId()}' AND InstanceId = '{user.InstanceId()}' AND BarcodeProfileId = '{profile.Id}' AND PurchaseBarcode LIKE '{barcodeValue}%'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var num = (await QueryExecuter.SingleValueAsyc(qb));
            return num.ToString();
        }

        public async Task UpdateCustomBarcode(ProductStock data)
        {
            data.UpdatedBy = user.Identity.Id();
            data.UpdatedAt = CustomDateTime.IST;
            data.OfflineStatus = _configHelper.AppConfig.OfflineMode;
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qb.AddColumn(ProductStockTable.PurchaseBarcodeColumn, ProductStockTable.BarcodeProfileIdColumn, ProductStockTable.UpdatedAtColumn, ProductStockTable.UpdatedByColumn, ProductStockTable.OfflineStatusColumn);
            qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, user.AccountId());
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, user.InstanceId());
            qb.ConditionBuilder.And(ProductStockTable.IdColumn);
            qb.Parameters.Add(ProductStockTable.IdColumn.ColumnName, data.Id);
            qb.Parameters.Add(ProductStockTable.PurchaseBarcodeColumn.ColumnName, data.PurchaseBarcode);
            qb.Parameters.Add(ProductStockTable.BarcodeProfileIdColumn.ColumnName, data.BarcodeProfileId);
            qb.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, data.UpdatedAt);
            qb.Parameters.Add(ProductStockTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.Parameters.Add(ProductStockTable.OfflineStatusColumn.ColumnName, data.OfflineStatus);
            await QueryExecuter.NonQueryAsyc(qb);
        }

        public async Task<List<Product>> GetBarcodeExistData(string eancode, string instanceId, string accountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn);
            qb.ConditionBuilder.And(ProductTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(ProductStockTable.PurchaseBarcodeColumn, eancode);
            var result = (await List<Product>(qb)).ToList();
            return result;
        }

        public async Task<string> GetProductStockInstanceId()
        {
            var query = $"SELECT TOP 1 InstanceId FROM ProductStock (nolock) UNION SELECT TOP 1 InstanceId FROM SyncDataSeed(nolock)"; //Nolock added by poongodi on 30112018
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var InstanceId = (await QueryExecuter.SingleValueAsyc(qb));
            return InstanceId as string ?? "";
        }
		// Added by Gavaskar Kind Of Product Master Settings Start 14-12-2017
        public async Task<List<KindProductMaster>> GetKindOfProductList(string accountId, string type)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("Type", type);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_KindOfProductMaster", parms);
            var KindProductList = result.Select(x =>
            {
                var kindProductMaster = new KindProductMaster
                {
                    Id =x.Id,
                    AccountId = accountId,
                    KindName = x.KindName,
                    IsActive = x.IsActive
                };

                return kindProductMaster;
            });

            return KindProductList.ToList();
        }
        // Added by Gavaskar Kind Of Product Master Settings End 14-12-2017

    }

    public class BarcodeProfileParticulars
    {
        public int? SeqId { get; set; }
        public string Field { get; set; }
        public string SetYesNo { get; set; }
        public int? Order { get; set; }
        public int? MaxLength { get; set; }
        public string Tag { get; set; }

    }

    

}
