﻿using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Enum;
using DataAccess.Criteria;
using HQue.Contract.Infrastructure.Master;
using HQue.DataAccess.Helpers.Extension;
using DataAccess;
using Utilities.Helpers;
using HQue.DataAccess.Helpers;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.External;
using System.Data.SqlClient;
using Newtonsoft.Json;
using HQue.Contract.Base;
using System.Text.RegularExpressions;

namespace HQue.DataAccess.Inventory
{
    public class VendorOrderDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        private readonly HelperDataAccess _HelperDataAccess;
        private readonly ProductDataAccess _productDataAccess;
        public VendorOrderDataAccess(ISqlHelper sqlQueryExecuter, HelperDataAccess helperDataAccess, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper, ProductDataAccess productDataAccess) : base(sqlQueryExecuter, queryBuilderFactory)
        {

            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
            _HelperDataAccess = helperDataAccess; //Added by Poongodi on 26/03/2017
            _productDataAccess = productDataAccess;
        }

        public override Task<VendorOrder> Save<VendorOrder>(VendorOrder data)
        {
            return Insert(data, VendorOrderTable.Table);
        }

        //Created by Sarubala on 21-08-18 for sync in vendor order
        public Task<VendorOrder> SaveVendorOrder(VendorOrder data)
        {
            if (data.WriteExecutionQuery)
            {
                SetInsertMetaData(data, VendorOrderTable.Table);
                WriteInsertExecutionQuery(data, VendorOrderTable.Table);

            }
            else
            {
                return Insert2(data, VendorOrderTable.Table);
            }
            return Task.FromResult(data);

        }

        public async Task<IEnumerable<VendorOrderItem>> Save(IEnumerable<VendorOrderItem> itemList)
        {
            foreach (var vendorOrderItem in itemList)
            {
                if (vendorOrderItem.WriteExecutionQuery)
                {
                    SetInsertMetaData(vendorOrderItem, VendorOrderItemTable.Table);
                    WriteInsertExecutionQuery(vendorOrderItem, VendorOrderItemTable.Table);
                }
                else
                {
                    await Insert(vendorOrderItem, VendorOrderItemTable.Table);

                }

            }
            return itemList;
        }

        public async Task<IEnumerable<VendorOrder>> List(VendorOrder data, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortByDesc(VendorOrderTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);

            var result = await List<VendorOrder>(qb);

            var query = result.GroupBy(x => x.Id).Select(y => y.FirstOrDefault());
            var voi = await GetVendorOrderItemsList(query, instanceid);

            var vendorOrder = query.Select(x =>
            {
                x.VendorOrderItem = voi.Where(y => y.VendorOrderId == x.Id).OrderBy(y => y.OrderItemSno).ToList();
                return x;
            });

            var res = vendorOrder.Select(item =>
            {

                string mobile = string.Empty;

                if (!string.IsNullOrEmpty(item.Vendor.Mobile) && string.IsNullOrEmpty(item.Vendor.Mobile1) && string.IsNullOrEmpty(item.Vendor.Mobile2))
                {
                    mobile = item.Vendor.Mobile;
                }

                if (string.IsNullOrEmpty(item.Vendor.Mobile) && !string.IsNullOrEmpty(item.Vendor.Mobile1) && string.IsNullOrEmpty(item.Vendor.Mobile2))
                {
                    mobile = item.Vendor.Mobile1;
                }

                if (string.IsNullOrEmpty(item.Vendor.Mobile) && string.IsNullOrEmpty(item.Vendor.Mobile1) && !string.IsNullOrEmpty(item.Vendor.Mobile2))
                {
                    mobile = item.Vendor.Mobile2;
                }

                if (!string.IsNullOrEmpty(item.Vendor.Mobile) && !string.IsNullOrEmpty(item.Vendor.Mobile1) && !string.IsNullOrEmpty(item.Vendor.Mobile2))
                {
                    mobile = item.Vendor.Mobile;
                }

                item.Vendor.Mobile = mobile;

                item.Vendor.Email = !string.IsNullOrEmpty(item.Vendor.Email) ? item.Vendor.Email : "";

                return item;
            });
            return res;
        }


        //add by nandhini
        public async Task<OrderSettings> GetOrderSettings(string AccId, string InstId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(OrderSettingsTable.Table, OperationType.Select);
            qb.AddColumn(OrderSettingsTable.NoOfDaysFromDateColumn, OrderSettingsTable.DateSettingTypeColumn);
            qb.ConditionBuilder.And(OrderSettingsTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(OrderSettingsTable.InstanceIdColumn, InstId);
            var result = (await List<OrderSettings>(qb)).FirstOrDefault();
            if (result != null)
            {
                if (result.DateSettingType == null)
                {
                    result.DateSettingType = 1;
                }
            }
            else
            {
                result = new OrderSettings();
                result.DateSettingType = 1;
            }

            return result;
        }

        public async Task<string> GetHeadOfficeVendorId(string AccId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VendorTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(VendorTable.IsHeadOfficeVendorColumn, true);
            qb.AddColumn(VendorTable.IdColumn);
            var res = (await List<Vendor>(qb)).FirstOrDefault();

            return res.Id;
        }

        //Added by Sarubala on 24-11-18 - Start    

        public async Task<OrderSettings> GetHeadOfficeOrderSettings(string AccId, string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.AddColumn(InstanceTable.IdColumn, InstanceTable.isHeadOfficeColumn, InstanceTable.NameColumn);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(InstanceTable.isHeadOfficeColumn, true);

            var result = (await List<Instance>(qb)).FirstOrDefault();

            OrderSettings setting = new OrderSettings();

            if(result != null)
            {
                setting.IsHeadOfficeEnabled = result.isHeadOffice;
                setting.HeadOffice_InstanceName = result.Name;
                setting.HeadOffice_InstanceId = result.Id;
            }
            else
            {
                setting.IsHeadOfficeEnabled = false;
            }
            
            return setting;
        }

        public async Task<List<Instance>> GetInstanceList(string AccId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.AddColumn(InstanceTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, AccId);
            var result = (await List<Instance>(qb));

            return result;
        }

        public async Task<OrderSettings> saveHeadOfficeSettings(OrderSettings setting)
        {
            var result = await UpdateHeadOfficeInstance(setting.AccountId, setting.HeadOffice_InstanceId);        

            return setting;

        }

        public async Task<bool> UpdateHeadOfficeInstance(string AccId, string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Update);
            qb.AddColumn(InstanceTable.isHeadOfficeColumn, InstanceTable.UpdatedAtColumn);
            qb.Parameters.Add(InstanceTable.isHeadOfficeColumn.ColumnName, true);
            qb.Parameters.Add(InstanceTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, InsId);

            var result = await QueryExecuter.NonQueryAsyc(qb);
            WriteToSyncQueue(new SyncObject() { Id = InsId, QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "A" });
            return result;
        }

        //Added by Sarubala on 24-11-18 - End

        public async Task<OrderSettings> SaveOrderSettings(OrderSettings data)
        {
            var count = await CountOrderSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(OrderSettingsTable.Table, OperationType.Update);
                qb.AddColumn(OrderSettingsTable.DateSettingTypeColumn, OrderSettingsTable.NoOfDaysFromDateColumn, OrderSettingsTable.UpdatedAtColumn, OrderSettingsTable.UpdatedByColumn);
                qb.Parameters.Add(OrderSettingsTable.NoOfDaysFromDateColumn.ColumnName, data.NoOfDaysFromDate);
                qb.Parameters.Add(OrderSettingsTable.DateSettingTypeColumn.ColumnName, data.DateSettingType);
                qb.Parameters.Add(OrderSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(OrderSettingsTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
                qb.ConditionBuilder.And(OrderSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(OrderSettingsTable.InstanceIdColumn, data.InstanceId);

                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, OrderSettingsTable.Table);
            }

        }

        public async Task<int> CountOrderSettings(OrderSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(OrderSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(OrderSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(OrderSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        //end


        public async Task<List<VendorOrderItem>> GetVendorOrderItemsList(IEnumerable<VendorOrder> data, string instanceid)
        {
            var voiqb = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Select);
            voiqb.AddColumn(VendorOrderItemTable.Table.ColumnList.ToArray());
            voiqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, VendorOrderItemTable.ProductIdColumn);
            voiqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());

            var i = 1;
            foreach (var vendroOrder in data)
            {
                var paramName = $"s{i++}";
                var caol = new CriteriaColumn(VendorOrderItemTable.VendorOrderIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(caol);
                voiqb.ConditionBuilder.Or(cc);
                voiqb.Parameters.Add(paramName, vendroOrder.Id);
            }
            var list = new List<VendorOrderItem>();
            using (var reader = await QueryExecuter.QueryAsyc(voiqb))
            {
                while (reader.Read())
                {
                    var vendorOrderItem = new VendorOrderItem().Fill(reader);
                    vendorOrderItem.Product.Fill(reader);
                    list.Add(vendorOrderItem);
                }
            }
            var result = await GetVendorPurchaseItemlist(instanceid);
            foreach (var resultitem in result)
            {
                foreach (var listitem in list)
                {
                    if (resultitem.ProductStock.ProductId == listitem.ProductId)
                    {
                        listitem.Product.Discount = resultitem.Discount;
                        listitem.Product.purchaseprice = resultitem.PurchasePrice * resultitem.PackageSize;
                    }
                }
            }
            return list;
        }
        public async Task<List<VendorPurchaseItem>> GetVendorPurchaseItemlist(string instanceid)
        {
            var voiqb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
            voiqb.AddColumn(VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.CreatedAtColumn, VendorPurchaseItemTable.PackageSizeColumn);

            voiqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn);
            voiqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.ProductIdColumn);

            voiqb.ConditionBuilder.And(VendorPurchaseItemTable.InstanceIdColumn, instanceid);
            var list = new List<VendorPurchaseItem>();
            using (var reader = await QueryExecuter.QueryAsyc(voiqb))
            {
                while (reader.Read())
                {
                    var VendorPurchaseItem = new VendorPurchaseItem()
                    {
                        Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        CreatedAt = reader[VendorPurchaseItemTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        ProductStock = { ProductId = reader[ProductStockTable.ProductIdColumn.FullColumnName].ToString() }
                    };
                    list.Add(VendorPurchaseItem);
                }
            }
            var custsLastAccess = from c in list
                                  group c by c.ProductStock.ProductId into grp
                                  select grp.OrderByDescending(c => c.CreatedAt).FirstOrDefault();

            return custsLastAccess.ToList();
        }

        public async Task<int> Count(VendorOrder data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        private static QueryBuilderBase SqlQueryBuilder(VendorOrder data, QueryBuilderBase qb)
        {
            /*Prefix Added by poongodi on 21/06/2017*/
            qb.AddColumn(VendorOrderTable.OrderIdColumn, VendorOrderTable.PrefixColumn, VendorOrderTable.OrderDateColumn, VendorOrderTable.IdColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorOrderTable.VendorIdColumn);
            //qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.EmailColumn, VendorTable.MobileColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.Table.ColumnList.ToArray());

            if (!string.IsNullOrEmpty(data.Vendor.Name))
            {
                qb.ConditionBuilder.And(VendorTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorTable.NameColumn.ColumnName, data.Vendor.Name);
            }
            if (!string.IsNullOrEmpty(data.Vendor.Mobile))
            {
                qb.ConditionBuilder.And(VendorTable.MobileColumn, data.Vendor.Mobile);
                /* qb.ConditionBuilder.And(VendorTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorTable.MobileColumn.ColumnName, data.Vendor.Mobile); */
            }
            if (!string.IsNullOrEmpty(data.Vendor.Email))
            {
                qb.ConditionBuilder.And(VendorTable.EmailColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorTable.EmailColumn.ColumnName, data.Vendor.Email);
            }
            if (!string.IsNullOrEmpty(data.OrderId))
            {

                /*Prefix filter added by Poongodi on 23/06/2017*/
                var col1 = new CustomCriteria("(Isnull(VendorOrder.Prefix,'')+ VendorOrder.OrderId like '" + data.OrderId + "%' or  VendorOrder.OrderId like '" + data.OrderId + "%')");
                qb.ConditionBuilder.And(col1);

                //qb.ConditionBuilder.And(VendorOrderTable.OrderIdColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                //qb.Parameters.Add(VendorOrderTable.OrderIdColumn.ColumnName, data.OrderId);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(VendorOrderTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(VendorOrderTable.InstanceIdColumn, data.InstanceId);
            }
            if (!(data.fromDate == DateTime.MinValue) && !(data.toDate == DateTime.MinValue))
            {
                var filterFromDate = data.fromDate.AddHours(00).AddMinutes(00).AddSeconds(00);
                var filterToDate = data.toDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                qb.ConditionBuilder.And(VendorOrderTable.OrderDateColumn, CriteriaEquation.Between);
                qb.Parameters.Add("FilterFromDate", filterFromDate);
                qb.Parameters.Add("FilterToDate", filterToDate);
            }
            return qb;
        }


        public async Task<ProductStock> getQuantityForProduct(string productid, string instanceid)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            //qb.AddColumn(ProductStockTable.StockColumn, ProductStockTable.ExpireDateColumn);
            //qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            //qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceid);
            //qb.ConditionBuilder.And(ProductTable.IdColumn, productid);
            //var list = new List<ProductStock>();
            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var productstock = new ProductStock()
            //        {
            //            Stock = reader[ProductStockTable.StockColumn.ColumnName] as decimal? ?? 0,
            //            ExpireDate = reader[ProductStockTable.ExpireDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue
            //        };
            //        list.Add(productstock);
            //    }
            //}
            //var nonExpiredQuantity = list.Where(item => item.ExpireDate > DateTime.Today).Sum(g => g.Stock);
            //var expiredQuantity = list.Where(item => item.ExpireDate < DateTime.Today).Sum(a => a.Stock);
            //var obj = new ProductStock();
            //obj.Stock = nonExpiredQuantity;
            //obj.Expiredstock = expiredQuantity;
            //return obj;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("productid", productid);
            parms.Add("instanceid", instanceid);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetQuantityForProduct", parms);
            if (result.ToList().Count != 0)
            {
                var data = result.Select(item =>
                {
                    var productstock = new ProductStock()
                    {
                        Stock = Convert.ToDecimal(item.nonExpiredQuantity),
                        Expiredstock = Convert.ToDecimal(item.expiredQuantity),
                        ProductId = item.productid,
                        PurchasePrice = item.purchaseprice == null ? 0.00M : item.purchaseprice,
                        PackageSize = item.packagesize,
                        VAT = item.vat == null ? 0.00M : item.vat,
                        Igst = item.Igst == null ? 0.00M : item.Igst,
                        Cgst = item.Cgst == null ? 0.00M : item.Cgst,
                        Sgst = item.Sgst == null ? 0.00M : item.Sgst,
                        GstTotal = item.GstTotal == null ? 0.00M : item.GstTotal
                    };
                    return productstock;
                }).FirstOrDefault();
                return data;
            }
            else
            {
                var obj = new ProductStock();
                obj.Stock = 0.00M;
                obj.Expiredstock = 0.00M;
                obj.ProductId = productid;
                obj.PurchasePrice = 0.00M;
                obj.VAT = 0.00M;
                obj.Igst = 0.00M;
                obj.Cgst = 0.00M;
                obj.Sgst = 0.00M;
                obj.GstTotal = 0.00M;
                return obj;
            }
        }

        public async Task<string> GetOrderNo(string instanceid, string sPrefix)
        {
            /*  var query = $@"SELECT ISNULL(MAX(CONVERT(INT, OrderId)),0) + 1 AS OrderId FROM VendorOrder where InstanceId = '{instanceid}'";
              var qb = QueryBuilderFactory.GetQueryBuilder(query);
              return (await QueryExecuter.SingleValueAsyc(qb)).ToString();*/
            /*Prefix added by POongodi on 11/10/2017*/
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.Order, CustomDateTime.IST, instanceid, "", "", sPrefix);
            return sBillNo.ToString();
        }
        public async Task<VendorOrder> GetOrderNo(VendorOrder v, SqlConnection con, SqlTransaction transaction)
        {
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.Order, CustomDateTime.IST, v.InstanceId, "", "", v.Prefix);
            v.OrderId = sBillNo.ToString();
            return v;
        }
        //finyear method added by Poongodi on 26/03/2017
        public async Task<string> GetFinYear(VendorOrder data)
        {

            string sFinyr = await _HelperDataAccess.GetFinyear(CustomDateTime.IST, data.InstanceId, data.AccountId);
            return Convert.ToString(sFinyr);

        }
        public async Task<VendorOrder> GetById(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VendorOrderTable.IdColumn);
            qb.Parameters.Add(VendorOrderTable.IdColumn.ColumnName, id);
            qb.AddColumn(VendorOrderItemTable.VendorOrderIdColumn, VendorOrderItemTable.QuantityColumn);

            qb.JoinBuilder.Join(VendorOrderTable.Table, VendorOrderTable.IdColumn, VendorOrderItemTable.VendorOrderIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorOrderTable.Table, VendorOrderTable.IdColumn);

            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorOrderTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.MobileColumn, VendorTable.EmailColumn);

            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, VendorOrderItemTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);

            qb.JoinBuilder.Join(InstanceTable.Table, InstanceTable.IdColumn, VendorOrderTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.NameColumn, InstanceTable.AddressColumn, InstanceTable.PhoneColumn);

            qb.SelectBuilder.SortByDesc(VendorOrderTable.CreatedAtColumn);

            var list = new List<VendorOrderItem>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new VendorOrderItem
                    {
                        Product = { Name = reader[ProductTable.NameColumn.FullColumnName].ToString() },
                        Quantity = (decimal)reader[VendorOrderItemTable.QuantityColumn.ColumnName]
                    };
                    item.VendorOrder.Vendor.Name = reader[VendorTable.NameColumn.FullColumnName].ToString();
                    item.VendorOrder.Vendor.Email = reader[VendorTable.EmailColumn.FullColumnName].ToString();
                    item.VendorOrder.Vendor.Mobile = reader[VendorTable.MobileColumn.FullColumnName].ToString();
                    item.VendorOrder.Instance.Name = reader[InstanceTable.NameColumn.FullColumnName].ToString();
                    item.VendorOrder.Instance.Address = reader[InstanceTable.AddressColumn.FullColumnName].ToString();
                    item.VendorOrder.Instance.Phone = reader[InstanceTable.PhoneColumn.FullColumnName].ToString();
                    list.Add(item);
                }
            }

            var vl = list;
            var vendorOrder = vl.GroupBy(x => x.VendorOrder.Id).Select(y =>
            {
                var v = new VendorOrder();
                v = vl.First(w => w.VendorOrder.Id == y.Key).VendorOrder;
                v.VendorOrderItem = y.ToList();
                return v;
            }).ToList();
            return vendorOrder.FirstOrDefault();
        }


        public async Task<List<MissedOrder>> MissedOrderList(MissedOrder data)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(MissedOrderTable.Table, OperationType.Select);
            //qb.AddColumn(MissedOrderTable.IdColumn, MissedOrderTable.PatientIdColumn, MissedOrderTable.PatientMobileColumn, MissedOrderTable.ProductIdColumn,
            //    MissedOrderTable.QuantityColumn, MissedOrderTable.SellingPriceColumn, MissedOrderTable.CreatedAtColumn);            
            //qb.JoinBuilder.LeftJoin(ProductStockTable.Table, MissedOrderTable.ProductIdColumn, ProductStockTable.ProductIdColumn);

            //qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.VendorIdColumn);            
            //qb.JoinBuilder.LeftJoin(VendorTable.Table, ProductStockTable.VendorIdColumn, VendorTable.IdColumn);
            //qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn);
            //qb.JoinBuilder.LeftJoin(ProductTable.Table, MissedOrderTable.ProductIdColumn, ProductTable.IdColumn);
            //qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);

            //qb.ConditionBuilder.And(MissedOrderTable.AccountIdColumn, data.AccountId);
            //qb.ConditionBuilder.And(MissedOrderTable.InstanceIdColumn, data.InstanceId);

            //qb.SelectBuilder.SortByDesc(MissedOrderTable.CreatedAtColumn);

            //string query = $@"select mo.id,mo.PatientId,mo.patientmobile,pt.Name as PatientName,mo.ProductId,p.name as productName,mo.SellingPrice,mo.Quantity,mo.CreatedAt,
            //                    ps.VendorId,v.Name as vendorName from MissedOrder mo 
            //                    left join ProductStock ps on ps.ProductId = mo.ProductId and ps.InstanceId = mo.InstanceId and ps.AccountId = mo.AccountId
            //                    left join Vendor v on v.Id = ps.VendorId and v.InstanceId = mo.InstanceId and v.AccountId = mo.AccountId
            //                    left join Product p on mo.ProductId = p.Id
            //                    join Patient pt on pt.Mobile = mo.patientmobile
            //                    where mo.AccountId = '{data.AccountId}' and mo.InstanceId='{data.InstanceId}' and mo.isActive = 1
            //                    order by mo.CreatedAt desc";

            string query = $@"select ab.Id,ab.PatientId,ab.patientmobile,ab.PatientName,ab.productName,ab.ProductId,ab.SellingPrice,ab.Quantity,ab.CreatedAt,
                                B.ProductStockId,B.VendorId,B.vendorName from 
                                (select mo.id,mo.PatientId,mo.patientmobile,mo.PatientName as PatientName,mo.ProductId,p.name as productName,mo.SellingPrice,mo.Quantity,mo.CreatedAt                                
                                from MissedOrder mo left join Product p on mo.ProductId = p.Id
                                where mo.AccountId = '{data.AccountId}' and mo.InstanceId='{data.InstanceId}' and mo.isActive = 1) AB
                                left join
                                (select A.ProductId,A.cdate,psk.Id as ProductStockId,psk.VendorId,v.Name as vendorName from 
                                (select ps.ProductId,MAX(ps.CreatedAt) as cdate from ProductStock ps
                                where ps.AccountId = '{data.AccountId}' and ps.InstanceId='{data.InstanceId}'
                                group by ps.ProductId) A 
                                left join ProductStock psk on psk.ProductId=a.ProductId and psk.CreatedAt=a.cdate
                                left join Vendor v on v.Id=psk.VendorId and v.Status=1) B on ab.ProductId = b.ProductId
                                order by ab.CreatedAt desc";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);

            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);

            List<MissedOrder> result = new List<MissedOrder>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var order1 = new MissedOrder();

                    order1.Id = reader["Id"].ToString();
                    order1.PatientId = reader["PatientId"].ToString();
                    order1.PatientMobile = reader["patientmobile"].ToString();
                    order1.ProductId = reader["ProductId"].ToString();
                    order1.Quantity = reader["Quantity"] as decimal?;
                    order1.SellingPrice = reader["SellingPrice"] as decimal?;
                    order1.CreatedAt = Convert.ToDateTime(reader["CreatedAt"]);
                    order1.MissedItem.ProductStock.Product.Name = reader["productName"].ToString();
                    order1.MissedItem.ProductStock.VendorId = reader["VendorId"].ToString();
                    order1.MissedItem.ProductStock.Vendor.Name = reader["vendorName"].ToString();
                    order1.PatientName = reader["PatientName"].ToString();

                    result.Add(order1);
                }
            }



            if (data.Values != null)
            {
                if (data.Select == "product")
                {
                    var rst = (from r in result where r.MissedItem.ProductStock.Product.Name.ToLower().Contains(data.Values.ToLower()) select r);
                    //var rst1 = result.Where(p => p.MissedItem.ProductStock.Product.Name.ToLower().Contains(data.Values.ToLower())).ToList();
                    result = rst.ToList<MissedOrder>();

                }
                else if (data.Select == "vendorName")
                {
                    var rst = (from r in result where r.MissedItem.ProductStock.Vendor.Name.ToLower().Contains(data.Values.ToLower()) select r);
                    result = rst.ToList<MissedOrder>();
                }
                else if (data.Select == "patient")
                {
                    var rst = (from r in result where r.PatientName.ToLower().Contains(data.Values.ToLower()) select r);
                    result = rst.ToList<MissedOrder>();
                }
                else if (data.Select == "orderDate" && data.Select1 != null)
                {
                    DateTime dt = Convert.ToDateTime(data.Values);
                    if (data.Select1 == "equal")
                    {
                        var rst = (from r in result where DateTime.Compare(dt.Date, r.CreatedAt.Date) == 0 select r);
                        result = rst.ToList<MissedOrder>();
                    }
                    else if (data.Select1 == "less")
                    {
                        var rst = (from r in result where DateTime.Compare(dt.Date, r.CreatedAt.Date) > 0 select r);
                        result = rst.ToList<MissedOrder>();
                    }
                    else
                    {
                        var rst = (from r in result where DateTime.Compare(dt.Date, r.CreatedAt.Date) < 0 select r);
                        result = rst.ToList<MissedOrder>();
                    }

                }

            }


            return result;
        }

        public async Task<string> CancelMissedOrder(string id)
        {
            if (id != null)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(MissedOrderTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(MissedOrderTable.IdColumn, id);
                qb.Parameters.Add(MissedOrderTable.IsActiveColumn.ColumnName, 0);
                qb.AddColumn(MissedOrderTable.IsActiveColumn);
                await QueryExecuter.NonQueryAsyc(qb);
            }

            return id;
        }

        public async Task<IEnumerable<VendorOrder>> GetAuthorisationList(VendorOrder data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderTable.Table, OperationType.Select);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorOrderTable.VendorIdColumn);
            qb.AddColumn(VendorOrderTable.IdColumn, VendorOrderTable.VendorIdColumn, VendorOrderTable.OrderIdColumn, VendorOrderTable.OrderDateColumn,
                VendorOrderTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.IdColumn, VendorTable.NameColumn, VendorTable.MobileColumn, VendorTable.EmailColumn);

            qb.ConditionBuilder.And(VendorOrderTable.PendingAuthorisationColumn, true);

            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(VendorOrderTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(VendorOrderTable.InstanceIdColumn, data.InstanceId);
            }
            qb.SelectBuilder.SortByDesc(VendorOrderTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);

            var result = await List<VendorOrder>(qb);

            var itemList = await SqlOrderItemList(result);

            var orderData = result.Select(x =>
            {
                x.VendorOrderItem = itemList.Where(y => y.VendorOrderId == x.Id).Select(z => z).ToList();
                return x;
            });

            return orderData;
        }

        public async Task<List<VendorOrderItem>> SqlOrderItemList(IEnumerable<VendorOrder> vendorOrder)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorOrderItemTable.IdColumn, VendorOrderItemTable.QuantityColumn, VendorOrderItemTable.VendorOrderIdColumn);

            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, VendorOrderItemTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.IdColumn);

            var list = new List<VendorOrderItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorOrderItem = new VendorOrderItem
                    {
                        Id = reader[VendorOrderItemTable.IdColumn.ColumnName].ToString(),
                        VendorOrderId = reader[VendorOrderItemTable.VendorOrderIdColumn.ColumnName].ToString(),
                        Quantity = (decimal)reader[VendorOrderItemTable.QuantityColumn.ColumnName],
                        Product = { Name = reader[ProductTable.NameColumn.FullColumnName].ToString() },
                        ProductId = reader[ProductTable.IdColumn.FullColumnName].ToString(),
                    };
                    list.Add(vendorOrderItem);
                }
            }

            return list;
        }

        public async Task<VendorOrder> UpdateAuthorisationOrder(VendorOrder data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(VendorOrderTable.IdColumn, data.Id);
            qb.Parameters.Add(VendorOrderTable.PendingAuthorisationColumn.ColumnName, false);
            qb.Parameters.Add(VendorOrderTable.AuthorisedByColumn.ColumnName, data.CreatedBy);
            qb.AddColumn(VendorOrderTable.PendingAuthorisationColumn, VendorOrderTable.AuthorisedByColumn);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task<List<VendorOrderItem>> getPreviousOrders(string productid, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorOrderItemTable.IdColumn, VendorOrderItemTable.ProductIdColumn, VendorOrderItemTable.QuantityColumn);
            qb.ConditionBuilder.And(VendorOrderItemTable.ProductIdColumn, productid);

            qb.JoinBuilder.LeftJoin(VendorOrderTable.Table, VendorOrderTable.IdColumn, VendorOrderItemTable.VendorOrderIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorOrderTable.Table, VendorOrderTable.IdColumn, VendorOrderTable.OrderDateColumn, VendorOrderTable.OrderIdColumn);
            qb.ConditionBuilder.And(VendorOrderTable.InstanceIdColumn, instanceid);

            qb.JoinBuilder.LeftJoin(VendorTable.Table, VendorTable.IdColumn, VendorOrderTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.IdColumn);

            qb.JoinBuilder.LeftJoin(ProductTable.Table, ProductTable.IdColumn, VendorOrderItemTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);

            var list = new List<VendorOrderItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item = new VendorOrderItem()
                    {
                        Id = reader[VendorOrderItemTable.IdColumn.ColumnName].ToString(),
                        ProductId = reader[VendorOrderItemTable.ProductIdColumn.ColumnName].ToString(),
                        Quantity = reader[VendorOrderItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        VendorOrder = {
                            Id = reader[VendorOrderTable.IdColumn.FullColumnName].ToString(),
                            OrderId = reader[VendorOrderTable.OrderIdColumn.FullColumnName].ToString(),
                            OrderDate = reader[VendorOrderTable.OrderDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                            Vendor = {
                                Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
                                Id = reader[VendorTable.IdColumn.FullColumnName].ToString()
                            }
                        },
                        Product = {
                            Name = reader[ProductTable.NameColumn.FullColumnName].ToString()
                        }
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        public async Task<List<OrderBasedSales>> GetOrderBasedSales(OrderBasedSales obs)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("InstanceId", obs.InstanceId);
            parms.Add("AccountId", obs.AccountId);
            parms.Add("PreviousDays", obs.PreviousDays);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_OrderBasedSalesList", parms);
            //var i = 0;
            //foreach(var x in result)
            //{
            //    var ordersales = new OrderBasedSales();
            //    var product = new Product();
            //    product.Name = x.ProductName.ToString();
            //    product.Id = x.Id.ToString();
            //    ordersales.Product = product;
            //    ordersales.Stock = x.Stock as decimal? ?? 0;

            //    Vendor vendor = new Vendor();
            //    vendor.Name = x.VendorName.ToString();
            //    vendor.Id = x.VendorId.ToString();
            //    ordersales.Vendor = vendor;
            //    var quantityCount = new List<int>() { Convert.ToInt32(x.Day1),Convert.ToInt32(x.Day2),Convert.ToInt32(x.Day3),Convert.ToInt32(x.Day4),
            //            Convert.ToInt32(x.Day5),Convert.ToInt32(x.Day6),Convert.ToInt32(x.Day7),Convert.ToInt32(x.Day8),
            //            Convert.ToInt32(x.Day9),Convert.ToInt32(x.Day10)};
            //    ordersales.QuantityCount = quantityCount;

            //    i++;
            //}

            var orderSalesList = result.Select(x =>
            {
                var ordersales = new OrderBasedSales
                {
                    Product = new Product
                    {
                        Name = x.ProductName.ToString(),
                        Id = x.Id.ToString(),
                    },
                    Stock = x.Stock as decimal? ?? 0,
                    Vendor = new Vendor
                    {
                        Name = x.VendorName.ToString(),
                        Id = x.VendorId.ToString(),
                    },
                    QuantityCount = new System.Collections.Generic.List<int>()
                    {
                        Convert.ToInt32(x.Day1),Convert.ToInt32(x.Day2),Convert.ToInt32(x.Day3),Convert.ToInt32(x.Day4),
                        Convert.ToInt32(x.Day5),Convert.ToInt32(x.Day6),Convert.ToInt32(x.Day7),Convert.ToInt32(x.Day8),
                        Convert.ToInt32(x.Day9),Convert.ToInt32(x.Day10)
                    }

                };
                return ordersales;
            });

            return orderSalesList.ToList();
        }







        public async Task<List<Vendor>> getVendors(string AccountId, string InstanceId, string vendorname)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb.AddColumn(VendorTable.Table.ColumnList.ToArray());
            qb.SelectBuilder.SetTop(10).SortBy(VendorTable.NameColumn);
            qb.ConditionBuilder.And(VendorTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(VendorTable.StatusColumn, 1); // Active Vendors Only
            //Changed by Settu 07-06-2017, Vendor should be searched based on account level
            //qb.ConditionBuilder.And(VendorTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(VendorTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
            qb.Parameters.Add(VendorTable.NameColumn.ColumnName, vendorname);
            return await (List<Vendor>(qb));
        }

        public async Task<List<VendorOrderItem>> getVendorOrders(string id, bool PO, string instanceid, string accountid, string toInstance = "")
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("VendorId", id);
            parms.Add("chkPO", PO);
            parms.Add("InstanceId", instanceid);
            parms.Add("AccountId", accountid);
            parms.Add("ToInstance", toInstance);
            //var dtExpiry = Convert.ToDateTime("01-01-0001");
            var batchNo = " ";
            //(PO == true? batchNo : x.BatchNo),
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_vendorBasedOrder", parms);
            var vendorPO = result.Select(x =>
            {
                if (!PO)
                {
                    var stripCalc = x.PurchasePrice; // * x.PackageSize;
                    var purchaseVat = x.PurchasePrice;
                    var sellPrice = x.SellingPrice;
                    var Qty = x.Quantity;


                    if (x.VAT > 0)
                    {
                        stripCalc = ((x.PurchasePrice - ((x.PurchasePrice * x.VAT) / (100 + x.VAT))));
                    }

                    if (x.TransferQtyType)
                    {
                        if (x.PackageSize > 0)
                        {
                            stripCalc = stripCalc / x.PackageSize;
                            sellPrice = x.SellingPrice / x.PackageSize;
                        }
                        if (x.PackageSize > 0)
                        {
                            purchaseVat = (x.PurchasePrice / x.PackageSize);
                        }
                    }


                    //if (x.PackageSize > 0)
                    //{
                    //    Qty = (x.Quantity * x.PackageSize);
                    //}



                    var vendorOrderItem = new VendorOrderItem
                    {
                        Id = x.VendorOrderItemId,
                        VendorOrderId = x.VendorOrderId,
                        ProductId = x.ProductId,
                        Quantity = Qty,
                        ProductName = x.Name,
                        AccountId = x.AccountId,
                        InstanceId = x.InstanceId,
                        PackageSize = x.PackageSize,
                        PackageQty = x.PackageQty,
                        PurchasePrice = purchaseVat,
                        CreatedAt = x.CreatedAt,
                        PackageSellingPrice = stripCalc as decimal? ?? 0,
                        FreeQty = x.FreeQty,
                        Discount = 0,
                        SellingPrice = sellPrice,
                        TransferQtyType = x.TransferQtyType as bool? ?? false,
                        Product = new Product
                        {
                            VAT = x.VAT,
                            Name = x.Name,
                            Id = x.ProductId,
                            Instance = { Name = x.BranchName },
                            InstanceId = x.InstanceId,
                            AccountId = x.AccountId,
                        },
                        ProductStock = new ProductStock
                        {
                            BatchNo = batchNo,
                            ExpireDate = x.ExpireDate,
                            ProductId = x.ProductId,
                            Id = x.Id,
                            SellingPrice = x.SellingPrice,
                            Igst = x.gst,
                            Sgst = x.sgst,
                            Cgst = x.cgst,
                            GstTotal = x.gstTotal,
                        },
                        VendorOrder = new VendorOrder
                        {
                            OrderId = x.OrderId
                        }
                    };
                    return vendorOrderItem;
                }
                else
                {
                    var stripCalc = x.PurchasePrice; //* x.PackageSize;
                    //if (x.VAT > 0)
                    //{
                    //    stripCalc = ((x.PurchasePrice - ((x.PurchasePrice * x.VAT) / (100 + x.VAT))) * x.PackageSize);
                    //}
                    var vendorOrderItem = new VendorOrderItem
                    {
                        Id = x.VendorOrderItemId,
                        VendorOrderId = x.VendorOrderId,
                        ProductId = x.ProductId,
                        Quantity = x.Quantity,
                        ProductName = x.Name,
                        AccountId = x.AccountId,
                        InstanceId = x.InstanceId,
                        PackageSize = x.PackageSize,
                        PackageQty = x.PackageQty,
                        PurchasePrice = stripCalc as decimal? ?? 0,
                        CreatedAt = x.CreatedAt,
                        PackageSellingPrice = x.SellingPrice as decimal? ?? 0,
                        FreeQty = x.FreeQty,
                        Discount = 0,
                        SellingPrice = x.SellingPrice,
                        Product = new Product
                        {
                            VAT = x.VAT,
                            Name = x.Name,
                            Id = x.ProductId,
                            Instance = { Name = x.BranchName },
                            InstanceId = x.InstanceId,
                            AccountId = x.AccountId,
                        },
                        ProductStock = new ProductStock
                        {
                            BatchNo = batchNo,
                            ExpireDate = x.ExpireDate,
                            ProductId = x.ProductId,
                            Id = x.Id,
                            SellingPrice = x.SellingPrice,
                            Igst = x.gst,
                            Sgst = x.sgst,
                            Cgst = x.cgst,
                            GstTotal = x.gstTotal

                        },
                        VendorOrder = new VendorOrder
                        {
                            OrderId = x.OrderId
                        }
                    };
                    return vendorOrderItem;
                }


            });

            return vendorPO.ToList();
        }

        public async Task<VendorPurchase> updateVendorOrderItem(VendorPurchase vpData)
        {
            var tmpData = vpData.VendorPurchaseItem.ToList();

            for (int i = 0; i < tmpData.Count; i++)
            {
                if (tmpData[i].fromDcId == null)
                {
                    // Condition Apply by Gavaskar 08-09-2017
                    if (tmpData[i].VendorOrderItemId != null)
                    {
                        var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Update);
                        qb.ConditionBuilder.And(VendorOrderItemTable.VendorOrderIdColumn, tmpData[i].VendorOrderId);
                        qb.ConditionBuilder.And(VendorOrderItemTable.InstanceIdColumn, tmpData[i].InstanceId);
                        qb.ConditionBuilder.And(VendorOrderItemTable.AccountIdColumn, tmpData[i].AccountId);
                        qb.ConditionBuilder.And(VendorOrderItemTable.IdColumn, tmpData[i].VendorOrderItemId);
                        qb.Parameters.Add(VendorOrderItemTable.VendorPurchaseIdColumn.ColumnName, tmpData[i].VendorPurchaseId);
                        qb.AddColumn(VendorOrderItemTable.VendorPurchaseIdColumn);
                        //added by nandhini for transaction
                        if (tmpData[i].WriteExecutionQuery)
                        {
                            tmpData[i].AddExecutionQuery(qb);
                        }
                        else
                        {
                            await QueryExecuter.NonQueryAsyc(qb);
                        }
                    }

                    // added by violet raj 03-05-2017
                    //Condition Apply by Gavaskar 08 - 09 - 2017
                    if (tmpData[i].ProductStock.Product.Id != null)
                    {
                        var qb1 = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Update);
                        qb1.ConditionBuilder.And(ProductTable.IdColumn, tmpData[i].ProductStock.Product.Id);

                        qb1.Parameters.Add(ProductTable.ManufacturerColumn.ColumnName, tmpData[i].ProductStock.Product.Manufacturer);
                        qb1.Parameters.Add(ProductTable.ScheduleColumn.ColumnName, tmpData[i].ProductStock.Product.Schedule);
                        qb1.Parameters.Add(ProductTable.GenericNameColumn.ColumnName, tmpData[i].ProductStock.Product.GenericName);
                        qb1.Parameters.Add(ProductTable.CategoryColumn.ColumnName, tmpData[i].ProductStock.Product.Category);
                        qb1.Parameters.Add(ProductTable.PackingColumn.ColumnName, tmpData[i].ProductStock.Product.Packing);
                        qb1.Parameters.Add(ProductTable.CommodityCodeColumn.ColumnName, tmpData[i].ProductStock.Product.CommodityCode);
                        qb1.Parameters.Add(ProductTable.TypeColumn.ColumnName, tmpData[i].ProductStock.Product.Type);
                        qb1.Parameters.Add(ProductTable.RackNoColumn.ColumnName, tmpData[i].RackNo);
                        qb1.AddColumn(ProductTable.ManufacturerColumn, ProductTable.ScheduleColumn, ProductTable.GenericNameColumn, ProductTable.CategoryColumn, ProductTable.PackingColumn, ProductTable.CommodityCodeColumn, ProductTable.TypeColumn, ProductTable.RackNoColumn);
                        //added by nandhini for transaction
                        if (tmpData[i].WriteExecutionQuery)
                        {
                            tmpData[i].AddExecutionQuery(qb1);
                        }
                        else
                        {
                            await QueryExecuter.NonQueryAsyc(qb1);
                        }
                    }


                }

            }

            return vpData;
        }

        public async Task<StockTransfer> updateVendorOrderItem(StockTransfer vpData)
        {
            var tmpData = vpData.StockTransferItems.ToList();

            for (int i = 0; i < tmpData.Count; i++)
            {
                if (tmpData[i].StockTransfer.IsNew == true && tmpData[i].VendorOrderItemId != null && tmpData[i].VendorOrderItemId != "")
                {
                    var qbRevQty = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Select);
                    qbRevQty.ConditionBuilder.And(VendorOrderItemTable.IdColumn, tmpData[i].VendorOrderItemId);
                    qbRevQty.AddColumn(VendorOrderItemTable.ReceivedQtyColumn);
                    var revQty = (await (List<VendorOrderItem>(qbRevQty))).FirstOrDefault();
                    var ReceivedQty = (revQty == null ? 0 : (revQty.ReceivedQty == null ? 0 : revQty.ReceivedQty)) + tmpData[i].Quantity;

                    var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Update);
                    qb.ConditionBuilder.And(VendorOrderItemTable.IdColumn, tmpData[i].VendorOrderItemId);
                    qb.Parameters.Add(VendorOrderItemTable.PurchasePriceColumn.ColumnName, tmpData[i].ProductStock.PurchasePrice);
                    //qb.Parameters.Add(VendorOrderItemTable.QuantityColumn.ColumnName, tmpData[i].Quantity); //Commented by Settu for partial order/transfer implementation
                    qb.Parameters.Add(VendorOrderItemTable.SellingPriceColumn.ColumnName, tmpData[i].ProductStock.SellingPrice);
                    qb.Parameters.Add(VendorOrderItemTable.ReceivedQtyColumn.ColumnName, ReceivedQty);
                    //qb.ConditionBuilder.And(VendorOrderItemTable.InstanceIdColumn, tmpData[i].InstanceId);
                    //qb.ConditionBuilder.And(VendorOrderItemTable.AccountIdColumn, tmpData[i].AccountId);
                    qb.Parameters.Add(VendorOrderItemTable.StockTransferIdColumn.ColumnName, tmpData[i].Id);
                    qb.Parameters.Add(VendorOrderItemTable.ToInstanceIdColumn.ColumnName, tmpData[i].ToInstanceId); //ToInstanceId parameter added for offline sync by Poongodi on 25/05/2017
                    qb.Parameters.Add(VendorOrderItemTable.AccountIdColumn.ColumnName, tmpData[i].AccountId);//AccountId parameter added for offline sync by Poongodi on 27/05/2017
                    qb.AddColumn(VendorOrderItemTable.StockTransferIdColumn, VendorOrderItemTable.PurchasePriceColumn, VendorOrderItemTable.SellingPriceColumn, VendorOrderItemTable.ReceivedQtyColumn);
                    //added by nandhini for transaction
                    if (tmpData[i].WriteExecutionQuery)
                    {
                        tmpData[i].AddExecutionQuery(qb);
                    }
                    else
                    {
                        await QueryExecuter.NonQueryAsyc(qb);
                    }

                }

            }

            return vpData;
        }

        public async Task<VendorOrder> SaveBulkOrder(List<VendorOrder> data)
        {
            List<SyncObject> syncObjectList = new List<SyncObject>();
            SyncObject syncObject = null;
            SqlConnection con = null;
            SqlTransaction transaction = null;
            List<dynamic> qbList = null;
            var index = 0;

            try
            {
                con = QueryExecuter.OpenConnection();
                transaction = con.BeginTransaction();
                if (_configHelper.AppConfig.OfflineMode)
                {                   
                    //ExecuteTransactionOrder is to avoid deadlock issue
                    var list = data[0].GetExecutionQuery();
                    list = list.Where(x => x.Table != null).Select(x => x.Table).Distinct().ToList();
                    var tableList = "'" + string.Join("','", from item in list select item.TableName) + "'";
                    await ExecuteTransactionOrder(tableList, con, transaction);
                }

                qbList = data[0].GetExecutionQuery();

                for (; index < qbList.Count; index++)
                {
                    QueryBuilderBase qbData = qbList[index];
                    if (qbData.ExecuteCommand == true)
                    {
                        if (qbData.Table == VendorOrderTable.Table && qbData.OperationType == OperationType.Insert)
                        {
                            VendorOrder getData = new VendorOrder();
                            getData.InstanceId = data[0].InstanceId;
                            getData.AccountId = data[0].AccountId;
                            getData.Prefix = data[0].Prefix;
                            //  var getOrder = await GetOrderNo(getData, con, transaction);
                            //  data[0].OrderId = getOrder.OrderId;  
                            data[0].OrderId = Regex.Replace(data[0].OrderId, "[^0-9]+", string.Empty);                         
                            qbData.Parameters[VendorOrderTable.OrderIdColumn.ColumnName] = data[0].OrderId;
                            await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                            getData = null;
                        }
                        else if (qbData.Table == VendorOrderItemTable.Table && qbData.OperationType == OperationType.Insert)
                        {
                            var vendorOrderItem = data.Where(x => x.VendorOrderItem[0].Id == qbData.Parameters[VendorOrderItemTable.IdColumn.ColumnName].ToString());
                            await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                        }

                        else
                        {
                            await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                        }
                    }

                    if (qbData.Table == ProductTable.Table && qbData.OperationType == OperationType.Insert)
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters, EventLevel = "A" };
                    }
                    else
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters };
                    }
                    //Add sync data to list
                    syncObjectList.Add(syncObject);
                }
                //Write sync file
                WriteToSyncQueue(syncObjectList);

                //Commit the transaction
                if (transaction != null)
                {
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                transaction.Rollback();
                var unSavedData = new List<SyncObject>();
                if (qbList != null)
                {
                    for (; index < qbList.Count; index++)
                    {
                        syncObject = new SyncObject() { AccountId = user.AccountId(), InstanceId = user.InstanceId(), QueryText = qbList[index].GetQuery(), Parameters = qbList[index].Parameters };

                        unSavedData.Add(syncObject);
                    }
                }
                var errorLog = new BaseErrorLog()
                {
                    AccountId = user.AccountId(),
                    InstanceId = user.InstanceId(),
                    ErrorMessage = e.Message,
                    ErrorStackTrace = e.StackTrace,
                    UnSavedData = JsonConvert.SerializeObject(unSavedData),
                    Data = JsonConvert.SerializeObject(data[0]),
                    CreatedBy = user.Identity.Id(),
                    UpdatedBy = user.Identity.Id()
                };
                data[0].ErrorLog = errorLog;
                await Insert(errorLog, ErrorLogTable.Table);

                //Write executed sync query for reverse sync
                if (!_configHelper.AppConfig.OfflineMode && syncObjectList.Count > 0)
                {
                    WriteToSyncQueue(syncObjectList);
                }
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                }
                if (con != null)
                {
                    QueryExecuter.CloseConnection(con);
                }
            }
            return data[0];
        }

        public async Task<VendorOrderItem> updateOrderItem(VendorOrderItem data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(VendorOrderItemTable.IdColumn, data.Id);
            qb.Parameters.Add(VendorOrderItemTable.ExpireDateColumn.ColumnName, data.ExpireDate);
            qb.Parameters.Add(VendorOrderItemTable.QuantityColumn.ColumnName, data.Quantity);
            qb.Parameters.Add(VendorOrderItemTable.SellingPriceColumn.ColumnName, data.SellingPrice);
            qb.Parameters.Add(VendorOrderItemTable.IsDeletedColumn.ColumnName, data.IsDeleted);
            qb.Parameters.Add(VendorOrderItemTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(VendorOrderItemTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.AddColumn(VendorOrderItemTable.ExpireDateColumn, VendorOrderItemTable.SellingPriceColumn, VendorOrderItemTable.QuantityColumn, VendorOrderItemTable.IsDeletedColumn, VendorOrderItemTable.UpdatedAtColumn, OrderSettingsTable.UpdatedByColumn);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task<VendorOrder> GetVenodrOrderById(string vendororderid, string AccountId, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderTable.Table, OperationType.Select);
            qb.AddColumn(VendorOrderTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(VendorOrderTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(VendorOrderTable.IdColumn, vendororderid);
            var result = (await List<VendorOrder>(qb)).ToList().FirstOrDefault();


            result.VendorOrderItem = await GetVendorOrderItems(vendororderid, AccountId);
            result.Vendor = await GetVendorById(result.VendorId, AccountId);
            if (result.Vendor == null)
                result.Vendor = await GetVendorById(result.VendorId);

            var data = await GetVendorPurchaseItemlist(instanceid);
            foreach (var resultitem in data)
            {
                foreach (var listitem in result.VendorOrderItem)
                {
                    if (resultitem.ProductStock.ProductId == listitem.ProductId)
                    {
                        listitem.Product.Discount = resultitem.Discount;
                        listitem.Product.purchaseprice = resultitem.PurchasePrice * resultitem.PackageSize;
                    }
                }
            }
            return result;

        }

        public async Task<List<VendorOrderItem>> GetVendorOrderItems(string venodrOrderId, string AccountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorOrderItemTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(VendorOrderItemTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(VendorOrderItemTable.VendorOrderIdColumn, venodrOrderId);

            var statusCondition = new CustomCriteria(VendorOrderItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            var cc = new CriteriaColumn(VendorOrderItemTable.IsDeletedColumn, "0", CriteriaEquation.Equal);
            var col = new CustomCriteria(cc, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col);

            var result = (await List<VendorOrderItem>(qb)).ToList();
            var data = result.Select(async (y) =>
            {
                y.Product = await _productDataAccess.GetProductById(y.ProductId);
                return y;
            });
            return (await Task.WhenAll(data)).OrderBy(x => x.OrderItemSno).ToList();
        }
        public async Task<Vendor> GetVendorById(string VendorId, string AccountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb.AddColumn(VendorTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(VendorTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(VendorTable.IdColumn, VendorId);
            return (await List<Vendor>(qb)).ToList().FirstOrDefault();
        }

        //Newly created by Mani on 13-04-2017
        public async Task<Vendor> GetVendorById(string VendorId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb.AddColumn(VendorTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(VendorTable.IdColumn, VendorId);
            return (await List<Vendor>(qb)).ToList().FirstOrDefault();
        }

        public async Task<List<ProductStock>> getProductDetails(string accountId, string instanceId, string[] prodsId, string fromDate, string toDate, int type)
        {
            //Converted as SP for refreshing stock & sold qty while loading order draft - Settu
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", instanceId);
            parms.Add("AccountId", accountId);
            parms.Add("fromDate", fromDate);
            parms.Add("ToDate", toDate);
            parms.Add("ProductIds", prodsId[0]);
            parms.Add("type", type);
            var result = await sqldb.ExecuteProcedureAsync<ProductStock>("usp_RefreshOrderDraft", parms);
            return result.ToList();
        }
        //To get head office Id for stock transfer offline sync by Settu
        public async Task<Instance> getHeadOfficeVendor(string AccountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.AddColumn(InstanceTable.IdColumn, InstanceTable.TinNoColumn, InstanceTable.isHeadOfficeColumn, InstanceTable.AccountIdColumn, InstanceTable.GsTinNoColumn);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(InstanceTable.isHeadOfficeColumn, 1);
            var result = await List<Instance>(qb);
            return result.FirstOrDefault();
        }
        // Added by Settu on 06-06-2017 for minmax reorder
        public async Task<List<ProductStock>> ReorderByMinMax(string AccountId, string InstanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            var productlist = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetReorderDetailsByMinMaxReorder", parms);
            var data = productlist.Select(item =>
            {
                var productstock = new ProductStock()
                {
                    Product = { Id = item.Id, Name = item.Name },
                    PurchasePrice = item.PackagePurchasePrice,
                    VAT = item.VAT,
                    Igst = item.Igst as decimal? ?? 0,
                    Cgst = item.Cgst as decimal? ?? 0,
                    Sgst = item.Sgst as decimal? ?? 0,
                    GstTotal = item.GstTotal as decimal? ?? 0,
                    VendorId = item.VendorId,
                    PackageSize = item.PackageSize,
                    Stock = item.Stock,
                    RequiredQty = item.MaxReorder,
                    OrderQty = item.ReOrderQty,
                    MRP = item.PackageMRP,
                    SellingPrice = item.PackageMRP,
                    soldqty = item.MinReorder,
                    pendingPOQty = item.pendingPOQty, //pendingPOQty value passed to order generation
                };
                if (productstock.OrderQty % productstock.PackageSize == 0)
                {
                    productstock.packageQty = productstock.OrderQty / productstock.PackageSize;
                }
                else
                {
                    productstock.packageQty = (productstock.OrderQty + productstock.PackageSize - (productstock.OrderQty % productstock.PackageSize)) / productstock.PackageSize;
                }
                return productstock;
            }).Where(a => a.Stock < a.RequiredQty);
            var dataList = data.ToList();
            return dataList;
        }

        //Added by Sarubala on 20-11-17 to get sms settings
        public async Task<bool> GetIsOrderCreateSms(string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SmsSettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SmsSettingsTable.InstanceIdColumn, InsId);
            bool setting = false;
            var result = await List<SmsSettings>(qb);
            if (result != null && result.Count > 0)
            {
                setting = Convert.ToBoolean(result.First().IsOrderCreateSms != null ? result.First().IsOrderCreateSms : false);
            }
            return setting;
        }

    }
}
