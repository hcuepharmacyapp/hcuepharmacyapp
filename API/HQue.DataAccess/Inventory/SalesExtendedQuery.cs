﻿using DataAccess.Criteria.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Helpers.Extension;
using System;
using Utilities.Enum;
using Utilities.Helpers;

namespace HQue.DataAccess.Inventory
{
    public class SalesExtendedQuery
    {
        

        public static void BuildExtendedSalesQuery(Sales data, QueryBuilderBase qb)
        {
            
            switch (data.Select)
            {
                case "batchNo":
                    qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    qb.JoinBuilder.Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    qb.ConditionBuilder.And(ProductStockTable.BatchNoColumn, data.Values);
                    break;
                case "product":
                    qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    qb.JoinBuilder.Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, data.Values);
                    break;
                case "quantity":
                    qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    SetComparerQuery(SalesItemTable.QuantityColumn, qb, data.Values, data.Select1);
                    break;
                case "mrp":
                    qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    qb.JoinBuilder.Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    SetComparerQuery(ProductStockTable.SellingPriceColumn, qb, data.Values, data.Select1);
                    break;
                case "discount":
                    SetComparerQuery(SalesTable.DiscountColumn, qb, data.Values, data.Select1);
                    break;
                case "vat":
                    qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    qb.JoinBuilder.Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    SetComparerQuery(ProductStockTable.VATColumn, qb, data.Values, data.Select1);
                    break;
                case "billNo":
                    qb.ConditionBuilder.And(SalesTable.InvoiceNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(SalesTable.InvoiceNoColumn.ColumnName, data.Values);
                   if (data.Select1 != "")
                    {
                        qb.ConditionBuilder.And(SalesTable.InvoiceSeriesColumn);
                        qb.Parameters.Add(SalesTable.InvoiceSeriesColumn.ColumnName, data.Select1);
                    } 
                  
                    break;
                case "customerName":
                    qb.ConditionBuilder.And(SalesTable.NameColumn, CriteriaEquation.Like);
                    qb.Parameters.Add(SalesTable.NameColumn.ColumnName, data.Values);
                    break;
                case "customerMobile":
                    qb.ConditionBuilder.And(SalesTable.MobileColumn);
                    qb.Parameters.Add(SalesTable.MobileColumn.ColumnName, data.Values);
                    break;
                case "doctorName":
                    qb.ConditionBuilder.And(SalesTable.DoctorNameColumn, CriteriaEquation.Like);
                    qb.Parameters.Add(SalesTable.DoctorNameColumn.ColumnName, data.Values);
                    break;
                case "doctorMobile":
                    qb.ConditionBuilder.And(SalesTable.DoctorMobileColumn);
                    qb.Parameters.Add(SalesTable.DoctorMobileColumn.ColumnName, data.Values);
                    break;
                case "billDate":
                    SetComparerQuery1(SalesTable.InvoiceDateColumn,SalesTable.InvoiceDateColumn.ToDateColumn(), qb, data.Values, data.Select1);
                    break;
                case "expiry":
                    qb.JoinBuilder.Join(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
                    qb.JoinBuilder.Join(ProductStockTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                    SetComparerQuery(ProductStockTable.ExpireDateColumn, qb, data.Values, data.Select1);
                    break;
            }
        }

        private static void SetComparerQuery(IDbColumn column, QueryBuilderBase qb, string value, string condition)
        {
            const string compareGreater = "greater";
            const string compareLesser = "less";

            switch (condition)
            {
                case compareGreater:
                    qb.ConditionBuilder.And(column, CriteriaEquation.Greater);
                    break;
                case compareLesser:
                    qb.ConditionBuilder.And(column, CriteriaEquation.Lesser);
                    break;
                default:
                    qb.ConditionBuilder.And(column);
                    break;
            }

           
                qb.Parameters.Add(column.ColumnName, value);
          
                
          
           
        }
        private static void SetComparerQuery1(IDbColumn column, IDbColumn customColumn, QueryBuilderBase qb, string value, string condition)
        {
            const string compareGreater = "greater";
            const string compareLesser = "less";



            if (customColumn.ColumnVariable == null)
            {
                customColumn.ColumnVariable = $"@{column.ColumnName}";
            }

            switch (condition)
            {
                case compareGreater:
                    qb.ConditionBuilder.And(customColumn, CriteriaEquation.Greater);
                    break;
                case compareLesser:
                    qb.ConditionBuilder.And(customColumn, CriteriaEquation.Lesser);
                    break;
                default:
                    qb.ConditionBuilder.And(customColumn, CriteriaEquation.Equal);
                    break;
            }


            qb.Parameters.Add(column.ColumnName, value);
        }
    }
}