using System.Data.Common;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Helpers.Extension;

namespace HQue.DataAccess.Inventory
{
    public static class StockTransferItemHelper
    {
        public static StockTransferItem Fill(this StockTransferItem stockTransferItem,DbDataReader reader)
        {
            stockTransferItem.Id = reader.ToString(StockTransferItemTable.IdColumn.ColumnName);
            stockTransferItem.ProductStockId = reader.ToString(StockTransferItemTable.ProductStockIdColumn.ColumnName);
            stockTransferItem.TransferId = reader.ToString(StockTransferItemTable.TransferIdColumn.ColumnName);
            stockTransferItem.Quantity = reader.ToDecimal(StockTransferItemTable.QuantityColumn.ColumnName);
            stockTransferItem.Discount = reader.ToDecimalNullable(StockTransferItemTable.DiscountColumn.ColumnName ,0);
         
            return stockTransferItem;
        }
    }
}