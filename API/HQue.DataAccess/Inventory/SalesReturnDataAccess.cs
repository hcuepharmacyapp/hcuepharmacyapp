﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using System.Threading.Tasks;
using Utilities.Enum;
using DataAccess.Criteria;
using Utilities.Helpers;
using HQue.DataAccess.Helpers;
using DataAccess;
using HQue.DataAccess.Helpers.Extension;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.Settings;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure.Master;

namespace HQue.DataAccess.Inventory
{
    public class SalesReturnDataAccess : BaseDataAccess
    {
        private readonly ProductStockDataAccess _productStockDataAccess;
        private readonly SalesDataAccess _salesDataAccess;
        private readonly ProductDataAccess _productDataAccess;
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        private readonly HelperDataAccess _HelperDataAccess;

        public SalesReturnDataAccess(ISqlHelper sqlQueryExecuter, ProductStockDataAccess productStockDataAccess, HelperDataAccess helperDataAccess, QueryBuilderFactory queryBuilderFactory, SalesDataAccess salesDataAccess, ProductDataAccess productDataAccess, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _productStockDataAccess = productStockDataAccess;
            _salesDataAccess = salesDataAccess;
            _productDataAccess = productDataAccess;
            _HelperDataAccess = helperDataAccess; //Added by Poongodi on 26/03/2017
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }

        public override Task<SalesReturn> Save<SalesReturn>(SalesReturn data)
        {
            return string.IsNullOrEmpty(data.Id) ? Insert(data, SalesReturnTable.Table) : Update(data, SalesReturnTable.Table);
        }
        /// <summary>
        /// Update Productstock  - for GST Values by Poongodi on 30/06/2017
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<SalesReturn> updateProductStockGST(SalesReturn data)
        {
            foreach (var item in data.SalesReturnItem)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
                // qb.AddColumn(ProductStockTable.GstTotalColumn);
                qb.ConditionBuilder.And(ProductStockTable.IdColumn, item.ProductStockId);
                var productstock = (await List<ProductStock>(qb)).FirstOrDefault();
                if (productstock != null)
                {
                    var product = item.ProductStock.Product; //Added by Sarubala on 25-06-18
                    item.ProductStock = productstock;
                    var gstTotal = productstock.GstTotal;
                    item.ProductStock.Product = product;

                    if ((gstTotal == 0 || gstTotal == null) && (gstTotal != item.GstTotal))
                    {
                        item.Cgst = item.GstTotal / 2;
                        item.Sgst = item.GstTotal / 2;
                        item.Igst = item.GstTotal;
                        var qbTemp = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                        qbTemp.AddColumn(ProductStockTable.GstTotalColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.IgstColumn, ProductStockTable.UpdatedAtColumn, ProductStockTable.UpdatedByColumn);
                        qbTemp.ConditionBuilder.And(ProductStockTable.IdColumn, item.ProductStockId);
                        qbTemp.Parameters.Add(ProductStockTable.GstTotalColumn.ColumnName, item.GstTotal);
                        qbTemp.Parameters.Add(ProductStockTable.CgstColumn.ColumnName, item.Cgst);
                        qbTemp.Parameters.Add(ProductStockTable.SgstColumn.ColumnName, item.Sgst);
                        qbTemp.Parameters.Add(ProductStockTable.IgstColumn.ColumnName, item.Igst);
                        qbTemp.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                        qbTemp.Parameters.Add(ProductStockTable.UpdatedByColumn.ColumnName, data.UpdatedBy);

                        await QueryExecuter.NonQueryAsyc(qbTemp);

                    }

                }
            }
            return data;
        }

        //Added by Sarubala on 20-06-18 - start
        public async Task<LoyaltyPointSettings> getSalesReturnLoyaltyPoint(string AccountId, string LoyaltyId, string SalesReturnId)
        {

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("LoyaltyId", LoyaltyId);
            parms.Add("SalesReturnId", SalesReturnId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesReturnLoyaltyPointSettings", parms);
            if (result.Count() <= 0)
            { return null; }
            var salesLoyaltyPts = result.Select(x =>
            {
                var LoyaltyPointSettings = new LoyaltyPointSettings
                {
                    Id = x.Id,
                    LoyaltySaleValue = x.LoyaltySaleValue as decimal? ?? 0,
                    LoyaltyPoint = x.LoyaltyPoint as decimal? ?? 0,
                    StartDate = x.StartDate as DateTime? ?? DateTime.MinValue,
                    EndDate = x.EndDate as DateTime? ?? DateTime.MinValue,
                    MinimumSalesAmt = x.MinimumSalesAmt as decimal? ?? 0,
                    RedeemPoint = x.RedeemPoint as decimal? ?? 0,
                    RedeemValue = x.RedeemValue as decimal? ?? 0,
                    MaximumRedeemPoint = x.MaximumRedeemPoint as decimal? ?? 0,
                    IsActive = x.IsActive,
                    LoyaltyOption = x.LoyaltyOption,
                    IsMinimumSale = x.IsMinimumSale as bool? ?? false,
                    IsMaximumRedeem = x.IsMaximumRedeem as bool? ?? false,
                    IsEndDate = x.IsEndDate as bool? ?? false,

                };

                return LoyaltyPointSettings;
            });
            var getLoyaltyPointSettings = salesLoyaltyPts.FirstOrDefault();
            getLoyaltyPointSettings.LoyaltyProductPoints = await _productDataAccess.GetLoyaltyPointProductList(getLoyaltyPointSettings.Id);
            return getLoyaltyPointSettings;
        }


        public async Task ModifyLoyaltyPatientUpdate(SalesReturn salesReturn, string mode)
        {
            var Loyalty = await _salesDataAccess.getLoyaltyPatient(salesReturn.AccountId, salesReturn.PatientId);
            if (Loyalty != null)
            {
                //if (salesReturn.LoyaltyPts > 0)
                //{
                    if (mode == "new" )
                    {
                        Loyalty.LoyaltyPoint = Convert.ToDecimal(Loyalty.LoyaltyPoint) - Convert.ToDecimal(salesReturn.LoyaltyPts);
                    }
                    else if(mode == "edit")
                    {
                        Loyalty.LoyaltyPoint = Convert.ToDecimal(Loyalty.LoyaltyPoint) - Convert.ToDecimal(salesReturn.LoyaltyPts) + Convert.ToDecimal(salesReturn.PreviousLoyaltyPts);
                    }
                    else if (mode == "return_cancel")
                    {
                        Loyalty.LoyaltyPoint = Convert.ToDecimal(Loyalty.LoyaltyPoint) + Convert.ToDecimal(salesReturn.LoyaltyPts);
                    }
                    else if(mode == "cancel")
                {
                    Loyalty.LoyaltyPoint = Convert.ToDecimal(Loyalty.LoyaltyPoint) - Convert.ToDecimal(salesReturn.LoyaltyPts)+Convert.ToDecimal(salesReturn.RedeemPts);
                }

                    var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
                    qb.AddColumn(PatientTable.LoyaltyPointColumn, PatientTable.UpdatedAtColumn);
                    qb.ConditionBuilder.And(PatientTable.AccountIdColumn, salesReturn.AccountId);
                    qb.ConditionBuilder.And(PatientTable.IdColumn, salesReturn.PatientId);
                    qb.Parameters.Add(PatientTable.LoyaltyPointColumn.ColumnName, Loyalty.LoyaltyPoint);
                    qb.Parameters.Add(PatientTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);

                    var result = (await List<Patient>(qb)).FirstOrDefault();

                //}
            }

        }

        public async Task ModifyLoyaltySalesUpdate(SalesReturn salesReturn)
        {
            var salesData = await getSalesLoyaltyPoint(salesReturn.SalesId, salesReturn.AccountId, salesReturn.InstanceId);
            if (!string.IsNullOrEmpty(salesData.LoyaltyId) && Convert.ToDecimal(salesData.LoyaltyPts) != 0)
            {
                salesData.LoyaltyPts = Convert.ToDecimal(salesData.LoyaltyPts) - Convert.ToDecimal(salesReturn.LoyaltyPts);

                var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
                qb.AddColumn(SalesTable.LoyaltyPtsColumn, SalesTable.UpdatedAtColumn);
                qb.ConditionBuilder.And(SalesTable.AccountIdColumn, salesReturn.AccountId);
                qb.ConditionBuilder.And(SalesTable.IdColumn, salesReturn.SalesId);
                qb.Parameters.Add(SalesTable.LoyaltyPtsColumn.ColumnName, salesData.LoyaltyPts);
                qb.Parameters.Add(PatientTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);

                var result = (await List<Sales>(qb)).FirstOrDefault();
            }

        }

        public async Task<Sales> getSalesLoyaltyPoint(string salesId, string accId, string insId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(SalesTable.IdColumn, salesId);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, insId);

            return (await List<Sales>(qb)).FirstOrDefault();
        }

        //Added by Sarubala on 20-06-18 - end


        public Task<List<SalesReturnItem>> SalesReturnItemList(string salesId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesReturnItemTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(SalesReturnTable.Table, SalesReturnTable.IdColumn, SalesReturnItemTable.SalesReturnIdColumn);

            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
            qb.ConditionBuilder.And(SalesReturnTable.SalesIdColumn, salesId);
            qb.ConditionBuilder.And(SalesReturnTable.IsAlongWithSaleColumn, 1);

            return List<SalesReturnItem>(qb);
        }

        public Task<List<SalesReturnItem>> ReturnOnlyItemList(string salesReturnId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesReturnItemTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());

            qb.ConditionBuilder.And(SalesReturnItemTable.SalesReturnIdColumn, salesReturnId);

            var isDeletedCondition = new CustomCriteria(SalesReturnItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            string isDelete = "1";
            var cc1 = new CriteriaColumn(SalesReturnItemTable.IsDeletedColumn, isDelete, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, isDeletedCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);

            return List<SalesReturnItem>(qb);
        }

        public Task<List<SalesReturn>> GetSalesReturnAlongWithSales(string salesId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Select);
            qb.AddColumn(SalesReturnTable.Table.ColumnList.ToArray());

            qb.ConditionBuilder.And(SalesReturnTable.SalesIdColumn, salesId);
            qb.ConditionBuilder.And(SalesReturnTable.IsAlongWithSaleColumn, 1);

            var srlist = List<SalesReturn>(qb);
            return srlist;
        }

        public async Task<List<SalesReturnItem>> GetSalesReturnItem(string salesReturnId)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
            //qb.AddColumn(SalesReturnItemTable.Table.ColumnList.ToArray());

            //qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            //qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
            //qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            //qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            //qb.ConditionBuilder.And(SalesReturnItemTable.SalesReturnIdColumn, salesReturnId);

            //var salesReturnItemList = List<SalesReturnItem>(qb);
            //return salesReturnItemList;

            var qb = QueryBuilderFactory.GetQueryBuilder("usp_GetSalesReturnItem");
            qb.Parameters.Add("@SalesReturnId", salesReturnId);
            var salesReturnItemList = new List<SalesReturnItem>();

            using (var reader = await QueryExecuter.ExecuteProcAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesReturnItem = new SalesReturnItem().Fill(reader);
                    salesReturnItem.ProductStock.Fill(reader);
                    salesReturnItem.ProductStock.Product.Fill(reader);

                    salesReturnItemList.Add(salesReturnItem);
                }
            }

            return salesReturnItemList;

        }

        public async Task<IEnumerable<SalesReturnItem>> Save(IEnumerable<SalesReturnItem> itemList, string salesid)
        {
            foreach (var salesReturnItem in itemList.Where(salesReturnItem => salesReturnItem.Quantity != null && salesReturnItem.Quantity > 0))
            {
                // Update Product Stock 
                var productStock = await _productStockDataAccess.GetStockValue(salesReturnItem.ProductStockId);
                productStock.Stock = productStock.Stock + salesReturnItem.Quantity;
                await _productStockDataAccess.Update(productStock);

                //salesReturnItem.MrpSellingPrice = salesReturnItem.rettotsellprice;
                //salesReturnItem.MRP = salesReturnItem.rettotsellprice;
                salesReturnItem.MrpSellingPrice = salesReturnItem.SellingPrice;
                salesReturnItem.MRP = salesReturnItem.SellingPrice;

                await Insert(salesReturnItem, SalesReturnItemTable.Table);
            }
            return itemList;
        }

        //Added By Sarubala
        public async Task<IEnumerable<SalesReturnItem>> UpdateStockAndSalesReturnItem(IEnumerable<SalesReturnItem> itemList, string salesid)
        {
            foreach (var salesReturnItem in itemList.Where(salesReturnItem => salesReturnItem.Quantity != null))
            {
                // Update Product Stock 
                var productStock = await _productStockDataAccess.GetStockValue(salesReturnItem.ProductStockId);
                productStock.Stock = productStock.Stock + salesReturnItem.ReturnedQuantity;
                await _productStockDataAccess.Update(productStock);
                salesReturnItem.MrpSellingPrice = salesReturnItem.SellingPrice;

                salesReturnItem.Quantity = Math.Abs(salesReturnItem.Quantity.Value);
                await Update(salesReturnItem, SalesReturnItemTable.Table);

            }
            return itemList;
        }

        public async Task updateReturnedItem(SalesReturnItem item1)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Update);
            qb.AddColumn(SalesReturnItemTable.IsDeletedColumn, SalesReturnItemTable.UpdatedAtColumn);
            qb.Parameters.Add(SalesReturnItemTable.IsDeletedColumn.ColumnName, true);
            qb.Parameters.Add(SalesReturnItemTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.ConditionBuilder.And(SalesReturnItemTable.IdColumn, item1.Id);
            qb.ConditionBuilder.And(SalesReturnItemTable.AccountIdColumn, item1.AccountId);
            qb.ConditionBuilder.And(SalesReturnItemTable.InstanceIdColumn, item1.InstanceId);
            await QueryExecuter.NonQueryAsyc(qb);

        }

        //End

        //Code to update ProductStock and SalesReturnItem for cancel Return Items along with sale
        public async Task<SalesReturnItem> UpdateSalesReturnItemForCancel(SalesReturnItem sritem)
        {
            var productStock = await _productStockDataAccess.GetStockValue(sritem.ProductStockId);
            productStock.Stock = productStock.Stock - sritem.Quantity;
            await _productStockDataAccess.Update(productStock);

            await Update(sritem, SalesReturnItemTable.Table);
            return sritem;
        }

        //End

        public async Task<string> GetReturnNo(string AccountId, string InstanceId, string Canceltype, string InvoiceSeries, string sPrefix) // Updated by Settu to include invoice series in sales return
        {
            /*Prefix Added by Poongodi on 10/10/2017*/
            string sBillNo = await _HelperDataAccess.GetNumber(Canceltype == "0" ? Helpers.HelperDataAccess.TableName.SalesReturn : Helpers.HelperDataAccess.TableName.SalesCancel, CustomDateTime.IST, InstanceId, AccountId, InvoiceSeries, sPrefix);
            return sBillNo.ToString();
        }
        //finyear method added by Poongodi on 26/03/2017
        public async Task<string> GetFinYear(SalesReturn v)
        {
            DateTime InvoiceDate = v.ReturnDate as DateTime? ?? CustomDateTime.IST;
            string sFinyr = await _HelperDataAccess.GetFinyear(InvoiceDate, v.InstanceId, v.AccountId);
            return Convert.ToString(sFinyr);

        }

        //Added By Sarubala
        public async Task<SalesReturn> GetSalesReturnDetails(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SalesReturnTable.IdColumn, id);
            qb.AddColumn(SalesReturnTable.ReturnNoColumn, SalesReturnTable.ReturnDateColumn, SalesReturnTable.FinyearIdColumn, SalesReturnTable.LoyaltyIdColumn, SalesReturnTable.LoyaltyPtsColumn);
            var sr = await List<SalesReturn>(qb);

            return sr.FirstOrDefault();
        }


        //Code to get Return Items for edit - start
        public async Task<SalesReturn> GetReturnOnlyDetails(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Select);
            qb.AddColumn(SalesReturnTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(SalesReturnItemTable.Table, SalesReturnTable.IdColumn, SalesReturnItemTable.SalesReturnIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesReturnItemTable.Table, SalesReturnItemTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(ProductStockTable.Table, SalesReturnItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(ProductTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.LeftJoin(PatientTable.Table, PatientTable.IdColumn, SalesReturnTable.PatientIdColumn);
            qb.JoinBuilder.LeftJoin(SalesTable.Table, SalesReturnTable.SalesIdColumn, SalesTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.DiscountColumn);

            qb.JoinBuilder.AddJoinColumn(PatientTable.Table, PatientTable.IdColumn, PatientTable.NameColumn, PatientTable.ShortNameColumn, PatientTable.MobileColumn, PatientTable.EmailColumn, PatientTable.EmpIDColumn);

            qb.ConditionBuilder.And(SalesReturnTable.IdColumn, id);

            var isDeletedCondition = new CustomCriteria(SalesReturnItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            string isDelete = "1";
            var cc1 = new CriteriaColumn(SalesReturnItemTable.IsDeletedColumn, isDelete, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, isDeletedCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);

            var isCancelledCondition = new CustomCriteria(SalesReturnItemTable.CancelTypeColumn, CriteriaCondition.IsNull);
            qb.ConditionBuilder.And(isCancelledCondition);

            var sreturn = new SalesReturn();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    sreturn.Id = reader.ToString(SalesReturnTable.IdColumn.ColumnName);
                    sreturn.ReturnNo = reader.ToString(SalesReturnTable.ReturnNoColumn.ColumnName);
                    sreturn.InvoiceSeries = reader.ToString(SalesReturnTable.InvoiceSeriesColumn.ColumnName);
                    sreturn.Prefix = reader.ToString(SalesReturnTable.PrefixColumn.ColumnName);
                    sreturn.ReturnDate = reader.ToDateTime(SalesReturnTable.ReturnDateColumn.ColumnName);
                    sreturn.SalesId = reader.ToString(SalesReturnTable.SalesIdColumn.ColumnName);
                    //sreturn.IsAlongWithSale = reader.ToBoolNullable(SalesReturnTable.IsAlongWithSaleColumn.ColumnName);
                    sreturn.IsAlongWithSale = reader.ToString(SalesReturnTable.IsAlongWithSaleColumn.ColumnName) != "" ? reader.ToBoolNullable(SalesReturnTable.IsAlongWithSaleColumn.ColumnName) : false;
                    sreturn.TaxRefNo = reader.ToString(SalesReturnTable.TaxRefNoColumn.ColumnName) != "" ? reader.ToIntNullable(SalesReturnTable.TaxRefNoColumn.ColumnName) : 0; //Added by Poongodi on 01/07/2017

                    sreturn.Sales.Discount = reader.ToDecimalNullable(SalesTable.DiscountColumn.FullColumnName);
                    sreturn.Discount = sreturn.Sales.Discount;
                    sreturn.CreatedAt = reader.ToDateTime(SalesReturnTable.CreatedAtColumn.ColumnName); // Added by Sarubala on 30-08-17
                    sreturn.ReturnCharges = reader.ToDecimalNullable(SalesReturnTable.ReturnChargesColumn.ColumnName);
                    sreturn.Patient = new Contract.Infrastructure.Master.Patient()
                    {
                        Id = Convert.ToString(reader[PatientTable.IdColumn.FullColumnName]),
                        Name = Convert.ToString(reader[PatientTable.NameColumn.FullColumnName]),
                        ShortName = Convert.ToString(reader[PatientTable.ShortNameColumn.FullColumnName]),
                        Mobile = Convert.ToString(reader[PatientTable.MobileColumn.FullColumnName]),
                        Email = Convert.ToString(reader[PatientTable.EmailColumn.FullColumnName]),
                        EmpID = Convert.ToString(reader[PatientTable.EmpIDColumn.FullColumnName])

                    };


                    SalesReturnItem srItem = new SalesReturnItem();
                    srItem.Id = reader.ToString(SalesReturnItemTable.IdColumn.FullColumnName);
                    srItem.SalesReturnId = reader.ToString(SalesReturnItemTable.SalesReturnIdColumn.FullColumnName);
                    srItem.ProductStockId = reader.ToString(SalesReturnItemTable.ProductStockIdColumn.FullColumnName);
                    srItem.Quantity = reader.ToDecimalNullable(SalesReturnItemTable.QuantityColumn.FullColumnName);
                    srItem.Discount = reader.ToDecimalNullable(SalesReturnItemTable.DiscountColumn.FullColumnName);
                    srItem.MrpSellingPrice = reader.ToDecimalNullable(SalesReturnItemTable.MrpSellingPriceColumn.FullColumnName);
                    srItem.MRP = reader.ToDecimalNullable(SalesReturnItemTable.MRPColumn.FullColumnName);
                    /*GST columns added by Poongodi on 01/07/2017*/
                    srItem.Cgst = reader.ToDecimalNullable(SalesReturnItemTable.CgstColumn.FullColumnName);
                    srItem.Sgst = reader.ToDecimalNullable(SalesReturnItemTable.SgstColumn.FullColumnName);
                    srItem.Igst = reader.ToDecimalNullable(SalesReturnItemTable.IgstColumn.FullColumnName);
                    srItem.GstTotal = reader.ToDecimalNullable(SalesReturnItemTable.GstTotalColumn.FullColumnName);
                    srItem.IsDeleted = false;
                    srItem.PurchaseQuantity = 0;
                    srItem.Action = "I";

                    srItem.ProductStock.Fill(reader).Product.Fill(reader);

                    sreturn.SalesReturnItem.Add(srItem);

                }
            }

            if ((!string.IsNullOrEmpty(sreturn.SalesId) || !string.IsNullOrWhiteSpace(sreturn.SalesId)) && (string.IsNullOrEmpty(sreturn.PatientId) || string.IsNullOrWhiteSpace(sreturn.PatientId)))
            {
                sreturn.Patient = GetPatientName(sreturn.SalesId).Result;
            }

            if (!string.IsNullOrEmpty(sreturn.SalesId) || !string.IsNullOrWhiteSpace(sreturn.SalesId))
            {
                List<SalesItem> data = GetSalesItemList(sreturn.SalesId).Result;

                foreach (var x1 in sreturn.SalesReturnItem)
                {
                    foreach (var y1 in data)
                    {
                        //if (x1.ProductStock.Id == y1.ProductStock.Id && x1.MrpSellingPrice == y1.SellingPrice)
                        if (x1.ProductStock.Id == y1.ProductStock.Id)
                        {
                            x1.PurchaseQuantity = y1.Quantity;
                            x1.SoldPrice = y1.SellingPrice > 0 ? y1.SellingPrice : y1.ProductStock.SellingPrice;
                        }
                    }
                }

                List<SalesReturnItem> srdata = GetSalesCancelList(sreturn.SalesId).Result;

                foreach (var j1 in sreturn.SalesReturnItem)
                {
                    foreach (var k1 in srdata)
                    {
                        //if (j1.ProductStock.Id == k1.ProductStock.Id && ((j1.MrpSellingPrice == k1.ProductStock.SellingPrice) || (j1.MrpSellingPrice == k1.ProductStock.MRP)))
                        if (j1.ProductStock.Id == k1.ProductStock.Id)
                        {
                            j1.PurchaseQuantity = j1.PurchaseQuantity - k1.Quantity;
                        }
                    }
                }

                foreach (var item in sreturn.SalesReturnItem)
                {
                    decimal? qty = await GetPreviousSalesReturnItems(sreturn.SalesId, item.ProductStockId, item.Id);
                    item.PurchaseQuantity = item.PurchaseQuantity - qty;
                }

            }

            return sreturn;

        }

        // Added by sarubala on 10-07-17 - start
        public async Task<decimal?> GetCustomerPayments(string salesId, string AccId, string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Select);
            qb.SelectBuilder.GroupBy(CustomerPaymentTable.SalesIdColumn);
            qb.AddColumn(DbColumn.SumColumn("DebitAmt", CustomerPaymentTable.DebitColumn));
            qb.ConditionBuilder.And(CustomerPaymentTable.SalesIdColumn, salesId);
            qb.ConditionBuilder.And(CustomerPaymentTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(CustomerPaymentTable.InstanceIdColumn, InsId);
            return Convert.ToDecimal(await SingleValueAsyc(qb));
        }
        // Added by sarubala on 10-07-17 - end

        public async Task<decimal?> GetPreviousSalesReturnItems(string salesid, string productstockid, string salesReturnItemId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
            qb.SelectBuilder.GroupBy(SalesReturnItemTable.ProductStockIdColumn);
            qb.AddColumn(DbColumn.SumColumn("ReturnedQuantity", SalesReturnItemTable.QuantityColumn));
            qb.JoinBuilder.Join(SalesReturnTable.Table, SalesReturnTable.IdColumn, SalesReturnItemTable.SalesReturnIdColumn);
            qb.ConditionBuilder.And(SalesReturnTable.SalesIdColumn, salesid);
            qb.ConditionBuilder.And(SalesReturnItemTable.ProductStockIdColumn, productstockid);

            qb.ConditionBuilder.And(SalesReturnItemTable.IdColumn, CriteriaEquation.NotEqual);
            qb.Parameters.Add(SalesReturnItemTable.IdColumn.ColumnName, salesReturnItemId);

            var isDeletedCondition = new CustomCriteria(SalesReturnItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            string isDelete = "1";
            var cc1 = new CriteriaColumn(SalesReturnItemTable.IsDeletedColumn, isDelete, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, isDeletedCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);

            var isCancelCondition = new CustomCriteria(SalesReturnItemTable.CancelTypeColumn, CriteriaCondition.IsNull);
            string isCancel = "1";
            var cc2 = new CriteriaColumn(SalesReturnItemTable.CancelTypeColumn, isCancel, CriteriaEquation.NotEqual);
            var col2 = new CustomCriteria(cc2, isCancelCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col2);

            return Convert.ToDecimal(await SingleValueAsyc(qb));
        }

        private Task<List<SalesItem>> GetSalesItemList(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesItemTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesItemTable.ProductStockIdColumn);
            qb.ConditionBuilder.And(SalesItemTable.SalesIdColumn, id);
            return List<SalesItem>(qb);
        }

        private async Task<List<SalesReturnItem>> GetSalesCancelList(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Select);
            qb.JoinBuilder.Join(SalesReturnItemTable.Table, SalesReturnTable.IdColumn, SalesReturnItemTable.SalesReturnIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesReturnItemTable.Table, SalesReturnItemTable.IdColumn, SalesReturnItemTable.AccountIdColumn, SalesReturnItemTable.InstanceIdColumn, SalesReturnItemTable.SalesReturnIdColumn, SalesReturnItemTable.ProductStockIdColumn, SalesReturnItemTable.QuantityColumn, SalesReturnItemTable.MrpSellingPriceColumn, SalesReturnItemTable.MRPColumn, SalesReturnItemTable.DiscountColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
            qb.ConditionBuilder.And(SalesReturnTable.SalesIdColumn, id);
            qb.ConditionBuilder.And(SalesReturnTable.CancelTypeColumn, 1);

            var list1 = new List<SalesReturnItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    SalesReturnItem srItem = new SalesReturnItem();
                    srItem.Id = reader.ToString(SalesReturnItemTable.IdColumn.FullColumnName);
                    srItem.SalesReturnId = reader.ToString(SalesReturnItemTable.SalesReturnIdColumn.FullColumnName);
                    srItem.ProductStockId = reader.ToString(SalesReturnItemTable.ProductStockIdColumn.FullColumnName);
                    srItem.Quantity = reader.ToDecimalNullable(SalesReturnItemTable.QuantityColumn.FullColumnName);
                    srItem.Discount = reader.ToDecimalNullable(SalesReturnItemTable.DiscountColumn.FullColumnName);
                    srItem.MrpSellingPrice = reader.ToDecimalNullable(SalesReturnItemTable.MrpSellingPriceColumn.FullColumnName);
                    srItem.MRP = reader.ToDecimalNullable(SalesReturnItemTable.MRPColumn.FullColumnName);

                    srItem.ProductStock.Fill(reader);

                    list1.Add(srItem);
                }
            }

            return list1;
        }

        private async Task<Contract.Infrastructure.Master.Patient> GetPatientName(string salesId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SalesTable.IdColumn, salesId);
            qb.AddColumn(SalesTable.NameColumn, SalesTable.MobileColumn, SalesTable.PatientIdColumn);
            qb.JoinBuilder.LeftJoin(PatientTable.Table, PatientTable.IdColumn, SalesTable.PatientIdColumn);
            qb.JoinBuilder.AddJoinColumn(PatientTable.Table, PatientTable.IdColumn, PatientTable.NameColumn, PatientTable.ShortNameColumn, PatientTable.MobileColumn, PatientTable.EmailColumn, PatientTable.EmpIDColumn);

            var result = await List<Sales>(qb);
            var result1 = result.FirstOrDefault();

            if (string.IsNullOrEmpty(result1.PatientId) || string.IsNullOrWhiteSpace(result1.PatientId))
            {
                result1.Patient.Name = result1.Name;
                result1.Patient.Mobile = result1.Mobile;
            }

            return result1.Patient;

        }

        //Code to get Return Items for edit - end

        //Code to get update edited Return Items - start

        public async Task<SalesReturn> UpdateSalesReturn(SalesReturn sr)
        {
            HandleSalesReturnItems(sr);

            //Code added by Sarubala to update NetAmount in Sales for (Return Along With Sale) - Start
            List<SalesReturnItem> data = new List<SalesReturnItem>();
            if (!string.IsNullOrEmpty(sr.SalesId) && sr.IsAlongWithSale == true && sr.IsUpdateNetAmt == true)
            {
                data = ReturnOnlyItemList(sr.Id).Result;
            }

            foreach (var item in sr.SalesReturnItem)
            {
                if (!string.IsNullOrEmpty(sr.SalesId) && sr.IsAlongWithSale == true && sr.IsUpdateNetAmt == true)
                {
                    if (data.Any(x => x.Id == item.Id))
                    {
                        var salesReturnItem1 = data.FirstOrDefault(x => x.Id == item.Id);

                        var netAmount = await GetSalesNetAmt(sr.SalesId, sr.InstanceId, sr.AccountId);
                        netAmount = netAmount + (decimal)salesReturnItem1.FinalPrice;

                        var salesReturnItem2 = item;
                        salesReturnItem2.Quantity = salesReturnItem2.ReturnedQuantity;
                        var newNetAmt = netAmount - (decimal)salesReturnItem2.FinalPrice;
                        Sales s = new Sales();
                        s.Id = sr.SalesId;
                        s.AccountId = sr.AccountId;
                        s.InstanceId = sr.InstanceId;
                        s.UpdatedBy = sr.UpdatedBy;
                        s.RoundoffNetAmount = Math.Round(newNetAmt) - newNetAmt;
                        s.NetAmount = Math.Round(newNetAmt);

                        await UpdateSalesNetAmt(s);

                    }
                }
                var qbReturn = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Update);
                qbReturn.ConditionBuilder.And(SalesReturnTable.IdColumn, sr.Id);
                qbReturn.AddColumn(SalesReturnTable.ReturnChargesColumn, SalesReturnTable.UpdatedByColumn, SalesReturnTable.UpdatedAtColumn);
                qbReturn.Parameters.Add(SalesReturnTable.ReturnChargesColumn.ColumnName, sr.ReturnCharges);

                qbReturn.Parameters.Add(SalesReturnItemTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qbReturn.Parameters.Add(SalesReturnItemTable.UpdatedByColumn.ColumnName, sr.UpdatedBy);
                await QueryExecuter.NonQueryAsyc(qbReturn);

                //Code added by Sarubala to update NetAmount in Sales for (Return Along With Sale) - End

                var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(SalesReturnItemTable.IdColumn, item.Id);
                qb.AddColumn(SalesReturnItemTable.QuantityColumn, SalesReturnItemTable.DiscountColumn, SalesReturnItemTable.MrpSellingPriceColumn, SalesReturnItemTable.UpdatedByColumn, SalesReturnItemTable.UpdatedAtColumn);
                qb.Parameters.Add(SalesReturnItemTable.QuantityColumn.ColumnName, item.ReturnedQuantity);
                qb.Parameters.Add(SalesReturnItemTable.DiscountColumn.ColumnName, item.Discount);
                qb.Parameters.Add(SalesReturnItemTable.MrpSellingPriceColumn.ColumnName, item.MrpSellingPrice);
                qb.Parameters.Add(SalesReturnItemTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(SalesReturnItemTable.UpdatedByColumn.ColumnName, sr.UpdatedBy);
                await QueryExecuter.NonQueryAsyc(qb);

                if (item.Quantity != item.ReturnedQuantity)
                {
                    var alterQty = item.ReturnedQuantity - item.Quantity;
                    var productStock = await _productStockDataAccess.GetStockValue(item.ProductStock.Id);
                    productStock.Stock = productStock.Stock + alterQty;
                    if (productStock.Stock >= 0)
                    {
                        await _productStockDataAccess.Update(productStock);
                    }

                }

                //Code added by Sarubala on 10-07-17 for Accounts - start
                if (!string.IsNullOrEmpty(sr.SalesId))
                {
                    int CustomerPaymentSalesCount = await _salesDataAccess.CustomerPaymentSalesCount(sr.SalesId);
                    if (CustomerPaymentSalesCount > 0)
                    {
                        decimal creditAmt = 0;
                        if (item.Quantity > item.ReturnedQuantity)
                        {
                            decimal? alterQty = item.Quantity - item.ReturnedQuantity;
                            creditAmt = (decimal)((alterQty * item.MrpSellingPrice) - (alterQty * item.MrpSellingPrice * item.Discount / 100));

                            var Credit = await _salesDataAccess.GetSalesCredit(sr.SalesId);
                            creditAmt = Credit + creditAmt;
                        }
                        else if (item.Quantity <= item.ReturnedQuantity)
                        {
                            decimal? alterQty = item.ReturnedQuantity - item.Quantity;
                            creditAmt = (decimal)((alterQty * item.MrpSellingPrice) - (alterQty * item.MrpSellingPrice * item.Discount / 100));

                            var Credit = await _salesDataAccess.GetSalesCredit(sr.SalesId);
                            creditAmt = Credit >= creditAmt ? Credit - creditAmt : 0;
                        }

                        await UpdateCustomerPaymentSalesReturn(sr, creditAmt);
                    }

                }
                //Code added by Sarubala on 10-07-17 for Accounts - end
            }

            return sr;
        }

        //Code to get update edited Return Items - end

        //Code by Sarubala to save salesReturnItems in salesReturnItemAudit log - start

        public async Task HandleSalesReturnItems(SalesReturn sr)
        {

            foreach (var item in sr.SalesReturnItem)
            {
                if (item.Action == "U")
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
                    qb.ConditionBuilder.And(SalesReturnItemTable.IdColumn, item.Id);
                    qb.AddColumn(SalesReturnItemTable.Table.ColumnList.ToArray());

                    var salesReturnItem = List<SalesReturnItem>(qb);

                    var auditItem = salesReturnItem.Result.FirstOrDefault();
                    auditItem.Action = "U";
                    await Insert(auditItem, SalesReturnItemAuditTable.Table);

                }
            }
        }

        //Code by Sarubala to save salesReturnItems in salesReturnItemAudit log - end

        //Code by Sarubala to get salesReturnItems from Audit table - start
        public async Task<List<SalesReturnItem>> GetReturnItemAuditList(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemAuditTable.Table, OperationType.Select);
            qb.AddColumn(SalesReturnItemAuditTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(SalesReturnItemAuditTable.SalesReturnIdColumn, id);

            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemAuditTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, SalesReturnItemAuditTable.CreatedByColumn);
            qb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);

            List<SalesReturnItem> itemList = new List<SalesReturnItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {

                    SalesReturnItem srItem = new SalesReturnItem();
                    srItem.Id = reader.ToString(SalesReturnItemAuditTable.IdColumn.ColumnName);
                    srItem.SalesReturnId = reader.ToString(SalesReturnItemAuditTable.SalesReturnIdColumn.ColumnName);
                    srItem.ProductStockId = reader.ToString(SalesReturnItemAuditTable.ProductStockIdColumn.ColumnName);
                    srItem.Quantity = reader.ToDecimalNullable(SalesReturnItemAuditTable.QuantityColumn.ColumnName);
                    srItem.Discount = reader.ToDecimalNullable(SalesReturnItemAuditTable.DiscountColumn.ColumnName);
                    srItem.MrpSellingPrice = reader.ToDecimalNullable(SalesReturnItemAuditTable.MrpSellingPriceColumn.ColumnName);
                    srItem.MRP = reader.ToDecimalNullable(SalesReturnItemAuditTable.MRPColumn.ColumnName);
                    srItem.Action = reader.ToString(SalesReturnItemAuditTable.ActionColumn.ColumnName);
                    srItem.CreatedAt = reader.ToDateTime(SalesReturnItemAuditTable.CreatedAtColumn.ColumnName);
                    srItem.CreatedBy = reader.ToString(SalesReturnItemAuditTable.CreatedByColumn.ColumnName);

                    srItem.ProductStock.Fill(reader).Product.Fill(reader);

                    srItem.UpdatedUser = reader.ToString(HQueUserTable.NameColumn.FullColumnName);

                    itemList.Add(srItem);
                }
            }

            return itemList;
        }

        //Code by Sarubala to get salesReturnItems from Audit table - end


        //End Code Sarubala
        public async Task<int> Count(SalesReturn data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Count);
            qb.JoinBuilder.LeftJoin(SalesTable.Table, SalesTable.IdColumn, SalesReturnTable.SalesIdColumn);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        private static QueryBuilderBase SqlQueryBuilder(SalesReturn data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Sales.PatientId)) // Changed by Violet search Based on Patientid
            {
                //qb.ConditionBuilder.And(SalesTable.PatientIdColumn, CriteriaEquation.Equal);
                //qb.Parameters.Add(SalesTable.PatientIdColumn.ColumnName, data.Sales.PatientId);

                string id1 = "\'" + data.Sales.PatientId + "\'";
                string id2 = "\'" + data.PatientId + "\'";

                var cc1 = new CriteriaColumn(SalesTable.PatientIdColumn, id1, CriteriaEquation.Equal);
                var cc2 = new CriteriaColumn(SalesReturnTable.PatientIdColumn, id2, CriteriaEquation.Equal);
                var col1 = new CustomCriteria(cc1, cc2, CriteriaCondition.Or);
                qb.ConditionBuilder.And(col1);

            }
            else
            {
                if (!string.IsNullOrEmpty(data.Sales.Name))
                {
                    qb.ConditionBuilder.And(SalesTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Both);
                    qb.Parameters.Add(SalesTable.NameColumn.ColumnName, data.Sales.Name);
                }
                if (!string.IsNullOrEmpty(data.Sales.Mobile))
                {
                    qb.ConditionBuilder.And(SalesTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(SalesTable.MobileColumn.ColumnName, data.Sales.Mobile);
                }
                if (!string.IsNullOrEmpty(data.Sales.Email))
                {
                    qb.ConditionBuilder.And(SalesTable.EmailColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(SalesTable.EmailColumn.ColumnName, data.Sales.Email);
                }

            }

            //Added by Sarubala for Return No filter on 04-07-2017 - start
            if (data.Select1 == "Custom")
            {
                var DefaultSeriesCondition = new CustomCriteria(SalesReturnTable.InvoiceSeriesColumn, CriteriaCondition.IsNotNull);
                string isManual = "\'" + "MAN" + "\'";
                var cc1 = new CriteriaColumn(SalesReturnTable.InvoiceSeriesColumn, isManual, CriteriaEquation.NotEqual);
                var col1 = new CustomCriteria(cc1, DefaultSeriesCondition, CriteriaCondition.And);
                qb.ConditionBuilder.And(col1);

                var emptySeries = new CustomCriteria("SalesReturn.InvoiceSeries != ''");
                qb.ConditionBuilder.And(emptySeries);
            }
            else if (data.Select1 == "Default")
            {
                var DefaultSeriesCondition1 = new CustomCriteria(SalesReturnTable.InvoiceSeriesColumn, CriteriaCondition.IsNull);
                string emptySeries1 = "\'\'";
                var cc2 = new CriteriaColumn(SalesReturnTable.InvoiceSeriesColumn, emptySeries1, CriteriaEquation.Equal);
                var col2 = new CustomCriteria(cc2, DefaultSeriesCondition1, CriteriaCondition.Or);
                qb.ConditionBuilder.And(col2);
            }
            else if (data.Select1 == "Manual")
            {
                qb.ConditionBuilder.And(SalesReturnTable.InvoiceSeriesColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(SalesReturnTable.InvoiceSeriesColumn.ColumnName, "MAN");
            }

            if (data.Select1 == "Custom" && data.SelectedSeries != "All")
            {
                qb.ConditionBuilder.And(SalesReturnTable.InvoiceSeriesColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(SalesReturnTable.InvoiceSeriesColumn.ColumnName, data.SelectedSeries);
            }

            if (!string.IsNullOrEmpty(data.FromReturnNo))
            {
                var col1 = new CustomCriteria("(Isnull(SalesReturn.Prefix,'')+ SalesReturn.ReturnNo like '" + data.FromReturnNo + "%' or   SalesReturn.ReturnNo like '" + data.FromReturnNo + "%')");
                qb.ConditionBuilder.And(col1);
            }

            //Added by Sarubala for Return No filter on 04-07-2017 - end


            // Changed by Violet for returnlist date and series filter on 16-06-2017
            //if (!string.IsNullOrEmpty(data.FromReturnNo) && !string.IsNullOrEmpty(data.ToReturnNo))
            //{
            //    var cc1 = new CriteriaColumn(SalesReturnTable.ReturnNoColumn, "" + Convert.ToInt32(data.FromReturnNo) + "", CriteriaEquation.GreaterEqual);
            //    var cc2 = new CriteriaColumn(SalesReturnTable.ReturnNoColumn, "" + Convert.ToInt32(data.ToReturnNo) + "", CriteriaEquation.LesserEqual);
            //    var condition4 = new CustomCriteria(cc1, cc2, CriteriaCondition.And);
            //    qb.ConditionBuilder.And(condition4);

            //}
            //else if (!string.IsNullOrEmpty(data.FromReturnNo))
            //{
            //    qb.ConditionBuilder.And(SalesReturnTable.ReturnNoColumn, CriteriaEquation.GreaterEqual);
            //    qb.Parameters.Add(SalesReturnTable.ReturnNoColumn.ColumnName, Convert.ToInt32(data.FromReturnNo));

            //}
            //else if (!string.IsNullOrEmpty(data.ToReturnNo))
            //{
            //    qb.ConditionBuilder.And(SalesReturnTable.ReturnNoColumn, CriteriaEquation.LesserEqual);
            //    qb.Parameters.Add(SalesReturnTable.ReturnNoColumn.ColumnName, Convert.ToInt32(data.ToReturnNo));
            //}
            //else
            //{
            //    qb.ConditionBuilder.And(SalesReturnTable.ReturnNoColumn, CriteriaEquation.LesserEqual);
            //    qb.Parameters.Add(SalesReturnTable.ReturnNoColumn.ColumnName, data.FromReturnNo + data.ToReturnNo);
            //}
            if (!string.IsNullOrEmpty(data.Sales.InvoiceNo))
            {
                //qb.ConditionBuilder.And(SalesTable.InvoiceNoColumn, CriteriaEquation.Equal, CriteriaLike.Begin);
                //qb.Parameters.Add(SalesTable.InvoiceNoColumn.ColumnName, data.Sales.InvoiceNo);

                /*Prefix filter added by Poongodi on 23/06/2017*/
                /*exact InvoiceNo search  by nandhini on 14/12/2017*/
                var col1 = new CustomCriteria("(Isnull(Sales.Prefix,'')+ isnull(sales.InvoiceSeries,'')+ Sales.InvoiceNo = '" + data.Sales.InvoiceNo + "' or   isnull(sales.InvoiceSeries,'')+ Sales.InvoiceNo = '" + data.Sales.InvoiceNo + "' or Sales.InvoiceNo = '" + data.Sales.InvoiceNo + "')");
                qb.ConditionBuilder.And(col1);
            }
            if (!string.IsNullOrEmpty(data.ReturnNo))
            {
                //qb.ConditionBuilder.And(SalesReturnTable.ReturnNoColumn, CriteriaEquation.Equal, CriteriaLike.Begin);
                //qb.Parameters.Add(SalesReturnTable.ReturnNoColumn.ColumnName, data.ReturnNo);
                /*Prefix filter added by Poongodi on 23/06/2017*/
                var col1 = new CustomCriteria("(Isnull(SalesReturn.Prefix,'')+ SalesReturn.ReturnNo like '" + data.ReturnNo + "%' or   SalesReturn.ReturnNo like '" + data.ReturnNo + "%')");
                qb.ConditionBuilder.And(col1);
            }
            //added by nandhini for sales cancel no filter search on 11.9.2017
            if (data.FromCancelNo != null && data.ToCancelNo != null)
            {
                string r1 = "1", r2 = "2";
                var cc1 = new CriteriaColumn(SalesReturnTable.CancelTypeColumn, r1);
                var cc2 = new CriteriaColumn(SalesReturnTable.CancelTypeColumn, r2);
                var condition3 = new CustomCriteria(cc1, cc2, CriteriaCondition.Or);
                qb.ConditionBuilder.And(condition3);
                var cond1 = new CustomCriteria("(SalesReturn.ReturnNo between " + data.FromCancelNo + " and " + data.ToCancelNo + ")");
                qb.ConditionBuilder.And(cond1);

            }
            //end

            if (data.FromReturnDate != null && data.ToReturnDate != null)
            {
                var cc1 = new CriteriaColumn(SalesReturnTable.ReturnDateColumn, "'" + data.FromReturnDate.Value.ToString("yyyy-MM-dd") + "'", CriteriaEquation.GreaterEqual);
                var cc2 = new CriteriaColumn(SalesReturnTable.ReturnDateColumn, "'" + data.ToReturnDate.Value.ToString("yyyy-MM-dd") + "'", CriteriaEquation.LesserEqual);
                var condition4 = new CustomCriteria(cc1, cc2, CriteriaCondition.And);
                qb.ConditionBuilder.And(condition4);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(SalesReturnTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SalesReturnTable.InstanceIdColumn, data.InstanceId);
            }

            return qb;
        }

        public async Task<IEnumerable<SalesReturn>> List(SalesReturn data)
        {
            /*Prefix Added by Poongodi on 15/06/2017*/
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Select);
            qb.JoinBuilder.LeftJoin(SalesTable.Table, SalesTable.IdColumn, SalesReturnTable.SalesIdColumn);
            qb.JoinBuilder.LeftJoin(PatientTable.Table, PatientTable.IdColumn, SalesReturnTable.PatientIdColumn);//by San 
            qb.JoinBuilder.LeftJoin(VoucherTable.Table, VoucherTable.ReturnIdColumn, SalesReturnTable.IdColumn);
            qb = SqlQueryBuilder(data, qb);
            //ReturnDateColumn
            //qb.AddColumn(SalesReturnTable.IdColumn, SalesReturnTable.ReturnNoColumn, SalesReturnTable.ReturnDateColumn);
            qb.AddColumn(SalesReturnTable.IdColumn, SalesReturnTable.PrefixColumn, SalesReturnTable.ReturnNoColumn, SalesReturnTable.UpdatedAtColumn, SalesReturnTable.ReturnDateColumn, SalesReturnTable.TaxRefNoColumn, SalesReturnTable.InvoiceSeriesColumn, SalesReturnTable.SalesIdColumn, SalesReturnTable.CancelTypeColumn, SalesReturnTable.ReturnChargesColumn, SalesReturnTable.NetAmountColumn, SalesReturnTable.RoundOffNetAmountColumn);
            qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.IdColumn, SalesTable.NameColumn, SalesTable.MobileColumn, SalesTable.EmailColumn, SalesTable.PrefixColumn, SalesTable.InvoiceNoColumn, SalesTable.InvoiceSeriesColumn, SalesTable.DiscountColumn, SalesTable.NetAmountColumn, SalesTable.RoundoffNetAmountColumn);
            qb.JoinBuilder.AddJoinColumn(PatientTable.Table, PatientTable.NameColumn, PatientTable.MobileColumn);
            qb.JoinBuilder.AddJoinColumn(VoucherTable.Table, VoucherTable.IdColumn, VoucherTable.AmountColumn, VoucherTable.OriginalAmountColumn);

            if (!string.IsNullOrEmpty(data.InstanceId))
            {
                qb.JoinBuilder.Join(InstanceTable.Table, InstanceTable.IdColumn, SalesReturnTable.InstanceIdColumn);
                qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.GstselectColumn);
                qb.ConditionBuilder.And(InstanceTable.IdColumn, data.InstanceId, CriteriaEquation.Equal);
            }

            //added by nandhini for sales return product search filter 18.9.2017
            if (!string.IsNullOrEmpty(data.searchProductId))
            {
                qb.JoinBuilder.Join(SalesReturnItemTable.Table, SalesReturnItemTable.SalesReturnIdColumn, SalesReturnTable.IdColumn);
                qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
                qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, data.searchProductId, CriteriaEquation.Equal);
            }
            //end
            ////by san 
            //qb.JoinBuilder.LeftJoin(SalesItemTable.Table, SalesItemTable.SalesIdColumn, SalesTable.IdColumn);
            //qb.JoinBuilder.AddJoinColumn(SalesItemTable.Table, SalesItemTable.SellingPriceColumn);
            qb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, SalesReturnTable.UpdatedByColumn);
            qb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);

            qb.SelectBuilder.SortByDesc(SalesReturnTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);

            var result = await List<SalesReturn>(qb);
            //var RetrunId = string.Join(",", result.Select(x => x.Id).ToList());
            //var SalesId = string.Join(",", result.Select(x => x.Sales.Id).ToList());
            //Dictionary<string, object> parms = new Dictionary<string, object>();
            //parms = new Dictionary<string, object>();
            //parms.Add("InstanceId", data.InstanceId);

            //parms.Add("SaleId", SalesId);

            //var saleReturnItem_Result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_get_saleRetrunItem_Details", parms);
            //var saleReturnItems = saleReturnItem_Result.Select(x =>
            //{
            //    var salesReturnItem = new SalesItem
            //    {
            //        ProductStockId = x.ProductStockId,
            //        salesReturnId = x.SalesReturnId,
            //        Quantity = x.Quantity,
            //        //CancelType = x.CancelType as int? ?? 0,
            //       SellingPrice = x.MrpSellingPrice as decimal? ?? 0,
            //        Discount = x.Discount as decimal? ?? 0,
            //        ProductStock =
            //            {
            //                SellingPrice = (decimal) x.SellingPrice,
            //                Product = {Name = x.ProductName.ToString()},
            //                BatchNo = x.BatchNo.ToString(),
            //                VAT = (decimal) x.VAT,
            //                ExpireDate = (DateTime) x.ExpireDate
            //            },

            //    };
            //    return salesReturnItem;
            //});

            foreach (var item in result)
            {
                item.Discount = item.Sales.Discount;

                item.DebitAmt = 0;
                if (!string.IsNullOrEmpty(item.SalesId) && !string.IsNullOrWhiteSpace(item.SalesId))
                {
                    item.DebitAmt = GetCustomerPayments(item.SalesId, data.AccountId, data.InstanceId).Result as decimal? ?? 0; // Added by sarubala on 10-07-17
                }
            }

            if (result.Count > 0)
            {
                var sri = await SqlQueryList(result, data.searchProductId);
                //do 
                var salesReturn = result.Select(x =>
                {
                    x.SalesReturnItem = sri.Where(y => y.SalesReturnId == x.Id).Select(z => z).ToList();

                    //x.SalesItem = saleReturnItems.Where(y => y.salesReturnId == x.Id).Select(z => z).ToList();
                    return x;
                });
                return salesReturn;
            }
            return result as IEnumerable<SalesReturn>;
        }


        public async Task<List<SalesReturnItem>> SqlQueryList(IEnumerable<SalesReturn> data, string productId)
        {
            var sriqb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
            sriqb.AddColumn(SalesReturnItemTable.ProductStockIdColumn, SalesReturnItemTable.QuantityColumn, SalesReturnItemTable.SalesReturnIdColumn, SalesReturnItemTable.MrpSellingPriceColumn, SalesReturnItemTable.MRPColumn, SalesReturnItemTable.DiscountColumn, SalesReturnItemTable.CancelTypeColumn, SalesReturnItemTable.IsDeletedColumn);

            sriqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
            sriqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.MRPColumn, ProductStockTable.VATColumn, ProductStockTable.GstTotalColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn);

            sriqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            sriqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            ////by san
            //sriqb.JoinBuilder.LeftJoin(SalesItemTable.Table, SalesItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            //sriqb.JoinBuilder.AddJoinColumn(SalesItemTable.Table, SalesItemTable.SellingPriceColumn);
            sriqb.SelectBuilder.SortBy(SalesReturnItemTable.CreatedAtColumn);// Added by Sarubala to list the items in sales order on 21-08-2017

            if (!string.IsNullOrEmpty(productId))
            {
                sriqb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, productId, CriteriaEquation.Equal);
            }

            var i = 1;
            if (string.IsNullOrEmpty(productId))
            {
                foreach (var salereturn in data)
                {
                    var paramName = $"s{i++}";
                    var caol = new CriteriaColumn(SalesReturnItemTable.SalesReturnIdColumn, $"@{paramName}");
                    var cc = new CustomCriteria(caol);
                    sriqb.ConditionBuilder.Or(cc);
                    sriqb.Parameters.Add(paramName, salereturn.Id);
                }

            }

            var list = new List<SalesReturnItem>();

            using (var reader = await QueryExecuter.QueryAsyc(sriqb))
            {
                while (reader.Read())
                {
                    var salesReturnItem = new SalesReturnItem
                    {
                        ProductStockId = reader[SalesReturnItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        SalesReturnId = reader[SalesReturnItemTable.SalesReturnIdColumn.ColumnName].ToString(),
                        // Quantity = (decimal)reader[SalesReturnItemTable.QuantityColumn.ColumnName],
                        Quantity = reader[SalesReturnItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        CancelType = Convert.ToInt16(reader[SalesReturnItemTable.CancelTypeColumn.ColumnName] is System.DBNull ? 0 : reader[SalesReturnItemTable.CancelTypeColumn.ColumnName]),
                        MrpSellingPrice = reader[SalesReturnItemTable.MrpSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        MRP = reader[SalesReturnItemTable.MRPColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[SalesReturnItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        select = reader.ToString(SalesReturnItemTable.IsDeletedColumn.ColumnName),
                        ProductStock =
                        {
                           // SellingPrice = (decimal) reader[ProductStockTable.SellingPriceColumn.FullColumnName],
                           SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName]as decimal ? ??0,
                        // MRP = (decimal) reader[ProductStockTable.MRPColumn.FullColumnName],
                           MRP =reader[ProductStockTable.MRPColumn.FullColumnName]as decimal ? ??0,
                            Product = {Name = reader[ProductTable.NameColumn.FullColumnName].ToString()},
                            BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString(),
                            //VAT = (decimal) reader[ProductStockTable.VATColumn.FullColumnName],
                            VAT = reader[ProductStockTable.VATColumn.FullColumnName]as decimal? ??0,
                            GstTotal = reader[ProductStockTable.GstTotalColumn.FullColumnName] as decimal? ?? 0,
                            ExpireDate = (DateTime) reader[ProductStockTable.ExpireDateColumn.FullColumnName]
                        },

                        //SalesItem =
                        //{
                        //     SellingPrice = reader[SalesItemTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                        //}

                    };

                    salesReturnItem.IsDeleted = salesReturnItem.select == "True";

                    list.Add(salesReturnItem);
                }
            }
            return list;
        }
        //Added by Settu to capture sales return credit amount
        public async Task<Voucher> saveVoucherNote(Voucher data)
        {
            var voucher = await GetExistingVoucher(data.AccountId, data.InstanceId, data.ReturnId);
            if (voucher != null)
            {
                data.Id = voucher.Id;
                return await UpdateVoucherNote(data);
            }
            else
            {
                if (data.OriginalAmount > 0)
                {
                    return await Insert(data, VoucherTable.Table);
                }
                return data;
            }
        }

        public async Task<Voucher> UpdateVoucherNote(Voucher voucher)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VoucherTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(VoucherTable.IdColumn, voucher.Id);
            if (voucher.ParticipantId != null && voucher.ParticipantId != "")
            {
                qb.AddColumn(VoucherTable.OriginalAmountColumn, VoucherTable.AmountColumn, VoucherTable.UpdatedByColumn, VoucherTable.UpdatedAtColumn, VoucherTable.OfflineStatusColumn, VoucherTable.ParticipantIdColumn);
            }
            else
            {
                qb.AddColumn(VoucherTable.OriginalAmountColumn, VoucherTable.AmountColumn, VoucherTable.UpdatedByColumn, VoucherTable.UpdatedAtColumn, VoucherTable.OfflineStatusColumn);
            }
            qb.Parameters.Add(VoucherTable.OriginalAmountColumn.ColumnName, voucher.Amount);
            qb.Parameters.Add(VoucherTable.AmountColumn.ColumnName, voucher.Amount);
            qb.Parameters.Add(VoucherTable.OfflineStatusColumn.ColumnName, voucher.OfflineStatus);
            qb.Parameters.Add(VoucherTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(VoucherTable.UpdatedByColumn.ColumnName, voucher.UpdatedBy);
            if (voucher.ParticipantId != null && voucher.ParticipantId != "")
            {
                qb.Parameters.Add(VoucherTable.ParticipantIdColumn.ColumnName, voucher.ParticipantId);
            }
            await QueryExecuter.NonQueryAsyc(qb);

            return voucher;
        }

        public async Task<Voucher> GetExistingVoucher(string AccountId, string InstanceId, string ReturnId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VoucherTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VoucherTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(VoucherTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(VoucherTable.ReturnIdColumn, ReturnId);

            var voucher = (await List<Voucher>(qb)).FirstOrDefault();
            return voucher;
        }

        public async Task<Sales> GetSalesNet(string SalesId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SalesTable.IdColumn, SalesId);

            var sales = (await List<Sales>(qb)).FirstOrDefault();
            return sales;
        }

        //Added by Sarubala to update Customer payments during Return Edit - start

        public async Task UpdateCustomerPaymentSalesReturn(SalesReturn salesReturn, decimal creditAmt)
        {
            var List = await _salesDataAccess.GetCustomerPaymentId(salesReturn.SalesId);

            var CreditAmount = creditAmt;
            var CustomerPayment = new CustomerPayment();

            CustomerPayment.Debit = 0.00M;
            CustomerPayment.Credit = Convert.ToDecimal(CreditAmount);

            // Update CustomerPayment
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(CustomerPaymentTable.AccountIdColumn, salesReturn.AccountId);
            qb.ConditionBuilder.And(CustomerPaymentTable.InstanceIdColumn, salesReturn.InstanceId);
            qb.ConditionBuilder.And(CustomerPaymentTable.SalesIdColumn, salesReturn.SalesId);
            qb.ConditionBuilder.And(CustomerPaymentTable.IdColumn, List[0].Id);
            qb.AddColumn(CustomerPaymentTable.TransactionDateColumn, CustomerPaymentTable.DebitColumn, CustomerPaymentTable.CreditColumn, CustomerPaymentTable.UpdatedAtColumn, CustomerPaymentTable.RemarksColumn);
            qb.Parameters.Add(CustomerPaymentTable.TransactionDateColumn.ColumnName, CustomDateTime.IST);
            qb.Parameters.Add(CustomerPaymentTable.DebitColumn.ColumnName, CustomerPayment.Debit);
            qb.Parameters.Add(CustomerPaymentTable.CreditColumn.ColumnName, CustomerPayment.Credit);
            qb.Parameters.Add(CustomerPaymentTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            string remarks = "Sales ReturnEdit";
            qb.Parameters.Add(CustomerPaymentTable.RemarksColumn.ColumnName, remarks);
            await QueryExecuter.NonQueryAsyc(qb);

            // Update Sales
            var qbSale = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
            qbSale.ConditionBuilder.And(SalesTable.AccountIdColumn, salesReturn.AccountId);
            qbSale.ConditionBuilder.And(SalesTable.InstanceIdColumn, salesReturn.InstanceId);
            qbSale.ConditionBuilder.And(SalesTable.IdColumn, salesReturn.SalesId);
            qbSale.AddColumn(SalesTable.CreditColumn, SalesTable.UpdatedAtColumn);
            qbSale.Parameters.Add(SalesTable.CreditColumn.ColumnName, CreditAmount);
            qbSale.Parameters.Add(SalesTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            await QueryExecuter.NonQueryAsyc(qbSale);

        }

        //Added by Sarubala to update Customer payments during Return Edit - end

        //Code added by Sarubala to get NetAmount in Sales for (Return Along With Sale) - Start

        public async Task<decimal> GetSalesNetAmt(string salesId, string InsId, string AccId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, InsId);
            qb.ConditionBuilder.And(SalesTable.IdColumn, salesId);
            qb.AddColumn(SalesTable.NetAmountColumn, SalesTable.RoundoffNetAmountColumn);

            var netAmt = (await List<Sales>(qb)).FirstOrDefault().NetAmount as decimal? ?? 0;
            var roundOff = (await List<Sales>(qb)).FirstOrDefault().RoundoffNetAmount as decimal? ?? 0;
            return (netAmt + (roundOff * (-1)));
        }
        //Code added by Sarubala to get NetAmount in Sales for (Return Along With Sale) - End

        //Code added by Sarubala to update NetAmount in Sales for (Return Along With Sale) - Start
        public async Task UpdateSalesNetAmt(Sales s)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
            qb.AddColumn(SalesTable.NetAmountColumn, SalesTable.UpdatedAtColumn, SalesTable.UpdatedByColumn, SalesTable.RoundoffNetAmountColumn);
            qb.Parameters.Add(SalesTable.NetAmountColumn.ColumnName, s.NetAmount);
            qb.Parameters.Add(SalesTable.RoundoffNetAmountColumn.ColumnName, s.RoundoffNetAmount);
            qb.Parameters.Add(SalesTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(SalesTable.UpdatedByColumn.ColumnName, s.UpdatedBy);

            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, s.AccountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, s.InstanceId);
            qb.ConditionBuilder.And(SalesTable.IdColumn, s.Id);
            await QueryExecuter.NonQueryAsyc(qb);
        }

        //Code added by Sarubala to update NetAmount in Sales for (Return Along With Sale) - End

        // By San -- 08-08-2017
        public async Task<string> GetSalesReturnIdByRetrunNo(int InvoiceSeriesType, string SeriesTypeValue, string InvNo, string accountid, string instanceid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Type", InvoiceSeriesType);
            parms.Add("TypeValue", SeriesTypeValue);
            parms.Add("Value", InvNo);
            parms.Add("AccountId", accountid);
            parms.Add("InstanceId", instanceid);

            var saleReturnId = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesReturnIdByRetrunNo", parms);
            var rows = saleReturnId;
            foreach (IDictionary<string, object> row in rows)
            {
                foreach (var pair in row)
                {
                    Console.WriteLine("  {0} = {1}", pair.Key, pair.Value);
                    return (string)pair.Value;
                }
            }
            return "";
        }
        #region Sales Order Cancel
        public async Task<SalesReturnItem> UpdateSalesOrderCancelQty(SalesReturnItem data, string AccountId, string InstanceId)
        {

            var salesOrderQty = await getSalesOrderReceivedQty(data.SalesOrderEstimateId, AccountId, InstanceId, data.ProductStock.ProductId);
            if (salesOrderQty != null)
            {
                foreach (var salesOrder in salesOrderQty)
                {
                    decimal? ReceivedQty = 0;
                    if (salesOrder.ReceivedQty == 0)
                    {
                        if (salesOrder.Quantity > data.PurchaseQuantity)
                        {
                            ReceivedQty = salesOrder.ReceivedQty - data.PurchaseQuantity;
                        }
                        else
                        {
                            ReceivedQty = salesOrder.ReceivedQty;
                        }
                    }
                    else
                    {
                        if (salesOrder.Quantity > salesOrder.ReceivedQty)
                        {
                            ReceivedQty = salesOrder.ReceivedQty - data.PurchaseQuantity;
                        }
                        else
                        {
                            ReceivedQty = salesOrder.ReceivedQty - data.PurchaseQuantity;
                        }
                        if (ReceivedQty < 0)
                        {
                            ReceivedQty = 0;
                        }
                    }

                    var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Update);
                    qb.ConditionBuilder.And(SalesOrderEstimateItemTable.AccountIdColumn, AccountId);
                    qb.ConditionBuilder.And(SalesOrderEstimateItemTable.InstanceIdColumn, InstanceId);
                    qb.ConditionBuilder.And(SalesOrderEstimateItemTable.ProductIdColumn, data.ProductStock.ProductId);
                    qb.ConditionBuilder.And(SalesOrderEstimateItemTable.SalesOrderEstimateIdColumn, data.SalesOrderEstimateId);
                    qb.AddColumn(SalesOrderEstimateItemTable.IsOrderColumn, SalesOrderEstimateItemTable.UpdatedAtColumn, SalesOrderEstimateItemTable.UpdatedByColumn, SalesOrderEstimateItemTable.ReceivedQtyColumn);
                    qb.Parameters.Add(SalesOrderEstimateItemTable.IsOrderColumn.ColumnName, "");
                    qb.Parameters.Add(SalesOrderEstimateItemTable.ReceivedQtyColumn.ColumnName, ReceivedQty);
                    qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                    qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedByColumn.ColumnName, data.ProductStock.UpdatedBy);
                    await QueryExecuter.NonQueryAsyc(qb);

                    int salesCount = await SalesOrderEstimatePendingCount(data.SalesOrderEstimateId, AccountId, InstanceId);
                    if (salesCount == 0)
                    {
                        var qb1 = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateTable.Table, OperationType.Update);
                        qb1.ConditionBuilder.And(SalesOrderEstimateTable.AccountIdColumn, AccountId);
                        qb1.ConditionBuilder.And(SalesOrderEstimateTable.InstanceIdColumn, InstanceId);
                        qb1.ConditionBuilder.And(SalesOrderEstimateTable.IdColumn, data.SalesOrderEstimateId);
                        qb1.AddColumn(SalesOrderEstimateTable.IsEstimateActiveColumn, SalesOrderEstimateTable.UpdatedAtColumn, SalesOrderEstimateTable.UpdatedByColumn);
                        qb1.Parameters.Add(SalesOrderEstimateTable.IsEstimateActiveColumn.ColumnName, 0);
                        qb1.Parameters.Add(SalesOrderEstimateTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                        qb1.Parameters.Add(SalesOrderEstimateTable.UpdatedByColumn.ColumnName, data.ProductStock.UpdatedBy);
                        await QueryExecuter.NonQueryAsyc(qb1);
                    }
                }
            }

            return data;
        }

        public async Task<int> SalesOrderEstimatePendingCount(string SalesOrderEstimateId, string AccountId, string InstanceId)
        {
            var query = $@"select count(SalesOrderEstimateId) as SalesOrderEstimateId from SalesOrderEstimateitem (nolock) where SalesOrderEstimateId='{SalesOrderEstimateId}' 
            and AccountId='{AccountId}' and InstanceId='{InstanceId}'  and IsOrder = 1";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var count = Convert.ToInt32(await SingleValueAsyc(qb));
            return count;
        }

        public async Task<IEnumerable<SalesOrderEstimateItem>> getSalesOrderReceivedQty(string SalesOrderEstimateId, string AccountId, string InstanceId, string ProductId)
        {
            string query = $@"select Quantity,ISNULL(ReceivedQty,0) as ReceivedQty from SalesOrderEstimateitem where SalesOrderEstimateId='{SalesOrderEstimateId}' and AccountId='{AccountId}' and InstanceId='{InstanceId}' and ProductId='{ProductId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<SalesOrderEstimateItem>(qb);
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
