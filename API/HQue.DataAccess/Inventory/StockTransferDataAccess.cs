﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccess.Criteria;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Helpers.Extension;
using Utilities.Enum;
using System.Linq;
using Utilities.Helpers;
using HQue.Contract.Infrastructure.Setup;
using HQue.Contract.Infrastructure.Settings;
using HQue.DataAccess;
using DataAccess;
using HQue.DataAccess.Helpers;
using HQue.Contract.External;
using Microsoft.AspNetCore.Http;
using HQue.Contract.Infrastructure.Misc;
using HQue.Contract.Base;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;

namespace HQue.DataAccess.Inventory
{
    public class StockTransferDataAccess : BaseDataAccess
    {
        private readonly HelperDataAccess _HelperDataAccess;
        private readonly IHttpContextAccessor _httpContextAccessor;
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        private readonly ProductStockDataAccess _productStockDataAccess;
        public StockTransferDataAccess(ISqlHelper sqlQueryExecuter, HelperDataAccess helperDataAccess, IHttpContextAccessor httpContextAccessor, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper, ProductStockDataAccess productStockDataAccess) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _HelperDataAccess = helperDataAccess;
            _productStockDataAccess = productStockDataAccess;
            _httpContextAccessor = httpContextAccessor;
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        public override Task<StockTransfer> Save<StockTransfer>(StockTransfer data)
        {
            return Insert(data, StockTransferTable.Table);
         }       
        public async Task Save(IEnumerable<StockTransferItem> data, string sInstanceId, bool bOfflineSyncQueue)
        {
    
            foreach (var stockTransferItem in data)
            {
                await FillStockDetails(stockTransferItem);
                //added by nandhini for transaction

                if (stockTransferItem.WriteExecutionQuery)
                {
                    SetInsertMetaData(stockTransferItem, StockTransferItemTable.Table);
                    WriteInsertExecutionQuery(stockTransferItem, StockTransferItemTable.Table);
                }
                else
                {
                    await Insert(stockTransferItem, StockTransferItemTable.Table);

                    //Manual Sync insert process for Online to offline transfer by Poongodi on 08/11/2017
                    if (_configHelper.AppConfig.OfflineMode == false && bOfflineSyncQueue)
                    {
                        WriteSyncQueueInsert(stockTransferItem, StockTransferItemTable.Table, "I", sInstanceId, true);
                    }
                }
            }
        }
        //Newly created by Mani on 01-May-2017
        private async Task FillStockDetails(StockTransferItem stockTransferItem)
        {
            if (stockTransferItem.ProductStockId != null)
            {
                var ps = await _productStockDataAccess.GetStockValue(stockTransferItem.ProductStockId);
                stockTransferItem.ProductId = ps.ProductId;
                stockTransferItem.BatchNo = ps.BatchNo;
                stockTransferItem.ExpireDate = ps.ExpireDate;
                if (stockTransferItem.TaxRefNo == 1)
                {
                    stockTransferItem.VAT = 0;
                    if (stockTransferItem.FromTinNo == stockTransferItem.ToTinNo)
                    {
                        stockTransferItem.Cgst = ps.Cgst;
                        stockTransferItem.Sgst = ps.Sgst;
                        stockTransferItem.Igst = 0;
                    }
                    else
                    {
                        stockTransferItem.Igst = ps.Igst;
                        stockTransferItem.Cgst = 0;
                        stockTransferItem.Sgst = 0;
                    }
                    stockTransferItem.GstTotal = stockTransferItem.GstTotal != null? stockTransferItem.GstTotal:ps.GstTotal ;
                }
                else
                {
                    stockTransferItem.VAT = ps.VAT;
                    stockTransferItem.Igst = 0;
                    stockTransferItem.Cgst = 0;
                    stockTransferItem.Sgst = 0;
                    stockTransferItem.GstTotal = 0;
                }

                stockTransferItem.PurchasePrice = ps.PurchasePrice;
                stockTransferItem.SellingPrice = ps.SellingPrice;
                stockTransferItem.MRP = ps.MRP;

                stockTransferItem.PackageSize = ps.PackageSize;
                stockTransferItem.VendorId = ps.VendorId;
            }
        }
        public async Task UpdateStockTransfer(string stockTransferItemId, string productStockId, StockTransferItem stockTransferItem)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(StockTransferItemTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(StockTransferItemTable.IdColumn, stockTransferItemId);
            qb.Parameters.Add(StockTransferItemTable.ProductStockIdColumn.ColumnName, productStockId);
            qb.AddColumn(StockTransferItemTable.ProductStockIdColumn);
            if (stockTransferItem.WriteExecutionQuery)
            {
                stockTransferItem.AddExecutionQuery(qb);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qb);
            }           
        }

        //Added for offine transfer issue fix BY Bala on 16/05/17
        public async Task UpdateStockTransferToPdtId(string stockTransferItemId, string productId, string toProductStockId, string sInstanceId,  bool bWriteSyncQue =false, StockTransferItem stockTransferItem=null)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(StockTransferItemTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(StockTransferItemTable.IdColumn, stockTransferItemId);
            qb.Parameters.Add(StockTransferItemTable.ToProductStockIdColumn.ColumnName, toProductStockId);
            qb.AddColumn(StockTransferItemTable.ToProductStockIdColumn);
            qb.Parameters.Add(StockTransferItemTable.AccountIdColumn.ColumnName, user.AccountId()); //AccountId parameter added  for Offline by Poongodi on 29/05/2017
            qb.Parameters.Add(StockTransferItemTable.ToInstanceIdColumn.ColumnName, sInstanceId); //InstanceId parameter added  for Offline by Poongodi on 29/05/2017
            if (stockTransferItem != null && stockTransferItem.WriteExecutionQuery)
            {
                stockTransferItem.AddExecutionQuery(qb);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qb);
                //Manual Sync insert process for Online to offline transfer by Poongodi on 08/11/2017
                if (bWriteSyncQue)
                {
                    WriteToOnlineSyncQueue(new SyncObject() { Id = stockTransferItemId, QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "I" }, sInstanceId);
                }
            }
        }
        /// <summary>
        /// Stock Transfer Item Accepted details updated
        /// </summary>
        /// <param name="stockTransferItem"></param>
        /// <param name="sInstanceId"></param>
        /// <returns></returns>
        /// Added by Poongodi on 16/06/2017
        public async Task UpdateAcceptItem(StockTransferItem stockTransferItem, string sInstanceId, bool bWriteSyncQue =false)
        {

            var qb = QueryBuilderFactory.GetQueryBuilder(StockTransferItemTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(StockTransferItemTable.IdColumn, stockTransferItem.Id);
            qb.Parameters.Add(StockTransferItemTable.AcceptedPackageSizeColumn.ColumnName, stockTransferItem.AcceptedPackageSize);
            qb.Parameters.Add(StockTransferItemTable.AcceptedStripColumn.ColumnName, stockTransferItem.AcceptedStrip);
            qb.Parameters.Add(StockTransferItemTable.AcceptedUnitsColumn.ColumnName, stockTransferItem.AcceptedUnits);

            qb.AddColumn(StockTransferItemTable.AcceptedPackageSizeColumn, StockTransferItemTable.AcceptedStripColumn, StockTransferItemTable.AcceptedUnitsColumn);
            qb.Parameters.Add(StockTransferItemTable.AccountIdColumn.ColumnName, user.AccountId());
            qb.Parameters.Add(StockTransferItemTable.ToInstanceIdColumn.ColumnName, sInstanceId);
            if (stockTransferItem.WriteExecutionQuery)
            {
                stockTransferItem.AddExecutionQuery(qb);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qb);
                //Manual Sync insert process for Online to offline transfer by Poongodi on 09/11/2017
                if (bWriteSyncQue)
                {
                    WriteToOnlineSyncQueue(new SyncObject() { Id = stockTransferItem.Id, QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "I" }, sInstanceId);
                }
            }
        }
        /*
        public async Task<string> GetTransferNo(string instanceId)
        {
            string query = $"SELECT ISNULL(MAX(CONVERT(INT, TransferNo)),0) + 1 AS InvoiceNo FROM StockTransfer WHERE InstanceId = '{instanceId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await QueryExecuter.SingleValueAsyc(qb)).ToString();
        }
        */
        public async Task<string> GetTransferNo(string instanceId,string sPrefix)
        {
            /*Prefix added by POongodi on 11/10/2017*/
            /*   string query = $"SELECT ISNULL(MAX(CONVERT(INT, TransferNo)),0) + 1 AS InvoiceNo FROM StockTransfer WHERE InstanceId = '{instanceId}'";
               var qb = QueryBuilderFactory.GetQueryBuilder(query);
               return (await QueryExecuter.SingleValueAsyc(qb)).ToString(); */
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.Transfer, CustomDateTime.IST, instanceId, "", "", sPrefix);
            return sBillNo.ToString();
        }
        public async Task<string> GetTransferNo(string instanceId, string sPrefix, SqlConnection con, SqlTransaction transaction)
        {
            /*Prefix added by POongodi on 11/10/2017*/
            /*   string query = $"SELECT ISNULL(MAX(CONVERT(INT, TransferNo)),0) + 1 AS InvoiceNo FROM StockTransfer WHERE InstanceId = '{instanceId}'";
               var qb = QueryBuilderFactory.GetQueryBuilder(query);
               return (await QueryExecuter.SingleValueAsyc(qb)).ToString(); */
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.Transfer, CustomDateTime.IST, instanceId, "", "", sPrefix, con, transaction);
            return sBillNo.ToString();
        }
        //finyear method added by Poongodi on 26/03/2017
        public async Task<string> GetFinYear(StockTransfer data)
        {
            string sFinyr = await _HelperDataAccess.GetFinyear(CustomDateTime.IST, data.InstanceId, data.AccountId);
            return Convert.ToString(sFinyr);
        }
        public async Task<int> Count(StockTransfer data, string filterId, bool forAccept)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(StockTransferTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(data, qb, filterId, forAccept);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<StockTransfer> SaveBulkData(StockTransfer data)
        {
            List<SyncObject> syncObjectList = new List<SyncObject>();
            SyncObject syncObject = null;
            SqlConnection con = null;
            SqlTransaction transaction = null;
            List<dynamic> qbList = null;
            var index = 0;

            try
            {
                con = QueryExecuter.OpenConnection();
                if (_configHelper.AppConfig.OfflineMode)
                {
                    transaction = con.BeginTransaction();

                    //ExecuteTransactionOrder is to avoid deadlock issue
                    var list = data.GetExecutionQuery();
                    list = list.Where(x => x.Table != null).Select(x => x.Table).Distinct().ToList();
                    var tableList = "'" + string.Join("','", from item in list select item.TableName) + "'";
                    await ExecuteTransactionOrder(tableList, con, transaction);
                }

                await validateStock(data);
                if (data.SaveFailed)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    return data;
                }                

                //Get query list
                qbList = data.GetExecutionQuery();
                //Update product stock
                await _productStockDataAccess.BulkUpdateProductStock(data.TransactionStockList, con, transaction, syncObjectList);

                for (; index < qbList.Count; index++)
                {
                    QueryBuilderBase qbData = qbList[index];
                    if (qbData.Table == StockTransferTable.Table && qbData.OperationType == OperationType.Insert)
                    {
                        data.TransferNo = await GetTransferNo(data.InstanceId, data.Prefix, con, transaction);
                        await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                    }
                    else
                    {
                        await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                    }

                    if (qbData.Table == StockTransferTable.Table && qbData.OperationType == OperationType.Insert)
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters };
                        if (_configHelper.AppConfig.OfflineMode == false && data.IsToInstanceOfflineInstalled) //Not handled transaction for online transfer
                        {
                            WriteSyncQueueInsert(data, StockTransferTable.Table, "I", data.ToInstanceId, true);
                        }
                    }
                    else if (qbData.Table == StockTransferItemTable.Table && qbData.OperationType == OperationType.Insert)
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters };
                        if (_configHelper.AppConfig.OfflineMode == false && data.IsToInstanceOfflineInstalled) //Not handled transaction for online transfer
                        {
                            WriteSyncQueueInsert(data.StockTransferItems.Where(x => x.Id == qbData.Parameters[StockTransferItemTable.IdColumn.ColumnName].ToString()).First(), StockTransferItemTable.Table, "I", data.ToInstanceId, true);
                        }
                    }
                    else
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters };
                    }
                    syncObjectList.Add(syncObject);
                }

                //Write sync file
                WriteToSyncQueue(syncObjectList);

                //Commit the transaction
                if (transaction != null)
                {
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                data.SaveFailed = true;
                if (transaction != null)
                {
                    transaction.Rollback();
                }

                var unSavedData = new List<SyncObject>();
                if (qbList != null)
                {
                    for (; index < qbList.Count; index++)
                    {
                        syncObject = new SyncObject() { AccountId = user.AccountId(), InstanceId = user.InstanceId(), QueryText = qbList[index].GetQuery(), Parameters = qbList[index].Parameters };

                        unSavedData.Add(syncObject);
                    }
                }

                var errorLog = new BaseErrorLog()
                {
                    AccountId = user.AccountId(),
                    InstanceId = user.InstanceId(),
                    ErrorMessage = e.Message,
                    ErrorStackTrace = e.StackTrace,
                    UnSavedData = JsonConvert.SerializeObject(unSavedData),
                    Data = JsonConvert.SerializeObject(data),
                    CreatedBy = user.Identity.Id(),
                    UpdatedBy = user.Identity.Id()
                };
                data.ErrorLog = errorLog;
                await Insert(errorLog, ErrorLogTable.Table);

                //Write executed sync query for reverse sync
                if (!_configHelper.AppConfig.OfflineMode && syncObjectList.Count > 0)
                {
                    WriteToSyncQueue(syncObjectList);
                }
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                }
                if (con != null)
                {
                    QueryExecuter.CloseConnection(con);
                }
            }
            return data;
        }
        public async Task<bool> SaveBulkDataAcceptReject(List<StockTransferItem> stocktransferItem)
        {
            List<SyncObject> syncObjectList = new List<SyncObject>();
            SyncObject syncObject = null;
            SqlConnection con = null;
            SqlTransaction transaction = null;
            List<dynamic> qbList = null;
            var index = 0;

            try
            {
                con = QueryExecuter.OpenConnection();
                if (_configHelper.AppConfig.OfflineMode)
                {
                    transaction = con.BeginTransaction();

                    //ExecuteTransactionOrder is to avoid deadlock issue
                    var list = stocktransferItem[0].GetExecutionQuery();
                    list = list.Where(x => x.Table != null).Select(x => x.Table).Distinct().ToList();
                    var tableList = "'" + string.Join("','", from item in list select item.TableName) + "'";
                    await ExecuteTransactionOrder(tableList, con, transaction);
                }
                var status = await GetTransferStatus(stocktransferItem[0]);
                if (status != 1)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    stocktransferItem[0].SaveFailed = true;
                    stocktransferItem[0].ErrorLog = new BaseErrorLog() { ErrorMessage = "It seems, Transfer already Accepted/Rejected." };
                    return false;
                }
                
                //Get query list
                qbList = stocktransferItem[0].GetExecutionQuery();

                for (; index < qbList.Count; index++)
                {
                    QueryBuilderBase qbData = qbList[index];
                    if (qbData.ExecuteCommand == true)
                    {
                        if (qbData.Table == ProductStockTable.Table && qbData.OperationType == OperationType.Update)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();

                            qbData.Parameters.Where(x => x.Key.ToLower() != "transquantity").ToList().ForEach(x => param.Add(x.Key, x.Value));
                            var result = await sqldb.ExecuteProcedureAsync<Sales>("usp_Productstock_Update", param, con, transaction);
                        }
                        else
                        {
                            await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                        }
                    }

                    if (qbData.Table == StockTransferTable.Table && qbData.OperationType == OperationType.Update)
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters };
                        if (_configHelper.AppConfig.OfflineMode == false && stocktransferItem[0].IsFromInstanceOfflineInstalled)
                        {
                            WriteToOnlineSyncQueue(syncObject, stocktransferItem[0].InstanceId);
                        }
                    }
                    else if (qbData.Table == StockTransferItemTable.Table && qbData.OperationType == OperationType.Update)
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters };
                        if (_configHelper.AppConfig.OfflineMode == false && stocktransferItem[0].IsFromInstanceOfflineInstalled)
                        {
                            WriteToOnlineSyncQueue(syncObject, stocktransferItem[0].InstanceId);
                        }
                    }
                   else if (qbData.Table == ProductTable.Table && qbData.OperationType == OperationType.Insert)
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters, EventLevel = "A" };
                    }
                    else
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters };
                    }
                    //Add sync data to list
                    syncObjectList.Add(syncObject);
                    }
                    //Write sync file
                    WriteToSyncQueue(syncObjectList);

                    //Commit the transaction
                    if (transaction != null)
                    {
                        transaction.Commit();
                    }                
            }
            catch (Exception e)
            {
                stocktransferItem[0].SaveFailed = true;
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                var unSavedData = new List<SyncObject>();
                if (qbList != null)
                {
                    for (; index < qbList.Count; index++)
                    {
                        syncObject = new SyncObject() { AccountId = user.AccountId(), InstanceId = user.InstanceId(), QueryText = qbList[index].GetQuery(), Parameters = qbList[index].Parameters };

                        unSavedData.Add(syncObject);
                    }
                }
                var errorLog = new BaseErrorLog()
                {
                    AccountId = user.AccountId(),
                    InstanceId = user.InstanceId(),
                    ErrorMessage = e.Message,
                    ErrorStackTrace = e.StackTrace,
                    UnSavedData = JsonConvert.SerializeObject(unSavedData),
                    Data = JsonConvert.SerializeObject(stocktransferItem[0]),
                    CreatedBy = user.Identity.Id(),
                    UpdatedBy = user.Identity.Id()
                };
                stocktransferItem[0].ErrorLog = errorLog;
                await Insert(errorLog, ErrorLogTable.Table);

                //Write executed sync query for reverse sync
                if (!_configHelper.AppConfig.OfflineMode && syncObjectList.Count > 0)
                {
                    WriteToSyncQueue(syncObjectList);
                }
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                }
                if (con != null)
                {
                    QueryExecuter.CloseConnection(con);
                }
            }
            return true;
        }
            
        public async Task<StockTransfer> validateStock(StockTransfer data)
        {
            var result = data.StockTransferItems.GroupBy(x => x.ProductStockId).Select(y => new ProductStock
            {
                Id = y.First().ProductStockId,
                transQuantity = y.Sum(c => c.Quantity),
            }).ToList();

            string StockIdsWithReqQty = string.Join(",", from item in result select item.Id + "~" + item.transQuantity);

            var validateStock = await _productStockDataAccess.ValidateStock(data.AccountId, data.InstanceId, StockIdsWithReqQty);
            if (validateStock.Count() > 0)
            {
                data.SaveFailed = true;
                data.ErrorLog = new BaseErrorLog() { ErrorMessage = "Stock Validation Failed" };
                data.validateStockList = validateStock;
                return data;
            }
            else
            {
                data.TransactionStockList = result;
                data.SaveFailed = false;
            }
            return data;
        }
        private async Task<int> GetTransferStatus(StockTransferItem stockTransferItem)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(StockTransferTable.Table, OperationType.Select);
            qb.AddColumn(StockTransferTable.TransferStatusColumn);
            qb.ConditionBuilder.And(StockTransferTable.AccountIdColumn, user.AccountId());
            qb.ConditionBuilder.And(StockTransferTable.IdColumn, stockTransferItem.TransferId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<IEnumerable<StockTransfer>> List(StockTransfer data, string filterId, bool forAccept)
        {
            //Left join Added for user table by Poongodi on 24/05/2017
            //Prefix col added by Poongodi on 15/06/2017
            //TransferItemCount addedby Settu on 01/09/2017
            var queryresult = $@"select StockTransfer.Id,
                                  StockTransfer.AccountId,
                                  StockTransfer.InstanceId,
                                  StockTransfer.FromInstanceId,
                                  StockTransfer.ToInstanceId,
                                 isnull( StockTransfer.Prefix,'') + StockTransfer.TransferNo [TransferNo],
                                  StockTransfer.TransferDate,
                                  StockTransfer.FileName,
                                  StockTransfer.Comments,
                                  isnull(StockTransfer.TransferStatus,0) as TransferStatus,
                                  StockTransfer.OfflineStatus,
                                  StockTransfer.CreatedAt,
                                  StockTransfer.UpdatedAt,
                                  StockTransfer.CreatedBy,
                                  StockTransfer.UpdatedBy,
                                  isnull(StockTransfer.TransferItemCount,0) TransferItemCount,
                                  isnull(StockTransfer.TaxRefNo,0) as TaxRefNo,
                                  Instance.Id AS [Instance.Id],
                                  Instance.AccountId AS [Instance.AccountId],
                                  Instance.Name AS [InstanceName],
Instance.Area AS [InstanceArea],
                                  Instance.Mobile AS [InstanceMobile],
                                  ToInstance.AccountId AS [ToInstance.AccountId],
                                  ToInstance.Name AS [ToInstanceName],
 ToInstance.Area AS [ToInstanceArea],
                                  HQueUser.Id AS [HQueUser.Id],
                                  HQueUser.AccountId AS [HQueUser.AccountId],
                                  HQueUser.InstanceId AS [HQueUser.InstanceId],
                                  HQueUser.UserId AS [HQueUser.UserId],
                                  isnull(HQueUser.Name ,TransferBy)AS [HQueUserName]
                                    FROM StockTransfer 
                                    INNER JOIN Instance ON Instance.Id = StockTransfer.FromInstanceId 
                                    INNER JOIN Instance AS ToInstance ON ToInstance.Id = StockTransfer.ToInstanceId 
                                    Left JOIN HQueUser ON HQueUser.Id = StockTransfer.CreatedBy 
                             WHERE ";
            if (!string.IsNullOrEmpty(data.TransferNo))
            {
                //Prefix filter added by Poongodi on 23/06/2017
                //exact filter condition added by nandhini on 14.12.17
                queryresult += "(isnull(StockTransfer.Prefix,'')+ StockTransfer.TransferNo = '" + data.TransferNo +"'  or  StockTransfer.TransferNo = '" + data.TransferNo + "') AND ";
            }
            if (data.TransferDate.HasValue && data.TransferToDate.HasValue)
            {
                //queryresult += " cast(StockTransfer.TransferDate as date) = '" + data.TransferDate.Value.ToString("yyyy-MM-dd") + "' AND ";
                queryresult += " cast(StockTransfer.TransferDate as date) between '" + data.TransferDate.Value.ToString("yyyy-MM-dd") + "' AND '" + data.TransferToDate.Value.ToString("yyyy-MM-dd") + "' AND ";  // between condition added for transfer list from and to date "from and to date"
            }
            if (data.TransferStatus.HasValue)
            {
                queryresult += " StockTransfer.TransferStatus = '" + data.TransferStatus + "' AND ";
            }
            if (forAccept)
            {
                if (!string.IsNullOrEmpty(data.ToInstanceId))
                {
                    queryresult += " StockTransfer.FromInstanceId = '" + data.ToInstanceId + "' AND ";
                }
                queryresult += " StockTransfer.ToInstanceId = '" + filterId + "'";
            }
            else
            {
                if (!string.IsNullOrEmpty(data.ToInstanceId))
                {
                    queryresult += "StockTransfer.FromInstanceId = '" + data.ToInstanceId + "' AND";
                }
                queryresult += " (StockTransfer.ToInstanceId = '" + filterId + "' Or StockTransfer.FromInstanceId = '" + filterId + "')";
            }
            queryresult += " ORDER BY StockTransfer.CreatedAt desc offset " + ((data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize) + " rows FETCH next " + data.Page.PageSize + " rows only"; // Pagination with 30 records
            var list = new List<StockTransfer>();
            var qb = QueryBuilderFactory.GetQueryBuilder(queryresult);
            using (var x = await QueryExecuter.QueryAsyc(qb))
            {
                while (x.Read())
                {
                    var stockTransferlist = new StockTransfer
                    {
                        Instance =
                    {
                        Name = x["InstanceName"].ToString(),
                        Mobile = x["InstanceMobile"].ToString(),
                        Area=x["InstanceArea"].ToString(),
                    },
                        ToInstance =
                    {
                        Name = x["ToInstanceName"].ToString(),
                         Area=x["ToInstanceArea"].ToString(),
                    },
                        HQueUser =
                    {
                       Name  = x["HQueUserName"].ToString(),
                    },
                        AccountId = x[StockTransferTable.AccountIdColumn.ColumnName].ToString(),
                        Id = x[StockTransferTable.IdColumn.ColumnName].ToString(),
                        InstanceId = x[StockTransferTable.InstanceIdColumn.ColumnName].ToString(),
                        ToInstanceId = x[StockTransferTable.ToInstanceIdColumn.ColumnName].ToString(), //Accountid removed and ToInstanceId assigned by Poongodi on 29/05/2017
                        TransferDate = x[StockTransferTable.TransferDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        CreatedAt = x[StockTransferTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        TransferStatus = Convert.ToInt32(x[StockTransferTable.TransferStatusColumn.ColumnName]),
                        Comments = x[StockTransferTable.CommentsColumn.ColumnName].ToString(),
                        FromInstanceId = x[StockTransferTable.FromInstanceIdColumn.ColumnName].ToString(),
                        TransferNo = x[StockTransferTable.TransferNoColumn.ColumnName].ToString(),
                        UpdatedAt = x[StockTransferTable.UpdatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        UpdatedBy = x[StockTransferTable.UpdatedByColumn.ColumnName].ToString(),
                        TaxRefNo = Convert.ToInt32(x[StockTransferTable.TaxRefNoColumn.ColumnName]) as int? ?? 0,
                        TransferItemCount = x[StockTransferTable.TransferItemCountColumn.ColumnName] as int? ?? 0,
                    };
                    list.Add(stockTransferlist);
                }
                var result = list;
                //var si = await GetStockTransferItem(result.Select(x => x.Id));
                var si = await EditStockTransferItem(result);
                var stockTransfer = result.Select(c =>
                {
                    c.StockTransferItems = si.Where(y => y.TransferId == c.Id).ToList();
                    return c;
                });
                return stockTransfer;
            }
        }
        public async Task<IEnumerable<Instance>> GetFilterInstance(string filterId, bool forAccept)
        {
            var where = string.Empty;
            var join = string.Empty;
            if (forAccept)
            {
                join = " i.Id = st.FromInstanceId ";
                where = $" st.ToInstanceId = '{filterId}' and st.TransferStatus = 1";
            }
            else
            {
                join = " (i.Id = st.FromInstanceId or i.Id = st.ToInstanceId) ";
                where = $" st.ToInstanceId = '{filterId}' OR st.FromInstanceId = '{filterId}'";
            }
            var query = $"select distinct i.Id, i.Name, i.Area from instance as i inner join StockTransfer as st on {join} where {where}";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return await List<Instance>(qb);
        }
        public async Task<IEnumerable<StockTransferItem>> EditStockTransferItem(IEnumerable<dynamic> result)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            if (result.GetType() == typeof(string[]))
            {
                var chkValue = ((string[])result)[0];
                parms.Add("TransferID", chkValue);
            }
            else
            {
                var whereCondition = string.Join(",", result.Select(x => x.Id).ToList());
                parms.Add("TransferID", whereCondition);
            }
            /*InstanceId parameter added by Poongodi on 16/06/2017*/
            parms.Add("InstanceId", user.InstanceId());
            //ProductId, Instanceid and VendorId added by Bala on 16/05/17
            var resultPurchaseId = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetTransferStockList", parms);
            var purchaseItems = resultPurchaseId.Select(x =>
            {
                var stockTransferItem = new StockTransferItem
                {
                    VendorOrderItemId = x.VendorOrderItemId as System.String ?? "",
                    AccountId = x.AccountId as System.String ?? "",
                    Id = x.Id as System.String ?? "",
                    ProductStockId = x.ProductStockId as System.String ?? "",
                    TransferId = x.TransferId as System.String ?? "",
                    ToInstanceId = x.ToInstanceId as System.String ?? "",
                    InstanceId = x.InstanceId as System.String ?? "",
                    Quantity = x.Quantity as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    ProductId = x.ProductId as System.String ?? "",
                    VendorId = x.ProductStockVendorId as System.String ?? "",
                    PackageSize = x.StockTransferPackageSize as decimal? ?? 0,
                    Quantity1 = x.Quantity1 as decimal? ?? 0,
                    PurchasePrice = x.ProductStockPurchasePrice,
                    SellingPrice = x.ProductStockSellingPrice,
                    MRP = x.ProductStockMRP,
                    NoofStrip = x.Noofstrips,

                    ProductStock =
                    {
                        SellingPrice =x.ProductStockSellingPrice as decimal? ?? 0,
                        //MRP Implementation by Settu
                        MRP =x.ProductStockMRP as decimal? ?? 0,
                        VAT =x.ProductStockVAT as decimal? ?? 0,
                        CST= x.ProductStockCST as decimal? ?? 0,
                        GstTotal=x.ProductStockGstTotal as decimal? ?? 0,
                        BatchNo= x.ProductStockBatchNo as System.String ?? "",
                        ExpireDate =x.ProductStockExpireDate as DateTime? ?? DateTime.MinValue,
                        TaxType=x.ProductStockTaxType as System.String ?? "",
                        PackagePurchasePrice = x.ProductStockPackagePurchasePrice  as decimal? ?? 0,
                        PurchasePrice = x.ProductStockPurchasePrice  as decimal? ?? 0,
                        VendorId = x.ProductStockVendorId as System.String ?? "",
                        Stock = x.Quantity as decimal? ?? 0,
                        PackageSize = x.ProductStockPackageSize as decimal? ?? 0,
                        Product =
                        {
                            Name=x.ProductName as System.String ?? "",
                            Id = x.ProductId as System.String ?? "",
                        },
                    }


                };
                return stockTransferItem;
            });
            return purchaseItems;
        }
        public async Task<IEnumerable<StockTransferItem>> GetStockTransferItem(IEnumerable<string> result)
        {
            var siqb = QueryBuilderFactory.GetQueryBuilder(StockTransferItemTable.Table, OperationType.Select);
            siqb.AddColumn(StockTransferItemTable.Table.ColumnList.ToArray());
            //siqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn,StockTransferItemTable.ProductStockIdColumn);
            //siqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            //siqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            //siqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            siqb.JoinBuilder.LeftJoin(ProductStockTable.Table, ProductStockTable.IdColumn, StockTransferItemTable.ProductStockIdColumn);
            siqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            siqb.JoinBuilder.LeftJoin(VendorOrderItemTable.Table, VendorOrderItemTable.StockTransferIdColumn, StockTransferItemTable.IdColumn);
            siqb.JoinBuilder.AddJoinColumn(VendorOrderItemTable.Table, VendorOrderItemTable.Table.ColumnList.ToArray());
            siqb.JoinBuilder.ORJoin(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn, ProductTable.IdColumn, VendorOrderItemTable.ProductIdColumn);
            siqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            var i = 1;
            foreach (var stockTransfer in result)
            {
                var paramName = $"s{i++}";
                var caol = new CriteriaColumn(StockTransferItemTable.TransferIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(caol);
                siqb.ConditionBuilder.Or(cc);
                siqb.Parameters.Add(paramName, stockTransfer);
            }
            var list = new List<StockTransferItem>();
            using (var reader = await QueryExecuter.QueryAsyc(siqb))
            {
                while (reader.Read())
                {
                    var stockTransferItem = new StockTransferItem();
                    stockTransferItem.Fill(reader)
                        .ProductStock.Fill(reader)
                        .Product.Fill(reader);
                    list.Add(stockTransferItem);
                }
            }
            return list;
        }
        private QueryBuilderBase SqlQueryBuilder(StockTransfer data, QueryBuilderBase qb, string filterId, bool forAccept)
        {
            if (data.TransferStatus.HasValue)
            {
                qb.ConditionBuilder.And(StockTransferTable.TransferStatusColumn, data.TransferStatus.Value);
            }
            if (data.TransferDate.HasValue && data.TransferToDate.HasValue)
            {
                qb.ConditionBuilder.And(StockTransferTable.TransferDateColumn, CriteriaEquation.Between);  // Added for Transfer and accept list filtered by "from and to date"
                qb.Parameters.Add("FilterFromDate", data.TransferDate.Value);
                qb.Parameters.Add("FilterToDate", data.TransferToDate.Value); //qb.ConditionBuilder.And(StockTransferTable.TransferDateColumn, data.TransferDate.Value);                
            }
            if (!string.IsNullOrEmpty(data.TransferNo))
            {
                qb.ConditionBuilder.And(StockTransferTable.TransferNoColumn, data.TransferNo);
            }
            if (forAccept)
            {
                if (!string.IsNullOrEmpty(data.ToInstanceId))
                {
                    qb.ConditionBuilder.And(StockTransferTable.FromInstanceIdColumn, data.ToInstanceId);
                }
                qb.ConditionBuilder.And(StockTransferTable.ToInstanceIdColumn, filterId);
            }
            else
            {
                if (!string.IsNullOrEmpty(data.ToInstanceId))
                {
                    var cc = new CustomCriteria(StockTransferTable.ToInstanceIdColumn, StockTransferTable.FromInstanceIdColumn, CriteriaCondition.Or);
                    qb.ConditionBuilder.And(cc);
                    qb.Parameters.Add(StockTransferTable.ToInstanceIdColumn.ColumnName, ""); //data.ToInstanceId // count mismatch for pagination issue fixed.
                    qb.Parameters.Add(StockTransferTable.FromInstanceIdColumn.ColumnName, data.ToInstanceId);
                }
                var filterCc = new CustomCriteria(StockTransferTable.ToInstanceIdColumn, StockTransferTable.FromInstanceIdColumn, "@Filter", CriteriaCondition.Or);
                qb.ConditionBuilder.And(filterCc);
                qb.Parameters.Add("Filter", filterId);
            }
            return qb;
        }
        //To InstanceId parameter(for offline sync) added for by Poongodi on 23/05/2017
        public async Task UpdateStatus(string id, int status, string userId, StockTransferItem stockTransferItem, bool bWriteSyncQue = false)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(StockTransferTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(StockTransferTable.IdColumn, id);
            qb.AddColumn(StockTransferTable.TransferStatusColumn, StockTransferTable.UpdatedAtColumn, StockTransferTable.UpdatedByColumn);
            qb.Parameters.Add(StockTransferTable.TransferStatusColumn.ColumnName, status);
            qb.Parameters.Add(StockTransferTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
            qb.Parameters.Add(StockTransferTable.UpdatedByColumn.ColumnName, userId);
            qb.Parameters.Add(StockTransferTable.ToInstanceIdColumn.ColumnName, stockTransferItem.InstanceId); //Parameter added by Poongodi on 23/05/2017
            qb.Parameters.Add(StockTransferTable.AccountIdColumn.ColumnName, user.AccountId()); // For offline Parameter added by Poongodi on 27/05/2017
            if (stockTransferItem.WriteExecutionQuery)
            {
                stockTransferItem.AddExecutionQuery(qb);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qb);
                //Manual Sync insert process for Online to offline transfer by Poongodi on 08/11/2017
                if (bWriteSyncQue)
                {
                    WriteToOnlineSyncQueue(new SyncObject() { Id = id, QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "I" }, stockTransferItem.InstanceId);
                }
            }
        }

        public async Task<TransferSettings> GetTransferSetting(TransferSettings ts)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TransferSettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(TransferSettingsTable.AccountIdColumn, ts.AccountId);
            qb.ConditionBuilder.And(TransferSettingsTable.InstanceIdColumn, ts.InstanceId);
            qb.AddColumn(TransferSettingsTable.Table.ColumnList.ToArray());
            var result = await List<TransferSettings>(qb);

            if (result.Count() > 0)
            {
                if (result[0].ProductBatchDisplayType == null)
                    result[0].ProductBatchDisplayType = "";

                if (result[0].TransferQtyType == null)
                    result[0].TransferQtyType = true;

                return result[0];
            }
            else
            {
                TransferSettings transferSettings = new TransferSettings()
                {
                    TransferQtyType = true,
                    ProductBatchDisplayType = "",
                };
                return transferSettings;
            }
        }
        public async Task<TransferSettings> UpdateTransferSetting(TransferSettings ts)
        {
            var count = await CountTransferSetting(ts);
            if (count > 0)
            {
                await UpdateBatchDisplay(ts);
                await UpdateTransferType(ts);
                return ts;
            }
            else
            {
                return await Insert(ts, TransferSettingsTable.Table);
            }

        }


        public async Task UpdateBatchDisplay(TransferSettings ts)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TransferSettingsTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(TransferSettingsTable.AccountIdColumn, ts.AccountId);
            qb.ConditionBuilder.And(TransferSettingsTable.InstanceIdColumn, ts.InstanceId);
            qb.AddColumn(TransferSettingsTable.ProductBatchDisplayTypeColumn, TransferSettingsTable.UpdatedAtColumn, TransferSettingsTable.UpdatedByColumn, TransferSettingsTable.ShowExpiredQtyColumn);
            qb.Parameters.Add(TransferSettingsTable.ProductBatchDisplayTypeColumn.ColumnName, ts.ProductBatchDisplayType);
            qb.Parameters.Add(TransferSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(TransferSettingsTable.UpdatedByColumn.ColumnName, ts.UpdatedBy);
            qb.Parameters.Add(TransferSettingsTable.ShowExpiredQtyColumn.ColumnName, ts.ShowExpiredQty);

            await QueryExecuter.NonQueryAsyc(qb);
        }

        public async Task UpdateTransferType(TransferSettings ts)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TransferSettingsTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(TransferSettingsTable.AccountIdColumn, ts.AccountId);
            qb.AddColumn(TransferSettingsTable.TransferQtyTypeColumn, TransferSettingsTable.UpdatedAtColumn, TransferSettingsTable.UpdatedByColumn);
            qb.Parameters.Add(TransferSettingsTable.TransferQtyTypeColumn.ColumnName, ts.TransferQtyType); // ProductTransferQtyType);
            qb.Parameters.Add(TransferSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(TransferSettingsTable.UpdatedByColumn.ColumnName, ts.UpdatedBy);
            await QueryExecuter.NonQueryAsyc(qb);
        }
        private async Task<int> CountTransferSetting(TransferSettings ts)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TransferSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(TransferSettingsTable.AccountIdColumn, ts.AccountId);
            qb.ConditionBuilder.And(TransferSettingsTable.InstanceIdColumn, ts.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        /// <summary>
        /// Method added to Reverse the Rejected stock 
        /// </summary>
        /// <param name="stockTransferItems"></param>
        /// <returns></returns>
        /// //Method added by Poongodi on 24/05/2017
        public int RejectProductStockUpdate(IEnumerable<StockTransferItem> stockTransferItems, string FromInstanceId)
        {
            foreach (var transferItem in stockTransferItems)
            {
                var user = _httpContextAccessor.HttpContext.User;
                transferItem.SetLoggedUserDetails(user);
                Dictionary<string, object> parms = new Dictionary<string, object>();
                parms.Add("Id", transferItem.ProductStockId);
                parms.Add("Qty", transferItem.Quantity);
                parms.Add("AccountId", transferItem.AccountId);
                parms.Add("UpdatedAt", CustomDateTime.IST);
                parms.Add("UpdatedBy", transferItem.UpdatedBy);
                parms.Add("ToInstanceId", FromInstanceId); //Parameter added  for Offline by Poongodi on 29/05/2017

                string sQryTxt = "UPDATE ProductStock SET Stock = Stock+ @Qty , UpdatedAt = @UpdatedAt, UpdatedBy =@UpdatedBy where Id =@Id";
                
                var qb = QueryBuilderFactory.GetQueryBuilder(sQryTxt);
                parms.ToList().ForEach(x => qb.Parameters.Add(x));
                if (transferItem.WriteExecutionQuery)
                {
                    qb.SetExecuteCommand(false);
                    transferItem.AddExecutionQuery(qb);
                }
                else
                {                
                    WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = qb.Parameters, EventLevel = "I" });
                }



            }
            return 0;
        }

        //By San 
        public async Task<StockTransfer> TransferById(string id, string instanceId)
        {
            TransferSettings ts = new TransferSettings();
            ts.AccountId = user.AccountId();
            ts.InstanceId = instanceId;
            var getTranserStatus = await GetTransferSetting(ts);
            /*GST Rleated fields  Added by Poongodi on 03/07/2017*/
            var queryresult = $@"Select StockTransfer.Id,
                                  StockTransfer.AccountId,
                                  StockTransfer.InstanceId,
                                  StockTransfer.FromInstanceId,
                                  StockTransfer.ToInstanceId,
                                  Isnull( StockTransfer.Prefix,'') + StockTransfer.TransferNo [TransferNo],
                                  StockTransfer.TransferDate,
                                  StockTransfer.FileName,
                                  StockTransfer.Comments,
                                  Isnull(StockTransfer.TransferStatus,0) as TransferStatus,
                                  StockTransfer.OfflineStatus,
                                  StockTransfer.CreatedAt,
                                  StockTransfer.UpdatedAt,
                                  StockTransfer.CreatedBy,
                                  StockTransfer.UpdatedBy,
                                  isnull(StockTransfer.TaxRefNo,0) [TaxRefNo],
								  StockTransferItem.TransferId,								 
								  Isnull(Isnull(StockTransferItem.BatchNo,ProductStock.BatchNo),0) as [BatchNo],
								  Isnull(Isnull(StockTransferItem.ExpireDate,ProductStock.ExpireDate),null) as ExpireDate,
								  Isnull(Isnull(StockTransferItem.MRP,ProductStock.MRP),0) as [MRP],
								  Isnull(Isnull(StockTransferItem.SellingPrice,ProductStock.SellingPrice),0) as [SellingPrice],
								  Isnull(Isnull(StockTransferItem.PurchasePrice,ProductStock.PurchasePrice),0) as [PurchasePrice],
								  Isnull(Isnull(StockTransferItem.VAT,ProductStock.VAT),0) as [VAT],
								  Isnull(Isnull(StockTransferItem.PackageSize,ProductStock.PackageSize),1) as [PackageSize],
								  StockTransferItem.isStripTransfer,
                                  StockTransferItem.Quantity,
                                  Isnull(Isnull(StockTransferItem.gsttotal,ProductStock.GstTotal),0) as [GstTotal],
                                  Isnull(Isnull(StockTransferItem.Cgst,ProductStock.Cgst),0) as [Cgst],
                                  Isnull(Isnull(StockTransferItem.Sgst,ProductStock.Sgst),0) as [Sgst],
                                  Isnull(Isnull(StockTransferItem.Igst,ProductStock.Igst),0) as [Igst],
                                  isnull( Product.Name, isnull(P1.Name,'')) [Name] ,
                                  Instance.Id AS [Instance.Id],
                                  Instance.AccountId AS [Instance.AccountId],
                                  Instance.Name AS [InstanceName],
                                  Instance.Mobile,
								  Instance.DrugLicenseNo,
								  Instance.TinNo,
								  Instance.Phone,
								  Instance.City,
								  Instance.State,
								  Instance.Pincode,
                                  Isnull(Instance.GsTinNo,'') [GsTinNo],
                                  ToInstance.AccountId AS [ToInstance.AccountId],
                                  ToInstance.Name AS [ToInstanceName],
                                  ToInstance.Name AS [ToInstanceName],
								  ToInstance.Mobile AS [ToInstanceMobile],
								  ToInstance.Address AS [ToInstanceAddress],
                                  ToInstance.Area AS [ToInstanceArea],
								  ToInstance.DrugLicenseNo AS [ToInstanceDrugLicenseNo],
                                  ToInstance.TinNo AS [ToInstanceTinNo],
                                  Isnull(ToInstance.GsTinNo,'') [ToInstanceGsTinNo],
                                  isnulL(ToInstance.isUnionTerritory,0) [isUnionTerritory],
                                  HQueUser.Id AS [HQueUser.Id],
                                  HQueUser.AccountId AS [HQueUser.AccountId],
                                  HQueUser.InstanceId AS [HQueUser.InstanceId],
                                  HQueUser.UserId AS [HQueUser.UserId],
                                  Isnull(HQueUser.Name ,TransferBy)AS [HQueUserName]
                                    FROM StockTransfer 
									Inner Join StockTransferItem ON StockTransferItem.TransferId=StockTransfer.Id
                                    LEFT JOIN Product [P1] ON P1.Id=StockTransferItem.ProductId
                                    INNER JOIN Instance ON Instance.Id = StockTransfer.FromInstanceId 
                                    INNER JOIN Instance AS ToInstance ON ToInstance.Id = StockTransfer.ToInstanceId
                                    LEFT JOIN ProductStock ON ProductStock.Id=StockTransferItem.ProductStockId
									LEFT JOIN Product ON Product.Id=ProductStock.ProductId
                                    Left JOIN HQueUser ON HQueUser.Id = StockTransfer.CreatedBy
                             WHERE StockTransfer.Id='" + id + "'";
            //WHERE StockTransfer.Id='" + id + "' And StockTransfer.InstanceId='" + instanceId+"'";

            var list = new List<StockTransfer>();
            var qb = QueryBuilderFactory.GetQueryBuilder(queryresult);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var stockTransferlist = new StockTransfer
                    {
                        Instance =
                        {
                            Name = reader["InstanceName"].ToString(),
                            Mobile = reader["Mobile"].ToString(),
                            Phone = reader["Phone"].ToString(),
                            DrugLicenseNo = reader["DrugLicenseNo"].ToString(),
                            TinNo = reader["TinNo"].ToString(),
                            State = reader["State"].ToString(),
                            City = reader["City"].ToString(),
                            Pincode = reader["Pincode"].ToString(),
                            isUnionTerritory = reader ["isUnionTerritory"] as bool? ?? false,
                            GsTinNo = reader["GsTinNo"].ToString(),
                        },
                        ToInstance =
                        {
                            Name = reader["ToInstanceName"].ToString(),
                            Mobile = reader["ToInstanceMobile"].ToString(),
                            DrugLicenseNo = reader["ToInstanceDrugLicenseNo"].ToString(),
                            Address = reader["ToInstanceAddress"].ToString(),
                            Area = reader["ToInstanceArea"].ToString(),
                            TinNo = reader["ToInstanceTinNo"].ToString(),
                            GsTinNo = reader["ToInstanceGsTinNo"].ToString(),
                        },
                        HQueUser =
                        {
                           Name  = reader["HQueUserName"].ToString(),
                        },
                        AccountId = reader[StockTransferTable.AccountIdColumn.ColumnName].ToString(),
                        Id = reader[StockTransferTable.IdColumn.ColumnName].ToString(),
                        InstanceId = reader[StockTransferTable.InstanceIdColumn.ColumnName].ToString(),
                        ToInstanceId = reader[StockTransferTable.ToInstanceIdColumn.ColumnName].ToString(),
                        TransferDate = reader[StockTransferTable.TransferDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        CreatedAt = reader[StockTransferTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        TransferStatus = Convert.ToInt32(reader[StockTransferTable.TransferStatusColumn.ColumnName]),
                        Comments = reader[StockTransferTable.CommentsColumn.ColumnName].ToString(),
                        FromInstanceId = reader[StockTransferTable.FromInstanceIdColumn.ColumnName].ToString(),
                        TransferNo = reader[StockTransferTable.TransferNoColumn.ColumnName].ToString(),
                        UpdatedAt = reader[StockTransferTable.UpdatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        UpdatedBy = reader[StockTransferTable.UpdatedByColumn.ColumnName].ToString(),
                        TaxRefNo = Convert.ToInt32(reader[StockTransferTable.TaxRefNoColumn.ColumnName].ToString()),

                    };
                    var stockTransferItem = new StockTransferItem
                    {
                        TransferId = reader[StockTransferItemTable.TransferIdColumn.ColumnName].ToString(),
                        BatchNo = reader[StockTransferItemTable.BatchNoColumn.ColumnName].ToString(),
                        ExpireDate = reader[StockTransferItemTable.ExpireDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        MRP = reader[StockTransferItemTable.MRPColumn.ColumnName] as decimal? ?? 0,
                        SellingPrice = reader[StockTransferItemTable.SellingPriceColumn.ColumnName] as decimal? ?? 0,
                        PurchasePrice = reader[StockTransferItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageSize = reader[StockTransferItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        Quantity = reader[StockTransferItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        VAT = reader[StockTransferItemTable.VATColumn.ColumnName] as decimal? ?? 0,
                        GstTotal = reader[StockTransferItemTable.GstTotalColumn.ColumnName] as decimal? ?? 0,
                        Cgst = reader[StockTransferItemTable.CgstColumn.ColumnName] as decimal? ?? 0,
                        Sgst = reader[StockTransferItemTable.SgstColumn.ColumnName] as decimal? ?? 0,
                        Igst = reader[StockTransferItemTable.IgstColumn.ColumnName] as decimal? ?? 0,

                    };
                    stockTransferItem.ProductStock.productName = reader[ProductTable.NameColumn.ColumnName].ToString();
                    stockTransferlist.StockTransferItems.Add(stockTransferItem);
                    list.Add(stockTransferlist);
                }

            }
            var stockTransfer = list.FirstOrDefault();

            if (stockTransfer == null)
                return new StockTransfer();
            stockTransfer.StockTransferItems = list.SelectMany(x => x.StockTransferItems).ToList();
            stockTransfer.IsTransferSettings = getTranserStatus.TransferQtyType;
            return stockTransfer;
        }
        //Added by Settu for indent transfer with stock reducing
        public async Task<List<VendorOrder>> GetActiveIndentList(string OrderId, int Type, DateTime? fromDate, DateTime? toDate)
        {
            /*
             Type:
             1-Indent
             2-Purchase
             */
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", user.AccountId());
            parms.Add("InstanceId", user.InstanceId());
            parms.Add("OrderId", OrderId);
            parms.Add("Type", Type);
            parms.Add("FromDate", fromDate);
            parms.Add("ToDate", toDate);            
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Get_ActiveIndentList", parms);      

            var orderList = result.GroupBy(y => y.Id).Select(z => z.First()).Select(x =>
            {
                var order = new VendorOrder
                {
                    Id = x.Id,
                    OrderId = x.OrderId,
                    Instance =
                    {
                        Id = x.InstanceId,
                        Name = x.InstanceName,
                    },
                    OrderDate = x.OrderDate,
                    Vendor =
                    {
                        Id = x.VendorId,
                        Name = x.VendorName,
                    },
                };
                return order;
            }).ToList();

            var orderItemList = result.Select(x =>
            {
                var orderItem = new VendorOrderItem
                {
                    Id = x.VendorOrderItemId,
                    VendorOrderId = x.Id,
                    ProductStock =
                    {
                        Product = {
                            Id = x.ProductId,
                            Name = x.ProductName,
                            Schedule = x.ProductSchedule,
                            AccountId = user.AccountId(),
                            InstanceId = user.InstanceId(),
                        },
                        Stock = x.Stock,
                        ProductId = x.ProductId,
                        Id = x.ProductStockId,
                    },
                    PackageSize = x.PackageSize,
                    PackageQty = x.PackageQty,
                    Quantity = x.Quantity,
                    ReceivedQty = x.ReceivedQty,
                    TransferQtyType = x.TransferQtyType,
                    VendorPurchaseId = x.VendorPurchaseId,
                    StockTransferId = x.StockTransferId,
                    VendorPurchaseItemId = x.VendorPurchaseItemId,
                    OrderItemSno = x.OrderItemSno,
                    FreeQty = x.FreeQty
                };
                return orderItem;
            }).OrderBy(x => x.OrderItemSno).ToList();

            if (OrderId != null && OrderId != "")
            {
                foreach (var item in orderItemList)
                {
                    if (item.ProductStockList == null)
                    {
                        var pslist = await _productStockDataAccess.GetInStockItem(item.ProductStock.Product, 0,true, item.ProductStock.Id);
                        if (pslist.Count > 0)
                        {
                            if (Type == 2)
                            {
                                item.ProductStockList = pslist;
                            }
                            else
                            {
                                foreach (var ps in orderItemList)
                                {
                                    if (item.ProductStock.Product.Id == ps.ProductStock.Product.Id && ps.ProductStockList == null)
                                    {
                                        ps.ProductStockList = pslist;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            var list = orderList.Select(x =>
            {
                x.VendorOrderItem = orderItemList.Where(y => y.VendorOrderId == x.Id).Select(z => z).ToList();
                return x;
            }).ToList();
            return list;
        }

        public async Task<List<ProductStock>> getAcceptedTransferList(string TransferNo)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", user.AccountId());
            parms.Add("InstanceId", user.InstanceId());
            parms.Add("TransferNo", TransferNo);

            var list = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetAcceptedTransferList", parms);
            var result = list.Select(x =>
            {
                var productStock = new ProductStock
                {
                    Id = x.Id as System.String ?? "",
                    SellingPrice = x.SellingPrice as decimal? ?? 0,
                    MRP = x.MRP as decimal? ?? 0,
                    BatchNo = x.BatchNo as System.String ?? "",
                    ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                    Stock = x.Stock as decimal? ?? 0,
                    Product =
                        {
                            Code=x.ProductCode as System.String ?? "",
                            Name=x.ProductName as System.String ?? "",
                            Id = x.ProductId as System.String ?? "",
                        },
                    ProductId = x.ProductId as System.String ?? "",
                    PurchaseBarcode = x.PurchaseBarcode as System.String ?? "",
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.Now,
                };
                return productStock;
            });
            return result.ToList();
        }

        public async Task<List<StockTransferItem>> GetStockTransferItem(string id, string instanceId)
        {
            TransferSettings ts = new TransferSettings();
            ts.AccountId = user.AccountId();
            ts.InstanceId = instanceId;
            var getTranserStatus = await GetTransferSetting(ts);
            /*GST Rleated fields  Added by Poongodi on 03/07/2017*/
            var queryresult = $@"Select StockTransfer.Id,
                                  StockTransfer.AccountId,
                                  StockTransfer.InstanceId,
                                  StockTransfer.FromInstanceId,
                                  StockTransfer.ToInstanceId,
                                  Isnull( StockTransfer.Prefix,'') + StockTransfer.TransferNo [TransferNo],
                                  StockTransfer.TransferDate,
                                  StockTransfer.FileName,
                                  StockTransfer.Comments,
                                  Isnull(StockTransfer.TransferStatus,0) as TransferStatus,
                                  StockTransfer.OfflineStatus,
                                  StockTransfer.CreatedAt,
                                  StockTransfer.UpdatedAt,
                                  StockTransfer.CreatedBy,
                                  StockTransfer.UpdatedBy,
                                  isnull(StockTransfer.TaxRefNo,0) [TaxRefNo],
								  StockTransferItem.TransferId,								 
								  StockTransferItem.BatchNo,
								  StockTransferItem.ExpireDate,
								  StockTransferItem.MRP,
						     	StockTransferItem.SellingPrice,
								StockTransferItem.PurchasePrice,
								StockTransferItem.VAT,
								StockTransferItem.PackageSize,
								  StockTransferItem.isStripTransfer,
                                  StockTransferItem.Quantity,
                                  StockTransferItem.gsttotal as [GstTotal],
                                  StockTransferItem.Cgst,
                                  StockTransferItem.Sgst,
                                 StockTransferItem.Igst,
                                  P1.Name ,
                                    P1.Id as ProductId                               
                                    FROM StockTransfer 
									Inner Join StockTransferItem ON StockTransferItem.TransferId=StockTransfer.Id
                                    LEFT JOIN Product [P1] ON P1.Id=StockTransferItem.ProductId
                                 
                             WHERE StockTransfer.Id='" + id + "'";
            //WHERE StockTransfer.Id='" + id + "' And StockTransfer.InstanceId='" + instanceId+"'";

            var list = new List<StockTransferItem>();  //new List<StockTransfer>();
            var qb = QueryBuilderFactory.GetQueryBuilder(queryresult);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    //var stockTransferlist = new StockTransfer
                    //{
                       
                    //    AccountId = reader[StockTransferTable.AccountIdColumn.ColumnName].ToString(),
                    //    Id = reader[StockTransferTable.IdColumn.ColumnName].ToString(),
                    //    InstanceId = reader[StockTransferTable.InstanceIdColumn.ColumnName].ToString(),
                    //    ToInstanceId = reader[StockTransferTable.ToInstanceIdColumn.ColumnName].ToString(),
                    //    TransferDate = reader[StockTransferTable.TransferDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                    //    CreatedAt = reader[StockTransferTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                    //    TransferStatus = Convert.ToInt32(reader[StockTransferTable.TransferStatusColumn.ColumnName]),
                    //    Comments = reader[StockTransferTable.CommentsColumn.ColumnName].ToString(),
                    //    FromInstanceId = reader[StockTransferTable.FromInstanceIdColumn.ColumnName].ToString(),
                    //    TransferNo = reader[StockTransferTable.TransferNoColumn.ColumnName].ToString(),
                    //    UpdatedAt = reader[StockTransferTable.UpdatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                    //    UpdatedBy = reader[StockTransferTable.UpdatedByColumn.ColumnName].ToString(),
                    //    TaxRefNo = Convert.ToInt32(reader[StockTransferTable.TaxRefNoColumn.ColumnName].ToString()),

                    //};
                    var stockTransferItem = new StockTransferItem
                    {
                        AccountId = reader[StockTransferItemTable.AccountIdColumn.ColumnName].ToString(),


                        TransferId = reader[StockTransferItemTable.TransferIdColumn.ColumnName].ToString(),
                        BatchNo = reader[StockTransferItemTable.BatchNoColumn.ColumnName].ToString(),
                        ExpireDate = reader[StockTransferItemTable.ExpireDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        MRP = reader[StockTransferItemTable.MRPColumn.ColumnName] as decimal? ?? 0,
                        SellingPrice = reader[StockTransferItemTable.SellingPriceColumn.ColumnName] as decimal? ?? 0,
                        PurchasePrice = reader[StockTransferItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageSize = reader[StockTransferItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        Quantity = reader[StockTransferItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        VAT = reader[StockTransferItemTable.VATColumn.ColumnName] as decimal? ?? 0,
                        GstTotal = reader[StockTransferItemTable.GstTotalColumn.ColumnName] as decimal? ?? 0,
                        Cgst = reader[StockTransferItemTable.CgstColumn.ColumnName] as decimal? ?? 0,
                        Sgst = reader[StockTransferItemTable.SgstColumn.ColumnName] as decimal? ?? 0,
                        Igst = reader[StockTransferItemTable.IgstColumn.ColumnName] as decimal? ?? 0,
                        ProductId = reader[StockTransferItemTable.ProductIdColumn.ColumnName].ToString(),
                      

                    };
                    stockTransferItem.ProductStock.productName = reader[ProductTable.NameColumn.ColumnName].ToString();
                    //stockTransferlist.StockTransferItems.Add(stockTransferItem);
                    list.Add(stockTransferItem);
                }

            }

          //  var StockTransferItem = list.FirstOrDefault();

          //  if (StockTransferItem == null)
            //    return new StockTransferItem();
            // stockTransfer.StockTransferItems = list.SelectMany(x => x.StockTransferItems).ToList();
            // stockTransfer.IsTransferSettings = getTranserStatus.TransferQtyType;
            return list;
        }
    }
}
