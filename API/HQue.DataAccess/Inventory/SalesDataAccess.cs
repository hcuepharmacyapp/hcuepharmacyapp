﻿using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using DataAccess.QueryBuilder;
using Utilities.Enum;
using Utilities.Helpers;
using DataAccess.ManageData.Interface;
using DataAccess.Criteria;
using HQue.DataAccess.Helpers.Extension;
using HQue.Contract.Infrastructure.Settings;
using HQue.Contract.Infrastructure.Accounts;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure.Master;
using HQue.Contract.External;
using DataAccess;
using HQue.DataAccess.Inventory;
using HQue.DataAccess.Helpers;
using System.Reflection;
using System.IO;
using HQue.Contract.Infrastructure.Setup;
using HQue.DataAccess.Inventory;
using System.Data;
using System.Data.SqlClient;
using HQue.Contract.Base;
using HQue.ServiceConnector.Connector;
using HQue.Contract.External.Common;
using Newtonsoft.Json;

namespace HQue.DataAccess.Inventory
{
    public class SalesDataAccess : BaseDataAccess
    {
        SqlDatabaseHelper sqldb; //Added to convert qry to SP on 15022017 by Poongodi
        private readonly ConfigHelper _configHelper;
        private readonly ProductDataAccess _productDataAccess;
        private readonly HelperDataAccess _HelperDataAccess;
        private readonly ProductStockDataAccess _productStockDataAccess;
        public SalesDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory, ProductDataAccess productDataAccess, ProductStockDataAccess productStockDataAccess, HelperDataAccess helperDataAccess, ConfigHelper configHelper) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _configHelper = configHelper;
            _productDataAccess = productDataAccess;
            _productStockDataAccess = productStockDataAccess;
            _HelperDataAccess = helperDataAccess; //Added by Poongodi on 26/03/2017
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
        }
        //Added by Bikas on 15-05-18
        public string ConvertNumbertoWords(string numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";

            String endStr = ("Only.");

            try

            {

                int decimalPlace = numb.IndexOf(".");

                if (decimalPlace > 0)

                {

                    wholeNo = numb.Substring(0, decimalPlace);

                    points = numb.Substring(decimalPlace + 1);

                    if ((Convert.ToInt32(points) > 0) && Convert.ToInt32(wholeNo) == 0)

                    {
                        andStr += translateWholeNumber(points).Trim() + " Paise";
                    }
                    else if ((Convert.ToInt32(points) > 0) && Convert.ToInt32(wholeNo) > 0)
                    {
                        andStr = ("Rupees and ");// just to separate whole numbers from points/Rupees                  
                        andStr += translateWholeNumber(points).Trim() + " Paise";
                    }
                    else if ((Convert.ToInt32(points) == 0) && Convert.ToInt32(wholeNo) > 0)
                    {
                        andStr = ("Rupees");// just to separate whole numbers from points/Rupees                  
                        //andStr += translateWholeNumber(points).Trim() + "";
                    }
                }

                val = String.Format("{0} {1}{2} {3}", translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);

            }

            catch

            {

                ;

            }

            return val;
        }
        private String translateWholeNumber(String number)

        {

            string word = "";

            try

            {

                bool beginsZero = false;//tests for 0XX

                bool isDone = false;//test if already translated

                double dblAmt = (Convert.ToDouble(number));

                //if ((dblAmt > 0) && number.StartsWith("0"))



                if (dblAmt > 0)

                {//test for zero or digit zero in a nuemric

                    beginsZero = number.StartsWith("0");

                    int numDigits = number.Length;

                    int pos = 0;//store digit grouping

                    String place = "";//digit grouping name:hundres,thousand,etc...

                    switch (numDigits)

                    {

                        case 1://ones' range

                            word = ones(number);

                            isDone = true;

                            break;

                        case 2://tens' range

                            word = tens(number);

                            isDone = true;

                            break;

                        case 3://hundreds' range

                            pos = (numDigits % 3) + 1;

                            place = " Hundred ";

                            break;

                        case 4://thousands' range

                        case 5:

                            pos = (numDigits % 4) + 1;

                            place = " Thousand ";

                            break;

                        case 6:



                        case 7://millions' range

                            pos = (numDigits % 6) + 1;

                            // place = " Million ";

                            place = " Lakh ";

                            break;

                        case 8:

                        case 9:



                        case 10://Billions's range

                            pos = (numDigits % 8) + 1;

                            place = " Core ";

                            break;


                        //add extra case options for anything above Billion...

                        default:

                            isDone = true;

                            break;

                    }

                    if (!isDone)

                    {//if transalation is not done, continue...(Recursion comes in now!!)

                        if (beginsZero) place = "";

                        word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));

                        //check for trailing zeros

                        if (beginsZero) word =  word.Trim();

                    }

                    //ignore digit grouping names

                    if (word.Trim().Equals(place.Trim())) word = "";

                }

            }

            catch

            {

                ;

            }

            return word.Trim();

        }
        private String tens(String digit)

        {

            int digt = Convert.ToInt32(digit);

            String name = null;

            switch (digt)

            {

                case 10:

                    name = "Ten";

                    break;

                case 11:

                    name = "Eleven";

                    break;

                case 12:

                    name = "Twelve";

                    break;

                case 13:

                    name = "Thirteen";

                    break;

                case 14:

                    name = "Fourteen";

                    break;

                case 15:

                    name = "Fifteen";

                    break;

                case 16:

                    name = "Sixteen";

                    break;

                case 17:

                    name = "Seventeen";

                    break;

                case 18:

                    name = "Eighteen";

                    break;

                case 19:

                    name = "Nineteen";

                    break;

                case 20:

                    name = "Twenty";

                    break;

                case 30:

                    name = "Thirty";

                    break;

                case 40:

                    name = "Fourty";

                    break;

                case 50:

                    name = "Fifty";

                    break;

                case 60:

                    name = "Sixty";

                    break;

                case 70:

                    name = "Seventy";

                    break;

                case 80:

                    name = "Eighty";

                    break;

                case 90:

                    name = "Ninety";

                    break;

                default:

                    if (digt > 0)

                    {

                        name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));

                    }

                    break;

            }

            return name;

        }
        private String ones(String digit)

        {

            int digt = Convert.ToInt32(digit);

            String name = "";

            switch (digt)

            {

                case 1:

                    name = "One";

                    break;

                case 2:

                    name = "Two";

                    break;

                case 3:

                    name = "Three";

                    break;

                case 4:

                    name = "Four";

                    break;

                case 5:

                    name = "Five";

                    break;

                case 6:

                    name = "Six";

                    break;

                case 7:

                    name = "Seven";

                    break;

                case 8:

                    name = "Eight";

                    break;

                case 9:

                    name = "Nine";

                    break;

            }

            return name;
            //end function
        }
        public async Task<Sales> SaveBulkData(Sales data)
        {
            List<SyncObject> syncObjectList = new List<SyncObject>();
            SyncObject syncObject = null;
            SqlConnection con = null;
            SqlTransaction transaction = null;
            List<dynamic> qbList = null;
            var index = 0;

            try
            {
                con = QueryExecuter.OpenConnection();
                if (_configHelper.AppConfig.OfflineMode)
                {
                    transaction = con.BeginTransaction();

                    //ExecuteTransactionOrder is to avoid deadlock issue
                    var list = data.GetExecutionQuery();
                    list = list.Where(x => x.Table != null).Select(x => x.Table).Distinct().ToList();
                    var tableList = "'" + string.Join("','", from item in list select item.TableName) + "','ProductStock'";
                    await ExecuteTransactionOrder(tableList, con, transaction);
                }

                await validateStock(data);
                if (data.SaveFailed)
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    return data;
                }

                //Get query list
                qbList = data.GetExecutionQuery();
                //Update product stock
                await _productStockDataAccess.BulkUpdateProductStock(data.TransactionStockList, con, transaction, syncObjectList);

                for (; index < qbList.Count; index++)
                {
                    QueryBuilderBase qbData = qbList[index];
                    if (qbData.ExecuteCommand == true)
                    {
                        if (qbData.Table == SalesTable.Table && qbData.OperationType == OperationType.Insert)
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            qbData.Parameters.ToList().ForEach(x => param.Add(x.Key, x.Value));

                            var result = await sqldb.ExecuteProcedureAsync<Sales>("usp_Sales_Create", param, con, transaction);
                            qbData.Parameters[SalesTable.InvoiceNoColumn.ColumnName] = result.FirstOrDefault().InvoiceNo;
                        }
                        else if (qbData.Table == UserReminderTable.Table && qbData.OperationType == OperationType.Insert)
                        {
                            qbData.Parameters[UserReminderTable.ReferenceIdColumn.ColumnName] = data.Id;
                            await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                        }
                        else if (qbData.Table == SalesItemTable.Table && qbData.OperationType == OperationType.Insert)
                        {
                            var salesItem = data.SalesItem.Where(x => x.Id == qbData.Parameters[SalesItemTable.IdColumn.ColumnName].ToString()).First();
                            if (salesItem.SalesPatientId == salesItem.SalesOrderEstimatePatientId)
                            {
                                if (salesItem.SalesOrderEstimateId != null && salesItem.SalesOrderEstimateId != "")
                                {
                                    if (salesItem.SalesOrderProductSelected == 1)
                                    {
                                        if (salesItem.productId != null)
                                        {
                                            var salesOrderQty = await getSalesOrderReceivedQty(salesItem.SalesOrderEstimateId, salesItem.AccountId, salesItem.InstanceId, salesItem.productId, con, transaction);
                                            if (salesOrderQty != null)
                                            {
                                                foreach (var salesOrder in salesOrderQty)
                                                {
                                                    decimal? ReceivedQty = 0;
                                                    if (salesOrder.ReceivedQty == 0)
                                                    {
                                                        if (salesOrder.Quantity > salesItem.Quantity)
                                                        {
                                                            ReceivedQty = salesItem.Quantity;
                                                        }
                                                        else
                                                        {
                                                            ReceivedQty = salesOrder.Quantity;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (salesItem.OriginalQty != null || salesItem.OriginalQty != 0)
                                                        {
                                                        }
                                                        else
                                                        {
                                                            if ((salesOrder.ReceivedQty + salesItem.Quantity) > salesOrder.Quantity)
                                                            {
                                                                ReceivedQty = salesOrder.Quantity;
                                                            }
                                                            else
                                                            {
                                                                ReceivedQty = salesOrder.ReceivedQty + salesItem.Quantity;
                                                            }
                                                        }
                                                    }
                                                    if (salesItem.OriginalQty == null || salesItem.OriginalQty == 0)
                                                    {
                                                        if ((salesOrder.ReceivedQty + salesItem.Quantity) > salesOrder.Quantity)
                                                        {
                                                            ReceivedQty = salesOrder.Quantity;
                                                        }
                                                        else
                                                        {
                                                            ReceivedQty = salesOrder.ReceivedQty + salesItem.Quantity;
                                                        }

                                                        if (salesOrder.Quantity == ReceivedQty)
                                                        {
                                                            var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Update);
                                                            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.AccountIdColumn, salesItem.AccountId);
                                                            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.InstanceIdColumn, salesItem.InstanceId);
                                                            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.ProductIdColumn, salesItem.productId);
                                                            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.SalesOrderEstimateIdColumn, salesItem.SalesOrderEstimateId);
                                                            qb.AddColumn(SalesOrderEstimateItemTable.IsOrderColumn, SalesOrderEstimateItemTable.UpdatedAtColumn, SalesOrderEstimateItemTable.UpdatedByColumn, SalesOrderEstimateItemTable.ReceivedQtyColumn);
                                                            qb.Parameters.Add(SalesOrderEstimateItemTable.IsOrderColumn.ColumnName, 1);
                                                            qb.Parameters.Add(SalesOrderEstimateItemTable.ReceivedQtyColumn.ColumnName, ReceivedQty);
                                                            qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                                            qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                                            qb.SetExecuteCommand(false);
                                                            qbList.Add(qb);
                                                            await QueryExecuter.NonQueryAsyc(qb, con, transaction);
                                                        }
                                                        else
                                                        {
                                                            var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Update);
                                                            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.AccountIdColumn, salesItem.AccountId);
                                                            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.InstanceIdColumn, salesItem.InstanceId);
                                                            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.ProductIdColumn, salesItem.productId);
                                                            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.SalesOrderEstimateIdColumn, salesItem.SalesOrderEstimateId);
                                                            qb.AddColumn(SalesOrderEstimateItemTable.UpdatedAtColumn, SalesOrderEstimateItemTable.UpdatedByColumn, SalesOrderEstimateItemTable.ReceivedQtyColumn);
                                                            qb.Parameters.Add(SalesOrderEstimateItemTable.ReceivedQtyColumn.ColumnName, ReceivedQty);
                                                            qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                                            qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                                            qb.SetExecuteCommand(false);
                                                            qbList.Add(qb);
                                                            await QueryExecuter.NonQueryAsyc(qb, con, transaction);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (salesItem.SalesEstimateProductSelected == 2)
                                    {
                                        var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateTable.Table, OperationType.Update);
                                        qb.ConditionBuilder.And(SalesOrderEstimateTable.AccountIdColumn, salesItem.AccountId);
                                        qb.ConditionBuilder.And(SalesOrderEstimateTable.InstanceIdColumn, salesItem.InstanceId);
                                        qb.ConditionBuilder.And(SalesOrderEstimateTable.IdColumn, salesItem.SalesOrderEstimateId);
                                        qb.AddColumn(SalesOrderEstimateTable.IsEstimateActiveColumn, SalesOrderEstimateTable.UpdatedAtColumn, SalesOrderEstimateTable.UpdatedByColumn);
                                        qb.Parameters.Add(SalesOrderEstimateTable.IsEstimateActiveColumn.ColumnName, 1);
                                        qb.Parameters.Add(SalesOrderEstimateTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                        qb.Parameters.Add(SalesOrderEstimateTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                        qb.SetExecuteCommand(false);
                                        qbList.Add(qb);
                                        await QueryExecuter.NonQueryAsyc(qb, con, transaction);

                                        var qb1 = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Update);
                                        qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.AccountIdColumn, salesItem.AccountId);
                                        qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.InstanceIdColumn, salesItem.InstanceId);
                                        qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.SalesOrderEstimateIdColumn, salesItem.SalesOrderEstimateId);
                                        qb1.AddColumn(SalesOrderEstimateItemTable.IsOrderColumn, SalesOrderEstimateItemTable.UpdatedAtColumn, SalesOrderEstimateItemTable.UpdatedByColumn);
                                        qb1.Parameters.Add(SalesOrderEstimateItemTable.IsOrderColumn.ColumnName, 1);
                                        qb1.Parameters.Add(SalesOrderEstimateItemTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                        qb1.Parameters.Add(SalesOrderEstimateItemTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                        qb1.SetExecuteCommand(false);
                                        qbList.Add(qb1);
                                        await QueryExecuter.NonQueryAsyc(qb1, con, transaction);
                                    }

                                    int salesCount = await SalesOrderEstimatePendingCount(salesItem.SalesOrderEstimateId, salesItem.AccountId, salesItem.InstanceId, con, transaction);
                                    if (salesCount == 0)
                                    {
                                        var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateTable.Table, OperationType.Update);
                                        qb.ConditionBuilder.And(SalesOrderEstimateTable.AccountIdColumn, salesItem.AccountId);
                                        qb.ConditionBuilder.And(SalesOrderEstimateTable.InstanceIdColumn, salesItem.InstanceId);
                                        qb.ConditionBuilder.And(SalesOrderEstimateTable.IdColumn, salesItem.SalesOrderEstimateId);
                                        qb.AddColumn(SalesOrderEstimateTable.IsEstimateActiveColumn, SalesOrderEstimateTable.UpdatedAtColumn, SalesOrderEstimateTable.UpdatedByColumn);
                                        qb.Parameters.Add(SalesOrderEstimateTable.IsEstimateActiveColumn.ColumnName, 1);
                                        qb.Parameters.Add(SalesOrderEstimateTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                        qb.Parameters.Add(SalesOrderEstimateTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                        qb.SetExecuteCommand(false);
                                        qbList.Add(qb);
                                        await QueryExecuter.NonQueryAsyc(qb, con, transaction);
                                    }
                                }
                            }

                            await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                        }
                        else
                        {
                            await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                        }
                    }

                    //Add sync data to list
                    if (qbData.Table == PatientTable.Table && qbData.OperationType == OperationType.Insert)
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters, EventLevel = "A" };
                    }
                    else
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters };
                    }
                    syncObjectList.Add(syncObject);
                }

                //Write sync file
                WriteToSyncQueue(syncObjectList);

                //Commit the transaction
                if (transaction != null)
                {
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                data.SaveFailed = true;
                if (transaction != null)
                {
                    transaction.Rollback();
                }

                var unSavedData = new List<SyncObject>();
                if (qbList != null)
                {
                    for (; index < qbList.Count; index++)
                    {
                        if (qbList[index].Table == PatientTable.Table && qbList[index].OperationType == OperationType.Insert)
                        {
                            syncObject = new SyncObject() { AccountId = user.AccountId(), InstanceId = user.InstanceId(), QueryText = qbList[index].GetQuery(), Parameters = qbList[index].Parameters, EventLevel = "A" };
                        }
                        else
                        {
                            syncObject = new SyncObject() { AccountId = user.AccountId(), InstanceId = user.InstanceId(), QueryText = qbList[index].GetQuery(), Parameters = qbList[index].Parameters };
                        }
                        unSavedData.Add(syncObject);
                    }
                }

                var errorLog = new BaseErrorLog()
                {
                    AccountId = user.AccountId(),
                    InstanceId = user.InstanceId(),
                    ErrorMessage = e.Message,
                    ErrorStackTrace = e.StackTrace,
                    UnSavedData = JsonConvert.SerializeObject(unSavedData),
                    Data = JsonConvert.SerializeObject(data),
                    CreatedBy = user.Identity.Id(),
                    UpdatedBy = user.Identity.Id()
                };
                data.ErrorLog = errorLog;
                await Insert(errorLog, ErrorLogTable.Table);

                //Write executed sync query for reverse sync
                if (!_configHelper.AppConfig.OfflineMode && syncObjectList.Count > 0)
                {
                    WriteToSyncQueue(syncObjectList);
                }
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                }
                if (con != null)
                {
                    QueryExecuter.CloseConnection(con);
                }
            }
            return data;
        }
        public async Task<Sales> validateStock(Sales data)
        {
            var result = data.SalesItem.GroupBy(x => x.ProductStockId).Select(y => new ProductStock
            {
                Id = y.First().ProductStockId,
                transQuantity = y.Sum(c => c.Quantity),
            }).ToList();

            string StockIdsWithReqQty = string.Empty;
            if (data.SalesEditMode)
            {
                List<ProductStock> soldItems = data._soldItems.GroupBy(x => x.ProductStockId).Select(y => new ProductStock
                {
                    Id = y.First().ProductStockId,
                    transQuantity = y.Sum(c => c.Quantity),
                }).ToList();

                var result1 = (from saleItem in result
                               join soldItem in soldItems on saleItem.Id equals soldItem.Id into tmp
                               from tm in tmp.DefaultIfEmpty()
                               select new ProductStock
                               {
                                   Id = saleItem.Id,
                                   transQuantity = saleItem.transQuantity - (tm?.transQuantity ?? 0),
                               }).ToList();
                var result2 = (from soldItem in soldItems
                               join saleItem in result on soldItem.Id equals saleItem.Id into tmp
                               from tm in tmp.DefaultIfEmpty()
                               select new ProductStock
                               {
                                   Id = soldItem.Id,
                                   transQuantity = (tm?.transQuantity ?? 0) - soldItem.transQuantity,
                               }).ToList();
                result1.AddRange(result2);
                data.TransactionStockList = result1.Where(x => x.transQuantity != 0).ToList();
                StockIdsWithReqQty = string.Join(",", from item in result1.Where(x => x.transQuantity > 0) select item.Id + "~" + item.transQuantity);
            }
            else
            {
                data.TransactionStockList = result;
                StockIdsWithReqQty = string.Join(",", from item in result select item.Id + "~" + item.transQuantity);
            }

            if (!string.IsNullOrEmpty(StockIdsWithReqQty))
            {
                var validateStock = await _productStockDataAccess.ValidateStock(data.AccountId, data.InstanceId, StockIdsWithReqQty);
                if (validateStock.Count() > 0)
                {
                    data.SaveFailed = true;
                    data.ErrorLog = new BaseErrorLog() { ErrorMessage = "Stock Validation Failed" };
                    data.ValidateStockList = validateStock;
                    return data;
                }
                else
                {
                    data.SaveFailed = false;
                }
            }
            return data;
        }
        public async Task<int> SalesOrderEstimatePendingCount(string SalesOrderEstimateId, string AccountId, string InstanceId, SqlConnection con, SqlTransaction transaction)
        {
            var query = $@"select count(SalesOrderEstimateId) as SalesOrderEstimateId from SalesOrderEstimateitem 
            where AccountId='{AccountId}' and InstanceId='{InstanceId}' and SalesOrderEstimateId='{SalesOrderEstimateId}' and (IsOrder is NULL or IsOrder='')";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var count = Convert.ToInt32(await SingleValueAsyc(qb, con, transaction));
            return count;
        }
        public override Task<Sales> Save<Sales>(Sales data)
        {
            if (data.WriteExecutionQuery)
            {
                if (string.IsNullOrEmpty(data.Id))
                {
                    SetInsertMetaData(data, SalesTable.Table);
                    WriteInsertExecutionQuery(data, SalesTable.Table);
                }
                else
                {
                    WriteUpdateExecutionQuery(data, SalesTable.Table);
                }
                return Task.FromResult(data);
            }
            else
            {
                return string.IsNullOrEmpty(data.Id) ? Insert(data, SalesTable.Table) : Update(data, SalesTable.Table);
            }
        }
        public async Task<Sales> SaveSale(Sales data)
        {
            //if(string.IsNullOrEmpty(data.Id))
            //{
            //    await InsertSale(data);
            //}
            //else
            //{ }
            return string.IsNullOrEmpty(data.Id) ? await InsertSale(data) : await Update(data, SalesTable.Table);
        }

        private async Task<Sales> InsertSale(Sales sales)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();

            //param.Add("InstanceId", psExisting.InstanceId);
            sales.Id = Guid.NewGuid().ToString();
            sales.CreatedAt = CustomDateTime.IST;
            sales.UpdatedAt = CustomDateTime.IST;
            sales.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());

            //SalesTable.Table.ColumnList.ToList().ForEach(x => qb.Parameters.Add(x.ColumnName, salesItem[x.ColumnName]));
            SalesTable.Table.ColumnList.ToList().ForEach(x => param.Add(x.ColumnName, sales[x.ColumnName]));
            //param.Remove("IsRoundOff");

            var result = await sqldb.ExecuteProcedureAsync<Sales>("usp_Sales_Create", param);

            var sale = result.FirstOrDefault();

            var query = @"Insert Into Sales(Id, AccountId, InstanceId, PatientId, InvoiceSeries, Prefix, InvoiceNo, InvoiceDate, Name , Mobile, Email, DOB, Age, Gender, Address, Pincode, DoctorName, DoctorMobile, PaymentType, DeliveryType, BillPrint , SendSms, SendEmail, Discount, DiscountValue, DiscountType, RoundoffNetAmount, Credit, FileName, OfflineStatus , CreatedAt, UpdatedAt, CreatedBy, UpdatedBy, CashType, CardNo, CardDate, ChequeNo, ChequeDate, CardDigits , CardName, Cancelstatus, SalesType, NetAmount, BankDeposited, AmountCredited, FinyearId, DoctorId, VatAmount , SalesItemAmount, LoyaltyPts, RedeemPts, RedeemAmt, DoorDeliveryStatus, GstAmount,IsRoundOff, TaxRefNo,LoyaltyId,RedeemPercent) Values(@Id, @AccountId, @InstanceId, @PatientId, @InvoiceSeries, @Prefix, @InvoiceNo, @InvoiceDate, @Name , @Mobile, @Email, @DOB, @Age, @Gender, @Address, @Pincode, @DoctorName, @DoctorMobile, @PaymentType, @DeliveryType, @BillPrint , @SendSms, @SendEmail, @Discount, @DiscountValue, @DiscountType, @RoundoffNetAmount, @Credit, @FileName, @OfflineStatus , @CreatedAt, @UpdatedAt, @CreatedBy, @UpdatedBy, @CashType, @CardNo, @CardDate, @ChequeNo, @ChequeDate, @CardDigits , @CardName, @Cancelstatus, @SalesType, @NetAmount, @BankDeposited, @AmountCredited, @FinyearId, @DoctorId, @VatAmount , @SalesItemAmount, @LoyaltyPts, @RedeemPts, @RedeemAmt, @DoorDeliveryStatus, @GstAmount,@IsRoundOff, @TaxRefNo,@LoyaltyId,@RedeemPercent)";
            param[SalesTable.InvoiceNoColumn.ColumnName] = sale.InvoiceNo;

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            param.ToList().ForEach(x => qb.Parameters.Add(x.Key, x.Value));

            WriteToSyncQueue(new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qb.GetQuery(), Parameters = param, EventLevel = "I" });
            return sales;
        }

        public async Task UpdateSalesByItems(Sales sales)
        {
            var query = @"Update S Set S.SalesItemAmount = IsNull(SI.ItemAmount, 0)
                    , S.NetAmount = Round(IsNull(NetAmountFromSalesItemAmount, 0), 0)
                    , S.RoundoffNetAmount = (Round(IsNull(NetAmountFromSalesItemAmount, 0), 0) - IsNull(NetAmountFromSalesItemAmount, 0))
                    , S.GstAmount = IsNull(SI_GstAmount, 0)
                    , S.VatAmount = IsNull(SI_VatAmount, 0)
                    , S.DiscountValue = IsNull(Calc_DiscountValue, 0) from Sales S Join(
                         Select S1.Id as SalesId, Sum(IsNull(SI.ItemAmount, (SI.SellingPrice * SI.Quantity))) ItemAmount
                        , Sum(IsNull(SI.ItemAmount, (SI.SellingPrice * SI.Quantity)) * (1 - (IsNull(SI.Discount, 0) / 100))) * (1 - (IsNull(S1.Discount, 0) / 100)) NetAmountFromSalesItemAmount
                        , Sum(IsNull(SI.GstAmount, 0) * (1 - (IsNull(S1.Discount, 0) / 100))) SI_GstAmount, Sum(IsNull(SI.VatAmount, 0) * (1 - (IsNull(S1.Discount, 0) / 100))) SI_VatAmount
                        , Sum(IsNull(SI.ItemAmount, (SI.SellingPrice * SI.Quantity)) * (1 - (IsNull(SI.Discount, 0) / 100))) * IsNull(S1.Discount, 0) / 100 Calc_DiscountValue
                         from Sales S1 Left Join SalesItem SI On SI.SalesId = S1.Id Where S1.Id = @SalesId
                        Group By S1.Id, S1.Discount
                ) SI On SI.SalesId = S.Id";

            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.Parameters.Add(SalesItemTable.SalesIdColumn.ColumnName, sales.Id);
            await QueryExecuter.NonQueryAsyc(qb);
        }

        public async Task DeleteSale(Sales sales)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Delete);
            qb.ConditionBuilder.And(SalesTable.IdColumn, sales.Id);
            await QueryExecuter.NonQueryAsyc(qb);
        }

        public async Task Save(IEnumerable<SalesItem> itemList)
        {
            //Parallel.ForEach(itemList, async salesItem =>
            //{
            //    if (salesItem.IsNew)
            //        await Insert(salesItem, SalesItemTable.Table);
            //    else
            //        await Update(salesItem, SalesItemTable.Table);
            //});


            foreach (var salesItem in itemList)
            {
                //salesItem.ItemAmount = salesItem.Total;
                //salesItem.DiscountAmount = salesItem.SalesItemDiscount;
                //salesItem.VatAmount = salesItem.SalesItemVatAmount;

                //if (salesItem.SalesDiscount > 0) // Added by Sarubala on 11-09-17
                //{
                //    salesItem.VatAmount = (salesItem.Total - (salesItem.Total * salesItem.SalesDiscount / 100)) * (1 - (100 / (salesItem.VAT + 100)));
                //}
                // Added by Sales Order and Estimate Converted Status 28-10-2017 Start
                salesItem.SalesOrderEstimateType = salesItem.SalesOrderProductSelected;
                if (salesItem.SalesPatientId == salesItem.SalesOrderEstimatePatientId)
                {
                    if (!salesItem.WriteExecutionQuery)
                    {
                        if (salesItem.SalesOrderEstimateId != null && salesItem.SalesOrderEstimateId != "")
                        {
                            if (salesItem.SalesOrderProductSelected == 1)
                            {
                                if (salesItem.productId != null)
                                {
                                    var salesOrderQty = await getSalesOrderReceivedQty(salesItem.SalesOrderEstimateId, salesItem.AccountId, salesItem.InstanceId, salesItem.productId);
                                    if (salesOrderQty != null)
                                    {
                                        foreach (var salesOrder in salesOrderQty)
                                        {
                                            decimal? ReceivedQty = 0;
                                            decimal? PendingQty = 0;
                                            if (salesOrder.ReceivedQty == 0)
                                            {
                                                if (salesOrder.Quantity > salesItem.Quantity)
                                                {
                                                    ReceivedQty = salesItem.Quantity;
                                                }
                                                else
                                                {
                                                    ReceivedQty = salesOrder.Quantity;
                                                }
                                            }
                                            else
                                            {
                                                if (salesItem.OriginalQty != null || salesItem.OriginalQty != 0)
                                                {

                                                }
                                                else
                                                {
                                                    if ((salesOrder.ReceivedQty + salesItem.Quantity) > salesOrder.Quantity)
                                                    {
                                                        ReceivedQty = salesOrder.Quantity;
                                                    }
                                                    else
                                                    {
                                                        ReceivedQty = salesOrder.ReceivedQty + salesItem.Quantity;
                                                    }
                                                }
                                            }
                                            if (salesItem.OriginalQty == null || salesItem.OriginalQty == 0)
                                            {
                                                if (salesItem.IsNew == true)
                                                {
                                                    if ((salesOrder.ReceivedQty + salesItem.Quantity) > salesOrder.Quantity)
                                                    {
                                                        ReceivedQty = salesOrder.Quantity;
                                                    }
                                                    else
                                                    {
                                                        ReceivedQty = salesOrder.ReceivedQty + salesItem.Quantity;
                                                    }

                                                    if (salesOrder.Quantity == ReceivedQty)
                                                    {
                                                        var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Update);
                                                        qb.ConditionBuilder.And(SalesOrderEstimateItemTable.AccountIdColumn, salesItem.AccountId);
                                                        qb.ConditionBuilder.And(SalesOrderEstimateItemTable.InstanceIdColumn, salesItem.InstanceId);
                                                        qb.ConditionBuilder.And(SalesOrderEstimateItemTable.ProductIdColumn, salesItem.productId);
                                                        qb.ConditionBuilder.And(SalesOrderEstimateItemTable.SalesOrderEstimateIdColumn, salesItem.SalesOrderEstimateId);
                                                        qb.AddColumn(SalesOrderEstimateItemTable.IsOrderColumn, SalesOrderEstimateItemTable.UpdatedAtColumn, SalesOrderEstimateItemTable.UpdatedByColumn, SalesOrderEstimateItemTable.ReceivedQtyColumn);
                                                        qb.Parameters.Add(SalesOrderEstimateItemTable.IsOrderColumn.ColumnName, 1);
                                                        qb.Parameters.Add(SalesOrderEstimateItemTable.ReceivedQtyColumn.ColumnName, ReceivedQty);
                                                        qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                                        qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                                        if (salesItem.WriteExecutionQuery)
                                                        {
                                                            salesItem.AddExecutionQuery(qb);
                                                        }
                                                        else
                                                        {
                                                            await QueryExecuter.NonQueryAsyc(qb);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Update);
                                                        qb.ConditionBuilder.And(SalesOrderEstimateItemTable.AccountIdColumn, salesItem.AccountId);
                                                        qb.ConditionBuilder.And(SalesOrderEstimateItemTable.InstanceIdColumn, salesItem.InstanceId);
                                                        qb.ConditionBuilder.And(SalesOrderEstimateItemTable.ProductIdColumn, salesItem.productId);
                                                        qb.ConditionBuilder.And(SalesOrderEstimateItemTable.SalesOrderEstimateIdColumn, salesItem.SalesOrderEstimateId);
                                                        qb.AddColumn(SalesOrderEstimateItemTable.UpdatedAtColumn, SalesOrderEstimateItemTable.UpdatedByColumn, SalesOrderEstimateItemTable.ReceivedQtyColumn);
                                                        qb.Parameters.Add(SalesOrderEstimateItemTable.ReceivedQtyColumn.ColumnName, ReceivedQty);
                                                        qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                                        qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                                        if (salesItem.WriteExecutionQuery)
                                                        {
                                                            salesItem.AddExecutionQuery(qb);
                                                        }
                                                        else
                                                        {
                                                            await QueryExecuter.NonQueryAsyc(qb);
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                            else if (salesItem.SalesEstimateProductSelected == 2)
                            {
                                var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateTable.Table, OperationType.Update);
                                qb.ConditionBuilder.And(SalesOrderEstimateTable.AccountIdColumn, salesItem.AccountId);
                                qb.ConditionBuilder.And(SalesOrderEstimateTable.InstanceIdColumn, salesItem.InstanceId);
                                qb.ConditionBuilder.And(SalesOrderEstimateTable.IdColumn, salesItem.SalesOrderEstimateId);
                                qb.AddColumn(SalesOrderEstimateTable.IsEstimateActiveColumn, SalesOrderEstimateTable.UpdatedAtColumn, SalesOrderEstimateTable.UpdatedByColumn);
                                qb.Parameters.Add(SalesOrderEstimateTable.IsEstimateActiveColumn.ColumnName, 1);
                                qb.Parameters.Add(SalesOrderEstimateTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                qb.Parameters.Add(SalesOrderEstimateTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                if (salesItem.WriteExecutionQuery)
                                {
                                    salesItem.AddExecutionQuery(qb);
                                }
                                else
                                {
                                    await QueryExecuter.NonQueryAsyc(qb);
                                }

                                var qb1 = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Update);
                                qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.AccountIdColumn, salesItem.AccountId);
                                qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.InstanceIdColumn, salesItem.InstanceId);
                                //qb.ConditionBuilder.And(SalesOrderEstimateItemTable.ProductIdColumn, salesItem.productId);
                                qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.SalesOrderEstimateIdColumn, salesItem.SalesOrderEstimateId);
                                qb1.AddColumn(SalesOrderEstimateItemTable.IsOrderColumn, SalesOrderEstimateItemTable.UpdatedAtColumn, SalesOrderEstimateItemTable.UpdatedByColumn);
                                qb1.Parameters.Add(SalesOrderEstimateItemTable.IsOrderColumn.ColumnName, 1);
                                qb1.Parameters.Add(SalesOrderEstimateItemTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                qb1.Parameters.Add(SalesOrderEstimateItemTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                if (salesItem.WriteExecutionQuery)
                                {
                                    salesItem.AddExecutionQuery(qb1);
                                }
                                else
                                {
                                    await QueryExecuter.NonQueryAsyc(qb1);
                                }
                            }
                            int salesCount = -1;
                            if (salesItem.WriteExecutionQuery)
                            {
                                salesCount = 0;
                            }
                            else
                            {
                                salesCount = await SalesOrderEstimatePendingCount(salesItem.SalesOrderEstimateId, salesItem.AccountId, salesItem.InstanceId);
                            }
                            if (salesCount == 0)
                            {
                                var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateTable.Table, OperationType.Update);
                                qb.ConditionBuilder.And(SalesOrderEstimateTable.AccountIdColumn, salesItem.AccountId);
                                qb.ConditionBuilder.And(SalesOrderEstimateTable.InstanceIdColumn, salesItem.InstanceId);
                                qb.ConditionBuilder.And(SalesOrderEstimateTable.IdColumn, salesItem.SalesOrderEstimateId);
                                qb.AddColumn(SalesOrderEstimateTable.IsEstimateActiveColumn, SalesOrderEstimateTable.UpdatedAtColumn, SalesOrderEstimateTable.UpdatedByColumn);
                                qb.Parameters.Add(SalesOrderEstimateTable.IsEstimateActiveColumn.ColumnName, 1);
                                qb.Parameters.Add(SalesOrderEstimateTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                                qb.Parameters.Add(SalesOrderEstimateTable.UpdatedByColumn.ColumnName, salesItem.UpdatedBy);
                                if (salesItem.WriteExecutionQuery)
                                {
                                    salesItem.AddExecutionQuery(qb);
                                }
                                else
                                {
                                    await QueryExecuter.NonQueryAsyc(qb);
                                }
                            }
                        }
                    }
                }
                else
                {
                    salesItem.SalesOrderEstimateType = 0;
                    salesItem.SalesOrderEstimateId = "";
                }

                var sUpdatedAt = CustomDateTime.IST;

                var psUpdated = false;
                if (salesItem.WriteExecutionQuery)
                {
                    psUpdated = true;
                }
                else
                {
                    psUpdated = await _productStockDataAccess.UpdateProductStock(salesItem);
                }
                if (psUpdated)
                {
                    if (salesItem.IsNew)
                    {
                        if (salesItem.WriteExecutionQuery)
                        {
                            SetInsertMetaData(salesItem, SalesItemTable.Table);
                            WriteInsertExecutionQuery(salesItem, SalesItemTable.Table);
                        }
                        else
                        {
                            await Insert(salesItem, SalesItemTable.Table);
                        }
                        //var sql = "Insert Into SalesItem (Id, AccountId, InstanceId, SalesId, SaleProductName, ProductStockId, ReminderFrequency, ReminderDate, Quantity, ItemAmount, Vat, VatAmount, DiscountAmount, Discount, SellingPrice, Mrp, OfflineStatus, Igst, Cgst, Sgst, GstTotal, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy) ";
                        //sql += "Select @Id, @AccountId, @InstanceId, @SalesId, @SaleProductName, @ProductStockId, @ReminderFrequency, @ReminderDate, @Quantity, @ItemAmount, @Vat, @VatAmount, @DiscountAmount, @Discount, @SellingPrice, @Mrp, @OfflineStatus, @Igst, @Cgst, @Sgst, @GstTotal, @CreatedAt, @UpdatedAt, @CreatedBy, @UpdatedBy From ProductStock Where Id = @ProductStockId ";
                        ////sql += "And UpdatedAt = @UpdatedAt";
                        //sql += "And Stock >= @Stock";
                        //var qb = QueryBuilderFactory.GetQueryBuilder(sql);
                        //salesItem.Id = Guid.NewGuid().ToString();
                        //salesItem.CreatedAt = sUpdatedAt;
                        //salesItem.UpdatedAt = sUpdatedAt;
                        //salesItem.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());

                        ////foreach (var dbColumnName in SalesItemTable.Table.ColumnList)
                        ////{ 
                        ////    qb.Parameters.Add(dbColumnName.ColumnName, salesItem[dbColumnName.ColumnName]);
                        ////}
                        //SalesItemTable.Table.ColumnList.ToList().ForEach(x => qb.Parameters.Add(x.ColumnName, salesItem[x.ColumnName]));
                        //qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, salesItem.Quantity);
                        //await QueryExecuter.NonQueryAsyc(qb);
                    }

                    else
                    {
                        if (salesItem.WriteExecutionQuery)
                        {
                            WriteUpdateExecutionQuery(salesItem, SalesItemTable.Table);
                        }
                        else
                        {
                            await Update(salesItem, SalesItemTable.Table);
                        }
                        //var sqlUpdate = "Update SI Set ProductStockId = @ProductStockId, ReminderFrequency = @ReminderFrequency, ReminderDate = @ReminderDate, Quantity = @Quantity, ItemAmount = @ItemAmount, Vat = @Vat, VatAmount = @VatAmount, DiscountAmount = @DiscountAmount, Discount = @Discount, SellingPrice = @SellingPrice, Mrp = @Mrp, OfflineStatus = @OfflineStatus, Igst = @Igst, Cgst = @Cgst, Sgst = @Sgst, GstTotal = @GstTotal, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy From SalesItem SI Join ProductStock PS On SI.ProductStockId = PS.Id Where SI.Id = @Id And PS.Id = @ProductStockId And PS.Stock >= @Stock";
                        //var qbUpdate = QueryBuilderFactory.GetQueryBuilder(sqlUpdate);

                        //salesItem.CreatedAt = sUpdatedAt;
                        //salesItem.UpdatedAt = sUpdatedAt;
                        //salesItem.OfflineStatus = Convert.ToBoolean(user.OfflineStatus());
                        ////if (salesItem.ReminderDate == null)
                        ////    salesItem.ReminderDate = DateTime.MinValue;
                        //SalesItemTable.Table.ColumnList.ToList().ForEach(x => qbUpdate.Parameters.Add(x.ColumnName, salesItem[x.ColumnName]));
                        //qbUpdate.Parameters.Add(ProductStockTable.StockColumn.ColumnName, salesItem.Quantity);

                        //await QueryExecuter.NonQueryAsyc(qbUpdate);

                    }
                    salesItem.IsCreated = true;
                }
                else
                {
                    salesItem.IsCreated = false;
                }
                //var si = await GetSalesItemById(salesItem.Id, sUpdatedAt);
                //if (si != null)
                //    salesItem.IsCreated = true;
                //else
                //    salesItem.IsCreated = false;
            }
        }

        public async Task<int> SalesOrderEstimatePendingCount(string SalesOrderEstimateId, string AccountId, string InstanceId)
        {
            var query = $@"select count(SalesOrderEstimateId) as SalesOrderEstimateId from SalesOrderEstimateitem where SalesOrderEstimateId='{SalesOrderEstimateId}' 
            and AccountId='{AccountId}' and InstanceId='{InstanceId}' and (IsOrder is NULL or IsOrder='')";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var count = Convert.ToInt32(await SingleValueAsyc(qb));
            return count;
        }

        public async Task<List<SalesItem>> GetQtyBySalesId(string salesId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SalesItemTable.SalesIdColumn, salesId);

            return (await List<SalesItem>(qb));
        }

        public async Task<IEnumerable<SalesOrderEstimateItem>> getSalesOrderReceivedQty(string SalesOrderEstimateId, string AccountId, string InstanceId, string ProductId)
        {
            string query = $@"select Quantity,ISNULL(ReceivedQty,0) as ReceivedQty from SalesOrderEstimateitem where SalesOrderEstimateId='{SalesOrderEstimateId}' and AccountId='{AccountId}' and InstanceId='{InstanceId}' and ProductId='{ProductId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<SalesOrderEstimateItem>(qb);
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        public async Task<IEnumerable<SalesOrderEstimateItem>> getSalesOrderReceivedQty(string SalesOrderEstimateId, string AccountId, string InstanceId, string ProductId, SqlConnection con, SqlTransaction transaction)
        {
            string query = $@"select Quantity,ISNULL(ReceivedQty,0) as ReceivedQty from SalesOrderEstimateitem where AccountId='{AccountId}' and InstanceId='{InstanceId}' and SalesOrderEstimateId='{SalesOrderEstimateId}' and ProductId='{ProductId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<SalesOrderEstimateItem>(qb, con, transaction);
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        public Sales RemoveReturnItems(Sales data)
        {
            var tmpSalesReturn = data.SalesReturn;
            data.SalesReturn = new SalesReturn();
            foreach (SalesItem si in data.SalesItem)
            {
                if (si.salesReturn == true)
                {
                    SalesReturnItem sri = new SalesReturnItem();
                    sri.Id = si.Id != null ? si.Id : null;
                    sri.ProductStock = si.ProductStock;
                    sri.ProductStockId = si.ProductStockId;
                    sri.Quantity = si.Quantity.HasValue ? Math.Abs(si.Quantity.Value) : 0;
                    sri.SellingPrice = si.SellingPrice.HasValue ? Math.Abs(si.SellingPrice.Value) : 0;
                    //sri.Total = si.Total.HasValue ? Math.Abs(si.Total.Value) : 0;
                    sri.Discount = si.Discount;
                    sri.DiscountAmount = si.DiscountAmount;                    
                    sri.PurchaseQuantity = Math.Abs(si.Quantity.Value);
                    sri.BatchNo = si.BatchNo;
                    sri.ExpireDate = si.ExpireDate;
                    sri.MRP = si.MRP;
                    sri.VAT = si.VAT;
                    sri.CreatedAt = CustomDateTime.IST;
                    //sri.BatchNo = si.BatchNo;
                    //sri.ExpireDate = si.ExpireDate;
                    //sri.MRP = si.MRP;
                    //sri.VAT = si.VAT;
                    sri.CreatedBy = data.CreatedBy;
                    sri.UpdatedAt = CustomDateTime.IST;
                    sri.UpdatedBy = data.UpdatedBy;
                    /*Gst Section Added by Poongodi*/
                    sri.GstTotal = si.GstTotal;
                    sri.Igst = si.GstTotal;
                    sri.Sgst = si.GstTotal / 2;
                    sri.Cgst = si.GstTotal / 2;
                    sri.GstAmount = si.GstAmount;
                    sri.TotalAmount = si.TotalAmount;
                    data.SalesReturn.SalesReturnItem.Add(sri);
                    data.SalesReturn.PaymentType = data.PaymentType;
                    data.SalesReturn.IsAlongWithSale = true;
                    data.SalesReturn.ReturnDate = Convert.ToDateTime(data.InvoiceDate); // Added by Sarubala on 13-07-17 to change return date
                    data.SalesReturn.PatientId = data.PatientId;
                    data.SalesReturn.PatientMobile = data.Mobile;
                    if (data.SalesReturn.Id == null)
                    {
                        data.SalesReturn.Id = si.salesReturnId != null ? si.salesReturnId : null;
                    }

                }
            }

            data.SalesReturn.NetAmount = tmpSalesReturn.NetAmount;
            data.SalesReturn.RoundOffNetAmount = tmpSalesReturn.RoundOffNetAmount;
            data.SalesReturn.ReturnItemAmount = tmpSalesReturn.ReturnItemAmount;
            data.SalesReturn.Discount = tmpSalesReturn.Discount;
            data.SalesReturn.DiscountValue = tmpSalesReturn.DiscountValue;
            data.SalesReturn.TotalDiscountValue = tmpSalesReturn.TotalDiscountValue;
            data.SalesReturn.GstAmount = tmpSalesReturn.GstAmount;
            data.SalesReturn.IsRoundOff = tmpSalesReturn.IsRoundOff;

            data.SalesItem.RemoveAll(x => x.salesReturn == true);

            return data;



        }
        public async Task SaveSalesItemAudit(IEnumerable<SalesItem> itemList)
        {
            foreach (var salesItem in itemList)
            {
                if (salesItem.IsNew == false)
                {
                    if (salesItem.Action == "U")
                    {
                        var auditData = GetActiveRecord(salesItem.Id);
                        auditData.Result[0].CreatedBy = salesItem.UpdatedBy;
                        auditData.Result[0].UpdatedBy = salesItem.UpdatedBy;
                        if (salesItem.WriteExecutionQuery)
                        {
                            auditData.Result[0].SetExecutionQuery(salesItem.GetExecutionQuery(), salesItem.WriteExecutionQuery);
                            SetInsertMetaData(auditData.Result[0], SalesItemAuditTable.Table);
                            WriteInsertExecutionQuery(auditData.Result[0], SalesItemAuditTable.Table);
                        }
                        else
                        {
                            await Insert(auditData.Result[0], SalesItemAuditTable.Table);
                        }
                    }
                }

                //if (salesItem.IsNew == true && salesItem.Action == "N")  // Added by Sarubala to create edit log for insert in Edit Sale
                if (salesItem.IsNew == true && salesItem.Action == "N" && salesItem.IsCreated != false || salesItem.WriteExecutionQuery == true && salesItem.Action == "N" && salesItem.IsCreated != false)
                {
                    var salesItemAudit = JsonConvert.DeserializeObject<SalesItemAudit>(JsonConvert.SerializeObject(salesItem));
                    salesItemAudit.Action = "I";
                    if (salesItemAudit.WriteExecutionQuery)
                    {
                        salesItemAudit.SetExecutionQuery(salesItem.GetExecutionQuery(), salesItem.WriteExecutionQuery);
                        SetInsertMetaData(salesItemAudit, SalesItemAuditTable.Table);
                        WriteInsertExecutionQuery(salesItemAudit, SalesItemAuditTable.Table);
                    }
                    else
                    {
                        await Insert(salesItemAudit, SalesItemAuditTable.Table);
                    }
                }                                                       // End
            }
        }


        public Task<List<SalesItem>> GetActiveRecord(string itemId)
        {
            var query =
                $@"SELECT * FROM SalesItem WHERE Id = '{itemId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return List<SalesItem>(qb);
        }
        //Added by Sarubala on 28-10-17
        public async Task<string> GetSalesPaymentType(Sales data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.CashTypeColumn);
            qb.ConditionBuilder.And(SalesTable.IdColumn, data.Id);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, data.InstanceId);
            string paymentType = "";
            var result = (await List<Sales>(qb)).FirstOrDefault();
            if (result.CashType != null)
            {
                paymentType = result.CashType;
            }
            return paymentType;
        }


        public async Task DeleteCustomerPayment(Sales sales)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Delete);
            qb.ConditionBuilder.And(CustomerPaymentTable.AccountIdColumn, sales.AccountId);
            qb.ConditionBuilder.And(CustomerPaymentTable.InstanceIdColumn, sales.InstanceId);
            qb.ConditionBuilder.And(CustomerPaymentTable.SalesIdColumn, sales.Id);
            if (sales.WriteExecutionQuery)
            {
                sales.AddExecutionQuery(qb);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qb);
            }
        }
        public async Task SavetoCustomerPayment(Sales sales)
        {
            var CustomerPayment = new CustomerPayment();
            CustomerPayment.AccountId = sales.AccountId;
            CustomerPayment.InstanceId = sales.InstanceId;
            CustomerPayment.SalesId = sales.Id;
            CustomerPayment.TransactionDate = CustomDateTime.IST;
            CustomerPayment.Debit = 0.00M;
            CustomerPayment.Credit = sales.Credit;
            CustomerPayment.CreatedBy = sales.CreatedBy;
            CustomerPayment.UpdatedBy = sales.UpdatedBy;
            // Added Gavaskar 31/01/2017 Update Statement Start
            int salesCount = await CustomerPaymentSalesCount(sales.Id);
            if (salesCount > 0)
            {
                var List = await GetCustomerPaymentId(sales.Id);
                var Debit = await GetCustomerPaymentDebit(sales.Id);
                var CreditAmount = "";
                CreditAmount = (Convert.ToDecimal(Debit) + CustomerPayment.Credit).ToString();
                var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(CustomerPaymentTable.AccountIdColumn, CustomerPayment.AccountId);
                qb.ConditionBuilder.And(CustomerPaymentTable.InstanceIdColumn, CustomerPayment.InstanceId);
                qb.ConditionBuilder.And(CustomerPaymentTable.SalesIdColumn, CustomerPayment.SalesId);
                qb.ConditionBuilder.And(CustomerPaymentTable.IdColumn, List[0].Id);
                qb.AddColumn(CustomerPaymentTable.TransactionDateColumn, CustomerPaymentTable.DebitColumn, CustomerPaymentTable.CreditColumn, CustomerPaymentTable.UpdatedAtColumn);
                qb.Parameters.Add(CustomerPaymentTable.TransactionDateColumn.ColumnName, CustomDateTime.IST);
                qb.Parameters.Add(CustomerPaymentTable.DebitColumn.ColumnName, CustomerPayment.Debit);
                qb.Parameters.Add(CustomerPaymentTable.CreditColumn.ColumnName, CreditAmount);
                qb.Parameters.Add(CustomerPaymentTable.UpdatedAtColumn.ColumnName, sales.UpdatedAt);
                if (sales.WriteExecutionQuery)
                {
                    sales.AddExecutionQuery(qb);
                }
                else
                {
                    await QueryExecuter.NonQueryAsyc(qb);
                }
            }
            else
            {
                if (sales.WriteExecutionQuery)
                {
                    CustomerPayment.SetExecutionQuery(sales.GetExecutionQuery(), sales.WriteExecutionQuery);
                    SetInsertMetaData(CustomerPayment, CustomerPaymentTable.Table);
                    WriteInsertExecutionQuery(CustomerPayment, CustomerPaymentTable.Table);
                }
                else
                {
                    await InsertCustomerPayment(CustomerPayment, CustomerPaymentTable.Table);
                }
            }
            // Added Gavaskar 31/01/2017 Update Statement End
        }
        // Added Gavaskar 31/01/2017 Customer Payment Count Start
        public async Task<int> CustomerPaymentSalesCount(string SalesId)
        {
            var query = $@"select count(SalesId) as SalesId from customerpayment where SalesId='{SalesId}' and PaymentType is NULL";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var count = Convert.ToInt32(await SingleValueAsyc(qb));
            return count;
        }
        public async Task<List<CustomerPayment>> GetCustomerPaymentId(string SalesId)
        {
            var query = $@"select Id,Credit,sum(Debit)as Debit from customerpayment where SalesId='{SalesId}' and PaymentType is NULL group by Id,Credit";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return await List<CustomerPayment>(qb);
        }
        public async Task<int> GetCustomerPaymentDebit(string SalesId)
        {
            var query = $@"select sum(Debit)as Debit from customerpayment where SalesId='{SalesId}'";
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //return await List<CustomerPayment>(qb);
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var Debit = Convert.ToInt32(await SingleValueAsyc(qb));
            return Debit;
        }
        //Changed by Settu on 05-06-2017, payment decimal handled
        public async Task<decimal> GetSalesCredit(string SalesId)
        {
            var query = $@"select sum(Credit)as Credit from sales where id='{SalesId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //var Credit = Convert.ToDecimal(await SingleValueAsyc(qb));
            //Added by Sarubala on 02-11-17
            var Credit = await SingleValueAsyc(qb);
            decimal creditvalue = 0;
            if (!String.IsNullOrEmpty(Convert.ToString(Credit)) && !String.IsNullOrWhiteSpace(Convert.ToString(Credit)))
            {
                creditvalue = Convert.ToDecimal(Credit);
            }

            return creditvalue;
        }
        //Added by Senthil.S
        public async Task<int> CheckOBPaymentPaid(string accountId, string instanceId, string customerId)
        {
            int Debit = 0;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("InstanceId", instanceId);
            parms.Add("CustomerId", customerId);
            parms.Add("type", "CUS");
            var paymentDone = await sqldb.ExecuteProcedureAsync<dynamic>("usp_CheckCustomerVendorOBPaid", parms);

            foreach (IDictionary<string, object> row in paymentDone)
            {
                foreach (var pair in row)
                {
                    if (pair.Key == "Debit")
                        //Debit = Convert.ToDecimal(pair.Value); 
                        Debit = Convert.ToInt32(pair.Value);

                }
            }

            return Debit;
        }

        public async Task<int> CustomerPaymentCancelSalesCount(string SalesId)
        {
            var query = $@"select count(SalesId) as SalesId from customerpayment
inner join sales on sales.id=customerpayment.salesId where customerpayment.SalesId='{SalesId}' and sales.cancelstatus =1 ";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var count = Convert.ToInt32(await SingleValueAsyc(qb));
            return count;
        }
        // Added Gavaskar 31/01/2017 Customer Payment Count End
        // Added Gavaskar 31/01/2017 Cancel Sales Update Start
        public async Task UpdateCustomerPaymentSalesDetail(SalesReturn salesReturn)
        {
            // Update CustomerPayment
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(CustomerPaymentTable.AccountIdColumn, salesReturn.AccountId);
            qb.ConditionBuilder.And(CustomerPaymentTable.InstanceIdColumn, salesReturn.InstanceId);
            qb.ConditionBuilder.And(CustomerPaymentTable.SalesIdColumn, salesReturn.SalesId);
            qb.AddColumn(CustomerPaymentTable.TransactionDateColumn, CustomerPaymentTable.DebitColumn, CustomerPaymentTable.CreditColumn, CustomerPaymentTable.UpdatedAtColumn);
            qb.Parameters.Add(CustomerPaymentTable.TransactionDateColumn.ColumnName, CustomDateTime.IST);
            qb.Parameters.Add(CustomerPaymentTable.DebitColumn.ColumnName, "0.00");
            qb.Parameters.Add(CustomerPaymentTable.CreditColumn.ColumnName, "0.00");
            qb.Parameters.Add(CustomerPaymentTable.UpdatedAtColumn.ColumnName, salesReturn.UpdatedAt);
            await QueryExecuter.NonQueryAsyc(qb);
            // Update Sales
            var qbSale = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
            qbSale.ConditionBuilder.And(SalesTable.AccountIdColumn, salesReturn.AccountId);
            qbSale.ConditionBuilder.And(SalesTable.InstanceIdColumn, salesReturn.InstanceId);
            qbSale.ConditionBuilder.And(SalesTable.IdColumn, salesReturn.SalesId);
            qbSale.ConditionBuilder.And(SalesTable.CancelstatusColumn, 1);
            qbSale.AddColumn(SalesTable.PaymentTypeColumn, SalesTable.CreditColumn, SalesTable.UpdatedAtColumn);
            qbSale.Parameters.Add(SalesTable.CreditColumn.ColumnName, "0.00");
            qbSale.Parameters.Add(SalesTable.UpdatedAtColumn.ColumnName, salesReturn.UpdatedAt);
            await QueryExecuter.NonQueryAsyc(qbSale);
        }
        public async Task UpdateCustomerPaymentSalesReturnDetail(SalesReturn salesReturn)
        {
            var List = await GetCustomerPaymentId(salesReturn.SalesId);
            var Credit = await GetSalesCredit(salesReturn.SalesId);
            var CreditAmount = "";
            //Changed by Settu on 05-06-2017, discount payment not working
            //if (Convert.ToDecimal(Credit) >= salesReturn.SalesReturnItem[0].Total)
            if (Convert.ToDecimal(Credit) >= salesReturn.ReturnTotal)
            {
                //CreditAmount = (Convert.ToDecimal(Credit) - Math.Round((decimal)(salesReturn.SalesReturnItem[0].Total), 0, MidpointRounding.AwayFromZero)).ToString();
                CreditAmount = (Convert.ToDecimal(Credit) - Math.Round((decimal)(salesReturn.ReturnTotal), 2, MidpointRounding.AwayFromZero)).ToString();
            }
            else
            {
                CreditAmount = "0.00";
            }
            var CustomerPayment = new CustomerPayment();
            //CustomerPayment.AccountId = salesReturn.AccountId;
            //CustomerPayment.InstanceId = salesReturn.InstanceId;
            //CustomerPayment.SalesId = salesReturn.SalesId;
            //CustomerPayment.TransactionDate = CustomDateTime.IST;
            // CustomerPayment.PaymentType= "Cash";
            var CreditAmt = "";
            if (Credit > 0)
            {
                //if (salesReturn.SalesReturnItem[0].Total >= Convert.ToDecimal(Credit))
                //{
                //    CreditAmt = Convert.ToDecimal(Credit).ToString();
                //}
                //else
                //{
                //    CreditAmt = salesReturn.SalesReturnItem[0].Total.ToString();
                //}
                //if (Convert.ToDecimal(Credit) >= salesReturn.SalesReturnItem[0].Total)
                if (Convert.ToDecimal(Credit) >= salesReturn.ReturnTotal)
                {
                    //CreditAmt = (Convert.ToDecimal(List[0].Credit) - Math.Round((decimal)(salesReturn.SalesReturnItem[0].Total), 0, MidpointRounding.AwayFromZero)).ToString();
                    CreditAmt = (Convert.ToDecimal(List[0].Credit) - Math.Round((decimal)(salesReturn.ReturnTotal), 2, MidpointRounding.AwayFromZero)).ToString();
                }
                else
                {
                    CreditAmt = (Convert.ToDecimal(List[0].Credit) - Convert.ToDecimal(Credit)).ToString();
                }
                CustomerPayment.Debit = 0.00M;
                CustomerPayment.Credit = Convert.ToDecimal(CreditAmt);
                //CustomerPayment.CreatedBy = salesReturn.CreatedBy;
                //CustomerPayment.UpdatedBy = salesReturn.UpdatedBy;
                // Update CustomerPayment
                var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(CustomerPaymentTable.AccountIdColumn, salesReturn.AccountId);
                qb.ConditionBuilder.And(CustomerPaymentTable.InstanceIdColumn, salesReturn.InstanceId);
                qb.ConditionBuilder.And(CustomerPaymentTable.SalesIdColumn, salesReturn.SalesId);
                qb.ConditionBuilder.And(CustomerPaymentTable.IdColumn, List[0].Id);
                qb.AddColumn(CustomerPaymentTable.TransactionDateColumn, CustomerPaymentTable.DebitColumn, CustomerPaymentTable.CreditColumn, CustomerPaymentTable.UpdatedAtColumn);
                qb.Parameters.Add(CustomerPaymentTable.TransactionDateColumn.ColumnName, CustomDateTime.IST);
                qb.Parameters.Add(CustomerPaymentTable.DebitColumn.ColumnName, CustomerPayment.Debit);
                qb.Parameters.Add(CustomerPaymentTable.CreditColumn.ColumnName, CustomerPayment.Credit);
                qb.Parameters.Add(CustomerPaymentTable.UpdatedAtColumn.ColumnName, salesReturn.UpdatedAt);
                await QueryExecuter.NonQueryAsyc(qb);
            }
            ////if (Credit > 0)
            ////{
            ////    // Insert CustomerPayment
            ////    await InsertCustomerPayment(CustomerPayment, CustomerPaymentTable.Table);
            ////}
            // Update Sales
            var qbSale = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
            qbSale.ConditionBuilder.And(SalesTable.AccountIdColumn, salesReturn.AccountId);
            qbSale.ConditionBuilder.And(SalesTable.InstanceIdColumn, salesReturn.InstanceId);
            qbSale.ConditionBuilder.And(SalesTable.IdColumn, salesReturn.SalesId);
            qbSale.AddColumn(SalesTable.CreditColumn, SalesTable.UpdatedAtColumn);
            qbSale.Parameters.Add(SalesTable.CreditColumn.ColumnName, CreditAmount);
            qbSale.Parameters.Add(SalesTable.UpdatedAtColumn.ColumnName, salesReturn.UpdatedAt);
            await QueryExecuter.NonQueryAsyc(qbSale);
        }
        // Added Gavaskar 31/01/2017 Cancel Sales Update Start
        public async Task<bool> IsInvoiceManualSeriesAvail(string InvoiceNo, string Instanceid, string Accountid, DateTime InvoiceDate)
        {
            //var query = $@"select COUNT(InvoiceNo) from Sales where InstanceId ='{Instanceid}' and InvoiceSeries='MAN' and InvoiceNo='{Invoiceseries}'";
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //int count = Convert.ToInt32(await SingleValueAsyc(qb));
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("instanceid", Instanceid);
            parms.Add("invoiceno", InvoiceNo);
            parms.Add("InvoiceSeries", "MAN");
            parms.Add("BillDate", InvoiceDate);
            parms.Add("Accountid", Accountid);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_Sales_Invoice_Validate", parms);
            var res = result.Select(item =>
            {
                var obj = new Sales()
                {
                    SalesCount = item.count
                };
                return obj;
            }).FirstOrDefault();
            if (res.SalesCount == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public async Task<MissedOrder> CreateMissedOrder(MissedOrder order)
        {
            if (order.MissedOrderItem.Count() > 0)
            {
                foreach (var saleitem in order.MissedOrderItem)
                {
                    if (!string.IsNullOrEmpty(saleitem.ProductMasterID))
                    {
                        Product product = new Product();
                        product.Name = saleitem.ProductMasterName;
                        product.ProductMasterID = saleitem.ProductMasterID;
                        product.Manufacturer = saleitem.Manufacturer;
                        product.Schedule = saleitem.Schedule;
                        product.GenericName = saleitem.GenericName;
                        product.Category = saleitem.Category;
                        product.Packing = saleitem.Packing;
                        product.AccountId = order.AccountId;
                        product.InstanceId = order.InstanceId;
                        product.CreatedBy = order.CreatedBy;
                        product.UpdatedBy = order.UpdatedBy;
                        product.CreatedAt = CustomDateTime.IST;
                        product.UpdatedAt = CustomDateTime.IST;
                        //int NoOfRows = await _productDataAccess.TotalProductCount(product);
                        //var getChar = product.Name.Substring(0, 2);
                        //product.Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
                        product = await _productDataAccess.Save(product);
                        saleitem.Id = product.Id;
                    }
                    var qb = QueryBuilderFactory.GetQueryBuilder(MissedOrderTable.Table, OperationType.Insert);
                    qb.AddColumn(MissedOrderTable.IdColumn, MissedOrderTable.AccountIdColumn, MissedOrderTable.InstanceIdColumn, MissedOrderTable.PatientIdColumn,
                        MissedOrderTable.PatientMobileColumn, MissedOrderTable.PatientNameColumn, MissedOrderTable.PatientAgeColumn, MissedOrderTable.PatientGenderColumn, MissedOrderTable.ProductIdColumn, MissedOrderTable.QuantityColumn, MissedOrderTable.SellingPriceColumn,
                        MissedOrderTable.CreatedByColumn, MissedOrderTable.UpdatedByColumn, MissedOrderTable.CreatedAtColumn, MissedOrderTable.UpdatedAtColumn);
                    var Id = Guid.NewGuid().ToString();
                    qb.Parameters.Add(MissedOrderTable.IdColumn.ColumnName, Id);
                    qb.Parameters.Add(MissedOrderTable.AccountIdColumn.ColumnName, order.AccountId);
                    qb.Parameters.Add(MissedOrderTable.InstanceIdColumn.ColumnName, order.InstanceId);
                    qb.Parameters.Add(MissedOrderTable.PatientIdColumn.ColumnName, order.PatientId);
                    qb.Parameters.Add(MissedOrderTable.PatientMobileColumn.ColumnName, order.PatientMobile);
                    qb.Parameters.Add(MissedOrderTable.PatientNameColumn.ColumnName, order.PatientName);
                    qb.Parameters.Add(MissedOrderTable.PatientAgeColumn.ColumnName, order.PatientAge);
                    qb.Parameters.Add(MissedOrderTable.PatientGenderColumn.ColumnName, order.PatientGender);
                    qb.Parameters.Add(MissedOrderTable.ProductIdColumn.ColumnName, saleitem.Id);
                    qb.Parameters.Add(MissedOrderTable.QuantityColumn.ColumnName, saleitem.Quantity);
                    qb.Parameters.Add(MissedOrderTable.SellingPriceColumn.ColumnName, saleitem.SellingPrice);
                    qb.Parameters.Add(MissedOrderTable.CreatedByColumn.ColumnName, order.CreatedBy);
                    qb.Parameters.Add(MissedOrderTable.UpdatedByColumn.ColumnName, order.UpdatedBy);
                    qb.Parameters.Add(MissedOrderTable.CreatedAtColumn.ColumnName, CustomDateTime.IST);
                    qb.Parameters.Add(MissedOrderTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                    await QueryExecuter.NonQueryAsyc(qb);
                    //order.ProductId = saleitem.Id;
                    //order.Quantity = saleitem.Quantity;
                    //order.SellingPrice = saleitem.SellingPrice;
                    //await Insert(order, MissedOrderTable.Table);
                }
            }
            return order;
        }
        // The below method name changed and the same method created to convert from direct qry to sp by Poongodi on 23/02/2017
        public async Task<IEnumerable<Sales>> List_Old(Sales data, string InstanceId, string AccountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.IdColumn, SalesTable.InvoiceSeriesColumn, SalesTable.InvoiceNoColumn, SalesTable.InvoiceDateColumn, SalesTable.CreatedAtColumn,
SalesTable.NameColumn, SalesTable.EmailColumn, SalesTable.MobileColumn, SalesTable.FileNameColumn, SalesTable.DiscountColumn, SalesTable.CreditColumn, SalesTable.DoctorNameColumn, SalesTable.DoctorMobileColumn, SalesTable.CancelstatusColumn, SalesTable.CashTypeColumn);
            // Added Gavaskar 13-02-2017 one Field SalesTable.CashTypeColumn
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            var result = await List<Sales>(qb);
            //cancel
            var query = result.GroupBy(x => x.Id).Select(y => y.FirstOrDefault());
            var si = await SqlQueryList(query, InstanceId, AccountId);
            var SIA = await SqlQueryAuditList(query);
            //var si = await SqlQueryList(result);           
            //var sales = result.Select(x =>
            //{
            //    x.SalesItem = si.Where(y => y.SalesId == x.Id).ToList();
            //    return x;
            //});
            var sales = query.Select(x =>
            {
                x.SalesItem = si.Where(y => y.SalesId == x.Id).ToList();
                x.SalesItemAudit = SIA.Where(y => y.SalesId == x.Id).ToList();
                return x;
            });
            return sales;
        }
        /*Start here*/
        // sales history Section optimized(Method separated Binding for Sales and SalItem)  by Violet on 10/05/17
        public async Task<IEnumerable<Sales>> List(Sales data, string InstanceId, string AccountId)
        {
            var Pageno = (data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("PatientId", data.PatientId);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            parms.Add("fromDate", data.Values);
            parms.Add("ToDate", data.SelectValue);
            parms.Add("PageNo", Pageno);
            parms.Add("PageSize", data.Page.PageSize);
            var SalesHistory = await sqldb.ExecuteProcedureAsync<Sales>("usp_get_sale_history", parms);
            if (SalesHistory.Count() <= 0)
            { return null; }

            var Sales = SalesHistory.Select(async (x) =>
            {

                if (x.PaymentType == "Cheque")
                {
                    x.isChequeRecieved = true;
                }
                else
                {
                    x.isChequeRecieved = await isChequeRecieved(x.Id);
                }
                return x;
            });
            return await Task.WhenAll(Sales);
        }

        //New method created to get sale item details based on saleid by Violet on 10/05/17
        public async Task<Sales> getList(Sales data, string InstanceId, string AccountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms = new Dictionary<string, object>();
            parms.Add("InstanceId", InstanceId);
            parms.Add("AccountId", AccountId);
            parms.Add("SaleId", data.SalesID);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            parms.Add("fromDate", data.Values);
            parms.Add("ToDate", data.SelectValue);
            var saleItem_Result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_get_saleItem_Details", parms);
            var salesItem = saleItem_Result.Where(x => x.SaleItemType == 1).OrderBy(x => x.SalesItemSno).ToList();

            var salesItemsHistory = salesItem.Select(x =>
            {
                var salesItems = new SalesItem
                {
                    Id = x.Id,
                    SalesId = x.SalesId,
                    Discount = x.Discount,
                    Quantity = x.Qty,
                    SellingPrice = x.SellingPrice,
                    MRP = x.MRP,
                    //VATValue = x.VATValue,

                    ProductStock =
                {
                              VAT = x.Vat as decimal? ?? 0 ,
                              Product = { Name   = x.ProductName.ToString() },
                              BatchNo =x.BatchNo,
                              ExpireDate =x.ExpireDate,
                              //add by nandhini for sales history gst
                                GstTotal = x.GstTotal as decimal? ?? 0,
                                Igst=x.Igst as decimal? ?? 0,
                                Cgst=x.Cgst as decimal? ?? 0,
                                Sgst=x.Sgst as decimal? ?? 0,
                              //end
                },

                };
                return salesItems;
            });
            var ItemAudit = (saleItem_Result.Where(x => x.SaleItemType == 2).OrderBy(x => x.CreatedAt).ToList());
            var salesItemAudit = ItemAudit.Select(x =>
            {
                var salesItemAudits = new SalesItemAudit
                {
                    UpdatedByUser = x.HQueUserName,
                    Discount = x.Discount,
                    Quantity = x.Qty,
                    SellingPrice = x.SellingPrice,
                    MRP = x.MRP,
                    SalesId = x.SalesId,
                    CreatedAt = x.CreatedAt,
                    Action = x.Action,
                    //VATValue = x.VATValue,
                    ProductStock =
              {

                                VAT = x.Vat as decimal? ?? 0,
                                Product = { Name   = x.ProductName.ToString() },
                                BatchNo =x.BatchNo,
                                ExpireDate =x.ExpireDate,
                                 //add by nandhini for sales history gst
                                GstTotal = x.GstTotal as decimal? ?? 0,
                                Igst=x.Igst as decimal? ?? 0,
                                Cgst=x.Cgst as decimal? ?? 0,
                                Sgst=x.Sgst as decimal? ?? 0,
                              //end
              }
                };
                return salesItemAudits;
            });
            var RetutnItem = (saleItem_Result.Where(x => x.SaleItemType == 3).OrderBy(x => x.CreatedAt).ToList());
            var salesRetrunItem = RetutnItem.Select(x =>
            {
                var salesRetrunItems = new SalesReturnItem
                {
                    CreatedBy = x.HQueUserName,
                    Discount = x.Discount,
                    Quantity = x.Qty,
                    SellingPrice = x.SellingPrice,
                    saleid = x.SalesId,
                    //VATValue = x.VATValue,
                    SalesReturn =
                     {
                            SalesId = x.SalesId,
                     },
                    CreatedAt = x.CreatedAt,
                    ProductStock =
               {

                                VAT = x.Vat as decimal? ?? 0,
                                Product = { Name   = x.ProductName.ToString() },
                                BatchNo =x.BatchNo,
                                ExpireDate =x.ExpireDate,
                                 //add by nandhini for sales history gst
                                GstTotal = x.GstTotal as decimal? ?? 0,
                                Igst=x.Igst as decimal? ?? 0,
                                Cgst=x.Cgst as decimal? ?? 0,
                                Sgst=x.Sgst as decimal? ?? 0,
                              //end
               }




                };
                return salesRetrunItems;
            });

            var ReturnItemHistory = (saleItem_Result.Where(x => x.SaleItemType == 4).OrderBy(x => x.CreatedAt).ToList());
            var salesReturnItemHistory = ReturnItemHistory.Select(x =>
            {
                var salesRetrunItems1 = new SalesReturnItem
                {
                    CreatedBy = x.HQueUserName,
                    Discount = x.Discount,
                    Quantity = x.Qty,
                    SellingPrice = x.SellingPrice,
                    saleid = x.SalesId,
                    MRP = x.MRP,
                    //VATValue = x.VATValue,
                    SalesReturn =
                     {
                            SalesId = x.SalesId,
                     },
                    CreatedAt = x.CreatedAt,
                    ProductStock =
               {

                                VAT = x.Vat as decimal? ?? 0,
                                Product = { Name   = x.ProductName.ToString() },
                                BatchNo =x.BatchNo,
                                ExpireDate =x.ExpireDate,
                                 //add by nandhini for sales history gst
                                GstTotal = x.GstTotal as decimal? ?? 0,
                                Igst=x.Igst as decimal? ?? 0,
                                Cgst=x.Cgst as decimal? ?? 0,
                                Sgst=x.Sgst as decimal? ?? 0,
                              //end
               }

                };
                return salesRetrunItems1;
            });

            var Sales = new Sales();

            Sales.SalesReturnItemHistory = salesReturnItemHistory.Where(y => y.saleid == data.SalesID).ToList();
            Sales.SalesReturnItem = salesRetrunItem.Where(y => y.saleid == data.SalesID).ToList();
            Sales.SalesItem = salesItemsHistory.Where(y => y.SalesId == data.SalesID).ToList();
            Sales.SalesItemAudit = salesItemAudit.Where(y => y.SalesId == data.SalesID).ToList();


            return Sales;
        }
        // End here

        public async Task<bool> isChequeRecieved(string salesid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CustomerPaymentTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(CustomerPaymentTable.SalesIdColumn, salesid);
            qb.SelectBuilder.GroupBy(CustomerPaymentTable.SalesIdColumn);
            qb.AddColumn(DbColumn.SumColumn("Debit", CustomerPaymentTable.DebitColumn));
            var data = (await List<CustomerPayment>(qb)).FirstOrDefault();
            if (data == null)
            {
                return false;
            }
            if (data.Debit == 0)
            {
                return false;
            }
            return true;
        }
        public async Task<List<Sales>> GetPatientList(string data, string accountid, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            if (!string.IsNullOrEmpty(data))
            {
                qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data);
            }
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, accountid);
            //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            //qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, instanceid);
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(PatientTable.NameColumn);
            var list = new List<Sales>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var Sales = new Sales
                    {
                        Name = reader[PatientTable.NameColumn.ColumnName].ToString().ToUpper(),
                        Mobile = reader[PatientTable.MobileColumn.ColumnName].ToString(),
                        PatientId = reader[PatientTable.IdColumn.ColumnName].ToString(),
                    };
                    list.Add(Sales);
                }
            }
            //var genGroupBy = list.GroupBy(x => new { x.Name }).Select(group => group.First());
            return list;
        }

        //Added by Sarubala to update stock GST if it is 0 - start

        public async Task<Sales> updateProductStockGST(Sales data)
        {
            foreach (var item in data.SalesItem)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
                //qb.AddColumn(ProductStockTable.GstTotalColumn);
                qb.ConditionBuilder.And(ProductStockTable.IdColumn, item.ProductStockId);
                var productstock = (await List<ProductStock>(qb)).FirstOrDefault();
                if (productstock != null)
                {
                    item.ProductStock = productstock;
                    var gstTotal = productstock.GstTotal;

                    if ((gstTotal == 0 || gstTotal == null) && (gstTotal != item.GstTotal))
                    {
                        item.Cgst = item.GstTotal / 2;
                        item.Sgst = item.GstTotal / 2;
                        item.Igst = item.GstTotal;
                        var qbTemp = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                        qbTemp.AddColumn(ProductStockTable.GstTotalColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.IgstColumn, ProductStockTable.UpdatedAtColumn, ProductStockTable.UpdatedByColumn);
                        qbTemp.ConditionBuilder.And(ProductStockTable.IdColumn, item.ProductStockId);
                        qbTemp.Parameters.Add(ProductStockTable.GstTotalColumn.ColumnName, item.GstTotal);
                        qbTemp.Parameters.Add(ProductStockTable.CgstColumn.ColumnName, item.Cgst);
                        qbTemp.Parameters.Add(ProductStockTable.SgstColumn.ColumnName, item.Sgst);
                        qbTemp.Parameters.Add(ProductStockTable.IgstColumn.ColumnName, item.Igst);
                        qbTemp.Parameters.Add(ProductStockTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                        qbTemp.Parameters.Add(ProductStockTable.UpdatedByColumn.ColumnName, data.UpdatedBy);

                        await QueryExecuter.NonQueryAsyc(qbTemp);

                    }
                }
            }
            return data;
        }


        //Added by Sarubala to update stock GST if it is 0 - end


        private async Task<List<SalesReturn>> SqlQueryCancelList(IEnumerable<Sales> data)
        {
            var siqb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Select);
            siqb.AddColumn(SalesReturnTable.Table.ColumnList.ToArray());
            siqb.JoinBuilder.Join(SalesReturnItemTable.Table, SalesReturnItemTable.SalesReturnIdColumn, SalesReturnTable.IdColumn);
            siqb.JoinBuilder.AddJoinColumn(SalesReturnItemTable.Table, SalesReturnItemTable.Table.ColumnList.ToArray());
            var i = 1;
            foreach (var salese in data)
            {
                var paramName = $"s{i++}";
                var caol = new CriteriaColumn(SalesReturnTable.SalesIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(caol);
                siqb.ConditionBuilder.Or(cc);
                siqb.Parameters.Add(paramName, salese.Id);
            }
            var list = new List<SalesReturn>();
            using (var reader = await QueryExecuter.QueryAsyc(siqb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesReturn
                    {
                        SalesId = reader[SalesReturnTable.SalesIdColumn.ColumnName].ToString()
                    };
                    list.Add(salesItem);
                }
            }
            return list;
        }
        private async Task<List<SalesItem>> SqlQueryList(IEnumerable<Sales> data, string InstanceId, string AccountId)
        {
            var siqb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);
            siqb.AddColumn(SalesItemTable.Table.ColumnList.ToArray());
            siqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesItemTable.ProductStockIdColumn);
            siqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            siqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            siqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            siqb.ConditionBuilder.And(SalesItemTable.InstanceIdColumn, InstanceId);
            siqb.ConditionBuilder.And(SalesItemTable.AccountIdColumn, AccountId);
            var i = 1;
            foreach (var salese in data)
            {
                var paramName = $"s{i++}";
                var caol = new CriteriaColumn(SalesItemTable.SalesIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(caol);
                siqb.ConditionBuilder.Or(cc);
                siqb.Parameters.Add(paramName, salese.Id);
            }
            siqb.SelectBuilder.SortBy(SalesItemTable.CreatedAtColumn);
            var list = new List<SalesItem>();
            using (var reader = await QueryExecuter.QueryAsyc(siqb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesItem().Fill(reader);
                    salesItem.ProductStock.Fill(reader)
                        .Product.Fill(reader);
                    list.Add(salesItem);
                }
            }
            return list;
        }
        public async Task<List<SalesItemAudit>> SqlQueryAuditList(IEnumerable<Sales> data)
        {
            var siaqb = QueryBuilderFactory.GetQueryBuilder(SalesItemAuditTable.Table, OperationType.Select);
            siaqb.AddColumn(SalesItemAuditTable.Table.ColumnList.ToArray());
            siaqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesItemAuditTable.ProductStockIdColumn);
            siaqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            siaqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            siaqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            siaqb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, SalesItemAuditTable.UpdatedByColumn);
            siaqb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);
            var i = 1;
            foreach (var salese in data)
            {
                var paramName = $"s{i++}";
                var caol = new CriteriaColumn(SalesItemAuditTable.SalesIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(caol);
                siaqb.ConditionBuilder.Or(cc);
                siaqb.Parameters.Add(paramName, salese.Id);
            }
            siaqb.SelectBuilder.SortByDesc(SalesItemAuditTable.CreatedAtColumn);
            var list = new List<SalesItemAudit>();
            using (var reader = await QueryExecuter.QueryAsyc(siaqb))
            {
                while (reader.Read())
                {
                    //var SalesItemAudit = new SalesItemAudit
                    //{
                    //    SalesId = reader[SalesItemAuditTable.SalesIdColumn.ColumnName].ToString(),
                    //    Discount = reader[SalesItemAuditTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
                    //    Quantity = reader[SalesItemAuditTable.QuantityColumn.FullColumnName] as decimal? ?? 0,
                    //    UpdatedByUser = reader[HQueUserTable.NameColumn.FullColumnName].ToString(),
                    //};
                    //SalesItemAudit.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    //SalesItemAudit.ProductStock.VAT = Convert.ToDecimal(reader[ProductStockTable.VATColumn.FullColumnName]);
                    //SalesItemAudit.ProductStock.SellingPrice = Convert.ToDecimal(reader[ProductStockTable.SellingPriceColumn.FullColumnName]);
                    //SalesItemAudit.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    //SalesItemAudit.ProductStock.ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName]);
                    //SalesItemAudit.ProductStock.CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0;
                    //SalesItemAudit.CreatedAt  = Convert.ToDateTime(reader[SalesItemAuditTable.CreatedAtColumn.ColumnName]);
                    //list.Add(SalesItemAudit);
                    var salesItemAudit = new SalesItemAudit().Fill(reader);
                    salesItemAudit.UpdatedByUser = reader[HQueUserTable.NameColumn.FullColumnName].ToString();
                    salesItemAudit.ProductStock.Fill(reader)
                        .Product.Fill(reader);
                    list.Add(salesItemAudit);
                }
            }
            return list;
        }
        public async Task<int> Count_old(Sales data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<int> Count(Sales data)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            parms.Add("fromDate", data.Values);
            parms.Add("ToDate", data.SelectValue);
            var SalesHistory = await sqldb.ExecuteProcedureAsync<Sales>("usp_get_sale_history_Count", parms);
            return SalesHistory.FirstOrDefault().SalesCount;
        }

        public async Task<int> CustomerHistoryInvoiceCount(Sales data)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", data.InstanceId);
            parms.Add("AccountId", data.AccountId);
            //parms.Add("CustomerName", data.Name);
            //parms.Add("CustomerMobile", data.Mobile);
            parms.Add("CustomerId", data.PatientId);

            var SalesHistory = await sqldb.ExecuteProcedureAsync<Sales>("usp_get_sale_customer_history_Count", parms);
            return SalesHistory.FirstOrDefault().SalesCount;
        }
        public async Task<string> GetCustomInvoiceNo(string instanceId, string invoiceSeries, DateTime invoiceDate, string sPrefix)
        {
            /*var invoiceSeriesQuery = string.Empty;
            if (!string.IsNullOrEmpty(invoiceSeries))
            {
                invoiceSeriesQuery = $" and InvoiceSeries='{invoiceSeries}'";
            }
            else
            {
                invoiceSeries = string.Empty;
            }
            var query = _configHelper.AppConfig.IsSqlServer
            ? $"SELECT ISNULL(MAX(CONVERT(INT, InvoiceNo)),0) + 1 AS InvoiceNo FROM Sales WHERE InstanceId = '{instanceId}' and InvoiceSeries='{invoiceSeries}'"
            : $"SELECT IFNULL(MAX(CAST(InvoiceNo as INT)),0) + 1 AS InvoiceNo FROM Sales WHERE InstanceId = '{instanceId}' and InvoiceSeries='{invoiceSeries}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return (await QueryExecuter.SingleValueAsyc(qb)).ToString(); */
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.Sales, invoiceDate, instanceId, "", invoiceSeries, sPrefix);
            return sBillNo.ToString();
        }
        public async Task<string> GetInvoiceNo(string instanceId)
        {
            /*Prefix added by Poongodi on 10/10/2017*/
            string sPrefix = "";
            if (Convert.ToString(user.OfflineInstalled()).ToLower() == "true" && _configHelper.AppConfig.OfflineMode == false)
            {
                sPrefix = "O";
            }
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.Sales, CustomDateTime.IST, instanceId, "", "", sPrefix);
            return sBillNo.ToString();
        }
        #region
        //Added by Manivannan for retrieving the Invoice No in sales screen on 30-Jan-2017 begins here
        public async Task<string> GetNumericInvoiceNo(string instanceId)
        {
            string sPrefix = "";
            if (Convert.ToString(user.OfflineInstalled()).ToLower() == "true" && _configHelper.AppConfig.OfflineMode == false)
            {
                sPrefix = "O";
            }
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.Sales, CustomDateTime.IST, instanceId, "", "", sPrefix);
            return sBillNo.ToString();
        }
        public async Task<string> GetFinYear(Sales v)
        {
            DateTime InvoiceDate = v.InvoiceDate as DateTime? ?? CustomDateTime.IST;
            string sFinyr = await _HelperDataAccess.GetFinyear(InvoiceDate, v.InstanceId, v.AccountId);
            return Convert.ToString(sFinyr);
        }
        public async Task<string> GetCustomInvoiceNum(string instanceId, string customSeries)
        {
            string sPrefix = "";
            if (Convert.ToString(user.OfflineInstalled()).ToLower() == "true" && _configHelper.AppConfig.OfflineMode == false)
            {
                sPrefix = "O";
            }
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.Sales, CustomDateTime.IST, instanceId, "", customSeries, sPrefix);
            return sBillNo.ToString();
        }
        //Added by Manivannan for retrieving the Invoice No in sales screen on 30-Jan-2017 ends here
        #endregion
        /// <summary>
        /// fromTable added for fetch the value form Patient Table
        /// </summary>
        /// <param name="data"></param>
        /// <param name="qb"></param>
        /// <param name="fromTable"></param>
        /// <returns></returns>
        private static QueryBuilderBase SqlQueryBuilder(Sales data, QueryBuilderBase qb, string fromTable = "")
        {

            // Newly Added Gavaskar 09-11-2016 Start 
            if (!string.IsNullOrEmpty(data.PatientId))
            {
                qb.ConditionBuilder.And(SalesTable.PatientIdColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(SalesTable.PatientIdColumn.ColumnName, data.PatientId);
            }
            else
            {
                if (!string.IsNullOrEmpty(data.Name) && fromTable == "")
                {
                    qb.ConditionBuilder.And(SalesTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(SalesTable.NameColumn.ColumnName, data.Name);
                }
                else if (!string.IsNullOrEmpty(data.Name) && fromTable == "frmPatient")
                {
                    qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data.Name);
                }
                // Newly Added Gavaskar 09-11-2016 End
                if (!string.IsNullOrEmpty(data.Mobile) && fromTable == "")
                {
                    qb.ConditionBuilder.And(SalesTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(SalesTable.MobileColumn.ColumnName, data.Mobile);
                }
                else if (!string.IsNullOrEmpty(data.Mobile) && fromTable == "frmPatient")
                {
                    qb.ConditionBuilder.And(PatientTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, data.Mobile);
                }
                if (!string.IsNullOrEmpty(data.Email) && fromTable == "")
                {
                    qb.ConditionBuilder.And(SalesTable.EmailColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(SalesTable.EmailColumn.ColumnName, data.Email);
                }
                else if (!string.IsNullOrEmpty(data.Email) && fromTable == "frmPatient")
                {
                    qb.ConditionBuilder.And(PatientTable.EmailColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                    qb.Parameters.Add(PatientTable.EmailColumn.ColumnName, data.Email);
                }
            }

            if (data.Credit != null)
            {
                qb.ConditionBuilder.And(SalesTable.CreditColumn, data.Credit);
            }
            if (data.InvoiceDate != null)
            {
                qb.ConditionBuilder.And(SalesTable.InvoiceDateColumn, data.InvoiceDate);
            }
            if (!string.IsNullOrEmpty(data.InvoiceNo))
            {
                qb.ConditionBuilder.And(SalesTable.InvoiceNoColumn, data.InvoiceNo);

            }
            if (!string.IsNullOrEmpty(data.Select) && !string.IsNullOrEmpty(data.Values))
            {
                SalesExtendedQuery.BuildExtendedSalesQuery(data, qb);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId) && fromTable == "")
            {
                qb.ConditionBuilder.And(SalesTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, data.InstanceId);
            }
            else if (!string.IsNullOrEmpty(data.AccountId) /*&& !string.IsNullOrEmpty(data.InstanceId)*/ && fromTable == "frmPatient")
            {
                qb.ConditionBuilder.And(PatientTable.AccountIdColumn, data.AccountId);
                // qb.ConditionBuilder.And(PatientTable.InstanceIdColumn, data.InstanceId); // Commented by Violet
            }
            if (data.Patient.Status != null)
            {
                qb.ConditionBuilder.And(PatientTable.StatusColumn, CriteriaEquation.Equal);
                qb.Parameters.Add(PatientTable.StatusColumn.ColumnName, data.Patient.Status);
            }

            return qb;
        }
        private static QueryBuilderBase SqlCustomerQueryBuilder(Sales data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data.Name);
            }
            if (!string.IsNullOrEmpty(data.Mobile))
            {
                qb.ConditionBuilder.And(PatientTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, data.Mobile);
            }
            if (!string.IsNullOrEmpty(data.Email))
            {
                qb.ConditionBuilder.And(PatientTable.EmailColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.EmailColumn.ColumnName, data.Email);
            }
            if (!string.IsNullOrEmpty(data.AccountId) /*&& !string.IsNullOrEmpty(data.InstanceId)*/)
            {
                qb.ConditionBuilder.And(PatientTable.AccountIdColumn, data.AccountId);
                //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn, data.InstanceId);
            }
            return qb;
        }
        public async Task<Sales> SalesDetail(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SalesTable.IdColumn);
            qb.Parameters.Add(SalesTable.IdColumn.ColumnName, id);
            qb.AddColumn(SalesTable.IdColumn, SalesTable.InstanceIdColumn, SalesTable.InvoiceNoColumn,
                SalesTable.InvoiceDateColumn, SalesTable.DiscountColumn, SalesTable.DiscountValueColumn, SalesTable.PaymentTypeColumn,
                SalesTable.DeliveryTypeColumn, SalesTable.SendSmsColumn, SalesTable.SendEmailColumn,
                SalesTable.NameColumn, SalesTable.AgeColumn, SalesTable.GenderColumn, SalesTable.MobileColumn,
                SalesTable.DoctorNameColumn, SalesTable.InvoiceSeriesColumn, SalesTable.PrefixColumn, SalesTable.TaxRefNoColumn, SalesTable.PatientIdColumn, SalesTable.CreatedAtColumn, SalesTable.RoundoffNetAmountColumn, SalesTable.NetAmountColumn,
                SalesTable.SalesItemAmountColumn, SalesTable.LoyaltyIdColumn,SalesTable.RedeemPtsColumn);
            qb.JoinBuilder.Join(InstanceTable.Table, InstanceTable.IdColumn, SalesTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.GstselectColumn);
            var sales = (await List<Sales>(qb)).FirstOrDefault();
            sales.Discount = sales.Discount == null ? 0 : sales.Discount;
            return sales;
        }
        //Same method rename by Old and New method created by Poongodi on 27/07/2017
        public async Task<Sales> GetById(string id)
        {

            var qb = QueryBuilderFactory.GetQueryBuilder("usp_get_SaleById");
            qb.Parameters.Add("@AccountID", user.AccountId());
            qb.Parameters.Add("@InstanceID", user.InstanceId());
            qb.Parameters.Add("@SaleId", id);


            var list = new List<Sales>();
            var ReturnItemList = new List<SalesReturnItem>();
            decimal dReturn = await GetSalesReturnValue(id); /*Added by Poongodi on 06/04/17*/
            decimal returnLoyaltyPts = await GetSalesReturnLoyaltyPts(id); /*Added by Sarubala on 28/06/18*/
            using (var reader = await QueryExecuter.ExecuteProcAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesItem().Fill(reader);
                    salesItem.ProductStock.Fill(reader).Product.Fill(reader);
                    salesItem.MRPPerStrip = (salesItem.MRP) * (salesItem.ProductStock.PackageSize);
                    salesItem.SellingPricePerStrip = (salesItem.SellingPrice) * (salesItem.ProductStock.PackageSize);
                    salesItem.CgstValue = salesItem.CgstValue;
                    salesItem.SgstValue = salesItem.SgstValue;
                    salesItem.IgstValue = salesItem.IgstValue;
                    salesItem.GstTotalValue = salesItem.CgstValue + salesItem.SgstValue;

                    //Added by Sarubala on 10/05/18 to include custom template fields - start
                    salesItem.MRPAfterDiscount = salesItem.MRP - (salesItem.MRP * salesItem.Discount / 100);
                    salesItem.SellingPriceAfterDiscount = salesItem.SellingPrice - (salesItem.SellingPrice * salesItem.Discount / 100);
                    salesItem.MRPValueAfterDiscount = (salesItem.MRP - (salesItem.MRP * salesItem.Discount / 100)) * salesItem.Quantity;
                    salesItem.SellingPriceValueAfterDiscount = (salesItem.SellingPrice - (salesItem.SellingPrice * salesItem.Discount / 100)) * salesItem.Quantity;
                    //Added by Sarubala on 10/05/18 to include custom template fields - end

                    salesItem.salesReturn = false;
                    var sales = new Sales().Fill(reader);
                    salesItem.NetAmount = sales.NetAmount;
                    sales.ReturnAmount = dReturn;
                    sales.Instance.Fill(reader);
                    //for(int i =0; i<  )
                    // sales.SavingAmount = (salesItem.MRPValueAfterDiscount - salesItem.SellingPriceValueAfterDiscount);
                    sales.NetAmount = sales.NetAmount;

                    sales.AmountinWords = ConvertNumbertoWords(Math.Round((decimal)sales.NetAmount, 2).ToString());
                    sales.Patient.GsTin = reader[PatientTable.GsTinColumn.FullColumnName].ToString() ?? "";
                    sales.Patient.DrugLicenseNo = reader[PatientTable.DrugLicenseNoColumn.FullColumnName].ToString() ?? "";
                    sales.Patient.Pan = reader[PatientTable.PanColumn.FullColumnName].ToString() ?? "";
                    sales.Patient.ShortName = reader[PatientTable.ShortNameColumn.FullColumnName].ToString() ?? "";
                    sales.Patient.City = reader[PatientTable.CityColumn.FullColumnName].ToString() ?? "";
                    sales.Patient.EmpID = reader[PatientTable.EmpIDColumn.FullColumnName].ToString() ?? "";
                    sales.HQueUser.Name = reader[HQueUserTable.NameColumn.FullColumnName].ToString() ?? "";
                    salesItem.ProductStock.Product.RackNo = reader[ProductInstanceTable.RackNoColumn.FullColumnName].ToString() ?? ProductTable.RackNoColumn.FullColumnName.ToString();
                    salesItem.ProductStock.Product.BoxNo = reader[ProductInstanceTable.BoxNoColumn.FullColumnName].ToString() ?? string.Empty;
                    salesItem.ProductStock.MRP = Convert.ToDecimal(reader[ProductStockTable.MRPColumn.FullColumnName]) > 0 ? Convert.ToDecimal(reader[ProductStockTable.MRPColumn.FullColumnName]) : salesItem.MRP;

                    sales.SalesItem.Add(salesItem);
                    list.Add(sales);
                }
            }
            var sale = list.FirstOrDefault();
            ReturnItemList = SalesReturnItemList(sale).Result;
            int i = 0;

            sale.ReturnAmount = dReturn; //Return value assigned to sales object
            if (sale == null)
                return new Sales();
            sale.SalesItem = list.SelectMany(x => x.SalesItem).ToList();

            //Code to convert sales return into salesItems for edit ReturnItems along with sale - Sarubala

            var slReturn = await getReturnItemsAlongWithSale(id);
            if (slReturn != null)
            {
                sale.SalesReturn = slReturn;
            }

            if (sale.SalesReturn != null && sale.SalesReturn.SalesReturnItem.Count > 0)
            {
                if ((bool)sale.SalesReturn.IsAlongWithSale)
                {
                    foreach (SalesReturnItem sri in sale.SalesReturn.SalesReturnItem)
                    {
                        SalesItem si = new SalesItem();
                        si.salesReturnId = sri.SalesReturnId;
                        si.salesReturn = true;
                        si.Id = sri.Id;
                        si.ProductStockId = sri.ProductStockId;
                        si.Quantity = sri.Quantity * (0); //si.Quantity = sri.Quantity * (-1); // _1 to 0
                        si.ReturnedQty = sri.Quantity * (-1);
                        si.Discount = sri.Discount;
                        si.DiscountAmount = sri.DiscountAmount;
                        si.ProductStock = sri.ProductStock;
                        si.SellingPrice = sri.MrpSellingPrice > 0 ? sri.MrpSellingPrice : sri.ProductStock.SellingPrice;
                        si.ReminderFrequency = sale.SalesItem.First().ReminderFrequency;
                        si.MRPAfterDiscount = sri.MRPAfterDiscount * (-1);
                        si.SellingPriceAfterDiscount = sri.SellingPriceAfterDiscount * (-1);
                        si.MRPValueAfterDiscount = sri.MRPValueAfterDiscount * (-1);
                        si.SellingPriceValueAfterDiscount = sri.SellingPriceValueAfterDiscount * (-1);
                        si.GstTotal = 0;
                        si.ProductStock.MRP = sri.ProductStock.MRP;
                        sale.SalesItem.Add(si);

                        var salesReturnItem = new SalesReturnItem
                        {
                            ProductStock = sri.ProductStock,
                            ProductStockId = sri.ProductStock.Id
                        };

                        var product = await _productDataAccess.GetProductById(sri.ProductStock.ProductId);
                        salesReturnItem.ProductStock.Product.Name = product.Name;
                        salesReturnItem.Total = sri.Total;
                        salesReturnItem.PurchaseQuantity = sri.Quantity;
                        salesReturnItem.SellingPrice = sri.MrpSellingPrice > 0 ? sri.MrpSellingPrice : sri.SellingPrice;
                        salesReturnItem.MrpSellingPrice = sri.MrpSellingPrice;
                        salesReturnItem.MRP = sri.MRP;
                        salesReturnItem.Cgst = sri.Cgst;
                        salesReturnItem.Sgst = sri.Sgst;
                        salesReturnItem.Igst = sri.Igst;
                        salesReturnItem.GstTotal = sri.GstTotal;
                        if (sri.DiscountAmount != null)
                            salesReturnItem.DiscountAmount = sri.DiscountAmount;
                        else
                            salesReturnItem.DiscountAmount = 0;
                        if (sri.Discount != null)
                            salesReturnItem.Discount = sri.Discount;
                        else
                            salesReturnItem.Discount = 0;

                        bool Rntflag = true;
                        foreach (var returnItem in ReturnItemList)
                        {
                            if (salesReturnItem.ReturnedQuantity == null)
                                salesReturnItem.ReturnedQuantity = 0;

                            if (salesReturnItem.ReturnAmt == null)
                                salesReturnItem.ReturnAmt = 0;

                            if (sale.Discount > 0)
                                returnItem.Discount = sale.Discount;
                            else
                                returnItem.Discount = returnItem.Discount;

                            if (salesReturnItem.ProductStock.Id == returnItem.ProductStockId) //&& salesReturnItem.SellingPrice == returnItem.MrpSellingPrice
                            {
                                var RetAmount = (returnItem.Quantity * returnItem.MrpSellingPrice);
                                salesReturnItem.ReturnedQuantity += returnItem.Quantity;
                                salesReturnItem.Quantity = salesReturnItem.ReturnedQuantity;
                                salesReturnItem.ReturnAmt += RetAmount;
                                salesReturnItem.Discount = returnItem.Discount;
                                salesReturnItem.DiscountAmount = returnItem.DiscountAmount;
                                salesReturnItem.ProductStock.HsnCode = returnItem.HsnCode;
                                if (Rntflag == true)
                                {
                                    if ((salesReturnItem.Total.HasValue && salesReturnItem.Cgst.HasValue))
                                    {
                                        salesReturnItem.CgstValue = (((salesReturnItem.Total - salesReturnItem.DiscountAmount) - (((salesReturnItem.Total - salesReturnItem.DiscountAmount) * 100) / ((salesReturnItem.Cgst + salesReturnItem.Sgst) + 100)))) / 2;
                                        salesReturnItem.SgstValue = (((salesReturnItem.Total - salesReturnItem.DiscountAmount) - (((salesReturnItem.Total - salesReturnItem.DiscountAmount) * 100) / ((salesReturnItem.Cgst + salesReturnItem.Sgst) + 100)))) / 2;
                                        salesReturnItem.GstTotalValue = salesReturnItem.CgstValue + salesReturnItem.SgstValue;
                                    }
                                    else
                                    {
                                        salesReturnItem.IgstValue = (((salesReturnItem.Total - salesReturnItem.DiscountAmount) - (((salesReturnItem.Total - salesReturnItem.DiscountAmount) * 100) / ((salesReturnItem.Igst) + 100))));
                                        salesReturnItem.GstTotalValue = salesReturnItem.IgstValue;
                                    }
                                }
                            }
                            Rntflag = false;
                        }
                        salesReturnItem.MRPAfterDiscount = salesReturnItem.MRP - (salesReturnItem.MRP * salesReturnItem.Discount / 100);
                        salesReturnItem.SellingPriceAfterDiscount = salesReturnItem.MrpSellingPrice - (salesReturnItem.MrpSellingPrice * salesReturnItem.Discount / 100);
                        salesReturnItem.MRPValueAfterDiscount = (salesReturnItem.MRP - (salesReturnItem.MRP * salesReturnItem.Discount / 100)) * salesReturnItem.Quantity;
                        salesReturnItem.SellingPriceValueAfterDiscount = (salesReturnItem.MrpSellingPrice - (salesReturnItem.MrpSellingPrice * salesReturnItem.Discount / 100)) * salesReturnItem.Quantity;
                        sale.SalesReturnItem.Add(salesReturnItem);
                        i++;
                    }
                }
                sale.SalesReturn.IgstTotalValue = sale.SalesReturnItem.Sum(m => m.IgstValue);
                sale.SalesReturn.CgstTotalValue = sale.SalesReturnItem.Sum(m => m.CgstValue);
                sale.SalesReturn.SgstTotalValue = sale.SalesReturnItem.Sum(m => m.SgstValue);                

                if (sale.LoyaltyId == null && !string.IsNullOrEmpty(sale.SalesReturn.LoyaltyId))
                {
                    sale.LoyaltyId = sale.SalesReturn.LoyaltyId;
                }
            }
            sale.UserName = sale.HQueUser.Name;
            sale.LoyaltyPts = Convert.ToDecimal(sale.LoyaltyPts) - returnLoyaltyPts;
            //End

            return sale;
        }
        public async Task<Sales> GetById_old(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SalesTable.IdColumn);
            qb.Parameters.Add(SalesTable.IdColumn.ColumnName, id);
            qb.AddColumn(SalesTable.AccountIdColumn, SalesTable.InstanceIdColumn, SalesTable.TaxRefNoColumn, SalesTable.DoctorIdColumn);
            qb.AddColumn(SalesItemTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(SalesTable.Table, SalesTable.IdColumn, SalesItemTable.SalesIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesTable.Table, SalesTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(InstanceTable.Table, InstanceTable.IdColumn, SalesTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.Table.ColumnList.ToArray());
            //For UserName
            qb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, SalesTable.CreatedByColumn);
            qb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);
            //For RackNo - Added by Santhiyagu 03-04-2017
            // For RackNo  - Changed by Violet 13-04-2017 
            qb.JoinBuilder.LeftJoins(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductTable.IdColumn, ProductInstanceTable.InstanceIdColumn, ProductStockTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductInstanceTable.Table, ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn); /*Box no added by Settu on 22/05/17*/
            //qb.ConditionBuilder.And(ProductInstanceTable.InstanceIdColumn);
            //qb.Parameters.Add(ProductInstanceTable.InstanceIdColumn.ColumnName, Instance);
            //qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);

            qb.SelectBuilder.SortBy(SalesItemTable.CreatedAtColumn);
            var list = new List<Sales>();
            var ReturnItemList = new List<SalesReturnItem>();
            decimal dReturn = await GetSalesReturnValue(id); /*Added by Poongodi on 06/04/17*/
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesItem().Fill(reader);
                    salesItem.ProductStock.Fill(reader).Product.Fill(reader);
                    salesItem.MRPPerStrip = (salesItem.MRP) * (salesItem.ProductStock.PackageSize);
                    salesItem.SellingPricePerStrip = (salesItem.SellingPrice) * (salesItem.ProductStock.PackageSize);
                    salesItem.CgstValue = salesItem.CgstValue;
                    salesItem.SgstValue = salesItem.SgstValue;
                    salesItem.IgstValue = salesItem.IgstValue;
                    salesItem.GstTotalValue = salesItem.CgstValue + salesItem.SgstValue;

                    salesItem.salesReturn = false;
                    var sales = new Sales().Fill(reader);

                    sales.ReturnAmount = dReturn;
                    sales.Instance.Fill(reader);
                    sales.HQueUser.Name = reader[HQueUserTable.NameColumn.FullColumnName].ToString() ?? "";
                    salesItem.ProductStock.Product.RackNo = reader[ProductInstanceTable.RackNoColumn.FullColumnName].ToString() ?? ProductTable.RackNoColumn.FullColumnName.ToString();
                    salesItem.ProductStock.Product.BoxNo = reader[ProductInstanceTable.BoxNoColumn.FullColumnName].ToString() ?? string.Empty;
                    sales.SalesItem.Add(salesItem);
                    list.Add(sales);
                }
            }
            var sale = list.FirstOrDefault();
            ReturnItemList = SalesReturnItemList(sale).Result;
            int i = 0;

            sale.ReturnAmount = dReturn; //Return value assigned to sales object
            if (sale == null)
                return new Sales();
            sale.SalesItem = list.SelectMany(x => x.SalesItem).ToList();

            //Code to convert sales return into salesItems for edit ReturnItems along with sale - Sarubala

            var slReturn = await getReturnItemsAlongWithSale(id);
            if (slReturn != null)
            {
                sale.SalesReturn = slReturn;
            }

            if (sale.SalesReturn != null && sale.SalesReturn.SalesReturnItem.Count > 0)
            {
                if ((bool)sale.SalesReturn.IsAlongWithSale)
                {
                    foreach (SalesReturnItem sri in sale.SalesReturn.SalesReturnItem)
                    {
                        SalesItem si = new SalesItem();
                        si.salesReturnId = sri.SalesReturnId;
                        si.salesReturn = true;
                        si.Id = sri.Id;
                        si.ProductStockId = sri.ProductStockId;
                        si.Quantity = sri.Quantity * (0); //si.Quantity = sri.Quantity * (-1); // _1 to 0
                        si.ReturnedQty = sri.Quantity * (-1);
                        si.Discount = sri.Discount;
                        si.ProductStock = sri.ProductStock;
                        si.SellingPrice = sri.MrpSellingPrice > 0 ? sri.MrpSellingPrice : sri.ProductStock.SellingPrice;
                        si.ReminderFrequency = sale.SalesItem.First().ReminderFrequency;

                        sale.SalesItem.Add(si);

                        var salesReturnItem = new SalesReturnItem
                        {
                            ProductStock = sri.ProductStock,
                            ProductStockId = sri.ProductStock.Id
                        };

                        var product = await _productDataAccess.GetProductById(sri.ProductStock.ProductId);
                        salesReturnItem.ProductStock.Product.Name = product.Name;
                        salesReturnItem.Total = sri.Total;
                        salesReturnItem.PurchaseQuantity = sri.Quantity;
                        salesReturnItem.SellingPrice = sri.MrpSellingPrice > 0 ? sri.MrpSellingPrice : sri.SellingPrice;
                        salesReturnItem.MrpSellingPrice = sri.MrpSellingPrice;
                        salesReturnItem.MRP = sri.MRP;
                        salesReturnItem.Cgst = sri.Cgst;
                        salesReturnItem.Sgst = sri.Sgst;
                        salesReturnItem.Igst = sri.Igst;
                        salesReturnItem.GstTotal = sri.GstTotal;
                        if (sri.Discount != null)
                            salesReturnItem.Discount = sri.Discount;
                        else
                            salesReturnItem.Discount = 0;

                        bool Rntflag = true;
                        foreach (var returnItem in ReturnItemList)
                        {
                            if (salesReturnItem.ReturnedQuantity == null)
                                salesReturnItem.ReturnedQuantity = 0;

                            if (salesReturnItem.ReturnAmt == null)
                                salesReturnItem.ReturnAmt = 0;

                            if (sale.Discount > 0)
                                returnItem.Discount = sale.Discount;
                            else
                                returnItem.Discount = returnItem.Discount;

                            if (salesReturnItem.ProductStock.Id == returnItem.ProductStockId) //&& salesReturnItem.SellingPrice == returnItem.MrpSellingPrice
                            {
                                var RetAmount = (returnItem.Quantity * returnItem.MrpSellingPrice);
                                salesReturnItem.ReturnedQuantity += returnItem.Quantity;
                                salesReturnItem.Quantity = salesReturnItem.ReturnedQuantity;
                                salesReturnItem.ReturnAmt += RetAmount;
                                salesReturnItem.Discount = returnItem.Discount;
                                if (Rntflag == true)
                                {
                                    if ((salesReturnItem.Total.HasValue && salesReturnItem.Cgst.HasValue))
                                    {
                                        salesReturnItem.CgstValue = (((salesReturnItem.Total - salesReturnItem.DiscountAmount) - (((salesReturnItem.Total - salesReturnItem.DiscountAmount) * 100) / ((salesReturnItem.Cgst + salesReturnItem.Sgst) + 100)))) / 2;
                                        salesReturnItem.SgstValue = (((salesReturnItem.Total - salesReturnItem.DiscountAmount) - (((salesReturnItem.Total - salesReturnItem.DiscountAmount) * 100) / ((salesReturnItem.Cgst + salesReturnItem.Sgst) + 100)))) / 2;
                                        salesReturnItem.GstTotalValue = salesReturnItem.CgstValue + salesReturnItem.SgstValue;
                                    }
                                    else
                                    {
                                        salesReturnItem.IgstValue = (((salesReturnItem.Total - salesReturnItem.DiscountAmount) - (((salesReturnItem.Total - salesReturnItem.DiscountAmount) * 100) / ((salesReturnItem.Igst) + 100))));
                                        salesReturnItem.GstTotalValue = salesReturnItem.IgstValue;
                                    }
                                }
                            }
                            Rntflag = false;
                        }
                        sale.SalesReturnItem.Add(salesReturnItem);
                        i++;
                    }
                }
                sale.SalesReturn.IgstTotalValue = sale.SalesReturnItem.Sum(m => m.IgstValue);
                sale.SalesReturn.CgstTotalValue = sale.SalesReturnItem.Sum(m => m.CgstValue);
                sale.SalesReturn.SgstTotalValue = sale.SalesReturnItem.Sum(m => m.SgstValue);
            }
            sale.UserName = sale.HQueUser.Name;
            //End

            return sale;
        }
        /// <summary>
        /// 
        /// Sales return value taken for sales bill print
        /// </summary>
        /// <param name="saleid"></param>
        /// <returns></returns>
        public async Task<decimal> GetSalesReturnValue(string saleid)
        {
            decimal dReturnValue = 0;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("saleid", saleid);
            var salesReturn = await sqldb.ExecuteProcedureAsync<dynamic>("USP_Get_SalesReturnValue", parms);
            if (salesReturn != null && salesReturn.Count() != 0)
            {
                dReturnValue = Convert.ToDecimal(salesReturn.FirstOrDefault().ReturnValue);
            }
            return dReturnValue;
        }

        //Code added by Sarubala on 28/06/18 - start
        public async Task<decimal> GetSalesReturnLoyaltyPts(string saleid)
        {
            decimal returnLoyaltyPt = 0;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("saleid", saleid);
            var salesReturn = await sqldb.ExecuteProcedureAsync<dynamic>("Usp_Get_SalesReturnLoyaltyPts", parms);
            if (salesReturn != null && salesReturn.Count() != 0)
            {
                returnLoyaltyPt = Convert.ToDecimal(salesReturn.FirstOrDefault().LoyaltyPts);
            }
            return returnLoyaltyPt;
        }
        //Code added by Sarubala on 28/06/18 - end

        //Code added by Sarubala for printing only Return in A4 - start
        public async Task<Sales> GetByReturnId(string salesreturnid)
        {
            /*Prefix Added by Poongodi on 15/06/2017*/
            var patientDetails = await GetPatientDetailsForSalesReturn(salesreturnid);
            var query = $@"select     sri.MrpSellingPrice as [SalesReturnItem.MrpSellingPrice], 
                                      sri.Quantity as [SalesReturnItem.Quantity],                           
                                      sri.MRP as [SalesReturnItem.MRP],
                                      sri.Igst as [SalesReturnItem.Igst], 
									   sri.Cgst as [SalesReturnItem.Cgst], 
									   sri.Sgst as [SalesReturnItem.Sgst],
									   sri.GstTotal as [SalesReturnItem.GstTotal], 
                                       isnull(sri.Discount,0) + isnull(s.Discount,0) as  [SalesReturnItem.Discount],
isnull(sri.DiscountAmount,0) as [SalesReturnItem.DiscountAmount],
						              isnull(sr.prefix,'') + isnull(sr.InvoiceSeries,'') + sr.ReturnNo as                     [SalesReturn.ReturnNo],
                                      sr.ReturnDate as [SalesReturn.ReturnDate], 
                                      sr.ReturnCharges as [SalesReturn.ReturnCharges],
                                      sr.CreatedAt as [SalesReturn.CreatedAt], 
                                      sr.TaxRefNo as [SalesReturn.TaxRefNo],
                                      sr.NetAmount as [SalesReturn.NetAmount],
                                      sr.RoundOffNetAmount as [SalesReturn.RoundOffNetAmount],
                                      sr.TotalDiscountValue as [SalesReturn.TotalDiscountValue],
                                      sr.ReturnItemAmount as [SalesReturn.ReturnItemAmount],
                                      sr.GstAmount as [SalesReturn.GstAmount],
						              ps.BatchNo as [ProductStock.BatchNo],
                                      ps.ExpireDate as [ProductStock.ExpireDate],
                                      ps.VAT as [ProductStock.VAT],
                                      ps.SellingPrice as [ProductStock.SellingPrice],
                                      ps.MRP as [ProductStock.MRP],
						              p.Name as [Product.Name],
                                      p.Manufacturer as [Product.Manufacturer],
						              i.Name as [Instance.Name] ,
                                      i.Phone as [Instance.Phone],
                                      i.DrugLicenseNo as [Instance.DrugLicenseNo],
                                      i.TinNo as [Instance.TinNo],
                                      i.GstinNo as [Instance.GsTinNo],
                                      i.Address as [Instance.Address],
                                      i.Area as [Instance.Area],
                                      i.City as [Instance.City],
                                      i.state as [Instance.state],
                                      i.pincode as [Instance.pincode],
                                      i.status as [Instance.status],
                                      i.Gstselect as [Instance.Gstselect],
                                      i.isUnionTerritory as [Instance.isUnionTerritory],
                                      h.Name [HQueUser.Name],
                                      s.Name [Sales.Name],
                                      pti.RackNo as [ProductInstance.RackNo],pti.BoxNo  as [ProductInstance.BoxNo]
                                      from salesreturnitem as sri WITH(NOLOCK) inner join salesreturn as sr WITH(NOLOCK) on sri.salesreturnid = sr.id
			                                     left join productstock as ps WITH(NOLOCK) on ps.id = sri.productstockid  
							         			 left join Product as p WITH(NOLOCK) on p.Id = ps.ProductId 							         			 
							         			 inner join Instance as i WITH(NOLOCK) on sr.InstanceId = i.Id 
                                                 inner join HQueUser h WITH(NOLOCK) on h.id=sr.CreatedBy
  											     left join Sales s WITH(NOLOCK) on s.id=sr.SalesId
                                                 left join ProductInstance as pti WITH(NOLOCK) on pti.ProductId =p.Id and pti.InstanceId=ps.InstanceId
							         			 where sr.id = '{salesreturnid}' and (sri.IsDeleted is null or sri.IsDeleted != 1) order by sri.CreatedAt";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var obj = new ReturnInvoice();
            var list = new List<ReturnInvoiceList>();
            decimal total = 0.00M;
            decimal discount = 0.00M;
            decimal roundOff = 0.00M;
            decimal netAmount = 0.00M;
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var returnInvoiceList = new ReturnInvoiceList()
                    {
                        salesReturnItemInvoice =
                        {
                            Quantity = reader[SalesReturnItemTable.QuantityColumn.FullColumnName] as decimal? ?? 0,
                            MrpSellingPrice = reader[SalesReturnItemTable.MrpSellingPriceColumn.FullColumnName] as decimal? ?? 0,
                            Discount = reader[SalesReturnItemTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
                             DiscountAmount = reader[SalesReturnItemTable.DiscountAmountColumn.FullColumnName] as decimal? ?? 0,
                            MRP = reader[SalesReturnItemTable.MRPColumn.FullColumnName] as decimal? ?? 0,
                            Igst = reader[SalesReturnItemTable.IgstColumn.FullColumnName] as decimal? ?? 0,
                            Cgst = reader[SalesReturnItemTable.CgstColumn.FullColumnName] as decimal? ?? 0,
                            Sgst = reader[SalesReturnItemTable.SgstColumn.FullColumnName] as decimal? ?? 0,
                            GstTotal = reader[SalesReturnItemTable.GstTotalColumn.FullColumnName] as decimal? ?? 0,
                        },
                        productStockInvoice = {
                                BatchNo =  reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString() ?? "",
                                ExpireDate =reader[ProductStockTable.ExpireDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                                VAT  = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                                SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                                MRP = reader[ProductStockTable.MRPColumn.FullColumnName] as decimal? ?? 0,
                        },
                        salesInvoice = {
                            Name =  patientDetails != null ? patientDetails.Name : reader[SalesTable.NameColumn.FullColumnName].ToString() ?? "",
                            Mobile = patientDetails != null ? patientDetails.Mobile : "",
                        },
                        instanceInvoice = {
                                Name =  reader[InstanceTable.NameColumn.FullColumnName].ToString() ?? "",
                                Phone =  reader[InstanceTable.PhoneColumn.FullColumnName].ToString() ?? "",
                                DrugLicenseNo =  reader[InstanceTable.DrugLicenseNoColumn.FullColumnName].ToString() ?? "",
                                TinNo =  reader[InstanceTable.TinNoColumn.FullColumnName].ToString() ?? "",
                                GsTinNo =  reader[InstanceTable.GsTinNoColumn.FullColumnName].ToString() ?? "",
                                Address =  reader[InstanceTable.AddressColumn.FullColumnName].ToString() ?? "",
                                Area =  reader[InstanceTable.AreaColumn.FullColumnName].ToString() ?? "",
                                City =  reader[InstanceTable.CityColumn.FullColumnName].ToString() ?? "",
                                State =  reader[InstanceTable.StateColumn.FullColumnName].ToString() ?? "",
                                Pincode =  reader[InstanceTable.PincodeColumn.FullColumnName].ToString() ?? "",
                                Status = reader[InstanceTable.StatusColumn.FullColumnName] as bool? ?? false,
                                isGstSelect = reader[InstanceTable.GstselectColumn.FullColumnName] as bool? ?? false,
                                isUnionTerritory = reader[InstanceTable.isUnionTerritoryColumn.FullColumnName] as bool? ?? false
                        },
                        salesReturnInvoice = {
                              ReturnNo = reader[SalesReturnTable.ReturnNoColumn.FullColumnName].ToString() ?? "",
                              ReturnDate = reader[SalesReturnTable.ReturnDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                              TaxRefNo = reader[SalesReturnTable.TaxRefNoColumn.FullColumnName] as byte? ?? 0,
                              ReturnCharges = reader[SalesReturnTable.ReturnChargesColumn.FullColumnName] as decimal? ?? 0,
                              NetAmount = reader[SalesReturnTable.NetAmountColumn.FullColumnName] as decimal? ?? 0,
                              RoundOffNetAmount = reader[SalesReturnTable.RoundOffNetAmountColumn.FullColumnName] as decimal? ?? 0,
                              TotalDiscountValue = reader[SalesReturnTable.TotalDiscountValueColumn.FullColumnName] as decimal? ?? 0,
                              ReturnItemAmount = reader[SalesReturnTable.ReturnItemAmountColumn.FullColumnName] as decimal? ?? 0,
                              GstAmount = reader[SalesReturnTable.GstAmountColumn.FullColumnName] as decimal? ?? 0,
                              CreatedDate = reader[SalesReturnTable.CreatedAtColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                        },
                        productInvoice = {
                              Name =  reader[ProductTable.NameColumn.FullColumnName].ToString() ?? "",
                              Manufacturer = reader[ProductTable.ManufacturerColumn.FullColumnName].ToString() ?? "",
                              //RackNo =  reader[ProductTable.RackNoColumn.FullColumnName].ToString() ?? "",
                        },
                        productInstance = {
                                RackNo =  reader[ProductInstanceTable.RackNoColumn.FullColumnName].ToString() ?? "",
                                BoxNO =  reader[ProductInstanceTable.BoxNoColumn.FullColumnName].ToString() ?? "",
                        },
                        hqueuserInvoice =
                        {
                               Name=reader[HQueUserTable.NameColumn.FullColumnName].ToString() ?? "",
                        }
                    };
                    decimal? price = 0.00M;
                    if (returnInvoiceList.salesReturnItemInvoice.MrpSellingPrice != 0)
                    {
                        price = returnInvoiceList.salesReturnItemInvoice.MrpSellingPrice * returnInvoiceList.salesReturnItemInvoice.Quantity;
                    }
                    if (returnInvoiceList.salesReturnItemInvoice.MrpSellingPrice == 0)
                    {
                        price = returnInvoiceList.productStockInvoice.SellingPrice * returnInvoiceList.salesReturnItemInvoice.Quantity;
                    }
                    total += price.GetValueOrDefault();
                    discount = returnInvoiceList.salesInvoice.Discount.GetValueOrDefault();
                    list.Add(returnInvoiceList);
                }
            }

            foreach (var item in list)
            {
                item.salesInvoice.InvoiceNo = item.salesReturnInvoice.ReturnNo;
                item.salesInvoice.InvoiceDate = item.salesReturnInvoice.ReturnDate;
                item.salesInvoice.CreatedAt = item.salesReturnInvoice.CreatedDate;
                item.salesInvoice.PaymentType = "";
            }

            decimal? discountItemReturn = 0.00M;
            foreach (ReturnInvoiceList item in list)
            {
                discountItemReturn += (item.salesReturnItemInvoice.Quantity * (item.salesReturnItemInvoice.MrpSellingPrice != 0 ? item.salesReturnItemInvoice.MrpSellingPrice : item.productStockInvoice.SellingPrice) * item.salesReturnItemInvoice.Discount / 100);
            }
            decimal? discountedAmount = 0.00M;
            if (discount != 0)
            {
                discountedAmount = Math.Round(total * (discount / 100), MidpointRounding.AwayFromZero);
                netAmount = Math.Round(total - discountedAmount.GetValueOrDefault() - discountItemReturn.GetValueOrDefault(), MidpointRounding.AwayFromZero);
            }
            else
            {
                discountedAmount = 0.00M;
                netAmount = Math.Round(total - discountItemReturn.GetValueOrDefault(), MidpointRounding.AwayFromZero);
            }
            roundOff = netAmount - (total - discountedAmount.GetValueOrDefault() - discountItemReturn.GetValueOrDefault());
            obj.returnInvoiceList = list;
            obj.total = total;
            //obj.roundOff = roundOff;
            //obj.netAmount = netAmount;
            obj.discount = discountedAmount + discountItemReturn;
            obj.patientDetails.EmpId = patientDetails != null ? patientDetails.EmpID : "";
            obj.patientDetails.Address = patientDetails != null ? patientDetails.Address : "";
            obj.patientDetails.City = patientDetails != null ? patientDetails.City : "";
            obj.patientDetails.Pincode = patientDetails != null ? patientDetails.Pincode : "";

            Sales sale = new Sales();
            if (obj.returnInvoiceList.Count() > 0) //Added by Poongodi on 04/07/2017
            {
                var returnInvoice = obj.returnInvoiceList.First();
                sale.InvoiceNo = returnInvoice.salesInvoice.InvoiceNo;
                sale.InvoiceDate = returnInvoice.salesInvoice.InvoiceDate;
                sale.CreatedAt = (DateTime)returnInvoice.salesInvoice.CreatedAt;
                sale.PaymentType = returnInvoice.salesInvoice.PaymentType;
                sale.Name = returnInvoice.salesInvoice.Name;
                sale.Mobile = returnInvoice.salesInvoice.Mobile;
                sale.Patient = patientDetails;
                sale.SalesCreatedAt = (DateTime)returnInvoice.salesInvoice.CreatedAt;
                sale.TaxRefNo = returnInvoice.salesReturnInvoice.TaxRefNo;
                if (patientDetails == null)
                {
                    sale.Patient = new Patient();

                    sale.Patient.Name = "";
                    sale.Patient.Mobile = "";
                    sale.Patient.City = "";
                    sale.Patient.Address = "";
                    sale.Patient.Area = "";
                    sale.Patient.Email = "";
                    sale.Patient.GsTin = "";

                }

                sale.DoctorName = string.Empty;
                sale.DoctorMobile = string.Empty;
                sale.Address = string.Empty;
                sale.Pincode = string.Empty;
                sale.Instance.Name = returnInvoice.instanceInvoice.Name;
                sale.Instance.Phone = returnInvoice.instanceInvoice.Phone;
                sale.Instance.DrugLicenseNo = returnInvoice.instanceInvoice.DrugLicenseNo;
                sale.Instance.TinNo = returnInvoice.instanceInvoice.TinNo;
                sale.Instance.GsTinNo = returnInvoice.instanceInvoice.GsTinNo;
                sale.Instance.Address = returnInvoice.instanceInvoice.Address;
                sale.Instance.Area = returnInvoice.instanceInvoice.Area;
                sale.Instance.City = returnInvoice.instanceInvoice.City;
                sale.Instance.State = returnInvoice.instanceInvoice.State;
                sale.Instance.Pincode = returnInvoice.instanceInvoice.Pincode;
                sale.Instance.Status = returnInvoice.instanceInvoice.Status;
                sale.Instance.Gstselect = returnInvoice.instanceInvoice.isGstSelect;
                sale.Instance.isUnionTerritory = returnInvoice.instanceInvoice.isUnionTerritory;
                sale.SalesReturn.ReturnNo = returnInvoice.salesReturnInvoice.ReturnNo;
                sale.SalesReturn.ReturnDate = returnInvoice.salesReturnInvoice.ReturnDate;
                sale.SalesReturn.TaxRefNo = returnInvoice.salesReturnInvoice.TaxRefNo;
                sale.SalesReturn.ReturnCharges = returnInvoice.salesReturnInvoice.ReturnCharges;
                //sale.NetAmount = obj.netAmount * (-1);
                //sale.RoundoffNetAmount = obj.roundOff;
                sale.NetAmount = returnInvoice.salesReturnInvoice.NetAmount;
                sale.SalesReturn.TotalDiscountValue = returnInvoice.salesReturnInvoice.TotalDiscountValue;
                sale.SalesReturn.ReturnItemAmount = returnInvoice.salesReturnInvoice.ReturnItemAmount;
                sale.RoundoffNetAmount = returnInvoice.salesReturnInvoice.RoundOffNetAmount;
                sale.HQueUser.Name = returnInvoice.hqueuserInvoice.Name;

                foreach (var item in obj.returnInvoiceList)
                {
                    var salesReturnItem = new SalesReturnItem()
                    {
                        Quantity = item.salesReturnItemInvoice.Quantity,
                        ReturnedQuantity = item.salesReturnItemInvoice.Quantity,
                        SellingPrice = item.salesReturnItemInvoice.MrpSellingPrice,
                        MrpSellingPrice = item.salesReturnItemInvoice.MrpSellingPrice,
                        Discount = item.salesReturnItemInvoice.Discount,
                        DiscountAmount = item.salesReturnItemInvoice.DiscountAmount,
                        MRP = item.salesReturnItemInvoice.MRP,
                        Igst = item.salesReturnItemInvoice.Igst,
                        IgstValue = (item.salesReturnItemInvoice.Igst / 100),
                        Sgst = item.salesReturnItemInvoice.Sgst,
                        SgstValue = (item.salesReturnItemInvoice.Sgst / 100),
                        Cgst = item.salesReturnItemInvoice.Cgst,
                        CgstValue = (item.salesReturnItemInvoice.Cgst / 100),
                        GstTotal = item.salesReturnItemInvoice.GstTotal,
                        ProductStock =
                    {
                        BatchNo = item.productStockInvoice.BatchNo,
                        ExpireDate = item.productStockInvoice.ExpireDate,
                        VAT = item.productStockInvoice.VAT,
                        SellingPrice = item.productStockInvoice.SellingPrice,
                        MRP = item.productStockInvoice.MRP,
                        Product =
                        {
                            Name = item.productInvoice.Name,
                            Manufacturer = item.productInvoice.Manufacturer,
                            //RackNo=item.productInstance.RackNo!=""?item.productInstance.RackNo:item.productInvoice.RackNo,
                            RackNo=item.productInstance.RackNo,
                            BoxNo=item.productInstance.BoxNO
                        }
                    }
                    };



                    salesReturnItem.ReturnAmt = salesReturnItem.Quantity * salesReturnItem.SellingPrice;
                    sale.SalesReturnItem.Add(salesReturnItem);
                }
                sale.SalesReturn.IgstTotalValue = sale.SalesReturnItem.Sum(m => m.IgstValue);
                sale.SalesReturn.CgstTotalValue = sale.SalesReturnItem.Sum(m => m.CgstValue);
                sale.SalesReturn.SgstTotalValue = sale.SalesReturnItem.Sum(m => m.SgstValue);
                //CgstTotalValue = salesReturnItem.Sum(m => m.CgstValue);
                //SgstTotalValue = salesReturnItem.Sum(m => m.SgstValue);
            }
            return sale;
        }
        //Code added by Sarubala for printing only Return in A4 - end

        // Code To get ReturnItems along with sale - Sarubala
        public async Task<SalesReturn> getReturnItemsAlongWithSale(string salesId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(SalesReturnTable.IsAlongWithSaleColumn);
            qb.Parameters.Add(SalesReturnTable.IsAlongWithSaleColumn.ColumnName, 1);
            qb.ConditionBuilder.And(SalesReturnTable.SalesIdColumn, salesId);

            var isDeletedCondition = new CustomCriteria(SalesReturnItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            string isDelete = "1";
            var cc1 = new CriteriaColumn(SalesReturnItemTable.IsDeletedColumn, isDelete, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, isDeletedCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);


            qb.AddColumn(SalesReturnTable.IdColumn, SalesReturnTable.ReturnNoColumn, SalesReturnTable.SalesIdColumn, SalesReturnTable.IsAlongWithSaleColumn, SalesReturnTable.TaxRefNoColumn, SalesReturnTable.LoyaltyIdColumn, SalesReturnTable.LoyaltyPtsColumn);

            qb.JoinBuilder.Join(SalesReturnItemTable.Table, SalesReturnTable.IdColumn, SalesReturnItemTable.SalesReturnIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesReturnItemTable.Table, SalesReturnItemTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.LeftJoins(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductTable.IdColumn, ProductInstanceTable.InstanceIdColumn, ProductStockTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductInstanceTable.Table, ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn); /* Added by San on 15/06/17 */
            qb.SelectBuilder.SortBy(SalesReturnItemTable.CreatedAtColumn);

            var sreturn = new SalesReturn();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    sreturn.Id = reader.ToString(SalesReturnTable.IdColumn.ColumnName);
                    sreturn.ReturnNo = reader.ToString(SalesReturnTable.ReturnNoColumn.ColumnName);
                    sreturn.SalesId = reader.ToString(SalesReturnTable.SalesIdColumn.ColumnName);
                    sreturn.IsAlongWithSale = reader.ToBoolNullable(SalesReturnTable.IsAlongWithSaleColumn.ColumnName);
                    //sreturn.TaxRefNo= reader.ToIntNullable(SalesReturnTable.TaxRefNoColumn.ColumnName);
                    sreturn.TaxRefNo = reader[SalesReturnTable.TaxRefNoColumn.ColumnName] as int? ?? 0;
                    sreturn.LoyaltyId = reader.ToString(SalesReturnTable.LoyaltyIdColumn.ColumnName);
                    sreturn.LoyaltyPts = reader[SalesReturnTable.LoyaltyPtsColumn.ColumnName] as decimal? ?? 0;
                    
                    SalesReturnItem srItem = new SalesReturnItem();
                    srItem.Id = reader.ToString(SalesReturnItemTable.IdColumn.FullColumnName);
                    srItem.SalesReturnId = reader.ToString(SalesReturnItemTable.SalesReturnIdColumn.FullColumnName);
                    srItem.ProductStockId = reader.ToString(SalesReturnItemTable.ProductStockIdColumn.FullColumnName);
                    srItem.Quantity = reader.ToDecimalNullable(SalesReturnItemTable.QuantityColumn.FullColumnName);
                    srItem.Discount = reader[SalesReturnItemTable.DiscountColumn.FullColumnName] as decimal? ?? 0;
                    srItem.DiscountAmount = reader[SalesReturnItemTable.DiscountAmountColumn.FullColumnName] as decimal? ?? 0;
                    srItem.MrpSellingPrice = reader[SalesReturnItemTable.MrpSellingPriceColumn.FullColumnName] as decimal? ?? 0;
                    srItem.MRP = reader[SalesReturnItemTable.MRPColumn.FullColumnName] as decimal? ?? 0;
                    srItem.Cgst = reader[SalesReturnItemTable.CgstColumn.FullColumnName] as decimal? ?? 0;
                    srItem.Sgst = reader[SalesReturnItemTable.SgstColumn.FullColumnName] as decimal? ?? 0;
                    srItem.Igst = reader[SalesReturnItemTable.IgstColumn.FullColumnName] as decimal? ?? 0;
                    srItem.GstTotal = reader[SalesReturnItemTable.GstTotalColumn.FullColumnName] as decimal? ?? 0;
                    srItem.LoyaltyProductPts = reader[SalesReturnItemTable.LoyaltyProductPtsColumn.FullColumnName] as decimal? ?? 0;

                    srItem.MRPAfterDiscount = srItem.MRP - (srItem.MRP * srItem.Discount / 100);
                    srItem.SellingPriceAfterDiscount = srItem.MrpSellingPrice - (srItem.MrpSellingPrice * srItem.Discount / 100);
                    srItem.MRPValueAfterDiscount = (srItem.MRP - (srItem.MRP * srItem.Discount / 100)) * srItem.Quantity;
                    srItem.SellingPriceValueAfterDiscount = (srItem.MrpSellingPrice - (srItem.MrpSellingPrice * srItem.Discount / 100)) * srItem.Quantity;                    

                    srItem.ProductStock.Fill(reader).Product.Fill(reader);
                    srItem.ProductStock.Product.RackNo = reader[ProductInstanceTable.RackNoColumn.FullColumnName].ToString() ?? ProductTable.RackNoColumn.FullColumnName.ToString();
                    srItem.ProductStock.Product.BoxNo = reader[ProductInstanceTable.BoxNoColumn.FullColumnName].ToString() ?? string.Empty;
                    sreturn.SalesReturnItem.Add(srItem);

                }
            }

            if (sreturn.SalesReturnItem.Count > 0)
            {
                return sreturn;
            }
            else
            {
                return null;
            }

        }

        //End
        // Modified by Violet handled PatientId 
        public async Task<Patient> GetExistingPatientData(string patientId, string mobile, string name, string Accountid, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);


            qb.ConditionBuilder.And(PatientTable.AccountIdColumn);
            //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn);
            qb.Parameters.Add(PatientTable.AccountIdColumn.ColumnName, Accountid);
            //qb.Parameters.Add(PatientTable.InstanceIdColumn.ColumnName, InstanceId);


            if (!string.IsNullOrEmpty(patientId))
            {
                qb.ConditionBuilder.And(PatientTable.IdColumn);
                qb.Parameters.Add(PatientTable.IdColumn.ColumnName, patientId);
            }
            else
            {
                if (!string.IsNullOrEmpty(mobile))
                {
                    qb.ConditionBuilder.And(PatientTable.MobileColumn, CriteriaEquation.Equal);
                    qb.Parameters.Add(PatientTable.MobileColumn.ColumnName, mobile);
                }
                if (!string.IsNullOrEmpty(name))
                {
                    qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Equal);
                    qb.Parameters.Add(PatientTable.NameColumn.ColumnName, name);
                }
            }
            qb.SelectBuilder.MakeDistinct = true;
            var pt = await List<Patient>(qb);
            return pt.FirstOrDefault();
        }
        //End

        //Added by Sarubala to get sales multiple payment details - start
        public async Task<List<SalesPayment>> GetSalesPaymentDetails(string id, string AccId, string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesPaymentTable.Table, OperationType.Select);
            qb.AddColumn(SalesPaymentTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(SalesPaymentTable.SalesIdColumn, id);
            qb.ConditionBuilder.And(SalesPaymentTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(SalesPaymentTable.InstanceIdColumn, InsId);
            qb.ConditionBuilder.And(SalesPaymentTable.isActiveColumn, 1);
            return await List<SalesPayment>(qb);
        }

        //Added by Sarubala to get sales multiple payment details - end

        public Task<List<SalesItem>> SalesItemList(Sales sale)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesItemTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesItemTable.ProductStockIdColumn);
            qb.ConditionBuilder.And(SalesItemTable.SalesIdColumn, sale.Id);
            qb.SelectBuilder.SortBy(SalesItemTable.SalesItemSnoColumn);// Added by nandhini to return the items in sales order on 21-08-2017
            return List<SalesItem>(qb);
        }
        public async Task<decimal?> GetPreviousSalesRetunrItems(string salesid, string productstockid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
            qb.SelectBuilder.GroupBy(SalesReturnItemTable.ProductStockIdColumn);
            qb.AddColumn(DbColumn.SumColumn("ReturnedQuantity", SalesReturnItemTable.QuantityColumn));
            qb.JoinBuilder.Join(SalesReturnTable.Table, SalesReturnTable.IdColumn, SalesReturnItemTable.SalesReturnIdColumn);
            qb.ConditionBuilder.And(SalesReturnTable.SalesIdColumn, salesid);
            qb.ConditionBuilder.And(SalesReturnItemTable.ProductStockIdColumn, productstockid);

            var isDeletedCondition = new CustomCriteria(SalesReturnItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            string isDelete = "1";
            var cc1 = new CriteriaColumn(SalesReturnItemTable.IsDeletedColumn, isDelete, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, isDeletedCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);

            return Convert.ToDecimal(await SingleValueAsyc(qb));
        }
        public Task<List<SalesReturnItem>> SalesReturnItemList(Sales sale)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesReturnItemTable.QuantityColumn, SalesReturnItemTable.ProductStockIdColumn, SalesReturnItemTable.MrpSellingPriceColumn, SalesReturnItemTable.DiscountColumn, SalesReturnItemTable.DiscountAmountColumn, SalesReturnItemTable.IgstColumn, SalesReturnItemTable.CgstColumn, SalesReturnItemTable.SgstColumn, SalesReturnItemTable.GstTotalColumn, SalesReturnItemTable.MRPColumn, SalesReturnItemTable.LoyaltyProductPtsColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, SalesReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.LeftJoin(SalesReturnTable.Table, SalesReturnItemTable.SalesReturnIdColumn, SalesReturnTable.IdColumn);
            qb.ConditionBuilder.And(SalesReturnTable.SalesIdColumn, sale.Id);

            var isDeletedCondition = new CustomCriteria(SalesReturnItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            string isDelete = "1";
            var cc1 = new CriteriaColumn(SalesReturnItemTable.IsDeletedColumn, isDelete, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, isDeletedCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);

            return List<SalesReturnItem>(qb);
        }
        public async Task<List<Sales>> PatientList(Sales data)
        {
            //Get the values for Patient Table
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(data, qb, "frmPatient");
            qb.AddColumn(PatientTable.IdColumn, PatientTable.NameColumn, PatientTable.ShortNameColumn, PatientTable.MobileColumn,
                PatientTable.EmailColumn, PatientTable.DOBColumn, PatientTable.GenderColumn, PatientTable.EmpIDColumn,
                PatientTable.AddressColumn, PatientTable.CityColumn,
                PatientTable.StateColumn, PatientTable.PincodeColumn, PatientTable.DiscountColumn,
                PatientTable.PatientTypeColumn, PatientTable.CustomerPaymentTypeColumn,
                PatientTable.LocationTypeColumn, PatientTable.PanColumn, PatientTable.DrugLicenseNoColumn,
                PatientTable.GsTinColumn, PatientTable.BalanceAmountColumn, PatientTable.StatusColumn
                /*SalesTable.PatientIdColumn, SalesTable.NameColumn, SalesTable.MobileColumn, */);
            // qb.JoinBuilder.LeftJoin(SalesTable.Table, SalesTable.PatientIdColumn, PatientTable.IdColumn);
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(PatientTable.NameColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);


            var list = new List<Sales>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var sales = new Sales
                    {
                        Name = reader[PatientTable.NameColumn.ColumnName].ToString() ?? "",
                        Mobile = reader[PatientTable.MobileColumn.ColumnName].ToString() ?? "",
                        Email = reader[PatientTable.EmailColumn.ColumnName].ToString() ?? "",
                        DOB = reader[PatientTable.DOBColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        Gender = reader[PatientTable.GenderColumn.ColumnName].ToString() ?? "M",
                        Address = reader[PatientTable.AddressColumn.ColumnName].ToString() ?? "",
                        Pincode = reader[PatientTable.PincodeColumn.ColumnName].ToString() ?? "",
                        Discount = reader[PatientTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        PatientId = reader[PatientTable.IdColumn.ColumnName].ToString() ?? ""
                    };
                    sales.Patient.City = reader[PatientTable.CityColumn.ColumnName].ToString() ?? "";
                    sales.Patient.ShortName = reader[PatientTable.ShortNameColumn.ColumnName].ToString() ?? "";
                    sales.Patient.State = reader[PatientTable.StateColumn.ColumnName].ToString() ?? "";
                    sales.Patient.EmpID = reader[PatientTable.EmpIDColumn.ColumnName].ToString() ?? "";
                    sales.Patient.Id = reader[PatientTable.IdColumn.ColumnName].ToString() ?? "";
                    sales.Patient.CustomerPaymentType = reader[PatientTable.CustomerPaymentTypeColumn.ColumnName].ToString() ?? "Cash";
                    sales.Patient.BalanceAmount = reader[PatientTable.BalanceAmountColumn.ColumnName] as decimal? ?? 0;
                    sales.Patient.PatientType = reader[PatientTable.PatientTypeColumn.ColumnName] as Int32? ?? 0;
                    sales.Patient.Status = reader[PatientTable.StatusColumn.ColumnName] as Int32? ?? 0;

                    int? LocationtyTecolumn = reader[PatientTable.LocationTypeColumn.ColumnName] as Int32? ?? 0;

                    sales.Patient.LocationTypeName = LocationtyTecolumn == 0 ? Enum.GetName(typeof(LocationType), 0) : Enum.GetName(typeof(LocationType), LocationtyTecolumn);

                    sales.Patient.Pan = reader[PatientTable.PanColumn.ColumnName].ToString() ?? "";

                    sales.Patient.DrugLicenseNo = reader[PatientTable.DrugLicenseNoColumn.ColumnName].ToString() ?? "";


                    sales.Patient.GsTin = reader[PatientTable.GsTinColumn.ColumnName].ToString() ?? "";

                    sales.SalesCount = 0;

                    var qb1 = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
                    qb1.ConditionBuilder.And(SalesTable.PatientIdColumn, CriteriaEquation.Equal);
                    qb1.Parameters.Add(SalesTable.PatientIdColumn.ColumnName, sales.Patient.Id);
                    var result1 = await List<Sales>(qb1);
                    if (result1.Count > 0) // Already discount column check.Modified sales PatientId column
                    {
                        sales.SalesCount = 1;
                    }
                    list.Add(sales);
                }
            }
            return list;
        }
        public Task<List<Sales>> PatientListBulkSms(Sales data)
        {
            //Added by Annadurai on 06/05/2017
            //var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            //qb = SqlQueryBuilder(data, qb);
            //qb.AddColumn(SalesTable.NameColumn, SalesTable.MobileColumn, SalesTable.EmailColumn, SalesTable.DOBColumn,
            //    SalesTable.GenderColumn, SalesTable.PatientIdColumn);
            //var cc = new CustomCriteria(SalesTable.PatientIdColumn, CriteriaCondition.IsNotNull);
            //var name = new CustomCriteria(SalesTable.NameColumn, CriteriaCondition.IsNotNull);
            //var mobile = new CustomCriteria(SalesTable.MobileColumn, CriteriaCondition.IsNotNull);
            //qb.ConditionBuilder.And(cc);
            //qb.ConditionBuilder.And(name);
            //qb.ConditionBuilder.And(mobile);
            //qb.SelectBuilder.MakeDistinct = true;
            //qb.SelectBuilder.SortBy(SalesTable.NameColumn);
            //qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            //return List<Sales>(qb);
            //Commented by Annadurai
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            //qb = SqlQueryBuilder(data, qb);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn, data.AccountId);
            qb.AddColumn(PatientTable.NameColumn, PatientTable.MobileColumn, PatientTable.EmailColumn, PatientTable.DOBColumn,
                PatientTable.GenderColumn, PatientTable.IdColumn);
            var id = new CustomCriteria(PatientTable.IdColumn, CriteriaCondition.IsNotNull);
            qb.ConditionBuilder.And(id);
            var statusCondition = new CustomCriteria(PatientTable.MobileColumn, CriteriaCondition.IsNotNull);
            string condition1 = "''";
            var cc1 = new CriteriaColumn(PatientTable.MobileColumn, condition1, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.And);
            qb.ConditionBuilder.And(col1);
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data.Name);
            }
            if (data.PatientStatus != null)
            {
                qb.ConditionBuilder.And(PatientTable.StatusColumn, data.PatientStatus);
            }
            //var mobile = new CustomCriteria(PatientTable.MobileColumn, CriteriaCondition.IsNotNull);
            //qb.ConditionBuilder.And(mobile);
            qb.SelectBuilder.MakeDistinct = true;
            qb.SelectBuilder.SortBy(PatientTable.NameColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            return List<Sales>(qb);
        }
        public async Task<Sales> PatientDetail(Sales data)
        {
            //Get the values for Patient Table
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(PatientTable.NameColumn, data.Name);
            qb.ConditionBuilder.And(PatientTable.MobileColumn, data.Mobile);
            qb.AddColumn(PatientTable.IdColumn, PatientTable.NameColumn, PatientTable.AgeColumn, PatientTable.DOBColumn, PatientTable.GenderColumn, PatientTable.MobileColumn,
                PatientTable.EmailColumn, PatientTable.EmpIDColumn, PatientTable.AddressColumn, SalesTable.DoctorNameColumn,
                SalesTable.PatientIdColumn, SalesTable.MobileColumn, SalesTable.NameColumn, PatientTable.CustomerPaymentTypeColumn);
            qb.JoinBuilder.LeftJoins(SalesTable.Table, SalesTable.NameColumn, PatientTable.NameColumn, SalesTable.MobileColumn, PatientTable.MobileColumn);
            qb.SelectBuilder.MakeDistinct = true;
            var sales = new Sales();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    sales.Name = reader[PatientTable.NameColumn.ColumnName].ToString() ?? "";
                    sales.Mobile = reader[PatientTable.MobileColumn.ColumnName].ToString() ?? "";
                    sales.Email = reader[PatientTable.EmailColumn.ColumnName].ToString() ?? "";
                    sales.Age = reader[PatientTable.AgeColumn.ColumnName] as int? ?? 0;
                    sales.DOB = reader[PatientTable.DOBColumn.ColumnName] as DateTime? ?? DateTime.MinValue;
                    sales.Gender = reader[PatientTable.GenderColumn.ColumnName].ToString() ?? "M";
                    sales.Patient.EmpID = reader[PatientTable.EmpIDColumn.ColumnName].ToString() ?? "";
                    sales.PatientId = reader[SalesTable.PatientIdColumn.ColumnName].ToString() ?? "";
                    sales.Address = reader[PatientTable.AddressColumn.ColumnName].ToString() ?? "";
                    sales.DoctorName = reader[SalesTable.DoctorNameColumn.ColumnName].ToString() ?? "";
                    sales.Patient.CustomerPaymentType = reader[PatientTable.CustomerPaymentTypeColumn.ColumnName].ToString() ?? "Cash";
                    sales.Patient.Id = reader[PatientTable.IdColumn.ColumnName].ToString() ?? "";
                }
            }
            return sales;
            //return (await List<Sales>(qb)).FirstOrDefault();
        }

        //Added by Sarubala on 20-11-17
        public async Task<bool> GetCustomerBulkSmsSetting(string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SmsSettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SmsSettingsTable.InstanceIdColumn, InsId);
            bool setting1 = false;
            var result = await List<SmsSettings>(qb);
            if (result != null && result.Count > 0)
            {
                setting1 = Convert.ToBoolean(result.First().IsCustomerBulkSms != null ? result.First().IsCustomerBulkSms : false);
            }
            return setting1;
        }

        public async Task<IEnumerable<Sales>> SalesList(Sales data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            var result = await List<Sales>(qb);
            return await SalesPriceCount(result);
        }
        private async Task<IEnumerable<Sales>> SalesPriceCount(IEnumerable<Sales> sales)
        {
            var list = new List<Sales>();
            foreach (var item in sales)
            {
                item.SalesItem = await SalesItemList(item);
                list.Add(item);
            }
            return list;
        }
        public async Task<int> PatientCount(Sales data)
        {
            //Added by Annadurai on 06/05/2017
            //var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Count);
            //qb.AddColumn(SalesTable.MobileColumn);
            //qb = SqlQueryBuilder(data, qb);
            //qb.SelectBuilder.MakeDistinct = true;
            //return Convert.ToInt32(await SingleValueAsyc(qb));
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Count);
            qb.AddColumn(PatientTable.IdColumn);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn, data.AccountId);
            var id = new CustomCriteria(PatientTable.IdColumn, CriteriaCondition.IsNotNull);
            qb.ConditionBuilder.And(id);
            var statusCondition = new CustomCriteria(PatientTable.MobileColumn, CriteriaCondition.IsNotNull);
            string condition1 = "''";
            var cc1 = new CriteriaColumn(PatientTable.MobileColumn, condition1, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, statusCondition, CriteriaCondition.And);
            qb.ConditionBuilder.And(col1);
            if (!string.IsNullOrEmpty(data.Name)) //Customer Bulk SMS - Pagination Issue => Not match with List
            {
                qb.ConditionBuilder.And(PatientTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(PatientTable.NameColumn.ColumnName, data.Name);
            }
            qb.SelectBuilder.MakeDistinct = true;
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        //Added for get the count for the patient table
        public async Task<int> CustomerCount(Sales data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Count);
            qb.AddColumn(PatientTable.MobileColumn);
            qb = SqlCustomerQueryBuilder(data, qb);
            qb.SelectBuilder.MakeDistinct = true;
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<Sales> UpdateSaleDetail(Sales data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(SalesTable.IdColumn, data.Id);
            qb.AddColumn(SalesTable.NameColumn, SalesTable.MobileColumn, SalesTable.DoctorNameColumn, SalesTable.DoctorMobileColumn, SalesTable.DoorDeliveryStatusColumn);
            qb.Parameters.Add(SalesTable.NameColumn.ColumnName, data.Name);
            qb.Parameters.Add(SalesTable.MobileColumn.ColumnName, data.Mobile);
            qb.Parameters.Add(SalesTable.DoctorNameColumn.ColumnName, data.DoctorName);
            qb.Parameters.Add(SalesTable.DoctorMobileColumn.ColumnName, data.DoctorMobile);
            qb.Parameters.Add(SalesTable.DoorDeliveryStatusColumn.ColumnName, data.DoorDeliveryStatus);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }
        public async Task<Sales> UpdateSaleDetailsforFileUpload(Sales data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(SalesTable.IdColumn, data.Id);
            qb.AddColumn(SalesTable.FileNameColumn);
            qb.Parameters.Add(SalesTable.FileNameColumn.ColumnName, data.FileName);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }
        public async Task UpdateSaleTableDetail(string salesid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(SalesTable.IdColumn, salesid);
            qb.AddColumn(SalesTable.CancelstatusColumn);
            qb.Parameters.Add(SalesTable.CancelstatusColumn.ColumnName, 1);
            await QueryExecuter.NonQueryAsyc(qb);
        }
        //public async Task<int> Count(CardTypeSettings data)
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Count);
        //    qb = SqlQueryBuilder(data, qb);
        //    return Convert.ToInt32(await SingleValueAsyc(qb));
        //}
        //Newly Added Gavaskar 24-10-2016 Start
        public async Task<CardTypeSettings> SaveCardType(CardTypeSettings data)
        {
            var count = await CountCardType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(CardTypeSettingsTable.Table, OperationType.Update);
                qb.AddColumn(CardTypeSettingsTable.CardTypeColumn);
                qb.Parameters.Add(CardTypeSettingsTable.CardTypeColumn.ColumnName, data.CardType);
                qb.ConditionBuilder.And(CardTypeSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(CardTypeSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, CardTypeSettingsTable.Table);
            }
        }
        public async Task<SalesBatchPopUpSettings> saveBatchPopUpSettings(SalesBatchPopUpSettings data)
        {
            var count = await CountBatchPopUpSettings(data);
            if (count > 0)
            {
                return await Update(data, SalesBatchPopUpSettingsTable.Table);
            }
            else
            {
                return await Insert(data, SalesBatchPopUpSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> SaveRound(SaleSettings data)
        {
            var count = await CountSaleSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.PrinterTypeColumn);
                qb.Parameters.Add(SaleSettingsTable.PrinterTypeColumn.ColumnName, data.PrinterType);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                //await UpdateNonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> SavePrintType(SaleSettings data)
        {
            var count = await CountSaleSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.PrinterTypeColumn);
                qb.Parameters.Add(SaleSettingsTable.PrinterTypeColumn.ColumnName, data.PrinterType);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        // Insert Custom Template
        public async Task<SaleSettings> SaveCustomTemplate(SaleSettings data)
        {
            var count = await CountSaleSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.CustomTempJSONColumn);
                qb.Parameters.Add(SaleSettingsTable.CustomTempJSONColumn.ColumnName, data.CustomTempJSON);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> savePatientSearchType(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.PatientSearchTypeColumn);
                qb.Parameters.Add(SaleSettingsTable.PatientSearchTypeColumn.ColumnName, data.PatientSearchType);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> saveDoctorSearchType(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.DoctorSearchTypeColumn);
                qb.Parameters.Add(SaleSettingsTable.DoctorSearchTypeColumn.ColumnName, data.DoctorSearchType);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        //Added by Sarubala on 03-10-17
        public async Task<bool> GetSettingsIsCancelEditSms(string AccId, string InsId)
        {
            bool sendSms = false;
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.IsCancelEditSMSColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InsId);
            var saleSettings = (await List<SaleSettings>(qb)).FirstOrDefault();
            if (saleSettings.IsCancelEditSMS != null)
            {
                sendSms = Convert.ToBoolean(saleSettings.IsCancelEditSMS);
            }
            return sendSms;
        }
        //Added by Sarubala on 03-10-17
        public async Task<Account> GetAccountDetailToSendSms(string AccId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(AccountTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(AccountTable.IdColumn, AccId);
            Account tempAccount = new Account();
            var account = (await List<Account>(qb));
            if (account != null && account.Count > 0)
            {
                tempAccount = account.FirstOrDefault();
            }
            return tempAccount;  //Modified by Sarubala on 21-11-17
        }
        public async Task<SaleSettings> isDoctorMandatory(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.DoctorNameMandatoryTypeColumn);
                qb.Parameters.Add(SaleSettingsTable.DoctorNameMandatoryTypeColumn.ColumnName, data.DoctorNameMandatoryType);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> saveSalesTypeMandatory(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.SaleTypeMandatoryColumn);
                qb.Parameters.Add(SaleSettingsTable.SaleTypeMandatoryColumn.ColumnName, data.SaleTypeMandatory);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> isMaximumDiscountAvail(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.MaxDiscountAvailColumn, SaleSettingsTable.InstanceMaxDiscountColumn);
                qb.Parameters.Add(SaleSettingsTable.MaxDiscountAvailColumn.ColumnName, data.MaxDiscountAvail);
                qb.Parameters.Add(SaleSettingsTable.InstanceMaxDiscountColumn.ColumnName, 100);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> saveAutoTempStockAdd(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.AutoTempStockAddColumn);
                qb.Parameters.Add(SaleSettingsTable.AutoTempStockAddColumn.ColumnName, data.AutoTempStockAdd);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> isAutosavecustomer(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.AutosaveCustomerColumn);
                qb.Parameters.Add(SaleSettingsTable.AutosaveCustomerColumn.ColumnName, data.AutosaveCustomer);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> saveIsCreditInvoiceSeries(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.IsCreditInvoiceSeriesColumn);
                qb.Parameters.Add(SaleSettingsTable.IsCreditInvoiceSeriesColumn.ColumnName, data.IsCreditInvoiceSeries);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> saveDiscountType(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.DiscountTypeColumn);
                qb.Parameters.Add(SaleSettingsTable.DiscountTypeColumn.ColumnName, data.DiscountType);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> saveisDepartment(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.PatientTypeDeptColumn);
                qb.Parameters.Add(SaleSettingsTable.PatientTypeDeptColumn.ColumnName, data.PatientTypeDept);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> saveCustomseriesInvoice(SaleSettings data)
        {
            var count = await CountPatientSeachType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.CustomSeriesInvoiceColumn);
                qb.Parameters.Add(SaleSettingsTable.CustomSeriesInvoiceColumn.ColumnName, data.CustomSeriesInvoice);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> savecancelBillDays(SaleSettings data)
        {
            var count = await CountCancelBillDays(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.PostCancelBillDaysColumn);
                qb.Parameters.Add(SaleSettingsTable.PostCancelBillDaysColumn.ColumnName, data.PostCancelBillDays);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> saveMaxDiscount(SaleSettings data)
        {
            var count = await CountCancelBillDays(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.InstanceMaxDiscountColumn);
                qb.Parameters.Add(SaleSettingsTable.InstanceMaxDiscountColumn.ColumnName, data.InstanceMaxDiscount);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<InvoiceSeries> saveInvoiceSeries(InvoiceSeries data)
        {
            var count = await CountInvoiceSeries(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(InvoiceSeriesTable.Table, OperationType.Update);
                qb.AddColumn(InvoiceSeriesTable.InvoiceseriesTypeColumn);
                qb.Parameters.Add(InvoiceSeriesTable.InvoiceseriesTypeColumn.ColumnName, data.InvoiceseriesType);
                qb.ConditionBuilder.And(InvoiceSeriesTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(InvoiceSeriesTable.InstanceIdColumn, data.InstanceId);
                // Added Gavaskar 11-04-2017
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, InvoiceSeriesTable.Table);
            }
        }
        public async Task<int> CountCardType(CardTypeSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CardTypeSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(CardTypeSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(CardTypeSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<int> CountBatchPopUpSettings(SalesBatchPopUpSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesBatchPopUpSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(SalesBatchPopUpSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SalesBatchPopUpSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<int> CountSaleSettings(SaleSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<int> CountPatientSeachType(SaleSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<int> CountCancelBillDays(SaleSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<int> CountInvoiceSeries(InvoiceSeries data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InvoiceSeriesTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(InvoiceSeriesTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(InvoiceSeriesTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<string> GetCardType(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(CardTypeSettingsTable.Table, OperationType.Select);
            qb.AddColumn(CardTypeSettingsTable.CardTypeColumn);
            qb.ConditionBuilder.And(CardTypeSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(CardTypeSettingsTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var userTypes = await List<CardTypeSettings>(qb);
            var userType = "Last4Digits";
            if (userTypes.Count > 0)
            {
                userType = userTypes.First().CardType;
            }
            return userType;
        }
        public async Task<SalesBatchPopUpSettings> getBatchPopUpSettings(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesBatchPopUpSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SalesBatchPopUpSettingsTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(SalesBatchPopUpSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SalesBatchPopUpSettingsTable.InstanceIdColumn, InstanceId);
            SalesBatchPopUpSettings result = (await List<SalesBatchPopUpSettings>(qb)).FirstOrDefault();
            if (result == null)
            {
                SalesBatchPopUpSettings data = new SalesBatchPopUpSettings();
                return data;
            }
            else
            {
                return result;
            }
        }

        //Added by Sarubala on 06-11-17
        public async Task<int> getSalesPriceSetting(string InsId)
        {
            int setting = 0;
            var qb = QueryBuilderFactory.GetQueryBuilder(SettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SettingsTable.InstanceIdColumn, InsId);
            var result = await List<Setting>(qb);
            if (result != null && result.Count > 0)
            {
                if (result.FirstOrDefault().SalesPriceSetting != null)
                    setting = Convert.ToInt32(result.FirstOrDefault().SalesPriceSetting);
            }
            return setting;
        }

        //Added by Sarubala on 15-11-17
        public async Task<SmsSettings> getSmsSettings(string InstanceId, string AccountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SmsSettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SmsSettingsTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(SmsSettingsTable.AccountIdColumn, AccountId);
            var result = await List<SmsSettings>(qb);
            var settings = new SmsSettings();
            if (result != null && result.Count > 0)
            {
                settings = result.First();
            }
            settings.IsSalesCreateSms = settings.IsSalesCreateSms == null ? false : settings.IsSalesCreateSms;
            settings.IsSalesEditSms = settings.IsSalesEditSms == null ? false : settings.IsSalesEditSms;
            settings.IsSalesCancelSms = settings.IsSalesCancelSms == null ? false : settings.IsSalesCancelSms;
            settings.IsSalesEstimateSms = settings.IsSalesEstimateSms == null ? false : settings.IsSalesEstimateSms;
            settings.IsSalesOrderSms = settings.IsSalesOrderSms == null ? false : settings.IsSalesOrderSms;
            settings.IsSalesTemplateCreateSms = settings.IsSalesTemplateCreateSms == null ? false : settings.IsSalesTemplateCreateSms;

            return settings;
        }

        //Added by Sarubala on 23-11-17
        public async Task<SmsSettings> getSmsSettingsForInstance(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SmsSettingsTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(SmsSettingsTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(SmsSettingsTable.AccountIdColumn, AccountId);
            var result = await List<SmsSettings>(qb);
            var settings = new SmsSettings();
            if (result != null && result.Count > 0)
            {
                settings = result.First();
            }
            settings.IsSalesCreateSms = settings.IsSalesCreateSms == null ? false : settings.IsSalesCreateSms;
            settings.IsSalesEditSms = settings.IsSalesEditSms == null ? false : settings.IsSalesEditSms;
            settings.IsSalesCancelSms = settings.IsSalesCancelSms == null ? false : settings.IsSalesCancelSms;
            settings.IsSalesEstimateSms = settings.IsSalesEstimateSms == null ? false : settings.IsSalesEstimateSms;
            settings.IsSalesOrderSms = settings.IsSalesOrderSms == null ? false : settings.IsSalesOrderSms;
            settings.IsSalesTemplateCreateSms = settings.IsSalesTemplateCreateSms == null ? false : settings.IsSalesTemplateCreateSms;

            settings.IsVendorCreateSms = settings.IsVendorCreateSms == null ? false : settings.IsVendorCreateSms;
            settings.IsLoginSms = settings.IsLoginSms == null ? false : settings.IsLoginSms;
            settings.IsCustomerBulkSms = settings.IsCustomerBulkSms == null ? false : settings.IsCustomerBulkSms;
            settings.IsOrderCreateSms = settings.IsOrderCreateSms == null ? false : settings.IsOrderCreateSms;
            settings.IsUserCreateSms = settings.IsUserCreateSms == null ? false : settings.IsUserCreateSms;

            return settings;
        }
        //Added by Sarubala on 24-11-17
        public async Task<SmsSettings> saveSmsSettingsForInstance(SmsSettings data)
        {
            int count1 = GetSmsSettingsCount(data.AccountId, data.InstanceId).Result;

            if (count1 > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SmsSettingsTable.Table, OperationType.Update);
                qb.ConditionBuilder.And(SmsSettingsTable.InstanceIdColumn, data.InstanceId);
                qb.AddColumn(SmsSettingsTable.IsSalesCreateSmsColumn, SmsSettingsTable.IsSalesEditSmsColumn, SmsSettingsTable.IsSalesEstimateSmsColumn, SmsSettingsTable.IsSalesOrderSmsColumn, SmsSettingsTable.IsVendorCreateSmsColumn, SmsSettingsTable.IsOrderCreateSmsColumn, SmsSettingsTable.IsCustomerBulkSmsColumn, SmsSettingsTable.IsUserCreateSmsColumn, SmsSettingsTable.UpdatedAtColumn, SmsSettingsTable.UpdatedByColumn);
                qb.Parameters.Add(SmsSettingsTable.IsSalesCreateSmsColumn.ColumnName, data.IsSalesCreateSms);
                qb.Parameters.Add(SmsSettingsTable.IsSalesEditSmsColumn.ColumnName, data.IsSalesEditSms);
                qb.Parameters.Add(SmsSettingsTable.IsSalesEstimateSmsColumn.ColumnName, data.IsSalesEstimateSms);
                qb.Parameters.Add(SmsSettingsTable.IsSalesOrderSmsColumn.ColumnName, data.IsSalesOrderSms);
                qb.Parameters.Add(SmsSettingsTable.IsVendorCreateSmsColumn.ColumnName, data.IsVendorCreateSms);
                qb.Parameters.Add(SmsSettingsTable.IsOrderCreateSmsColumn.ColumnName, data.IsOrderCreateSms);
                qb.Parameters.Add(SmsSettingsTable.IsCustomerBulkSmsColumn.ColumnName, data.IsCustomerBulkSms);
                qb.Parameters.Add(SmsSettingsTable.IsUserCreateSmsColumn.ColumnName, data.IsUserCreateSms);
                qb.Parameters.Add(SmsSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(SmsSettingsTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
                await QueryExecuter.NonQueryAsyc(qb);
            }
            else
            {
                await Insert(data, SmsSettingsTable.Table);
            }

            return data;
        }


        //Added by Sarubala on 29-11-17
        public async Task<int> GetSmsSettingsCount(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SmsSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(SmsSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SmsSettingsTable.InstanceIdColumn, InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        //Added by Sarubala on 28-11-17
        public async Task<long> GetRemainingSmsCount(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, InstanceId);
            long smscount = 0;
            var result = await List<Instance>(qb);

            if (result != null && result.Count > 0)
            {
                result.FirstOrDefault().TotalSmsCount = result.FirstOrDefault().TotalSmsCount != null ? result.FirstOrDefault().TotalSmsCount : 0;
                result.FirstOrDefault().SentSmsCount = result.FirstOrDefault().SentSmsCount != null ? result.FirstOrDefault().SentSmsCount : 0;
                smscount = Convert.ToInt64(result.FirstOrDefault().TotalSmsCount - result.FirstOrDefault().SentSmsCount);
            }
            return smscount;
        }


        public async Task<SaleSettings> getPatientSearchType(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.PatientSearchTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getDoctorSearchType(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.DoctorSearchTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getDoctorNameMandatoryType(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.DoctorNameMandatoryTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getIsMaxDiscountAvail(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.MaxDiscountAvailColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getSalestypeMandatory(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.SaleTypeMandatoryColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getAutoTempstockAdd(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            //qb.AddColumn(SaleSettingsTable.AutoTempStockAddColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getautoSaveisMandatory(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.AutosaveCustomerColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getIsCreditInvoiceSeries(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.IsCreditInvoiceSeriesColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getPharmacyDiscountType(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.DiscountTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getCustomSeriesSelected(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.CustomSeriesInvoiceColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getCancelBillDays(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.PostCancelBillDaysColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getMaxDiscountValue(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.InstanceMaxDiscountColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<InvoiceSeries> getInvoiceSeries(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InvoiceSeriesTable.Table, OperationType.Select);
            qb.AddColumn(InvoiceSeriesTable.InvoiceseriesTypeColumn);
            qb.ConditionBuilder.And(InvoiceSeriesTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(InvoiceSeriesTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var result = (await List<InvoiceSeries>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getIsDepartmentadded(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.PatientTypeDeptColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        //public async Task<string> GetBillPrintStatus(string accountId, string instanceId)
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(BillPrintSettingsTable.Table, OperationType.Select);
        //    qb.AddColumn(BillPrintSettingsTable.StatusColumn);
        //    qb.ConditionBuilder.And(BillPrintSettingsTable.AccountIdColumn, accountId);
        //    qb.ConditionBuilder.And(BillPrintSettingsTable.InstanceIdColumn, instanceId);
        //    var printSettings = await List<BillPrintSettings>(qb);
        //    string status1 = null;
        //    if (printSettings.Count > 0)
        //    {
        //        status1 = printSettings.FirstOrDefault().Status;
        //    }
        //    return status1;
        //}
        public async Task<Tuple<string, string>> GetBillPrintStatus(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(BillPrintSettingsTable.Table, OperationType.Select);
            qb.AddColumn(BillPrintSettingsTable.StatusColumn, BillPrintSettingsTable.FooterNoteColumn);
            qb.ConditionBuilder.And(BillPrintSettingsTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(BillPrintSettingsTable.InstanceIdColumn, instanceId);
            var printSettings = await List<BillPrintSettings>(qb);
            string status1 = null;
            string footerNote = null;
            if (printSettings.Count > 0)
            {
                status1 = printSettings.FirstOrDefault().Status;
                footerNote = printSettings.FirstOrDefault().FooterNote;
            }
            return new Tuple<string, string>(status1, footerNote);
        }
        public async Task<string> GetBatchListDetail(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(BatchListSettingsTable.Table, OperationType.Select);
            qb.AddColumn(BatchListSettingsTable.BatchListTypeColumn);
            qb.ConditionBuilder.And(BatchListSettingsTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(BatchListSettingsTable.InstanceIdColumn, instanceId);
            var batchSettings = await List<BatchListSettings>(qb);
            string type = "";
            if (batchSettings.Count > 0)
            {
                type = batchSettings.FirstOrDefault().BatchListType;
            }
            return type;
        }
        public async Task<int> GetPrintType(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.PrinterTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, instanceId);
            var saleSettings = await List<SaleSettings>(qb);
            int? type = 0;
            if (saleSettings.Count > 0)
            {
                type = saleSettings.FirstOrDefault().PrinterType;
            }
            return Convert.ToInt32(type);
        }

        //Added by Sarubala on 14-10-17
        public async Task<SaleSettings> GetAllSalesSettings(string accountId, string instanceId)
        {
            SaleSettings settings1 = await getScanBarcodeOption(accountId, instanceId);
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, instanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();

            if (result == null)
            {
                result = new SaleSettings();
                result.IsEnableRoundOff = true;
            }
            else if (result.IsEnableRoundOff == null)
            {
                result.IsEnableRoundOff = true;
            }

            if (result.PrinterType == null)
            {
                result.PrinterType = 0;
            }
            if (result != null && settings1 != null)
            {
                result.ScanBarcode = settings1.ScanBarcode;
                result.AutoScanQty = settings1.AutoScanQty;
                result.ShowSalesItemHistory = settings1.ShowSalesItemHistory;
                result.PrintCustomFields = settings1.PrintCustomFields != null ? settings1.PrintCustomFields : result.PrintCustomFields;
            }

            return result;
        }
        //Added by Sarubala on 20-10-17
        public async Task<SaleSettings> GetBaseSaleSettings(string accId, string insId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.CompleteSaleKeyTypeColumn, SaleSettingsTable.ShortCustomerNameColumn, SaleSettingsTable.ShortDoctorNameColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, insId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();

            if (result == null)
            {
                result = new SaleSettings();
            }

            if (string.IsNullOrEmpty(result.CompleteSaleKeyType))
            {
                result.CompleteSaleKeyType = "";
            }
            if (result.ShortDoctorName == null)
            {
                result.ShortDoctorName = false;
            }
            if (result.ShortCustomerName == null)
            {
                result.ShortCustomerName = false;
            }

            return result;
        }

        //Added by Sarubala on 20-10-17
        public async Task<Sales> GetLastSaleDetails(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.IdColumn, SalesTable.DiscountTypeColumn, SalesTable.SendSmsColumn, SalesTable.SendEmailColumn, SalesTable.BillPrintColumn);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceId);
            qb.SelectBuilder.SetTop(1);
            qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);
            var result = (await List<Sales>(qb)).FirstOrDefault();

            if (result == null)
            {
                result = new Sales();
            }
            if (result.BillPrint == null)
            {
                result.BillPrint = false;
            }
            if (result.SendSms == null)
            {
                result.SendSms = false;
            }
            if (result.SendEmail == null)
            {
                result.SendEmail = false;
            }
            if (result.DiscountType == null)
            {
                result.DiscountType = 1;
            }

            return result;
        }

        /// <summary>
        /// Department print returnStatus parameter added on 30Aug2017
        /// </summary>
        /// <param name="returnStatus"></param>
        /// <returns></returns>
        public async Task<string> GetSaleIdByInvoiceNo(int InvoiceSeriesType, string SeriesTypeValue, string InvNo, string accountid, string instanceid, bool returnStatus)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Type", InvoiceSeriesType);
            parms.Add("TypeValue", SeriesTypeValue);
            parms.Add("Value", InvNo);
            parms.Add("AccountId", accountid);
            parms.Add("InstanceId", instanceid);
            parms.Add("returnStatus", returnStatus);
            var saleId = await sqldb.ExecuteProcedureAsync<dynamic>("USP_GetSaleIdByInvoiceNo", parms);
            var rows = saleId;
            foreach (IDictionary<string, object> row in rows)
            {
                Console.WriteLine("row:");
                foreach (var pair in row)
                {
                    Console.WriteLine("  {0} = {1}", pair.Key, pair.Value);
                    //if (!string.IsNullOrEmpty((string)pair.Value) && !string.IsNullOrEmpty(customer) && returnStatus!=true)
                    //{
                    //   await UpdateCustomerByDepartment(accountid, instanceid, (string)pair.Value, customer);
                    //}
                    return (string)pair.Value;
                }
            }
            return "";
        }
        public async Task<string> GetCustomTemplate(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.CustomTempJSONColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, instanceId);
            var saleSettings = await List<SaleSettings>(qb);
            var type = "";
            if (saleSettings.Count > 0)
            {
                //type = saleSettings.FirstOrDefault().CustomTempJSON.ToString();
                //type = saleSettings.FirstOrDefault().CustomTempJSON;
                if (saleSettings.FirstOrDefault().CustomTempJSON != null)
                {
                    type = saleSettings.FirstOrDefault().CustomTempJSON.ToString();
                }
                else
                {
                    type = "";
                }
            }
            return type;
        }
        public async Task<int> GetInvoicePrintType(SaleSettings getSettings)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.PrinterTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, getSettings.AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, getSettings.InstanceId);
            var saleSettings = await List<SaleSettings>(qb);
            int? type = 0;
            if (saleSettings.Count > 0)
            {
                type = saleSettings.FirstOrDefault().PrinterType;
            }
            return Convert.ToInt32(type);
        }
        public async Task<BatchListSettings> SaveBatchType(BatchListSettings data)
        {
            var count = await CountBatchType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(BatchListSettingsTable.Table, OperationType.Update);
                qb.AddColumn(BatchListSettingsTable.BatchListTypeColumn);
                qb.Parameters.Add(BatchListSettingsTable.BatchListTypeColumn.ColumnName, data.BatchListType);
                qb.ConditionBuilder.And(BatchListSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(BatchListSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, BatchListSettingsTable.Table);
            }
        }
        //public async Task<BillPrintSettings> SaveBillPrintStatus(BillPrintSettings ps)
        //{
        //    var count = await CountPrintStatus(ps);
        //    if (count > 0)
        //    {
        //        var qb = QueryBuilderFactory.GetQueryBuilder(BillPrintSettingsTable.Table, OperationType.Update);
        //        qb.AddColumn(BillPrintSettingsTable.StatusColumn);
        //        qb.Parameters.Add(BillPrintSettingsTable.StatusColumn.ColumnName, ps.Status);
        //        qb.ConditionBuilder.And(BillPrintSettingsTable.AccountIdColumn, ps.AccountId);
        //        qb.ConditionBuilder.And(BillPrintSettingsTable.InstanceIdColumn, ps.InstanceId);
        //        await QueryExecuter.NonQueryAsyc(qb);
        //        return ps;
        //    }
        //    else
        //    {
        //        return await Insert(ps, BillPrintSettingsTable.Table);
        //    }
        //}
        //Newly added for Print Footer Note column by Mani on 28-02-2017
        public async Task<BillPrintSettings> SaveBillPrintStatus(BillPrintSettings ps)
        {
            var count = await CountPrintStatus(ps);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(BillPrintSettingsTable.Table, OperationType.Update);
                qb.AddColumn(BillPrintSettingsTable.StatusColumn, BillPrintSettingsTable.FooterNoteColumn);
                qb.Parameters.Add(BillPrintSettingsTable.StatusColumn.ColumnName, ps.Status);
                qb.Parameters.Add(BillPrintSettingsTable.FooterNoteColumn.ColumnName, ps.FooterNote);
                qb.ConditionBuilder.And(BillPrintSettingsTable.AccountIdColumn, ps.AccountId);
                qb.ConditionBuilder.And(BillPrintSettingsTable.InstanceIdColumn, ps.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return ps;
            }
            else
            {
                return await Insert(ps, BillPrintSettingsTable.Table);
            }
        }

        ////Newly created by Manivannan on 29-Apr-2017
        //public async Task SaveDraft(string accountId, string instanceId, string draftName, string salesJSON)
        //{
        //    var draftSales = new DraftSales();
        //    draftSales.Id = Guid.NewGuid().ToString();
        //    draftSales.AccountId = accountId;
        //    draftSales.InstanceId = instanceId;
        //    draftSales.CreatedAt = CustomDateTime.IST;
        //    draftSales.UpdatedAt = CustomDateTime.IST;
        //    draftSales.CreatedBy = user.UserId();
        //    draftSales.UpdatedBy = user.UserId();
        //    var draft = await Insert(draftSales, DraftSalesTable.Table);
        //}

        ////Newly created by Manivannan on 29-Apr-2017
        //public async Task<List<DraftSales>> GetDrafts(string accountId, string instanceId)
        //{
        //    var query = "Select Id, DraftName from DraftSales where AccountId = @AccountId and InstanceId = @InstanceId";
        //    var qb = QueryBuilderFactory.GetQueryBuilder(query);
        //    qb.Parameters.Add(DraftSalesTable.AccountIdColumn.ColumnName, accountId);
        //    qb.Parameters.Add(DraftSalesTable.InstanceIdColumn.ColumnName, instanceId);

        //    return await List<DraftSales>(qb);
        //}

        ////Newly created by Manivannan on 29-Apr-2017
        //public async Task<DraftSales> LoadDraft(string accountId, string instanceId, string draftId)
        //{
        //    var query = "Select Id, DraftName, SalesJSON from DraftSales where AccountId = @AccountId and InstanceId = @InstanceId";
        //    var qb = QueryBuilderFactory.GetQueryBuilder(query);
        //    qb.Parameters.Add(DraftSalesTable.AccountIdColumn.ColumnName, accountId);
        //    qb.Parameters.Add(DraftSalesTable.InstanceIdColumn.ColumnName, instanceId);
        //    qb.Parameters.Add(DraftSalesTable.IdColumn.ColumnName, draftId);

        //    return (await List<DraftSales>(qb)).FirstOrDefault();
        //}

        ////Newly created by Manivannan on 02-May-2017
        //public async Task EditDraft(string accountId, string instanceId, string draftId, string draftName)
        //{
        //    var query = "Update DraftSales Set DraftName = @DraftName where AccountId = @AccountId and InstanceId = @InstanceId and Id = @DraftId";
        //    var qb = QueryBuilderFactory.GetQueryBuilder(query);
        //    qb.Parameters.Add(DraftSalesTable.DraftNameColumn.ColumnName, draftName);
        //    qb.Parameters.Add(DraftSalesTable.AccountIdColumn.ColumnName, accountId);
        //    qb.Parameters.Add(DraftSalesTable.InstanceIdColumn.ColumnName, instanceId);
        //    qb.Parameters.Add(DraftSalesTable.IdColumn.ColumnName, draftId);

        //    await QueryExecuter.NonQueryAsyc(qb);
        //}

        ////Newly created by Manivannan on 02-May-2017
        //public async Task DeleteDraft(string accountId, string instanceId, string draftId)
        //{
        //    var query = "Delete from DraftSales where AccountId = @AccountId and InstanceId = @InstanceId and Id = @DraftId";
        //    var qb = QueryBuilderFactory.GetQueryBuilder(query);
        //    qb.Parameters.Add(DraftSalesTable.AccountIdColumn.ColumnName, accountId);
        //    qb.Parameters.Add(DraftSalesTable.InstanceIdColumn.ColumnName, instanceId);
        //    qb.Parameters.Add(DraftSalesTable.IdColumn.ColumnName, draftId);

        //    await QueryExecuter.NonQueryAsyc(qb);
        //}

        ////Newly created by Manivannan on 02-May-2017
        //public async Task<List<DraftSales>> GetWallets(string accountId, string instanceId)
        //{
        //    var query = "Select Id, PaymentType from PaymentType where PaymentCategory = 'Wallet'";
        //    var qb = QueryBuilderFactory.GetQueryBuilder(query);

        //    return await List<DraftSales>(qb);
        //}

        //Newly created by Manivannan on 24-Apr-2017
        public async Task<IEnumerable<SalesPayment>> GetSalesPayments(string accountId, string instanceId, string salesId)
        {
            var query = "Select * from SalesPayment where AccountId=@AccountId and InstanceId=@InstanceId and SalesId=@SalesId And isActive = 1";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.Parameters.Add(SalesPaymentTable.AccountIdColumn.ColumnName, accountId);
            qb.Parameters.Add(SalesPaymentTable.InstanceIdColumn.ColumnName, instanceId);
            qb.Parameters.Add(SalesPaymentTable.SalesIdColumn.ColumnName, salesId);

            return await List<SalesPayment>(qb);
        }

        //Added by Sarubala on 28-10-2017
        public async Task InactiveSalesPayments(Sales data)
        {
            var cmdUpdate = "Update SalesPayment Set isActive = 0 where AccountId = @AccountId and InstanceId=@InstanceId and SalesId=@SalesId";
            var qb = QueryBuilderFactory.GetQueryBuilder(cmdUpdate);
            qb.Parameters.Add(SalesPaymentTable.AccountIdColumn.ColumnName, data.AccountId);
            qb.Parameters.Add(SalesPaymentTable.InstanceIdColumn.ColumnName, data.InstanceId);
            qb.Parameters.Add(SalesPaymentTable.SalesIdColumn.ColumnName, data.Id);
            if (data.WriteExecutionQuery)
            {
                data.AddExecutionQuery(qb);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qb);
            }
        }

        //Added by Sarubala on 13-11-17
        public async Task<List<SalesPayment>> SaveSalesPayment(List<SalesPayment> data, IEnumerable<SalesPayment> existingSalesPayments)
        {
            if (existingSalesPayments.Count() > 0)
            {
                foreach (var item in existingSalesPayments)
                {
                    var cmdUpdate = "Update SalesPayment Set isActive = 0 where AccountId = @AccountId and InstanceId=@InstanceId and Id = @Id";
                    var qb = QueryBuilderFactory.GetQueryBuilder(cmdUpdate);
                    qb.Parameters.Add(SalesPaymentTable.AccountIdColumn.ColumnName, item.AccountId);
                    qb.Parameters.Add(SalesPaymentTable.InstanceIdColumn.ColumnName, item.InstanceId);
                    qb.Parameters.Add(SalesPaymentTable.IdColumn.ColumnName, item.Id);
                    if (item.WriteExecutionQuery)
                    {
                        item.AddExecutionQuery(qb);
                    }
                    else
                    {
                        await QueryExecuter.NonQueryAsyc(qb);
                    }
                }
            }

            foreach (var temp in data)
            {
                if (temp.WriteExecutionQuery)
                {
                    SetInsertMetaData(temp, SalesPaymentTable.Table);
                    WriteInsertExecutionQuery(temp, SalesPaymentTable.Table);
                }
                else
                {
                    await Insert(temp, SalesPaymentTable.Table);
                }
            }

            return data;
        }

        //Newly created by Manivannan on 24-Apr-2017
        public async Task<List<SalesPayment>> SaveSalesPayment_old(List<SalesPayment> data, IEnumerable<SalesPayment> existingSalesPayments)
        //public async Task<List<SalesPayment>> SaveSalesPayment(List<SalesPayment> data)
        {
            //var list = data.Select(async (x) =>
            //{
            //    IEnumerable<SalesPayment> payment = null;

            //    if (existingSalesPayments != null)
            //        payment = existingSalesPayments.Where(y => y.PaymentInd == x.PaymentInd);

            //    if (payment.Count() == 0)
            //        return await Insert(x, SalesPaymentTable.Table);
            //    else
            //    {
            //        x.Id = payment.First().Id;
            //        return await Update(x, SalesPaymentTable.Table);
            //    }

            //});

            if (data != null && existingSalesPayments != null && data.Count > 0 && existingSalesPayments.Count() > 0) //Altered by Sarubala on 11-11-17
            {
                var cmdUpdate = "Update SalesPayment Set isActive = 0 where AccountId = @AccountId and InstanceId=@InstanceId and SalesId=@SalesId";
                var qb = QueryBuilderFactory.GetQueryBuilder(cmdUpdate);
                qb.Parameters.Add(SalesPaymentTable.AccountIdColumn.ColumnName, data.First().AccountId);
                qb.Parameters.Add(SalesPaymentTable.InstanceIdColumn.ColumnName, data.First().InstanceId);
                qb.Parameters.Add(SalesPaymentTable.SalesIdColumn.ColumnName, data.First().SalesId);
                await QueryExecuter.NonQueryAsyc(qb);
            }

            var list = data.Select(async (x) =>
            {
                if (existingSalesPayments != null)
                {
                    var existPayment = getExistingPayment(x, existingSalesPayments);
                    if (existPayment.Count() > 0)
                    {
                        var cmdUpdate = "Update SalesPayment Set isActive = 1 where AccountId = @AccountId and InstanceId=@InstanceId and Id = @Id";
                        var qb = QueryBuilderFactory.GetQueryBuilder(cmdUpdate);
                        qb.Parameters.Add(SalesPaymentTable.AccountIdColumn.ColumnName, existPayment.First().AccountId);
                        qb.Parameters.Add(SalesPaymentTable.InstanceIdColumn.ColumnName, existPayment.First().InstanceId);
                        qb.Parameters.Add(SalesPaymentTable.IdColumn.ColumnName, existPayment.First().Id);
                        await QueryExecuter.NonQueryAsyc(qb);
                        return x;
                    }
                    else
                        return await Insert(x, SalesPaymentTable.Table);
                }
                else
                {
                    return await Insert(x, SalesPaymentTable.Table);
                }
            });

            return (await Task.WhenAll(list)).ToList();
        }

        //Newly created by Manivannan on 25-Apr-2017
        private IEnumerable<SalesPayment> getExistingPayment(SalesPayment payment, IEnumerable<SalesPayment> existingPayments)
        {
            var list = existingPayments.Where(x => x.PaymentInd == payment.PaymentInd && x.SubPaymentInd == payment.SubPaymentInd && x.Amount == payment.Amount && x.CardNo == payment.CardNo && x.CardDigits == payment.CardDigits && x.CardName == payment.CardName && x.CardDate == payment.CardDate && x.CardTransId == payment.CardTransId && x.ChequeNo == payment.ChequeNo && x.ChequeDate == payment.ChequeDate && x.WalletTransId == payment.WalletTransId); //Modified by Sarubala on 26-10-17

            return list;
        }

        //Newly Added Shortcut keys setting Gavaskar 03-02-2017 Start
        public async Task<SaleSettings> SaveShortCutKeySetting(SaleSettings SaleSettings)
        {
            var count = await CountShortCutKeySetting(SaleSettings);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.ShortCutKeysTypeColumn, SaleSettingsTable.NewWindowTypeColumn);
                qb.Parameters.Add(SaleSettingsTable.ShortCutKeysTypeColumn.ColumnName, SaleSettings.ShortCutKeysType);
                qb.Parameters.Add(SaleSettingsTable.NewWindowTypeColumn.ColumnName, SaleSettings.NewWindowType);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, SaleSettings.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, SaleSettings.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                //await UpdateNonQueryAsyc(qb);
                return SaleSettings;
            }
            else
            {
                return await Insert(SaleSettings, SaleSettingsTable.Table);
            }
        }
        public async Task<int> CountShortCutKeySetting(SaleSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<SaleSettings> GetShortCutKeySetting(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.ShortCutKeysTypeColumn, SaleSettingsTable.NewWindowTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<int> CountPrintStatus(BillPrintSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(BillPrintSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(BillPrintSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(BillPrintSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        //Newly Added Shortcut keys setting Gavaskar 03-02-2017 End
        public async Task<int> CountBatchType(BatchListSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(BatchListSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(BatchListSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(BatchListSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        //Newly Added Gavaskar 24-10-2016 End
        public async Task<SalesType> GetSalesType(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTypeTable.Table, OperationType.Select);
            qb.AddColumn(SalesTypeTable.IdColumn, SalesTypeTable.NameColumn);
            qb.ConditionBuilder.And(SalesTypeTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SalesTypeTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(SalesTypeTable.IsActiveColumn, 1);
            qb.ConditionBuilder.And(SalesTypeTable.StatusColumn, 1);
            qb.SelectBuilder.SortBy(SalesTypeTable.CreatedAtColumn);
            var slist = new SalesType();
            slist.SalesTypeList = await List<SalesType>(qb);
            return slist;
        }

        //Added by Sarubala on 05-10-17
        public async Task<List<DomainValues>> GetDomainValues(string AccId, string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DomainValuesTable.Table, OperationType.Select);
            qb.AddColumn(DomainValuesTable.Table.ColumnList.ToArray());
            //qb.ConditionBuilder.And(DomainValuesTable.AccountIdColumn, AccId);
            //qb.ConditionBuilder.And(DomainValuesTable.InstanceIdColumn, InsId);           
            var list1 = await List<DomainValues>(qb);
            return list1;
        }

        public async Task<SalesType> UpdateSalesType(SalesType st)
        {
            if ((await GetCount(st.Name, st.AccountId, st.InstanceId) == 0) && (await GetSalesTypeCount(st.Id) == 0))
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SalesTypeTable.Table, OperationType.Update);
                qb.AddColumn(SalesTypeTable.NameColumn, SalesTypeTable.UpdatedAtColumn, SalesTypeTable.UpdatedByColumn);
                qb.ConditionBuilder.And(SalesTypeTable.AccountIdColumn, st.AccountId);
                qb.ConditionBuilder.And(SalesTypeTable.InstanceIdColumn, st.InstanceId);
                qb.ConditionBuilder.And(SalesTypeTable.IdColumn, st.Id);
                qb.Parameters.Add(SalesTypeTable.NameColumn.ColumnName, st.Name);
                qb.Parameters.Add(SalesTypeTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(SalesTypeTable.UpdatedByColumn.ColumnName, st.AccountId);
                await QueryExecuter.NonQueryAsyc(qb);
                //await UpdateNonQueryAsyc(qb);
                return st;
            }
            else { return null; }
        }
        // Newly Added Gavaskar 08-11-2016 Start
        public async Task<InventorySmsSettings> GetSmsSetting(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InventorySmsSettingsTable.Table, OperationType.Select);
            qb.AddColumn(InventorySmsSettingsTable.SmsTypeColumn, InventorySmsSettingsTable.SmsOptionColumn, InventorySmsSettingsTable.SmsContentColumn);
            qb.ConditionBuilder.And(InventorySmsSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(InventorySmsSettingsTable.InstanceIdColumn, InstanceId);
            var smsSetting = await List<InventorySmsSettings>(qb);
            return smsSetting.FirstOrDefault();
        }
        public async Task<SalesType> GetPrescriptionSetting(SalesType st)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTypeTable.Table, OperationType.Select);
            qb.AddColumn(SalesTypeTable.IdColumn, SalesTypeTable.NameColumn, SalesTypeTable.StatusColumn, SalesTypeTable.IsActiveColumn);
            qb.ConditionBuilder.And(SalesTypeTable.AccountIdColumn, st.AccountId);
            qb.ConditionBuilder.And(SalesTypeTable.InstanceIdColumn, st.InstanceId);
            qb.ConditionBuilder.And(SalesTypeTable.IsActiveColumn, 1);
            qb.SelectBuilder.SortBy(SalesTypeTable.CreatedAtColumn);
            st.SalesTypeList = await List<SalesType>(qb);
            return st;
        }
        public async Task<List<InvoiceSeriesItem>> getInvoiceSeriesItems(InvoiceSeriesItem IS)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InvoiceSeriesItemTable.Table, OperationType.Select);
            qb.AddColumn(InvoiceSeriesItemTable.IdColumn, InvoiceSeriesItemTable.SeriesNameColumn, InvoiceSeriesItemTable.ActiveStatusColumn, InvoiceSeriesItemTable.InvoiceseriesTypeColumn);
            qb.ConditionBuilder.And(InvoiceSeriesItemTable.AccountIdColumn, IS.AccountId);
            qb.ConditionBuilder.And(InvoiceSeriesItemTable.InstanceIdColumn, IS.InstanceId);
            qb.ConditionBuilder.And(InvoiceSeriesItemTable.InvoiceseriesTypeColumn, 2);
            qb.SelectBuilder.SortBy(InvoiceSeriesItemTable.CreatedAtColumn);
            return await List<InvoiceSeriesItem>(qb);
            //IS.InvoiceSeriesItemList = await List<InvoiceSeriesItem>(qb);
        }

        //Added by Bikas on 02/07/2018
        public async Task<Patient> getCustomerIDSeriesItem(SaleSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.IdColumn, SaleSettingsTable.CustomerIDSeriesColumn, SaleSettingsTable.isEnableCustomerSeriesColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
            qb.SelectBuilder.SetTop(1).SortByDesc(SaleSettingsTable.CreatedAtColumn);

            var result = (await List<SaleSettings>(qb)).FirstOrDefault();

            string sPrefix = "";
            string PatientNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.Patient, CustomDateTime.IST, data.InstanceId, data.AccountId, sPrefix, result.CustomerIDSeries);
            var patient = new Patient();
            patient.CustomerSeriesId = PatientNo;
            patient.Prefix = result.CustomerIDSeries;
            patient.isEnableCustomerSeries = result.isEnableCustomerSeries;
            return patient;            
        }

        //Added by Sarubala on 03-12-18 - start
        public async Task<SaleSettings> getCustomerIdSeriesSettings(string AccId, string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.IdColumn, SaleSettingsTable.isEnableCustomerSeriesColumn, SaleSettingsTable.CustomerIDSeriesColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InsId);
            qb.SelectBuilder.SortByDesc(SaleSettingsTable.CreatedAtColumn);
            
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();

            if(result == null)
            {
                result = new SaleSettings();
                result.isEnableCustomerSeries = false;
            }

            return result;

        }

        public async Task<SaleSettings> saveCustomerIdSeriesSettings(SaleSettings settings)
        {
            int count1 = await CountSaleSettings(settings);

            if(count1 > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.isEnableCustomerSeriesColumn, SaleSettingsTable.CustomerIDSeriesColumn, SaleSettingsTable.UpdatedAtColumn, SaleSettingsTable.UpdatedByColumn);
                qb.Parameters.Add(SaleSettingsTable.isEnableCustomerSeriesColumn.ColumnName, settings.isEnableCustomerSeries);
                qb.Parameters.Add(SaleSettingsTable.CustomerIDSeriesColumn.ColumnName, settings.CustomerIDSeries);
                qb.Parameters.Add(SaleSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(SaleSettingsTable.UpdatedByColumn.ColumnName, settings.UpdatedBy);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, settings.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, settings.InstanceId);

                await QueryExecuter.NonQueryAsyc(qb);
            }
            else
            {
                await Insert2(settings, SaleSettingsTable.Table);
            }
            
            return settings;
        }

        //Added by Sarubala on 03-12-18 - end

        public async Task<SalesType> SaveSalesType(SalesType st)
        {
            st.Id = Guid.NewGuid().ToString();
            st.Status = false;
            st.IsActive = true;
            if (await GetCount(st.Name, st.AccountId, st.InstanceId) == 0)
            {
                return await Insert(st, SalesTypeTable.Table);
            }
            else { return null; }
        }
        public async Task<InvoiceSeriesItem> saveInvoiceSeriesItem(InvoiceSeriesItem IS)
        {
            return await Insert(IS, InvoiceSeriesItemTable.Table);
        }
        //public async Task<CustomerIDSeriesItem> saveCustomerIDSeriesItem(CustomerIDSeriesItem IS)
        //{
        //    return await Insert(IS, CustomerIDSeriesItemTable.Table);
        //}
      

        private async Task<long> GetCount(string name, string accountid, string instanceid)
        {
            long count = 0;
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTypeTable.Table, OperationType.Count);
            qb.AddColumn(SalesTypeTable.IdColumn);
            qb.ConditionBuilder.And(SalesTypeTable.NameColumn, name);
            qb.ConditionBuilder.And(SalesTypeTable.IsActiveColumn, 1);
            qb.ConditionBuilder.And(SalesTypeTable.AccountIdColumn, accountid);
            qb.ConditionBuilder.And(SalesTypeTable.InstanceIdColumn, instanceid);
            count = Convert.ToInt64(await QueryExecuter.SingleValueAsyc(qb));
            return count;
        }
        private async Task<long> GetSalesTypeCount(string id)
        {
            long count = 0;
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Count);
            qb.AddColumn(SalesTable.IdColumn);
            qb.ConditionBuilder.And(SalesTable.SalesTypeColumn, id);
            count = Convert.ToInt64(await QueryExecuter.SingleValueAsyc(qb));
            return count;
        }
        public async Task<string> RemoveSalesType(string id, string accountId)
        {
            if (await GetSalesTypeCount(id) == 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SalesTypeTable.Table, OperationType.Update);
                qb.AddColumn(SalesTypeTable.IsActiveColumn, SalesTypeTable.UpdatedAtColumn, SalesTypeTable.UpdatedByColumn);
                qb.ConditionBuilder.And(SalesTypeTable.IdColumn, id);
                qb.Parameters.Add(SalesTypeTable.IsActiveColumn.ColumnName, false);
                qb.Parameters.Add(SalesTypeTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(SalesTypeTable.UpdatedByColumn.ColumnName, accountId);
                await QueryExecuter.NonQueryAsyc(qb);
                //await UpdateNonQueryAsyc(qb);
                return id;
            }
            else { return null; }
        }
        public async Task<SalesType> SaveStatus(SalesType slist)
        {
            for (var i = 0; i < slist.SalesTypeList.Count(); i++)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SalesTypeTable.Table, OperationType.Update);
                qb.AddColumn(SalesTypeTable.StatusColumn);
                qb.ConditionBuilder.And(SalesTypeTable.IdColumn, slist.SalesTypeList[i].Id);
                qb.Parameters.Add(SalesTypeTable.StatusColumn.ColumnName, slist.SalesTypeList[i].Status);
                await QueryExecuter.NonQueryAsyc(qb);
                //await UpdateNonQueryAsyc(qb);
            }
            return slist;
        }
        public async Task<int> CountSmsSetting(InventorySmsSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InventorySmsSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(InventorySmsSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(InventorySmsSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<InventorySmsSettings> saveSmsSetting(InventorySmsSettings inventorySmsSettings)
        {
            var count = await CountSmsSetting(inventorySmsSettings);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(InventorySmsSettingsTable.Table, OperationType.Update);
                qb.AddColumn(InventorySmsSettingsTable.SmsOptionColumn, InventorySmsSettingsTable.SmsContentColumn);
                qb.Parameters.Add(InventorySmsSettingsTable.SmsOptionColumn.ColumnName, inventorySmsSettings.SmsOption);
                qb.Parameters.Add(InventorySmsSettingsTable.SmsContentColumn.ColumnName, inventorySmsSettings.SmsContent);
                qb.ConditionBuilder.And(InventorySmsSettingsTable.AccountIdColumn, inventorySmsSettings.AccountId);
                qb.ConditionBuilder.And(InventorySmsSettingsTable.InstanceIdColumn, inventorySmsSettings.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return inventorySmsSettings;
            }
            else
            {
                return await Insert(inventorySmsSettings, InventorySmsSettingsTable.Table);
            }
        }
        // Newly Added Gavaskar 08-11-2016 End
        public async Task<DiscountRules> SaveDiscount(DiscountRules data)
        {
            //if (data.Id != null || data.DiscountItem.First().Id!= null)
            //{
            //    if (data.BillAmountType != "slabDiscount")
            //    {
            //        var qb = QueryBuilderFactory.GetQueryBuilder(DiscountRulesTable.Table, OperationType.Update);
            //        qb.ConditionBuilder.And(DiscountRulesTable.IdColumn, data.Id);
            //        qb.ConditionBuilder.And(DiscountRulesTable.InstanceIdColumn, data.InstanceId);
            //        qb.AddColumn(DiscountRulesTable.AmountColumn, DiscountRulesTable.BillAmountTypeColumn, DiscountRulesTable.DiscountColumn, DiscountRulesTable.FromDateColumn, DiscountRulesTable.ToDateColumn);
            //        qb.Parameters.Add(DiscountRulesTable.AmountColumn.ColumnName, data.Amount);
            //        qb.Parameters.Add(DiscountRulesTable.BillAmountTypeColumn.ColumnName, data.BillAmountType);
            //        qb.Parameters.Add(DiscountRulesTable.DiscountColumn.ColumnName, data.Discount);
            //        qb.Parameters.Add(DiscountRulesTable.FromDateColumn.ColumnName, data.FromDate);
            //        qb.Parameters.Add(DiscountRulesTable.ToDateColumn.ColumnName, data.ToDate);
            //        await QueryExecuter.NonQueryAsyc(qb);
            //    }
            //    else
            //    {
            //        foreach (DiscountRules item in data.DiscountItem)
            //        {
            //            var qb = QueryBuilderFactory.GetQueryBuilder(DiscountRulesTable.Table, OperationType.Update);
            //            qb.ConditionBuilder.And(DiscountRulesTable.IdColumn, item.Id);
            //            qb.ConditionBuilder.And(DiscountRulesTable.InstanceIdColumn, data.InstanceId);
            //            qb.AddColumn(DiscountRulesTable.AmountColumn, DiscountRulesTable.ToAmountColumn, DiscountRulesTable.BillAmountTypeColumn, DiscountRulesTable.DiscountColumn, DiscountRulesTable.FromDateColumn, DiscountRulesTable.ToDateColumn);
            //            qb.Parameters.Add(DiscountRulesTable.AmountColumn.ColumnName, item.Amount);
            //            qb.Parameters.Add(DiscountRulesTable.ToAmountColumn.ColumnName, item.ToAmount);
            //            qb.Parameters.Add(DiscountRulesTable.BillAmountTypeColumn.ColumnName, data.BillAmountType);
            //            qb.Parameters.Add(DiscountRulesTable.DiscountColumn.ColumnName, item.Discount);
            //            qb.Parameters.Add(DiscountRulesTable.FromDateColumn.ColumnName, data.FromDate);
            //            qb.Parameters.Add(DiscountRulesTable.ToDateColumn.ColumnName, data.ToDate);
            //            await QueryExecuter.NonQueryAsyc(qb);
            //        }
            //    }
            //    return data;
            //}
            //else
            //{
            var query = $@"DELETE from DiscountRules WHERE instanceId = '{data.InstanceId}' and accountId = '{data.AccountId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            await QueryExecuter.NonQueryAsyc(qb);
            if (data.BillAmountType != "slabDiscount")
            {
                return await Insert(data, DiscountRulesTable.Table);
            }
            else
            {
                foreach (DiscountRules item in data.DiscountItem)
                {
                    if (item.Amount != null && item.ToAmount != null && item.Discount != null)
                    {
                        item.FromDate = data.FromDate;
                        item.ToDate = data.ToDate;
                        item.BillAmountType = data.BillAmountType;
                        item.InstanceId = data.InstanceId;
                        item.AccountId = data.AccountId;
                        item.CreatedAt = DateTime.Now;
                        item.CreatedBy = data.AccountId;
                        item.UpdatedAt = DateTime.Now;
                        item.UpdatedBy = data.AccountId;
                        await Insert(item, DiscountRulesTable.Table);
                    }
                }
                var data1 = await EditDiscountRules(data);
                return data1;
            }
            //}
        }
        public async Task<DiscountRules> GetDiscountRules(DiscountRules discountRules)
        {
            var query = $@"SELECT top 1 * FROM DiscountRules WHERE FromDate<=CONVERT(VARCHAR(10), GETDATE(), 120) and ToDate >=CONVERT(VARCHAR(10), GETDATE(), 120) and instanceId = '{discountRules.InstanceId}' order by CreatedAt";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = (await List<DiscountRules>(qb)).FirstOrDefault();
            if (result != null)
            {
                if (result.BillAmountType != "slabDiscount")
                    return result;
                else
                {
                    var query1 = $@"SELECT top 5 * FROM DiscountRules WHERE FromDate<=CONVERT(VARCHAR(10), GETDATE(), 120) and ToDate >=CONVERT(VARCHAR(10), GETDATE(), 120) and instanceId = '{discountRules.InstanceId}' order by CreatedAt";
                    var qb1 = QueryBuilderFactory.GetQueryBuilder(query1);
                    discountRules.DiscountItem = (await List<DiscountRules>(qb1));
                    discountRules.FromDate = discountRules.DiscountItem.First().FromDate;
                    discountRules.ToDate = discountRules.DiscountItem.First().ToDate;
                    discountRules.BillAmountType = discountRules.DiscountItem.First().BillAmountType;
                    return discountRules;
                }
            }
            else
            {
                //result = new DiscountRules();
                //result.BillAmountType = "billAmountDiscount";
                return result;
            }
        }
        public async Task<DiscountRules> EditDiscountRules(DiscountRules discountRules)
        {
            var query = $@"SELECT top 1 * FROM DiscountRules WHERE instanceId = '{discountRules.InstanceId}' order by CreatedAt";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = (await List<DiscountRules>(qb)).FirstOrDefault();
            if (result != null)
            {
                if (result.BillAmountType != "slabDiscount")
                    return result;
                else
                {
                    var query1 = $@"SELECT top 5 * FROM DiscountRules WHERE instanceId = '{discountRules.InstanceId}' order by CreatedAt";
                    var qb1 = QueryBuilderFactory.GetQueryBuilder(query1);
                    discountRules.DiscountItem = (await List<DiscountRules>(qb1));
                    discountRules.FromDate = discountRules.DiscountItem.First().FromDate;
                    discountRules.ToDate = discountRules.DiscountItem.First().ToDate;
                    discountRules.BillAmountType = discountRules.DiscountItem.First().BillAmountType;
                    return discountRules;
                }
            }
            else
            {
                result = new DiscountRules();
                result.BillAmountType = "billAmountDiscount";
                return result;
            }
        }
        public async Task<DiscountRules> EditDiscountRules1(DiscountRules discountRules)
        {
            if (discountRules.BillAmountType == "slabDiscount")
            {
                var query = $@"SELECT top 5 * FROM DiscountRules WHERE instanceId = '{discountRules.InstanceId}' order by CreatedAt";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                discountRules.DiscountItem = (await List<DiscountRules>(qb));
                if (discountRules.DiscountItem.Count() > 0)
                {
                    discountRules.FromDate = discountRules.DiscountItem.First().FromDate;
                    discountRules.ToDate = discountRules.DiscountItem.First().ToDate;
                }
                return discountRules;
            }
            else
            {
                var query = $@"SELECT top 1 * FROM DiscountRules WHERE instanceId = '{discountRules.InstanceId}' order by CreatedAt";
                var qb = QueryBuilderFactory.GetQueryBuilder(query);
                var result = (await List<DiscountRules>(qb)).FirstOrDefault();
                if (result == null)
                {
                    result = new DiscountRules();
                    result.BillAmountType = discountRules.BillAmountType;
                }
                return result;
            }
        }
        public async Task<string> GetTotalTransactionSummary(string instanceId)
        {
            var summary = new StringBuilder();
            var query = string.Format(@"SELECT	'Total Sales' Description,
		                                            ISNuLL(cast(round(sum((ps.sellingprice * si.Quantity) * ps.vat /100 + (ps.sellingprice * si.Quantity) - (ps.sellingprice * si.Quantity) * s.discount /100),2) as numeric(18,2)),0) Amount
		                                            , 1 sno
                                            FROM    Sales s INNER JOIN SalesItem si ON si.SalesId = s.id
	                                                INNER JOIN ProductStock ps ON si.productstockId = ps.id 
                                            WHERE   s.InstanceId ='{0}'
                                            AND     CONVERT(VARCHAR(10),s.CreatedAt,110) = CONVERT(VARCHAR(10),GETDATE(),110)
                                            UNION
                                            SELECT	'Sales Returns' Description,
		                                            ISNuLL(cast(round(sum((ps.sellingprice * si.Quantity) * ps.vat /100 + (ps.sellingprice * si.Quantity)),2) as numeric(18,2)),0) Amount
		                                            , 2 sno
                                            FROM    SalesReturn s INNER JOIN SalesReturnItem si ON si.SalesReturnId = s.id
	                                                INNER JOIN ProductStock ps ON si.productstockId = ps.id 
                                            WHERE   s.InstanceId ='{0}'
                                            AND     CONVERT(VARCHAR(10),s.CreatedAt,110) = CONVERT(VARCHAR(10),GETDATE(),110)
                                            UNION
                                            SELECT	'Total Purchases' Description, 
		                                            ISNULL(cast(round(sum((ps.sellingprice * vpi.Quantity) * ps.vat /100 + (ps.sellingprice * vpi.Quantity) - (ps.sellingprice * vpi.Quantity) * vp.discount /100),2) as numeric(18,2)),0) Amount
		                                            , 3 sno
                                            FROM    vendorPurchase vp INNER JOIN vendorPurchaseItem vpi ON vpi.vendorpurchaseid = vp.id
	                                                INNER JOIN ProductStock ps ON vpi.productstockId = ps.id 
                                            WHERE   vp.InstanceId ='{0}'
                                            AND     CONVERT(VARCHAR(10),vp.CreatedAt,110) = CONVERT(VARCHAR(10),GETDATE(),110)
                                            UNION
                                            SELECT	'Purchases Returns' Description, 
		                                            ISNULL(cast(round(sum((ps.sellingprice * vpi.Quantity) * ps.vat /100 + (ps.sellingprice * vpi.Quantity)),2) as numeric(18,2)),0) Amount
		                                            , 4 sno
                                            FROM    vendorReturn vp INNER JOIN vendorReturnItem vpi ON vpi.vendorReturnid = vp.id
	                                                INNER JOIN ProductStock ps ON vpi.productstockId = ps.id 
                                            WHERE   vp.InstanceId ='{0}'
                                            AND     CONVERT(VARCHAR(10),vp.CreatedAt,110) = CONVERT(VARCHAR(10),GETDATE(),110)", instanceId);
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    summary.Append(reader[0]);
                    summary.Append(" : ");
                    summary.AppendLine(reader[1].ToString());
                }
            }
            return summary.ToString();
        }
        //Sabarish
        public async Task<ReturnInvoice> GetReturnListById(string salesid, string salesreturnid)
        {
            var patientDetails = await GetPatientDetails(salesid);
            var query = $@"select     sri.MrpSellingPrice as [SalesReturnItem.MrpSellingPrice], 
                                      sri.Quantity as [SalesReturnItem.Quantity],                           
                                      sri.Discount as [SalesReturnItem.Discount],
						              sr.ReturnNo as [SalesReturn.ReturnNo],
                                      sr.CreatedAt as [SalesReturn.ReturnDate],
						              ps.BatchNo as [ProductStock.BatchNo],
                                      ps.ExpireDate as [ProductStock.ExpireDate],
                                      ps.VAT as [ProductStock.VAT],
                                      ps.SellingPrice as [ProductStock.SellingPrice],
                                      ps.MRP as [ProductStock.MRP],
						              p.Name as [Product.Name],
                                      p.Manufacturer as [Product.Manufacturer],
						              i.Name as [Instance.Name] ,
                                      i.Phone as [Instance.Phone],
                                      i.DrugLicenseNo as [Instance.DrugLicenseNo],
                                      i.TinNo as [Instance.TinNo],
                                      i.Address as [Instance.Address],
                                      i.Area as [Instance.Area],
                                      i.City as [Instance.City],
                                      i.state as [Instance.state],
                                      i.pincode as [Instance.pincode],
                                      i.status as [Instance.status],
						              s.Name as [Sales.Name],
                                      s.Mobile as [Sales.Mobile],
                                      s.Email as [Sales.Email],
                                      s.DOB as [Sales.DOB],
                                      s.InvoiceNo as [Sales.InvoiceNo],
                                      s.InvoiceSeries as [Sales.InvoiceSeries],
                                      s.Discount as [Sales.Discount],
                                      s.InvoiceDate as [Sales.InvoiceDate],
                                      s.Address as [Sales.Address],
                                      s.PaymentType as [Sales.PaymentType],
                                      s.DoctorName as [Sales.DoctorName],
                                      s.Pincode as [Sales.Pincode],
                                      s.CreatedAt as [Sales.CreatedAt]
                                      from salesreturnitem as sri inner join salesreturn as sr on sri.salesreturnid = sr.id
			                                     left join productstock as ps on ps.id = sri.productstockid  
							         			 left join Product as p on p.Id = ps.ProductId 
							         			 left join Sales as s on s.id = sr.salesid
							         			 inner join Instance as i on s.InstanceId = i.Id 
							         			 where sr.id = '{salesreturnid}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var obj = new ReturnInvoice();
            var list = new List<ReturnInvoiceList>();
            decimal total = 0.00M;
            decimal discount = 0.00M;
            decimal roundOff = 0.00M;
            decimal netAmount = 0.00M;
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var returnInvoiceList = new ReturnInvoiceList()
                    {
                        salesReturnItemInvoice =
                        {
                            Quantity = reader[SalesReturnItemTable.QuantityColumn.FullColumnName] as decimal? ?? 0,
                            MrpSellingPrice = reader[SalesReturnItemTable.MrpSellingPriceColumn.FullColumnName] as decimal? ?? 0,
                            Discount = reader[SalesReturnItemTable.DiscountColumn.FullColumnName] as decimal? ?? 0
                        },
                        productStockInvoice = {
                                BatchNo =  reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString() ?? "",
                                ExpireDate =reader[ProductStockTable.ExpireDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                                VAT  = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                                SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                                MRP = reader[ProductStockTable.MRPColumn.FullColumnName] as decimal? ?? 0,
                        },
                        salesInvoice = {
                            Name =  reader[SalesTable.NameColumn.FullColumnName].ToString() ?? "",
                            Mobile = reader[SalesTable.MobileColumn.FullColumnName].ToString() ?? "",
                            Email = reader[SalesTable.EmailColumn.FullColumnName].ToString() ?? "",
                            DOB = reader[SalesTable.DOBColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                            InvoiceNo = reader[SalesTable.InvoiceNoColumn.FullColumnName].ToString() ?? "",
                            InvoiceSeries = reader[SalesTable.InvoiceSeriesColumn.FullColumnName].ToString() ?? "",
                            InvoiceDate = reader[SalesTable.InvoiceDateColumn.FullColumnName]as DateTime? ?? DateTime.MinValue,
                            Address = reader[SalesTable.AddressColumn.FullColumnName].ToString() ?? "",
                            PaymentType = reader[SalesTable.PaymentTypeColumn.FullColumnName].ToString() ?? "",
                            DoctorName = reader[SalesTable.DoctorNameColumn.FullColumnName].ToString() ?? "",
                            Discount = reader[SalesTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
                            Pincode = reader[SalesTable.PincodeColumn.FullColumnName].ToString() ?? "",
                            CreatedAt = reader[SalesTable.CreatedAtColumn.FullColumnName]as DateTime? ?? DateTime.MinValue
                        },
                        instanceInvoice = {
                                Name =  reader[InstanceTable.NameColumn.FullColumnName].ToString() ?? "",
                                Phone =  reader[InstanceTable.PhoneColumn.FullColumnName].ToString() ?? "",
                                DrugLicenseNo =  reader[InstanceTable.DrugLicenseNoColumn.FullColumnName].ToString() ?? "",
                                TinNo =  reader[InstanceTable.TinNoColumn.FullColumnName].ToString() ?? "",
                                Address =  reader[InstanceTable.AddressColumn.FullColumnName].ToString() ?? "",
                                Area =  reader[InstanceTable.AreaColumn.FullColumnName].ToString() ?? "",
                                City =  reader[InstanceTable.CityColumn.FullColumnName].ToString() ?? "",
                                State =  reader[InstanceTable.StateColumn.FullColumnName].ToString() ?? "",
                                Pincode =  reader[InstanceTable.PincodeColumn.FullColumnName].ToString() ?? "",
                                Status = reader[InstanceTable.StatusColumn.FullColumnName] as bool? ?? false
                        },
                        salesReturnInvoice = {
                              ReturnNo = reader[SalesReturnTable.ReturnNoColumn.FullColumnName].ToString() ?? "",
                              ReturnDate = reader[SalesReturnTable.ReturnDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                        },
                        // Added Gavaskar 13-02-2017 Start
                        //patientInvoice = {
                        //      Patient = reader[PatientTable.EmpIDColumn.FullColumnName].ToString() ?? "",
                        //},
                        // Added Gavaskar 13-02-2017 End
                        productInvoice = {
                                    Name =  reader[ProductTable.NameColumn.FullColumnName].ToString() ?? "",
                                    Manufacturer = reader[ProductTable.ManufacturerColumn.FullColumnName].ToString() ?? ""
                        }
                    };
                    decimal? price = 0.00M;
                    if (returnInvoiceList.salesReturnItemInvoice.MrpSellingPrice != 0)
                    {
                        price = returnInvoiceList.salesReturnItemInvoice.MrpSellingPrice * returnInvoiceList.salesReturnItemInvoice.Quantity;
                    }
                    if (returnInvoiceList.salesReturnItemInvoice.MrpSellingPrice == 0)
                    {
                        price = returnInvoiceList.productStockInvoice.SellingPrice * returnInvoiceList.salesReturnItemInvoice.Quantity;
                    }
                    total += price.GetValueOrDefault();
                    discount = returnInvoiceList.salesInvoice.Discount.GetValueOrDefault();
                    list.Add(returnInvoiceList);
                }
            }
            decimal? discountItemReturn = 0.00M;
            foreach (ReturnInvoiceList item in list)
            {
                discountItemReturn += (item.salesReturnItemInvoice.Quantity * (item.salesReturnItemInvoice.MrpSellingPrice != 0 ? item.salesReturnItemInvoice.MrpSellingPrice : item.productStockInvoice.SellingPrice) * item.salesReturnItemInvoice.Discount / 100);
            }
            decimal? discountedAmount = 0.00M;
            if (discount != 0)
            {
                discountedAmount = Math.Round(total * (discount / 100), MidpointRounding.AwayFromZero);
                netAmount = Math.Round(total - discountedAmount.GetValueOrDefault() - discountItemReturn.GetValueOrDefault(), MidpointRounding.AwayFromZero);
            }
            else
            {
                discountedAmount = 0.00M;
                netAmount = Math.Round(total - discountItemReturn.GetValueOrDefault(), MidpointRounding.AwayFromZero);
            }
            roundOff = netAmount - (total - discountedAmount.GetValueOrDefault() - discountItemReturn.GetValueOrDefault());
            obj.returnInvoiceList = list;
            obj.total = total;
            obj.roundOff = roundOff;
            obj.netAmount = netAmount;
            obj.discount = discountedAmount + discountItemReturn;
            obj.patientDetails.EmpId = patientDetails.EmpId;
            obj.patientDetails.Address = patientDetails.Address;
            obj.patientDetails.City = patientDetails.City;
            obj.patientDetails.Pincode = patientDetails.Pincode;
            return obj;
        }

        public async Task<ReturnInvoice> GetReturnListWithoutSaleById(string salesreturnid)
        {
            var patientDetails = await GetPatientDetailsForSalesReturn(salesreturnid);
            var query = $@"select     sri.MrpSellingPrice as [SalesReturnItem.MrpSellingPrice], 
                                      sri.Quantity as [SalesReturnItem.Quantity],                           
                                      sri.Discount as [SalesReturnItem.Discount],
						              sr.ReturnNo as [SalesReturn.ReturnNo],
                                      sr.CreatedAt as [SalesReturn.ReturnDate],
                                      sr.TaxRefNo as [SalesReturn.TaxRefNo],
						              ps.BatchNo as [ProductStock.BatchNo],
                                      ps.ExpireDate as [ProductStock.ExpireDate],
                                      ps.VAT as [ProductStock.VAT],
                                      ps.SellingPrice as [ProductStock.SellingPrice],
                                      ps.MRP as [ProductStock.MRP],
						              p.Name as [Product.Name],
                                      p.Manufacturer as [Product.Manufacturer],
						              i.Name as [Instance.Name] ,
                                      i.Phone as [Instance.Phone],
                                      i.DrugLicenseNo as [Instance.DrugLicenseNo],
                                      i.TinNo as [Instance.TinNo],
                                      i.Address as [Instance.Address],
                                      i.Area as [Instance.Area],
                                      i.City as [Instance.City],
                                      i.state as [Instance.state],
                                      i.pincode as [Instance.pincode],
                                      i.status as [Instance.status]
                                      from salesreturnitem as sri inner join salesreturn as sr on sri.salesreturnid = sr.id
			                                     left join productstock as ps on ps.id = sri.productstockid  
							         			 left join Product as p on p.Id = ps.ProductId 							         			 
							         			 inner join Instance as i on sr.InstanceId = i.Id 
							         			 where sr.id = '{salesreturnid}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var obj = new ReturnInvoice();
            var list = new List<ReturnInvoiceList>();
            decimal total = 0.00M;
            decimal discount = 0.00M;
            decimal roundOff = 0.00M;
            decimal netAmount = 0.00M;
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var returnInvoiceList = new ReturnInvoiceList()
                    {
                        salesReturnItemInvoice =
                        {
                            Quantity = reader[SalesReturnItemTable.QuantityColumn.FullColumnName] as decimal? ?? 0,
                            MrpSellingPrice = reader[SalesReturnItemTable.MrpSellingPriceColumn.FullColumnName] as decimal? ?? 0,
                            Discount = reader[SalesReturnItemTable.DiscountColumn.FullColumnName] as decimal? ?? 0
                        },
                        productStockInvoice = {
                                BatchNo =  reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString() ?? "",
                                ExpireDate =reader[ProductStockTable.ExpireDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                                VAT  = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                                SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0,
                                MRP = reader[ProductStockTable.MRPColumn.FullColumnName] as decimal? ?? 0,
                        },
                        salesInvoice = {
                            Name =  patientDetails != null ? patientDetails.Name : "",
                            Mobile = patientDetails != null ? patientDetails.Mobile : ""
                        },
                        instanceInvoice = {
                                Name =  reader[InstanceTable.NameColumn.FullColumnName].ToString() ?? "",
                                Phone =  reader[InstanceTable.PhoneColumn.FullColumnName].ToString() ?? "",
                                DrugLicenseNo =  reader[InstanceTable.DrugLicenseNoColumn.FullColumnName].ToString() ?? "",
                                TinNo =  reader[InstanceTable.TinNoColumn.FullColumnName].ToString() ?? "",
                                Address =  reader[InstanceTable.AddressColumn.FullColumnName].ToString() ?? "",
                                Area =  reader[InstanceTable.AreaColumn.FullColumnName].ToString() ?? "",
                                City =  reader[InstanceTable.CityColumn.FullColumnName].ToString() ?? "",
                                State =  reader[InstanceTable.StateColumn.FullColumnName].ToString() ?? "",
                                Pincode =  reader[InstanceTable.PincodeColumn.FullColumnName].ToString() ?? "",
                                Status = reader[InstanceTable.StatusColumn.FullColumnName] as bool? ?? false
                        },
                        salesReturnInvoice = {
                              ReturnNo = reader[SalesReturnTable.ReturnNoColumn.FullColumnName].ToString() ?? "",
                              ReturnDate = reader[SalesReturnTable.ReturnDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                              TaxRefNo = reader[SalesReturnTable.TaxRefNoColumn.FullColumnName] as int? ?? 0
                        },
                        productInvoice = {
                                    Name =  reader[ProductTable.NameColumn.FullColumnName].ToString() ?? "",
                                    Manufacturer = reader[ProductTable.ManufacturerColumn.FullColumnName].ToString() ?? ""
                        }
                    };
                    decimal? price = 0.00M;
                    if (returnInvoiceList.salesReturnItemInvoice.MrpSellingPrice != 0)
                    {
                        price = returnInvoiceList.salesReturnItemInvoice.MrpSellingPrice * returnInvoiceList.salesReturnItemInvoice.Quantity;
                    }
                    if (returnInvoiceList.salesReturnItemInvoice.MrpSellingPrice == 0)
                    {
                        price = returnInvoiceList.productStockInvoice.SellingPrice * returnInvoiceList.salesReturnItemInvoice.Quantity;
                    }
                    total += price.GetValueOrDefault();
                    discount = returnInvoiceList.salesInvoice.Discount.GetValueOrDefault();
                    list.Add(returnInvoiceList);
                }
            }

            foreach (var item in list)
            {
                item.salesInvoice.InvoiceNo = item.salesReturnInvoice.ReturnNo;
                item.salesInvoice.InvoiceDate = item.salesReturnInvoice.ReturnDate;
                item.salesInvoice.CreatedAt = item.salesReturnInvoice.ReturnDate;
                item.salesInvoice.PaymentType = "";
            }

            decimal? discountItemReturn = 0.00M;
            foreach (ReturnInvoiceList item in list)
            {
                discountItemReturn += (item.salesReturnItemInvoice.Quantity * (item.salesReturnItemInvoice.MrpSellingPrice != 0 ? item.salesReturnItemInvoice.MrpSellingPrice : item.productStockInvoice.SellingPrice) * item.salesReturnItemInvoice.Discount / 100);
            }
            decimal? discountedAmount = 0.00M;
            if (discount != 0)
            {
                discountedAmount = Math.Round(total * (discount / 100), MidpointRounding.AwayFromZero);
                netAmount = Math.Round(total - discountedAmount.GetValueOrDefault() - discountItemReturn.GetValueOrDefault(), MidpointRounding.AwayFromZero);
            }
            else
            {
                discountedAmount = 0.00M;
                netAmount = Math.Round(total - discountItemReturn.GetValueOrDefault(), MidpointRounding.AwayFromZero);
            }
            roundOff = netAmount - (total - discountedAmount.GetValueOrDefault() - discountItemReturn.GetValueOrDefault());
            obj.returnInvoiceList = list;
            obj.total = total;
            obj.roundOff = roundOff;
            obj.netAmount = netAmount * (-1);
            obj.discount = discountedAmount + discountItemReturn;
            obj.patientDetails.EmpId = patientDetails != null ? patientDetails.EmpID : "";
            obj.patientDetails.Address = patientDetails != null ? patientDetails.Address : "";
            obj.patientDetails.City = patientDetails != null ? patientDetails.City : "";
            obj.patientDetails.Pincode = patientDetails != null ? patientDetails.Pincode : "";
            return obj;
        }
        public async Task<PatientDetails> GetPatientDetails(string salesid)
        {
            PatientDetails patient = new PatientDetails();
            var Sale = await GetSaleById(salesid);
            if (!string.IsNullOrEmpty(Sale.Mobile) || !string.IsNullOrEmpty(Sale.Name))
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
                qb.AddColumn(PatientTable.Table.ColumnList.ToArray());
                qb.ConditionBuilder.And(PatientTable.NameColumn, Sale.Name);
                qb.ConditionBuilder.And(PatientTable.MobileColumn, Sale.Mobile);
                var result = (await List<Patient>(qb)).FirstOrDefault();
                patient.EmpId = result != null ? result.EmpID : string.Empty;
                patient.Address = result != null ? result.Address : string.Empty;
                patient.City = result != null ? result.City : string.Empty;
                patient.Pincode = result != null ? result.Pincode : string.Empty;
            }
            else
            {
                patient.EmpId = string.Empty;
            }
            return patient;
        }
        public async Task<Sales> GetSaleById(string salesid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(SalesTable.IdColumn, salesid);
            return (await List<Sales>(qb)).FirstOrDefault();
        }

        public async Task<Patient> GetPatientDetailsForSalesReturn(string salesreturnid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.AddColumn(PatientTable.Table.ColumnList.ToArray());
            qb.JoinBuilder.Join(SalesReturnTable.Table, SalesReturnTable.PatientIdColumn, PatientTable.IdColumn);
            qb.ConditionBuilder.And(SalesReturnTable.IdColumn, salesreturnid);
            var patient = (await List<Patient>(qb)).FirstOrDefault();

            return patient;
        }

        public async Task<string> GetReturnWithoutSaleById(string id)
        {
            SalesReturn sreturn = new SalesReturn();
            if (!string.IsNullOrEmpty(id))
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Select);
                //qb.AddColumn(SalesReturnTable.Table.ColumnList.ToArray());
                qb.AddColumn(SalesReturnTable.PatientIdColumn);
                qb.ConditionBuilder.And(SalesReturnTable.IdColumn, id);
                var result = (await List<SalesReturn>(qb)).FirstOrDefault();
                sreturn.PatientId = result != null ? result.PatientId.ToString() : string.Empty;
            }
            return sreturn.PatientId;
        }

        public async Task<ProductStock> GetTotalStock(string productstockId, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.AddColumn(ProductStockTable.StockColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn, productstockId);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceid);
            return (await List<ProductStock>(qb)).FirstOrDefault();
        }
        //public async Task<List<ProductStock>> GetTotalStock(string productId, string instanceid, string batchno)
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
        //    qb.AddColumn(ProductStockTable.Table.ColumnList.ToArray());
        //    qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, productId);
        //    qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceid);
        //    qb.ConditionBuilder.And(ProductStockTable.BatchNoColumn, batchno);
        //    qb.ConditionBuilder.And(ProductStockTable.StockColumn, CriteriaEquation.Greater);
        //    qb.Parameters.Add(ProductStockTable.StockColumn.ColumnName, 0);
        //    qb.SelectBuilder.SortBy(ProductStockTable.StockColumn);
        //    return await List<ProductStock>(qb);
        //}
        public async Task<Boolean> getOfflineStatus(string accountId, string instanceId)
        {
            return await getOfflineStatusValue(accountId, instanceId);
        }

        //Added by Sarubala on 18-05-2018
        public async Task<Boolean> getOnlineEnabledStatus(string accountId, string userId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(HQueUserTable.Table, OperationType.Select);
            qb.AddColumn(HQueUserTable.IsOnlineEnableColumn);
            qb.ConditionBuilder.And(HQueUserTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(HQueUserTable.UserIdColumn, userId);
            var onlineEnabledStatus = await SingleValueAsyc(qb);
            if (onlineEnabledStatus == DBNull.Value)
            {
                onlineEnabledStatus = 0;
            }
            return Convert.ToBoolean(onlineEnabledStatus);
        }

        //Added by Sarubala on 23-10-17
        public async Task<Instance> GetInstanceValues(string accId, string insId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.AddColumn(InstanceTable.OfflineStatusColumn, InstanceTable.GsTinNoColumn, InstanceTable.GstselectColumn);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, insId);
            var result = (await List<Instance>(qb)).FirstOrDefault();

            return result;
        }

        public async Task<List<ProductStock>> searchSales(DateTime fromdate, DateTime todate, decimal reOrderFactor, string AccountId, string InstanceId, int type, bool loadAllProducts)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("InstanceId", InstanceId);
            parms.Add("AccountId", AccountId);
            parms.Add("fromDate", fromdate);
            parms.Add("ToDate", todate);
            parms.Add("multiplyfactor", reOrderFactor);
            parms.Add("type", type);
            var productlist = await sqldb.ExecuteProcedureAsync<dynamic>("usp_SearchSales", parms);
            var data = productlist.Select(item =>
            {
                var productstock = new ProductStock()
                {
                    Product = { Id = item.Productid, Name = item.productname },
                    PurchasePrice = item.Purchaseprice,
                    VAT = item.vat,
                    Igst = item.igst as decimal? ?? 0,
                    Cgst = item.cgst as decimal? ?? 0,
                    Sgst = item.sgst as decimal? ?? 0,
                    GstTotal = item.gstTotal as decimal? ?? 0,
                    soldqty = item.soldqty,
                    ReOrderQty = item.orderQty,
                    VendorId = item.vendorid,
                    PackageSize = item.packageSize,
                    Stock = item.stock,
                    RequiredQty = item.ReqQty,
                    OrderQty = item.ReqQty - item.stock,
                    SellingPrice = item.SellingPrice,
                    ExpireDate = item.ExpireDate,
                    pendingPOQty = item.pendingPOQty as decimal? ?? 0,
                    TranQty = item.TransQty


                };
                if (productstock.OrderQty % productstock.PackageSize == 0)
                {
                    productstock.packageQty = productstock.OrderQty / productstock.PackageSize;
                }
                else
                {
                    productstock.packageQty = (productstock.OrderQty + productstock.PackageSize - (productstock.OrderQty % productstock.PackageSize)) / productstock.PackageSize;
                }
                return productstock;
            }).Where(a => a.Stock < a.RequiredQty || loadAllProducts == true).OrderByDescending(xx => (xx.RequiredQty - xx.Stock));
            var dataList = data.ToList();
            return dataList;
        }
        public async Task<SaleSettings> saveShortName(SaleSettings SaleSettings)
        {
            var count = await CountShortCutKeySetting(SaleSettings);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.ShortCustomerNameColumn, SaleSettingsTable.ShortDoctorNameColumn);
                qb.Parameters.Add(SaleSettingsTable.ShortCustomerNameColumn.ColumnName, SaleSettings.ShortCustomerName);
                qb.Parameters.Add(SaleSettingsTable.ShortDoctorNameColumn.ColumnName, SaleSettings.ShortDoctorName);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, SaleSettings.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, SaleSettings.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                //await UpdateNonQueryAsyc(qb);
                return SaleSettings;
            }
            else
            {
                return await Insert(SaleSettings, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> getShortName(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.ShortCustomerNameColumn, SaleSettingsTable.ShortDoctorNameColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<bool> GetShortDoctorName(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.ShortDoctorNameColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var shortDoctor = (await List<SaleSettings>(qb)).FirstOrDefault();
            if (shortDoctor != null && shortDoctor.ShortDoctorName != null)
            {
                if (shortDoctor.ShortDoctorName == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> GetShortCustomerName(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.ShortCustomerNameColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var shortCustomer = (await List<SaleSettings>(qb)).FirstOrDefault();
            if (shortCustomer != null && shortCustomer.ShortCustomerName != null)
            {
                if (shortCustomer.ShortCustomerName == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public async Task<SaleSettings> saveCompleteSaleKeys(SaleSettings data)
        {
            var count = await CountCompleteSaleType(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.CompleteSaleKeyTypeColumn);
                qb.Parameters.Add(SaleSettingsTable.CompleteSaleKeyTypeColumn.ColumnName, data.CompleteSaleKeyType);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                //await UpdateNonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> getScanBarcodeOption(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            /*InstanceId Added by Poongodi on 31/07/2017*/
            qb.AddColumn(SaleSettingsTable.InstanceIdColumn, SaleSettingsTable.ScanBarcodeColumn, SaleSettingsTable.AutoScanQtyColumn, SaleSettingsTable.ShowSalesItemHistoryColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            var result = (await List<SaleSettings>(qb));
            var res = result.Where(x => x.ShowSalesItemHistory == true).FirstOrDefault();
            var ShowSalesItemHistory = res == null ? false : res.ShowSalesItemHistory;
            var result1 = result.Where(x => x.InstanceId == InstanceId).FirstOrDefault();
            if (result1 == null)
            {
                result1 = new SaleSettings();
                result1.SetLoggedUserDetails(user);
                if (_configHelper.AppConfig.OfflineMode == false)
                    result1 = await DefaultSaleSettingsInsert(result1);
            }
            result1.ShowSalesItemHistory = ShowSalesItemHistory;

            return result1;
        }
        public async Task<SaleSettings> saveBarcodeOption(SaleSettings barcodeOption)
        {
            var count = await CountCancelBillDays(barcodeOption);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.ScanBarcodeColumn, SaleSettingsTable.AutoScanQtyColumn);
                qb.Parameters.Add(SaleSettingsTable.ScanBarcodeColumn.ColumnName, barcodeOption.ScanBarcode);
                qb.Parameters.Add(SaleSettingsTable.AutoScanQtyColumn.ColumnName, barcodeOption.AutoScanQty);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, barcodeOption.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, barcodeOption.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                //await UpdateNonQueryAsyc(qb);
                return barcodeOption;
            }
            else
            {
                return await Insert(barcodeOption, SaleSettingsTable.Table);
            }
        }
        public async Task<int> CountCompleteSaleType(SaleSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<string> getCompleteSaleType(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.CompleteSaleKeyTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var CompleteSales = await List<SaleSettings>(qb);
            var CompleteSale = "";
            if (CompleteSales.Count > 0)
            {
                CompleteSale = CompleteSales.First().CompleteSaleKeyType;
            }
            return CompleteSale;
        }
        public async Task<SaleSettings> getCompleteSaleKeys(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.CompleteSaleKeyTypeColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<SaleSettings> getCustomCreditSeriesSelected(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.CustomSeriesInvoiceColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var result = (await List<SaleSettings>(qb)).FirstOrDefault();
            return result;
        }
        //By San
        public async Task<Sales> getLastSalesId(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.IdColumn, SalesTable.DiscountTypeColumn);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, InstanceId);
            //qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);
            qb.SelectBuilder.SetTop(1).SortByDesc(SalesTable.CreatedAtColumn);
            var result = (await List<Sales>(qb)).FirstOrDefault();
            if (result != null)
            {
                if (result.DiscountType == null)
                {
                    result.DiscountType = 1;
                }
            }

            return result;
        }
        /// <summary>
        /// To GetTop 1 Custom series type
        /// </summary>
        /// <param name="AccId"></param>
        /// <param name="InsId"></param>
        /// <returns></returns>
        ///  Added on 26/04/2017       
        public async Task<string> getInvoiceSeriesItemForPurchaseSettings(string AccountId, string InstanceId)
        {
            string query = $@"select top 1 SeriesName from InvoiceSeriesItem where AccountId = '{ AccountId }' and InstanceId = '{ InstanceId }'and InvoiceseriesType = 2 and isnull(SeriesName,'') != '' order by CreatedAt";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = (await QueryExecuter.SingleValueAsyc(qb));
            if (result != null)
            {
                return result.ToString();
            }
            else
            {
                return null;
            }
        }
        //By San - 30-05-2017
        public async Task<SalesReturn> GetLastReturnId(string userId, string instannceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesReturnTable.Table, OperationType.Select);
            qb.AddColumn(SalesReturnTable.IdColumn);
            qb.ConditionBuilder.And(SalesReturnTable.CreatedByColumn, userId);
            qb.ConditionBuilder.And(SalesReturnTable.InstanceIdColumn, instannceId);
            //qb.SelectBuilder.SortByDesc(SalesTable.CreatedAtColumn);
            qb.SelectBuilder.SetTop(1).SortByDesc(SalesReturnTable.CreatedAtColumn);
            var result = (await List<SalesReturn>(qb)).FirstOrDefault();
            return result;
        }

        //#region Loyalty Points  Functionalty
        public async Task<LoyaltyPointSettings> getLoyaltyPointSettings(string AccountId, string InstanceId)
        {
            // var qb = QueryBuilderFactory.GetQueryBuilder(LoyaltyPointSettingsTable.Table, OperationType.Select);
            // qb.AddColumn(LoyaltyPointSettingsTable.IdColumn, LoyaltyPointSettingsTable.LoyaltySaleValueColumn, LoyaltyPointSettingsTable.LoyaltyPointColumn, LoyaltyPointSettingsTable.StartDateColumn, LoyaltyPointSettingsTable.EndDateColumn, LoyaltyPointSettingsTable.MinimumSalesAmtColumn, LoyaltyPointSettingsTable.RedeemPointColumn, LoyaltyPointSettingsTable.RedeemValueColumn, LoyaltyPointSettingsTable.LoyaltyOptionColumn, LoyaltyPointSettingsTable.IsMinimumSaleColumn, LoyaltyPointSettingsTable.IsEndDateColumn, LoyaltyPointSettingsTable.MaximumRedeemPointColumn, LoyaltyPointSettingsTable.IsMaximumRedeemColumn);
            // qb.ConditionBuilder.And(LoyaltyPointSettingsTable.AccountIdColumn, AccountId);
            // qb.ConditionBuilder.And(LoyaltyPointSettingsTable.InstanceIdColumn, InstanceId);
            // qb.ConditionBuilder.And(LoyaltyPointSettingsTable.IsActiveColumn, 1);
            // qb.ConditionBuilder.And(LoyaltyPointSettingsTable.LoyaltyOptionColumn, 1);
            // var result = (await List<LoyaltyPointSettings>(qb)).FirstOrDefault();
            // LoyaltyPointSettings loyaltyPoints = new LoyaltyPointSettings();
            //result.LoyaltyProductPoints = await _productDataAccess.GetLoyaltyPointProductList(result.Id);

            // return result;


            Dictionary<string, object> parms = new Dictionary<string, object>();

            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetLoyaltyPointSettings", parms);
            if (result.Count() <= 0)
            { return null; }
            var salesLoyaltyPts = result.Select(x =>
            {
                var LoyaltyPointSettings = new LoyaltyPointSettings
                {
                    Id = x.Id,
                    LoyaltySaleValue = x.LoyaltySaleValue as decimal? ?? 0,
                    LoyaltyPoint = x.LoyaltyPoint as decimal? ?? 0,
                    StartDate = x.StartDate as DateTime? ?? DateTime.MinValue,
                    EndDate = x.EndDate as DateTime? ?? DateTime.MinValue,
                    MinimumSalesAmt = x.MinimumSalesAmt as decimal? ?? 0,
                    RedeemPoint = x.RedeemPoint as decimal? ?? 0,
                    RedeemValue = x.RedeemValue as decimal? ?? 0,
                    MaximumRedeemPoint = x.MaximumRedeemPoint as decimal? ?? 0,
                    IsActive = x.IsActive,
                    LoyaltyOption = x.LoyaltyOption,
                    IsMinimumSale = x.IsMinimumSale as bool? ?? false,
                    IsMaximumRedeem = x.IsMaximumRedeem as bool? ?? false,
                    IsEndDate = x.IsEndDate as bool? ?? false,

                };

                return LoyaltyPointSettings;
            });
            var getLoyaltyPointSettings = salesLoyaltyPts.FirstOrDefault();
            getLoyaltyPointSettings.LoyaltyProductPoints = await _productDataAccess.GetLoyaltyPointProductList(getLoyaltyPointSettings.Id);
            return getLoyaltyPointSettings;
        }


        public async Task<LoyaltyPointSettings> saveLoyaltyPointSettings(LoyaltyPointSettings lps)
        {
            //inActiveLoyaltySettings(lps);

            if (await CountLoyaltySettings(lps) > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(LoyaltyPointSettingsTable.Table, OperationType.Update);
                qb.AddColumn(LoyaltyPointSettingsTable.IsActiveColumn, LoyaltyPointSettingsTable.UpdatedAtColumn, LoyaltyPointSettingsTable.UpdatedByColumn);
                qb.ConditionBuilder.And(LoyaltyPointSettingsTable.AccountIdColumn, lps.AccountId);
                qb.ConditionBuilder.And(LoyaltyPointSettingsTable.InstanceIdColumn, lps.InstanceId);
                qb.Parameters.Add(LoyaltyPointSettingsTable.IsActiveColumn.ColumnName, false);
                qb.Parameters.Add(LoyaltyPointSettingsTable.LoyaltyOptionColumn.ColumnName, false);
                qb.Parameters.Add(LoyaltyPointSettingsTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                qb.Parameters.Add(LoyaltyPointSettingsTable.UpdatedByColumn.ColumnName, lps.UpdatedBy);
                await QueryExecuter.NonQueryAsyc(qb);
            }

            lps.IsActive = true;
            if (lps.MinimumSalesAmt == null)
            {
                lps.MinimumSalesAmt = 0;
            }
            var LoyaltyPointSettings = await Insert(lps, LoyaltyPointSettingsTable.Table);

            var qb1 = QueryBuilderFactory.GetQueryBuilder(LoyaltyProductPointsTable.Table, OperationType.Update);
            qb1.AddColumn(LoyaltyProductPointsTable.IsActiveColumn, LoyaltyProductPointsTable.UpdatedAtColumn, LoyaltyProductPointsTable.UpdatedByColumn);
            qb1.ConditionBuilder.And(LoyaltyProductPointsTable.AccountIdColumn, lps.AccountId);
            qb1.ConditionBuilder.And(LoyaltyProductPointsTable.InstanceIdColumn, lps.InstanceId);
            qb1.Parameters.Add(LoyaltyProductPointsTable.IsActiveColumn.ColumnName, false);
            qb1.Parameters.Add(LoyaltyProductPointsTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
            qb1.Parameters.Add(LoyaltyProductPointsTable.UpdatedByColumn.ColumnName, lps.UpdatedBy);
            await QueryExecuter.NonQueryAsyc(qb1);

            foreach (var LoyaltyProductPts in lps.LoyaltyProductPoints)
            {
                LoyaltyProductPts.AccountId = lps.AccountId;
                LoyaltyProductPts.InstanceId = lps.InstanceId;
                LoyaltyProductPts.LoyaltyId = LoyaltyPointSettings.Id;
                LoyaltyProductPts.CreatedAt = lps.CreatedAt;
                LoyaltyProductPts.CreatedBy = lps.CreatedBy;
                LoyaltyProductPts.IsActive = true;
                LoyaltyProductPts.UpdatedAt = CustomDateTime.IST;
                LoyaltyProductPts.UpdatedBy = lps.UpdatedBy;
                await Insert(LoyaltyProductPts, LoyaltyProductPointsTable.Table);
            }

            //LoyaltyPointSettings.

            return lps;
        }

        //public async Task inActiveLoyaltySettings(LoyaltyPointSettings lp)
        //{
        //    if (await CountLoyaltySettings(lp) > 0)
        //    {
        //        var qb = QueryBuilderFactory.GetQueryBuilder(LoyaltyPointSettingsTable.Table, OperationType.Update);
        //        qb.AddColumn(LoyaltyPointSettingsTable.IsActiveColumn, LoyaltyPointSettingsTable.UpdatedAtColumn, LoyaltyPointSettingsTable.UpdatedByColumn);
        //        qb.ConditionBuilder.And(LoyaltyPointSettingsTable.AccountIdColumn, lp.AccountId);
        //        qb.ConditionBuilder.And(LoyaltyPointSettingsTable.InstanceIdColumn, lp.InstanceId);
        //        //qb.ConditionBuilder.And(LoyaltyPointSettingsTable.IsActiveColumn, true);
        //        qb.Parameters.Add(LoyaltyPointSettingsTable.IsActiveColumn.ColumnName, false);
        //        qb.Parameters.Add(LoyaltyPointSettingsTable.LoyaltyOptionColumn.ColumnName, false);
        //        qb.Parameters.Add(LoyaltyPointSettingsTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
        //        qb.Parameters.Add(LoyaltyPointSettingsTable.UpdatedByColumn.ColumnName, lp.UpdatedBy);
        //        await QueryExecuter.NonQueryAsyc(qb);
        //    }

        //}

        public async Task<int> CountLoyaltySettings(LoyaltyPointSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(LoyaltyPointSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(LoyaltyPointSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(LoyaltyPointSettingsTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(LoyaltyPointSettingsTable.IsActiveColumn, true);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public async Task<Patient> LoyaltyPatientUpdate(Sales data, string AccountId, string InstanceId, string Name, string Mobile)
        {
            var Loyalty = await getLoyaltyPatient(AccountId, data.PatientId); //InstanceId, Name, Mobile
            if (Loyalty != null)
            {
                if (data.RedeemPts == null)
                {
                    data.RedeemPts = 0;
                }
                if (Loyalty.LoyaltyPoint == null)
                {
                    Loyalty.LoyaltyPoint = 0;
                }
                if (Loyalty.RedeemedPoint == null)
                {
                    Loyalty.RedeemedPoint = 0;
                }
                //if(data.RedeemPercent != 0)
                //{
                //    data.LoyaltyPts = data.LoyaltyPts - data.RedeemPercent;
                //}

                var ReddemPts = Convert.ToDecimal(Loyalty.LoyaltyPoint) - data.RedeemPts;
                var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
                qb.AddColumn(PatientTable.LoyaltyPointColumn, PatientTable.RedeemedPointColumn, PatientTable.UpdatedAtColumn);
                qb.ConditionBuilder.And(PatientTable.AccountIdColumn, AccountId);
                qb.ConditionBuilder.And(PatientTable.IdColumn, data.PatientId);
                qb.ConditionBuilder.And(PatientTable.MobileColumn, data.Mobile);
                qb.Parameters.Add(PatientTable.LoyaltyPointColumn.ColumnName, data.LoyaltyPts + ReddemPts);
                qb.Parameters.Add(PatientTable.RedeemedPointColumn.ColumnName, Loyalty.RedeemedPoint + data.RedeemPts);
                qb.Parameters.Add(PatientTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                if (data.WriteExecutionQuery)
                {
                    data.AddExecutionQuery(qb);
                    return data.Patient;
                }
                else
                {
                    //var result = await QueryExecuter.NonQueryAsyc(qb);
                    var result = (await List<Patient>(qb)).FirstOrDefault();
                    return result;
                }
            }
            else
            {
                return Loyalty;
            }

        }

        public async Task<Patient> ModifyLoyaltyPatientUpdate(Sales data, string AccountId, string InstanceId, string Name, string Mobile)
        {
            var Loyalty = await getLoyaltyPatient(AccountId, data.PatientId); //InstanceId, Name, Mobile
            if (Loyalty != null)
            {
                if (data.RedeemPts == null)
                {
                    data.RedeemPts = 0;
                }
                if (Loyalty.LoyaltyPoint == null)
                {
                    Loyalty.LoyaltyPoint = 0;
                }
                if (Loyalty.RedeemedPoint == null)
                {
                    Loyalty.RedeemedPoint = 0;
                }

                var SalesLoyalty = await getLoyaltySales(data.Id, AccountId, InstanceId, Name, Mobile);
                if (SalesLoyalty != null)
                {
                    decimal ModifyPts = 0;
                    decimal RedeemPts = 0;
                    if (SalesLoyalty.LoyaltyPts != null)
                    {
                        if (SalesLoyalty.LoyaltyPts >= data.LoyaltyPts)
                        {
                            ModifyPts = Convert.ToDecimal(SalesLoyalty.LoyaltyPts) - Convert.ToDecimal(data.LoyaltyPts);
                            Loyalty.LoyaltyPoint = Loyalty.LoyaltyPoint - ModifyPts;
                        }
                        else
                        {
                            ModifyPts = Convert.ToDecimal(data.LoyaltyPts) - Convert.ToDecimal(SalesLoyalty.LoyaltyPts);
                            Loyalty.LoyaltyPoint = Loyalty.LoyaltyPoint + ModifyPts;
                        }
                    }
                    else
                    {
                        Loyalty.LoyaltyPoint = Loyalty.LoyaltyPoint + data.LoyaltyPts;
                    }

                    if(SalesLoyalty.RedeemPts != null)
                    {
                        if(SalesLoyalty.RedeemPts >= data.RedeemPts)
                        {
                            RedeemPts = Convert.ToDecimal(SalesLoyalty.RedeemPts) - Convert.ToDecimal(data.RedeemPts);
                            Loyalty.RedeemedPoint = Loyalty.RedeemedPoint - RedeemPts;
                            Loyalty.LoyaltyPoint = Loyalty.LoyaltyPoint + RedeemPts;
                        }
                        else
                        {
                            RedeemPts = Convert.ToDecimal(data.RedeemPts) - Convert.ToDecimal(SalesLoyalty.RedeemPts);
                            Loyalty.RedeemedPoint = Loyalty.RedeemedPoint + RedeemPts;
                            Loyalty.LoyaltyPoint = Loyalty.LoyaltyPoint - RedeemPts;
                        }
                    }
                    else
                    {
                        Loyalty.RedeemedPoint = Loyalty.RedeemedPoint + data.RedeemPts;
                    }
                   
                    var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
                    qb.AddColumn(PatientTable.LoyaltyPointColumn, PatientTable.UpdatedAtColumn, PatientTable.RedeemedPointColumn);
                    qb.ConditionBuilder.And(PatientTable.AccountIdColumn, AccountId);
                    qb.ConditionBuilder.And(PatientTable.IdColumn, data.PatientId);
                    qb.Parameters.Add(PatientTable.LoyaltyPointColumn.ColumnName, Loyalty.LoyaltyPoint);
                    qb.Parameters.Add(PatientTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                    qb.Parameters.Add(PatientTable.RedeemedPointColumn.ColumnName, Loyalty.RedeemedPoint);
                    if (data.WriteExecutionQuery)
                    {
                        data.AddExecutionQuery(qb);
                        return data.Patient;
                    }
                    else
                    {
                        var result = (await List<Patient>(qb)).FirstOrDefault();
                        return result;
                    }

                }
                else
                {
                    return Loyalty;
                }
            }
            else
            {
                return Loyalty;
            }

        }

        //////public async Task<Patient> ModifyLoyaltyReturnPatientUpdate(SalesReturn data, string AccountId, string InstanceId, string Name, string Mobile)
        //////{
        //////    var Loyalty = await getLoyaltyPatient(AccountId, data.PatientId); //InstanceId, Name, Mobile
        //////    if (Loyalty != null)
        //////    {
        //////        if (data.RedeemPts == null)
        //////        {
        //////            data.RedeemPts = 0;
        //////        }
        //////        if (Loyalty.LoyaltyPoint == null)
        //////        {
        //////            Loyalty.LoyaltyPoint = 0;
        //////        }

        //////        var SalesLoyalty = await getLoyaltySales(data.SalesId, AccountId, InstanceId, Name, Mobile);
        //////        if (SalesLoyalty != null)
        //////        {
        //////            decimal ModifyPts = 0;
        //////            decimal RedeemPts = 0;

        //////            if (SalesLoyalty.LoyaltyPts != null && SalesLoyalty.LoyaltyPts != 0)
        //////            {
        //////                if (SalesLoyalty.LoyaltyPts >= data.LoyaltyPts)
        //////                {
        //////                    // ModifyPts = Convert.ToDecimal(SalesLoyalty.LoyaltyPts) - Convert.ToDecimal(data.LoyaltyPts);
        //////                    Loyalty.LoyaltyPoint = Loyalty.LoyaltyPoint - Convert.ToDecimal(data.LoyaltyPts);
        //////                }

        //////                var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
        //////                qb.AddColumn(PatientTable.LoyaltyPointColumn, PatientTable.UpdatedAtColumn);
        //////                qb.ConditionBuilder.And(PatientTable.AccountIdColumn, AccountId);
        //////                qb.ConditionBuilder.And(PatientTable.IdColumn, data.PatientId);
        //////                //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn, InstanceId);
        //////                //qb.ConditionBuilder.And(PatientTable.NameColumn, Name);
        //////                //qb.ConditionBuilder.And(PatientTable.MobileColumn, Mobile);
        //////                qb.Parameters.Add(PatientTable.LoyaltyPointColumn.ColumnName, Loyalty.LoyaltyPoint);
        //////                qb.Parameters.Add(PatientTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
        //////                var result = (await List<Patient>(qb)).FirstOrDefault();
        //////                // await QueryExecuter.NonQueryAsyc(qb);

        //////                var qb1 = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
        //////                qb1.AddColumn(SalesTable.LoyaltyPtsColumn);
        //////                qb1.ConditionBuilder.And(SalesTable.AccountIdColumn, AccountId);
        //////                qb1.ConditionBuilder.And(SalesTable.InstanceIdColumn, InstanceId);
        //////                qb1.ConditionBuilder.And(SalesTable.IdColumn, data.SalesId);
        //////                qb.ConditionBuilder.And(SalesTable.PatientIdColumn, data.PatientId);
        //////                qb1.Parameters.Add(SalesTable.LoyaltyPtsColumn.ColumnName, Loyalty.LoyaltyPoint);
        //////                var result1 = (await List<Sales>(qb1)).FirstOrDefault();
        //////                // await QueryExecuter.NonQueryAsyc(qb1);
        //////                return result;
        //////            }
        //////            else
        //////            {

        //////                Loyalty.LoyaltyPoint = Loyalty.LoyaltyPoint + data.LoyaltyPts;
        //////                var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
        //////                qb.AddColumn(PatientTable.LoyaltyPointColumn, PatientTable.UpdatedAtColumn);
        //////                qb.ConditionBuilder.And(PatientTable.AccountIdColumn, AccountId);
        //////                qb.ConditionBuilder.And(PatientTable.IdColumn, data.PatientId);
        //////                //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn, InstanceId);
        //////                //qb.ConditionBuilder.And(PatientTable.NameColumn, Name);
        //////                //qb.ConditionBuilder.And(PatientTable.MobileColumn, Mobile);
        //////                qb.Parameters.Add(PatientTable.LoyaltyPointColumn.ColumnName, Loyalty.LoyaltyPoint);
        //////                qb.Parameters.Add(PatientTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
        //////                var result = (await List<Patient>(qb)).FirstOrDefault();
        //////                return result;
        //////            }

        //////        }
        //////        else
        //////        {
        //////            return Loyalty;
        //////        }
        //////    }
        //////    else
        //////    {
        //////        return Loyalty;
        //////    }
        //////}

        public async Task<Patient> RedeemPatientUpdate(Sales data, string AccountId, string InstanceId, string Name, string Mobile)
        {
            var Loyalty = await getLoyaltyPatient(AccountId, data.PatientId); //InstanceId, Name, Mobile
            if (Loyalty != null)
            {
                if (data.RedeemPts == null)
                {
                    data.RedeemPts = 0;
                }

                var ReddemPts = Convert.ToDecimal(Loyalty.LoyaltyPoint) - Convert.ToDecimal(data.RedeemPts);

                var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Update);
                qb.AddColumn(PatientTable.LoyaltyPointColumn, PatientTable.UpdatedAtColumn, PatientTable.RedeemedPointColumn);
                qb.ConditionBuilder.And(PatientTable.AccountIdColumn, AccountId);
                qb.ConditionBuilder.And(PatientTable.IdColumn, data.PatientId);
                //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn, InstanceId);
                //qb.ConditionBuilder.And(PatientTable.NameColumn, Name);
                //qb.ConditionBuilder.And(PatientTable.MobileColumn, Mobile);
                qb.Parameters.Add(PatientTable.LoyaltyPointColumn.ColumnName, ReddemPts);
                qb.Parameters.Add(PatientTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                qb.Parameters.Add(PatientTable.RedeemedPointColumn.ColumnName, Convert.ToDecimal(Loyalty.RedeemedPoint) + data.RedeemPts);
                if (data.WriteExecutionQuery)
                {
                    data.AddExecutionQuery(qb);
                    return data.Patient;
                }
                else
                {
                    var result = (await List<Patient>(qb)).FirstOrDefault();
                    return result;
                }
            }
            else
            {
                return Loyalty;
            }
        }

        public async Task<Patient> getLoyaltyPatient(string AccountId, string PatientId)  // string InstanceId, string Name, string Mobile
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.AddColumn(PatientTable.LoyaltyPointColumn, PatientTable.RedeemedPointColumn);
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(PatientTable.IdColumn, PatientId);
            //qb.ConditionBuilder.And(PatientTable.InstanceIdColumn, InstanceId);
            //qb.ConditionBuilder.And(PatientTable.NameColumn, Name);
            //qb.ConditionBuilder.And(PatientTable.MobileColumn, Mobile);
            var result = (await List<Patient>(qb)).FirstOrDefault();
            return result;
        }
        public async Task<Sales> getLoyaltySales(string SalesId, string AccountId, string InstanceId, string Name, string Mobile)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Select);
            qb.AddColumn(SalesTable.LoyaltyPtsColumn, SalesTable.RedeemPtsColumn);
            qb.ConditionBuilder.And(SalesTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SalesTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(SalesTable.IdColumn, SalesId);
            qb.ConditionBuilder.And(SalesTable.NameColumn, Name);
            qb.ConditionBuilder.And(SalesTable.MobileColumn, Mobile);
            var result = (await List<Sales>(qb)).FirstOrDefault();
            return result;
        }

        public async Task<int> GetfirstLoyaltyCheck(string patientId, string AccountId, string InstanceId)
        {
            var query =
                 $@"select count(S.PatientId) as FirstLoyaltyCount from patient P inner join sales S on S.PatientId=P.Id where S.AccountId='{AccountId}' and S.InstanceId='{InstanceId}' and S.PatientId='{patientId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var count = Convert.ToInt32(await SingleValueAsyc(qb));
            return count;
        }

        public async Task<LoyaltyPointSettings> GetSaleLoyaltyPointSettings(string loyaltyId, string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(LoyaltyPointSettingsTable.Table, OperationType.Select);
            qb.AddColumn(LoyaltyPointSettingsTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(LoyaltyPointSettingsTable.IdColumn, loyaltyId);
            qb.ConditionBuilder.And(LoyaltyPointSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(LoyaltyPointSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<LoyaltyPointSettings>(qb)).FirstOrDefault();

            return result;

        }

        public async Task<LoyaltyPointSettings> getSalesLoyaltyPoint(string AccountId, string LoyaltyId, string SalesId)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(LoyaltyPointSettingsTable.Table, OperationType.Select);
            //qb.AddColumn(LoyaltyPointSettingsTable.IdColumn, LoyaltyPointSettingsTable.LoyaltySaleValueColumn, LoyaltyPointSettingsTable.LoyaltyPointColumn, LoyaltyPointSettingsTable.StartDateColumn, LoyaltyPointSettingsTable.EndDateColumn, LoyaltyPointSettingsTable.MinimumSalesAmtColumn, LoyaltyPointSettingsTable.RedeemPointColumn, LoyaltyPointSettingsTable.RedeemValueColumn, LoyaltyPointSettingsTable.LoyaltyOptionColumn, LoyaltyPointSettingsTable.IsMinimumSaleColumn, LoyaltyPointSettingsTable.IsEndDateColumn);
            //qb.JoinBuilder.Join(SalesTable.Table, SalesTable.LoyaltyIdColumn, LoyaltyPointSettingsTable.IdColumn);
            //qb.ConditionBuilder.And(LoyaltyPointSettingsTable.AccountIdColumn, AccountId);
            //qb.ConditionBuilder.And(SalesTable.LoyaltyIdColumn, LoyaltyId);
            //qb.ConditionBuilder.And(SalesTable.IdColumn, SalesId);
            //var result = await List<LoyaltyPointSettings>(qb);
            //LoyaltyPointSettings loyaltyPoints = new LoyaltyPointSettings();
            //if (result.Count > 0)
            //{
            //    loyaltyPoints = result.First();
            //}
            //return loyaltyPoints;

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("LoyaltyId", LoyaltyId);
            parms.Add("SalesId", SalesId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesLoyaltyPointSettings", parms);
            if (result.Count() <= 0)
            { return null; }
            var salesLoyaltyPts = result.Select(x =>
            {
                var LoyaltyPointSettings = new LoyaltyPointSettings
                {
                    Id = x.Id,
                    LoyaltySaleValue = x.LoyaltySaleValue as decimal? ?? 0,
                    LoyaltyPoint = x.LoyaltyPoint as decimal? ?? 0,
                    StartDate = x.StartDate as DateTime? ?? DateTime.MinValue,
                    EndDate = x.EndDate as DateTime? ?? DateTime.MinValue,
                    MinimumSalesAmt = x.MinimumSalesAmt as decimal? ?? 0,
                    RedeemPoint = x.RedeemPoint as decimal? ?? 0,
                    RedeemValue = x.RedeemValue as decimal? ?? 0,
                    MaximumRedeemPoint = x.MaximumRedeemPoint as decimal? ?? 0,
                    IsActive = x.IsActive,
                    LoyaltyOption = x.LoyaltyOption,
                    IsMinimumSale = x.IsMinimumSale as bool? ?? false,
                    IsMaximumRedeem = x.IsMaximumRedeem as bool? ?? false,
                    IsEndDate = x.IsEndDate as bool? ?? false,

                };

                return LoyaltyPointSettings;
            });
            var getLoyaltyPointSettings = salesLoyaltyPts.FirstOrDefault();
            getLoyaltyPointSettings.LoyaltyProductPoints = await _productDataAccess.GetLoyaltyPointProductList(getLoyaltyPointSettings.Id);
            return getLoyaltyPointSettings;
        }

        public async Task<List<Product>> GetKindOfProductDataList(string AccountId, string InstanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            var productlist = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetKindOfProductList", parms);
            var data = productlist.Select(item =>
            {
                var product = new Product()
                {
                    KindName = item.KindName,
                };
                return product;
            });
            return data.ToList();
        }

        //#endregion Loyalty Points  Functionalty End

        // Code by Sarubala to get GSTin from db - start

        public async Task<string> getGSTINDetail(string AccId, string InsId)
        {
            string gstin = null;
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Select);
            qb.AddColumn(InstanceTable.GsTinNoColumn);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, InsId);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, AccId);

            var result = (await List<Instance>(qb)).FirstOrDefault();

            if (!string.IsNullOrEmpty(result.GsTinNo))
            {
                gstin = result.GsTinNo;
            }

            return gstin;
        }

        public async Task<Instance> SaveGstIn(Instance data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(InstanceTable.Table, OperationType.Update);
            qb.AddColumn(InstanceTable.GsTinNoColumn, InstanceTable.UpdatedAtColumn, InstanceTable.UpdatedByColumn);
            qb.Parameters.Add(InstanceTable.GsTinNoColumn.ColumnName, data.GsTinNo);
            qb.Parameters.Add(InstanceTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(InstanceTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.ConditionBuilder.And(InstanceTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(InstanceTable.IdColumn, data.InstanceId);
            await QueryExecuter.NonQueryAsyc(qb);

            return data;
        }

        //Code by Sarubala to get GSTin from db - end


        public async Task<string> ExportOnlinePatient(string accountId, string instanceid)
        {
            StringBuilder sb = new StringBuilder();
            string fileName = null;
            string filePath = null;
            FileInfo info = null;
            try
            {
                // Updaete PatientId in Sales table
                var qbPatient = QueryBuilderFactory.GetQueryBuilder("usp_DataCorrection_Patient_Accountwise");
                qbPatient.Parameters.Add("@AccountID", accountId);
                qbPatient.Parameters.Add("@InstanceID", instanceid);

                var result = await QueryExecuter.ExecuteProcAsyc(qbPatient);


                var qb = QueryBuilderFactory.GetQueryBuilder("sp_syncPatient");
                qb.Parameters.Add("@AccountID", accountId);
                qb.Parameters.Add("@InstanceID", instanceid);

                using (var reader = await QueryExecuter.ExecuteProcAsyc(qb))
                {
                    //fileName = Path.Combine(_environment.WebRootPath, "temp", string.Format("hq_db_{0}.txt", instanceid));
                    fileName = string.Format("hq_db_{0}.txt", instanceid);
                    filePath = Path.Combine("./temp", fileName);
                    info = new FileInfo(filePath);

                    if (info.Exists)
                        info.Delete();

                    using (StreamWriter file = info.AppendText())
                    {
                        file.Write("<SyncPatientData>");
                        //file.Write(File.ReadAllText("./temp/productmaster"));
                        bool bResult = true;
                        while (bResult)
                        {
                            while (reader.Read())
                            {
                                //sb.Append(reader.GetString(0));
                                file.Write(reader.GetString(0));
                            }
                            bResult = reader.NextResult();
                        }

                        file.Write("</SyncPatientData>");
                    }
                }

            }
            catch (Exception e)
            {
                return e.Message;
            }

            return info.FullName;
        }

        /// <summary>
        /// Added by Annadurai on 07032017
        /// </summary>
        /// <param name="name">patientId</param>
        /// <param name="Mobile"></param>
        /// <param name="accountId"></param>
        /// <param name="SelectedProduct">productId</param>
        /// <returns></returns>
        public async Task<List<SalesItem>> PreviousSalesDetailsForSelectedCustomer(string name, string Mobile, string accountId, string SelectedProduct)
        {
            #region query
            string query =
                $@"select top 5 sales.invoicedate AS InvoiceDate,sales.invoiceno as InvoiceNo,ltrim(isnull(sales.prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,isnull(sales.Discount,0) as  salesDiscount,sales.Name,sales.DoctorName,sales.Address,salesitem.quantity,convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END)) As SellingPrice,salesitem.Discount,ps.BatchNo,ps.ExpireDate,ps.VAT,ps.Stock,ps.PurchasePrice,p.Name as ProductName, p.Manufacturer, p.Schedule,p.Type,
                           convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) *salesitem.quantity) As InvoiceAmount
                           , isnull(sales.Discount,0) as Discount,ISNULL(ISNULL(vpi.PurchasePrice,ps.PurchasePrice) * (ps.Stock),0)  As CostPrice
                           ,(salesitem.Quantity * (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) * isnull(salesitem.Discount,0) / 100) as DiscountSum,
                           salesitem.MRP, (select top 1 ShowSalesItemHistory from SaleSettings where sales.AccountId = AccountId and ShowSalesItemHistory = 1 ) as ShowSalesItemHistory
                           from sales sales 
                           join salesitem salesitem on sales.id= salesitem.salesid
                           join productstock ps on ps.id=salesitem.productstockid
                           inner join Product p on p.Id=ps.ProductId
                           left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem group by productstockId) vpi on ps.id=vpi.productstockid 					                   
                           WHERE sales.AccountId = '{accountId}' and sales.PatientId='{name}' and sales.Cancelstatus is NULL and p.Id like '{SelectedProduct}%' 
                           and exists (select top 1 ShowSalesItemHistory from SaleSettings where sales.AccountId = AccountId and ShowSalesItemHistory = 1)                   					
                           ORDER BY sales.CreatedAt desc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var list = new List<SalesItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var salesItem = new SalesItem
                    {
                        Sales =
                               {
                                   InvoiceNo = reader["InvoiceNo"].ToString(),
                                   InvoiceSeries=reader["InvoiceSeries"].ToString(),
                                   ActualInvoice= reader["InvoiceSeries"].ToString()+" "+ reader["InvoiceNo"].ToString(),
                                   InvoiceDate = Convert.ToDateTime(reader["InvoiceDate"]),
                                   Name = reader["Name"].ToString(),
                                   DoctorName = reader["DoctorName"].ToString(),
                                   Address = reader["Address"].ToString(),
                               },
                        ProductStock =
                               {
                                    BatchNo = reader["BatchNo"].ToString(),
                                    ExpireDate = Convert.ToDateTime(reader["ExpireDate"]),
                                    Stock=reader["Stock"] as decimal? ?? 0,
                                    VAT=reader["VAT"] as decimal? ?? 0,
                                    PurchasePrice=reader["PurchasePrice"] as decimal? ?? 0,
                                    CostPrice=reader["CostPrice"] as decimal? ?? 0,
                                   Product=
                                   {
                                       Name=reader["ProductName"].ToString(),
                                       Manufacturer=reader["Manufacturer"].ToString(),
                                       Schedule=reader["Schedule"].ToString(),
                                       Type=reader["Type"].ToString(),
                                   }
                               }

                    };
                    salesItem.SellingPrice = reader["SellingPrice"] as decimal? ?? 0;
                    salesItem.Quantity = reader["quantity"] as decimal? ?? 0;
                    salesItem.Discount = reader["Discount"] as decimal? ?? 0;
                    salesItem.Sales.Discount = (decimal)reader["salesDiscount"];
                    decimal? discountSum = reader["DiscountSum"] as decimal? ?? 0;
                    salesItem.Sales.InvoiceAmount = reader["InvoiceAmount"] as decimal? ?? 0;
                    salesItem.ProductStock.TotalCostPrice = salesItem.SellingPrice * salesItem.Quantity;
                    salesItem.Sales.FinalValue = reader["MRP"] as decimal? ?? 0;//salesItem.Sales.InvoiceAmount - (salesItem.Sales.InvoiceAmount * (salesItem.Sales.Discount / 100)) - discountSum;
                    //salesItem.Sales.FinalValue = Math.Round((decimal)salesItem.Sales.FinalValue);
                    //salesItem.ReportTotal = (decimal)salesItem.Sales.FinalValue;
                    salesItem.Sales.FinalValue = Math.Round((decimal)(salesItem.Sales.FinalValue), 2, MidpointRounding.AwayFromZero); //Modified by Poongodi on 05/07/2017
                    salesItem.ReportTotal = Math.Round((decimal)(salesItem.Sales.FinalValue), 0, MidpointRounding.AwayFromZero);
                    salesItem.ProductStock.TotalCostPrice = salesItem.ProductStock.PurchasePrice * salesItem.Quantity;
                    if (salesItem.ProductStock.TotalCostPrice > 0)
                    {
                        // string Tval=String.Format("{0:0.##}", salesItem.ReportTotal);
                        //string TCost = String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice);
                        salesItem.SalesProfit = ((decimal.Parse(String.Format("{0:0.##}", salesItem.ReportTotal)) - decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) / decimal.Parse(String.Format("{0:0.##}", salesItem.ProductStock.TotalCostPrice))) * 100;
                    }
                    //salesItem.SalesProfit = ((salesItem.ReportTotal - salesItem.ProductStock.TotalCostPrice) / salesItem.ProductStock.TotalCostPrice) * 100;
                    else
                        salesItem.SalesProfit = 0;
                    salesItem.ShowSalesItemHistory = reader["ShowSalesItemHistory"] as bool? ?? false;
                    list.Add(salesItem);
                }
                return list;
            }
            #endregion
        }
        /// <summary>
        /// Default insert for laser print invoice(pdf)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private async Task<SaleSettings> DefaultSaleSettingsInsert(SaleSettings data)
        {
            string txt = string.Empty;
            if (user.IsComposite() == "True")
            {
                txt = "\r\n{\r\n\"printCustomFields\":\r\n[\r\n{\"field\":\"sno\",\"enable\":1,\"width\":\"3%\"},\r\n{\"field\":\"product\",\"enable\":1,\"width\":\"*\"},\r\n{\"field\":\"hsncode\",\"enable\":1,\"width\":\"10%\"},\r\n{\"field\":\"i/u\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"batch\",\"enable\":1,\"width\":\"10%\"},\r\n{\"field\":\"expiry\",\"enable\":1,\"width\":\"8%\"},\r\n{\"field\":\"qty\",\"enable\":1,\"width\":\"5%\"},\r\n{\"field\":\"mrp\",\"enable\":1,\"width\":\"8%\",\"title\":\"MRP\"},\r\n{\"field\":\"maxmrp\",\"enable\":0,\"width\":\"5%\",\"title\":\"MRP\"},\r\n{\"field\":\"discount\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"cgst%\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"cgstamt\",\"enable\":0,\"width\":\"6%\"},\r\n{\"field\":\"utgst/sgst%\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"utgst/sgstamt\",\"enable\":0,\"width\":\"6%\"},\r\n{\"field\":\"igst%\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"igstamt\",\"enable\":0,\"width\":\"6%\"},\r\n{\"field\":\"gst%\",\"enable\":1,\"width\":\"5%\"},\r\n{\"field\":\"gstamt\",\"enable\":0,\"width\":\"7%\"},\r\n{\"field\":\"amount\",\"enable\":1,\"width\":\"15%\"},\r\n\r\n{\"field\":\"doctorname\",\"enable\":1},\r\n{\"field\":\"custname\",\"enable\":1,\"title\":\"Customer Name\"},\r\n{\"field\":\"custaddr\",\"enable\":1},\r\n{\"field\":\"custmobile\",\"enable\":1},\r\n{\"field\":\"custgstin\",\"enable\":0},\r\n{\"field\":\"custempid\",\"enable\":0},\r\n\r\n{\"field\":\"totcgstamt\",\"enable\":0},\r\n{\"field\":\"totutgst/sgstamt\",\"enable\":0},\r\n{\"field\":\"totigstamt\",\"enable\":0},\r\n{\"field\":\"totgstamt\",\"enable\":0}\r\n],\r\n\r\n\"saleDetails\":{\r\n\t\"style\": \"tableStyle\",\r\n\t\"table\": {\r\n\t\t\"headerRows\": 1,\r\n\t\t\"widths\": [\"3%\", \"*\", \"10%\", \"10%\", \"8%\", \"5%\", \"8%\", \"15%\"],\r\n\t\t\"body\": [\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Sno\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Product\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"HSN Code\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Batch\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Exp.\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Qty\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"MRP\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Amount\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}],\r\n\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"@sno\",\r\n\t\t\t\t\"alignment\": \"right\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@productName\",\r\n\t\t\t\t\"alignment\": \"left\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@hsnCode\",\r\n\t\t\t\t\"alignment\": \"left\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@batchNo\",\r\n\t\t\t\t\"alignment\": \"left\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@expiry\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@saleQty\",\r\n\t\t\t\t\"alignment\": \"right\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@rate\",\r\n\t\t\t\t\"alignment\": \"right\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@total\",\r\n\t\t\t\t\"alignment\": \"right\"\r\n\t\t\t}]\r\n\t\t]\r\n\t},\r\n\t\"layout\": \"leftTopRightBottomWTHeaderColSpanLayout\"\r\n},\r\n\r\n\"pdfDesign\":{\r\n\t\"pageSize\": \"@pageSize\",\r\n\t\"pageOrientation\": \"@pageOrientation\",\r\n\t\"content\": [{\r\n\t\t\"table\": {\r\n\t\t\t\"style\": \"tableStyle\",\r\n\t\t\t\"widths\": [\"0%\", \"100%\"],\r\n\t\t\t\"body\": [\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"@pharmacyName\",\r\n\t\t\t\t\t\"alignment\": \"center\",\r\n\t\t\t\t\t\"style\": {\r\n\t\t\t\t\t\t\"bold\": true,\r\n\t\t\t\t\t\t\"fontSize\": 15\r\n\t\t\t\t\t}\r\n\t\t\t\t}],\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"@pharmacyAddressArea\",\r\n\t\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t\t}],\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"@pharmacyCityStatePincode\",\r\n\t\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t\t}],\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"PH: @pharmacyPhone, MOBILE: @pharmacyMobile\",\r\n\t\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t\t}],\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo\",\r\n\t\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t\t}]\r\n\t\t\t]\r\n\t\t},\r\n\t\t\"layout\": \"leftTopRightBottomLayout\"\r\n\t},\r\n\t{\r\n\t\"table\": {\r\n\t\t\"style\": \"tableStyle\",\r\n\t\t\"widths\": [\"75%\", \"25%\"],\r\n\t\t\"body\": [\r\n\t\t\t[{\r\n\t\t\t\t\"colSpan\": 2,\r\n\t\t\t\t\"margin\": [0, 2, 0, 0],\r\n\t\t\t\t\"text\": \"BILL OF SUPPLY\", \"style\": \"header\", \"alignment\" : \"center\"\r\n\t\t\t}, {}],\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Doctor Name       : @doctorName\"\r\n\t\t\t}, \r\n\t\t\t{\r\n\t\t\t\t\"text\": \"Bill No       : @billNo\"\r\n\t\t\t}],\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Customer Name  : @customerName\"\r\n\t\t\t}, \r\n\t\t\t{\r\n\t\t\t\t\"text\": \"B Date       : @billDate\"\r\n\t\t\t}],\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Address                : @customerAddress\"\r\n\t\t\t}, \r\n\t\t\t{\r\n\t\t\t\t\"text\": \"B Time      : @billTime\"\r\n\t\t\t}],\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Mobile                   : @customerMobile\"\r\n\t\t\t}, \r\n\t\t\t{\r\n\t\t\t\t\"text\": \"B Type       : @invoiceType\"\r\n\t\t\t}]\r\n\t\t]\r\n\t},\r\n\t\"layout\": \"leftRightLayout\"\r\n}\r\n\t],\r\n\t\"defaultStyle\": {\r\n\t\t\"fontSize\": \"@defaultFontSize\",\r\n\t\t\"bold\": false\r\n\t},\r\n\t\"pageMargins\": [5, 5, 5, 8],\r\n\t\"styles\": {\r\n\t\t\"header\": {\r\n\t\t\t\"bold\": true,\r\n\t\t\t\"fontSize\": 12\r\n\t\t},\r\n\t\t\"tableStyle\": {\r\n\t\t\t\"fontSize\": \"@tableFontSize\"\r\n\t\t}\r\n\t}\r\n}\r\n}";
            }
            else
            {
                txt = "\r\n{\r\n\"printCustomFields\":\r\n[\r\n{\"field\":\"sno\",\"enable\":1,\"width\":\"3%\"},\r\n{\"field\":\"product\",\"enable\":1,\"width\":\"*\"},\r\n{\"field\":\"hsncode\",\"enable\":1,\"width\":\"10%\"},\r\n{\"field\":\"i/u\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"batch\",\"enable\":1,\"width\":\"10%\"},\r\n{\"field\":\"expiry\",\"enable\":1,\"width\":\"8%\"},\r\n{\"field\":\"qty\",\"enable\":1,\"width\":\"5%\"},\r\n{\"field\":\"mrp\",\"enable\":1,\"width\":\"8%\",\"title\":\"MRP\"},\r\n{\"field\":\"maxmrp\",\"enable\":0,\"width\":\"5%\",\"title\":\"MRP\"},\r\n{\"field\":\"discount\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"cgst%\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"cgstamt\",\"enable\":0,\"width\":\"6%\"},\r\n{\"field\":\"utgst/sgst%\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"utgst/sgstamt\",\"enable\":0,\"width\":\"6%\"},\r\n{\"field\":\"igst%\",\"enable\":0,\"width\":\"4%\"},\r\n{\"field\":\"igstamt\",\"enable\":0,\"width\":\"6%\"},\r\n{\"field\":\"gst%\",\"enable\":1,\"width\":\"5%\"},\r\n{\"field\":\"gstamt\",\"enable\":0,\"width\":\"7%\"},\r\n{\"field\":\"amount\",\"enable\":1,\"width\":\"15%\"},\r\n\r\n{\"field\":\"doctorname\",\"enable\":1},\r\n{\"field\":\"custname\",\"enable\":1,\"title\":\"Customer Name\"},\r\n{\"field\":\"custaddr\",\"enable\":1},\r\n{\"field\":\"custmobile\",\"enable\":1},\r\n{\"field\":\"custgstin\",\"enable\":0},\r\n{\"field\":\"custempid\",\"enable\":0},\r\n\r\n{\"field\":\"totcgstamt\",\"enable\":1},\r\n{\"field\":\"totutgst/sgstamt\",\"enable\":1},\r\n{\"field\":\"totigstamt\",\"enable\":1},\r\n{\"field\":\"totgstamt\",\"enable\":1}\r\n],\r\n\r\n\"saleDetails\":{\r\n\t\"style\": \"tableStyle\",\r\n\t\"table\": {\r\n\t\t\"headerRows\": 1,\r\n\t\t\"widths\": [\"3%\", \"*\", \"10%\", \"10%\", \"8%\", \"5%\", \"8%\", \"5%\", \"15%\"],\r\n\t\t\"body\": [\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Sno\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Product\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"HSN Code\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Batch\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Exp.\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Qty\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"MRP\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"GST%\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"Amount\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}],\r\n\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"@sno\",\r\n\t\t\t\t\"alignment\": \"right\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@productName\",\r\n\t\t\t\t\"alignment\": \"left\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@hsnCode\",\r\n\t\t\t\t\"alignment\": \"left\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@batchNo\",\r\n\t\t\t\t\"alignment\": \"left\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@expiry\",\r\n\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@saleQty\",\r\n\t\t\t\t\"alignment\": \"right\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@rate\",\r\n\t\t\t\t\"alignment\": \"right\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@gstPerc\",\r\n\t\t\t\t\"alignment\": \"right\"\r\n\t\t\t}, {\r\n\t\t\t\t\"text\": \"@total\",\r\n\t\t\t\t\"alignment\": \"right\"\r\n\t\t\t}]\r\n\t\t]\r\n\t},\r\n\t\"layout\": \"leftTopRightBottomWTHeaderColSpanLayout\"\r\n},\r\n\r\n\"pdfDesign\":{\r\n\t\"pageSize\": \"@pageSize\",\r\n\t\"pageOrientation\": \"@pageOrientation\",\r\n\t\"content\": [{\r\n\t\t\"table\": {\r\n\t\t\t\"style\": \"tableStyle\",\r\n\t\t\t\"widths\": [\"0%\", \"100%\"],\r\n\t\t\t\"body\": [\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"@pharmacyName\",\r\n\t\t\t\t\t\"alignment\": \"center\",\r\n\t\t\t\t\t\"style\": {\r\n\t\t\t\t\t\t\"bold\": true,\r\n\t\t\t\t\t\t\"fontSize\": 15\r\n\t\t\t\t\t}\r\n\t\t\t\t}],\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"@pharmacyAddressArea\",\r\n\t\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t\t}],\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"@pharmacyCityStatePincode\",\r\n\t\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t\t}],\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"PH: @pharmacyPhone, MOBILE: @pharmacyMobile\",\r\n\t\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t\t}],\r\n\t\t\t\t[{}, {\r\n\t\t\t\t\t\"text\": \"DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo\",\r\n\t\t\t\t\t\"alignment\": \"center\"\r\n\t\t\t\t}]\r\n\t\t\t]\r\n\t\t},\r\n\t\t\"layout\": \"leftTopRightBottomLayout\"\r\n\t},\r\n\t{\r\n\t\"table\": {\r\n\t\t\"style\": \"tableStyle\",\r\n\t\t\"widths\": [\"75%\", \"25%\"],\r\n\t\t\"body\": [\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Doctor Name       : @doctorName\"\r\n\t\t\t}, \r\n\t\t\t{\r\n\t\t\t\t\"text\": \"Bill No       : @billNo\"\r\n\t\t\t}],\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Customer Name  : @customerName\"\r\n\t\t\t}, \r\n\t\t\t{\r\n\t\t\t\t\"text\": \"B Date       : @billDate\"\r\n\t\t\t}],\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Address                : @customerAddress\"\r\n\t\t\t}, \r\n\t\t\t{\r\n\t\t\t\t\"text\": \"B Time      : @billTime\"\r\n\t\t\t}],\r\n\t\t\t[{\r\n\t\t\t\t\"text\": \"Mobile                   : @customerMobile\"\r\n\t\t\t}, \r\n\t\t\t{\r\n\t\t\t\t\"text\": \"B Type       : @invoiceType\"\r\n\t\t\t}]\r\n\t\t]\r\n\t},\r\n\t\"layout\": \"leftRightLayout\"\r\n}\r\n\t],\r\n\t\"defaultStyle\": {\r\n\t\t\"fontSize\": \"@defaultFontSize\",\r\n\t\t\"bold\": false\r\n\t},\r\n\t\"pageMargins\": [5, 5, 5, 8],\r\n\t\"styles\": {\r\n\t\t\"header\": {\r\n\t\t\t\"bold\": true,\r\n\t\t\t\"fontSize\": 12\r\n\t\t},\r\n\t\t\"tableStyle\": {\r\n\t\t\t\"fontSize\": \"@tableFontSize\"\r\n\t\t}\r\n\t}\r\n}\r\n}";
            }
            data.PrintCustomFields = txt;
            var result = await Insert(data, SaleSettingsTable.Table);
            return result;
        }



        public async Task UpdateCustomerByDepartment(string accountid, string instanceid, string salesId, string customer)
        {
            var qbSale = QueryBuilderFactory.GetQueryBuilder(SalesTable.Table, OperationType.Update);
            qbSale.ConditionBuilder.And(SalesTable.AccountIdColumn, accountid);
            qbSale.ConditionBuilder.And(SalesTable.InstanceIdColumn, instanceid);
            qbSale.ConditionBuilder.And(SalesTable.IdColumn, salesId);
            qbSale.AddColumn(SalesTable.AddressColumn);
            qbSale.Parameters.Add(SalesTable.AddressColumn.ColumnName, customer);
            await QueryExecuter.NonQueryAsyc(qbSale);
        }
        #region Sale Order, Template, Estimate
        // Added by Gavaskar 03-10-2017 Save Sales Order Start
        public async Task<string> GetSalesOrderNo(string InstanceId, int SalesOrderEstimateType, string Prefix)
        {
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.SalesOrderEstimate, CustomDateTime.IST, InstanceId, "", SalesOrderEstimateType.ToString(), Prefix);
            return sBillNo.ToString();
        }

        public async Task<string> GetTemplateNo(string InstanceId, string Prefix)
        {
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.SalesTemplate, CustomDateTime.IST, InstanceId, "", "", Prefix);
            return sBillNo.ToString();
        }

        public Task<SalesOrderEstimate> SaveSalesOrder(SalesOrderEstimate data)
        {
            return Insert(data, SalesOrderEstimateTable.Table);
        }

        public Task<SalesOrderEstimateItem> SaveSalesOrderItem(SalesOrderEstimateItem data)
        {
            return Insert(data, SalesOrderEstimateItemTable.Table);
        }

        public Task<SalesTemplate> SaveSalesTemplate(SalesTemplate data)
        {
            return Insert(data, SalesTemplateTable.Table);
        }

        public Task<SalesTemplateItem> SaveSalesTemplateItemItem(SalesTemplateItem data)
        {
            return Insert(data, SalesTemplateItemTable.Table);
        }

        public async Task<List<SalesTemplate>> GetTemplateNameList(string AccountId, string InstanceId)
        {
            string query = $@" select * from Salestemplate(nolock) where AccountId='{AccountId}' and InstanceId='{InstanceId}' and (IsActive =0 or IsActive is null) order by TemplateName asc";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<SalesTemplate>(qb);
            return result.ToList();

        }

        public async Task<List<SalesTemplateItem>> getSelectedTemplateNameList(string TemplateId, string AccountId, string InstanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("TemplateId", TemplateId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSelectedTemplateNameList", parms);
            var templateNameList = result.Select(x =>
            {
                var salesTemplate = new SalesTemplateItem
                {
                    ProductId = x.ProductId,
                    Quantity = x.Quantity as decimal? ?? 0,
                    AccountId = x.AccountId,
                    InstanceId = x.InstanceId,
                    Id = x.Id,
                    TemplateId = x.TemplateId,
                    Product = new Product
                    {
                        Name = x.Name.ToString(),
                        Manufacturer = x.Manufacturer.ToString(),
                        GenericName = x.GenericName.ToString(),
                    }
                };

                return salesTemplate;
            });

            return templateNameList.ToList();

        }

        public async Task<SalesTemplateItem> UpdateSalesTemplateItemItem(SalesTemplateItem data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTemplateItemTable.Table, OperationType.Update);
            qb.AddColumn(SalesTemplateItemTable.IsDeletedColumn, SalesTemplateItemTable.UpdatedAtColumn, SalesTemplateItemTable.UpdatedByColumn);
            qb.Parameters.Add(SalesTemplateItemTable.IsDeletedColumn.ColumnName, true);
            qb.Parameters.Add(SalesTemplateItemTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(SalesTemplateItemTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.ConditionBuilder.And(SalesTemplateItemTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SalesTemplateItemTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(SalesTemplateItemTable.IdColumn, data.Id);

            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task<int> SalesOrderCount(SalesOrderEstimate data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(SalesOrderEstimateTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SalesOrderEstimateTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(SalesOrderEstimateTable.SalesOrderEstimateTypeColumn, data.SalesOrderEstimateType);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public async Task<IEnumerable<SalesOrderEstimate>> SalesOrderEstimateList(SalesOrderEstimate data, string instanceid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            Dictionary<string, object> parmsSalesOrderEstimateId = new Dictionary<string, object>();

            var Pageno = (data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize;
            parms.Add("AccountId", data.AccountId);
            parms.Add("InstanceId", instanceid);
            parms.Add("PageNo", Pageno);
            parms.Add("PageSize", data.Page.PageSize);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesOrderEstimateHistoryList", parms);
            if (result.Count() <= 0)
            { return null; }
            var salesOrderEstimate = result.Select(x =>
            {
                var salesOrder = new SalesOrderEstimate
                {
                    Prefix = x.Prefix as System.String ?? "",
                    OrderEstimateId = x.OrderEstimateId as System.String ?? "",
                    OrderEstimateDate = x.OrderEstimateDate as DateTime? ?? DateTime.MinValue,
                    PatientId = x.PatientId as System.String ?? "",
                    SalesOrderEstimateType = x.SalesOrderEstimateType,
                    Id = x.Id,
                    IsEstimateActive = x.IsEstimateActive,
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,

                    Patient =
                    {
                        Name=x.PatientName as System.String ?? "",
                        Mobile=x.PatientMobile as System.String ?? "",
                        Email=x.PatientEmail as System.String ?? "",
                        PatientType = x.PatientType,
                    }
                };
                return salesOrder;
            });
            var SalesOrderEstimateId = string.Join(",", result.Select(x => x.Id).ToList());
            parmsSalesOrderEstimateId.Add("AccountId", data.AccountId);
            parmsSalesOrderEstimateId.Add("InstanceId", instanceid);
            parmsSalesOrderEstimateId.Add("SalesOrderEstimateId", SalesOrderEstimateId);
            var resultSalesOrderEstimateId = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesOrderEstimateHistoryItemList", parmsSalesOrderEstimateId);
            var salesOrderEstimateItems = resultSalesOrderEstimateId.Select(x =>
            {
                var salesOrderEstimateItem = new SalesOrderEstimateItem
                {
                    Id = x.Id as System.String ?? "",
                    ProductId = x.ProductId as System.String ?? "",
                    SalesOrderEstimateId = x.SalesOrderEstimateId as System.String ?? "",
                    Quantity = x.Quantity as decimal? ?? 0,
                    SellingPrice = x.SellingPrice as decimal? ?? 0,
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    IsDeleted = x.IsDeleted as bool? ?? false,
                    BatchNo = x.BatchNo as System.String ?? "",
                    ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                    Discount = x.Discount as decimal? ?? 0,
                    IsOrder = x.IsOrder,
                    ReceivedQty = x.ReceivedQty as decimal? ?? 0,

                    Product =
                    {
                      Name=x.ProductName as System.String ?? "",
                      Status = x.Status,
                    }
                };
                return salesOrderEstimateItem;
            });


            var salesOrders = salesOrderEstimate.Select(x =>
            {
                x.SalesOrderEstimateItem = salesOrderEstimateItems.Where(y => y.SalesOrderEstimateId == x.Id).Select(z => z).ToList();
                x.SalesEstimateItem = salesOrderEstimateItems.Where(y => y.SalesOrderEstimateId == x.Id).Select(z => z).ToList();
                return x;
            });
            return salesOrders;
        }

        public async Task<int> SalesTemplatedCount(SalesTemplate data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTemplateTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(SalesTemplateTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SalesTemplateTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        public async Task<IEnumerable<SalesTemplate>> SalesTemplateList(SalesTemplate data, string instanceid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            Dictionary<string, object> parmsSalesTemplateId = new Dictionary<string, object>();
            Dictionary<string, object> parmsProductId = new Dictionary<string, object>();

            var Pageno = (data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize;
            parms.Add("AccountId", data.AccountId);
            parms.Add("InstanceId", instanceid);
            parms.Add("PageNo", Pageno);
            parms.Add("PageSize", data.Page.PageSize);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesTemplateHistoryList", parms);
            if (result.Count() <= 0)
            { return null; }
            var salesTemplate = result.Select(x =>
            {
                var salesTemp = new SalesTemplate
                {
                    Prefix = x.Prefix as System.String ?? "",
                    TemplateNo = x.TemplateNo as System.String ?? "",
                    TemplateDate = x.TemplateDate as DateTime? ?? DateTime.MinValue,
                    TemplateName = x.TemplateName as System.String ?? "",
                    IsActive = x.IsActive,
                    Id = x.Id,
                };
                return salesTemp;
            });
            var SalesTemplateId = string.Join(",", result.Select(x => x.Id).ToList());
            parmsSalesTemplateId.Add("AccountId", data.AccountId);
            parmsSalesTemplateId.Add("InstanceId", instanceid);
            parmsSalesTemplateId.Add("SalesTemplateId", SalesTemplateId);
            var resultSalesTemplateId = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesTemplateHistoryItemList", parmsSalesTemplateId);
            var salesTemplateItems = resultSalesTemplateId.Select(x =>
            {
                var salesTemplateItem = new SalesTemplateItem
                {
                    Id = x.Id as System.String ?? "",
                    ProductId = x.ProductId as System.String ?? "",
                    TemplateId = x.TemplateId as System.String ?? "",
                    Quantity = x.Quantity as decimal? ?? 0,
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    IsDeleted = x.IsDeleted as bool? ?? false,
                    Product =
                    {
                      Name=x.ProductName as System.String ?? "",
                      Status = x.Status,
                    }
                };
                return salesTemplateItem;
            });

            var salesOrders = salesTemplate.Select(x =>
            {
                x.SalesTemplateItem = salesTemplateItems.Where(y => y.TemplateId == x.Id).Select(z => z).ToList();
                return x;
            });
            return salesOrders;
        }
        public async Task<SalesTemplate> UpdateSalesTemplateStatus(SalesTemplate data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTemplateTable.Table, OperationType.Update);
            qb.AddColumn(SalesTemplateTable.IsActiveColumn, SalesTemplateTable.UpdatedAtColumn, SalesTemplateTable.UpdatedByColumn);
            qb.Parameters.Add(SalesTemplateTable.IsActiveColumn.ColumnName, data.IsActive);
            qb.Parameters.Add(SalesTemplateTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(SalesTemplateTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.ConditionBuilder.And(SalesTemplateTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SalesTemplateTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(SalesTemplateTable.IdColumn, data.Id);

            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task<SalesOrderEstimate> GetSalesOrderById(string salesOrderEstimateId, string AccountId, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateTable.Table, OperationType.Select);
            qb.AddColumn(SalesOrderEstimateTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(SalesOrderEstimateTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SalesOrderEstimateTable.IdColumn, salesOrderEstimateId);

            var result = (await List<SalesOrderEstimate>(qb)).ToList().FirstOrDefault();


            result.SalesOrderEstimateItem = await GetSalesOrderItems(Convert.ToInt32(result.SalesOrderEstimateType), salesOrderEstimateId, AccountId, instanceid);
            result.Patient = await GetPatientById(result.PatientId, AccountId);
            return result;

        }

        public async Task<Patient> GetPatientById(string PatientId, string AccountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PatientTable.Table, OperationType.Select);
            qb.AddColumn(PatientTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(PatientTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(PatientTable.IdColumn, PatientId);
            return (await List<Patient>(qb)).ToList().FirstOrDefault();
        }

        public async Task<List<SalesOrderEstimateItem>> GetSalesOrderItems(int SalesOrderEstimateType, string salesOrderEstimateId, string AccountId, string instanceid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Select);
            qb.AddColumn(SalesOrderEstimateItemTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.SalesOrderEstimateIdColumn, salesOrderEstimateId);

            var statusCondition = new CustomCriteria(SalesOrderEstimateItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            var cc = new CriteriaColumn(SalesOrderEstimateItemTable.IsDeletedColumn, "0", CriteriaEquation.Equal);
            var col = new CustomCriteria(cc, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col);

            var result = (await List<SalesOrderEstimateItem>(qb)).ToList();
            var data = result.Select(async (y) =>
            {
                y.Product = await _productDataAccess.GetProductById(y.ProductId);
                y.ProductMaster = await GetProductMasterProductById(y.ProductId);
                if (SalesOrderEstimateType == 2)
                {
                    y.ProductStock = await GetProductStockProductById(y.ProductId, y.ProductStockId);
                }
                return y;
            });
            return (await Task.WhenAll(data)).OrderBy(x => x.Product.Name).ToList();

        }

        public async Task<ProductStock> GetProductStockProductById(string ProductId, string ProductStockId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn);
            qb.ConditionBuilder.And(ProductStockTable.IdColumn);
            qb.Parameters.Add(ProductStockTable.ProductIdColumn.ColumnName, ProductId);
            qb.Parameters.Add(ProductStockTable.IdColumn.ColumnName, ProductStockId);
            qb.SelectBuilder.MakeDistinct = true;
            return (await List<ProductStock>(qb)).FirstOrDefault();
        }
        public async Task<ProductMaster> GetProductMasterProductById(string productId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductMasterTable.IdColumn);
            qb.Parameters.Add(ProductMasterTable.IdColumn.ColumnName, productId);
            qb.SelectBuilder.MakeDistinct = true;
            return (await List<ProductMaster>(qb)).FirstOrDefault();
        }

        public async Task<SalesOrderEstimateItem> updateSalesOrderEstimateItem(SalesOrderEstimateItem data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(SalesOrderEstimateItemTable.IdColumn, data.Id);
            qb.Parameters.Add(SalesOrderEstimateItemTable.QuantityColumn.ColumnName, data.Quantity);
            qb.Parameters.Add(SalesOrderEstimateItemTable.DiscountColumn.ColumnName, data.Discount);
            qb.Parameters.Add(SalesOrderEstimateItemTable.BatchNoColumn.ColumnName, data.BatchNo);
            qb.Parameters.Add(SalesOrderEstimateItemTable.ExpireDateColumn.ColumnName, data.ExpireDate);
            qb.Parameters.Add(SalesOrderEstimateItemTable.IsDeletedColumn.ColumnName, data.IsDeleted);
            qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(SalesOrderEstimateItemTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            if (data.IsDeleted == true)
            {
                qb.Parameters.Add(SalesOrderEstimateItemTable.IsOrderColumn.ColumnName, 2);
            }
            else
            {
                qb.Parameters.Add(SalesOrderEstimateItemTable.IsOrderColumn.ColumnName, "");
            }
            qb.AddColumn(SalesOrderEstimateItemTable.DiscountColumn, SalesOrderEstimateItemTable.QuantityColumn, SalesOrderEstimateItemTable.IsDeletedColumn, SalesOrderEstimateItemTable.BatchNoColumn, SalesOrderEstimateItemTable.ExpireDateColumn, SalesOrderEstimateItemTable.IsOrderColumn);
            await QueryExecuter.NonQueryAsyc(qb);

            var salesOrderQty = await getSalesOrderReceivedQty(data.SalesOrderEstimateId, data.AccountId, data.InstanceId, data.ProductId);
            if (salesOrderQty != null)
            {
                foreach (var salesOrder in salesOrderQty)
                {
                    if (salesOrder.Quantity == salesOrder.ReceivedQty)
                    {
                        var qb1 = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateItemTable.Table, OperationType.Update);
                        qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.AccountIdColumn, data.AccountId);
                        qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.InstanceIdColumn, data.InstanceId);
                        qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.ProductIdColumn, data.ProductId);
                        qb1.ConditionBuilder.And(SalesOrderEstimateItemTable.SalesOrderEstimateIdColumn, data.SalesOrderEstimateId);
                        qb1.AddColumn(SalesOrderEstimateItemTable.IsOrderColumn, SalesOrderEstimateItemTable.UpdatedAtColumn, SalesOrderEstimateItemTable.UpdatedByColumn);
                        qb1.Parameters.Add(SalesOrderEstimateItemTable.IsOrderColumn.ColumnName, 1);
                        qb1.Parameters.Add(SalesOrderEstimateItemTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
                        qb1.Parameters.Add(SalesOrderEstimateItemTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
                        await QueryExecuter.NonQueryAsyc(qb1);
                    }
                }
            }

            return data;
        }

        public async Task<List<ProductStock>> GetSelectedProductStockList(string TemplateId, string AccountId, string InstanceId, string Types)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("TemplateId", TemplateId);
            parms.Add("Types", Types);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetTemplate_StockList", parms);

            if (Types == "Template")
            {
                var productStockList = result.Select(x =>
                {

                    var productStock = new ProductStock
                    {
                        SellingPrice = x.SellingPrice as decimal? ?? 0,
                        MRP = x.MRP as decimal? ?? 0,
                        VAT = x.VAT as decimal? ?? 0,
                        Stock = x.Stock as decimal? ?? 0,
                        PackageSize = x.PackageSize as decimal? ?? 0,
                        PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? 0,
                        PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                        Igst = x.Igst as decimal? ?? 0,
                        Cgst = x.Cgst as decimal? ?? 0,
                        Sgst = x.Sgst as decimal? ?? 0,
                        GstTotal = x.GstTotal as decimal? ?? 0,
                        CST = x.CST as decimal? ?? 0,
                        BatchNo = x.BatchNo as System.String ?? "",
                        ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        TaxType = x.TaxType as System.String ?? "",
                        ProductId = x.ProductId as System.String ?? "",
                        Id = x.stockid,
                        OrderQty = x.OrderQty as decimal? ?? 0,
                        Product =
                {
                  Name = x.Name as System.String ?? "",
                  Schedule = x.Schedule as System.String ?? "",

                  ProductInstance =
                    {
                        BoxNo = x.BoxNo  as System.String ?? "",
                        RackNo = x.RackNo  as System.String ?? "",
                    }
                },

                    };

                    return productStock;
                });
                return productStockList.ToList();
            }
            else
            {
                var productStockList = result.Select(x =>
                {

                    var productStock = new ProductStock
                    {
                        SellingPrice = x.SellingPrice as decimal? ?? 0,
                        MRP = x.MRP as decimal? ?? 0,
                        VAT = x.VAT as decimal? ?? 0,
                        Stock = x.Stock as decimal? ?? 0,
                        PackageSize = x.PackageSize as decimal? ?? 0,
                        PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? 0,
                        PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                        Igst = x.Igst as decimal? ?? 0,
                        Cgst = x.Cgst as decimal? ?? 0,
                        Sgst = x.Sgst as decimal? ?? 0,
                        GstTotal = x.GstTotal as decimal? ?? 0,
                        CST = x.CST as decimal? ?? 0,
                        BatchNo = x.BatchNo as System.String ?? "",
                        ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        TaxType = x.TaxType as System.String ?? "",
                        ProductId = x.ProductId as System.String ?? "",
                        Id = x.stockid,
                        OrderQty = x.OrderQty as decimal? ?? 0,
                        OrderEstimateDiscount = x.Discount as decimal? ?? 0,
                        OrderEstimateCutomerName = x.PatientName as System.String ?? "",
                        OrderEstimateCutomerMobile = x.PatientMobile as System.String ?? "",
                        OrderEstimateCutomerId = x.PatientId as System.String ?? "",
                        OrderEstimateType = x.SalesOrderEstimateType,
                        Product =
                {
                  Name = x.Name as System.String ?? "",
                  Schedule = x.Schedule as System.String ?? "",

                  ProductInstance =
                    {
                        BoxNo = x.BoxNo  as System.String ?? "",
                        RackNo = x.RackNo  as System.String ?? "",
                    }
                },

                    };

                    return productStock;
                });
                return productStockList.ToList();
            }


            // return productStockList.ToList().FirstOrDefault();

        }
        public async Task<string> getSalesOrderEstimateFinYear(SalesOrderEstimate data)
        {
            string sFinyr = await _HelperDataAccess.GetFinyear(CustomDateTime.IST, data.InstanceId, data.AccountId);
            return Convert.ToString(sFinyr);
        }
        public async Task<string> getSalesTemplateFinYear(SalesTemplate data)
        {
            string sFinyr = await _HelperDataAccess.GetFinyear(CustomDateTime.IST, data.InstanceId, data.AccountId);
            return Convert.ToString(sFinyr);
        }

        public async Task<SalesTemplateItem> UpdateSalesTemplateItem(SalesTemplateItem data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesTemplateItemTable.Table, OperationType.Update);
            qb.AddColumn(SalesTemplateItemTable.IsDeletedColumn, SalesTemplateItemTable.QuantityColumn, SalesTemplateItemTable.UpdatedAtColumn, SalesTemplateItemTable.UpdatedByColumn);
            qb.Parameters.Add(SalesTemplateItemTable.IsDeletedColumn.ColumnName, data.IsDeleted);
            qb.Parameters.Add(SalesTemplateItemTable.QuantityColumn.ColumnName, data.Quantity);
            qb.Parameters.Add(SalesTemplateItemTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(SalesTemplateItemTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.ConditionBuilder.And(SalesTemplateItemTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SalesTemplateItemTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(SalesTemplateItemTable.IdColumn, data.Id);

            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }

        public async Task<string> CheckUniqueTemplate(SalesTemplate data)
        {
            return await CheckTemplateExist(data, data.InstanceId);
        }
        public async Task<string> CheckTemplateExist(SalesTemplate data, string instanceid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Accountid", data.AccountId);
            parms.Add("Instanceid", instanceid);
            parms.Add("TemplateName", data.TemplateName);

            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_CheckSalesTemplateExist", parms);
            var res = result.Select(item =>
            {
                var obj = new SalesTemplate()
                {
                    TemplateNo = item.TemplateNo //SalesTemplateCount = item.count
                };
                return obj;
            }).FirstOrDefault();
            if (res != null)
                return res.TemplateNo;
            else
                return null;
        }

        public async Task<List<SalesOrderEstimate>> GetSelectedCustomerOrderEstimateList(string PatientId, string AccountId, string InstanceId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            Dictionary<string, object> parmsSalesOrderEstimateId = new Dictionary<string, object>();
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("PatientId", PatientId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetCustomerOrderEstimateList", parms);
            var salesOrderEstimate = result.Select(x =>
            {
                var salesOrder = new SalesOrderEstimate
                {
                    Prefix = x.Prefix as System.String ?? "",
                    OrderEstimateId = x.OrderEstimateId as System.String ?? "",
                    OrderEstimateDate = x.OrderEstimateDate as DateTime? ?? DateTime.MinValue,
                    PatientId = x.PatientId as System.String ?? "",
                    SalesOrderEstimateType = x.SalesOrderEstimateType,
                    Id = x.Id,
                    IsEstimateActive = x.IsEstimateActive,

                    Patient =
                    {
                        Name=x.PatientName as System.String ?? "",
                        Mobile=x.PatientMobile as System.String ?? "",
                        Email=x.PatientEmail as System.String ?? ""
                    }
                };
                return salesOrder;
            });
            var SalesOrderEstimateId = string.Join(",", result.Select(x => x.Id).ToList());
            parmsSalesOrderEstimateId.Add("AccountId", AccountId);
            parmsSalesOrderEstimateId.Add("InstanceId", InstanceId);
            parmsSalesOrderEstimateId.Add("SalesOrderEstimateId", SalesOrderEstimateId);
            var resultSalesOrderEstimateId = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetSalesOrderEstimateHistoryItemList", parmsSalesOrderEstimateId);
            var salesOrderEstimateItems = resultSalesOrderEstimateId.Select(x =>
            {
                var salesOrderEstimateItem = new SalesOrderEstimateItem
                {
                    Id = x.Id as System.String ?? "",
                    ProductId = x.ProductId as System.String ?? "",
                    SalesOrderEstimateId = x.SalesOrderEstimateId as System.String ?? "",
                    Quantity = x.Quantity as decimal? ?? 0,
                    SellingPrice = x.SellingPrice as decimal? ?? 0,
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    IsDeleted = x.IsDeleted as bool? ?? false,
                    BatchNo = x.BatchNo as System.String ?? "",
                    ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue,
                    Discount = x.Discount as decimal? ?? 0,
                    IsOrder = x.IsOrder,
                    ReceivedQty = x.ReceivedQty as decimal? ?? 0,

                    Product =
                    {
                      Name=x.ProductName as System.String ?? "",
                      Status = x.Status,
                    }
                };
                return salesOrderEstimateItem;
            });


            var salesOrders = salesOrderEstimate.Select(x =>
            {
                x.SalesOrderEstimateItem = salesOrderEstimateItems.Where(y => y.SalesOrderEstimateId == x.Id).Select(z => z).ToList();
                x.SalesEstimateItem = salesOrderEstimateItems.Where(y => y.SalesOrderEstimateId == x.Id).Select(z => z).ToList();
                return x;
            });
            return salesOrders.ToList();
        }

        public async Task<SalesOrderEstimate> updateSalesOrderEstimateConvertedPending(SalesOrderEstimate data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SalesOrderEstimateTable.Table, OperationType.Update);
            qb.ConditionBuilder.And(SalesOrderEstimateTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(SalesOrderEstimateTable.InstanceIdColumn, data.InstanceId);
            qb.ConditionBuilder.And(SalesOrderEstimateTable.IdColumn, data.Id);
            qb.AddColumn(SalesOrderEstimateTable.IsEstimateActiveColumn, SalesOrderEstimateTable.UpdatedAtColumn, SalesOrderEstimateTable.UpdatedByColumn);
            qb.Parameters.Add(SalesOrderEstimateTable.IsEstimateActiveColumn.ColumnName, 1);
            qb.Parameters.Add(SalesOrderEstimateTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
            qb.Parameters.Add(SalesOrderEstimateTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }
        // Added by Gavaskar 03-10-2017 Save Sales Order End
        #endregion


        public async Task<SaleSettings> SaveRoundOff(SaleSettings data)
        {
            var count = await CountSaleSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.IsEnableRoundOffColumn);
                qb.Parameters.Add(SaleSettingsTable.IsEnableRoundOffColumn.ColumnName, data.IsEnableRoundOff);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }
        public async Task<SaleSettings> SaveFreeQty(SaleSettings data)
        {
            var count = await CountSaleSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.IsEnableFreeQtyColumn);
                qb.Parameters.Add(SaleSettingsTable.IsEnableFreeQtyColumn.ColumnName, data.IsEnableFreeQty);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }        
        
        public async Task<bool> GetRoundOffSettings(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.IsEnableRoundOffColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, instanceId);
            var saleSettings = await List<SaleSettings>(qb);
            bool type = true;
            if (saleSettings.Count > 0)
            {
                var data = saleSettings.First();
                if (data.IsEnableRoundOff == null)
                {
                    data.IsEnableRoundOff = true;
                }
                type = Convert.ToBoolean(data.IsEnableRoundOff);
            }
            return type;
        }

        //Added by Sarubala on 27-03-19 - start
        public async Task<bool> GetSalesScreenSettings(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.IsEnableNewSalesScreenColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, instanceId);
            var saleSettings = await List<SaleSettings>(qb);
            bool type = false;
            if (saleSettings.Count > 0)
            {
                var data = saleSettings.First();
                if (data.IsEnableNewSalesScreen == null)
                {
                    data.IsEnableNewSalesScreen = false;
                }
                type = Convert.ToBoolean(data.IsEnableNewSalesScreen);
            }
            return type;
        }

        public async Task<SaleSettings> SaveSalesScreenSettings(SaleSettings data)
        {
            var count = await CountSaleSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.IsEnableNewSalesScreenColumn, SaleSettingsTable.UpdatedAtColumn, SaleSettingsTable.UpdatedByColumn);
                qb.Parameters.Add(SaleSettingsTable.IsEnableNewSalesScreenColumn.ColumnName, data.IsEnableNewSalesScreen);
                qb.Parameters.Add(SaleSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(SaleSettingsTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }

        //Added by Sarubala on 27-03-19 - end
        public async Task<bool> getFreeQtySettings(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.IsEnableFreeQtyColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, instanceId);
            var saleSettings = await List<SaleSettings>(qb);
            bool type = true;
            if (saleSettings.Count > 0)
            {
                var data = saleSettings.First();
                if (data.IsEnableFreeQty == null)
                {
                    data.IsEnableFreeQty = true;
                }
                type = Convert.ToBoolean(data.IsEnableFreeQty);
            }
            return type;
        }        

        public async Task<SaleSettings> SaveSaleUserSettings(SaleSettings data)
        {
            var count = await CountSaleSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Update);
                qb.AddColumn(SaleSettingsTable.IsEnableSalesUserColumn);
                qb.Parameters.Add(SaleSettingsTable.IsEnableSalesUserColumn.ColumnName, data.IsEnableSalesUser);
                qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, SaleSettingsTable.Table);
            }
        }

        public async Task<bool> GetSaleUserSettings(string accountId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(SaleSettingsTable.Table, OperationType.Select);
            qb.AddColumn(SaleSettingsTable.IsEnableSalesUserColumn);
            qb.ConditionBuilder.And(SaleSettingsTable.AccountIdColumn, accountId);
            qb.ConditionBuilder.And(SaleSettingsTable.InstanceIdColumn, instanceId);
            var saleSettings = await List<SaleSettings>(qb);
            bool type = false;
            if (saleSettings.Count > 0)
            {
                type = Convert.ToBoolean(saleSettings.FirstOrDefault().IsEnableSalesUser);
            }
            return type;
        }

        public async Task<bool> SendOTPToAPI(string mobileNo)
        {
            string url = "https://qucentis.com/ayu/dispatch-code";
            
            var sc = new RestConnector();
            MobileVerifySearch mobile = new MobileVerifySearch
            {
                phone = "+" + mobileNo
            };
            var result = await sc.PostAsycMobileVerify<string>(url, mobile);

            var result1 = JsonConvert.DeserializeObject<MobileVerifyResponse>(result);

            if(result1.error == null)
            {
                return true;
            }
            else
            {
                return false;
            }            

        }

        public async Task<bool> VerifyOTPAPI(string otp,string mobileNo)
        {
            string url = "https://qucentis.com/ayu/verify-code";

            var sc = new RestConnector();
            OTPVerifySearch otpCode = new OTPVerifySearch
            {
                phone = "+" + mobileNo,
                code = otp
            };
            var result = await sc.PostAsycMobileVerify<dynamic>(url, otpCode);

            var result1 = JsonConvert.DeserializeObject<MobileVerifyResponse>(result);

            if (result1.error == null)
            {
                if (result1.verified)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            else
            {
                return false;
            }

        }
    }
}
