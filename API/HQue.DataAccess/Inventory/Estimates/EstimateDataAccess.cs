﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Criteria;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using HQue.Contract.Infrastructure.Estimates;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using HQue.DataAccess.Helpers.Extension;

namespace HQue.DataAccess.Inventory.Estimates
{
    public class EstimateDataAccess : BaseDataAccess
    {
        public EstimateDataAccess(ISqlHelper sqlQueryExecuter, QueryBuilderFactory queryBuilderFactory) : base(sqlQueryExecuter, queryBuilderFactory)
        {
        }

        public override Task<Estimate> Save<Estimate>(Estimate data)
        {
            return string.IsNullOrEmpty(data.Id) ? Insert(data, EstimateTable.Table) : Update(data, EstimateTable.Table);
        }

        public async Task Save(IEnumerable<EstimateItem> itemList)
        {
            foreach (var estimateItem in itemList)
            {
                if (estimateItem.IsNew)
                    await Insert(estimateItem, EstimateItemTable.Table);
                else
                    await Update(estimateItem, EstimateItemTable.Table);
            }
        }

        public Task<List<EstimateItem>> ItemList(string estimateId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(EstimateItemTable.Table, OperationType.Select);
            qb.AddColumn(EstimateItemTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.LeftJoin(ProductStockTable.Table, ProductStockTable.IdColumn, EstimateItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());
            
            qb.ConditionBuilder.And(EstimateItemTable.EstimateIdColumn, estimateId);

            return List<EstimateItem>(qb);
        }

        public async Task<string> GetCustomInvoiceNo(string instanceId)
        {
            var query = QueryBuilderFactory.GetQueryBuilder(EstimateTable.Table,OperationType.Select);
            query.AddColumn(new MaxDbColumn(EstimateTable.EstimateNoColumn.Convert("INT")).IsNull("0"));
            query.ConditionBuilder.And(EstimateTable.InstanceIdColumn, instanceId);
            var estimateNo = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(query)) + 1;
            return estimateNo.ToString();
        }

        public async Task<Estimate> GetById(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(EstimateItemTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(EstimateTable.IdColumn);
            qb.Parameters.Add(EstimateTable.IdColumn.ColumnName, id);

            qb.AddColumn(EstimateItemTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(EstimateTable.Table, EstimateTable.IdColumn, EstimateItemTable.EstimateIdColumn);
            qb.JoinBuilder.AddJoinColumn(EstimateTable.Table, EstimateTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, EstimateItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(InstanceTable.Table, InstanceTable.IdColumn, EstimateTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.Table.ColumnList.ToArray());

            qb.SelectBuilder.SortByDesc(EstimateTable.CreatedAtColumn);

            var list = new List<Estimate>();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var estimateItem = new EstimateItem().Fill(reader);
                    estimateItem.ProductStock.Fill(reader).Product.Fill(reader);

                    var est = new Estimate().Fill(reader);
                    est.Instance.Fill(reader);
                    est.EstimateItem.Add(estimateItem);

                    list.Add(est);
                }
            }

            var estimate = list.FirstOrDefault();
            if (estimate == null)
                return new Estimate();

            estimate.EstimateItem = list.SelectMany(x => x.EstimateItem).ToList();

            return estimate;
        }

        public async Task<int> Count(Estimate data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(EstimateTable.Table, OperationType.Count);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        private QueryBuilderBase SqlQueryBuilder(Estimate data, QueryBuilderBase qb)
        {
            if (string.IsNullOrEmpty(data.AccountId) || string.IsNullOrEmpty(data.InstanceId))
                return qb;

            qb.ConditionBuilder.And(EstimateTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(EstimateTable.InstanceIdColumn, data.InstanceId);
            return qb;
        }

        public async Task<IEnumerable<Estimate>> List(Estimate data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(EstimateTable.Table, OperationType.Select);

            qb.AddColumn(EstimateTable.Table.ColumnList.ToArray());

            qb = SqlQueryBuilder(data, qb);
            qb.SelectBuilder.SortByDesc(EstimateTable.CreatedAtColumn);
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);

            var result = await List<Estimate>(qb);
            var si = await SqlQueryList(result);

            
            var estimates = result.Select(x =>
            {
                x.EstimateItem = si.Where(y => y.EstimateId == x.Id).ToList();
                return x;
            });

            return estimates;
        }

        private async Task<List<EstimateItem>> SqlQueryList(IEnumerable<Estimate> data)
        {
            var siqb = QueryBuilderFactory.GetQueryBuilder(EstimateItemTable.Table, OperationType.Select);
            siqb.AddColumn(EstimateItemTable.Table.ColumnList.ToArray());

            siqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, EstimateItemTable.ProductStockIdColumn);
            siqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());

            siqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            siqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());

            var i = 1;
            foreach (var estimate in data)
            {
                var paramName = $"s{i++}";
                var caol = new CriteriaColumn(EstimateItemTable.EstimateIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(caol);
                siqb.ConditionBuilder.Or(cc);
                siqb.Parameters.Add(paramName, estimate.Id);
            }

            var list = new List<EstimateItem>();

            using (var reader = await QueryExecuter.QueryAsyc(siqb))
            {
                while (reader.Read())
                {
                    var estimateItem = new EstimateItem().Fill(reader);

                    estimateItem.ProductStock.Fill(reader)
                        .Product.Fill(reader);
                    list.Add(estimateItem);
                }
            }
            return list;
        }
    }
}
