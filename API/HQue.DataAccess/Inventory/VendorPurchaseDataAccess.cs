﻿using System.Collections.Generic;
using HQue.Contract.Infrastructure.Inventory;
using HQue.DataAccess.DbModel;
using System;
using System.Linq;
using System.Threading.Tasks;
using HQue.DataAccess.Master;
using HQue.Contract.Infrastructure.Audits;
using DataAccess.ManageData.Interface;
using DataAccess.QueryBuilder;
using DataAccess.Criteria;
using Utilities.Enum;
using HQue.Contract.Infrastructure.Settings;
using HQue.DataAccess.Accounts;
using HQue.Contract.Infrastructure.Accounts;
using HQue.Contract.Infrastructure.Master;
// Done 23-02-2017
using Dapper;
using DataAccess;
using Utilities.Helpers;
using HQue.DataAccess.Helpers;
using HQue.DataAccess.Helpers.Extension;
using Newtonsoft.Json;
using HQue.Contract.External;
using HQue.Contract.Base;
using System.Data.SqlClient;
using System.Data;

namespace HQue.DataAccess.Inventory
{
    public class VendorPurchaseDataAccess : BaseDataAccess
    {
        // Done 23-02-2017
        SqlDatabaseHelper sqldb;
        private readonly ConfigHelper _configHelper;
        private readonly ProductDataAccess _productDataAccess;
        private readonly ProductStockDataAccess _productStockDataAccess;
        private readonly HelperDataAccess _HelperDataAccess;
        private readonly VendorDataAccess _vendorDataAccess;

        public VendorPurchaseDataAccess(ISqlHelper sqlQueryExecuter, ProductDataAccess productDataAccess, ProductStockDataAccess productStockDataAccess, HelperDataAccess helperDataAccess, QueryBuilderFactory queryBuilderFactory, ConfigHelper configHelper,
            VendorDataAccess vendorDataAccess) : base(sqlQueryExecuter, queryBuilderFactory)
        {
            _productDataAccess = productDataAccess;
            _productStockDataAccess = productStockDataAccess;
            _HelperDataAccess = helperDataAccess; //Added by Poongodi on 26/03/2017
            // Done 23-02-2017
            _configHelper = configHelper;
            sqldb = new SqlDatabaseHelper(_configHelper.AppConfig.ConnectionString);
            _vendorDataAccess = vendorDataAccess;
        }
        public async Task SaveBuyTemplateSetting(List<TemplatePurchaseChildMaster> listTemplates, string VendorId)
        {
            //var count = await CountTemplatePurchase(listTemplates, VendorId);
            var qb = QueryBuilderFactory.GetQueryBuilder(TemplatePurchaseChildMasterTable.Table, OperationType.Delete);
            qb.ConditionBuilder.And(TemplatePurchaseChildMasterTable.AccountIdColumn, listTemplates[0].AccountId);
            qb.ConditionBuilder.And(TemplatePurchaseChildMasterTable.InstanceIdColumn, listTemplates[0].InstanceId);
            qb.ConditionBuilder.And(TemplatePurchaseChildMasterTable.VendorIdColumn, listTemplates[0].VendorId);
            await QueryExecuter.NonQueryAsyc(qb);
            foreach (var temp in listTemplates)
            {
                await Insert(temp, TemplatePurchaseChildMasterTable.Table);
            }
        }

        public async Task<TemplatePurchaseChildMasterSettings> SaveBuyTemplateChildSetting(TemplatePurchaseChildMasterSettings templatePurchaseChildMasterSettings, string VendorId)
        {
            var count = await CountTemplatePurchaseChild(templatePurchaseChildMasterSettings, VendorId);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(TemplatePurchaseChildMasterSettingsTable.Table, OperationType.Delete);
                qb.ConditionBuilder.And(TemplatePurchaseChildMasterSettingsTable.AccountIdColumn, templatePurchaseChildMasterSettings.AccountId);
                qb.ConditionBuilder.And(TemplatePurchaseChildMasterSettingsTable.InstanceIdColumn, templatePurchaseChildMasterSettings.InstanceId);
                qb.ConditionBuilder.And(TemplatePurchaseChildMasterSettingsTable.VendorIdColumn, templatePurchaseChildMasterSettings.VendorId);
                await QueryExecuter.NonQueryAsyc(qb);
                return await Insert(templatePurchaseChildMasterSettings, TemplatePurchaseChildMasterSettingsTable.Table);
            }
            else
            {
                return await Insert(templatePurchaseChildMasterSettings, TemplatePurchaseChildMasterSettingsTable.Table);
            }
        }
        public async Task<int> CountTemplatePurchase(TemplatePurchaseChildMaster listTemplates, string VendorId)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(TemplatePurchaseChildMasterTable.Table, OperationType.Count);
            //qb.ConditionBuilder.And(TemplatePurchaseChildMasterTable.AccountIdColumn, listTemplates.AccountId);
            //qb.ConditionBuilder.And(TemplatePurchaseChildMasterTable.InstanceIdColumn, listTemplates.InstanceId);
            //return Convert.ToInt32(await SingleValueAsyc(qb));
            var query = $@"select count(*) Count from TemplatePurchaseChildMaster a inner join TemplatePurchaseChildMasterSettings b on a.AccountId=b.AccountId and a.InstanceId=b.InstanceId and a.VendorId=b.VendorId where b.AccountId='{listTemplates.AccountId}' and b.InstanceId='{listTemplates.InstanceId}' and b.VendorId='{VendorId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<int> CountTemplatePurchaseChild(TemplatePurchaseChildMasterSettings templatePurchaseChildMasterSettings, string VendorId)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(TemplatePurchaseChildMasterSettingsTable.Table, OperationType.Count);
            //qb.ConditionBuilder.And(TemplatePurchaseChildMasterSettingsTable.AccountIdColumn, templatePurchaseChildMasterSettings.AccountId);
            //qb.ConditionBuilder.And(TemplatePurchaseChildMasterSettingsTable.InstanceIdColumn, templatePurchaseChildMasterSettings.InstanceId);
            //return Convert.ToInt32(await SingleValueAsyc(qb));
            var query = $@"select count(*) Count from TemplatePurchaseChildMaster a inner join TemplatePurchaseChildMasterSettings b on a.AccountId=b.AccountId and a.InstanceId=b.InstanceId and a.VendorId=b.VendorId where b.AccountId='{templatePurchaseChildMasterSettings.AccountId}' and b.InstanceId='{templatePurchaseChildMasterSettings.InstanceId}' and b.VendorId='{VendorId}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<List<TemplatePurchaseChildMaster>> getBuyTemplateSetting(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TemplatePurchaseChildMasterTable.Table, OperationType.Select);
            qb.AddColumn(TemplatePurchaseChildMasterTable.HeaderIndexColumn, TemplatePurchaseChildMasterTable.HeaderNameColumn);
            qb.ConditionBuilder.And(TemplatePurchaseChildMasterTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(TemplatePurchaseChildMasterTable.InstanceIdColumn, InstanceId);
            var userTypes = await List<TemplatePurchaseChildMaster>(qb);
            return userTypes;
            //var query = $@"select b.HeaderOption,b.VendorRowPos,b.ProductRowPos,a.VendorHeaderName,b.HeaderName from TemplatePurchaseMaster a
            //inner join TemplatePurchaseChildMaster b on a.id=b.TemplateColumnId where b.AccountId='{AccountId}' and b.InstanceId='{InstanceId}'";
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            //var list = new List<TemplatePurchaseMaster>();
            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var item = new TemplatePurchaseMaster
            //        {
            //            VendorHeaderName = reader["VendorHeaderName"].ToString(),
            //            TemplateColumnId = reader["VendorHeaderId"].ToString()
            //        };
            //        list.Add(item);
            //    }
            //}
            //return list;
        }
        public override Task<VendorPurchase> Save<VendorPurchase>(VendorPurchase data)
        {
            //added by nandhini for transaction
            if (data.WriteExecutionQuery)
            {
                SetInsertMetaData(data, VendorPurchaseTable.Table);
                WriteInsertExecutionQuery(data, VendorPurchaseTable.Table);
                return Task.FromResult(data);
            }
            else
            {
                return Insert(data, VendorPurchaseTable.Table);
            }
        }
        public Task<DraftVendorPurchase> SaveDraftVendorPurchase(DraftVendorPurchase data)
        {
            return Insert(data, DraftVendorPurchaseTable.Table);
        }
        public async Task DeleteDraftVendorPurchase(string draftVendorPurchaseId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DraftVendorPurchaseTable.Table, OperationType.Delete);
            qb.ConditionBuilder.And(DraftVendorPurchaseTable.IdColumn, draftVendorPurchaseId);
            qb.ConditionBuilder.And(DraftVendorPurchaseTable.InstanceIdColumn, InstanceId);
            await QueryExecuter.NonQueryAsyc(qb);
        }
        public async Task DeleteDraftVendorPurchaseItem(string draftVendorPurchaseId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DraftVendorPurchaseItemTable.Table, OperationType.Delete);
            qb.ConditionBuilder.And(DraftVendorPurchaseItemTable.DraftVendorPurchaseIdColumn, draftVendorPurchaseId);
            qb.ConditionBuilder.And(DraftVendorPurchaseItemTable.InstanceIdColumn, InstanceId);
            await QueryExecuter.NonQueryAsyc(qb);
        }
        public async Task<IEnumerable<VendorPurchaseItem>> Save(IEnumerable<VendorPurchaseItem> itemList, int? TaxRefNo = 1, int? LocationType = 1)
        {
            //Calculation handled in js itself based on purchase formula & commented below here
            foreach (var vendorPurchaseItem in itemList)
            {
                if (vendorPurchaseItem.ProductStock.VAT == null)
                    vendorPurchaseItem.ProductStock.VAT = 0;
                //Strips Logic 
                //vendorPurchaseItem.Quantity = vendorPurchaseItem.PackageSize * vendorPurchaseItem.PackageQty;
                //decimal toatlPrice = (decimal)(vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty));
                //decimal discount = (decimal)(toatlPrice * vendorPurchaseItem.Discount / 100);
                //decimal priceAfterTax = 0;


                //if (TaxRefNo == 1)
                //{
                //    priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vendorPurchaseItem.ProductStock.GstTotal / 100));
                //    vendorPurchaseItem.ProductStock.VAT = 0;
                //    vendorPurchaseItem.ProductStock.CST = 0;
                //}
                //else
                //{
                //    if (vendorPurchaseItem.ProductStock.VAT == null)
                //        vendorPurchaseItem.ProductStock.VAT = 0;
                //    if (vendorPurchaseItem.ProductStock.CST > 0)
                //    {
                //        priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vendorPurchaseItem.ProductStock.CST / 100));
                //    }

                //    else
                //    {
                //        priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vendorPurchaseItem.ProductStock.VAT / 100));
                //    }

                //}
                //vendorPurchaseItem.ProductStock.Igst = vendorPurchaseItem.ProductStock.GstTotal;
                //vendorPurchaseItem.ProductStock.Sgst = vendorPurchaseItem.ProductStock.GstTotal/2;
                //vendorPurchaseItem.ProductStock.Cgst= vendorPurchaseItem.ProductStock.GstTotal/2;

                //Added by Sarubala on 11-09-17
                //vendorPurchaseItem.GstTotal = vendorPurchaseItem.ProductStock.GstTotal;
                //vendorPurchaseItem.Igst = vendorPurchaseItem.ProductStock.GstTotal;
                //vendorPurchaseItem.Sgst = vendorPurchaseItem.ProductStock.GstTotal / 2;
                //vendorPurchaseItem.Cgst = vendorPurchaseItem.ProductStock.GstTotal / 2;

                //if (LocationType == 1)
                //{
                //    vendorPurchaseItem.ProductStock.Igst = vendorPurchaseItem.ProductStock.GstTotal;
                //    vendorPurchaseItem.Igst = vendorPurchaseItem.ProductStock.GstTotal;
                //}


                //vendorPurchaseItem.PurchasePrice = (priceAfterTax / vendorPurchaseItem.PackageQty) / vendorPurchaseItem.PackageSize;
                vendorPurchaseItem.PurchasePrice = Math.Round((decimal)vendorPurchaseItem.PurchasePrice, 6);
                //vendorPurchaseItem.ProductStock.SellingPrice = Math.Round(Convert.ToDecimal(vendorPurchaseItem.PackageSellingPrice / vendorPurchaseItem.PackageSize), 5);
                //vendorPurchaseItem.ProductStock.MRP = Math.Round(Convert.ToDecimal(vendorPurchaseItem.PackageMRP / vendorPurchaseItem.PackageSize), 5);
                //vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize; //Added by Poongodi on 21/06/2017
                //vendorPurchaseItem.Total += vendorPurchaseItem.PackagePurchasePrice * vendorPurchaseItem.PackageQty;

                vendorPurchaseItem.ProductStock.PurchasePrice = Math.Round(Convert.ToDecimal(vendorPurchaseItem.ProductStock.PurchasePrice), 6);
                vendorPurchaseItem.ProductStock.SellingPrice = Math.Round(Convert.ToDecimal(vendorPurchaseItem.ProductStock.SellingPrice), 6);
                vendorPurchaseItem.ProductStock.MRP = Math.Round(Convert.ToDecimal(vendorPurchaseItem.ProductStock.MRP), 6);

                ProductStock productStockItem = new ProductStock();
                if (vendorPurchaseItem.fromTempId == null && vendorPurchaseItem.fromDcId == null)
                {
                    var productStockExist = await _productStockDataAccess.GetProductStock(vendorPurchaseItem.ProductStock);
                    if (productStockExist.Count > 0) // ==1 replace to >0 and loop removed by Poongodi on 22/06/2017
                    {
                        var stockItem = productStockExist[0];
                        //foreach (var stockItem in productStockExist)
                        //{
                        stockItem.Stock += vendorPurchaseItem.Quantity;
                        stockItem.Eancode = vendorPurchaseItem.ProductStock.Eancode;
                        stockItem.VendorId = vendorPurchaseItem.ProductStock.VendorId;
                        stockItem.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);
                        productStockItem = await _productStockDataAccess.Update(stockItem);
                        //}
                    }
                    else
                    {
                        vendorPurchaseItem.ProductStock.Stock = vendorPurchaseItem.Quantity;
                        //vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize;
                        //vendorPurchaseItem.ProductStock.PackagePurchasePrice = vendorPurchaseItem.PackagePurchasePrice;
                        //vendorPurchaseItem.ProductStock.PurchasePrice = vendorPurchaseItem.PurchasePrice;

                        //if (LocationType == 1)
                        //{
                        //    vendorPurchaseItem.ProductStock.Igst = vendorPurchaseItem.ProductStock.GstTotal;
                        //    vendorPurchaseItem.Igst = vendorPurchaseItem.ProductStock.GstTotal; //Added by Sarubala on 11-09-17
                        //}
                        //if (LocationType == 2)
                        //{
                        //    vendorPurchaseItem.ProductStock.Cgst = vendorPurchaseItem.ProductStock.Igst / 2;
                        //    vendorPurchaseItem.ProductStock.Sgst = vendorPurchaseItem.ProductStock.Igst / 2;
                        //    //Added by Sarubala on 11-09-17
                        //    vendorPurchaseItem.Cgst = vendorPurchaseItem.ProductStock.Igst / 2;
                        //    vendorPurchaseItem.Sgst = vendorPurchaseItem.ProductStock.Igst / 2;
                        //}
                        //if (LocationType == 3 || LocationType == 4)
                        //{
                        //    vendorPurchaseItem.ProductStock.Cgst = 0;
                        //    vendorPurchaseItem.ProductStock.Sgst = 0;
                        //    vendorPurchaseItem.ProductStock.Igst = 0;
                        //    vendorPurchaseItem.ProductStock.GstTotal = 0;
                        //    //Added by Sarubala on 11-09-17
                        //    vendorPurchaseItem.Cgst = 0;
                        //    vendorPurchaseItem.Sgst = 0;
                        //    vendorPurchaseItem.Igst = 0;
                        //    vendorPurchaseItem.GstTotal = 0;
                        //}


                        productStockItem = await _productStockDataAccess.Save(vendorPurchaseItem.ProductStock);

                        //Added By Sabarish For Updating UpdateProductGST
                        vendorPurchaseItem.ProductStock = productStockItem;
                    }
                    vendorPurchaseItem.ProductStockId = productStockItem.Id;
                }
                else if (vendorPurchaseItem.fromTempId != null)
                {
                    /*Modified by Settu to implement Purchase price edit for Tempstock Product*/
                    var oldTempItem = await createTempItemBalance(vendorPurchaseItem);
                    vendorPurchaseItem.fromTempId = oldTempItem.Id;
                    var productStockExist = (await _productStockDataAccess.GetTempProductStock(vendorPurchaseItem.ProductStock, vendorPurchaseItem.fromTempId)).First();
                    productStockExist.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);

                    var serialized = JsonConvert.SerializeObject(vendorPurchaseItem.ProductStock);
                    var tmpProductStock = JsonConvert.DeserializeObject<ProductStock>(serialized);
                    tmpProductStock.VendorId = productStockExist.VendorId;
                    tmpProductStock.Eancode = productStockExist.Eancode;
                    var productStockExist1 = await _productStockDataAccess.GetProductStock(tmpProductStock);

                    if (productStockExist1.Count > 0)
                    {
                        int tempQty = await getTempItemQty(vendorPurchaseItem);
                        if (vendorPurchaseItem.Quantity >= tempQty)
                        {
                            productStockExist.Stock = productStockExist.Stock + vendorPurchaseItem.Quantity - tempQty;
                        }
                        productStockExist.VendorId = vendorPurchaseItem.ProductStock.VendorId;
                        productStockExist.Eancode = vendorPurchaseItem.ProductStock.Eancode;
                        await _productStockDataAccess.Update(productStockExist);
                        vendorPurchaseItem.ProductStockId = productStockExist.Id;
                    }
                    else
                    {
                        vendorPurchaseItem.ProductStockId = productStockExist.Id;
                        productStockExist.VendorId = vendorPurchaseItem.ProductStock.VendorId;
                        productStockExist.Eancode = vendorPurchaseItem.ProductStock.Eancode;
                        if (productStockExist.Stock > vendorPurchaseItem.Quantity)
                        {
                            vendorPurchaseItem.ProductStock.Stock = vendorPurchaseItem.Quantity;
                            productStockExist.Stock = productStockExist.Stock - vendorPurchaseItem.Quantity;

                            await _productStockDataAccess.Update(productStockExist);
                            if (vendorPurchaseItem.ProductStock.Stock > 0)
                            {
                                productStockItem = await _productStockDataAccess.Save(vendorPurchaseItem.ProductStock);
                                vendorPurchaseItem.ProductStock = productStockItem;
                                vendorPurchaseItem.ProductStockId = productStockItem.Id;

                                await UpdateTempItem(vendorPurchaseItem);
                            }
                        }
                        else
                        {
                            int tempQty = await getTempItemQty(vendorPurchaseItem);
                            if (vendorPurchaseItem.Quantity >= tempQty)
                            {
                                vendorPurchaseItem.ProductStock.Stock = productStockExist.Stock + vendorPurchaseItem.Quantity - tempQty;
                            }
                            else
                            {
                                vendorPurchaseItem.ProductStock.Stock = productStockExist.Stock;
                            }
                            productStockExist.Stock = 0;
                            vendorPurchaseItem.ProductStock.Id = productStockExist.Id;
                            await _productStockDataAccess.UpdateProductStock(vendorPurchaseItem.ProductStock);
                        }
                    }
                }
                else if (vendorPurchaseItem.fromDcId != null)
                {
                    //int dcQty = await getDcItemQty(vendorPurchaseItem);                    
                    var oldDcItem1 = await getDcItem(vendorPurchaseItem, vendorPurchaseItem.fromDcId);
                    if (vendorPurchaseItem.ProductStock.VendorId == oldDcItem1.VendorId)
                    {
                        decimal? dcQty = oldDcItem1.Quantity + (oldDcItem1.FreeQty * oldDcItem1.PackageSize);
                        if (vendorPurchaseItem.Quantity >= dcQty)
                        {
                            await inActiveDcItem(vendorPurchaseItem);
                            var productStockExist = await _productStockDataAccess.GetDcProductStock(vendorPurchaseItem.ProductStock, vendorPurchaseItem.fromDcId);
                            if (productStockExist.Count == 1)
                            {
                                foreach (var stockItem in productStockExist)
                                {
                                    stockItem.Stock += vendorPurchaseItem.Quantity - dcQty;
                                    stockItem.Eancode = vendorPurchaseItem.ProductStock.Eancode;
                                    stockItem.VendorId = vendorPurchaseItem.ProductStock.VendorId;
                                    stockItem.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);
                                    productStockItem = await _productStockDataAccess.Update(stockItem);
                                }
                            }
                            vendorPurchaseItem.ProductStockId = productStockItem.Id;
                        }
                        else
                        {
                            var oldDcItem = await createDcItemBalance(vendorPurchaseItem);
                            vendorPurchaseItem.fromDcId = oldDcItem.Id;
                            var productStockExist = await _productStockDataAccess.GetDcProductStock(vendorPurchaseItem.ProductStock, vendorPurchaseItem.fromDcId);
                            vendorPurchaseItem.ProductStockId = productStockExist.First().Id;
                            await _productStockDataAccess.UpdateBarcode(vendorPurchaseItem.ProductStock);
                        }
                    }
                    else
                    {
                        //vendorPurchaseItem.ProductStock.PurchasePrice = null;
                        //vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize;  //Added by Poongodi on 21/06/2017
                        var productStockExist = await _productStockDataAccess.GetProductStock(vendorPurchaseItem.ProductStock);
                        if (productStockExist.Count == 1)
                        {
                            foreach (var stockItem in productStockExist)
                            {
                                stockItem.Stock += vendorPurchaseItem.Quantity;
                                stockItem.Eancode = vendorPurchaseItem.ProductStock.Eancode;
                                stockItem.VendorId = vendorPurchaseItem.ProductStock.VendorId;
                                stockItem.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);
                                productStockItem = await _productStockDataAccess.Update(stockItem);
                            }
                        }
                        else
                        {
                            vendorPurchaseItem.ProductStock.Stock = vendorPurchaseItem.Quantity;
                            //vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize;
                            //vendorPurchaseItem.ProductStock.PackagePurchasePrice = vendorPurchaseItem.PackagePurchasePrice;
                            //vendorPurchaseItem.ProductStock.PurchasePrice = vendorPurchaseItem.PurchasePrice;
                            productStockItem = await _productStockDataAccess.Save(vendorPurchaseItem.ProductStock);
                        }
                        vendorPurchaseItem.ProductStockId = productStockItem.Id;
                    }
                }
                /*Enabled by Poongodi To Upade rack number to the instance table*/
                /*BoxNo included by settu to update in Product Instance table*/
                if ((vendorPurchaseItem.ProductStock.Product.RackNo != null && vendorPurchaseItem.ProductStock.Product.RackNo.Trim() != "") ||
                    (vendorPurchaseItem.ProductStock.Product.BoxNo != null && vendorPurchaseItem.ProductStock.Product.BoxNo.Trim() != ""))
                {
                    Product productValue = new Product();
                    productValue.Id = vendorPurchaseItem.ProductStock.ProductId;
                    productValue.InstanceId = vendorPurchaseItem.ProductStock.InstanceId;
                    productValue.AccountId = vendorPurchaseItem.ProductStock.AccountId;
                    productValue.RackNo = vendorPurchaseItem.ProductStock.Product.RackNo;
                    productValue.BoxNo = vendorPurchaseItem.ProductStock.Product.BoxNo;
                    productValue.UpdatedBy = vendorPurchaseItem.UpdatedBy;
                    productValue.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);
                    await _productDataAccess.UpdateProductRackNo(productValue);
                }


                //Direct Value Calculations By Sabarish Start
                //if (TaxRefNo == 1)
                //{
                //    vendorPurchaseItem.VAT = 0.00M;
                //    // vendorPurchaseItem.GstValue = vendorPurchaseItem.PurchasePrice * vendorPurchaseItem.ProductStock.GstTotal / 100; // Commented by Gavaskar
                //    // Added by Gavaskar 
                //    vendorPurchaseItem.GstValue = (decimal)(toatlPrice - discount) * vendorPurchaseItem.ProductStock.GstTotal / 100;
                //    vendorPurchaseItem.VatValue = 0.00M;
                //}
                //else
                //{
                //    vendorPurchaseItem.VAT = vendorPurchaseItem.ProductStock.VAT;
                //    vendorPurchaseItem.VatValue = vendorPurchaseItem.PurchasePrice * vendorPurchaseItem.VAT / 100;
                //    vendorPurchaseItem.GstValue = 0.00M;

                //}

                //GST related section
                //vendorPurchaseItem.HsnCode = vendorPurchaseItem.ProductStock.HsnCode;
                //if ( LocationType == 1 )
                //{
                //    vendorPurchaseItem.Igst = 0;
                //    vendorPurchaseItem.Cgst = vendorPurchaseItem.ProductStock.Cgst == null ? 0 : vendorPurchaseItem.ProductStock.Cgst;
                //    vendorPurchaseItem.Sgst = vendorPurchaseItem.ProductStock.Sgst == null ? 0 : vendorPurchaseItem.ProductStock.Sgst;
                //    vendorPurchaseItem.GstTotal = vendorPurchaseItem.ProductStock.GstTotal == null ? 0 : vendorPurchaseItem.ProductStock.GstTotal;
                //}
                //else if (LocationType == 2)
                //{
                //    vendorPurchaseItem.Igst = vendorPurchaseItem.ProductStock.Igst == null ? 0 : vendorPurchaseItem.ProductStock.Igst;
                //    vendorPurchaseItem.Cgst = 0;
                //    vendorPurchaseItem.Sgst = 0;
                //    vendorPurchaseItem.GstTotal = vendorPurchaseItem.ProductStock.GstTotal == null ? 0 : vendorPurchaseItem.ProductStock.GstTotal;
                //}
                //else
                //{
                //    vendorPurchaseItem.Igst = 0;
                //    vendorPurchaseItem.Cgst = 0;
                //    vendorPurchaseItem.Sgst = 0;
                //    vendorPurchaseItem.GstTotal = 0;
                //}

                //if (vendorPurchaseItem.Discount == 0)
                //{
                //    vendorPurchaseItem.Discount = 0.0M;
                //}
                //else
                //{
                //    // vendorPurchaseItem.DiscountValue = (vendorPurchaseItem.PurchasePrice - vendorPurchaseItem.VatValue) * (vendorPurchaseItem.Discount / 100); // Commented by Gavaskar 11-09-2017
                //    // Added by Gavaskar
                //    vendorPurchaseItem.DiscountValue = discount;
                //}
                //Direct Value Calculations By Sabarish End

                vendorPurchaseItem.VatValue = vendorPurchaseItem.VATValue;
                //added by nandhini for transaction
                if (vendorPurchaseItem.WriteExecutionQuery)
                {
                    SetInsertMetaData(vendorPurchaseItem, VendorPurchaseItemTable.Table);
                    WriteInsertExecutionQuery(vendorPurchaseItem, VendorPurchaseItemTable.Table);
                }
                else
                {
                    await Insert(vendorPurchaseItem, VendorPurchaseItemTable.Table);
                }
            }
            return itemList;
        }
        public async Task<IEnumerable<DraftVendorPurchaseItem>> SaveDraftVendorPurchaseItem(IEnumerable<DraftVendorPurchaseItem> itemList)
        {
            try
            {
                foreach (var draftvendorPurchaseItem in itemList)
                {
                    draftvendorPurchaseItem.ProductName = draftvendorPurchaseItem.ProductStock.Product.Name;
                    draftvendorPurchaseItem.BatchNo = draftvendorPurchaseItem.ProductStock.BatchNo;
                    draftvendorPurchaseItem.ExpireDate = draftvendorPurchaseItem.ProductStock.ExpireDate;
                    draftvendorPurchaseItem.TaxType = draftvendorPurchaseItem.ProductStock.TaxType;
                    draftvendorPurchaseItem.VAT = draftvendorPurchaseItem.ProductStock.VAT;
                    draftvendorPurchaseItem.CST = draftvendorPurchaseItem.ProductStock.CST;
                    draftvendorPurchaseItem.ProductId = draftvendorPurchaseItem.ProductStock.ProductId;
                    await Insert(draftvendorPurchaseItem, DraftVendorPurchaseItemTable.Table);
                }
                return itemList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<DcVendorPurchaseItem> SaveDCItems(DcVendorPurchaseItem vendorPurchaseItem)
        {
            //Strips Logic 
            vendorPurchaseItem.Quantity = vendorPurchaseItem.PackageSize * vendorPurchaseItem.PackageQty;
            decimal? freeQty = 0;
            if (vendorPurchaseItem.FreeQty > 0)
            {
                freeQty = vendorPurchaseItem.FreeQty * vendorPurchaseItem.PackageSize;
            }
            decimal toatlPrice = (decimal)(vendorPurchaseItem.PackagePurchasePrice * vendorPurchaseItem.PackageQty);
            //decimal toatlPrice = (decimal)(vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty));
            decimal discount = (decimal)(toatlPrice * vendorPurchaseItem.Discount / 100);
            decimal priceAfterTax = 0;
            if (vendorPurchaseItem.ProductStock.VAT == null)
                vendorPurchaseItem.ProductStock.VAT = 0;

            //GST fields update starts here
            if (vendorPurchaseItem.ProductStock.HsnCode == null)
                vendorPurchaseItem.ProductStock.HsnCode = "";
            if (vendorPurchaseItem.ProductStock.Igst == null)
                vendorPurchaseItem.ProductStock.Igst = 0;
            if (vendorPurchaseItem.ProductStock.Cgst == null)
                vendorPurchaseItem.ProductStock.Cgst = 0;
            if (vendorPurchaseItem.ProductStock.Sgst == null)
                vendorPurchaseItem.ProductStock.Sgst = 0;
            if (vendorPurchaseItem.ProductStock.GstTotal == null)
                vendorPurchaseItem.ProductStock.GstTotal = 0;
            //GST fields update ends here 
            //GST % included in price calculation by Poongodi on 27/07/2017
            if (vendorPurchaseItem.TaxRefNo == 1)
            {
                priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vendorPurchaseItem.ProductStock.GstTotal / 100));

            }
            else
            {
                if (vendorPurchaseItem.ProductStock.CST > 0)
                    priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vendorPurchaseItem.ProductStock.CST / 100));
                else
                    priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vendorPurchaseItem.ProductStock.VAT / 100));
            }
            vendorPurchaseItem.PurchasePrice = (priceAfterTax / (vendorPurchaseItem.PackageQty + vendorPurchaseItem.FreeQty)) / vendorPurchaseItem.PackageSize;
            vendorPurchaseItem.PurchasePrice = Math.Round((decimal)vendorPurchaseItem.PurchasePrice, 3);
            vendorPurchaseItem.ProductStock.SellingPrice = Math.Round(Convert.ToDecimal(vendorPurchaseItem.PackageSellingPrice / vendorPurchaseItem.PackageSize), 2);
            vendorPurchaseItem.ProductStock.MRP = Math.Round(Convert.ToDecimal(vendorPurchaseItem.PackageMRP / vendorPurchaseItem.PackageSize), 2);
            vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize;  //Added by Poongodi on 21/06/2017
            ProductStock productStockItem = new ProductStock();
            var productStockExist = await _productStockDataAccess.GetProductStock(vendorPurchaseItem.ProductStock);
            if (productStockExist.Count == 1)
            {
                foreach (var stockItem in productStockExist)
                {
                    stockItem.Stock += vendorPurchaseItem.Quantity + freeQty;
                    productStockItem = await _productStockDataAccess.Update(stockItem);
                }
            }
            else
            {
                vendorPurchaseItem.ProductStock.Stock = vendorPurchaseItem.Quantity + freeQty;
                vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize;
                vendorPurchaseItem.ProductStock.PackagePurchasePrice = vendorPurchaseItem.PackagePurchasePrice;
                vendorPurchaseItem.ProductStock.PurchasePrice = vendorPurchaseItem.PurchasePrice;
                productStockItem = await _productStockDataAccess.Save(vendorPurchaseItem.ProductStock);
            }
            vendorPurchaseItem.ProductStockId = productStockItem.Id;
            vendorPurchaseItem.isActive = true;
            //added by nandhini for transaction
            if (vendorPurchaseItem.WriteExecutionQuery)
            {
                SetInsertMetaData(vendorPurchaseItem, DCVendorPurchaseItemTable.Table);
                WriteInsertExecutionQuery(vendorPurchaseItem, DCVendorPurchaseItemTable.Table);
            }
            else
            {
                await Insert(vendorPurchaseItem, DCVendorPurchaseItemTable.Table);
            }
            return vendorPurchaseItem;
        }
        public async Task<List<DcVendorPurchaseItem>> GetDCItems(string id, string accId, string instId)
        {
            var dcitem = new List<DcVendorPurchaseItem>();
            var qb = QueryBuilderFactory.GetQueryBuilder(DCVendorPurchaseItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.InstanceIdColumn, instId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.VendorIdColumn, id);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.isActiveColumn, 1);
            qb.AddColumn(DCVendorPurchaseItemTable.IdColumn, DCVendorPurchaseItemTable.DCDateColumn, DCVendorPurchaseItemTable.DCNoColumn, DCVendorPurchaseItemTable.PackageSizeColumn,
                DCVendorPurchaseItemTable.PackageQtyColumn, DCVendorPurchaseItemTable.PackagePurchasePriceColumn, DCVendorPurchaseItemTable.PackageSellingPriceColumn, DCVendorPurchaseItemTable.PackageMRPColumn,
                DCVendorPurchaseItemTable.FreeQtyColumn, DCVendorPurchaseItemTable.DiscountColumn, DCVendorPurchaseItemTable.QuantityColumn, DCVendorPurchaseItemTable.isPurchasedColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, DCVendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.IdColumn, ProductStockTable.ProductIdColumn, ProductStockTable.BatchNoColumn,
                ProductStockTable.ExpireDateColumn, ProductStockTable.VATColumn, ProductStockTable.HsnCodeColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.GstTotalColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.MRPColumn, ProductStockTable.StockColumn, ProductStockTable.CSTColumn,
                ProductStockTable.InstanceIdColumn, ProductStockTable.AccountIdColumn, ProductStockTable.PurchasePriceColumn, ProductStockTable.HsnCodeColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.IdColumn);
            /*Product Instance sectionadded by POongodi on21/03/2017*/
            qb.JoinBuilder.LeftJoins(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductStockTable.ProductIdColumn, ProductInstanceTable.InstanceIdColumn, ProductStockTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductInstanceTable.Table, ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item1 = new DcVendorPurchaseItem();
                    item1.Id = reader[DCVendorPurchaseItemTable.IdColumn.ColumnName].ToString();
                    item1.DCDate = reader[DCVendorPurchaseItemTable.DCDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue;
                    item1.DCNo = reader[DCVendorPurchaseItemTable.DCNoColumn.ColumnName] as string ?? "";
                    item1.PackageSize = reader[DCVendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0;
                    item1.PackageQty = reader[DCVendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0;
                    item1.PackagePurchasePrice = reader[DCVendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0;
                    item1.PackageSellingPrice = reader[DCVendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0;
                    item1.PackageMRP = reader[DCVendorPurchaseItemTable.PackageMRPColumn.ColumnName] as decimal? ?? 0;
                    item1.FreeQty = reader[DCVendorPurchaseItemTable.FreeQtyColumn.ColumnName] as decimal? ?? 0;
                    item1.Discount = reader[DCVendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0;
                    item1.Quantity = reader[DCVendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0;
                    item1.isPurchased = reader[DCVendorPurchaseItemTable.isPurchasedColumn.ColumnName] as bool? ?? false;
                    item1.ProductStock.Id = reader[ProductStockTable.IdColumn.FullColumnName].ToString();
                    item1.ProductStock.ProductId = reader[ProductStockTable.ProductIdColumn.FullColumnName].ToString();
                    item1.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    item1.ProductStock.ExpireDate = reader[ProductStockTable.ExpireDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue;
                    item1.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;

                    //GST fields start here
                    //item1.ProductStock.HsnCode = reader[ProductStockTable.HsnCodeColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.HsnCode = reader[ProductStockTable.HsnCodeColumn.FullColumnName].ToString();

                    item1.ProductStock.Igst = reader[ProductStockTable.IgstColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.Cgst = reader[ProductStockTable.CgstColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.Sgst = reader[ProductStockTable.SgstColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.GstTotal = reader[ProductStockTable.GstTotalColumn.FullColumnName] as decimal? ?? 0;
                    //GST fields end here

                    item1.ProductStock.SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.MRP = reader[ProductStockTable.MRPColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.Stock = reader[ProductStockTable.StockColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.AccountId = reader[ProductStockTable.AccountIdColumn.FullColumnName].ToString();
                    item1.ProductStock.InstanceId = reader[ProductStockTable.InstanceIdColumn.FullColumnName].ToString();
                    item1.ProductStock.PurchasePrice = reader[ProductStockTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    item1.ProductStock.Product.Id = reader[ProductTable.IdColumn.FullColumnName].ToString();
                    item1.ProductStock.Product.RackNo = reader[ProductInstanceTable.RackNoColumn.FullColumnName].ToString();
                    item1.ProductStock.Product.BoxNo = reader[ProductInstanceTable.BoxNoColumn.FullColumnName].ToString();
                    //item1.ProductStock.HsnCode = reader[ProductStockTable.HsnCodeColumn.FullColumnName].ToString();
                    dcitem.Add(item1);
                }
            }
            dcitem = dcitem.OrderByDescending(a => a.DCDate).OrderBy(a => a.DCNo).ToList();
            return dcitem;
        }
        public async Task<List<DcVendorPurchaseItem>> getDCItemsDropDown(string id, string dcNo, string accId, string instId)
        {
            var dcitem = new List<DcVendorPurchaseItem>();
            var qb = QueryBuilderFactory.GetQueryBuilder(DCVendorPurchaseItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.InstanceIdColumn, instId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.VendorIdColumn, id);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.isActiveColumn, 1);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.DCNoColumn, dcNo);
            qb.AddColumn(DCVendorPurchaseItemTable.IdColumn, DCVendorPurchaseItemTable.DCDateColumn, DCVendorPurchaseItemTable.DCNoColumn, DCVendorPurchaseItemTable.PackageSizeColumn,
                DCVendorPurchaseItemTable.PackageQtyColumn, DCVendorPurchaseItemTable.PackagePurchasePriceColumn, DCVendorPurchaseItemTable.PackageSellingPriceColumn, DCVendorPurchaseItemTable.PackageMRPColumn,
                DCVendorPurchaseItemTable.FreeQtyColumn, DCVendorPurchaseItemTable.DiscountColumn, DCVendorPurchaseItemTable.QuantityColumn, DCVendorPurchaseItemTable.isPurchasedColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, DCVendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.IdColumn, ProductStockTable.ProductIdColumn, ProductStockTable.BatchNoColumn,
                ProductStockTable.ExpireDateColumn, ProductStockTable.VATColumn, ProductStockTable.HsnCodeColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.GstTotalColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.MRPColumn, ProductStockTable.StockColumn, ProductStockTable.CSTColumn,
                ProductStockTable.InstanceIdColumn, ProductStockTable.AccountIdColumn, ProductStockTable.PurchasePriceColumn, ProductStockTable.HsnCodeColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductStockTable.ProductIdColumn, ProductTable.IdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.IdColumn);
            /*Product Instance sectionadded by POongodi on21/03/2017*/
            qb.JoinBuilder.LeftJoins(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductStockTable.ProductIdColumn, ProductInstanceTable.InstanceIdColumn, ProductStockTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductInstanceTable.Table, ProductInstanceTable.RackNoColumn);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var item1 = new DcVendorPurchaseItem();
                    item1.Id = reader[DCVendorPurchaseItemTable.IdColumn.ColumnName].ToString();
                    item1.DCDate = reader[DCVendorPurchaseItemTable.DCDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue;
                    item1.DCNo = reader[DCVendorPurchaseItemTable.DCNoColumn.ColumnName] as string ?? "";
                    item1.PackageSize = reader[DCVendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0;
                    item1.PackageQty = reader[DCVendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0;
                    item1.PackagePurchasePrice = reader[DCVendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0;
                    item1.PackageSellingPrice = reader[DCVendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0;
                    item1.PackageMRP = reader[DCVendorPurchaseItemTable.PackageMRPColumn.ColumnName] as decimal? ?? 0;
                    item1.FreeQty = reader[DCVendorPurchaseItemTable.FreeQtyColumn.ColumnName] as decimal? ?? 0;
                    item1.Discount = reader[DCVendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0;
                    item1.Quantity = reader[DCVendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0;
                    item1.isPurchased = reader[DCVendorPurchaseItemTable.isPurchasedColumn.ColumnName] as bool? ?? false;
                    item1.ProductStock.Id = reader[ProductStockTable.IdColumn.FullColumnName].ToString();
                    item1.ProductStock.ProductId = reader[ProductStockTable.ProductIdColumn.FullColumnName].ToString();
                    item1.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    item1.ProductStock.ExpireDate = reader[ProductStockTable.ExpireDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue;
                    item1.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;

                    //GST fields start here
                    //item1.ProductStock.HsnCode = reader[ProductStockTable.HsnCodeColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.HsnCode = reader[ProductStockTable.HsnCodeColumn.FullColumnName].ToString();

                    item1.ProductStock.Igst = reader[ProductStockTable.IgstColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.Cgst = reader[ProductStockTable.CgstColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.Sgst = reader[ProductStockTable.SgstColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.GstTotal = reader[ProductStockTable.GstTotalColumn.FullColumnName] as decimal? ?? 0;
                    //GST fields end here

                    item1.ProductStock.SellingPrice = reader[ProductStockTable.SellingPriceColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.MRP = reader[ProductStockTable.MRPColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.Stock = reader[ProductStockTable.StockColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.AccountId = reader[ProductStockTable.AccountIdColumn.FullColumnName].ToString();
                    item1.ProductStock.InstanceId = reader[ProductStockTable.InstanceIdColumn.FullColumnName].ToString();
                    item1.ProductStock.PurchasePrice = reader[ProductStockTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0;
                    item1.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    item1.ProductStock.Product.Id = reader[ProductTable.IdColumn.FullColumnName].ToString();
                    item1.ProductStock.Product.RackNo = reader[ProductInstanceTable.RackNoColumn.FullColumnName].ToString();
                    //item1.ProductStock.HsnCode = reader[ProductStockTable.HsnCodeColumn.FullColumnName].ToString();
                    dcitem.Add(item1);
                }
            }
            dcitem = dcitem.OrderByDescending(a => a.DCDate).OrderBy(a => a.DCNo).ToList();
            return dcitem;
        }
        public async Task<IEnumerable<TempVendorPurchaseItem>> SaveTempItems(IEnumerable<TempVendorPurchaseItem> itemList)
        {
            foreach (var vendorPurchaseItem in itemList)
            {
                if (vendorPurchaseItem.IsDeleted == true)
                {
                    await IsDeletedTempItem(vendorPurchaseItem);
                    var productStockExist = await _productStockDataAccess.GetProductStockById(vendorPurchaseItem.ProductStock);
                    if (productStockExist.Count == 1)
                    {
                        foreach (var stockItem in productStockExist)
                        {
                            var quantity = vendorPurchaseItem.PackageSize * vendorPurchaseItem.ProductStock.packageQty;
                            stockItem.Stock -= quantity;
                            await _productStockDataAccess.Update(stockItem);
                        }
                    }
                }
                else
                {
                    //Strips Logic 
                    vendorPurchaseItem.Quantity = vendorPurchaseItem.PackageSize * vendorPurchaseItem.PackageQty;
                    decimal toatlPrice = (decimal)(vendorPurchaseItem.PackagePurchasePrice * (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty));
                    decimal discount = (decimal)(toatlPrice * vendorPurchaseItem.Discount / 100);
                    decimal priceAfterTax = 0;
                    if (vendorPurchaseItem.ProductStock.VAT == null)
                        vendorPurchaseItem.ProductStock.VAT = 0;

                    //GST fields update start here
                    if (vendorPurchaseItem.ProductStock.HsnCode == null)
                        vendorPurchaseItem.ProductStock.HsnCode = "";
                    if (vendorPurchaseItem.ProductStock.Igst == null)
                        vendorPurchaseItem.ProductStock.Igst = 0;
                    if (vendorPurchaseItem.ProductStock.Cgst == null)
                        vendorPurchaseItem.ProductStock.Cgst = 0;
                    if (vendorPurchaseItem.ProductStock.Sgst == null)
                        vendorPurchaseItem.ProductStock.Sgst = 0;
                    if (vendorPurchaseItem.ProductStock.GstTotal == null)
                        vendorPurchaseItem.ProductStock.GstTotal = 0;
                    //GST fields update end here
                    if (vendorPurchaseItem.TaxRefNo == 1)
                        priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vendorPurchaseItem.ProductStock.GstTotal / 100));
                    else if (vendorPurchaseItem.ProductStock.CST > 0)
                        priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vendorPurchaseItem.ProductStock.CST / 100));
                    else
                        priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vendorPurchaseItem.ProductStock.VAT / 100));
                    vendorPurchaseItem.PurchasePrice = (priceAfterTax / vendorPurchaseItem.PackageQty) / vendorPurchaseItem.PackageSize;
                    vendorPurchaseItem.PurchasePrice = Math.Round((decimal)vendorPurchaseItem.PurchasePrice, 3);
                    vendorPurchaseItem.ProductStock.SellingPrice = Math.Round(Convert.ToDecimal(vendorPurchaseItem.PackageSellingPrice / vendorPurchaseItem.PackageSize), 2);
                    vendorPurchaseItem.ProductStock.MRP = Math.Round(Convert.ToDecimal(vendorPurchaseItem.PackageMRP / vendorPurchaseItem.PackageSize), 2);
                    //vendorPurchaseItem.Total += vendorPurchaseItem.PackagePurchasePrice * vendorPurchaseItem.PackageQty;
                    vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize;   //Added by Poongodi on 21/06/2017
                    ProductStock productStockItem = new ProductStock();
                    var productStockExist = await _productStockDataAccess.GetProductStockById(vendorPurchaseItem.ProductStock);
                    if (productStockExist.Count == 1)
                    {
                        foreach (var stockItem in productStockExist)
                        {
                            var quantity = vendorPurchaseItem.PackageSize * vendorPurchaseItem.ProductStock.packageQty;
                            stockItem.Stock -= (quantity - vendorPurchaseItem.Quantity);
                            productStockItem = await _productStockDataAccess.Update(stockItem);
                        }
                    }
                    else
                    {
                        //if (vendorPurchaseItem.ProductStock.PurchasePrice==null)
                        //{ 
                        //    vendorPurchaseItem.ProductStock.PurchasePrice = vendorPurchaseItem.PurchasePrice;
                        //}
                        productStockExist = await _productStockDataAccess.GetProductStock(vendorPurchaseItem.ProductStock);
                        if (productStockExist.Count == 1)
                        {
                            foreach (var stockItem in productStockExist)
                            {
                                stockItem.Stock += vendorPurchaseItem.Quantity;
                                productStockItem = await _productStockDataAccess.Update(stockItem);
                            }
                        }
                        else
                        {
                            vendorPurchaseItem.ProductStock.Stock = vendorPurchaseItem.Quantity;
                            vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize;
                            vendorPurchaseItem.ProductStock.PackagePurchasePrice = vendorPurchaseItem.PackagePurchasePrice;
                            vendorPurchaseItem.ProductStock.PurchasePrice = vendorPurchaseItem.PurchasePrice;
                            productStockItem = await _productStockDataAccess.Save(vendorPurchaseItem.ProductStock);
                        }
                    }
                    vendorPurchaseItem.ProductStockId = productStockItem.Id;
                    vendorPurchaseItem.isActive = true;
                    //Added by Annadurai on 07302017
                    if (string.IsNullOrEmpty(vendorPurchaseItem.Id))
                    {
                        await Insert(vendorPurchaseItem, TempVendorPurchaseItemTable.Table);
                    }
                    else
                    {
                        //await Update(vendorPurchaseItem, TempVendorPurchaseItemTable.Table);
                        //update no of strips only.
                        var vendorPurchaseItemId = vendorPurchaseItem.Id;
                        var qbUpdate = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Update);
                        qbUpdate.ConditionBuilder.And(TempVendorPurchaseItemTable.IdColumn, vendorPurchaseItemId);
                        // Added by Gavaskar 29-08-2017 
                        qbUpdate.ConditionBuilder.And(TempVendorPurchaseItemTable.AccountIdColumn, vendorPurchaseItem.AccountId);
                        qbUpdate.ConditionBuilder.And(TempVendorPurchaseItemTable.InstanceIdColumn, vendorPurchaseItem.InstanceId);
                        if (vendorPurchaseItem.IsItemSaled)
                        {
                            // Added by Gavaskar 28-08-2017 
                            qbUpdate.AddColumn(TempVendorPurchaseItemTable.PackageQtyColumn, TempVendorPurchaseItemTable.QuantityColumn);
                            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.PackageQtyColumn.ColumnName, vendorPurchaseItem.PackageQty);
                            // Added by Gavaskar 28-08-2017 
                            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.QuantityColumn.ColumnName, vendorPurchaseItem.Quantity);
                        }
                        else
                        {
                            qbUpdate.AddColumn(TempVendorPurchaseItemTable.PackageQtyColumn, TempVendorPurchaseItemTable.PackageSizeColumn,
                                TempVendorPurchaseItemTable.PackagePurchasePriceColumn,
                                TempVendorPurchaseItemTable.PackageSellingPriceColumn, TempVendorPurchaseItemTable.PackageMRPColumn,
                                TempVendorPurchaseItemTable.PurchasePriceColumn, TempVendorPurchaseItemTable.QuantityColumn);  // Added by Gavaskar 28-08-2017 
                            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.PackageQtyColumn.ColumnName, vendorPurchaseItem.PackageQty);
                            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.PackageSizeColumn.ColumnName, vendorPurchaseItem.PackageSize);
                            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName, vendorPurchaseItem.PackagePurchasePrice);
                            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName, vendorPurchaseItem.PackageSellingPrice);
                            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.PackageMRPColumn.ColumnName, vendorPurchaseItem.PackageMRP);
                            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.PurchasePriceColumn.ColumnName, vendorPurchaseItem.PurchasePrice);
                            // Added by Gavaskar 28-08-2017 
                            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.QuantityColumn.ColumnName, vendorPurchaseItem.Quantity);
                        }
                        await QueryExecuter.NonQueryAsyc(qbUpdate);
                        if (!vendorPurchaseItem.IsItemSaled)
                        {
                            var qbUnSaledItemUpdate = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
                            qbUnSaledItemUpdate.ConditionBuilder.And(ProductStockTable.IdColumn, vendorPurchaseItem.ProductStockId);
                            qbUnSaledItemUpdate.AddColumn(ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.GstTotalColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.MRPColumn);
                            qbUnSaledItemUpdate.Parameters.Add(ProductStockTable.BatchNoColumn.ColumnName, vendorPurchaseItem.ProductStock.BatchNo);
                            qbUnSaledItemUpdate.Parameters.Add(ProductStockTable.ExpireDateColumn.ColumnName, vendorPurchaseItem.ProductStock.ExpireDate);
                            qbUnSaledItemUpdate.Parameters.Add(ProductStockTable.GstTotalColumn.ColumnName, vendorPurchaseItem.ProductStock.GstTotal);
                            qbUnSaledItemUpdate.Parameters.Add(ProductStockTable.SellingPriceColumn.ColumnName, vendorPurchaseItem.ProductStock.SellingPrice);
                            qbUnSaledItemUpdate.Parameters.Add(ProductStockTable.MRPColumn.ColumnName, vendorPurchaseItem.ProductStock.MRP);
                            await QueryExecuter.NonQueryAsyc(qbUnSaledItemUpdate);
                        }
                    }
                }

            }
            return itemList;
        }
        private async Task<TempVendorPurchaseItem> createTempItemBalance(VendorPurchaseItem vendorPurchaseItem)
        {
            var vendorPurchaseItemId = vendorPurchaseItem.fromTempId;
            var oldTempItem = await getTempItem(vendorPurchaseItem, vendorPurchaseItemId);
            var newTempItem = await getTempItem(vendorPurchaseItem, vendorPurchaseItemId);
            newTempItem.PackageQty -= vendorPurchaseItem.PackageQty;
            newTempItem.Quantity -= vendorPurchaseItem.Quantity;
            await inActiveTempItem(vendorPurchaseItem);
            //return await Insert(newTempItem, TempVendorPurchaseItemTable.Table);
            if (newTempItem.Quantity > 0) //Added by Settu on 27/10/2017
            {
                //added by nandhini for transaction
                newTempItem.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);
                if (newTempItem.WriteExecutionQuery)
                {
                    SetInsertMetaData(newTempItem, TempVendorPurchaseItemTable.Table);
                    WriteInsertExecutionQuery(newTempItem, TempVendorPurchaseItemTable.Table);
                }
                else
                {
                    await Insert(newTempItem, TempVendorPurchaseItemTable.Table);
                }
            }
            return oldTempItem;
        }
        private async Task<TempVendorPurchaseItem> getTempItem(VendorPurchaseItem vendorPurchaseItem, string itemId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Select);
            //qb.AddColumn(VendorPurchaseItemTable.QuantityColumn);
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.IdColumn, itemId);
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.AccountIdColumn, vendorPurchaseItem.AccountId);
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.InstanceIdColumn, vendorPurchaseItem.InstanceId);
            var existingTempItem = await List<TempVendorPurchaseItem>(qb);
            return existingTempItem.First();
        }
        private async Task<int> getTempItemQty(VendorPurchaseItem vendorPurchaseItem)
        {
            var vendorPurchaseItemId = vendorPurchaseItem.fromTempId;
            var qb = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Select);
            qb.AddColumn(TempVendorPurchaseItemTable.QuantityColumn);
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.IdColumn, vendorPurchaseItemId);
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.AccountIdColumn, vendorPurchaseItem.AccountId);
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.InstanceIdColumn, vendorPurchaseItem.InstanceId);
            //(await List<VendorPurchaseItem>(qb)).FirstOrDefault();
            var tempQty = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
            return tempQty;
        }
        private async Task inActiveTempItem(VendorPurchaseItem vendorPurchaseItem)
        {
            var vendorPurchaseItemId = vendorPurchaseItem.fromTempId;
            var qbUpdate = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Update);
            qbUpdate.ConditionBuilder.And(TempVendorPurchaseItemTable.IdColumn, vendorPurchaseItemId);
            qbUpdate.AddColumn(TempVendorPurchaseItemTable.isActiveColumn);
            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.isActiveColumn.ColumnName, 0);
            //added by nandhini for transaction
            if (vendorPurchaseItem.WriteExecutionQuery)
            {
                vendorPurchaseItem.AddExecutionQuery(qbUpdate);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qbUpdate);
            }

        }
        private async Task UpdateTempItem(VendorPurchaseItem vendorPurchaseItem)
        {
            var qbUpdate = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Update);
            qbUpdate.ConditionBuilder.And(TempVendorPurchaseItemTable.IdColumn, vendorPurchaseItem.fromTempId);
            qbUpdate.AddColumn(TempVendorPurchaseItemTable.ProductStockIdColumn);
            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.ProductStockIdColumn.ColumnName, vendorPurchaseItem.ProductStockId);
            if (vendorPurchaseItem.WriteExecutionQuery)
            {
                vendorPurchaseItem.AddExecutionQuery(qbUpdate);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qbUpdate);
            }
        }
        private async Task IsDeletedTempItem(TempVendorPurchaseItem vendorPurchaseItem)
        {
            var vendorPurchaseItemId = vendorPurchaseItem.Id;
            var qbUpdate = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Update);
            qbUpdate.ConditionBuilder.And(TempVendorPurchaseItemTable.IdColumn, vendorPurchaseItemId);
            qbUpdate.AddColumn(TempVendorPurchaseItemTable.isActiveColumn, TempVendorPurchaseItemTable.IsDeletedColumn);
            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.isActiveColumn.ColumnName, 0);
            qbUpdate.Parameters.Add(TempVendorPurchaseItemTable.IsDeletedColumn.ColumnName, vendorPurchaseItem.IsDeleted);
            await QueryExecuter.NonQueryAsyc(qbUpdate);
        }
        private async Task<int> getDcItemQty(VendorPurchaseItem vendorPurchaseItem)
        {
            var vendorPurchaseItemId = vendorPurchaseItem.fromDcId;
            var qb = QueryBuilderFactory.GetQueryBuilder(DCVendorPurchaseItemTable.Table, OperationType.Select);
            qb.AddColumn(DCVendorPurchaseItemTable.QuantityColumn);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.IdColumn, vendorPurchaseItemId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.AccountIdColumn, vendorPurchaseItem.AccountId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.InstanceIdColumn, vendorPurchaseItem.InstanceId);
            //(await List<VendorPurchaseItem>(qb)).FirstOrDefault();
            var dcQty = Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
            return dcQty;
        }
        private async Task inActiveDcItem(VendorPurchaseItem vendorPurchaseItem)
        {
            var vendorPurchaseItemId = vendorPurchaseItem.fromDcId;
            var qbUpdate = QueryBuilderFactory.GetQueryBuilder(DCVendorPurchaseItemTable.Table, OperationType.Update);
            qbUpdate.ConditionBuilder.And(DCVendorPurchaseItemTable.IdColumn, vendorPurchaseItemId);
            qbUpdate.AddColumn(DCVendorPurchaseItemTable.isActiveColumn, DCVendorPurchaseItemTable.isPurchasedColumn);
            qbUpdate.Parameters.Add(DCVendorPurchaseItemTable.isActiveColumn.ColumnName, 0);
            qbUpdate.Parameters.Add(DCVendorPurchaseItemTable.isPurchasedColumn.ColumnName, 1);
            //added by nandhini for transaction
            if (vendorPurchaseItem.WriteExecutionQuery)
            {
                vendorPurchaseItem.AddExecutionQuery(qbUpdate);
            }
            else
            {
                await QueryExecuter.NonQueryAsyc(qbUpdate);
            }

        }
        private async Task<DcVendorPurchaseItem> createDcItemBalance(VendorPurchaseItem vendorPurchaseItem)
        {
            var vendorPurchaseItemId = vendorPurchaseItem.fromDcId;
            var oldDcItem = await getDcItem(vendorPurchaseItem, vendorPurchaseItemId);
            var newDcItem = await getDcItem(vendorPurchaseItem, vendorPurchaseItemId);
            decimal? freeQty = vendorPurchaseItem.FreeQty * vendorPurchaseItem.PackageSize;
            newDcItem.PackageQty -= (vendorPurchaseItem.PackageQty - vendorPurchaseItem.FreeQty);
            newDcItem.Quantity -= (vendorPurchaseItem.Quantity - freeQty);

            //Added by Sarubala on 28-04-2018 to avoid free quantity issue
            var tempFreeQty = oldDcItem.FreeQty - vendorPurchaseItem.FreeQty;
            if (tempFreeQty > 0)
            {
                newDcItem.PackageQty = newDcItem.PackageQty + tempFreeQty;
                newDcItem.Quantity = newDcItem.Quantity + (newDcItem.PackageQty * newDcItem.PackageSize);
            }

            newDcItem.FreeQty = 0;
            newDcItem.isPurchased = true;
            await inActiveDcItem(vendorPurchaseItem);
            //added by nandhini for transaction
            newDcItem.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);
            if (newDcItem.WriteExecutionQuery)
            {
                SetInsertMetaData(newDcItem, DCVendorPurchaseItemTable.Table);
                WriteInsertExecutionQuery(newDcItem, DCVendorPurchaseItemTable.Table);
            }
            else
            {
                await Insert(newDcItem, DCVendorPurchaseItemTable.Table);
            }
            return oldDcItem;
        }
        private async Task<DcVendorPurchaseItem> getDcItem(VendorPurchaseItem vendorPurchaseItem, string itemId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DCVendorPurchaseItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.IdColumn, itemId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.AccountIdColumn, vendorPurchaseItem.AccountId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.InstanceIdColumn, vendorPurchaseItem.InstanceId);
            var existingDcItem = await List<DcVendorPurchaseItem>(qb);
            return existingDcItem.First();
        }
        public async Task<string> RemoveDcItem(DcVendorPurchaseItem item1)
        {
            //await _productStockDataAccess.DeleteRecords(item1.ProductStock, ProductStockTable.Table, ProductStockTable.IdColumn, item1.ProductStock.Id);

            var productStockExist = (await _productStockDataAccess.GetProductStockById(item1.ProductStock)).First();
            var quantity = (item1.PackageSize * item1.PackageQty) + (item1.PackageSize * item1.FreeQty);
            productStockExist.Stock -= quantity;
            await _productStockDataAccess.Update(productStockExist);

            var qbUpdate = QueryBuilderFactory.GetQueryBuilder(DCVendorPurchaseItemTable.Table, OperationType.Update);
            qbUpdate.ConditionBuilder.And(DCVendorPurchaseItemTable.IdColumn, item1.Id);
            qbUpdate.AddColumn(DCVendorPurchaseItemTable.isActiveColumn);
            qbUpdate.Parameters.Add(DCVendorPurchaseItemTable.isActiveColumn.ColumnName, 0);
            await QueryExecuter.NonQueryAsyc(qbUpdate);
            return item1.Id;
        }

        public async Task<DcVendorPurchaseItem> UpdateDcItem(DcVendorPurchaseItem vendorPurchaseItem)
        {
            vendorPurchaseItem.Quantity = vendorPurchaseItem.PackageSize * vendorPurchaseItem.PackageQty;
            var qb = QueryBuilderFactory.GetQueryBuilder(DCVendorPurchaseItemTable.Table, OperationType.Update);
            var oldDcItem = await getDcItem(vendorPurchaseItem);
            if ((oldDcItem.Quantity - vendorPurchaseItem.ProductStock.Stock) > vendorPurchaseItem.Quantity)
            {
                return null;
            }
            ProductStock productStockItem = new ProductStock();
            vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize;  //Added by Poongodi on 21/06/2017
            var stockItem = vendorPurchaseItem.ProductStock;
            stockItem.Stock += vendorPurchaseItem.Quantity - oldDcItem.Quantity;

            productStockItem = await _productStockDataAccess.Update(stockItem);
            //added by nandhini on 23.12.17
            await _productStockDataAccess.UpdateGst(stockItem);

            /*  var productStockExist = await _productStockDataAccess.GetProductStock(vendorPurchaseItem.ProductStock);
              if (productStockExist.Count == 1)
              {
                  foreach (var stockItem in productStockExist)
                  {
                      stockItem.Stock += vendorPurchaseItem.Quantity - oldDcItem.Quantity;
                      productStockItem = await _productStockDataAccess.Update(stockItem);
                  }
              }*/
            qb.AddColumn(DCVendorPurchaseItemTable.PackageQtyColumn, DCVendorPurchaseItemTable.QuantityColumn, DCVendorPurchaseItemTable.UpdatedAtColumn, DCVendorPurchaseItemTable.DCNoColumn);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.IdColumn, vendorPurchaseItem.Id);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.AccountIdColumn, vendorPurchaseItem.AccountId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.InstanceIdColumn, vendorPurchaseItem.InstanceId);
            qb.Parameters.Add(DCVendorPurchaseItemTable.PackageQtyColumn.ColumnName, vendorPurchaseItem.PackageQty);
            qb.Parameters.Add(DCVendorPurchaseItemTable.QuantityColumn.ColumnName, vendorPurchaseItem.Quantity);
            qb.Parameters.Add(DCVendorPurchaseItemTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(DCVendorPurchaseItemTable.DCNoColumn.ColumnName, vendorPurchaseItem.DCNo);
            await QueryExecuter.NonQueryAsyc(qb);
            return vendorPurchaseItem;
        }
        private async Task<DcVendorPurchaseItem> getDcItem(DcVendorPurchaseItem item)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DCVendorPurchaseItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.IdColumn, item.Id);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.AccountIdColumn, item.AccountId);
            qb.ConditionBuilder.And(DCVendorPurchaseItemTable.InstanceIdColumn, item.InstanceId);
            var existingDcItem = await List<DcVendorPurchaseItem>(qb);
            return existingDcItem.First();
        }
        public async Task<IEnumerable<VendorPurchaseItem>> GetVendorPurchaseItemByStockId(IEnumerable<string> productStockIds)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
            var i = 1;
            foreach (var productStockId in productStockIds)
            {
                var paramName = $"s{i++}";
                var col = new CriteriaColumn(VendorPurchaseItemTable.ProductStockIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(col);
                qb.ConditionBuilder.Or(cc);
                qb.Parameters.Add(paramName, productStockId);
            }
            return await List<VendorPurchaseItem>(qb);
        }
        public async Task<IEnumerable<VendorPurchase>> List_Old(VendorPurchase data, string InstanceId, string AccountId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseTable.Table, OperationType.Select);
            qb.AddColumn(VendorPurchaseTable.IdColumn, VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.InvoiceDateColumn, VendorPurchaseTable.DiscountColumn, VendorPurchaseTable.GoodsRcvNoColumn, VendorPurchaseTable.CommentsColumn, VendorPurchaseTable.GoodsRcvNoColumn, VendorPurchaseTable.FileNameColumn, VendorPurchaseTable.PaymentTypeColumn, VendorPurchaseTable.ChequeNoColumn, VendorPurchaseTable.ChequeDateColumn, VendorPurchaseTable.CreditNoOfDaysColumn, VendorPurchaseTable.CreditColumn, VendorPurchaseTable.CreatedAtColumn, VendorPurchaseTable.NoteAmountColumn, VendorPurchaseTable.NoteTypeColumn);
            qb.JoinBuilder.LeftJoin(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            //qb.JoinBuilder.LeftJoin(VendorReturnTable.Table, VendorPurchaseTable.IdColumn, VendorReturnTable.VendorPurchaseIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.EmailColumn, VendorTable.MobileColumn, VendorTable.EnableCSTColumn);
            //qb.JoinBuilder.AddJoinColumn(VendorReturnTable.Table, VendorReturnTable.VendorPurchaseIdColumn);
            qb = SqlQueryBuilder(data, qb);
            if (!string.IsNullOrEmpty(data.InvoiceNo))
            {
                qb.SelectBuilder.SortBy(VendorPurchaseTable.GoodsRcvNoColumn);
            }
            else if (!string.IsNullOrEmpty(data.GoodsRcvNo))
            {
                qb.SelectBuilder.SortBy(VendorPurchaseTable.GoodsRcvNoColumn);
            }
            else
            {
                qb.SelectBuilder.SortByDesc(VendorPurchaseTable.CreatedAtColumn);
            }
            qb.SelectBuilder.SetPage(data.Page.PageNo, data.Page.PageSize);
            var result = await List<VendorPurchase>(qb);
            var vp = await SqlQueryList(result, InstanceId, AccountId);
            var vpa = await SqlQueryAuditList(result);
            var vpr = await VendorPurchaseReturnList(result);
            var vendorPurchase = result.Select(x =>
            {
                x.VendorPurchaseItem = vp.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);
                x.VendorPurchaseItemAudits = vpa.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);
                x.VendorPurchaseReturns = vpr.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);
                //x.GrossTotal = vp.Sum(i => i.Total);
                //foreach(VendorPurchaseItem item in x.VendorPurchaseItem)
                //{
                //    item.Discount -= x.Discount;
                //}
                return x;
            });
            return vendorPurchase;
        }
        //added by nandhini on 26.09.17
        public async Task<IEnumerable<Vendor>> GetVendorPurchaseListLocal(string vendorName, string accountid, string instanceid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("Name", vendorName);
            parms.Add("AccountId", accountid);
            parms.Add("InstanceId", instanceid);
            var result = await sqldb.ExecuteProcedureAsync<Vendor>("usp_get_Purchasevendor_details", parms);


            return result;
        }

        public async Task<IEnumerable<VendorPurchase>> List(VendorPurchase data, string InstanceId, string AccountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            Dictionary<string, object> parmsPurchaseId = new Dictionary<string, object>();
            Dictionary<string, object> parmsAudit = new Dictionary<string, object>();
            Dictionary<string, object> parmsPurchaseReturnId = new Dictionary<string, object>();
            Dictionary<string, object> parmsIsAlongWithPurchaseReturnId = new Dictionary<string, object>();

            if (!string.IsNullOrEmpty(data.VendorId))
            {
                data.Values = data.VendorId;
            }
            var Pageno = (data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize;
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("PageNo", Pageno);
            parms.Add("PageSize", data.Page.PageSize);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            parms.Add("fromDate", data.Values);
            parms.Add("ToDate", data.SelectValue);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryList", parms);
            if (result.Count() <= 0)
            { return null; }
            var purchase = result.Select(x =>
            {
                var venPur = new VendorPurchase
                {
                    GoodsRcvNo = x.GoodsRcvNo as System.String ?? "",
                    InvoiceNo = x.InvoiceNo as System.String ?? "",
                    InvoiceDate = x.InvoiceDate as DateTime? ?? DateTime.MinValue,
                    VendorId = x.VendorId as System.String ?? "",
                    PaymentType = x.PaymentType as System.String ?? "",
                    Discount = x.Discount as decimal? ?? 0,
                    Id = x.Id,
                    PurchaseCount = x.PurchaseCount,
                    Comments = x.Comments as System.String ?? "",
                    BillSeries = x.BillSeries as String ?? "",
                    // FileName=x.FileName as System.String ?? "",
                    ChequeNo = x.ChequeNo as System.String ?? "",
                    ChequeDate = x.ChequeDate as DateTime? ?? DateTime.MinValue,
                    CreditNoOfDays = x.CreditNoOfDays as int? ?? 0,
                    Credit = x.Credit as decimal? ?? 0,
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    NoteAmount = x.NoteAmount as decimal? ?? 0,
                    NoteType = x.NoteType as System.String ?? "",
                    CancelStatus = x.CancelStatus,
                    TaxRefNo = x.TaxRefNo,
                    NetValue = x.NetValue as decimal? ?? 0,
                    ActualInvoice = (x.BillSeries as System.String ?? "") + (x.GoodsRcvNo as System.String ?? ""),

                    Vendor =
                    {
                        Name=x.VendorName as System.String ?? "",
                        Mobile=x.Mobile as System.String ?? "",
                        Status=Convert.ToInt32(x.status)
                    }
                };
                return venPur;
            });
            var VendorPurchaseId = string.Join(",", result.Select(x => x.Id).ToList());
            parmsPurchaseId.Add("AccountId", AccountId);
            parmsPurchaseId.Add("InstanceId", InstanceId);
            parmsPurchaseId.Add("VendorPurchaseId", VendorPurchaseId);
            parmsPurchaseId.Add("SearchColName", data.Select);
            parmsPurchaseId.Add("SearchOption", data.Select1);
            parmsPurchaseId.Add("SearchValue", data.Values);
            parmsPurchaseId.Add("fromDate", data.Values);
            parmsPurchaseId.Add("ToDate", data.SelectValue);
            var resultPurchaseId = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryItemList", parmsPurchaseId);
            var purchaseItems = resultPurchaseId.Select(x =>
            {
                var vendorPurchaseItem = new VendorPurchaseItem
                {
                    Id = x.Id as System.String ?? "",
                    ProductStockId = x.ProductStockId as System.String ?? "",
                    VendorPurchaseId = x.VendorPurchaseId as System.String ?? "",
                    PackageSize = x.PackageSize as decimal? ?? 0,
                    PackageQty = x.PackageQty as decimal? ?? 0,
                    PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? 0,
                    PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0,
                    PackageMRP = x.PackageMRP as decimal? ?? 0,
                    Quantity = x.Quantity as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    FreeQty = x.FreeQty as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    ProductStock =
                    {
                        Id = x.ProductStockId as System.String ?? "",
                        SellingPrice =x.SellingPrice as decimal? ?? 0,
                        MRP =x.MRP as decimal? ?? 0,
                        VAT =x.VAT as decimal? ?? 0,

                        //GST related fields
                          // HsnCode =x.HsnCode as decimal? ?? 0,
                        Igst =x.Igst as decimal? ?? 0,
                        Cgst =x.Cgst as decimal? ?? 0,
                        Sgst =x.Sgst as decimal? ?? 0,
                        GstTotal =x.GstTotal as decimal? ?? 0,

                        CST= x.CST as decimal? ?? 0,
                        BatchNo= x.BatchNo as System.String ?? "",
                        ExpireDate =x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        TaxType=x.TaxType as System.String ?? "",
                        Product =
                        {
                            Name=x.ProductName as System.String ?? "",
                            Code = x.ProductCode as System.String ?? "",
                            Id = x.ProductId as System.String ?? "",
                        },
                        PurchaseBarcode = x.PurchaseBarcode as System.String ?? "",
                        Stock = x.ProductStock as decimal? ?? 0,
                        ProductId = x.ProductId as System.String ?? "",
                        CreatedAt = x.psCreatedAt as DateTime? ?? DateTime.Now,
                    }
                };
                return vendorPurchaseItem;
            });
            parmsAudit.Add("VendorPurchaseId", VendorPurchaseId);
            string Productid = "";
            if (data.Select == "product")
            {
                Productid = data.Values;

            }
            parmsAudit.Add("Product", Productid);
            var resultAudit = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryAuditList", parmsAudit);
            var purchaseItemsAudit = resultAudit.Select(x =>
            {
                var vendorPurchaseItemAudit = new VendorPurchaseItemAudit
                {
                    ProductStockId = x.ProductStockId as System.String ?? "",
                    VendorPurchaseId = x.VendorPurchaseId as System.String ?? "",
                    PackageSize = x.PackageSize as decimal? ?? 0,
                    PackageQty = x.PackageQty as decimal? ?? 0,
                    PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? 0,
                    PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0,
                    PackageMRP = x.PackageMRP as decimal? ?? 0,
                    Quantity = x.Quantity as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    FreeQty = x.FreeQty as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    UpdatedByUser = x.UpdatedBy as System.String ?? "",
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    Action = x.Action as System.String ?? "",
                    ProductStock =
                    {
                        SellingPrice =x.SellingPrice as decimal? ?? 0,
                        MRP =x.MRP as decimal? ?? 0,
                        VAT =x.VAT as decimal? ?? 0,
                        CST= x.CST as decimal? ?? 0,
                         //GST related fields
                          // HsnCode =x.HsnCode as decimal? ?? 0,
                        Igst =x.Igst as decimal? ?? 0,
                        Cgst =x.Cgst as decimal? ?? 0,
                        Sgst =x.Sgst as decimal? ?? 0,
                        GstTotal =x.GstTotal as decimal? ?? 0,
                        BatchNo= x.BatchNo as System.String ?? "",
                        ExpireDate =x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        Product =
                        {
                            Name=x.ProductName as System.String ?? "",
                        }
                    }
                };
                return vendorPurchaseItemAudit;
            });
            //  var vpr = await VendorPurchaseReturnList(result);
            //// var vpr = await VendorPurchaseReturnListNew(result);
            var resultReturnList = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryReturnList", parmsAudit);
            var purchaseReturn = resultReturnList.Select(x =>
            {
                var vendorPurchaseReturn = new VendorPurchaseReturn
                {
                    ReturnNo = x.ReturnNo as System.String ?? "",
                    ReturnDate = x.ReturnDate,
                    Id = x.Id as System.String ?? "",
                    Reason = x.Reason as System.String ?? "",
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    CreatedBy = x.CreatedBy,
                    VendorPurchaseId = x.VendorPurchaseId,
                    VendorPurchase =
                    {
                         Id = x.VendorPurchaseId as System.String ?? "",
                    }
                };
                return vendorPurchaseReturn;
            });
            var VendorReturnIds = string.Join(",", resultReturnList.Select(x => x.Id).ToList());
            parmsPurchaseReturnId.Add("VendorReturnId", VendorReturnIds);
            parmsPurchaseReturnId.Add("Product", Productid);
            var resultReturnItemList = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryReturnItemList", parmsPurchaseReturnId);
            var purchaseReturnItem = resultReturnItemList.Select(x =>
            {
                var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
                {
                    ProductStockId = x.ProductStockId as System.String ?? "",
                    VendorReturnId = x.VendorReturnId,
                    Quantity = x.Quantity as decimal? ?? 0,
                    ReturnPurchasePrice = x.ReturnPurchasePrice as decimal? ?? 0,
                    Id = x.VendorReturnId,
                    CreatedBy = x.CreatedBy,
                    VendorPurchaseReturn =
                    {
                    Id = x.VendorReturnId as System.String ?? ""
                    },
                    ProductStock =
                    {
                        SellingPrice=x.SellingPrice as decimal? ?? 0,
                        MRP=x.MRP as decimal? ?? 0,
                        VAT=x.VAT as decimal? ?? 0,
                        BatchNo=x.BatchNo as System.String ?? "",
                        CST=x.CST as decimal? ?? 0,
                         //GST related fields
                          // HsnCode =x.HsnCode as decimal? ?? 0,
                        Igst =x.Igst as decimal? ?? 0,
                        Cgst =x.Cgst as decimal? ?? 0,
                        Sgst =x.Sgst as decimal? ?? 0,
                        GstTotal =x.GstTotal as decimal? ?? 0,
                        ExpireDate=x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        Product =
                        {
                            Name=x.ProductName as System.String ?? "",
                        }
                    },
                    VendorPurchaseItem =
                    {
                        PurchasePrice=x.PurchasePrice as decimal? ?? 0,
                        PackageQty=x.PackageQty as decimal? ?? 0,
                        FreeQty=x.FreeQty as decimal? ?? 0,
                        Discount=x.Discount as decimal? ?? 0,
                        PackageSize=x.PackageSize as decimal? ?? 0,
                    },
                };
                return vendorPurchaseReturnItem;
            });
            var vendorReturn = purchaseReturn.Select(x =>
            {
                x.VendorPurchaseReturnItem = purchaseReturnItem.Where(y => y.Id == x.Id).Select(z => z).ToList();
                return x;
            });


            // Added by Gavaskar Purchase With Return Start

            var resultIsAlongWithPurchaseReturnList = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetReturnListAlongPurchase", parmsAudit);
            var purchaseIsAlongWithReturn = resultIsAlongWithPurchaseReturnList.Select(x =>
            {
                var vendorPurchaseIsAlongWithReturn = new VendorPurchaseReturn
                {
                    ReturnNo = x.ReturnNo as System.String ?? "",
                    ReturnDate = x.ReturnDate,
                    Id = x.Id as System.String ?? "",
                    Reason = x.Reason as System.String ?? "",
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    CreatedBy = x.CreatedBy,
                    VendorPurchaseId = x.VendorPurchaseId,
                    VendorPurchase =
                    {
                         Id = x.VendorPurchaseId as System.String ?? "",
                    }
                };
                return vendorPurchaseIsAlongWithReturn;
            });
            var IsAlongWithPurchase = string.Join(",", resultIsAlongWithPurchaseReturnList.Select(x => x.Id).ToList());
            parmsIsAlongWithPurchaseReturnId.Add("VendorReturnId", IsAlongWithPurchase);
            parmsIsAlongWithPurchaseReturnId.Add("Product", Productid);

            var resultVendorReturnItemList = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryVendorReturnItemList", parmsIsAlongWithPurchaseReturnId);
            var purchaseVendorReturnItem = resultVendorReturnItemList.Select(x =>
            {
                var vendorPurchaseVendorReturnItem = new VendorPurchaseReturnItem
                {
                    ProductStockId = x.ProductStockId as System.String ?? "",
                    VendorReturnId = x.VendorReturnId,
                    Quantity = x.Quantity as decimal? ?? 0,
                    ReturnPurchasePrice = x.ReturnPurchasePrice as decimal? ?? 0,
                    ReturnedTotal = x.ReturnedTotal as decimal? ?? 0,
                    Discount = x.VendorReturnItemDiscount as decimal? ?? 0,
                    Total = x.GstAmount,
                    Id = x.VendorReturnId,
                    CreatedBy = x.CreatedBy,
                    IsDeleted = x.IsDeleted,
                    VendorPurchaseReturn =
                    {
                    Id = x.VendorReturnId as System.String ?? ""
                    },
                    ProductStock =
                    {
                        SellingPrice=x.SellingPrice as decimal? ?? 0,
                        MRP=x.MRP as decimal? ?? 0,
                        VAT=x.VAT as decimal? ?? 0,
                        BatchNo=x.BatchNo as System.String ?? "",
                        CST=x.CST as decimal? ?? 0,
                         //GST related fields
                          // HsnCode =x.HsnCode as decimal? ?? 0,
                        Igst =x.Igst as decimal? ?? 0,
                        Cgst =x.Cgst as decimal? ?? 0,
                        Sgst =x.Sgst as decimal? ?? 0,
                        GstTotal =x.GstTotal as decimal? ?? 0,
                        PackageSize =x.PackageSize as decimal? ?? 0,
                        PackagePurchasePrice =x.PackagePurchasePrice as decimal? ?? 0,
                        PurchasePrice =x.PurchasePrice as decimal? ?? 0,
                        ExpireDate=x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        Product =
                        {
                            Name=x.ProductName as System.String ?? "",
                        }
                    },
                    VendorPurchaseItem =
                    {
                        PackageQty=x.PackageQty as decimal? ?? 0,
                        FreeQty=x.FreeQty as decimal? ?? 0,
                        Discount=x.Discount as decimal? ?? 0,
                    },
                };
                return vendorPurchaseVendorReturnItem;
            });
            var vendorReturnList = purchaseIsAlongWithReturn.Select(x =>
            {
                x.VendorPurchaseReturnItem = purchaseVendorReturnItem.Where(y => y.Id == x.Id).Select(z => z).ToList();
                return x;
            });

            // Added by Gavaskar Purchase With Return End

            //var DeletedItems = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryDeletedItems", parmsAudit);
            //var DeletedpurchaseItems = DeletedItems.Select(x =>
            //{
            //    var DeletedvendorPurchaseItem = new VendorPurchaseItem
            //    {
            //        Id = x.Id as System.String ?? "",
            //        AccountId = x.AccountId as System.String ?? "",
            //        InstanceId = x.InstanceId as System.String ?? "",
            //        VendorPurchaseId = x.VendorPurchaseId as System.String ?? "",
            //        ProductStockId = x.ProductStockId as System.String ?? "",
            //        PackageSize = x.PackageSize as decimal? ?? 0,
            //        PackageQty = x.PackageQty as decimal? ?? 0,
            //        PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? 0,
            //        PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0,
            //        PackageMRP = x.PackageMRP as decimal? ?? 0,
            //        Quantity = x.Quantity as decimal? ?? 0,
            //        PurchasePrice = x.PurchasePrice as decimal? ?? 0,
            //        OfflineStatus = x.OfflineStatus as bool? ?? false,
            //        CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
            //        UpdatedAt = x.UpdatedAt as DateTime? ?? DateTime.MinValue,
            //        CreatedBy = x.CreatedBy as System.String ?? "",
            //        UpdatedByUser = x.username as System.String ?? "",
            //        FreeQty = x.FreeQty as decimal? ?? 0,
            //        Discount = x.Discount as decimal? ?? 0,
            //        Status = x.Status as int? ?? 0,
            //        VAT = x.VAT as decimal? ?? 0,
            //        VatValue = x.VatValue as decimal? ?? 0,
            //        DiscountValue = x.DiscountValue as decimal? ?? 0,
            //        fromTempId = x.fromTempId as System.String ?? "",
            //        fromDcId = x.fromDcId as System.String ?? "",
            //        ProductStock =
            //        {
            //            SellingPrice =x.productStockSellingPrice as decimal? ?? 0,
            //            MRP =x.productStockMRP as decimal? ?? 0,
            //            VAT =x.productStockvat as decimal? ?? 0,
            //            CST= x.productStockcst as decimal? ?? 0,
            //            BatchNo= x.productStockbatchno as System.String ?? "",
            //            ExpireDate =x.productStockexpiredate as DateTime? ?? DateTime.MinValue,
            //            TaxType=x.productStocktaxtype as System.String ?? "",
            //            Product =
            //            {
            //                Name=x.productname as System.String ?? "",
            //            }
            //        }
            //    };
            //    return DeletedvendorPurchaseItem;
            //});
            var vendorPurchase = purchase.Select(x =>
            {
                x.VendorPurchaseItem = purchaseItems.Where(y => y.VendorPurchaseId == x.Id).Select(z => { z.TaxRefNo = x.TaxRefNo; return z; });
                x.VendorPurchaseItemAudits = purchaseItemsAudit.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);
                x.VendorPurchaseReturns = vendorReturn.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);
                x.VendorPurchaseReturnsHistory = vendorReturnList.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);
                //x.DeletedVendorPurchaseItem = DeletedpurchaseItems.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);
                // x.VendorPurchaseReturns = vpr.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);            
                return x;
            });
            return vendorPurchase;
        }
        public async Task<IEnumerable<VendorPurchase>> CancelList(VendorPurchase data, string InstanceId, string AccountId)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            var Pageno = (data.Page.PageSize * data.Page.PageNo) - data.Page.PageSize;
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("PageNo", Pageno);
            parms.Add("PageSize", data.Page.PageSize);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            parms.Add("fromDate", data.Values);
            parms.Add("ToDate", data.SelectValue);
            parms.Add("fromCancelDate", data.FromDate);
            parms.Add("ToCancelDate", data.ToDate);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseCancelList", parms);
            if (result.Count() <= 0)
            { return null; }
            var purchase = result.Select(x =>
            {
                var venPur = new VendorPurchase
                {
                    GoodsRcvNo = x.GoodsRcvNo as System.String ?? "",
                    BillSeries = x.BillSeries as System.String ?? "",
                    InvoiceNo = x.InvoiceNo as System.String ?? "",
                    InvoiceDate = x.InvoiceDate as DateTime? ?? DateTime.MinValue,
                    VendorId = x.VendorId as System.String ?? "",
                    PaymentType = x.PaymentType as System.String ?? "",
                    Discount = x.Discount as decimal? ?? 0,
                    Id = x.Id,
                    PurchaseCount = x.PurchaseCount,
                    Comments = x.Comments as System.String ?? "",
                    // FileName=x.FileName as System.String ?? "",
                    ChequeNo = x.ChequeNo as System.String ?? "",
                    ChequeDate = x.ChequeDate as DateTime? ?? DateTime.MinValue,
                    CreditNoOfDays = x.CreditNoOfDays as int? ?? 0,
                    Credit = x.Credit as decimal? ?? 0,
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    UpdatedAt = x.CancelDate as DateTime? ?? DateTime.MinValue,
                    NoteAmount = x.NoteAmount as decimal? ?? 0,
                    NoteType = x.NoteType as System.String ?? "",
                    CancelStatus = x.CancelStatus,
                    TaxRefNo = Convert.ToInt32(x.TaxRefNo),
                    //added by nandhini on 25.12.17
                    NetValue = x.NetValue as decimal? ?? 0,
                    Vendor =
                    {
                        Name=x.VendorName as System.String ?? "",
                        Mobile=x.Mobile as System.String ?? ""
                    }
                };
                return venPur;
            });
            parms = new Dictionary<string, object>();
            var VendorPurchaseId = string.Join(",", result.Select(x => x.Id).ToList());
            parms.Add("AccountId", AccountId);
            parms.Add("InstanceId", InstanceId);
            parms.Add("VendorPurchaseId", VendorPurchaseId);
            parms.Add("SearchColName", data.Select);
            parms.Add("SearchOption", data.Select1);
            parms.Add("SearchValue", data.Values);
            parms.Add("fromDate", data.Values);
            parms.Add("ToDate", data.SelectValue);
            var resultPurchaseId = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryItemList", parms);
            var purchaseItems = resultPurchaseId.Select(x =>
            {
                var vendorPurchaseItem = new VendorPurchaseItem
                {
                    ProductStockId = x.ProductStockId as System.String ?? "",
                    VendorPurchaseId = x.VendorPurchaseId as System.String ?? "",
                    PackageSize = x.PackageSize as decimal? ?? 0,
                    PackageQty = x.PackageQty as decimal? ?? 0,
                    PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? 0,
                    PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0,
                    PackageMRP = x.PackageMRP as decimal? ?? 0,
                    Quantity = x.Quantity as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    FreeQty = x.FreeQty as decimal? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    ProductStock =
                    {
                        SellingPrice =x.SellingPrice as decimal? ?? 0,
                        MRP =x.MRP as decimal? ?? 0,
                        VAT =x.VAT as decimal? ?? 0,
                        CST= x.CST as decimal? ?? 0,
                         //GST related fields
                        Igst =  x.Igst as decimal? ?? 0,
                        Cgst =  x.Cgst as decimal? ?? 0,
                        Sgst =  x.Sgst as decimal? ?? 0,
                        GstTotal =  x.GstTotal as decimal? ?? 0,

                        BatchNo= x.BatchNo as System.String ?? "",
                        ExpireDate =x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        TaxType=x.TaxType as System.String ?? "",
                        Product =
                        {
                            Name=x.ProductName as System.String ?? "",
                        }
                    }
                };
                return vendorPurchaseItem;
            });
            parms = new Dictionary<string, object>();
            parms.Add("VendorPurchaseId", VendorPurchaseId);
            var resultAudit = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryAuditList", parms);
            var purchaseItemsAudit = resultAudit.Select(x =>
            {
                var vendorPurchaseItemAudit = new VendorPurchaseItemAudit
                {
                    ProductStockId = x.ProductStockId as System.String ?? "",
                    VendorPurchaseId = x.VendorPurchaseId as System.String ?? "",
                    PackageSize = x.PackageSize as decimal? ?? 0,
                    PackageQty = x.PackageQty as decimal? ?? 0,
                    PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? 0,
                    PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0,
                    PackageMRP = x.PackageMRP as decimal? ?? 0,
                    Quantity = x.Quantity as decimal? ?? 0,
                    PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                    FreeQty = x.FreeQty as int? ?? 0,
                    Discount = x.Discount as decimal? ?? 0,
                    UpdatedByUser = x.UpdatedBy as System.String ?? "",
                    CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
                    Action = x.Action as System.String ?? "",
                    ProductStock =
                    {
                        SellingPrice =x.SellingPrice as decimal? ?? 0,
                        MRP =x.MRP as decimal? ?? 0,
                        VAT =x.VAT as decimal? ?? 0,
                        CST= x.CST as decimal? ?? 0,
                         //GST related fields
                        Igst =  x.Igst as decimal? ?? 0,
                        Cgst =  x.Cgst as decimal? ?? 0,
                        Sgst =  x.Sgst as decimal? ?? 0,
                        GstTotal =  x.GstTotal as decimal? ?? 0,
                        BatchNo= x.BatchNo as System.String ?? "",
                        ExpireDate =x.ExpireDate as DateTime? ?? DateTime.MinValue,
                        Product =
                        {
                            Name=x.ProductName as System.String ?? "",
                        }
                    }
                };
                return vendorPurchaseItemAudit;
            });
            //  var vpr = await VendorPurchaseReturnList(result);
            //// var vpr = await VendorPurchaseReturnListNew(result);
            var vendorPurchase = purchase.Select(x =>
            {
                x.VendorPurchaseItem = purchaseItems.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);
                x.VendorPurchaseItemAudits = purchaseItemsAudit.Where(y => y.VendorPurchaseId == x.Id).Select(z => z);
                return x;
            });
            return vendorPurchase;
        }
        public async Task<List<DraftVendorPurchase>> GetAllDrafts(string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DraftVendorPurchaseTable.Table, OperationType.Select);
            qb.AddColumn(DraftVendorPurchaseTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(DraftVendorPurchaseTable.InstanceIdColumn, InstanceId);
            qb.SelectBuilder.SortByDesc(DraftVendorPurchaseTable.DraftAddedOnColumn);
            var result = await List<DraftVendorPurchase>(qb);
            foreach (var item in result)
            {
                item.DraftVendorPurchaseItem = await GetDraftVendorPurchaseItem(item.Id);
            }
            return result;
        }
        public async Task<List<DraftVendorPurchase>> GetDraftsByInstance(string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DraftVendorPurchaseTable.Table, OperationType.Select);
            qb.AddColumn(DraftVendorPurchaseTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(DraftVendorPurchaseTable.InstanceIdColumn, InstanceId);
            qb.SelectBuilder.SortByDesc(DraftVendorPurchaseTable.DraftAddedOnColumn);
            return await List<DraftVendorPurchase>(qb);
        }

        public async Task<Product> GetProductGST(string AccountId, string InstanceId, string ProductId)
        {
            var query = "Select P.HsnCode, P.Igst, P.Cgst, P.Sgst, P.GstTotal,P.PackageSize from Product P where P.accountId = @AccountId and P.Id = @Id";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            qb.Parameters.Add(ProductTable.AccountIdColumn.ColumnName, AccountId);
            qb.Parameters.Add(ProductTable.IdColumn.ColumnName, ProductId);

            var products = await List<Product>(qb);

            return products.FirstOrDefault();
        }

        public async Task<List<DraftVendorPurchaseItem>> DraftItemsByDraftId(string InstanceId, string draftid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DraftVendorPurchaseItemTable.Table, OperationType.Select);
            qb.AddColumn(DraftVendorPurchaseItemTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(DraftVendorPurchaseItemTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(DraftVendorPurchaseItemTable.DraftVendorPurchaseIdColumn, draftid);
            qb.SelectBuilder.SortByDesc(DraftVendorPurchaseItemTable.CreatedAtColumn);
            var result = await List<DraftVendorPurchaseItem>(qb);
            foreach (var item in result)
            {
                item.ProductStock.Product = await _productDataAccess.GetProductById(item.ProductId);
                item.ProductStock.BatchNo = item.BatchNo;
                item.ProductStock.ExpireDate = item.ExpireDate;
                item.ProductStock.TaxType = item.TaxType;
                item.ProductStock.VAT = item.VAT;
                item.ProductStock.CST = item.CST;
                item.ProductStock.ProductId = item.ProductId;
            }
            return result;
        }
        public async Task<List<DraftVendorPurchaseItem>> GetDraftVendorPurchaseItem(string draftvendorPurchaseid)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DraftVendorPurchaseItemTable.Table, OperationType.Select);
            qb.AddColumn(DraftVendorPurchaseItemTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(DraftVendorPurchaseItemTable.DraftVendorPurchaseIdColumn, draftvendorPurchaseid);
            qb.SelectBuilder.SortBy(DraftVendorPurchaseItemTable.CreatedAtColumn);
            var result = await List<DraftVendorPurchaseItem>(qb);
            foreach (var item in result)
            {
                // Product product = new Product { Name = item.ProductName, AccountId = item.AccountId, Id = item.ProductId, InstanceId = item.InstanceId };
                //item.ProductStock.Product = await GetProductDetails(product, item);
                item.ProductStock.Product = await _productDataAccess.GetProductById(item.ProductId);
                item.ProductStock.BatchNo = item.BatchNo;
                item.ProductStock.ExpireDate = item.ExpireDate;
                item.ProductStock.TaxType = item.TaxType;
                item.ProductStock.VAT = item.VAT;
                item.ProductStock.CST = item.CST;
                item.ProductStock.ProductId = item.ProductId;
            }
            return result;
        }
        //public async Task<Product> GetProductDetails(Product product, DraftVendorPurchaseItem data)
        //{
        //    var checkProductData = await GetProductData(product);
        //    //Item In Product Table
        //    if (checkProductData != null)
        //    {
        //        return checkProductData;
        //    }
        //    //No Item In Product Table
        //    else
        //    {
        //        var productMasterData = await GetMasterProductData(product);
        //        int NoOfRows = await _productDataAccess.TotalProductCount(product);
        //        var getChar = productMasterData.Name.Substring(0, 2);
        //        string Code = (getChar.ToUpper() + (NoOfRows + 1)).ToString();
        //        Product tempProduct = new Product()
        //        {
        //            ProductMasterID = productMasterData.Id,
        //            AccountId = product.AccountId,
        //            InstanceId = product.InstanceId,
        //            Code = Code,
        //            Name = productMasterData.Name,
        //            Manufacturer = productMasterData.Manufacturer,
        //            KindName = productMasterData.KindName,
        //            StrengthName = productMasterData.StrengthName,
        //            Type = productMasterData.Type,
        //            Category = productMasterData.Category,
        //            GenericName = productMasterData.GenericName,
        //            CommodityCode = productMasterData.CommodityCode,
        //            Packing = productMasterData.Packing,
        //            CreatedBy = productMasterData.CreatedBy,
        //            UpdatedBy = productMasterData.UpdatedBy,
        //            PackageSize = productMasterData.PackageSize,
        //            VAT = productMasterData.VAT,
        //            Price = productMasterData.Price,
        //            Status = productMasterData.Status,
        //            RackNo = productMasterData.RackNo
        //        };
        //        var productdata = await Insert(tempProduct, ProductTable.Table);
        //        data.ProductId = productdata.Id;
        //        await UpdateProductId(data.ProductId, data.Id);
        //        return productdata;
        //    }
        //}
        public async Task UpdateProductId(string productid, string draftvendorPurchaseItemId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DraftVendorPurchaseItemTable.Table, OperationType.Update);
            qb.AddColumn(DraftVendorPurchaseItemTable.ProductIdColumn);
            qb.Parameters.Add(DraftVendorPurchaseItemTable.ProductIdColumn.ColumnName, productid);
            qb.ConditionBuilder.And(DraftVendorPurchaseItemTable.IdColumn, draftvendorPurchaseItemId);
            await QueryExecuter.NonQueryAsyc(qb);
        }
        //public async Task<Product> GetProductData(Product product)
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
        //    qb.AddColumn(ProductTable.Table.ColumnList.ToArray());
        //    qb.ConditionBuilder.And(ProductTable.IdColumn, product.Id);
        //    var result = await List<Product>(qb);
        //    return result.FirstOrDefault();
        //}
        //public async Task<ProductMaster> GetMasterProductData(Product productdata)
        //{
        //    var qb = QueryBuilderFactory.GetQueryBuilder(ProductMasterTable.Table, OperationType.Select);
        //    qb.AddColumn(ProductMasterTable.Table.ColumnList.ToArray());
        //    qb.ConditionBuilder.And(ProductMasterTable.IdColumn, productdata.Id);
        //    var result = await List<ProductMaster>(qb);
        //    return result.FirstOrDefault();
        //}
        public async Task<List<VendorPurchaseItemAudit>> SqlQueryAuditList(IEnumerable<VendorPurchase> data)
        {
            var viqb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemAuditTable.Table, OperationType.Select);
            viqb.AddColumn(VendorPurchaseItemAuditTable.ProductStockIdColumn, VendorPurchaseItemAuditTable.PackageSizeColumn,
                VendorPurchaseItemAuditTable.PackageQtyColumn, VendorPurchaseItemAuditTable.PackagePurchasePriceColumn,
                VendorPurchaseItemAuditTable.PackageSellingPriceColumn, VendorPurchaseItemAuditTable.VendorPurchaseIdColumn,
                VendorPurchaseItemAuditTable.QuantityColumn, VendorPurchaseItemAuditTable.PurchasePriceColumn, VendorPurchaseItemAuditTable.FreeQtyColumn,
                VendorPurchaseItemAuditTable.DiscountColumn, VendorPurchaseItemAuditTable.CreatedAtColumn, VendorPurchaseItemAuditTable.ActionColumn);
            viqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemAuditTable.ProductStockIdColumn);
            viqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.CSTColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.BatchNoColumn);
            viqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            viqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            viqb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, VendorPurchaseItemAuditTable.UpdatedByColumn);
            viqb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);
            var i = 1;
            foreach (var vendorPurchase in data)
            {
                var paramName = $"s{i++}";
                var col = new CriteriaColumn(VendorPurchaseItemAuditTable.VendorPurchaseIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(col);
                viqb.ConditionBuilder.Or(cc);
                viqb.Parameters.Add(paramName, vendorPurchase.Id);
            }
            viqb.SelectBuilder.SortByDesc(VendorPurchaseItemAuditTable.CreatedAtColumn);
            var list = new List<VendorPurchaseItemAudit>();
            using (var reader = await QueryExecuter.QueryAsyc(viqb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseItemAudit = new VendorPurchaseItemAudit
                    {
                        ProductStockId = reader[VendorPurchaseItemAuditTable.ProductStockIdColumn.ColumnName].ToString(),
                        VendorPurchaseId = reader[VendorPurchaseItemAuditTable.VendorPurchaseIdColumn.ColumnName].ToString(),
                        PackageSize = Convert.ToDecimal(reader[VendorPurchaseItemAuditTable.PackageSizeColumn.ColumnName]),
                        PackageQty = Convert.ToDecimal(reader[VendorPurchaseItemAuditTable.PackageQtyColumn.ColumnName]),
                        PackagePurchasePrice = Convert.ToDecimal(reader[VendorPurchaseItemAuditTable.PackagePurchasePriceColumn.ColumnName]),
                        PackageSellingPrice = Convert.ToDecimal(reader[VendorPurchaseItemAuditTable.PackageSellingPriceColumn.ColumnName]),
                        Quantity = Convert.ToDecimal(reader[VendorPurchaseItemAuditTable.QuantityColumn.ColumnName]),
                        PurchasePrice = Convert.ToDecimal(reader[VendorPurchaseItemAuditTable.PurchasePriceColumn.ColumnName]),
                        FreeQty = Convert.ToInt16(reader[VendorPurchaseItemAuditTable.FreeQtyColumn.ColumnName] is System.DBNull ? 0 : reader[VendorPurchaseItemAuditTable.FreeQtyColumn.ColumnName]),
                        Discount = Convert.ToDecimal((reader[VendorPurchaseItemAuditTable.DiscountColumn.ColumnName] is System.DBNull) ? 0 : reader[VendorPurchaseItemAuditTable.DiscountColumn.ColumnName]),
                        UpdatedByUser = reader[HQueUserTable.NameColumn.FullColumnName].ToString(),
                        CreatedAt = Convert.ToDateTime(reader[VendorPurchaseItemAuditTable.CreatedAtColumn.ColumnName]),
                    };
                    vendorPurchaseItemAudit.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    vendorPurchaseItemAudit.ProductStock.VAT = Convert.ToDecimal(reader[ProductStockTable.VATColumn.FullColumnName]);
                    vendorPurchaseItemAudit.ProductStock.SellingPrice = Convert.ToDecimal(reader[ProductStockTable.SellingPriceColumn.FullColumnName]);
                    vendorPurchaseItemAudit.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    vendorPurchaseItemAudit.Action = reader[VendorPurchaseItemAuditTable.ActionColumn.ColumnName].ToString();
                    vendorPurchaseItemAudit.ProductStock.ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName]);
                    vendorPurchaseItemAudit.ProductStock.CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0;
                    list.Add(vendorPurchaseItemAudit);
                }
            }
            return list;
        }
        public async Task<List<VendorPurchaseItem>> SqlQueryList(IEnumerable<VendorPurchase> data, string InstanceId, string AccountId)
        {
            var viqb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
            viqb.AddColumn(VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.PackageSizeColumn,
                VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.VendorPurchaseIdColumn,
                VendorPurchaseItemTable.QuantityColumn, VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.CreatedAtColumn);
            viqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn);
            viqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.CSTColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.TaxTypeColumn);
            viqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            viqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            viqb.ConditionBuilder.And(VendorPurchaseItemTable.InstanceIdColumn, InstanceId);
            viqb.ConditionBuilder.And(VendorPurchaseItemTable.AccountIdColumn, AccountId);
            viqb.SelectBuilder.SortBy(VendorPurchaseItemTable.CreatedAtColumn);
            var i = 1;
            foreach (var vendorPurchase in data)
            {
                var paramName = $"s{i++}";
                var col = new CriteriaColumn(VendorPurchaseItemTable.VendorPurchaseIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(col);
                viqb.ConditionBuilder.Or(cc);
                viqb.Parameters.Add(paramName, vendorPurchase.Id); // vendorPurchase.Id
            }
            var list = new List<VendorPurchaseItem>();
            using (var reader = await QueryExecuter.QueryAsyc(viqb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseItem = new VendorPurchaseItem
                    {
                        ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        VendorPurchaseId = reader[VendorPurchaseItemTable.VendorPurchaseIdColumn.ColumnName].ToString(),
                        PackageSize = Convert.ToDecimal(reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName]),
                        PackageQty = Convert.ToDecimal(reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName]),
                        PackagePurchasePrice =
                            Convert.ToDecimal(reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName]),
                        PackageSellingPrice =
                            Convert.ToDecimal(reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName]),
                        Quantity = Convert.ToDecimal(reader[VendorPurchaseItemTable.QuantityColumn.ColumnName]),
                        PurchasePrice = Convert.ToDecimal(reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName]),
                        FreeQty = Convert.ToDecimal(reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] is System.DBNull ? 0 : reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName]),
                        CreatedAt = Convert.ToDateTime(reader[VendorPurchaseItemTable.CreatedAtColumn.ColumnName]),
                        Discount = Convert.ToDecimal((reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] is System.DBNull) ? 0 : reader[VendorPurchaseItemTable.DiscountColumn.ColumnName])
                    };
                    vendorPurchaseItem.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName]);
                    vendorPurchaseItem.ProductStock.SellingPrice = Convert.ToDecimal(reader[ProductStockTable.SellingPriceColumn.FullColumnName]);
                    vendorPurchaseItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.TaxType = reader[ProductStockTable.TaxTypeColumn.FullColumnName].ToString();
                    list.Add(vendorPurchaseItem);
                }
            }
            return list;
        }
        public async Task<int> Count(VendorPurchase data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseTable.Table, OperationType.Count);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.EmailColumn, VendorTable.MobileColumn);
            qb = SqlQueryBuilder(data, qb);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        private static QueryBuilderBase SqlQueryBuilder(VendorPurchase data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Vendor.Name))
            {
                qb.ConditionBuilder.And(VendorTable.NameColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorTable.NameColumn.ColumnName, data.Vendor.Name);
            }
            if (!string.IsNullOrEmpty(data.Vendor.Mobile))
            {
                qb.ConditionBuilder.And(VendorTable.MobileColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorTable.MobileColumn.ColumnName, data.Vendor.Mobile);
            }
            if (!string.IsNullOrEmpty(data.Vendor.Email))
            {
                qb.ConditionBuilder.And(VendorTable.EmailColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorTable.EmailColumn.ColumnName, data.Vendor.Email);
            }
            if (!string.IsNullOrEmpty(data.InvoiceNo))
            {
                qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorPurchaseTable.InvoiceNoColumn.ColumnName, data.InvoiceNo);
            }
            if (!string.IsNullOrEmpty(data.GoodsRcvNo))
            {
                qb.ConditionBuilder.And(VendorPurchaseTable.GoodsRcvNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorPurchaseTable.GoodsRcvNoColumn.ColumnName, data.GoodsRcvNo);
            }
            if (data.InvoiceDate.HasValue)
            {
                qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceDateColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.Parameters.Add(VendorPurchaseTable.InvoiceDateColumn.ColumnName, data.InvoiceDate != null ? data.InvoiceDate.Value.ToString("yyyy-MM-dd") : null);
            }
            if (!string.IsNullOrEmpty(data.Select) && !string.IsNullOrEmpty(data.Values))
            {
                PurchaseExtendedQuery.BuildExtendedPurchaseQuery(data, qb);
            }
            if (!string.IsNullOrEmpty(data.AccountId) && !string.IsNullOrEmpty(data.InstanceId))
            {
                qb.ConditionBuilder.And(VendorPurchaseTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(VendorPurchaseTable.InstanceIdColumn, data.InstanceId);
            }
            if (!string.IsNullOrEmpty(data.SearchProductId))
            {
                qb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.VendorPurchaseIdColumn, VendorPurchaseTable.IdColumn);
                qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn);
                qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
                qb.ConditionBuilder.And(ProductTable.IdColumn, data.SearchProductId);
            }
            if (!string.IsNullOrEmpty(data.BatchNo))
            {
                qb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.VendorPurchaseIdColumn, VendorPurchaseTable.IdColumn);
                qb.JoinBuilder.Join(ProductStockTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
                //qb.ConditionBuilder.And(ProductStockTable.BatchNoColumn, CriteriaEquation.Like, CriteriaLike.Begin);
                qb.ConditionBuilder.And(ProductStockTable.BatchNoColumn);
                qb.Parameters.Add(ProductStockTable.BatchNoColumn.ColumnName, data.BatchNo);
            }
            return qb;
        }
        public async Task<List<VendorPurchaseItem>> GetVendorPurchaseItem(string Id)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
            //qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn);
            //qb.ConditionBuilder.And(VendorPurchaseItemTable.VendorPurchaseIdColumn);
            //qb.Parameters.Add(VendorPurchaseItemTable.VendorPurchaseIdColumn.ColumnName, Id);
            //qb.AddColumn(VendorPurchaseItemTable.QuantityColumn, VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn, VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackageSizeColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn);
            //qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.IdColumn, ProductStockTable.ProductIdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.VATColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.StockColumn, ProductStockTable.CSTColumn, ProductStockTable.PackageSizeColumn);
            //return List<VendorPurchaseItem>(qb);
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("VendorPurchaseId", Id);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetVendorPurchaseItem", parms);
            var data = result.Select(item =>
            {
                var VendorPurchaseItem = new VendorPurchaseItem()
                {
                    Quantity = item.Quantity as decimal? ?? 0,
                    ProductStockId = item.ProductStockId as System.String ?? "",
                    PurchasePrice = item.PurchasePrice as decimal? ?? 0,
                    PackageSellingPrice = item.PackageSellingPrice as decimal? ?? 0,
                    //PackagePurchasePrice = item.PackagePurchasePrice as decimal? ?? 0, 
                    PackagePurchasePrice = item.pkgPurPrice as decimal? ?? 0,
                    PackageMRP = item.PackageMRP as decimal? ?? 0,
                    PackageQty = item.PackageQty as decimal? ?? 0,
                    PackageSize = item.PackageSize as decimal? ?? 0,
                    FreeQty = item.FreeQty as decimal? ?? 0,
                    Discount = item.Discount as decimal? ?? 0,
                    DiscountValue = item.DiscountValue as decimal? ?? 0,
                    Igst = item.Igst as decimal? ?? 0,
                    Cgst = item.Cgst as decimal? ?? 0,
                    Sgst = item.Sgst as decimal? ?? 0,
                    GstTotal = item.GstTotal as decimal? ?? 0,
                    ProductStock = {
                        Id =  item.productstockid as System.String ?? "",
                        ProductId  =  item.ProductId as System.String ?? "",
                        BatchNo  =  item.BatchNo as System.String ?? "",
                        ExpireDate  =  item.ExpireDate as DateTime? ?? DateTime.MinValue,
                        VAT = item.VAT as decimal? ?? 0,
                        Igst = item.Igst as decimal? ?? 0,
                        Cgst = item.Cgst as decimal? ?? 0,
                        Sgst = item.Sgst as decimal? ?? 0,
                        GstTotal = item.GstTotal as decimal? ?? 0,
                        SellingPrice  = item.SellingPrice as decimal? ?? 0,
                        MRP = item.MRP as decimal? ?? 0,
                        Stock =  item.Stock as decimal? ?? 0,
                        CST = item.CST as decimal? ?? 0,
                        PackageSize = item.PackageSize as decimal? ?? 0
                    }
                };
                return VendorPurchaseItem;
            }).ToList();
            return data;
        }
        public Task<List<VendorPurchaseReturnItem>> VendorPurchaseReturnItemList(VendorPurchase vendorPurchase)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorReturnItemTable.QuantityColumn, VendorReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.LeftJoin(VendorReturnTable.Table, VendorReturnItemTable.VendorReturnIdColumn, VendorReturnTable.IdColumn);
            qb.ConditionBuilder.And(VendorReturnTable.VendorPurchaseIdColumn, vendorPurchase.Id);
            //qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.IdColumn, ProductStockTable.StockColumn);
            //var query = "";
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            return List<VendorPurchaseReturnItem>(qb);
        }
        public async Task<List<VendorPurchaseReturnItem>> VendorPurchaseReturnItemList1(List<string> getVendorPurchaseId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorReturnItemTable.QuantityColumn, VendorReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.LeftJoin(VendorReturnTable.Table, VendorReturnItemTable.VendorReturnIdColumn, VendorReturnTable.IdColumn);
            var i = 1;
            foreach (var productStockId in getVendorPurchaseId)
            {
                var paramName = $"s{i++}";
                var col = new CriteriaColumn(VendorReturnTable.VendorPurchaseIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(col);
                qb.ConditionBuilder.Or(cc);
                qb.Parameters.Add(paramName, productStockId);
            }
            return await List<VendorPurchaseReturnItem>(qb);
        }
        public async Task<IEnumerable<VendorPurchase>> BindVendorPurchaseItemData(IEnumerable<VendorPurchase> data)
        {
            var list = new List<VendorPurchase>();
            foreach (var item in data)
            {
                var vendorPurchaseItem = await GetVendorPurchaseItem(item.Id);
                foreach (var purchaseItem in vendorPurchaseItem)
                {
                    var product = await _productDataAccess.GetProductById(purchaseItem.ProductStock.ProductId);
                    purchaseItem.ProductStock.Product.Name = product.Name;
                }
                list.Add(item);
            }
            return list;
        }
        public async Task<VendorPurchase> VendorPurchaseDetail(string id)
        {
            /*Prefix added by Poongodi on 15/06/2017*/
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseTable.Table, OperationType.Select);
            qb.AddColumn(VendorPurchaseTable.IdColumn, VendorPurchaseTable.InstanceIdColumn, VendorPurchaseTable.InvoiceNoColumn,
                VendorPurchaseTable.InvoiceDateColumn, VendorPurchaseTable.GoodsRcvNoColumn, VendorPurchaseTable.DiscountColumn,
                VendorPurchaseTable.PrefixColumn, VendorPurchaseTable.CreatedAtColumn, VendorPurchaseTable.BillSeriesColumn, VendorPurchaseTable.TaxRefNoColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.IdColumn);
            qb.ConditionBuilder.And(VendorPurchaseTable.IdColumn, id);
            return (await List<VendorPurchase>(qb)).FirstOrDefault();
        }
        //public async Task<VendorPurchaseReturn> GetNonmovingStocks1(VendorPurchaseReturn vpr)
        //{
        //    var VendorPurchaseReturn = new VendorPurchaseReturn
        //    {
        //    };
        //    var vendorItem=vpr.Vendor.GroupBy(i =>i.Id).Select(y => y.First());
        //    foreach (var item in vendorItem)
        //    {
        //        foreach (var itemList in vpr.Vendor)
        //        {
        //            if (itemList.Id == item.Id)
        //            {
        //                VendorPurchaseReturn.Vendor.Add(item);
        //            }
        //        }
        //    }
        //        return vpr;
        //}
        // Newly Added Gavaskar 26-10-2016 Start 
        public async Task<List<VendorPurchaseItem>> GetExpiremovingStocks(string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId).And(ProductStockTable.IsMovingStockExpireColumn, 1);
            qb.AddColumn(VendorPurchaseItemTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.PackageSizeColumn,
                VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.VendorPurchaseIdColumn,
                VendorPurchaseItemTable.QuantityColumn, VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn);
            qb.JoinBuilder.LeftJoin(VendorPurchaseItemTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.IdColumn, ProductStockTable.InstanceIdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.StockColumn, ProductStockTable.ProductIdColumn, ProductStockTable.VATColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.CSTColumn, ProductStockTable.PurchasePriceColumn, ProductStockTable.GstTotalColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.IgstColumn);
            qb.JoinBuilder.LeftJoin(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.IdColumn);
            qb.JoinBuilder.LeftJoin(VendorTable.Table, VendorTable.IdColumn, ProductStockTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.IdColumn, VendorTable.NameColumn);
            var list = new List<VendorPurchaseItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseItem = new VendorPurchaseItem
                    {
                        Id = reader[VendorPurchaseItemTable.IdColumn.ColumnName].ToString(),
                        ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        VendorPurchaseId = reader[VendorPurchaseItemTable.VendorPurchaseIdColumn.ColumnName].ToString(),
                        PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0,
                        Quantity = reader[VendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        PackagePurchasePrice = reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] as decimal? ?? 0
                    };
                    vendorPurchaseItem.ProductStock.Id = reader[ProductStockTable.IdColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.ProductId = reader[ProductStockTable.ProductIdColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.Stock = Convert.ToDecimal(reader[ProductStockTable.StockColumn.FullColumnName]);
                    vendorPurchaseItem.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName]);
                    vendorPurchaseItem.ProductStock.SellingPrice = Convert.ToDecimal(reader[ProductStockTable.SellingPriceColumn.FullColumnName]);
                    vendorPurchaseItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.PurchasePrice = reader[ProductStockTable.PurchasePriceColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.GstTotal = reader[ProductStockTable.GstTotalColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.Cgst = reader[ProductStockTable.CgstColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.Sgst = reader[ProductStockTable.SgstColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.Igst = reader[ProductStockTable.IgstColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.Vendor.Id = reader[VendorTable.IdColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.Vendor.Name = reader[VendorTable.NameColumn.FullColumnName].ToString();
                    if (vendorPurchaseItem.PackageQty == 0)
                    {
                        vendorPurchaseItem.PackageQty = 1;
                        vendorPurchaseItem.PackagePurchasePrice = vendorPurchaseItem.ProductStock.PurchasePrice * vendorPurchaseItem.ProductStock.Stock;
                        vendorPurchaseItem.PurchasePrice = vendorPurchaseItem.ProductStock.PurchasePrice;
                        vendorPurchaseItem.Quantity = vendorPurchaseItem.ProductStock.Stock;
                    }
                    list.Add(vendorPurchaseItem);
                }
            }
            return list;
        }
        // Newly Added Gavaskar 26-10-2016 End 
        public async Task<List<VendorPurchaseItem>> GetNonmovingStocks(string instanceId)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            //qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId).And(ProductStockTable.IsMovingStockColumn, 1);
            //qb.AddColumn(ProductStockTable.IdColumn, ProductStockTable.InstanceIdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.StockColumn);
            //qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            //qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.IdColumn);
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, instanceId).And(ProductStockTable.IsMovingStockColumn, 1);
            qb.AddColumn(VendorPurchaseItemTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.PackageSizeColumn,
                VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.VendorPurchaseIdColumn,
                VendorPurchaseItemTable.QuantityColumn, VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn);
            qb.JoinBuilder.LeftJoin(VendorPurchaseItemTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.IdColumn, ProductStockTable.InstanceIdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.StockColumn, ProductStockTable.ProductIdColumn, ProductStockTable.VATColumn, ProductStockTable.SellingPriceColumn, ProductStockTable.CSTColumn, ProductStockTable.GstTotalColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.IgstColumn);
            qb.JoinBuilder.LeftJoin(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.IdColumn);
            qb.JoinBuilder.LeftJoin(VendorTable.Table, VendorTable.IdColumn, ProductStockTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.IdColumn, VendorTable.NameColumn);
            //return await List<VendorPurchaseItem>(qb);
            var list = new List<VendorPurchaseItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseItem = new VendorPurchaseItem
                    {
                        Id = reader[VendorPurchaseItemTable.IdColumn.ColumnName].ToString(),
                        ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        VendorPurchaseId = reader[VendorPurchaseItemTable.VendorPurchaseIdColumn.ColumnName].ToString(),
                        PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0,
                        Quantity = reader[VendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        PackagePurchasePrice =
                            reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] as decimal? ?? 0,
                    };
                    vendorPurchaseItem.ProductStock.Id = reader[ProductStockTable.IdColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.ProductId = reader[ProductStockTable.ProductIdColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.Stock = Convert.ToDecimal(reader[ProductStockTable.StockColumn.FullColumnName]);
                    vendorPurchaseItem.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName]);
                    vendorPurchaseItem.ProductStock.SellingPrice = Convert.ToDecimal(reader[ProductStockTable.SellingPriceColumn.FullColumnName]);
                    vendorPurchaseItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.GstTotal = reader[ProductStockTable.GstTotalColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.Cgst = reader[ProductStockTable.CgstColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.Sgst = reader[ProductStockTable.SgstColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.Igst = reader[ProductStockTable.IgstColumn.FullColumnName] as decimal? ?? 0;
                    vendorPurchaseItem.ProductStock.Vendor.Id = reader[VendorTable.IdColumn.FullColumnName].ToString();
                    vendorPurchaseItem.ProductStock.Vendor.Name = reader[VendorTable.NameColumn.FullColumnName].ToString();
                    list.Add(vendorPurchaseItem);
                }
            }
            return list;
        }
        public async Task<bool> IsTempPurchaseItemAvail(string productId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Count);
            //qb.ConditionBuilder.And(TempVendorPurchaseItemTable.VendorPurchaseIdColumn, "temp");
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.isActiveColumn, true);
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, productId);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, TempVendorPurchaseItemTable.ProductStockIdColumn);
            int count = Convert.ToInt32(await SingleValueAsyc(qb));
            return count > 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<List<TempVendorPurchaseItem>> loadTempVendorPurchaseItem(string productId, string instanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Select);
            qb.AddColumn(TempVendorPurchaseItemTable.IdColumn, TempVendorPurchaseItemTable.PackageSizeColumn, TempVendorPurchaseItemTable.PackageQtyColumn, TempVendorPurchaseItemTable.PackagePurchasePriceColumn, TempVendorPurchaseItemTable.PackageSellingPriceColumn, TempVendorPurchaseItemTable.PackageMRPColumn, TempVendorPurchaseItemTable.CreatedAtColumn, TempVendorPurchaseItemTable.QuantityColumn);
            //qb.ConditionBuilder.And(TempVendorPurchaseItemTable.VendorPurchaseIdColumn, "temp");
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.isActiveColumn, true);
            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.InstanceIdColumn, instanceId);
            qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, productId);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, TempVendorPurchaseItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.IdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.VATColumn, ProductStockTable.HsnCodeColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.GstTotalColumn, ProductStockTable.StockColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.IdColumn, ProductTable.NameColumn);
            qb.JoinBuilder.LeftJoins(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductStockTable.ProductIdColumn, ProductInstanceTable.InstanceIdColumn, ProductStockTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductInstanceTable.Table, ProductInstanceTable.RackNoColumn);
            //Added by Annadurai for Edit when there is no Sales - Starts
            qb.JoinBuilder.LeftJoin(SalesItemTable.Table, SalesItemTable.ProductStockIdColumn, TempVendorPurchaseItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(SalesItemTable.Table, SalesItemTable.ProductStockIdColumn);
            //Added by Annadurai for Edit when there is no Sales - Ends
            //Added by Gavaskar 31-08-2018 forPurchase partial Edit - Starts
            qb.JoinBuilder.LeftJoin(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, TempVendorPurchaseItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.ProductStockIdColumn);
            //Added by Gavaskar 31-08-2018 forPurchase partial Edit - End
            //qb.SelectBuilder.SortByDesc(ProductStockTable.CreatedAtColumn); // Commented by Gavaskar 31-08-2017
            qb.SelectBuilder.MakeDistinct = true; // Added by Gavaskar 31-08-2017
            //var vendorPurchaseItems = await List<VendorPurchaseItem>(qb);
            var vendorPurchaseItems = new List<TempVendorPurchaseItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var purchaseItem = new TempVendorPurchaseItem
                    {
                        Id = reader[TempVendorPurchaseItemTable.IdColumn.ColumnName].ToString(),
                        PackageSize = Convert.ToInt32(reader[TempVendorPurchaseItemTable.PackageSizeColumn.ColumnName]),
                        PackageQty = Convert.ToInt32(reader[TempVendorPurchaseItemTable.PackageQtyColumn.ColumnName]),
                        PackagePurchasePrice = Convert.ToDecimal(reader[TempVendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName]),
                        PackageSellingPrice = Convert.ToDecimal(reader[TempVendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName]),
                        PackageMRP = Convert.ToDecimal(reader[TempVendorPurchaseItemTable.PackageMRPColumn.ColumnName]),
                        //CreatedAt = Convert.ToDateTime(reader[TempVendorPurchaseItemTable.CreatedAtColumn.ColumnName]),
                        CreatedAt = reader[TempVendorPurchaseItemTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        Quantity = Convert.ToDecimal(reader[TempVendorPurchaseItemTable.QuantityColumn.ColumnName]),
                        //Added by Annadurai
                        IsItemSaled = string.IsNullOrEmpty(reader[SalesItemTable.ProductStockIdColumn.FullColumnName].ToString()) ? false : true,
                        // Added By Gavaskar 31-08-2017 
                        IsItemPurchase = string.IsNullOrEmpty(reader[VendorPurchaseItemTable.ProductStockIdColumn.FullColumnName].ToString()) ? false : true,
                        ProductStock = {
                            Id = reader[ProductStockTable.IdColumn.FullColumnName].ToString(),
                            BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString(),
                           // ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName].ToString()),
                            ExpireDate = reader[ProductStockTable.ExpireDateColumn.FullColumnName] as DateTime? ?? DateTime.MinValue,
                            VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ?? 0,
                            
                            //GST related fields start here
                              HsnCode = reader[ProductStockTable.HsnCodeColumn.FullColumnName].ToString(),
                            Igst = reader[ProductStockTable.IgstColumn.FullColumnName] as decimal? ?? 0,
                            Cgst = reader[ProductStockTable.CgstColumn.FullColumnName] as decimal? ?? 0,
                            Sgst = reader[ProductStockTable.SgstColumn.FullColumnName] as decimal? ?? 0,
                            GstTotal = reader[ProductStockTable.GstTotalColumn.FullColumnName] as decimal? ?? 0,
                            PackageSize = Convert.ToInt32(reader[TempVendorPurchaseItemTable.PackageSizeColumn.ColumnName]),
                            packageQty = Convert.ToInt32(reader[TempVendorPurchaseItemTable.PackageQtyColumn.ColumnName]),
                            Stock = reader[ProductStockTable.StockColumn.FullColumnName] as decimal? ?? 0,
                            //GST related fields end here

                            Product = {
                                Id = reader[ProductTable.IdColumn.FullColumnName].ToString(),
                                Name = reader[ProductTable.NameColumn.FullColumnName].ToString(),
                                RackNo= reader[ProductInstanceTable.RackNoColumn.FullColumnName].ToString(),
                                 // HsnCode= reader[ProductStockTable.HsnCodeColumn.FullColumnName].ToString()

                                // HsnCode = Convert.ToDecimal(reader[ProductStockTable.HsnCodeColumn.FullColumnName].ToString()),
                            }
                        }
                    };
                    vendorPurchaseItems.Add(purchaseItem);
                }
            }
            return vendorPurchaseItems;
        }
        public async Task<VendorPurchase> BindOrderToVendorPurchase(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorOrderItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VendorOrderTable.IdColumn, id);
            qb.AddColumn(VendorOrderItemTable.VendorOrderIdColumn, VendorOrderItemTable.QuantityColumn);
            qb.JoinBuilder.Join(VendorOrderTable.Table, VendorOrderTable.IdColumn, VendorOrderItemTable.VendorOrderIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorOrderTable.Table, VendorOrderTable.VendorIdColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorOrderTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, VendorOrderItemTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.IdColumn);
            var list = new List<VendorPurchaseItem>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var purchaseItem = new VendorPurchaseItem
                    {
                        ProductStock = { Product = { Name = reader[ProductTable.NameColumn.FullColumnName].ToString() },
                                         ProductId = reader[ProductTable.IdColumn.FullColumnName].ToString()},
                        PackageQty = (decimal)reader[VendorOrderItemTable.QuantityColumn.ColumnName],
                    };
                    purchaseItem.VendorPurchase.Vendor.Name = reader[VendorTable.NameColumn.FullColumnName].ToString();
                    purchaseItem.VendorPurchase.VendorId = reader[VendorOrderTable.VendorIdColumn.FullColumnName].ToString();
                    list.Add(purchaseItem);
                }
            }
            var vendorOrder = list.GroupBy(x => x.VendorPurchase.Id).Select(y =>
            {
                var v = new VendorPurchase();
                v = list.First(w => w.VendorPurchase.Id == y.Key).VendorPurchase;
                v.VendorPurchaseItem = y.ToList();
                return v;
            });
            return vendorOrder.FirstOrDefault();
        }
        public async Task<VendorPurchase> GetById(string id)
        {
            /*Prefix Added by Poongodi on 15/06/2017*/
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VendorPurchaseTable.IdColumn);
            qb.Parameters.Add(VendorPurchaseTable.IdColumn.ColumnName, id);
            qb.AddColumn(VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.QuantityColumn, VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn, VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.FreeQtyColumn,
              VendorPurchaseItemTable.CgstColumn, VendorPurchaseItemTable.SgstColumn, VendorPurchaseItemTable.IgstColumn,
              VendorPurchaseItemTable.GstTotalColumn, VendorPurchaseItemTable.GstValueColumn, VendorPurchaseItemTable.PackageSizeColumn, VendorPurchaseItemTable.DiscountValueColumn, VendorPurchaseItemTable.VatValueColumn, VendorPurchaseItemTable.SchemeDiscountPercColumn, VendorPurchaseItemTable.MarkupPercColumn);
            var statusCondition = new CustomCriteria(VendorPurchaseItemTable.StatusColumn, CriteriaCondition.IsNull);
            var cc = new CriteriaColumn(VendorPurchaseItemTable.StatusColumn, "1", CriteriaEquation.Equal);
            var col = new CustomCriteria(cc, statusCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col);
            qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorPurchaseItemTable.VendorPurchaseIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorPurchaseTable.VendorIdColumn, VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.InvoiceDateColumn, VendorPurchaseTable.CreatedAtColumn, VendorPurchaseTable.DiscountColumn, VendorPurchaseTable.GoodsRcvNoColumn,
                VendorPurchaseTable.PrefixColumn, VendorPurchaseTable.BillSeriesColumn, VendorPurchaseTable.NoteAmountColumn,
                VendorPurchaseTable.NoteTypeColumn, VendorPurchaseTable.TaxRefNoColumn, VendorPurchaseTable.DiscountValueColumn, VendorPurchaseTable.TotalDiscountValueColumn, VendorPurchaseTable.TotalPurchaseValueColumn,
                VendorPurchaseTable.VatValueColumn, VendorPurchaseTable.FreeQtyTotalTaxValueColumn, VendorPurchaseTable.IsApplyTaxFreeQtyColumn, VendorPurchaseTable.RoundOffValueColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn,
                ProductStockTable.VATColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.CSTColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.GstTotalColumn);
            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.AddressColumn, VendorTable.AreaColumn,
                VendorTable.CityColumn, VendorTable.PincodeColumn, VendorTable.GsTinNoColumn, VendorTable.TinNoColumn,
                VendorTable.DrugLicenseNoColumn, VendorTable.LocationTypeColumn);
            qb.JoinBuilder.Join(InstanceTable.Table, InstanceTable.IdColumn, VendorPurchaseTable.InstanceIdColumn);
            qb.JoinBuilder.AddJoinColumn(InstanceTable.Table, InstanceTable.NameColumn, InstanceTable.AreaColumn, InstanceTable.CityColumn, InstanceTable.PincodeColumn, InstanceTable.AddressColumn,
                InstanceTable.TinNoColumn, InstanceTable.DrugLicenseNoColumn, InstanceTable.GsTinNoColumn,
                InstanceTable.isUnionTerritoryColumn);
            qb.SelectBuilder.SortBy(VendorPurchaseItemTable.VendorPurchaseItemSnoColumn);
            var list = new List<VendorPurchaseItem>();
            /*GST Related columns added by Poongodi on 04/07/2017*/

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseItem = new VendorPurchaseItem
                    {
                        ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        Quantity = Convert.ToDecimal(reader[VendorPurchaseItemTable.QuantityColumn.ColumnName]),
                        PurchasePrice = Convert.ToDecimal(reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName]),
                        Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        SchemeDiscountPerc = reader[VendorPurchaseItemTable.SchemeDiscountPercColumn.ColumnName] as decimal? ?? 0,
                        MarkupPerc = reader[VendorPurchaseItemTable.MarkupPercColumn.ColumnName] as decimal? ?? 0,
                        PackagePurchasePrice = reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0,
                        PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName] as decimal? ?? 0,
                        FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] as decimal? ?? 0,
                        Cgst = reader[VendorPurchaseItemTable.CgstColumn.ColumnName] as decimal? ?? 0,
                        Sgst = reader[VendorPurchaseItemTable.SgstColumn.ColumnName] as decimal? ?? 0,
                        Igst = reader[VendorPurchaseItemTable.IgstColumn.ColumnName] as decimal? ?? 0,
                        GstTotal = reader[VendorPurchaseItemTable.GstTotalColumn.ColumnName] as decimal? ?? 0,
                        GstValue = reader[VendorPurchaseItemTable.GstValueColumn.ColumnName] as decimal? ?? 0,
                        VatValue = reader[VendorPurchaseItemTable.VatValueColumn.ColumnName] as decimal? ?? 0,
                        DiscountValue = reader[VendorPurchaseItemTable.DiscountValueColumn.ColumnName] as decimal? ?? 0,
                        VendorPurchase =
                        {
                            Id = reader[VendorPurchaseTable.IdColumn.FullColumnName].ToString(),
                            InvoiceNo = reader[VendorPurchaseTable.InvoiceNoColumn.FullColumnName].ToString(),
                            InvoiceDate = Convert.ToDateTime(reader[VendorPurchaseTable.InvoiceDateColumn.FullColumnName]),
                            //GrnDate added by nandhini
                            GrnDate= Convert.ToDateTime(reader[VendorPurchaseTable.CreatedAtColumn.FullColumnName]),
                            Discount= reader[VendorPurchaseTable.DiscountColumn.FullColumnName] as decimal? ?? 0,
                            GoodsRcvNo= reader[VendorPurchaseTable.GoodsRcvNoColumn.FullColumnName].ToString(),
                            BillSeries = Convert.ToString( reader[VendorPurchaseTable.PrefixColumn.FullColumnName])+ reader[VendorPurchaseTable.BillSeriesColumn.FullColumnName].ToString(),
                            NoteAmount = reader[VendorPurchaseTable.NoteAmountColumn.FullColumnName] as decimal? ?? 0,
                            NoteType = reader[VendorPurchaseTable.NoteTypeColumn.FullColumnName].ToString(),
                            TaxRefNo = reader[VendorPurchaseTable.TaxRefNoColumn.FullColumnName] as int? ?? 0,
                            DiscountValue = reader[VendorPurchaseTable.DiscountValueColumn.FullColumnName] as decimal? ?? 0,
                            TotalDiscountValue = reader[VendorPurchaseTable.TotalDiscountValueColumn.FullColumnName] as decimal? ?? 0,
                            TotalPurchaseValue = reader[VendorPurchaseTable.TotalPurchaseValueColumn.FullColumnName] as decimal? ?? 0,
                            VatValue = reader[VendorPurchaseTable.VatValueColumn.FullColumnName] as decimal? ?? 0,
                            FreeQtyTotalTaxValue = reader[VendorPurchaseTable.FreeQtyTotalTaxValueColumn.FullColumnName] as decimal? ?? 0,
                            IsApplyTaxFreeQty = reader[VendorPurchaseTable.IsApplyTaxFreeQtyColumn.FullColumnName] as bool? ?? false,
                            RoundOffValue = reader[VendorPurchaseTable.RoundOffValueColumn.FullColumnName] as decimal? ?? 0,
                            Instance =
                            {
                                Name = reader[InstanceTable.NameColumn.FullColumnName].ToString(),
                                Address = reader[InstanceTable.AddressColumn.FullColumnName].ToString(),
                                Area = reader[InstanceTable.AreaColumn.FullColumnName].ToString(),
                                City = reader[InstanceTable.CityColumn.FullColumnName].ToString(),
                                Pincode = reader[InstanceTable.PincodeColumn.FullColumnName].ToString(),
                                TinNo = reader[InstanceTable.TinNoColumn.FullColumnName].ToString(),
                                DrugLicenseNo = reader[InstanceTable.DrugLicenseNoColumn.FullColumnName].ToString(),
                                GsTinNo  = reader[InstanceTable.GsTinNoColumn.FullColumnName].ToString(),
                                isUnionTerritory = reader[InstanceTable.isUnionTerritoryColumn.FullColumnName] as bool? ?? false,
                            },
                            Vendor=
                            {
                                Name = reader[VendorTable.NameColumn.FullColumnName].ToString(),
                                Address = reader[VendorTable.AddressColumn.FullColumnName].ToString(),
                                City = reader[VendorTable.CityColumn.FullColumnName].ToString(),
                                Area = reader[VendorTable.AreaColumn.FullColumnName].ToString(),
                                Pincode = reader[VendorTable.PincodeColumn.FullColumnName].ToString(),
                                TinNo = reader[VendorTable.TinNoColumn.FullColumnName].ToString(),
                                GsTinNo = reader[VendorTable.GsTinNoColumn.FullColumnName].ToString(),
                                DrugLicenseNo = reader[VendorTable.DrugLicenseNoColumn.FullColumnName].ToString(),
                                LocationType = reader[VendorTable.LocationTypeColumn.FullColumnName] as int? ?? 1,
                            },
                        },
                        ProductStock =
                        {
                            SellingPrice =
                                Convert.ToDecimal(reader[ProductStockTable.SellingPriceColumn.FullColumnName]),
                            VAT = Convert.ToDecimal(reader[ProductStockTable.VATColumn.FullColumnName]) as decimal? ??0,
                           // CST = Convert.ToDecimal(reader [ProductStockTable.CSTColumn.FullColumnName]!=null? reader [ProductStockTable.CSTColumn.FullColumnName]:  0) as decimal? ??0,
                            Product = {Name = reader[ProductTable.NameColumn.FullColumnName].ToString()},
                            BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString(),
                            ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName]),
                            Cgst = reader[VendorPurchaseItemTable.CgstColumn.ColumnName] as decimal? ?? 0,
                            Sgst = reader[VendorPurchaseItemTable.SgstColumn.ColumnName] as decimal? ?? 0,
                            Igst = reader[VendorPurchaseItemTable.IgstColumn.ColumnName] as decimal? ?? 0,
                            GstTotal = reader[VendorPurchaseItemTable.GstTotalColumn.ColumnName] as decimal? ?? 0,

                        },
                    };
                    if (reader[ProductStockTable.CSTColumn.FullColumnName] != null)
                        vendorPurchaseItem.ProductStock.CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ?? 0;
                    else
                        vendorPurchaseItem.ProductStock.CST = 0;



                    if (Convert.ToString(reader[VendorPurchaseTable.TaxRefNoColumn.FullColumnName]) != "")
                        vendorPurchaseItem.VendorPurchase.TaxRefNo = Convert.ToInt32(reader[VendorPurchaseTable.TaxRefNoColumn.FullColumnName].ToString());
                    else
                        vendorPurchaseItem.VendorPurchase.TaxRefNo = 0;


                    list.Add(vendorPurchaseItem);
                }
            }

            var sl = list;
            var vendorPurchase = sl.GroupBy(x => x.VendorPurchase.Id).Select(y =>
            {
                var s = new VendorPurchase();
                s = sl.First(w => w.VendorPurchase.Id == y.Key).VendorPurchase;
                s.VendorPurchaseItem = y.ToList();
                return s;
            }).ToList();
            return vendorPurchase.FirstOrDefault();
        }
        public async Task<PurchaseSettings> SaveTaxType(PurchaseSettings data)
        {
            var count = await CountPurchaseSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Update);
                qb.AddColumn(PurchaseSettingsTable.TaxTypeColumn);
                qb.Parameters.Add(PurchaseSettingsTable.TaxTypeColumn.ColumnName, data.TaxType);
                qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, PurchaseSettingsTable.Table);
            }
        }
        // Added Gavaskar 07-02-2017 Start 
        public async Task<PurchaseSettings> SaveBuyInvoiceDateEditOption(PurchaseSettings data)
        {
            var count = await CountPurchaseSettings(data);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Update);
                qb.AddColumn(PurchaseSettingsTable.BuyInvoiceDateEditColumn);
                qb.Parameters.Add(PurchaseSettingsTable.BuyInvoiceDateEditColumn.ColumnName, data.BuyInvoiceDateEdit);
                qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, data.AccountId);
                qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, data.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return data;
            }
            else
            {
                return await Insert(data, PurchaseSettingsTable.Table);
            }
        }
        public async Task<PurchaseSettings> getBuyInvoiceDateEditSetting(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Select);
            //qb.AddColumn(PurchaseSettingsTable.BuyInvoiceDateEditColumn);
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, InstanceId);
            var result = (await List<PurchaseSettings>(qb)).FirstOrDefault();
            if (result == null)
            {
                result = new PurchaseSettings();
            }
            if (result.EnableMarkup == false || result.EnableMarkup == null)
            {
                qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Select);
                qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, AccountId);
                qb.ConditionBuilder.And(PurchaseSettingsTable.EnableMarkupColumn, 1);
                var result1 = (await List<PurchaseSettings>(qb)).FirstOrDefault();
                if (result1 != null)
                {
                    result.EnableMarkup = result1.EnableMarkup;
                }
            }
            return result;
        }
        //Added Sarubala 18-04-2017 Start
        public async Task<PurchaseSettings> GetBillSeriesType(string AccId, string InsId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Select);
            qb.AddColumn(PurchaseSettingsTable.BillSeriesTypeColumn, PurchaseSettingsTable.CustomSeriesNameColumn);
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, InsId);
            var result = (await List<PurchaseSettings>(qb)).FirstOrDefault();
            if (result != null)
            {
                if (result.BillSeriesType == null)
                {
                    result.BillSeriesType = 1;
                }
            }
            else
            {
                result = new PurchaseSettings();
                result.BillSeriesType = 1;
            }
            return result;
        }
        public async Task<PurchaseSettings> GetBillSeriesType(string AccId, string InsId, SqlConnection con, SqlTransaction transaction)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Select);
            qb.AddColumn(PurchaseSettingsTable.BillSeriesTypeColumn, PurchaseSettingsTable.CustomSeriesNameColumn);
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, AccId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, InsId);
            var result = (await List<PurchaseSettings>(qb, con, transaction)).FirstOrDefault();
            if (result != null)
            {
                if (result.BillSeriesType == null)
                {
                    result.BillSeriesType = 1;
                }
            }
            else
            {
                result = new PurchaseSettings();
                result.BillSeriesType = 1;
            }
            return result;
        }
        public async Task<PurchaseSettings> SaveBillSeriesType(PurchaseSettings ps)
        {
            var count = await CountPurchaseSettings(ps);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Update);
                qb.AddColumn(PurchaseSettingsTable.BillSeriesTypeColumn, PurchaseSettingsTable.CustomSeriesNameColumn);
                qb.Parameters.Add(PurchaseSettingsTable.BillSeriesTypeColumn.ColumnName, ps.BillSeriesType);
                qb.Parameters.Add(PurchaseSettingsTable.CustomSeriesNameColumn.ColumnName, ps.CustomSeriesName);
                qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, ps.AccountId);
                qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, ps.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return ps;
            }
            else
            {
                return await Insert(ps, PurchaseSettingsTable.Table);
            }
        }
        //End Sarubala
        // Added Gavaskar 07-02-2017 End
        public async Task<PurchaseSettings> getTaxtypedata(string AccountId, string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Select);
            qb.AddColumn(PurchaseSettingsTable.TaxTypeColumn, PurchaseSettingsTable.IsSortByLowestPriceColumn); //Added by Sarubala on 26-09-17
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, InstanceId);
            //var userType = (await QueryExecuter.SingleValueAsyc(qb)).ToString();
            var result = (await List<PurchaseSettings>(qb)).FirstOrDefault();
            return result;
        }

        //Added by Sarubala on 11/05/18 - start
        public async Task<PurchaseSettings> getInvoiceValueSettings(string accId, string insId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Select);
            qb.AddColumn(PurchaseSettingsTable.InvoiceValueSettingColumn, PurchaseSettingsTable.InvoiceValueDifferenceColumn);
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, insId);
            var result = (await List<PurchaseSettings>(qb)).FirstOrDefault();
            return result;
        }

        public async Task<PurchaseSettings> saveInvoiceValueSettings(PurchaseSettings ps)
        {
            int count = await CountPurchaseSettings(ps);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Update);
                qb.AddColumn(PurchaseSettingsTable.InvoiceValueSettingColumn, PurchaseSettingsTable.InvoiceValueDifferenceColumn, PurchaseSettingsTable.UpdatedAtColumn, PurchaseSettingsTable.UpdatedByColumn);
                qb.Parameters.Add(PurchaseSettingsTable.InvoiceValueSettingColumn.ColumnName, ps.InvoiceValueSetting);
                qb.Parameters.Add(PurchaseSettingsTable.InvoiceValueDifferenceColumn.ColumnName, ps.InvoiceValueDifference);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedByColumn.ColumnName, ps.UpdatedBy);
                qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, ps.AccountId);
                qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, ps.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return ps;
            }
            else
            {
                return await Insert(ps, PurchaseSettingsTable.Table);
            }

        }
        //Added by Sarubala on 11/05/18 - end

        public async Task<TaxSeriesItem> saveTaxTypeseriesItem(TaxSeriesItem TS)
        {
            return await Insert(TS, TaxSeriesItemTable.Table);
        }
        public async Task<List<TaxSeriesItem>> getCustomTaxSeriesItems(TaxSeriesItem IS)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TaxSeriesItemTable.Table, OperationType.Select);
            qb.AddColumn(TaxSeriesItemTable.IdColumn, TaxSeriesItemTable.TaxSeriesNameColumn, TaxSeriesItemTable.ActiveStatusColumn);
            qb.ConditionBuilder.And(TaxSeriesItemTable.AccountIdColumn, IS.AccountId);
            qb.ConditionBuilder.And(TaxSeriesItemTable.InstanceIdColumn, IS.InstanceId);
            qb.SelectBuilder.SortBy(TaxSeriesItemTable.CreatedAtColumn);
            return await List<TaxSeriesItem>(qb);
            //IS.InvoiceSeriesItemList = await List<InvoiceSeriesItem>(qb);
        }
        public async Task<int> CountPurchaseSettings(PurchaseSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<ProductSearchSettings> SaveSearchType(ProductSearchSettings type1)
        {
            var count = await CountSearchType(type1);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(ProductSearchSettingsTable.Table, OperationType.Update);
                qb.AddColumn(ProductSearchSettingsTable.SearchTypeColumn, ProductSearchSettingsTable.UpdatedAtColumn);
                qb.Parameters.Add(ProductSearchSettingsTable.SearchTypeColumn.ColumnName, type1.SearchType);
                qb.Parameters.Add(ProductSearchSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.ConditionBuilder.And(ProductSearchSettingsTable.AccountIdColumn, type1.AccountId);
                qb.ConditionBuilder.And(ProductSearchSettingsTable.InstanceIdColumn, type1.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return type1;
            }
            else
            {
                return await Insert(type1, ProductSearchSettingsTable.Table);
            }
        }
        public async Task<string> GetSearchType(string accId, string insId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductSearchSettingsTable.Table, OperationType.Select);
            qb.AddColumn(ProductSearchSettingsTable.SearchTypeColumn);
            qb.ConditionBuilder.And(ProductSearchSettingsTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(ProductSearchSettingsTable.InstanceIdColumn, insId);
            string type1 = null;
            var searchType = (await List<ProductSearchSettings>(qb)).FirstOrDefault();
            if (searchType != null)
            {
                type1 = searchType.SearchType;
            }
            return type1;
        }
        public async Task<int> CountSearchType(ProductSearchSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(ProductSearchSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(ProductSearchSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(ProductSearchSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }
        public async Task<bool> GetAllowDecimalSetting(string accId, string insId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Select);
            qb.AddColumn(PurchaseSettingsTable.IsAllowDecimalColumn);
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, insId);
            bool allowDecimal = false;
            var purchaseSetting = (await List<PurchaseSettings>(qb)).FirstOrDefault();
            if (purchaseSetting != null)
            {
                allowDecimal = Convert.ToBoolean(purchaseSetting.IsAllowDecimal);
            }
            return allowDecimal;
        }
        public async Task<string> GetAllowCompletePurchaseKeyTypeSetting(string accId, string insId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Select);
            qb.AddColumn(PurchaseSettingsTable.CompletePurchaseKeyTypeColumn);
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, insId);
            string allowCompletePurchase = null;
            var purchaseSetting = (await List<PurchaseSettings>(qb)).FirstOrDefault();
            if (purchaseSetting != null)
            {
                allowCompletePurchase = purchaseSetting.CompletePurchaseKeyType;
            }
            return allowCompletePurchase;
        }
        public async Task<PurchaseSettings> SaveAllowDecimalSetting(PurchaseSettings ps)
        {
            var count = await CountAllowDecimalSetting(ps);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Update);
                qb.AddColumn(PurchaseSettingsTable.IsAllowDecimalColumn, PurchaseSettingsTable.UpdatedAtColumn, PurchaseSettingsTable.UpdatedByColumn);
                qb.Parameters.Add(PurchaseSettingsTable.IsAllowDecimalColumn.ColumnName, ps.IsAllowDecimal);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedByColumn.ColumnName, ps.UpdatedBy);
                qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, ps.AccountId);
                qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, ps.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return ps;
            }
            else
            {
                return await Insert(ps, PurchaseSettingsTable.Table);
            }
        }
        public async Task<int> CountAllowDecimalSetting(PurchaseSettings data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, data.InstanceId);
            return Convert.ToInt32(await SingleValueAsyc(qb));
        }

        //Added to save purchase settings by Sarubala on 26-09-17 - start
        public async Task<PurchaseSettings> SaveisSortByLowestPriceSetting(PurchaseSettings ps)
        {
            var count = await CountAllowDecimalSetting(ps);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Update);
                qb.AddColumn(PurchaseSettingsTable.IsSortByLowestPriceColumn, PurchaseSettingsTable.UpdatedAtColumn, PurchaseSettingsTable.UpdatedByColumn);
                qb.Parameters.Add(PurchaseSettingsTable.IsSortByLowestPriceColumn.ColumnName, ps.IsSortByLowestPrice);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedByColumn.ColumnName, ps.UpdatedBy);
                qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, ps.AccountId);
                qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, ps.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return ps;
            }
            else
            {
                return await Insert(ps, PurchaseSettingsTable.Table);
            }
        }

        //Added to save purchase settings by Sarubala on 26-09-17 - end

        public async Task<PurchaseSettings> SaveAllowCompletePurchaseKeyType(PurchaseSettings ps)
        {
            var count = await CountAllowDecimalSetting(ps);
            if (ps.CompletePurchaseKeyType != null)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Update);
                qb.AddColumn(PurchaseSettingsTable.CompletePurchaseKeyTypeColumn, PurchaseSettingsTable.UpdatedAtColumn, PurchaseSettingsTable.UpdatedByColumn);
                qb.Parameters.Add(PurchaseSettingsTable.CompletePurchaseKeyTypeColumn.ColumnName, ps.CompletePurchaseKeyType);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedByColumn.ColumnName, ps.UpdatedBy);
                qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, ps.AccountId);
                qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, ps.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
                return ps;
            }
            else
            {
                return await Insert(ps, PurchaseSettingsTable.Table);
            }
        }
        public async Task<VendorPurchase> GetVendorPurchaseSingleData(string id)
        {
            /*Prefix Column Added by Poongodi on 14/06/2017*/
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseTable.Table, OperationType.Select);
            qb.AddColumn(VendorPurchaseTable.IdColumn, VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.PrefixColumn, VendorPurchaseTable.InvoiceDateColumn
                , VendorPurchaseTable.VendorIdColumn, VendorPurchaseTable.DiscountColumn, VendorPurchaseTable.CreditColumn, VendorPurchaseTable.PaymentTypeColumn, VendorPurchaseTable.BillSeriesColumn
                , VendorPurchaseTable.ChequeNoColumn, VendorPurchaseTable.ChequeDateColumn, VendorPurchaseTable.CreditNoOfDaysColumn, VendorPurchaseTable.CommentsColumn, VendorPurchaseTable.GoodsRcvNoColumn, VendorPurchaseTable.FileNameColumn,
                VendorPurchaseTable.NoteTypeColumn, VendorPurchaseTable.NoteAmountColumn,
                VendorPurchaseTable.DiscountTypeColumn, VendorPurchaseTable.DiscountInRupeesColumn, VendorPurchaseTable.TaxRefNoColumn, VendorPurchaseTable.VendorPurchaseFormulaIdColumn,
                VendorPurchaseTable.MarkupPercColumn, VendorPurchaseTable.IsApplyTaxFreeQtyColumn, VendorPurchaseTable.InitialValueColumn);
            qb.ConditionBuilder.And(VendorPurchaseTable.IdColumn, id);
            qb.JoinBuilder.Join(VendorTable.Table, VendorTable.IdColumn, VendorPurchaseTable.VendorIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorTable.Table, VendorTable.NameColumn, VendorTable.EmailColumn, VendorTable.MobileColumn, VendorTable.EnableCSTColumn, VendorTable.DiscountColumn, VendorTable.LocationTypeColumn);
            var result = (await List<VendorPurchase>(qb)).First();
            var formula = await _vendorDataAccess.GetPurchaseFormulaById(result.VendorPurchaseFormulaId);
            result.Vendor.VendorPurchaseFormula = formula;
            // Added by Gavaskar Purchase Return 01-09-2017 Start
            ////  var ReturnItemList = new List<VendorPurchaseReturnItem>();
            decimal dReturn = await GetPurchaseReturnValue(id);

            var vp = await EditSqlQueryList(result);

            ////  ReturnItemList = PurchaseReturnItemList(result).Result;
            result.ReturnAmount = dReturn; //Return value assigned to sales object

            // Added by Gavaskar Purchase Return 01-09-2017 End

            result.VendorPurchaseItem = vp.Where(y => y.VendorPurchaseId == result.Id).Select(z => z);

            var PurReturn = await getReturnItemsAlongWithPurchase(id);
            if (PurReturn != null)
            {
                result.VendorReturn = PurReturn;
            }

            var vendorPurchaseItemList = new List<VendorPurchaseItem>();
            if (result.VendorReturn != null && result.VendorReturn.VendorPurchaseReturnItem.Count > 0)
            {
                vendorPurchaseItemList = result.VendorPurchaseItem.ToList();
                if ((bool)result.VendorReturn.IsAlongWithPurchase)
                {
                    foreach (VendorPurchaseReturnItem vpri in result.VendorReturn.VendorPurchaseReturnItem)
                    {
                        VendorPurchaseItem pi = new VendorPurchaseItem();
                        pi.vendorReturnId = vpri.VendorReturnId;
                        pi.VendorReturn = true;
                        pi.Id = vpri.Id;
                        pi.ProductStockId = vpri.ProductStockId;
                        pi.Quantity = vpri.Quantity * (0);
                        pi.PackageQty = vpri.Quantity / vpri.ProductStock.PackageSize;
                        pi.PackageSize = vpri.ProductStock.PackageSize;
                        //pi.PackagePurchasePrice = vpri.ProductStock.PackagePurchasePrice;
                        pi.PackagePurchasePrice = (vpri.ReturnPurchasePrice * vpri.ProductStock.PackageSize) + ((vpri.DiscountValue / vpri.Quantity) * vpri.ProductStock.PackageSize) - ((vpri.GstAmount / vpri.Quantity) * vpri.ProductStock.PackageSize);
                        pi.PackageSellingPrice = vpri.ProductStock.SellingPrice * vpri.ProductStock.PackageSize;
                        pi.PackageMRP = vpri.ProductStock.MRP * vpri.ProductStock.PackageSize;
                        pi.Discount = vpri.Discount;
                        pi.DiscountValue = vpri.DiscountValue;
                        pi.ProductStock = vpri.ProductStock;
                        pi.ProductStock.BatchNo = vpri.ProductStock.BatchNo;
                        pi.GstTotal = vpri.ProductStock.GstTotal;
                        pi.Igst = vpri.ProductStock.GstTotal;
                        pi.Sgst = vpri.ProductStock.GstTotal / 2;
                        pi.Cgst = vpri.ProductStock.GstTotal / 2;
                        pi.RackNo = vpri.ProductStock.Product.RackNo;


                        //result.VendorPurchaseItem.ToList().Add(pi);                        
                        vendorPurchaseItemList.Add(pi);
                    }

                }
            }
            if (result.VendorReturn != null && result.VendorReturn.VendorPurchaseReturnItem.Count > 0)
            {
                result.VendorPurchaseItem = vendorPurchaseItemList;
            }
            result.isPaymentStarted = await checkPaymentstatus(id);
            return result;
        }

        // added by Gavaskar Purchase with return 01-09-2017 Start
        public async Task<decimal> GetPurchaseReturnValue(string PurchaseId)
        {
            decimal dReturnValue = 0;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("PurchaseId", PurchaseId);
            var purchaseReturn = await sqldb.ExecuteProcedureAsync<dynamic>("USP_Get_PurchaseReturnValue", parms);
            if (purchaseReturn != null && purchaseReturn.Count() != 0)
            {
                dReturnValue = Convert.ToDecimal(purchaseReturn.FirstOrDefault().ReturnValue);
            }
            return dReturnValue;
        }

        public Task<List<VendorPurchaseReturnItem>> PurchaseReturnItemList(VendorPurchase Purchase)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorReturnItemTable.QuantityColumn, VendorReturnItemTable.ProductStockIdColumn, VendorReturnItemTable.ReturnPurchasePriceColumn, VendorReturnItemTable.ReturnedTotalColumn, VendorReturnItemTable.IgstColumn, VendorReturnItemTable.CgstColumn, VendorReturnItemTable.SgstColumn, VendorReturnItemTable.GstTotalColumn, VendorReturnItemTable.DiscountColumn, VendorReturnItemTable.DiscountValueColumn);
            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.LeftJoin(VendorReturnTable.Table, VendorReturnItemTable.VendorReturnIdColumn, VendorReturnTable.IdColumn);
            qb.ConditionBuilder.And(VendorReturnTable.VendorPurchaseIdColumn, Purchase.Id);

            var isDeletedCondition = new CustomCriteria(VendorReturnItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            string isDelete = "1";
            var cc1 = new CriteriaColumn(VendorReturnItemTable.IsDeletedColumn, isDelete, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, isDeletedCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);

            return List<VendorPurchaseReturnItem>(qb);
        }

        public async Task<VendorPurchaseReturn> getReturnItemsAlongWithPurchase(string PurchaseId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnTable.Table, OperationType.Select);

            qb.ConditionBuilder.And(VendorReturnTable.IsAlongWithPurchaseColumn);
            qb.Parameters.Add(VendorReturnTable.IsAlongWithPurchaseColumn.ColumnName, 1);
            qb.ConditionBuilder.And(VendorReturnTable.VendorPurchaseIdColumn, PurchaseId);

            var isDeletedCondition = new CustomCriteria(VendorReturnItemTable.IsDeletedColumn, CriteriaCondition.IsNull);
            string isDelete = "1";
            var cc1 = new CriteriaColumn(VendorReturnItemTable.IsDeletedColumn, isDelete, CriteriaEquation.NotEqual);
            var col1 = new CustomCriteria(cc1, isDeletedCondition, CriteriaCondition.Or);
            qb.ConditionBuilder.And(col1);

            qb.AddColumn(VendorReturnTable.IdColumn, VendorReturnTable.ReturnNoColumn, VendorReturnTable.VendorPurchaseIdColumn, VendorReturnTable.IsAlongWithPurchaseColumn);

            qb.JoinBuilder.Join(VendorReturnItemTable.Table, VendorReturnTable.IdColumn, VendorReturnItemTable.VendorReturnIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorReturnItemTable.Table, VendorReturnItemTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.Table.ColumnList.ToArray());
            qb.SelectBuilder.SortBy(VendorReturnItemTable.CreatedAtColumn);

            var preturn = new VendorPurchaseReturn();

            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    preturn.Id = reader[VendorReturnTable.IdColumn.ColumnName].ToString();
                    preturn.ReturnNo = reader[VendorReturnTable.ReturnNoColumn.ColumnName].ToString();
                    preturn.VendorPurchaseId = reader[VendorReturnTable.VendorPurchaseIdColumn.ColumnName].ToString();
                    preturn.IsAlongWithPurchase = reader[VendorReturnTable.IsAlongWithPurchaseColumn.ColumnName] as bool? ?? false;

                    VendorPurchaseReturnItem prItem = new VendorPurchaseReturnItem();
                    prItem.Id = reader[VendorReturnItemTable.IdColumn.FullColumnName].ToString();
                    prItem.VendorReturnId = reader[VendorReturnItemTable.VendorReturnIdColumn.FullColumnName].ToString();
                    prItem.ProductStockId = reader[VendorReturnItemTable.ProductStockIdColumn.FullColumnName].ToString();
                    prItem.Quantity = (reader[VendorReturnItemTable.QuantityColumn.FullColumnName]) as decimal? ?? 0;
                    prItem.ReturnPurchasePrice = (reader[VendorReturnItemTable.ReturnPurchasePriceColumn.FullColumnName]) as decimal? ?? 0;
                    prItem.ReturnedTotal = (reader[VendorReturnItemTable.ReturnedTotalColumn.FullColumnName]) as decimal? ?? 0;
                    prItem.Discount = (reader[VendorReturnItemTable.DiscountColumn.FullColumnName]) as decimal? ?? 0;
                    prItem.DiscountValue = (reader[VendorReturnItemTable.DiscountValueColumn.FullColumnName]) as decimal? ?? 0;
                    prItem.GstAmount = (reader[VendorReturnItemTable.GstAmountColumn.FullColumnName]) as decimal? ?? 0;
                    prItem.GstTotal = (reader[VendorReturnItemTable.GstTotalColumn.FullColumnName]) as decimal? ?? 0;

                    prItem.ProductStock.Fill(reader).Product.Fill(reader);
                    prItem.ProductStock.GstTotal = prItem.GstTotal;
                    //prItem.ProductStock.Product.RackNo = reader[ProductInstanceTable.RackNoColumn.FullColumnName].ToString() ?? ProductTable.RackNoColumn.FullColumnName.ToString();
                    //prItem.ProductStock.Product.BoxNo = reader[ProductInstanceTable.BoxNoColumn.FullColumnName].ToString() ?? string.Empty;

                    preturn.VendorPurchaseReturnItem.Add(prItem);

                }
            }
            if (preturn.VendorPurchaseReturnItem.Count > 0)
            {
                return preturn;
            }
            else
            {
                return null;
            }
        }

        // added by Gavaskar Purchase with return 01-09-2017 End
        public async Task<bool> checkPaymentstatus(string vendorPurchaseId)
        {
            bool status = false;
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("vendorpurchaseid", vendorPurchaseId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseEditPaymentStatus", parms);
            var data = result.Select(item =>
            {
                var payment = new Payment()
                {
                    Debit = item.Debit,
                    Credit = item.Credit
                };
                return payment;
            }).FirstOrDefault();
            if (data != null)
            {
                if ((data.Credit - data.Debit) == 0)
                {//Payment  Completed
                    status = true;
                }
                if (data.Debit != 0)
                {
                    //Payment Started
                    status = true;
                }
                if (data.Debit == 0)
                {
                    //Payment Not Started
                    status = false;
                }
            }
            return status;
        }
        public async Task<List<VendorPurchaseItem>> EditSqlQueryList(VendorPurchase data)
        {
            /* var viqb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
             viqb.ConditionBuilder.And(VendorPurchaseItemTable.VendorPurchaseIdColumn, data.Id);
             viqb.AddColumn(VendorPurchaseItemTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn, VendorPurchaseItemTable.PackageSizeColumn,
                 VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.PackagePurchasePriceColumn,
                 VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.VendorPurchaseIdColumn,
                 VendorPurchaseItemTable.QuantityColumn, DbColumn.CustomColumn("Quantity as ActualQty"), VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.AccountIdColumn
                 , VendorPurchaseItemTable.InstanceIdColumn, VendorPurchaseItemTable.CreatedByColumn, VendorPurchaseItemTable.UpdatedByColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.fromDcIdColumn, VendorPurchaseItemTable.fromTempIdColumn);
             viqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn);
             viqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.CSTColumn,
                 ProductStockTable.ProductIdColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.StockColumn,
                 ProductStockTable.AccountIdColumn, ProductStockTable.InstanceIdColumn, ProductStockTable.CreatedByColumn,
                 ProductStockTable.UpdatedByColumn, ProductStockTable.IdColumn, ProductStockTable.PurchaseBarcodeColumn);
             viqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
             viqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn, ProductTable.IdColumn);
             viqb.JoinBuilder.Join(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductStockTable.ProductIdColumn);
             viqb.(ProductInstanceTable.Table, ProductInstanceTable.InstanceIdColumn, ProductStockTable.InstanceIdColumn);
             viqb.JoinBuilder.AddJoinColumn(ProductInstanceTable.Table, ProductInstanceTable.RackNoColumn );
             viqb.SelectBuilder.SortBy(VendorPurchaseItemTable.CreatedAtColumn);*/
            //string sQry = $@"SELECT VendorPurchaseItem.Id,
            //                        VendorPurchaseItem.ProductStockId,
            //                     VendorPurchaseItem.PackageSize,
            //                     VendorPurchaseItem.PackageQty,
            //                     VendorPurchaseItem.PackagePurchasePrice,
            //                     VendorPurchaseItem.PackageSellingPrice,
            //                        VendorPurchaseItem.PackageMRP,
            //                     VendorPurchaseItem.VendorPurchaseId,
            //                     VendorPurchaseItem.Quantity,
            //                        Quantity as ActualQty,
            //                     VendorPurchaseItem.PurchasePrice,
            //                     VendorPurchaseItem.AccountId,
            //                     VendorPurchaseItem.InstanceId,
            //                     VendorPurchaseItem.CreatedBy,
            //                        VendorPurchaseItem.UpdatedBy,
            //                     VendorPurchaseItem.FreeQty,
            //                     VendorPurchaseItem.Discount,
            //                     VendorPurchaseItem.fromDcId,
            //                     VendorPurchaseItem.fromTempId,
            //                        isnull(VendorPurchaseItem.IsPoItem,0) as IsPoItem,
            //                        ProductStock.SellingPrice AS[ProductStock.SellingPrice],
            //                        ProductStock.MRP AS[ProductStock.MRP],
            //                     ProductStock.VAT AS[ProductStock.VAT],
            //                     ProductStock.CST AS[ProductStock.CST],
            //                        ProductStock.ProductId AS[ProductStock.ProductId],
            //                     ProductStock.BatchNo AS[ProductStock.BatchNo],
            //                     ProductStock.ExpireDate AS[ProductStock.ExpireDate], 
            //                        ProductStock.Stock AS[ProductStock.Stock],
            //                     ProductStock.AccountId AS[ProductStock.AccountId],
            //                     ProductStock.InstanceId AS[ProductStock.InstanceId],
            //                        ProductStock.CreatedBy AS[ProductStock.CreatedBy],
            //                     ProductStock.UpdatedBy AS[ProductStock.UpdatedBy],
            //                     ProductStock.Id AS[ProductStock.Id],
            //                        ProductStock.PurchaseBarcode AS[ProductStock.PurchaseBarcode],
            //                     Product.Name AS[Product.Name],
            //                     Product.Id AS[Product.Id],
            //                        ProductInstance.RackNo AS [Product.RackNo],
            //                     Product.Eancode AS [Product.Eancode],
            //                     ProductStock.Eancode AS [ProductStock.Eancode]
            //                                  FROM VendorPurchaseItem INNER JOIN ProductStock ON ProductStock.Id = VendorPurchaseItem.ProductStockId
            //                                     INNER JOIN Product ON Product.Id = ProductStock.ProductId 
            //                  INNER JOIN ProductInstance ON ProductInstance.ProductId = ProductStock.ProductId 
            //                  and ProductInstance.InstanceId = ProductStock.InstanceId 
            //                  and (VendorPurchaseItem.Status is null or VendorPurchaseItem.Status = 1) 
            //                    WHERE  VendorPurchaseItem.VendorPurchaseId = '{data.Id}'  ORDER BY VendorPurchaseItem.CreatedAt asc";
            //var list = new List<VendorPurchaseItem>();
            //var qb = QueryBuilderFactory.GetQueryBuilder(sQry);
            //using (var reader = await QueryExecuter.QueryAsyc(qb))
            //{
            //    while (reader.Read())
            //    {
            //        var vendorPurchaseItem = new VendorPurchaseItem
            //        {
            //            Id = reader[VendorPurchaseItemTable.IdColumn.ColumnName].ToString(),
            //            ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
            //            VendorPurchaseId = reader[VendorPurchaseItemTable.VendorPurchaseIdColumn.ColumnName].ToString(),
            //            PackageSize = Convert.ToDecimal(reader[VendorPurchaseItemTable.PackageSizeColumn.ColumnName]),
            //            PackageQty = Convert.ToDecimal(reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName]),
            //            PackagePurchasePrice = Convert.ToDecimal(reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName]),
            //            PackageSellingPrice = Convert.ToDecimal(reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName]),
            //            PackageMRP = (reader[VendorPurchaseItemTable.PackageMRPColumn.ColumnName]) as decimal? ?? 0,
            //            Quantity = Convert.ToDecimal(reader[VendorPurchaseItemTable.QuantityColumn.ColumnName]),
            //            PurchasePrice = Convert.ToDecimal(reader[VendorPurchaseItemTable.PurchasePriceColumn.ColumnName]),
            //            AccountId = reader[VendorPurchaseItemTable.AccountIdColumn.ColumnName].ToString(),
            //            InstanceId = reader[VendorPurchaseItemTable.InstanceIdColumn.ColumnName].ToString(),
            //            CreatedBy = reader[VendorPurchaseItemTable.CreatedByColumn.ColumnName].ToString(),
            //            UpdatedBy = reader[VendorPurchaseItemTable.UpdatedByColumn.ColumnName].ToString(),
            //            Discount = Convert.ToDecimal(reader[VendorPurchaseItemTable.DiscountColumn.ColumnName]),
            //            FreeQty = Convert.ToDecimal(reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName]),
            //            fromDcId = reader[VendorPurchaseItemTable.fromDcIdColumn.ColumnName].ToString(),
            //            fromTempId = reader[VendorPurchaseItemTable.fromTempIdColumn.ColumnName].ToString(),
            //            IsPoItem = reader["IsPoItem"] as bool? ?? false
            //        };
            //        vendorPurchaseItem.ProductStock.Id = reader[ProductStockTable.IdColumn.FullColumnName].ToString();
            //        vendorPurchaseItem.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
            //        vendorPurchaseItem.ProductStock.Eancode = reader[ProductStockTable.EancodeColumn.FullColumnName].ToString() ?? "";
            //        vendorPurchaseItem.ProductStock.Product.Eancode = reader[ProductStockTable.EancodeColumn.FullColumnName].ToString() ?? "";
            //        vendorPurchaseItem.ProductStock.ProductId = reader[ProductStockTable.ProductIdColumn.FullColumnName].ToString();
            //        vendorPurchaseItem.ProductStock.Product.Id = reader[ProductStockTable.ProductIdColumn.FullColumnName].ToString();
            //        vendorPurchaseItem.ProductStock.VAT = Convert.ToDecimal(reader[ProductStockTable.VATColumn.FullColumnName]);
            //        vendorPurchaseItem.ProductStock.CST = (reader[ProductStockTable.CSTColumn.FullColumnName]) as decimal? ?? 0;
            //        vendorPurchaseItem.ProductStock.SellingPrice = Convert.ToDecimal(reader[ProductStockTable.SellingPriceColumn.FullColumnName]);
            //        vendorPurchaseItem.ProductStock.MRP = (reader[ProductStockTable.MRPColumn.FullColumnName]) as decimal? ?? 0;
            //        vendorPurchaseItem.ProductStock.BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString();
            //        vendorPurchaseItem.ProductStock.ExpireDate = Convert.ToDateTime(reader[ProductStockTable.ExpireDateColumn.FullColumnName]);
            //        vendorPurchaseItem.ProductStock.Stock = Convert.ToDecimal(reader[ProductStockTable.StockColumn.FullColumnName]);
            //        vendorPurchaseItem.ActualQty = Convert.ToDecimal(reader["ActualQty"]);
            //        vendorPurchaseItem.ProductStock.VendorId = data.VendorId;
            //        vendorPurchaseItem.ProductStock.PurchaseBarcode = reader[ProductStockTable.PurchaseBarcodeColumn.FullColumnName].ToString();
            //        vendorPurchaseItem.ProductStock.AccountId = reader[ProductStockTable.AccountIdColumn.ColumnName].ToString();
            //        vendorPurchaseItem.ProductStock.InstanceId = reader[ProductStockTable.InstanceIdColumn.ColumnName].ToString();
            //        vendorPurchaseItem.ProductStock.UpdatedBy = reader[ProductStockTable.UpdatedByColumn.ColumnName].ToString();
            //        vendorPurchaseItem.ProductStock.Product.RackNo = reader[ProductTable.RackNoColumn.FullColumnName].ToString();
            //    }
            //}
            /*Productstock TaxRefNo Added by Poongodi on 14/08/2017*/
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("VendorPurchaseId", data.Id);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetVendorPurchaseItemsHistory", parms);
            if (result.Count() <= 0)
            { return null; }
            var res = result.Select(x =>
            {
                var obj = new VendorPurchaseItem()
                {
                    Id = x.vpiid.ToString(),
                    ProductName = x.vpiProductName as System.String ?? "",
                    ProductStockId = x.vpiProductStockId.ToString(),
                    VendorPurchaseId = x.vpiVendorPurchaseId.ToString(),
                    PackageSize = Convert.ToDecimal(x.vpiPackageSize),
                    PackageQty = Convert.ToDecimal(x.vpiPackageQty),
                    PackagePurchasePrice = Convert.ToDecimal(x.vpiPackagePurchasePrice),
                    PackageSellingPrice = Convert.ToDecimal(x.vpiPackageSellingPrice),
                    PackageMRP = (x.vpiPackageMRP) as decimal? ?? 0,
                    Quantity = Convert.ToDecimal(x.vpiQuantity),
                    PurchasePrice = Convert.ToDecimal(x.vpiPurchasePrice),
                    AccountId = x.vpiAccountId.ToString(),
                    InstanceId = x.vpiInstanceId.ToString(),
                    CreatedBy = x.vpiCreatedBy.ToString(),
                    UpdatedBy = x.vpiUpdatedBy.ToString(),
                    Discount = Convert.ToDecimal(x.vpiDiscount),
                    MarkupPerc = Convert.ToDecimal(x.vpiMarkupPerc),
                    SchemeDiscountPerc = Convert.ToDecimal(x.vpiSchemeDiscountPerc),
                    FreeQty = Convert.ToDecimal(x.vpiFreeQty),
                    fromDcId = !string.IsNullOrEmpty(x.vpifromDcId) ? x.vpifromDcId.ToString() : string.Empty,
                    fromTempId = !string.IsNullOrEmpty(x.vppifromTempId) ? x.vppifromTempId.ToString() : string.Empty,
                    IsPoItem = x.vpiIsPoItem as bool? ?? false,
                    ActualQty = (x.vpiActualQty) as decimal? ?? 0,
                    Debit = (x.PaDebit) as decimal? ?? 0,
                    vpiReturnid = !string.IsNullOrEmpty(x.vpiReturnProductStockId) ? x.vpiReturnProductStockId.ToString() : string.Empty,
                    TransferId = !string.IsNullOrEmpty(x.TransferId) ? x.TransferId.ToString() : string.Empty,
                    HsnCode = !string.IsNullOrEmpty(x.HsnCode) ? x.HsnCode.ToString() : string.Empty,
                    Igst = (x.Igst) as decimal? ?? 0,
                    Cgst = (x.Cgst) as decimal? ?? 0,
                    Sgst = (x.Sgst) as decimal? ?? 0,
                    GstTotal = (x.GstTotal) as decimal? ?? 0,
                    VendorReturn = false,
                    ProductStock = {
                                Id = x.psId.ToString(),
                                Eancode = !string.IsNullOrEmpty(x.psEancode) ? x.psEancode.ToString() : string.Empty,
                                ProductId = x.psProductId.ToString(),
                                VAT =  (x.psVAT) as decimal? ?? 0,

                                //GST related fields
                                    HsnCode =  !string.IsNullOrEmpty(x.psHsnCode)?x.psHsnCode.ToString():string.Empty,
                                Igst =  (x.psIgst) as decimal? ?? 0,
                                Cgst =  (x.psCgst) as decimal? ?? 0,
                                Sgst =  (x.psSgst) as decimal? ?? 0,
                                GstTotal =  (x.psGstTotal) as decimal? ?? 0,

                                CST =  (x.psCST) as decimal? ?? 0,
                                SellingPrice =  (x.psSellingPrice) as decimal? ?? 0,
                                MRP  = (x.psMRP) as decimal? ?? 0,
                                BatchNo = x.psBatchNo.ToString(),
                                ExpireDate= (x.psExpireDate) as DateTime? ?? DateTime.MinValue,
                                Stock = (x.psStock) as decimal? ?? 0,
                                VendorId =  data.VendorId,
                                PurchaseBarcode = !string.IsNullOrEmpty(x.psPurchaseBarcode) ? x.psPurchaseBarcode.ToString() : string.Empty,
                                AccountId  =  x.psAccountId.ToString(),
                                InstanceId =  x.psInstanceId.ToString(),
                                UpdatedBy =  x.psUpdatedBy.ToString(),
                                TaxRefNo = x.psTaxRefno,
                                Product = {
                                    Name = x.pName.ToString(),
                                    Eancode = !string.IsNullOrEmpty(x.pEancode) ? x.pEancode.ToString() : string.Empty,
                                    Id  = x.psProductId.ToString(),
                                    RackNo  =  !string.IsNullOrEmpty(x.pRackNo) ? x.pRackNo.ToString() : string.Empty,
                                    BoxNo  =  !string.IsNullOrEmpty(x.pBoxNo) ? x.pBoxNo.ToString() : string.Empty /*BoxNo included by settu on 19-05-2017*/
                                   //  HsnCode =  x.pHsnCode .ToString(),
                                }
                    }
                };
                //Check Sold OR Not
                obj.DisableDelete = (obj.PackageQty * obj.PackageSize) <= obj.ProductStock.Stock ? false : true;
                //Check Fromdc OR FromTemp OR Return OR FromPo OR PaymentStarted OR Transferred
                if (!string.IsNullOrEmpty(obj.fromDcId) || !string.IsNullOrEmpty(obj.fromTempId)
                || obj.vpiReturnid != string.Empty || obj.Debit != 0 || (bool)(obj.IsPoItem) || !string.IsNullOrEmpty(obj.TransferId))
                {
                    obj.DisableDelete = true;
                }
                return obj;
            }).ToList();
            return res;
        }
        public async Task<VendorPurchase> Update(VendorPurchase data)
        {
            if (data.WriteExecutionQuery)
            {
                WriteUpdateExecutionQuery(data, VendorPurchaseTable.Table);
                return await Task.FromResult(data);
            }
            else
            {
                return await Update(data, VendorPurchaseTable.Table);
            }
        }
        public async Task<IEnumerable<VendorPurchaseItem>> UpdateVendorPurchaseItem(IEnumerable<VendorPurchaseItem> itemList, int? LocationType = 0)
        {
            foreach (var vendorPurchaseItem in itemList)
            {
                //Strips Logic 
                //vendorPurchaseItem.Quantity = vendorPurchaseItem.PackageSize * vendorPurchaseItem.PackageQty;
                //vendorPurchaseItem.PurchasePrice = vendorPurchaseItem.PackagePriceAfterTax / vendorPurchaseItem.PackageSize;
                //vendorPurchaseItem.ProductStock.SellingPrice = (vendorPurchaseItem.PackageSellingPrice / vendorPurchaseItem.PackageSize);
                //vendorPurchaseItem.ProductStock.MRP = (vendorPurchaseItem.PackageMRP / vendorPurchaseItem.PackageSize);
                //vendorPurchaseItem.Total += vendorPurchaseItem.PackagePurchasePrice * vendorPurchaseItem.PackageQty;
                // var productStockExist = await _productStockDataAccess.GetProductStock(vendorPurchaseItem.ProductStock);
                if (vendorPurchaseItem.ProductStock.VAT == null)
                    vendorPurchaseItem.ProductStock.VAT = 0;
                if (vendorPurchaseItem.ProductStock.CST == null)
                    vendorPurchaseItem.ProductStock.CST = 0;
                vendorPurchaseItem.ProductStock.Stock += vendorPurchaseItem.QtyChange;

                vendorPurchaseItem.ProductStock.PurchasePrice = Math.Round(Convert.ToDecimal(vendorPurchaseItem.ProductStock.PurchasePrice), 6);
                vendorPurchaseItem.ProductStock.SellingPrice = Math.Round(Convert.ToDecimal(vendorPurchaseItem.ProductStock.SellingPrice), 6);
                vendorPurchaseItem.ProductStock.MRP = Math.Round(Convert.ToDecimal(vendorPurchaseItem.ProductStock.MRP), 6);

                //vendorPurchaseItem.ProductStock.PackageSize = vendorPurchaseItem.PackageSize;
                //vendorPurchaseItem.ProductStock.PackagePurchasePrice = vendorPurchaseItem.PackagePurchasePrice;
                //vendorPurchaseItem.ProductStock.PurchasePrice = vendorPurchaseItem.PurchasePrice;

                //if (LocationType == 1)
                //{
                //    vendorPurchaseItem.ProductStock.Igst = vendorPurchaseItem.ProductStock.GstTotal;
                //}
                //if (LocationType == 2 || LocationType == null)
                //{
                //    vendorPurchaseItem.ProductStock.Cgst = vendorPurchaseItem.ProductStock.Igst / 2;
                //    vendorPurchaseItem.ProductStock.Sgst = vendorPurchaseItem.ProductStock.Igst / 2;
                //}
                //if (LocationType == 3 || LocationType == 4)
                //{
                //    vendorPurchaseItem.ProductStock.Cgst = 0;
                //    vendorPurchaseItem.ProductStock.Sgst = 0;
                //    vendorPurchaseItem.ProductStock.Igst = 0;
                //    vendorPurchaseItem.ProductStock.GstTotal = 0;
                //}
                vendorPurchaseItem.ProductStock.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);
                var productStockItem = await _productStockDataAccess.UpdateProductStock(vendorPurchaseItem.ProductStock);

                //Added By Sabarish For UpdateProductGSt 
                vendorPurchaseItem.ProductStock = productStockItem;

                /*Enabled by Poongodi To Upade rack number to the instance table*/
                /*BoxNo included by settu to update in Product Instance table*/
                //if ((vendorPurchaseItem.ProductStock.Product.RackNo != null && vendorPurchaseItem.ProductStock.Product.RackNo.Trim() != "") ||
                //    (vendorPurchaseItem.ProductStock.Product.BoxNo != null && vendorPurchaseItem.ProductStock.Product.BoxNo.Trim() != ""))
                //{
                Product productValue = new Product();
                productValue.Id = vendorPurchaseItem.ProductStock.ProductId;
                productValue.InstanceId = vendorPurchaseItem.ProductStock.InstanceId;
                productValue.AccountId = vendorPurchaseItem.ProductStock.AccountId;
                productValue.RackNo = vendorPurchaseItem.ProductStock.Product.RackNo;
                productValue.HsnCode = vendorPurchaseItem.ProductStock.Product.HsnCode;
                productValue.BoxNo = vendorPurchaseItem.ProductStock.Product.BoxNo;
                productValue.UpdatedBy = vendorPurchaseItem.UpdatedBy;
                productValue.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(),
                    vendorPurchaseItem.WriteExecutionQuery);
                await _productDataAccess.UpdateProductRackNo(productValue);
                //}
                vendorPurchaseItem.ProductStockId = productStockItem.Id;
                vendorPurchaseItem.VatValue = vendorPurchaseItem.VATValue;
                if (vendorPurchaseItem.WriteExecutionQuery)
                {
                    WriteUpdateExecutionQuery(vendorPurchaseItem, VendorPurchaseItemTable.Table);
                }
                else
                {
                    await Update(vendorPurchaseItem, VendorPurchaseItemTable.Table);
                }
                /* Update GST Field value to Product Added by Poongodi on 25/07/2017 */
                if (Convert.ToString(vendorPurchaseItem.ProductStock.ProductId) != "")
                {
                    vendorPurchaseItem.ProductStock.Product.Id = vendorPurchaseItem.ProductStock.ProductId;
                    vendorPurchaseItem.ProductStock.Product.HsnCode = Convert.ToString(vendorPurchaseItem.ProductStock.HsnCode);
                    vendorPurchaseItem.ProductStock.Product.Igst = vendorPurchaseItem.ProductStock.GstTotal;
                    vendorPurchaseItem.ProductStock.Product.Cgst = vendorPurchaseItem.ProductStock.GstTotal / 2;
                    vendorPurchaseItem.ProductStock.Product.GstTotal = vendorPurchaseItem.ProductStock.GstTotal;
                    vendorPurchaseItem.ProductStock.Product.Sgst = vendorPurchaseItem.ProductStock.GstTotal / 2;
                    vendorPurchaseItem.ProductStock.Product.SetExecutionQuery(vendorPurchaseItem.GetExecutionQuery(), vendorPurchaseItem.WriteExecutionQuery);
                    var result = await _productDataAccess.UpdateProductGSTFromStock(vendorPurchaseItem.ProductStock.Product, true, true, true, true, true);
                    var result1 = await _productDataAccess.ProductStockUpdate(vendorPurchaseItem.ProductStock.ProductId, Convert.ToDecimal(vendorPurchaseItem.ProductStock.GstTotal), vendorPurchaseItem.CreatedBy, vendorPurchaseItem.AccountId);
                }
            }
            return itemList;
        }
        public async Task<IEnumerable<VendorPurchaseReturn>> VendorPurchaseReturnList(IEnumerable<VendorPurchase> data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnTable.Table, OperationType.Select);
            qb.AddColumn(VendorReturnTable.IdColumn, VendorReturnTable.ReturnNoColumn, VendorReturnTable.ReturnDateColumn, VendorReturnTable.ReasonColumn, VendorReturnTable.CreatedAtColumn);
            qb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorReturnTable.VendorPurchaseIdColumn);
            qb.JoinBuilder.AddJoinColumn(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorPurchaseTable.VendorIdColumn, VendorPurchaseTable.InvoiceNoColumn, VendorPurchaseTable.GoodsRcvNoColumn);
            qb.JoinBuilder.Join(HQueUserTable.Table, HQueUserTable.IdColumn, VendorReturnTable.CreatedByColumn);
            qb.JoinBuilder.AddJoinColumn(HQueUserTable.Table, HQueUserTable.NameColumn);
            var i = 1;
            foreach (var vendorPurchase in data)
            {
                var paramName = $"s{i++}";
                var col = new CriteriaColumn(VendorReturnTable.VendorPurchaseIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(col);
                qb.ConditionBuilder.Or(cc);
                qb.Parameters.Add(paramName, vendorPurchase.Id);
            }
            var list = new List<VendorPurchaseReturn>();
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var vendorPurchaseReturn = new VendorPurchaseReturn
                    {
                        ReturnNo = reader[VendorReturnTable.ReturnNoColumn.ColumnName].ToString(),
                        ReturnDate = (DateTime)reader[VendorReturnTable.ReturnDateColumn.ColumnName],
                        Id = reader[VendorReturnTable.IdColumn.ColumnName].ToString(),
                        Reason = reader[VendorReturnTable.ReasonColumn.ColumnName].ToString(),
                        CreatedAt = (DateTime)reader[VendorReturnTable.CreatedAtColumn.ColumnName],
                    };
                    vendorPurchaseReturn.VendorPurchaseId = reader[VendorPurchaseTable.IdColumn.FullColumnName].ToString();
                    vendorPurchaseReturn.CreatedBy = reader[HQueUserTable.NameColumn.FullColumnName].ToString();
                    list.Add(vendorPurchaseReturn);
                }
            }
            var vp = await SqlQueryReturnList(list);
            var vendorReturn = list.Select(x =>
            {
                x.VendorPurchaseReturnItem = vp.Where(y => y.VendorReturnId == x.Id).Select(z => z).ToList();
                return x;
            });
            return vendorReturn;
        }
        //public async Task<IEnumerable<VendorPurchaseReturn>> VendorPurchaseReturnListNew(IEnumerable<VendorPurchase> data)
        //{
        //    Dictionary<string, object> parmsAudit = new Dictionary<string, object>();
        //    Dictionary<string, object> parmsPurchaseReturnId = new Dictionary<string, object>();
        //    var VendorPurchaseId = string.Join(",", data.Select(x => x.Id).ToList());
        //    parmsAudit.Add("VendorPurchaseId", VendorPurchaseId);
        //    var resultReturnList = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryReturnList", parmsAudit);
        //    var purchaseReturn = resultReturnList.Select(x =>
        //    {
        //        var vendorPurchaseReturn = new VendorPurchaseReturn
        //        {
        //            ReturnNo = x.ReturnNo as System.String ?? "",
        //            ReturnDate = x.ReturnDate,
        //            Id = x.Id as System.String ?? "",
        //            Reason = x.Reason as System.String ?? "",
        //            CreatedAt = x.CreatedAt as DateTime? ?? DateTime.MinValue,
        //            CreatedBy = x.CreatedBy as System.String ?? "",
        //            VendorPurchase =
        //            {
        //                 Id = x.VendorPurchaseId as System.String ?? "",
        //            }
        //        };
        //        return vendorPurchaseReturn;
        //    });
        //    var VendorReturnId = string.Join(",", resultReturnList.Select(x => x.Id).ToList());
        //    parmsPurchaseReturnId.Add("VendorReturnId", VendorReturnId);
        //    var resultReturnItemList = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetPurchaseHistoryReturnItemList", parmsPurchaseReturnId);
        //    var purchaseReturnItem = resultReturnList.Select(x =>
        //    {
        //        var vendorPurchaseReturnItem = new VendorPurchaseReturnItem
        //        {
        //            ProductStockId = x.ProductStockId as System.String ?? "",
        //            VendorReturnId = x.VendorReturnId as System.String ?? "",
        //            Quantity = x.Quantity as decimal? ?? 0,
        //            ReturnPurchasePrice = x.ReturnPurchasePrice as decimal? ?? 0,
        //            ProductStock =
        //            {
        //                SellingPrice=x.SellingPrice as decimal? ?? 0,
        //                VAT=x.VAT as decimal? ?? 0,
        //                BatchNo=x.BatchNo as System.String ?? "",
        //                CST=x.CST as decimal? ?? 0,
        //                ExpireDate=x.ExpireDate as DateTime? ?? DateTime.MinValue,
        //                Product =
        //                {
        //                    Name=x.ProductName as System.String ?? "",
        //                }
        //            },
        //            VendorPurchaseItem =
        //            {
        //                PurchasePrice=x.PurchasePrice as decimal? ?? 0,
        //                PackageQty=x.PackageQty as decimal? ?? 0,
        //                FreeQty=x.FreeQty as int? ?? 0,
        //                Discount=x.Discount as decimal? ?? 0,
        //                PackageSize=x.PackageSize as decimal? ?? 0,
        //            },
        //        };
        //        return vendorPurchaseReturnItem;
        //    });
        //    var vendorReturn = resultReturnList.Select(x =>
        //    {
        //        x.VendorPurchaseReturnItem = resultReturnItemList.Where(y => y.VendorReturnId == x.Id).Select(z => z).ToList();
        //        return x;
        //    });
        //    return vendorReturn;
        //}
        public async Task<List<VendorPurchaseReturnItem>> SqlQueryReturnList(IEnumerable<VendorPurchaseReturn> data)
        {
            var vriqb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            vriqb.AddColumn(VendorReturnItemTable.ProductStockIdColumn, VendorReturnItemTable.QuantityColumn, VendorReturnItemTable.VendorReturnIdColumn, VendorReturnItemTable.ReturnPurchasePriceColumn);
            //changed join condition for accommadating temp stock on 04/11/2016
            vriqb.JoinBuilder.Join(VendorReturnTable.Table, VendorReturnTable.IdColumn, VendorReturnItemTable.VendorReturnIdColumn);
            vriqb.JoinBuilder.Join(VendorPurchaseTable.Table, VendorPurchaseTable.IdColumn, VendorReturnTable.VendorPurchaseIdColumn);
            vriqb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            vriqb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.SellingPriceColumn, ProductStockTable.VATColumn, ProductStockTable.CSTColumn, ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn);
            vriqb.JoinBuilder.Join(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.ProductStockIdColumn, ProductStockTable.IdColumn);
            vriqb.JoinBuilder.AddJoinColumn(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.PurchasePriceColumn, VendorPurchaseItemTable.PackageQtyColumn, VendorPurchaseItemTable.FreeQtyColumn, VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.PackageSizeColumn);
            vriqb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            vriqb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
            var i = 1;
            foreach (var vendorReturn in data)
            {
                var paramName = $"s{i++}";
                var caol = new CriteriaColumn(VendorReturnItemTable.VendorReturnIdColumn, $"@{paramName}");
                var cc = new CustomCriteria(caol);
                vriqb.ConditionBuilder.Or(cc);
                vriqb.Parameters.Add(paramName, vendorReturn.Id);
            }
            var list = new List<VendorPurchaseReturnItem>();
            using (var reader = await QueryExecuter.QueryAsyc(vriqb))
            {
                while (reader.Read())
                {
                    var vendorReturnItem = new VendorPurchaseReturnItem
                    {
                        ProductStockId = reader[VendorReturnItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        VendorReturnId = reader[VendorReturnItemTable.VendorReturnIdColumn.ColumnName].ToString(),
                        Quantity = reader[VendorReturnItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        ProductStock =
                        {
                            SellingPrice = (decimal) reader[ProductStockTable.SellingPriceColumn.FullColumnName],
                            Product = {Name = reader[ProductTable.NameColumn.FullColumnName].ToString()},
                            VAT = reader[ProductStockTable.VATColumn.FullColumnName] as decimal? ??0,
                            BatchNo = reader[ProductStockTable.BatchNoColumn.FullColumnName].ToString(),
                            CST = reader[ProductStockTable.CSTColumn.FullColumnName] as decimal? ??0,
                            ExpireDate = (DateTime)reader[ProductStockTable.ExpireDateColumn.FullColumnName]
                        },
                        VendorPurchaseItem = {
                            PurchasePrice = reader[VendorPurchaseItemTable.PurchasePriceColumn.FullColumnName] as decimal? ??0,
                            PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.FullColumnName] as decimal? ??0,
                            FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.FullColumnName] as int? ??0,
                            Discount = reader[VendorPurchaseItemTable.DiscountColumn.FullColumnName] as decimal? ??0,
                            PackageSize = reader[VendorPurchaseItemTable.PackageSizeColumn.FullColumnName] as decimal? ??0
                       },
                        ReturnPurchasePrice = reader[VendorReturnItemTable.ReturnPurchasePriceColumn.ColumnName] as decimal? ?? 0,
                    };
                    list.Add(vendorReturnItem);
                }
            }
            return list;
        }
        public async Task<int> GetGRNumber_Old(VendorPurchase v)
        {
            //  var query = $@"SELECT case when max(goodsRcvNo) > count(id) then max(goodsRcvNo) else count(id) end FROM VendorPurchase WHERE instanceId = '{v.InstanceId}'";
            //var query = $@"SELECT ISNULL(MAX(CONVERT(INT, goodsRcvNo)), 0) +1  FROM VendorPurchase WHERE InstanceId = '{v.InstanceId}'";
            //var qb = QueryBuilderFactory.GetQueryBuilder(query);
            DateTime InvoiceDate = v.InvoiceDate as DateTime? ?? CustomDateTime.IST;
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.VendorPurchase, InvoiceDate, v.InstanceId, v.AccountId, "");
            return Convert.ToInt32(sBillNo.ToString());
        }
        public async Task<VendorPurchase> GetGRNumber(VendorPurchase v)
        {
            DateTime InvoiceDate = v.InvoiceDate as DateTime? ?? CustomDateTime.IST;
            /*Prefix added by Poongodi on 10/10/2017*/
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.VendorPurchase, InvoiceDate, v.InstanceId, v.AccountId, "", v.Prefix);
            PurchaseSettings vp = await GetBillSeriesType(v.AccountId, v.InstanceId);
            if (vp.BillSeriesType == 2 && vp.CustomSeriesName != null)
            {
                v.BillSeries = vp.CustomSeriesName;
            }
            v.GoodsRcvNo = sBillNo;
            return v;
        }
        public async Task<VendorPurchase> GetGRNumber(VendorPurchase v, SqlConnection con, SqlTransaction transaction)
        {
            DateTime InvoiceDate = v.InvoiceDate as DateTime? ?? CustomDateTime.IST;
            /*Prefix added by Poongodi on 10/10/2017*/
            string sBillNo = await _HelperDataAccess.GetNumber(Helpers.HelperDataAccess.TableName.VendorPurchase, InvoiceDate, v.InstanceId, v.AccountId, "", v.Prefix, con, transaction);
            PurchaseSettings vp = await GetBillSeriesType(v.AccountId, v.InstanceId, con, transaction);
            if (vp.BillSeriesType == 2 && vp.CustomSeriesName != null)
            {
                v.BillSeries = vp.CustomSeriesName;
            }
            v.GoodsRcvNo = sBillNo;
            return v;
        }
        //finyear method added by Poongodi on 25/03/2017
        public async Task<string> GetFinYear(VendorPurchase v)
        {
            DateTime InvoiceDate = v.InvoiceDate as DateTime? ?? CustomDateTime.IST;
            string sFinyr = await _HelperDataAccess.GetFinyear(InvoiceDate, v.InstanceId, v.AccountId);
            return Convert.ToString(sFinyr);
        }
        public async Task<string> CheckUniqueInvoice(VendorPurchase data)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseTable.Table, OperationType.Count);
            //qb.ConditionBuilder.And(VendorPurchaseTable.VendorIdColumn, data.VendorId);
            //qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceNoColumn, data.InvoiceNo);
            //int count = Convert.ToInt32(await SingleValueAsyc(qb));
            //return count == 0;            
            return await CheckInvoiceExist(data, data.InstanceId);
        }
        public async Task<string> CheckInvoiceExist(VendorPurchase data, string instanceid)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("instanceid", instanceid);
            parms.Add("invoiceno", data.InvoiceNo);
            parms.Add("vendorid", data.VendorId);
            parms.Add("purchaseId", data.Id);
            parms.Add("BillDate", data.InvoiceDate);
            parms.Add("Accountid", data.AccountId);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_CheckInvoiceExist", parms);
            var res = result.Select(item =>
            {
                var obj = new VendorPurchase()
                {
                    GoodsRcvNo = item.GoodsRcvNo //PurchaseCount = item.count
                };
                return obj;
            }).FirstOrDefault();
            if (res != null)
                return res.GoodsRcvNo;
            else
                return null;
        }
        public async Task<VendorPurchase> GetAlternateName(VendorPurchase vp)
        {
            foreach (VendorPurchaseItem vpi in vp.VendorPurchaseItem)
            {
                if (vpi.ProductStock.ProductId == null)
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(AlternateVendorProductTable.Table, OperationType.Select);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.VendorIdColumn, vp.VendorId);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.AlternateProductNameColumn, vpi.ProductStock.Product.Name);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.AccountIdColumn, vp.AccountId);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.InstanceIdColumn, vp.InstanceId);
                    qb.AddColumn(AlternateVendorProductTable.IdColumn, AlternateVendorProductTable.ProductIdColumn);
                    qb.JoinBuilder.Join(ProductTable.Table, AlternateVendorProductTable.ProductIdColumn, ProductTable.IdColumn);
                    qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.NameColumn);
                    using (var reader = await QueryExecuter.QueryAsyc(qb))
                    {
                        while (reader.Read())
                        {
                            vpi.ProductStock.ProductId = reader[AlternateVendorProductTable.ProductIdColumn.ColumnName].ToString();
                            vpi.ProductStock.Product.Name = reader[ProductTable.NameColumn.FullColumnName].ToString();
                        }
                    }
                }
                //Added by Sarubala on 29-05-2018 to remove ' in units/strips - start

                if (vpi.AlternatePackageSize != null)
                {
                    string input;
                    if (vpi.AlternatePackageSize.Contains("'"))
                    {
                        input = vpi.AlternatePackageSize.Replace("'", " ");
                        vpi.AlternatePackageSize = input;
                    }
                    if (vpi.AlternatePackageSize == "-")
                    {
                        vpi.AlternatePackageSize = "";
                    }
                }

                //Added by Sarubala on 29-05-2018 to remove ' in units/strips - end
                if (vpi.ProductStock.PackageSize == null && vpi.AlternatePackageSize != null)
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(AlternateVendorProductTable.Table, OperationType.Select);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.VendorIdColumn, vp.VendorId);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.AlternatePackageSizeColumn, vpi.AlternatePackageSize);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.AccountIdColumn, vp.AccountId);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.InstanceIdColumn, vp.InstanceId);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.ProductIdColumn, vpi.ProductStock.ProductId);
                    qb.AddColumn(AlternateVendorProductTable.IdColumn, AlternateVendorProductTable.ProductIdColumn, AlternateVendorProductTable.UpdatedPackageSizeColumn);
                    //qb.JoinBuilder.Join(ProductTable.Table, AlternateVendorProductTable.ProductIdColumn, ProductTable.IdColumn);
                    //qb.JoinBuilder.AddJoinColumn(ProductTable.Table, ProductTable.PackageSizeColumn);
                    using (var reader = await QueryExecuter.QueryAsyc(qb))
                    {
                        while (reader.Read())
                        {
                            vpi.ProductStock.Product.PackageSize = Convert.ToInt32(reader[AlternateVendorProductTable.UpdatedPackageSizeColumn.ColumnName]);
                            vpi.ProductStock.PackageSize = vpi.ProductStock.Product.PackageSize;
                        }
                    }
                }
                else if (vpi.PackageSize != null && vpi.AlternatePackageSize != null)
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(AlternateVendorProductTable.Table, OperationType.Select);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.VendorIdColumn, vp.VendorId);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.AccountIdColumn, vp.AccountId);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.InstanceIdColumn, vp.InstanceId);
                    qb.ConditionBuilder.And(AlternateVendorProductTable.ProductIdColumn, vpi.ProductStock.ProductId);
                    qb.AddColumn(AlternateVendorProductTable.IdColumn, AlternateVendorProductTable.ProductIdColumn, AlternateVendorProductTable.UpdatedPackageSizeColumn);
                    using (var reader = await QueryExecuter.QueryAsyc(qb))
                    {
                        while (reader.Read())
                        {
                            vpi.PackageSize = reader[AlternateVendorProductTable.UpdatedPackageSizeColumn.ColumnName] as decimal? ?? null;
                        }
                    }
                }
            }
            return vp;
        }
        public async Task<PurchaseDetails> getPurchaseDetails(ProductStock ps, string name, string instanceid)
        {
            //var qb = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Select);
            //qb.ConditionBuilder.And(ProductStockTable.ProductIdColumn, ps.ProductId);
            //qb.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, ps.InstanceId);
            //qb.ConditionBuilder.And(ProductStockTable.AccountIdColumn, ps.AccountId);

            ////qb.AddColumn(ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.VATColumn, ProductStockTable.HsnCodeColumn, ProductStockTable.IgstColumn, ProductStockTable.CgstColumn, ProductStockTable.SgstColumn, ProductStockTable.GstTotalColumn, ProductStockTable.CSTColumn, ProductStockTable.PurchasePriceColumn, ProductTable.EancodeColumn);
            //qb.AddColumn(ProductStockTable.BatchNoColumn, ProductStockTable.ExpireDateColumn, ProductStockTable.VATColumn, ProductTable.HsnCodeColumn, ProductTable.IgstColumn, ProductTable.CgstColumn, ProductTable.SgstColumn, ProductTable.GstTotalColumn, ProductStockTable.CSTColumn, ProductStockTable.PurchasePriceColumn, ProductTable.EancodeColumn);

            //qb.JoinBuilder.LeftJoin(VendorPurchaseItemTable.Table, ProductStockTable.IdColumn, VendorPurchaseItemTable.ProductStockIdColumn);
            //qb.JoinBuilder.AddJoinColumn(VendorPurchaseItemTable.Table, VendorPurchaseItemTable.PackageSizeColumn, VendorPurchaseItemTable.PackageQtyColumn,
            //        VendorPurchaseItemTable.PackagePurchasePriceColumn, VendorPurchaseItemTable.PackageSellingPriceColumn, VendorPurchaseItemTable.PackageMRPColumn,
            //        VendorPurchaseItemTable.DiscountColumn, VendorPurchaseItemTable.FreeQtyColumn);
            //qb.JoinBuilder.Join(ProductTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
            ///*Product Instance sectionadded by POongodi on21/03/2017*/
            //qb.JoinBuilder.LeftJoins(ProductInstanceTable.Table, ProductInstanceTable.ProductIdColumn, ProductStockTable.ProductIdColumn, ProductInstanceTable.InstanceIdColumn, ProductStockTable.InstanceIdColumn);
            //qb.JoinBuilder.AddJoinColumn(ProductInstanceTable.Table, ProductInstanceTable.RackNoColumn, ProductInstanceTable.BoxNoColumn); /*BoxNo included by settu on 19-05-2017*/
            //var statusCondition = new CustomCriteria(ProductStockTable.StatusColumn, CriteriaCondition.IsNull);
            //var cc = new CriteriaColumn(ProductStockTable.StatusColumn, "1", CriteriaEquation.Equal);
            //var col = new CustomCriteria(cc, statusCondition, CriteriaCondition.Or);
            //qb.ConditionBuilder.And(col);
            //qb.SelectBuilder.SortBy(ProductStockTable.CreatedAtColumn);

            //Added by Sarubala on 26-09-17
            bool? bLowPrice = false;
            PurchaseSettings settings = await getTaxtypedata(ps.AccountId, ps.InstanceId);
            /*Filter condition, Join and VendorpuRCHASE added by Poongodi on 27/09/2017*/
            if (settings != null && settings.IsSortByLowestPrice != null)
                bLowPrice = settings.IsSortByLowestPrice;

            //string sortfilter = "ProductStock.CreatedAt desc";
            //string sQry = "";
            //if (bLowPrice==true)
            //{
            //    sortfilter = "ProductStock.PurchasePrice asc";
            //    sQry = " and isnull(ProductStock.Taxrefno,0) = 1 and isnull(ProductStock.PurchasePrice,0) > 0 ";
            //}

            //var query = $"SELECT Top 1  ProductStock.BatchNo,ProductStock.ExpireDate,ProductStock.VAT,ISNULL(Product.HsnCode, ProductStock.HsnCode) HsnCode, ISNULL(Product.Igst, ProductStock.Igst) Igst, ISNULL(Product.Cgst, ProductStock.Cgst) Cgst,ISNULL(Product.Sgst, ProductStock.Sgst) Sgst, ISNULL(Product.GstTotal, ProductStock.GstTotal) GstTotal,ProductStock.CST,ProductStock.PurchasePrice,ProductStock.TaxRefNo,Product.Eancode,isnull(VendorPurchaseItem.PackageSize,Product.PackageSize) AS[VendorPurchaseItem.PackageSize], VendorPurchaseItem.PackageQty AS[VendorPurchaseItem.PackageQty], VendorPurchaseItem.PackagePurchasePrice AS[VendorPurchaseItem.PackagePurchasePrice], VendorPurchaseItem.PackageSellingPrice AS[VendorPurchaseItem.PackageSellingPrice], VendorPurchaseItem.PackageMRP AS[VendorPurchaseItem.PackageMRP], VendorPurchaseItem.Discount AS[VendorPurchaseItem.Discount], VendorPurchaseItem.FreeQty AS[VendorPurchaseItem.FreeQty], ProductInstance.RackNo AS[ProductInstance.RackNo], ProductInstance.BoxNo AS[ProductInstance.BoxNo] FROM Product Left Join ProductStock On Product.Id = ProductStock.ProductId AND ProductStock.InstanceId = '{ps.InstanceId}' AND(ProductStock.Status = 1 Or(ProductStock.Status IS NULL)) INNER JOIN VendorPurchaseItem ON ProductStock.Id = VendorPurchaseItem.ProductStockId INNER JOIN VENDORPURCHASE VP ON VP.ID =VendorPurchaseItem.VendorPurchaseId LEFT JOIN ProductInstance ON ProductInstance.ProductId = ProductStock.ProductId And ProductInstance.InstanceId = ProductStock.InstanceId  WHERE Product.Id = '{ps.ProductId}'  AND Product.AccountId = '{ps.AccountId}'"+ sQry +"  ORDER BY "+ sortfilter ;
            //var query = $@"SELECT Top 1 ProductStock.BatchNo,ProductStock.ExpireDate,ProductStock.VAT,ISNULL(Product.HsnCode, ProductStock.HsnCode) HsnCode, ISNULL(Product.Igst, ProductStock.Igst) Igst, 
            //        ISNULL(Product.Cgst, ProductStock.Cgst) Cgst,ISNULL(Product.Sgst, ProductStock.Sgst) Sgst, ISNULL(Product.GstTotal, ProductStock.GstTotal) GstTotal,ProductStock.CST,
            //        ProductStock.PurchasePrice,ProductStock.TaxRefNo,Product.Eancode,isnull(ProductStock.PackageSize, Product.PackageSize) AS[VendorPurchaseItem.PackageSize], 
            //        ProductStock.PackageQty AS[VendorPurchaseItem.PackageQty], ProductStock.PackagePurchasePrice AS[VendorPurchaseItem.PackagePurchasePrice],
            //        ProductStock.PackageSellingPrice AS[VendorPurchaseItem.PackageSellingPrice], ProductStock.PackageMRP AS[VendorPurchaseItem.PackageMRP],
            //        ProductStock.Discount AS[VendorPurchaseItem.Discount], ProductStock.FreeQty AS[VendorPurchaseItem.FreeQty], ProductInstance.RackNo AS[ProductInstance.RackNo],
            //        ProductInstance.BoxNo AS[ProductInstance.BoxNo] FROM Product LEFT JOIN
            //        (SELECT ProductStock.Id, ProductStock.ProductId, ProductStock.Sgst, ProductStock.GstTotal, ProductStock.CST, ProductStock.BatchNo, ProductStock.ExpireDate, ProductStock.VAT,
            //        ProductStock.HsnCode, ProductStock.Igst, ProductStock.Cgst, ProductStock.PurchasePrice, VendorPurchaseItem.PackageSize, VendorPurchaseItem.PackageQty,
            //        VendorPurchaseItem.PackagePurchasePrice, VendorPurchaseItem.Discount, VendorPurchaseItem.FreeQty, VendorPurchaseItem.PackageSellingPrice, VendorPurchaseItem.PackageMRP,
            //        vendorpurchaseitem.CreatedAt, VP.TaxRefNo FROM ProductStock INNER JOIN VendorPurchaseItem ON ProductStock.Id = VendorPurchaseItem.ProductStockId
            //        INNER JOIN VendorPurchase VP ON VP.Id = VendorPurchaseItem.VendorPurchaseId WHERE productstock.AccountId = '{ps.AccountId}' AND productstock.InstanceId = '{ps.InstanceId}'
            //        AND ProductId = '{ps.ProductId}' AND (ProductStock.Status = 1 Or(ProductStock.Status IS NULL))) ProductStock On Product.Id = ProductStock.ProductId
            //        LEFT JOIN (Select * FROM ProductInstance(NOLOCK) WHERE AccountId = '{ps.AccountId}' AND InstanceId = '{ps.InstanceId}' and ProductId = '{ps.ProductId}') ProductInstance
            //        ON ProductInstance.ProductId = Product.id WHERE Product.AccountId = '{ps.AccountId}' AND Product.Id = '{ps.ProductId}' " + sQry + " ORDER BY " + sortfilter;

            //var qb = QueryBuilderFactory.GetQueryBuilder(query);

            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", ps.AccountId);
            parms.Add("InstanceId", ps.InstanceId);
            parms.Add("ProductId", ps.ProductId);
            parms.Add("SortByPrice", bLowPrice);
            var result = await sqldb.ExecuteProcedureAsync<dynamic>("usp_GetLastPurchaseDetails", parms);

            var item = new PurchaseDetails();
            bool filter1 = bLowPrice == true;//Added by Sarubala on 26-09-17
            item.getPreviousPurchase = await getPreviousPurchase(instanceid, name, filter1);
            if (result.Count() > 0)
            {
                item = result.Select(x =>
                {
                    item.vendorPurchaseItem.ProductStock = new ProductStock
                    {
                        BatchNo = x.BatchNo as string ?? "",
                        VAT = x.VAT as decimal? ?? 0,
                        HsnCode = x.HsnCode as string ?? "",
                        Igst = x.Igst as decimal? ?? 0,
                        Cgst = x.Cgst as decimal? ?? 0,
                        Sgst = x.Sgst as decimal? ?? 0,
                        GstTotal = x.GstTotal as decimal? ?? 0,
                        CST = x.CST as decimal? ?? 0,
                        PurchasePrice = x.PurchasePrice as decimal? ?? 0,
                        TaxRefNo = x.TaxRefNo as int? ?? 0
                    };
                    /*To display the null expiry date where the stock is not available Validation added by Poongodi on 27/06/2017*/
                    if (Convert.ToString(item.vendorPurchaseItem.ProductStock.BatchNo) != "")
                        item.vendorPurchaseItem.ProductStock.ExpireDate = x.ExpireDate as DateTime? ?? DateTime.MinValue;

                    item.vendorPurchaseItem.ProductStock.Product.RackNo = x.RackNo as string ?? "";
                    item.vendorPurchaseItem.ProductStock.Product.BoxNo = x.BoxNo as string ?? "";
                    item.vendorPurchaseItem.Discount = x.Discount as decimal? ?? 0;
                    item.vendorPurchaseItem.FreeQty = x.FreeQty as decimal? ?? 0;
                    item.vendorPurchaseItem.PackageSize = x.PackageSize as decimal? ?? 0;
                    item.vendorPurchaseItem.PackageQty = x.PackageQty as decimal? ?? 0;
                    item.vendorPurchaseItem.PackagePurchasePrice = x.PackagePurchasePrice as decimal? ?? 0;
                    item.vendorPurchaseItem.PackageSellingPrice = x.PackageSellingPrice as decimal? ?? 0;
                    item.vendorPurchaseItem.PackageMRP = x.PackageMRP as decimal? ?? 0;
                    item.vendorPurchaseItem.ProductStock.Product.Eancode = x.Eancode as string ?? "";
                    return item;
                }).FirstOrDefault();
            }

            if (item.vendorPurchaseItem.ProductStock.VAT == null || item.vendorPurchaseItem.PackageSize == null)
            {
                var data = await _productDataAccess.GetProductById(ps.ProductId);
                if (data != null)
                {
                    item.vendorPurchaseItem.ProductStock.VAT = data.VAT;
                    item.vendorPurchaseItem.PackageSize = data.PackageSize;
                }
            }

            return item;
        }
        public async Task<VendorPurchaseItem> getPurchasePrice(string id)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Select);
            qb.ConditionBuilder.And(VendorPurchaseItemTable.ProductStockIdColumn, id);
            qb.AddColumn(VendorPurchaseItemTable.PurchasePriceColumn);
            qb.SelectBuilder.MakeDistinct = true;
            return (await List<VendorPurchaseItem>(qb)).FirstOrDefault();
        }
        public async Task<List<VendorPurchaseItem>> getPreviousPurchase(string instanceid, string name, bool isSortFilter = false)
        {
            /*Prefix Added by Poongodi on 15/06/2017*/
            name = name.Replace("'", "''");

            //Added by Sarubala on 26-09-17
            string sortfilter = "vpi.CreatedAt desc";
            string sQry = "";
            /*Filter condition added by Poongodi on 27/09/2017*/
            if (isSortFilter == true)
            {
                sortfilter = "ps.PurchasePrice asc";
                sQry = " and isnull(VP.Taxrefno,0) =1 and isnull(ps.PurchasePrice,0) > 0 ";
            }

            string query = $@"select top 5 vpi.VendorPurchaseId,
                                           vpi.Quantity,
                                           vpi.ProductStockId,
										   vpi.FreeQty,
										   vpi.Discount,
                                           vpi.SchemeDiscountPerc,
                                           vpi.MarkupPerc,
										   vpi.PackageQty,
										   vpi.PackagePurchasePrice,
										   vpi.PackageSellingPrice,
                                           vpi.PackageMRP,
                                           vp.InvoiceDate,
                                           ps.ProductId,
                                           ps.VendorId,
                                           ps.BatchNo,
                                           ps.VAT,
                                           ps.PurchasePrice,										   
                                           ps.cst,
                                           ps.ExpireDate,
                                           p.Name as productname,
										   p.Manufacturer,
                                           v.Name as vendorname,
                                           v.Code as vendorcode,
                                           v.Mobile as vendormobile,
										   vp.GoodsRcvNo,
                                           isnull(vp.prefix,'') + isnull(vp.BillSeries ,'') [BillSeries]
                                               from VendorPurchaseItem as vpi inner join ProductStock as ps on vpi.ProductStockId = ps.Id  
                                                          inner join Product as p on ps.ProductId = p.Id 
		                                                  inner join VendorPurchase as vp on vpi.VendorPurchaseId = vp.id
                                                          left join Vendor as v on vp.VendorId = v.Id
                                                             where vpi.InstanceId='{instanceid}' and p.Name = '{name}' and (ps.Status is null or ps.Status = 1) and isnull(vp.cancelstatus,0)  in (0) " + sQry + " order by " + sortfilter;
            var list = new List<VendorPurchaseItem>();
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            using (var reader = await QueryExecuter.QueryAsyc(qb))
            {
                while (reader.Read())
                {
                    var VendorPurchaseItem = new VendorPurchaseItem
                    {
                        BuyPurchase = {
                            GoodsRcvNo = reader[VendorPurchaseTable.GoodsRcvNoColumn.ColumnName].ToString(),
                            BillSeries = reader[VendorPurchaseTable.BillSeriesColumn.ColumnName].ToString(),
                        },
                        VendorPurchaseId = reader[VendorPurchaseItemTable.VendorPurchaseIdColumn.ColumnName].ToString(),
                        Quantity = reader[VendorPurchaseItemTable.QuantityColumn.ColumnName] as decimal? ?? 0,
                        ProductStockId = reader[VendorPurchaseItemTable.ProductStockIdColumn.ColumnName].ToString(),
                        FreeQty = reader[VendorPurchaseItemTable.FreeQtyColumn.ColumnName] as decimal? ?? 0,
                        Discount = reader[VendorPurchaseItemTable.DiscountColumn.ColumnName] as decimal? ?? 0,
                        SchemeDiscountPerc = reader[VendorPurchaseItemTable.SchemeDiscountPercColumn.ColumnName] as decimal? ?? 0,
                        MarkupPerc = reader[VendorPurchaseItemTable.MarkupPercColumn.ColumnName] as decimal? ?? 0,
                        PackageQty = reader[VendorPurchaseItemTable.PackageQtyColumn.ColumnName] as decimal? ?? 0,
                        PackagePurchasePrice = reader[VendorPurchaseItemTable.PackagePurchasePriceColumn.ColumnName] as decimal? ?? 0,
                        PackageSellingPrice = reader[VendorPurchaseItemTable.PackageSellingPriceColumn.ColumnName] as decimal? ?? 0,
                        PackageMRP = reader[VendorPurchaseItemTable.PackageMRPColumn.ColumnName] as decimal? ?? 0,
                        //CreatedAt = reader[VendorPurchaseItemTable.CreatedAtColumn.ColumnName] as DateTime? ?? DateTime.MinValue,                
                        CreatedAt = reader[VendorPurchaseTable.InvoiceDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                        ProductStock = {
                            ProductId = reader[ProductStockTable.ProductIdColumn.ColumnName].ToString(),
                            VendorId = reader[ProductStockTable.VendorIdColumn.ColumnName].ToString(),
                            BatchNo =  reader[ProductStockTable.BatchNoColumn.ColumnName].ToString(),
                            VAT = reader[ProductStockTable.VATColumn.ColumnName] as decimal? ?? 0,
                            PurchasePrice = reader[ProductStockTable.PurchasePriceColumn.ColumnName] as decimal? ?? 0,
                            CST = reader[ProductStockTable.CSTColumn.ColumnName] as decimal? ?? 0,
                            ExpireDate = reader[ProductStockTable.ExpireDateColumn.ColumnName] as DateTime? ?? DateTime.MinValue,
                            Product = {
                                Name = reader["productname"].ToString(),
                                Manufacturer = reader[ProductTable.ManufacturerColumn.ColumnName].ToString()
                            },
                            Vendor = {
                                Name = reader["vendorname"].ToString(),
                                Code=  reader["vendorcode"].ToString(),
                                Mobile = reader["vendormobile"].ToString()
                            }
                        }
                    };
                    list.Add(VendorPurchaseItem);
                }
                return list;
            }
        }
        public async Task<int> GetDraftCount(string InstanceId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(DraftVendorPurchaseTable.Table, OperationType.Count);
            qb.ConditionBuilder.And(DraftVendorPurchaseTable.InstanceIdColumn, InstanceId);
            return Convert.ToInt32(await QueryExecuter.SingleValueAsyc(qb));
        }
        // Newly Added Gavaskar 21-02-2017 Start
        public Task<List<Vendor>> VendorList(Vendor vendor)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorTable.Table, OperationType.Select);
            qb = SqlQueryBuilder(vendor, qb);
            qb.SelectBuilder.SortByDesc(VendorTable.CreatedAtColumn);
            // qb.SelectBuilder.MakeDistinct = true;
            return List<Vendor>(qb);
        }
        private static QueryBuilderBase SqlQueryBuilder(Vendor data, QueryBuilderBase qb)
        {
            if (!string.IsNullOrEmpty(data.Id))
            {
                qb.ConditionBuilder.And(VendorTable.IdColumn, data.Id);
            }
            if (!string.IsNullOrEmpty(data.Name))
            {
                qb.ConditionBuilder.And(VendorTable.NameColumn, data.Name, CriteriaEquation.Like, CriteriaLike.Begin);
            }
            if (!string.IsNullOrEmpty(data.Mobile))
            {
                qb.ConditionBuilder.And(VendorTable.MobileColumn, data.Mobile);
            }
            if (!string.IsNullOrEmpty(data.AccountId)) // Altered by Sarubala on 12/09/17
            {
                qb.ConditionBuilder.And(VendorTable.AccountIdColumn, data.AccountId);
            }
            qb.JoinBuilder.Join(TemplatePurchaseChildMasterSettingsTable.Table, TemplatePurchaseChildMasterSettingsTable.VendorIdColumn, VendorTable.IdColumn);
            return qb;
        }
        // Newly Added Gavaskar 21-02-2017 End
        public async Task<Boolean> getOfflineStatus(string accountId, string instanceId)
        {
            return await getOfflineStatusValue(accountId, instanceId);
        }
        //Po cancel method added by Poongodi on 06/03/2017
        public async Task<int> POCancel(string sAccountId, string sInstanceId, string poId, string updatedby)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", sAccountId);
            parms.Add("InstanceId", sInstanceId);
            parms.Add("PurchaseId", poId);
            parms.Add("UpdatedBy", updatedby);
            int nReturn = 2;
            var result = await sqldb.ExecuteProcedureAsync<VendorPurchase>("USP_PO_Cancel", parms);
            if (result != null && result.Count() > 0)
                nReturn = result.FirstOrDefault().POStatus;
            if (nReturn == 0)
            {
                parms.Add("Updatedat", CustomDateTime.IST.ToString("dd-MMM-yyyy HH':'mm':'ss.fff"));
                string sqry = $" UPDATE vendorpurchase  SET cancelstatus = 1, updatedby = @UpdatedBy,  updatedat = @Updatedat WHERE id = @PurchaseId";
                WriteToSyncQueue(new Contract.External.SyncObject() { QueryText = sqry, Id = poId, Parameters = parms });
                //Vendor Purchase Item Deleted Status Included in sync By Poongodi on 04/05/2017
                string sqry1 = $"UPDATE productstock SET stock = stock - vpi.quantity, updatedby =@UpdatedBy,  updatedat =  @Updatedat FROM productstock PS INNER JOIN vendorpurchaseitem VPI ON  VPI.productstockid = Ps.id AND VPI.vendorpurchaseid =  @PurchaseId and isnull(vpi.Status ,0) Not in (2)";
                WriteToSyncQueue(new Contract.External.SyncObject() { QueryText = sqry1, Id = sAccountId, Parameters = parms });
            }
            return nReturn;
        }
        public async Task<string> CheckUniqueEancode(VendorPurchase data)
        {
            string res = "";
            foreach (var vendorPurchaseItem in data.VendorPurchaseItem)
            {
                if (vendorPurchaseItem.ProductStock.Eancode != null && vendorPurchaseItem.ProductStock.Eancode != "")
                {
                    var qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
                    qb.JoinBuilder.LeftJoin(ProductStockTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
                    qb.ConditionBuilder.And(ProductTable.AccountIdColumn, data.AccountId);
                    var eancode = vendorPurchaseItem.ProductStock.Eancode;
                    eancode = "'" + eancode.Replace("'", "''") + "'";
                    var statusCondition = new CriteriaColumn(ProductStockTable.EancodeColumn, eancode, CriteriaEquation.Equal);
                    var cc = new CriteriaColumn(ProductTable.EancodeColumn, eancode, CriteriaEquation.Equal);
                    var col = new CustomCriteria(cc, statusCondition, CriteriaCondition.Or);
                    qb.ConditionBuilder.And(col);
                    if (vendorPurchaseItem.ProductStock.ProductId != null)
                        qb.ConditionBuilder.And(ProductTable.IdColumn, vendorPurchaseItem.ProductStock.ProductId, CriteriaEquation.NotEqual);
                    else if (vendorPurchaseItem.ProductStock.Product.Id != null)
                        qb.ConditionBuilder.And(ProductTable.IdColumn, vendorPurchaseItem.ProductStock.Product.Id, CriteriaEquation.NotEqual);
                    qb.ConditionBuilder.And(ProductTable.NameColumn, vendorPurchaseItem.ProductStock.Product.Name, CriteriaEquation.NotEqual);
                    qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn, ProductTable.EancodeColumn, ProductStockTable.EancodeColumn);
                    qb.SelectBuilder.MakeDistinct = true;
                    var result = await List<Product>(qb);
                    if (result.Count() > 0)
                    {
                        res = "This UPC/EAN Code[" + vendorPurchaseItem.ProductStock.Eancode + "] already exists with another product[" + result.FirstOrDefault().Name + "]. Please check entered UPC/EAN Code.";
                        return res;
                    }
                    else
                    {
                        qb = QueryBuilderFactory.GetQueryBuilder(ProductTable.Table, OperationType.Select);
                        qb.JoinBuilder.Join(ProductStockTable.Table, ProductTable.IdColumn, ProductStockTable.ProductIdColumn);
                        qb.AddColumn(ProductTable.IdColumn, ProductTable.NameColumn);
                        qb.ConditionBuilder.And(ProductTable.AccountIdColumn, data.AccountId);
                        qb.ConditionBuilder.And(ProductStockTable.PurchaseBarcodeColumn, vendorPurchaseItem.ProductStock.Eancode);
                        result = (await List<Product>(qb)).ToList();
                        if (result.Count() > 0)
                        {
                            res = "This UPC/EAN Code[" + vendorPurchaseItem.ProductStock.Eancode + "] already exists as 'Barcode' with this product[" + result.FirstOrDefault().Name + "]. Please check entered UPC/EAN Code and try again.";
                            return res;
                        }
                    }
                }
            }
            return res;
        }
        public async Task<bool> inActiveProduct(VendorPurchaseItem model, string InstanceId, string AccountId)
        {
            await UpdateProductStock(InstanceId, AccountId, model);
            // await VendorPurchaseItem(InstanceId, AccountId, model.Id);
            return true;
        }
        public async Task<bool> UpdateProductStock(string InstanceId, string AccountId, VendorPurchaseItem model)
        {
            var qbUpdate = QueryBuilderFactory.GetQueryBuilder(ProductStockTable.Table, OperationType.Update);
            qbUpdate.ConditionBuilder.And(ProductStockTable.IdColumn, model.ProductStock.Id);
            qbUpdate.ConditionBuilder.And(ProductStockTable.InstanceIdColumn, InstanceId);
            qbUpdate.ConditionBuilder.And(ProductStockTable.AccountIdColumn, AccountId);
            qbUpdate.AddColumn(ProductStockTable.StatusColumn, ProductStockTable.StockColumn);
            qbUpdate.Parameters.Add(ProductStockTable.StatusColumn.ColumnName, model.ProductStock.Status);
            qbUpdate.Parameters.Add(ProductStockTable.StockColumn.ColumnName, model.ProductStock.Stock);
            if (model.WriteExecutionQuery && model.ProductCount <= 1)
            {
                model.AddExecutionQuery(qbUpdate);
                return true;
            }
            else
            {
                return await QueryExecuter.NonQueryAsyc(qbUpdate);
            }
        }
        public async Task<bool> VendorPurchaseItem(string InstanceId, string AccountId, string vendorPurchaseItemId)
        {
            var qbUpdate = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseItemTable.Table, OperationType.Update);
            qbUpdate.ConditionBuilder.And(VendorPurchaseItemTable.IdColumn, vendorPurchaseItemId);
            qbUpdate.ConditionBuilder.And(VendorPurchaseItemTable.InstanceIdColumn, InstanceId);
            qbUpdate.ConditionBuilder.And(VendorPurchaseItemTable.AccountIdColumn, AccountId);
            qbUpdate.AddColumn(VendorPurchaseItemTable.StatusColumn, VendorPurchaseItemTable.UpdatedAtColumn);
            qbUpdate.Parameters.Add(VendorPurchaseItemTable.StatusColumn.ColumnName, 2);
            qbUpdate.Parameters.Add(VendorPurchaseItemTable.UpdatedAtColumn.ColumnName, CustomDateTime.IST);
            return await QueryExecuter.NonQueryAsyc(qbUpdate);
        }
        public async Task<bool> getEnableSelling(string accId, string insId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Select);
            qb.AddColumn(PurchaseSettingsTable.EnableSellingColumn);
            qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, accId);
            qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, insId);
            bool enableSelling = false;
            var purchaseSetting = (await List<PurchaseSettings>(qb)).FirstOrDefault();
            if (purchaseSetting != null)
            {
                enableSelling = Convert.ToBoolean(purchaseSetting.EnableSelling);
            }
            return enableSelling;
        }
        public async Task saveEnableSelling(PurchaseSettings ps)
        {
            var count = await CountAllowDecimalSetting(ps);
            if (count > 0)
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(PurchaseSettingsTable.Table, OperationType.Update);
                qb.AddColumn(PurchaseSettingsTable.EnableSellingColumn, PurchaseSettingsTable.UpdatedAtColumn, PurchaseSettingsTable.UpdatedByColumn);
                qb.Parameters.Add(PurchaseSettingsTable.EnableSellingColumn.ColumnName, ps.EnableSelling);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                qb.Parameters.Add(PurchaseSettingsTable.UpdatedByColumn.ColumnName, ps.UpdatedBy);
                qb.ConditionBuilder.And(PurchaseSettingsTable.AccountIdColumn, ps.AccountId);
                qb.ConditionBuilder.And(PurchaseSettingsTable.InstanceIdColumn, ps.InstanceId);
                await QueryExecuter.NonQueryAsyc(qb);
            }
            else
            {
                await Insert(ps, PurchaseSettingsTable.Table);
            }
        }
        // Added by Gavaskar Purchase With Return
        public VendorPurchase RemoveReturnItems(VendorPurchase data)
        {
            data.VendorReturn = new VendorPurchaseReturn();
            data.VendorReturn.TotalReturnedAmount = 0;
            data.VendorReturn.TotalDiscountValue = 0;
            data.VendorReturn.TotalGstValue = 0;
            foreach (VendorPurchaseItem vpi in data.VendorPurchaseItem)
            {

                if (vpi.VendorReturn == true)
                {
                    VendorPurchaseReturnItem vpri = new VendorPurchaseReturnItem();
                    //decimal priceAfterTax = 0;
                    vpri.Id = vpi.Id != null ? vpi.Id : null;
                    vpri.ProductStock = vpi.ProductStock;
                    vpri.ProductStockId = vpi.ProductStockId;
                    //vpri.Quantity = vpi.Quantity.HasValue ? Math.Abs(vpi.Quantity.Value) * Math.Abs(vpi.PackageSize.Value) : 0;
                    vpri.Quantity = vpi.Quantity;
                    vpri.Total = vpi.Total.HasValue ? Math.Abs(vpi.Total.Value) : 0;
                    vpri.Discount = vpi.Discount;
                    vpri.DiscountValue = vpi.DiscountValue;
                    //vpri.PurchaseQuantity = Math.Abs(vpi.PackageQty.Value) * Math.Abs(vpi.PackageSize.Value);
                    vpri.PurchaseQuantity = vpri.Quantity;
                    //vpri.ReturnedTotal = (Math.Abs(vpi.PackageQty.Value) * Math.Abs(vpi.PackagePurchasePrice.Value)) - vpi.DiscountValue;
                    vpri.ReturnedTotal = vpi.PurchasePrice * vpri.Quantity;
                    //decimal toatlPrice = (decimal)(vpi.PackagePurchasePrice * (vpi.PackageQty));
                    //decimal discount = (decimal)(toatlPrice * vpi.Discount / 100);
                    //priceAfterTax = (decimal)((toatlPrice - discount) + ((toatlPrice - discount) * vpi.ProductStock.GstTotal / 100));
                    //vpri.ReturnPurchasePrice = (Math.Abs(vpi.PackageQty.Value) * Math.Abs(vpi.PackagePurchasePrice.Value)) / Math.Abs(vpi.PackageSize.Value);
                    //vpri.ReturnPurchasePrice = (priceAfterTax / vpi.PackageQty) / vpi.PackageSize;
                    vpri.ReturnPurchasePrice = vpi.PurchasePrice;
                    vpri.CreatedAt = CustomDateTime.IST;
                    vpri.CreatedBy = data.CreatedBy;
                    vpri.UpdatedAt = CustomDateTime.IST;
                    vpri.UpdatedBy = data.UpdatedBy;
                    vpri.GstTotal = vpi.GstTotal;
                    //vpri.Igst = vpi.GstTotal;
                    //vpri.Sgst = vpi.GstTotal / 2;
                    //vpri.Cgst = vpi.GstTotal / 2;
                    vpri.Igst = vpi.Igst;
                    vpri.Sgst = vpi.Sgst;
                    vpri.Cgst = vpi.Cgst;
                    //vpri.GstAmount = vpri.ReturnedTotal * (vpi.GstTotal / 100);
                    vpri.GstAmount = vpi.GstValue;
                    data.VendorReturn.TotalDiscountValue = data.VendorReturn.TotalDiscountValue + vpri.DiscountValue;
                    data.VendorReturn.TotalGstValue = data.VendorReturn.TotalGstValue + vpri.GstAmount;
                    if (vpi.isDeleted)
                    {
                        vpri.IsDeleted = true;
                    }
                    data.VendorReturn.VendorPurchaseReturnItem.Add(vpri);
                    data.VendorReturn.Reason = vpi.reason;
                    data.VendorReturn.PaymentStatus = "Received";
                    data.VendorReturn.RoundOffValue = data.ReturnRoundedValue;
                    data.VendorReturn.TotalReturnedAmount = data.ReturnAmount;
                    data.VendorReturn.IsAlongWithPurchase = true;
                    if (data.VendorReturn.Id == null)
                    {
                        data.VendorReturn.Id = vpi.vendorReturnId != null ? vpi.vendorReturnId : null;
                    }

                }
            }

            var vpItems = data.VendorPurchaseItem.ToList();
            vpItems.RemoveAll(x => x.VendorReturn == true);
            data.VendorPurchaseItem = vpItems;

            return data;
        }
        // Added by Gavaskar Purchase With Return
        public Task<List<VendorPurchaseReturnItem>> VendorPurchaseReturnItemList(string VendorPurchaseId)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorReturnItemTable.Table, OperationType.Select);
            qb.AddColumn(VendorReturnItemTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(VendorReturnTable.Table, VendorReturnTable.IdColumn, VendorReturnItemTable.VendorReturnIdColumn);

            qb.JoinBuilder.AddJoinColumn(ProductStockTable.Table, ProductStockTable.Table.ColumnList.ToArray());

            qb.JoinBuilder.Join(ProductStockTable.Table, ProductStockTable.IdColumn, VendorReturnItemTable.ProductStockIdColumn);
            qb.ConditionBuilder.And(VendorReturnTable.VendorPurchaseIdColumn, VendorPurchaseId);
            qb.ConditionBuilder.And(VendorReturnTable.IsAlongWithPurchaseColumn, 1);

            return List<VendorPurchaseReturnItem>(qb);
        }

        public async Task<List<TaxValues>> GetTaxValues(string accountId, string type)
        {
            Dictionary<string, object> parms = new Dictionary<string, object>();
            parms.Add("AccountId", accountId);
            parms.Add("Type", type);
            var result = await sqldb.ExecuteProcedureAsync<TaxValues>("usp_Get_TaxValues", parms);
            return result.ToList();
        }

        //added by nandhini 
        public async Task<TaxValues> saveTaxSeries(TaxValues TaxValues)
        {
            TaxValues.CreatedAt = CustomDateTime.IST;
            TaxValues.UpdatedAt = CustomDateTime.IST;
            return await Insert(TaxValues, TaxValuesTable.Table);

        }
        public async Task<string> CheckUniqueTax(TaxValues data)
        {
            return await CheckTaxExist(data);
        }
        public async Task<string> CheckTaxExist(TaxValues data)
        {
            string query = $@" select Tax from TaxValues(nolock) where AccountId='{data.AccountId}' and Tax='{data.Tax}'";
            var qb = QueryBuilderFactory.GetQueryBuilder(query);
            var result = await List<TaxValues>(qb);

            var res = result.Select(item =>
            {
                var obj = new TaxValues()
                {
                    Tax = item.Tax
                };
                return obj;
            }).FirstOrDefault();
            if (res != null)
                return res.Tax.ToString();
            else
                return null;

            //if (result != null)
            //    return result.ToString();
            //else
            //    return null;
        }
        public async Task<TaxValues> UpdateTaxValuesStatus(TaxValues data)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(TaxValuesTable.Table, OperationType.Update);
            qb.AddColumn(TaxValuesTable.IsActiveColumn, TaxValuesTable.UpdatedAtColumn, TaxValuesTable.UpdatedByColumn);
            qb.Parameters.Add(TaxValuesTable.IsActiveColumn.ColumnName, data.IsActive);
            qb.Parameters.Add(TaxValuesTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
            qb.Parameters.Add(TaxValuesTable.UpdatedByColumn.ColumnName, data.UpdatedBy);
            qb.ConditionBuilder.And(TaxValuesTable.AccountIdColumn, data.AccountId);
            qb.ConditionBuilder.And(TaxValuesTable.IdColumn, data.Id);

            await QueryExecuter.NonQueryAsyc(qb);
            return data;
        }
        public async Task<VendorPurchase> SaveBulkData(VendorPurchase data)
        {
            List<SyncObject> syncObjectList = new List<SyncObject>();
            SyncObject syncObject = null;
            SqlConnection con = null;
            SqlTransaction transaction = null;
            List<dynamic> qbList = null;
            var index = 0;

            try
            {
                con = QueryExecuter.OpenConnection();
                if (_configHelper.AppConfig.OfflineMode)
                {
                    transaction = con.BeginTransaction();

                    //ExecuteTransactionOrder is to avoid deadlock issue
                    var list = data.GetExecutionQuery();
                    list = list.Where(x => x.Table != null).Select(x => x.Table).Distinct().ToList();
                    var tableList = "'" + string.Join("','", from item in list select item.TableName) + "'";
                    await ExecuteTransactionOrder(tableList, con, transaction);
                }

                //Get query list
                qbList = data.GetExecutionQuery();

                for (; index < qbList.Count; index++)
                {
                    QueryBuilderBase qbData = qbList[index];
                    if (qbData.CommandType == CommandType.StoredProcedure)
                    {
                        if (qbData.ProcedureName.ToLower() == "usp_productstock_update")
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            qbData.Parameters.Where(x => x.Key.ToLower() != "transquantity").ToList().ForEach(x => param.Add(x.Key, x.Value));
                            var result = await sqldb.ExecuteProcedureAsync<dynamic>(qbData.ProcedureName, param, con, transaction);
                        }
                        else if (qbData.ProcedureName.ToLower() == "usp_purchase_productstock_update")
                        {
                            Dictionary<string, object> param = new Dictionary<string, object>();
                            qbData.Parameters.Where(x => x.Key.ToLower() != "transquantity").ToList().ForEach(x => param.Add(x.Key, x.Value));
                            var result = await sqldb.ExecuteProcedureAsync<dynamic>(qbData.ProcedureName, param, con, transaction);
                        }
                    }
                    else if (qbData.Table == VendorPurchaseTable.Table && qbData.OperationType == OperationType.Insert)
                    {
                        VendorPurchase getData = new VendorPurchase();
                        getData.InstanceId = data.InstanceId;
                        getData.AccountId = data.AccountId;
                        getData.Prefix = data.Prefix;
                        var getGRN = await GetGRNumber(getData, con, transaction);
                        data.GoodsRcvNo = getGRN.GoodsRcvNo.ToString();
                        data.BillSeries = getGRN.BillSeries;
                        qbData.Parameters[VendorPurchaseTable.GoodsRcvNoColumn.ColumnName] = data.GoodsRcvNo;
                        qbData.Parameters[VendorPurchaseTable.BillSeriesColumn.ColumnName] = data.BillSeries;
                        await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                        getData = null;
                    }
                    //else if (qbData.Table == ProductTable.Table && qbData.OperationType == OperationType.Insert)
                    //{
                    //    var query = $"SELECT * FROM {ProductTable.TableName} WHERE {ProductTable.NameColumn} = '{qbData.Parameters[ProductTable.NameColumn.ColumnName].ToString().Replace("'", "''")}' and {ProductTable.AccountIdColumn} = '{data.AccountId}'";
                    //    var qb1 = QueryBuilderFactory.GetQueryBuilder(query);
                    //    var resultProduct = await List<Product>(qb1);
                    //    if (resultProduct.Count == 0)
                    //    {
                    //        await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                    //    }

                    //}
                    //else if (qbData.Table == ProductInstanceTable.Table && qbData.OperationType == OperationType.Insert)
                    //{
                    //    var query = $"SELECT top 1 * FROM {ProductTable.TableName} WHERE {ProductTable.IdColumn} = '{qbData.Parameters[ProductInstanceTable.ProductIdColumn.ColumnName]}' and {ProductTable.AccountIdColumn} = '{data.AccountId}'";
                    //    var qb1 = QueryBuilderFactory.GetQueryBuilder(query);
                    //    var resultProduct = await List<Product>(qb1);
                    //    if (resultProduct.Count > 0)
                    //    {
                    //        await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                    //    }
                    //}
                    else
                    {
                        await QueryExecuter.NonQueryAsyc(qbData, con, transaction);
                    }

                    //Add sync data to list
                    if (qbData.Table == ProductTable.Table && qbData.OperationType == OperationType.Insert)
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters, EventLevel = "A" };
                    }
                    else
                    {
                        syncObject = new SyncObject() { Id = Guid.NewGuid().ToString(), QueryText = qbData.GetQuery(), Parameters = qbData.Parameters };
                    }
                    syncObjectList.Add(syncObject);
                }

                //Write sync file
                WriteToSyncQueue(syncObjectList);

                //Commit the transaction
                if (transaction != null)
                {
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                data.SaveFailed = true;
                if (transaction != null)
                {
                    transaction.Rollback();
                }

                var unSavedData = new List<SyncObject>();
                if (qbList != null)
                {
                    for (; index < qbList.Count; index++)
                    {
                        if (qbList[index].Table == ProductTable.Table && qbList[index].OperationType == OperationType.Insert)
                        {
                            syncObject = new SyncObject() { AccountId = user.AccountId(), InstanceId = user.InstanceId(), QueryText = qbList[index].GetQuery(), Parameters = qbList[index].Parameters, EventLevel = "A" };
                        }
                        else
                        {
                            syncObject = new SyncObject() { AccountId = user.AccountId(), InstanceId = user.InstanceId(), QueryText = qbList[index].GetQuery(), Parameters = qbList[index].Parameters };
                        }
                        unSavedData.Add(syncObject);
                    }
                }

                var errorLog = new BaseErrorLog()
                {
                    AccountId = user.AccountId(),
                    InstanceId = user.InstanceId(),
                    ErrorMessage = e.Message,
                    ErrorStackTrace = e.StackTrace,
                    UnSavedData = JsonConvert.SerializeObject(unSavedData),
                    Data = JsonConvert.SerializeObject(data),
                    CreatedBy = user.Identity.Id(),
                    UpdatedBy = user.Identity.Id()
                };
                data.ErrorLog = errorLog;
                await Insert(errorLog, ErrorLogTable.Table);

                //Write executed sync query for reverse sync
                if (!_configHelper.AppConfig.OfflineMode && syncObjectList.Count > 0)
                {
                    WriteToSyncQueue(syncObjectList);
                }
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                }
                if (con != null)
                {
                    QueryExecuter.CloseConnection(con);
                }
            }
            return data;
        }

        public async Task<int> GetInvoiceCount(string AccountId, string InstanceId, string VendorId, string InvoiceNo)
        {
            var qb = QueryBuilderFactory.GetQueryBuilder(VendorPurchaseTable.Table, OperationType.Count);
            qb.AddColumn(VendorPurchaseTable.Table.ColumnList.ToArray());
            qb.ConditionBuilder.And(VendorPurchaseTable.AccountIdColumn, AccountId);
            qb.ConditionBuilder.And(VendorPurchaseTable.InstanceIdColumn, InstanceId);
            qb.ConditionBuilder.And(VendorPurchaseTable.VendorIdColumn, VendorId);
            qb.ConditionBuilder.And(VendorPurchaseTable.InvoiceNoColumn, InvoiceNo);

            return Convert.ToInt32(await SingleValueAsyc(qb));

        }

        public async Task<bool> DeleteTempVendorPurchaseItem(TempVendorPurchaseItem tempVendorPurchaseItem)
        {

            try
            {
                var qb = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Select);
                qb.AddColumn(TempVendorPurchaseItemTable.IdColumn, TempVendorPurchaseItemTable.isActiveColumn, TempVendorPurchaseItemTable.PackageQtyColumn, TempVendorPurchaseItemTable.QuantityColumn, TempVendorPurchaseItemTable.ProductStockIdColumn);

                qb.ConditionBuilder.And(TempVendorPurchaseItemTable.isActiveColumn, 1);
                qb.ConditionBuilder.And(TempVendorPurchaseItemTable.InstanceIdColumn, tempVendorPurchaseItem.InstanceId);
                qb.ConditionBuilder.And(TempVendorPurchaseItemTable.IdColumn, tempVendorPurchaseItem.Id);
                qb.ConditionBuilder.And(TempVendorPurchaseItemTable.AccountIdColumn, tempVendorPurchaseItem.AccountId);

                var resultTempVendorPurchaseItem = (await List<TempVendorPurchaseItem>(qb)).FirstOrDefault();
                if (resultTempVendorPurchaseItem != null)
                {
                    if (Convert.ToDecimal(resultTempVendorPurchaseItem.Quantity).Equals(tempVendorPurchaseItem.Quantity))
                    {
                        var productStock = await _productStockDataAccess.GetStockValue(resultTempVendorPurchaseItem.ProductStockId);
                        if (tempVendorPurchaseItem.Quantity <= productStock.Stock)
                        {
                            productStock.Stock = productStock.Stock - tempVendorPurchaseItem.Quantity;
                            await _productStockDataAccess.Update(productStock);

                            qb = QueryBuilderFactory.GetQueryBuilder(TempVendorPurchaseItemTable.Table, OperationType.Update);
                            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.IdColumn, tempVendorPurchaseItem.Id);
                            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.InstanceIdColumn, tempVendorPurchaseItem.InstanceId);
                            qb.ConditionBuilder.And(TempVendorPurchaseItemTable.AccountIdColumn, tempVendorPurchaseItem.AccountId);
                            qb.AddColumn(TempVendorPurchaseItemTable.isActiveColumn, TempVendorPurchaseItemTable.UpdatedAtColumn, TempVendorPurchaseItemTable.UpdatedByColumn);
                            qb.Parameters.Add(TempVendorPurchaseItemTable.isActiveColumn.ColumnName, 0);
                            qb.Parameters.Add(TempVendorPurchaseItemTable.UpdatedAtColumn.ColumnName, DateTime.UtcNow);
                            qb.Parameters.Add(TempVendorPurchaseItemTable.UpdatedByColumn.ColumnName, tempVendorPurchaseItem.UpdatedBy);
                            await QueryExecuter.NonQueryAsyc(qb);
                            return true;
                        }

                    }

                }
                return false;
            }
            catch(Exception ex)
            {
                return false;
            }
                         
           
        }
    }
}