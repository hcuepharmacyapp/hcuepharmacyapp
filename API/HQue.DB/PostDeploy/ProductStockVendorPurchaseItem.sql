﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


--IF NOT EXISTS(SELECT 1 FROM ProductStock WHERE PurchasePrice IS NOT NULL)
--BEGIN
--	UPDATE PS 
--	SET PS.[PackageSize] =VPI.[PackageSize] ,
--	PS.[PackagePurchasePrice] = VPI.[PackagePurchasePrice],    
--	PS.[PurchasePrice] =VPI.[PurchasePrice]
--	FROM ProductStock AS PS INNER JOIN VendorPurchaseItem AS VPI ON PS.Id = VPI.ProductStockId
--END
 
--TRUNCATE TABLE ProductInstance
--go
--INSERT INTO ProductInstance (Id,AccountID,InstanceId,ProductId,ReorderLevel,ReOrderQty,RackNo,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy)
--select NEWID(),P.AccountId,I.Id,P.Id, P.ReOrderLevel,P.ReOrderQty,P.RackNo,P.CreatedAt,P.UpdatedAt,P.CreatedBy,P.UpdatedBy
--from Product P inner join Instance I  on I.AccountId =P.AccountId

--TRUNCATE TABLE FinYearMaster
--GO
--INSERT INTO  FinYearMaster(Id,
--AccountId,
--InstanceId,
--FinYearStartDate,
--FinYearEndDate,
--CreatedAt,
--UpdatedAt,
--CreatedBy,
--UpdatedBy)
--select NEWID(), id, NULL, '2017-04-01','2018-03-31',getdate(), getdate(), 'admin','admin' 
-- from account 
--GO
--update VendorPurchaseItem set PackageMRP = PackageSellingPrice where isnull(PackageMRP,0) = 0
--GO
--update ProductStock set MRP = SellingPrice where isnull(MRP,0) = 0
--GO
--update VendorPurchaseItemAudit set PackageMRP = PackageSellingPrice where isnull(PackageMRP,0) = 0
--GO
--update TempVendorPurchaseItem set PackageMRP = PackageSellingPrice where isnull(PackageMRP,0) = 0
--GO
--update DCVendorPurchaseItem set PackageMRP = PackageSellingPrice where isnull(PackageMRP,0) = 0
--GO
--update SalesReturnItem set MRP = MrpSellingPrice where isnull(MRP,0) = 0
--GO
--update SalesItemAudit set MRP = SellingPrice where isnull(MRP,0) = 0
--GO
--update SalesItem set MRP = SellingPrice where isnull(MRP,0)=0
--GO
----update Patient set PatientType = 1 where isnull(PatientType,0)=0
--GO
--Delete from TemplatePurchaseMaster where VendorHeaderName = 'BoxNo'
--GO
--INSERT INTO [dbo].[TemplatePurchaseMaster]
--           ([Id]
--           ,[AccountId]
--           ,[InstanceId]
--           ,[VendorHeaderName]
--           ,[TemplateColumnId]
--           ,[CreatedAt]
--           ,[UpdatedAt]
--           ,[CreatedBy]
--           ,[UpdatedBy])
--     VALUES
--           (NEWID() ,
--		   '18204879-99ff-4efd-b076-f85b4a0da0a3',
--           '013513b1-ea8c-4ea8-9fed-054b260ee197',
--           'BoxNo',
--           NEWID(), 
--           CURRENT_TIMESTAMP,
--           CURRENT_TIMESTAMP,
--           '45563b33-66cd-4d1e-a726-676a4f74fc74',
--           '45563b33-66cd-4d1e-a726-676a4f74fc74')

--GO
--update [TemplatePurchaseMaster] set  TemplateColumnId = Id where VendorHeaderName = 'BoxNo' 
--GO

-- Added for V41 - StockTransfer
 UPDATE stocktransfer  set  transferby= u.Name from stocktransfer ST inner join HQueUser U on U.Id = ST.CreatedBy 
 where isnull(transferby,'') =''
GO

---- Added for V41 - Customer registration data correction
--Delete from patient where isNull(Name, '') ='' and isNull(Mobile, '') = '' and isNull(EmpId, '') = ''
--GO

--Insert into Patient (Id, AccountId, Name, Mobile, Email, DOB, Age, Gender, [Address], Pincode, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy)
--select newId() as Id, AccountId, Name, Mobile, Email, DOB, Age, Gender, [Address], Pincode, CreatedAt, UpdatedAt, CreatedBy, UpdatedBy
-- from sales s where Name+Mobile not in (
--select Name+Mobile from Patient P where P.AccountId = s.AccountId and isNull(Name, '') != '' and isNull(Mobile, '') != ''
--)
--and isNull(Name, '') != '' and isNull(Mobile, '') != ''
--GO

--Update s set s.patientId = p.id from Patient p join Sales s on p.name = s.name and p.mobile = s.mobile and p.AccountId = s.AccountId
--where isNull(p.name, '') != '' and isNull(p.mobile, '') != '' and isNull(s.patientId, '') = ''
--GO

--Insert into Patient (Id, AccountId, Name, Mobile, Email, DOB, Age, Gender, [Address], Pincode)
--select newId() Id, * from (
--select distinct AccountId, Name, Mobile, Email, DOB, Age, Gender, [Address], Pincode
-- from sales s where Name not in (
--select Name from Patient P where P.AccountId = s.AccountId and isNull(Mobile, '') = ''
--)
--and isNull(Mobile, '') = ''
--) a
--GO

--Update s set s.patientId = p.id from Patient p join Sales s on p.name = s.name and p.AccountId = s.AccountId
--where isNull(p.name, '') != '' and isNull(s.mobile, '') = ''
--GO

--Insert into Patient (Id, AccountId, Name, Mobile, Email, DOB, Age, Gender, [Address], Pincode)
--select newId() Id, * from (
--select distinct AccountId, Name, Mobile, Email, DOB, Age, Gender, [Address], Pincode
-- from sales s where Mobile not in (
--select Mobile from Patient P where P.AccountId = s.AccountId and isNull(Name, '') = ''
--)
--and isNull(Name, '') = ''
--) a
--GO

--Update s set s.patientId = p.id from Patient p join Sales s on p.mobile = s.mobile and p.AccountId = s.AccountId
--where isNull(p.mobile, '') != '' and isNull(s.name, '') = ''
--GO

--Update Patient set EmpId = Replace(EmpId, 'X', 'Y') where EmpId like 'X%'
--GO

--With PatientCTE As
--(select Id, 'X'+ltrim(str(row_number() over (partition by accountId order by createdAt))) newEmpId, empid as EmpId from patient where empid like 'X%' and accountId in (
--select distinct accountId from (
--select accountId, empId, count(1) as countP from patient where empid like 'X%' group by accountId, empId 
--having count(1) > 1
--) a
--)
--)
--Update PatientCTE Set empId = newEmpId
--GO

--Added by v43 Release
/*
update vr set vr.vendorid=vp.vendorid
from vendorreturn vr join vendorpurchase vp
on vr.VendorPurchaseId = vp.Id and vr.instanceId = vp.instanceId 
where isnull(vr.vendorid ,'')  = ''
GO
UPDATE VendorOrderItem SET ReceivedQty = Quantity WHERE ReceivedQty IS NULL AND (StockTransferId IS NOT NULL OR VendorPurchaseId IS NOT NULL)
--GO
 --PrintCustomFields = '
--[
--{"field":"sno","enable":1,"width":"3%"},
--{"field":"product","enable":1,"width":"*"},
--{"field":"hsncode","enable":0,"width":"7%"},
--{"field":"i/u","enable":0,"width":"4%"},
--{"field":"batch","enable":1,"width":"10%"},
--{"field":"expiry","enable":1,"width":"8%"},
--{"field":"qty","enable":1,"width":"6%"},
--{"field":"mrp","enable":1,"width":"8%"},
--{"field":"discount","enable":0,"width":"4%"},
--{"field":"cgst%","enable":0,"width":"4%"},
--{"field":"cgstamt","enable":0,"width":"6%"},
--{"field":"utgst/sgst%","enable":0,"width":"4%"},
--{"field":"utgst/sgstamt","enable":0,"width":"6%"},
--{"field":"igst%","enable":0,"width":"4%"},
--{"field":"igstamt","enable":0,"width":"6%"},
--{"field":"gst%","enable":0,"width":"4%"},
--{"field":"gstamt","enable":0,"width":"7%"},
--{"field":"amount","enable":1,"width":"15%"},

--{"field":"doctorname","enable":1},
--{"field":"custname","enable":1,"title":"Patient Name"},
--{"field":"custaddr","enable":1},
--{"field":"custmobile","enable":1},
--{"field":"custgstin","enable":0},
--{"field":"custempid","enable":0},

--{"field":"totcgstamt","enable":0},
--{"field":"totutgst/sgstamt","enable":0},
--{"field":"totigstamt","enable":0},
--{"field":"totgstamt","enable":0}
--]
--'
--WHERE AccountId = '3de28f60-6d55-4ca7-bfbf-7e834b228e85' AND InstanceId = 'ce834029-e550-4615-9147-f8dcd654ee5e' AND PrintCustomFields IS NULL
--GO
--UPDATE SaleSettings SET PrintCustomFields = '
--[
--{"field":"sno","enable":1,"width":"3%"},
--{"field":"product","enable":1,"width":"*"},
--{"field":"hsncode","enable":1,"width":"10%"},
--{"field":"i/u","enable":0,"width":"4%"},
--{"field":"batch","enable":1,"width":"10%"},
--{"field":"expiry","enable":1,"width":"8%"},
--{"field":"qty","enable":1,"width":"5%"},
--{"field":"mrp","enable":1,"width":"8%"},
--{"field":"discount","enable":0,"width":"4%"},
--{"field":"cgst%","enable":0,"width":"4%"},
--{"field":"cgstamt","enable":0,"width":"6%"},
--{"field":"utgst/sgst%","enable":0,"width":"4%"},
--{"field":"utgst/sgstamt","enable":0,"width":"6%"},
--{"field":"igst%","enable":0,"width":"4%"},
--{"field":"igstamt","enable":0,"width":"6%"},
--{"field":"gst%","enable":1,"width":"5%"},
--{"field":"gstamt","enable":0,"width":"7%"},
--{"field":"amount","enable":1,"width":"15%"},

--{"field":"doctorname","enable":1},
--{"field":"custname","enable":1,"title":"Patient Name"},
--{"field":"custaddr","enable":1},
--{"field":"custmobile","enable":1},
--{"field":"custgstin","enable":0},
--{"field":"custempid","enable":0},

--{"field":"totcgstamt","enable":1},
--{"field":"totutgst/sgstamt","enable":1},
--{"field":"totigstamt","enable":1},
--{"field":"totgstamt","enable":1}
--]
--'
--WHERE PrintCustomFields IS NULL
--GO



*/
/*
 
UPDATE SaleSettings SET PrintCustomFields = '
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"8%"},
{"field":"i/u","enable":1,"width":"4%"},
{"field":"batch","enable":1,"width":"6%"},
{"field":"expiry","enable":1,"width":"5%"},
{"field":"qty","enable":1,"width":"4%"},
{"field":"mrp","enable":1,"width":"7%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":1,"width":"4%"},
{"field":"cgst%","enable":1,"width":"4%"},
{"field":"cgstamt","enable":1,"width":"6%"},
{"field":"utgst/sgst%","enable":1,"width":"4%"},
{"field":"utgst/sgstamt","enable":1,"width":"6%"},
{"field":"igst%","enable":1,"width":"4%"},
{"field":"igstamt","enable":1,"width":"6%"},
{"field":"gst%","enable":1,"width":"4%"},
{"field":"gstamt","enable":1,"width":"6%"},
{"field":"amount","enable":1,"width":"10%"},

{"field":"doctorname","enable":0},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":1},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
]'
WHERE  id ='0947361f-cd0d-44fd-872b-d8f28a900775'

go

UPDATE SaleSettings SET PrintCustomFields = '
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":0,"width":"7%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"6%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":0,"width":"4%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":0},
{"field":"totutgst/sgstamt","enable":0},
{"field":"totigstamt","enable":0},
{"field":"totgstamt","enable":0}
]
'
WHERE  id ='0abbb2b3-3929-481b-b1e1-c01b902f0ab2'


 
UPDATE SaleSettings SET PrintCustomFields = '
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"10%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"5%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
]
'
where PrintCustomFields not like '%title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"%'
 go
 */
 /*
 GO
 UPDATE SaleSettings SET PrintCustomFields = '
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"10%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"5%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "10%", "10%", "8%", "5%", "8%", "5%", "15%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "HSN Code",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "MRP",
				"alignment": "center"
			}, {
				"text": "GST%",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],

			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@hsnCode",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@gstPerc",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["0%", "100%"],
			"body": [
				[{}, {
					"text": "@pharmacyName",
					"alignment": "center",
					"style": {
						"bold": true,
						"fontSize": 15
					}
				}],
				[{}, {
					"text": "@pharmacyAddressArea",
					"alignment": "center"
				}],
				[{}, {
					"text": "@pharmacyCityStatePincode",
					"alignment": "center"
				}],
				[{}, {
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "center"
				}],
				[{}, {
					"text": "DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo",
					"alignment": "center"
				}]
			]
		},
		"layout": "leftTopRightBottomLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["75%", "25%"],
		"body": [
			[{
				"text": "Doctor Name       : @doctorName"
			}, 
			{
				"text": "Bill No       : @billNo"
			}],
			[{
				"text": "Customer Name  : @customerName"
			}, 
			{
				"text": "B Date       : @billDate"
			}],
			[{
				"text": "Address                : @customerAddress"
			}, 
			{
				"text": "B Time      : @billTime"
			}],
			[{
				"text": "Mobile                   : @customerMobile"
			}, 
			{
				"text": "B Type       : @invoiceType"
			}]
		]
	},
	"layout": "leftRightLayout"
}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": "@headerFontSize"
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}' WHERE PrintCustomFields IS NULL OR PrintCustomFields = '
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"10%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"5%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
]
'
GO
UPDATE SaleSettings SET PrintCustomFields = '
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":0,"width":"7%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"6%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":0,"width":"4%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":0},
{"field":"totutgst/sgstamt","enable":0},
{"field":"totigstamt","enable":0},
{"field":"totgstamt","enable":0}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "10%", "8%", "6%", "8%", "12%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "MRP",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],

			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["40%", "60%"],
			"body": [
				[{
					"fit": [200, 200],
                    "image": "@printLogo",
                    "rowSpan": 5
				}, {}],
				[{}, {
					"text": "@pharmacyAddressArea",
					"alignment": "left"
				}],
				[{}, {
					"text": "@pharmacyCityStatePincode",
					"alignment": "left"
				}],
				[{}, {
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "left"
				}],
				[{}, {
					"text": "DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo",
					"alignment": "left"
				}]
			]
		},
		"layout": "leftTopRightBottomLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["75%", "25%"],
		"body": [
			[{
				"text": "Doctor Name       : @doctorName"
			}, 
			{
				"text": "Bill No       : @billNo"
			}],
			[{
				"text": "Customer Name  : @customerName"
			}, 
			{
				"text": "B Date       : @billDate"
			}],
			[{
				"text": "Address                : @customerAddress"
			}, 
			{
				"text": "B Time      : @billTime"
			}],
			[{
				"text": "Mobile                   : @customerMobile"
			}, 
			{
				"text": "B Type       : @invoiceType"
			}]
		]
	},
	"layout": "leftRightLayout"
}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": "@headerFontSize"
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}' WHERE PrintCustomFields = '
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":0,"width":"7%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"6%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":0,"width":"4%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":0},
{"field":"totutgst/sgstamt","enable":0},
{"field":"totigstamt","enable":0},
{"field":"totgstamt","enable":0}
]
'
GO
UPDATE SaleSettings SET PrintCustomFields = '
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"7%"},
{"field":"i/u","enable":1,"width":"3%"},
{"field":"batch","enable":1,"width":"6%"},
{"field":"expiry","enable":1,"width":"5%"},
{"field":"qty","enable":1,"width":"3%"},
{"field":"mrp","enable":1,"width":"5%","title":"Rate"},
{"field":"maxmrp","enable":1,"width":"5%","title":"MRP"},
{"field":"discount","enable":1,"width":"4%"},
{"field":"cgst%","enable":1,"width":"4%"},
{"field":"cgstamt","enable":1,"width":"6%"},
{"field":"utgst/sgst%","enable":1,"width":"4%"},
{"field":"utgst/sgstamt","enable":1,"width":"6%"},
{"field":"igst%","enable":1,"width":"5%"},
{"field":"igstamt","enable":1,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":1,"width":"6%"},
{"field":"amount","enable":1,"width":"9%"},

{"field":"doctorname","enable":0},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":1},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "7%", "3%", "8%", "4%", "3%", "5%", "5%","4%", "4%", "6%", "4%", "6%", "5%", "6%", "5%", "6%", "8%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "HSN Code",
				"alignment": "center"
			}, {
				"text": "I/U",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "Rate",
				"alignment": "center"
			}, {
				"text": "MRP",
				"alignment": "center"
			}, {
				"text": "Disc%",
				"alignment": "center"
			}, {
				"text": "CGST%",
				"alignment": "center"
			}, {
				"text": "CGST Amt",
				"alignment": "center"
			}, {
				"text": "@sgstOrUtgstFlagPerc%",
				"alignment": "center"
			}, {
				"text": "@sgstOrUtgstFlagAmt Amt",
				"alignment": "center"
			}, {
				"text": "IGST%",
				"alignment": "center"
			}, {
				"text": "IGST Amt",
				"alignment": "center"
			}, {
				"text": "GST%",
				"alignment": "center"
			}, {
				"text": "GST Amt",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],
			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@hsnCode",
				"alignment": "left"
			}, {
				"text": "@packageSize",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@maxMRP",
				"alignment": "center"
			}, {
				"text": "@productDiscount",
				"alignment": "center"
			}, {
				"text": "@productCgstPerc",
				"alignment": "right"
			}, {
				"text": "@productCgstAmt",
				"alignment": "right"
			}, {
				"text": "@productSgstPerc",
				"alignment": "right"
			}, {
				"text": "@productSgstAmt",
				"alignment": "right"
			}, {
				"text": "@productIgstPerc",
				"alignment": "right"
			}, {
				"text": "@productIgstAmt",
				"alignment": "right"
			}, {
				"text": "@gstPerc",
				"alignment": "right"
			}, {
				"text": "@productGstAmt",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["0%", "100%"],
			"body": [
				[{}, {
					"text": "@pharmacyName",
					"alignment": "center",
					"style": {
						"bold": true,
						"fontSize": 15
					}
				}],
				[{}, {
					"text": "@pharmacyAddressArea",
					"alignment": "center"
				}],
				[{}, {
					"text": "@pharmacyCityStatePincode",
					"alignment": "center"
				}],
				[{}, {
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "center"
				}],
				[{}, {
					"text": "DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo",
					"alignment": "center"
				}]
			]
		},
		"layout": "leftTopRightBottomLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["75%", "25%"],
		"body": [
			[{
				"text": "Customer Name  : @customerName"
			}, 
			{
				"text": "Bill No       : @billNo"
			}],
			[{
				"text": "Address                : @customerAddress"
			}, 
			{
				"text": "B Date       : @billDate"
			}],
			[{
				"text": "Mobile                   : @customerMobile"
			}, 
			{
				"text": "B Time      : @billTime"
			}],
			[{
				"text": "GSTIN No              : @customerGSTinNo"
			}, 
			{
				"text": "B Type       : @invoiceType"
			}]
		]
	},
	"layout": "leftRightLayout"
}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": "@headerFontSize"
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}' WHERE PrintCustomFields = '
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"7%"},
{"field":"i/u","enable":1,"width":"3%"},
{"field":"batch","enable":1,"width":"6%"},
{"field":"expiry","enable":1,"width":"5%"},
{"field":"qty","enable":1,"width":"3%"},
{"field":"mrp","enable":1,"width":"5%","title":"Rate"},
{"field":"maxmrp","enable":1,"width":"5%","title":"MRP"},
{"field":"discount","enable":1,"width":"4%"},
{"field":"cgst%","enable":1,"width":"4%"},
{"field":"cgstamt","enable":1,"width":"6%"},
{"field":"utgst/sgst%","enable":1,"width":"4%"},
{"field":"utgst/sgstamt","enable":1,"width":"6%"},
{"field":"igst%","enable":1,"width":"5%"},
{"field":"igstamt","enable":1,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":1,"width":"6%"},
{"field":"amount","enable":1,"width":"9%"},

{"field":"doctorname","enable":0},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":1},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
]
'
GO
UPDATE SaleSettings SET PrintCustomFields ='
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"10%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"5%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1},
{"field":"pharmacyOfficialNote","enable":1}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["4%", "*", "10%", "6%", "5%", "9%", "8%", "5%", "8%", "14%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "HSN",
				"alignment": "center"
			}, {
				"text": "GST%",
				"alignment": "center"
			}, {
				"text": "Sch.",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "Rate",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],
			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@hsnCode",
				"alignment": "left"
			}, {
				"text": "@gstPerc",
				"alignment": "right"
			}, {
				"text": "@schedule",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["51%", "20%", "29%"],
			"body": [
				[{
					"text": "@pharmacyName",
					"alignment": "center",
					"style": {
						"bold": true,
						"fontSize": 12
					}
				},
				{},
				{}],
				[{
					"text": "@pharmacyAddressArea",
					"alignment": "center"
				},
				{
				"text": "SALE INVOICE",
				"alignment": "center"
				},
				{}],
				[{
					"text": "@pharmacyCityStatePincode",
					"alignment": "center"
				},
				{
				"text": "@invoiceType",
				"alignment": "center"
				},
				{
				"text": "DL No : @pharmacyDLNo"
				}],
				[{
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "center"
				},
				{},				
				{
				"text": "@pharmacyGSTinNoOrTinNo"
				}]
			]
		},
		"layout": "leftTopRightBottomWTVerticalLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["71.6%", "28.4%"],
		"body": [
			[{
				"text": "Doctor Name : @doctorName"
			}, 
			{
				"text": "Date : @billDateTime"
			}],
			[{
				"text": "Customer Name : @customerName"
			}, 
			{
				"text": "B.No : @billNo"
			}]
		]
	},
	"layout": "tableVerticalLayout"
}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": "@headerFontSize"
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}'
where InstanceId='3e2ec066-1551-4527-be53-5aa3c5b7fb7d' OR InstanceId='8fd7a007-b9b9-432d-b8b2-3cef7c087c24'
GO
UPDATE SaleSettings SET PrintCustomFields = '
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"7%"},
{"field":"i/u","enable":1,"width":"3%"},
{"field":"batch","enable":1,"width":"6%"},
{"field":"expiry","enable":1,"width":"5%"},
{"field":"qty","enable":1,"width":"3%"},
{"field":"mrp","enable":1,"width":"5%","title":"Rate"},
{"field":"maxmrp","enable":1,"width":"5%","title":"MRP"},
{"field":"discount","enable":1,"width":"4%"},
{"field":"cgst%","enable":1,"width":"4%"},
{"field":"cgstamt","enable":1,"width":"6%"},
{"field":"utgst/sgst%","enable":1,"width":"4%"},
{"field":"utgst/sgstamt","enable":1,"width":"6%"},
{"field":"igst%","enable":1,"width":"5%"},
{"field":"igstamt","enable":1,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":1,"width":"6%"},
{"field":"amount","enable":1,"width":"9%"},

{"field":"doctorname","enable":0},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":1},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":0},
{"field":"courierfees","enable":0}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "8%", "8%", "4%", "4%", "5%", "5%", "5%", "5%", "5%", "5%", "10%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "HSN Code",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "Rate",
				"alignment": "center"
			}, {
				"text": "MRP",
				"alignment": "center"
			}, {
				"text": "CGST%",
				"alignment": "center"
			}, {
				"text": "@sgstOrUtgstFlagPerc%",
				"alignment": "center"
			}, {
				"text": "IGST%",
				"alignment": "center"
			}, {
				"text": "GST%",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],
			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@hsnCode",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@maxMRP",
				"alignment": "center"
			}, {
				"text": "@productCgstPerc",
				"alignment": "right"
			}, {
				"text": "@productSgstPerc",
				"alignment": "right"
			}, {
				"text": "@productIgstPerc",
				"alignment": "right"
			}, {
				"text": "@gstPerc",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["0%", "100%"],
			"body": [
				[{}, {
					"text": "@pharmacyName",
					"alignment": "center",
					"style": {
						"bold": true,
						"fontSize": 15
					}
				}],
				[{}, {
					"text": "@pharmacyAddressArea",
					"alignment": "center"
				}],
				[{}, {
					"text": "@pharmacyCityStatePincode",
					"alignment": "center"
				}],
				[{}, {
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "center"
				}],
				[{}, {
					"text": "DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo",
					"alignment": "center"
				}]
			]
		},
		"layout": "leftTopRightBottomLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["75%", "25%"],
		"body": [
			[{
				"text": "Customer Name  : @customerName"
			}, 
			{
				"text": "Bill No       : @billNo"
			}],
			[{
				"text": "Address                : @customerAddress"
			}, 
			{
				"text": "B Date       : @billDate"
			}],
			[{
				"text": "Mobile                   : @customerMobile"
			}, 
			{
				"text": "B Time      : @billTime"
			}],
			[{
				"text": "GSTIN No              : @customerGSTinNo"
			}, 
			{
				"text": "B Type       : @invoiceType"
			}]
		]
	},
	"layout": "leftRightLayout"
}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": "@headerFontSize"
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}' WHERE InstanceId='ef7d6b49-152d-4e6c-ab39-8a7651aa46cf'
GO
-- Added by Settu on 29-09-17, Vendor purchase formula default insert 
-- Don't remove this script until it's executed in offline/online for all users
GO
INSERT INTO [dbo].[VendorPurchaseFormula]([Id],[AccountId],[InstanceId],[FormulaName],[FormulaJSON],[OfflineStatus],[CreatedAt],[UpdatedAt],[CreatedBy],[UpdatedBy])
SELECT NEWID(),I.AccountId,MAX(I.Id), 
'Default Formula', 
'[{"formulaTag":"markup","operation":"+","order":1},{"formulaTag":"discount","operation":"-","order":2},{"formulaTag":"schemediscount","operation":"-","order":3},{"formulaTag":"tax","operation":"+","order":4}]',
0,GETDATE(),GETDATE(),'admin','admin' FROM Instance I 
WHERE I.AccountId NOT IN(SELECT F.AccountId FROM VendorPurchaseFormula F) GROUP BY I.AccountId 
GO
UPDATE V SET V.VendorPurchaseFormulaId = F.Id FROM Vendor V INNER JOIN VendorPurchaseFormula F ON V.AccountId = F.AccountId WHERE V.VendorPurchaseFormulaId IS NULL AND F.FormulaName = 'Default Formula'
GO
UPDATE VP SET VP.VendorPurchaseFormulaId = V.VendorPurchaseFormulaId FROM VendorPurchase VP INNER JOIN Vendor V ON VP.VendorId = V.Id 
INNER JOIN VendorPurchaseFormula F ON F.Id = V.VendorPurchaseFormulaId
WHERE VP.VendorPurchaseFormulaId IS NULL AND F.FormulaName = 'Default Formula'
GO
DELETE FROM TemplatePurchaseMaster WHERE VendorHeaderName = 'VendorMarkupPerc'
GO
INSERT INTO [dbo].[TemplatePurchaseMaster]
           ([Id]
           ,[AccountId]
           ,[InstanceId]
           ,[VendorHeaderName]
           ,[TemplateColumnId]
           ,[CreatedAt]
           ,[UpdatedAt]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (NEWID() ,
		   '18204879-99ff-4efd-b076-f85b4a0da0a3',
           '013513b1-ea8c-4ea8-9fed-054b260ee197',
           'VendorMarkupPerc',
           NEWID(), 
           CURRENT_TIMESTAMP,
           CURRENT_TIMESTAMP,
           '45563b33-66cd-4d1e-a726-676a4f74fc74',
           '45563b33-66cd-4d1e-a726-676a4f74fc74')
GO
UPDATE [TemplatePurchaseMaster] SET TemplateColumnId = Id WHERE VendorHeaderName = 'VendorMarkupPerc' 
GO
DELETE FROM TemplatePurchaseMaster WHERE VendorHeaderName = 'ProductMarkupPerc'
GO
INSERT INTO [dbo].[TemplatePurchaseMaster]
           ([Id]
           ,[AccountId]
           ,[InstanceId]
           ,[VendorHeaderName]
           ,[TemplateColumnId]
           ,[CreatedAt]
           ,[UpdatedAt]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (NEWID() ,
		   '18204879-99ff-4efd-b076-f85b4a0da0a3',
           '013513b1-ea8c-4ea8-9fed-054b260ee197',
           'ProductMarkupPerc',
           NEWID(), 
           CURRENT_TIMESTAMP,
           CURRENT_TIMESTAMP,
           '45563b33-66cd-4d1e-a726-676a4f74fc74',
           '45563b33-66cd-4d1e-a726-676a4f74fc74')
GO
UPDATE [TemplatePurchaseMaster] SET TemplateColumnId = Id WHERE VendorHeaderName = 'ProductMarkupPerc' 
GO
DELETE FROM TemplatePurchaseMaster WHERE VendorHeaderName = 'SchemeDiscountPerc'
GO
INSERT INTO [dbo].[TemplatePurchaseMaster]
           ([Id]
           ,[AccountId]
           ,[InstanceId]
           ,[VendorHeaderName]
           ,[TemplateColumnId]
           ,[CreatedAt]
           ,[UpdatedAt]
           ,[CreatedBy]
           ,[UpdatedBy])
     VALUES
           (NEWID() ,
		   '18204879-99ff-4efd-b076-f85b4a0da0a3',
           '013513b1-ea8c-4ea8-9fed-054b260ee197',
           'SchemeDiscountPerc',
           NEWID(), 
           CURRENT_TIMESTAMP,
           CURRENT_TIMESTAMP,
           '45563b33-66cd-4d1e-a726-676a4f74fc74',
           '45563b33-66cd-4d1e-a726-676a4f74fc74')
GO
UPDATE [TemplatePurchaseMaster] SET TemplateColumnId = Id WHERE VendorHeaderName = 'SchemeDiscountPerc' 
GO

-- Added for update status Version 58
update vendor set status=1 where status not in (0)
GO
update vendor set status=2 where status = 0 

--Added for paytm options on 10/11/17

Delete from DomainMaster where Code in ('1','2')
GO

Delete from DomainValues where DomainId in ('1','2')
GO

insert into DomainMaster values(1,'1','PaymentMode',null,null,null,GETDATE(),GETDATE(),'admin','admin')

insert into DomainMaster values(2,'2','eWallet',null,null,null,GETDATE(),GETDATE(),'admin','admin')

insert into DomainValues values(4,'1','1','cash',null,null,null,null,GETDATE(),GETDATE(),'admin','admin')

insert into DomainValues values(5,'1','2','card',null,null,null,null,GETDATE(),GETDATE(),'admin','admin')

insert into DomainValues values(6,'1','3','cheque',null,null,null,null,GETDATE(),GETDATE(),'admin','admin')

insert into DomainValues values(7,'1','4','credit',null,null,null,null,GETDATE(),GETDATE(),'admin','admin')

insert into DomainValues values(8,'1','5','eWallet',1,2,null,null,GETDATE(),GETDATE(),'admin','admin')

insert into DomainValues values(9,'2','6','paytm',null,null,null,null,GETDATE(),GETDATE(),'admin','admin')

insert into DomainValues values(10,'2','7','Rupay',null,null,null,null,GETDATE(),GETDATE(),'admin','admin')

insert into DomainValues values(11,'2','8','Freecharge',null,null,null,null,GETDATE(),GETDATE(),'admin','admin')
GO


-- Barcode related data batch script
-- ---------------------------------
GO
UPDATE Product SET Code = NULL WHERE AccountId != '67067991-0e68-4779-825e-8e1d724cd68b'
GO
UPDATE Product SET Code = NULL WHERE AccountId = '67067991-0e68-4779-825e-8e1d724cd68b' AND ISNUMERIC(ISNULL(Code,0)) != 1
GO
UPDATE Product set Product.Code = p.Code
FROM Product JOIN 
(
	SELECT gp.Id,(SELECT ISNULL(MAX(CAST(ISNULL(Code,0) AS INT)),0) FROM Product WHERE AccountId = gp.AccountId)+ROW_NUMBER() OVER (PARTITION BY gp.AccountId ORDER BY gp.CreatedAt ASC) as Code
	FROM Product gp WHERE gp.Code IS NULL
) p on p.Id = Product.Id WHERE Product.Code IS NULL
GO
UPDATE ProductStock SET PurchaseBarcode = NULL WHERE BarcodeProfileId IS NULL
GO
UPDATE Instance set Instance.Code = i.Code
FROM Instance JOIN 
(
	SELECT gi.Id,(SELECT ISNULL(MAX(CAST(ISNULL(Code,0) AS INT)),0) FROM Instance 
	WHERE AccountId = gi.AccountId)+ROW_NUMBER() OVER (PARTITION BY gi.AccountId ORDER BY gi.CreatedAt ASC) as Code
	FROM Instance gi WHERE gi.Code IS NULL
) i on i.Id = Instance.Id WHERE Instance.Code IS NULL
GO

-- Dokk Clinics Laser Print changes
-- --------------------------------
GO
UPDATE SaleSettings SET 
PrintCustomFields = '
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"10%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"5%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "10%", "10%", "8%", "5%", "8%", "5%", "15%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "HSN Code",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "MRP",
				"alignment": "center"
			}, {
				"text": "GST%",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],

			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@hsnCode",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@gstPerc",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["35%", "65%"],
			"body": [
				[{
					"fit": [136, 160],
                    "image": "@printLogo",
                    "rowSpan": 5,
					"margin": [30, 0, 0, 0]
				}, {
					"text": "@pharmacyName",
					"alignment": "left",
					"style": {
						"bold": true,
						"fontSize": 15
					},
					"margin": [0, 1, 0, 0]
				}],
				[{}, {
					"text": "@pharmacyAddressArea",
					"alignment": "left"
				}],
				[{}, {
					"text": "@pharmacyCityStatePincode",
					"alignment": "left"
				}],
				[{}, {
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "left"
				}],
				[{}, {
					"text": "DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo",
					"alignment": "left"
				}]
			]
		},
		"layout": "leftTopRightBottomLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["75%", "25%"],
		"body": [
			[{
				"text": "Doctor Name       : @doctorName"
			}, 
			{
				"text": "Bill No       : @billNo"
			}],
			[{
				"text": "Customer Name  : @customerName"
			}, 
			{
				"text": "B Date       : @billDate"
			}],
			[{
				"text": "Address                : @customerAddress"
			}, 
			{
				"text": "B Time      : @billTime"
			}],
			[{
				"text": "Mobile                   : @customerMobile"
			}, 
			{
				"text": "B Type       : @invoiceType"
			}]
		]
	},
	"layout": "leftRightLayout"
}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": 12
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}',
PrintLogo = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/7QAsUGhvdG9zaG9wIDMuMAA4QklNA+0AAAAAABABLAAAAAEAAQEsAAAAAQAB/+E/Pmh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4NCjx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4NCgk8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPg0KCQk8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iPg0KCQkJPGRjOmZvcm1hdD5pbWFnZS9qcGVnPC9kYzpmb3JtYXQ+DQoJCQk8ZGM6dGl0bGU+DQoJCQkJPHJkZjpBbHQ+DQoJCQkJCTxyZGY6bGkgeG1sOmxhbmc9IngtZGVmYXVsdCI+UHJpbnQ8L3JkZjpsaT4NCgkJCQk8L3JkZjpBbHQ+DQoJCQk8L2RjOnRpdGxlPg0KCQk8L3JkZjpEZXNjcmlwdGlvbj4NCgkJPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBHSW1nPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvZy9pbWcvIj4NCgkJCTx4bXA6TWV0YWRhdGFEYXRlPjIwMTctMTEtMjNUMTQ6MTg6NTQrMDU6MzA8L3htcDpNZXRhZGF0YURhdGU+DQoJCQk8eG1wOk1vZGlmeURhdGU+MjAxNy0xMS0yM1QwODo0ODo1Nlo8L3htcDpNb2RpZnlEYXRlPg0KCQkJPHhtcDpDcmVhdGVEYXRlPjIwMTctMTEtMjNUMTQ6MTg6NTQrMDU6MzA8L3htcDpDcmVhdGVEYXRlPg0KCQkJPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBJbGx1c3RyYXRvciBDUzYgKFdpbmRvd3MpPC94bXA6Q3JlYXRvclRvb2w+DQoJCQk8eG1wOlRodW1ibmFpbHM+DQoJCQkJPHJkZjpBbHQ+DQoJCQkJCTxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPg0KCQkJCQkJPHhtcEdJbWc6d2lkdGg+MjU2PC94bXBHSW1nOndpZHRoPg0KCQkJCQkJPHhtcEdJbWc6aGVpZ2h0PjE4NDwveG1wR0ltZzpoZWlnaHQ+DQoJCQkJCQk8eG1wR0ltZzpmb3JtYXQ+SlBFRzwveG1wR0ltZzpmb3JtYXQ+DQoJCQkJCQk8eG1wR0ltZzppbWFnZT4vOWovNEFBUVNrWkpSZ0FCQWdFQVNBQklBQUQvN1FBc1VHaHZkRzl6YUc5d0lETXVNQUE0UWtsTkErMEFBQUFBQUJBQVNBQUFBQUVBDQpBUUJJQUFBQUFRQUIvKzRBRGtGa2IySmxBR1RBQUFBQUFmL2JBSVFBQmdRRUJBVUVCZ1VGQmdrR0JRWUpDd2dHQmdnTERBb0tDd29LDQpEQkFNREF3TURBd1FEQTRQRUE4T0RCTVRGQlFURXh3Ykd4c2NIeDhmSHg4Zkh4OGZId0VIQndjTkRBMFlFQkFZR2hVUkZSb2ZIeDhmDQpIeDhmSHg4Zkh4OGZIeDhmSHg4Zkh4OGZIeDhmSHg4Zkh4OGZIeDhmSHg4Zkh4OGZIeDhmSHg4Zkh4OGYvOEFBRVFnQXVBRUFBd0VSDQpBQUlSQVFNUkFmL0VBYUlBQUFBSEFRRUJBUUVBQUFBQUFBQUFBQVFGQXdJR0FRQUhDQWtLQ3dFQUFnSURBUUVCQVFFQUFBQUFBQUFBDQpBUUFDQXdRRkJnY0lDUW9MRUFBQ0FRTURBZ1FDQmdjREJBSUdBbk1CQWdNUkJBQUZJUkl4UVZFR0UyRWljWUVVTXBHaEJ4V3hRaVBCDQpVdEhoTXhaaThDUnlndkVsUXpSVGtxS3lZM1BDTlVRbms2T3pOaGRVWkhURDB1SUlKb01KQ2hnWmhKUkZScVMwVnROVktCcnk0L1BFDQoxT1QwWlhXRmxhVzF4ZFhsOVdaMmhwYW10c2JXNXZZM1IxZG5kNGVYcDdmSDErZjNPRWhZYUhpSW1LaTR5TmpvK0NrNVNWbHBlWW1aDQpxYm5KMmVuNUtqcEtXbXA2aXBxcXVzcmE2dm9SQUFJQ0FRSURCUVVFQlFZRUNBTURiUUVBQWhFREJDRVNNVUVGVVJOaElnWnhnWkV5DQpvYkh3Rk1IUjRTTkNGVkppY3ZFekpEUkRnaGFTVXlXaVk3TENCM1BTTmVKRWd4ZFVrd2dKQ2hnWkpqWkZHaWRrZEZVMzhxT3p3eWdwDQowK1B6aEpTa3RNVFU1UFJsZFlXVnBiWEYxZVgxUmxabWRvYVdwcmJHMXViMlIxZG5kNGVYcDdmSDErZjNPRWhZYUhpSW1LaTR5TmpvDQorRGxKV1dsNWlabXB1Y25aNmZrcU9rcGFhbnFLbXFxNnl0cnErdi9hQUF3REFRQUNFUU1SQUQ4QTlVNHE3RlhZcTdGWFlxN0ZYWXE3DQpGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGDQpYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYDQpZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZDQpxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxDQo3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3DQpGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGDQpYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGWFlxN0ZXTjZ0NXZqdHJwN2EzQVl4SGpJNTMrSWRRUGxuSWRyZTBNOGVRNDhJSHA1azkvDQprN0xCMmVaUjRqMWRwSG02TzZ1a3Rad0EwcDR4dU52aVBRRWUrUzdJOW9KNWNneDVnUFZ5STcrNHJxTkFZeDRoMFpKbld1dGRpcnNWDQpkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnliekxaM2RqDQpyRnlreWtMTEkwa1Q5bVJqVUVmZnZubXZhbWxsaXp5RWh6Skk4d1hyTkhralBHSzZCZDVZc3JxKzFtMldJSGhESXNzempvcXFhOWZlDQpsQmg3SzBzc3VlUER5aVFUNVVqVzVJd3htK29wNnZucEx5anNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzDQpWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXI1cDg4Zjg1TmVZMTF5ZTI4cncyME9sMjBoU080blF5eVQ4VFF2OW9LcU4reUFLMDc5czJjDQpORUFQVnpjRTZxenR5ZW9ma3orYlArUGRNdWt2SUV0ZFowNHA5YWppcjZVaVNWNHlJR3FSdXBCV3BwNDc1aWFqQndIYms1T0xKeE05DQp2OVRzTENNU1hreXhLMnlnN3N4L3lWRldQMEROZnFOVmp3aTVtdngwRGxZc004aHFJdEFheHJYbDZCRWkxWGdXY0JoYnlSK293QjdsDQpRR3A5T1ltczF1bWdBTTFiOUNMK3pkdjArbnpTTjQvbmRONlJyWGwrZU40OUxLQW9DNXRvMDlOaUIxSVNpMStqSFI2N1RUQkdHdHY0DQpRSyt6WmRScHMwVGVUNTNhT3NOU3NiK0wxYk9aWmtIMnVQVlQ0TXAzWDZjek5QcWNlWVhBaVEvSHlhTXVHZU0xSVU4Ly9PZjgyLzhBDQpBV24yc05qYnBkYTFxSE0yeVMxOUtPT09nYVNRS1ZZMUxVVUFpdSsrMitmcDhIR2QrVGk1Y25DOHo4aC84NU0rWXBkZnRyTHpSRGJUDQphYmVTckUxekFoaWtnTGtLSDZzckl2N1FwWHZYc2NuSm94Vng1dE1kUWIzZlIxOWRwWjJOeGR1Q3lXMFR5c3E5U0VVc1FLL0xOZUJaDQpjc2w0MUgvemxiNUlMZ1M2VnFhcDNLcmJzZnVNcS9yekwvSlM3dzQvNWdQUVBJMzVvZVQvQURxa2cwVzZiNjFBb2VleG5YMDUxVW1uDQpMalVxd3IzVW1uZktNbUdVT2JiRElKY2xubi84MC9LZmtlQ002dk04bDVNT1VHbjI0RHp1dGFjcUVxcXJYdXhIdFhIRmhsUGtzOGdqDQp6WU5vL3dEemxONUt1NzFZTlFzTHpUb1hJQXVpRW1SZmR3aDVnZjZvT1h5MFVnTm1zYWdQUi9OWG5mU1BML2srZnpXM0svMHlGSVpFDQpOcVZZeUpQSWtTTWhKQ2tmdkFldlRNZUdNeWx3OVcyVXdCYVYrWFB6WTh1YTE1SnZmT0RwTFlhVllQSWs0dU9IcVZpVlQ4SVZtQkxjDQp3cWl1NXlVOEJFdUhxeGprQkZwVjVRL1BIU2ZOTVdxWEZobzEvSGFhUGF5WGQ1Y3kra0VBUlN5eGlqbXJ2eE5CN1pQSnBqR3JJM1JIDQpLRDBURDh1dnpkMEh6eERxczFuYlQyRU9rTEU5MUpkbU5WNHpDUThnVlpxQlJDYTF5T1hBWVZmVk1NZ2tsdmxYODk5QjgxZWFSNWYwDQpYUzc2NFlzOWIzakVzS3d4bWhuYXI4bFE3VXFLN2dVcmtwNll4alpLSTVnVFFlbVpqTnJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyDQpzVmRpcnNWZkhIbmo4bHZPK2lhMWNXOWxwTjNxZGdYWTJkMVp3dmNCb3EvRHpFWVlvMU9vYjZNM3NkVENjYnVpNldPbnlZNVZWaDZmDQorU25scUg4dHRMdk5lODYzVVdrWGVzZW5CWldNemdUZW1qRW1zWXF4WjJZZkNCOElHL1hiQTFFdkVOUjNwMkdJY0F1V3pOYlV2ZGVjDQo3OTdvZldKYlQ2dzlwRSs0TFJFK2tvSHNNODh4RTVOZmtNL1VZY1hDRC9SK2tQVnpxT21pSTdDWERmeDVvRHpNMGQ1YWFkclBBUjNGDQorc2kzS3FmaExRc0U1Q3ZpTXcrMUNNc01lZXFsa3ZpOThkcmNqUkF3bExGekVhcjRxbmtjMnFhckpOSklxM0VVVEcxV1IvVFJuT3hEDQpOUTlqayt3ZUFaakluMUNKNGJOQWxqMm54SEdBT1JPL1ZFY3JlMDg4eExwVTFJcFprV1VJM0pUeklNaVY3akwvQUU0KzBBTUoyTWhkDQplZjFCcjNucFQ0ZzNBUDdHRS9uVDVaVDh5dFBzUE1ma2k1ajFpWFRCSmJYMWxDdzlZSXg1S2ZUYml3WlRXcWtWTlJUUFJ0Tkx3end6DQoydDVITU9NWEhkNXQ1Ri9KTHpwcm11VzBPb2FWZGFacGlTSzE3YzNrVHdVaUJxd1JaQXJPekRZY2ZwMnpOeWFpRVkyQ0NYRGhqbk9WDQpVUUgxcDVqL0FPVWUxVC9tRW4vNU5ObWxoekR0cGNuemYvempINWM4djY1ZWVZb3RaMDIxMUZJNGJmMGhkUXBOdzV0SUdLRndlSk5PDQpvelk2eVpqVkZ4Y0VRYnRMUHFGdjVPLzV5T3RkTzBBbU96VFZMU0JJMUpZTEZmTEdKb3ExSklRVHN1L2hrcjQ4Tm51WTF3NU5sUmJLDQpIem4vQU01SFhXbitZSDlXekdwWGNCaFlrQm9iQlpQU2hGT2dZUWl2U3RUM09DK0REWTdrMXhaTjNxbjU1Zmw1NU1INWM2aHFOdnBsDQpycDE1cGFwTGFYRnRFa0Iza1ZQVGJnRjVLd2FnQjcwekcwMldYR0JmTnR5d0hDODA4dmFuZVhYL0FEakg1cXRaM0x3Mk4vYngydGYyDQpVZTV0WkNvOXViRS9UbVJNQVpoN3YxdFVUKzdMemVUek5xN2VUckx5eFUyMmovVzVieVNTalVtbFlLbnhVRzZ4QmVnN241Wms4QTR1DQpMcTFjUnFuMWRwZmxqeTk1Yy9KMi9zdENrVzVzNWRLdWJocjlmK1BsNWJaaVpqL3JkaDJGQm1xbE15eVdlOXpCRUNPejVRMGZ6UnJPDQpsK1Z0YzBteERSMm1zeVdrZC9jcldvU0VUTXNOZTNxOHlUNGhTT2xjMnNvQXlCUFJ3eElnRVBxRC9uSHZ5djVkMHJ5SGJhbnBzeVhsDQo5cW9FbW8zaWloVjFxUHE5RHVvaTZlNXEzUWpOWnFwa3lvOUhMd3hBaTlRekdibllxN0ZYWXE3RlhZcTdGWFlxN0ZYWXE3RlhZcTdGDQpYWXF4UDh5L1AxbjVKOHRTYW5JZ252Slc5R3d0U2FlcEtSWDRxYjhGQXEzM2RUbVJwc0J5U3JvNCtwMUF4UnZyMGZHL21UekxyZm1EDQpWNWRXMWE2ZTV2WlRYbVRRSUFhaFl3TmtWZXdHYm9ZeEVVT1RxNHpNalpmUnN1c05mSlllWUxTUm94cTFwRmRCMFBFaDVFNFRMVmZDDQpRTXB6eVR0L0RMQnJKR0pyaTMyK1IvSG0rZ2RsNUk1ZE9BZDYvdFFmUGFsZHZETkE3TjNMR2txMFdvTHBWaHFldU9RcTZUWnpYS0U3DQpBemNDa0MvTnBYV21iLzJiMHB5NnNIcEhmOUg0OXpxKzE4M0JnUG0rZC9MSG1YVy9MbXF4YXBvOTA5cmR4ZnRMOWwxN282blpsUGdjDQo5YWxqRXhSZUFNekUySDJYK1czbnl6ODYrV1lkVmlVUTNhSDBiKzFCcjZjNmdFMHIreXdQSmZiM0J6UzZqQWNjcWRwcDg0eVJ0UHRiDQp0NXJuUnIrMmdYbk5OYlN4eEpVQ3JPaENpcG9PcHltSm9odVBKOHUrVnZ5Mi93Q2NpUEtwdXo1ZjA4MkxYeXFseXkzR211V1ZLbGFHDQpTUnl0T1IrelROblBOaGx6UDN1SkdFeHlaMStVMzVGYTdwdm1VZWJmT2R3czJwUk8wMXZhaVF6dVozcldhZVhvU0trZ0FuZmV1MU1vDQp6NmtHUERIazJZOEpCc3RmbS84QWtwNWh2UE1vODUrU1hDNm9XU2E0czFjUXlldkhRQ2FCMktwVWdEa3BJMzMzclRIQnFBSThNdVM1DQpNUnV3eFRWUExmOEF6a1o1OVNEUnRkaWVEVG8zVjVYbkVGckRVYkI1UFNBZVNuZ0FmbGxzWjRZYmhnWTVKYkY2VHIvNVQzT21ma2pmDQplUy9Ma1IxRFU1ekJLN0ZraE04NHVZWkpYckl5b29DUjdBdDBBRzV6SGpudkx4SGsybkhVS0RHL0wvNUY2cnFINVJUNkJyMW91bitZDQo3ZTludk5Ka1o0NU9KZU9NQldlRnBCd2w0Y1dGZHRqVFlaWkxVZ1pMSEpoSEVUR2p6WC9saDVaL043UlBMR3RlVTlhMFZqcE41WlhRDQoweVg2MWFPWUxpV0poNmRGbUo5T1ZqOURiOXljYzA4Y3BDUU82Y2NaQUVFSWY4cC95UjFwUEx2bXZSUE9tbS9Vb2RXV3orb3lpV0NaDQowbGc5Zjk2bnBQSnhhTXlMMXBXdE9sY09mVUN3WW5rakhpTkVGdjhBS0R5WitiL2tMek5OWno2VjliOHIza3BqdTNqdWJYaUNwNHBkDQp4STBxdU52dEx4cVY3VkF4ejVNYzQzZnFYSEdVVDVQZjh3SEpkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJ3Yi9BSnlnDQowRFdwNGRMMXVLc3VrMmF2Qk9pLzdwbGxZRVNON1BRTFhzUVBITnQyYmtpTGoxTHFPMDhjaVJMK0VQbkdUTStUajRuc3Y1UGE2TlU4DQpxWG5sMlJxM3VpTTE5WUwzYTBtWWZXRUgvR09RaC84QVpIT045cXV6L0Z4REpIbkQ3dXY0OG5wK3c5VndUNER5a3liMWhublBDOWRiDQp2V0dQQ3RzVy9PWFhmMFo1WnNmTFViVXZkV1pkUTFGUnNWdG82cmJSdC9ydnlrcDdMbnBIc3IyZjRXTHhKZlZQN3VuNDgza08yOVZ4DQp6NFJ5aitQeDduamtmWE92aTgza2ZTZi9BRGkvNWYxdTFzdFUxbWNHSFNiOFJ4VzBiQTFsZUV0V1ZmOEFKWGtWcjNOZkROZjJqa2lhDQpqMURrZG00NUM1ZndsN0xyK3BuVE5KdUx4VjVTSUFJMVBUa3hDaXZzSzF6bnUwTlY0R0dVeHpITDNsM3VsdytMa0VVbnRQS1J2TGRMDQp2VkwyNWt2cGxEc3lQeENjaFVLb29lbWF6RDJQNHNSUE5PWnlTMzJOVjduTXlhL2dsdzQ0eDRSOXEzVjRicXgvdy9iUGRTVHNMMVZlDQpaaVF6cVpBUUczTmFBMHlPc2hQRjRFREl5UGlEZnYzNnAwOG96OFdRQUhvNWZCRytkNUpJL0xzN3hzVVlOSFJsSkIrMk80eks3ZGtZDQo2V1JCcmNmZTA5bWdITUw4MHMxM1V0VXNkZXNaTEZUTVRhQnByZnRJaWxpMzBnZE13ZGZxczJMVVFPUDFmdTl4M2dXNU9sdzQ1NHBDDQplM3EyUGNxK1lOYnQ5UjhveTNsbElWUEtNT29OSFJ1WXFwcGxuYU91am0wUm5qUFVlOGJzTkpwamoxQWpJZDZ2TkxML0FJdjB0T2JjDQpHdEdMTFUwSm8yNUdXem1menVJWHQ0Wi9Td2pFZmw1bitsK3BDMmR0UDVvdUxtNnU3aVdMVElaVEZiV3NSNGNpdFBpWS9UbEdIRkxYDQp5bE9jaU1VWlZHSTIrSmJja3hwUUl4QU15TEpLUC9RbDVwZW4zLzZPdTU1dVVKK3J3U25tVWNBN29mRWpvS1psL2taNmZGUHdweVBwDQoyQjNvK1RSK1pqbG5IamlCdnVRbFBsTzIwMmVTM3VJdFJuajFTTWszbHZJMjhoN3J4YnFQOCt1YTdzakZpbVl5R1NReWo2b2s4L2dlDQpuNDV1WHJwemlDREVjQjVIdVpyblZPa2RpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcWxkMmx0ZVcwdHJkUkpQYlRvWTVvDQpaQUdSMFlVS3NEc1FSaEJJTmhCQUlvdm12ODBmK2NkTlQwK1NYVlBKNk5mYWVhdkpwbGVWeENPdjdxdThxKzMydjlicm0wdzYwUzJsDQp6ZGZQU2NPOGVUeVB5MXIrcCtWZk1scnF0dWhTNnNaZjN0dklDb2Rmc3l3eUFqbzZrcWN5TWtCT0pCNUZqQ1JpYmU1NmxMWXZGYTZ0DQpwYm1UUnRWaitzV01oNnIya2hiL0FDNG5xckRQTE8xT3pUcDhwSDhKNVBjYUhWak5qdnFPYnRPdUxDQzN1OVoxVml1amFVbnJYaEhXDQpSaWFSd0pXbnh5dDhJeVhaUFpoMUdVRCtFYy8xSTErc0dISGY4UjVQQ3ZNR3Q2cjVwOHgzT3FYS21XKzFDYXFRUmd0U3Z3eHhScU42DQpLdEZVWjZqQ0loR2h5RHhFeVpHM3NINVcvd0RPT2VvM3NzT3JlY28yczdCYVBIcE5hWEV2Y2V0VCs2WHhYN1grcm1ObTFvRzBlZmV6DQp4Nlc5NWNuMHBiMjhGdEJIYjI4YXd3UXFFaWlRQlZWVkZGVlZHd0FHYXdtM1BBcENhNXBpNm5wYzlrVzRHUUFvM2d5bmt2MFZHK1llDQp1MG96NFpZKy93Qzl5Tk5tOExJSmR5UzJ2bUhWckMyU3p2dEp1WmJxRUJGa2dYbkhJRkZBZVE5dkROVmk3U3pZWWpIa3hUTTQ3WEVXDQpDNXM5Smp5UzRvVGlJbnY1aFQxOTlWbnRkR3ZYc0pXbmh1UlBOYXdxenNxcTFRRFFIcUJrTzBKWnB3dzVEQThVWjhSaU42cGxwUmpqDQpMSkVTRkdOQWxyVzc2LzFqeS9lUkpwbDFCS2pROEk1STI1UFY2bmlLVlBIanZnMTJmSnFkTk1ESE9KSER6QjMzNmU1T214UXc1b25qDQppUnYxUmMxcmRIelpwazRoY3dSMnJMSkx4UEJXbzJ4YWxBY3laNHAvbk1jcVBDSWM2MjZ0TVp4L0x6Rjc4U0E4MmVWSkRGUGVhVUNwDQprRmJ5MFRwSUFlWEpSL01EMi96T0gydjJRU0pUdzlmcWozOWI5LzQ5OStnMXdzUnlkT1I3a2ZMYTNKODJhWk9Jbk1DV3JLOG9VOEZZDQpodGkzUUhNeWVLWjFtT1ZIaEVPZlRxMFJuSDh2TVh2eElXM09wZVdycTVpRm5MZTZWUElaWVhnSEowWnVxc1B3ekh4K0xvWnlqd0dlDQpHUnNjTzVEYlBnMU1RZUlSeUFVYjZwaGI2MXJONWJYazF2cHJRR0pBYlJMbXF0SzNmYmJhbiszbVpqMXVmTENjbzR6R2g2ZUxxZngvDQphNDg5UGloS0lNN3ZuWFJJcmhyeldOVDArU0RTWmJLK2dsVjd1N2RDaWdLUlhjMDVmVHZtcHlHZXB5NHpIRktHU01nWlNxdjdmaTUwDQpCSERDUU14S0pHd1p6bld1amRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyRlBPZjVYZVN2T0NFNnhwNi9YS1VUDQpVSVAzVnl1MUI4WSsxVHNIQkh0bHVQTktISXNKWXhMbXd6US95UjFMUXJXKzBPRFZGdjhBeTVlRXoyaXpyd3ViTzdBb3NpY2FwSWpqDQo0WkI4UGFnOGF0ZkNHcHgxSVZMdmJOSk9XR2RqazdYZnlRMURYNGJIUmJqVTEwL3kxWUVUVEpBdk82dkx0aFI1bkxVU05WSHd4ajRxDQpEcjEyZEJHR214OE1SY2wxVTVacDJlVE5QSjM1WWVTdktLQTZQcHlMZGdVYS9tL2UzTFY2L3ZHK3pYd1NnOXN0eVpwVDVscmpqRWVUDQpLc3FadXhWMkt1eFYyS3V4VjJLb0dmWE5MdDc0V1UwM3B6MEJQSkhFWURLekRsTHg5TVZFYkdoYnRpcXV1b1dEQ3EzTVJCNkVPcC9sDQo5LzhBaXhmdkhqaXF6OUxhVng1ZlhZT0pYbUc5VktjUWVQTHIwNWJWeFZSbjh3YVJESk9qM0FaclpZMm5FWWFRcjZyOEVGRURIa1c3DQpkY1ZWN1BVN0M4aVdXM21WMWRpZ1UxVitTMXFwUnFNR0ZPaEZjVlJPS3V4VjJLdXhWMkt1eFYyS3V4VjJLdXhWMkt1eFYyS3V4VjJLDQp1eFYyS3V4VjJLdXhWMkt1eFYyS29UVnRXc05JMDZmVWRRbEVObmJnTkxJUVRTcENnVUFKSkxFQVlDYWFzK2FPS0JuTTFFUE41UHo0DQowOXJobHROSW1sZzI5TjVaVmpkaVJ2VkZXUURmL0tPWitqMGZqSDZ1SDRQTVp2YXFNVDZjWkk4elg2Q3pQeWg1MzBuelRGY215U1dHDQplektDNWduVUJoNmdKVmd5bGxaU1ZZZGE3YmdiWlhxdEpMQktwUFE2TFdRMUdNWkk4aW04T3A2YlBjdGF3M2NNdDBuUG5Ba2lOSVBUDQpLcTlWQnI4QmRRM2hVWml1V2w5M29taDZocU4yWkhWOVI5S0ZaQURHMHR1dngrbTZxUXhRdjhXN0NocFRwWEZVQmNlUi9MZ2REZFR5DQpocDVLQVBJaWVySzNOMm9BcWprNW94Qy95TFNuSEZVU1BKV2xDQm8vVWw1bjBxVDBoRGowWGQxMkVZUS8zaEJxdjQ3NHExSDVKMHhMDQplNXR4Tk42VnlGVXFSRFJBczdYRkZIcDBvWkhPekE3YmRNVlZ0SjhyV1dtWHd1WVdMS2tIb3hxd0FvelNOSTcwVUtnMlpWVUtvNHFLDQpkS0FLcDFpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJIdlBQbGF6OHlhRWJLDQo2a2xpV0tSYmlOb2lLOGxCWGNFR29vNXpFMXVTVU1SbEVXUTQycTBFTlZFWTVrZ1hlekE5SjB2US9KVXN0M0RFYjNVM1RoYXpUa0VRDQpuY0ZsVUFVcjBKNjloc1RtbHllMHVTTU9FZ1dlVmZwOG5PN0o5ajhHT2ZIRXlKNzVWL3NkdGo4LzFpZExIbURXL0wzbU9QU1ZqZ3Y3DQpsRXBjMnlKWnlTUHo1UEg2OFNvUXp4bGxEazFVdFd1WkhZL2FHcDFCbDR4TW94NWVYa0haZHA5bmFmVFY0VVJFeXNtdXZtVXNQa256DQovRlpTcnAxcGNXZHRQY1hNcjJMWHhtbUVVdDVieW9DNjNNUHFTZWxHNDNuQS9aNUVkZDY2bEI2ZjVYODdhVmVXTjFxMXc5aGVCYk1EDQpWcm5WRkVVVU9uM2x6UGNyUEQ2bytzaDdKMVVWNWNSL0x2aXF5THl4NXd0SXJKOWJ1UzkvWlhOdGNoYnJWdUllNGdndUV1cnVQbE01DQpXSVBMQ3o3Z2tEKzZYOXRWSGVRMzFmVGx2eHJ1cXhtMWtlMk1OcStxeDgvV0VCam1JYzNWeXkvdldqTkRKOFZRMUsvRG1Gck1jNUFjDQpOa0RtQWVIcHR2NUZ5dExrakVtNkhtUmZYdVpNbXBDN2E1bWgxZTJraDRreXl4YWhHeUNNelJxcjhoTHQwSzE0cnVhWnJKYVhVbVI1DQo3LzB2TWVmY080T2ZIVVlBQnkyL28rUjh1L3pLYmFOZldyK1lUQkJxa0Z5dEhNY0tYYVNzWWVDbEFJdWJzU3FrRW5pT3RhbXVabURUDQo1WVpyTjhHL1hwMEZYekg0SnR4YzJiSExGVzNGN3V2WDhmWXl6Tm82OTJLdXhWMkt1eFYyS3V4VjJLdXhWMkt1eFYyS3V4VjJLdXhWDQoyS3V4VjJLdXhWMkt1eFYyS3V4VjJLcFJkK1V2THQzSVpKN0pDeDY4QzhZKzVDb3pXejdIMHNwY1hBTDhySDJEWnpjZmFPZUFvUy9UDQo5NloyOXRiMjBLdzIwU1F3clhqSEdvUlJVMU5BS0RybXdoQ01CVVJROG5Fbk9VamNqWjgxVEpNVUJyR2dhUnJNVWNPcVd5M2NNUlprDQppa0xjS3NwUWtxQ0FmaFkwcjA3WXFwbnl6b1pFL3dEbzNFM0ptYVZsZVJXSnVDaGxJS3NDcFl3cDltbEtiWXFscWZsdjVJanVGdVk5DQpLampuVjFsRG84aW5tamlSVzJZYnF3RkQ0Q25UYkZVUkQ1Rzhyd3RJME5rWTJrUVJzVm1tQkNySXNvNDBmNGZqalZxclRwaXJ0SzhqDQplVTlKMUJkUTAzVG83UzdSR2pXU0puQTR2UUVGZVhFOUIyeFZQY1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpDQpyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyDQpzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzDQpWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWDQpkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkDQppcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpDQpyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyc1ZkaXJzVmRpcnNWZGlyDQpzVmYvMlE9PTwveG1wR0ltZzppbWFnZT4NCgkJCQkJPC9yZGY6bGk+DQoJCQkJPC9yZGY6QWx0Pg0KCQkJPC94bXA6VGh1bWJuYWlscz4NCgkJPC9yZGY6RGVzY3JpcHRpb24+DQoJCTxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyI+DQoJCQk8eG1wTU06SW5zdGFuY2VJRD54bXAuaWlkOkQxNkFENTEzMkJEMEU3MTE4Q0YwOEE2MDhFM0ExMzY5PC94bXBNTTpJbnN0YW5jZUlEPg0KCQkJPHhtcE1NOkRvY3VtZW50SUQ+eG1wLmRpZDpEMTZBRDUxMzJCRDBFNzExOENGMDhBNjA4RTNBMTM2OTwveG1wTU06RG9jdW1lbnRJRD4NCgkJCTx4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ+dXVpZDo1RDIwODkyNDkzQkZEQjExOTE0QTg1OTBEMzE1MDhDODwveG1wTU06T3JpZ2luYWxEb2N1bWVudElEPg0KCQkJPHhtcE1NOlJlbmRpdGlvbkNsYXNzPnByb29mOnBkZjwveG1wTU06UmVuZGl0aW9uQ2xhc3M+DQoJCQk8eG1wTU06RGVyaXZlZEZyb20gcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPg0KCQkJCTxzdFJlZjppbnN0YW5jZUlEPnhtcC5paWQ6RDA2QUQ1MTMyQkQwRTcxMThDRjA4QTYwOEUzQTEzNjk8L3N0UmVmOmluc3RhbmNlSUQ+DQoJCQkJPHN0UmVmOmRvY3VtZW50SUQ+eG1wLmRpZDpEMDZBRDUxMzJCRDBFNzExOENGMDhBNjA4RTNBMTM2OTwvc3RSZWY6ZG9jdW1lbnRJRD4NCgkJCQk8c3RSZWY6b3JpZ2luYWxEb2N1bWVudElEPnV1aWQ6NUQyMDg5MjQ5M0JGREIxMTkxNEE4NTkwRDMxNTA4Qzg8L3N0UmVmOm9yaWdpbmFsRG9jdW1lbnRJRD4NCgkJCQk8c3RSZWY6cmVuZGl0aW9uQ2xhc3M+cHJvb2Y6cGRmPC9zdFJlZjpyZW5kaXRpb25DbGFzcz4NCgkJCTwveG1wTU06RGVyaXZlZEZyb20+DQoJCQk8eG1wTU06SGlzdG9yeT4NCgkJCQk8cmRmOlNlcT4NCgkJCQkJPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+DQoJCQkJCQk8c3RFdnQ6YWN0aW9uPnNhdmVkPC9zdEV2dDphY3Rpb24+DQoJCQkJCQk8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOkQwNkFENTEzMkJEMEU3MTE4Q0YwOEE2MDhFM0ExMzY5PC9zdEV2dDppbnN0YW5jZUlEPg0KCQkJCQkJPHN0RXZ0OndoZW4+MjAxNy0xMS0yM1QxNDoxODoyOSswNTozMDwvc3RFdnQ6d2hlbj4NCgkJCQkJCTxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIElsbHVzdHJhdG9yIENTNiAoV2luZG93cyk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+DQoJCQkJCQk8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPg0KCQkJCQk8L3JkZjpsaT4NCgkJCQkJPHJkZjpsaSByZGY6cGFyc2VUeXBlPSJSZXNvdXJjZSI+DQoJCQkJCQk8c3RFdnQ6YWN0aW9uPnNhdmVkPC9zdEV2dDphY3Rpb24+DQoJCQkJCQk8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOkQxNkFENTEzMkJEMEU3MTE4Q0YwOEE2MDhFM0ExMzY5PC9zdEV2dDppbnN0YW5jZUlEPg0KCQkJCQkJPHN0RXZ0OndoZW4+MjAxNy0xMS0yM1QxNDoxODo1NCswNTozMDwvc3RFdnQ6d2hlbj4NCgkJCQkJCTxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIElsbHVzdHJhdG9yIENTNiAoV2luZG93cyk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+DQoJCQkJCQk8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPg0KCQkJCQk8L3JkZjpsaT4NCgkJCQk8L3JkZjpTZXE+DQoJCQk8L3htcE1NOkhpc3Rvcnk+DQoJCTwvcmRmOkRlc2NyaXB0aW9uPg0KCQk8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczppbGx1c3RyYXRvcj0iaHR0cDovL25zLmFkb2JlLmNvbS9pbGx1c3RyYXRvci8xLjAvIj4NCgkJCTxpbGx1c3RyYXRvcjpTdGFydHVwUHJvZmlsZT5QcmludDwvaWxsdXN0cmF0b3I6U3RhcnR1cFByb2ZpbGU+DQoJCTwvcmRmOkRlc2NyaXB0aW9uPg0KCQk8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczpwZGY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGRmLzEuMy8iPg0KCQkJPHBkZjpQcm9kdWNlcj5BZG9iZSBQREYgbGlicmFyeSAxMC4wMTwvcGRmOlByb2R1Y2VyPg0KCQk8L3JkZjpEZXNjcmlwdGlvbj4NCgk8L3JkZjpSREY+DQo8L3g6eG1wbWV0YT4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgPD94cGFja2V0IGVuZD0ndyc/Pv/iDFhJQ0NfUFJPRklMRQABAQAADEhMaW5vAhAAAG1udHJSR0IgWFlaIAfOAAIACQAGADEAAGFjc3BNU0ZUAAAAAElFQyBzUkdCAAAAAAAAAAAAAAAAAAD21gABAAAAANMtSFAgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEWNwcnQAAAFQAAAAM2Rlc2MAAAGEAAAAbHd0cHQAAAHwAAAAFGJrcHQAAAIEAAAAFHJYWVoAAAIYAAAAFGdYWVoAAAIsAAAAFGJYWVoAAAJAAAAAFGRtbmQAAAJUAAAAcGRtZGQAAALEAAAAiHZ1ZWQAAANMAAAAhnZpZXcAAAPUAAAAJGx1bWkAAAP4AAAAFG1lYXMAAAQMAAAAJHRlY2gAAAQwAAAADHJUUkMAAAQ8AAAIDGdUUkMAAAQ8AAAIDGJUUkMAAAQ8AAAIDHRleHQAAAAAQ29weXJpZ2h0IChjKSAxOTk4IEhld2xldHQtUGFja2FyZCBDb21wYW55AABkZXNjAAAAAAAAABJzUkdCIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAA81EAAQAAAAEWzFhZWiAAAAAAAAAAAAAAAAAAAAAAWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPZGVzYwAAAAAAAAAWSUVDIGh0dHA6Ly93d3cuaWVjLmNoAAAAAAAAAAAAAAAWSUVDIGh0dHA6Ly93d3cuaWVjLmNoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALklFQyA2MTk2Ni0yLjEgRGVmYXVsdCBSR0IgY29sb3VyIHNwYWNlIC0gc1JHQgAAAAAAAAAAAAAALklFQyA2MTk2Ni0yLjEgRGVmYXVsdCBSR0IgY29sb3VyIHNwYWNlIC0gc1JHQgAAAAAAAAAAAAAAAAAAAAAAAAAAAABkZXNjAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAsUmVmZXJlbmNlIFZpZXdpbmcgQ29uZGl0aW9uIGluIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdmlldwAAAAAAE6T+ABRfLgAQzxQAA+3MAAQTCwADXJ4AAAABWFlaIAAAAAAATAlWAFAAAABXH+dtZWFzAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAACjwAAAAJzaWcgAAAAAENSVCBjdXJ2AAAAAAAABAAAAAAFAAoADwAUABkAHgAjACgALQAyADcAOwBAAEUASgBPAFQAWQBeAGMAaABtAHIAdwB8AIEAhgCLAJAAlQCaAJ8ApACpAK4AsgC3ALwAwQDGAMsA0ADVANsA4ADlAOsA8AD2APsBAQEHAQ0BEwEZAR8BJQErATIBOAE+AUUBTAFSAVkBYAFnAW4BdQF8AYMBiwGSAZoBoQGpAbEBuQHBAckB0QHZAeEB6QHyAfoCAwIMAhQCHQImAi8COAJBAksCVAJdAmcCcQJ6AoQCjgKYAqICrAK2AsECywLVAuAC6wL1AwADCwMWAyEDLQM4A0MDTwNaA2YDcgN+A4oDlgOiA64DugPHA9MD4APsA/kEBgQTBCAELQQ7BEgEVQRjBHEEfgSMBJoEqAS2BMQE0wThBPAE/gUNBRwFKwU6BUkFWAVnBXcFhgWWBaYFtQXFBdUF5QX2BgYGFgYnBjcGSAZZBmoGewaMBp0GrwbABtEG4wb1BwcHGQcrBz0HTwdhB3QHhgeZB6wHvwfSB+UH+AgLCB8IMghGCFoIbgiCCJYIqgi+CNII5wj7CRAJJQk6CU8JZAl5CY8JpAm6Cc8J5Qn7ChEKJwo9ClQKagqBCpgKrgrFCtwK8wsLCyILOQtRC2kLgAuYC7ALyAvhC/kMEgwqDEMMXAx1DI4MpwzADNkM8w0NDSYNQA1aDXQNjg2pDcMN3g34DhMOLg5JDmQOfw6bDrYO0g7uDwkPJQ9BD14Peg+WD7MPzw/sEAkQJhBDEGEQfhCbELkQ1xD1ERMRMRFPEW0RjBGqEckR6BIHEiYSRRJkEoQSoxLDEuMTAxMjE0MTYxODE6QTxRPlFAYUJxRJFGoUixStFM4U8BUSFTQVVhV4FZsVvRXgFgMWJhZJFmwWjxayFtYW+hcdF0EXZReJF64X0hf3GBsYQBhlGIoYrxjVGPoZIBlFGWsZkRm3Gd0aBBoqGlEadxqeGsUa7BsUGzsbYxuKG7Ib2hwCHCocUhx7HKMczBz1HR4dRx1wHZkdwx3sHhYeQB5qHpQevh7pHxMfPh9pH5Qfvx/qIBUgQSBsIJggxCDwIRwhSCF1IaEhziH7IiciVSKCIq8i3SMKIzgjZiOUI8Ij8CQfJE0kfCSrJNolCSU4JWgllyXHJfcmJyZXJocmtyboJxgnSSd6J6sn3CgNKD8ocSiiKNQpBik4KWspnSnQKgIqNSpoKpsqzysCKzYraSudK9EsBSw5LG4soizXLQwtQS12Last4S4WLkwugi63Lu4vJC9aL5Evxy/+MDUwbDCkMNsxEjFKMYIxujHyMioyYzKbMtQzDTNGM38zuDPxNCs0ZTSeNNg1EzVNNYc1wjX9Njc2cjauNuk3JDdgN5w31zgUOFA4jDjIOQU5Qjl/Obw5+To2OnQ6sjrvOy07azuqO+g8JzxlPKQ84z0iPWE9oT3gPiA+YD6gPuA/IT9hP6I/4kAjQGRApkDnQSlBakGsQe5CMEJyQrVC90M6Q31DwEQDREdEikTORRJFVUWaRd5GIkZnRqtG8Ec1R3tHwEgFSEtIkUjXSR1JY0mpSfBKN0p9SsRLDEtTS5pL4kwqTHJMuk0CTUpNk03cTiVObk63TwBPSU+TT91QJ1BxULtRBlFQUZtR5lIxUnxSx1MTU19TqlP2VEJUj1TbVShVdVXCVg9WXFapVvdXRFeSV+BYL1h9WMtZGllpWbhaB1pWWqZa9VtFW5Vb5Vw1XIZc1l0nXXhdyV4aXmxevV8PX2Ffs2AFYFdgqmD8YU9homH1YklinGLwY0Njl2PrZEBklGTpZT1lkmXnZj1mkmboZz1nk2fpaD9olmjsaUNpmmnxakhqn2r3a09rp2v/bFdsr20IbWBtuW4SbmtuxG8eb3hv0XArcIZw4HE6cZVx8HJLcqZzAXNdc7h0FHRwdMx1KHWFdeF2Pnabdvh3VnezeBF4bnjMeSp5iXnnekZ6pXsEe2N7wnwhfIF84X1BfaF+AX5ifsJ/I3+Ef+WAR4CogQqBa4HNgjCCkoL0g1eDuoQdhICE44VHhauGDoZyhteHO4efiASIaYjOiTOJmYn+imSKyoswi5aL/IxjjMqNMY2Yjf+OZo7OjzaPnpAGkG6Q1pE/kaiSEZJ6kuOTTZO2lCCUipT0lV+VyZY0lp+XCpd1l+CYTJi4mSSZkJn8mmia1ZtCm6+cHJyJnPedZJ3SnkCerp8dn4uf+qBpoNihR6G2oiailqMGo3aj5qRWpMelOKWpphqmi6b9p26n4KhSqMSpN6mpqhyqj6sCq3Wr6axcrNCtRK24ri2uoa8Wr4uwALB1sOqxYLHWskuywrM4s660JbSctRO1irYBtnm28Ldot+C4WbjRuUq5wro7urW7LrunvCG8m70VvY++Cr6Evv+/er/1wHDA7MFnwePCX8Lbw1jD1MRRxM7FS8XIxkbGw8dBx7/IPci8yTrJuco4yrfLNsu2zDXMtc01zbXONs62zzfPuNA50LrRPNG+0j/SwdNE08bUSdTL1U7V0dZV1tjXXNfg2GTY6Nls2fHadtr724DcBdyK3RDdlt4c3qLfKd+v4DbgveFE4cziU+Lb42Pj6+Rz5PzlhOYN5pbnH+ep6DLovOlG6dDqW+rl63Dr++yG7RHtnO4o7rTvQO/M8Fjw5fFy8f/yjPMZ86f0NPTC9VD13vZt9vv3ivgZ+Kj5OPnH+lf65/t3/Af8mP0p/br+S/7c/23////bAEMAAgEBAgEBAgICAgICAgIDBQMDAwMDBgQEAwUHBgcHBwYHBwgJCwkICAoIBwcKDQoKCwwMDAwHCQ4PDQwOCwwMDP/bAEMBAgICAwMDBgMDBgwIBwgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAS4CKQMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP38ooooAKKKKACiiigAooooAKKKKACiiigAoooNABWH4t+Iuk+CIFfUrqOBpMmOL70kmOuFHOPfpXL/AB++OVv8ItBhWNY5tY1DctpAx+VQMbpX/wBlcjjuTj1I+WdT8d3WtajNeXlxLc3Vw26SV2yzH/AdgOBX4X4qeMlPhyp/ZmWQVXFWu735Kaeq5krOUmtVFNWTTb2T+syHhWrj4fWKl40+nd+nl599PT6O1L9qG3EzCz0xmXPDTzbSfwAP86rQftQkP++0uFl/6Z3BB/Va+dP+Epb+81H/AAlLf3mr+Za3jNxzOr7VY5x8lTpW+5wf43fmfZR4NwaVuT8X/mfXfg747aH4rnjtvO+xXchCrFcELvJ7K3Qn24J9K7RG3LXwe/ibzEKsdytwQehr2n9m39o521e18N63cGWO4IisLqV8sjdBE5PUHopPOeOcjH7h4Z+PFXH4qGVcRxjGc2lCrHROT2U47Jt6KS0u7NLc+czrg2dCk8RhbtLVrrbuu9u259EUUiNvXNLX9PHwgUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU132496dVTXJGh0m5kX7yQuR9dpqZy5YuT6DSu7Hwh8dvim3xG+LGsagJG+zRzG1tRn7sUZKjH1OW/4FXJf2r/tGuZ02+abTrd2bczxKxPqSASan+1N61/mVmWMrZhi6uOxGs6knJ+snf8A4Y/qjDZZDD0o0Ka0ikl8tDf/ALV/2jR/av8AtGsD7U3rR9qb1ri9ib/VUb/9q/7RpH1Tcv8ArHU9QynDKexB7EdQawftTetH2lj3o9iH1VH6JfAD4jP8UPhPo+rzbWu5ojFdEdDMh2Ocdskbsds12leB/wDBPO7kufgzfK2SsOrSqnsCkZP6mvfK/wBGuB8yq5hw/g8ZXd5ypx5n3aVm/m1c/mbPsJDC5jWw9PaMnb0vovkFFFFfVHkBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFR3EPnrtYblYEMPUGpKKAPzB8e+Epvh7461jQZ48SaTdyW4HTKBvkI9ipU1k7m/wCef619Y/t7/s63WuIvjjQ7dpri0h8rV7eNcvNEv3Z1A5JQZDDnKlSMbDu+So52kC7WB3DIIGa/z3464Vr8P5vVwU4vkbcqb6ODelvNbPzT8j+pOGs4p5pgIYiL96yUl2kt/v3XkyTc3/PP9aNzf88/1pMv/eH/AHzRl/7w/wC+a+PPe5Rdzf8APP8AWjeV/h2++aazuo+9/wCO16L+zJ8ALr9oHx2tvMki+HdOdZNUnAwHXqLdT/efocfdXJ9M+llOU4rM8ZTwGCjzVKjsl+bfZJat9ErnJjsZQweHlicRK0Yq7/y9Xsl1Z9Z/sR+DJfCP7PmjSTIyTasz6kwPB2yH5PzQKfxr16o7WBbW3WONFjRAFVVGFUDgADsAKkr/AESyPK4ZZl1DL6buqUIxv3skr/Pc/lTMcZLF4qpip7zk36Xe3yCiiivUOMKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKRn2DmgBaK8f/am/b2+EH7F2nQzfEvx9oXhWa6RpLaznkMt7dKOCY4Iw0jDkchcV8h6z/wAHRv7Mel6l5EMfxO1KHdj7TbeGcRfX55Fb/wAdrsoZfiq0ealTbXdJ2OepiqNN8s5JP1P0cor5O/Zl/wCC237Nn7VetwaX4f8AiPY6VrV0wSHTPEEL6TcSsegTzgEcn0Via+sA+Tisa1CpSly1YuL81Y0p1ITXNBpryFooorE0CiiigAooooAa6byPavnH48/sA6f42vptW8I3NvoGozEvLZSRk2Nwx6ldvMLHrkBl/wBkda+kKK8HiLhjLc8w31XM6SnHddHF94tap+m+zuj08qzjGZdW9vg58r69U12aej/TofnR4l/ZV+InhG6KXHhfULpQf9bZbbmNv++Tn8wKz9P/AGf/AB5rMwjtvCHiAsTj57Uxj82wK/SXyVBzyO9DRBvWvx2p9HnJ3U5oYmqo9vcb+/l/Q+9j4qZgoWlRg33978r/AKnxr8J/+Cduu69dx3PjC+j0fT1OWs7NhNdzD0L/AHIx7jeev3etfWngjwLpXw88N2uk6PYw6fYWgxHDEPzZj1Zj3YkknrWuq7BgUtfp/CfAeT8Oway6n77VnOTvN+V+i8opLra58dnfE2PzWSeLn7q2itIr5dX5u78wooor7E8AKKKKACiiigAopGcLXkn7UX7dnwj/AGNNJhuviX490Hwm10pe3trmbfeXKjvHAgaVx7hcVdOnKcuWCbfZEykoq8nZHrlFfnHrv/B0X+zHpOpfZ7dfiZqkW7H2m18NERY9R5kit/47Xsv7NX/BcP8AZp/aj1u30vRfiPZ6NrN0wSHTvEVu+kzSseiqZgEZvZWJrrqZXi4R5505JejMI4yhJ8sZq/qfW9FNDgtjvTq4TpCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK/Ov/guZ/wWPuP2CvD1p8PfhzJZ3XxY8SWxumu5kWaDwpZk4W4kjPElxIdwijIKgIzv8oVZP0K1vWLfQNKuL67mS3s7OJ555XOFjjRSzMT6AAmv5I/2l/j1qv7V3x58XfErWmla+8aalLqQSQ820DHEEA9BHCI0x/s19Rwtk8MbXlUqq8IW07t7L00bPneIs2eEpRhD4p/glucP4y8V6x8Q/FuoeIPEWrap4g8QatKZr3U9Tunury7c9TJK5LMfQdAOAAABWbjzEqaaP/8AXULja2elfpUo8uiPk6NTmV2QzW63URSVVkVuCrjIP4V+mX/BEn/guPrv7MHjfSPhZ8XvEF5rXwt1aWOw0rWdTuTNceDpSdsYeVyWexJKqVY/uMAqQmVH5oyDZk1HNGtxC0ciRyRyAq6MMqwPBBHcH0rz8dgaWJpOlWWn4rzR6GHxE6M/aQZ/ZpGcj6HFOr4q/wCCAf7Vmo/tUf8ABNfwjNrl7JqPiLwPNL4S1C6ml8ya5+yBRbySMRlpGtmgLMSWY5YkljX2rX5HiKEqNWVGW8W0fdUaiqQU47NBRQTgVzvxH+KWh/CnQm1LXtSt9PtRwm85eZv7qKOWPsBXHicTRw9KVevJRhFXbbSSXdt6I6KNGpVmqdJOUnskrt+iOgaQKeazfFPjTSfBen/atW1Kx0235xJczLErfTJ5/CvkP4w/8FDdd8TvJa+ELX+wbLlRe3CLLeSj1CnKR/Q7j9OlcL+z/wDCPUv2pvi239tahqWoWNii3OrX1xO802wkhIVdiSrOVYDGAqqxGMAV+M5h4zYWvjoZVw5ReJrTfKm3yQv3vZyaSu27JWV02j9Awvh/XpYaWOzaoqNOKu0vel+dk3stW76WPt74ffF/SPiqbiTQJLnULG3JQ3y27Jau4OCqO2N5HfbkD1rqklDDrz3rhviV8TvDP7NPw6huLlY7OxtlFrYafaooeZgOIol4HTkk8AcmvjP4rfth+N/ireyLHqVx4b0tiQlnpczQtj/bmGHY49Cq+1e9xR4lYHhmjChmcva4pq7hTVlr1d2+WPa7cnukzy8l4RxOcVJVMGuSinbmm7/LRK79Ektrn3zq3ijTdBK/btQsbLd0+0TrHn/voiobDxxouqziK11fS7mRuiRXcbsfwBr8uprVbmVpJP3rscln+ZifcnmkGnxf880/BRX5XL6Rlbn93AK3/Xx3/wDTf6H2y8J4cuuKd/8AB/8AbH6slwKqa1qv9kaZLciC5uVhXeY7ePzJGH+yvc+w5r82fBHxm8XfDCWN9D8SatZxoc/ZmnM1q31hfKc+oAPvX11+yz+2Lb/Gq4XQdbhh03xGib4zGf8AR9QCjLFAeVcdShzxyCecfoXCPjHlOe11gZp0K0tIqVnFvspK2vZNRvsrvQ+Vz7gHHZbTeJi1VprdrRpd2u3mm7dT0nwH8cvCvxIkEej69YXdwG2tbl/KuEYZBUxthwRg5GMjFdaJVNfKP7f/AOzxapZ/8J5pNvHFNFIsesLGuPODEKlx/vKcKx7gg9q8p+FH7ZXjj4WSxxvqT+ItMTA+yapI0rKPRJuZF/EsBxx2rnzDxYlkWbSyniOhy7ONWndxlF7S5H7y2aaUpNNNJNavXC8DrMsEsdlFS/Rwno01uuZaPurqKs1tsv0Gory34G/tZ+GfjkUtbWWTTNa27n027IEjevlsOJAOenOOoFeoqwbpX6vlWb4LMsOsXgKiqU3s07/J9U11Ts11R8RjcDiMJVdDEwcZLo/61XmtBaCcCiqus6vb6DpN1fXkiw2tnC888h6RoqlmP4AGvROU/Pz/AILkf8FlD/wT78J2fgXwA1nffF/xRbG5jkmRZrfwvZElReTIeHldgywxHglGdvlUB/56vGvjHWviX4y1DxH4k1jVPEXiLV5TNfapqd011eXbnu8jEk+gHAUAAAAAV137Vn7R+rfteftLeOPiZrU0j3fjDVp72GN33fY7QNstbdT/AHY4FjQf7pJ5JNcCmX9vev1fJ8rhg6KSXvvd+fb0R8Tj8ZLEVL/ZW3+Y3cQetD2q3cbJKqyRt1VhuB/OpAAn1xmgHdXsKJ5zkfpB/wAEWf8AguRr37JHjLRvhn8WNcu9a+EOpSJY2OqX8xluvBbsQkZ81iS1j0VkYnyRhlIRWWv6DkbctfxpG3WZGR1DKw2src7h0wfav6T/APg3u/abv/2kf+CbfhiHWLqa81rwBcS+FLmeZ98k8VuFNszHqSIHiXJ5OzueT8NxVlMKaWMpK13aXz2f+Z9HkeYOcnh5vpdf5H3BRRRXxJ9IYfxN+IGn/Cf4c694o1YzjS/Dmnz6neGGPzJBDDG0j7V/ibapwO5r4Di/4OiP2XpolZZviRtYAj/il3+v9+vsb9uL/ky/4tf9idq3/pHLX8i+muf7Nt/+uS/yFfUcP5PQxsJyrX0a2dv0PHzTH1cPKKp217n9Ei/8HQf7L5P+u+JA+vheT/4utLSP+Dmn9lfU3VZdc8aafnqbnwzcAD/vndX86G9vajef9mve/wBVMF3l96/yPL/tvEeX3f8ABP6bfBn/AAXw/ZP8bXMUUXxe0rTZJun9p2N1ZKv+80kYUfia+nvhX8bfB/xx8O/2t4N8UaB4q035d1xpN/HdxpuGQGKE7SR2ODX8eZYsMGt74UfFXxV8BfG1v4l8C+J9e8F+IbU/u9R0S9eznIyDtfYdskZxzHIGRhwykcVy1+EaTj+5m0/Oz/KxtRz2d/3kVbyP7FQ26lr8nf8Agj3/AMHDa/tC+KdI+FfxzOnaT411FktNE8UwItvY+IJiQq29xH92C6cn5Sv7uUnACNtVv1hRty/zr4/G4GthKnsqys/wfoe/h8RTrQ56bFoorz39qT9qPwV+xz8Eta+IHj7WItH8O6JGDI2N811K3EdvBH1kmkb5VQdTycAEjljGU5KMVds2lJJXex3V/qlvpVlPdXU8NtbWsbSzTSuEjiRRlmZjwFABJJ4Ar4d/ad/4OIP2a/2btSuNMtvEmp/ETWbZ2jktfCVoL6ON14KtcMyQfiHPSvxn/wCCl3/BYf4nf8FIvEl5p95d3nhD4XLMTY+ELK5Kx3CK2Uk1B1I+0y9DsOYUYDapZQ5+TUHlKFXaqjoMYAr7XL+E1y8+LevZfq/8vvPnsVnbvy0F82ftV4h/4O5tEj1Bl0f4B+JLqz/hk1HxVa2czfVI4ZlH/fddJ8Mf+Ds/4Z63eRw+MvhN8QvC8bNhrjTru01iGMeuN0Uh+gSvwyLH/ZpN5/2a9eXDWXtWUGvm/wDM4VnGKTvf8Ef1ffsi/wDBSj4LftxW+34c+PNJ1jUljEs2kTE2mqW4xk7raTDkDnJUED1r3VXDjiv40tL1K60LWrPUtPvL3TdT0+VZ7S9sriS2urSRTlZIpY2V43B5DKwI7Gv2a/4Iyf8ABwjqHizxPo/wl/aC1SGe+1F0svD/AI3m2RG6mJ2pbajjCB24VJ1ADNgOATvPzmacMzoRdXDPmit091/metg84jUahV0ffp/wD9lKD0oBzRXyh7R8KfHf/g4d/Z5/Zy+Nfir4f+JJvHn/AAkHg3Un0rURaeHnngEyBSdjhvmXDDmuUH/B0J+y+f8Alr8SP/CXk/8Ai6/GH/grAxH/AAVD/aC/7HW5/wDRUNeAq53Dp1r9Cw/DODnRhOXNdpPfuvQ+XrZxXhUlGNrJ9v8Agn9d/wCyr+014Z/bF+Anh34keDW1FvDfiiF5rI31sba42pI8Z3Rknb8yN36Yr0Ovjr/ggL/yiL+DX/YOuv8A0tuK+jv2jf2g/DP7LHwQ8SfEHxhfrp/h3wvZteXcnBeTHCRIP4pJHKoq92YCvh8TQ5MROjT1tJpd97I+io1L0lUl2Tf3HlX7ev8AwVJ+En/BOOPw6vxH1TVFv/FLyiw0/SbE3140UQHmTvGpGyIMVTcTyzgAHDY+df8AiKE/Zf8A+evxI/8ACXf/AOLr8Nf21f2vvFH7dn7S/iT4m+LHaK81uXy9P08Sb4tFsEJFvZx9vkU5ZgBvkaR8fNgeVvN5aMzMqqoyWY4CjuSfQV9theFMP7Je3b5utnp6bdD52tndXnfsrcvS6P6K/DH/AAcvfs4+N/E+m6Jo1t8UtU1jWrqKxsLK28KSPNdzyMFSNBv5Ykgfr0r7h+LfxY0v4IfCLxF41177Wmj+F9Nn1a+EEXmzrDDGZH2oD8zbVPAPWvyv/wCDcH/gk83gDQbD9or4h6aV17xBZlvBOnXUWG0yxlUZ1FlYfLNOnEeeUhcngykD9Cv+Ckq7f+CfHxs/7ErVv/SSSvl8ww+Ehi1Qw12k7Nt31v006HtYWpXdB1K1k90j5VT/AIOh/wBl6RAyzfEjDc/8iu//AMXWh4W/4OY/2Z/F/ivSdGs5viJ9s1q/t9Ot/M8NOiebPKsSbjv4G51yewr+dC0JW1j/AN0fyrrPgYxPx6+H3/Y2aN/6cLevrqnC2CUW1zfev8jwqedYhySdvu/4J/YTGc5+tKxwtIp+99a+L/8Agtr/AMFO7b/gnV+zK0ehT29x8UfHAk07wvakhvsXy/vtRlX/AJ5QKRgH78rRJjBYr+f4fDzr1FRpq7Z9RVqRpxc57Ir/ALTn/Bf79nf9lH44698PfEWq+KdQ8QeGZFt9ROjaK17bW8xUM0JkDAGRMgMo+6eDyCBwf/EUL+y/j/XfEj/wl3/+Lr+eW6vbnUL24uru6uL69vJnuLq5uZDLNdTOxeSWRycs7sSzMeSSTWl4E8D638UvHGj+GPDem3Gs+IfEV5Hp+m2FuMyXc8h2og9PUk8AAk8A199HhXBRh77ei1d0vXofMSzrEOXupfcf0w/sW/8ABav4P/t+fGc+BfhzZ/EC+1eGxk1K7nu/D7W1nYW6YAeaUthNzkIo5LNkAcEj69HSvln/AIJM/wDBNXQ/+Ca/7NFv4dja11Hxz4g8vUPF2sxKcX95twIYyeRbwAmOMHGRucgM7V9TDgV8LjfYKs1hr8i2v18z6TD+09mnV+LyCiiiuU2CiiigDzn9sPTbvWf2SvijZ2Cu19deEdWhtlT7zSNZTBQPfcRX8mYgV7G3ZfuNEpUjuMDGK/sQmiEqbWAZTwQR1Ffyy/8ABQL9ka9/Yo/ar8W/D+4hePT9NumuNGlZSq3OmyktbsueuF+Q9fmjYdq/SPD+tB+2wz+J2kvNK6f3XR+a+IFOcHQxS+FNxflezX36nz1dR4qpIPwz1xWtewYzWdcR4J9K+1r07Hi4DEKSRWU5Xa1MPy8ZPFSMMNn0qOY8fLHNNIzBUiijMkkrMQFRFHLMxIAUckkAda4ZI9yDvsfu/wD8GnVndR/sZ/EyeQt9ln8fP5II7rptkHI/HA/A1+qlfMP/AAR7/Y3vf2HP2AfA/gvWolh8VTRPrXiCIMGEF/dt5ssORw3lArFkEg+USOCK+nq/IM0rxrYupUhs3p+R99g6bp0Iwe9jzP8AaV/aS079nvw3HJJGL7WdQVhYWIbb5hGMu5/hjXIyepzge3wb4/8AiBrfxX8SSax4gv5L69f5Uz8sVun9yJOiL7Dr1JJ5r3L/AIKWnPxU8MDt/ZUv/o4V87k4Ffw740cVZhjM8rZTOdqFFpKK0TfKm5S7u70volsrtt/0d4e5LhcPltPHRjepUTu3uldqy7LTXu9+lrGiaFfeKdes9L022kvNQ1CVYLeBPvSMf0A7kngAE19//stfAkfAH4cnTbme2utVvpzd3s8KkIzkBQqk8lVUAAnGeeBXiH/BN34fQahqviHxTcxrJNYhNNsiRnyiw3zMPcjy1z1xuHQmov2xf2qPEmi/FT/hH/C+qzaNb6Dta6lhVWa7nIDbW3A/u1BA29yTntX0nh7hss4UyOPGOaqUqlVuFOMbXSbadk2ld8sm23pFWWrd/I4qrYzO8yeQYJqMYLmm3s3ZPW13ZXSsl8W+i0i/4KE/DXxVN4ph8VXEiX3hi1iW0gWIHOlFiNxkU8fvHx84/uqpxha+b42+Uetff37Ofxksf2nfhZc/2jb2xvYQbHWLEjMbll6gH/lnIuSM9CGHavif4z/DR/hB8VtY8PlmkhsZs20jfekgcboyfU7SAT6g18/4tcO0JOnxXltR1MPi3dtu7jK22utmk1yv4HFx2sl6vA2bVUp5JjIKNWgtLdY3/O7Tv9pO+92+booor8VP0IKdZa9d+FtUtdT02Ywalp06XFo4PSVSCg45OTxgdQxHem19Wf8ABPv4IWD6HN431CCK6v5rh7bTN67haInyvIvo7NkZ6hRx9419ZwVwvis/zangMJLkfxOf8ijb3ls272SV1q1qlqvD4izqhleBlia8eZbKP8zfT0te/lffY+gtb0iH4tfCi5tLiCW1h8SaYUMVxHiS386LgMp6MpYe4Ir8y7VZYV8q5hkt7qPCzQyoUkhfHKsp5BHoa+6fE/7engfwr49uNBuG1OSOzuGtbrUo4Va1gkVtr5O7cyqQQzBSBg9ayv27fgpp/jD4ZXXi62hhj1rQUWZ5415vLbIDIxH3toO5SemD2Jr+hPFLJcJxRgpY/KMTCpWwKkqkVvJaN6rZrlk46NPVJ3R+WcF5jXyXELDY+jKFPEtcrfR7fO90n1WjsfGCho545Y5JIpImDxvGxR42HIZWHKkHBBHIr6x/ZJ/bUn13UrTwn4ynEl9MVhsNVbA+0t0EU3o57P0Y8HB6/JqNuFbPw3by/ib4adThl1e0Ix/12SvwHgfivMMjzOnWwU7RlKKnH7Mk3bVd1fR7p+V0/wBP4kyTC5lg5U8RHWKbi+qduj7d1s/uP09rhf2oLK61L9mv4hW9kWF5ceGdSigx18w2soXH44ruFbJNJcQR3UDxyxrJHIpVkYblYHggjuDX+hEZcskz+WpK6sfxj6CoGiWed3+ojGCeh2irYbfXuX/BSD9jrUP2Ff20/HPw8uLeSPSre8fU/D0zA7bvSbmR3tnDEDdsG6FiOBJA47V4iqE/09K/bKFSNaCqU9VJXR+bV26UnCW6GBM//X6VKsXPqc96lit8mrMNnnoK7KdFs8uvjVFEENtX7l/8Goul3ln+zN8VbmQMLG68VxLb5Pyl1tI/Mx+a1+I8NltXIV2bgBUUsznsAByWJ4AHJJxX9N//AAR7/Y/vv2Kv2EfCPhXWrVbTxRqSvrmuw9Wt7u5w5hY92iTZGeoyhwSOa+d4zqQo5eqct5tWXpq3+X3nocJ1J4jHucfhind+b0S/X5H1DRRRX5Ofpx5b+3F/yZh8Wv8AsTtW/wDSOWv5F9MJ/s236/6pf5Cv66P24v8Aky/4tf8AYnat/wCkctfyL6av/Eut/l/5ZJ29h7V95wf/AAqnqvyZ81n3xw9GaOhWK6t4h0uzkaRY76/trR2XG5VlmSMke4DEj3r9z9W/4NOvhHPbSLY/FH4nWsw+68i2Myj6r5K5/MV+G3g8f8Vx4e+Uf8hmw/8ASqKv7IY/vN9a04mx+Iwzp+wly3vf8CMnwtKsp+0V7W/U/BD9qn/g1s+K3wq0G61f4X+NNH+J8FqhkbSLyz/sfVJAOcQsXeCRgB0Zo89vSvzE1bTLzQNZvtN1CzvNO1LTLmSzvbO6haG4s542KSRSRsAyOrAgqRkEV/ZYRkV+Gf8AwdS/shaT8PPil4C+Mui2MNpN44aXw/4iMUe1bm7giElrctjjeYVljJPJEcY/hrHIuIK1assPidb7PbXs7aGmZZXTp03VpaW3R+StzbrdwtHIu6Nxhh/+rn8RyK/pK/4IBf8ABQe+/bj/AGOBpvirUZtS8f8Awzli0TWru4k33GpwlM2t5ITyzyRqVdjyzxOx5Nfzcbf9kf5/Cv0P/wCDYz4zz/Db/gpVL4XMkn9n/Ejwte2EkOfka6syt5BIR6rEl4v/AG0r1eIsHGvg5S6w1X6/h+hxZTiHTxCj0lp/kf0RO+NuP4jX82H/AAXq/wCCid5+2/8Atgah4X0m+kb4b/Cq/uNK0m3R/wBzf6ghMV3fsBwzbleGNjnbGH2481937u/8FLP2g7r9lf8AYG+K3j7T3aPVPD/hy5fT3UgNHdyL5MDDPdZJFOO+K/k7s7RbO0ihX5hCgQFuScDGSe5Pr3rwuEsFGUpYqXTRevV/d+Z6eeYhxiqMeurJRkDvX1L/AMEyf+CTHxC/4Kc+Lr5tDuIfCvgPQZ1g1jxRewNNEkxAb7NbRAr59wFIYjcqRgjcwLKrfN3gbwLqXxQ8daH4Y0aNW1jxLqNvpNiCOPOnlWJCeOgLA/hX9bf7Kf7NPhv9kL9nrwn8OPCltHb6N4TsI7NHCBXvJQMzXMnrLLIXkc92c17Wf5tLB01Gl8ctvJd/8jzcrwKxE25/CvxPif4cf8Gv/wCzX4Z8PQwa3J4+8VagqgTXtzrjWvmN3KxwBFUe3P1NeTftgf8ABqx4P1bwrdah8EPGGseHfEFupkh0jxJP9u02+P8Azz88KJoCeznzB2K9x+utB5FfEU88x0Jc6qN+uq+7Y+jll2GceXkR/HT8Uvhl4i+CPxL1zwb4u0i88P8Aijw1dtY6lp1yB5ltKMEcjKsrKVZXUlXVlYEg1z9xAl3byRTRrLFIpR0cAq6nggjuDX7if8HTv7Fum6/8GfDXx10mxSPxF4XvodB16aKMA32m3BYQvIe7QT7QpP8ADO49Mfh9t/2R/n8K/R8rx6xmHVZKz2a81/Vz5LG4Z0Krp/cf0Q/8G7f/AAUd1L9sj9mO88D+MtUuNW+IPwsWCzuL68mMt1rGmuGW1uZXY7pJV2NFJI2WdkDsSzkn9Eq/mW/4IC/Hmf4Df8FTvh8qySR6f48juvCN/GvSVbiPzYM+63FvDg9gzepr+mmvz/iHBRw2LfJtJXX6/ifU5XiHWoJy3Wh/KX/wVgP/ABtD/aC/7Ha56f8AXGGvAVJ3D73Wvfv+CsAz/wAFQ/2guB/yOtz/AOioa8BRfnX5R19P/rV+j4L/AHen/hj+SPlMV/Gl6s/ps/4IDtj/AIJGfBr/ALB91/6W3FflP/wcLf8ABUH/AIbE+Ox+FPg7UGk+Gfw11B1vJYXzF4h1mMtG8uR96G2y8cY+6zmSTnERXu/E/wDwVDl/Y4/4IN/BP4XeCb6SH4nfEjQ7zzLqCXZL4b0g3tykl3kHcs8rDyoQMf8ALWTI8oBvysht1t4UjjRVSNQqj0Ar57Kcp/2urjKq+1Ll+96/5ff2PUx2OtQhh4dlf7th+So/ir73/wCCD/8AwSpb9vr44N428aab53wh8A3i/a45o8w+JdSXa6WGDw0KArJMeQQUjx87Ffmn9gv9iXxR/wAFBv2mNH+G/hYmz+0qb7WdU8vdHoenIyiW5btuyypGp+9I6joGI/qb/Zz/AGfPC37LHwT8NfD/AMF6amleGfCtmtnZQDl26s8sjfxyyOWkdzy7uzHkmtOIs4+rU/YUn78vwXf1fT7+xnlOB9rL2k/hX4s7S3iWCBUVVVVG1VUYCgdABXi3/BSf/lHz8bP+xJ1b/wBJJK9srxP/AIKT/wDKPn42f9iTq3/pJJX5/hv40fVfmfUVv4b9GfyaWh/0aP8A3R0rrPgYf+L9fD7r/wAjZo//AKcLeuSsxm2j4/hFdd8B4Wm+P/w7RF3M/i3RlUAcknULfgV+x1PgfofA0viR/Wx8evjp4b/Zo+Dnibx54x1BNL8M+FbJ7+/uDyyovRVH8UjsVRVHLMygcmv5W/26/wBs3xN+31+074i+JXiTzrUalJ9n0fSzJvTQ9OQnyLVe24Kd0jAANIznpivs3/g4t/4Kef8ADU/xsk+C/g++3/D/AOG+qMdYmiPya5rUO6NlJH3obUl0A+6ZtzYJjjK/moRgdB+X/wBavm+G8p+r0vrFRe9LbyX+b/rqexnGO9rP2UNlv5v/AIAk9wttE8kjeXHGCzMxwFA5JNfvB/wbof8ABJo/AbwTa/Hz4gWG3x14u07HhjT7iMFvDulzKp88gj5Lq5TGSPmjhbZwZJVPxt/wb/f8Eo2/bK+L6/FXx1pqt8KfAt4PsdrcR5TxTqsbAiLBGGtrcjdLnh5CiDIWTH9DkabFrh4mzi3+x0X/AIn+n+f3dzoyfAf8xFT5f5/5DgMCiiiviD6IKKKKACiiigAr5X/4Kjf8EvfDP/BRz4W28E1xDoHjzw+sh0DX/KL+Tu5a2uFHMlu7BSR95CNy/wAQb6oorowuLq4arGvQlyyjsznxeFpYmjKhXjzRluj+UH9rb9jX4kfsVeM20T4leFb3w7I0nl2t+QZdM1Hrg290B5cmcE7SQ4HVVORXj1ym4Ej5h6561/Yh4l8L6b4y0G60vVtPsdU029QxXNpeW6TwXCHqrowKsD6EV8+eJv8Agj/+zF4t1pr+8+Bvw5+0MckW2lLaxZ6/6uLan/jtffUeO4ThbE0nzd4vR/J7fez4j/UqVGf+zVPd7S3XzW/3H8ufhPwpqvxF8W2fh/w3pep+Itev2222maXaveXlx0HyxRgsQOMnGBnkiv2r/wCCKv8AwQF1L4G+NdJ+MHx0tbIeKNNK3fhvwmsi3C6JMM7bu8dco9yvBSNCyxHksz4Cfp/8G/2dPAf7O+lTWPgLwV4T8F2d1sNxFomkwWC3BQEKZPLVd5GTy2TyfWpPjf8AHjwf+zb8ONQ8X+OvEGl+F/DWlqDc39/L5cak9FUdXdugRQWY8AGvFzLiati17DDx5VLTu35eR9Dgclp4f95Wldr5JHXKNgNeZ/HP9sv4X/s2eJPD+i+NvG2g+H9c8V39tpuj6ZPcBr7UZ7iZYIRHAuZCrSMF37do7kV+OP8AwUM/4OXvGnxbur3w78B7O68BeG9zQnxLfwo2tagOm+GFgyWqnsW3SYwSIzlR+Y7/ABV1bwr8SYfiNqGoalrXiLR9WtvEtxqN9dPdX19Pazx3O+SaQl3cmLqxPbtWmB4Rrzh7TEvl7Lr8+i/H5GdfiKip+zoLm136fLuf0mf8FMLXb8QvCdxz+8064j/75lQ/+zV85MflNfVn/BTPSftXh3wfqkYVoYbu4t2kU5GJEV1/PyzXyfsHoPyr/OXxhwro8W4u/wBrkkvnTj+qZ/W3ANZVMioeXMv/ACZ/pY+xP+CaOqRT/DbxJaAjzbbVA7D2eJcf+gn8q+f/ANq3SJNG/aP8WJMMefdi5TP8SyIrD/Cuq/YG+JaeCPjVJo9xJ5dn4qthbDPQXMRLxfmDKvuWWtj/AIKNW+hyfELR72x1Kym1wQNaalYxtuljRfmikbAwvVlwxDHKkAgMR9XmlSlmnhph6kZJTwtTlabSvrJJK+75Zxl8n2PDwcJ4LjCrCSbjXhdO3o7vsrxa+4Z/wTe1Ka0+MuuWCPiC/wBEM8oHRmguIghP0FxJ/wB9Gof+Ci2nxWfxz0yZFCyXeixtIR/EVllUE/hgfhXD/swfHSy/Z78c32sXmk3WqLfWX2JVt5VjeFTIrscNw2dq8ZHSpP2qvjdp/wAfPiZa6xpdve29ja6dHaIl2ipLuDu7ZCsw6uOQe1ePLiDL5eHiyiVVOuqt1DW6XM3dXVrWu9G7c2up6H9l4pcV/XlB+ycLOWlm+VL17LXsed7qN1RbB6D8qNg9B+VfkHKfeEu6vtD/AIJ6ePbPXfhFNoCyKNQ0G6kaSIn5milYujgemdy+xX3FfFWweg/Kr/hHxdq3w/8AEcOraHfzabqFvkJNEeqnGUYdGU4GVPBwK+28P+Lnw3m8cwcHODTjNLdxdnpfS6aT13ta6vdfO8U5D/a+AeFUuWSalFva67+TTa8t9dj6O8W/8E39R1b4gXkljr+n2vhq/upJ23xu17ao7lmjVcbGxkhWLDGBkGvVf2wvGGn/AA0/Zv1LTFZYpNUtl0jT7fdlnyApxnnCxgkn6eorwi2/4KK+OIdOEUmneHZrgDHnmKRcn12hsV4/8RviTr3xb8R/2p4g1CS+ulXZGCNsVunXbGg4Uevc9ya/TMfx1wnlOXYulwvRn7bFJqTldKKd11b25nyqN1fd2SPjsLwznmOxdCeczj7Og7pK15Wt2XWyu3rbZXbMeMbB1re+Ett9t+L3hWLG7zNYtAQO485T/Sue2D0H5V6F+yTon9vftLeE4QoIhumuiPaON3/pX4tw5hXiM2wtBfaqQX3ySP0PNq3ssDWqv7MJP7os+x/iR+2h8Lfg78atH+Hnizxx4f8ADPi7xBZDUNMsdVuRaC/iMjxfu5HxGz70I2btxyMA5FenBtwYdK/nl/4Oe/GyeM/+CkNro7eTLH4Z8H2Fm0ZUMFMstxcEEepEg/DFeU/sC/8ABaP42fsFvZ6XY6zL468B2qqg8MeIbl5orSMZwtpcHdJbYHRRujHHyYr/AFKp8K1K+EhiKEveavZ/o/8AP7z+K6nEFOliJUaq0Ttdfqj9xv8Agqn/AMEsvCf/AAUu+D9vYXs8Xh/x14dEkvhvxEsPmNZs4G+3mUYMltKVXcmcgqrryvP87/7WH7EHxO/YY8byaH8TvCt5oP7zyrXVYwZ9I1Qc7Wt7sDY24AkI22QD7yKQQP6NP+Ce/wDwVX+Ff/BRvwrI3hC+m0vxZpsKy6r4W1XbFqViDwXUAlZ4d3AliJHIDBGO2vorxD4c0/xfolxpuqWNnqWn3kZiuLW7gWaCdD1V0YFWB9CKyy3PMVlUnh60LxX2Xo16P+kGYZTQzGCrUpWfdap+q/pn8fNpaeaAVPHqDXQeCPA+qePfEtroug6bqGuazfNst9P062e6upz/ALMaAsfrjA74r+mjxF/wSP8A2afFGstf3XwS+HqXDHJ+y6WtpGT/ANc4tqfpXrXwg/Z48Cfs/wCnSWfgfwX4V8H2syqsyaNpcNl5+0YUuY1Bcjnlsnk19RLj7Dwh+6oty82kvwufIvgPFVan76slHyTv+J+Z3/BHr/gg/e/CjxdpPxU+NtjZ/wBuaeUvPD/hVis66bMMFLq8IypmQ8pEpIjbDMS4AT9YkXauOtKBiivgc0zXEZhW9viHr0S2S7I+8yrKcPl1D2GGWnV9W+7YUUUV5p6R5b+3F/yZf8Wv+xO1b/0jlr+RfTk/4l1v/wBcl7ewr+uj9uL/AJMv+LX/AGJ2rf8ApHLX8i+m/wDIOt/+uS/yFfecH/wqnqvyZ81n3xw9Ga3g1ceOfDv/AGGbDt/09RV/ZFH95vrX8bvg4/8AFceHf+wzYf8ApVFX9kUf3m+tc/GG9L/t79DXINp/L9R1fmb/AMHV3kf8O7PCvmbfP/4WBp/k56/8el9ux+FfpkzbRX4a/wDB1T+13pXj74m+Afgzot6t3ceC3l8Q+IxHJlba5niEdpbsB/GImlkIPIEkf97jw8gpSnj6fL0d36L+rHo5nUUcNK/XQ/JXZ/nFfXX/AAQYFyv/AAVz+DP2Xd5n2vUt+0f8s/7Jvd/4bc18i1+hv/Bsd8F7j4kf8FLZPE/lt/Z/w38LXuoSS4+Vbq7K2cEf1aN7th7RH1FfombVFDB1W/5X+KsfK4CLeIgl3P1D/wCDjY3A/wCCRXxI+z5wbrSBLj/nn/adtv8A0r+a/bn/APVX9XX/AAU6/Z+vP2pP+CfnxY8CabF52ra54cuf7OiC7mkuogJoVHu0kaqPrX8oNndLfWkM6/dmQSD6EZrxOEaieFnDqpX+9L/I9DPIv20ZdGv1/wCCfQH/AASrhsZP+ClnwLXUfLNp/wAJjZbvMHy7stszn/b21/VunSv45Phz8QdQ+EvxF8O+LNJUNqnhfVLbV7RScb5YJVlVfxK4/Gv64/2bv2gPDv7UnwK8KfELwndC88P+L9Pj1G0fPzR7h88TjtJG4ZGXqGRh2rg4voy56dXpZr57/wBeh05DUXLKHW9zuaKKRmwK+NPoD47/AOC+awP/AMEmfjD9o27RYWpTd/f+2wbce+cV/Mhs/wA4r91P+Dp39sPTfB/7PHhr4KabeCTxJ441CHWdVgjf/j10q1ZipkHbzrkRhB3EMp/hr8K6/SOFaMoYLml9ptr00X6HyedVFLEWXRJHsP8AwTvN1H/wUE+Bpst/2r/hO9I8soPm/wCPlM4/4Dmv606/mP8A+CCXwHuPjv8A8FUfhuI4nax8Dm58XahIvSGO2j8uLP8AvXM8Ax1xuPY1/ThXh8XVE8TCC3UfzbPSyOLVFy7s/lL/AOCsIz/wVD/aC/7HW57f9Moa+f8AZ/nFfQH/AAVh/wCUof7QX/Y7XX/oqGvAE++PrX2+C/3en/hj+SPnMV/Gl6sfcyz3DR/aJLiTy4USLzizbIgPkCbuiAfdC/LjpUez/OK/UvxL/wAEsZv2xv8AghV8E/it4FsBN8Tvh5od2Lm2iTMniXSEvLpnthjrPCSZIT3HmR871KfllFMtxEsituWQbgR3FRg8ZTrqShvFtNej/UeIw8qVnLaSTR+3n/Bqx8dfhXF8LfGXw7stNh0X4vNdnWdVuZ5FaXxPp4YrA8DYDBLUP5bQ8hGl8znzjj9eh0r+PD4NfGLxN+z18V/D/jjwbqcmj+KPC94t7p90oyFcZDI6/wAUbqWR1PDKxFf1Ef8ABM3/AIKG+F/+CkP7NGneNNDWPTdcsyth4m0Qvuk0PUFUF48/xwvnfFJ/HGwyFYOi/E8TZXOlWeLjrGW/k/8ALt93Y+iyfGRnD2L0a/FH0PXif/BSf/lHz8bP+xJ1b/0kkr2yvE/+Ck//ACj5+Nn/AGJOrf8ApJJXzeG/jR9V+Z61b4H6M/kztRm2j/3R29qtadJc2upWsli91HfRzI9s9sWWdJgwMZjK/MHDYKlfm3Yxziqtqf8ARo/90V13wEma3/aA+HckbMrx+LdGZWBwVI1C3wa/ZJytFs+AgrtI5OIK0SlMFSMqRzkfWh0yh4ZvUBthYdxuwdufXBx1r9Iv+Dhz/glv/wAMi/GuX4veC7Dy/hv8RtSc6jbwpiPw/rMxaRlwOFguW3uh4VZdycbowfzfrnweMp4miq1PZ/g+xtiMPKjUdOZ/VR/wSm+Ofwt+PP7C/gS++Een2+g+E9H0+PSDoII8/QLmFQJrWfH3pQxLGQ8y7xJk78n6Mr+Wz/gk3/wUs1j/AIJn/tILrrfbNQ+H3iby7Lxdo8RLebCp/d3sSf8APzBlsf30Z0P8BX+nn4efEHR/it4G0fxN4d1C11fQfEFlDqOm31s26G8t5UDxyKfRlYH8a/N88yueEr33jLVP9H5/mfW5djI16duq3RtUUUV4p6AUUUUAFFFFABRRRQAUUV47+3N+2r4R/YM/Z71bx94uaWaG1IttO06BlFzrN44PlW0WTjLYJLHhVVmPArSjRnVmqdNXk3ZIzq1YUoOpUdktW2Yf/BQr/gor4D/4Jz/B9fEnjCSa91TU2kg0HQbN1+3a5cIAWVN3CRpuQySt8qBh1Yqrfzk/tz/t7/Ej/goH8T28SeP9WL2lk7nR9BtXZdL0JGwCIYyeZCAA0zZdsYyB8tZP7WH7U/jT9s3436p498eak2oaxqJ8qGBGb7LpVsCSlrbIx+SJck46sxZmyzE15hcjEbfj/Sv17I+HKeXw56nvVXu+3kv1fU/Lc04iqY+pyUtKa2Xfzf8AkVJvuj/fqO+gW6glicZSVSjA9wRg1JP91f8Afps3U/59K9iexjRP6OPgv46P7TX/AARU+FnijPmX2l+HdM+1M0nmMZ7MCzuGJ65Yo7YPPzV4n5q0f8GyXxVtPjn+wX8RPhJqMknmeE9WmjAODtstSiZ0K85+WVbjqMA4wfSOSzudIuprO+VYr6zla3uUz9yVCUcfgwNf5x/SWyF4TP6WMS0qRcfnF3X3xmreh/YPg9mixGVzoN6xaf8A4ErP7nF/eSR3clrdxTwSyQzQMskUkbFXjdTlWUjkEEAgjkGmqykszZZpGLMxO5nJ5JJ6kn1PJpnm+6/nR5vuv51/N/vWsfrfW5J5ikf/AFqQOoP/ANameb7r+dHm+6/nRysd/Ul85aPOWovN91/Ojzfdfzo5WHN6kvnLR5y1F5vuv50eb7r+dHKw5vUl85aPOWovN91/Ojzfdfzo5WHN6kvnLXv/APwTj8KHW/jNrGsNGrQaHpnlBj/DLcSAKR/wCKUf8CFfPTyZXqv519QfALxrZfsmfsG/EL4rasFSHT7K+1w5xumjtYWWJBnuzqQB3L+9fqHg5k0sw4qw6Suqd5v5K0f/ACdxPjOP8wWFySrrrO0V83d/+Spn4Of8Fafi5H8b/wDgpZ8aNet5VmtIfEkuk20inKvHZqtqCPYmJj+NfP0PT/gP+NQtf3erSS3uoTNcajfStdXcpOTLNIxeRvxdmP41ND0/4CP61/qphaKpU40l9lJfcrH8N4yo5zc31bf3mz4K8Za18N/F+m+IvDmraloGvaNN9psNS0+4aC6s5AfvI68jPQjoRkEEEiv3f/4I5f8ABdix/a4vdP8Ahl8WGs9F+KDIItM1RAIbHxYQOVC9IbvjJj+4/VMHKD8D4/8AH+Yq1aO8E8ckUksM0TJJHJFIY5InByrqwIKsDghgQQQCKzzLJaGYUuSorS6S6r/NeX6nPgs4rYCrzw1j1XR/8HzP7Egc0V+cP/BCb/grtJ+2B4V/4Vb8RtQ8z4peG7TzLLUZiF/4SqyTAMmc83UXHmDA3qVkGf3mz9Hgdwr8dzDAVsHXeHrqzX4ro15M/UsBjqOMoKvRd0/wfZ+aCiiiuM7AooooA8t/bi/5Mv8Ai1/2J2rf+kctfyL6c2NOt+T/AKpf5D3r+uj9uL/ky/4tf9idq3/pHLX8i+m/8g23/wCuS/yFfecH/wAKp6r8mfNZ98cPRmhoWoppPiLS7yQSSR2N/bXbomNzrFMkjAZOMkKQM8ZIr9yNd/4O0fhTFYytpfwh+LF1dc7Eu5dLto29Mut1IR+Cmvwu/Cj/AD/nmvoMdleHxbi66vy7a23/AOGPLw2Nq0Lqn1P07/ai/wCDpP4s/Fbw9c6X8NfB+i/C6O6XYdSnu/7Z1KIEYJjLRxwo3U5KPjj61+ZWrazeeItavtT1K+vdT1TVLmS9vr68na4ur64kYvJNLI5LSSOxLMzEkk1EFz/CT9K2vhh8M/Enxv8AGMPh3wX4d1rxdr1wQqWGkWb3c/JxyEztHqWwB3NaYXBYbCRfsYqK6v8AzbIrYirXl+8d/wCuxgzXK28TSOxCqMk4/wA/l3r+kX/g35/4J96h+w/+xydU8Vac2nePviZNHrerW0q4n063CYtLSQdVdI2LOv8AC8rDqDXgv/BHn/g3jk+BfijTfil8fLfSdT8Xae6XWg+EopFvLPQpRhkuLt8bJrpG+6iFoomG4PI21l/WlF2j+dfG8RZ1Cuvq2Hd49X38l5H0GVZdKk/a1d+iCRSxXHY1/ND/AMF0/wDgnnefsL/tlapq2l2LQ/Dj4nXlxrOgTRpiG0unbzbuw9FKOzSIveN+M7Gx/TBXmv7Wf7Jfgf8AbX+B+rfD/wCIGjx6toOq4dWGEudPnXPl3VvJgmKeMklXHqQcqzA+Pk+ZvBV+d6xejX6+qO7H4NYiny9VsfyK7v8AaP5f/Xr66/4Ja/8ABYfx1/wTJ1q+022sv+E0+G+tT/ar7wzcXXkG2uDgNc2cpDCGRgBvQgpJgE7W+asP/gpD/wAEnPid/wAE1/GVw2vWcniT4ez3DJpPi+whZrWWMk7Eu1A/0W4xjKtmNj9x25C/MQIIBHIIyD61+lNYfG0NbShL+vVP8T5H97hqn8skf0Z/Dn/g5h/ZT8Z+H47rU/EXjDwdeMCZNO1rwxdNcQ47GS1WeBs9fklb8+K8j/a7/wCDp34X+EPD11Y/Bfw34g8e+IpkxDqer2T6TotoSG+Zll23MzKdvyLEiMG/1oIwfwo/z/nmj8K8inwtgYz5nd+Ten4JP8TvlnWIcbKy87HT/G343eKv2kPi1r3jrxxrV1r/AIq8SXJub69mwM9kjjUHEcMagIka/KqgD1J5We5jtoHkkkWOONSzsxCqoHJJOeAKmtbabUNQt7O1t7i7vLyQQ29tbxNLNcueAiIuWdiegUE1+wv/AARl/wCDe7VJPE2lfFj9oTRYbSzsWjvPD/ge8USSyzA5W51NCNqhcBktstkkGTaR5depjMdQwVLmnouiXXyS/pI4sPhquJqWjr3Z9D/8G4H/AATrv/2Uv2bNQ+JXi7TZNP8AG/xWjguIrW4jKXOl6Sm5raJweUeUuZmQjK7kU8qQP0kpFXaPU+vrS1+V4zFTxNaVee7/AKS+R9pQoxpU1Tjsj+Uv/grC2P8AgqH+0F2/4ra5/wDRUNeAI3zr8x6/5717/wD8FX/+Uof7QX/Y7XP/AKKhrwFPvrx3r9awX+70/wDDH8kfEYr+NL1Z/TZ/wQGGf+CRXwZ/7B11/wCltxX5a/8ABw1/wSxf9kr4wyfGPwTpqR/DPx/qBGq29suE8OaxKzO2VHCW9ycshHyrLuQ7d8Yb9S/+CAv/ACiL+DX/AGDrr/0tuK+nPjX8GPDf7Qvwo1/wT4t0y31fw34ms3sdQtJVyssbDqPRlOGVhyrKCORX51TzGeDzKpUW3M013V/z7H1csLHEYSMHvZW9bH8em/8A2j/n8a99/wCCbH/BQPxF/wAE3/2mrLx1o8d5qeg3yLYeKNDicAazYb9xChiF+0RHLxMcYJZchZHzQ/4KF/sLeJv+Cd37UGtfDvxC0l7Yxlr7w7q+CE1vTHciGboMSr/q5VHCyK2CVZGbxL/P+ea/RmqOKo6+9CS+9f19zPk/3lGp2kmf2D/BD41+Gf2iPhL4d8ceDdUt9c8L+KbKPUNOvYT8s0TjOCDyrqcqyMAyMrKwBBFef/8ABSf/AJR8/Gz/ALEnVv8A0kkr8Pf+CDP/AAVm/wCGE/i9/wAK98d6lIvwj8cXgxPLJmPwtqUhVVuuThbaX7swH3W2Sdnz+4H/AAUilWf/AIJ6/GplIZW8E6sQQcgj7JJyK/NcZls8FjI038Las+6v+a6n12HxkcRQclvZ3R/Jrati2j5I+UV1nwLbPx6+H3P/ADNmj/8Apwt/euUtP+PWP/dFdX8DP+S9fD7/ALGzRv8A04W9fp9T4H6HxtL4kf1u/HH4I+G/2jvhJ4k8D+MNMh1jwz4os5LDULSQf6yNh1U/wurYZWHKsqkcgV/LJ/wUE/Yb8T/8E8v2ndb+HfiFpryziY3nh/Vym1dc01mIhn9BIPuSqMhZFbBwRX9ZCfxfWvk//gsB/wAEz9N/4KTfsxTaNarZaf8AELwyz6j4R1WUbRBc7cPayuAW+zTqAjjoGEb4LRrX5pkObPCVuWfwS38vP/Py9D7DM8D7eF4/Etv8j+X0t/tH/P41+nX/AAby/wDBWv8A4Zk+IFr8D/iFqnl/DnxXdt/wjmoXL/J4b1OVs/Z2JPy2twxbnpHMwOAsrsn5p+J/DGp+BvFOqaFrmnXWka5od5Lp+o2F0myeyuYnKSxOOzKykHscZGRg1QliW4iaN13KwwQf/wBdfoeMwdLF0XRqbPr27NHyuHrzoVOeO/8AWh/ZorbqWvy5/wCDeX/gro37THgm3+B/xF1GWb4keEdP3aHqdzJufxRpkIVcMxOXvIFwJM8yR7ZMsRLt/UYHIr8oxmDqYWs6NXdfiu6PtsPXjWgqkOoUUUVymwUUUUAFFFFAEd3MsELOzLGqjczMcBQOSSa/mt/4LD/8FA5/2/v2q7q90y6mf4feDWm0rwvAT+7mTcBNfbf707KuCeRGkY4O7P69f8F7f2uZv2Yf2ENV0vS7uS28SfEq4/4RmwaJ9ssMDxs93KOcgCBWTcOjSp0JFfztyKEVVVQqqMAdgO1fp3AeTrllmNRa/DH9X+n3n5bx/nUlOGW0n/el+i/X7ijc/wCsP1/xqpc/6s/Q/wBKuXX+s/H/ABqnc/6s/Q/0r7qsfK4IqT/dX/fps3U/59KdP91f9+mzdT/n0rhkfQUT7s/4Nwv2mh8Av+CkGm+HbufydH+K2mSeHZgzHb9si3XNm2O7blmjH/Xev0y/bm8Af8IB+0HqVxHGFtPESLqUPGBuPyyj67xn/gQr+eXwz4q1bwB4l0rxF4fvDYeIPD17b6rpd1tDfZruCRZoXIPBAkRSQeCMg8Gv6Wvjh410n9t39hHwH8avDcXlwz6fBrBh373tYbhVW5t2OBloZQATjH7pscHNfzN9JDhN5jkUsXSV50vfX/bt+b74N/OKP27wizxYXMo0Kj0n7v8A4Fa3/kyX3nzFlv7qUZb+6n51W+0kfxN+VH2o/wB5vyr/ADx9nI/q4s5b+6n50Zb+6n51W+1H+835Ufaj/eb8qPZyAs5b+6n50Zb+6n51W+1H+835Ufaj/eb8qPZyAs5b+6n50Zb+6n51W+1H+835Ufaj/eb8qPZyAs5b+6n50Zb+6n51W+1H+835Ufaj/eb8qPZyAt2ljc6vqFvZ2kfmXl5KlvAg/ikdgqj8WIrU/wCDkL41W/7Mv/BN7wZ8HNJuWj1L4hahBp0ix5XdpunqlxeSfRpvssRU9RcE/wAJr1D9gH4Yt8Q/jvHqk0e/T/CcX22RiOGuHykCfX77+3lj1FflV/wX/wD2q4/2ov8Ago34jsbG48/QfhhCPCVkQRtaeNi944x1zMxTPcRD0Ff2T9F3hN2qZzWXxO0f8MOq9Zuz/wAB/PvjRniXJgIP4Vr6y/yir/8Abx8UqMK31qWD7v8AwEfyNRAcH/eqWDp/wEf1r+3IH8z1yzH2/H+Yq1B1H0FVY+34/wAxVqDqPoK7aO6PFxWzOm+GPxG174O/EHRfFXhXU7jRfEfh67S+069h+9bzIeCR0ZSMqynhlJB4Nf1Af8E+/wBsfSf27/2W/DXxE02NbG8vofsus6crbv7M1GLC3EIOclA/zIxwWjdGIUkqP5ZYuor9MP8Ag2u/awm+Ff7UutfC3ULp10L4kWhubCNn/dwapbKWBA7ebBvU46mGMc8Y8DjLJ1isC8TBe/T19Y9V8t/k+52cHZ08Jj1haj9ypp6S6P57fcfutRTY/wDVrTq/GT9oCiiigDE+JXgDT/iv8O9e8L6sszaX4i0+fTLwQyGOQwzRtG+1hyp2scHtXwZF/wAGwv7LcMSotj8QNsYCj/iqJu34V+hrNtXNc38SPi54f+E2hf2h4g1ODTbdiVjD5aSZh2RBlmP0Heqlmzy+jKtKt7OC1k2+VLzbeglgViqkaahzyeiVrv5I+GP+IYn9lz/nx+IH/hUTf4VPp/8AwbLfsr2UoaTRfG90o/gl8VXSg/8AfBU/rXqvjP8A4KVk3Ekfhnww8kC/dudVuPLZ/wDtjHnA9CZM+qiuVH/BRjx0LncdL8NGP/nn5U2Pz8zNfm+M+kDkNCp7JYyc/OMZtfe0r/K/kfW4fwuzOrHneHjH1cb/AHa2+Ze8Ff8ABv8AfsleCZlkHwh07WmU5xrWp3upp+KTSsp/EV9Q/Cb4FeCvgN4cXSPBPhHw14R0wBVNro2mw2MTbRgZEajccdzk14F4E/4KUW01xHD4o8N3FnGeDd6ZN9oVT6tE4VgvurOfavozwN8SND+JehLqWg6lbanZv/HC3KH0ZTyp9iAa+kyPxByviFWwOK9pJauLbUl58srO3mlbzPJzLhfG5VriaHIu6s196uvlubgUL2orjPj78WP+FJfDO88R/wBntqf2OSJPs4n8nfvdUzu2tjGc9K8p+Df7d/8Awt74m6T4b/4RWTTf7UaRRcnUxN5WyJ5Pu+Uuc7MdR1rHMuM8my/MKeVYyty16nLyx5ZO/M3GOqi4q7TWrXnoXg+H8fisLPG0Kd6cL3d4q1ld6Np6LXRH0TRSJ9wZpa+oPGKeuaDZeJdHutP1CztdQsb2Mw3FtcxLLDOjcFXRgQynuCMGvgn9pz/g21/Zx+Pt7c6h4f0vWPhVq1wS7P4WuFisSx5J+xyq8K/SNUHrX3Z498Tf8IX4J1bWPJNx/ZdnLd+UH2ebsQtt3YOM4xnBr5x8N/8ABSP/AISHXdOsf+ELlhGoXMVv5n9rhvL3sFzjyecZ6V8/mvH2W8P4inQxmIdKdT4UlN31t9mLW/c9TA8M4vNKUqmHpc8Yb6xVuvVp/cfAPiL/AINFZ2v2/sX9oJobTPyrqPgxbiYD3aK8iU/gorpvhh/waS+EtMv4ZvGnxo8Va7CjZkttG0W20pZR6b5GncfUEV+vUQIXnnmvG/ix+1TD8Ffj/p+ga4ir4d1TTo5jeKPnsZjJIu5x3jIAz3XryM4+kzrjypllCNfH1+SDko81lZOWiu7aK/Xpu7K7PJy/hqOMqunhqfNJJu13qlvZX1fl16anPfsff8EsPgZ+ww8d18PvAOl2uvCPy5PEGoE3+sSjv/pMuXQHuse1f9kV9DhdtQ2t9FfW8c0MiSwzKHjdGDK6kZBBHBBHORU1FStOq/aTk5N9W7hGmoLlirBQelFB6VmUfDfxy/4N7P2d/wBon40+KvH3iSz8at4g8Zai+qai1t4ilghMzqqnYgGFGFHFcv8A8QxX7LoP/Hj8QP8AwqJ/8K9k8Q/8FKv+Ef1nVLT/AIQqSb+zbu4tS/8Aa4XzPKlePdjyeM7c47Z719RWNz9rgWTG3eitj0yM15XD/iJhc4lUo5VipTdKykrTjy3ul8SV/he19juzThWvgFCpjaKj7S9n7rva19m+63OE/Za/Zp8M/sf/AAI8P/DnwbHfxeGfDELw2S3tybmcK8jSHdIeW+Z2r0GvH/2l/wBqf/hnTUdGg/sJ9a/tmOeXIvRb+T5bRjGNjZz5ntjFO/Zf/ak/4aOv9ch/sN9G/sWO3fJvRced5plH9xduPL987vavOlxtlE85eRyr3xTeseWW/Lz/ABcvL8Ou/lvodS4ex0cv/tJU/wBz/NePfl2vffTb8DI/bo/4J0fC/wD4KKeDNF0X4maTd3i+HL1r7TL2wu2s76yZ12SIsy/N5ci7d6fdYxoSMopHzR/xDE/suf8APj8QP/Con/wr9DKRm2ivsqOYYqjHkpVGl2TPn6mFozlzTim/Q/PJ/wDg2G/ZbljZX0/x8ysMMreJ5iGHcEYr608Kfsa+FfCn7H7fBH7b4l1TwV/Yc3hxG1DU2uNQSwkVoxCLhhuIjjbYhOSqqoycCqvxl/bU8H/CW+nsFmuNe1iJijWen7WELDtJIxCJzwQMsP7teK61/wAFJPFV1O39n+HdDsY26CaaW5Zfx+QH/vkV+ecReMWRZfV+r43GOc4v4Y807Pzauk/JtPyPq8p4DzLFQ9rh6HLF9XaN16PVrzSsefx/8Gw37LaIFFj8QMAY/wCRom/wq94Z/wCDan9mXwj4p0nWLOx8efbdFvrfUbYv4mmdRLDKsqZBHI3IuR3Fddo3/BSHxdazL9u0HQL2MH5hE0tux/4FlgP++TXsHwi/bt8I/Em6hsdQNx4X1SbCiK+ZWt5HPaOdeD/wMIT6U8m8bslzSoqFLGuM3olPmhfyu/du+17sWYeHWY4KHtZ4ZNLrG0rfJa/Ox7hGpXOe5pXG5TTUmWSPcvIxnjvXKfFr4yaF8F/DJ1bXrxreBm8uGGNd891JjISNP4m/IDqSBX1uMxlDC0ZYjEzUIRV3JuyS7tnh0KFStUVKjFyk9Ekrtv0Pmz9rj/ghr+z/APtp/Gu9+IHi/Q9ctfE2pwRQ30+j6tJYR3xjG1ZZUQYaXbtUv1IRQegrzT/iGJ/Zc/58fH//AIVE3+FekeJ/+Ck+uXN439ieGdNs7YEhGv7h55mXsSE2qp6ZALAepqHQP+Ck/iW1uF/tTwzo19B/ELWeS2k+oLbx+GOfUda/Po+P/D1OosPHGT5VpdRny/le3yPqH4X5rKPtZUI37c0b/nb8TlPh9/wbk/s7/Cfx7ovijw3J8StG8QeH7yO/0++tfFc6TW0yHIYHHQ8gg8EEg5BNfeijCjvx1NcH8D/2hvDvx30WW60aaaO4tcC6sbpQlzak9NwBIKnsykg+td6DkV+jYfOoZrQhjKNb2sJL3ZJ3VvX13XR6PU+UrZfLBVJYepDkkt1awUUUVoQFFFFABRRRQB+HP/Bz98Q9Q139rjwD4Ymhuo9L8NeF5b20MkW2Oaa8uSJ3Rv4gFtLdT6FSOMnP5j3PUfT/AAr+kT/gsP8A8E6o/wDgoL+zW0Gjw28fxE8FtNqHhe4kYR+ezqBPYu56R3Cxx9cASRQsTha/m/1exuNJ1G4s7y3uLO9s5Xt7i3uIzHNbyo2143U8qysCCDyCK/buDcwo18sjQp6Sp6Nerbv8/wA7n4XxpltehmssRU1jU1T9Fa3yMy6/1h/3qp3P+rP0P9Kt3P8ArD/vf41Uuf8AVn6H+le1WOfBFSf7q/79Nm6n/PpTp/ur/v02bqf8+lcMj6CiV0+7+A/rX7Qf8GuX7Wdp49+GXj/9nnxNItyNLR9f0OCZsrdafcnyr23HOf3czI5A7XWexr8YYa9G/Y4/al1b9ij9p3wV8UdHW4mk8J36T3tnC+1tSsHGy7tuwJkhLhc8bwhPSvDzrL443CToNXutPX/g7fM9zK8U8PiFUX9f1ufsd8VPh1dfCD4i6x4bvWeSTSbgxRytnNxCeY5P+BIQT75rn/MHrX1R+3z4R0n4vfC3wt8YfCNzDqmj31nAZbu3X5buxuAHtrjkA/KWCnPIEozjaa+TftH+cV/lbx1wvLIs5rYBq0L80POD2+7WL80z+3uGc6WZ5fTxV/e2l/iW/wB+68mWvMHrR5g9aq/aPb9KPtHt+lfI8iPe5mWvMHrR5g9aq/aPb9KPtHt+lHIg5mWvMHrR5g9aq/aPb9KPtHt+lHIg5mWvMHrQ86pGzM2FUZJ9Kq/aPb9K9M/ZD+DB+PHxu0/T7iFZNF0vGo6oGHytCjDbEfXzHwuO6h/SvQyrKa2Y4yngcMrzqNRXz6vyW77JM5cdjqeEw88TWfuxTb/ru9l5nrHxI+Mtv/wS5/4Jb+KviVeRwjxVfWYuNOt5VybjUroCKwgYHGQpZGZeyrJX82S3NxeyzXF5czXt5dStPc3MzFpLmV2LySOT1ZmZmJ7kmv05/wCDnj9t7/hc37SGh/BnRbxpNA+GGdR1lY3Pl3OsTxbUDDOCbe3ZgPQ3Ug7cfmLb/wCr/H/Cv9TfD7hujkuT0sJRVkopLvZLRvzesn5s/iXizOKmYY+dep1bfzf+W3yHZyD9alg6f8BH8jUWcK31qWDp/wABH8jX3sD42uWY+34/zFWoPvL9BVWPt+P8xVqDqPoK7aO6PFxWzLkfWvTf2WPiRf8Awe/aZ+HvirS4by6vtB8S6fdRW9qhknugLhFeKNRyzujMgHcsBXmcQy1fqv8A8G7X/BNyX4heNF+PnjLTlbw/oErxeD7adMi/vlJWS+wRzHDysZ7yFm48sZM2zCjg8DUrV9rWt3bVrfP8rs8nK8tr43MKdLD6NO7fZJp3/rqftaowKWmpwgp1fz0f0UFFFNlbahoA4n4+fGmw+BHgG41q+DTyMwt7K1VsNdzsCVQeg4JY9lUn2r8+fHPjrXPi54wk1PWLifUtVvHEcUcalliBPyxQpztXsFHJPJycmvUf2+fiJJ4x+ODaWsjNZ+GYFt0QH5fOcB5Wx642L9Frtv8Agnp8E7fVJLzxxqMKTNbymy0lHXIiZf8AWz/73IRT2w/97j+UeMsdj+NeK/8AVrBz5cPSk0+14/HOS6tP3YLa9tuZs/buH8NheHck/tfER5qs0mu/vfDFdrr3pP8AOyMv4Sf8E7dY8R6fHfeLNUOhxyKGSws1WW5AP/PR2+VD/sqG9yDxXpa/8E5/Agh2tdeKC2Mb/tyZPvjZivf1TZS1+zZX4S8LYKgqP1VVH1lP3m/PXRf9upLyPz/GccZ1iKntPbOC7R0S/V/Ns+QviR/wTb1CwtGuPCevDUGXJ+xamgiZh/sSoMZ7YZQP9oVs/wDBOrwbqHhHXvH1nq1ncaff2r2cUsEybWHExz6EHHBGQa+pCMio47ZIZGdVG5gAWxyQOnPtk/nWWB8Kcmy/OqGdZanSdPmvC7cXzRlHS7bi1e+jt0t1KxPG2YYrL6mX4xqala0tmrST6aPb18zyH9vP/k2bWv8Arva/+j0r5X/Y35/ac8Jf9dLj/wBJZa+qP28/+TZda/672v8A6PSvlf8AY3P/ABk54S/66XH/AKSy1+UeJn/JxMs/7gf+npH23B//ACSmM/7i/wDpuJ+hQ6UUDpRX9SH4ycp8dv8Akini3/sEXX/opq/Oj4b/API8+Hf+wlbf+jVr9Fvjt/yRTxb/ANge6/8ARTV+dPw3/wCR58O/9hK2/wDRq1/Lnjx/yOsu9P8A29H7N4Z/8i/Fev8A7az9QV6V8Tf8FHV3fHTS/wDsCx/+jpa+2l6fjXxL/wAFHP8Akuul/wDYFj/9HS1+h+OX/JKz/wAcPzPlfDf/AJHUf8MvyKf7KP7XNx8GrqHQfEEk114TlbEMv35NJYnqPWHuVHK9V7ivtzTdVt9VsYbq3mjuLe6RZYZYm3JKrDIZSOCCOc1+aF78ONTh+GVr4uRPO0ee9k0+ZkB3Wsq4K7/9lweD6jBxkZ9G/ZW/ayuPgjfppOsSXF54TnboAXk0tifvxjvGT95B9RzkH8v8M/E+vk7pZNxBdUJKLpzf2IyV43fWm+j+xs9FaP2XGHBtLHqeYZXb2ibU4r7TW9u0u6+1vvv950VR0XXLXXdLgvbW4iurS6jWWGaJg0cqEZBUjqDV6v6ujJSipRd0z8SlFp2Z+WvxE/5G/wAUf9ha/wD/AEqmr9P9G/48If8Arkn/AKCK/MD4if8AI3+KP+wtf/8ApVNX6f6N/wAeEP8A1yT/ANBFfzX4A/79mnrT/Oqfrnid/uuC9JflA+UP+Cl3/IxeCv8Ar1vv/Q7enf8ABMT/AJDfjr/rjp//AKFdU3/gpd/yMXgr/r1vv/Q7enf8ExP+Q546/wCuOn/+hXVcUf8Ak7/z/wDdU6X/AMkF8v8A3MfWzvsHNfKf7bf7V91p2o3XgfwzdSW8sfy6tfwvh0yM/Z42ByrYOXbqMhR3x9EfGTx4vwx+F+ua8wVm0y0eWMHo0nRB+LEV+a1raah4s12KOMteatrF0F3SMS008r8sx68s2SevWvq/G7jTE5dh6WTZe3GrXTcmt1C9kl1vN3V10TXU8Tw74fo4urPMMWk4U9k9nLe78orX1a7G/wDCH4KeIPjZr7ad4fs0docNcXM7GO2tQehdwCcnsoBY+mOa+mPCH/BNnQbO0Q69r+salclfnFoEtYVb/ZyGbH1P+Fe1/BX4R6d8Fvh7YaHp6qxt13XNxtw95OR88re5PQdgABwBXW11cGeCuUYHCQqZxTVau1dpt8kf7qS0dtm3e71VloYcQeIWPxNeUMBP2dNPS3xPzb3V+yt53PnXXP8Agm94QurUrp+reJNPm7O08dwv/fLIP514J8eP2NPFnwZ0q71HbD4g0OFSXu7NGEkC4+9LCclR7qWA74r9BaZLbrOCGG5WGCD0Ir1uIPBnhvMaDhh6XsKltJQ0s/ON+Vrvon2aOHK/EDN8JUUqs/aR6qX6PdP715MwPhPZNp3wr8OW7HLw6VaxsfUiFBXwv+1/8RLj4i/HjXFkkc2egTNplpFu+WMR8SMB6s+ST7D0r9B44lt4tqjaqjAA6DFfnF+0t4UuPBPx78WWdxGy/adQkvoD/wA9Ipj5in/x4j6g18r49fWMPw/hcNSb9nzpSts7RfKn5XV/VI9vwz9lVzWtVn8XK2vnJXt+Xo2eqfspfsXaf8U/BcPibxNc3hsb5mFjZWsnlFkVivmSPjPJU4VccYOecDsPjF/wT00afw3cXXg2a+0/VbdC6Wt1cGe3usdUy2WRj2OSM9Rjmqv7D37UOk2nhOz8Ea5cx6beafuXTriYhYbuItkR7jwsikkAH7wAwc5FfUTOsg5zXocD8F8IZxw1SjSownKUEqkv+Xkalve13i072W1rWTT15eI+Ic+wGbzc6kopSbivsON9NNndbve9+p8k/sg/sneNvCXj3T/FmpTR+HbW3R1ayf8AeXV7GwwY3QHaing5JLAqPlHBr66XgU1ECdKdX6VwjwjguHcD9QwLk4t8zcnduTSTdtEtlokl89T5HPM8xGa4n61ibJ2sklZJdu736thRRRX1B4wUUUUAFFFFACMu4V+UH/Bej/gjxdfFL7f8cPhRpIm8SW0Rl8W6DaRfvNajUD/TrdV63KKMSIB+9UBh86kSfrBQRmvQyvMq2AxCxFB6rddGuz/rzPPzPLaOOw7w9daPr1T7r+vI/jvadblVkRg6yYZWHQiq1z/qz9D/AEr9pf8Ags7/AMEGpvHmq6t8W/gRpMQ1y4d7zxF4PtkCLqjnl7qxUABbgnczw8CUksuHyJPxZu1aNpY5FkjkiZo3SRCjxupwysp5VgQQQeQRg1+yZfm1DMKPtaL16rqn5/o+p+T4nKa+X1vZVVp0fRr+uhVn+6v+/TZup/z6U6f7q/79Nm6n/PpW0jsokEB/p/WmH/UL9RT4P8P60w/6hfqK55HdDc/bb/g2o/bO0/48fALxV+zV42l+2XXhm0kvNBiuG/4/9FmbZNAh7NbTOvGchbiMj7p23fjD8NtQ+CfxL1bwxqTF5tLl2xTFcfaoTzHL/wACXBPocivx2/Zn/aN8SfsiftCeE/iV4TY/254Rv1vEg8wxpqEJ+We1cj/lnNEXjPpuB6gV/Q5+1Zp/h39t39k/wn8dvh2739q2mLf52bZrjTn5kjkUZxLbvuyuTjEoyeK/lv6QfAP17Cf2rhY3nTvLTqvtx/DmXpJLc/cfCnihYbEfUq792dl6P7L/APbX8m9j5H+1+/6Ufa/f9KzBdgjr+VL9rr+I/ZH9Jcxpfa/f9KPtfv8ApWb9ro+10ezDmNL7X7/pR9r9/wBKzftdH2uj2YcxoS6itvE0jsqpGCzEjoB1NfXWu/FDS/8Agk1/wTk8T/FLxJYxyeKLy3S4g06U7Hvb+UbLGxPIIAZgXxyo809q8r/4J/fs+f8AC9/jGmoahb+b4b8JvHd3YcfLdXGcwQ+/I3sPRQD96vg3/g43/wCCgX/DU/7WEfwz8O6hJN4K+ENxNa3HlS/uNR1s/JcSEDhvs65gUn7rNPj7xr+nvo98AvFYl51iY6aqHptKXz+BP/Efi3itxQqNFZdReujl6/ZXy+J/I/P/AMSeLtX+IPirWPEHiDUJtX8QeILybUtTv5vv3l1M7SSyEdBlmOFHCjCgAACqducx/j/hQv8AF9P8aLf/AFf4/wCFf3DFJaI/mmo29WOHQ/71TQdP+Aj+tQjo31qWD7v/AAEfyNbwPPrlmPt+P8xVmE4K/QVWj5x68/zr7O/4JRf8Ef8Axf8A8FGfFMWuakb7wv8ACPTZ9l/roXZPqzK2GtrDcCGfIIeUgpH/ALTDbWlbGUcLSdeu7RX9WXmebHB1cVU9jQV2/wCtR3/BI7/gldrX/BRj4rtd6ot5pfwr8M3CjX9UjZoZL9+D9gtXHPmsMF3BHlIc53sgP9HHg3wZpPw98KaZoeh6bZ6Poui2kVjYWFpCsNvZQRoEjijRQAqKoAAHAArL+DPwY8Lfs9/DLRvBvgvRLHw74Y0C3FtYafaJtjhTOSSTlndmJZ5HJd3ZmYszEnqK/Ic+z2rmVbmekI/Cv1fm/wDgH6hkOR0sto8kdZy+J9/+AgooorwT3ApsgytOpshwlAH5p/HG5e9+NfjOST7za3dZHpiQgD8AK+3f2LrKOx/Zj8JiMD99byTNjuzyux/U18eftZ+FZPB/7Q3imGRcLf3X9oRHH3lmG/8A9C3D8K+mv+CfXxBh8S/A5dH8wfbPDV08EsefmEUjNJE3+6cso/3CO1fyl4TSjhOOcdhcTpUkqsVfq1UUmvmk36I/bOOIuvw3hq1HWK5G/JODS/FpfM96ooor+rT8TCignFIrhulAHj37ef8AybNrX/Xe1/8AR6V8r/sbn/jJzwl/10uP/SWWvqj9vP8A5Nm1r/rva/8Ao9K+V/2Nv+TnPCX/AF0uP/SWWv5b8TP+Ti5Z/wBwP/T0j9m4P/5JTGf9xf8A03E/QodKKB0or+pD8ZOT+O3/ACRTxb/2B7r/ANFNX50/Df8A5Hnw7/2Erb/0atfot8d/+SKeLf8AsD3X/opq/On4b/8AI8+Hf+wlbf8Ao1a/lzx4/wCR1l3p/wC3o/ZvDP8A5F+K9f8A21n6gr0r4m/4KOf8l10v/sCx/wDo6Wvtpen418S/8FHP+S66X/2BY/8A0dLX6H45/wDJKz/xw/M+V8N/+R1H/DL8j1T9g3w9Z+Kf2Z9Q0/ULWG8srzU7qKaCVdySqVTIIrwn9qP9lO9+A+oNqenede+E7qTEcrfNJp7E8RSnuOyv36Hnk/Qn/BOv/kgU/wD2F7j+SV7drGj2uuabcWt5bw3lpcxmKaCaMSRzIeqsp4IPoa8/B8BYHibgvAUq3u1Y0o8k0tYu2z7xfVfNWZ1VuJsTk/EGJnT1g5vmj0eu67NdH9+h8G/su/tU3nwJ1ddP1Bri/wDCdy/7yAfNJp7E8yxD07sg69Rzwfuzw54hs/FGi2uoWF1DeWN7GssE8TbklU9CD/nFfD/7Vf7JF18ErmXWtEWa78JzPyCxkl0snork8mPPCueRwG5wTR/ZT/acuvgR4lWx1CWa68KahKPtEH3jYyEgefGPT++o6jkcjn4ngnjbMeEcw/1Z4mTVJO0ZPXkT2afWk/8AyXys0vouIuHcLnuF/tjJ37/2o/zW3TXSa/8AJvmm/M/iJ/yN/ij/ALC1/wD+lU1fp/o3/HhD/wBck/8AQRX5fePJ0uvFPiWaORZIZtTvZI3U5WRWuJWVge4IIIPoa/UHRv8Ajwh/65J/6CK9DwBaeNzRrvT/ADqnL4nK2GwSfaX5QPlD/gpd/wAjF4K/69b7/wBDt6d/wTE/5Dnjr/rjp/8A6FdU3/gpd/yMXgr/AK9b7/0O3p3/AATE/wCQ546/646f/wChXVcUf+Tv/P8A91To/wCaC+X/ALmPUP2/LuS2/Zq1RUJAnurWJ8d1Mq5/lXyf+yrZR6h+0P4PjlVWUXxk59Vjdh+oFfZv7X/hGXxr+zt4ktbdPMuIbcXcYx1MTBz+imvhD4VeN0+HnxE8P68xPkaZexzykdfKzh//AB0mubxhksLxpgMdif4SVJ36WhVk5fcmn8zXgNe34exWHo/Hea+coJL8T9OIvuU6obG8hvbSKaGRJYZlDxupyrqeQQfQjmpq/quLTV0fim2gUUUE7RTAGG4Yry/9pD9mTSf2gtGi82Q6frmnoRZ6gibioJyY5F/jjJ5x1U8gjJB9PVw44pGdVPNedm2U4TM8LPBY6CnTmrNP+rpp6prVPVanXgcdXwdeOIw0nGcdmv61T6rZrc/NX4ufArxN8Fr9ofEWntHal9kV9D+8s5/TD44z/dYA+1bnwh/aw8ZfB2SKC31CTWNHjAX+ztRcyRoPSN+Xj44ABKj+7X6Ealplvq1lLb3MMNxbzKVkilQOkgPUMp4I9jXy9+0/+w9pttoF94i8E266bcWitcXOkp/x7zRgEsYR/wAs2HUIPlI4AXiv5s4i8JM24enLN+FcRK0dXG9ppLVpNaVF/daV1paTP13K+OsDmsVgc7pL3tL2vG/5xfmn9yPY/gF+0v4f+PumSHT3ks9UtUDXWm3GPOg7blI4ePPAYe2QDxXo1fl/8OfHd18MvHWkeILGVoprGdXbaceZESBIh9VZSQR9PSv08tLhbq1jlj+5IgZc+hGRX6b4T8fVuJcDUjjElXotKTWikpX5ZW6PRppaaXVr2XxvG3DEMoxMXh2/Z1LtJ7pq11fqtVZ799ruSiiiv1c+JCiiigAooooAKKKKAEZdw/Wvz+/4Krf8EI/Bv7c76h408DPpvgT4sSgyTXnlFNM8RvgAC+RASJOABcIpfHDCQBQv6BUV1YPGVsLVVahKz/rR90c+KwtLEU3SrK6P5Df2hv2efG/7K/xQvPBfxC8N6h4X8SWLF2trlQ0dzHnHnQSrlJoj2dCRzg4PA4uRw5bH+elf1pftbfsW/DX9uH4at4W+JXhix8QaehaSznYeXe6XKRjzradcPDJ0yVOGHDBlJB/DH/gol/wbx/FT9kddQ8TfDr7V8V/h/BvmlW0g/wCJ9o8S85mtl/4+EA/jgy/BJiA+av0fK+KKGKSp1/cn+D9H09H97Pi8ZkNXDy5qPvR/FH56Qf4Uw/6hfqKW1mWTO1lYKxU+qsOCp9CDwQeQRSE/uV+or6GR5sdx7DNfqb/wbR/8FFl+FPxUvf2e/GF5v8M+Op2u/Ckk7gx6fqe0meyIb/lncqNyAdJUcYJmGPyzPWmW9/daVqUF5YXl1p2oWUyXNrd2spintJkYMksbrgq6sAwIOQQK8/McFDF0JUJ9fwfRnoYPESoVFUifun+21+zvJ+zH8ZZbG1hMfhvXA95op/hSMECSAe8TMox/ddK8f/tJv8mvrv8AYy/aP0T/AILdf8E62j1C4sLP4qeEWjttZjChDp2rJGfKu1Ucrb3Ue4jAxhpUGTGcfGmuWd54X1y80vVLWaw1LTZ3tbu2lGHt5UJVlP0I69CMHvX+eHiZwLPIs1lyRtSqNuPaL6x9Osf7rXZn9b8F8TRzTArmd6kEk/NdJfo/NeaND+0m/wAmj+0m/wAmsb+0F9aP7QX1r86+qn2XtDZ/tJv8mrnh7TdR8Y6/Y6TpNq99qmqTpa2duhwZpXOFGewzySeAAT0Fc2L9Sa+2v+CcXwd0b4OfDLWvjx8QLi10fR9P0+e50+5u8iOwsI1LT3x4J+dVKpgZ2A4zvFfQ8LcK1s5zGGCpXSesn2it369F5tHjZ7nlPLcHLEz32S7vovTq/JMyf+Clf7Uum/8ABHL/AIJzQeG/DN9D/wALU8cRy6Zo08S4lN46D7XqZHXZbqw2Zz85gUggmv53rWBbeFVUu3GSXYszE9SxPLMTkknkkknk17t/wUn/AG8tY/4KL/tXa18Qr5L6w0FQdO8L6Vckb9K0xGJjDqCVWaUnzZQCcO23JCKa8Mi+4v8Au1/o9wvkNHKcBDC0Y8tklZdElZL5L73dn8e55mdTHYmVao73bfq29X8/yHD+L6f40W/+r/H/AAoH8X0/xpsJxF719LE8WexJng/WpbYNJLFHGkksk7LFFHGhkklduFRVAJZiSAAASSeK9z/YW/4Jq/F3/goj4i+z/Dvw+q6DBLsv/FGqlrbRbDsR5uC08g6eXAHYH72wZYfu9/wTc/4IffCj/gn5Ja+Ini/4Tz4kxRBT4k1W3X/QGP3vsUHK22ehcEyFcgvgkV5GZcQYbBLlb5p/yr9X0/PyOjC5PWxTv8Me7/TufB//AASy/wCDc/WviXPp/jz9oaxuNB8M/LPY+CvMMeoamvDA37LzbxHj9yp8xhkOY/un9s/CfhLS/AnhrT9F0PTbDR9G0m2js7GwsbdLe1soI1CxxRRoAqIqgAKoAAAAFaNFfm2ZZtiMdU56z06JbL0/z3PtMDl1HCQ5KS9X1YUUUV5p3BRRRQAUUUUAfO37e/wCn+IPhmDxTo9u02reH0ZLqJAS13afeOPVozlgO6lxycV8tfBL4z6l8EPHUGvaZtnjZPKurUviO9hPO0n1B5Vux9iQf0tYblr5o/aJ/YHt/Fuoz6x4Lks9L1CZjLcadN8lpcMeS0ZUHy2JzkYKkntX8++JvhzmNTMI8TcOXVeNnKK0bcdFON9G7aSi/iXRttP9S4P4swscK8nzb+E7pN7JPeL6pX1T6eStb1r4S/tIeFfjLp8Mmk6pCl4wHmafcsIbqFvQoT831UkH1rvWPymvzG8c/B7xT8P73ytc8O6rYuvR2tzLEcd1kTchHuGrBk1ySVPs7alcSKvAha5dgP8AgJP9K8Kj4/Y/BQ+r5vl/75aP3nTu/OMoSafz+R6VTwwwuIl7XAYr929tFP8A8mUkn9x+inxQ/af8G/CW1k/tLWbe4vFJC2NkwuLpz6bVOF+rECsD9k79oTUP2g7vxVeXFpDp9hp9xBFY2q/M8aMrEl2/iY4HTgYwM9T8VeA/gz4r+IV15Og+G9UvN2MyeR5MIz3Mkm1B+f519sfsffs7ah+z/wCFdSj1W+tbvUNYkjmmjtgTDbbFIChjgseTk4FfR8Fca8T8S55Srzw7o4KCk5WTs24tRvOVudptaRSS3a2Z5PEPDuTZRls6aqqpiJWtdq6V03aKvy6dW9dkxv7ef/Js2tf9d7X/ANHpXyj+yPqVvpH7R/hW4u7iC1t4nuC8s0gjRAbaUcseBzx+NfXX7avh3UfFf7PWrWOl2N3qV7NNbFLe2iMkjgTIThRzwATXxO/wC8dSrtbwX4mZfQ6bJz+lfI+L9PHUOMMJmWFoSqKlClLSMmm41JytdJ26elz3OA54apkNfCVqsYOcprVpNKUIq9m0fogPin4Yx/yMeg/+DCL/AOKoPxU8LqOfEmg/+DCL/wCKr86/+Gd/G3/Qj+I//BXJ/wDE1Hdfs7eNntpMeB/EhLKRxpcnp/u163/EbM/S/wCRRL/yp/8AKzhXh5lf/Qev/Jf/AJI/Qn44yrP8D/FckbK6Po10yspyGBhbBBr86vhv/wAjz4d/7CVt/wCjVr9CPGGj3Vz+zdqGnx2s73snh57dLdUJkaT7PtCBeu7PGPWvh/wD8DfHFl400GWbwf4lhhh1C3eR30+QKiiRSSTjgDrXP41YLF4vNcuq0aUpLl15Ytpe8nrZafM18PcTQoYLFwqVIrXS7Svo9rs/Rpen418S/wDBRz/kuul/9gWP/wBHS19sRnIP1NfIX7fPwy8S+M/jJpt1o3h/WdWtY9JSJprS0eZFYSyHaSo64IOPev0LxrwtavwxOnQg5S54aRTb37K7PlvDytTpZxGdWSiuWWraS28z0f8A4J1/8kCn/wCwvcfySveK8V/YM8Lap4O+CM1nrGm32l3japPKILqFopCpCYbB5wfWvaq+o8PaU6XDWBp1YuMlTjdNWa06p7Hi8U1IzzfESg005uzWq3K+pabBqllNb3EMVxb3EbRSwyoHjlRhgqyngggkEHgg18RftZfsizfB+SbxF4bhluPCsjZmgGXk0kn9TD2DHleAeOa+5ahvbOO+t5I5o0lhkQo8bruV1IwQQeCCOMVPG3A+A4lwX1bFLlqRvyTS1i/1i/tR6+TSarh3iPE5RiPa0dYv4o9Gv0a6Pp5q6PypvCpsJtuPuGv1R0b/AI8If+uSf+giviz9sP8AY/k+GVpe+JvC1u0nh1gXvLJPmfS8/wASjqYc+nKZHG0Er9p6N/yD4e/7pMe/yivzHwW4bx2RZlmWAzCNpJUmn0kr1bSi+qf3p6NJpo+y8Qs4w2ZYTB4nCu6bnddU/c0a6P8A4daHyh/wUu/5GLwV/wBet9/6Hb07/gmJ/wAhzx1/1x0//wBCuq1P+ChPw+8QeN9c8IyaJomqastrb3izG0tmmERZoCobaOM4OPoaf/wTs+H2v+BtZ8ZNreiaro63cNiIDeWzQ+cVa43bdw5xuXOPUV5McvxX/EWfrPspezv8XK+X/drfFa2+m++h2fWqH+o/sedc9trq/wDGvtvtr6H07dwLc27RuiyRuCrKwyrA8EEehr87P2mfgZdfAr4lXFkI3/sPUpHuNJnP3THnJhJ/vx5xjuu09zj9F65v4m/C7Rvix4Vn0fXLNbyzmbev8MkEgztkjbqrjJ5HYkHIJFfqXiTwHT4my5UoNRrU23CT213i/wC7Ky1WzSetmn8bwjxNLJ8W5yXNTnpJLfya81+KbWm58s/sjftl2/w+0m28K+LZpRpNriPTdSwZPsadoZB18teisM7BhcbQMfXXhzxPp/izTVvNNvrPULaTlZbaVZEOeRyDXxF8Xf2E/GHgG5lm0OH/AISjR9xKG2AW8iX/AG4jjd6ZjJJ/uivGbyxvPCl032m31DR5wSGMkclqwI68kA9a/F8p8SuJeEKSyvPsG6kIaRk24uy2SnaUZpdLarZvov0HHcI5RntR43LMQoylq0ldXfeN04t9fvsfqRrWu2fh6xa5vru1srdestxKsaD8WIFfPPx+/wCCgWi+ENKvLHwc0evavsZRdlf9Btjg856ysPRfl/2u1fHEYm8RT/uzdapN6KHuX/qa9W+Ff7FXjb4pun2qxfw3pEoBku9QXbIyn/nnD98nHOWCr7npXRi/GDiXiGP1HhvBuEpacybnJed+WMIesr27p6mVHgPKMrf1nN8QpJa2fup/K7lL0X4n3lpV1M/hqG42+fcPbLIVGF8x9gOPQZP86+NNd/4KF+K9S8dWFwljBpGh2N4Gu9OjUSXNzGpKvG8jDhhzwoHzAc4zn7S0y2+w6bFApLCGNYwT3wMf0r5T/a1/Yv1K/wDEt74p8G2q3o1CQzahpkbBZRIeWlhB4bceWTIOSSM5xX6V4qUeJY5bRxOQVJJ03epGGspLRpq3vNJp3iviT1TSPj+CqmUPFzo5pFWmrRlLZb3XZXT0b2a0aufTHw++Iuj/ABN8PxapouoW+oWcyhgY2G+PI+669VYdwcGue/aJ+MOk/B/4daheahcwrd3FvJDZWu4ebdSspCqq9cZ5J6AA1+dd5bXvg/UHFzHqGi3anafMWS0kBHGM/Kam0XQNU8damq6baaprl5MdoaGKS5kc+7c/qa/NK/j7j62EeCpYK2JkuW6k2k3pdQ5b+keb5s+wp+GGGp11iJ4i9FO9rJadnK9vnb5EOj6FP4h1TT9Lt1MlxfzR2qKvdnYL/X9K/UzTrT7BYQwg7vJjVM+uBivm39kH9je88BazD4q8WJCmrQoTYacGEn2EtwZJGHymTGQApIXJ5J6fTAGBX3PgnwZjMlwFbF5hFwqV3G0Xuoxva66NuT0eqVr63S+b8ROIMPmGKp0MK+aFNPVbNu17d0rLXrr01Ciiiv24/OwooooAKKKKACiiigAooooAKRlytLRQB8d/t8/8EQ/gn+3nNea3qGk/8IX48nQ/8VN4fjS3uLl+zXUWPLusdMyDfgYDgV+LH7cv/BC749fsRNdakdD/AOFkeCYSXXX/AAvBJcPbIMn/AEqzwZoTgcsgki9XBIWv6a6COK9vL8/xWEtFPmj2f6Pdfl5Hm4rK6Fd8zVn3R/Gbb3kd3HvjdXXOMqciiQAuTX9PP7an/BEj4A/tuSXOp6z4Th8KeLrjLHxH4ZC6ffStz80yqvlXB95UZuBgivyY/bI/4Np/jp+z59p1b4dyad8YvDMZLeTYYsdetkGTlrWQ+XMAABmGUyEnAiPWvs8HxJg8RpJ8kuz2+/b77Hz9fKK9LVLmXl/kfNv/AATK/b51f/gnD+1dpfj6zS6vPDd5GNL8VaZANzalprOGYqvQzQt+9jPUHeoIErg/tV/wUu+AWk/GD4b6T8fPhvcWutaPqFhDd6rNZNuj1GxdA0F/HjqVUgP3KEHgoc/zw+KfDOqeA/Es+i69pOpaFrFqSs1hqVpJaXURBxzHIoYdD2r9UP8Ag3D/AOCn8fw88Sf8M3/ES8W48KeKpn/4Qy4vG8yKwupcmbTHDZHkz5LRDosnmJgiRdvy/iJwfQzzLpRktbXv1VtpLzX4xbR7vCPEVbK8ZGS27d+6fr+DszBGpZH3v1o/tH/a/WvWv+Ch37Itx+x58ZfLsY5W8E+JGe50KfkrbEHMlk5P8ceQVz96MqQSVcL4z4B8K618V/G2l+GvDtm2pa5rVwtrZ2+7arue7tj5UUZZm7KCeelfw3jMhxGGxTwdWL507WXV9Ld76W7n9TYXMqOIw6xVOXuNXv2737W6nun7C/7Ltx+1v8ZY9NuEmXwrooS6164UlcxEnZbqw6PLgjjkKGPpXDf8HJn/AAUltNbvrb9mX4eXMFtoXhxoZfGcljtSBpUVWttJQLwEiG2SVRwG8lONrivq/wDbq/ai8O/8EMf2A9P8N+Fbmz1D4teMllh0d5IQ7Xd9tX7TqcyHjyLcMgVWyCfJT5ssa/nj1XWZ9Qv7m/1S/udQ1DUbiS6u729naa5vriVy8s0rtlpJZJGZ2YkszMSeTX9eeEfAMMpwv1qvH95LV+vRLyj+Mrs/nbxA4slmGI9jSfuLRenV+svwVkNwPapY2AjX/dr6f/Y//wCCMn7RH7a8lrdeHPA8nhfwzcYZvEfiwvpVgEOeYoyjXFxnaQPKiKZwGdMg1+r37Gn/AAbF/Bv4KfZdU+KN9efF3XoQrm0uozY6HG49LVGLSjOOJnZTjletfrGMz3B4XScrvstX/kvmfA4fLa9ZXSsu7PxY/ZO/Yg+LX7cXic6b8LfBGqeJkWQRXOpkC10nT+uTPdyYiXABO1S0hwQqMeK/Yb9g3/g2D8BfCRrXXfjhq1v8Ttehw40SzElv4ft3HZgcS3QB/wCem1D3j7V+oHg/wZpPw/8ADVpo2haXpui6Rp8YhtbHT7VLa2tkHARI0AVVHoABWnXx2YcT4qveFL3I+W/3/wCVj3sLk1GlaU/efnt93+ZQ8MeF9N8F+H7PSdH0+x0rStPiW3tLKyt1t7e1jUYVI41AVVA4AAAFX6KK+aPYCiiigAooooAKKKKACiiigAoxRRQA2SPzPpjGKrf2Nb7932e33evlj/CrdFTKKe4+ZrYbHH5Yx27U7GKKKoQUUUUAFFFFABRRRQAUUUUAFFFFABRRRQBHcW4uEKsFZWGCpGQR706OJYVCqoVVAAAHAFOoo8wCiiigAooooAKhuLGO6P7xEkX0Zd386mooavowK9vp0Nq26OOKP12IF/lViiikklog1DpSMu4GlopgV59Ojuh+9SOT03qGx+dOtrKO1XEarGvog2ipqKnlV79R3drBRRRVCCiiigAooooAKKKKACiiigAooooAKKKKACiiigAo25FFFAHmv7Rf7H3ww/a38NjSfiV4E8M+M7JARF/aVksk9tnGTFMAJIidoyUZScV+bX7VP/Bqj4Q8QvJqvwV+IeteA9Sifz7bTtaL6jZxSghkMVwpW5hKsAQ2ZCp5A4xX630EZruwmZYnDfwZtLtuvuehz1sHRrfxI/5/efKPwn+BfxC/ai/Youfhf+0podjaeOtDCWD+JNJu476x1yWJP9H1m0bCSI55Ekc0cT71k+QRuhPN/wDBOH/gn/qH7GHhXxJ4x8XaZH4i+IkwuLays9JkjlaO0iZgkds8rRxiW6KqxaR0VVaNXaMCQ19ohAO1ARR2rwcVleEr46OYVKa9pG9u2vl5a27X9LetRzLE0sJLBQl7krX+X+el+9kfkN4w/wCCD/xo/wCClX7SGpfFr9pb4gaT4HTVtkWm+EPC8h1a40HT15jsftUirBGy5Yu0SSh5GZ9xyAPtn9kj/gjl+z3+xg1reeFfh9peoeIrdFU+INeH9qamzDHzrJLlYiTz+6VBzgDHFfUAQA9KWvoK2a4mrBUnK0VpZaK36/O549PB0YSc0tX1erGhMCnUUV551BRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFfPH/BVHxrq/wAPP2H/ABdrGhanfaPqlm9mYbqznaGaPN1ECAykEZBIPqCa4T40/su+KPgp8CtU8feC/jN8TrfXfDum/wBriDW9XXUNPvFjTzHheJ06MAQPfA70AfYWc0V4j4P/AG3PCWkfsv8Agf4g+OtTtfDP/CXWMMiW3lyTS3Fyy/OkEMYeWTkEgKpIUjNdF8Jf2wPh38co9X/4RnxB9uutBgNzqFhNY3Npf20Qz85tpo0mxxgEIcnA6kUAemUVxvhD4/8Ag/x58HF+IGl63b3PhBrSW+/tExyRqsMW7zGZGUOpXa2VKg5HSl8PfH7wh4p+C6/EOx1qCXwa1i+o/wBpGORFWBM72KModSu1gVK7gQRjNAHY0V5j4w/bI+HPgH4eeHPFGreIltNJ8YQR3OiL9iuJLzU0dBIpitVjM7fKykjy8jIzitH4RftN+B/jromqX3hfXEv49DO3UYJbaa1u7A7S2JbeZEljyASNyjODjNAHe0V4rH/wUQ+Dtxe+FLaHxhHdXPjTZ/ZMNvp13NJIHlaFTKqxEwAyIygzbASp7Ctf4uftpfDf4H+LW0HxB4gkXXEhFxLYadpl3qlzbRHkSSx2sUjRKeoLgAigD1OgtiuU8O/HLwf4s+Fg8baf4i0m58Jm3a7Oqi4C2yRr95mY427cEENggjBGa+RP2/f27Ph/8aP2Ttd07wP4s1D+2WvNPntWFlf6W17Et7D5ht5ZY4xMoUknymb5cnpQB9zA5FFRWRzZw/7g/lXw1+xh8BtW/ai8B+KvEOufFP4uafeWfi3VdNgTTvETxQRxRTnYArK3QHHXGAOKAPurNFfI/wCyT+0xqfwysPjTpvxG8WXnijwv8IdVS2i8VSWjXFxLAwYvHKIEZpJIiF3EKSN/PA4+n4PiBo914Bj8UR6hbt4flsRqa327ERtjH5glz127OemaANmisH4X/E3RfjL4A0vxR4dunvtD1qH7TZXLW8kH2iIkgOEkVXCnGQSBkEEZBBreoAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooprybKAHUU3zP9lvyo8z/Zb8ulADqKakyueKdQAUUUUAFFFFABRRRQB8z/APBXx1j/AGAvGjMvmKr2RK5xuH2uHiqd/wDsKeNPjT4Xs9L+IHxx8WeI/CFxFFJc6NZaTZ6St4gCsIpJYV3lOBkdT1yDzXu3xy+B/h39oz4Zah4Q8V2txeaFqnlm4hhupLZ22Orrh4yrD5lHQ811VnaJYWcUEYIjhQRoCc4AGBzQB8lf8Iro/hv/AIKs+D9FvrO1tdF0X4cTQ+D7WQHyre4jniRhCDxvW3Eo7naD3FWf2moobT/gpT8BpPD6wjxJdW2qprQjA3yab5aY831XPm4z/EF74r3T46/s1eDf2j9JsrXxZpTXkmlzfaLC8trmWzvtPkxgtDcRMskeR1AbB4yDWd8DP2RPA37PWtahq2gafez69qkQgu9X1TUZ9S1CeIHcIvOndmWPIB2LhSQCQcCgD451Ce/+HumeP/2a7Lzo7rxR8RrW20jDD9zomoiS/nYf7EcdrPGcDAaVBxupvxIF54E8L+PP2adP/wBHfxV8QbG00ONM4h0LUg97LtBOdkQtrmIkcAuv96vtq/8A2bPB+pfHux+Jk2ls3jLT7FtOgvBcSBViYMOYt2wttZlDFcgMRmk1z9mvwf4i+Ouj/Ei70tpPF2h2bWNpdCeRUWNt4+aIHYzDzGwzAkZ4IoA+YPGPhPxND/wVBbSvDOseGfDl9p/gG2h8Mtr2kyajALZZgs6WyLNFslBXlgSSgcY6mvRfCH7Oni/w38f/ABF8QPFnjPwfqerXng+fR7jT9F0R9Oe6jEgkjnlD3EpYoQyhsDhsZ659W+O37L/gv9o+0sF8VaVJcXekyGXT9QtLqWyvtPZvvGKeFlkUHjIzg4GQcCs74Pfsc+A/gdd6pe6Lp19PrGtWpsr/AFXU9TuNQ1C4gPPlefM7MqZwdqkDIBxkUAeYf8EkPAej6T+xP4b1WDT7VdS1q5vbi9uSgaSd47yeGPLHnCpGoA6Dkjqa8y/Yz8O/ErXPiV8cJfDPizwToevr8QNUTWIda8PTahfMguH+zESLcxH7P5OwRqVwFAwea+w/gv8ABrQfgB8NtP8ACPhm2mtND0sym2hmuJLh082V5Xy8hLHLyMeTxnA4ArjPi/8AsP8Aw++NPjVvE2oWOraV4jmiWC41PQ9YutJubyJRgJM1u6+YAOAWBIAGCMUAfKH7V3wH1T4B/soeILXXPFGj6zo3jL4nafqHiSHRrJ7Gx062lcG5iMfmyFFaVUcgsMFvevYP+Ct0ej6f+wpdW8S6bDH/AGlpcelooQDi5jOIB7Qh/u/wbu2a9l8Mfsk/Dvwl8H9S8BWnhexbwrrO9tQs53kuGv3fG6SaV2MjyHap3ltwKggjArhbP/gmN8IYPDN5pNzo+t6rbXUK20b6jr97dy6fEsiSBLZnlJtxujQEx7SyjaSRxQB75Y/8eUP+4v8AKvgj9gn9j3wz8e/hl4x1jWdW8bWNwvjLWbTytJ8SXen2zItw3WKJwhb5jliMnjPSvvqOPyo1VeijAr54k/4JafB9ru8mTT/FVu19cy3cy2/irUoY3klcu52pOFGSx6DpxQBw37FllDofxj+MXwFjuIfE3w38MWdu1pctBEs1ubtXE9pPJGqiZ+uHYFxsbJPAHk1j4r8SWPwVuv2UPOuP+Es/4TEeFIJxkSL4ZZWvDdg/3Rbr5I5yPMXspr7q+C3wA8H/ALPHhd9H8G6DZ6HYyymeYRbnluZD1eWVyXkb/adia8m+C/wc1fxj+298Q/it4k8PyaNb6fbR+FPC6XCJ5l3bJ89xfHGT+8YBIycMI9wwAxFAHvfhbw3Z+DPDOn6PpsK22n6VbR2drCowsUUaBEUewUAVfoooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAChjtXNFeKf8FA/2lLj9lT9mHXvFGnsi63Js07Sd6B1W7mJVHIPB2AM+DwdmDwamclGLk9kceY4+jgsLUxmIdoU4uT9Er/f2Oe/bU/4KSeC/wBkBP7LYP4k8ZzR74dFtJAv2cHo9zJyIVPUDBduy4yR+ePxb/4KkfGr4xX0wXxU3hPTZGzHZaBGLXyxk4zcEGdjg4JDqDjO0V863uqXniLV7rUNRvLrUNQvpmuLq7uZDLNcysctI7HlmJJJJqxax4FdeUQdefNJadj+MeMPFXOs2qyjQqOjR6Rg2nb+9JWbfdbeR2Uvxl8ZarcGa78XeKLqZ23NJLq07OSTnOS/rzV7Tfjp488PzrcWPjXxdaSRnKyRavcKVP8A33XIWyY/PA75zwMV+hf/AAT9/wCCXipFY+NvihpokkYJcaZ4cuFysfQrLdr3boRCeB/GCflH7Ll+f0crwvPjUpw2UWk2/Jf8HRHxvDPDeecR5gsPltSaktZT5pKMF3k779ktW/nb51+Gn/BTb49fDR4ZP+Emh8R6bCMG11+0SfzBx/y1QLPn3Lkexr7e/Yx/4KleGf2l/Elp4V16zPhPxpdRk20DS+bY6qVGWFvKcESdT5TgMQCVLYOOA/az/wCCSq6zLc6z8L5Le1eUl5dAu5dkJJ/595W+5z0jf5R2ZQAtfG2j/sbfF5Pi1o+j6f4L8U6Z4itdQguLS7a0aOGydJFYTG4/1YVMZ3bjnGBnpXRmWE4Yz7AyxOC5KVRK72hJf4o7Nd2r+TP2ng3MOM+GM5jlPECqVqE3aM/eqRfblm7tf4XZr+U/bQHcKKbFkRru5bHJHc1k+N/iDoPwz0J9U8Sa1pOgaZGwVrvUbuO1gViCQN7kDOAeM9q/CT+ozYzRmvy6/wCDin9rvxB4I+BX7O+v/CH4matpFp4i+K1jpl7qPhPXmij1K2a2uGe3kkgfEkZIUlGJGQDjIFfS3/BRD9nz4ofGf48/s/6t4B+NUfws0Pwd4klvfFGjNq1xZf8ACa2pe0K2oSIhZtojlXbJkfv/AHNAH1fRVfUtVtdG0+S7vLiG0tYF3yzTOI44l7lmPAHua4/wj+0x8OfiB4ibSNB8feC9a1ZW2Gzsdatri4z6bEct+lAHcE80V8E/8HKvxo8YfAH/AIJO+MPE3gTxV4i8F+JLXWNIhh1XRL+SxvIUkvYkdVljIYBlJBGeQcV7v+yX+054Y0r9kb4Jnxx8QNBtvFXiTwPol7Ida1qGO+1OaWwhd5T5jh5GdixLc5JNAHv9FNhmWeNXRgysMhgchh6iuc+Ivxm8I/CC0iuPFninw74ZhnOIpNV1GGzWU/7JkYZ69qAOloJxWf4b8V6X4y0WHUtH1Gx1bT7hd0N1ZTrcQyj/AGXQlT+Br4D+L/8AwU+8WaV/wXg+GvwDsdf8L6X8J28MXuua5cJ5Mk+o3QtL0pBNcSZEMcTxRuFj2OWX5mKEoQD9DKKqXOvWNlo7ahNeWsOnrH5xunlVYQmM7i5ONuOc5xXJ+Ef2mPhz4/8AELaRoPj7wbrWrK202djrVtcXGfTYjlv0oA7eikZtorIu/iBoOn6n9iuNa0uG8zjyJLuNZM/7pOazqVoU1eo0vV2LhTnPSCb9DYopvmisjU/iJoGi3v2a81vSbW4zt8qa7jR8+mCc0VK1OmuapJJebsFOnObtBN+hs0VFDexXEKyRuskcg3KyncrD1B71Qv8Axro+l2JurrVNPt7ZZPJMslwixh/7uScbvbrRKrCK5pNJCjTlJ2irs1KKhTUreW0+0LNG0BXf5gYbNvXOemPeqGj+OtF8RXb2+n6tpt9cR53R29ykjrj2Uk0SrU01FyV3trv6dxqnNptJ6b+XqatFNSQP0p1aEBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV8l/8FnvhpqXxE/YzuLjTIJrlvC+rW2s3Ucec/Z0EkcjYHXaJAx9ApPavrSqOvMo0y53Isi+S2UZdyuMHgg8EH0rDFShGjKVTZJt/LU8rPMohmmX1sum7KrFxv2vs/kz+f3wd8O9c8Z27S6VpN9fQocNLEn7sH03HC59gc1pS/DrX9N1S3sptD1YXd1IIbeFLV5XuHPAVAgO9j6Lk1+o3iT9lzwkwRdPtZtDt4xhbTTysdrGPRI8FYx/sphR2ApPDvww0X4eymTTLUpd9DdyuZJ+PRv4fooH41+P5V40YmhXv9Vi6XRczUreb1Xy5fn1Py7C/RZpYikoVMbKNTS8lFOPnaOkvvl526HMf8E7v+CY4+Fc1n45+I1nDceKExPpmkOwkh0Y9pZcfK9wOw5WPqMtgr9uIqxeu5uteCeDv2iNU8JTLDqnmapYj5S5x9piA9G/jx/tcn1riv22P2zYU8PL4V8G6g/2zUog+o30OUeyhb/lip6rK38R6ovuQR9fmnitlcsvqZriZu8V/Dekm3tGKvZ37p2Su5W1P3Xg/wjnlTpZJldK0G7ue9+85u29ull0jFWsjsPjf/wAFBvC/wz1q40nSbW48UapauY5zbyCO1t3HBUynO5h3CggY5INcn8P/APgp1p+qa5Fa+JvD0ui2EzBDe2tz9oS3z3kQqG2juVyR6V8d28SxIu0bRjAA7U6QAj5vWv5mxXjZxNUxn1mlOMIX0p8qcbdm2uZ+bTXlY/pXD+FWQww3sKkJSnb4+Zp37pJ8q8lZ+dz9arC9ivrOKaCRJopkEkciHcrqRkEHoQRzmvxT+En7P2m/8HA3/BX39oS++MeoaxrXwT/Z31FfC3h3wla6jPZWl7O01xB5zPCySAMbOWZyrLIxmgXdsjKH9WP2HL+6v/2YPCb3TPI0UEsMTMckxJK6x/koAHsK/LH9kX476B/wQ3/4LKftGeBfjTK3g34dftDaqvizwZ4pu0YaZuW4uZfKeQA7Ri8MTuxxG9su4KsytX9f5NmH1/AUMcly+0hGdu3NFO3yufzVmWD+p4yrhL39nKUb9+VtX/A8d/4Lp/8ABJDwb/wTd1/4J+Ivg7Nrfh34XeMviHpdhq3g+fWrq+sLTWYw8lrfQC4kkfc1uL6NtzHbuULhWIH1Z/wcTRq/7ev/AAT63DOPiPdf+j9Jr57/AODiP/gqZ8L/ANsfxR8EPhj8K/EOn+OtN8IfEXS/EHiHxFpjGbSrK6bzILOxjuR+7llkjlu5SEJAW3xkncF+hP8Ag4mlWP8Ab2/4J9BmVS3xHusAnGf9I0mvSOI9Y/4LG/sC+Jf24fjV8Mj48+K3h3wH+yr4U33fjHSZNal0m+1u+PmFMy7REUCKiLukDIJJ2UbtpX87P+Csn7MH/BNr4N/svare/AHxn4a0H40abJb3Hh6Pw1401PWpNSYTKskD7p544/3ZdhJujdWRTvxlW9R/4LQ3fgn4gf8ABe74Y+D/ANq3V9U0v9mlfCy3OiQS31xZ6RcXzCUSPLJCylMzhEllUhlRYQzLGxNUf+Ct3xr/AGIfgx+xH4q+F/7MPhT4S698SfFGnRw3F/4E0aDUJ9D0iGeOe6u73UYkdkQIgX55SxaRSflBIAR6L/wVi+NerfHz/g1O8A+OfFl5Jqmu69o3g6+1i8lUb7648628+ZgABudwzHAAyxwAK7v9jn/g2s+BPxd/Y48Ja/8AHXSPFfjj4veMvDtnf6r4gu/E9/DdaFJLaRCK0tUimEHl2aBI08yOQN5XIKbY18N/bg1Ox8Rf8GdPwtMd1E1r/Y/ha0llVxtRo76KKUZ6fKysD7qa+sv2Lf8Agv7+zzY/sMeHb74nfEfSfBPxA8B6Da6Z4u8M6rHJb6xHqNvAscwt7QjzJ1kdC6eWG+V13bSGAA6aHjv/AARH/a08Ufsbab+158AviJr2oeNLH9kuS61bQb2d900+kIly4gDHJUbYI3VCSIzOUX5VAHyj+wDpn7J/7e9t4m+On7d/xg0LxN8S/Gmp3AsfCepeJ7zS7Xw1Zo4CBI7eSNwDgrGhfy1iCttLszV9Lf8ABGT9mrxF+3za/tsfH3WtJvPCWgftWC80Dwil8mG/s8x3Ua3JA+8o86FdwyrNFIVJGCfGP+CM3jP9j/4O+A/EXwF/bA+F/wAF/Bvxz+HOs3cFzqPj7wvYMdYt2kZh/p80TKxjOUTe4EkRhaMuC20A1v2Qfjt8K/8Agm//AMFmPAXgn9mT4nW/jj9nf47j+z9W8NxazLqkHhjUmZ1haOSRmdWVxFtZyzPFJKjl9sbpD8ev+CWvwH8S/wDByf4Z+Dd54Fkl+HHjzwtfeKde0n+3tTU3+pSxX9w8/ni48+PMiKfLjkWPjG3BIPsH7On7Qv7Of7RH/BW7wx8Nv2Wv2Xf2ffFvg3whCureJviRZeEbaxk8OzxMWE1nKsA3bW8pI2GDJI5KEojPWR/wUD/aK8HfsTf8HPXwx+JPxQ1g+FfBFv8ADqWN9VmtZp4wXg1CAHbEjOw8xlUlVO0sM4HNAz6A/wCCq3/BM7VP2m/Hnwa0HxH8TvD/AMN/2PvhpYx23iDQZ/ENxp99qs0SMkKmdxskVYY441MspdQ87j52DD4F/wCCr37Mf/BNv4M/sxanffs/+NvDvh741aXLb3XhxfC/jTU9Zl1CRZVV4XLzzxx/IzOJA0civGp34yrdx/wVj+IXw2/aP/4Lb/Bez/aO8SX/APwyXr3gu31zwlK13cWOi6lJcwPJ57SxbHRXkMAkkyGRPJDFEY1c/wCCs3xq/Yf+C/7Evir4X/sxeEfhP4g+JXinTYoJ7/wJo8GoT6DpMM8c9zeXuoxI7LGqoF+eUuWkQn5ckAj768FftZ+KtY/4I8fA3xnNq11eeNviF4M0Bb3U3IS4nu5tPjkup8qAFd3VzkAYL5GMCvU/DX/BOLwDH4Chtdatb7UvEM0A+06uL6dZBORy6Ju2bQ3QMrZA+bca+bP2W/Cs3xr/AOCCv7OOo+Gf+JlceGPB+jXBWIbyTbWpt7hQB1ZHVsgc/I3evrLwn+3f8OdX+GsOvXniKx06eOBXutOdibqKUD5o1TGX+bIDLlT1yK/J80p5TV4lxMeI+RwVKm6Kq25LXn7Vx5tOZPl5mveSt0P0PL6mZUsioSyPnU3Umqjp35r2j7NS5deW3NZPRu/UyvAXwe+KvhX9mHVPCEniLR08Rbjb6VqP2iZvslqSNyl9u4MFDhCM7Qw5+UVw2n/s9fs/eAvBcFv401zR77xJHAP7RvBr8/mGcj5yiRyDChs4ymcAbsnNYvxW+MvxK+Jv7Fet+Jb62j0ux1TV0S3FpE0Mi6Wd+S7biShfykLjAZQx+61dD8O5P2a/h18MtP1qRfCuoXSWsUkn9oRi91OSbaMgQPuZJCwPCAKO2BXy9SvlOKxFGhSp03Cnh4uMsZJtckpSs4U2tXp707xfLyo+gp0cxw9KrWqTmpzrSTjhYq/OlG6lNPRa6Rs1zczLn/BN3Xw9x4+0Gw1K41Xw3pOpK+jyyk5MDtIFYAgbdyojEYHJPAzXnX7Kn7M2m/tF3XjFvEl5qn9i6JrV1FZ2VrMIl8+WR2kmJweQoQfhzxxXdf8ABOzXv7a+K3xWuJrRtMn1C9ivxYuvlyWkckk7IhXjG1WUdMVq/wDBOBt+g/ELBz/xU1x0+prm4dwOGzPD5JhcalUp3xa5XflajJ8qabbsrK0Xe1lfY3zrF18BXzXEYVuE7YZ3VrpyirtNLd3d5K17u25x37RVzpVh8SPBvwbk16Twv8PdAsIpdTnafy5LvCMVWR8cnC8DG0vKWIJVNtH4ufD/AOBmieA7nUvh74otdB8XaOn2qwns9XuJJLh058siRmGWxwVwd2O2Qd39rXw/pvww/ao0Dx14r0K31zwLq1sun6gLmzW6ht5QrKCyMCNwG114yQrgc8Ve+L3xI/Z98C+AZL/QfDHw28RazdKF0+wttIgkaSRunmBUzGo7g4Y9ACa5swwtB4jM4Zg8PB05NR9qpe0hSUV7J0bNWVvh5Fdzvu7G+BxFVUMBLBqvJTinL2fLyTqOT9p7W+7v8XPoo26Ht/7LvxOuPjD8DPD3iC+2f2hd25ivCi7VaeJ2ikYAcAMyFsds16BXE/s9WMlh8IdD87w3pfhG4uIDcy6Rp0QigsmkYvt2gDDEEFhj7xPXrXbV+/ZH7b+zcP8AWJc0+SHNJppt8qu2nqm3q09V1PxzNvZfXq3sY8sOeVkmmkruyTWjSWl1owooor1DzwooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAqO4gWeNlYZVgQR6ipKKTSaswPG/HPh6bw7fNHIreUxJhk7SL/iO4ridV+bP519Iaxo1vrti9vdRrNC45U+vqD2PvXi3xL/AGdPFFxHI3hfXtPXcSRDqFr86j0Ei5B+pWv584o8OsZgaksRlVN1qb15I254+SUmuZdrO/Rrq/vMhzqhNqliZqD7u9n9ydvyPDPjD49j8D6R+6KtqVyCIEPzeX6yMPQdgep/Gvm3UZ5JLiSV5HeWQl3djlnY8kk9yc17x4z/AGP/AIrS6lNPceHZtWlkb5riDUbaTzPpukVse20Yrm7T9h34r63IFTwhNaqxwZLvULWNV9ziRmx9FNfhuYZLxJj8TaeArRitIxdKfzbfLa777fmfvWQ5pkmDoXeLpNvd88fute/6nkH9viEfvFbnuOa634IfDXVv2h/HsPh/w/DM7DEl7eNHiDTYc4Mjt0z/AHVHLHoMAke/fDT/AIJWXV5cwz+NPEUMdv1ez0gEu3qDM4GPqF/KvrT4XfCfw/8AB3wtDo/hvS7bSbCHnZEMtK3d5HOWdj3ZiTX6Jwn4J43GVY1s4j7GktXG655eVlflT6tu66LqvB4o8VsswtGVLKX7Wq9paqEfPXWTXRLTu+jueA/Bdn8PvB2l6Jp6tHY6Tax2sKnrtRcZPuep9zWH8bv2c/AP7S3hT+wfiJ4K8K+OdFV/NWx17SoNQt0fs6pKrBW4HIwa7Siv6uo0YUoKnTVoxSSS2SWiR/NVSpKpN1Ju7bu33b6nkWgfsA/Azwt8P7Hwnp3wd+GFn4Z03U01q10qLwxZLaQX6KyJdrH5e0TqruolxvAdhnk12fxB+Bfgr4ta9oOqeKvCPhnxJqXhW5N5ot3qmmQ3c+kTEoTJbvIpMTkxodyEH5F9BXVUVoQcV8c/2b/h9+054Uj0P4jeB/CfjvR4ZfPjstf0qDUIIpOm9VlVgrf7Qwawvhf+w78Gfgn4K1jw34Q+E/w58NaD4iha21XT9N8O2ltb6pEwwyToqASqRwQ+QRXqVFAHntz+yT8K7z4Nj4dTfDfwJN8P1k84eGn0G1bSA/meZuFqU8rO/wCbO373PWvH/wBsH4Dfsn+FvFWheKPi58K/hrrfiS6AtdKll8Crrurzx2sQY7Ibe2muHht41Us+0xwrtLFARX1FXzf+3V8cbz4VeJfDGnaDpus6X4h16yvYH8bWfgTUvE3/AAjFjvtzNHElnbTBrmaQQtHFKViP2Vnfd5aI4B6hr37RHw6+F/wh8O+LLzxJ4f0vwXrzadZ6HfxSqbO++2tHHZJb+WCHEvmJsCAjac8KCRn/AB9/Yo+Dv7U9xaz/ABL+Fvw/8eXFiNtvPr2g21/NbjjIR5ELKOBwDivkT9ov9lbxnF8FPDMnw58O2Pij4d+B/DOi6d4C07VZb621mwle5thd39xavaGU3jwKIjI+ySKF7wbV8+ZW+gf+CiGm+PvEHwG0C0+HMerW/wAXJtat38Mz6dPJ/Z+nXiwTNPLfOdsbWItftSETrtd5IFVfPaHAB3ei+C/hb+wr8ILn/hG/B+ieBfCNjIrzWfhbw0wXe7BQ32ayhZ3JZhkhDgZJIGTXKSad8AP+Cl2j3sGteD/CXxPt/BN+LWe18V+ES8mkXTwpKFEN/ArozRSxtkLgqw5re+HPxHsfhv8AsXaX4otdD8cyW2g+GVul0fV4J5vEcskMPzW06uDLJdtIpRm53uSwJBBMn7Fvwd1L4N/Aawj8SbZPG/ia4m8S+LJgdwl1a9bzrhVOT+7iLLBGAcLFBGo4AoA8yj8afsnftb6bo/wbvNH+HHivSdFkl0vQfDWseGAmmq9jugeHTluYFgkMAhdcWxbasTY4U1d+Hfhb9lH9n/4EJceGfC/wk8I/D74iat/wibCw8P29rZa/eSXD2Zs5VWIeaWmikjIkBX5DngZrw/4QXl/4/wDgH4B+ENj4L+IK+ONA+Jsuu393qHhDUtN07w7Z2/iS6vXvP7QuoIrWQtBtjjjt5ZJZPtQKqYhJInK+Hf2Wfih+0h8FvCfw3sdH/wCERt/C+m+MtcmvfE+m3cFumq6lrOoW2ltEF2M0kNv9suD97aJrVsYkRiDPrTWf2nPgD+w7dR/DX7d4f+HNrolsmoNpmnaJLa6Xo8FzLKVmmeCH7NapJIszbpGQEhie5rP8Z/F/9nXR/j1ceF9T03RL7xzBc2sdwtt4Qub/AMma5VZIPNuIbZ4lZ0ZWy7jAIJwOa+QPjd4r8YfGDRPiJrGs2/xw8B6x8Vvg94egfw7ovw1v9YOrakINTF5pVy62Ev2dkll8iQiWAhJS4dBtkHsHwC8aav8ABf8Aa88X3HjYfEbwVJ4gfw23/CNaD4Hvta8P3cx0G0tZUGowWMwWOC53R7hcIoEALYXJPPicHh8QksRBSS1XMk7Purm9HE1qLbozcb6Ozav9x7Vof/BSL4G+LDrFjD4ouli0HTbi/wBVF74a1O0t9OtILZriVp2mtlSNRAjOFcgsB8oOQKwfDn7R/wCzRomk33i3TtP0/Tf7MvLayWX/AIQW/t765u7jf5MFnA1oJ7qd/LciO2R3wjHGATXLftMfDjxN4l+GP7cNnY+H9cvp/FXhA2mhwx2Urf2vMfDzw+XbcfvW8whMJnDcHmsD4i/EPw38QPgtoU2p6p+0T4ivvAXiqz1yPxVYeAb3SNW8Au9rdWq3sNtJp0S6haqsstrNFHb3RWG+eSRBGjSJNbBYetKM61OMnHZtJtel9vkFLFVqUXGlNxUt0m1f1tufVfwf8X/D34q+G2+Ifg+bQbyz1ZJBcaxbwLBM3lttliuCyrJHJG6FZI5QrxtGVZVZSByv7Nf7UXwZ+MOt6ppXw31bSGultxrUsUGlzaauq2rOUGo2zSxRre2rONourcyQkkYc5GfK/h5pvxA/ai/4Jr/F7Qrq1aTX/FVl4j0fw3qtzoR8N3HiqCa3kjtb+4s5VRreSZn2szogYKJQqq61zPxG/adl+OvwYu9L+Gfwo8eWPjS38DTeGE1nVvBt7pdx4Jv9RudNs10wedAhkXhrmd7V3giTSond9sluzaRw1KLUoxSau9lu938+vfqRKtUas5PW3Xtt93TsfQGv/tl/CHVf2d7f4i3HijS9Y+HetXq6Tb3tvZzahFf3LXJtRAkEcbySOZ1KbQh5HpzXE6Z+0X+zN8PfBumePtLk8K2tnqWuN4btbjTPDs02oLqiwS3D2ZtooGuYp1hhlkZGjUqi7jgEZ8F1j9nn4m/D/TdS+GdnYf2XbWPxL8GeKfD2qeHNDmudM0uzkljgulRZS6kwyWEksodvlF2rkAMCcjxh4E8YfD2+0LUvFl58SrPx1pXx3uNQ8U+NvD3gy41T+0dPPhXV7TTL+0tLe0uI1tzbSWVpIFjkEdx5od9zBjnWwWHrTjVq04ylHZtJteje3yLp4qtTg6dObSluk2k/VdT9BvhT8S9F+Mfgaz8S+Hp7q50nUvM8iS5sZ7GRtkjRtmKdEkX5lYfMoyACMgg10dcn8EPH9j8S/hlpWqafdeIL63MX2drrW9EudGvrqSImN5JLW4hhkjLMpb/VqpzlRtIrrK6jnCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKADGaMUUUAGKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//Z'
WHERE AccountId = 'c787480e-2587-4f6b-b725-9e27ace254a5'
GO
-- QMS Laser Print Changes
-- -----------------------
GO
UPDATE SaleSettings SET PrintCustomFields = '
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"7%"},
{"field":"i/u","enable":1,"width":"3%"},
{"field":"batch","enable":1,"width":"6%"},
{"field":"expiry","enable":1,"width":"5%"},
{"field":"qty","enable":1,"width":"3%"},
{"field":"mrp","enable":1,"width":"5%","title":"Rate"},
{"field":"maxmrp","enable":1,"width":"5%","title":"MRP"},
{"field":"discount","enable":1,"width":"4%"},
{"field":"cgst%","enable":1,"width":"4%"},
{"field":"cgstamt","enable":1,"width":"6%"},
{"field":"utgst/sgst%","enable":1,"width":"4%"},
{"field":"utgst/sgstamt","enable":1,"width":"6%"},
{"field":"igst%","enable":1,"width":"5%"},
{"field":"igstamt","enable":1,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":1,"width":"6%"},
{"field":"amount","enable":1,"width":"9%"},

{"field":"doctorname","enable":0},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":1},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "7%", "10%", "5%", "3%", "6%", "6%", "6%", "9%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "Rack No",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "Rate",
				"alignment": "center"
			}, {
				"text": "MRP",
				"alignment": "center"
			}, {
				"text": "Disc%",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],
			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@rackNo",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@stripRate",
				"alignment": "right"
			}, {
				"text": "@stripMRP",
				"alignment": "center"
			}, {
				"text": "@QMSDiscount",
				"alignment": "center"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"saleFooterDetails":{
	"table": {
		"style": "tableStyle",
		"widths": ["35%", "35%", "3%", "15%", "2%", "10%"],
		"body": [
			[{}, {}, {}, {}, {}, {}],
			[{}, {}, {}, {
				"text": "Total Amount"
			},  {
				"text": ":"
			},  {
				"text": "@QMSTotalSaleAmt",
				"alignment": "right"
			}],
			[{
				"colSpan": 2,
				"text": "Rs: @rupeesInWords Only"
			}, {}, {}, {
				"text": "Discount"
			}, {
				"text": ":"
			}, {
				"text": "@QMSTotalDiscount",
				"alignment": "right"
			}],
			[{}, {}, {}, {
				"text": "Net Amount",
				"style": "header"
			}, {
				"text": ":",
				"style": "header"
			}, {
				"text": "@netAmtPayable",
				"alignment": "right",
				"style": "header"
			}]		
		]
	},
	"layout": "leftRightBottomLayout"
},

"saleFooterDetailsWithReturn":{
	"table": {
		"style": "tableStyle",
		"widths": ["35%", "32%", "3%", "18%", "2%", "10%"],
		"body": [
			[{}, {}, {}, {}, {}, {}],			
			[{}, {}, {}, {
				"text": "Total Amount"
			}, {
				"text": ":"
			}, {
				"text": "@QMSTotalSaleAmt",
				"alignment": "right"
			}],	
			[{
				"colSpan": 2,
				"text": "Rs: @rupeesInWords Only"
			}, {}, {}, {
				"text": "Discount"
			}, {
				"text": ":"
			}, {
				"text": "@QMSTotalDiscount",
				"alignment": "right"
			}],		
			[{}, {}, {}, {
				"text": "Return Amount",
				"style": "header"
			}, {
				"text": ":",
				"style": "header"
			}, {
				"text": "@netReturnAmt",
				"alignment": "right",
				"style": "header"
			}],
			[{}, {}, {}, {
				"text": "Net Amount",
				"style": "header"
			}, {
				"text": ":",
				"style": "header"
			}, {
				"text": "@netAmtPayable",
				"alignment": "right",
				"style": "header"
			}]
		]
	},
	"layout": "leftRightBottomLayout"
},

"termsConditions":{
"table": {
		"style": "tableStyle",
		"widths": ["35%", "30%", "35%"],
		"body": [
			[{}, {}, {}],
			[{},
			{				
				"text": "*** You Have Saved Rs. : @youHaveSavedAmt ***",
				"alignment": "center",
				"style": {
						"bold":true,
						"fontSize": 9
						}
			}, 
			{}],
			[{}, {}, {}],
			[{}, {}, {}],
			[{
				"colSpan": 3,
				"margin": [5, 0, 0, 0],
				"stack": [
					"Terms & Conditions:",
					{						
						"ul": [
							"Return of cut strip/loose pills-caps/seal opened & broken bottles not accepted.",
							"Medicines will be retuned only within 7days with batch No. & expiry date.",
							"Please consult your doctor before using the medicines.",
							"All disputes subject to @pharmacyCity Jurisdiction only."
						],
						"margin": [10, 0, 0, 0]			
					}
				]
			}, {}, {}],	
			[{}, {} ,{}],
			[{
				"text":"USER: @billingUser"
			}, {
				"text": "PHARMACIST",
				"alignment": "right"
			} ,{
				"text": "CASHIER",
				"alignment": "center"
			}],		
			[{
				"colSpan": 3,
				"text": "@printFooterNote",
				"alignment": "center"
			}, {}, {}]
		]
	},
	"layout": "leftRightBottomWTThirdRowLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [
		{"text": "BILL OF SUPPLY", "style": "header", "alignment" : "center", "margin": [0, 0, 0, 3]},		
		{
		"table": {
			"style": "tableStyle",
			"widths": ["60%", "40%"],
			"body": [
				[{
					"text": "@pharmacyName",
					"alignment": "left",
					"style": {"bold": true, "fontSize" : 15}
				},
				{
					"text": "Bill No                 :  @billNo"
				}],
				[{
					"text": "@pharmacyAddressArea",
					"alignment": "left"
				},
				{
					"text": "B Date                 :  @billDateTime"
				}],
				[{
					"text": "@pharmacyCityStatePincode",
					"alignment": "left"
				},
				{
					"text": "Patient Name    :  @customerName"
				}],
				[{
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "left"
				},
				{
					"text": "Mobile                :  @customerMobile"
				}],
				[{
					"text": "DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo",
					"alignment": "left"
				},
				{
					"text": "Doctor Name     :  @doctorName"
				}],
				[{},
				{
					"text": "Sale Type           :  @invoiceType"
				}]
			]
		},
		"layout": "leftTopRightLayout"
	}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 15],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": "@headerFontSize"
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}'
WHERE AccountId = 'ee8d79cd-c362-4725-abd0-5cce2cb7a227'
GO
-- Panchamrut Enterprise Laser Print Changes
-- -----------------------------------------
GO
UPDATE SaleSettings SET PrintCustomFields = '
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},

{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"10%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"5%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "10%", "10%", "8%", "5%", "8%", "5%", "15%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "HSN Code",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "PRICE",
				"alignment": "center"
			}, {
				"text": "GST%",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],

			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@hsnCode",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@gstPerc",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["70%", "30%"],
			"body": [
				[{"margin":[40,0,0,0],
					"text": "@pharmacyName",
					"alignment": "center",
					"style": {
						"bold": true,
						"fontSize": 15
					}
				},{
					"height": 75,
					"width": 100,
                    "image": "@printLogo",
                    "rowSpan": 6
				}],
				[{"margin":[40,0,0,0],
					"text": "@pharmacyAddressArea",
					"alignment": "center"
				}, {}],
				[{"margin":[40,0,0,0],
					"text": "@pharmacyCityStatePincode",
					"alignment": "center"
				}, {}],
				[{"margin":[40,0,0,0],
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "center"
				}, {}],
				[{"margin":[40,0,0,0],
					"text": "DL No : @pharmacyDLNo",
					"alignment": "center"
				}, {}],
				[{"margin":[40,0,0,0],
					"text": "@pharmacyGSTinNoOrTinNo",
					"alignment": "center"
				}, {}]
			]
		},
		"layout": "leftTopRightBottomLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["75%", "25%"],
		"body": [
			[{
				"text": "Doctor Name       : @doctorName"
			}, 
			{
				"text": "Bill No       : @billNo"
			}],
			[{
				"text": "Customer Name  : @customerName"
			}, 
			{
				"text": "B Date       : @billDate"
			}],
			[{
				"text": "Address                : @customerAddress"
			}, 
			{
				"text": "B Time      : @billTime"
			}],
			[{
				"text": "Mobile                   : @customerMobile"
			}, 
			{
				"text": "B Type       : @invoiceType"
			}]
		]
	},
	"layout": "leftRightLayout"
}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": 12
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}',
PrintLogo = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wgARCAFpAYADASIAAhEBAxEB/8QAGwABAAIDAQEAAAAAAAAAAAAAAAYHAwQFAQL/xAAaAQACAwEBAAAAAAAAAAAAAAAABAIDBQYB/9oADAMBAAIQAxAAAAG1AAAAAA8D1G4jW5Y3BrxTqyrm8dW9t6vnkbvnL4PNvc43vsZT1oAkvbPUpLctQuFApVbmdMTXNOO02yrlRT1F7u6/KLsdTLxnnsk6sF8vosVCpI+h0RfSAAAAAAB8wSN0igHMKdIOvW5yNiwJGxj1525UtzOVv5k1Q98w87rvPYjHrPQcpPy34TRsRb36n0GN/U52rRjeDN0WTbmD6Ee3+u087l/HXShHOVOFF9dJhFM3S7UmrvttqSkaOeAAAAxe1fBr74Yl1bc69h25vCkg1z4ewAAAAGmG4gvBq0LY+aYmHnsqiWfQzrAztBnwSltXqZ6rlOyjKxYkAADQ33ntd+dfk8/vTDqQqa7OODFAAIhGzixsR7BMfmfMYwMYgAAAAAIJG7egeMn1L3yeee5ej24JbzesMHZAMnR+4rsKcjPgeb9ybMEnb/GBKkAAOJFpfEcjW+LErydWVbY084A0akkEbU6V3ORbnnu36OcyAAAAAAHDq7rclLqQq0t234JPHOW04NKotnzDN0ACS1lZVablIU7me5KUtJnB7b4iFmPMlc5lGrAR+QOpcWK93hZGt8z6C2EwuGlnuX1K6gzFz6R7CZzjW2X+MCVIAAAAA4/YgUGIeEOzALKkfN6WhxEdjsjjmPqgi4+Pv4nGTVnY1c7QFGwn8AnF2Zv8Lb081MFWUhj3jFG1reewn2JZqbe9hBbXipuxK3V33f4E7g5Mg7ygAAEZ6UbOoJVgCpbXpZfY8CvRvPfDy5tjFl0uF4sVnUEydT0ZuiNm2vcgM3hGveFOmn0BtC/I4ep3eHmL+BVkPRJfmQ7GQD6IBX0T63JR69ZtZWrYl2A1zoAAQuDXbXq+1qWHTnQgzbrX2G+c5lS2dWKvQgvtPPXsbq+/PdHhkFnXCWYjAwN12eLMn0olEujzm9cK3MtywKwHOX8jMnSz4L7OSzMV7m8YoC2oAAKZwfP1ndutWqrNvypAGudAABgzgpfFIY9n9nKbEpO4WcLiVxYdeVvhRrD32F2DR4cAgmtNYZja+51N+sXo6Yo6Zkxzqakm3R7kA98AAAAAACkPrJjzu4T2BSOxKyg7ygAAAFfRORR1HrVhV7MJVdCv7Ar8iFOq+vn69rusaPEADXVnBpxhLrD2a+1as+j8flz9hov1WEum+Pq2v1qc+Eu2jcj989EvAAAqflS6Io9ezYUGrnzVrZT3HBNcAYssEjdE8Ih2aWROZ2I79e2JXftQU6r7+Mntd0mjo8RvcSKRmjW29PodqjXivVnfduzeL1fvj25HO1djSzdDU9M3R+PMj3zzYwSVhfufZvYYAAAHFqy7apX2uQFehSSNpUXNnpjus4FleV1xPYyuEC28EGlkV3cTGHwK4smtvLQo12THl9rujBnaPEaux9AGEM0f5Ojm6P38+e5el5k3Ji+lGt3ttHP4mnJx5X8u2/YTzBlcAAAByuq89pX4squEus+BW8AAAAOl7X3p7iyv8dHqzs+sFt0KNhmw7EqrlGhxI8DFCcmnkawZz7c1Zy+jn+jZyAAAGMMjSy+GwPQAAAByOu8lU/Ku3jUbFVplyKdPiOh9Ru5qSSWasOsvaNYATV4VX2hV6nRBRstvU3pU2+NDikY7kGSc98MTaHso9+SY8nRc8FkABytGOou7uj9MrUfP0rs++rx19E43K7k+pmd0OKAAAOF5Luq75dWhbCp+57GeMOa3PAADg1hZ9YKdEFGy6PO681rWMb/GxbkHPb4UXOrypO6p3Rt4oA5XVg69+l6YG8PfDx857IYhGTz157LevX893cP7DSzT+Ksrd6PDE+oCNwB3rPpK4W+b3BfkAEfrKya2U6MKNh3uDJ7ErG43ZjLHLcLw53oAD2YwydaebtjVzADFX03heXpeHubpfUj6O7tY3x9jijk9ZH2AYp1CMfY+ZLGehGU0+PuHbWTFeaIdl9SjuSRrn6Vb2ir0IeTW1W9tNc96GMUAiVfzOGJ9QFOmmkLsO7MlUSlsVnz/ABRgboAnEIl+nndQauWAasEsWvs3Q+O/wJrTdvjYyQAAcLu+QlXvvmPn9+x6itOm9tf2Qx6y6tCRB3ltCD2OgzVfTsFFjU2y3OAACso/s6yHZBBla9XXMzge8Dv6V2NCHnvN9EHgkUd3W1ZuN3EAEXlGKuyvbDgU2Sc2xo54AAAEI5+9pYG7L6ptip9Wa2ams4974a54AAAAAcfsV9BiJhDswCSWTHpC9x4WKQPXkkcwdzwKsvPXpNd+DzjoMAL6QDna/ZRkEogAAwZ4vXZwtrX7OLsSSmLtqbWr5cpi3q+3djidt/jg9iAAA8DTqLtcJPpgp1XR51lW58i9HeUAPmBT/lLMRB57g7oeDs8ZfVYiIyrcw8gtrAAAAHDjLYh/vmJteTaOzF1FC5pgeWpls6yHaZrJrBNK7VcSxvnO2+fqaxij/k5HXnK5i26C+2Oj7XvWdr7D3IhNYAAI3H7EjWe/wHnuRrB4MmNKPf60KPJWIrrOxRPkCx++TvkxX7ou29MReZPZk0tmzmzjB6cGsrr4VOnV7a1VOlDyfuTE9rDyYHpsTWxOOWVset80E1QAAAADkRawPhVmv/JFw8vTwhZkPAAHoePOrdVyupIOjpZuDOPoh6AABhicyRtqjlXZyqdKp3vOq0N/YkFi2KVnI5Utz8WUsTAAAAAAAAAADQ50gVziuGYKbodkloI3u9dbX8fZbUAAAB//xAAuEAABBAECBQIHAAMBAQAAAAADAQIEBQAQIAYREhMwFCEiIzEyMzQ1FSRAQVD/2gAIAQEAAQUC8h5IQYW7A3CXZ1x9lLfjpJ3Yq9WdLcRVbjZJ24yyltwd1ITBXglwM+MbwPe1iPsQJjrN2f5A+f5CRjbIqYyyZgZAjf8AEvtkq3ALJFnJNu5689oZJgZHu3pkadHk6yJIwYaeV+L8Ttyoi4GWYWRpYz+edYhi5LmmlLqEJDKGlkPwVIBuMr4jMaJjdXCG7CV0R+FoxLh6qSLF9l05c1rBGjxpNgrs/wDdBseVw61VxK+PnoI2LXR8fWYWKcWIvPSHOVPI5Ualjbq/ZEr5EnI1PHFjWo1PFJihkpNpyCxGq50GGOAKSd8ldYkZZKjG0bN0qEw2EY4b8rpXQ7wlI0Q7KwfLdpGjlkvhVIgefmiqVoAuOVxyagEpzfLALwTI6SBYqc0rz94O8j2jZZTnTCaVtY+TgRMCzxyZIozZF25cJYSiY57nZw8AjSTZHqC7K4aBjWc1ZhaOb3R+CzH0Scgk7cvfcTvUk0qqzr8tlbILHvcR+IiqtbVNHloboFsGzul4gkdAcCRwSxjNODfbN5xsd7NavU3bezehulNX9zy3Fkqu1ooXSzJJO9J2VTOqRal7s/Th6RyfvtP0cX6Q/eJsmyGxY5HuI/KqF6syJyTx28v00bWGH1EpE5JMJ2oyJyTZV/BEVepdAFUJmqjm7rZf9XHezY7ekGy7ld6TgmOKSJHbGB5LSR6mZrw4LmXLdf8AX2u+TR7KYncr3uRjZE0hlVqLg3vHkWf1Llu/mTGt7j9llI9NE04ejeW2P2IOygZ0wMuPpsf9lz8FXs4cd/r2xebtfrlbJXnJJ3ZOVY+s+y/P3JWNRXOjCQAPJxEXmXZWs6IGXH27H/bxCvKDs4b+2YvOZsXniJyT3VYoUAHUr0GN71I/KQXcn+WzJ3Z+q/SP+DLZOcXZy5rxGvyNnDjfkS05TNq+yV0VW7b8vRC04bH8vca3GGTHnx5G17ulnPnsX6C/FkofdjtXmmsZvVK4kX22Uo+3XWwuTtsKFyXbxETnJ0om8q7db1quXIVoaPkY45AtLN3RA2L9GfZpKH2pOtUzqkcRv5yNRDUpWNRjFRHIeuc3FjnTGxpDsHXEdkeKIG+1f12OlR/N33kLtPyFKfEMArTiy8XlW7U+mlqLqFovsleLsxrA3qJmvD8frN5TO6zaVH83ecTTBIxRkyildo+cQ/o+BffJAfTmyEHvyLqV2I2rGue+GBI0fyt+3SidzrvBej6LDEVUWIbvxuIv0/DMjpIE1r3ETtQIkuQ6SfWihK3zt+3Thx/yfBxH+1pw6Tqi8R/q7E+u1/bFllNdMLrUVvdVrmu85G9BNKEnRO8F8/qn6cNu+dxH+rsb92yQZgB2M58x+tZU5YS8HzGorF6YydHfjXtdo+QFmEshJgSSZqp7JutGduw0C9RFCRphbivQYzEUpdOHP2uI/wBTYz79Z9iKLkqSSUTSJBPKyDXCiYVquGtaRuPhyGYi80zobnQ3EREwIlOZjUY3fxELkfWon+lf9d17N6l14bb8fEX6Wwf5Mkywxkm3BC4nuo4ckmApTOyNVxgaOcjUJYiTH2BnY+Qd6bahny/BcA78HZW2bo2BKwzNFVESytk5bKAfRC4h/R2D/JjwieqRwpiIibJFiiYRziroNjyuHWvXErRYtaHCVr0x7XDfXJyheG0jell7BEeJw7iU3HXUlUkSTSNrGKR4BoIN/wDz9gvy7SkaJkqS+RshxVkqxjWN2GEwwwjQQvDYxElx3tVjvHQReomX387YH82wxWhHIM+QTWKBZJGojW/8NrXpJRzVa7wwYj5ZhDaIeXn83ZH/AGNVXkkqQskmqIrnRgtALe97WZ6uPjTif4p0AUtJcA8XwQIBZaxgDjC0u/5uyJ+3raH6nbKkO+VNYFSyjlzpTRWouMc4eCsCtyPIGdPBIrYx8NRvTH1ctueilY2DKdgqeS/ItQAWfTZd/wA3ZB/d0lm7APfZyVyjYg2bZ8xeaJy3/wDsGb3F/wCK8/m7K73n6WZeuRsrGdcrbYSOyJPZPAvvlfJ7zN0q0jgw10d2OsJbsbYS25FunooisMPbefzdlSnOywjkYzmrttQ35O2YTuytnNFXtkx3w7GEURGOR7dZUgcYc6yLJ3U8tY8nbe/ztlInOyy1dyi7a5OULYV3QNv26Na57wVzExrWsTSRBEXCjeF+lSTmLSbKZEDKkEkl3wyd2Ls4gX/Q2cPN5zct1+PYv0ifq7J/6WiIrnRI7Y49skDTje1w35Xv6JmPcjGz5TpcjGornGq+xXbaxvRA2cRu/wBbZw234stf29i/SGvOJslt64yLzTKkXNd9qHmPEd0vziCT0j0poHYY9qPbMjuin1gRVlyE9k2cSO+PZw6zlDy2T/Y21i84W17O2T6ZAZ0Q96pzTp6HE+zLAvfmZSxu/K0mRRyxnppDF/xkzI9KVyxo44w9t6/rsNlUPt1+W7flbah/w7bUXSR/4x/j8E32mk/HKf242lCPogeSQTvSNRM7pUTkmTB92Ki802QidqXtKNpRmG4b4juuL4Ja9Ux3ult7V2lX/P8AHbG7MHZQh7k3WQPtSNipzSEbvg2zYySGVfUgN5yIISc8jN65VqnOu0oyddf4+IJHWfZRg7MLW2F8G2If05vNaG6n5Us6jGZ3BaUEjtyPFMkNjR3uV79YEf1UpPZNXNRzSjUJdtfK7XlnSuw3FXkkEPZj5Zi7M7EXktXNSWHwKvJLab6s2yki9iNtsI/eGnvuiTXBxj2vb4Zc9rMXmrsrw94+nEQObdAleElfYjlJuc5GttbL1G2oh+qPvsYvv9dw3vE4VkqYOYAm0hRjwlkJMPJMbPpoNjikAJoRaHG0wpAXRzaxLcwcBZxS41yOTCEYNJFxHHkyaaWuyDEfLLHCwAvBNhe6e+9ffETpxClTO8bFc92I1E2DY8r4sdsdmy1g+rG5Fa7Yntncfvr68ktY4GRxeKXCabCseF3kVeWR4RDYETAs3WNcyWkgBI7/AAhCQ7oVM1uInJPI9jXtNW4URBYnv4E91FBMTI8MQfEUbCsk0jVw1fKFi/DsGIhMDUyiZHpQswY2jb/wliAIrqxuLXFz0EjPQSMSvPjaxcZXgTGMaxPNYfimfe361X1T6f8AxP/EADERAAEDAgQEBQQCAwEBAAAAAAIBAwQABRAREjETICEyIjNBUXEUI0JhgZEkMFJiof/aAAgBAwEBPwHlYgPPdUTpTVmBPMLOgt8cPxrgtD+KVkz+qWKwf4pR2qOWyZU7ZiTyyp6K6z3pg2ybnalBb/8AtaSE0npSw2vajt4/itOxza7udhg3y0glRba2z1LqtOvAymZrlT159Gk/unJ77m5Uqqu+CEo7U1cJDf5Z/NR7s250c6LUyaDA+6rTUTUut3+qRMuiVImaPCFE84W60hknrQS3Q9aZki94V3qVE0+MNuWLFKSelKYYBgdIVMuiN+BnqtG4Ti6jXPlRM+iU1bJDnplUi3LHDURVDj5JxCwlvcMOm607BcaaR0uRFy6pTLnFDOpDXCNUxaaJ40AajsDHDQNXG4avtNbczDBvnoCosJuMnTf3qZKSM3q9aa1Sns3OuLYfUS0RdhohQ00lUhlWHFBeS3r4Fq4J1FcbVF4YcUt1q6S+EPDHdee2RuC1qXdcLo7xH9PtVvHwqWNpTUbjmF5a7XP4pponVyGlt55dFowIFyKoA5N51cC8SJhEY47qBSqgJnUh5XnFNeaK1xnhDF0tZqVQPLwPtWrMn2lX94XZP8f+aiBpaTCQyjo/ugRGgy9qec4hqWFma6E5/FXNzRHX98hMuCmpU6Y2YM3FL2wLbC3l0UcHlybVatKf4/8AOF4PJlB91qIaE0mCrl1WpUrieEdsbeGiOKVel+2KcltnZLwXNvSptsE01s74WUfAS4H2rhFc4bmeE5zSGn3qA3w2BRcLq9xHtKelNuE2uY19c7RvG53LyNJkApV6T7YryxXeKyJ1do/Dc4ietWfyV+cHOxcYslFDI/SozSzX9S9qYTZKR28/X0pVz6rztLmCLV0b1x1/XLbUyjDV2DVHz9qs/kfzg72FjFiHJLIdqZZFkNAVKlhGHMt6Jt6Z90qNkw7kwECLZKVNK5LyW9zXHFaMUMVFakxyjnoLFhknzQBoBQBQUq6LlGWrP5C/OD3ll8UiZ7VGtil4n1ySvqoscdKL/VP3hV6MpXjdLPdabSVhpSpTnDb6ctnkaSVpfXCRHCQOk6dtDwr4OtN2l8l8XSosQIw5DheXegt/zVm8lfnB7yy+ORiFn4nKEUFMhqRN0LpCikOFutC+4Oy09IV4UReUSUVzSoU0ZI/+uV10Wh1nUh5X3FNas3lF84P+UXxjEjafGW+EyRoTQO/+oSUVzGmLwSdHUzoLjHP8q+rY/wC0p66sB29aky3JK5lthZvLL5wk+SXwuENniFqXZMCXSma04amSkuMeIrniLagaAO1Kyo4zZ7pT8QmuqbcgARrkKZ0FqkFv0py1vtpnlnyWbyy+cJi5MH8YR2+GCJhNLS3l74x2uKeWKuAm60hIW1b1KZ4R9NsIcIpK/qmWAYTSCY3dhANHE9cbMn2l+cLguUYqYHU4iY3Fe1MbcnctKqCma0/LJzom2CEormlRZPF8Jb1NDU3n7Uy0rxoCetNNiyCAOyVMuRGeTeyf/abcF0UMcLw8hELaemNpHKP84XYsmMveonmpjcE6CuNvLxKNTzyFB9+QCUF1JS/cb+aszeZkftV0e4bOSeuDEt2P2LR3SQSZZ5Uq59Vxhhw2BHC9OdRCmy0EhYzA1NfGLTitkhJU4kNBJOVnownxVl7Tq9J4BXmiM8Z1AxnPcZ5SwiOa2/jF9rhHljqXLTyNNq4SClSS4bK1ZnMnFD3qUxx2lCjBQXSXLa4nCDiFuuFxk8BrJN1xjPcI/wBVvg8yjo5LTjRNLkXKIqa5DUaPwU671Od1FoT0ph1WXENPSgNDFCGpkAJPXZaehvM9yYA2bi5AmdQrXoXW9/WDrotDrPapUlZDmteSLK0eA9sSFDTIqOAC9q5UtvP0WkgOe9Bb0/JaBoW0yFKlSkbTSO+MCf8AT+A+2gMXE1CuCtAu6UiIm2Emc1H36rUmWcksy5mZJtdPSm5bbn65XHwb7lp6cRdA6crT7jK5guVN3h1O5M6G45pnppy8GnQRp24Pu7r/AKhcIO1aSW6nrX1j3vRPOFuvL//EACsRAAEDAwIGAgEFAQAAAAAAAAEAAgMEEBESMQUTICEyQSJRcRQwM0JSYf/aAAgBAgEBPwHpdI1qM59IyOPtZK+S1uHtCZyE/wBoPDtrSTMi8in8R/wEa2U+0K2b7TOIO/sFFUMl8etzg3dPlLkGl2ybB9oRtHQYmlOhI2TGalPW4+MaJz3Kp6LX8npsEbdgtDT6T6SJ/pT0rofkNlSVev4P36XvDQnOLu5TIc9ygMbdRmaE2XUcBVlTk8ttqOHmP77BCQE46CM9ip4+VJhU0vNjBuTpGU52o5UUXs9TnBoyU+QuTGaiqh/Jj7Xpxy4M/aBwmu1DPRxEfMLhx7OF5n5OFCzJyeuV2TaEYauIu+QbeQaWtbaA+lLM2IZchxBme4THteMtVe7MmFw5vxLrPdpGVumt0jHU86W5uBgKv/lszu4Kfe0Pkqx+qU2p5zC7PpPcZX5+1DHy2Btpz6UQy7o1C857Y6OIt7h1oRmQBTeVoB3VWwtlNgM9gqSl5fzdveQ5coN+iWP+wUcuOzrT72FqqLmR4tQR6n6vpSHLrQtw3KkjbIMOX6CJRwsj8R0HdQb9Lxg4ULsjCn8rDe9VSkPyz2mtFPHp92jZqP7B3UJw7pl8lCfkp/Kw3u94anOLjkpjC5PqY4DoTJmP8TZz2t8imnUMjokGHIdkx2oZu52kZROVD5KfysN7Ol/ytD3JsH+kS1g79lIaS2o/apIubJ36Z2+7NcW7ITj2jM1PeX2gHtT+Vm79E9dj4xpzy85cqei1jU9Np427NTqeN27VDTCFxI6d1JHp6QMnATW6RhT72bvesqtXwbtaip9Z1u2/axlOg+kYnBaHfSbC4pjA20+9m7i1bPy26RubNbqOAo2CNoaL1NYI/i3dPme/yNmVMjNiqerbL2PY9BOEZmoTNPRPvZnkLVEnMkJtQs1S5+r1MvKZlb2ETzsEWlu4WypZuazvvaSTQnOLt7wOyMXn3tF5BTu0xk34cPI34ifEIAuOAoKRsYyd7FocMFVdLyvk3ZUT9MuPtOOBlE6jlMi7d0RjtaBvu83laHyVX/Cb8OPdwvxFvxDlw9mXl310PaHjSUMxyfgqc9sKFuXWcwO3QhaOh5y60A9qRutpbeifol/N5YxI0tKoWlhc09M3ec/lVG4UG/U92kZvG3S21ZHok/NtlTzc1mb6RnV0SyCNpcVTNMkwKnHbKY7ScoHPTM/JxaJuo3qoeaz/AKtrQTGF2QopWyjLelzwwZcqmp5x7bKhh0t1n2nDUMIjCZIWprw6xIG6kmz2bYDPYJjdIx0VdJr+bN7teWHLUziDx5DKHEGewjxCP6T+In+oUkrpDlxVLSmQ6nbXkj1dwiMWybtjLkxgb1T0rJe/tSUkkf8A3pjgkk8QoaFre7+/SWh26MAXKQgCETR+06Nj/II0cJ9L9HD9JsMbdh0//8QAPBAAAQICBQkGBgEEAwEBAAAAAQIDABEQICExURIiIzAyQVJhcQRygaGxwRMzQmKR0ZIUQIKiQ3PhNFD/2gAIAQEABj8C1mlcSnqY0aFr8ozENo84+cR3QBGc86f8jGcSesbIjNJHQxmvuj/KPnE94AxnobV5RpGlp6WxmPJngbNRNagkc4zcpfdEZrP8lRstecXNfg/uM5tB6GUZ7a0+caNaScP7OTelVyu/Mbfw04I1N9XROqTy3RJ9sK5psjRuDK4TYadIq3cBeY0ejT+TE1TUrFVte2NrLTgv9xIZq+E6/J23eERpFSRwC6pJpCl9BGkUhvzMaRS1+UWMI8bYzUJHQU5yEnqItYSO7ZGicWjrbEwkOD7IkbDTIWmCe1Ok8idnxjJ7PYOM+0E3k3k05LSSo+kaV2XJEWpKuqjHyhFgUnooxo3j/kJxnIyhii2LKAjtBs3L/esJUZAQW+yGSePHpUmlOSjiVE3NKr7rvxEkgAavSoB574yuz6ROH1RkpBKjZKPj9pI+L6chGdYjcj91J3NC84wEoACRuFfKGY5xD3gocEletAZcOadg+2qK3DkpF5jJE0s8OPWnJZTPE7hAU7pXOdw18piFdpWlIUBtStjLXZwpwqBsWb1HAQNlCE2amVyxak4RaJEWEUZ3zE2K/eoKlmSReYssaGyn3pC3JoZ81QENJCUjcNZN5YT7xLs7cua4tfUO7ZGctR6mFuqRkoKZAnfFnyk3czjV+KuwrzjyEWfJGyPePgOHSIu5jUhYucHmKEYLzT7aj4TZ0Kf9jSHu0jN+lB39daW+zSUvercIKlqKlHeaAAJk7oDnaQFOcO5MBtO0vyFVDfEbekJYT9dp6UJcRtJM4Q4i5Q1AVwrH6omLxbAI31v6ds5yto4CkdofGZ9CceetUwwZAWKV7VP6lwZx2OQoWvdsjoKq18CZfmHTuSckeFK+zquOcn31C/D1pZ7g9KqnFbrhiYUtZmpRmaJr+Sja58os1madIuxP7qNtbibekSF0OLF4FkAYVVuHeon8QVHfbSh0fSZwCLjXA4lCg9IbTgkCr8NOw360JQgTUoyEJbRu3461ShsJzU1HXcBkihI4lj91j/1etVrFOb+IKlGSReYzCW2+V5i23rGjcUnxsgIfklW5QuNDSMJqoQjiUBVWsbVyetKu0K7qffWrUNo5oqg8aiaGe8fSqrpC09E+dV1OC/aEMi7aV7VLY+A4Zn6Djyhxe6eSPChThuRYOtUNDZb9aAlO0bBCG03JEta00PpGUarA+wUMn7vasOaxV7R1EPdQPKrmnJULjhEoASJqNgEJQPE4moparkicKWraUZmhJ3IGVrnlfdL8VDDfdFAPCoGqBiQPOGR9/tVeViuXlD3WflWtj4zozzsjhFXI3uGVLzmJya62ltLzTKYiTbgyuE2GqVHcJxM77ahhHShxGIgGoyPun+I7OOtVvFWdCXhdsq9qoxNwG+A4/tfSnCs23wpn+aUHiJPnXV2hi1V6k49KAlc3G8DeIC2lTTS+fsNYdKXE7jnDxqLXuQJfmGkYJnUS2m9RlASm4CUSNoMT7ORk8Kv3FrC/CRixlXjIRpFpSPttjMTncRvrvnA5NLHd1Hx2xmKOcMDRlou+pOMJcbM0qoc5kDz1IdF7d/SmZgZW0rOMOuDZnIdKinzciwddc4rFRNLHd1C217KhKFIVtJMjR8FWw5d1oHNY1Rb+m9PSgT2EWq9hGQk6RywchUShAmpRkIQ0ndecTrh0pQOEkeepJ40hVExYRdDbvEIb/wCz2OqlcoWpOEfCCdLOWThE1HNTaTiYU4vfcMBU/qXRadge+vHSl5vBU9S13PelaOBXrDXf9qorKeXkpstVyiyxpOyPeoHnxo/pSfqjNIO6zXqRwkinJ40y1MuBIFL6cUgw13/aqnrVK3VSSI4Whcn91A72sdG/3BZZNv1Kw5RNolB5RJ5GVzR+o+YEnBVkZqgelGe6geMaMKc6CUWH4LO8pvPjqHxicr80ocTekzhLjZmlVorqWvZSJmFuKvUZ0u9z3hvv+1VPUVJbbvCPeMt0zwG4U5iZI41XRMZ7nGYUlKsgn6sIzHEHqJRsA9FQDRsj8RsiLABCWxvtJwEBKRIDdqG3eIZNT4bp0Kv9TX/pmzYNs+1R9XICEf8AYPeqnqKJvLCeW8wUsaJGP1RZaYzGHPESjSrSgcrTE8n4isV20TUQBGjynOl35jNShHnElPKlyAFZbnEZeA1K5bSc4VQ27NTPmmMtpQUnlTM2CC32Q9XP1VyuNU4HfFVHeHrRNbaFHEiLGm/4xYJVMns4Czxbv/Ym6orPO6mTSSo+QjSOBPJIi1Th/wAosU4P8o0Ts+SxGS4kpPrDPdnqlAfLVnJq5TS1IViItyF9RFiWk+EaZxShhuqpQnaUZCENpuSJR/kKrfeHrWK3DJIiWy1w49ak1WMj/aAlAASNwqlDgmIQ2JkJErdUU3LFqTzgpWJKFhGsPaFXJsT1oV3h61W+8PWqVrMgIyl2AbKcKmT9A2j7QAkSA/sviNWPD/aClQIULwdVkJsSNpWEJQgSSmwUOdR61Wu+PWpM3RP/AIxsj3qBKNpVggIT+cdRnKA6mPnt/wAozXEHorVZ2a5uWIz05SONN2onstcZ9oDbQkKXfD1qsd9PrU+Am76/1VL57qa+SM9zhG7rFq8kYIsi62i0CNGtaOhjSpDgxFhjRqt3jeNTMoyVYosjQvA8lCPlZXdMf/O7+Is7O542Rn5DY5mcTc0qvuu/FV3w9avZ/wDsFKl79wxMWmZNpNQITtKMhCUJuAkKxaZNv1Kw1AIJChcRAbesc3Hi/s3PD1q9n79IbGy361crcgeZrSR8xdg/erKV/MTfz515T+IvBMaJKGx+TFr6/CyLH1+NsS7SgEcSYC21BSTvrOeHrVY6+1ClKuAnBUraVaaq18SvSss7k5gqyFp5Wx8p3+BjPCk94SqJcTenzEBSbQbRUy3VSHrBSNG1wjf1rBJOicMjyONZfUetVvkCaMnjITWZ6TqqVgJwJ305DYylYRN85Zw3RJIAHKmaR8NeKYyHBI+RpU0foNnSkrX4DGPiOm3cMNQ0s3qSDV6rFVRwRQynqaphnuD0qv8AcNISgTUqwRIWqO0rGtkK8DhCkL2k0IwUCmgqUZAWmCs7NyRgKAlIJJuAhbq7XhbIbhWYBvyBVaTiv2qvq6ChPc96zPcFV1OKTANC3j3U++o+MNpF/ShCuFQNCezpvVarpT8V0aZX+ogpVcbIU2rwOIqBH03qPKswjkTVWriXQ2cUkVm+VlZbfCZUND7Z6iRuhSOE5MK6UOr3TkOlGUoZjdvjupyXR0IvEaLJcT+DHyT+RGnWlAwFpjIaTIetZQ4EgVWRvyZ/mhtfCr1rOt4HK/NZLwuOar2hXSE9NS94HyhXSHV8KSaQreslWtcc4lE1ENj6jKJChxAvlZ1ieNVB3KzDWUheyYU0u/HEYw0rFI1Lx5y8oljZD/dp7P3BrHD9RzR41cvc2J+NRxG6cx0NYKO0LFda2CxsmFNuJKVNqI1ClquSJxnbRtMMp+6f4jtHcpQOCadYlkXItPWqFHaczv1US8PpsPStlH5arFfvXhlNybVewoW5wjJhaOISpLSrnLuurU4rdcMTBUszUozNRDf03q6VSFWgwptW644isGnTmfScOWtkm11Vw94xO8xOEpO1errQ8ncTlDxomLDGd81O0PfUzMSR8lF3PnVy1DSOW+G6tNHzE3c+VfIdmpvcd6YCkEFJ3jVFDMlueQgqUZqN5NGUdhvzNLb43ZqqQtsyUIyTmPcOPSuSogAbzBaY+TvPF/5VyljQov5nDUF5od9PvFlbKaUUn1jTN+KP1Ga6meBsq6RaU9TGiCnD+BElKyU8KYsoDbe0fLnAQi4UqbXsqEoU0u9PnUyXtKnzj5mQcF2RNJBomtSUjmY0U3VcrvzGkMkcAuq5KLEjaVhCW2xJI1Jc7OO8j9ai2M2aehlFjzv8o+c7+YznHD1WYsAqZDQmryESTaTerGrNHzk3c+UFKgQReDVss6R8xf8AI157LXF+oDbQkkavLRmOY49YyXU5J8jrbYmubaP9jGS2JCvlDMd4sesZLyck+uqyWUFZ5RldqOWeAXRIXa3JWAQdxibCpfaq0RpWyOd4izUSTNSsE2xnSaHO0xMDKXxKv1WS4kKTgYn2deT9qrRFrJIxTbGdZ1qaNtaugjOSGx9xibyi4cLhGS2kJTgP7KamxPEWGMx1Y62xmuIPUSj/AIv5f+Rc3/L/AMi1TQ/JjPe/imM4FfeMSQkJHL+wNA//ABv/xAAqEAABAgIJBQEBAQEAAAAAAAABABEhMRAgQVFhcYGRoTCxwdHw4fFAUP/aAAgBAQABPyHqBtPibKEG72bz6T22YufhTIb4ILkUUbkJ1/JXPOBd2CpP9AkmVjyJqAYywUeAxJdsehjeUmCmsujuYIiWMfSETyBoXmgZyTvgi96DDxHhbtpHaf8AiIASSwCfXx34PlyexsLmamSTMzNVl43QBMgdkQRMHZMub1JzR4NWJ9hgmsJz2xgmwfAjUgxM3rQTuAF98gcoYneh1RjSwsBaWCawMVR0m3dR+LMZ+l+nXfXYnLM2I+F0Qf1rUczM4DWSaCIu9UOVEi+beFPebx8lwZop4n0p1er/AEp5Jy6H7T3emUdigJAEEwQxFIIACQsAA5JUKEPHNR7yeicnkpogmaDk5mkhBc2gMxsTccoeT6Q4i153lf3z7RnybNXFwF2MhLsv0E0AHIEYIgFrxEEQIOCKuxQ8Pt1AlAnJJYBOQSjb8GdRhd+GNLSmh36P6QKIJABh02SdskDIpyMdu9oVNJgiL3J1YzPzCrE8/pePFQ9Ik5gzO4e1KzoCu55KTyWpjvxwC8YIxDFEc6AWw3/G3SFtDciImWYWsfpTHdiZDMKhSBaGwPfXDYIbnTKEhslyl9DQ+6hVyBgPh7FBCQQBywHRlhmC9KMQ8sARYgEBkUds3LnXavfQANFclgTFcfmYse1OAseC4YqTwLqEWwkJnIEcIIZx2HtSqrvSo/nOUV5JwmfZTHF+jipKckYsiPHQ4jqmY4FzcSmaBGftx0ceA+Lu1E6fVy79B0imY3Mqc97lPRANLqEmRhN9pU6MRHNBmRDAA5JQWYATewop2z4cap8aoQDCAqXdt8h4CdOxkbtmp7UHvZgxwVolMsOhjEN/1QZJdPMR8IMhA9aRoULYzPamzSiTmwu6pI0EzEm0PdQALgjuZntRHJxPyYvVcSTDP8DlRu4F+npdx3ax536HN7dERZInJt7KrEOk2AKIyQxohJaYiAAAYCwdR7haVfoqH9QqJQhgAAMALESfDqkOU1lhqoCVHvCDwizgZLWNM50ecW8IobgcGu3cXy/ig3dxFPdPiKsb47Z2zpLeiCWQUr4I3lp6rpH2AW6mo5VlqRPYUM3cbfiq7RuUZtedQ/asTFwE9TdmQ/wLksCJBZaTONmSJOLryJKJvhYdsMEMJIhMXg0MHYL2Hmj81m3h6tvrvPnWZfGh0j3+d436rlW38/FSlUvd8D4oKDicqvOJgz6Cq9j7cPSMUSZ1zudKhAAgHBsRjgw5bF7Edk03gYO70Noh6yfHeq6eDHP+NvQFRygGJUmZ5seq/qBM4wHY1cyzeNAaoOVWb9amDcu5q/RuKf12xD3VkEpDhmV6AAJCCZwxxRRGnIjuBqT3oeinUjOND+ByjzkO/HWuCDOUFThIWH8NR9i0PNX5bMTV9I8qrV0Nge01TaO4KxAhJgFKQBi/DmrAxgdMz2pYKEkNA/mu2+RmC8LkaDk/iGqKVDFoiRmdE1qcYoWBh7UXmnAzsWJgep9fsf0tZH2qxiGdPUw4ZFGoxk3uW2qkxAAkjAOcgRxsMjNGM3ntWY4wfZl+U/EOzxXGKSXvN6KYvRgm9mwfBQUhtwbjjTkubwqzMlCHBTC1jaf6eo6lh5/gcrEbLU/lSQpx1QhmGAYBHIwAxBtRQkr1LL2R/wCAzUkeJ+RadoXbmHCiMYGJXLWvdQy0DUi2q5PQDCzYG5r3zoBIpQsB9pugnFDc/hKpkoQypbH3hz2npSQJIESihg2kk2aBgnQOWhQH2NRpsLXT2Hfrfe4aTfRcnoAQcglO3WcKDu8TR+6CYVz7nogAIIcFGzC33NJbUNwJWMfg/wBT+2e5A/X1G0CAvKj8N7getx1PxxfrosVLUJeKAArpyuKEKsScDbyjgYKoJjMVYxjNF6UEqIK1aThihK1I/cVAzMGxCoFgBMSwe3br8dS/cLePzo6l1Jx5wcou7r6cVXnDvWOVCLuYqeB0W3FjUI14x5A4d0NLYC5zsbuv/GlNMXmBtYiPPRdx8rPyKWLBsBPtfDiq8Z3qh6Ww24BMyJGfjipAJIABJMABaomIE+bIoLNILix7Ig767eYtUNIZR3e03DWHygDgsTokCZC50xUybIblE7Yy0Thgu7IGAHhfXukZaHpkkcdE3ITlcuDEEwUzjnhhUVwfdV+FfUDl9waWaxRIwkcIpOA6caL0KAr0MdLkbmEwAOcSDMHx+TlAiSOIuPyyayRD0OrfxVwMBkYIgQLU9INoJgFg6DXCD7Mfh4qPMQz5Mr0CAcFxWFGRHFf8jUfufdlBEu7FV8a+h7mMuwC3UmL0nThrolHQ4BtYcpnN1v4kUBGH6CVBKIJklgE/gC3i24pNWLn4RY4AxHgEAwAEhVZPm22O79Eggvx/49USDYm4Lxgh8nWlSdFAIkmxRRCYAkPltV9MRNpEB2QaXnq/YuUQHCzMlSBZAhrCDANTJGzIElL8u3V3+LDISpMh1AkQzCgb54uT6QfhnZEeS90AcGUcj0g6zt5ZDas5Buj0mxNsto09VcXgLIGx6WeE1sYBHyi3CmwQqipcDOKk4cdEMWHfq/buVh8ZwlFTFvfiypGEYxIgSuGGKhMUAGAqvAXjEIzIEJmLdKFnIP0Rbhriw9Q4nOx7R0lQFVX37lYlL4GHvUKYhLuzHwgpAmAFg/xQ2BaBcfaNjRgGIPSE3Iq6e0IMMwKPo3KsY/llQRygAHJNiEYxq2LHxUHs5mVEPadorSegGcXkIg9NGGP4B6TzBgNMa3hGCdUNV3QCQBtgT8uybAu5N5xp53aqg4PplSPcLEr7vI6VXGs9m06ntXLhm8OSxTj+481HcgbxiaJkswtuyDsYKG/BQSPCLogTYZg6J4hmxnpRAHguR6V1l4SiAx5KLcB5I9yA2HtFw8L4/SAAAAMBU53bqg4vl6QmQ4QviQVovLEEzNQS+rCe3yhUsMDCsI1hgPgMeyADCvETsiYhQBWDAehw/wAZ7vaqiywUvQgOc/od6r6Vr4OwNYwycDfo9IAwOiDLcxYjmY42yuNkTBjtmZI2Rij+SOwWjsCOuVp7ggq4NYjS1ShRBW5/aqtG4zyomtYsgnk0HONVm2H2g8GtfvbM+e1UaQwu4QLJpDuavcgQQ4IIwpjZkiNwIhjMEvFQvswBaVwCJkm8I5nitITBOhWE2OPhVeH8ZvND/a0SZ4FUTWfuePmq78j7An4Y5zpEiSxZYLybAhg2XHS3VDI6sBhS6YZp5i1XhSCJN4ptiOZn69MSsyHM7kRuJASG4V5hHn4mbVWG3Hz4q4jfJFD92HbHk1YMpCwvllU2a+BS5MTGKvXgkz+srE+aCZXhBLANFpG4jA0YiT3HbmgNoNw2BPpAfmWmguJmAiSjlME8g9EYwrEquDGMdiqv3cd80Fo1YHIYJ439mqw0whssZA9DGZfs3hp0GMPdW9p0fUsaHifxg1PakA4UgDsZ3oBjjJC8J0thHajUFcFuS/SAAADAVXbt2Yq3kk2AA90M2fZCPdUTTbtE7SRWci3DKY4IRLCTZFXlsLMxPfoCOByDEImZmfZLhlylEV32VAUDj5s8bDzpTY2RmjBELrFHiPtAhZrP3IONo/4iHbdk2leTWuU8j5quXM7nF5ofwScgbu1aMrEMg9g1mwd2teNlzfZQsyZ26IMBaeAuS7K9nhEJCgLXFnYduoSwcyWEUZWcVDzAPeUIYGADCg86dpEcoQwkD1XiLc5LnvWEU4WKkqxC9ST7JN0WSk3aAQw8zG4sj2elsLqDXlg68HurEQ78gPNSy0OWexpVeBJ7blh8q4Pn1rALGfucDgjboVNxiGNoj0JjwzKA8wcwxK1QcovSZgvHaNILWImhhwR1HvwdV+d6rY2O/L8d6jwCO9+j5rejBu0dkCCAREdZ+UQZ/wAHah1pNMzE8MnkkXeExEDMQNAT5uN9jt04z7OwBO7AJeTUOwm03DP1qgAAAwFlQSQCYg2hOpG2aJ8aVnUUwPzYXbdUUPAysWCDxJJIXJMm9PBWIk+95P1RARtCRe6CAIQFwRYUzCAE15gw6ICEAAmSoidz+QqynRbzFh51rFGIT+9qRMeORsqkAgghwbEJGo32DlSmKEcHpXCQT37zgiKs95o0Q3jOeEaT2pZX4xlz3pIflz4yQ0zaEd16uLIzkjAI70RkT9b1SzIvxPboFBMTMFuDHugQAJODbWICVNpZgmgPa3L9RpgLy7YoF5VArgcIITmgDkKdXTgvmZnhAAAAAAsFF/wE2DeQFoVsziaQmuQSnW07lhpECCJiKGADdpLDrbqmUML9bExAXgvRihgQQkjYPxgpAyIgh7q8HgD7wTDIw99E4cjxDbjiwtRAHFcADACMUIo8y7FIhrPdOTV+gRmiThG9qmOwEybyr/yNMvrKowtAMZsXkUAzTANUzPOYyhtB+rUYlzE3msDx25ieF5Cg8w3nHplyTBCGR5mopRIzyD8eqAHJgm7PfYs1QQOPnE13ogIYM2I/7YGzIbelfUWwMzYjIE0TnehCAAEAB1SKZYDgoUfnBjMcqRnd7g8ogDkCLx0Ch9wHcJuP0mEkTB2ZaLtOkVH6YHRsnn8hn3Rc6WfEUJJgJ3AyyocXjdF2zHKbsSRtgmr5YN+oHAkgsP8AF8p4RC4tsH2h+XfIojvakFy60K+Q2VocgDu6m7454ksByAw/x+DJyQ/4n//aAAwDAQACAAMAAAAQ88888xtMdzNOhy8yxwoo18888884Ozt/esf98hjen8v+4c8888s9m888885x3P8A7nPPPPHVvPP3nHPPPPPBPzP/AOi3XzzzBvzzTLzzzzzx/wDG0/8A/wD/AO0x/wD/APyz3Tzzzzz5795T+1f/AMv/AHvffD6fPPN/PMv6tO/6nP8A1n/pTzxyzzzwzr3z+jT3+zX/AFPd8888X8888tXaV/48sfkfu+888888cs8888/9/wD7/PIPNpPdOt/PPFLUvPOPbHv+vathKoNXivPPPDf705f/AOdn/wCs88cL4std8888/wA//wD/AP7V2/8APOFP4fPPPO/PPPPqc/8AqTz/APo8he52889t8d58887yx888X/58V/488+975y8W89t//wAfPN/7en/3/PI/RvP1CPupdv8ATzzX+yn/APQ88Ln888qD/q8uMd841/z89/8A/POAPPPPAvrPPPPPO3/1vMv/AHnzzzzzziev7bzzzzb+Xzxnz/jfzzzzwzf/AM/10xF//U888M9/vLW4Du0f8a9/ff8A8DPPPPPOldf/APvr/wD888+TbiN888888888/etMc888/8QAKBEBAAADBQkBAQEAAAAAAAAAAQARIRAxQVFhIHGBkaGxwdHh8PEw/9oACAEDAQE/ENmRyeZoe3hFQRaUPPiLiLvr3gHQOBCtE6YCq24PEXybj7nFUB0adSfaLzwzvOZSx3qYQF/J9/IvKbiws92EXxvrFxKZl23V2ehvgcLXbjceYl3j9cQQvF+h7i8kNKdodmp2NTaboVpIyq+9YYD1sPn6sSCZI8uneHYqtZKHTtAGRIgU1XFwIdm/OKoLnDGI1r9g+RLJnAF3gy+bN3EL3L7lAEpHV1YedQcDdm9N8N0Jz2UUibAk0DVl0qwzPPAz/m6JMNcNCxpWQPMDVfeYhhP9SmwjEkkEbxoxKW683WkHVg7xHNzhUtqc9DTPPdfsgAmvQzYngTxq/hkROCqoGvoiqWZ3ZbrQqoJvD6kPQmJJjDOu1MNhEdfEHxHi0pJk6H2/lCz/AD9D772wAMd3YHmx0DSnjj+0iXmmXL+2zoxTqr6sIR3u55iTD0QCYFiSqTExWLBOTJ8/5Y2DXu4v9QlVAOhGIs9MDanhctdxVgJWT2xV5sd18WKSGTEnPdgWEzZDzBML2rY7MF0CVcP7CM49rJbcaO74hZW+Q539J7AF4qjKnO2b+Du/LFJNk/LM7NzTEqbNeLCxLsD8gULyjYAmSI7zZ/LdWifOsSc29j7sMDzVE4absocFLLg7snpCIyYkZtDkfbHJNGySK5o2EIv7CFv0k+dbDmVKeOPrhE2MmKUqcoxq7bGnodom5Jep82BRmQzF6V3lHrACNL+8v5+4Mm1diyncva1ckkcyJO/xQ442K56qDX0QiUmu3J7EO0TGYh8PR2VK6vNYBMwfHmL3e7FnTPa26EL3A+6QeCQddWJsJq4xfmsMKyyMJafqwnUO3OxGTO6HxDYDGglypFyCknjDu4wcz9fbjldDFi5BCRwgAuKdyFzHYs6h2hFITYkGkp1fXeEo4GFXbzBbKat/Au7wrlNOMDCoGsvEF1YVvCHa6tDZBHmN+PM7WSbbnE3RWUPJ606xIwDqz6EXsVvW9+aWGFvWbseeUKjq7FnXO1l8BOHJQMvcS8SIBKrnhCU17doWmvOfeBhqddkG8kqRKLQXnk07bKdJBGPndoYEOh+ULHJdXZtMB1XGX2xp5V0NmSbJ9pJiQSb8L+V3aDqSOtPnWECfUIBZp9Lub4nE8shcFx91s/ZpYpL+JNlNfZYKXRGKhabc9TAsgIQ0YLuHMpGMXZv2JiyyKwTMG8+pxKgDRn0o8oSVG39mhZMGrtYOO3u+ymsUvNspd17ugAJFjshOJFeY7oQEmKNeXerJ/uN74Ne0U6u7vbS5lXPeY8bZD/lCyfdJc0iZedtDeeLRnuPMOGkEJqy7t9k4MmJbAdYF8VXuL6tf14RRgD8sCXkw8D20hiJjY0E2ZeMqWypzL48Wb/A8+IQT9ezbzh4tMViT5f2HMxdtgTeEMRLh3IB3BI4/yHn9XLhj642UqUOF5ElBuH9hEpNbZpXyObXzZMHhNex5jRRIGZMsmyX1W4BUXA7PxsqcYM4x5hXBi9T5tJh067i+2Q11xuPd9kuG+h8WIJJh2w4brb6unPYxCIABlI/boB7BPl8YNm9u34QnCSUdlGHxn2/lZOUwzy8O9teb1H3wgQTLMbWDlEo3p2ThTYOZUr/UCSUvb/kYozpj0hspiTIAlo4+GGa8sypz92TxFoRIKyXYDfnuusTpIQ2oFwZH6/YvxocvkCJMslwmRXU6oDcPOFXjr6gLOfupFKKECT7fsLNm2KrxdN2mZ+TIkcSxeYruIFkJFgiLQL+OUToyC4Lj7rtU1XJ6ikTmyYnPYzYyxigkmePyFnV2Jga/YXQLI+h8nSPf/wAhxCb1fUFykmRT71/yv4OMfGnqFPk9RRXeOz//xAAoEQEAAQMCBAcBAQEAAAAAAAABABARMSFBIFFhcYGRobHB0eHw8TD/2gAIAQIBAT8Q4dEXWOwoy/uy/VDcIHLeHtmaUOv4W8bHm/R9zHWeBAc3gQWhPbSZ5ryc8Yl40Y0IxYTdcxxADFEHM2u01zXHV3QJ87+vuI7l2MDTNjdhNj8o5wfCcsPTT8idy/NuRUdk8/334biYhBtiQDYcK21ZvV4VojMuhnr0oF7nH4I/PABQuMRjtqS4OcPepoo95LHHgEdcRrW0N8V0K9cQiuQhHAAvT5j+ArpXBOTRx6eYKapzl3ki/n/legpTPN7P2xViBLrrkIRsfsQ5pt5f7S8xqoQDivlVoJ7R80Fg8yPSdKOVI4NChHcz/dIzBqtPiGTt701ybb04BGw61tGgzSzzxb+86d+iOm42Ojh1KIrF2eiXT9reYdTwXubFWxR6CmRS9DJqURXj3MvLTWG8s5vNS+vnNmvfz4HdMeo4bvGuNpg7UwVLhcXkwwMveiW9oFtDjNkSw9eFXctW857FMVQbuaAFbGIJsvzttD7g+/lQG4HeDsDwXkiVcg2lSRRFdg0zB2pii2zDNNTLm6QjVTXQHlFK2V6X+Y50gegoA5hq8N0DtR28NNkNjWKXaa7OLtTDVbQm6jz+pehdjhpmxvArB5X94FYPK3tGh0beHCgLMR9OF5BAJj7Uw96ol+p5/lAtmj1fzhEeFAswHV2mxTqJkNILpTH2p66movwFEHMzCUVRy+g/YndH28oKakfynJ1n+pnb64AF1g+satjgx9qG/cou04OxTUGy/wAVupzg7xVXaE3V8Gacx3gquZmvYNH7oA6xS6qiLauPtQXlZvlXW7HzV7dz4hgrrDw39naloNyXG59Jbtmj6gso9xlhl2b6RndRgXvV3pN7+UK2enuV8ofNVRbNvP8AyCzs9+BTgYQDn2GWxzTUnamDRK9rwLYreGmaOqgkSzZpZBxo/vGu41CH1E+eEBDegPUcV5qsBS7pjUfPrQVXIBb9+9cUa2twbDUYHO77+8vjkljgBc4dGYKZXBUbRhqfXjEVZpsy3Ocu2+zhaNYjWdAx9xnDXDt+wUURWZpuSYZoLdSwoO4HacGCdRz/AGIjZpelZmkl6IvIeX3A4Xp9xOlvvNTKGj+/5AsWKHzkRWaBYYq5pi8QWxxai05vvnNetZzP68S2jwckOeCb7uW37AtocBFhNq2jY2vGF1mJP+WMHwn6F+4F+37mFzw4f//EACoQAQABAgQGAwEBAQEBAQAAAAERACExQVFhECBxgZGhMLHB8NHh8UBQ/9oACAEBAAE/EPkkTmISfQXfFTI1ggve6jIcYMR7Hqp7bX0Evdf1Ytmmp6g/doBkk1jX9gDo168n+2ouD0+0l7qNGsUU7inqkxW4qD6fVSRn8nZaEQRs86sQxOXdptNzT6XtU0EZAHw33T1j0fyq107VELNnPe1UaO5iDyosYq6IHq4Hj/4hLAlVgDWm8OVobdWexp2/dW3fG8nSlVS3kZXq4vI2JbFXYv6QrEB0TWADqirsXdIVlOXFAQBNG9RYdmEzzelK4HVe6tLslPDGaT3Y9p4w0AyLthfvhvTa+wQOf1+qTc4vZXDtFLON+ALgTWw+Kwx4OJy5OPQ4nal4HxYg8L+IpSEJQSObkG6d4+e6QixJ3YR96FXoJiR6mL38Dkhb1lTqMDu0Qm8ZJdoeyijM0j4RPunBIan5bRQGGX0BQAQWNqQSG5QqAOX2BUuSZCvcorICB+ftRwJved0T4mn/ANwsjcbnElIFiGABeelIDEEwMwVddZYIg1pPQrJ6nl/wHGi4kSovVLvG2mWY6rt0XdqKuzE5HbV7CjR/5QED1TBgbgtC79qP0oogVLIr5k+6YZ7KkN4h7DUn6tKm+lX1Iko5EouO5Qs0D4L5ZI7bOcN3434YHAMVXApjIG4/I3XcoxpVVVVVVZVcVc3goCqAYrRm+Cc1H0W3og5byILsLeTQKXgYDYLfHHtiIexbn1RJPlWAO0W7Ydmp7GYr2sZ2yzpNYARZPIzcJMcC0rPSzxsmyFtjBlLfkfnpGAYvkGD2F5QE6RAB/wB353r/AC3jcA+9EqBgbBnIPn7DZoCAIkIkiU2xQzLlJmav8fFLLgMAf7tnVwMTWgw/DAbuHBSiBIg65XS65FB2BJd2/wDcnpQBh8shTgRJQFO1R3M65Kw1VgM2xUKmWSyNrkvN7FseIQgzIWFNzZ1XKpQDAB7AS9j4U81VZk56rBMzcKIrjCm7ofLzZqZPYYTccnOabGCTDDMGy/UGXwO26zAGK1jg8llaOrIybzxlWi6LF/SWOWtEVywg6ua7t35DxT8aC7V0HweTcaDv2U+zTTHoPukHNa/btD3kZJxkDdhnhpTRTrCYFZ6cTZLmRxUCoASrkUxkmWIS1cgubqkyLp7aYdXIyN5pkuTVcLd7A6kOvwkEhlu0T3Zwq5hb92VexUw5lAlwpcwqli+hw1b6cFAVQC6tR0RwZ2FppmxdKAAAC0HxrFIe8uu8w/8AIznCs5NnHfTYtwFJydFYAzWrS5vhWU5b+BlrTAZakIeJZBvLKgAABAGAaciwkg6T9Q7016GORLdEB0XCFU0uGpbJI9aTNTEcVgrcZHp8BQV5vZK4ZXSBTJYe6YQkPRJ5lx+6bv6eiWpWUFuAuQSJsjANGTPHCJPkR+jGs0cgwcUyGbQAAABYDjNONhYlvyadTWBSB+Q4knWXucsRJBNrS+mjXQS9YCH7OL2BEVwOB1I7Pgdza60uLQhuCj1S4iW8pCGhdXaw9XwS05RjVrTbIMgOCOmQTDOD1xdutEkEAEAaHyISZurb6HtK892eN1eKGV58CdUoZ6gIAWApyo78/oKG8gA6wRygPcGyj9/NPQrE5ql98ZEbYMhs7yO9EsMNmJI+OfUwXtL6fD/xcBpLYTdicr3i5w2w/qOCGYWUlzdi67DV4qUxCt13X8MvkaeaU6a1/Iekchj8fRm/o8nBTXFjUl+PKgKwue1JwGY7j7rEWMrcjraSO4KFp4ZgDFayaY2LX6K+rkKmPdWu6rQuFXkN1m9KUOJbskM3bdRyZtRRKMA9f9vHgp4sxG6fVQw5J4EULnaHtfspVZRN1Yrq78AwluTw+uz5SxTRcy3PYl2oABYBBySCYm1Bj64drvgf7crSPFA6pH7SwaQun+Jyqw2A/wAM1VhJBbGE2k7DkMoKFEiaNGhlIyjimYXHPcNE0m8daXeXvweREpqY+ETlvIMkmCIviDu4b9KUQD2lRtxB1GPUsvf5cMwg1fo8nJMXcC9ZSojqJe3g+gPO/wAuXI42+lX3y7oflyz2aVn3CNhH2vPKLUhhpwH9rROwIO1EJG6zwOma5AtRmKwoUZ7ivaDkT2HnYS/VMtPVglO0x24Be5hh/wBMqHyNLLSzph9xe/J7j6rYw/XgoOe7Cz9ctQWJncK/8yj/AE5XVLENA2LhOkP6fHMIQUq5VfTEjdMVMrU6EGvLg6gu38gd+OIo02me/XnuCv4SGZIhfVre7Q7UE9p5fU9AJ/KVBXUdVL7eT+FpW0A+nCE/D8LvMVKpCSNHM88kW6F0T7lMY4r7B/XkxpkewSfRSw2rBlZXaUnejjfkgmnAK0C60/09yJ1MPTklvzPMDC0/wHzx18Q8h6HOwKGWqy+9BfNlezYGwxKJm4GUW/j4NyrAYXLNBiDR4scwweo/1URYwLcnvqGxB6OK+3Cyp2j8ckQpCjrS+AoBiLtRjPT5Cn7JShexL2rZkAQQeimFkHkDZEzKxKKHDtvW0B3qDNdWHkohG0M+z6pIc9M8YDyp5YzEG0lgbEG3PeCQdA/tPAxKMhn7E/fgB2aJZ8AZGbTgOkxksaPYZOXRau/hcE1EyRkTU4Qlj5j/AC5RKKEPQHri0tIsS8L6jua8VUg1NAqa1k3uH8BQX0D7p3heQt18qYlcdSPm3nm99WnrgYnWicZegPz4OkGMzmbjCblHTG5CRPRx78EeN020dvAjqGvCHv8AAPw5QlDVPuixxEsCESRNKVgyF+rm1b2Sz4JIjocMTuJLYKFABhN/8Ng32UAABAWDirAh8xg7a7TS2AwTEunVXtHzSuMtx3g4mRir2T9fCIPFkdT+vBn4BcQZHshQchEjojsEqLUL45Qyev2FHIqKeUTZz3EiaOsUGSqZwsRpEdQkXSnNALC8xYzSAOhlV6oXMgYPTNzVeOBLYqYGLWFcUZIsf6+dSuqejjNTcc2j+38MJDVfT94qjlCNAKCsbeVMnr9SjlLA4hQZFBxIKwatGAsMErCHUYGRuvHrSBkBV1kXqfTFATSAAsqME0+eZiLJ1T84mEKNL/UHn8JPBEIyUviJK4C3BQ+Hy9wf3Cjkxc3YiyDFXIKERl0l1lFjoMDKW/FmxgCqcAC67UgbtF8BydfrrOFIhO74IxdZi5N0ge4AlgBgJYdRo0GrTHcSPHZTEj5L0rAezRZCzEeqEkBqtDsoZTvEzQKZVUl7PoaU1sFMMTIIzFmAzcNMgAKVtqt3nWBjqQD7LxHSQTWUp3JO9CwBBpo6Jgmpzl9cTIEtG/AhubdhB24hmyBPh/2he05ZMn/EaOOA85tocj7cisesFuDl9y3c3g2FbBitLsfEGL27bblJEhgCGocD0vqtLjqKQ5BtNHxLJJ1USK3WKI96lLBOBoAJBESGEmm5Dc0cKUSy6wPyl8W6lr1yX0ouSKlxiQ6qg6zlRd/mwBgHwLeqqa8nlOQShezcb7vDHWjIkJEZE5lRx0LIudrDug15EVLAPVb8qblghI/3GiiYwmeeg3fqiFSWUUepbtl3KkrwlvZc2JVo0wA92iUmjsZPyHlq9MAUg7Rh4mgAAICiUvIwN1wqPM7p7A9pppE+EB97PTUjugwjZLS90RiAANA5TI3r/wAM34bIWEYrOQ6odyhEEZG48hhFZ3D/ABzGWlYOspkOjmOzfiD2ZKAaq4UyNpwAzNb4GU4nlzlZ5GhY86xfce9SPqPQ/eW86Qq2flhWkpMUtKGo/wArZwhfSo4KBVgKKQlTv7JdeFAIEyLOi2dcd6WceDwdsiNFsO13ahGrpc9dCr3q/QChLvqL6GnWg5h/vVT8jcRQMUrfYzCo/m51Svb8KSUqGhQLCfaY6PK1BAtOzkmzNGxZn7Rh6pwh1P7hWIoSNB6ReQ8sUZhuQf72ryKgAie+NSLq/p+8pktaA5QsBPpAYq4AXafZNglB6s+ttZyLAFgsBlxlcwTgsVyGCMcDNoEiQgGxyigv4wVkmImSUhEohAQLFp+JiqcwwjB3FnrOVJxPi5Yn9jj8jD8cmJY+izddOEj6L6cpktTqOQi45XFXIDNWwZtCiVMDINXVmLlgZrxnJDroWbjajFy3JRzMHwAQAZH/AMULI2TYGDZJlkwbYdB8qhJ8VoEAltvVZHfAoOYciD768BL6Lyw3A6jiu1SEALqulBmXZ7aItRgZbl5AvmownFXYBXYqVwGWLu+6X/Mvg3G0R90hDL/OdFVrAS9NT8MtUEpE0GGw9kp7gthYNmPfbdoRBERwTnl3PKBoHFvg1cKE1fq3zSZrX8+AnvyXIaAFtV/p2IZvKMab5TAtvoR03c4eGXEGjI6buhSqMs9HfH8nSrtuxObuy0AEAB0r0ZrTYwF5X3vSlwMxAPufZRYAMhesLnXDf4EEhwrFuk0zqhd3GlHJBb9hRdtD/h1H1Ual7P6UMALoj2KELzz+O0xZaIAXYWe5oyQIAIA+EjvR9OjgFgghxEdxx2miS84rF0+T1ByOpCFqUeBPZWwQ5AQc2q/pP98v2wgLBKt5VcVW6urfnJWDMk9n8wcxpZTCCWe2jdzYmYfCxnFEZRUHPBvHy3VL8S/lGBweYITkj+vLgdKw6SDx5PMXAZdJjGBs9ooRPBmsqt1XNW66vwjAwuIkKGRHJG41EGRItLgG+CZJonMsVZekxzaYLzO1ZKGofMx6NSTZCXoorsEE9qFOqz1SWx2I7NEEGWyP+JmNz4z1mJ9G9GBS5Qp6BL9UeGLaynaY7cs3FjHUA+/JzJMT2Q7+8+w5McKmM6s8AWhJXq/5U/Sz+QFE3DBUj34k8rI5n3mG4UfcUcEJHxyCQmhk+YH+Yp8FsW5s49FutAAAAGAcq7wjFksGl4HUdqMOWNcpVsOFSPX7a+1O9LKryYE4UxnE11byxYuX3H8ojEILqrr5Xidl4JB2BuPQltWY04Tss3X4FYQLjl2OCCQ3KKITAgLxnrfRKYnhZdBm/Y3PC0WqcmCeuh2B6BxQNRmNqPtci9Si6ewV01cXF50UBhSB0pnpQdwn3PLJ/wDBLykk4eWP8eEatkTezyjkGSfVCVgFyjMxQ+1JCxlwj0auwrNcgBV0GgjWgEanYMDId+YnUZyb4NqeyRs1DmBkL4Rfa5lwS9iUNY+9nAzRqVgJV7VcA9WpincexlwMyaYKwApR82RGEUYpKra1tXkmLuBehnQIjlJP7y3sv/Q9TlkcwZ7t+cGQcJTu/wDDl3CR6oDc168qipHt1x7ojsD8ieBy5JmyCEOsO7r8B27W0u7b9HR14RfbwIZ9LwaA8Hcr6Dxu4KArgVkhnMb7M3bJkarJYIQnho+lfZaWxvk6I7ckkpgsJ7k64A3XKhDAgDANOUwHGTqh9PLIBg7r9oOBojFm6uUoDvUkMo9n6wOVJL0p5L+3y0BwUFdr1JX9Huq+A5ySMEbJTKrKnMULvJ3rF6J4JppTJK/pLwvfgTgQKSO3OkPY1o4SSq1SNUvsbOZTZn4HuWPFD3VBnmk1w3M+0wDregQAyGdYLrzRij3HC/rySF3Axpg8AHcXwRCXBtEcxKQv5wP5Y8zWOyDITJ1l7qf0dVEFgGeHwmMg7sz/AFX8PVTEMA3cae4ooSygTwjIHDOB+r2+QkQASrkU9zjqo/gcmH7/ALAviWikDAZBY4EBKn+jgKsDABpJPKd1gukhfiFDlkQ2ecOZomI61dncQQSAOuZkiaURjMl6wn3PwnewJ3j3NKuAHqH6pVmKvJD9pxetGJQGX90X+T/XIydiXagAAwLHIt7InFrvrn25IaOiUyI6PKAdWDBisk3G/arbS0uq7NhsOYeOWsmTjqPM6JcKQKgOyk2BBETTLD4F+vAzgWDdYO9SppQdWfYtaXR6A/c8qQlKfzOlOLwJzO1cj8gBKdtOXY7cumIIBcjA+3dyYpzb1C72Ho0eW/cADoP1I7nQpmRCRGRPmObGMzx9jH/rgoa1eNPwjzoMfG7h+0BDS0OSWffCFeha2AseQ6j44BrKS6WHq+pcq3piOJf7Tkl4TifwNhQQwIAQBpyJdHhyEI9qfpIuMb3WVuteY4sYesnBvZliyov8g+bk9w57L22M4Ak9MyplW60BhQTBi7G7hQNEChm8+GDYKaj2Saf+RR24BrE9CDIjqINEweHbYGr0yafCWBlRABirUI+RMuQv62Xz5GAVYDFag7OQvxNmW+zmvrnKxBxXSBGiDrQiAXRCFDCJkjInKTYEKJE0qJW2CTaJieuDcwDPkkdAnxIxg3MruPAvqlZsDCP4GAFjLgoSDzFsX7Dfq4tIC5DWl9pHEi9pG4maM1mVHfDJIYpk2xPdHMqcAgBiq4FXAVF7YBie3RjyGEIUS2NuGDsgzosc71VYZYmFmDEyGpcigpAyJrzMyaTF63Z62d6GV1PB6pJ2aBmX/wAmtABUjglSa1JrU1vOz7VpgZO+k09hoEK2XQNPSENqMoUAQHQ4CqQSSZN9jQzbaw8ohSpRupmrKu/GHbzzhzNzE3KM3CWwJg7JfyZcUoEQCMImCOTWFJRG3p7N6PLLzLy3dmgSXgIeSppONXQPtp6hYBhd1bwajoouislzW72DlCmwFyf0LLubVZi4MVc05q3X4ZdptonnlGrBkhxlDJKMiImIjcTRvz7ASAnupwlr9+K9o36FJoUbE+ijYL0QeLK2MCE+caWW/GN2wzpWblsYuWohRcC4bF0DAFg7vKFTjDGIpo4jk7LT6zFQLETXkyjKkZZ1T60rImhgoD3IPl5gBCfeQcW+BvhVzWUYuYmKs1+MjLl5keBmdEDWLUEY6J/p70Y2fLFgjAuboau1MAXeyA2WHe7YxpuvTBdWaN1dW/OhiGUgGAZmjieqltu8euF9tQ+LTdAeUt3NFOlyTuMemxs0NtQCADIMvlIz0AA3Ggqee/0f8BtS7B8n/O7Ct39gnk5wXArr9V8co7xUUG7W6Fh3XpRtSRJF9BsD4sPcZh36704MXJOgGzvSML792sdynJDipnZihHEPS9Q6UnibuKNL3RvIRSKT5F9t8pSoPv8A4zL37KwpqiXY/wDhil0ixN9S027OiTyHtT9i3b1Rhh0T+6JYd41FrDqP4UejAz9tX0p8Qmk+EelFAHA5dj5nEr62vYePvVVgP/xP/9k='
WHERE AccountId = '979f00a3-bf13-4986-96f5-b74ac81ebb8d'
GO
-- ESSAR Medicals Laser Print Changes
-- ----------------------------------
GO
update SaleSettings set PrintCustomFields='
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"10%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"5%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "10%", "10%", "8%", "5%", "8%", "5%", "15%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "HSN Code",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "MRP",
				"alignment": "center"
			}, {
				"text": "GST%",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],

			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@hsnCode",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@gstPerc",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

	
"saleFooterDetails":{
	"table": {
		"style": "tableStyle",
		"widths": ["15%", "15%", "15%", "15%","*"],
		"body": [
			[{"text": "Tot Items : @totalSaleItem"}, 
			{"text": "SUB TOTAL: @totalSaleAmt"},
				{"text": "@totalCgstAmt",
				"alignment": "left"},
				{ "text": "Dis Rs: @totalDiscountAmt"
			}, 
				{}],

			[{"text": "Tot Qty : @totalQty"},
			 {},
			 {"text": "@totalSgstAmt @totalIgstAmt"},
			 { "text": "Aft Disc Tot  : @amtAfterDisc"}, 
			 {"text": "Net Amount: @netAmtPayable",
			"alignment": "right",
				"style": "header"}]

			
		]
	},
	"layout": "leftRightBottomLayout"
},
"saleFooterDetailsWithReturn":{
	"table": {
		"style": "tableStyle",
		"widths": ["15%", "15%", "15%", "15%","*"],
		"body": [
			[{"text": "Tot Items : @totalSaleItem"}, {"text": "SUB TOTAL: @totalSaleAmt"}, {"text": "@totalCgstAmt"}, {"text": "Discount: @totalDiscountAmt"}, {}],
				[{"text": "Tot Qty : @totalQty"}, {"text": "Aft Disc Tot  : @amtAfterDisc"}, {"text": "@totalSgstAmt @totalIgstAmt"}, {"text": "Return Amount: @netReturnAmt"}, {"text": "Net Amount : @netAmtPayable","style": {
						"bold": true,
						"fontSize": 8
					}}]
		]
	},
	"layout": "leftRightBottomLayout"
},

"termsConditions":{
	"table": {
		"style": "tableStyle",
		"widths": ["75%", "25%"],
		"body": [
			[{
				"text": "Please get your medicines checked by your doctor before use.",
				"style": {
						"bold": true,
						"fontSize": 8
					}
			},{
			"text": "Signature of Qualified Person",
				"style": {
						"bold": true,
						"fontSize": 8
					}
					}], 
			
				[
				{"text":"Items once sold can not be taken back",
				"style": {
						"bold": true,
						"fontSize": 8
					}},
				{}],		
				[{},{}],
				[{},{}]
		]
	},
	"layout": "leftRightBottomLayout"

},
"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [	{
	"table": {
		"style": "tableStyle",
		"widths": ["100%"],
		"body": [
			[{
					"width": 580,
                    "image": "@printLogo"
			}]
		]
	},
	"layout": "leftTopRightBottomLayout"
},

	{
	"table": {
		"style": "tableStyle",
		"widths": ["75%", "25%"],
		"body": [
			[{
			
				"text": "Patient Name   : @customerName"
			}, 
			{
				"text": "Inv No    : @billNo"
			}],
			[{
					"text": "Doct.Name       : @doctorName"
			}, 
			{
				"text": "Date       : @billDate"
			}]
		]
	},
	"layout": "leftRightLayout"
}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": 12
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}',
PrintLogo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAf8AAABVCAYAAABHAe2CAAAAA3NCSVQICAjb4U/gAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAPB1SURBVHhe7H0FgFXV/vW6PT3DwNDdpagYYKHY3WJ3d7fos7s7MbEQEwMLRQQRDEC6GwamZ+7c2t9a+86Gwzigvvf+771PZ8Gee87u+O1f7LPPPj5D4K+IJF0gfZkyKRifgZ/XPv41KR8SvPHZsCR8Ph+CSf4qsj/IAHvViEb8fwNNY9Hx/2/4V+r9z6T9V/tpU+k3Fvavlvmv4o+WXz+e9/6fbcMfSfd7cf7ZshuxaUge/uXhM374Un4k5ZCiTwKhVAxBRBFMJRGgApDy+xlCbcGkiSyVosJAomtEI/4X8Hu0+N+m1X+2/D+SbmNxnH9Dv9409a+9938Gv8cTvHl74yndn4E3nz+DjaX7vbxcOm88V2fn11Aef6RdDaUTvP7uemP5KXxj+fxZePP5s+PyV8Nf1/Kvg2uefpN0vkCAVn6C10n4/ZzMtPe1GpDyBe0vO8SqRKlkymqbfioFDi6vRi20Ef8u/N70E60pjpyXFoXfS+vFH6XZ+nnWT7exMuX/r8yLTaXdVDtd/+hXzFx95Ji6y7N+3dy9168+vGUqnu7r51MfCvPWwZWRTIrXpMfO5bUpuPBNldUQ/tV0Xrg8vGHefHWtdul3U+UpvYvjzcv5e6/VZwHyZ288B2/8Pwtv2S4f/aq8YDBo/f+O+MsK/7qhhkmlJy1/aNn7UFFZjozcXGSRyPypBBIMCJCmUnQJ40fAb1UAUYnNwe/jpK27TaU2nMQWilZ32YhG/FmIAbkpqF/Rl7sXjckpjiDG6IXzF3TtmJrLYx2NEo5ufw/ePF1eXsjPWz/vtdBQmODu3a+ro/PbVP1cPOe87ZNzcVR3F+bg4rj+EVza3yvTpXHpHXTv8hA2du2FN41+5Rycnxfe/hEaylNh3rQujvfe2wYX1xvu8pBzbdS14O0z99tQWbp2/oK798YR7SYSiQ3Gx3td38/dqw71x6l+OpUj55Ss+mm89XPx3a/g0v9pKMs/kuyPxiMajJqu+h/O44/iryv82SpOXyv8hTgJIhwK4edpv2LEuKnYeaedsU3PlmhiQxkzHkVlPMb+DSErI5NKAH2tZkuGS8JIGk4MOp+UAY0Cf3yGRFM3WnpaQJKyuTWMf/PINeIvDS8D0xTVvVf4y88xMC/jEgN08ZTGxfEyzIYYneI6fxfPy0C9cHl64S23Puoz49+796KhshryUx6C6iFrzhuuMDmvlbep+goKVx7eeslPaChd/fj16yjB5y1f4arTxurQUPn18/CioT7cVH29UF2cU5n6dWmUr/y8bXFxveU1FE9+Qv161YfieevvzUdtkP/vlS/Ir3487/3G8IfjWcdydSWGb5k+r3XJMMmDdDz5sT9sgOIyhd/GSPvZOC6NnP5wrlK21KVKuzrZlb7jtc1DztaAcLn9cwjcSNRd/6VghTIJytCSJ5XQ+Wjh+5Cfm4X3v5+DB974HmOXxoBoFM1aFiIvHEaQUStrE4hFa9nHZA4iOn8QKQ6OulsEmkiQoKUZMC+NhS2HMLyn7qyrBpz8f0uoDn+E8Brx10QsFsOrr76KDz74AJ9++ikWLFiALbfc0tLEN998gwkTJqB79+7rmH595iYG+NNPP+Hzzz9Hx44dkZmZiYkTJ+KTTz7BVltttY6pKp1jxvXpTf7OT3nqXsx/6tSpePHFF/HZZ59ZN2fOHHTr1g0ZGRkYNWoU7rnnHnz99dfWr7CwEGvWrMGwYcNs2arPzJkzbd0V/5lnnsF3332Hzp07Iysry+Y3cuRItGzZ0qatD8eQ5crLy/HYY4/hzTffRK9evVBQUGDjPPvss3j//ffRs2dP5ObmrmuH2vvaa6/h4Ycftnl36NBhnaB47rnnbF+q3I0xffnLOQF43333YcWKFejdu7fN5/vvv8fw4cNtG8aOHWv7vG3btmQlUXz88cdYtWqVLXPy5Mnr+idM/jJjxgzceuuttp8233xzm9fLL7+M6upqm/4f//iHHe8ddthh3ZjNnz8f7733Hlq1aoW8vDzbxmuvvRa77bYbnnzySdtOtUVOUP+rr3feeWfb78qnsrISb731lm2P6qE++Pbbb+34/Pjjj+jbt6/tE9cfird48WI8+OCD2GWXXayfaErjLag98hs/fjwkPlq3bo02bdpYP9HMvffea8vt0qWL9RMd33777fZa9BCPx229X3/9dfTo0QP5+fkbjIXSTpo0ydZRdZP/V199hTvvvNOOs+hcGD16NJ566ilLD658tfOhhx7C1ltvbeNqXr377ruYPn26HSvVVeW98soreP7557HZZpvZfnVjLrh61Eeam7Oe6/g64+pXgpsCXsqADWFy46PiZaOQhlP0t4KCrk5BSK9ES66kvZWHBL9WoAX506sO8mQkvzxUlvKzAco9ffHPgA3+ayJJl0iaZCrFy5RJ8F+cv8Iq/ux0wQMGu95i8k8bYY54+Bvz9HcLzIJokrGMqaiuNqvWlJnikkpTnWQe9Esl4yZRGzc1NVFTFYuZGPNKMczEmTvLsfcp/k0l6lycqZSbnK43BDXada4Rf19UVFQYCnuzzTbbmKOPPtq0a9fODBkyxIbdfPPNZu+99zZlZWX2nozf/jo42iGTtpyFDNXe33LLLaZJkyb2uj7IeH+TTyIhGl0Pd0+GbfM98sgjzb777msuvfRS0n+Nefzxx03z5s3NgQceaChkDAWTbcfcuXNNdna22W677cxxxx1nKNDMsccea/Miw7Z5UeAbCj+z0047rbt3UL1c3VzbVN5JJ51kKBDNtttua8i8rf/TTz9tKPBsWaeddpqhgmD9hQce4Nxm3iq7U6dOZsyYMdafioyhYFvXTyrDW6aga/m7PqDQs3kNGDDA3gtXXHGFocA3hx12mNlxxx1tX1BIm+LiYnPUUUeZjz76yKxcudJsv/32hsLTUPjadBS8ZtCgQaZfv362/gKFuKFwMtdcc42hYDT9+/e3fSeoT08++WTTtWtX8+uvv1o/KmCGgs48+uijtt933XVXW7dFixYZKjuGypXtKwo/G19tuemmmwwFnKGiZBYuXGjrpLFTPhSENp4g2hBKSkrM4MGDbbsFjSuVGTNw4EBblw8//NBQATJUxCytUviaefPmmeXLl9vxoEC3Y0VFwDr1j8aQQt1Q4TBU4gyFro1zyCGHGCpWthw3DlSa7Dgfc8wx9v6LL74wVDhsXCpztq/VH6qLylN7VUfRkuqk9il/4YYbbrD1llN7qAAYKiM2n7322suOkYPG3EsL9aEQSy+UCJIm8VSU8iVGX6ajHIiR59fyPmp/EyYmOoozNqPYVKK3GK/iTE2XUNpkDculPEmJ5ihjJEMStcyX6VlgLacBk1GUpek0RUmTSLI8lZmWTKraP4W/pPBPD5IbKDs09p+6inLaYkFZpWl5zF0GR75scNQw0/KsV8wJT4w1I35cZMrr0q2uqDRLV681FVUS7QRHIcZBrqQCUE0FwCoWHIgkB4ZDQmWA8agkWEWBA+lVBGxdPM7Be92Ivx8ktNq3b29osRhagIbWoWVStKwNLRjLoJzwqA8nICUIlEYCn9anueOOO6wQFiQUJGD22GMPM2vWLOvnhaM/WrHmuuuuW6doCBJItB7r7tL4+eefLSMeOnSoqaqqMjHOA91LmNDitW2h1Wbj3n///SYSidh2iZk3bdrUXHTRRbaNEp4SPG+//baNKzhhLDjhW1paaoXXd999Z/vKCaStttrKnHXWWVagSQh8+eWX1l+46667zBNPPGHrJ2FwxhlnGFq7VjAqvYSP4OaiK9c5rxA44ogjrHBr1qyZ+eGHH6yfypUQl4AXlOdtt91mli1bZpUkKRsHHHCAFbh77rnnuvGjpW7bs88++1hFT5ByJ+Gmfv7ll1/suLuxO/XUU62/hKT6TKC1avbbbz8zYsQI23cSZhLM77zzjhVkV111la2/6rR06VLzzDPP2OvevXvbOAKtevsrfymOghP8oh8pNOpThQtSalSe6EdKnJSrCy+8cF0b1BfHH3+8VUw0/lIEDj74YKskii5EK7W1tTZcfSQhfvHFF1va7NKli22/g4S4lATVVwqDIIXh888/t9eqk5Srq6++2iooUg5VxksvvWTnisbaxRs3bpylPSlRZ599tjnooINsmBRW1UFtVbzRo0dbf/Wbo7uGQZogrUieSOTXks9TCqQlCwV2KlZpkvFKy///OSgf5hyrZjG1pooKgWRRlL+SQMkEnZSEmJMpKqdOoP0T2HAt+i8CjWhSqyG+9EK8FmPYd3Ypxe8zYLehQ1425rxyOVC1BMjLx4qKJF787BdcPuwHnPjQV/hg5mo0y8lGUWEeaiorsGptCZLGh0DQj2AwjEQyiThdimWoF9NlsGzmr4UhPQ8gsdQ5LaWml1edkx+ZDmvUiL8z3BJjKBSyy8daBu3Tp49dvqQwt3Qi5+Doxwsti++///42/t13322XkLXkK2iZnMwctHzsYwAt1Qr189XSMJmxXZJ10HKolrKLiorscrGWv7UcrDqrPC3fq94UznZpWnFo/YFM2D66IIPHkUceadulOp555pl2qZZCyS7V02K15Qpqk5Z7XX9o6Vn1Ux2ohNi4F1xwgQ0TKCRsX1HY2HhaSndQuSqLwtoubVMwgRa0XXan1W0ftQhK5y3XOS/0qGHKlCmgULd9K6g9Kl9L1ypL2GabbWzbtXyuJXcKS1DRWdcOgYIJl19+OWi12iV7PT5o164dcnJybD9rCVrtodKCJUuW2OV7Pe6gAmWf9wt6lKG2HHrooaAAs3mo/upP9bEeTahMQfWmAmGXz2l9gwLY+os+tKRPhWZdn7rHSipLY6THEw5U2kBBa9sqGjvllFPs2FO42nC1W4+sNJbqm06dOlmaURsWLlxo89Jyux5n6PEFlRtQobCPO0pKSixNCeonLd9TqQKtfhsu6FGBylc9qJSACpMtX/krn9WrV9s+VT5K70BlytZHjxBECxoPQY9StttuO9tW1UWPNBzqj78X5Nj8F0cglUAwaRBIUBak0s/p4wHSTigb/mB2+lEx409bXIpnP5yE4697FNscdTm67nU2Ou56CjoMOgmd9zodWxx+EQ679EE88OZYjJ+zHBVJCpJAiMwgk2MaRpYvhmxfhX0l3Z+KUXZRXjBjbUJPsVwreOo9Tv4z+OdT/g9DjRL5awA0lH794wQR7NiaOIcwgWxe7r1tNyoACeTkN0GYjGbeylJ89utK3PTCeFzw/FjMq00ht3khQuzwtcVrEU1oL4D2DwRQG0+fGqDnN34Sg/YCaNNGWhngH1um7u0db9PPI11dBMcYGvH3hGihIRqQIBUULgErKJ6jHymOLp0Eh5gqrR/7XJYWIVq0aAFaPJaZ6bksLTUrWCS0BPkrvZwEIK0ivPDCC5bBOqWUVpX9ffTRR60QEtNVmJxXSXCCSXnp+auEkgSNnhfTArdhEgy6F0On5WiFuQSEU2TUJgllJ4D13NYxYgmzyy67zD7P1r4DwbXf1V/pBd1LkImh0yLFI488YpUfQf2ourp8FVd5a8+E+lvpJLTlpzh6niwoXP0iAaL6S2BI8Ki/pEypb3bffXfQsrbKhaCylIfapHIc1Je03HHiiSfavRFSYCQY68MJY/1qHFz79Iz+sMMOs9dSIC655BLcf//9Nh/VzY2d4MZIz75FI15IEEq5EKQkqE2qr/YgSHH01lnQuIq+pESInm677TYrQKVISOnQ3gpHB4Lqq/xEg1JAJKSlONxwww1Wifz555+x5557Yu3atevoW5BAVl8rT0cbDlLwtPfiyiuvxHXXXWf3VejZvoS+lESV5623BL+gPQeaHzvttJO9V91c36gMpXNw/dwwguTvISSVJlVNdl9LQa/TYbRNPITZ1Sk8MPJr7HzcVQi02w19++yNMy+4EeO/m4rsSCZ23H4r7LfHzth/r52xy4Dt0LqgJeZOnYuhQ+/EwAGHIa/ZIPQ78DLc9ewnmLOinOVlUG7kUdZEeO1j/8bTmwMl3FjPpGTNhl30p7Cplv5/DW2WCND9toESxBEOYFo7e/Cqg4HaJcgI+lCQ1wKZdBXlMUxavBIjp6/BCXd+hhGTFiCSnwt/VgRrSssQI2MLh0mwfoOYJlsyof2ElijqWBCJxFn9stToxQHTmwJpAl3PeBrx94YTYqIFMXkJ52nTpuGoo46yjFHMSAxUFlJpaallWmKATkA5LFu2DNtvv70V0BL+EhqyjgTF10ZCJ7hUjpiroPKVj4S1yvMyQ2chy3o/7bTTrFUpp7zfeOMNlJWV2XBZoscee6yNr/qed955GDNmDGbNmmUtVQcpJBIeEnrbbrutje8VGBICN998s1VgtFIhqC5SFrSJTILfCUox83nz5lnGr76QFeuEn/pK+ctSP/vss60lLaif1X+ufe5Xeck6VbnaiOcgP+V79dVX241jskgl4NV/skRldWsTo8qQn+5dvQUnYNQujauEpa4lsLQpUBapynbWqu5lxWolwG3g03io3lJOtHIhISxoQ+VZZ51lhbUUN0FhiuPgFBFHYw5SXASt3giqg/pdAt2lcWMvyKoXPUp4a4VBfa5ytIqhvLQpTxsQJYDVZvmpfVIMRC9OoGrs5K842oSnDZFSWrRSoDqKvgVXX0cbUm40BoJWxb744gt7fdddd9lVB421FAEpt1pVctCKgfLUCkX//v3X8VspF1J4lL/GTSsSgo/1rK9wrAd5epK8PMZ+9FExC1EgBzPZT0l89P007Hn89ejedS9ccs5Q5GVm4fUnb8aieaORmPs+Zn/+OL564SYMu+F0PHrNSXSn4Ll/nIZRz1yFnz55COXT30fl8nF4//0HsVmvzrjypgfRbbP90f+Iq/DUB99jFeVRwB9GIJxJucK+oeTyUa6kz6SRMEnT9p8GE/0lkUro+Qwv6DigaU+LJMMSJl5bbRLxGuvz8OhJpv25r5hWF35q2l34sWly3vsmcPIrBkc8aZpcNNIMuOVTc/xjH5ufSqpNcbTWLCsuNeVVUVPNfNdWV5q4dnRs9NlLerOhqiCX3rih3/RmP/u7Qf0a8XeCngfr2aWef2tjlKaknpEKeh5LYWGfN9OKMWSydm+Am7aOrvWMXxvN3PN6bXLTxjFBm97IsO39CSecYP30vNxdkwHaXz0blr+eEzvo2TkVCHutssi07bWeNZOB2ue/FKD2mbNAhmqvKSzsvZ75UtkwVDzsr57b00q2G9Rmz55tDj/8cEPr2cZ1edeHnh9rU5v6p6ioyLadgsk+61efaNOfNiIK2tQ1bNgwuxGP1q7d0EbBYy677DIbrrbqea/b8Kf7jZWrTWdKq82JDueee6654IIL7F4IjZk2uAnqG8VTn7jNa4I2XurZPxUte0/hYzea6Tm39nOcc845657D33nnnbZtao/yd6Bwt8/gtVFPG/r0LF3QXgYKY5uf2qo9DxoXKlj2XvTg8NNPP9l2u70O2hgnmhI25I3r8corr6zbe0BhbjeiKl/VT/tDvv32W1s2hbHdE6ExUR9ob4nascUWW9gxllOdRIMaD+15UJu0l0F7Iqg4Wj5444032g2RgvJSH7jNouPHj7d9pr0lGnPtHdCGP5WjPFSm9l9o74BoRG1TeYJoUptLNW8ctImTCpbdhyKacdAGbtVFPWKd7Zt0/2iTXTJeyws6YnVF1Dw9coxpv8tJJqPDLmafk64173060YZ5UVVdY1avKTfLV60102fNN9FYwlTWVJvFy1eZWCLG8nSflkNejJ++3Bx43h0mb4sjTdOtTjBXP/KmmbckvcdEG8gTsVrKL9ZX+wAkU1RvN5S8WF/zjeMv+56/PaGP5rjevaS+tn45R81NxBELUKNGCBF/AFXUos4b9gNG/bCa1lIISQTBfkVFtBpVlauRnx1Gj/Yd0TIrhmN2aYsdOrVHDrMKRvx2xSAYCGFNRTVmLCnGmlgCsZShXwBtMgLYrF0BIhlp0z9taXitfZ066IPfF4JvA/9G/F0gK0hLubJOdS0LRtaQoOelZJ7WWlSYLHlZUloy1ZI2mZOla8WTFa7n+4qrlQNZNLIwBVlZsqZk6cmCVHnKS+VQAFpLXtaarChZfspDtCo/rRho2VxlyU/sQmXKCpYFppWEAw880JajFQq96qU6UljbMvW8Wa+G6dm7rGK1U/XTEqyW02Xhyrl6qBzHkmSpyXLTa1ryV35qp9qhusiik6Ws1Q4tF8uSlBWtvQuyXJWP8pVFqFcMBT0O0KtieuSg/FybBF2rHNcf6icKbxsmaHVFlqvK0kqJ2qn+VBqVp34eMmRIXWxYC9mNi9qiNFoxkUWqVQBZ+qqb6iLokYOW3PVYQHVSfVSe2qMVCVmrKk97A/SaocZQbVBcWcSyaLUSof7VSo2DxkWWumhLKy9qh1YiNAauv1WWoGvVVeOrMdP+BUGrQh9++KGlKbVHUJ6iAT3CEB0IWnVR3WRpuz4XDWm1R+Om8VEZqqdWpPbZZx+7PK+VDCq9ll4UrufyohW1VRBtal+J4qvPBNVPfahHLmqXIBrQGGvPh6DHTVqBUhqt4qi9ol+NrfYEDBlyFPs8Qv8EG09aoMzQa9t+cmSa+el+Mdq3QT/KjFQyjo++nYlbnh6F6bNmYv9BfXH9OSejR8emtrxkKok402awjLXFJXhu5BcYN2kR86pCbibw0G1X4tufpuDpl97DIzddCD/H+/bHX0KvNs1R6Atjv8N2Q06GHvmlx2PZqgo8/uoovDByNCjhces1p2PI7tshEgqAgt/WN2ASNrZOqaW+ykoYpOpWOTa1tP8XPuQn/TzQTW5H3Foi0aBGA+woCtwIRX15LI7LX/wSb4yvQGHTVuwVEgjj17CzKxK1KC8rRdgk0bl9G+RnVeOgni1wxPZd0alZPqqqY3hn3C/44JdF+HlOBWI+g4xwBIjG0TN3LYYe1g7tCqOoTWrJPwJ/kGG+LBJYFplMLnLzWiAjrwn9NjVMjfgrwjGi+nC021CYg8IdvIzbm66h/L1+unbzwxvP5V0/X927NPXzdcLbYWNtawguT8FblnP/Drh8XZ285UiAyt9blvfaKQkNtad+H+pX8JYlbKo/XD28cPnW9xcait8QlIfgLdeb1tXVwYXp16XRtfdecPG8aKiPGkqreG753QspEVICpCB689e1UL+8+nXYWDwvFMel88ZLpdKPgpSFfmk3IyUlQN2XSO8h0X6tBStL8OCTI/DO+19ji2364aKzD8OgzdJnDixasQrffz8dP1H5RWY2Tjt6f7TIzMCHYyfh+fe+wcqlS/H1iMeRRfb/2kdj8czLH2Pk49dh0pyFOOu6+3Dsntti2dwluPK6C9GqMBvfTZiCbh3bok3btFKzuLgSdz36Jp5++z0cOngnnH/qgRjYtysbFaOxyR9fQDsSEPAnWf+QVWDUQqvEbAR/3UN+NIh1xO+uOez8xxH2cyJa0c8JxhGPhKgCRIL4bPIshLLzEAnrmF+/td6DGRx4Dn6ClLCaAxDzFWLqsnIUVxej1hfDvc+PxiPvzcSsqkw0bdkZYdL1skXLkJVXgOZNKtG36So0CceR9GVQSwuR8FkWLX0Jf38wB+HMHARZvpcYG/H3gqXNOqYkOCbl/JwgcPQsf0FM1aX5Da0zjsK9ebk4zt+bXr/y069zgksjOD9BcR3D17Wg/FzZ3ri6dnEV7sJcOv3KufQuXL/yd3Xw1kVw6ZxTGYK3LMGFu/y81/p1TvcO7tr7665dOYL8vPV2vy6u4PJ3cGHOz/02lMbr59CQf0N5eMdX/vXzdPcujovfUBxvvi6+4M3XOa+/g67VN946Ca4vtUqgNx8EhdfP11sf7+/vxfPeO+eFLHqXzoYzuk/P9pHghTYiBikHfPjy55k49ap7MXXWYlx3/tG444rj0LGF9l9oX4If970wEg88+g7223sgRo76FqlkAoN22Aq9u3dEOCuEj74ej0tOPlhFYtyP0zB38WqcfMRgPP/WN1hVEsd5J+6Lw44+EK3zszBz/jJccPkD2HGbzSn8i1BRU4si+u+7y5bYarNeeOPT7/DYqx+iU4fm6NOFyoc/gLhWm61s8yFBRSDAeqVVrvWKV31sPOT/czhCkxPSA8sJrDCEOcAMs3RHa4W/O3Vvi+N22xz+RJV924K6ALIoyXODGSjIykEehXlGTgbKSsqxvCYTH8yqwcXDvsIrk4qR23VrdG/bEU2yMtCpaws0bZ0PX04mOm++DXr0PxJt+hyLtr0ORdue+6Nll93QpvMOaNWpHwrbdICfeabsyU2N+DtCdCrGI2vIMaf6TNgbJnjp2kH3iuviy+naOw9cevnXv3ZpnZ/3XvCmd/nJIpKfC3NCV/eCN72gMFcvF6ZfV19BcbxOeSnMlenKcmHuXr9pCy3tp3zdtZzL2927a+fv7r3p3NgI3jSu3XKu3vqtf6+4Di4fwaWtD+fvDXfXSi9X3985B3fvLU9oKI4XDcVvKE39dO6+flhD/vXjOLi2qb8d6sfztt2L+n71y6h/L+h+w/wcLaZomJGOaaT5kwFKiRjClAPvfvE9jjzpGrRsVoi3nxmKk4/c3cZ/9a2PMGHCz/a6X89eKKdht8W2W6BJ01w0a97M+gvd2nfGvGXpzYxCFRWDUKZ28APTFyxDfiQHz734Hl568Q3r9/WkWQiGk9h2u16Ys6gYx556G267+1kbtu+O/fDuk9fjpEP3xfHnPYqbn/6A4suHrAD7j+G1CFk557dKyYZjWh8bcpC/CEREjpDcBE4Ptnx8iHKQ9TpegkI35k+HNeUo771NZ/ToWIhQyMfO1ytWPmT7g8iNRNIuL4xAbgrx6hhq17KTI9Tq2nZHnHHjqSgqquNYVFqF7DbtmDYHnZq3QeuijiyrKcvO4uDomSYZDGshQpPMDybJsFK/XQZrxN8Hbrlcz0v1DFtwAkhOz2z1mpO71xK79gA4uta1/MTMFC4hLH9dK2/dO8Hs5oP7dXHr3zvn6uLCdN0Q5C/nytCvnMtDwlDPxQXFc+W4vN0ubZeHnNqleGqba5/yUP7Oz/l74a2LO0fACWcvdO+c3hQQFE95qmzlISjc1VVlaz+F81M8b9+5+PLzXitc7p+F8pH7o/gzcYU/G/+fQUNluLESNlWHf3f9XH76sc/zaTHL6o+nOPYUnEZ18mfguU/G4dBjL8IxB++PN++/Ept1aIWysjguuuFxvPrhl+jQrYvNZ4+BfdGieTauvflJNKHh9+6nY7F0dVrgd2ldiOTM2YhG029/1JSVwFStQVU0htlTf8AJRw3ANReegMP22dXKhtfe+wL77LOLjfvgs++iSZv2WE6a3GKP9Cue7ahc3HvZUXhw6Om44+GXMfROKQaUV7Rag6S1oOGcoXGr9e1NYdOh/x9Dg7t+gNNMUa9yyCuDHeSj9qVz+ivjNaiIVSFWG0O7Zlno2qkQsSCZE03/QHaGfaMjJxRAXiSELGprmZkhZAeSSMVq4QtQMcjPsYcAad4nk7WoLk0iXhVA9aoyVC4tRjxRBV8whoQ9/IdDS2XDR83Dn2L9qAhI8LsPQjTi7wXRpbMU9Q683pXWxiW9Jy9og5jOcddGLm1Wcq8k6bAVbfzSq1R6fU4b0HTOvWOiei1Or10JEk56TUrvdas8beZzDFfCSn4STnKuPgpzfrrWtwf0qw1jOh9d77Xr+azK1utbu+66qy1L6XVeu/x1br/uHST4tClMr8UJUkpUls5Y1ytkaoviv/nmmzZc6bVpUO+oq84qQxvZtFlPbdPGOPWBDhhSPbXRTvkrD21MEw4//HCbXu+ZCzosSHFUlsq8/vrrrb82DMpf788rL21gUz3VP6qj/Nw46SmpO9hHZw+oDipDG8r0yps2X2rjn+Jr3PQ6ouvrfwVql7c/fw9/Jq7wZ+P/u+Da5dzG8O+un8sv/SPljNccJwlOPQ4WfQ4fPRWnHngRbrjjCjx4w8nIthvx9K2HFzBp8gy89OD1WLR0JUZ9+DlysyI4/pC9kBnOxMsPXIuS0jJcdf2tiNZUI5LhxyHH7QN9W0Y4bI/BuOL041FZWooBfdthq64t0LZ1Edq1aYWJUxdgyq+zcOGpQzBp6nw89+YYdNmiGx68+kzcfuX6Q66U01nH7IpHbz0fj7z6Ma57OD1vQn4qzAkKfx/rag8B2jj+ksJfjEYT1mrbvNdpfNHqGlSWl6GqrAymohqpWkPLP4LsUDbysnJoxeu0pwx07toMrbu1RaogEzVBCuZwEMGQ3+6uzPJR8PvDCGUFUZObjRLeR0gsGSmDMmp11RyRKmqFxfOWI+jjPQ26WDKEREqWHQOpjelQRW3GkLGfoiKQDNEC0oEEjfjbQpuctLva7SbXu+Q6MU674XUSmwShrFHtbpeQ1/vNOtFMioIOy5HAOeKII+pyS5/4J4tXO74lOLWioJUDB+1C1+5tt3yteSIBJWiHtQSVwpylql3YOhBG7+/rWrurP/roI/vRGu3Cdoen6CAZvQ+vA10U7rWe9TEZtUG73eWvcvURnOOOO84qOCpLBxHpTAGVr3ro/XPt5lZ+2i2uOCpPb0Cojbq+4oor7EE7apN2g2sHuHbLK2/tnteubu0g18EyEuj6IJEOx9FbBFIKpDyoz6Vo6ZmzdtSrPPm59kuQq790KI3eh1f91IarrrrK7iRXXRVfO/sVdscdd9izCbTjXWcFKK76UzypEf9rSK8Ia2zsYW28CQcimDBjFY7Z8zjceP8VGHpi+lm9G71LrjoWNf4oTrzycRx85CVoUff2wS5922Dsl2MweepMfDjsLtx26zXIyMxifgG8/fw9KKkqxew5C5AZ8aFvDyruLZvj8XtvQteOHWx64fkXR2CXfl2RkxnE/S9+gCGH7YmvPxiJk8+8DLvtPMDGqahN4sGnhtvrkw7YEXdcczpuvesJPPXBGPpESGucc2pPatMK519yt7+alOJIiu/4KJx1YajPqSvU2BK6xbUJzCyrwfi5K7GqeBWWr6LfqjUoj9NaigWxxdabw0+ZXbWyDFnRGFKJGKoSKVRXpVAerUY0ECKxBBGJpcg4KlGdjKJpqgR92nTCATttjTYtqUxgJQa2a4b8UCbroFdJSD4aF1ZC9fAzfcpH4vP76J1mko34e0EHw+i1PQl6vXon2nVWiV5b0mt0OvRFguboo4+2Vr4gYSWLVa+NSZgJEjwSVDoBTUqCBL7ylcCVciEBLoGkV5wkiE866ST7ZTNdSzjJqld5OphHh/aIITrhrWV5vfInC1vCTdChMDoARl9cE1RfCW8dhSvhrwNo9EqbIAv5lltusUJZr29JqZHwlVDVaoHCvXjxxRetUqATAvVKm/LR63fqIx19rJUH9YGOEta9Xu2SNa8+kaKhe9VBr8np9TCtWrjTBrfYYgu7KqBT4lQXrSbooBkpUFJkdGCN8lK9nMUvIf/000/bsdErajr9zx2hrOOD1VaNnXat6xhfKUFyOhVP/aP83Pg04r8H8V3JAbsoTuFoN1+TzrVBTrv8A8EMzF1Rha59DsepZxyAZ24/x6Z74Nk38dQLwzF6+JNo06YIH3zzCw44/iYsmvcW2nGKXDH0Tlx45vFARjYK8rIQJKufOmsOpvw8AxMnTEJWu9ZoXtSE45+NIMtJVcexoqwSa0pWoVvLltiiXxdsu/WWTJt+ZfLdT77CCVfcg1Ev3ocd+nVHooYKeWYAa+IpdNl8CMpYxysvPhR3DE2/0vnYW5/i3OOuwuif38fuPdrQ4o0jSRkVsIoNI9hN7mq9PFjh9P+/HhzzlK6mhpdWxzBlxVq88etSXPvpNAx57juc9Oz3uOGd6XhjWik+WRLGL+VhlPmLKORzUbsmjimfjUF+Koo2rYuQyGS3ZYRJJBwYMkka/shM1NCsj6KSE1rnAWQnynDK4AE4+bABKMwnIa1djpxYLXQUcEJnMkuT0E5CERuFvl8bDe1zprRrxN8TesYv4eYEiYSwlqW1BK2z+CVAZJHKalWcL7/80saTwJZwllUpeJUGWcUSYDrhTsJa9ypDy/VKo+fbWmXQcbkSZCpTgkkCVu/p69x4QfnJX5ASoVUEV09BKxAS6A46616nzEloS0mRMiHoZD7V8/zzz7eKgRQGQYqF4kvwy0KWgNZSuc4x0JK/hL0UhMGDB6/7doAg5eb000/HpZdeavPSp2N1Rr0+GytlRha/lvHVLkHlSFEQ9A64BLlWGQTtp5AypPrqFDutEiit1yZSn0nJUn9KEdMzfxeuMiX4JfQF9a3C5CToFVfpBadINeK/CzsaMgo1TvynrXF63S9AeimmUXjw6bdj690HrBP8CxavwLMvUpl98j4r+E0iif132hxnHbMdhhx+DXYbcj2qqgxatWmNCBWKr78Yjzvvew7jJ81Hky5dsdcpZ6J1280wb1kU3/0yH99MW4LJlEc5Ldti74MOR58dt8Pc1VHc+fDzGP7mO1jKOTmYfsPuvBIXXnobnn3zYyv4V5bE0LnnEGw/cHOsKR6FqXMW4fnho2wdzzl8T5x92Tk4aMjlWFpVTUZCWRNPqIlW5Kd/2XLnQfwlqVEMK2V0yIcP1SkfPp04Bbc99w4uu+dV3PvSx/hq4gJM5cDM+f5XrJw0A6W/LkDZzEWomLUc5YuXIBldhpL5s/Dx/U8gsmAqilrlwRQ0BbkCEsEAyoIRlEd4Hc5EsqYcHQKVuOzgXdG3fSesWr0MVYkq1CarkRv2ISusVy60KUhMVMxZz1Xd/gNa/Lpep6w04u8GfQBHgkLH4Qru++862EZCSs+59aEVCRF91ERHqjpIqHiFlIOEjtLqmbQEv863134CLZNL2Epx0JK6ICVAsLRImpTwlZDVHHJ0KWGqpXQJVi2f66x0LcNLQLsDXyRctWKgMk444QT7K8GsfLRSoLxkWes7+FpWl2KgQ3L0/Xot1esRhc6910qAltFVRx04pGfmaqMEs/wFKRNaiVDYwQcfbPcdSDnSs3rtnVC5OkxGgl1QOgltQd+E1+qEu9ejE/fRHo2FDq6R0qC+UP/W72ONg8KkBOmRhPYRqN1u34OULEGPXbTyon6X0iQ4JaAR/10EOJz6xr6+nS/hr0NyAkEplkHceNcLpK3FePfRS9ORie9+nQN/ZgRb9dI7/TH4ggalZWW47vILkG2iuGvoCbjj5vMx6vNv8dwbH2F5TQrNe/TD51MW4owrnsZBR1yMS255Bk+9/QPe/WY+Rn41By9/OBXX3/MGDj/2Whxx9r0YPno8Cjr3g8lvgZff+dxa/rsM7Ifhjw1Fj47NMHb6Amx/wFnYZ/AWGPXc9SgMAAN36osPP/kmXUnisVtOQxs27uRz77H3gZCUUMkdyiCtOlMeWnXHvuZm/rqWf9JPBsbfSNCHfdiJj19xIkbfcSaeOnVXXLtnZ+zVKo7NTTk6xWg5TZ0Gs2Am4qvmUZivpSuhdc/OSlTi9fuvxvQRwxCIrYW/yI9ws1xEmuUhkpeBJKpQEKnGEXtuhaaBCpSUzkVGKgx/tBxh32I0L5JxT+tDc551aYhRN+LvDQkundEuIaKlfQkUCUM989dzaW3U0xK8hJYsYfdsX7QkgSthVB/yk9AX3Nn6EkZaTZBwld/JJ59sVxS0uqC8JNB0EpuWwvWc3C1P61cfj5GQ1cd2JCCVRpa4BL+W3wUt5+uZv/KWIJXA14dwtFlQjyBUf60SaDOiylU8nUkvK/vcc8+1CoY2+0l4auVD9dUzdDkHKQSCLH+tLqhe2g/x/vvv25P49PxfSoWEu06TUx9oc56ex7uP+2hVQfV3io0epehbCOp7KSzatCchrRUAfYRGypbbGCiozxWuumhcBPW36qT4ao+UFu2N0GZG1cudee/KbMR/D9rR7+f4adO1PRHPl0BQr135Qvjwu2l4/7OvcM91x6N1sxxa+OnVok6tm2Pt6rX44vtZvAtjdXkC1978HKpK1mL0yPtgqivw/IiPsGRtHDXhIjz60me48LZnMGr8bNSECqgIbIdWnXqiRVFbNGnaEk0L26CoqB1aduiKtr37Iadpa0yctRZX3fUS3etYVhlBzJ+J19/5FIuWLseO22yNubMXYt7cRbjzjvSmvx9+noO3Xh+LPt172vu4FfLA8Ffvw+iPv8PDb31p25Sk0Nejb7Xb7jfTL9ssofQXPeTH0L72U9tJP+mPBILIJBNrnk3trWMrDO7VFgcM6IHdt+mJPbboiCxTiqK8MPJoqa+Yvxxm+UrEyXyS1ZVUBqOIVoYRz2yDNVQVq2viqOVkj61ZCbN2OfbebnO0z4uQM9EqyUghOx5DRvU89G4HdGzTAakkLQx7et/v72htxN8TWtaWgNDSugSwrFxZwjrSVc+kZY3quFMJb+3cFw1JYEswK1zOKZYKk4DSUrgEnp5l63mz4qgcHaUra1sWuZ67617CTMJfyoZWGnRUrPwVR/7akKjn+W5JX8vzsmqloCiu6qFwfWBFy/Ha7S8BK0GoPLQTXvsDlKeer2sFQ2m0mqA6yoqXUJYiIeXDffRHVrPKdo8aVE/lp/bJmpeT8iQhrPYrDwlzrQDo7QitTGhJX/d6/KG2KI1WB5RO0B4I5auVFilE2h+gPtR4KK4UGoVrL4H8JfTVp3oMoHsJeq1kaJVBAl6bArUJUwqUwqSQOAVJ8eUa8d8EZQNpJUVakJNF7PcHUBlNUfA+j3Yt83D9uUfRug/Z1/50cE7romZYtrocjzz/HubMXY5XKeiLmuRg78Hb4JuvvsWi4lLUBPLx/pe/4oURn6M87kdWYXOEdICbP2RXGXwpKhImbmWSHjkYrTbICmcZssEDkWxk5zZDVW0AEydOw/xFK9CpSwfGTVDwz6AyOxht2rXAY4+NxIQfpuGpYSPRtk1bXHP5cSjQWTEJnVGQRLuifMSC2bj26ntwzWUnIsy2ydpnDawcSnMJzmuf3jJzXOOvBDWJE9Y2TJNNF34fdR11g7z0Oocsm/TCR4whxWuqsKS4Al+Nn4HZ8xdjDa0GZfP53NnI6zMIiLTHCg5yUoOYqAZWL8WAvl2x+9Y9kY0q6C2QWHwlWvpWoV/bLPTqSaaY2QGJVIYlIGXmrKlGNMLBCV4HTUcnILzX3ngSMqIld68wF1e/js42lrfXX8LMXSvMlSch6669eQj16yV40ypP97y9fr3dErjgDVN5Dc0Pl79Qvx714a2Xgzff+tdCQ2U2BOUt1M//j+BfSduIfzcoA5JxJLQHi8OizXd6Zfu1T8bhmruewWO3XIi9B/bTfjmYQA0ChvMsEEZFLIGXR36BOb/ORpfOrXH8kXvjw48/RUF+EX6aV45n3hiHBavLUNSyKYWwnxa3eL7y15diOC859hLz2t+lawVTLpO+SRe60QoxBbK+DB9impqqNQjTQj94r22xa/828NWU4OhDD8Go0RPwxffT0LJFAU44fE80L6BxmZAEC4A6h/1WDbNAXtdDcdmlx+CGsw8nrevRc4LKTphl++FLxizd/zWFP3vUPuLQ+Opdeq15UOD79PEGDb60HnZ2ihoZu8LpAAphJ/pQUxOz1kWiIBddj7sZqc5bIlZKTa06hXiiHKmqamSlYjh6z4Ho0dQgGV2MWHQVurXPxpZdC9C5VUeEI12R8uVYbUxPEFIcdD3rb0QjvHDCTdNQwk0CQtfeaekVkC5cv87f5eGEqZyunbBxeXsFnstffi6+BLa3bKV3+TjnynJl18/TW65L4/Jz4a6O3nIEhTk/b7jXz8V1UJhc/bJcOfKrn6/zc/curn69+ejXwcVx+Xjz88L5u7T182nEfxukFx3kQ+EfpLXsDwawuqwKh114O1o0ycKr919D4csxjPmQDEUpMsm3U2HOjQTt5ZAVrFJrX399FAraNMOY6YvwyJNfIpzTFv5MbbBTDI63pSPOuxTHX2Kf8kbrCHrkoDA5yRqFBrUKwHhJfThIMklaAUOj0VrUVtbiiL3745Cd22DuzxNw+WWXQVt8Rb1670cfErJSi1n69V6/VhmCfjz/zjc4/4KhmDX1E7TO08enauzJhVoLD+pNOMVn+r8e2DDjZ0fyMsHrWjrqOhzPEO/pOAjJpAZHGwOrkUrWUBuMMTI7hVqhvqrUlIL/m6+/R+WqGJKLS5AoWYlY5XyEKhcBlUvRJFLJTBdg7ZIvUOSfgv0GZGG/gW3Qo3Nf+CMdmVUWq0F9TBv9LD2kmcJ/FHWMSH/VF/r1Xm8MaSUo7dy/9ak9TvnXlSG4kEb8eTiB4wS8g/wa8nfCSv4ujvwUx/kJiuPSCy5c/u5e4e7XOd075+Aty5XthJ/Lz9VB9y6Oi+/N19VHcGFy1iKpSy9sLI2DSyfn8hFcGpePg+5dvV06F8/dO+eg+PXjuHxdndy9fp0TlK4R/1uwI6Pl/rohnjJvOX6aNhcnHLwnxbvCq0jItJQpHlP+eNpa10qAlRzAF6O/QYvmzfH9T8V44OkvEc/JA3JIF1QqZGhqk519zk4akDxOyNqmERpMUMjrkDfJBBqPsvADtMr1bX5twpPN7vfV8rrWrhxkRHKRRTn06nuf45Nxi9G997Z44YVXbB1lvSdjUZuvVjE4o9PliU4ZfvS+A5DMysRLr6XfBrDzQYoI26xXy1Wxv6jwFwPSh3v8CLOdEYrhsDQsWvx++gcVFtBk1oBG6LTkErSdIoUAdkME8OqIrxCJ8b54MTLKlsJXsgK185cBMyeipW8Ktms1A0ftmoEj99oK/ftsh7zcXUkAnTiwudI/CFEXh5Qd75jkfxQiMn3ekZckSf4TgahO6buNwTCOjh92RJyis6/GKCOlF2O0+cg7aZeqdGf5YNq7EX8Cog2vc36CnbT16MctVTv/+nGcn4SUrus7hTkh65zLU+HOindwaVx6F9fFc2EuLxcm5w1zfi6N+xX06712ZTk/b7leuDju15uH+/WGuXtBv64Oci5/XXvhwp2/0jnXUNyG/BvxvwKOi5byaQ6KN8fJ50Z8MQ47dO6OfQb2p58EZASJEMeXUiPTZFNmxKwQD/Lf3PmLsKx0LeaXVuPZd8YhlNMUWTmFVBJo6PloYeuLejocjoqCnyJZzDFEf58vk8WGUcsya4NBBEJJyiHyV9JKgpZ6XHJIZdE61yqxz2RIP0EomERhi5Z46rVvsCCWj/KKcsycuxBh5pVgWknwUJJls1y7j4EkLLM3IxzCxaeciGfeHoNKTmUfjd6ADpsj39YaBgUByxD1/02hg4A0R9UDOtvZ+un5SCCICb/OwyFnXIWyUD47NYjoqpUImhLstUsXXHjkVtilJweKxOAv7Euq6EJSykHYykd1Z3qJ8L/OAJJx1kTKB6+lWVptRxKaHjpzwOqxDpLsaaZoL9PN+C2YVDTkWpYi4WmaUK9aZyGp7Y1oRCMa8T8JCuQEoggGMrG6tAzbH3kZzjt8f1x4xkHWeicD05Y4ck7xNwlxCkryS0oLvDbyPWTmF+H+F8di8oy1FPzNUV1TjFAiigrLbX0IBSKIZBaQx2agNlqBaMUSmzbpjyKckYesUB7iteUU0DkwmdlI6tsw8SSqi9cgHItREaBiHMxAMCMEvTmaGcljvYJY9f2nWDF3FG655W48fPfVlkdrhYm2LBlyeiVQcixBnhyiIvvT7OXY9cgr8OTdF+LI3beu2xSouAyXfPpvCH9vkX9UQNavpjedC5Ofrr2/DcEbvz603KIzmK+842Hc+/L7JJB8ILEa1595IK69YB/GWIhE6RoOThtqff0o8JpIj7LLNUF/+vQ1L+qXtamyN4aNtcnlJTSYH4lcGqsGmyRi5bnSWGFNrVOvftBHEWwcJ/y1A1bxpDRQlPPaXqyDbuVnN67wXkHUO+1SlN24os00jWhEIxrxP4gkhaO1zGk9fzjuFxxx6lBM/XIYOrcsqBP+9mPvlhumaEBZFkrhP+GHKZixcBV+WlCJ5974GpGCPJSX1+LE/Xog35RS+Gv1KIhIIBdjJi3GT3MWYZetO+P0o3ZGi/wClFVW4633R2PhgjXYetut8dXkJVhQXIlqypO2uUkce0B/7Ll9fxqbPixdVYpPvvoRX/wwHWuS2QhntUSqcil27tkWZx0/GGtLV+K4g/eW9Gd9pZ6k+bA4veXvvK6qqcUR596JgowgXn3sGiRSNdQRyKlTAQp/xieTXy9B/kNwz8MECa0/Igi9aQSXzgqpuibI4kxrP+t/BYW7MrzxBcWx4bxO8Vf3UV7vsN8JmDx5NsKhCH4Z+wx6tM9A1ZqxiKcKkNG0L4VqCyYIIJyispDy2QHzB9eX4VC/LNcOV7ffg6uvS+vtq/p514eK8ukdVopoEXPcFi3CSH/7WYtf63NIQ7krtjYqavlJqgKbR4VA/S19mOmkHDCmdrWS3KkEKJYS6vlY+sMxjWhEIxrxvwgZeL4EhXoojKH3v4O33/0MU796hPxO2/vEY0M06CT8yefsaiYtcxpS4yb+hGkLluHZDyZjxqIy5OQ1weplS/Du05fiwG161OUOrK6I4ujzbkXbFk0x7K6LLCNesGQ1TDiA1kWFVA78KI0lcep1w/D2+2NxxO798PgtZ6JpfiaWrFqLypo4OrQoQGZGBF99/ytOuWUYVpaHUZgZwZI58zHqlWvxxWfv4e5r9IEpcfA6ubPub5qL6/fGB1/Hl2N+xui3bkPEX0OVR58r1vkGkgT/BUg4OPdHBL/gTSPnhKhXAHqFo+I4eMvQtTcfweWh59zCB+9/gRk/zkaL3Ex8NuIeCv4gSpb/wPDWyGy6I4KptghRyGUyfSAYAMLMq07wCyrDOW85qp+3vn8Eiq90rr0u3z+SlzY76tG8z75TmkQG65HBqkToAqoT/2vJyOvSftoXEWKzqMXyOpMBEVJL2BdA0K4WCFSukspXy2IsRPWkSsHa1YU3ohGNaMT/FizHlJyoW50cPWYyBu+a/v5E0j0SrYMUAckEnz+AFStXY8nylaioAuYvXInsnBwqBmE0bdMOBx1yFUZ+NM6mWVFcgRPPvRdfj/8Vpx9/gPV74PHX0G37c9Bl1/Nx6Lm3YOai5axD+nW+5vnAXdefaAX/TwuXYPMDL8dmh1yDo698CGXRKHbZtjdOO2wXctZau3Ews0kTPPDs+xi40w74aqzK9NZXLs2B7bHx/O3frxsqmM+0GYt5p1MM5a9eoAJEAbJpCfJvhorT0aN6lc5tCPqjUNxEImGP5tRBG04gKk8JWJeXDt3Qmem6V9jGytABHvpQiDuEI6ndmiSKw444A1NmLMVDj9yInbbKx6IF4yjkmiM3rycyc5qhSW4eSirWYOHiRbR6qQDQSQBqGbx+WapjUVHRuuNUndD21ndTUFy12b0fra+Oqb46KEVhyk/5uLy8edYyXDtK/X6Dymgci4sr09a/4tv6poc+/dcRBa/0460a88kIBVHUtAkKQnoSJtFvkEzE7O5Vq9wkfYhROQgpH6mVjWhEIxrxPwaxtjgFrw6/Ka6KofN2p+LFhy7GwYO3QsKQn0lo2lfi7Nom4ok4QsFMTJ4yE2PG/4yf5qfw2he/oqAgF4FUHKlMP1bOmIbh95+Do/YdiKUS/lc9gQkz5uGT567F9t3b4r3RY/HSqF8wv6QGq+f/iq26NsU5px+HK+9+Cdv07oz7rznFfi5+5+OvwTc/LUFOURtkJUvw+NXH4tC9d8LMJWtw5MWPYv7aGLL8EZRVrML7T1+O2eM+x9lnnmzbZDlumn2noWuy5SmzFuD0qx7BBScdgmMO3IGmWgqBJIWA5A8FiDfJ/zl0bKe+V64jN3WG+B8VgE7A6ShNHTH6wgsvrHvNxu3S1Xnjcvrkp75iJqHk0taH/N0XxXQamRWiFFo/TpuOu+55BIN22hH9endBxcpZFHDsq+xmaNqiIzq3a08lIBvvj3oft996OyoqKhEMBNXPjJQuR+W5btWvThvTqWj6Apm+JiaoPLcisCm4+qvdOhrVfdFMp5bpWFhBeQmujxzSz6vYR4EMjJ+7FDcPH4PFlSEEwln0V3tZx/R/uvTSffqv3ktlnsxKGqQ/VYvcoB9tWzRBp6JsDOjcArv264oMhuvADJ/dL6CVBuZtJ89v+7sRjWhEI/7bEK9Lkl8GyXt/Jk/cYs8LMOubp9CtdVMK+gS0gKtDf9KR43Z/gN7RHzPuJ/y6YBWe//gXTFkWQ5OIH+FUGeKhfKxaMA2v3nM+jt5nIFasLseZ1w2jwJ+MZ5+4BKfsubnNqioex7LiapSWlGLR7Dn4fNwUPD7iWzx463m4YMggG6fz7hejIpmNZCgH4doyDD1jMM45dg8aqSnsddY9mLywHNmRDJSWluCSEwdhp855GDigPwryc2276thwGmLBdEtWFeP06x7FLltviSvPOJDSQMKf7fJT9lK4KN1/DDqrW1a7vjH+z0LWuo48lfCX4NexpPrgiD7ZKYv/z0AfUtFnPNUNEqILFy3G0qXL4A/4EK2uokRLIRQJo6BpU7Rt3x4FuTkU2gG8+NKL9ijTWK1eGflj0PGoOndcHzDxHqhSX2g7f0EKgk5G0xnjOo7VQe3WGeo6u9zVXXlsoFCkJMT1zCqCL35dhGMeHYOV5ZlAJKTjCKkCq+6OYqhA2XV/wr7USqdJYBiXebAAaJdIKBRGs0gCO3aI4JJjd8aAts0YpwZJGy9IgvpjSk0jGtGIRvynYdmq3vIif3/xk4k49dzbEZ/ztkIo/CVPeCUFgPzQbpeif0V1NZ5+6XUE8tvhxsc/BTKaIiNQS8MnhmSkOYpnTcFbD52Gw/YeiOUU/mfc9Co+GD0TLfICuOG8vXD6CXts8F6VMH3OMlxw/dM44tDdcMYR6e9OdN7jeqxJZMFH/pxRE8VVp26Ni07Y24btTeH/7bS1yMnNRWX5Wgzs3QzXnH4YmmX70bd3Z3vYjzaqk/va+DLu1IrK2jjOGvoYwlQonrr1NFsPe+IfefR/nEurUJ357aCPeMjy1hnhOstc52E7p3t9e9vd61rL3Tr7W3AWv76zffPNN28g+KUgbL/99vbsbp1BrjRep8+XSgnRV8wcJDzbtmmNzfv2Rida6j179kIP1qt3377oxnIL8/PZnWkhXZBfgK5duliLXl9hU7169uxpy3VOHwnR18UcpKToO+A6H93BCX8vJMid8Bf0KVYn+NV/arcUH308Zfz48b9Jvw60wFO0xgW92pERoeBn3wep3nZv7sdWHUPYsp0PW7b1YXO6fu382KIt0K9tgNeZ6NsmC5u1DTMsC11aZSDDJBAvrcCKWDben7Iap90+HOOXlrLjMpHQjNEjBquDNqIRjWjE/x7SnDL9d/rCpTTI8u21hLwMPvtmE3kvuTL96CRQaf1XVOm0P30CugYReVO4Jn0BGD/5a8o+RE3n4jeoTVYiqyCOYw7fASPGfI1Bp12PS25/Ho+/NgpjJ81RUejVtTX22WVzhGhkOY6Z3zSMWl8l4v5yllWOcDC9sr22KoaqmhhCNBj1SmA44sfK1aUwgQxU16Q/7KXaatdVyse/tuoU/rzOyAijSWY2Vi9fgajOrFE8tslGoZD5j3JrfQBDn/LUF8oEWeuyoPURjd+DhJwEo4SfUyD0wZPjjz/efoVLkEWtawleNW1TzZMA1f4BJ0yVv5xLJydh6/xVtuLJT7/6drcLawjy17N6fZNdFr+riz4Kos+i6nOiLj9vHoonf7VFHyvRh1600qE4Lg+FJRIJ+2W0d955x/opjVOI0mA+HGb5fD1jCU546lssrMrSB9/x8rW7Yr/uRYhrWYvhOqhHVZA2qCJSTKd7aYoi7Rr+XVRZi+fen4wXP5+PYE5T1FStpeLgx+Q7jkc14wXjVfbtiA3PD2hEIxrRiP8hSAaSKZ5+2zOYOG4OfvrgDsspZTXL2pcA1WFwZKh2hWBtaSXufPgl5LXti+seeBEt23Rm/BjiZJAJk4fy2ZPw6j2n4+iDdsGS4nIce8VDmD9vMb548SbMWLYSB+x5IdC+F3QQTEZNLW4+/xBcds5BePTFD7B4yWpcc8EQ5OVk4YTrnsRLz39Bi7gN8nzVePXOE7HfoIH4+se5OGPoc1hZHbaPmH2pKPLDlXjshlMQIM8dvOMA+1aCJHrSHg0sDqzVWjF0P4Y+MALjJ0zG609fhyY5melVAvr/n1r+Epa/Bz33l/DLzc39XSdBra9tSfA7IajHB/oSmoO+HibLXp8GVTyl2ZjTV78kLFVPJ4B1rV/dS8A6oSxB6yA/1VvL+MpDdWrIqc76Ipme82t53kH7HvQtcgfXFgeVpbIV76677tpA8EtR0mqF4kixePfdd+132wXVecM+T2t46SuNh8qhS9Yij0NfEA6hWWYEzelaZUXQsu66RVaI90HeBxnuQ9NM0mNmANsX5eKZUwbhwkM2R01tLVDQCj/+tAoTVtdAXzH368MR+qBCIxrRiEb8jyJhd/VTdqxdS6Gb3uxt+aJORCUk+K1JpMedsvD9Mn+C9p1+iVVJAq10BlN+Wu60vOMVaNE0R0lBlokmVB4ilTEkorXYf8Dm+O7LF7DnVp3RoakPe+zQEfvvubWNO39NOR557XOUlst0Al685UwcOKgXdmrmw2OXHGEFv/Do66Mxb0UZApQJWlmIsTYJCvaK8jIUryq2cX4rabV6nL7Kz1Gd2Y66t9kc/k+Ev7OQneD0QkJTgs1BAuufgfIXJOC1oc7h8ccf/81z//rC1QuFOcEv55QB1wYnTBVHTgJXv0r3e85BnywdPnx43V26D9zuf6F+P7md/XpEoO+hC8pPj0jk99FHH1k/7QUQ9N3w0aNHr2uD6q3nOpZ+6+ovS16nP7mS2Dr7q+MtU2onXULXJgHDSSBFIUryqEUItSaMOLNTuLBL71YoKspEJEWizc7H5CnpVRwtmaVr1IhGNKIR/5vwO34rfrnuQDL5bciH01ug/BArDZAn20PRTAYN6gD8JoBA0mDN/EX4dfI7tL63Ju80aFqQh7eevR77H7cPVpaVY2V5OXr3bopPXrwMC0Y/gfeeuRE9u7bBqx+OxVujfkRVqBCHnHs/Js7Rq3jAu6/eiK8/vBfHHrkbVlVU44JbXsaoL35Gbl4ThIPpVVydpaLHEtrPFVfl5Meqe1tg7Np/nQwi30+y7utkf12kf/uyvxN8EpoS8hJ8evVO1xKsWio/4IADsGzZMhv/1ltvtcv25eykDZes1SDXlHS+Tih37tzZWvYScvrut74Brg1/DrLq9V1tryD3KhnefBWm743rG9/aB6BvjgvO0vcqKrLW9X1wWdtqkzdPoX59FS4B/eGHH9b5pnHooYdixIgR69rk0nl/v/rqK+y666723uG7776z3woX9A1zpxgIe+yxB1566SX7LXXlmSRhpqsuhcCPMTOX4sSnvsWiygjMmtV45x9746A+bUkeFNeMLzox2qWvJS+dcsV0em9fewZ0GpbPxElD0iYy8cbPS3HWsEmI1iZRs2QJnr1sX5yyczfEpIwEQ/Z7Co1oRCMa8b8IGfhaHT/x+kcwY+pKTBh5M9kfDR6yNz+Fpvil/eqPVgh4vaasEnc88BIymvfFLY+NQNN2LREk7w7GUqiJ12CrPi2RZ6LMNxsZJoZoOIRfV69A61bNsHTxCiyfPxv7774v2nYrZJwofv5hOib8uAT+gtbIyIygprwE0aoSDOrfHVv16IAg+fCCFWsxfso8LFtVjtxmlEnBsD15Vd8XqIwmUZhRjQevPhjxslIcesDezJc8mnLDLfsHaMSRgds3F2595l18/sUkvPnElWial82WJrWd8f/mmb8TahK++++//2+E37+K999/3+Yr4a8y9Nrg1VdfjZdfftkqA/8spDRoB/3JJ5+MSCSybvldZxJo090ll1xSF/OfgwZHgl/11OMA9ZHgHQK1Z82aNXazos5DcH4q/4wzzrD3Dtok+euvv9pw9cU111xjlSldK8tgUONAOqZ2K+F/wtPjsKQigtTq1Rh50944uO/6zYibhuqppS8fllVFceXL4/HK92sRzsxEbNkMzBp2Drrlhdlf+s68Xt9Mp2pEQ0iPdfovGU3d0BtPn9lL+js/TtK6i8aO3TRoeNRdpXtRd+k+s/3My3U+dRHdWRfC+nWxRvyV4YT/Rfe/gnffGYf5Yx6VL61jcjiSg10ZkAKgjVABH0pLy3D3Y68gp1UfXHPf62jerjmS0QRiUaoMIX2YJ45YSSUtRi37hxAP+qGvAQaifmRltkd1uBLx0pUiNroUIrlNkZ2dgyr+88VqaCzpQz+5KC+LIrl6OZAVRCgjj3EN8gtp5JK/x+K1QG0l42aykHwURWrw6I3HIiNRjh0HbG1Xa0W+2oQo6AwCu05AA/TyW17CtOmzMPyZ65GfGV4n/Dc0Xf8NkCDzCjM9924IsorrW85/FBKcgoSpBLQ2zj3yyCPWHXzwwdhss83sLnstk7dr1+53nbPu9Yz97LPPtjvrJZid/wUXXPAbwa8VAi3dt27d+jdOZcvpWnG083/w4MFWsdB7+qq/+kj19zrXH/fff78V/O5eKwB6Y0F7G+Qvp+sbbrjBhkvYC1r90KuLUgasYqEybAj7ikOipSAt/+t/HuOUVsXw1cwVmLigGJPnL8ckuh/mr8TEeSswYcFyfD9/BX5YQM14fjFGz16N576fhwue/QofTCaBhin4ly7DPtv1oOCPQJ8QshPGM/aNaAh6mkjlbJ30Yb/R5FC3aRRt99U5xZHaZedT3Xg2YuNQb+mf61td2TvXn3XOBluX7uF0Cnk04u8AxxNbt2qHlWvTK9BGotDo8S9vrEJYRw/WetLHehKUyXFkR4Ior6pATqbBgbv3QZdWhYgV16Jv3644+NCtECkMo6AohAN36Yt+XVoi5luDXp2ycODefVDYNBdFLdsjJxjhXI+jTats7D+4H3q0zUNl8VLKiQIMOWUQ8ltmoKBlGIN37Ia2RTlUKmoxsFcrHH3gAHTt3hwV1auRl63z+TORlaHdVmmI20tiWKkhXsy2xMlbSqM1yM3PR1ZEJ/xRAtTxkf/zZf/XXnvNnkrnnpXrVD1ZsXrfXzjwwAPtcvsftdj1VoAsYL1e58oRJPAE5TN9+nQsX77cliehujEoTHksXrzYviHgVg60j+Djjz/GjjvuaAWthLegw3VUXwlyCXcntOvD+bm+0Aa9Xr162U2AgoS14jjhrjYontqg/jr66KOtv8IVpsccHTt2tH2neIL6U9fff/+9XZlwcfU6pA46kkKkb0YbaoI+um9mLMVxT43DoioqTrT8Jzx4KKYtXIWrn/8WTdq0RjBWaeOl7NeipZjYYiiX2L/JFKIJg9WVSVTozZIA65Asxa7dm+Kx0/dEz5Y5qE3VIqT3/El6DfVJI+rAMbLChpPTfhNB54mLhHlv38zQ2qNOldLeC204sucnpJ/X6fCkdc8rG/EbpD89LeuNfSRa9tWyR3WvJU4qvuw7zQd9syLAvteZFEJSc1FO3LIRf3nYL5Fyjr1HY+ag489C+U/vIzeTBkyCMitgJxpjkbfrebo/gOqaWrz+zieoSmXh9mGjURyP4r6LhqBrURi5BW1x8ElD8c5rtyGbpv/w0eNRWrkGlx9/BCZMW4DL7n8Cr9xzDTbr0hpF/S9An55dEAnVYvbSlTjmsME4avBW5JsxnHHpnXjm4aGc/0mM/GQixk2bhbeeuAb3PfIORnw6Di/deQHrUY3bXvgSn339PY7aZ1uctt826NSmOTp3bGt5/4bySHLCh+LSCpx6wxPo1r4t7rn0aCTEbxims1glQP7tYEUMLXJDIVfnsyE6deqkmlk3fPjwOt8/h3g8bn9VhnMq91/BWWedta5eDz30kPU7++yz1/kdcMABtl3/LOrX1dXX5bl27VoTDodtWRzEdeX+UefSnHrqqTY/9hLLitmrr6cvMe0vft34znjP4OBnzZiVleby9yYYDPiHwVFvGRz2Ct2rBkfKvcxrumNHGJzxocEpb5jQacNN4Iy3TPDMkabbJcPNFa+NMXPLa2zehsOcSsTYnur0TSM2DnZPkup4KsE5kkyYeCpOV8txipIQoiaZqDXVJmWoTPG+kkMYMzHSSSzF+dTYt5tEkv2Z0vyKcX6xqxLxWv5Wmniygq7KpOJVJpGKmSj7spZzI56KMpxpOBb/IutoxP8n0DDHyauEWasqjK/XQeabydPtfTwRJx3QuZmma9JHksQxZsIv5r5n3jA7n3qfabf/ULOoImpuuuMhw1lrLr3vWbOstMxcePW9Zm7xajNq3A/m8RfeMT/MnGsGnXStufiBV5WbQZcTzR2vf28eGvWFyd7+NNN1vyvNfiffYFZUVZvbHhlm8zrg9AvMwpVlpvteF5mvps81tzz5ptn5pBvMquqYGTd1rul5wFCD3ieap96fYJ595Q3ykPVysCHMmr/U7HjMNeaJNz+193HGI9dh27QF8P8A0kJYjnW69kLv+VPY1d3BLrULLo3g0tZ3iiOnTXSydOXnII3HhcuxM+qefafTeuGNJyiesNtuu9lfQfUUFi1aZH8FnR3gltTVBm8+cq6s+s6FO7i6O+dWLYYMGWJXHtQW+estBq0y6Pn/wIED7a9zO+ywg3VandDKiff1x2effRbfjfuOV/p2XxqGCqEsnzo9AanaGLZs1xT9duqOnfoXYdA2LTFo2yLssG0hdt2yEAfs0Bxd2lI7XL4YkYKmCIQzkO+P4uw92uOTGw/EzYfthM65EcRox8apc2gnbBJhu2mmEZsAtXx/gDRvrXramskgtX3tk2Df0eLw+2nxywK1Fj/712/f2GWcKPweGmrEbyGLJqVDVmjVxbTqyT71mUxSaBb9M3mfgUAigGBKMRlX04FOKrMuG/F3gGiEo52KoV2zHBQ1aYrxv6SX/hlEWklfWDYmmUJLWatCTfKyEY9VYaueXVE8P4phr4xGny03g041KchohrWrVuPbH6cglJWLzJwcjPtlGkrKSsj3czFqzE/p/EIpvPL2KLz8xteIJfIwZ84q7LP39njqhbexYkUpJHFGTfjOPhaet6wGk6YsQCA7D1N/XYxdDjiGdanABeccisxUnPXohcryKgTIHxzfbwirSitRXlKBds0L7b2v7p+I/v9E+Eu4SaDpd/0yxHp4BaE22QneuPptyCmOnHuE4PXTtStTTtdyLq0XLo6coHh6++D666+394Lb9a/vCDiMHDnSbrBTOj3S8OYj58qq7+rHE9y1woUHHnjAvq4naDD1WEGPHN5++227wbG+e++996zTmwdffPEF7rzzznV9Keyy6y72N6ATqAgJEKoc9lpjHy+JYsgWnfHZ9Yfg3XMGYuR5O9LtjPfO3gUjzh+MV87aBeNv3AfnHbEVan+diWigGSpTTfDeZz9j3PT59rUT7Zzxm2rmSxWAgiuOAOu+YV834reQklTFSVulE8X8VPaoVKXpJEQlLWTPTAiLQbFHpQywV9nPfk5+KZiaP+lXUJ1y6xRP56df56SkKqx+uNdvY86b1hvfW4Y3zPm7eskJ+nX5edO5X8V3abxp68dxab1x3TWNs7SCm4zb89n1Bat4TO9l+9mXnGvxpH08pue3/oQPwSR5A8OYG8fA5bth2+RcH6wvZ33dvWEuvu698etfuzj108qg8cap7+TvoHvFV1qvc2kV5tK4Pq+ft4vvTSsovqvbXxZ2gdRvv02y28Ct8f6Y8Wl/UoP+KSwNqeEiJoNWRYXo0KE1DTIgMyOGX+cuxsDdtsOU5cW47Z5X0atbF7z4wr34etwkfP/jL3j2nmvQtkMXzFu6Cs3zm9hcABpPp+yJM48+AP6aFB66+VSce+SeGLhlN0ybuRwFjDHxoxH4+vuZpNE42hQW0DjwY5cdd8aZ556JHfr2wyTmfdqRe+PLUR/gyEMPtrk6OdMQfpy5wn7HoH+f7ryzZkVdXfTY6z88yrKodQyue9Xv1FNPxX777Wc/XCMilEBUlZyQdHB+TqjrXkTsGl6/8S4fQb/ecCd05WRpf/rpp1bIape9UFhYiC+//NIeLyx40+od/X322cda5O65u4MUApevF4rjFBF37+qktwp02qEOJ9Lrjg4qX68f/hnoJEB9qMhhj732wqcff2yvv5qR3u2/uJK6avFKvDd0XxzQT7v9ZbuniTxdOykLunInLmbjkje+xf2v/IBIm25kDFGkqtfivtMH4aJB3RhOpiXGJSuVFleQ3aFXVRrRMAwFDAkQCY6/RE+AgifOPh9fGsO4hZVYsqwEnZqF0K9NAQa2zqMiQFohnccp/LUVQOc2+ElLXmgeOHjnjaNNR2+K52jUwdHhn4UTGK48b7mCm8sqU7RfHxIw3jnhhcLk7/J0cVw765flkGR99GGpRFUlYpE4FqZW4Jdl02BiNWhf0A7tm/ZCRjwTTSO5zIyKV4BC0NRSH1DfyoZb3y6VWb/e8le42wjcEFT3+uESxq6trl+8bXBj4/pT8VybBYV5773XguuvhvrZ1dmV5y1X/g2lEVSmN+5fCUnDdpNO2EA8P+JLXDP0ESyYNoJWvES/eKE+gE7DidegMqn9I/qs75fjJ2PKvKW05GegZYvO6NK5CZ545Q2ULjfY68DtsPNWPfHI028iGPHhzCF7YeyEmfjwh6Xo2jYH5xxzOM6/7WEcsu+WyPMX4L1PvsNRh2+P/l3aIxIK4JYH3kbLPp1w3J69ccu9b6M0GsKhe2yOlRXVWLG8FCcO2Y1sewH+8Y/n8P2o5/DOq8/h1tuvSNO8hx4sLfFXfrWJFM645jGsXbEU7794O1uj17rTrwNqTfi/Lvz/F6Ed+Ycddti6yaEDeo455pi60P87aLJp0p133nl4+OGHNzmpHdzEVjydPaDTDXXkscMTTzyJM888w77qd+zT47G0gsJ9zSq8/o99cGQfCn9TScGSQesngBApXm8DaAIEfAlqn1FpO8wlgmfGTMfpt76DYPv+SIQzgWU/4byj+uG+ITshxLJTiRjzSsIXjLAuf02m8e+ArEu/Lw6fNriGMlBGa/+Br+bi1SlLsDYrD/5kc0Rq5yKTVsJefTrgsp16oH04hVpOWh/HOhxKCxYJFJ074Tajepm13gSRkqq3TQTRkeCEksL1nQm9FSPaEr1oM6lTDJS3Tq/Umypus66jQdGbPlGtTaxiHcpb4YIekem6/uerXVqdVKn42qwrpVcKv84BURw594lthXmhTb7awKvvaCj/H3/8cYNVLuWp8y2ktMdqE0gGY3jt57fw0PjnUV2ozbCk7WgQm+f3w7mDTkF3f1sUZjRJ03k43WcrmL9OUFPb6kP1Vv5NmjSx93pU6Y4nd68Eq19Uvt1oS+iLoTp9VN8P0bgo3I2R+kNtmjt3rl3hcyd2un7SGMgoUX+qL/VIT1AeguLpvJQ5c+bY8twqpSvTjYugo8QdXTioLxVH9LFixQp7KJraoXhuzLTJ2I3rXw060CyWSn+m/NdFqzHgoNPx2iM3Yd8daOxpBYnGixP+qaT4GnlwMIzpM+fhO1rl0VAWrr77VZRXZqBJu0KEyfNWlq+lHRVFYW4RYsE4KosXIyOnFSK5zehdjJqVNcjt1AJRxgtFA8gqDKC4rJSDxpL8MRS0botSnb1fvgxNCjsAWdkcy2XwmxCyMw1q165E7NfpePbt5zD/l7E46Yg90aWbPugjAyI9vm7sdOcjjaxcU4pBxwzFEfttj5svOAoJKjIB8p6kP4NtowJqU/0HoQqKiEXgYlD/C06Er1cSZdGLoUrwqxMF/Wr3vSaaJpJ2/DeUx7/DqW/0bF+CX+Wqj34PiqeJLqc9AnqTQow7WMdIrrnmKvw6ZaplGMHqUviqi4HqEgTsh/2JRMgeU+lPsiwKfF9Ay9C0NFNals5FjBaniZfjtEG98OULZ6MgNRP+iukIsqxHHvkSgy55Bgtqa8lww8wjRG3y9+v8d4aP5nucejcyMrDIBHH1ZwvwyrRK7NS/B47vk4ch21TjpB2a4oKT9sB7M0tw7afjsaA6hojmS53g1/kOohcp0WLYWrXS7y233GJpRgJHQkVCQ0JGQt0JlpNOOsmG63GWPn0tgau9LPrehuhbglnXOkZawkmrX3pTRWFyutaR04JoTgJC53ioXAln96rr5MmTbZ2c4NGqlNJK4dD3KgTtTVF5yldCXx/vUpkOyldvyEjQK54MBglFfQjM1UdO9dVjM6HaH8d9P7yF6955Ept12wzNTFNEUnlI5mdh7NoJuHL4jVgWX41EqtYK/i+++MrWvRXrLUGqz25LMRL0iq3CVG8Jdr3po/n2+eef2w+GyV9C0vWLXgkW/1BcCXR97Ev9rseF+lVf6VcHeKlNau8//vEPq2xpjNSfyl/7eMST1GbF09tNemShtHIS/BpbpdfKqcOZZ55py1Q61UfhMggEKSz6BLjaozHSIWHChRdeaMdY/a90+hVdeQ2IvxTIY/3kUYEQ+RRZfMfmedhxwFZ4ccSndRHouW4hTSsukv1p5bRXj85oTjoqzAnhuCHbURmkskQaSjBSXsssNClqhtxACllU6Avb90BudgYyEzXIz4qgbYdMZCSrURDOR8vsJlb4NmvaFkVdu6Bl187IDkpxyELztn2QR93Xn6hEi4Jsuqa0EZqRPgqx1WH7o3OvJsjLT1nBH4+pflo/TP/TThYtuhrSiPDFN9OxdG0Jjh2yv71nrvY/LX7bxv+45S/Br2fWIkYxMN07BtEQVD2F16+miNhp0g01QeGCwty1g/NzeWgiOeL/PYhxiUGJCW2s7D8Cb/1cffT64xFHHLHOwvhnoeN/y8jAguzW6mgcm5PRd+zeE1/9Mh0VcYPKWBz7bdkb7QvWvyP6W2gGrB8XtVI1nrO6FGNnLbTLYHqWunbZUmzduwe269EeKSoUfvtB7A37uxHroSNA7QmKgRDemluMW8YtQmZeExy9VSF+nlWJcvZpIYqxecf2eOLbJYgmy/GP7Trh6J4tbf9fdNFF9hwKnRmhjZ4SSGLeejVVR1uLaesETZ0gqaOgzznnHPsJaWHChAk47rjjrGCZNm2aXQGQwNarobIYZUG++OKLeP75521eEhYvvfTSOiv7hRdesHtMdLKkFGIJKwktWcbPPPOMpV2tmt1zzz3rVq8EnYopQTRo0CBrtU+cONEKTIXrdE61RQqJLFAdgqUNT6q/NsDq0ZcUFsWXwJXw08fAVFfNG13rI1mql065XBZbgd43HoDd+gzEm8c+hBM+vgkfzf0aRTSek4Ekli5M4Jadz8Kp/XdHfnYLO+/aUll5ke3U4z99KVNKySmnnGIfxYknSCm44oor8NNPP9m2y0j47LPP7MqFzhKR0qX+V//qXsdtS7kSn9B4qW3aKySoz/SdD/WzIOF74403Wl7ieKH6Xgq8+MBNN91k+0t5q/2C6qZxEd9SXdzR4ep/vfqrsXDP/fVIVbxWafQBMI2LNjZLgVS9NB5a3dHKhn41HspTY+1eTf5LgTSj0/CSfoNQikIyGMBTb4/BHY+9iY+evw492pGuaP3XcrKFDflZKol4MGRP/tNqaDSexNOvfMJxbomn3vgMn/+0Ek2L2pH3VZBdio9TGFO5sJsKCctBOQ4+lqMnfvK1nwpm3KSMLl7qvH492ov4QjCJuA1LaT9QknGYrrKiFp1oXD1x24l49c3n8eR9N9p2aFk/pGeBVHgl+gMJGgesczQUgL5YMPjoa9C0eTO8+eAlrAMNRNaLE8i+whhg/f7jwl9EqeUpTWLBFe8Vhl44/4bQUNUVX0TvTbexPNxkUz6alBLom4LiSnt3y6ebqtvGsKnuVn6yxFSXTWFj7Za/frU6obqqetokloyT2OMxZLDuIjbtXq2mXy2J3LZAf7xZ6toGuAspSqLhFMIkxiw65at3YBP0y8jKRn5Brl369wUyGHe90tCIDUHbjnM7iUp26N0TV+DlKWvRqU0G+hXE8NG3tSijQGqSmoJD9twG706Ko6KkAmf1COP8Hdsih/1e1KwZitesWUcDEtASJGLsOtLaQZaihKgY+ahRo6ygknCQINfBVhLQ2lBaX+FVnEmTJlnrVMdeO8iClPCRkvzGG29Y4SQ6FZ3Jae5IyGuvytChQ+1BVaqb4kg50SFVEjYSrhKisi4vvfRS+zluCdmtt97anlGhOksR0ZsssnAljCSYtDK3884727IcdD6H4ulRl4SV6H7knC9x+ONnY98t9sG7Jz6I0964Aa/M/AiFWX7Ukv7LokEcmb0lnjpjKDJD+WS4fhx2yMF46+30Zl5Z/hKYqr/ykwDXMrkUKAlHKQZSuryQcqD+UH8KarOupRRJ0CtPKWKClBQpZHJSEg455BCrPAjiR+KLmsNSzLTsL8VI5b/55pu2/6ZOnWpXTzTm6j/tPdLZIILqpceqOuVTY+eUBSkssvR1jLqUK0HKmPLzQqsnqq9WklSvvyr0nn/KT9qlQPdTCZ+9YDn2PnkozhqyBy4/60j7WCBpAgjK4pdNrbdyAtp4W0tWmINZC5bgq8++RV7bLrj3+VGYsnAx8vNbUYjnUPaSd/sowJG0K6j+ZAABE0LCJ9ucYJ7iqGkqTvNW1oi8NYkQ+Wkq6UOCNGDPAGHpldUlKMqqxS1nDMHPY7/FOeceg86dO8BQCfFJaaCSQGZgc0tqEyvL1imrv85Zhj4DjsKnHz2DPbbpznyjLCXIOurtIrZd+7Nsqv8gJNx++OEHS+CayCJ4J+zE0ET8TrjqXgJHcZxwqw+tHjih5/1V3i6tE/KCy8OVr/Lk5/VXfKWtD2/+gsrWvUsjxcYLL6NyCona5tonuHTevL119ZYnKMw5QX3nmLA3XjofKUJJ+whA3/PXsyFtdE0yfi3DtUNfpOeF7pU2nZPKZzlGy2Qqk8oDiS1EwtY5/nH6V1ZVo0vXLtiu/9bML8UJUj/HRnihIdKYRmtTWFWZQjwZAmIJdG7WBDv3TaKCfdw81Bw5GT7EfdXgXEWpiaOKEzaHvasl709Hj8bNN99sGbgsQtGQ3clu80/TjN7+ELQKIOgAK30bQodZyWoU9EVKQbQp+pEAleV+7733WuHh/AVZ2BJGeiNGgs7Rs5wgS93ti9l7771xwgkn2GsJbx26JcEqP9G+e4YtQS9lREJMyoNTVGQcKI3KUZ1kzSpcFq4eH4jelY8EshQACWrlKb6brOWcDDbB+FnT8NIvH2Orjr3x2ezv0Br5nAMZmBibi7XBNbT8yATraHUyrd+PWe735EsrV660j0rUp4JO/HRv4ehxhwSs+ljtF+/QSopWLLRa4qCVCCk+UgCkGEgZEqTUyNLX0eQSxDqRVH3g+tAZRIp3/vnn20+BCxovJ6ilmKh/lV79qfSC+kerDuorCXjlpb7XqoJ7zCKlTis+eiQj5UtKmFaBBI2PTgjVSoteJf7Lg3yN7EsyGl06tsIxB+6C1z+ZgIMP2RPdWhTAT6taT0JlwQd4LUO+lsL159kL0JPxB2zZB9+M/wXXXnwUHnj6ZYyZvIpKah7zpPUuMU9eaJgBZ6NEu533aR6dpivtK6DsFovlf/JqlcVrbcYzsuZNDGWlJWjLPM89el8sWTYf+x24K1pR8D/35oc4+Yj9qKBUsxjJEvJlpk/QabqKii645QVst+sOVvBTU2AtKEMZj1EI/hUfIiGrCv8xqDhNFk1edYYIX7+aTAqTQJWCIOLVBFS4rhXuhWM8YgRu0rimyF/XCnObVuo3U+VrwiitwuQco9Nyl/LQcpnyqA/F9dbPMQrl6eopfy8UJkYnQa+3CvSrPPScXm0Ww/EqARuDGKK+ZaBJq7j6VTpXrkuf/pVwFwFqiYnhpBD2cnqpSJSm/4wnbKxM+efn5jNeCitXrUCUfQYqAHn0K8gvQDUtylTSIDsjwuzTE0pKRyMahpQxPaWrSgTxj3FL8crsGnQp8OGKbQuQVZCFZWtq0Jr8fHEqhDvHrLIfDjmmSxBXDOiM/FDEWqKyzrVcK0Yt61J0JMG400472TKcpadn1noNVM+hZcnJmpSg1RK1FAEtBYvhSwnQnJQgkzIh613L1qJr5a3HUaI7CT6l1bWjedGH5o0EnYSk8pUA15K+hIic3mLRowqtNsycOdN+g0LLz1qN0IYz5SXrX3mrHSpbwklHdUtASkideOKJVsCrDSpTberSpYtd6tdjRDsfapKYXb0IWz54BLLIwLtX5uHSw86FL5yHLsEWWB5fhBNevgJHb74vrt3lXBTlNsGtt9+KB+55ABlZmcyvM8aMGWM/kS3rWfNSz+ulYEgp0kqE2uDewtG95rSeoWsDoJt72sSofLQSIutejzukfOnRgSx5jYUet2h/jhQ4tV1L8U6h0HzWoxgJZK2yKE/3WEfKl8ZW/aNrPeOXv+okwS7eKUVQSpJWDLQ/QeMhhU7KkmhH9Rg2bJhVrJySpnC1+8knn7R7DFxb/nqgaNbpmbR7fTrtzvCX/Ey7+I85704cedCuuP7MQyiFq5GgIk6xj7B4ZSCIL3+ch3OufQT77NAH91x9MulyBkaPm4w2Hbvjkx8W4uXhn6BJURNk5BTat3PYgTSIaEhql09Ksk7v5GveUHGWrLGsl3/Yzextu8wfCGr8q1GyegkGb9cX++y8JWLFK7DtFl2x+6BtsespV6BZOANvPnGTEtv0esyQZFtk2YWofEyetRj9+x6MidM/wtZdmpP3R8mbyf8N54hVDciDNLYc5P8oSJCGhG4GDx5sfwcOHGjINOpCjTn88MMNJ4vZfffdzQ477GA4WepCfoupU6ea/v3727hkdGbAgAFm6623Nttss431p5VkSNR1sTcEhbrhhLF1kNtxxx1tXVQ2NXNDrdmUlpbWxaboa+AIMMVRHmRw5qabbqrzbRg6ve/cc881e+65p9l2223N5ptvbuuosjnpDC0OG6+hcrwgw7B1VZvJ7M0//vGPupDfIqWTnAgyTHPCSSeYnQftYrZm2V9/M9b6bxKealx99dWG1gb7tr8hszF9+vSxZR980IHmu3Hf2jjJeI2Jx6p0Ze8b0TBSyZSJJWMmwX9P/LTadHxmthnw7gLz5vTZZtzaYvPSxFlmzNylZti8tabnsAWm7dNTzV3fL2QajqXnJEhab4aM2p7qKHqlILJhtPit39FHH73uFMwffvjBbLHFFqZdu3amb9++hkJUbMeeJklr3sZ59913rZ9o0UFzRFCeCnOnXlJYr3MChY39FSiwbFxan2bEiBGW1lUuhdy60ytVFyoLdSnSoLC1YRRohoqIvXYnVVJ5ML179zZU5NeV6U7epFVs79WviWiM1Fdtjhl9oYk8v5Xp+vxOZpdhB5prv7jbDJ/+sbn4s7tN3/v2NZ8t+cZUl5bbdMLcOXPN119/bahgbJAnjQP7K1BJsmGXX355nY8xF198sfWjYmDvVbf687dXr142jiDe1qJFC0PBbWilW385KhR2rFxabx6XXHKJjTN+/HjbZioapnv37qZbt27r0l977bV1sddD81NhL7744rrxoyJow1xbqFjZ+/nz55vc3FzLy2hYWL/f40P/vyLFfwmdpaexIn3rRL8kaUe45ZE3TO89zjS/zF3Bu7hJJKtNbTI9ByZOX2z6HXyj8fU9wxx49r1m6Yo11n/BwiXs32fM7U8MNw+89aXpu9+lJrPvmaZghxtNsz3uMkX73Wny9rvRNN37JlO4+02m+d63mmZ73WyK9rrFNN2Tceha7EO/vW8xzXYbasJbnmWa7nyOOf/et8zDL39sbrlnmBn33S+2rAdefttsf9z1piwWN59NmG1e+fA7E68bphp7amF6brTb6Syz14lD7bVO8kulOC/sKaKasxpb8QVqGukY/zlIEDmidU6Tjhqrqaio+E0Ytdy6lOnJ5cW33367Lp6I95xzzjE33HCDFcpXX3W1ZWRvv/12XewNofK85UhpGDJkiGnVqtU6v/bt262bBJqc6c5bXwcd9+viHnnkkDrfNBTPMd9ly5ZZgeniSuhfeOGFVvA7v2AwaGhh2fheKB/HhPVbVFS0Lo2c7qndrwv3TlodYipMmTLFdO7ccV2ad8iULerNb93K2brXMT71pUsnhj537jzz8AMPmxbNW1q/pk2bmWhcoixlauIisg3HqBEbQkNZzcmY4th8v7rG7PXaFLPF8J/NK8Xl5vPapHlrcYX5qLrGPL2s0nR7arIZ8Mov5uMllWJFTJMeMFrohhacycvLM7Q81zH1m2++ed1YKfywww4zV155paUrWq9mzpw5pri42BxzzDE2jgSKIGVCCmV+fr6hxWr9HJ2LhhW3efPm6+jQhbnf0047zdBat8djS0grvgRjVVWVVS7mzp1rBTqtdJOZmWkmTJiQVkhPOMEceOCBtj5OIVmzZo1VJvbaay97rzm933772WtayLY8Kcq6V5kOiURcJyGb2kTSnP/69abPKweZ7i8PNl1e3Mr0fH6A6fnILmbAo0eYR6eNMCvLl5MBpOn7sy++pKJ0jG2/8lSfqb8qKyutoqR+VD2zsrIMrXJDS9ymmzFjhmnZsqUV5l68+eabZrPNNjMnnXSS2XnnnW2eykdYsWKFHYMlS5aY1157zY7fRRddZEpKStbNWxlHSq9yxY8ikYjJyMgwv/zyi42nvly4cKEZO3aszVuKn/Ddd9+ZffbZx/LLgw8+2Ia1b9/ejpn4g+qufKVkOcVDSpWgftW9lBBBdXFj/VeDetnOJQlLSk4dd1uTUlsTZnVZldnx0IvMkRfcRp62XpjOXLrGdN/9VLP5oTeZ/S5/0VxBJUF49bNvTHFtwpQUrzXPvjbCXH7zg2b4x+PNnc99YvoecK3x9TnF+LekIrDrJaZoj+tMuwPY9/v+w7Tf/ybTngpBm72vM812v8pkDTzfoNdxpnDAmeaSe0eYFz/93gx9YJh57Pk3zPyFy1iLNG302vcMs9PpD5r9LnnQoN3Bpu2O55hPxv5kwxKx9Hidc8PjDNvfrKljwwm2Uwf6SsGRS+f0XxL+YkJiACJqWf8iUDEdCX7HvGR9a8Loev059b+FmIjiyHXu3Nm8/PLLdlJoJWH0Z6PNhx98yMbX9UI9iHFpQittfn6B+fDDD62/Jp/q5fJVnQSnmTstUdDkdPFOOcXVc315bgKJwbl4uw3e3foprzjD3x7xtnn22ec4Qb8wFVUV68Ic0oOWzvPSSy9dl8+nn3667lrMycXZoPy07mGmT5/B/uy3Lv4HdW21UVVF/qrE9BRQ+SSXuE6a3rDMNq1am3POO8/cfe895vU3X6PFNNu2QUjorHrNl/VVb0QD0PnzSQp5zT/13Gtz1ph+T00y2z33ndl/+Cyz+1uzzU7vzTTbPf+D6ffYN+aRGctMpR2n9da1xkJz6JRTTrFCVJBQ18qVwvx+9zQR1spUmBdnnnmmDZMgEkaOHGnvJbydwupo142/mweCwkSjjk6vuuoqWx/Fa9q0qV2NkvCsD61WKY4E2NKlSzeYF1o900qegxRmZ4mrPcpTCrugVQ35y4IVbF1ZlzIqDWsTVebryR+bCcVTzciFn5mbP7/ZXDjmGnPv5AfNuOVfmqXlq0w8mjAxywCNueGaa2xeXbt1NY8//oSpqUl/r0IKhrd+UmocjxAktOUvniO4/lKcTp5vl0iIN2RNO8NFRoDgwqQsuXGU08qJViXqQ4qEwrUiJ0j4a0XFpRNvcquJwscff2xpQWFt2rSxK0eC6ibFQMbJvHnzrJ9owLXnrwb1skY+ZUqpANI8In+tSEXJx9L0+u6YScbfbg/zygfp/hGOPPsfBl32Ns9+NdUMufIJ884PU8xdL71v0O1o02G74+tiGbNw0TIz9NanzFnX3GHe+OoH896k+ea8+982HXe9yqDrKQaFVGJb7G/QkkZjS163P9Rkbnee2e+KZ8ywz2eY0b8sNmdfdZe5+MY7zVcTfqjLlahj6Q8MJ89vsa/Z98J7zPs/zzId977KPDnis3QgMeHXuQaBPuaVj9NpU/pWAcdRbDnGPKzosmSmHvgvCH9pvY5RSAuV1aBrTSJHnBMnTrRL/rr2Cv/1Ai6NCeN/sHE25eYtmG3jSvvRp1EcScfjtRT+GTZOXl6Oee+99+pCjGWqLv2gQYOtXypVS6fy1ZXpXIYMOXJdvJNOPs76JevKWNfPRMeO663uhx9Ja9eK2RBisbSSoTxsPnXLTj/99AutjOY2j5122tH6XXbZFel8/TAvvZJmQuIhiVQNldo1TJ9eCv51+jQK//UrD6M++tj6e2HrW8fU08w9Xa7wzjvvmK1oZWk52eXhnFYxHGIx9ZFrdSMagnTRVA0FZ4xjQ6YjfL+iylz0zg9mu6dHmy0fGme2fOxbc+qb08xXiyostZlkFQlWH6VZPyZe/Kt9vqn0mwpzSsDGoDC5PyNINMfrz/P68ObnyhAWLFlsFi1fYirKSPuxmKmqqjZV1RWmPFpp1laUUHmgpcu4yTjLYF/GPQqVF79Xfv1wV4dNpdMjBBfP1deL3+tLQflvLP3GsKm4DdVX8VWXPzNm/79BTUtx7PVBpyRdLBXn+ERNqk7xveUZCvbIduYXCnNhTUXUjP7uRzP2h1lmi6NvMd0PuN70OeIR02b/f5jtT7rRxjn5+gdNRSzd12vLKszltz9k9j3pTHPz/cPNqIlTzbezl5tpS4z5ZkbCfDGj3IyfnzLfzisxo3781dzz4tvmiFOvNMcce72JVq9Xmq9/4GVz7Dm3m5LSNB/34o1PZ5j2O5xs1tSkaXjByhKT2+tAc/pNw+w9G0YDro6fk9a945oe24R2Jfz3oI11tAbsZh1tNtLmJb3So9d+tFmlPtyGPIekr7ruCujffyssWbJIyswGrlOHrgxNaNtm+tUOuI1x2pyX3tCiT9lqc48Qi9Vg9epV9lrYZtst7C8tcBs/ncZtMFxfH2N3Xaah9zj1ugg72d5TQNpfYcznE+uu0mmvv/E6DBq0E24YeiOWLVmIUEinrGlTmNIm7Pv0wrXXXouVK9P1isUSuPDCi/Hjj5PtvZr0yEP3Y/6CudpjApOMsMIFzKPupDSTbqdDMJCuKye/dbae7CudCkUiqeu7dBrtONbO7+GvvWY3EilM7zjrtSxBO4jdASuNr/j9AfhSSITjiAf9qDUc21qDbVpk4f6D+uO103bHW+dvh9Fnb49nDu+NQe1yrIZV7ctCLbKYdP1uYUFjofFyY7YxKHxjUNimNnZ5wxrKp6GynZ83/sbSeqE0Kk/+oktvuMLcfNKmOMFbrq5bN2+BNkWtkJNXyDghZGXpe+c5yA1no0lOAflMrt346mffqy/9dgPW+jxcufJzzgvnt7E+kX/9NEL9NIKLp1+VKd6mvBpK7/xd3whK49BQOt07P/164wsuv/rplFd9PvtXg18M2k9+72cfsKnBlA5P0jtMvGF3XHvq/jj83EOw+XanYllJDIU5Eew+YAtMmrMAP335Hbq0ycPdVx6JaPFc3HX9yfjy5/l4/s7haL7DYbjzkbeQEcrAXVedjw+ffwJD9u2P6WN/xGcjPsSwxx7Eey89iI9eehJvP34PvnrtJcz46lsM6NIGbzxzB155+SasSsSwpKLS1rNt+5b4YPwvmDhlpr13OPXqRzDk5PPxwcu3ozAjjPmrK3HAKTegTZtWeOxqHd5Eni6aCqbnieSDyE/j6pyVYRz8/yi8z/yHDh1qnzdq85vz0zNKoVmzZvb++OPXL6uQgOuu0hg79rN16bIys+2zu/POO9ecddbp5uyzzzAnnHiseeihx9KRmZS6Dy/SmpJ7lunc4MG72rQ9enRZ59esqJmpiWoTm+KnLQWvRXzQQelna3JDjkxb/lodSG8iiVHzsnabWb16zbpVDbl99jjE3Hf3g+bwww5f5ye3cEF6GZZMTqop65tu7513pDfsyDVp0sT06t3DtGzZwnTs3IG/rdeF3XZremlW3RSv1ZKPvTVTp0wzXbuu3yDUu3dfs+uug+0zST3r1JLrc889Z+OqbPWN62s3DnLaRKTNY998843dVOn8teQoKC2Zh71uRMNImqjRJ2Zrktr4J4+EqWF/z1xdYd6esdI8OmmBGTF1sflp6RpTquVX2f7JSvZr9W/o39vXCtO9pZ3/EOqXp3vn6tfVC4W7xwsNwbVFTnDXm/Jz5enXOj26qqPH+k5w9Xb3ggv/vT508Zz7I/ij8bxw/fBH8Gfq4oXrt382/V8NiYRW49K02e/g60yTPsebsoq05T1ryQrz9MjPTXUsbs675ila3udY/+0Ov970POZ2M3ZlqTn27BvM8HfG0pr/1Dzy5ldm5vzlNs7GsHzVGuafljFjf6FsLNrK7H92em/L4lVlZrMDLjYX3v2ivV9ZXGLe++Znc9mdT5vJs5ek/UqrzK5HDTWddjrXXgu18SoTk6yqWzXeGAI36kXQ/yD0esy4cePs8ZcUOvYVFb3uo9eBZPXrEA1Z4XrdSL96TUaOdbXpvRr02lWV+OrrMejatQvzK7SrBYsWLaRbgvnzF2Lp0hUoatoMe+29J9Pr9Q455aHX/Grx+WdfolXrltSY2tny9RpVJJKFzfpujkMPPRxvvfU2crKzdWgStSWVW0tNUS/P6dqHn3+ZjOqaqC17wMDtsdtuu9JfJegVQGreVLkSScN25eDss8/EggXzkZOTiRWrF+PrsWOwanUxOnXsgH322dceZdq2bXtwvOo0cmloPqwpXoO33x6JKK3ugoImGDbsOdx77/32tZyLLrwI2++wI36a/BNatmqJlSuLseOOO6CwMJtWfIx5BG19KyvL7WtAGRkRtKMVTzsAerVQrwXJkRHbE9Q0HupnZ2HIwtJrQe6da70DrcNK9NqTXqHU4Sp6dUmrN2QiNq20Su8YNWJDGK0+cVxCRq+w+rAilsCwX5bglSmlmFzsw7wKYOrqDHyzuBLzy8vQtiATTTN0SIi0dVIe07m54P114+Z+BXf/R/Fn4gqKLxrRnNYrfh07dlxnnf+e9ah0Oq5Xc1+vGiq+8hPdqd7OabXJnQbq8pS/4rk0Dl4/O0sZz6F+XN2rrvpV/fUKok7U0yu53nTeNA5ePxe3oXjefLzXfxRK80fTufL/bDku/j+b/i8F8nm9midepq+hnn7UYLz5+bcYeu+L2GmHrbBF13bYqmcnLFtbhYuuvgcXXjwE3Xp1w0WXPISW7ZohM5XAHdecgsXFpbj6jhcwf+5KPPnSx1iWjGKP/r3x3phpGPnBd9hx2574dtoSxhmO6+59yh4itPM2ffDyR+Mxfa0f0Sry/VXlOGDw5piztATvjvkF/sICnHf1XVi5cDGeuv1StCrMw6wFK3DSJbdh6Zq1eO3Zm9CtZT5Fj97pzyCP1zHtGlPRc7p59fFfec9fglaTXwLEvacuIaSJ6A4AcQdtKE5mZqa9VriXOE0ihapoNfPyIxQMcUYHULK2DFRnUdC0CdawU6ora9Gla1v7rqNtKhmvPvShfKoqdIiKlj+Yp09Mh1nQRTJ0OKJDLaqrmI7CPDPTz3w0Qci8/EHEE+xoXldXRy1zyohksJ46d0DL6mJUKcYnI2IiP9shJJMxLFy8BKUl5WjevAVat2nJmMyEiCViCLilc5Yn4R+tUd/E2U8h20eFhU2sepH+kz5noKamlvkCNbXVyM6JIMTyK9nuJgV59kAfrW3V1LCfgn7Wk/W25wmwHLWbENPUYSEaE/WRc97+FqPUu+AaOzFIKW9SCATFVR76VR5/awbyO5Ay6EuJuVB55dC8PGUJvltZDV84FxlU/OIhjmkqG1F9XTFeii0z4zhpm45olZv+9oODxkZwfa9+d3BC0EFxvGNimZsn/qaguEL9/AVXhhRLfR9A5ej9eJ0vIEXSi/p1EHTGgNLq8BzRk4Nr09dff22PK9ZZGFLsBwwYYN+Lb6h9wsboriHeIaiu+i6A3pXXoTw6Y0CPuBRfcb1l1O+z+n1QH79Xp383/tXy/tP1/V+EIa9OsR/0TydGBUNhlHHcDznnFkyYNA/P3HIOjt57AKqq47jnhTdx8kkH4YprXsCIz6bii7euxdvPvYmdd98avyxZha++nYwRj9yICVOWYd89TsS0Oe/h5Vc+x4h3x+DTD27HUWfchuaZWTjy8N2wdZ9O6NG2Gc69+yXy+BRatWmPu+5/HmNH3koj1eCg02+DXpI98aD+uOPK021dx06ehStuegyVvgSevetKbNOtHWJJfcQqjEDdd1rsUwyjz9w3PKYNU+7/MaTp63AMCX5NIgl4Wf1O8IsQda09ARL8jjDdryArPhWotflkshMXLFqJRx55HW+PHIthr4zC/MUr8P2kmXj+ufdt/IA/g50Qss+1HSPIyctGVlYEWdl0WZm2rA0FvxChpfs1Pv1IpxJGEAxEmJeepVPIGVpoX85AXm4BBX8WBWQVGUQYFZVxjJ/wKxYuKmZcClS/tMn06X+PPfoBPh41FYuW1OCtt7/AR9T27ME5RDjIgaPgto51TZqUrVOTJnm2nhL8goZSczQ9T6WUZNoVhSIy0KxIDpasqMbtt71lYwaoFIlp5eRQuGRkY9Kkhcybbc3Isunk1NeK4xial/GJ6anfJfD1hTEd76rDVZzg96b5owLlbw2OWTLAPgsYTFpWgYnLo+jUJBfH9M7Fab0ycXz3PJzSI4ATegXQt1Umfl1Tg++WFCOhZ/3sazc2Eoo6ctf1u6xXHbSjg3MUrl/toZGiJ8jK1gl9guLrGFjBfVVP9/qgjaxs5y9lU3HduCp/laP85XRIllaD9Ouga6cwKF8dxqO8VE+l1ceEpEAKsupdfQXVV6f7Ka4OJ9KRtlI6daCRDtq58sorbZsVX6fgyU95uBUsxZWSoDwFla1r1V918vaBoMOLHP2qTipXcH2svlOdVKbrAx3mo0O2XB+o/SpXZQkqT3VQHi6//wT+1fL+0/X9nwSbL2NZh5UlQzrCvAr55MXvPDoUh+6/Iy668QlcddeL9iPoN5x9DNpnUn7kBhHOCGDaL5Nx0RUnYNcdB2LSTwvRqWlL5If82Geb9gD59+jPfkQqNxPtu3XE19/9RJoqw/03no5j99rGCn5h7cpKfD/mBywiXWtF+sTz7uIcSuL6cw7GsFvPWSf4737hQxx/4e0obF6Al1g3CX7DeefzaYUwAaOPs+m7BdqHZvdoN4x6y/6K+H9LAF5Bbq/rihPhubD6cDXakECppfGvNustX7YGt97+HCLhAPbYYwCKigrQvl0Rxo2fhZFvTEJGtg8zf52P7j06IkRL/+uvp1Kgf475C5Zi8826YcmyFXhrxBeYOGEOGUQ5B6YSw1/9GNHaODp3bo0P3tfxmAH037oLPh09Ee99OIqKSx4++3QGbr7hRTLmavTr34XC24eqqhRGjPwK48ZNJYNexnro3G8qC/5atiOCyy97Br37dsTgXbdASWklnnzqU3uyWEnpGoz/bgpGjRqHdu1aUIkZja226INvx/6MOXNXolOnVlixYg2eefpdTJ26GN9PnIou3TpZbfX14V8w7VSMm/Aj2rZrhQA11sefGIkTjtsDE8bPxBuvfYpovAq/TFmMG69/CXMXLcVWW3al4E8rX95+d/3r+lph+lU8F+YYpoOYoIvv4jRiI2BX+/knxn79fEElZpfGsV/3ArRFGfp3aINu+VnoWECFt7aYJOfHsmoKtlgU3VsUIDdM5ZX9e/XVV1vBqPP3ZTFLedb58Dq1TefgaxVKp/dpKV5fmJNg11GvOldep0vqpD4d/6upL2Esq12n6Wkjp74RoONpdbKdTuXTkb3KX1+M04dvdDywHhFJGVQZss5lMWtVSJtSda/T66SU65x41XPs2LH2yFlZ1yrjvffes3lIGEuQ6iuaerykI4J1qp0eBerIXimZEu464W/HHXe0S/P6LoEeFaheWjXQ6Xb5BflYsXwFLr/8cnvK4EsvvWTjqj+0YXW33XazR9qqPQqT0NcjK7XRfbHv/Q/exzFHH7PudE+dzqc2q+6qk04SVJ/pVD6dfS9lQ0ct6wNHOkFPJ/Gpb1SeypWyoG8aNOL/H2jcJVV0Jr9Wa300wOLxBLJCQey3S380zc7Ds298jmGffobObVuhW7uW2G/X/ujSsRmGv/s5Phg3AVv17Y7HXxyNvQfvgB237IzlNQnc+49ncMY5QzCFynN5RYIyoRCTfl6Kkw/fFXlZ6Y3mwk1Pfo7srBCuO+8g7LpVDywhnx6wdS8M2WM7dO3UAnNXrcChp95Do/FbnHAM58j1Z6NjYT5S+hgQ2S7ZMOtPQ1MbxtkWqqfSZBjUME9mqBg5BYB1DQtfQSEbc78LTyQnHNb9pjasnBM2zimp7AhbFu/TtdUNNZt4+nTuCeOnIhnL4GQ9lsK8M3baYXM0k5VMwVhZBbTu0AoffDwOI9+nBf/5JAx/8xvssPPOeO21sRj20sdkBrV44tGPkU3NbPhr3+ClV0ejS6/2ZFaPI648aopRUlZOJjQb11z/NLr37I1u1LaaNMtAnMrHNgM2tysKUhAyMoJYsbIC475bgKbNmsNPJqmzl13f5uQE0KFjIdq2aYYhh++O7p3a0bpYhrHfLsM1Q5/GgB23wIIla/DEkyNt/C/HzsCYMdPJBIFjTr8Drdu0QLfuHXHRtcOwbGkVnn32QxLSPHTv3RVPPfkp5s7T0cnAwiWLUV2TwtFH3YwevfUp1R5oyj4JRfzYfItuZHJaZVk/MN4+90KCXWOiX0HhTti7sfqjsGO4Efcv4V/O4LeoXz+vs1h38eegz2lK+FdRsSyNkrLJZHo0yUaAl2+M/cXG+eznmShZVYItmuZTXQygvDJAZYB9zX/6uIw+jqOz33VuvL76puN8JYAuvuhie/a7hJqEkI5qlaCXgJPVLIGr43Jl4cua1h4XKRKyXiUcdY69xlRhEsay9CWoPxz1of3VufQSqhLoEoSMbP30ZUFZ9YIUB1nXKkerAFI6FEe46qqrrNCWlSwhrEdNUiy02qBHADqS1ln3Sxanj8uVkiGBL6dVJ82jAw86EGdSkTj5lJNRvLYY8+bOwzJa21OmTMW555xrBbPO2pdCpLoobx19LGVEAtl9SEfKkRQIlWHH0zOmJ5988rojdKXkaN/Lc889Z49HPvzww61yotUH9bMUCbVN/a3vE6ifpSSoXwWXdX3XoOc6pz+O29VzCtsg7r/L6U8D5VnHsA3i/puc/dNQeXIM88b9d7kGy3KOFEbjJmDjks9ReCZJxzoY9/SjB+PVx65Ezw7tcfTx1+LECx7E/KVlOGbPbfHKvVfitrNP4DytRYahPHnpAxx+8QPYcpfj0Kx3exyyY08snbsUuVQkOnduSR4/A3c+/h5+mLMGDzz/DuavrcSUCWOoNPTCwF6dsOdOffDCfZdi537dsbI6iWseehn9tj9Wz4bx4as34ZaLT0BBFo3KBOW2hD6dz7DetPj9vElZqUkFJt2k3zpCSWyT9Sv2sjE0lN6534UnkhMW+nXOi4bu5VPfX/DXLWkkEjHEE2ltJ420oEol/OhNa3+3nTajcOxDDX4NPvl0Gq2NSnzy+Vhk5mRj1Zq1qE2EkBlug2OPGYxmzQvRtWd3HH7QTsjPbYPly8upANSioiqBHj3bU2nYEs8/9yXmL1qLbj0LkV8Ywrb9u1O8S8iHae2vwMrl1ejUuRuViULcc9cw+i0nC09/RCXoYzwqFA7V0VpkZgdQW2UwaOeB2H7b3iwvwXjpxw/hLD36KMTPPy5AVaoABxy4MwYN6o2CVm2xcvVa/PLrQhx5xJ7YZec+6NqtG7tarzAaVK8JIivTj9PPORhPPzUS33+/0FpkzVsWoP9mHezHfjTXbf/W9Vv9X8GFb8zP6/97UKs35v4l/MsZ/Bb16+d1Fusu/hzSHwth3/uk8ZO5cPatomVfW12FLXt1xIfffY+mRUXICGdixZpVJJgsRP0ZNp2gM98lrPR1NwlXWaQSVHpV9pzzz7EWqD4jq7PwJegUT5tnJVSVTsvfEo7CQQcdZB/l6LXUzrSy9VVAKQr63KteT9WKwBpa9FNpBcuS10djREMSwhN/mIgTTjjRPoPXWfMOeqSnzalTmGaXQYOs8qFXd4UzzjjTWveql1ua1yqSlACdgS9FRMqCyorWRm2YBK42AWujqRSQgQMGYi3n7Omnncb5Mgh77L6HbZfasP0O22PQLoPQf+v0t/ZVNzlZ+lq1kMKk/QNaphf0uEtlS8mwrm5QFV9L91rJ0Bf01F4pCXrUpf0M6m+l1aMCKcL6UI++OChoc7JWKbKyOG51/Wzppq4MDWO6LBewMecVSGTwKfcKIpm6whpM8ycdK2OYl6ub8k7fp8vzlm9dQ3n8q65+GfVdQ2nqu9+04/dcXRt/45LMSiOU/vodPeyX9mRSKyxFObB1z5Z47Y6L8PKzt+H76bPRY5tDcfLFd2BNSRz9e3XD1r264pNXbsbtNx2LLbfqhKvPPx4T30u/Cj3n59koX7IM+2zWEzdReA8fMRY7HXYpnnnzfZSVV2Psu3fjilMOsnHDLH9NbQ2uuOspdN32IDz+zIe44x/XYtTL12H7Lci/KdX1JDlpeTDrp9dWaYhqf5ffhNmKsP0okbon3T9sN+WCnPahCfbTwmx2XR+mPRuCfTWSwQ05O06bgvrTAycw7K/kdF14Q8JEmotYpHXeMlVnK/xT2HW3HbCieAmuvf5hFK+pxpKlJaipTaGG1khl6VpFRrSqFglaW0XNshEOJmklHYEXXrgaV1x0DIpXFKO8NP0cMhqtQLwm/QyzpqYUVdU1qCmvQmV5KTIzIrj9trPRoX17PPboq1izphwLqc2tpdam5RZhxvRlWLxoJW6/40Q8+vAbWLyghgxn/Wam1aurUF2eZj633/kypkz/FQO37ULLaQmZUqn1b5KXixkzF2DugrWYOO4nrGb9uvRojWmTJ2HGjJVYsboUK5cuQ0HTDBJkAh989C2KK6OYO2cRAv4khUISK1am23POBYfg+BMOwQP3vYw5C5ZixfIqap3pMNff9fu8Plz4b8bmd9LVx0Zp6Pfo5/fw56rxh9BgPeucnSZp/fJPI0VGEqVmnpURQttcaekJTFxRhs5UTns2zcPggdti89ZN0aN7VyyhUrq8rAQtmvmRkamVJWCH7be3wktL6LI+ZaFr6V9C8/RTT7f3gpaeJdylFLiP5RxPYR2Lx6xQLWG+6wQwGUOl9gCwXbLgndCqqqxCNFZLS6WLtWIlPPUJXi3h5+Xm4YEH00zt5bqvBAoV5RVUOjNR1Lw53nv3XSTq9hxM//VXfPXVl1aYa3+P+5CVHknoVysCOjNCXw+U8NeqxoIFC/DpJ5/aJfh+m/ezCoKW5QUJc9Xzk1GfWAEci8ZQU1XNtqRQzV/XBuWtNn300UfYd9997YdtXLv1q3jqj1oq4Vqq16qGyhWTvPmmm208KetSkn6YONEqBfpYjvYxSAFQWilhejwhqCw55a0wwZ9kx+rauvS1/AwJf+NO60PrnTYzc8LRaQ8RLTsyxoackWswPzmVyWF2zlZOeTNf63itDcdyUlA95VvXYJ5p11Bd/oj7TRn1XQNlpR0VFbVB7jft+B3n2vgbJ0Ef4CWv6XTGipE/r1NG/jKY4ggz2v6D+mHa6Efw2ot34eNxv6DXZnug1XbH48HhXyK7WQGO3H4rXHv8QbjwuP3QsUV6r9ZX79yBYQ+dz1YBl59wIFZOfhRrfx6GqaOexRYdm2OH/n2RFcnE6x9MQv+9L0SnTgfhqZc/wrUXn4x545/HeccORmYWDQEqZslUnPVJsDmSg6yrmsX6azO75oNbndUKQNqt73P42HfqXRG5P6UDJrTDluKfhCmi/Y3jpGrQX05hbJCYSIOOFWzQn6Nmq8b0v3HyZ97SyBoqU5BKEE8m0KxpFj4d9SC27N8VF1xwI847+ypM/mEKtfQ+OPjY9HO37Qd0wtbbtcFVlx+G/fbdDBeddwNOOP4iLFi0Gp27NcdJp6YP4tlzr97YbmBre33Cyf3RrDCCXffYCrvt1Qez5i7DCcdeTiWgAtdcdRx2HNAPV15xIi2tW+2Asgew7/7b4KjjtsXQoXfgjLP3xLY7dCYTTTMi4biTtsfX336Ps86+g9aBH59/ej+6dG6JrbZog712Sx+cs0Xfjnjy6aEYesPd6N6jCHvv1wNNm4QxfcrLeP6ZV/Dqi9+RSFqhZVFzPHz/BcjNCeKhu9+CvlClRw8FBRm44qo9sXpNFc46/VKW9wnuuPt07Lqj+udADHvmTSxfscaW9ZsxkV8D/e11Gx2zOmf5W0P5KqyB/DTGm8yTYQ2lW+dI+L8p74+4jbWVZeqXfxp0ChNtbqrOYkiMnXa2P8ik6AL0CdOCyORv/1YZaBmpxU+LSnHVOz/iole/wLUjPsNFw3/ABSNm4ONZFchMxrB7y0w0D1Gzp3U7kMJfu+M1sWWd33777VYoSjg9+dST+PTzz/DAQw8il0Ly23Hj8NWYMTj5lFPw9jsjcfQxR6GoRUtau5noRWWje4/urB3rRMu5O61bKVFtqdi2btfO+vfdYjN6BXDEkUfisMMPQ3ZONjbfYgsrIO1X7qLV6Nq9G95+/z3stvtutt86du6InNwcPPb445DY78R7rST06t2bVvnOVJw74vMvP0PfPn2tINfqggRq1y5d7c5717Ynn34SwUgYZ559Ji6+5GL023rLdYdl6dHHscceh5132sku+8vyL2pRhA4dO1ih3YyKQ/eePW1c9Uufvn3scrz2B+gZvZbzBa0KaKVCeyQ223IzlFaUs96PYdx34zBrziyMfO8dKmE9sPPgQdhhhx1Yp6cxcOBAXHjxxXju+eftKkJTujTj9dmv++m3qKjIlqF8BVIE+1FWv+gufS1jYZM81TrttWnAMU+fZeIbOvlZBt9gXnIs0zrWo8HwBspa5xqKv941VJ/fc7a+Nn1D5cltWMaGTm3YWDt+zzVUlhzHqgEnQSoVQ69OJ1KZSCSplJgYeX4CR+7eH8snvIrJMz7BEfvthMsuvw1NfFsiv9tJOPqcJ3DLo+/hk+/n4McFixHMyUd2y2ZYVR3H0rIqzF+xCj9Nn4cHX/kI59z4JFoOPJV9sjXlx3Xo3LULxnw7HKW/jMRVpx6GgqwwEiZKHqA3tXwI+sNUfDjDVDXWnVZFA3XnH9Vbv96+Fx2KhnQMjZ43auLzP7UJdg5z9IlbyUMQ82IheqYgshUUpCv7S81Dp+eti/9vhM2WBa0ri/+sRsO6+DkICLC+nFDqjI0hhSq2qO5NAr3/iPpx07mrS6RQCElUk+1l2euNIcXB99vlfntDwqgmI1L69De2HZR7Sq94BaTLpvP3IknrXa8O2mtZCYEYy9YbBV4k8cjTr6J7u3746ae5GP3NLxj94Q0Y+91EzPh1GWI1IYwdOxW333kyOnQoqkuzIagvMt+0BSmIUW5yzCStxNkc6t//GTBpmobS/SF4x5PDaZXyTeLfWR8vvPl4rjVBVMN0TTfEb0fRAyaRlaUL2S96t39dFpy4etUvxTFO+jLw1fxivDx+CVbG/AiF/AgnsxAL0hJNxNEsEMXhfYuw32YdEElo9NhXQWr2/44210E0oOea/0uwc1sWVz2ImYkRz5k3x54pMHvGbJx73tk47rgT7HP5/0Uk7LfUSQViunV8Vk4/ejZrCUnzcB2BEIqr3drWL02F6yGLjhlpzJSsPuRtmX69/rNZKYH8G0r4e2AGtLatqf0bkG9KQP7ZbG1d9WdjCVUm28I58xuIkdg+qt8//yJUlMr0Zis/WdhkYJwtbCuvKXMUYJLad0auWicGhOnz1mDkB19jzPif8dP8BVhVvAqIGwQiQRptEQR8QfuGV7SqAgiFkdW0BTq1aY+Bm7XBBScfjs06pVcKBPWMXmn3+atZXIJDmM3y0idT6iwXuybeoOxVSikpnjDRRZ0iozBfjFJJtBZQBPqJETeEJBuvpnshVYBZaOjZfAnPfy+UX7qa65EuU41SqbyjNmZ0+I40mKR2r/sR0KtUIgw7SZiIA2UQsvFT7ECfCdGPAX4t/zGPVBbbr47REmDEtsb4ovwNUWgrH9aCWpZ9fYICX8ShI3T1rMWvCcG49nhfbUIUkaiivFYNfb4YiwhxrjJMRB6s4j21Nr3HbeMxCf+ke115qWrU6BhHc1WbHO2xxEGDGTMW4Ndpi2itFNDS2tImXraiGJMmzqTgiGDQzlsgM0vaaSWzYX1YF71hIKmbrpX6g7XypfP32efOtrYbQM218ApFoe6+obn4e1ASlr7BWAqOhlTkxrK1/oqwkfoogoL/LBrMt16edpJ5yyTkv6FPfai313MPF1fZJElP+v62CcRJc7ICMzBuXglGz1iGVfEEauOkukyDfI7Rbl1aYbcebRAgreuoiUxm4CdRqHyrAFAAmKR6kH3oF+2xPznBVV9bZ0aUIFX56Xul4Y1VMNPh2gPgp1IqWk4LV9G40mkPjSKn/YW0BZQuQ3DluD5ywllQvvLTvXv1T6+92sqojz1Myc5llaV7tk+rMul5W79M5efDRZdcgC8++wq5udnYfY898Y8bb7BhttfZNvdaXrptTK/imKfNS9koSx3YoTBBeTPMtkP58NcqIPRL95uek6bb7PJWfZgts2AeWsLnP5/ys+PB2vDaPqe3/IA+ah/ro/ksqBy2pi5//VeqOiiK53ZDqI7Ma2NxbHUbCNhknn8Qm8jDteNPYWN19WJjZf472rMxSBZw/NOF1MGWRxrSYXEc8yQNyKT4P+sQoL8Uejsymld23qzHiopqLFlVieI1JaiiwA9Q/kQywygszEPr5vlo1STHmZB1oMwgHaUoN7SUL6LV63tG8gyZrIfoRt5pfp7uCE9dLeTHeqgpvLLjI4GiaKqiJHbcJHXsjWI3mEV9KI7gjef1c9eCi+Py9Yb9UXjTefMTJMzTM1lwpQhe/zS8ZbtYVpDbeC5uOt36/NXZ6cm+IeKMkxbe6bwUT5NeCkYarjbeWqXxW58N8du6Cw2l2lRO6fWb+vmsb4/SCn8mTwcXxxvX5efgzaN+mOBNVz+uy9vBG74xNFSGF948/0h+m0L9fFydBXftjVPfTyMsyM+NkPbKzy6tQnlVNbIzQujUNB9N6nLVqIlSRXGK781bcPk71A/3whu3oXy8kP8fzbuheC690FDYxrCpcBdWk4gjGAywT9I9WH/WePNoqA4NoaF4DdXFxRMayrN+2d5rlzZdVxfqjfWvwpXw78rvj8LbFmFj5f+36tcQ/my/u5m7ntLSOeivHnIx3Ho4Ua54XqrcBLSSUKcYpjMhn9aP/lh/3cj9wfzWwctt0tBrxqohhX/CxBcuRAY9TECniCmYEdP/15WfksXIjGSF6ojRFDUcLRn4mc4a2gxN0C9Ay0bakKxXHUnqowUcYMOUrdWq0yqLvbYWn8q1BdFiVz7M09ASSVv26bJUAVlSKeVH61tZ+HXUgixwalFWO1LnySrSta+Whg2td9Y1QS0tIE0pxcFRVkxpDSBa6kmVxcr6GO73ayMUrTKWEdQyCuuSDFQxLNOuekCbK6QmKTGtZtn18NUwL9UkyH9sj9X01So9z+Mv68AasAzWjfcSyEFqc1p8TwRUR2aR1C5NWb9MQ2vPbyIsKoBkkJqebT/rS2vHn2Rf+bV3gJY9tc6Inv2QDvScX6sZWmlQ3RIsSxppUMuNJiOdRuOh3aBO2WEdZHVq5VALR9qEJusyHVGvufCXnaydpII2J2m0bL8xb2sFMbHdZWoVIWnEWvnQiPGOYRJYPo6JPQ5ZdVN3sZ3WgvOz7xXO64D8OA4JjoOWQYPMxx5QpXisI2tCupAH68kf9a+10BietkxVuuKRDmx/sJdV/QDbl1CCUJpOZGUzHVtqxzNFYguRVhP0SzkasLUX3XFyMP8E2+P3ZSBO2lA9A+oAQs8H1Xc2H6YNcgxF4VpR8fs4vmyP6qxes/OG6UyS+euxD62/pMpjWo0ZK8p2k87UuGABG0s6VQckYyxXY8j5kwwzLtvNOqu1zFZ/rZM1oq1Z2ldjx0iBRJCdEFd8doqlHfUfQzXp7ZsDTBeWlc1r+/aBxkIJWTflq3JTGv8U6ZD0YVegOJ7BlOhYtKF2MYW1guivvlF05cG6igJ8tJIc3ag3dKf4IY6LHWPRIMfLkOYTzN+Q5gPsy4D6PiTLh9FZK3WVViI0p0Xrmi9hzkExWe3AVo0DbJloKm0daaldL2dpxomNigaZh8oQccgq50+Yjdfz+CQHU3xMdRQd6D5tsXM86Ws3ezEfhSkjxWM32fxUP8vDbDn6JSVYelIeTC0yZil2Ix7rqBiiw8pwNqpz81HDuWB5KHmWVoSUcUC0wuuk7XfmobrJyhM/sfmy31ieclS/6Z/oVdWLs0/sPGK/1OoDRqyY5VkaD9VcfcD2hDiumvdp/qJ+Uh/w2v3TvGX/hCzvZqvUJv2jn+01vS1kLc8waZuFBFmHeDqOfRorC5llCcpPHa48yQGtn/iTHQ/RgXg9L0RPalmKPCys8WZZcaWxnc+iOHaqoaVUOybqE/E0BpJvxINMQ/+ApS/mp1VehiqNOkcx7QqvaE71Jr8PMk5SbdSAKifJE8kB1kH8S0xFYypr3dZDNMG2qEzJCjlmyP8aV0kBN06k4UCC6Ugh7B+ZYqmglu0lI6nGU56k51S6XpZGeE3qZ26SGeLpogPJB6ZjOeKhium3vExjJt5GH/UtQ8QDxKOUj9IH6WnTM2aC8ZKsVzgWRWFOEJkB1jVWXmq+PuIotI/VckQi6cFVXzKh2qpM9VcDKopje+ygaDU8QeYihhCKqzCD6kgK4XgmwoyTDFVz4MjYeA8KOnVcyFKFBAErY6vE6cCCUgF2OActzCrUhGPkDwk2i0JOlbc9rpg1jEehG89hHhKUVSyThC6Ba7KRTNYyDevJRgURtRNWzKHGH0KEeYRRiwSJI0Z/MeDsRBjVLFDvcyY4sGEJcnZ8whcmc2JSLeUF46xzEKUZbCNrEWY7kyEOYiqDeZBgA2VqKKoDmchO1bCbKHwZLkbqJ8P22zqw7TrgJ0klIlzFzqeChQzUhKoowEkk7BO1I2JqURWJwh9risxaEkR2hSWIlJQUCWs9KoisYlszaCU2Q058DcdJUy2L5FZjyxLTSLFNcX8OMmJxEnWIYyQFJZ1PmDHV68lAFluqyRVDLdubCDGXGEmJeVSSQAOaZKxbencwJwfna1BKXVwT26op9E+SvYpQqOSxmVqiYua2jCDbJ/YLjYM0nETEbnKLkXh8AfUdu02Mh30sRi+yiHFsREuReAYJVWml3LBs5hNmfEsnLLE6ov6NIlOCXY9hNOHVJp/6gQSv5TIrgNUnIY6fDliKIxrk+DOvDI6DSCrF+yzSW5TjZQ/FUB9LPyRBSGBLRUuRAP3JXETDNWlmRD9NswSySa8MZ9pa0mFusglq9ajGX45gLIf8guOa0AmU7BLRciKEmC/Cfo8iIxFjf7FD2R/66mICOfwtZZ9pLFqyjEpEogYVGezdQDXrwUqmchGIsZUhPUJinmyAoQCLW4bM8dJ4aL5y/KQMi4+FkwnWm3UU/YuPiAa07E4ajbF4P8MzmV1tmO1nYgniMMPU9rTQV64gjbIdmova4kwajcQ12D7Usi4B9hlHoy4/zh2Oo8ZfjFibsYLMMyn6DkpR45wn89G4R7SqqTE2NaybmH0Oq6VBruAY5No6VpGOJSyCnHMRzUW7MYkFkKNFSTMZ5CsyLvzkLVISfaTvaJA0Tj4QpDIVSzXlGJOeNBfFEFOkJZYtvhAgjYvW/LUULhEKDdt+PSiLMr0UESnY6uMqZDB9jPQsRVfKYopEkrAKC8tmjYIUcn7yrwT5iYRpyqdjymMcc10HEGMkPR8OMR9D2k+wDyv9GZgwYG9M6TcQKyO57IN02dUBnZpJvsB5aYUZa0GOaulJ88cydZ3ixr7ISGgWkjrEwDgGGTQGUqx3VShkxzUzVYHSSCb7jt2juUk+JMIQ34UvG5nxasu/ZYxE2SYpGWGOlZQF9XOtJAf/Z3MOJNi/cZYjLmp5Ltsg5U18PMX+inBs44wT5DiJFyRI61nkhzXk1/YRq3iHn3zIX8F5QHpnmTkcpzj7XPElTEkittwExzERoYDleIXjnIWsR4I0KQVWfEACSPw6iUyrgPt8FewXhWWhLFJC2gohi9e1iqdn4lQspcCozDhlj+Y6W4w4yyHjQybz1twQDxKvsYolbxL0kxGocY8GRRcyAshT68I19pbz6FEP6UHywLDd7DmWTXnC+lvlnXNc5QfiYcTCZRxLypdoFovWeMURJG9U36d81awD5UAs0yqCUckCttmfpGxTuRy7oJ79s/ZSajjbSJecNwwLs9+lNJH5MD5/dTw4+VKIhl8wKX4U51hwzrJ+rcuX4+K9+6J3QR4t/xXF5ps+/dE1WoVgWBpt2tqyloQGRLSiyU3ijuvEIxbAOcWJICtTPIKDzk60WhuZW1pDSWuzIQpm8ZFqDmCEF3oVp1wHJlCQhXLYQVkkpuoaVNTUgLSMSG4GIpkZSJZRsHNiagJKv6PMQVY2Jzzzrq0Qm6zhQPqQzQmSEeZkiORzYlGTJUGVVdeSsDno+UUIy5JgJf1kZNHaClQlWT9/BAVZWQjWRLHSVKAJCSyvoBBl5eWoZtszggFkZKU1qpqSuLUffC3JaIqrydADLJm1YmfGWTN/Hgk/kEOmSaWkchXWSAGRRsX8C7Tpj32gScQ5wtikcts3aWGl2SMtNcUBUr+GWGAtO1YTzMc6V1avZb/WIBCOICeoU8cyUFK7AnGGZea3RK5vDSrINCqqKpHJSZ6T6UcF2+2v0ecc85gLGVAmCT2UTUadFgAZJMga5llFQq0hwYQ4sPnUzSKUxrVVnMAcx3hmBIFMfXiGk6iWTKGmGsuoqIgQC/xZMAz3sU3xBAm0UoKJbSLRZhVmkgdXc3KnEC3RBkYygByKAjJkKYyhQDnDAohXs916DUoiJScL/mAGiT+D9ERa42TQ5AxQ4CQ5qaSpamXEnnfPSRWp5aQOkSCp0GTESWPs41qOSG00ilhCVloCueFMhCJZJNggSqjlpmqrKUA4QbJz7bj4mZ+fkyiT9B2trUJFtJo14RTNzEKWaEmvn9VSnWIfNc+mCsphqyXzTZRqzDkeGWTU2QWoqaFlznLDrLOPDFCsOCOLkzmVx/qIaTO+hBeFoBTHFPMOZgZQVVaBnFA+y5NSwrZocrMetbFVFBTsM9JbODeTaXPJJGpZlzVkPn7WzUc64NgYHSMtAabWkubZ3xJWshckuPmfcaTpS3EhQ6dYSZLZxWrKWIYUTwoxtr0gTIsgKwerOC9zyEz8bHdlBRV2MtLscJadA1p9CpGZxiqjqCHdUo1FFplXLhWTZCZpuzqKZE0VmU4AhRmtqWhTSFQXUzhT4QnnI8x4ITLcFA0LfaNC35nQKoE/QmUzQ8dOc65w/pUlKsiIq63yxdlHvpCDIOkixvxCFKahWBKVUbaC8zcnkI9gVqZlejIhEGeYzkmQUhUKI5/j24J5rwnlwYQ4HyuTKI1WIhxRm7NpAGShonwl+z2OjLwsKggUhByjBBl9ZaIEiRiFTiIL4XAulS0qrFVSwiMcD63myFqjgkn6iuTkkbFRyWDdmRWFSD6S9nRA0SF5pPgf52eNlDTyqjCZPzkolZJKTGvVHW8cfxm+7bwFKSrTxo2WV7Dvc5HNNsaC5UgVFKGJ0fHDBmXIRVjjybkhgyjqz+ZcJl8i/7DPh1lDWXjWpKKgikjxp+BNGvKFOJVeKsZSGFIpjhXnMycxklnNyFvIHyuraEywzRkyVmRTk2YZXkuDJc6Jq2tZ5SkfldM4qbqScZPkr6Qhf34+x5CzMCVrOEjhEuf85JhS+ARIB5XkEdk0guKkUdFBNvlBVIpcUHxdKwesP9tglVYqTEnycClN2nRezj4zyKOSQwGmlVwaWYZGVzwsGcNxZ3q9paawJPlvqJa0FlnBMQqg3BSQL8dQzT6LxTlnqkqQR/qOk96qguIPmbR8aRzQuKulEunjvAvWzZkElZQI89XKQYYMICpcpWTlYdKV8bOnaHyFySc1d6VAB1iXKOVjQIYC/QKc+yQ5+NnWMMc/UUGenEmzhXRXSatSq38ZtVQK2c8mFGU6zWkaRKhAJedFJrXFDPZRJee3jOAU+bDaFJQywTbJoLb/OLel/AWpiKXrIsWNfRPkHI9z/nJUfeFyjgEVSvIhyeEYFaC2ZQvwyLFbYNtCCf+1Jeb7bbZHD/ueK2MQpEU2VMQmrUb/yZDZYNs57DANC3VRO3ASZKQDFi6BSeKmRiMNPyORzTrJ2tNZ4dkIUlBUNMtDtEcnCihqXEuXILRyKapzc2E6tkKYwqu2tBi++WuR0bkbqnOaUHuhzs2JFSGjwry5ZGAViLZvj2BujvZDILC2EmWL5iOvtpaMmhMtIxNVnXuQSXKAp82mdbgYa6hcLKClXNC5O5qw/EQ0hrKlC5BDoYqeXZBTUoXELzOR7NkNtc0K4F++FMGVC6lQUMi164XsrGws//Fr5LRmuUVdUEtrJay3B0js4dUlSBYvxbLiCkTatEJOjy5AKSdNcTHalJSRQZNBceJEqa1LKwtHm3HwOOEiFCKyujgoWgqSEiBNSox/beVazGFfFPTph+xALioqi5G/eCnLzECqO/uO9Y5MmYnMmhVY0rYt8ju0hllBJrxoLspaFyLQtAWCoVwy2hqsXfAjWq6IoVVeIXlrFRZRWJUVNkWTTu3hzy5EFQVYwfI5KCxZhpIOXRAp6gyzfDFy1y6jklWNhVW0VFuzjzq3o4XICbdyFZosX4NMtmkNGXCqfQeYvCbIXMZxWzQLeaEUVmZwQnbtQ4FLmli4kIw/jmy205+swuxVK+Dr1BuFLTtwoicRWzkHBRSGYVpEGTFaRZwYlRmVVAQoIkioIVrpUlpqybi1A1473nOoFNaSDtn9KF1bheXS+rt2QW4btpEKQMbCJWhVvgZzVpahokNbtrUdQlRAqmcvRGRNMQqzSZuc7ItWl2FZh+bo3qmvZfzVKxcgXLISodxshFp1ZP6kv9XzkFuyEGVVmcjYbAC1JzaqmkJ64TKEmxTBNBONajWEiixd5trFlAcc2849qdRGOaRiahQMlGqB4hLEWd9wj64ILyf9LF9EoVGKlWVkUs2borBTMyoHWagpq0XWqtmkwSqszitCVvc+1JzItEpmciyrbTlJKkMhMgO7ypGKcAZqDUbWca2dc3o8UEmSyidjC1BoFrP/apo3g6+ghT36GWXFqJk9E01Ic9lNCsksaKXpFaZunZDUdzcWrEB4xWJOvDjKaXnXtGuOAOdOkOOYYH3LZi9GYbQCuZv1QRUFqD9ag9wfFiNKxWhNt2bIatUKuUvWIrVsAWm7lsyY9N+qC5IFTTnWVNdKipGYPRutyIwraflWteW8y8uGL9IMqbK1KJ/9C9qQHppxPq+qXI6ltKQyu/ZFkybNUVG6BmU//oy+hWRyNAXXhJsi2bwDgnk0EtgHKSqe1dOmcsb7UJTPtue2gOHcB+dR9oIFKKMyE+7eG6YpjZVZc9GkJEYFsATFVDB8ndsj+/819R1gUlRp16dz7pmenCMDM+QMigRR1DWhmDAu5oRhzasr667r6q6urAFdxRwQDJgzgiKS8wBDmJxz93Ss7q7u/s9b7fc8fykw06Hq3vuGc86tW+8tyoeOnwl3tZAcEBBzx3AMCRQUNjHmR52BZNw3jJH6AxhmonVPnUmSTd3ez9ho60cWgStiThMyR5z5UUgTbUHaRJAc1p47+nLBpfjggpvRwxwpcKuo3ZjkSeCqWVORx2S6b2AYb+/ow22nFKFzOIK1R0lgrU5NERoTEVI5EhP6loCmTK/L7Issdk4IoInC5uuKmeSH8eCOjZCwkDQL0deHcO30fPQMBPB2PROow4jTcxSMrsrFO1t7kLC6OIZCD2X6maAlI0rlHJcZDTWESnsC51bSVwnefqMJHzZH0O0TwNGzHRQYJAcSC0oggLEZIZwxtQQ1srkYScT3+/rxYxOBLNNKkhjkaxRuJCaECwIt8zITRoxgaCfBt8msFUcqHNIhw0TPZqqO61yMJ5J64ok+EWT7iFW8lvQ9zvwaH1Jw9YwMlOVZ8MRPTcy9OcSvFIqtSb6ejTwKzThV2NZjXfhsewduOH8myW8YL37bCntGCQlgjGRTBCcVNoVFgu0xawKXmGLSp2dndCSCGrmW8ZUZPZJ//h6gwDLTXxNJP2rcSVxUW4E1DR1oZt5+dO54BLIdeOnHRqhO+jcptI2EVZudpK1IxWlHkl0SM4V+IkTQSLCPMt+LLTk6vIaAPXmbheJJCDnJv14Vskcslr4TQ+QWs13yGF8NUgCfX2DEmdNy8Ni37YhREJmZp0LxFPIjnVi5eCxmeAT8/UOpLTPmoJYK0mzg4Mq0Af8XyKdLpQ/+EJN7OkzEjlSc7InclgNvYQM4DBhW+8nKwgxmJnVh/xwhAxO3TBWFTGE6nQ0tJ5pR++67sF11GRK+APr++W90P/0vFD65EiX33EZqpUPzX/6C9mfex4yDP8Mxuvz3i6ePHWddjqIli1C87EoNAP/vCLe2oPXJvyP03vsoO+tCZL/4HIyFBWjNqYY/2g0DP1/70OMwEJz/79i//GY4xk1Bza23IPj9Bmy56BKcvPV7uCfOhG/bNrReeQOvX43KNe/AyCTzIR1vwfdfIe+MP/x+hvThv+8J7H/1WYx7YxWyL176+6vMM79sg/+Py6lkfFQEVI2yYQ+dORlIwmvxkXEakK3LRYRMOU6Fao2RedLZhod6ETnjLNSu+jf0+WXpkzHYG5ddi+HBMCa89RpsdKSGsbPhGuyH/6m/Y+xNN6Nr5VvofPNd1L3xH7imT2ZY0CNpxGhgBAMrnsDI868iXFuCgrvvQtHSy2Egefq/I/jSW9h2192Y8dPncM89Ba0rHuV5n4Nh1jRUP/UXWOecoU2vyRE6sg/eS25A+FgjrA88gOKH7taStrL5Zxy+cAmqTDnwz56BijWrET7YgP777oeJpMDFxNcwHEXZi0+gcNlV6ZPxCG7fhpE7H4CpjQFodiLKZsccwnbpa6LsEySPCRILOr8VeUjao2TTMkWWicZgN4xnzMP4R/4G4/j0c9ty+F54H9vvvR2jH78XlQ8+KilCO5JDw2ha+R/g3bcRJjXPuvceFD54j1bpUA6lsRF7Lr4UmQsXYNyTTxD4bOj9z0p0P3QvnAsux+gf39c+N7B+LTrueAw1/34arsvP+z1W0sfgO28hGgyg+LY7eEECs1SdpOKXo/XFV5DoDqD6n/chtG4N2pYtR6y8ACUP3Imsy+jTDpf2ObHcb1NnIHf2ZNSufJrt+H0DpYF+9F10HVLNR5GyhmHVxog2JgkIUE3a9Jmw0U8lMkg7tOl8J8dQErF/zjxUv/MGkJVui9yW0ilD2HPy+RjV3UPNoSC4+CrUrn5Ge3/4zbcx9MSTcPX64V20EHWfvsdXmaU1lWmBsvcYjlx/A3IWzkXJP/5OZWPF9myS41l1mP7NZ4h1ejF0O22/dSNCAv6nn4Vxb79FxS9Pl/D6zCfBbTvQcO21yJs2HaXP/QeGrFwRc1rOibQ2ovmWGxH+eQccZ52Hsif+Bte4tI1lgiixaSP2XH0N3BaqpAdWoPrmG7T35JDxw2AvDl97E6y/fIvkfQ9jzIq/of+zT+C9+04EqGRrf/iORH0UDs86C/r6/Rg+bQImPfYYnNPmaOeQo/Ozd+AhGXOceTEvmOSYMcEKceJx9M03ENmyBbX3r4CNiV4OlYTVe8WViP/wM8xZGRwpM+wkEXrGvZB8mUWP64LYWzwVn1z5IDYX1lKccGziKmrdPqw+eyy+rm/GiaAeeQU5WP9LA/5x9jjs6xjCyo0tMJPE6HOymKNV2plqNNBNES+LHjNhdZI8Mx/7wkNUsWboQoQw+gcVkbaLp4mgrPA7RYSUl04rxTiPFRe8sx379Tm4qVrFmRMrcO0nx6iq8xEK+ei3I7DwPBkUDTJtHzFT/lEIXFZpwZWzs3CgrQ/9FHQ/nYigzRvn++wn48DK2AyqQ5iZr8OjCyehg+Loh/oelBS6sK8/hp+76HexEOJh/mvPRKbDDDU6gmiUfkXSFCIoGs15cPB8RUonrji5Cnvbh/Bds5+x74GOYJsi6czK8EClQvZKMTYqb1MWSUtQxd/n5mFirh63fboXw56JFJkhnJoVxqOnVeHZjQdh9WTixpk1eP6b3Zg/pgDO7AzcuvYQ4MpEytdPEuKB2Z0Nq4WkNNCHBIWigHRKDcLh4usch8iwn2KBwJ1tg4OkjzCs3dqxRSwYIdGbURTBX+eOxt8/+gXjRhXg3hl1+ONnW9Hgy6S9UvQNXoNZKUEyqrNSMJMghuSWO8mH1ZFBEs78FvWDnIc+FYclK5MKXqb6Ldq+MabhTpJwElX2RQRcMkoyFetCKhiinfVQiX0xjuedBXFcd2o1Lnl7F4Ys9BsKILkNY1c6sOrcsZhHkq9PxXVwUB3YOY4GrXAB1YPcj2Rj5J6wLMwgt2AnOci/31+TEDPKfZTEADqV48i/uRwFD01AS+gEUjEFFjpegsY0Jc3wsHGWaABDBDrrH87QAsWY6UIky47ApEpknn0yEgR+OfpaWqhcGXCjixBm0tr650ex/9rb0XjtcoSaT8AxgYqVwF///JvYtORqjOzcAXtFJSruvh+xkjr4ckpgYOfjzU3YPdQMx80PYtwLr2mvNb/9LrY//AAGVq9G9/eb4B5VqV2zZctOqtscOKkC5cicNRPxqkr4S6kmOPAju34jdGUia2wd4kocDStfws5ly3D49rtwdN2HqHz4TxrwD7W343sm9P4XV8H/6QcE3l6YZCbEKPd2IxgI+9E7qgdV/56E5BQmWF8flRmNwQQtU5iRVAD9DgvGrlsLEPgPPfscjhC8+p59GcHd9cihyrZl52CEajowROCPxpE3dryW7Po7j1NZZcBWVopUfyfWzl2I9m9/hMWVAdc556C1uBjlq/6L0htvRHxgAIf/8QR23n0rIl98iJ6XVyGjpBCOuolIDfvg338M0dnzMfqjj2E/5QxE9tbjlzvvw/Gnn0LsjQ8QbGxGrCYf9vMWwCBqjddPjvJgwEFFl4hBqSRpsdsRJJEJDbTAqQ8j3NmCwrtu0oA/SMW54YF70fTyfxD46BPEO5horHaEZWEiCWaG3woH2X8qGoLPOYCMW0fBeFEGfOiCOaSn8rWjJ8TAmTcfkz9YpwF/+7dfYdu9dyLy+nv44fXXUE3QrBLgHxrCjrsfRuM770KfnYVR99wHX14V3Oech9KH70cq4sX3y+9EyxPPIfjO21C7+5E7aQaVRrq0srWwGK0kvKUrlmu/y6F29WrJHFUlGvBvffwJ7L3pVhy/6Q4Mvf4uvF/9jB1/fgpDR5s04D/44v9w5E/34MB/X0LmonQJ2MFj9VT7eah44QXkXHcLkn1dOPLoU6h/8BlEnn0RirsItS+tRsAbxfa/PYHmlS+g638vINB1Qrt9lhmiOlLsGI6H4K+JovSBakRnhOFLtSGZYPwxYl2SF0nIw5QM8ZIMDfg7N3yN9RdeCR8VCey5cE+bjCGlH/05VuTecJnWNjnU6hwMeOIIKlGEJ6dBd2jrIaw783IMNpyAdeo45J40DX0frmXySe8C2F5eirq//5k/2dD8yuvw//i9dqslkrLCXctETOAf/G07Nlx4Hce5G66TZ8FBUpIsr4YhNxcj32/Gp/NPQqynlzE9CrYZsxE5aQbGvblaA/6mV17D7j89BIOqwnzqQpTdvhwdQQW5NeO16x/474vYtvQaqIcOESQLMOHl59FscMBamX4/1NiFaJcPnpJ8WLKZ7Pnaic56ROdOw5yPP9GAf+i7H7HzgUfQtep/GH7vc/hWrcPGm++Cr7sLOhL4lg++wtYbbsVw/UGU//lhDfg3PvQUGu5dAf0X69G0aQPsDgdsURUOArus34hakloOcCSYA41W7DtpIY4VUNhQSUeodOUWXLUzH8VUwmu+2ItX9w/gua3DBKT0hi1ji5z426V1WHFOCfLiPaRpFFT9/Vha68HqZRNxz2l5KKMiNYRDuHleOZaO1uHZy2uw6sIiTDUGkRwhDTEQnOkLslZGIUDJkyWPXjAJaifVvo6+IYBDwlBGELl3thtvXDkB10/Oh83bRaAJa2suMpn3Cx0kpkcG8fDaBjz3YzdaBokXRgtJjAGZYblVZoDboMcVFBptx7vxxPoj+KTFiud2e7FpwACnMoCbxtnx9hVjceM4G3S+QUwvsGP59CzcNiUH7ywdj4tKqXajbTj95Eosq83FbaePwrlTCnHFOAfuHmvCc0snYHyWDpMLLXj5qho8s7gS01wR5osRJDiWUQoFNSWkRcZfxxAlISEoHvbn4e1vetDfPYIJZWVy5xBWKntrKIHZ5XY8uWwqnrqwElNdYUSHenDTgkKsvHYcVl9agZcvqcE5dRaU2BO0xWisXDYOC/Op1kMmKKqHY0sxZeE4J0wwx5wYGfFj8eh8XDa5Bvf/fBA7vS5Uuc14/IpKuBNeOHUhXH1yDmYV6jBvlAV3Tuf1F1dgVnkKZ48y4M8kMX+Zm41nllRjgjOIRHwE6kgzATuIN6+ZiKfoDxMdAQqaAbj9nVhcouLFy2rx/MWjUaO2w0zyEGWfdQkS1qTcjpBbS2bIwmRzzK7d1pCDVhclpYes3pSpKrkvIguS4nSYhFEWl6Rfk3uXsjAiYJQ1AXLPZQSdpl6UPTwazgv8cJ7nQ+kjxejM6obPNIIkFX/S6CfLSSBBxmOtqGMC/v3hJSJWPJNBePYFcI6vS7N1/p0gaBvHSnU9DmpfDw7+8AtO7NqB7g3fwC0LtH5X7wqTfjbVyJ477yFz9MNeWApr6WgYKku18zdv3QAdAb36mcegKgp23rIcnTddg7znXkH/rfcjO6xHVl06Kfh/+hXOsVOhzyBwygtU6onTFkI3fYb2fvc3P8NVVAljSQVC3gHs274JJ7b8Au/GbxDsO4ZcJiw5ov4RtGz4FkepZMMEm0w3QZ0KP5lSEY4SMCYEUfdcNTCjHuV3uYGFMfQmu2iYAMEmgjBJU3bVGE39qGoSI4ebcODZZ9H4xD9h6+qBe1wNwVGP1NadMEUCCNosyJk0DjESgWjzYSbYXJhyMzFUfwgNVCWpUDope9sakXHeIuQtOB0DW7bjpwsvQvM/n0bn6x/iu6U38/utsFTmwZTjQYzkK0KFWX7nbbAU5qHjg/XYOf10FLz1JlIrqIZffQspjxOGM89DztRZaNq2E7GRKEnJKLLiHO2BCyPtKUeopRPJAH0g4UScgJ1z8lTt9diIF97vvsYJjlP0tfeRx6QS5xjFbfQT4wgRi2o/1Yvh/HYU3JkJ55II8u6g095uxGBmF+KhYYTdcUx75SnNXg3/+BtaF1+MoldW49gNV6OkzI6a29NbX3596umIPfckBv75d8QaGqAj6w5WFcM4c6z2vkq2PPDTBrT+5a8I/3MlKoryYKkqT/soj5AnF/pTz4dt7klso9zCAXx9IzDkUG1RNUh9iYZff8aRbdvQ/+tGWI7uh3XzT9CtXQsLbZSg78Xffwd2AlFGUysyp9cgHlXQergDpiWXkgycBu/+fdh22rnAv1fA8MoKHCWJmTAnXckuxfEb3LMLux9+ECPProRLNwCr3EJI6DGsG0R4egiVf86F+fxBlN3lRGg+CbRliG1lgtfLwqMwgg4muGoSFR5dB3bT3n1UFeke+gmUEZlqnL0Q2bNmYIjAHhwYgqmwEiZLLuIkEkXjJ2mfPbF9F9+LUp04EOloQ5yETu3sQOzXXdr7Z77xJhzTF6Bv7Q/w/+9NFFIZySKwEHOKfUK6ymb/V5+g6bP3EZSiJwShSIqErzRdkOrotg3o2bydCYlqKxSGdzgAz41/1LZDPfb0Cxj50wNQ//svdL63Tvu8ZcbJSGbQDlXMCSE/Ej98DcO6d7GTRFXuk4PxaiitgKdW/JH5igQ5RkA21k2G3uVG6kQDury9mPzq6zyZG50kFwf+cC48L72AyIMPIX/THji+34rIR+8R3OLE6hBCH78F/ev/Q9nAMDxF6SqgvoZtOPTis9h5zfUodDqp0phHSQ5lkS97r029m5k3E2oM9WXTcHj8TAwnZRV9XKZgYLObsa+rH7to67dWLMV1U7IJkv30E/Iznn9SMX1QTeEPxXbcekoFlJ42XDOtCA/MHoXDBzswIdOEhxaUIzc1jHkOAx47uQ7bSNizPTY8u2QiKnRBKFFZR5Oe+o1Qor9KwTPRbcP5tTZtN9EIh8tFInLr/CLMKnXj4J5GXD8tH9dTOfbyfVkELYtnbTYn5o4rwj9unIf5xUYQNOgjBHFigo98OcTcWW6zojrHjY1dClqsWXBk2uG0lcIQt+CCiXlYMq0aDUcbce6kLCwe60AhwfC6SWWYWZEDb28A/zqtDpNzKfBae9EdDuJYUw8aT3SixmkgMamFxWChkIqhoNiFlhPt8AR9uHveGBRZmYNJUuTWtCxNTOllXVEUirZ2gKIhPozJORRMdhOavX4oVN0RWUDJNpdku9F2pAN5FAN3LCxHYaYOO48HsW3DYcSPt+PcgjwMUXhkZDjRPxTFIMnznxYWoSZjADq9F4YY7akSXJNB2juM6gwXrpo2FtsPNWFTm6wBoU11PpzPsTCTgEm7Jhc6UWxNoJgE7Z5pVRiT4cBAxyBmWOO4Y2whyjwulDOO76M9i5QYTs+x45VzR+Pgvhak/CqeXzwTEywhLCzT4d4FE9HW0o1scxKvLz2J9HsAAb0ZbBbiFFSy5iSml1k7RbulIjcJ5dDrTQZE5UPp2U8yBGFLgkGEZJmfE2RmMMmqUFnZqU/aYY9Yxe4YfedsZJzJwDAyEYSakHGBB1W3zuCgmmCgMknwXEELgz6eQBHZuhzDjUcR9vYha9ZkVF/+R/Sz0eGeAW1WMdg4QEBLbwKSNWo0btmzGZcc2o2y5dfDwGDWW9PqXNfZhgoSiSyzjYBhhuL3kV2rsFeny5J2ffELZl9xk/azcvgIdOvexNR8Dmh2EcoznciXMpxk1trBxO2ZKMnWgN59e+BvaUf50sXIP2WW9nZgww9wzU4DWmZhEa5Y9xGubGzB9CefRY4nG10vr2EiHGKSnIBbOpsw/qNXEBpdg0hYFqu5tU2DdHUFGPf4yYywNsQUH/tC0vTnOpjmFSEcZ6KI6pFryAJ2NiO0cxeMhiTmvP48LqPSy71oCUJOE4zV6dsggV+3EQTMMBRXaNNVyX46X+cAHEX8XWeAY8oUrOgaQvnFF6Lvmy/Q9ORKTLh2ifbd3jUfwx324dyfvseSpm5cSOJgOGUhwllU6/SBwb52JMsK4ZoqxCiGw6+s0haGlGY7UV6Yo92uCBUVIvvKy6maY9j8v5cQ9PZT1TmQR7UaSgXgHpueUVFbj8EUirAv2Ugw2ba+sRoJXi9r+mxccvAoFnz6FWJVeQjq+mFhsrRE2cekDYqJPmMPo+TaMXAtJoAkGkgGGlHyx2I4L6/AEAPXM/csIK+C/nIM0bUfYwrHIZ+K3kIbTrwk3dfEzh1w1u9HXXEpPLJ4S5gCjyyC8sjHX0FpaIGNCuyqhiOYv3MjOiZUwVtaCGc1r9HaSX/uQ4qJcM5fH4YaiWJ45x7o6cfh/h4Yyz3aDJUsBLv+hx9xFa9zyhefIhZW4Hbq4JxeDHtNFdvXBqc+DrvHjBySX4OLBItsPUR1W7lwrtae5tVvozIYQ2VeGUZl5KKmpBz9P3+HyP4DcNdU4twvPsM5e3fCSmVqUCxIxaksCDDhUSmMe2QKCS+BIkRlWjKE8Q9PgGNsejfLuEXuJRqRtGTAUTtBu9asux7A0s0b4Cwrwa5/vAD9rmNIZPOa998Pb1cXdrz7NgbbW+HOL0OmIwd+asTCWXOYAmKYvfxSXL/3e363DIdefhm+7YdQklGEg2+t5pnDyJg8GoFtu9D+10dRIkWmDCr00QAsFhsc1VXa9dV5Y7F457fwTJqMoW83ItByAtayNPiPuu4W3EYiaSosQ/OqZxHdfQITL7oM0fYe+L75BuVOCzKszD8EMzkUbwCxvCyYK0oQJAhkekMoYC4zWwlKMjPjJ/FhUnHXlCPWzSTd1MyclYBp/BgYLGb4d++Aq5AxVV6AkdZWqv03MINkopCkN6uApMJBtUwCY64j4FRWQfUPwdB2HGNsRdAf6oDv1x+JNhEs+fxzXOLtgXXxhTCMJJGMpxBhnMjTHUmD3Oe3wB4zotHgxJ6zLkdjBskl82KKhNiYssBKgtRJoXXJ+r1Ye6gHfzm5Bq9dNYPx4COB0+Oz3c148N2d+GDfACbmWkkMBnHZzHys2d6CFd+14+XNzIVs7+gSF/udwtu/nsBr9cCNq7+A1BmdMy4HvoQsWswk4bDDYTVgS3cMrx5ox5MXTEe1yUefimFUjgnFedn8fhse/7Ufa3e34bIx+cz30h89wlYzPmsdwn+2nICOfr7yvPGYmS/rTSIIJqLsqxWmeBh2g5/kK4540kGxyDZRJCI5iJSvF1dPrsDRY2042NhJAO3FhCwXRV0KXUEFj3+8H8vf/41nAyZm5+NEcwid4Sjqe4042KcnkbViO4nIjS9+juMhO7buPIrDXX0YIjBm0eTFFo6pPGHD8bTwLCZZ9KySjCXMKLLrcccfyvH4jXOwhzGxvr4LSausxUogRp/YsuM4jnX70RNIIt+UQEGmBT93p7CFYDz1pAl48VAvfj7Sj8a2QQJ6K/wUekWkdmUWPdS4LEtnBOhUxKwphA0ULwTQrmAU42vLUWczUQD7GA+yIoV5iSQhCo4NQVdPsWQlOT4cUXDLm5uwJ+CC0+zA9pEgLnltI16ijfMcFhQZLThnXDV+6/NjxY5+rPiiEb5gHBfPrsJJFU78urcDz20awr1vbIUrkcSCmjwkY4OEeuZUUGgmiNcU8kkSAZmRl4dj5aC7yPOeDDqyJnlUQ09GKotU5I88okDdr/0rq7BlOYCsipRKXaaYDT2b2hE5moRO54bR5IGyK4LBjZ08j4nObaSTkw2RVcgDS7Y5UpEOOLh1L5RAEPlTJiC3rBg9v/zC5OUHIlR04X5kT0qrhO4vvkH9FTej4/bbMbzuU1jLa2B0uhGj6oh0NqM9EIP7jPPIBK3w1h9GKDzCRJBm4+qBDiaVdMKJxxUkoin4adg+suuhWICZphQ6i9wdDZIjjcA5aQyJTRz9mzZiYM+vKKysRv7odN3zkcPH4ZqeVq2+bXuwb+lNaLnjTgytWgW33gLve5/i8JIrceDfT2tcybPkEmTNX4AElbcsmpGyw5H+CPpoMCgVMBmzkBrOwtB3/VCOp+//y50j4V4mSwS/XbgYe+6+HT2/bYQ+rwhFBH+1tBJJtwSSqLdDCIZDSM5Oq8P48CBCQT8MRWlyMLCvAUoknSD79+6H0udDRn4RRUYKClVIIqKi+cBeJMmuQcfsPnAUjsm/q3KybTsTkoMEKR5WNcfuSfoQ9EcQ8XoxaE3CedJc5E2dDtWk4uRrr4EzN71pkaHIgwAdzF1dSxYxgMTRYzDIStewFxaHFSPf/IQ9ZyzCsZUvQPHFYD33D7CetxheUUVye4m+R+/SFhjpYw4Mb/civpf+p8+DKZmNyOYIYjsHmFTIssvS91kTIQabkmQCiNO+YQSE87vytfcGBkM8G//1hxAeVwfr2DrE2jqRMRCA+stGHLrwXOx6fIW2+Ew/YyrybrgKIwRFA8eqdetvGOnqRHFdHXLnz8TuN1+BQ2XgREPQcTxNRTUEhgJ0/bIVO66/G613/hkdd98Hu40xoHfClDsaencmQaUZOgZyMKjCNCE925AiWTP5+mDPph/wd53JQr9nwmA7fSMhzQ8Sh47j4JLLcPixFRhuboKjbjxcF16McNKt3XmXx83gS6L/2x4k+3JhMGdB9WVi8Os+RHsVxrAsjlSp9mRFfxZMlZWIUSHt++9KnHhvrSxHwIybriFJT8F55nmwsZ82Kuyx556FvKoq7XZOioRTdZAgl5CUJhTs/ttTaHzvI+kCRi86E86iAlgJ8IkG+jX7KH1pe+JpuLuaYOG5FJ0DKoEvSL8wMMnLUVw2HnarHX2r30fXo38nKKZgrxJiHYNvL30ymVYggz9u1vY1h4VJK0JfZ2wPeAcwYM+H57T0rmfBLT/Cki8+b0CwpRFh2naASq7s4gs08eLbupN+5SBjdyHaP0SC0AWFhMhI8iJH+4EmFBdUaz/rGPsx+mCAinKA5HIkntRWxyscM/vo9G2PWICksZXEz+WAv7EBR2++FweX3Yqhfey/3YXJ/3kUTcMdMBv09FeOBgFeFkJLPYogz9dYOwX1NdMwxOQs59YTNvkPwdUAl0zLogBP/tCIv7y3CRPsBtTlZyEUi0G1uGDPLSYApxBN6plntaVuGJHzZ2aSOBvpl7LOipmaZCFG4HW4qDBtdvoLL8CY16qMyqPM9Al5XMxFJfu/LUfRxZeXzpqCaDQOayqGBONdFpwZ7HaSF3nQkCmCPmRnDKcI7PU9Cj47GMLL3x1FB8epOFNFlDZ0pCJwxILagvFeip5EJIU5DEOXOgjyP8RiCpT4CPKNRpSTsE2ZNh69Oiu2dI5QtFngJ3lAVhaMBXUIyFMYhCeLjarcTHLBQZTzSnXLgC+izcK6jQk8c+EknDW1jm2meFUIZwRfWVzLUaEvCo7JTDbpeUphG5PYXO/DP75t5J92DCQ8yEzIJxNwGmJYccFknDNjlPZES4yfTcV4faUdjyydj+Mjfrz84xHkOF24eX45rjtjAsfFCVVW8rOlhqgRNo65VJVVSSwsyMKAksBTP+1FX58Xy+dWwhDzUcTII+j01ngQeiUIuzFJsiRPClGU8fO6PPqyhWKRnwmyHY7cQg5gDkL0JVpXe6ogTvEhBcDsJHCyk608biyzeDwFdCRnRhIgK/OpVaE4pK1F4cuCazMxzxpVCBYy08HgJ+rLoZciLLKKNf0MKeFKnIgKUrsFIH84iLIoTQbUKIVy2BB5Xt4SNyO1M4julX1Qt1DB1uei5cV2xLZFkUGGKQ5HrsHBiSOSYYPz5LSSbiSLN8tTBHy3kWCrHD0BT04OIn1dTOwEhcnp6XiZvu4lqHfVN2A44IVufCn0dguGqVCCEybC+MAdqLjvVqRGhjC45iM4LU7Y80vYPDpBfzdCHS3aeZy1VJDLl6Nv9kkInTUfAwX5UGTDEg6iuvuQxjStdMZEKIzI1h0IbfmNAUPFKisuRgbRRye3n5zecKePxKPl0E50HzoAX0czlDEFyL1lKZIEwsMbvtSeC09G4kgOhal4ZVzjkLvHNq+K/jUD6HrDC7WlED2fM8m8M4iMLrJlJi8dg7pPYagtX4qCOSdhcNtxDBxq0q4ZGyGkMWAN8lwqj8y5C+CbPguV96XvQ/u+/wFRYxz68aO03/es+CsaqbLlqDidqo0KJtDZA7Fz6aWLoU6dioZftjNhMPITI/Adb0XxzHT/ol1ehJq6Eec1TUwAVbfcgOGZsxE5eRH8U2eji3bKvZzKmuYb2rOD/SLpkLpDPIxlpejPyIE1LxeBQAj+gmIMLpqHngklGKwsRN4tNyKV5cSRTT9RIVMNRKkMBxT6kYOKzE7/k3Ggw9IP3TEGwdYg2l7rRXxnPoIbPOh+sRfGfXo4HG4m4bRtHVTXWTfdjIG5bONpsxCuHIPhtn7tvXySI2XOHMTPPgvlf07vdNe+bh2GRZ0vv5WDQ4X5wy8IU0HKoVI5Zo+nQqbZvbvrESUQ62QBrHcEe197E85p0xCmrwUjPrh+V7K9JxrRf7gBXft+g9LWDAMTc8xshLUqPQMVa++AJURgJ0u3z0nPHqneXpjb+xDx0af5e8HicxD+w5nonDMLCn9u4bhksE/mcePRtmsP/PR3OZRh2YhJniFXmGxJsoZyMLB2AD1rqW/rcxBeF0N4dR8MgxaobINZEF6mI5lEnOyr3Cdu3fQr+htOaEmSEgEt9iQKHrhTA27fts2wEMS1AleSNEheUqPTsahGIjj63Y8YaTqm/W6wENhNcYRJCnTFxQwyDxVvFKFfdmhT33qmCbmvGFQUJGkjPYEMBIlNi5eh9fJbEXzkMWQePoB8Eg1bSRni/V348ZrLodRTsvLIJ3kOyG2TwT5YRlXBSRsOzZ6Nqsf/ys8XwH+4Hr1ff4OCyelZwkgyDO+EKjgeeQgFy5YRqBQ0vPg0PLZ0zBhJUCMzToLzytvgWbiQZGIYyubNJGbp/fbtFVX0zz+ia+50xBedAWNxNWwE1ETAjzwSQ+3o6GRcU005Ce5LzoRtwRw0HdyPoc60LwZa2qmx5AkVPWNVEisBnslZ6lj0uPNxfMFZaDbJU1AcfL7OTAF5jEt2Gq0lx7quzoELxmdh/Jg8dBCA+wgQNiZ0C8FPSETSYoaZ/oWEHb+0+7BwUgEuqTbhrNos9PEcHcPMEwSKeePKMM8zgmUzRiNEQx/u8sEhfkxYSVoo1GThtolkXOfBaxsakO90wMJc1zmSgk/R4fSxmbi0WocFzG1bukfgSKi4f3o2FhSlcFq5BZfVenDxlBKSESOODahws38rphehyiYPgprQFvPgy8M9GFPpwB9n2HBWvoolY3IwucCFH9t64WeeO1Dfhi317djbN4KggWNmtTLumeMIaAn+LrcRkomY1p+pJVaMyuYYMMdZbDbmwihB1I9pzCWtTR0YYXyaZVtmGSezASl51I34JbNR8hi1PHbkJ1Ha1xfCwUGen+NopB2MJl6XY2HTqxhDpd/b3kkhMMS2mEkAwrhnSiHOzrLh5/2HUVthQ122DlUkj0pHH4YHh2HgdxX62YwsqvbZ2RQsClQKGJkPdPMaHaoV7+7pxNhCF5ZMLESnTx4UBq6enI1LauwYlZPendJEnHCwPZakhgTaeR20s4WxK6LQbGUsGvXY0TyI2hIPzi9P4sJJGRRoDmw4MYydAxFUV2RhSZUJy04egy6SmgMkaQaSVNk4yWDy48xReiyutJAokEzKAkntypLG6YwSqFKFSDYsEOkh2CwknJqMiULclMyEHxYnlKISYSPVtDnG4MqE+agTA6sG0f18N8xN2cg1lcLEwdbLRgT6KAcoBEdtGdy1TCR9Q3Bu3wqrKFOyG+/Hn8MeZ4czM5A8SlVmqYBdPsej+qarMeeHjzF19UsoOGkePPlMMjycdZMw6/P1qH38SUSaj+LEPx6H8u3nKKgpg72Uyp9KKV8XwOAnH8O7eTdMGdkY98zTGLPmDVS/8C/oRk9CRvU4TR10/bINubZcZFRUIzHkhflYM4yb9yNKlSCHcuiYNlDFk9KqrfTMBTj9+68w86MvYJ11Flzzz0D5Sy9h4hfrcdnnH5HV+dH71vsIb9kKg1NmFgS0SWjolEVJsvePQxj47zACH4WRGcmCxeyCgUo3IatxmTTL//oYJn74CU5d/yUmXn8jAvv2YvCzL2Gkw4V+20dGmkDtk09g1vqPkVNTiz4qMe/rH6E4rxK5o6hsB/pQQFAxdHdr7bVm5cHGRNL+6hrEqeoLTl+IU955FQv+/aTm5OHtOxggMWRPFXVDtutlMjx2GN3vf4Q4gX3s1Vdj4jvvoOSNV2C8/lbEp81F9ilzENizC233/AlHLrsSiYNHtGtlVBYjIYvgeNhLKlD72D9Q99H7yLjhBhgXLkL1ymcw+eOvcN77b8OdZUL3B28g+tPn8JA/q3QsklVtBkp4roWMOieVD8NeA4ZfHsTwq36YOzIYsDlweGwkm5vQ9+5a9sGEWoLX+A/eRfn/nkfu+YvQ8sEaeLfvgi4rA6d88yXqXnkR2aNr0LtqNQaffxGZBNWq51axLd/i0k8+Q3ZxJfrWrcfghu0omzWHKEhCcqgRht707EnHCy+iLEzHtxqhDhKwkyFk/r4b3rgl5+O0j17D9O8/gefMc6CMkNBY7PCMS5MDQ0sfBRfVmM4Gz0npmRq1sRWZIxEMfPQ51Wozik6dh3FvvYrK995B2b+eQQdZfdXTT2DyRx9g4TvvomLaPAx8txnBb9dT/YQZV7wGbWpP2JATKULkmwh6SMBDn0aQPZLH5MNYIvCocs+fpNRekQ8jfctVmI8Fz/wLJz18P3Sd3Th4+wOovvR82MtHk7gfwtHrr0frbcsR2nNAa6elNBeW6elYNFuyccaqVZh2721QWlrQ99r7sHX7oUQNyJo6jh1l4iIZ1gUHkLLRgtojTASDYBjVYyZpz1X3Hz6EotY2lPmGmCOSFPVOOCrrKHbcSB3rQFGYiqivVbuerYyE4UQHY+kDAkIC4x95FNO/+gQlty2D78ARNN5yJzICPlRMTs8S5s07C1Nefwe1j/4d/qY2HP/TPXBt3sRYGELgl99gKy7E1FeexOTnntSITvMjT6P8eBNyujrR9/4aGEm4xiy/A+M/XINRb70J66mLCJxWmaxF8ez0EwCpQyR3/E8pKEXpg49g8ltv44yNX2P0eacjcPgguv76HIrcJGEGkngCmZ5CSWpTeDkSRyadgq01s+Bl7jQSkC1MrvJYmRRuSYgqp61GF9lx9oQ8FJVWYNWOFhyjGjxC0tgSJgm1G9FBRb13kKScBPv1Le1o7A/gotkcJ5cVq7c2YWAkBpuBQo0K/swp1RhDUvX6b01oCBhhMmWzTQatDsOOgSAGQjpkeQrxa9sQ3mjqx+FAiqBhx7o97dr0/qJTxuCIl+r651Y4szwozTeDXBCjC/Q4b3IuKkoy8eHBPoJpGGNywrigOhdBP4GKfZMFgOs7QnitfhBlecU4Y2IuZlfbUZ6fg5c2n0AbsebUOeMwfcooZBaWo4eBf6AvSuFnYz404LfhENpVI4HSgU1HBmBz21BVkYMGH8F7KAo9gT5IgH9tfxfK66qRX12Ggz0++rsFXSMK6gdJOA3ymDlzL/8bSpi1c0bsLthtLgIisc4K7Pcn0DBIwqqzYs2+LuRWViJ/VCV29gQR1TlQUZSJ3SQnxbkFWDKtFHkkAp8d6UYq24O68VXY4Y2jl8T+0ql5GFWQQigQJZDr4WW+39kbgGL3YNuADmv292FyTTa8bMfq/R0YN7qA53bgt/Z+tCet6IymcKRfCtbpoDPF0BCMoX6AmGG1g5oROxhnUf78VVMEH9b34cw5tRhTU4DXdnRgc3cc33XGsYvXmzepDCUlGfjP9lYcTwADBjN2cFxSZivyM6woyrQTl/XQisZpaE48jw8Np7bOnoIJAapHs53OKLyfvsIEo5VRZaNkkwszvyTT+GHS2qTcz6MKETKg01sRk8pDhgRsqltjurJjWZwJQEo5xBX+XF0O3ZJrYejsQNea11FwwQWIOrIQ++pLWGuqYZBV0Lt3Eazr4brzehIReVaZSdNJcBzph+HLTUiVVzLbjkWSbF6lwtY1d0E9cgCWxhNwxWmwGTOQPO0P0Dc2Qf/ZByTXCkJ542A49VSYiz1MSmz7YA9in3+L3OkLYJw4CUMEbfOJo7DffQdiXb1IfPQxrAqB+MLzYZhUg9S2vQh//ik8D90Hv1kqjRk4RlYYyKSVT9cjriOXm7sINk8eoskuRA4ch2PrQTiDPsEKBrU8lylTUBwTftfGhJ2IkFCRgcct8hiJEFMLcwXb6jAiumgh9KPGkp3ytbZ2JHdtg72xBa5YHMNZOYidPg8ZJZWaXYKtjTBt2gF7Tw8wpgqJSy5BjEop9vFapAqL4bzsjzB1NkBZs5bXocKYMBMWKltjtpVgS9XmC8Owew8S+xqQffdtTHQJOL76CdYjxzCcm0m1Px2u0dVIiZKjWg7vOEhHArLmzkf0122w/boRsVAchouWQJk9FRnf/8iE1IOSpUupbql4zCYkMvVI7dyKSBtBgcotI4sELhREuGk/Ent2wz0wgExDuqJa1ESPlWmspF2bCtXKqxLEkiQ8cpspZUohSsZmo9/Fo0EMukphWHA6dEwMUrwn6R+A7oefkDragUhtDYzzT4I7Pw+qPwi1/hjHchcsw13QjZ2I6Lw/wJadj4jOh1hjG0CyJlPb2ZdeihiJaWz9V9BNnwnDmAkYWfcesqiUbNfcikDjASg7foLjlEVQqKa1oh4kzSaPA8l1X0C39zD0VBGGsxciVjUKyU9+gut4A/wq/eTmq2HKLkLqp5+g37ENOpsT/kljYJrC62TlalX21OZWEtn1sP7xOlgcTu1Wm/74IJLbtsIx1ACzMUOLKamWl5I5WY6blJGWnePk2SBLIn2fP2GMUplRCUpVwKqJSC2ajyT9V6pPJvpIavYdQOzXn+FZdhH0o2dxbPYg9vVaWEnmE6efAd2sSYj/8CN88QTcp82HPaRH3GxByNcBw749MB06TpVLZ/ArCJw2C/GzT4Fj73HE166Dx2OVzEBSa0SIyTh51hlIzZmFyMFDyPz4S7hdBq29aiyFyEkLkFqwiJl4C3zrP4D7/POB2afCtn8nAp9/DrUgB+Zp82GoG42EjSSxqR2RHYeQcfwAHG4r/GeeC9TQp5Mq1XwUavcgc8IROBoOIpeExC+Lw6orYJrP3FBI9PKHoew5hsxtB0kuQwiT+Pa6s+GaOQemqirNj5R4BPENG+BhHvGT7OXcdQt8BR7ov94Iw669UAsLkThpDvRjqklqjAh0ttHH98JT3wSnXa8VZ6EV2CYSHCWE/Xll+Oryu/HVqHkYduiRHYpAKrJJQReKMOZKC8zMZ5n0P5eVyixkQDfzhGozMTb8PJMOIzEjHEYTc0oQ3rgHIX6+CIPwUNAPJxIYkgXMJEmrLxqLb+qP4odW5meCcHuCyd7soYKUHBMgpQ7AbrIgEGdyIgjHSCQJhyTVFoJknCQoiGwz/zXbMELtFiIgywRoji2AIZlypo/mMw77Unb4o7Szvxc3n1OFXJ0b//yyE/Y8YfAxKKIcY3oUmOKwWwLwM078UV4pFoPREkeOSbanTcEbdsHKNljpr37VqRXlybZ5oUgxtZgDTsaNy8oxlVX0bKczkUSrmZ8zJpETCMDj5AAqChSiudSScHBAk1T0vpidOUOKz1lh11PUsU9Daib0FLoJquwQ80wxYvRR5tWYGbYk85o1gqRKUat3wau3wWkZhEPl+UkkzHp+P2yDl8Sq2ExSQWKmkIAMRaJ4ftlEvPtNPbZ6ZQaD12VbreawVkQtGXeT6EWQ5/SRvHlgUcPIsqu0n6xNkNl1N/2ERJuU1EdyFjBSiLGfdibaAK/BN2gvKUTE8SM5k7oLVQ4fu5xCv1QJlDUwxF4L+5NjZ0wlImiP8EvWTGSoIXhoq36FuCN1CmS8Yzb2qQf/vagWs91Ogr9vKLV9xiRMYk4Q8BcV9n/gr/3Av2TK30An0zHZQNv1jmAmdyfkPossbJHVoAxAooR2P5GZn39JCEgCjxH8dCQIBEJhvjSKiQBoFAC3UPHRWDLxaqchLDzXEIHKHiUT4vfjRil0EOZ7zM3svNzbkFWcKoPWHRTnpQIyGrUkHGbQhZhwpLaxjUyaDYYpyAEVP+Qg2OMGeRpDbmJp4Kcm0yVF5bN+Xotfp7Ox1TSal78QbmCK8hxMOj4pwZoQgwkZIp+U+gZ6P/sVRSyWRYcyaos6jFETlR8TNtskRZFiej3EFnaFidcYoaHpaFpZUoK/mUmGxtbJykkaXipExaVCH7+j8DvOEAkX+5OUKSojNQQJgo99JwcHWQe/EqMhpT9S+UplH6SCGA1NJa3dF2Zwy392nVQQjCMibWMwpwiycblnzP+dZMFSJy/AcZf67S5Vrh/RptvidDiVtonQlKY4EwcDPmFxIZakMlcZ1CSDKZ0OfnsYcQZQPhl82JrACO1ljdpoBwOi9GyzXqq3yawvx1B+V6U0KAkRA4yQSj/iwWsaaN0Ex5ZDo7HgOAkTP6TdE7PS/kZeN0xloLLtphiTW8yk3QMVkqCSTMmTKJl8zcbgCzBIQ7yWgSTVSHUqW3GqDg4Vz4doFAr9hZ4IKUtrjyS1hWCS+MWlDUwWFrZdqqrJOgRtulEaxT7FSHwNenl0S+whxYQNsMbFz8VG/I3+LTu3SWIN8VoO9sFNv46wXwMcSzP9L5Pfi8v0KxNNVGKK42pgTIXl1g2NkqU4ETUEeYoUAhLHiqoV44naGJt6GkMQg3+kwItWa56X02v38SSZMWATFvpdEqqZvktVIb4qhWWknK6U5HWFUkx2UskS6HNbYA3Qf3RR2Em2pYyvn6QhwaSZI+qLYzhkU2CjbU3spyhUPQmvwUw4SVl4fjBWdAhScTo4Vk62X+JH2il15aVxIX5HR9+VR40sKpMVY9HCJJUUIcF8kqK9pN6ZlNyVG4Vibx3jIs7PyQJlc0SnxYPMCFllVTJjwkj1J8WDvPTrMNttSTDeZBV1SmGc60mSKO0IfgkhRXGF428lcbTCyfgy08cNFv5OX9LKpsZoC4WCgihn4Jjo6Z/yhJKB35cdQ2WxY5gAZUvRW9nWkBgxGpcW00ZU1TEqPjbaIM9sMx40v+J4SKW6EIXPlxdehvWnXoYufTnHhcCa8nK8aUexFcdLisikmBdi7A8MUv7bSNtksAVxKV6o3b7RWaRcMX1M+k8/CktMKrwWbZQw83v0FXdEwT2nU7m2dOO7E/QrK/snW8jycwa2Sx75tBIoEuIzBCeeia8xvxJEBLDpaOyvGUaCNk0IK/O23O8PWmhVAonsXki3Zt5k3mK/HSTproTcmhjhcBhl5ZRG0MVG8nA4B4L2UxGgKUyMR0amtupcqtBZSOjFngbmHQYNVX8SJvqMlBuW6q1MABxTEQFEmYSsm5L75fwG4yVmYa5kfhLSrcptFcZ/QnyRuCJ34WU/DhP7ZUjKAjsrXyM5oB/IAkspliP3vFX6l9yGkkqYSRKRpGqjjzJP8T3ZrU9iz0LiZOS1I5Ib9DJlzg7xXYWxZKVqk/0ApEZIvjkAb8COKPO+lhtg53ipcEiO5fXlyS0pSmYkGZXH42Nsu5Tz5WBruV6EoTzNYSBASxlhPcWg2NnEeJA8FeO1BHFlZRijT2uBFBYyyq1SiUWeM8BzCwaYeB6p1SO3iqK0WUywSt5TOSY0nhRDyok24vnzazEzQ8hYvzf15ahqjAkw0dnoBzINxYv9/4ckmTgjXcDPLOUSJenSMeVjAoZCF3SyYEQvoC1Kl6xCFm5JomJz02sJIkz6BGAxFFmqXEfAUUtYYhR2TR5FEXmZlEAmgZDnEmVrXalsZIxKPXomSIIufR62qJHOJCREeBONIayPSVpOwXRAN7CRdcnARuBzMDFF+CfGJEsHjViCbAPBjIMr9ZVNBDLZyMHIf2UjhijPJbMPehpdakTbI7w2E6Q8uUATa4VmpKa43N6IMQiEXGSG/TBKCV6bLG8UYzBZs12yWDJd+lgWpfAPx8vEDgjoSznWuIlsk04iJZLlESUBdFnxLuRIlvaoTG5xJi4zv0sqQzZOssVzGDl+RlnFSZuYGFCyQFMKi4iSkFs1OgK2qnOyJQLw6cWazIvi31pQueigIQJElE6XYn+kmpVsQxplYEg1KxtVY1QWEwnwGf10HAZPlAHOq+hMEZ7DzD4SgC1DDASGNsdT3DPKRKltRMODnsB20848L7OUFnhyfZUERJ4pkdLLYjBRIOLQsiEHW6ORRHlcSlxDPIOm4SHgZCLLF7rCPoudmah1VAOigOVxVL7E5M/v8vvaRiQx9onnlvrgIPGRTTTSFuQffk/hNUhb+VkSSbZeT/VjMI7wczYmRgKj1LvgtcP0fSt9Q6pYSqlNxUQbU3rL3TM9QYFOxCZwZASUeR47E6hiYiIga5ca7Nr2nwLWYmkaIc6EI1XPZEMOeYJAKwNKQiAzbGCSslF9ygiFqPZSPKeJNpC7iQYpq8oxFmKc3tBKho8JQyd1vSSppG0iZU+1Ov0EpQQTriRzC68ri7jkdqgU5+KXMMRBzyRBiPFfK1+Li+KwD9EXqJgY52qCICEV29gXa5zEh/EYtghwSUKXAlVsiVyLvqPIQLE9RjqapCpZNyR5Q2qO2Xke+V5E9ilko21MTFGjED1GKfuv8hpSgtpKUNATXFSTlf0hmeUpaU2SLeYGAUHaUIsZArMpypFmTpESyiJI5GmfJJWxlL2Vcq/SxQSJqKhsqyLxxT+0ctgaZlwwBmljESxh5hZrJB3/kkO0bMafDbIWhYCkGALsR5QZJYNJmWrXSIAmWAoAyr4QsleDkGyp4ie5QzbBkohjuKKppAzrbngA2/LGE8hFyAh5jNC3hAixgfQhAUETc2KEPpYwjPAc/Jwkf7F5kqo9Tj+jClUNJMsRB+2nIGgNEaQdvJYrfT4z20x/zqGyJNYgZLJpNjdwbOwc3zjjL0SlKvuKWEWZW32IknTKDJvskaEn6ESYR2QeQKpGCrGyyJ4g9OOALYufi5E4iX+bOfZU6rS9niRFVHI0SR8x+Ek0GJXa5wh+zIMidmScpHS3jq9J9PoopLTrCX5ovslkx9gxaIvv+KP8zXaIX/MX7WeBXCH2IthEbEifbBQ/QhIVmTVk7Iqf2aS0uJYDorQ18yBtnTATg3ge8UXKBvpPehbbyDGT7cy1tCK4IY8rSp7g56T/QspMKgmYUaozKvRLihn2J0F/CRndzNEUduyzUWJQURCyZcJF8sDTa222EfeF8Es5XjvzeFCfpc2GW0mWTCSUKcaREMck85SQE4YXMYefJ2G2c3xEaEdlHR7tRlTiuIodBTuklLAQXREM9Bw6meQ5yXeSs6XKopBj2UdC/DYuxYGYwyRGRXgOk3QURtvx5hVzMCOHsZiMqanmte9pNbt1VJna7lGaKdKH7K6n56CrDCgxhCzkk4UEkoAkNQtzEWknu+kJyLNX8iWehyNApqulc15YpXML2zTJxi0EbRlYbfcqJkeVrEN7NIiBr+f7CSkly/Oxt5rh+QPPIwlPVISUr+TnTU6ez09jMtkxKcs5ZYB5MbJxtpUBBSYDmRJUqE4kQNMuxnNKIqbDGei4SXkEQsgKE4X0i5mZ40dSQKPrqFSkbKKQENnhT/rPgeB3eBaZGRHSQPYnm8GYCKYJGkU2J5G9DVQGl8x6GNleKXkplaCEqqR36eLI8VJiVCGBKo3KRnBwaFgClkrFJfemZIZFdkZL0FFlYx0DFZMoBQ3Y6MBISQKQc6TvL8rqVlmBKsZPUkmKJUVxmbSZgigdVlaCCzgwETLRxly0GZO6zPJo/eG48ML8w8aJjXl+AeiUfpDtZt+YmHRS51/UEpltin0UwpKkfXVMhICP52GCFjJHe8sTHyaF78uqXTq07IWtwbkUn2D0ilIQe9AZ+LLYiP3QzC0qip8T8JAXCMpJ2kSmkmWamx/gyPD6GoCSRHCsmLLS/smgF3+UfuiTLraBgS7Mmn1OMIAl5xpUF0kswctG2zApm0giJBgTUs2LPqQXNU8AEDsZOb6qBiy0INtpkMRCZS5JWwJNfFh8Tx6N1ZQfY0BPoE0yMJMWkgeOn+yuKHNJ8piNXuJLGiqJg21XzYwT9k/sLSWjYxaSr5AQJX6EnxcCJL0SfxUxna5rnu6pNi9IcpnSQERm4GS8RJsIsRK/0mngyKuQDAg5YJxIpxgTsohI/E9UjLiexFJC7+ZrknBF9fIKVDUWhWRH4kcSlayE1uJcfEJmTJhUFDEdfZRsTMsJbLBmMyGJ9AU6NceN42xmu6I8P8dK25tcEpWsROfYyvUk7kT9JxgLcuvCpKl8KmOrkHr2iD6sbZRFQEkI2DHp05zaUAi5k2sLIIhCVdlPAXPSSFqF52J76Dm0r9gyzHMxnuIZ2vmTJj9PIpmJ7YJD+zmlC9LXaC+SDdl9k0NIO4rjSB6i/zO+E8wZIky0nePoQ9IvyWUy8LJhleQMf3UZ9udR8XPMZNhVflY2z9JsJ4bVbKtqawAkdoVYi/qWKqmykZRsxJKkIcz8nkYxeF0zOylxEmGj5Bq0KBTJlWy/nE70rswUOul3UeZCIfzaDpL8vE7IK88XY74gRNH2kpE4Bjyn7KYpTic742k7xvE7zLYaiZTHv038npBdyjb+LfEnFpaZKH5GVrtzjBUT8x3HSfxYdrTTdnXkz2IbIW2y+ZC0XhRpOrvxfZ6HLdNylVzfLvlE7MQxEE+XseGwk9xR5vE9XknrewZZDvEOQfqgbF5k5XiNMBeb6T/SBlqIQM341fKJrEfgGHIsZMZA8Ew2QYqxj3J7QMiHWFb8Q/BDhIjYLEwXFRph41gmRVwxZmQDKdkoTmaNZMQtfD1A3JTV9VqZb+mhtJPnl1lu6ZhQD3ZDy3tyHclgcil6DH8SG8jf7Ct9l/xevsDxY7v4onxaQkE2Dkr7ME9J/5ZbRRxo9kFP29AmGqFKEfQlZzEG+J6c1UzMjIpg0ki5iI0I5leXIsdswf8DgaWw4caYR2oAAAAASUVORK5CYII='
where AccountId='f59b41be-91da-41ac-b958-ab34ab57f1c6'
GO
-- Savita Traders Laser Print Changes
-- ----------------------------------
GO
UPDATE SaleSettings SET PrintCustomFields = '
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"7%"},
{"field":"i/u","enable":1,"width":"3%"},
{"field":"batch","enable":1,"width":"6%"},
{"field":"expiry","enable":1,"width":"5%"},
{"field":"qty","enable":1,"width":"3%"},
{"field":"mrp","enable":1,"width":"5%","title":"Rate"},
{"field":"maxmrp","enable":1,"width":"5%","title":"MRP"},
{"field":"discount","enable":1,"width":"4%"},
{"field":"cgst%","enable":1,"width":"4%"},
{"field":"cgstamt","enable":1,"width":"6%"},
{"field":"utgst/sgst%","enable":1,"width":"4%"},
{"field":"utgst/sgstamt","enable":1,"width":"6%"},
{"field":"igst%","enable":1,"width":"5%"},
{"field":"igstamt","enable":1,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":1,"width":"6%"},
{"field":"amount","enable":1,"width":"9%"},

{"field":"doctorname","enable":0},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":1},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1},
{"field":"courierfees","enable":0},
{"field":"pharmacyOfficialNote","enable":0}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "3%", "3%", "6%", "5%", "5%", "6%", "5%", "6%", "5%", "6%", "5%", "6%", "8%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "I/U",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "MRP",
				"alignment": "center"
			}, {
				"text": "Disc%",
				"alignment": "center"
			}, {
				"text": "CGST%",
				"alignment": "center"
			}, {
				"text": "CGST Amt",
				"alignment": "center"
			}, {
				"text": "@sgstOrUtgstFlagPerc%",
				"alignment": "center"
			}, {
				"text": "@sgstOrUtgstFlagAmt Amt",
				"alignment": "center"
			}, {
				"text": "IGST%",
				"alignment": "center"
			}, {
				"text": "IGST Amt",
				"alignment": "center"
			}, {
				"text": "GST%",
				"alignment": "center"
			}, {
				"text": "GST Amt",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],
			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@packageSize",
				"alignment": "left"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@maxMRP",
				"alignment": "right"
			}, {
				"text": "@productDiscount",
				"alignment": "right"
			}, {
				"text": "@productCgstPerc",
				"alignment": "right"
			}, {
				"text": "@productCgstAmt",
				"alignment": "right"
			}, {
				"text": "@productSgstPerc",
				"alignment": "right"
			}, {
				"text": "@productSgstAmt",
				"alignment": "right"
			}, {
				"text": "@productIgstPerc",
				"alignment": "right"
			}, {
				"text": "@productIgstAmt",
				"alignment": "right"
			}, {
				"text": "@gstPerc",
				"alignment": "right"
			}, {
				"text": "@productGstAmt",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["0%", "100%"],
			"body": [
				[{}, {
					"text": "@pharmacyName",
					"alignment": "center",
					"style": {
						"bold": true,
						"fontSize": 20
					}
				}],
				[{}, {
					"text": "@pharmacyAddressArea",
					"alignment": "center"
				}],
				[{}, {
					"text": "@pharmacyCityStatePincode",
					"alignment": "center"
				}],
				[{}, {
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "center"
				}],
				[{}, {
					"text": "DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo",
					"alignment": "center"
				}]
			]
		},
		"layout": "leftTopRightBottomLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["72%", "28%"],
		"body": [
			[{
				"text": "Customer Name  : @customerName"
			}, 
			{
				"text": "Bill No       : @billNo"
			}],
			[{
				"text": "Address                : @customerAddress"
			}, 
			{
				"text": "B Date       : @billDateTime"
			}],
			[{
				"text": "Mobile                   : @customerMobile"
			}, 
			{
				"text": "B Type       : @invoiceType"
			}],
			[{
				"text": "GSTIN No              : @customerGSTinNo"
			}, 
			{				
			}]
		]
	},
	"layout": "leftRightLayout"
}
	],
	"defaultStyle": {
		"fontSize": "10",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": "14"
		},
		"tableStyle": {
			"fontSize": "8"
		}
	}
}
}'
WHERE AccountId = '7cc19e20-a6f5-4852-ae7c-c050f516a668'
GO
-- ZI Clinic Laser Print Changes
-- -----------------------------
GO
Update SaleSettings set PrintCustomFields='
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":0,"width":"10%"},
{"field":"manufacturer","enable":1,"width":"10%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"5%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
],

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "*", "10%", "8%", "10%", "5%", "10%"],
		"body": [
			[{
				"text": "NO.",
				"alignment": "left",
				"style": {
						"bold": true
					}
			}, {
				"text": "DESCRIPTION",
				"alignment": "left",
				"style": {
						"bold": true
					}
			}, {
				"text": "Mfr.",
				"alignment": "left",
				"style": {
						"bold": true
					}
			}, {
				"text": "Batch",
				"alignment": "left",
				"style": {
						"bold": true
					}
			}, {
				"text": "Exp.",
				"alignment": "left",
				"style": {
						"bold": true
					}
			}, {
				"text": "UNIT PRICE",
				"alignment": "left",
				"style": {
						"bold": true
					}
			}, {
				"text": "QTY",
				"alignment": "left",
				"style": {
						"bold": true
					}
			},  {
				"text": "SUBTOTAL",
				"alignment": "left",
				"style": {
						"bold": true
					}
			}],

			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@manufacturer",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "left"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			},  {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"termsConditions":{
"style": "tableStyle",
	"table": {
		"widths": ["100%"],
		"body": [
		[{
					"style": {
						"bold": true,
						"fontSize": 12
					},
					"text": " Terms & Conditions ",
					"alignment": "left"
				}],
			[{
			"stack":[{
			"style": {
						"fontSize": 8
					},
				"ul":[
				"   Payment must be made in full. "	,
				" Goods once Sold will not be taken back. "	,
				"   All consumables and drugs charges are subject to GST at the current rate.  "	,
				"Amount paid once is non-refundable. ",
				" Dispute if any will be subject to sellers court jurisdiction. "
				]	
				}]		
			}],
			[{
					"style": {
						"bold": true,
						"fontSize": 12
					},
					"text": "  Bank Details ( For NEFT / Cheques)  ",
					"alignment": "left"
				}],
			[{
			"stack":[{
			"style": {
						"fontSize": 8
					},
				"ul":[
				"   Acc Name : Sethu Healthcare LLP  "	,
				"  Acc Number : 602805020237  "	,
				"  Bank Name : ICICI Bank  "	,
				"Branch:Alwarpet,Chennai  ",
				"  IFSC: ICIC0006028  "
				]	
				}]		
			}]	,	
			[{
				"text": "@printFooterNote",
				"alignment": "center"
				}]	
		]
	},
	"layout": "leftRightBottomLayout"

},

"paymentDescription":{
	"table": {
		"style": "tableStyle",
		"widths": ["35%", "35%","*"],
		"body": [
			[{
				"text": "PAYMENT DATE",
				"style": {
						"bold": true,
						"fontSize": 8
					}
			},{
			"text": "DESCRIPTION",
				"style": {
						"bold": true,
						"fontSize": 8
					}
					},{
					"text": "AMOUNT OUTSTANDING:@custBalanceAmt",
					"alignment":"right",
				"style": {
						"bold": true,
						"fontSize": 8
					}
					}], 
			
				[{},{},{}],		
				[{},{},{}],
				[{},{},{}]
		]
	},
	"layout": "leftRightBottomLayout"

},
	
"saleFooterDetails":{
	"table": {
		"style": "tableStyle",
		"widths": ["35%", "25%", "40%"],
		"body": [
			[{}, {}, {}],
			[{"columns":[{
				"text": "@totalCgstAmt",
				"alignment": "left"},{
				"text": "@totalSgstAmt @totalIgstAmt",
				"alignment": "right"}
			]}, {
				"text": "GST Amount: @totalGstAmt",
				"alignment": "right"
			}, {
				"text": "Total: @totalSaleAmt",
				"alignment": "right"
			}],
			[{
				"colSpan": 2,
				"text": "Rs: @rupeesInWords "
			}, {}, {
				"text": "Discount: @totalDiscountAmt",
				"alignment": "right"
			}],
			[{}, {}, {
				"text": "Round Off: @roundoffAmt",
				"alignment": "right"
			}],
			[{}, {}, {
				"text": "Net Amount: @netAmtPayable",
				"alignment": "right",
				"style": "header"
			}]
		]
	},
	"layout": "leftRightBottomLayout"
},
"saleFooterDetailsWithReturn":{
	"table": {
		"style": "tableStyle",
		"widths": ["35%", "30%", "35%"],
		"body": [
			[{}, {}, {}],
			[{"columns":[{
				"text": "@totalCgstAmt",
				"alignment": "left"},{
				"text": "@totalSgstAmt @totalIgstAmt",
				"alignment": "right"}
			]}, {
				"text": "GST Amount: @totalGstAmt",
				"alignment": "right"
			}, {
				"text": "Total: @totalSaleAmt",
				"alignment": "right"
			}],
			
			[{"colSpan": 2,
				"text": "Rs: @rupeesInWords "
				}, {}, {
				"text": "Discount: @totalDiscountAmt",
				"alignment": "right"
			}],
			[{}, {}, {
				"text": "Round Off: @roundoffAmt",
				"alignment": "right"
			}],
			[{}, {}, {
				"text": "Return Amount: @netReturnAmt",
				"alignment": "right",
				"style": "header"
			}],
			[{}, {}, {
				"text": "Net Amount Payable: @netAmtPayable",
				"alignment": "right",
				"style": "header"
			}]
		]
	},
	"layout": "leftRightBottomLayout"
},

"pdfDesign":{
	"pageSize": "@pageSize",
	"pageOrientation": "@pageOrientation",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["10%", "60%","30%"],
			"body": [
				[{
					
				}, {
					"text": "@pharmacyName",
					"alignment": "center",
					"style": {
						"bold": true,
						"fontSize": 15
					}
				},{
				"text": "Bill To","style": {
						"bold": true,
						"fontSize": 15
					}
					}],
				[{}, {
					"text": "@pharmacyAddressArea",
					"alignment": "center"
				},
				{"text": "Patient Name   : @customerName"}],
				[{}, {
					"text": "@pharmacyCityStatePincode",
					"alignment": "center"
				},
				{"text": "Patient File No :@customerEmpIDNo"}],
				[{}, {
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "center"
				},
				{"text": "Mobile               : @customerMobile"}],
				[{}, {
					"text": "DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo",
					"alignment": "center"
				},{}]
			]
		},
		"layout": "leftTopRightBottomLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["70%", "30%"],
		"body": [
			[{
				"text": "DATE       : @billDate",
				"style": {
						"bold": true,
						"fontSize": 8
					}
			}, 
			{
				"text": "INVOICE NO      : @billNo",
				"style": {
				
						"bold": true,
						"fontSize": 8
					}
			}]	,
			[{
				"text": "Doctor     : @doctorName"
			},{}]		
		]
	},
	"layout": "leftRightLayout"

}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": 12
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}' 
where AccountId='e19e49df-f3f7-4424-83df-e689839a65c4'
GO
-- SA Custom Settings
-- ------------------
GO
DELETE FROM CustomSettings WHERE GroupId = 1
GO
INSERT INTO CustomSettings([Id], [GroupId], [GroupName], [CreatedAt], [UpdatedAt], [CreatedBy], [UpdatedBy])
VALUES(NEWID(), 1, 'Barcode Management', GETDATE(), GETDATE(), 'admin', 'admin')
GO
DELETE FROM CustomSettings WHERE GroupId = 2
GO
INSERT INTO CustomSettings([Id], [GroupId], [GroupName], [CreatedAt], [UpdatedAt], [CreatedBy], [UpdatedBy])
VALUES(NEWID(), 2, 'Sales Templates', GETDATE(), GETDATE(), 'admin', 'admin')
GO
-- Total Default Sms Count config script
-- -------------------------------------
GO
DELETE FROM DomainMaster WHERE Id = 4
GO
INSERT INTO DomainMaster(Id,Code,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(4, 4,'Total Default Sms Count', GETDATE(), GETDATE(), 'admin', 'admin')
GO
DELETE FROM DomainValues WHERE DomainId = 4
GO
INSERT INTO DomainValues(Id,DomainId,DomainValue,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(12, 4, 1000, 'Total Default Sms Count', GETDATE(), GETDATE(), 'admin', 'admin')
GO
UPDATE Instance SET TotalSmsCount = 1000 WHERE TotalSmsCount IS NULL
GO
INSERT INTO SmsConfiguration(Id,AccountId,InstanceId,Date,SmsCount,IsActive,OfflineStatus,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy)
SELECT NEWID(), AccountId, Id, CAST(GETDATE() AS DATE), 1000, 1, 0, GETDATE(), GETDATE(), 'admin', 'admin' FROM Instance WHERE Id NOT IN(SELECT InstanceId FROM SmsConfiguration)
GO
*/
-- V64 Post deploy script - start
-- ------------------------------
GO
-- Markup changes in report
-- ------------------------
GO
UPDATE [dbo].[CustomDevReport] SET 
[Query] = 'select 
product.Name AS [Product Name]
,isnull(vendorpurchase.BillSeries,'''') + vendorpurchase.GoodsRcvNo  as [GRN]
,vendorpurchase.InvoiceNo as [Invoice No]
,vendorpurchase.InvoiceDate as [Invoice Date]
,ISNULL(vendorpurchaseitem.MarkupPerc,0) as [MarkupPerc]
,ISNULL(vendorpurchaseitem.Discount,0) as [Discount]
,ISNULL(vendorpurchaseitem.SchemeDiscountPerc,0) as [SchemeDiscountPerc]
,vendorpurchaseitem.FreeQty as [Free Qty]
,vendorpurchaseitem.packagepurchaseprice [Purchase Price]
,vendorpurchaseitem.packagesellingprice as [Selling Price]
,vendorpurchaseitem.PackageQty as [Package Qty]
,vendorpurchaseitem.PackageSize as [Package Size]
,vendorpurchase.PaymentType as [Payment Type]
,productstock.BatchNo as [Batch No]
,productstock.ExpireDate as [Expire Date]
,CASE ISNULL(vendorpurchase.TaxRefNo,0) WHEN 1 THEN vendorpurchaseitem.GstTotal ELSE vendorpurchaseitem.VAT END as [VAT]
,(vendorpurchaseitem.PurchasePrice*vendorpurchaseitem.Quantity) as [Total Amount]
, ( Isnull(vendorpurchaseitem.packageqty, 0) - 
             Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
             vendorpurchaseitem.packagesellingprice, 0) as [MRP Value]
,Round((( Isnull(vendorpurchaseitem.packageqty, 0) - 
             Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
             vendorpurchaseitem.packagesellingprice, 0)) - (vendorpurchaseitem.PurchasePrice*vendorpurchaseitem.Quantity), 2)  as [Margin Amount]
,Round(( (( Isnull(vendorpurchaseitem.packageqty, 0) - 
             Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
             vendorpurchaseitem.packagesellingprice, 0)) / (vendorpurchaseitem.PurchasePrice*vendorpurchaseitem.Quantity) * 100 ) - 100, 2) [Profit]
from vendorpurchaseitem vendorpurchaseitem
join vendorpurchase vendorpurchase on vendorpurchase.Id = vendorpurchaseitem.VendorPurchaseId
join productstock on productstock.Id = vendorpurchaseitem.ProductStockId  
join product on product.id = productstock.productid
/*
CROSS apply (SELECT ( ( 
                                   Isnull( 
                         vendorpurchaseitem.packageqty, 0) 
                                   - 
                                             Isnull( 
                                 vendorpurchaseitem.freeqty, 0 
                                                                        ) 
                                                                        ) * 
                                 vendorpurchaseitem.packagepurchaseprice ) - 
                                                            ( 
                                 ( ( 
                                 Isnull( 
                                 vendorpurchaseitem.packageqty, 0) 
                                 - 
                                                      Isnull( 
                                 vendorpurchaseitem.freeqty, 0) ) * 
                                 vendorpurchaseitem.packagepurchaseprice ) * 
                                 Isnull 
                                 ( 
                                 vendorpurchaseitem.discount, 0 
                                 ) / 
                                 100 ) + ( ( ( ( Isnull( 
                                             vendorpurchaseitem.packageqty, 
                                                 0) - 
             Isnull(vendorpurchaseitem.freeqty, 0) ) * 
             vendorpurchaseitem.packagepurchaseprice ) - ( 
             ( ( 
             Isnull(vendorpurchaseitem.packageqty, 0) - 
                 Isnull(vendorpurchaseitem.freeqty, 0) 
             ) * 
               vendorpurchaseitem.packagepurchaseprice 
             ) * 
             Isnull( 
               vendorpurchaseitem.discount, 0) / 100 ) 
             ) * ( 
             Isnull( 
             productstock.vat, 0) / 100 ) ) TotalPo, 
             ( Isnull(vendorpurchaseitem.packageqty, 0) - 
             Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
             vendorpurchaseitem.packagesellingprice, 0) MrpVal, 
             Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * 
             ( Isnull (vendorpurchaseitem.packageqty, 0) - 
             Isnull ( 
             vendorpurchaseitem.freeqty, 0) ) - ( Isnull( 
             vendorpurchaseitem.packagepurchaseprice, 0) * 
             ( 
             Isnull (vendorpurchaseitem.packageqty, 0) 
             - 
                                    Isnull ( 
             vendorpurchaseitem.freeqty, 0) ) * Isnull ( 
             vendorpurchaseitem.discount, 
                  0) / 
                  100 )   Poval) AS PoCal
				  */

where (vendorpurchaseitem.FreeQty > 0  OR vendorpurchaseitem.Discount > 0)
and vendorpurchaseitem.InstanceId = [##] and Convert(date,vendorpurchaseitem.CreatedAt) 
between  [#dt@From#] and [#dt@To#] AND isnull(VendorPurchase.CancelStatus ,0) =0',
ConfigJSON = '{"selectedFields":[{"Id":"Product Name","Label":"Product Name","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"GRN","Label":"GRN","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Invoice No","Label":"Invoice No","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Invoice Date","Label":"Invoice Date","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"MarkupPerc","Label":"Markup %","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Discount","Label":"Discount %","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"SchemeDiscountPerc","Label":"Scheme Discount %","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Free Qty","Label":"Free Qty","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Purchase Price","Label":"Purchase Price","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Selling Price","Label":"Selling Price","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Package Qty","Label":"Package Qty","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Package Size","Label":"Package Size","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Payment Type","Label":"Payment Type","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Batch No","Label":"Batch No","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Expire Date","Label":"Expire Date","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"VAT","Label":"VAT/GST%","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Total Amount","Label":"Total Amount","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"MRP Value","Label":"MRP Value","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Margin Amount","Label":"Margin Amount","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false},{"Id":"Profit","Label":"Profit","Type":"","Size":15,"IsGrouped":false,"Aggregate":null,"IsShowTotal":false}],"filter":null,"reportName":"Purchase Free and Discount","reportType":1}'
WHERE [ReportName] = 'Purchase Free and Discount'
GO
-- Vendor Return Item GstTotal data batch
-- --------------------------------------
GO
UPDATE VRI SET VRI.GstTotal = CASE WHEN ISNULL(PS.GstTotal,0) != 0 THEN PS.GstTotal ELSE ISNULL(VPI.Gsttotal,0) END FROM VendorReturn VR
INNER JOIN VendorReturnItem VRI ON VR.Id = VRI.VendorReturnId
INNER JOIN ProductStock PS ON PS.Id = VRI.ProductStockId
LEFT JOIN VendorPurchase VP on VP.Id = VR.VendorPurchaseId
LEFT JOIN VendorPurchaseItem VPI on VPI.ProductStockId = VRI.ProductStockId and VPI.InstanceId = VR.InstanceId and VPI.VendorPurchaseId = VP.Id
WHERE VR.TaxRefNo = 1 AND CASE WHEN ISNULL(PS.GstTotal,0) != 0 THEN PS.GstTotal ELSE ISNULL(VPI.Gsttotal,0) END != ISNULL(VRI.GstTotal, 0)
GO
UPDATE VRI SET VRI.GstTotal = 0 FROM VendorReturn VR
INNER JOIN VendorReturnItem VRI ON VR.Id = VRI.VendorReturnId
WHERE VR.TaxRefNo = 1 AND VRI.GstTotal IS NULL
GO
UPDATE VRI SET VRI.GstAmount = CAST((VRI.ReturnPurchasePrice*VRI.Quantity*VRI.GstTotal)/(100+VRI.GstTotal) AS DECIMAL(18,6)) FROM VendorReturn VR
INNER JOIN VendorReturnItem VRI ON VR.Id = VRI.VendorReturnId
WHERE VR.TaxRefNo = 1 AND CAST((VRI.ReturnPurchasePrice*VRI.Quantity*VRI.GstTotal)/(100+VRI.GstTotal) AS DECIMAL(18,6)) != ISNULL(VRI.GstAmount, 0)
GO
-- Transaction order execution for avoiding deadlocks
-- --------------------------------------------------
GO
DELETE FROM [dbo].[TransactionOrder]
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('AlternateVendorProduct', 'UPDATE AlternateVendorProduct SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM AlternateVendorProduct WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('BarcodePrnDesign', 'UPDATE BarcodePrnDesign SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM BarcodePrnDesign WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('BarcodeProfile', 'UPDATE BarcodeProfile SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM BarcodeProfile WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('BatchListSettings', 'UPDATE BatchListSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM BatchListSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('BillPrintSettings', 'UPDATE BillPrintSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM BillPrintSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('CardTypeSettings', 'UPDATE CardTypeSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM CardTypeSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('CommunicationLog', 'UPDATE CommunicationLog SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM CommunicationLog WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('CustomDevReport', 'UPDATE CustomDevReport SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM CustomDevReport WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('CustomerPayment', 'UPDATE CustomerPayment SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM CustomerPayment WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('CustomReport', 'UPDATE CustomReport SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM CustomReport WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('CustomSettingsDetail', 'UPDATE CustomSettingsDetail SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM CustomSettingsDetail WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('DCVendorPurchaseItem', 'UPDATE DCVendorPurchaseItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM DCVendorPurchaseItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('DefaultDoctor', 'UPDATE DefaultDoctor SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM DefaultDoctor WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('DeliveryBoy', 'UPDATE DeliveryBoy SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM DeliveryBoy WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('DiscountRules', 'UPDATE DiscountRules SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM DiscountRules WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Doctor', 'UPDATE Doctor SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Doctor WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('DomainMaster', 'UPDATE DomainMaster SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM DomainMaster WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('DomainValues', 'UPDATE DomainValues SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM DomainValues WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('DraftVendorPurchase', 'UPDATE DraftVendorPurchase SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM DraftVendorPurchase WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('DraftVendorPurchaseItem', 'UPDATE DraftVendorPurchaseItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM DraftVendorPurchaseItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('ErrorLog', 'UPDATE ErrorLog SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM ErrorLog WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Estimate', 'UPDATE Estimate SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Estimate WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('EstimateItem', 'UPDATE EstimateItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM EstimateItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('FinYearMaster', 'UPDATE FinYearMaster SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM FinYearMaster WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('FinYearResetStatus', 'UPDATE FinYearResetStatus SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM FinYearResetStatus WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('HQueUser', 'UPDATE HQueUser SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM HQueUser WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('InventoryReOrder', 'UPDATE InventoryReOrder SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM InventoryReOrder WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('InventorySettings', 'UPDATE InventorySettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM InventorySettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('InventorySmsSettings', 'UPDATE InventorySmsSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM InventorySmsSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('InventoryUpdateHistory', 'UPDATE InventoryUpdateHistory SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM InventoryUpdateHistory WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('InvoiceSeries', 'UPDATE InvoiceSeries SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM InvoiceSeries WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('InvoiceSeriesItem', 'UPDATE InvoiceSeriesItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM InvoiceSeriesItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Leads', 'UPDATE Leads SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Leads WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('LeadsProduct', 'UPDATE LeadsProduct SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM LeadsProduct WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('LoginHistory', 'UPDATE LoginHistory SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM LoginHistory WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('MissedOrder', 'UPDATE MissedOrder SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM MissedOrder WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('OrderSettings', 'UPDATE OrderSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM OrderSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Patient', 'UPDATE Patient SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Patient WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PatientOrder', 'UPDATE PatientOrder SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PatientOrder WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PatientOrderInstanceStatus', 'UPDATE PatientOrderInstanceStatus SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PatientOrderInstanceStatus WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Payment', 'UPDATE Payment SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Payment WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PettyCash', 'UPDATE PettyCash SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PettyCash WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PettyCashDtl', 'UPDATE PettyCashDtl SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PettyCashDtl WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PettyCashHdr', 'UPDATE PettyCashHdr SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PettyCashHdr WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PettyCashSettings', 'UPDATE PettyCashSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PettyCashSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PharmacyPayment', 'UPDATE PharmacyPayment SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PharmacyPayment WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PharmacyTiming', 'UPDATE PharmacyTiming SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PharmacyTiming WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PharmacyUploads', 'UPDATE PharmacyUploads SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PharmacyUploads WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PhysicalStockHistory', 'UPDATE PhysicalStockHistory SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PhysicalStockHistory WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Product', 'UPDATE Product SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Product WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('ProductInstance', 'UPDATE ProductInstance SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM ProductInstance WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('ProductMaster', 'UPDATE ProductMaster SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM ProductMaster WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('ProductSearchSettings', 'UPDATE ProductSearchSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM ProductSearchSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('ProductStock', 'UPDATE ProductStock SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM ProductStock WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('PurchaseSettings', 'UPDATE PurchaseSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM PurchaseSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('ReminderProduct', 'UPDATE ReminderProduct SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM ReminderProduct WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('ReminderRemark', 'UPDATE ReminderRemark SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM ReminderRemark WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('ReplicationData', 'UPDATE ReplicationData SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM ReplicationData WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Requirement', 'UPDATE Requirement SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Requirement WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Sales', 'UPDATE Sales SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Sales WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesBatchPopUpSettings', 'UPDATE SalesBatchPopUpSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesBatchPopUpSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SaleSettings', 'UPDATE SaleSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SaleSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesItem', 'UPDATE SalesItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesItemAudit', 'UPDATE SalesItemAudit SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesItemAudit WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesOrderEstimate', 'UPDATE SalesOrderEstimate SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesOrderEstimate WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesOrderEstimateItem', 'UPDATE SalesOrderEstimateItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesOrderEstimateItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesPayment', 'UPDATE SalesPayment SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesPayment WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesReturn', 'UPDATE SalesReturn SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesReturn WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesReturnItem', 'UPDATE SalesReturnItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesReturnItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesReturnItemAudit', 'UPDATE SalesReturnItemAudit SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesReturnItemAudit WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesTemplate', 'UPDATE SalesTemplate SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesTemplate WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesTemplateItem', 'UPDATE SalesTemplateItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesTemplateItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SalesType', 'UPDATE SalesType SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SalesType WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SchedulerLog', 'UPDATE SchedulerLog SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SchedulerLog WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SelfConsumption', 'UPDATE SelfConsumption SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SelfConsumption WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Settings', 'UPDATE Settings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Settings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Settlements', 'UPDATE Settlements SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Settlements WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SmsConfiguration', 'UPDATE SmsConfiguration SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SmsConfiguration WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SmsLog', 'UPDATE SmsLog SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SmsLog WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SmsSettings', 'UPDATE SmsSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SmsSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('StockAdjustment', 'UPDATE StockAdjustment SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM StockAdjustment WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('StockLedgerUpdateHistory', 'UPDATE StockLedgerUpdateHistory SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM StockLedgerUpdateHistory WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('StockLedgerUpdateHistoryItem', 'UPDATE StockLedgerUpdateHistoryItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM StockLedgerUpdateHistoryItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('StockTransfer', 'UPDATE StockTransfer SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM StockTransfer WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('StockTransferItem', 'UPDATE StockTransferItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM StockTransferItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('StockValueReport', 'UPDATE StockValueReport SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM StockValueReport WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('SyncSettings', 'UPDATE SyncSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM SyncSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('TaxSeriesItem', 'UPDATE TaxSeriesItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM TaxSeriesItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('TemplatePurchaseChildMaster', 'UPDATE TemplatePurchaseChildMaster SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM TemplatePurchaseChildMaster WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('TemplatePurchaseChildMasterSettings', 'UPDATE TemplatePurchaseChildMasterSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM TemplatePurchaseChildMasterSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('TemplatePurchaseMaster', 'UPDATE TemplatePurchaseMaster SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM TemplatePurchaseMaster WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('TempVendorPurchaseItem', 'UPDATE TempVendorPurchaseItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM TempVendorPurchaseItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Tool', 'UPDATE Tool SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Tool WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('TransferSettings', 'UPDATE TransferSettings SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM TransferSettings WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('UserReminder', 'UPDATE UserReminder SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM UserReminder WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Vendor', 'UPDATE Vendor SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Vendor WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('VendorOrder', 'UPDATE VendorOrder SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM VendorOrder WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('VendorOrderItem', 'UPDATE VendorOrderItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM VendorOrderItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('VendorPurchase', 'UPDATE VendorPurchase SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM VendorPurchase WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('VendorPurchaseFormula', 'UPDATE VendorPurchaseFormula SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM VendorPurchaseFormula WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('VendorPurchaseItem', 'UPDATE VendorPurchaseItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM VendorPurchaseItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('VendorPurchaseItemAudit', 'UPDATE VendorPurchaseItemAudit SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM VendorPurchaseItemAudit WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('VendorReturn', 'UPDATE VendorReturn SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM VendorReturn WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('VendorReturnItem', 'UPDATE VendorReturnItem SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM VendorReturnItem WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO
INSERT INTO [dbo].[TransactionOrder](TableName, LockQuery) VALUES('Voucher', 'UPDATE Voucher SET UpdatedAt = UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND Id = (SELECT TOP 1 Id FROM Voucher WHERE AccountId = @AccountId AND InstanceId = @InstanceId)')
GO

--Added by Sarubala on 23-01-18 - Total Sms Count must be 0 while creating new branch
UPDATE DomainValues set DomainValue=0 where DomainId=4
GO
-- Roundoff dependency report(Sales Modified Details Report)
-- ---------------------------------------------------------
GO
UPDATE CustomDevReport SET Query = N'Select 
name as[Modified by],
UpdatedAt As [Modified date],
UpdatedTime as [Modified Time],
InvoiceNo as [Bill No],
InvoiceDate AS [Bill Date],
prodname as [Product Name],
BatchNo [Batch No],
Expire as [Expiry],
Quantity as [Qty],
case when TaxRefNo = 1 then 0 else VAT end as [VAT],case when TaxRefNo = 1 then GstTotal else 0 end as [GST],
PurchasePrice AS [COST],
[MRP],
[Status] from 
(
	SELECT 
	ISNULL(S.TaxRefNo,0) AS TaxRefNo,
	u.name,
	si.UpdatedAt,
	substring(convert(varchar(20), si.UpdatedAt, 9), 13, 5) + '' '' + substring(convert(varchar(30), si.UpdatedAt, 9), 25, 2) as UpdatedTime,
	S.InstanceId,
	Isnull(S.InvoiceSeries+'' '','' '') +S.InvoiceNo as InvoiceNo,
	S.InvoiceDate,
	P.Name prodname, 
	PS.BatchNo,
	PS.ExpireDate as Expire,
	SI.Quantity,
	SI.VAT,
	ISNULL(SI.GstTotal,0) as GstTotal,
	S.Cancelstatus,
	PS.PurchasePrice AS PurchasePrice,
	PS.ProductId,
	SI.SellingPrice AS MRP,
	(Case when S.Cancelstatus is null Then ''Modified'' else ''Deleted'' End) as Status
	FROM Sales S WITH(NOLOCK)
	Inner JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
	right JOIN SalesItemAudit SIA  WITH(NOLOCK) on S.Id= SIA.salesid and SIA.productstockid=SI.productstockid
	INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SIA.productstockid
	INNER JOIN  Product p WITH(NOLOCK) on p.Id=ps.ProductId
	INNER JOIN HQueUser u WITH(NOLOCK) On S.UpdatedBy=u.id
) sales 
WHERE sales.InstanceId = [##] AND sales.ProductId=Isnull(null,sales.ProductId)   
AND Convert(date,sales.UpdatedAt) BETWEEN [#dt@From#] AND [#dt@To#] 
AND sales.Cancelstatus is null'
WHERE ReportName = 'Sales Modified Details Report'
GO
-- Laxmi Medical Laser print
-- -------------------------
GO
UPDATE SaleSettings SET PrintCustomFields = '
{
"printCustomFields":
[
{"field":"sno","enable":1,"width":"3%"},
{"field":"product","enable":1,"width":"*"},
{"field":"hsncode","enable":1,"width":"10%"},
{"field":"i/u","enable":0,"width":"4%"},
{"field":"batch","enable":1,"width":"10%"},
{"field":"expiry","enable":1,"width":"8%"},
{"field":"qty","enable":1,"width":"5%"},
{"field":"mrp","enable":1,"width":"8%","title":"MRP"},
{"field":"maxmrp","enable":0,"width":"5%","title":"MRP"},
{"field":"discount","enable":0,"width":"4%"},
{"field":"cgst%","enable":0,"width":"4%"},
{"field":"cgstamt","enable":0,"width":"6%"},
{"field":"utgst/sgst%","enable":0,"width":"4%"},
{"field":"utgst/sgstamt","enable":0,"width":"6%"},
{"field":"igst%","enable":0,"width":"4%"},
{"field":"igstamt","enable":0,"width":"6%"},
{"field":"gst%","enable":1,"width":"5%"},
{"field":"gstamt","enable":0,"width":"7%"},
{"field":"amount","enable":1,"width":"15%"},

{"field":"doctorname","enable":1},
{"field":"custname","enable":1,"title":"Customer Name"},
{"field":"custaddr","enable":1},
{"field":"custmobile","enable":1},
{"field":"custgstin","enable":0},
{"field":"custempid","enable":0},

{"field":"totcgstamt","enable":1},
{"field":"totutgst/sgstamt","enable":1},
{"field":"totigstamt","enable":1},
{"field":"totgstamt","enable":1}
],

"termsConditions":{
"table": {
		"style": "tableStyle",
		"widths": ["100%"],
		"body": [
			[{
				"margin": [5, 0, 0, 0],
				"stack": [
					{"text":"Terms & Conditions:","style":{"bold":true,"fontSize":11}},
					{						
						"ul": [
							"Goods once sold will not be taken back or exchanged.",
							"All disputes subject to @pharmacyCity Jurisdiction only."
						],
						"margin": [10, 0, 0, 0]			
					},
					"Remarks: "
				]
			}],
			[{
				"text": "FOR @pharmacyName",
				"alignment": "right"
				}],
			[{
				"columns":[{
				"text": "@printFooterNote",
				"alignment": "right"
				},{
				"text": "(@pharmacyArea)",
				"alignment": "right",
				"margin": [0,0,25,0]}]		
			}]
		]
	},
	"layout": "leftRightBottomLayout"
},

"saleFooterDetails":{
	"table": {
		"style": "tableStyle",
		"widths": ["35%", "35%", "3%", "15%", "2%", "10%"],
		"body": [
			[{}, {}, {}, {}, {}, {}],
			[{
				"text": "@totalCgstAmt",
				"alignment": "left"},{
				"text": "@totalSgstAmt @totalIgstAmt",
				"alignment": "left"}, {}, {
				"text": "TOTAL"
			}, {
				"text": ":"
			}, {
				"text": "@totalSaleAmt",
				"alignment": "right"
			}],
			[{
				"colSpan": 2,
				"text": "Rs: @rupeesInWords "
			}, {}, {}, {
				"text": "Discount"
			}, {
				"text": ":"
			}, {
				"text": "@totalDiscountAmt",
				"alignment": "right"
			}],
			[{}, {}, {}, {
				"text": "GST Amount"
			}, {
				"text": ":"
			}, {
				"text": "@totalGstAmt",
				"alignment": "right"
			}],
			[{}, {}, {}, {
				"text": "Round Off"
			}, {
				"text": ":"
			}, {
				"text": "@roundoffAmt",
				"alignment": "right"
			}],
			[{
				"text": ""
			}, {
				"text": "",
				"alignment": "right"
			}, {}, {
				"text": "Net Amount",
				"style": "header"
			}, {
				"text": ":",
				"style": "header"
			}, {
				"text": "@netAmtPayable",
				"alignment": "right",
				"style": "header"
			}]
		]
	},
	"layout": "leftRightBottomLayout"
},

"saleFooterDetailsWithReturn":{
	"table": {
		"style": "tableStyle",
		"widths": ["35%", "32%", "3%", "18%", "2%", "10%"],
		"body": [
			[{
				"text": "@totalCgstAmt",
				"alignment": "left"},{
				"text": "@totalSgstAmt @totalIgstAmt",
				"alignment": "left"}, 
			{}, {
				"text": "TOTAL"
			}, {
				"text": ":"
			}, {
				"text": "@totalSaleAmt",
				"alignment": "right"
			}],
			[{
				"colSpan": 2,
				"text": "Rs: @rupeesInWords "
			}, {}, {}, {
				"text": "Discount"
			}, {
				"text": ":"
			}, {
				"text": "@totalDiscountAmt",
				"alignment": "right"
			}],
			[{}, {}, {}, {
				"text": "GST Amount"
			}, {
				"text": ":"
			}, {
				"text": "@totalGstAmt",
				"alignment": "right"
			}],
			[{}, {}, {}, {
				"text": "Round Off"
			}, {
				"text": ":"
			}, {
				"text": "@roundoffAmt",
				"alignment": "right"
			}],
			[{}, {}, {}, {
				"text": "Return Amount",
				"style": "header"
			}, {
				"text": ":",
				"style": "header"
			}, {
				"text": "@netReturnAmt",
				"alignment": "right",
				"style": "header"
			}],
			[{
				"text": ""
			}, {
				"text": "",
				"alignment": "right"
			}, {}, {
				"text": "Payable Amount",
				"style": "header"
			}, {
				"text": ":",
				"style": "header"
			}, {
				"text": "@netAmtPayable",
				"alignment": "right",
				"style": "header"
			}]
		]
	},
	"layout": "leftRightBottomLayout"
},

"saleDetails":{
	"style": "tableStyle",
	"table": {
		"headerRows": 1,
		"widths": ["3%", "*", "10%", "10%",  "8%", "5%", "8%", "5%", "15%"],
		"body": [
			[{
				"text": "Sno",
				"alignment": "center"
			}, {
				"text": "Product",
				"alignment": "center"
			}, {
				"text": "HSN Code",
				"alignment": "center"
			}, {
				"text": "Batch",
				"alignment": "center"
			}, {
				"text": "Exp.",
				"alignment": "center"
			}, {
				"text": "Qty",
				"alignment": "center"
			}, {
				"text": "MRP",
				"alignment": "center"
			}, {
				"text": "GST%",
				"alignment": "center"
			}, {
				"text": "Amount",
				"alignment": "center"
			}],

			[{
				"text": "@sno",
				"alignment": "right"
			}, {
				"text": "@productName",
				"alignment": "left"
			}, {
				"text": "@hsnCode",
				"alignment": "left"
			}, {
				"text": "@batchNo",
				"alignment": "left"
			}, {
				"text": "@expiry",
				"alignment": "center"
			}, {
				"text": "@saleQty",
				"alignment": "right"
			}, {
				"text": "@rate",
				"alignment": "right"
			}, {
				"text": "@gstPerc",
				"alignment": "right"
			}, {
				"text": "@total",
				"alignment": "right"
			}]
		]
	},
	"layout": "leftTopRightBottomWTHeaderColSpanLayout"
},

"pdfDesign":{
	"pageSize": "A5",
	"pageOrientation": "Landscape",
	"content": [{
		"table": {
			"style": "tableStyle",
			"widths": ["50%", "50%"],
			"body": [
				[{
					"text": "@pharmacyName",
					"alignment": "center",
					"style": {
						"bold": true,
						"fontSize": 15
					}
				},{
					"fit":[260,260],
                    "image": "@printLogo",
                    "rowSpan": 5
				}],
				[{
					"text": "@pharmacyAddressArea",
					"alignment": "center"
				}, {}],
				[{
					"text": "@pharmacyCityStatePincode",
					"alignment": "center"
				}, {}],
				[{
					"text": "PH: @pharmacyPhone, MOBILE: @pharmacyMobile",
					"alignment": "center"
				}, {}],
				[{
					"text": "DL No : @pharmacyDLNo,       @pharmacyGSTinNoOrTinNo",
					"alignment": "center"
				}, {}]
			]
		},
		"layout": "leftTopRightBottomLayout"
	},
	{
	"table": {
		"style": "tableStyle",
		"widths": ["75%", "25%"],
		"body": [
			[{
				"text": "Doctor Name       : @doctorName"
			}, 
			{
				"text": "Bill No       : @billNo"
			}],
			[{
				"text": "Customer Name  : @customerName"
			}, 
			{
				"text": "B Date       : @billDate"
			}],
			[{
				"text": "Address                : @customerAddress"
			}, 
			{
				"text": "B Time      : @billTime"
			}],
			[{
				"text": "Mobile                   : @customerMobile"
			}, 
			{
				"text": "B Type       : @invoiceType"
			}]
		]
	},
	"layout": "leftRightLayout"
}
	],
	"defaultStyle": {
		"fontSize": "@defaultFontSize",
		"bold": false
	},
	"pageMargins": [5, 5, 5, 8],
	"styles": {
		"header": {
			"bold": true,
			"fontSize": 12
		},
		"tableStyle": {
			"fontSize": "@tableFontSize"
		}
	}
}
}',
PrintLogo = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCADAApADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDw+iiiug/zfCiiigAooooAKKM103wj+Cvi749+Km0TwZ4d1TxLqkcfnywWMJk8iPON8jcKi54BYgE8DNBth8PVr1FSoxcpPZJXb9EtTmaK9U+OH7D3xY/Zw8PnWPGXgjV9J0dWCyagAtxawknCh3jLBMnj5sDJAzk15WDmi5tjcvxWDn7LFU5Ql2kmn9zCiiig4wooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooqO7vItPi8y4lht4+u6WRY1x9WIo30RUYtuyJKKzv+Ev0n/oL6Pj1+3w//FVfgf7TD5kRWaMDJaNg6j8RmnytK7RUqM4/ErDqKM/hRSMwooooAKKCcCvUPhd+xR8VfjT4KtvEXhfwbfarot48kcF0t1axrKY3KNgPKGxuU9QK83M84wOXUlWzCtClBuyc5KKb3tdtK9k3byPYybh/Ms2qSo5ZQlVlFXaim2le13bpdnl9Fe4r/wAE0fjoxx/wr3UOeOb+y/n51eIXdvPpmoXdneW1zY32n3D2l3aXMRhns504eKRG5Vx3BHcHkEGsMr4iyrMpSjl2Jp1nGzahOMrJ7Xs3Y6844RzrKaca2ZYWpSjJ2TlFpN9rvqNooor2T5wKKaZAqMzFVSMFnYnCoB1JPYe5o8OLN4zsGutDs9U8QWiyGJrjSNPn1KAOOq+ZAjpuHpuzWdStCnHnqNJd3ojsweX4rFS5MNTlNrpGLk/wTHUVe/4QrxCf+ZV8Zf8AhM6h/wDGahvvDuraPZy3V5oPiixtbdDJLPc6BewRRKBkszvEFVQOSSQAK545lhJO0asfvR6D4ZzZK7wtX/wXP/Ir0VV0jWbPXtPS60+8tNQtX+7PazrNG30ZSR+tWgciuzbc8WcJRfLJWaCiiigkKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoq34d8O6h4y8S6bouj2NzqmraxcLaWVnbqGluZSCQq5IHRWPJHSvSR+wn8ayP+SV+Mv8Avzb/APx2vHzDiDK8BUVLHYmnSk1dKc4xbW10pNXV018j6HKuE86zOi8Rl+FqVYJ2bhCUlfR2ulvZrTzPK6Ksa7od/wCFPEGoaTqtjdabqml3L2l5aXChZbaVDhlbBI49iRVdIpLq5gt4YpJ7i6nitoIoxl5pZHWNEHQZZ2UckDmvSjiKUqSrRknBq6aas1a977Wtrfax5UsvxEcT9TlBqpzcvLZ83Ne3Lbe99Ldwor1U/sI/GxeP+FV+MuPSG3/+PV55408D638NPFd5oXiLSb7Q9a08oLiyvECyxb0V0J2kjBVgQQe9eZl/EWVY6p7HA4qnVla9oTjJ272TbtqtfM9fNOEM7y6j9Zx+EqUoXtzShKKu9ldq1zNooor2T5wKKKKACiiigAooooAKKKKACiiigAoooLYoA0vBfgzVfiR4x0vw/oVlJqWta1dR2VlbR/emldgqjPYZOSTwACTwDX78fsY/snaL+xz8C9K8H6WIbi8QC41bUETa2qXjAeZKe+0YCqD91FUetfGP/BCz9iltK025+M/iKz/0i+D2HhaKRcGKHlZ7vHq5zGnoqSHneMfpMr+Si5XnoOxP0FYyd2f1l4N8GLL8F/bGKj+9rL3b/Zh/nPf0t3ZHqenQapYzWtzDDcWt0jRTQzIHimRgQysp4ZSCQQeor8K/+Cm37FbfsYftCyWemwyDwX4mD3/h+RskRICPNtSTnLQs6j1KPGTyTX7F3f7bXwig8TWOi/8ACxvB1xq2pXEFra2ltqcdzLPLOwSJVEZbO5mUenPOKxP29f2RbH9sz9njVfC8pgt9Ytz9v0K8k/5c75FYISeyOGaNuvyuTg4FCdmfSeIHDOG4lyucMJKMq9LWLTT16xdnpzLv1s+h+AoOaKsa3od74W1290rUrO40/UtNne1u7WddsltMjFXjYeqsCPwqvWx/FdSnKEnCSs0FFFFBIUUUUAFFangrwNrXxK8U2uh+H9Kvta1i/LC3s7RA0su1GdsAkDhVY9e1ehf8MJfGn/omHjD/AMBo/wD4uvHx/EOV4Gp7HG4mnTla9pzjF272bTto9fI+gy3hPOsxo/WMBhKlWF7c0ISkrq11dJq+qPKaK9W/4YS+NP8A0TDxh/4DR/8AxdH/AAwl8af+iYeMP/AaP/4uuP8A1z4f/wCg6j/4Nh/8keh/xD3if/oX1v8AwVP/AORPKaK9W/4YS+NP/RMPGH/gNH/8XR/wwl8af+iYeMP/AAGj/wDi6P8AXPh//oOo/wDg2H/yQf8AEPeJ/wDoX1v/AAVP/wCRPKaK9W/4YS+NP/RMPGH/AIDR/wDxdcj8Wfgb40+Aun2d3428Mat4VtL9pFt59RRIo5DGm9+QxxheecVvheKMmxVWOHw2LpTnLaMakG36JNt/I58VwRxDhqMsRiMDVjCOrbpzSS7ttWRy9FezfDL/AIJ4fGj4s6LBqWm+Bb2x0+6j82GbWrmLSzIpAKkRynzcHPGUHT6V03/Dpb47f9C54Z/8KaH/AOIrysT4icMUKjpVcfRUluueLs+zs9z2cN4U8W4imqtPAVLPulF/dJp/gfOdFe7+Mf8AgmN8cvBWkPeyeCF1SONWZ49H1S3vplA/6Z5V278IGJx06Z8T8M+HtQ8a+JrHRdJ0+81DWNTuvsNtYxxFbiScbsxFGwVYbGyGwQVIPSvVy3ijKMwpTr4HFU6kYK8nGcWore8tfdVk3d2WjPHzTgnPstrU8PjcJOEqjtFcrfM3sotXTfkm2VKK9VH7CfxoI/5Jh4w/8Bo//i6X/hhL40/9Ew8Yf+A0f/xdYf658P8A/QdR/wDBsP8A5I3/AOIe8T/9C+t/4Kn/APIml+wX+yVD+2D8brjRdR1C70/w74fsRqusPafLcTo0nlQ2yPgiMyuJCXwSqwsAMsGH6h/D39jr4VfCmwSHQvh14Ps9oGbiXTIrq6lI6F5pg8jH3LV8Kf8ABP8A+GHxk/ZV/aLtdWv/AIVeMpPDviC3Gia0Ut491vC0ivFcgb+fJkBJHUpLJjJwK/TIzqh+6zBTngHDCv4+8duJsbis79lhsWqmE5YuCp1E43t73Motrm5r/Fry8vQ/tDwN4bw+XcPwnWwsqWKvL2jnBxm/efLZySfLy8traXv1ueX/ABE8a/BH4O6zDp3i7VPhF4V1KaAXMVprEumWNxJESQJBHJhthIIDYxkEdqztU/ZT+Bf7RfhS21SPwX8O/EOl6hHm01jRba3TzkBIzFd2u0kZzyrEZFfn7+0x+wp8ZI/2kPHt/ZeEPEnie117XLnVINYskRkvoZnLwhjvDBooysO0j5REMcYr6Q/4JAfBn4h/BKb4kaf4w8N634a0XUjp19p0F8iRxz3hNyl1JGqsfmMa2wc4GdqHk5rkzjgzLso4fjnuVZypYlKEnCM1GT5mk1FRnzpx5ru6vZNtLY9TK+NM1zPPquSZllM4Ye80qsk5Qly3s3eHLaSWlpPVpanhP/BQD/gmy37Lmit408H399q/gdp0hv7K+PmXvh9nIVJPN48+3ZyFO4CSJtuTIrkx/K2eT7HFftZ+2R4Yv/Gn7JvxN0XSdJudc1bWPDF/Y2FhbhfNuLmSBkh27iACsjI+e231wK/Ku7/YY+M8lxNIvwv8YKjMz/8AHtH05P8Afr9s8GvE5Y/KZ0+IcXCNSlLljKpOMZTi1fW7XM09HLrpe7u3+C+NnhXKhmdLE8N4Oco1VJzjThKUYyTWqUU1HmT20Wja3PJ6Kisp1urWOWPmOZRIh9VIBH6EVKTgV/Qh/M8otPlYHpX1x+zV/wAFTbL9l/8AZg0TwbpvgTUvEniLT5rt5Z7vUotO00CSd3XEirNK/wArdBGORjI6188/An9nvxd+0r4zuNB8G6fa6hqVrYvqMy3F9HZxpCkkUZO5+C26VeB2z7V61/w6d+On/Qs6F/4UltX5jx7W4OzDkyriTEQj7OSnyOpyO/K0r2ala0ns0fsXhxhONsspzzbhvCuarRcFOyaspa8qbSunG2qa8j6m/Zf/AOCvng34oSR6T8RLez+HWuSMI4717oyaHeE4C4uHCm3Y9NswC54EjV3v7cP/AAT10H9rnTW1S3mi8M/EGygEdlrPlbobxFyVt7xFIMsPUKwO+PdlSRlG+HP+HTvx0/6FnQcMMEf8JHbcj0r2T9lj4F/tafsjeRY6HofhvXfCUWAfDWq+KIPssajjFrMAz2h/3Q8fHMRPNfgPEHDnD2W4tZ1wPm9KjWhqqbqxs+6jOTej2cal4vrJLQ/ovhnPuIs0wryfjXKpSpzVnUUVZ/44J3TvrzQ2drRVrnxV8TPhn4h+DPjy+8L+KtIuND1/TgrzWkjeYrxtnZNFIBtlhbB2yLwSCDtYFRh43lccluBX7Q/tA/sxeGf2x/hRY6X430e40vUbdPtNlcW88T6h4euWAD+VMu5HHZl5jkAG5cgY/PfxB/wSI+Nmia7dWlnY+GdetLaQpBqMGsx2aXqdpPJly8RI6oWbacgMwAY/rXA/jhk+aYVwzipHDYiHxXdoS6c0JPT1i3dX0clqfinHvgLmuXYv2uQQlXoT2WnPDyldq67SXpJJ6v3z/gl1+wx4F1v4HaH8TPFWi2PirXPEvnXOnw6nClzZ6RbpM8UflwtlGlYR72kYFhuCjaAc/cj3UxZR5k2WOAAx/LFeC/8ABOj4W+Ovgb+zrb+DfHWl6fp9xoN9cDTGtNQjvFntJpGn+YoAFZJZJVwc5GDnqB137Ynh/wAc+Lv2cPFGj/Dho4fF2r262VtO16tm1vDJIq3DxysCFk8kyBTwQzAgggV/J/GmPrZ1xRWWIxUZwlVcYTcr04wcrRad7KEYtN221vrc/rrhXL6GVcP0KWHw7p8lNNwSXNzcqck11k3e7b1fU574gf8ABR74N/DTxdeaFqfxBtZtV06RobyHTobjUBayqcNHJJAjosingpu3KQQQCMUvw/8A+Cj/AMG/iX4pttE034g2sGpXzpFaxalDcactzIxIWNJJ0RDIx4CbtzEgAEmvz9tf+CSXxws7OOCHwv4dgt4FCRQx+IbZURR0VQBgAUT/APBJX443VrLBN4V8OzQTIY5YpPEVqySqeqkEYIPoa/Wf+IZ+HH1fk/tr95b4uely378tr28ue/Tm6n5N/wARC8QfrHN/YX7q+1/et/iva/ny/I/SL44/shfDb9orzpPGHg3R9R1KRdv9ppF9l1OPjAxcx7ZeABwWI4HHFfm/+3P/AME6da/ZHVfEWj3t54q8A3U/lveTRAX2gO7YjjutvyyRMSFW4UKMkK6gkO/3/wDsJ+F/iX8Ov2fbLw78ULeF9Z0G5ez0+5j1NL+W6sAqmHzZB/y0TLx8kkqiEkkmvVfFPh7T/HnhnUtD1rTVv9G1q2ksL+2lw0dxBKpSRCM9CrEV+fcMeIGbcH5q8Ph8R7fDQlaUVLmpzin8UN+VtapqzvpK6uj9B4r4Dyni7LebG0PZ1pRTjJpKpB20Ta3ts4ttPprZn4P9KK+j7j/gkb8cdKu5rWHSdE1S3tZHhgvX8QW8b3kasQkrKwyrMoViDyCTSSf8Emvjorf8i3oLf9zHbf4V/aEfE3hRpNZhS/8AA0vwufxFV8IeLoTcFgZuztdWs/Na7HzjRX0Z/wAOnPjp/wBCzoP/AIUdtR/w6c+On/Qs6D/4UdtVf8RL4U/6GFH/AMDj/mR/xCXi/wD6AKn/AJL/AJnznRX0Z/w6c+On/Qs6D/4UdtR/w6c+On/Qs6D/AOFHbUf8RL4U/wChhR/8Dj/mH/EJeL/+gCp/5L/mfOdFfRn/AA6c+On/AELOg/8AhR21KP8Agk38dCD/AMU3oI9v+EjtuaP+ImcKf9DCj/4HH/MP+IS8X/8AQBU/D/M+cqK+jP8Ah058dP8AoWdB/wDCjtqP+HTnx0/6FnQf/CjtqP8AiJfCn/Qwo/8Agcf8w/4hLxf/ANAFT/yX/M+c6K+jP+HTnx0/6FnQf/CjtqP+HTnx0/6FnQf/AAo7aj/iJfCn/Qwo/wDgcf8AMP8AiEvF/wD0AVP/ACX/ADPnOivoz/h058dP+hZ0H/wo7aj/AIdOfHT/AKFnQf8Awo7aj/iJfCn/AEMKP/gcf8w/4hLxf/0AVP8AyX/M+c6K7r9oD9mfxp+y7r+kab4002z0+51y2mvLIW2oR3iyRxPGj5KfdIMqYz159K4Qcge9fVZbmWFx+Gji8FUVSnK9pRd07Np2a7NNeqPjc4yXG5XipYLMKbp1Y2vF7q6TW3dNMWivaPhD/wAE9fiz8dPhxpnizw3oOk3Wh6wrvayz63b28jqkjRklG5X5kPBrpP8Ah058dP8AoWdB/wDCjtq+bxHiJwzQqyoVsfSjKLaac0mmnZp67p6H1tDwr4srU41qWBm4ySaemqeqe/Y8n/Zp+IWl/CT9pT4f+KtclNtovh3XoLy+uAMi2gw0byn/AGUEm9sc7VbrX7eWV1FqWnW91ayxXVpdRrLBcQuJIp4yAVZGHDKQQQQSCDX5Nj/gk98dlOV8N6ErDkEeJLbitzwH/wAE7v2nvhXYy2vhea78NWkxBa30rx2LOAkbjny0YIOWY8Ada/DPFbL+F+LK9LG4XN6NOrTjyNSknGSu5LVO6abfR3utra/0D4SYjifhTB1Mux2U1Z05S504cnMm0k005JNaKzura79PpL9pH/gkdoHx6+MWreMtP8aax4XuPEEn2nULIabDfW73G0KZY9zI0e4KCyksCckYyRWP8Kf+CLXhvwP8RNI1vW/HeueI7XR7uG/i06PS4bCO4mhkWSPzXDuxjDKCVXaWxgnGQfI1/Y+/bGLY/wCEs8Vf+HHP/wAVQ37H/wC2MG48WeKmHqPiQef/AB6vnqNbOqWBWXU+KMMqSjyJcyuo2slzcnNotE+a6XU+uqUMmqY7+0p8N13Wcufm5KV+a9+b+La99dt9T9MPLkuHbCs7McnA9a/Gj9v34naH8Y/2w/Guv+G9Qg1bRZpra0t723cSW92YLaOJ3iccPHvVgGBIbaSCQQa9P8b/APBPv9qb4m6I2m+Jb3UvEWmvndZ6n4++028meoaNm2sPZgRXPH/gk18dNgP/AAjegemP+EituP0xXr+FeT8M8LY2eZYzOKFSpKLgoxklFJtNttu7fupJWVtd9D5vxXx/EvE+XRyrL8oqwhzKTlPkvonZJRk11u3fpa3U+cqK+jP+HTnx0/6FnQf/AAo7aj/h058dP+hZ0H/wo7av3f8A4iXwp/0MKP8A4HH/ADP55/4hLxf/ANAFT/yX/M+c6K+jP+HTnx0/6FnQf/CjtqP+HTnx0/6FnQf/AAo7aj/iJfCn/Qwo/wDgcf8AMP8AiEvF/wD0AVP/ACX/ADPnOivoz/h058dP+hZ0H/wo7aj/AIdOfHT/AKFrQB/3Mlt/hR/xEzhX/oYUf/A4/wCYf8Ql4v8A+gCp/wCS/wCZ850VrfELwDq3wq8dat4a123js9Y0O4NpeQxzrOkcgAOA68NwRWTX2WHxFOvSjXoyUoSSaa1TTV015Nao+DxuDrYTETwuJjyzg3GSe6admn6MKKKK2OUKKKKACvX/ANhj9ky+/bO/aG0vwjC9xa6RCDf63ewj5rOyQjdg9BI5IRM/xNnkKa8ekcRoWb7q8n6V+5H/AASj/Y2/4ZK/Zttzq1kbXxl4y8vU9cDj95b8HyLU/wDXJHOR/fkkqJPofonhpwe8+zVRrL9zTtKfn2j/ANvP8E30O0/bP0uL4bfsB/Eaz8Pq2h2+h+D7u309LFzCbGOO3KxrGykFdqqACCDxX5Qf8Ej7mbWv+CjPgG6vZpr26QXirNcyNNIo+yy8BmJOPbNfrV/wUJGP2GPi1/2K1/8A+iWr8kv+CPv/ACkL8B/9vn/pLJUdD9h8RJOPF+UU46RvDTp/EXQ8e/YdUR/tLfBsBQoXxNovA/6+Ya/ovZNy4wOvp71/Oj+xD/ycv8HP+xm0X/0ohr+jDv8AjUr4Ua+BX+5Yv/r4vyPy3/4LqfsUNpWs2/xm8O2ifZrxo7DxRDEmDHLwsF5x1DDETk4wREedzY/N8HNf0nfEPwHpPxM8E6t4f12zj1DRtctZLK9tn+7NFIpVh6g4PBHIOCCCK/nz/ar/AGdtU/ZS+PniHwNqjy3DaTPmzvHTb9vtX+aGcDp8y9QOjBh2rSMuh8T40cGrBYxZxhY/u6z97ynvf/t7V+qfdHntFFFaH4WFFFFAHf8A7LXx1j/Zp+PmgeNptHuNei0Yz7rKC4SCSXzIJIgQzjaMFweewr7D/wCH5+k/9En8Rf8Ag+s//ia/Pxhk+9fV/wDwT7/4JsJ+1J4ck8aeMr7UNL8FmaS106ysH8m81to2KSStMQTDbq6lF2AvIwY7kVRv/HfFHh3g72f9v8TwbcUoK0ppy1k1GMYySbd5PW2mraSufvHhFxPxjJ/6v8N8nLdzk5xuop2Tbe9tEkkm7/O3qw/4Lk6aVz/wqXxLj1GuWn/xNNP/AAXQ0kf80n8Rf+D60/8Aia+g9J/4Jt/AjRtOe0T4X+G7hJAd0l5511cNxj/WyOzg/Rh7V4p+0/8A8EdPDGt+FrvU/hO1x4d8Q2cTSxaLeX8lxpmq7V4hEkxaS2c44kDMgJ+ZCOR+A5VmXhTi8UsPXwVajGTspynJxX+LlqNpeaTS62R/RWaYHxLw+GdbCYnD1ppX5PZuLflFttN9r8q8zIH/AAXO0n/ok/iLn/qO2n/xNfXP7NPxrt/2kvgf4e8cW+lXWiw6/FLItjcypNJbmOaSEgunytkxk5HYivnH4Jf8Ebfh74d8IWMnjy51rxT4jkiSS9W11OWx06CUglooUiKuyLnbvdizbd2FztH1N8JfhbofwU8A6b4W8NWbafoejxtHaW7TPMYwztI2XclmJZ2OWJPNfI+IGJ4HdJUOFqNSNSM9ZycnCUEmvdUpt6vlavFO33H0/BNDjBSdXiapScXHSNNPmjLT4ns7K60bV+rOe/an+Pdr+y98D9Y8bXWk3Wtw6S9vH9itpkhkmaadIVw7/KMFwTnsDXx94U/be8J/t4ftdfB3Tdc8LN4TsfB9/qeuQHWdVtZre+u1silvGMFfnSQiVQc8xdDjI94/4KxH/jBPxd/196X/AOnCCvyg8PeBtQ+JniGw8N6TpLa9qmuXC2dppyojG7kOSF+f5AAAWLN8qqpJ4Ffong7wDlGccPYvMsVJ0q0JVIKrzNKEXSjduPNGLspSbbtp10PgPFPxEzbIeIcHl2CpKtTqpN07e9KXM0lF2dndK2j1P3raCRnQlT+8YAOw4bJ65r4v+E3/AAVjk+I/7YCfD+XwXbWvhfVNbm0DTdUi1F3v0mjaRFlmhaMIY5XjxtVg0QKkmTJC8r8Af+CQ/wARfhlYW15B8cNS8AX2Q0mn+E7aeW3hIIbazyzpFLhh1+zhTyMEV3nwA/4JE6B8FfjFpPjXVPHnifxdqWi37arbwzWVtZRTXbFj5spjyz4Z2baCqk9RgYr4nCZXwPltPHQxmNWLk6bVFwp1YNVLOz6Rett5Sja+jPvMVjeK8a8HPCYdYZe0TrRnKE70+qi482va1nfqtUfXiplAetfmp/wUJ17S/wBmn/gp74V8dWekNe+Tp+m+JNRsrd0ga+uFa9tS4ZvlDtHHECT18v1NfpbCS5CqrOe2Bmvjv4k/s+/Df/goD+2j8QrW/wBX1qaT4aaDoOkzTaLfxLCLieXVJpYWJjcM8YEYOD8pYg8ggeT4W5thMtzKvisyUnhvYzhV5E2+Wpyws7NaOUl10eq1R38f5TjMxyyNDLHBYiNSnOm5/CpQmp9m/hTWi1V1sQfAv/gr/pfxs+Mvhfwanw313SZPE18LBL2XV7WaO2JR23MiqGYfIeB619kLGCnT25r80PE/7OPhX9kf/gqZ8E/C3h/UtTng1Z7TUkj1S7jlnaVjqMb7NqJlQsUeRg4Ppmv0wz+7/GtPE7K8iwtfB4jh2nKFCvS50pOTbftJxv7zk18O1zLgHG55Wo4nD8QOEq9Gpy3grRs6cJq17N/Frpvp0Pl39rr/AIKZ6f8Asm/GMeDp/AuseIpv7OttQN3b6nb2sQ84yAIFcFsjy+T05Fe2fs3/ABng/aL+CHhvxtb6XcaPD4jtjcrZTzLNJbYkZCpdflblc5HrX5s/8FlfEenaN+2z5N5qFjZyf8I3prBJ7hIyw3XAyAxGehr7m/4Jjzx3P7B3wzkjdZI5NMkZXQhlYG4mxgjrXucZcG5Xl/BOWZ1habjXrtc8uaTTvCTejdlqlskeHwrxZmeN4vzLJ8Tb2FBRcPds9bXu+u52X7VHx+t/2XPghq/ja40e512HSpbeI2VtcJbyTGWVYhh3BUYL5Oe2a8l/ZB/4KYab+1v8YD4QtfA+teHLgaVdap9qutRt7iMiCS3Qx7Yxuy32gEHoNh9RWl/wVl/5MV8Wf9fenf8ApbDXx7/wRwGf21G/7FLVf/SjTq24S4LyjHcCZhnmJpt4ijKahLmkkkoU2vdTs9ZPdGHE3GWZYHjbLcjoOPsMRFuaau7rn2fT4UfqhKo2lsZxzXx3+0J/wVw0z4C/GjxR4Jf4d65rEnhu5+wvfRatbQR3LNCjllRgWAHmY57gmvsSbmJvpX40/wDBQj/k+P4o/wDYaT/0lgri8FOEcr4izitg82pucI0nJJSlHVTgr3i09m/I18ZOMMx4ayWnj8saU3UjF8yurOMntp1SPF9KtmstMtoGwWghSJiDwSqgHH5VYoAxRX9+n+eFSbnJyfU9I/ZW/ah1z9kj4i3vibQ7HR9Sub7S5dLkg1FJWjCPNDLuUxuhDAwgc5yG7Y5+8v2A/wDgoX4o/a1+L2qeG9e0Hw3ptrY6NLqcU2m/aFk3pNDHtYSSOCpEueMEFe+a/MKeZbeF5JG2xopZiewr7O/4I0+Eta0X9pfxJLqOg+INLhj8M3ELSahpVxZpva6tcIDKigt8jcDn5TX4X40cK5DPJsXnGIpR+tci5ZOTUtHFaR5knZO2x/RXgbxdxJLNMJktKcngYufMlTi0rxnNXny8yvPVXl5baH6UyLt7dq/PH45f8FiPHXwt+K/jTRLHwj4NuLHwvrF3p8JuHu/OuI4JCgZmWQKGYLnhMDPQ1+iEnzR1+Jn7W/w/8S3v7RnxSSDwr4tuBd+JdSaF4dCvJY5ledtrK6xlWVgRgg4r+f8AwRyDJM2zHE0s8pxnCNO8eaTj73MlpaUb6H9FeLGdZ5luBw1TIubnlWjGXLBT9xp3unGVldLWya7n7aywLFO6jorFRn2OK+bf+Civ7Zuvfsc6D4OuNB0fRNWm8S391bT/ANpGYrAsUAkBURupJJOCScADpX0tdf8AH3N/10b+Zr4J/wCC53HhP4V/9hjUf/SNa+P8L8pwmZ8UYPAY+HPSm5KUW2r2hJrVNPdJ6M+h8Qs2xeWcN4vMMFLlq04Nxdk7PTW0k0/mmek/8E5P27vEn7Y/irxzp/iDRfD+lr4VsdLu7aTTBOpmN1LfxuriV3+79kQggj7zZzxX1O67iv1r86P+CFP/ACUv4wf9gbw7/wClOs1+jDdV+orq8XMlwWU8VYrAZdTVOlD2dopt2vShJ6tt6tt79Tn8Mc6xmb8M4TMcwnz1ailzOyV7TklpFJLRLZI/On4l/wDBYzx54M+KHirRbXwn4Jaz0LXNQ0uBphdtI6W13LAruVmUbmEQY4AAJwKi8Df8FbvjJ8Utbk0zwv8ADHQfE2oQkCeHStL1K7+yZBK+c6TFIdwU7fNZd2DjNfI/7QSySfHr4hxwytbyzeNNYgWZVDNCX1a4TeAeCV3ZAPGQK/bH4WfCPw/8CvAmn+EfCumw6ToWjR+TbwR8lyODJIx5klcjczsSzEkkmv2TxCwPB/C2W4OUMrjVrV43V51FFWUbt2ndtuWiVuuq0v8AlfAOa8XcSZpjlVzJ0sPh6soJRpUXJ+87K8qbskktWm3f5nxd4k/br/ay0vw1qN1Zfs4tfX1raSzW9t/Zd6puZVRmSPP2k43MAucHGehr70dFDsqncisQpznIz6189fti/wDBQnw7+x14u0PQtQ8M+JPE+qa5ZSaisWlvbRLbwLKIgWaeRAzO24Kq/wBw5IyM9z4X/bI+E/ivwxpurWfxI8ELZ6paxXkAudbtreZY5EDqJI3cNG4DAMjAFTkEZFfivEWCxeOwWGx+FypYelPn5ZU/aSVSzSd+ac2uVp2+G93vZW/ZsmxVGjia+Bq4916tPl5oyVKMocyvHSnCHxLXVPbQ8j/4KJft2+Iv2OvE/g3TdA0Pw/qi+JLO+vJ5dSM58ryJbeNURY3Tr5zEkk9AMV85z/8ABbT4gWkMksvhX4fxxxjc8kgvESNR3JNxgD3NH/BZP4neGfiZ8R/hrJ4b8RaD4hSx0jVFuW0zUIrwW5e5tCgfy2O3cFbGeu0+lWv+CN/7N3h34peLPFXjzxDp9vq8ng68tLDRLW5jWSC1u3iaeW7KHIaQI0KRkj93+8IyWBX9syHhfhTLOAKXEWfYD2lVKV05TjKTdWUIL4klpa7t8KvZvR/ieecUcXY7j+pw1kmNVGilGV3Tpz5V7OMpP3otybb0XNu90lpb0P8A4K6/GjxVpkV9o/wkh1ywmJWO70rwpr2oW0hBwdssG9GweOGNex/8E9f+ChPiT9r/AOJ2saFrmh+G9PtbHRzqcE+m/aFk3rcRRMjrLI4IPm/7JUoQc54+unleQlpJG2jks7HCgdST6Dk18Z/sP/En4T/Hb9tzxp45+GcniDT5Nf8ADb3GpWF/pQt7XUZXvLcnUbdxISpkCp5kbopYujjBMgP5z/aWQ5vlGYVMLkyoulCLjVhOpNQk5pJTu7LmSkk2t01bqv1Cngc4wGaYOGJzV1Y1JSUqc4UYOaVOT9xwhGV4y5ZNX+G930f2Y8a7elfnh46/4K9fESw+OHjDwj4Z8EeG/EUnhrXL/So4LCw1LVL0pbTvFvkS2ckMQoY/IAM+nNfog/3a828HftafDXxV8Wr74faH4w0mbxdZTXCT6VAHiZpoiTOqttEckiHcXCszDDE9Dj4vhDGYTDOvVxmXfXYqN/inFU7PWUnBPTpq0l3PpOIcNiq8aVPC414WTluo05Oej91KopK/XRN6ep8O6/8A8Fmvij4S1KKx1rwB4X0G+uMGK11fS9V024mBBIKR3EiM3Q/dB6H0qIf8FrfiJ/0KPgP/AL4vf/j9fo3448F6T8SfCl7ofiPS7HXtEv0MdxYX8K3EEy+6tn8xyOoINfjF+1n8BYf2aP2jPE/gu0ubi60vTZo59NluXMk32OaNZYldjkuyBim45LbMnJJr958MafA3FeIqYCtlMaNaMeZJVKkoyimk7e8mmm1prda36H4T4qY7jjhbCQzLB5o6tFyUZKVGgpRb2d1Ts07W2Vnbe+n0GP8AgtV8RGPHhHwGfYR3v/x+qp/4LieOFm8v/hHfhv5hONhkut35faK9I/4J8f8ABMnwnr3wo0fx58S9Lh8TX/ia2TUNO0S6JbT9OtJAGhMsYOJ5nXDnflE3hVXILN9jQfAbwS9g1nH4G8Hm1Iy0CaFbGP8A758vFeDxLxL4bZdjp4PBZR7dQbTl7ScI3Wj5dZOS82kn0urN/RcN5D4j43BQxWZZuqEpJNQWHozaT/mfLFJ90r27n58n/gtb8RF6+EfAf/fu9/8Aj9Pj/wCC03xHlZQvhDwJknAPlXp/9r17r+1R/wAEkPBfxfs2vvh8unfDPxMJVMht7RpNJulJw/mWisoSQAlleLZlhhgwPHoHgP8A4Ji/A/wj4bh0yT4c6L4qlhUGW+1+D+07y6YDBkdnyqk4ztjVUHZRV1uKvCuGCp4inlcpVZNp07zTja2rk6lmnf3bXejuo9cqPDvijLGTpVc2pxopLln7Gk3K/Tk5Fa3W8raqzetvTPgR4+n+LPwQ8G+K7y1gs7rxNodnq01vCxaOB5oUkZFLclQWIBPOOtef/t5/tMat+yh8DYfFGiabpWqXs2rW+neVqBk8lEkEhZsRsrFhsAHIHNevaB4fsvCWh2mlabZ2+n6bptulraWkEYjitYkAVI0UcKqgAADoBXy1/wAFlv8Ak0Kz/wCxmsv/AECavyTgvBYPMeKMLha1O9GpVScW38LltdO+2m/zP1Pi/HYnAcP4vGYeVqtOlOSlZO0oxbTs009Vs013PhL9rb9rzxB+2J4l8O6lr+maDpcnhu1urSBNNjmXzlnkhdi5kkfoYRgDH3jk9MeUAYC/7NA60tf6MZPk+DyvBwwGAhyUoX5Ypt2u3J6tt6tt7n+avEHEGPzrHTzHM6nPVla8rRjflSitIpLRJLReup9K/AD/AIKieNP2dfg9ovgvS/D3g++0/Q45EhuLyO6E0geV5SW2TBeC5HAHAr1v4df8FOP2gPi9phvvCnwXs/EWn7VdL6y0TUzaThiQDHM0yxyjg8xswGOa+a/2Cfh34d+K/wC2B4I0LxZb2l5olxcXE7Wd1gw388NvJNBC6nh1MiAlD9/Zt5BIr9mctdOu5jubgZ/l6V/K/jBiOF+H80+r0sphVr1V7WU5zqKPvSleyjJXbabeqS0sn0/sTwdrcUZ9lKxuLzOUKUH7OEIUqF7QSXvSlTl5WVr9W9T5L/Z+/av/AGhPH/xn0DRfF/wT/wCEd8M6hM8eoaobG5g/s9BE7K+553U/Oqrgrzu6ivrIL+7z6da+QZP+CwvgnT/2hrjwhqfh/U9H8M2mqTaPP4qvdQgit7eeORomkkgPzJb+YpXzC+4ZBKAZI9l/4b7+Bm3H/C4vhr/4UFv/APFV+N8W5Bmjr0qqyv6tGcE1GmpyjJNtqTblNpu9mm1ZJaLr+ycO5tg6lKpTjjvrDhNxlKXs04yVrxtCMI6el9dz5l/aZ/4Kv+Nvgp+0J4w8H6b4Y8H3Wn+HNQ+xQzXX2pp5l8tG3OUlVQcseAOgFfTf7Evx/wBS/af/AGfNL8Yaxp+n6bqF7c3VtJDZNIYP3MzRhlEhLDIAOCTj1r8rP2y/GmkfET9rX4ia3oOp2OtaPqOsmW1vrKZZre5TyYhuR14YZBGR3Br9GP8Agkh/yZH4f/7CWpf+lb1+o+JnBOT5XwVgMxwmHVPET9ipyvK75qUpSum2leSu9EflPh3xxnOacaZnlWMr89Cj7TkjywVuWqor3oxUno7at366nrH7UfxWvPgP+zr418Z6fZ2d/feGdKl1C3trpmWGZ1xhXK4bbz25r4Pf/gtV8RElZf8AhE/ARVSR/q70f+3FfZH/AAUU/wCTFfix/wBi3cf+y1+QPhrwlqvxA8a6b4f0K1jvtb12/j0+wgll8qN5pH2qZHAJSNeWdgCQqsQCcA9XgfwZw9m+UYvGZ5QjP2c/ilKSUYqCbvyySstXdnF42cbcSZNmeBwXD9XllWTXLyQlzS5oqPxxlbe2ll3PrRv+C1vxDRWZvCfgFVXqxS9wPx8+obX/AILf+Or19sHhr4dTt/dja7c/kLivrD9nX/gmZ8LvgLoVr/aGh6b468SxqPtWt65ZLcF5MYbyLdy8dvGTnCLlsY3O55r1XxF+zf4A8aaK1rqvw/8ABup2O3aY59Ct3jA9B8nH4V4GYcZ+HFLEunhcldSmn8TqTi35qN3p2u0+6R9LgOF/EKeGVTGZ5GFRr4Y4elJJ9uZqLf8A4D958AN/wWt+Ig/5lHwGO3+rvf8A4/XWfAv/AIK3+PPif8b/AAd4Z1Dwr4NgsPEms22mXEtul2s0KSvtLoWmZdw6gFSD0rsPFP8AwRd8F6l8ctL1bSdWv9J+Hojd9W8NebLJcPMv+rW2ui3mRwPn51Ysy+WAjDedvv8A4H/YV+D/AMO9X0vV9D+Fvg6x1TRZEmstSTS1kuraRM7ZBM+X3jJ+YsTz1rq4g4m8MIYPly3LpSq1IPW8o+zk00lK9RptOz928bW16HPkOQ+JLxnNm2ZwVKE1oqVNupFWb1UIuF1pvdO/SzfqaLu3V8e/t7f8FF/FX7J3xws/Cuh6B4Z1Kzm0S21R59RFw0peWa5jKgRyIoUCAHnJJY+lfYkdflz/AMFm/wDk8DTv+xQ0/wD9Kr+vi/BzIsBnHE1PBZlTVSm4Tbi21qldbNPT1PpvFziDH5LwxXzHLJ8lWLhZ2jK15xi9JJp3Te6/E+b/AIufEi++M3xU8ReLtShsra+8SXhvZobRHWGJiirtXezMeFzknqTwBXP0DpRX+guDwlHC0IYbDx5YQSjFdklZLXXRLqf515lmGIx+KqY3FS5qlSTlJ2SvJu7dkkld9EkgoooroOEKKKteH/D994v1+x0rSbSbUNU1O4jtbO1hGZLmZ2Coi+5YgfjQXTpynJQirt7H1j/wRy/Y2X9pb9ov/hJtYt/O8JfDuSK+uEkjDRX16ctb2/PDBdplcc4CoDxIK/auJcKD3789a8j/AGIf2WLP9j79nnQ/Blu8NzeW6G61W7iXat7eyAebJzztGAi552otevAYFYN3P7k8PeFY5FlEMPNfvZ+9N/3n09IrT1u+p47/AMFCv+TGPi1/2Kt//wCiWr8kf+CPv/KQvwH/ANvn/pLJX63f8FCv+TGPi1/2Kt//AOiWr8kf+CPv/KQvwH/2+f8ApLJTWx+eeI3/ACWWU+sP/Th4/wDsP/8AJy3wb/7GXRf/AEohr+i4vhj9a/mo+EOv618ONZ8KeI9KtbhNS0GWz1K0aWzeRFmh2SJuXHzDcoyO4r63P/BbX9ognOdA/wDCeP8AjUx+Feh8n4Z8cYHh/D4ijj4TvOd1yxvpa3Vo/aDPmcYr4f8A+C3H7HP/AAvD4FxfEDRrdn8SfDuKSWdIot732mthpkOOcxEeap5wPNGPmyMn/gk7+3H8Y/2yfi54mg8ZNpI8M+GtLjll+z6T9mdrqeTbCu8tn7sc7HAP3R04z95TQLNC6squGyCGGQw7gj0NM/dubA8XZFOKjJUqqaXMrNNPSSV3tJad7dj+ZxTkfrS19Af8FKv2PW/Y2/aY1DSbGFo/CfiDfqvh9yPlWBn+e3B7mFyEI6hWjJ+8M/P9axd0fxHnGV18uxlTBYlWnBtP/NeT3XkFFFFUeaCf69P94fzr9iP+CbYz+wn8M/8AsEH/ANHS1+O6f69fqv8AOv2I/wCCbP8AyYp8M/8AsEn/ANHy1/N/0lv+RFhf+vy/9Imf1D9GH/kZ43/r3H/0o4X/AIK7fEvxJ8L/ANmLTZvDOu6p4futW8Q2+n3Vzp05t7hrdoZ2aNZV+dMlVyUIbjgjNdl/wTZ+IuufFf8AY28J614m1S61zWHlvrWW+uiGnuFhvJo0LsANzBFUFsZOMnnNeZf8Fsmx+y74d5/5my2/9Jrmvk79nr/gpV8RP2aPhRp/g7QdP8D3WlabNcTQyalpV3NckzTPM25o7yNThnIGEHAHU8n834f4BxPEfAFKGVUoPELEybk7RfIoNNc1rtXcdL9L9D9V4i8QMFw5xrNZvXlGhLDx5YpSlHn9pLXlinZ2W9vI+xP+CuPxr8V/Bj4I+E38J69qHhy61zxGLO7urFwlwYVtpZdivjKgsqk45OAMjmvSv+CePjjWviR+xr4F1rxBql3rWsXltcC5vrpg09wUu5kUuQBkhVUZ74r81f2o/wBu3xt+134c0XSfFFn4Rs7LRNQOowHSdOubeVpTE8WGaW6mBXa5OAoOQOeor9Fv+CXvH7B3w9/64Xf/AKXXFcvHXBNbh3gXCUMfShHEPEO8o2bcXGbSckrtaLS9tEbcE8cYfiLjDFVMtrynho0IWT5opTU9WoytrZrW2pm/8FYv+TFPF3/X3pf/AKcIK+Lv+CQumwX37b9jJNFHI9l4a1W4gLD/AFchNrHvHodkki/Rz619o/8ABWL/AJMU8Xf9fel/+nCCvjP/AII+zxw/tt26u6I03hbVUjDMBvbfZtgep2qxwOyk9q93w9bXhhnFv5p/+m6Z4fHVn4m5NzfyP/3IfqwnCV+Yv/BRX9tX4n2n7WGveEfCvirxB4Z0nwzNb6ZZ2WhIPtOo3EkUTs7lUeSV3klCJGo4CjALMTX6dr92vy5/aI+E/iq5/wCCtKtb+GteuY9S8Y6Lq1rPDYSyW81ohtGkm80L5YRBDLuJYbShB5xn4bwTp5c84xFbMqcJxp0Kk4qpyuPNFx6S0va/eyu+h974sf2lLKaFDLJzhKpXpQlKnfmUJNpu61STtrt0ejPJPGfiv9ob4j6K2m+IW/aG1jT3zvtZtF1qKGYEFSsiw26eYpBIKPuU+lUfg18aPid+xTqN9beHrfWvAR8UwweZbav4We1W6jtS4UwrdQpwhuiG8vON6Z6jP7YSnfKx3M2WODnrX5y/8F0G2/FT4P8APTSNcPJ/6edNr9S4E8UMLxLmdLhmvldGFCvzOSSXLeEJVFePKk9YrfbdH5dxx4d4vh7Kq3EuHzXETr0Irlc5J25pRi1tdXT6WPr79hz4lat8c/2T/APjDxM1nfeINZ00zXV0lqkW5xNKmVAHy5CjhcDr6164FwK+bf8AglD460nxR+xH4P0nT76C61DwlBLpmrWyN++sZhcTMvmL1AdGVlPQg8E4OPpBH807VVmbsAMmv5r4swf1TOsXhlDkUKs0o2tZczsku1rW8j+isixSxOXUMQpc3NCLvve6TvfzI5NIiv28ySxhuCo27nt1kOPTJBpyRrbYjWJYdmAEC7Qv4DpX5G/8Fadd8M/Fr9tnUpLNtN1xdB0qx0W4lAWaOG5ia4aaENyMp5qBgOjZB5U19+/8EyYlg/YQ+GcaLtSPTZFVR0UC4mwB9K+q4k8O6uT8OYLP6ta7xLX7tw5XG8XJPm5nfRL7K3Pl8h49o5pxBjchpU7PDJXndNSbtdJdGm7PXdMyP+Csv/Jiviz/AK+9O/8AS2Gvj3/gjf8A8npt/wBilqv/AKUadX2F/wAFZf8AkxXxZ/196d/6Ww18e/8ABG//AJPTb/sUtV/9KNOr9Q4B/wCTW5t/jqf+m6R+accf8nOyX/BL/wByH6oS/wCrb6V+NP8AwUI/5Pj+KP8A2Gk/9JYK/ZaX/Vt9K/Gn/goR/wAnx/FH/sNJ/wCksFeb9Gv/AJKLEf8AXmX/AKcpmv0kv+SXpf8AX6P/AKTM8fooor+2D+FyprRxp0nttP8A48tf0DXsjTXUqszMqucAngcmv5+tagnudJuktVha6MTGBZWKxtIOVDEdFLAAnsDX7kfAH9pPwf8AtR+D4/EHg3VoNRimAa6sSyrf6XKcFobiHJaORCcHqp6gkEE/yp9JzB1508vxUYNwj7VOVtE5ez5U30vZ2vvZ22P7A+jDjKH1bG4ZyXPzQla+trNXS7J6Ptp3R3Dn5emc9q4bxB+1l8MfB+vXGj6t8Ufh/pOq2cognsbzxRZ29zbycfI8byhlbkcEA8it/wCJHxH0D4Q+GZ9a8V61pnhvSLVS8t3qVwtvGAOTjcQWPoFyScADJFfiD+0v8QrP4xfFjx94rsbWa20/xFqd5f2kdzFsmELkhC6nlWZQGKnlc4PINfkHhd4ZviytXVacqVKnG/Oo8ycr/Dq0r2u/kfsniL4g0+GKOHkqaqzrTUVFy5XazvJaO6Tsnot1qfu1t2fLjbt4we1fBv8AwXOP/FJfCv8A7DGo/wDpItfe11/x9zf9dG/ma+Cf+C5//Ip/Cv8A7DGo/wDpGtc3g1/yWmA/xS/9NzL8WNeD8e/+nb/NHMf8EKf+Sl/GD/sDeHf/AEp1mv0ZPb6ivzm/4IU/8lL+MH/YH8O/+lOs1+jJPT6iu3x2/wCS2xv/AHC/9M0zk8F/+SMwPpL/ANOTPwr/AGgY5R8ePiIYTF58fjPWpYvNz5ZddVuHUNjkKSACRkgE8HpX66fs4/ty/D39p3w9b3ml69pul67JGsl94f1K6jttS0+QgEqUYjzYwxKiWPdGxBwcggfkb8e5v+L8/EPapkd/GmtRKudu5m1W4QDJ6ckc19SeEv8Aghp4u8W2ka+M/GXg/RVRt32Sy0qTXJFOBnDytBGrA5GdrjgfSv37xUyfhjHZPgHn2MeFqRgvZtRc7pxhzJwjq1treNn1tofh3hbmvE2EzzM4ZRg1iKEq0+e840+WSlKzUnfdbrleytbr9v8A7QH7GvgH9qZtOuPGHh+TUrzSYpIbLULS5ltrmGOQgtH5kTDchYBtrZAPIwSc+VN/wSB+Cclk0f8AZviffId3nf27NvX6cY59wa+B/wDgoP8AsUeFv2LfiXoei+FbrVr6S98Mz6vLqFz5MN1BcJcOiPE1tHF5WFA+7zkA5zX68/B3W7rxN8IfCepX0rXF9qWiWN1cysAGllkto3diBgZLEnp1NfhufUc14ZybAY7Jc2qzw2JdTkS56XLySSfuqpNWk2307tan7nlNTLM+zXG4LNcupKvh/Z8zajUv7SLkkpOEXolZ3XoflV+3l+wRcfsV6jo+pWGtza/4P8S3jWFnLdxJHqGn3SwvN5E+zCTKyRSskqqhG0oy5w7fSf8AwQyGPhN8S/8AsZ7X/wBN8NW/+C5Y/wCLB/D3/sdk/wDTXqNYf/BDPxRZjw38UNB81RqUOp6fq5jJwWt5bZrcOPUCS3cHHQlc4yM/oWc8QZjnvhRLG5hL2lSNSMXKyTajOKTlbS+qV7K+l9W2/wA2ynh/Lck8VFhcugqcKlBy5VtzN6qK6K0b2Wi1tZaL7xvudPuf+uEv/oDV+Xf/AAQS4+KFxx08Bwj/AMjWFfqLJtnt5FbcEkVo2I6gMCDj86+Q/wDgnJ/wTg8RfsTePtW1DWfFGg69Y/2Mmg6b9htpo5rmNZIX+0TB/libbAo8tC4y5O/AAP5Nwbn2AwfDGd4HE1OWrXjQVONm+ZxnJys0mlZNPVryP1viLJsZiuI8ox9CF6dB1+d3Stz0uWOjd3d6aJ266H2Aa/K39ls7v+CxFwD28beJiP8Avi/r9UW6fjX5/wD7Fn7MJ8Z/t8+OviZY+L/D9xp/gvxpr8N7pMUc39qRXM8lxFHHNG6qscRWR3WVWcSeXgYO4L2+GuaYXA5XnUsVLlU8O4R0bvKV1FaJ2u9E3ZeZy8d5TisdjcqeGjzeyxEZy1StFRd3q03utFd+R9/yf6tvpX5K/wDBWf8A5Pi8Sf8AYL03/wBJhX60TPhCPUV+Qv8AwVB8W2Pi/wDbh8bPYyrPHpkdppczqeBPDbqJV/4Cx2n0II7V7/0c6cnxVOSWiozv5e9A+Q+kRUiuE3FvV1IJeerf5Js/Tv8AZM5/ZS+GP/YoaV/6RxV8ff8ABc/ULi4uPhJo73E7aTfSa1d3Fn5hENxLEunrE7r0ZkE8wUn7vmMRzg19hfsljH7KPwx/7FHSv/SOKvjv/gubYXH9qfB2+WGT7Kja5aedj5RPJHp8iR/7zJBMwHpE3pXheFUYS8QMOp2tz1d+/JO3zvt5n1fipUq0+DcZOk2pKnum01quq/qx9XfsJ6/feJ/2MPhbqWqXt3qWoXnhiykuLq6maae4bywNzuxLM3AySST3Oa+Q/wDgunrt9J4z+Dugm8ul0XUNP8R391YrMy29zcQTaJHBI6ggM0aXVwFJzt85iOea+fPh3/wUA+MXwn8B6P4Z0HxlNY6JoVqlnY250yyk8iFBhV3NCWbA7kk1yXxw/aL8aftL67oOoeONcbXLrw3FdWunN9lgt/s8d09q84IijTduazt8bs42nHU1+0cF+DGbZVxfHPMY6UqClVlypycrTjNQsnBK6covfS2h+M8ZeOGR5hwxWyzASqxryhGKfLbVON/eUr7J+p+vn7IN/car+yX8MLq7uLi8urrwlpks09xK0ssztaxks7sSzMTySSSe9eJf8Flv+TQrP/sZrL/0Cavaf2MuP2O/hT/2J2lf+kkdeLf8Flv+TQrP/sZrL/0CavwXgpW47wyX/QT/AO3n7xx1/wAkjjf+vFT/ANIZ+XY60tIOtLX+icdj/MyW51vwF+Bmv/tKfF3R/Bvhlo7fVNQZrn7dKZFj0iGHaz3rGNlceUShXYyMZGjUOhYMP2b+DHw41L4XeA7TSNV8YeJPHV9CoEmra2YftU5x3EKIoH4E+pJ5r83f+CNRx+25MMtz4H1jOP8Ar+0ev1PkXbFwOlfxR9IrP8VWz2OUyt7KlCMloruUtW3K17bKydtL2uf3f9HfI8PhuGFmML89eUubV2XJJwSSvbZXbtd31dkreKfEz/gnR8I/i742vvE+r+EJF1fVG829uLC+ubNLqToZXSJlTzDj5nwC2MnJ5rz7x/8A8EfPhR4r0qRdFuvF3hO92YiuLTUzdIjdi0VxvVh7ArxnkV8S/ty+M7+0/wCCini7XJtUvo7zwp4ltodPvftDiTSYI0tn2QkH92gDOxC8NubdnNfpNc/8FBvgYbmQL8WPAe3ccY1RMYzx7V5+a5Xxrw/gcuxeW46vWjWpqajD2jjTVotRavKLVpJK6S0elkj3Mrx3CnEOMx+HxuCoxlQqunJzjTvNreV2r7rvfz6L8pP2lf2edf8A2XPjBfeD/EU1vfXNvDFeWmo20bRW+q2kjOsdwiMzGMlo5FaIs5jZSNzgq7fpN/wSQ/5Mj8P/APYS1L/0revlH/grj8ePBfx2+Ifw5n8F+KNG8UW+k6Tqsd7JptwJ0tXluLFow5HQsI5CB/sNX1d/wSRP/GEegf8AYS1L/wBK5K+28VM1x+ZeHWAxuaQca8qsedNcrulVjdxsrcySlayWumlj4PwyyPAZR4h5lgcsd6KpJx1vZSdOVr3d0m2lfW293qdh/wAFFP8Akxb4sf8AYt3H/stfmD+xR/yer8Mf+xkh/lJX6ff8FFP+TFvix/2Ldx/7LX5g/sUf8nq/DH/sZYP5SVHgr/yRWc+lT/0yPxi/5LTIf+vi/wDTtM/aIjZF071+X/7Veu6ha/8ABY/SLiPUNQjmsfE/hyxtnW6kUwW8iWnmQIM4WJ/Nk3IPlbe2Qc1+oDcxn6mvyP8A+CkOsXvhf/gop4t1TTp5rPUtHu9H1S0nVAWhkS0tnikAYFWG+JuoIO1h2NfBeA+C+t51i8Kkrzw1WKvtdygvPvr5XPvvGbNHl2T4fGuUlGGIouXLvypttdL37Xs3Y/XHG3cvHp1r8ufip4q1TVf+C0dp52pai39m+N9M020AupFW3tfIhzCqhsCNt7FlxhixzmuKP/BUL48fxePJOf8AqEWAz/5ArnP2e/G+rfE79vTwH4l1++bUtb1zxlp91e3LRpH5smVQYVAqr8qKMADpX6RwZ4Q5rw1DMMfmkqU4yw1WK5XKTUmk7+9CNlZPW99T884k8Yso4grZfgMolUjP6zRbuuVOPNZptSd9WtGrH7Px96/Ln/gs3/yeBp3/AGKGn/8ApVf1+o0fevy5/wCCzf8AyeBp3/Yoaf8A+lV/X5n4A/8AJYUv8FT/ANJPvvHr/ki8V60//TsD5PFFFFf3sf56hRRRQAV9Zf8ABEvwjp/in9vfR5b6NJX0XSL3UbQNjAnVVjVsHuFkYj0IzXybXcfs2fHrVv2Xvjh4e8daLDHdXmg3HmPbSttS8hZSksJYA7d8bMu4A4JBwcYMy2PoOFcwoYHN8Ni8T8EJxb66J6v5bn9F0X+rWnV4d8Cv+Cinwe+PXhGPVtM8daDpbbV8/T9ZvItPvbN2z8jpIwyeD8yFlOOCa7f/AIae+Gv/AEUPwL/4P7X/AOLrE/u7D5xgK1NVaNaDi9U1Jf5nN/t+2M2p/sSfFW3toJ7m4m8MXyRxQxtJJIxhbAVVBLE+gGa/Dr4R/wDC0fgN8QbHxR4V0HxZpevabv8As9yNCml8vepRuGjKnKkjkV+9DftOfDV8f8XE8C/hr9r/APF0f8NO/DYf81G8Df8Ag/tf/jlO76H59xlwfg8+xtHHRxyozpKy5bN3vdNPmTTR+Qv/AA8b/a1H/MS8Yf8AhKD/AOMUD/go3+1sf+Yl4w/8JQf/ACPX69H9p34bD/mo3gb/AMH9r/8AF0f8NO/DY/8ANRvA3/g/tf8A45SuzyP9RcV/0Pqn/gX/ANueSf8ABLPx18S/il+zKviT4o6jqF9resarcPaR3mnLYy2lqgWNV2Kq53Osj7ioOGxyACfpZhuFcGP2mvhoH/5KJ4FJ/wCw/a//ABdQa3+1p8LdB0qa8uviR4Fht7dd8j/27bNtA9g5J/Cg/TstrYXBYOnh6mJU3BJOUpK8u7d29/XQ+Xf+C+Hg3TdX/ZC0nWLiNP7T0TxFALObgOFmR0lTPXDBVJHqintX4+V9hf8ABWv/AIKG6f8AtieMdL8M+D5JpPAvhWaS4W7eMx/2zeMNgmCsNyxxpuVM4Lea5I+6B8e1tHY/kPxWzjB5lxBUrYJqUYpR5ltJpatd0tr9bXWgUUUVR+bjc7XDf3ea/V3/AIJKfF7S/iJ+yDo2g2txG2teBDJpeq2e4edArTSPbzFc58uWM5VuhKSL1QivykPIqbRdVvvDGv2uraTf6hpGr2Kstvf2F1Ja3UCtjcqyxlXCnauVztO0ZBwK/O/EzgKPFmVLAqr7OcJKcZWurpNWautGm9VqnZ66p/qXhV4iLhLM54qrSdSnUjyySaUlZppq+jttZ2vffQ/cr4u/CPw38dPAt54b8VaVDq+j3hV3hkYo0ciHKSRupDJIp5DKQR9CRXwJ/wAFAv2LPgr+yL8D/temr4mk8Y+JJm0/QLefW2k2yABprp125aOBCGOeCzRqT84r53H7bXxkEEcY+KPjYLGMD/iYDd+JK5P4muO+IXxQ8T/F7WodS8WeItY8S39rb/ZIJ9RuPOaCLdvKL2UFuTjqQM5wMflvAvg3xJkmOpurmPLhlLmnTpTqLna2TVkrN2Unvy6Lpb9f4z8cOFs1wFT2GAc8Ty2hKrTpyUb9btyemrStZvfuYRO6TpjJzzX6+f8ABMBtv7B/w994Lz/0uuK/ITFd14F/am+J3ww8MWuieHfH3irRtHsd/wBnsra7AhgDuXYKCp4LMx/Gv0Lxa4DxnFeWUsDg6kYShUU253tZRlG2ibveSPy/wd8QMv4Ux9fF5hCco1IKK5Em78yet5R00P0v/wCCsDbv2E/F3/X3pf8A6cIK/LP4W/EzWPgt8SdD8W+HZobfWvD90Lu2MyloZflZHikCkExyRu6MAc4YkcgVuePv2oviX8VPC9xofiTx54n1zR7p43nsrq4RoZjG6yJuAQHh1U9e1cKBxWfhj4d1uHskr5RmkoVVVnJtRu4uMoRi07pPWzv5M6/FLxMw+e51hc3yXnpyoRVnJRTUlJyTVnJNbb79VY/VH4Rf8FefhH4+0K3fxFeal4C1hkzc2Go2ktzBC4ALCO5hRkkTJIViEYjqqngaPi7/AIK1/BTwu6W9n4i1vxBLNNFCsel6RcNGWlcKDvkWOPAJGTnivyeAwfSmyx+YvDOrAhgyNtZSCGBB7EEA59q+VrfRw4blWdSnVqxj0jzRsvK7g5W9W35n1WE+kvnShCniMPTb05pJSu11ajzWvbbW1+h+/Bi8iRlPLRsVOOnFfnL/AMF0+fih8IT/ANQfXf8A0o02vm0/tn/GBmLf8LR8clj3/tI/4VynxG+Lfiz4xX9hc+LfE2t+Jp9Lilgs5NRuPONskhRpAnAxuMcef9wV4vh54H5rw9xBh83xOIpzhT57qPNd81OUFa8Ut5J+h7XiB465HnvD2JynC0asalRRSclFLScZO9pt7LtuYuj6rfeG9ZTUtJ1LVtE1SNPLS+0q/m0+7VM5KebCyOUJ5KElT3BrqvGn7RnxH+JGl/YPEHxE8datYEMr2suuXEcMysMMsiRsolUjgrJuX2rkBxRX9GYjKcFXqqvXowlNbScU2vRtXR/NeD4mzbCUPq2FxVSFP+WM5JfcnYbZ20dokMUUccUMW1UjjUKiKOgCjgAegr9gv+CZ7f8AGCnw1/7B0n/pRNX4/wCcGu68G/tSfE74deGLTRdB+IXi7SNH09SttZ216Fht1LFiFG04GST+NfnPi1wDjOK8vo4PB1IwlCfO3O9muVq2ieup+keD3iFgOFcdiMVmMJzVSKiuRJu6lfW8on6Xf8FZDn9hTxZ/196b/wClsNfHv/BG/wD5PTb/ALFLVT/5MadXhvjr9pf4kfFHw5Lo3iTx54r1zSLh0kmsry93wzFGDruAAzhgD+FYPgD4ieIvhR4lXWPC+uap4f1ZbeS1+12EvlSNDIULxk4IIJjjP1UV8/wz4V5hlvB2O4cq1oOpXcnGS5uVc0YR1vG/2eie59VxH4uZRj+Mcv4go06ipYeLUk1Hmd+fZczX2lu0fu9JJujb6V+Nf/BQj/k+P4pf9hlP/SWCqK/ty/GhFwPil4y/8Ck/+IrzrxR4o1Tx14mv9a1zUbzVtY1Sbz7u8unDSzvtVckgAfdVRwB0rk8J/CPMuFc0q4/G1qc4zpuCUOa93KMr6xStaL6mni54wZPxRk0MvwFOpGcakZ3mopWUZLpKTvquhSooor+gT+bQqvdaVbX0qyT2trcSou1ZJYld0HsxG4fgasUUPXQ1o1qlKSnTbTWzTs/vKcOh2dteJcR2dqlxGCFmEK+aoznAbG7r70uuD/iSXndjC38jVumTwrcRsjqrxsCrKejA9RU8vu8q2Oujj6jxUMTXk5NNN3d27O+7P6Abq3k+1S/upT87chD6mvgn/gulE8XhL4V7lZf+JxqPUY/5c1/wr4FPi7XnO5vE3ixmbkk6/fEk/wDf2q1/qd7q7RG+1HVtR8jcYvtuoz3Qi3ABioldgpIABIwSBiv5u4J8Ba+Q53h83njFNUm3y8jV7xcd+d237H9McZfSAy7OckxOVUcLOMqseVNuNltq7M+3P+CFELP8TPjDtVmxo3h3O0Zx/pOs1+jP2aUkfuZev9w1+BNjqF5pczyWd/qenySKEkeyvprVpVBJUMY3XcAWYjdnBY4xk1a/4SzXP+hk8V/+D+9/+O1vx94F1+I89r5xDGKmqnJ7rg5W5YRhvzK9+W+3Uw4E8e8vyDIsPlNbCznKkmm042d5SlpfXZmp8f2lh+O/xGaGFpprfxrrU6QhgplaPVrhwgJ4BbbtyeATzX7afCj4s+H/AI9+CbPxV4R1KHW9E1QeZFPAdxiYgFopF6xypnDI4DKQQQK/CdVILFmeRmZpGZ3LvIzEszMzElmZiSSSSSSSc0ljG2lanNe2M11pt7cxiKa6sbmSznnQchXkiZWZR2DEgV9V4heEsOJ8FhKKxHsqmHjyp8vMpJqKd1dNfCmmm+qae6+Y8P8Axkp8O4/G1KuHdSjiajqWTSlFtt+jumk9Va10fuP8Y/2ZfAn7Qq6avjvwL4f8YJo7u9mur6al0LffjzEG4HMcm1d8bZjfYu5W2jHbPBcOctFMzMck7Dya/BP/AISzXP8AoZPFf/g+vf8A47VPXrq68VaY9lq1/q2s2MhBe11HUbi8t3IORmOV2Q47ZHFflr+jZjqkYUauZ+5G9l7NtRvq7L2lld6u1rvc/UP+Jmspi5Tp4GfM/OKv2u/+HPtT/gsV+1D4V+LA8K+AfC+pWOvXPhvVn1vWL6yuFntrOQW01tFZhlyryt9peR8N+7EKqQTICvyR8K/ix4k+B3j2w8UeE9Wm0fW9PDokyqJI5onxvhljPyyRPtUlG7qpBDKCOeijWGJEVVRIwFVFGFQDoABwB7CnV/QHCvA+AyTI45Ev3tO0ubnSfO5fFdbJPZLokrtu7f8AO3F/iJmGd5//AG/T/czjZQ5XrFRvb3rK71d3ZXva1tD7W8Pf8FxvGOnaWsWqfDDwhrV6Cd13a+JLrSo29P8AR2tbkj8JufavUP2Cf+ChPjz9rT9pC58P63pnhnS9Dj0S71CK20u2naZZI5bdV3zySEvxMRgIgyAcdh+bNOtrq4sLpbi1ur6yuI1ZFmtLqW2lCtjcu+NlbB2jIzg4HpXyOceB/C9bBVqWXYaNGtNWU26k+V91GU+X7rW6H2uQ+PnElLH0ama1nVoRb5oxjTi5e60tVFbOz0a2P34+zTH/AJYzf98Gvxh8RfHDxR+zt+298Q/FHhPUv7O1a18Wa3bSLJF5tveQPfTFoJ48jzI9wDAEgqwDAg5z5f8A8JTrgPHiTxYPp4hv+f8AyNVEbmZmkkmmkkYu8ksjSSSMTkszsSzMSSSSSTmvN8OvBtcOzxSx9aOIp14KDjyWVr31u5J/0z1vEbxwp55h8P8A2TTqUK1GpzqTcf5ZR0s3vfVPRq6eh9AfEn/gqH8bfiPpEtj/AMJVbeHbedPLkbQdOSxuGHfbMWkkQnplCCOoIPNfPtrbR2sSRKvlwrxxk49TzySckkk5JyScnNSUda/Wsl4cyvKKbp5Zh4UVLV8sUr22u1q7dL7dD8Xz7i7OM7lGWa4iVXl2u9F6JWSfmkfrd/wTd/aa8N/G/wDZz8K+HbG+tYfFng/RrbS9U0VpwbuIW6LCtwqHDPDIFDB1GBuKkhga9t+I/wAJNA+MPhO40HxV4b0/xFot0Q0lnqFmJ4i652yKCMrIpOVdSGU8gg81+Dl7pdvqTQG4t4ZmtZBNAzoC0Eg6OjdUYdmUgj1rV/4SzXCf+Rk8Wc8/8h++/wDj1fzznP0cvaZjPG5XjnSjKTkk4NuDbvpJTV7PZ2TStq3qf0nk30lqEcDGhmmDlKokk3Fq0tLN2drX6rVeZ9b/APBU34cfB/8AZ9tdH8E+CfBOl6b4y1ULqd/fQ3dyZNHskf8AdrtaUjzLiRWUAg4jilJGSmfjuLiRfqDxTriaW9upJ7i4urq4mwZJrm4kuJpMAKNzyFmbAAAyeAAOlNxjGONtfu3BnDtTJMrhga9eVepduU5ttyk+3M20krJK/S+7Z+BeIHF8OIs1eMoUlRpJKMIJJWS3bsrNt3fkrLoftf8AsZW0p/Y8+FOIZTnwfpXRD/z6R14t/wAFmbeSP9kGz3RyL/xU1lyVI/gnr8wbXXtV0+zht7fXPElvb26COKKDW7yKOJRwFVVlAVQOAAABTL7VtR1WNVvdX17UI0besV7q11dRhhkBgkkjLkZPOMjJr8VyPwFxGX8QUs7eMjJQq+05eRpv3r2vzP77H7txB9ITLcxyWvlcMLUjKpTlBNuNk5RcbvW9iEdaWiiv6UP5PZ6r+xN+0jD+yf8AtGaX4wvrO4v9IeyudG1WK3TfcR2ly0LtLEv8TxyW8LbRyyh1HJFfsD8Mfir4b+NfhePWvB+vaV4m0uTH+kadcLcLGSM7XA+aNvVXAYHIIr8KSM1DHYRQa3FqkUf2fVIAVivoGMN3GCMELMhEgHsGr8X8SPBvCcU4mOYU67o10lFvl5oyS2urxaava6e1lbQ/ePDHxsrcMYL+y8VQ9rRTbjZ8so3d2tU1JN69Gm3q1ov20+Kn7GHwz+Nnittd8U+ANH1bWnjWKS/aGSC4nRQAgkeJkMm0DCl8lRwMDivIP2m/2Zf2ff2WfgtrHjLXPhvYzx6egis7FdQu4pdUu3+WG2RmlIBdurEEKoZiMA1+X3/CWa5/0Mfir/wfXv8A8dqtqGpXusmL7dqWr6kIGLxC91G4uliYggsqyOwVsEjIGcEjOCa+RynwLznC1KVOrnFT2EGrwhzxvFPWMbVLRutL203sfdZp9IPIsRSqTpZZes07Oag1zW0b3bSIpriS8uZppVt43uJnnaK33/Z4C7FjHEHZmEa52qGYnAGSTmv1i/4JH28j/sQ+H2WORl/tLUuQpP8Ay9yV+TpGR3qxZa3qWlweTZ6xrtjBuL+VaardW8YYnJISOQKCTycDmv07xK4ClxRlNPLKNZUuScZXcXLRRlG1rr+be/Q/J/DHxIpcM5viM0xtOVV1YtPlstXKMm+ito9EfsV/wUVt5F/YU+LDNHIo/wCEbuOSpHpX5JfCP4lP8GPjT4X8YLZyagvhjWINRktYiPMuYkk/eImSBvMZfbkgbscjrWHe+INW1G1kt7rXPEd1bzDbLDPrN3NFKvoyNKVYH0IIqqMtnd1Y5PvXn+HXhmuHMqxOV4usq0a7d7RcfdceVr4nuup6XiR4rU8/zTBZpl1KVKWGd1zWd3zRktunu6n7rfCv4o+HPjf4Ot/EHhDWrHxJol1lUu7GTzFVgASki/ejkGRuRwGU8ECsb4yfsreA/wBoOeym8aeCdJ8Q3WmqY7S6uLZkurdGyWjWaMrJ5ZJyY92wsASMgEfiBaxf2brDalZtNY6k0XkNe2cz2t08eQdhljKuVyAdpOOK0U8Wa4o/5GTxYP8AuYL7P/o6vyv/AIlvxWGxXt8tzNwtflfI1NeXNGa6aNpK/Y/Vqf0mMtr4f2eOwEm38SUoyj+KX4o9M/bhvPh7/wANC6hpHwz8O6foXhzwmJdKeezuJpRq92HH2iUl3YGONkEUYHdZmJIdQuT+yAC37XXwtAGf+KrsOn/XSvOILeO1hSONFjjjUIiqMKoHQAU9WaOVJI5JopImEkckUjRyRsDkMrqQysOxBBFf0PR4e9jkTyWNWUv3coc825SbkmnJ3bbu23a+mysrH864njD6zxPHiCrTUYxqxmoRSSUYNWitleyte2ru3ufvytrMoP7mb/vg1+XH/BZ5Gj/bB04MrL/xSGn8EY/5er+vln/hKtcJ/wCRj8Wfh4gvh/7WqrcXFxfXTT3V5qN/cMqoZr29mu5Nq5KqGlZiFG44AOMk+tfj/h34JYjhrOoZrUxcaijGS5VBxfvK2/M9vQ/YPEXxyy/iPIa2U0MNOEpuLTbjZcs4y6O/Sw0UUUV/Qp/M4UUUUAFFFFAH1p8PP2N/Afjn9mez16w0fxR40vJdAn1PXNc8PazbXFx4Sv08zy7OXSCvmyQkKpaXzASCSoABq18Gv+CefgH4m2Wkw6hrl54euNe+Fum+Kbe+uZYza2+sXupPZRI/yZ+zl/KXGQRvJ3dq88+GX7d7fC7TdBvrP4a+B2+IPhHTH0jRPGCLNBdWkLI6eZLbIwhnuFWRlWV8cY3K+OcDXP2wtW1z4YSeFZNC0tLObwLZeBGmSeXzPs9rfG9S4/66F/lIzjHI5rHldz9Wo5pw3CnTlUhGTUNYqLSukrXdk7t3vrJrfnd0o+haB+wRoOnato7+LI9e0iz8P/D/AFDxn40sYvK+2iaz1C7s3s7csu2Ms0MS723AfM3IIxn/AAc+DnwY/af/AGgPBGi+GdL8XaHDq9nqMniDw9PqUEzWUlvA8tv9mv2iUMs20hvMjHllTk4PFHxH/wAFIvHHjL4neG/FesWGiaxf6P4UbwbqkN8sk1v4msHaQyC6GQwdxJyyMPmUMPSq+n/txw+GfiJ4R1HQvhr4Q0fwz4Ls9QsrTQI5p5Pta30bR3LXN2x8+ViGOOgXsBkknKZxzDh5VYcnL7NShzKUG5SScLtPVRXKpcy1u29HdOOL+118GPDfwkbw3Ho3hPVvDcupR3Es/wBu8Xab4iSdU8oLtNmi+UVLnO/7+Rt+6a1bL9lvwLq37MXg3xJa+LFk8ReIvHmn+GNYuJLTyNN8Pw3VpLMVO/Bmkj2q7yZVBkoOAXPAfFr4i+FfHlppkfhv4c6L4BNi0pnaw1S7vftwYIFDfaGYLs2nG3Gd5znim2PxpvLH4Er4BGnafJYL4pi8VG6lBkeSWO1kthA0Z+RoishJzycY6Hg5Xy6Hh/X8sjmFeVSMZ05QtGyaSlZbcsaaTbum+TTVrXU+gP2nv2M/h/8As4WFj4o1Dwb8ULTwxY+MdT8HXmmX15bw3uuLBFvtdTs5mtlVYZSrAoUYEj5X4JZvxB/ZU+E/grx142lh0fxwNL+E/g6HW/FWkHWbY3R1S4nhjSwjult9qiJZiZHEbZeNlGMZPnnxE/bYm8b6h4HjsfBHh3w/4f8ABfiCHxOdFgubm7t9TvU8lSrtOzmO38qBYUgjwkcbEDdgYxPBX7Ut14f+KnjvxBrnh/TfFum/ExLuDxFo95dzwR3cVxcrdEJPGfMjdJUQq/JwCCDmp5X0PexGc5D9YcKUI8kmteTRNQfvfCm48/LePKtFL3XzFP8Aa4+Cdn+zt+0Z4m8IabfXGo6Xps0ctjPOAJjBNCk0ayY4LqsgUkcEjPevOa6v45fGPVv2gvi3rnjLXEs4dS12cTPDaRmO3tkVVSOKNSSQiIqqMknjk1ylbx2PzvNqmHnja08IrU3KTiu0bu34BRRRTPPCiiigAooooAKKD0rW8J+Ade8evcLoei6trTWoUzCxs5Lgwhs7S2wHbnBxnrg0GlOlOpLlgrvstTJorr/+GeviB/0I/i//AME1x/8AEUf8M9fED/oRvF//AIJrj/4ig6v7Nxf/AD6l/wCAv/I5Ciuv/wCGeviB/wBCN4v/APBNcf8AxFH/AAz18QP+hG8X/wDgmuP/AIigP7Nxf/PuX/gL/wAjkKK6/wD4Z6+IH/QjeL//AATXH/xFH/DPXxA/6Ebxf/4Jrj/4igP7Nxf/AD7l/wCAv/I5Ciuv/wCGeviB/wBCN4v/APBNcf8AxFH/AAz18QP+hG8X/wDgmuP/AIigP7Nxf/PuX/gL/wAjkKK6/wD4Z6+IH/QjeL//AATXH/xFH/DPXxA/6Ebxf/4Jrj/4igP7Nxf/AD7l/wCAv/I5Ciuv/wCGeviB/wBCN4v/APBNcf8AxFH/AAz18QP+hG8X/wDgmuP/AIigP7Nxf/PuX/gL/wAjkKK6/wD4Z6+IH/QjeL//AATXH/xFH/DPXxA/6Efxf/4J7j/4igX9m4v/AJ9y/wDAX/kcgqtI4VVZmY4CgckmsvUfFtjp7snmSTyKcERKCAf97OPyrQ+MPhbXvhraWdrrGkapo11qgdo0vrSS3d4k2hiu8DIJbBxnGPevP0hZ5VjjV5HYhVRFLMx9AByT7Cvms2zqdGp7GitVu35n9wfRz+irgOKsnXE/Fc5xoVHJUqUGouShJxlOcmm0ueMoqKUW+VycrNJ9OfH9sP8Al3uvzWk/4T+2H/Lvc/8AfS17RpX/AASw+Kms6n8GdMitLW11741Wup31hpmoJJZzaJbWJtzJNebgSoZLiNwqruAwCMnAofF7/gnrrXwt+H9n4ys/GfhfxP4F/wCEql8G6xrtpbXdrH4ev4rk28puIZkDtCrAnzI9wOMAcgnz/wC1MySu1t5Lyf5Nel9T+gI/Rf8AB2U404xm3K6Vq1R3aco200u5Qkor7Ti+W55N/wAJ/b5/49rr81o/4T+3/wCfa6/76WvXfHP7Alro3wI8WePvCfxg+HfxHsfB81nbXljodvercTT3UvlQQRGRAskzt92JSWbt1Gd34mf8EpPGXw38PeM1j8YeC9e8afDfRrfxB4q8Jae9wb/R7KZGfzBK0YhmKKpLLG3AHuu5/wBqZl0t32Xn9+z+4X/EsHg7dJ06id+WzqVk0/d3TScV78PelaPvJXuzwX/hPrf/AJ9rr/vpahvPihp+nKrXCyQK2MGSRFHPA6/UD8aX4XeEdD8c67Pa694y03wPZx2pnivr7Tbq+iuHDoPJC26s6sVYsGI24QjOSM/eP/BPT9nDUv2Xdf8ABvxa0n40eDfE3ws8ea9b+CdY0u10zVfs+uSXTtbJbXMDR7VkSSU7GnTapcjIWQkmHzbHVZJKSt6LRd7XJz76LXhXllGU5YSrKf2Y+1rJSla6ipqMo8zSdlrrvbW3wn/wn9v/AM+11/30tH/Cf2//AD7XX5rXpn/BRj9lTRv2Nv2q/EHgvQ/EFprFjFMbyGzSGVJ9FgmCywW8zOoSRgj8NGSNqjOGyKsfs3/8E/NX/ay8C6xd+B/HXgnVfGGiaSdbn8Hn7THqJtw7JtErRiAykqPkDELvj3Mu4ET/AGtmHtHSTV15I2j9Fnwj+oU8znRqxozSak6tVWUtnL+VebsttbNN+V/8J9b/APPtdf8AfS0f8J9b/wDPtdfmtfTHgf8A4JAah8RPBnw017S/i94HbT/i9O9r4X8/RdSjkvZUhmmdHXZ+6ISCXl8AleCciqfjH/glBJ4B+HHjbxbqvxn8BW+g/DvXD4b16ddE1SRrO/HlZiCLHuk/10fzICvzdeDWn9o5ny8ztb5dr/lqcf8AxLX4M+09ioVea9rc9e9+blta178y5bfzabnzn/wn1v8A8+11+a0f8J9bn/l1uvzWvfPjD/wTHt/gR4b8Bat4l+M/gu3s/ida/bfDItfDur3k2px7IXGI44i6sRcQ4UgMS+McGug8F/8ABKDUoPBjfEF/HHh7VNB8LeILWx1DR7/wXr8d5eXCyQyCzey8gXLJKropKJ9xyQeKFmWZc3Lp+Gn9bhL6NPg2qUaypVXGTtF89e0ne1k7Wb5ly+uh8t2vxU02+KeTum8wMU2So3mBThiuPvYPBxnB461Y/wCE+t/+fa6/76Wv1K+Ej6b/AMFE/iN8fvhr4g8f2etW/iLw7BqWi+EbzStTiHw5vrNFt/Ptxd20JiTzLiMlUVHKnGCCSfyy8C+DfDvirxrcaff+PtI0TRY0d7fX5tLvLi1vwCNhWGNDMgkUlhuXK9DzVYjNMZT5WppqV1slt836/Oz1M8j+jB4YY91qdbAVqc6ShJr2taWk48yt7sW2neNknfl5otxejbz4nafp8e64WSBepaSRFAHTqfqKl/4T63/59br81r7S/YP+BOpfsqT6N8ffC3x68Ial8O7TXrXQfEtpY6dq8Y1FZJBELW6t/LyG/f5jeVCqNIGHDEnkP24/+CdXgn4O/tl+IPB2kfFDRdBfWLkalo3hiPwtq2pXlpDcZkW3QWsTq6qQ4ULkhAoIGKbzDMFSVTmX/kvXZp366mNP6NvhJLMJYJ4WtZRbUlUrt3i1zxlHlvFxvF9VZ62e/wAuf8J9b/8APtdfmtH/AAn1uf8Al1uvzWvpWx/4JDale/DfSvFbfFnwhYaPrXiGPwpbi/8ADmr2l0upyTeStvLbyRCWI7xgl1AHU4rxz9rT9lCT9kD49L8P9Y8YaFrWoW6QNql3p1pcrDpJlb7rrIoaTbGVkzHnIbaPmFZVM0zGEeeVradup6mC+i74P4uv9Ww9KrKeuntKy+G3NuujaTXdpbs4e8+J+n6coa4WSBWzgySoucdev4fnVqHx3ZyfejuowejYVh+hr7M/Yn/Zx1n9kaPwv+0Z4Z+OvhOb4e/2/D4e1BINM1iBdcElz9la0lg8vLfO+Ud0Ko2H4wTXj/8AwVi/ZH8N/sd/ta6xovhjWrC40/W5ZNXh0KG1lil8OwTEPHEXKiN42YyiMRklFjAYDjOlTMsfCl7WTXTSy2ez0etzzcP9GPwoxuYSyuhh6ybjLlmqlW6lGynFqcbRlG8XrzJ3s0nZPym2uo721WeGRZIWJAYeo6g+/SpK4nwzqraNqynrFPiOQeoPAP4Gu2ZWRiG6g4r3crzBYulzNWa0a/yP4a+kB4L1fDnP44GnVdXDVo89KbSUrJ2lCSWnNB2u1ZSTjK0buKKKKK9M/CAooooAKKKKACiiigAooooAKKKKACiiigAoooJxQAUUHpTpYJIEhaSOSNbhPNiLKVEiZK7l9RuVhkcZUjsaClFtXQ2ijNFBIUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABUcttHMBvRXx0DDOKkooGm1sQ/wBnW/8Azwh/74FH9nW//PCH/vgVNRS5UV7SXch/s+3/AOeEP/fAo/s63/54Q/8AfAqaijlQe0l3If7Ot/8AnhD/AN8Cj+zrf/nhD/3wKmoo5UHtJdyH+zrf/nhD/wB8Cj+zrf8A54Q/98CpqKOVB7SXch/s63/54Q/98Cj+zrf/AJ4Q/wDfAqaijlQe0l3If7Pt/wDnhD/3wKP7Ot/+eEP/AHwKmoo5UHtJdyH+zrf/AJ4Q/wDfAo/s63/54Q/98CpqDyKOVB7SXc5vx7YRxR2c0aRoPmhYKoHIO4Z+uT/3yaydC8Q6h4U1+z1bSb6803U9OmS5s7y1lMU9rKpBWSN15VgeQRyCK7W8to762aCZS8b9RnGPce9ctf8Agq8tZf3H+lJ/CVIVvxHb8zXx2dZXW9u69FNp9t07Jfof6efRT8fOGafC1HhHiDEww1fDOShKq1GFWnOcpr35WipRcnFxk03FRcea8lH9YPi740+K2o6/+wFrnw9n0nxB8R9Q+HWqXcq+JL4pFrYfTdDe6WWZjvaRwWfcDuLAnnmvMv2nviTp+tf8ETfiZqEvhXRfAtx/wtu+gvNNsNQnvoZ76LU991Iksx3SFnSZvlCqFThRgivgjWPHvxE8QweGYtQ8Q+LL2PwWoTw8JtTkb+wQBGALX5sw4EMQ+THEaDsKX4qeP/iN8dZbOTxx4j8WeMm09HjtP7b1KS++yq+N4QSOQu7ABx1AA6VnUrVpKVqcveVtu6Svtfp89D9lwGO4QozwrnnWCtRmptxxFNNpValRRt7Tla99LVXhZ2bTsfZX7X2l6b+wl8JP2PfhnqZitLa01qD4leOXPypLcie1XzJDyGEKSXAzngQIfSvpnT7+H4S/8FPf2w/FniS2mh8O6X8NbG+up3QGJonhJVOTtJfyJVUZ+Yow6g1+S3xB8XePPi1e2dz4r1jxJ4nuNOg+y2kuq3zXb2sOc+WhdjtXPOBxV7xJ8VPid4w+Gun+DNW8U+MNU8I6TsFnot1qkkljbiPHlgRFtuEwNoPC4GMYojWqxk3GlKytZWfSLVn99/8AhzTEZnwpXw0KdXPcHzyU41ZfWKWvtK0arlH391y8qTto07+7Z7v/AATd+HHhf4j/ALWXwx8H+PNPg1Tw/r9wmk3tvLdyW4aV4WWPDxlW3eaFAGeSQO9fYHiv9qzSfgv+2npH7L9v8MdM0f4c+E/iZor6PZ6Tq1xp9612dQt3h1G7eVJjcoxkR/LHlhti4kIwB+ec3g/ULiIxyWMkitwQwBB7+teheJf2h/jP400nTLHWfiB8RNXsdFuob6xt7zXZ5o7W4hYPDMoZz+8jYBlY5KkZBzXJho4ilDlVOV778t7rtqtup9Bn3FfB2Y4xV6mc4WVPktyPFwiozvdVI8tSzlZuNrLT7XQ9e/4LiusP/BTj4hMf4bTSycL/ANOEPavav+CV/wACPFFxJ8QPhh4k8H3Xw+0/xP4HGot8TdGjlhu/srlZok+3MzWs0DpKcpFsOISGYlcr8F+Nb/xd8SvE1xrXiS91rxBrV2EWe/1G5NzczBFCqGdmJIVQAPQU5dX8aJ8P5PCK6t4mXwnNKZpNDGpSjTHckMSbbf5Rywyfl5PJ55q6aqxxEq7pSd29LPr02/FGOI4o4UqZFQyaOeYNOEIRcvb0nrDltJL2iTs02oyTTur2tZ/rV8ArfQ7b4E/sAx+G77VNU0GPxTerZXWo6fHYXVxH/ZWq4Z4I5ZVj74AkbjBzzgeb/FyFvjl8BP20PhN4Zksb34hR/FSXxBBosl5Fb3F9ZBbJmliEhUMF+zz7ucLsySAwz+eul/Fj4oaHp3h+zsvFnjK0s/CcrT6HBDqsiR6NIyurPbKGxExV3BK4JDt61y/izRta8fa3dalr0d5rmpX0xuLm61BxczXEp6u7OSWY+p5ronXrOnyKjLts19lR7M8HDY3hOOMeKnn+EunzRft6MnzKu6y5lzwTTT5ZWcddVofstr/ibw78Avjx+wfJ451HRdPtdO8Cazpq6hNdRSWNveHTtJRHE4Jj2nDqsgbad4IODXk918PPjd8Lf+CU/wAbNC1DWNbi+Let/FCW3trzT9eC3l3JPdWqxlJ0kBhEvVUZkIR1GACAfzW8YeIvHHxC0TRdN8Qar4g1zTvDdubTSbW/vGuIdMhKopjgVmIjTbHGMLgYRfSotP1PxhpHw2uvBlpfa1a+Dr6f7Tc6DDclNNuJfl+d4AdjN8q8kfwj0qp4mtJy/dSs07Wunqku3lo+nZmGHxHCdKlQ/wCF7BOUZRck61JpqNapUVn7VNXVS0ovSTS1Vrn7Z/DrxTofiL/gr1LbWd7pepeLdF+Ck1h4unsWVwt8up2W2KRl48xR5mRnKhlBxwK/GL9gTw94W8YftFfCfQ/HFnBfeEte1Cz0zU4pp5IECTx+WjF4yGXEjR4OcevGazfhn4g8bfBXU5r7wZqmv+Eby4tms5Z9Gu2spJICyuYi0bAlCyI23plQawW8FXrWvkf2cxh27PLKrt29MYz0xxiubFSr1nGTpPRttWdtbabeWuh7/DufcH5VSxGHjn2FaqU4QjJYikpJxU05W9pprO8Um7JWv1PvL9qL9pbRf2V/jx4l/Zit/hPY6T8L7HxFp97BZ6XrN1puqaxI8tpJFf3FzKk5mh3ApsVVViijeNu0/XXxH8TWOn/8FIv2j9D03UrHR/iv4p+FWm2PgC7uLhbdzeeVqAeKF2IxI0z2LAAFiIieinP5H+IPjn8XvF3gm18M6t458f6p4dsmjaDTLvXJ5rWMxkGM7Gcg7CoK5ztIBHNc78R9U8YfGPXpNU8YX2t+LNTmRY3u9YuTezOq/dUtIxOB2Hat44ivFt+yk9bpctrKzVrpdL6M8etiuDcRCEamd4SMuRqclioTc5uVKXO1Kol73s/fV9U7cz3X6efDj4feKPhj/wAEx/gjo/jOG8tvEtv8bNI+2QXl8t5dwMdTbCTOHciQDAKs25cYIHSvkH/gtiJG/wCCkvxI8tgsm2zCEjIB+yR4zXhuheNfiB4X8G6f4b0vXPE2m+HdIvF1Gw0u1v3hs7K6Vt6zxRKwVJA3zBgAQeao+NL/AMXfEnxLca14jvNa1/WLzb599qNybm5m2jC7nZixwAAM9BWOI9rUoqlGlJWt0b2TXbzPY4f4k4VwGaVMxrZ5g5KftdI16S/iThJaOo7JcrVrvdas/Qq9/ab/AGe9B/4JUeAvBvhGAa1fTeMNGkvPCmrapcRajbai95HLc3DNGwbylkDOjL+6OVGOdo8p/wCDgBc/8FGtR/7FvTfx/wBdXxzB4a1W1uI5orW4jmhdZI5FIVo2UhlYHPBBAIPYgGtrx3rnjX4veJW1jxXq2t+ItYkjWF7/AFe+NzOUXO1S7ksQMnA7ZorTxNWl7J0mvhtaNtr/AOZnl/EHA+VZisyhnuGlH97KXPiqUpXqOD09+2nK7u3M7q97HN6bbtfarbRR4LvKgGRkcEdR6evtXs0vxi1RpGZbHwrtYkjPhuw/+NVwfh7w6uhBpGk824ZSpI4VAeoHrn1rUr6DJMvnh6TdX4pdO1j/AD7+lR42YHjHiGhQ4dqOWFwkZRVRXj7Sc2nNpNJ8q5Yxi2tWm1o0dN/wuHVP+fHwp/4Tdj/8ao/4XDqn/Pj4U/8ACbsf/jVczRXtcqP5d/tjGf8AP2X3s6b/AIXDqn/Pj4U/8Jux/wDjVH/C4dU/58fCn/hN2P8A8armaKOVB/bGM/5+y+9nTf8AC4dU/wCfHwp/4Tdj/wDGqP8AhcOqf8+PhT/wm7H/AONVzNFHKg/tjGf8/Zfezpv+Fw6p/wA+PhT/AMJux/8AjVH/AAuHVP8Anx8Kf+E3Y/8AxquZoo5UH9sYz/n7L72dN/wuHVP+fHwp/wCE3Y//ABqj/hcOqf8APj4U/wDCbsf/AI1XM0UcqD+2MZ/z9l97Om/4XDqn/Pj4U/8ACbsf/jVH/C4dU/58fCn/AITdj/8AGq5mijlQf2xjP+fsvvZ03/C4dU/58fCn/hN2P/xqj/hcOqf8+PhT/wAJux/+NVzNFHKg/tjGf8/Zfezpv+Fw6p/z4+FP/Cbsf/jVY/iHxLceKbxbi4h0+F1QR4s7GG0QgEkZWJVBPJ5PNUa9u/Ya/Yo1j9sr4jSW4kudL8I6MyPrerRoN0ak5FvCTwZ3AOOoQAuQQArGiOzL6eZZviIZfh3KpKbslf8AF30SW7b0S1Oi/wCCeH7Bl3+1140bVtaE1l8P9DnVdQmVjHLqkg5+yQN6nje4+4p4+YgD9Cv2xv2F/Df7VfwhsPD9rDp/h3WPDMBi8NXkUOyDTlCgC1dVGfszbVBABKEBwCdwb17wR4J0n4deENN8P6DYw6XoujwC2s7SEERwIMnAzkkkkkkkkkkkkk1rsu4Vm5Nn9fcMeG+WZZlE8uxEVUlVX7yTW/ZLqlH7PW/vaPb8BvHngXWPhf4y1Lw94g0+40vWdHmNvd2swG6Jxz1HBBBBDAkEEEEg1k1+wX/BQn9g2y/bB8HR6hpbW9h4+0K3KabdSfJHqMWd32Odv7uclHP3GY5+Vjj8h9c0S+8La5eaXqlncafqWmzvbXdrcIY5reVCVZGU8gggitIyufy/x5wNieHcZyO8qMvgn3XZ9pLr33WhVoooqj4MKKKM0AFWtC0K+8Va5Z6XpdndahqWoSrBa2ttGZJriRuiqo5JNbnwd+DfiX4+eP7Pwz4T0u41XVr45CIMR28YIDTSv0jjXIyzccgckgH9Z/2Kf2C/C/7FvhyfUprm11rxhcWxOp6/Mghhs4sFnjt9/wDqYQPvOxDOFy2F+QRKR+gcD+H+N4hrc69yhF+9N7eaj3f4Lq9r/nh+0T/wTb+JP7Nfwv0/xZrFvZalp8kPmasmnOZn8PuTwtwehXBGZEJQNkEjALeAg1+2nwH/AG1/hv8AtKeMNc8O+FNehvtS0lnj8iePy11aDBDzWwb/AF8OMhuM4IJXawJ+Sf29/wDgky1o154z+EOnyTxsTLf+FbaMvJHnkvZAZLL6wdRj5NwOxVGR9nxd4V0Pqv8AanDE/bUo3UopqTTjo3Frfu1ut1dbfANFMadUXJZVHTJNJFcLN91lb1wa0uj8O5WSUUUUEhRRRQAUUUUAFFFFABRRRQAUUUZoAK3Phx8MvEfxh8XwaD4V0XUvEGs3CmRLSxgMsmwYBc44VBkZZiAMjmsPPFfpf/wTJ1nSvgX+yxZ3NrHBHq/jCaS/1O5U/vJ0R2SCMnrtRQSF6AyOepos3sfbcB8JriDMvqk58kIxcpPd2TSsvNtr0V3rax8UfFP9hT4wfBTwjNr3ib4f6/pui2qB7i88tJorZe7SGNm2Ady2AO5rycGv2ui/aZTzCC6MrAqyMdysp4KkHgg8gg9QTX5J/tYfD/S/hh+0T4p0jRY1t9Gju/PsoVPy28UqiQRD2TdtHsBRZrc+o8R/DihkFCnjMFUcqcnytStdO11qkrp2fTTzvp57RRRQfkIUUGoRfRY/1kP/AH2KLopRb2JqKi+2xf8APSL/AL7FJ9uhP/LWPrjG4UuZB7OXYmpskywoWZlVR1JPFOWORrUz+XJ5AO3zdh2Z6Y3dM+1fXP8AwSm8c/CP4e3PxA1bxxeeD4/HsFikfg2LxMyJYfaPLnZvnk/dIzSCBdzlSAflIyaTloe1kOS/2jjoYOdRU1K75pbWSb07t2sl1bSPkG3vYrr/AFckbkdQrA4qUHNfpV+w1rMf/BYP4D/EXwf8WBpeoeNPDH2WbR/FEdhDFqGnC6WYINsYQskcsB3ISA6vtOCoYfmxe2VxpGp3VjeRiG+sJ5LW6iDh/KljYo65HBwwIyOtKMrnfn/DP1DDUMfQn7ShXT5ZNcrTi7SjKN3Zp7atPoxlFRyXUcT7WkRW9GYA0n22L/npF/32KrmR8tyS7EtFRfbY/wDnpF/32KT7dF/z0h/77FHMg5JdibFGKh+2x4/1kP8A32KdFcJMTskR9vXac4o5kHJIkxRijNFMkMUYozUb3CxY3sqkjox20FJN7EmKMVF9ti/56Rf99ij7bF/z0i/77FLmQ+SRLio5pkgTczKq+rHFJ9tj/wCekX/fYr7O/YB+MPwt+Bv7J3xI8QQ6n4QT49xx3M3h8a/HH/o0UcSGP7K0v7synEr7QQ7uETBGBUykke3w/kscxxXsKtVU4qLk2+0Ve0Vdc0nsldXPjK3uY7pd0bI49VOcVJiv0i+EXhu1/wCCtH7A3jHVPFlvplx8XPh/czrZeILOzjjvL8LAs8Mc4jUB1cb4ivOCisADxX5sW10l3AkkTLJG4yCpzkURlc6uIuG5ZbChiaU/aUa8eaErcr00aau7ST3SbW2pLijFRtdxxttaSNWHUFwMUn22L/npF/32KrmR81ySJcUYqL7bF/z0i/77FH22L/npF/32KOZBySJcUYqE3sX/AD0i/wC+xTop1mXcrKwHXBzijmQuWW5JijHNFFMkKKM1HLdRwttaSNT6FgKCoxb2JKKh+3Rf89If++xR9uiP/LSH/vsUuZB7OXYmJxUc1xHAAZJFjU8ZY4yab9thz/rIf++xX25+zT+0L8Nv2bf+Cd+v614F1zwra/H9pg9xPqcUX9oW0BuVjxZ+eNr7Ym3bI8sSzMQdowpSSPoOH8khmFacK1VU4whKbb1bUVe0Vdc0n0V111PiaKdZ03IysvqDmn1+i/xC+F2m/wDBRH/gl7/wui60/TbP4p+DY7x9Q1OwtEhOuJayETCcJgMWiAcMQSjA4wrEV+c0civGGVlZT0I6GiMrmnEnDc8qnSalz060FOErWvF91rZrqrv1HUVD9uhz/rYv++xS/bYv+ekX/fYp8yPm/Zy7EtFRfbYv+ekX/fYo+2xf89Iv++xRzIPZy7EtFQm+iH/LSL/vsU6K4WY/Kyt64bOKLoOWRJRnFFavgTwmvjvxhp+kSatpOgxX0ojk1HU5THaWSdTJIQC2AMnCgknAHJpl0aMqtRU4btpL5nd/skfsn+IP2v8A4qR+HtFb7Dp9qFuNX1WSIvDpduTjcR0aRsEImRuIPQBiP2Y+D3wh8P8AwL+HWm+FfC1j/Z+i6WmI1Y7pZnPLzSt/HK55ZvwGAAK8O/Zu+M37Of7LXwps/Cfhr4meE/s0Lefd3csrLcancEANPLhPvEAALkhFAUEgZPtfwv8Ajv4N+N0V9J4O8T6P4lTS2jW8NjIz/ZjIGKBsgfe2Nj/dPpWMnc/sPwz4XyvJaCSrU6mKqL3nGUW0t+WNm3Zbtrd67JHXUUUVJ+sCMNwr5L/4KVf8E9V/aY0VvGHhG3jj+IOlw7ZoAQo8RwKAFiOeBcIBhGOAw+Rv4Sv1oTngfe6AV5ZrX7bPwg0DWbvT7/4leFbO+0+d7W5t5J3DwSoxR0b5OqsCD9KD57ifL8rx+Blgs2lGMJ7OUlFprZxb6r/gNNNo/ESSOSCZ45I5IZI2KPHIhV42BIZWB5DAggg8g8Hmkr7Y/wCCnXhT4N/F6WX4ifD74heEJPFQAGsaRBOynW+QBcR5UD7QOjDpIuDwy/N8Tr0raLufxFxRkEsox0sLzxqR3jKLTTXR6N2fddH5BXqH7Kf7IXi79sDxs2leHYUtdOsih1TWLlD9k0xG6bsffkIBKxKdzYPIALDuP2FP+Cemvfthak2r3lxN4f8AAenXPkXeprGGmvZVAZre2U8b8FQ0jfLHu6Oflr9RbiPwZ+xn+z3fTWGlf2T4R8H2Ul29rYR75ZsdTlj88sjYBd25JyTgcTKR+gcAeGM8zh/aubfu8LFOXZzS1du0V1l12XVrnvhb8I/hp/wTy+B19NDc2+iaLbBJdZ1u/P8ApWqSgEJv28sxOQkKDAzwCck/nb+3Z/wUj139qyefw7ocdz4f+H8MnFmWAutXx0kuiONvcQqSq9y5wRD8Xdc+OH/BTnW5vFGm+EtW1Hwxo13JaafpWnyrJZ6Q5VGZMsU8yYo0e6UqCcjAUYUeRfF39mvx98ArHT7nxn4V1Lw5b6pJJFaPd7P9IdArOBtYngOpPbmlHzPQ444wx+KwH1PJMPOjl8VbmUGlNXtvayi30veT+J62XI6Jrd94Z1qz1LTby50/UtPlFxbXVvIY5reRejqw5BHqK/T/APYB/wCCoNn8fDZ+D/H0trpXjjCxWeoZEdrr7HPGAAsVxnHyD5ZCfl2n5a/LcNkU2SNZV2sqsrcEEZzVyjc/P+D+Ncfw9ivbYZ80H8UHtJfo10a281dP9y9O/Zf8I6D+0Jd/Emw02Ow8SalYzWOprHGvkaiZHR/OkjYECcFCC643BjuyRmvkb/guzpVvB4S+F08Vvbwt9t1RCY4lTOY7Y84HPSsr/gn/AP8ABV6SwfT/AAP8WtQkmt2Kwad4pupvmg4CrFek8snpcE5HRww+cfU/7cf7Hdj+2l8J7LSG1iPQtV0e4bUdI1Iw/abcM8YVkkVWGYpFCfOpJXCsAwBU57M/pLGV8BxTwriqeQRXtKlm4aRanzRk0+l5KOj2k9b3ufi6KK6f4y/BzxD8AfiRqPhPxRY/2frWluBLGG3o6sNySI38SMpBB4ODyAeK5jOK2P5BxGHq0KsqNaLjKLaaas01umu6CiiigxCiiigAooooAKKKKAPdf2e/2D9b/aE+HsPiSz1/S9PtJp5YDE9rNNKhQ4JO0Beeo5r0mP8A4JI3j2/mSeP/AC+cEr4WlkXPpu+0rz+Fd1+wXqVxYfslxyWsM15eW89/Jb2iy+V9pccrGGIIXcwC7iCFznBxisS8+LHjuX9pfTbyT4d6lHqC+HZol0T/AISpNs8RkJNyZvs+wbTxtMZJ67h0rkq1lG17/if0lgeFeFMLlOBxWOw05zrqne3tnrNK79xNbvSO76Hl3x5/4J5z/BL4a3Gvx+NLfWprdwPsb6M1gZECszFXaZ8soXO3HPrVj9lXxV4z1j4U3Fto/h/WtT0vw7KyyXlravJBGHy5UuBt3D0znBB9cey/tf65qmp/s527XVvNY32reUsmny3An+yyyQsDCXwocozFd+0Z64HSp/ht+0R4k/Y0ttE8N6XY/bNHsbdVXy28td5ALl/QsxJJ5zmu3D826Prst4HwuWZ/DG5TelQ9l7y1fM5PRe8+aNklJ9mkmldnkuv/ALTH/CH7nvXlWVASIB/rX9gD0z6ngV84/EDxrefEXxlfa5fDbPqD78AkqijhVBPUKAB+FfaHxH+JfwC/ag8Q3lv4y8Dz+A/EFxLtm1zSLny3ikYDDyJtMbDGDygBHfHNeK/F/wD4Jg+K/hJ4euvG2j/ETS/it4Zu7iRYX0K1FtZ6VBvAi89Xnk2uVOTIrbFIKknIaqrSsuaeiR8/4g8M8Q54pxocsqFJc0IwvKdSW1nGyfNa9kk1fRXbbXgNbng/4X+J/iHp+q3fh/w7rmuWuhw/adRmsLKS4jsIjkh5SikICFY89lJ7Guft7iO5gjkhkjmhlQPHLG4dJVIyGVhwykYII4IOa+rv2B/+CpupfsJfC7xF4YtfBVj4oXWL46pa3EuqNZNa3BiSIrIBDJ5ke2NGABQghhkh8rjJ6H8+5DgsBWxyoZtVdGnZ3kouTTSdlZa6v+lufKsU7ROkkbtG6EOjo2GUgggg9iODkV9BH/gq58drKVbmXxtZt5Z3sZ9EsWRz1+b90M57+tfPapgc7eeTtUIM+wHAHsOlfrL+x5beF7T/AIJP3HxTs/BPw8HjrwfoGqy22rN4btTcLcWQlEUkj7NzvhF3MTliSSeamT01PpuAcvx+NxNbD4DFyocsHN8t/eUX2TWqvpc679uP4yeLvDf/AATU8P8AxU0O7s/h/wCMGstL1G9tl0+B0la6jQSW2yVGIIaTco4YbcHuKg/4KR/GDX/hf/wTg8M+KNDnsLPXPEC6Zbajcf2dbyfao7m1YzqVZCoDn0Ax2xXwb4w/4Kl+L/jFa3dv8U/C/hP4nacrxz6dp+oCaxtdLlVJFeRUt2UyM4deZCdmw7du9s/an/BXy+j1P/glv4LuobOHT4bi70SWO0hYtHaq1sxESk8lVBwCecCp1P2KPFdLNcvzHGYCtK0KEVZ3UlJcyc7fDFy0+FvbWz0XPf8ABFf9p/xd+0trfj7wb4yn0rVvCul6Hp8dhpI0yCGzs4wXgeNYkUKUkXBZWBBIyMZILP8AgmZ4o1Twf+3h8T/gvBefaPhz4L/tWTRtLubeKY2eL6IKBKy+YQFkYAMxr0r/AII2+NPg94s+CNlD4L0XR9B+IlhpkFr4pgBzqN60eFN0Wb5pIZH+f5fljZ9nGBnD/Ys+PMPjb/gqD8YPCyeB/h/pEmjDUs63pelmDVb7y7yFCJ5t5D7ict8oyyig7snpyhgsnq1cWqk3NrmTk+eMk5ezb0dlZXUtE42toeh/sr/GbxN4v/4KK/tAeDL7UY38L+D7fTDpNjHaQxC1MqsXO9EDNnH8ROO1fmV/wVF+JmueP/20PGmn6tdQ3Ft4T1KfS9LVLWKE29tv3iMlFBfDMxBck/Meea/Q79i//lLJ+1J/17aN/wCgPX5pf8FFP+T6/ip/2MNx/SiGv9eZ8n4lY3ES4bgpTbviKyer1UZzsvRWVl0sdz+wF8Sv2gviJrdl8LfhP4gtbOxtxNqD/bbK2kttMhLr5kryvE7hN7rgAMSzYCmvtT4fftTeHf2Ovi54h0P41ftBaD8Qb2GBLb+zLLwULb+yLkNuYtJDvDMVIUp1HfB4ry//AIN3fFOkWfiX4qaLJJDFr19Dpl9CrcSXFpC1wkm09xHJMmR285fw8o/Yx/Ye8O/tM/tzfFvwT8UNQ1iz1bQZL28W2tZ1tri9me7YNNl1JKhXRwMc+YD04I9xcMYjM8LlOX18vm61avKUUqlSfs6ahdKPKpJXaV1e+2i2Oz8D/tM/H79tj9rbxl4f+C/irT7fwha3011a3t9o9rFa6Xp5cpC0hMLSZbB2rgu2GOMKxHsvwG8feMvC/wC3n4Z+Het/Hfwv8VF8/ULTXtEtPCcemyWMkVpJIhLhXR9rLhgsgKttGGBbbsf8Em/hLo/7K37RXx9+FcOu2Wt6not7pV7DchUjuLmyaGQhXUMfmheTbIB8qtMpwvmhR8yf8E6vAPiDwv8A8FkNSs9S07VGvNH1PXZNSkNo4WFZFuNk0nGESQupVicNvXBORU+aPawdXMMIsBVxU5zrYjEOM/3k+SnyTs4RipcrTs/i5lZO3Rn1/cfGjxHH/wAFgY/hmt1Z/wDCDt4XN+dL/s+32+f5Jbfv2b/vc43Yr4g/4LR/FjXNd/a51nwXcT2jeG/C7W9xpdtHZQxtavNaxmXEiqHIY8kEkcD0r60u/wDlPxF/2JR/9JzXxR/wWM/5SGeOP+uVj/6Sx1pHc87xEx2I/wBX8WnN/wC9yhu/hUbqP+FPVLa58xUxp1WQKWUMccZ9en8qeelfan7LH/BQP4V/Bv8A4J7+Lfhrr3gmbUvFGqJdxmMWySWuuvMGEM00pIaMw5UdCyiNShycCmfheQ5ZhcdWnTxeIVGKjKSbTd2to6Nav/hk3ZHxX1FekfAz9r/4i/s2aLqGm+C/EC6TY6pcLdXELWNvch5QuzePNRipKgA4xnA9K81iUpEqlixUAFj/ABe9fU3/AAR40jQvFv7auneHvEmg+GPEOk69pl3C9trWmw3yB4081DGsoIV8pjIGSuRRLY24Vp4irmtHD4Ws6M6klFTV7rm06NOz2ep9Vf8ABGj9szx5+1N8RvGXhvx1FpviTT9I02LUItU/suCFrCYzCMW7mNAreapdlDYYfZ3xkE7fTP2CfiN4o8Y/tkfHzwL4p16HxZovgO6tYdIM+nWsbwLJNcZDNHGu5gqohJ7pnANeE/8ABR/9ufxz+w/+0O3w7+F0fhbwj4Vt9OstRW0sNDt4tsjsxf7oAAbZg4AOCcEHmvQP+CK/xz0H4zfED4lNpPw38NeB7qC1sJ7q6028u7ifUjJNcn9807tuIYM24YJLnOe2ep/R3DudxjmuF4eqYqdSvQnUU3LmXP7svdVrqSi7O82mrK29lzXwTn/aE/a8/aP+MGl+G/ilovg3wv4D8U3mlRtP4dtL2QD7RMIYUjCKSFjjGXZx24Y5x03wT/4KZ/CP4LeHNc8P/ED4wxfErxNb6ncJFqEPglrBYQirH5CoqsH2yI7bsjO/HQVn/wDBNv4LR237U37SHxYl1TVy3h/xfrOmQaPZXLQxXpWeed3nVTiX7wWNWXCtuYZJG3lP+CaP7U/jb9uL4oeOF+Kel+Cdc+GUOlm+1dr/AEm0gtNHnlctCqs2GIcLLkuWP7rcWB5Mq/czy/H4jDrDKNSSxGInWUXNzqwcYydm4qpCMEl/dk0lfQ9Y/wCCJf7S3jn9pH4eeOrnxxrS6zdaVqVnBbutpBbeWrwsW4iRQckDk5r8w/2ufibrXxW/aN8XXmvXUF5dafq15pULx2sVvi3t7qWOFCI1UHagCgkZwoyTiv02/wCCNWn+FdJ8XfH618C3H2zwbB4xgTRpt5kWS2CPs2seWXsrHkqFPOc1+Vvx4/5L149/7GfVf/S2aqjvb+uh8Hx5isU+E8ujXqucnOqpPmbUnGTSd3v5PtsfUH/BPbxr+0p+0ZNb+Bfh14ssdJ8N+EbWNLm+1DS7aa30qBmby0LGJpJJGw+xM5IQ5KgZH1X8Gf24fh7+zRrnibw/8WPjxoXxO1qK+SGGSx8HG1h0wx7klh3Qh1kJfkkH5duOa5r/AIIJ65Za1+zP8SvDuj3EWneLodVNw9w3zFFmtRHbS7O6rJHJ9SDXzn/wSv8A+Ce3gr9rlPiRofju/wDEGjeKfBzWtjHplpKlvPp5bzVklcOreYyyxeWUIAXac58xdo/iPocgxGa4XAZa8tl7aviFUd6tSfs4qCtyKKkle3dPVaeXefss/Fz9pj9u34t+JrfwJ4303R/COl37s+q6hoVp5NpBJI5gjVBDukl8tc7MjAHzMuQT7v8AsPfFLxB4g/bF1DwZqnxm8M/GbSbPQr+S8Sx8NRaaNMvILi2jGflIkVhJIFeORlzG4IHyltf/AII8eGtP+HPwh+Lfw40/WLLVNe8HeMr6xur62UJ9pVolit7jaGJVWMLgDccFGAJ25r5b/wCCBng7VvDf7YPiq3vdJ1Gzk0Lw5Pp+oiS2dVsbkXEK+TK2MJITHJhWIJ2MRnBo6npZTVxuFnlSq1J1KmJnLnk6k3GPI9YKKlytbp8yls2vL7E+BPxa17xd/wAFRPi/8P8AUbizuvCPhnQ7S703Tzp8Crayv9n3MHCbzne/DEjn2r83/wDgqt8Uda8c/tleLtF1S4trjT/B2pT6fpCJZwwva27eW/lF0UM6hskBy20s2Mbmz9+fsx/8pqvj5/2LVj/7a1+cf/BSn/k/T4qf9h6T/wBAjojueF4jY3ES4ctKbd8TVi9XrFSlZPyVlZbKyseI0yWdYfvMq9cZOOnX8qe3SvsT/glV+3T8Nf2ONG8d2vjrw7qF5eeIFhNvf2VpHdyXUCI4eykV2UIhZtwIyGLtuxtXOsrn4nw/luGx2NjhsZXVCDvebV0rK60ut3puj47zkV6p8Gv23vih+z/4LPh/wl4mXTNH897kW76fbXAWR8biGkjZgDgcZxmvOfFerWviDxVqmoWOmx6LY315NcW2nRymRLCJ3LJCr4G4IpC5wM7egr7E/wCCHfhvw18QP2jPFPhnxR4X8K+JrHUNB+3RJrOkw3z281vMFHkmRWEe5bh9+B8+1P7gym9Lnp8I4XE184p4LA4h0ZTbipxuvNbNOzaR9Uf8Ebv2nvF37YPw78d2/jhNP1J/D11bQ2Wtw6dBBLJ56S74jtTy2eLYjA7ekoDds9R/wSi+JXib4zaZ8VrPxzrmneNLjwb4wl0Kxvxp9vCksMSBd6+Wigq5BfJz1r5V/bH/AOClHxF/Zx/ah8WfDnwvH4b03wD4V1NLaHRrLTEsVmtzBG0kBliKugfe3zR7WHBBGK+mf+CJnxP0P4pfCXxtd6H4A8NfD+C11yK3ktdGluJI7phaoRI5mdjuAIXjAwPWs/M/ovhXPqdbNMNkrxMqlXD+1jUcuZe0forxajaycmnpdLVnjH7E8X7Sn7ZngXXPFv8AwuLQ/B/hvSr6axjml8L2d/NcvEN0n7tVTy1RWXBJJYk4GBk72m/8FVPhj4O/ZU8RaMnxVXx58Tv7NvzpGtr4N+wCS5ZX+zfuyrRgK20bmOCOTWP/AMEpvBEn7Pn7GvxV+OlvqOralqCxakbXRI5nFgps1YiSWFWxJKz/AMRAZEXAI3Em5/wTe+L2pftjfDb4l337QGm+CdW+GOmrCsGranptpp9vZXLF2niilUIVCxsj7gQ0ZKENlqk8vKcZi44bB0KdWUcTiKc23UdSrHlWqk7VIxhto1CTWl2tb+pf8EoP2kfGf7QH7HvxE1/xbq0eqato+sXltaS/Y4YRFGun28wUpGqq3zyMckHOcHivyH+IPxL1r4z+LLrxN4iuor3WNWCSXc6W8duJ2CKu4rGqqDgc4Az1r9S/+CLUNnbfsVfGaPTpJJdNi8U6mlnJIMPJANOthGzcDkptJ4HPYV+TFjxZw/8AXJP5VcD4PxEx2JqZBlKrVHLmjNt3vzNNJNvro3Z9n5n3r+wN4p/ag/a9sJrPw54/0zw54N8Ixw2FzrOp6PaTpbhYxshQeVumkWMKWyygKVLMNwz9C/Bb/go78Jfgx4F1PR/Hvxm0n4neJIbmeSHVrHwe1pDs2DZEgRWWTDBiHBw24duTgf8ABNOQfEb/AII+eP8Awv4bzdeJoLfXbOaztzuuWnnid4QFHzfvEZQvrggZIIrwb/gmX/wTW+H/AO3P+zZ4s1fUPFGraf4us786faraeW0Wj4iDxSSwnmZXLHKlk4jKqynLUutz7DLK2c4WhgKeVSdetiKUpuVWpNxVkvcjHmUbx21u766Lb6//AOCfvxk8T/F7/gmdfeOPFN5Z6p4ytbPWGGoNp9ujLJAr+WdqIE+Vl9Occ5rx7/gjF+1v8QP2ovjb4p0rx5rVr4g0/T/DqXlvC+mWsPlzfaI0L5jjUnKsRg8V6N/wTZsf7L/4JMeJLXzra6+yx+JYfOt33wzbTMu9GxypxkHuCKxP+CEfiH4V6n8EBa6LZ6XZ/Faxhki15nGNQvbVpd8ciMfvwjCAhM7GUbgNylpXQ+hy+tjMRjMmU8Q43pOc05P941y6f3pa316Jmr8EP2hvF3i7/gr549+GuoahZ3HgfRbO5ltNMOm26rEyxWpU7wm/gu55Jzur5H/4LXfEzWdZ/a91HwfcXFu3h7wusFzpVqlpFGbNp7eMzYdVDkOVUkMSMqvpX1z8Afj1a+Kf+CwnxB8I2/g34f2/9m214W8RWWnldZuWRLYMktxvIb5iVYbR9wDjBr4o/wCCzn/KQvxd/wBelj/6TpWkd7HyXHWMqvhaty13O+KnFvX4VzPk13jFpWW2isfLdNKA/wD6qdRWh/OY0Jj/APVX6If8EHl/4kfxX/6+9I/9F3tfnjX6If8ABB7/AJAfxY/6/NI/9F3tRU2P03wfb/1swvpU/wDTcz9AKKKKyP7TEj/4+k+or8Iv2k1z+0f8RP8AsaNU7f8AT5LX7uxf8fSfUV+En7SP/Jx3xE/7GjVP/SyWrj8R+A+Pv+44T/HL8kcTt4+tOoorU/l256d+y3+1z4u/ZB8aT6x4amhuLO+AXUtKuyxs9SVQdu8KQVdcna64YZIyQSK/TL9pTxrefET/AIJfaz4j1BbddQ1/wdbahcrAhSJZJVjdggJJCgk4BJPvX4+zf6pvoa/Wz4tjb/wSGUf9SDYf+i4azkrbH7v4U5pi6mXZjgqtRulCjNxi3om0727f8P3Ob/4IjQqv7J2vMOsni66yPpaWn+Ncn/wXbbHgD4Wr3Op6of8AyDaf412H/BERs/skax6r4uvM/wDgJZVxf/Bd048F/Csf3tQ1b/0VY/41PU+zxi/41h/3Dj/6cR4f8Gv+CbUfxa/Yf1z4xHxTqtnNpNjqd4mlx6KJoZTaB8Az+aCA5Xk7Dt564pv/AAUC/wCCbkX7Dvgzw5q0PirU/EjeINSk0/yrjRRY+SUiMm4ESybs4xjA71e+CPj79n/Tv2ENb0vxVb6fJ8VGsdTSzaSG8abzWD/ZcMp8odVxnjjmtn9s74t/B261r4czfs+yabpXia28RfaJ76KG8hW34RIN/n5+Uu7Z2jOFPTis8RUlTpSnDdJuzdlt1etl3aTt2Z+e1Mp4flknMo0lWdOjrGrKVTmlJqbVNpJytvC6SVtdT42NrMf+WM3/AH6b/CvePgL/AMFGvil+zf8ACy/8J6NeQ3GmPEE0yXUbYzyaAxOS1sW+XBGcI4ZAeQByD9b+I/2gNd8Q/E749aPofizxxax+G5NE05muJY0/s29uNVMFw9iApMcLQlQgk3NkN1GK8I/4KP8AxxmsvDVn8K4fiFq/jS60HX9Rl1p7lSGVBLH9jt5nKKHliw+WQBc+vFflvD/iNj8zx9HBPBKPPZtqpOThB0oVeZ/uVFK04xScleTVnbU6sZwfDhvDVszwOYzjKCcVaEY88vaSp8i/fNvWEpXUXypX3R8m6pd6hr2pXF/fSahfX19I09xdXG+Wa4kblnd2yWY5ySTk16Z8JP2Ur74w+HNJ1LT/ABF4bs4bi+ltdZS/vEtJPD0S4K3MqyMpkjdSxBjBwV2nk19S/Dz9oLx5pl/8MdMt/FGtR6XJ8Gr7VZYUIEZuIItQWKc/LwyeXAAc/wAKZyTz0kPxt8c6N8GYY9I8ULrXjC78Bx+INQttZ8UyxazaAxfaJZ4LZLV7eM+WmY2aUHD4IywB8fN/EfOnbD4LD04T5vi9o5JR5qkG5KVKKSjyczfNo+VPmTlF9GU+HWT64nHV51I2u17OzcrU5pRaqtty5uWzWq5tnZn52arpn2LVbqC1kfULeGZ44bmOCRFukDELIqsNwDAAgHnB5p3hmxs9R12zXUpdQtdJkmVbu5s7P7VNBHn5mSPcoZgOilhn1r9DvGHx68U+HPiD8Q9F0HxB8QFtfDfwzuNdludT1QXqR3c8VvLbvGfKXyzGGkUMSfMOflG2vNf+CefxHj+DHwFbxtrnxE1fwz4X8P8AjH7JcaQqGSz1VJLMu0axohfz2baQSwUCM55r06fiNjqmT1cb9V95KmoRjUcpz9pFuNuWjK0n7riuVr3ve5bWPKl4d5fTzelhFinyt1HKTgoxj7OSUvirK8Vrd8yemnNe6+Z/j18E5vgt46j0+2vG17RtSsoNV0fVIrZo11KzmTdHJs52MPmVkJJVlIriTazAc29wAOpMTDH6V+iWr/GXxZpNpoN5P8TfEHijR/D/AMOtT8W30ug3ZsU8STQ6k8SK0skJeJVV1DOEJAiPB76Hxq+OnibW/wBlvxjeR61rEMN18J/DWqRxS3QmeOS9vLmO4Zn2Lvd4gqM20ZAzhc8edl/idmkIUMPWw0ajclTc/aON5e0VK7g6KabupNWSs7aO8V6ON8McqqyrYihiZ00ouah7NSsuR1LKSqtNKzindvS+q95/m7HBLMm5IZnUnAKxswP5Cl+y3H/Pvc/9+W/wr6o/YI+MXir4V6n4HbxB4r8ZaD8M9T17+zNMh0v7NLa3V/50RlguEf8AeJCyyfMyc5PHQ17n4c+PHxKsviD8F9L1TxNqv2rVviJ4j0XWoIZvNiuoba6jRIS20Fo4xvAOF4ySPT3M+8RMbl2NqYWGFpzjFSal7aSuoQqzkv4LXMlScZJN8spRTfU8TIvDvAZhgoYqpiakJScU4+yi7OUqcU/4qdn7ROLaXNFNpdD85Pstx/z73P8A35b/AApGtplHNvcLxnJiYY/Sv0Stvj9feDvAug3/AIq8feMpr7xJpfiLxDFM3iqLS4i+n3XlwabHGYGDGcEKHDbwRwrdBe8R/EfXPi1qXj9tR8V+KNP0mTwv4cstI0CPWRb2s19q9g5CT3RhLHdIoTcUVS0uSFyMeVU8VsdSm/a4FKCbXN7VtPlqxovlXsrv35fa5VZXbSaPVj4T4GpBeyxsnNpNRdNJpOm6icn7Sy91X0u3eyTdzz39gv8AaE+HXw2+AEekeKvFFr4f1q3v7h2trnR725YxuQVYNHGyhTgjGeoNerWfxw+BPif4gwa9aeONHvvEFrYPZxyW+g6lIy2zMGfKLHjG4jLY4yOaw/jD8eda0T4x/HaDTfEHiK28QeG/h7E2q7tiWMN7HNAY3sUIaSOEC4uMeazEmZuTjj8+fif8H/E37PHjfwi3iK48T+GZfE2mltP0S0vX8nW7VgdsksEJJkQhmA3DjDdt1fV8M8WVs2wVTG1sPycsacklJzb54KezhGyXMrPW6102P2rgXD4jFZ/l3BzqU3RvKDrTpuMafsnKMZSl7SUVJ8jWvLaXuptu59j/ALTX7Vnwg+JV74f8K6L8QdDvPEEWuW01zpc1jdaPdRRKc5KXiRls9Nq5bjOMZr1XW/DUPiS0LLGrT43DKfNz0Iz61+c/iL4TeF/jcjN4o0WHULqxt001ZpnmS8to4htSLJYHEYG0IwyvTHNP+DvxI8bfsTa1DJ4W1bWPF3giOTF74X1S5MktlGf+WtjM2SpXJJiI2tjqCcj2Mt4vwNafsJ3hL+9a33rb52P6k4o+j3xTllOeJoRjiYxV2qbblbvytJv0jzM6P9qPQdV0f4ya1/atnNbNI6vDJINyzR7QqyK3Qg7TgdeCOorJ+IH7V+ofCg+IPEHhG2vtAXxMsWneIJX1GS5jhsmBRpI7VEVGkG45c/Njp97A+vtS1DwT/wAFAvgK15ZzH7RYxyS6XqcIw9ncbfmjPTKMyhZEb0BwCAa8Buf+Canxo1KxkjvPhzrEljNEwnBlgZTEVyx4c/w5PFfU1IpxcJbS3+Z/CWKw/EHCPEKzXJVKtTrVFVko0+Zpxm5cr0lb4nZqyet1oee6p8MGt9AttS0Ro73R5oFuolhU7ooXJZWXruQ8/MOnQgVzA+lUf2H9a1S8/Z08VLfSXF1omj3lm2gzxztHcaTeTCV5vKkwcwsqoXhYYZmVgVO7d7b4O/Y3+J3x18M23iPwv4LvNUs7tSXltJIFjcqxUuEMm5FJ7MBz0JBFfOZdjqtPFyy2u+ZwWkkullZSWydn00Z+jeOXgNTq5FHxK4ajNU8RWcatCT55KpJybnRldznByTvGSc4780opuPkJGRX0h+w9/wAFJ/EH7G3h/VvC95oNh448B687yXWi3s5hMLSLslMUm1xtkXAaNkKsRn5SWJ5z/h2/8cj/AM051n/v7B/8XS/8O3vjl/0TjWv+/sH/AMXX0DaZ/LmUZVxPlmJWLwOGqwmuvs5PR7ppxaafZpox/jf8U/hJ4r8KXVj4D+EN74Pv7uZZBqN94qn1JrVFP+qhh2KoVgSCXZiBjFexftcf8FQrT9qD9krw/wDDCHwTdaJJob2Df2i+qrcLL9lhMZHliJSN2c/e49681/4du/HL/onGtf8Af2D/AOLo/wCHb3xy/wCica1/39g/+LqfdPUj/rZCFanSwk4xrRUZqNDlTSv0UFZ6vVa+ex4vpGqXfh/VIb7T7q5sL62O6G5tpWhmiOCMq6kMODjg17h/wT9/bRh/Yg+NOreMLvw7deKW1TSpdNMEd+LVlZ5o5DKzsj7v9WRjHVs57VX/AOHb3xy/6JxrX/f2D/4upYf+CaPx0nTd/wAK91Jeej3Nup/WSm3E8vK8o4mwGIhisLhasZwd4/u5NJ97OLX4HrvwV/4K3Wfwj/a2+KnxPbwFeX8fxKisUTTV1hI20426kEmTySJN2c8KuMd6+Y/2jvi8vx++PfizxrHp76SvifUZL8WbTiY2+7Hy7wFDY9cCvQv+HZXx0/6J9qH/AIF23/xyj/h2V8dB/wA0+v8A/wAC7b/45Sjyo9LNIcXZhhlg8Vh6soKcp29k170m23dRT1bem3Y8i+HvxF174TeMbHxD4Z1a80TW9NfzLa8tZNkkRwQfYggkEHIIJBBr6m+GH/BZT4nab8aPDGveKf8AhF72ztruGDW7u18PW8WpX1huAlTzgNwwuXVU2gsoHQmvNv8Ah2X8dP8Aon2of+Bdt/8AHKP+HZXx0P8AzT+//wDAu2/+OU3yswyjDcX5Y0sFRrxV1KyhOza6tWt5emhxfir4pJ8MP2lde8UfCfxJ4k0+yj1Ke40bU5naLUPJlJYrLuJLdSp353gAsMkivcfhH/wWL+LHhz4zaD4h8ba3qXjDQdGjmWXRLaSHTY71niZEaRkiIcqzBsMCMjjB5rgv+HZXx0H/ADT7UP8AwLtv/jlB/wCCZXx0P/NPtQ/8C7b/AOOUvdNsFDi/BV3WwVCtTTlz8sYT5L3v8LTTS7O+h6VP/wAFU7SX/goHH8ch4Gu/IXRP7HOjf2svmEmMp5nneVjHOcbPxrxH9s79o+L9rP8AaL1zx5Bo03h+PWEt0FjJdC5aIxQpGTvCqDnbnpxnHNdJ/wAOyvjp/wBE+1D/AMC7b/45Tl/4Jj/HZlYr8PdQIUZYi7t+Pr+8oVi8y/1vx+GnhMVh6jhOo6jXsmvfas3dRT+W3keD0V7t/wAOy/jp/wBE+1D/AMC7b/45R/w7L+On/RPtQ/8AAu2/+OVXMj5n/VHPP+gOr/4Ln/keE1t/DT4k638HfiHovirw3fNp2uaDdLd2dwFDbHHZlPDKwJVlPDKxHevW/wDh2X8dP+ifah/4F23/AMco/wCHZfx0/wCifah/4F23/wAco5kaUeF8/o1I1aWEqqUXdNQndNbNaHovx4/4KO/Df9rb+ydV+J3wLbUvFmk2xtTqGi+LJdNju0zuCsvkswUNnAJcoGOGOayP2EP+CjWk/sUfEn4ga5a/D03Wn+M/s4tNKs9XMUWkJFJM/liSVHeQfvQATg/L7jHI/wDDsr46f9E+1D/wLtv/AI5R/wAOy/jp/wBE+1D/AMC7b/45U6H1v1zjL67DMVhZqtG7U1QSk24uLcmoe9o+t++5ufAT/gph4q/Zp/aP8e+NvDWl2t1o3xB1i41XUdA1CYlG3zyyxYmRcrJGJSu8KQw6qcDGb49/aN+BGvTapqWj/s7fYNY1RJdsVz4xnfTLCSQNmSO3jiXO1mBVMqo2jAFVf+HZXx0z/wAk+1D/AMC7b/45R/w7K+On/RPb/wD8C7b/AOOUe6c3Nxe6H1ephpzheUkpUOblcneXLzQfLd62VtTtP+Ccn/BSu1/YK8J+KNNufB134qfxFe292JYtTWz8nyoymCDG+c5znivmvx94mHjbx/4g1xYWtl1zVbvUhCX3mETzvKELYGSu/GcDOM4HSvYP+HZXx0/6J9qH/gXbf/HKP+HZfx0/6J9qH/gXbf8AxyhcqdzhxmB4pxWBo5bWw1V0qN3FeyatzavVRu/mzzL4NfGzxZ+z348g8TeDNcvNB1q3QxC4gwwliYgtFIjAq6EqpKsCMqD1Ar6l+B3/AAV28U3/AMbLab4kroMfhHXrefTvE1xouiJZ6jeQyQuiyGaIecWRipGxlx17DHk//Dsv46f9E+1D/wAC7b/45R/w7K+OhH/JP9Q/8C7f/wCOU9DbJ6PF+WOKwtGsoRkpcvJPlb81a1mtH3Wh538P/jFrn7O3xOvNa+GfifXNH8mSW2s77YILi7tN/wAgniyyHICsUO4BuR2NfSf7P3/BaD4keAfi5N4j+IF1qnj7Tv7NlsbfSbe5h0u3hld4W89lSIq7BYioJGRvOCMkHzKD/gmL8d7mVY4/h5qUjt0Vbq3JP4eZTrj/AIJg/Hi1fbL8O9SjbGcNc26nH/fz2pe6VlkOMMumpYKjXhFPmUVCbjf/AAtNP5pnqXwy/wCCs9n8O/22/iB8YD4EvLuHxxpVvpy6V/ayI9mYvKy5m8oh8+X0CDGRycV81/tM/GNP2iPj/wCLPHEenvpMfia/a9Fm8wma3yqjaXCqG+71wOtd9/w7K+On/RPtQ/8AAu2/+OUf8Oy/jp/0T7UP/Au2/wDjlC5UGZ0+Lsww31TFYerKHPKpb2TXvSu27qKet3pseE0V7t/w7L+On/RPtQ/8C7b/AOOUf8Oy/jp/0T7UP/Au2/8AjlVzI+d/1Rzz/oDq/wDguf8AkeE12X7P/wAfPEn7MnxZ0vxp4UuYbfV9KY4SZPMguo2GHhlXILI65BwQR1BBANeif8Oy/jp/0T/UP/Au2/8AjlH/AA7L+On/AET7UP8AwLtv/jlHMjfC8N8Q4etGvQwtWMotNNQndNbNaHffHj/goD8Jf2lvGEPi7xh+z/LeeMFgjiuZrTxlLaWd+YxhPNVYNxUDA4+baAN3Gak/4J5/8FQrH9hPwh4q0k+A7jXo/EerjU4fI1YWy2SCJYxD88bs23aMMTkjryMnzxv+CZnx0QZ/4V9qHyjOBdW5P/oyq/8Aw7d+OX/RONa/7+wf/F1Fon1dPHcZ08csyp4Wcayv7yw6TfMrNytDVvzv33Zu/sVf8FJfFP7G9trWjx6Ppvi7wX4hmknvNB1CUxqruu12jkCsF3phXVkZWCjgHJrB+Jfxv+CuraBrUHhH4F3Giapq8LxwXuo+L7i8i0ZicrJbwLGoZl5A3tgDjBHFL/w7d+OX/RONa/7+wf8AxdH/AA7d+OX/AETfWf8Av7B/8XTtE4VHi36pHBTwtSUIpqPNQ5nFS3UXKDcfk9Oh6L+wr/wU4s/2M/gD4q8EzeC7rxFJ4m1G4vvtcWqJarbiS0htwmwxOWI8osTkfeAxxz8k20HkQRqTu2Iq59cDFe8af/wTP+OmqXPlL8PtQhO0tunu7eJPzMmPwrQ/4dXfHj/oR1/8Gtp/8co91anNjss4qx+FoYSvharhRTUF7Jqydr6qKb2W9zzb9n39pnx1+yz4ul1vwL4gudDvbiMQ3KBVlt7tASVWWJwUfaSSCRkZOCMmvov4V/8ABUa++Ier+MPD/wAWnstJ8D+OtCvrDU5fCmiR2l99slUeXdkx4eWQEMuHYoRI24EE157/AMOrvjx/0I6/+DW0/wDjlJ/w6t+PH/Qjr/4NbT/45ReJ15Th+MsvUadChW9mm/ccJuDvumuzW9rXO/8A2WP+Cqlp+zX+xzffCmbwPc63LdR6lENUj1RbZALreAfJMbEbQw43c4618gaLfXWgXdrdWV1cWd7ZkPDc28rRTRMBjcrqQyn3Br6A/wCHVvx4/wChHX/wa2n/AMcpf+HV3x4/6Edf/Braf/HKFyoxzLL+LcdSoUcRharjRXLC1Nqy06qKb23d2Yv7B37XyfsY/H+TxzeaJdeKGl065sXt1vRbyO8zRsZGkZXz9w54yS3X1yf21f2lof2uv2jNY8eQ6LL4fj1WG3i+xSXQuWj8qJUzvCqDnBOMccda7D/h1b8eP+hHX/wa2n/xyk/4dW/Hgf8AMjr/AODW0/8AjlF43uTPK+LJZasplhavsVLnt7N/Fa178t+u17Hz5RX0J/w6u+PH/Qjr/wCDW0/+OUf8Orvjx/0I6/8Ag1tP/jlVzo8X/UzP/wDoCq/+C5f5Hz3X6If8EHv+QH8WP+vzSP8A0Xe185/8Orvjx/0I6/8Ag1tP/jlfZX/BJf8AZf8AHX7MmmfEKHxton9iya5cabJYj7VFP54iS6En+rY4wZE64zu9qick1ofoPhZw1m2D4mw+IxWGqQgue8pQkkr05JXbVtW7ep9hUUUVB/XIkX/H0n1FfhJ+0j/ycf8AET/saNU/9LJa/duP/j6T6ivwk/aSGP2kPiIPTxTqg/8AJyWqj8R+AeP3+44T/HL8kcXRRRWx/Lwyb7p+hr9bfi9+7/4JDrnt4BsB1/6Zw1+fn/BPDwvpfjT9tPwHpetabp+saXeXNws9nfW63FvOBazMA6MCrAEA8jqBX7IXfgjR9S8Jf8I/caPpdxoP2dbX+zJLVGszCuAsRiI27BgYXGOBWcz+iPBjh+eJy/HYlTSVWMqSWuj5U7vy9710Px6/Zg/4KGePP2SvAd14b8L2/hq40u81CTUpBqFi00nnPHHG2GV1wu2JeMdc81S/av8A26PGH7Y2meH7XxTZeHbVfDctzNatpttJCzmdYVffudgQPJTGAOp68Y/Wn/hk74Wf9Ex+Hv8A4T1p/wDEUp/ZP+Fh/wCaYfDz/wAJ20/+IqeZdj2peFfEksB/ZcszXsLJclpWsndfjqfhfkUkiLIjKcFWGCD3Ffuh/wAMnfCz/omPw9/8J60/+Ir5u/b5+KXwv/Yj8X/DfR4/gh8CtU/4WFFrkzal4p1W08KaZpA0yC1m2vP9guQzTfaNighMMqjnf8te0Pn14A49arGQ/wDAZHw1qv7c/wAU/Enw71PwvrHi6+1rS9ShhhLXxEtxbeVIJEkil4ZZNyrlySflFYvxs/aa8VftBW1iniRtFkls3aaS5tNLgtbm+lKhTJPJGoMjbRjJ49s819nfCP8Abk/Z38d+IvDlv4l+BHhvwLpfizwl4b8S6de3Ph2K/ZJtauZLaG0uFitAkMYkSMJcNIFm85cIvNfVnjb4B/CD4fQ2b6h8K/B7/btSg0qMWfg+K8ZJpn2IXEUTFIwR80jYRBySBXh4Xh3KcNVVbDYWnCSbacYxVnJKMmrJbqKT72XY9zE+E/EmJpOjiM054tJNS53dJtpO7eibbXa77n5I/Dv9sT4jfC/wTL4Z0zxNdN4bmtbmzOl3OJrUJPG0b4B5GA7EAHAbnFe4eAfib8N/F/7PUNj4j8TWIbT/AApLo0j3VxPY+JdPdUZktYGgjMWoWUkgiCxzEGJQwYtxj6Q/bd8ZeAf2UNX0/SfDf7Ofwz8f61N4R1/x5fW92bXQ4bTR9GW3NyY5BZ3BluXa4VY4iqL8rFpFGKbqer6DcfHT4c+GfD/7NPwN13w18UrNtb0bUjrCW+rQaNHawzT6ld6f/ZDxwRxvcQQbftbb5JowpySF8nOOC8uxr56K9hNyU3KmoxlJ66t2u3q3fq9HeLcX62T+HOf4JclbGQrwUXFRqKcoxTtsr2S0Wmy3VpJNfA+kftsfE6x+Hd14Vm8VXuo+HbzSZNGNhfEXEUNu4C4QnncqjarEnaOlZnwZ/aX8U/AnTtSsdFk0m60zVnjmuLHVdOhv7UzRhhHMscgIWRdxww9BnIAFfoH4C8R6fqvi/wCJ2m+If2TvhS4+GNpaJdf8ITqMXiS4v9XuQrx6Uiz6XZIkiQuk00rOUhSWMtncQPRv2RfC3wn/AGqf2Zvh58Qn+D/wx8O3HxA0mLVItI+wWd00DOzL5UcrQxtNgqPmEa9egr15cPZU6VSisLTUKjUpJRilKSd02ktWnrfe55H/ABCfiT2tOs8096mmotubcU9Gk29E1pbax+WenftMeNNB1DwreaTrUmjX3g2ym0/TbqxURSrDNNJNIr9Q+XkbgjGMccV0Xjz9uH4gfE3QdasNcvtOvh4i0ay0K/uGtALi4t7SeSeJi4P+sLysWbHzccDFfVXjn9tL4MeHtf8AidHov7OXgvxFofgfStKvNA1f7LZ2tv40nv8AVm0lPJP2Z/Ks1uUcfaR5vmLG5SMgKWyPjn+3B8N/gJ8JTrWsfsx/B+117S/FfiXwfrltf6vBa6PZ32jQLP5dterpck1w96joLdWtYhu3CRk+XcpcO5VKpGtLDQ5ou6fKrp8/Pva/x+9/i13HT8J+JYU3RjmloyVmvfs/d5Nr2+H3fTQ+V/gr+1j40+Aehz6boN1pcljJdrqMEOo6dDfLYXiqVFzB5iny5cEDcvXaKn+EH7ZvxM+B19NJ4f8AFmoRQ3l+dTurS4fz7e8uGOXeRW5+c8tgjcetfoB4/wDiN8IfCf7Qvwj+HNj8APCeo6v8RbqyttfebR7KFfAb3lhPe2ttdMkUkc17KttPiBHAVIGlLhXi35/gn9oH9nvxT8ZPidpN58JPAOi/D34Z+F4PE58b3GjWz2+uxNe3NjMbS2WAtJEs9rLFHIrsZ5EIRMFSxiOHcpruq6+Fpy9rbnvGL5rbXutbX0+/cMP4UcS0FSVHNOX2V+SzmuW+9rPS58AXv7TXinVfhZfeDb5tI1DRbqea4g+1adFNc6Y003nSi2mYb4VdwCQDjirNh+138QtB8bN4g0fxFPoOqS6VaaNK+nqI45ra2iWKJWRtykhVByeckkYzX1lf/tfeDT8N9MvbP9kPwJb+MdUvfGSnw3rV3aWH9m2nhq3Se6a4uIrCfF3IssaLbpGyJIWDTELuP1p8JPgt8H/jL8KfDHjDTPhZ4Ft9N8WaRaazaxXHh6z86KK4hWVUfapXcAwBwSMjiqlkGVtSjLDQak5NpxTTcrOV1az5nGLfdpN6i/4hLxHdSWaWa5Umue6Ub8tne+l2l2TstD8oV/bX+JSfCnUvBt94ovNV8M3+lHSGtb5ftH2e3+XBjb725VTaCd2FJAFe3/H39vdv2Bvi7Zv4Z0Hw/wDEH9oDxN4W0qTVNa1mWe10P4f6J9mQ2Om20cbefNLJGDLKUkjLswZjtKxj9D5P2TPhW0bK3wv+HbKwwQfDtpz/AOOV+dv/AAUe+DPg/wCD/wDwUNvvEnxS/tDw58L/AIr6ZpOnaZ4l05xBHo89nBHbz2RLDyll2RK0ccmFeOdyoZodp7suy3BYRzWEpRp875pcsVG77uyV35n2vCHCea5M6lTMMV7dtRjFtybik27Lme2vyPj34/ft0fEL45fGG68ZePL7wnp+oXVlBYqulaXLaWF0sPmkOysXk852l2mRmxgRjIUVz9/8Q9YTSW1T7Hq1vaJgNPHbJJZwyf3GbHy/i2cEV64f2cPCPx8/aE8E/C34G+JtS+IWrarqsn/CZ60ljC+i+HtJEyhNQIgIMcjQmX93MwLSoEQsCzL+jfwn/wCCN3gz9nbxiuufDT4jfFbwTfbm8/yL2zuYr6JhjypopLfy3XbwcqSc9RgUV8pwM6jq1aUXJrXTf18/Pc/acp4+4swWFjgsDj6tOlB3SjJpL0621+G/LfW19T4W/wCCR+ha14h1Xxlq3/CR+GNN8O3V3GY9OmmEUz3u3Mska7sKjoUz13MvGMHP6S/En9pfUPgz+y/4q1COxtdS1zw7oF02neXfKsN06QMEZyR8mM5IGc7ODk4HJeMv+CS/gbxlqcl9ff8ACJ6pqFwzPNcaz4C026kmLcsWaA27Ek85LGubi/4Ig/C67vN99FoKx8HytJ8ONp4PPcNeSqR+ArvjKEYqKWi2Pmq0sTUqTrVHec223pq27t9tz8qf2Z9N/wCEC/ZI8XeL/F+sarpXw/0GSG30VbXTluLvXtXmWRI7aEF0HPlrukbdsG4gHYwr2Dw7onxE+G9r4f134WxX19rDaImuz2n9upp2rCO32R38TSJCYWninEqY2ldqiRk25FfU3/BS/wD4Je+Krb9mqx1bwF4m8XeOIfhv4ktPFsHgNNK0+C1e3t0eKVbKO2hjkM0UMjmONmYN8/Bd91fO3gb9qP4N+HvhdJ4gb4pWOuaNLPfazoGn2Nx9jvNFv7m1a0FvKGLSTrsndR9mVXkEaxyRbmEi+VWyLA4ip7adP3m7tpta28mfb5X4o8U5TgVlWFxP7hQcFFxhJJSab0lF3fRN3aTaVkfrF+yT+0d4f/av/Z38MePPDtxqFxY6tbGK5j1GJIb+zvIWMVzb3KRhUWaOVWVgiqpPKgAgV6PuATcR9K+Ff+Cdf7C3xP8AD37NcOqav8VPib8J9S8aaxfeJpfCmn6fpf8AxKUupAYhOLm3lkW5eJEeRdwCs+3aCpJ+rvFXwd8QeIvhHpfhu0+JvjLQ9Y0/yvP8UWdtYNqeo7AQ3mpJA1uN+QW2RjpxjmvQkknZHyEJNq7O8Eisen50b1HavCP+GQfH+P8Ak5r4yf8Agu0H/wCQa7u9+DniC5+CUPhVPid4zh16Lbu8XrbWB1aTEu85QwfZvmX92cRfd9+akq53fnL/AHTThhgP8eleEf8ADIPj/wD6Oa+Mg/7h2g//ACDXqnws8Dal8PfBdvpWreLNd8bX0Mkjvq+sRW0V3MGbIVlt4448KOBhQcDnPWgDzD9rn9vLw3+yT4v8H+FZPDfjTx9478fLdTaH4X8KWK3mo3UFqENxOwZlVUXeAOSzkNgfKSPHtY/4Le+CbzU/AMPg74Y/Fz4h2vxQ0v8AtXwvc6Np8C/2wEB+0wRRySBmlt2UrIBkA4wSCDXRf8FCv2o9P8IfEvwx8Fb74i6p+z7qHxM0a61LR/isGs/sNg9s+y408/aChWVlkQ7kkRl8yNgynGfgH/gnh4t8S+OP2gP2HbdrTwP4f8LaTZ+NNA8GTaDZXPmXaW0bxTalMlw2JVml2SRhSMgOG2k8AH1vr3/Bfzwn4U07xRd6p8EPjdptr4H1CLSvEMtzb2Ua6LeS58q3nJl+SR9pwp5OK2/jf/wV8k+GfgG5k8Zfs4ftNeC9M1h/7Ej1GSytbKaO4uA0UawS+Z8s5bPlkZ+YAjNfnX+2Bqtx8Gf2bf2oPB/xE+Inw+8a/FT4ieP9D8RPJ4Pu1voLmG13pPeTpCClgrPNGqxTMrbyVG7GT9r/APBXLwR4qi/bO8I2HxQ+Kmo6D+zD4+8WaddLeR6RayL4S1ywdjBaTTsV+z290xLi5YSDcCrJhA1AEH7DX/BUzx34T8G638LPEHwX/aA+K3i34T3/APZF7qtppcDautm4Z7L+1ojKRHemNWVirMJBGHzkmvZvB3/BWuPxN+0j4V+FeofAP45+F/FniwJcwQavYW0X2ayMpie/kUSFvs8TBi7DoFNfNv7N3we+M3jr4tf8FDfCvhf4n2tn8bLfxD4VdPFGn2C6TBqV0lpeyeWI9032VZYgItwZ9p5zgnG98BPgj4u+BH/BZv4Laf48+Ivi74j+LNW+DN9qWo3Ov3kFydMuDNMk1rbPFGmbdZUcqW3MSSd2MAAH2N+0h+1rr/wC8ZWOkaT8DfjJ8UobyyF4+peEdNiubO1Yuy+RIzOCJMLuxjGGFfnb+37/AMFMvi7rH7Rmh6x4L8D/ABh8Gz/B/TovFuq+D9d0oWMdjArSLJeXzwTN9os7mKRoXWaNfJ2RvGd28N9Tf8F0PgvrHxM/Zq8I61odr4/mk8I+K7WTWJPB93cpqdnok+F1CeK3hYfaZFWKEKrBtgd2A5NfI/jXxz4a8ZfFT9rP/hDtI8a6N4P0P9mFtK0U+LYbiHU7+zhlGy6YXP78xuWYK0oDMFJPUUAfYjf8Fgrn/hBrXxQf2ZP2iofDl/ZjULXUJNPs0t7m3MPniWN2lG9fKy4I6qM1d+Bv/BWpv2iJvD83hn9nv47X2h+ILm1iTW47O0msLOGeURi7mZJSywLlnLY+6jEV8h/Gy30fwN41+EvjT4qeFfEXibwN/wAMt2um/D67stDu9Us9H8SmygLswhDLDKYnYmWQBdrIc/LkVP8AglR8NNCHx3/ZN1D4U+D9Y0jXND8F6te/GPxJDp9za6Vq1rcxL9iiluX/AHNzL5ilgEztG3nKfKAfsLFB5tykfmKu5sb2Pyj3+lfFHiX/AILMSeBLzwrZ+Iv2Zf2jPD2peNrtdO0Oy1DTbWGfU7sx+YbaJTKN0oUE7Rzwa+vPh/8AEXw98W/B1l4g8K65pHibQNSVjaanpd0l1Z3QVirGORCVYBgwJB6givzs/a3+H/jix/4Kl/DHSvil8YLzQfh3q3iW58T/AAg1h/D1rc2+n+JWs/skejXMheMII2aOSJGDi5yVyjMzAA7/AFH/AILuaFpHgvxd4kuvgH8eLfw78P7uTT/E2pyWVotroNzGVEkFw/m4SRS65B9RW3b/APBZVrz4lf8ACFw/s1ftEz+Lv7NTWP7Gj0y1a++xO2xbny/Nz5ZYY3V8qL+zh8bP2iv2BP2vPh/4Ov8AQfGHjq++Nep2vi2BLSPTU8VRLCnnrZeY7LZyNcCOQBnb5EKbsnNdtd/B3413H/BRX4q+EdQ+I9j4k+Leqfs6zQ+Hdc0LRhoLWMjTkW0CJ5suJFdXXzwwb94CoBUUAe6eOv8Agss3ww8UeG9D8Rfs0/tFaHrXjK5ey0CxvdNtYZ9ZnTZuigUyZdhvXIHZq9Y/YG/b60r/AIKB6Z4i1DQvBHjbwfo/h29TTG1DxDHClve3O+RJ4YTGxy9uYx5oONvmJ68fmX8CPhv8J9N+MP8AwTz8efDmTWV1/wAXeMZ9K8bW+pavcXsljrNjFYrdW5jmZvIdZpZW2jBKSIT2A+z/APggFIsn7E/irawbHxQ8UDIP/T2KAL3xg/4LLx/s/wDhjVNc8afs6/H7w3oWiq8tzqOoWNpBAsKyeWJxul3eWzbcNj+Id6f4/wD+Cxkvwq8F6r4m8Rfs1/tDaR4b0LcNS1Wews/sdhtYKwllWYqpDMowTn5hXwb/AMFURo3wmm/bcvPiB4X8SSfFzxVeWUnw/wDEh0W6uLOLw7sjJjhvADBAgMW1kZgS21cE5A7L9qL4UeHfh34G/b8k+EXgrWPAPwP03wZpnhyS2udOuNN0/UfEVnqixzS20c4Bl2IJVaZeCJEJJ3rQB9taJ/wVC8U+I9Kt76x/ZK/agu7O6iSeCeLRLdo5o3UMrqfM5BUgj2NfNP7Rf/BYvVNC/aI0nxxonw8+PHh23+D88Oi/ErwprdvAmkLp+pM3lSyqshNtqAaLdBIwKyBCjbQQwxv2gvhLpfx//bXs/AnxR8YX3gm58U/CDw7F8BNYv7i5Gj2GtrFbG8kijgliLXvm+Xt+bIHJBPlKfEfjZ8J7X4A/B3/goR4Pi1TVtaOg6r8PIbnU9WvXu73UrhpdRea4mlkJZmklZ3+YkgNjJxQB9X/8FE/2ovGH7XP7K2p+Ev8Ahn/9p/wHo32y11bXL2XQImt7/TLVjNc2kzR3EbpDLGDudW4ABwRxXJ/8E7f27NF+GP7WUfgT4bfCn4vWPwc+NNpp+reC9AvQlzH4blW5lt9Q1G2YzORpRVkeUKW2PbsVAyxb2H9sf9j/AOO3jr4v/GHxNN8ePHOhfA9vh9d6tpei6Lf20BgvorFVk02aB4m3WsiJM5lUhiJNuQea+fv+CbGoQRftH/sQzySQwwp8Cdemdi4CRoNRu2ZieyqAST0ABNAH60X101jpl1cLDNcNbwvKIYuZJiqlgij+8xGAPUivjPxR/wAFkpvAmt+GdJ179mX9ozRdZ8ZTvZ6Hp95plpFcatcJGJHhhUy5d1U5IHYV9eeB/iBoPxU8HWfiDwvrWleItB1SMyWep6Zdpc2l2oJUtHKhKsAwIyCRkGvgHx94W8VeHP8AgtR8JNL+N3xPuv8AhX7+J73xV8IZW0eCO3vNQnjW3l0Ga6DKYZIFMPlIUczBuqs1AHVWf/BdjRNR+G+v+Mrf4A/Hu48I+FZ57bWdaisLVrLS5oGCzRzSCTCMhZQwPTNUdY/4L++E/D8esPffA744Wq+H9Ittf1NpbazUWGm3BTyLyT978sEnmR7X6HeK+XfAHw4+IvxM/wCCb/ii38J3Xji6+H8nx18V2PxJ0TwZFDJr2q6LNNbbpLZZEYu0LId0a8sshyCBxef4fL+3H8fP2tPAnwn8La74Vh1D4I6JoPhXRvFFjJo93JDZXNkIFaKY741kWHajSYySGbGTQB9caZ/wWe/tvx/H4Usv2a/2iLvxRNo8fiJdJi021a8bTZJFjS8Efm58kuyru6ZOK6P4Sf8ABVhfiv8AtL2Pwpb4D/G/w54pmjhu9QXVbG2jXQ7KVtqXtyBIWWDcQNwB5YV41+yl4x8SfGb/AIKz6hDqXg/xt8H9as/2a4NBtxrtvbSX9vJHqdnGL+KJGeNolmZgiucv5JJChsCX/glJpHibSv29fi9B8cPH0mrftCaB4ZsfCp0t9Ohs7bWdChmWWLWbWUMWvPNKRbvkjaHcQ4k3hkAP0TwpHfpnOaZ5q/3TVPxPpNxr/hrULG11K80e5vbV4Ib+0CNcWLspAmjEishdScjcpXI5BFeKr+yB8QFA/wCMmvjKcf8AUO0Hn/yRoA94BGen403zl/umuB8bfBfxF4p+GGg+H7D4peNvDeqaQIxdeIdPttPk1DWSkZVvPWW3aFd7HefLjX5hxgcVxH/DIPxAH/NzXxk/8F2g/wDyDQFz3f5SPw/KmmRQfu1wHiL4LeItb+DumeGbX4peNtI1mweNp/FVrbae2qaiF3bllSS3a3AfcM7IlPyLjHOeJH7IPxAA/wCTmvjJ/wCC7Qf/AJBoA91LKP4aTzF/u1wU/wAGfEM3wTj8Kr8UPGseuRkMfF629h/a74k34KGA23K/JxF90evNcR/wyB4/x/yc18ZB/wBw7Qf/AJBoA92yu3O2m+Yv92uD074M+ILH4K3XhWX4n+NbzXLhnZPF0tvYDVrfdIGARFgFthVBQZiPykk5PNcP/wAMg/ED/o5r4yf+C7Qf/kGgD3bcv92kLqD938q4Lwp8G/EHh34Rat4bvPid401zWNRMxg8UXlvYLqmnbwoTykjgW3PlkErvjbO45zXD/wDDIPxA/wCjmvjJ/wCC7Qf/AJBoA92yuzdim+Yv92uF+G3wc8QeB/AOuaPqnxO8ZeMNQ1ZpGttb1W3sI73SQ0KxqsKwQJEQjAyDzEb5mIORxXBp+x/8QFQD/hpr4ynAwSdO0Hn/AMkaAPeBtI+7Sx/N26VwXwf+DniD4aaTrVtrXxM8ZfECXVQq29zrlvYRSaVhHU+SLaCNTksrHzA3KL2yD1fgXw/deE/Bek6XfaxqHiK902zitZ9Wv0iW71N0UK08oiVYxI5G4hFVcngAUAatFFFACRf8fSfUV+En7SP/ACcd8RP+xo1T/wBLJa/dyL/j6T/eFfhH+0j/AMnH/ET/ALGnVP8A0slqo/EfgHj9/uOE/wAcvyRxdFFFbH8vHrn7B/xF0T4S/tdeCfEXiTUI9K0TS7id7q7kR3WFWtpUBIQFjlmUcA9a/T5f+Cl3wHXH/FyNL6Y/48bz/wCM1+MuKNtRKLZ+kcH+JeO4dwksHhaUJKUnJuXNe7SXSS00P2c/4eY/Af8A6KRpf/gDef8Axmj/AIeY/Af/AKKRpf8A4A3n/wAZr8Y8UYpezfc+s/4j3nH/AED0vun/APJH7Of8PMfgP/0UjS//AABvP/jNea/Ff9ov9mf4x/Hn4WfEHVviko1P4SvqsmlWSWEr2N42oRW8UhuEktHdjH9mRozG8ZDEk7uAPytxRij2b7h/xHvOP+gel90//kj9KPjl4y/Zc/aA+J/iTxZrHxk1q11LxTH4ajvI7OKRYV/sLUHv7UoHsnbMkrlZCzNlOF2H5q768/bV+Dl2+ot/w0Z4pjbUPFa+JQ0cO02tuPL3aPH/AKBxp77G3K26b94+Jhxj8mcUYo9m+/8AX3h/xHvOP+gel90//kj9M/2tPif+zP8Atd3ul3mpfGzX/COo6fpGqeG5rzw0ktvNqGj6kIRfafL59lMPLl+zxnegSRCCVcZNdL4A/aW/Zj+G3xwvvHWm/EiFb248KaX4MtdPltLlrHSdNsJZpY47cfZhIpkaUGQtIwfyYuAVBr8p8UYo9m/6/wCHD/iPecf9A9L7p/8AyR+sHwF/au/Zz/Z+8H63o+nfFqbVv+Ei17VPEV/e6nazvdTXV/M0svMdqiBE3BEG3Koig7sZPG/DvxJ+xf4L+Dfw38G6trnhXxxH8KNKbR/Dmr+JdDlu9VsYnOXZJktUEbtxlo1T7q9xmvzRxRij2b7/ANfeH/Ee84/6B6X3T/8Akj721n4V/sSXl34kbTviJrGgWfiDw1p3hmCysLu/a30RLC7F3a3FoJ7eUpKkiRAIxeFViAWJdzZXWvCX7LOqaJbNbftBeNtK8WGbxFNqfim1SNtS1wa+YjqaXCSaa9sFk8mIL5UEZiEYCkDNfBGKMUezff8Ar7w/4j3nH/QPS+6f/wAkfoNrvgX9hzVJvAps/F0+iw+A7mzmhgsLzVY4tXjtdOGnRRXamMhv9HSINJH5creSgL7MoXaV4E/YX0j4jza9H4lDWbaJp+iW2iNd6v8A2bpq2V291bz24CCaKVHZQoWURxiNSiIxZm/PfFGKPZvv/X3h/wAR7zj/AKB6X3T/APkj7s1X4Wfsgv4Gl0bRvjh428NTvrPiHVY9TsbiWa9gi1y3jtdRsQ1zYzIbd4IokVmQzKUD+azksfprwJ+35+zr8NfBOjeHNG+IOm2ujeH7CDTNPgNrfSGG3gjEcalmhy2FUDJ5NfjzijFHs33/AK+8P+I95x/0D0vun/8AJH7Of8PMfgP/ANFI0v8A8Abz/wCM1V1r/gon+z34n0K80vVPHXhvVNL1CPyrqxvdLuLm1uk/uyRSQFHX2YGvxtxRR7Nh/wAR7zj/AKB6X3T/APkj9iPCP7e37Nvw80R9L8O+LvB/h3TJJjcPZ6TokthbvKQFMjRw26qXICjcRnCgZ4Fag/4KXfAkD/kpGk/+Al3/APGa/GOijkH/AMR7zj/oHpfdP/5M/Zw/8FLfgQ3/ADUjSf8AwEu//jNH/Dy74E/9FI0n/wABLv8A+M1+MdFHIP8A4j5nH/QPS+6f/wAmfs1/w8r+BIZSvxJ0lSpyCLS8yP8AyDXN2/7YX7Kln8RW8YQ6r8N4fGEjmVtej8LlNUZyCC5uRbebvIJBfduOTk1+RNFHIL/iPecf9A9L7p//ACZ+zY/4KWfAkHP/AAsjSc/9el5/8Zpf+Hl/wJ/6KRpP/gJd/wDxmvxjoo5B/wDEfM4/6B6X3T/+TP2c/wCHl/wJ/wCikaT/AOAl3/8AGaP+Hl/wJ/6KRpP/AICXf/xmvxjoo9mH/EfM4/6B6X3T/wDkz9nP+Hl/wJ/6KRpP/gHef/GaR/8AgpV8Ccn/AIuVo/8A4CXf/wAar8ZKMUezYf8AEe83/wCgel/5P/8AJH7DeL/29f2bfiHoo03xF4u8F+ItNWVbgWeraLLfW4lAZVkEcsDKHCswDAZAZhnBNQz/ALcv7M9zrOh6jJ4q8FNqHheJoNFuRo0yy6NGyhGjtmEA8lSoClY9oIGMYr8f6KPZh/xHzN/+gen/AOTf/JH61XP7U37JF74a1bRZr/4ayaPr+oNq+qWJ8OP9n1O9Z95uZ0+z4ll3EtvbJBJIxmuq1f8A4KMfs/8AiCAxah8QPDupQtNHcmO70+5nQyxuJI5NrQkb0dVdWxlWUEYIBr8a6KPZh/xHzN/+gen/AOTf/JH7CaH+3Z+zX4Y8Ra5rGmeL/B+m6v4mljn1i+tdKuIrnVpI1KxvcSLDulZVZgCxOATRd/t2/s13/jW08TT+MPCM3iTT7V7G11aTSrhr22t2Ys0KSmHesbMxJUHBJJxX490UezD/AIj5m/8A0D0//Jv/AJI/ZtP+Cl3wKikVl+JmkKynIK212CP/ACFXPeIP20v2XvFutatqWreJPA+qalr2nf2Rqd3d6LNNPqNl/wA+sztATJD/ALDZX2r8haKPZh/xHzN/+gen/wCTf/JH7H2v/BQ39nuw8L/2Fb+PvDtvof2Qaf8A2fFYXSWv2UJ5Yg8sQ7fLCfLsxjbxjFZ6ft0/s6af8LpPBukfELRfDXh7+z5tMtbXRrS5s00yKWNkJtlWDbEy7yVIXAYA4r8gKKPZh/xHzN/+gen/AOTf/JH65fBL9sj9mn9nn4O+F/AfhXx9o+n+GfBulw6Rplu0F3I6QRLgF28n5nblmY8szE9TXRXf/BRn9n+/e2a4+IHh25aznFzbmbT7mQ28yhlWRN0J2uFdwGGCAx55NfjXRR7MP+I+Zv8A9A9P/wAm/wDkj9hPDP7eH7NvgqXUn0Xxh4R0eTWrx9R1FrLS7iA390/3p5SsI3yt3dsk4oX9u79mtPGz+Jh4v8H/APCSyWg09tW/sqf7c1qG3CAzeTv8sNzszjPavx7oo9mH/EfM3/6B6f8A5N/8kfr5qv7bP7MGvazo+pX3iTwLeah4dvZtS0q6m0OVptMu5mDzXMDeRmOaRhueRcM7ckk81N4K/bq/Zp+Gukyaf4b8W+DvD2nzXEl3JbabpM9rC88h3SSlUhALueS3UnrX4+0UezD/AIj5m/8A0D0//Jv/AJI/Yfx1+3t+zf8AFDwVfeGvE3jTwp4j8N6ouy80nU9MuLqxuxkNiSF4SjcgHkdQKn8b/wDBQj9nj4meHbzR/Enjzw14i0jUI/JurHU9Pubu3uY8htjxvCVZcgHBGMivxxoo9mw/4j5m/wD0D0//ACb/AOSP2O/4eGfs8my021bx14Xe30eVJ9Pik0yeRNPkSNo0eENCfLdY2ZAyYIViucEg87qv7V37JmunxB9u1L4b3n/CWyQS66ZvD0jnW3g3eQ91m3/fNHvfYXyV3NjGTX5J0UezD/iPmb/9A9P/AMm/+SP2S1H/AIKLfs/6xplxY3nxC8P3VldwtbT281jdPHPEy7WRlMOCrKSpB4IJFcVrv7Rv7JWs+En0i38QeDdDEeg3nhqwvdI0iWzvtFsbpJFmis5lt8wA+Y7YXjcc4Jr8oqKPZh/xHzN/+gen/wCTf/JH69/Cn9tz9m34JfC7w34L8M+PtF07w74S0y30jTLZba7PkW8EYRAT5PLYGSepYknk1sXP/BRf9n++uLOa4+IHh24m06cXVo8un3MjWkwVkEsZMJ2OFd13Lg4ZhnBNfjZRR7MP+I+Zv/0D0/8Ayb/5I/YLwT+3V+zT8M7C4tfDfi7wf4etbu7kv54dN0q4tY57mQgyTMqQgGRiASx5JAzWL8SP2zPgJ4l0fxhfeF/il4V8FfELxTosmkR+Mrfw7Ld6hafKRC7holM6RNtYRswAIBGCBX5L0UezD/iPmb/9A9P/AMm/+SPqT4ReMbo/t/eE/i742+MHwp0m+0dp4fFHiDQda16+l8Z2HkpHFpsemT2yw2Ft5iecyJI6+aWcBcKg+7v+Hin7Pv8AbD6l/wALA8N/2lJbLZPef2dc/aHt1dpFhMnk7jGHZmCZ27mJxnmvxtxRR7Nh/wAR8zf/AKB6f/k3/wAkfs0P+ClPwJJ/5KVo/wD4C3f/AMapw/4KXfAkf81I0n/wEu//AIzX4x4zRRyMP+I95v8A9A9L/wAn/wDkj9nP+Hl/wJ/6KRpP/gJd/wDxmj/h5f8AAn/opGk/+Al3/wDGa/GOij2Yf8R8zj/oHpfdP/5M/Zxf+Cl3wJP/ADUjSP8AwEu//jNH/Dy/4E/9FI0n/wABLv8A+M1+MeMUUezD/iPecf8AQPS+6f8A8kfs5/w8v+BP/RSNJ/8AAS7/APjNH/Dy/wCBP/RSNJ/8BLv/AOM1+MdFHsw/4j5nH/QPS+6f/wAmfs5/w8v+BP8A0UjSf/AS7/8AjNH/AA8v+BP/AEUjSf8AwEu//jNfjHRR7MP+I+Zx/wBA9L7p/wDyZ+zn/Dy/4E/9FI0n/wABLv8A+M0f8PL/AIE/9FI0n/wEu/8A4zX4x0UezD/iPmcf9A9L7p//ACZ+zn/Dy/4E/wDRSNJ/8BLv/wCM0f8ADy/4E/8ARSNJ/wDAS7/+M1+MdFHsw/4j5nH/AED0vun/APJn7Of8PL/gT/0UjSf/AAEu/wD4zR/w8v8AgT/0UjSf/AS7/wDjNfjHRR7MP+I+Zx/0D0vun/8AJn7Of8PL/gT/ANFI0n/wEu//AIzR/wAPL/gT/wBFI0n/AMBLv/4zX4x0UezD/iPmcf8AQPS+6f8A8mfs4n/BS74DidW/4WTpHUf8ud3/APGa/Iv45a7Z+Kfjj421TT7hbrT9T8Qahd2syghZopLmR0cAgEZUg8gHmuXzxQBinGNnc+L4y8RMbxJRp0cXThBU22uXm1ura3bCiiirPz0//9k='
WHERE AccountId='444f0122-6321-48fb-bc4d-123addde4b8e'
GO
DELETE FROM DomainMaster WHERE Id = 5
GO
INSERT INTO DomainMaster(id, code, DisplayText, AccountId, InstanceId,ParentId,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy)
values('5','5','ProductKindName',null, null, null,'2018-02-13 11:25:46.930','2018-02-13 11:25:46.930','admin','admin')
GO
DELETE FROM DomainValues WHERE DomainId = 5
GO
INSERT INTO DomainValues (id, DomainId, DomainValue, DisplayText, HasSubdomain, SubId,AccountId,InstanceId,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy)
VALUES ('13', '5', '0', 'OTC', null, null, null, null,'2018-02-13 11:25:46.930','2018-02-13 11:25:46.930','admin','admin')
GO
INSERT INTO DomainValues (id, DomainId, DomainValue, DisplayText, HasSubdomain, SubId,AccountId,InstanceId,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy)
VALUES ('14', '5', '1', 'Prescription', null, null, null, null,'2018-02-13 11:25:46.930','2018-02-13 11:25:46.930','admin','admin')
GO
INSERT INTO DomainValues (id, DomainId, DomainValue, DisplayText, HasSubdomain, SubId,AccountId,InstanceId,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy)
VALUES ('15', '5', '2', 'FMCG', null, null, null, null,'2018-02-13 11:25:46.930','2018-02-13 11:25:46.930','admin','admin')
GO
INSERT INTO DomainValues (id, DomainId, DomainValue, DisplayText, HasSubdomain, SubId,AccountId,InstanceId,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy)
VALUES ('16', '5', '3', 'F&B', null, null, null, null,'2018-02-13 11:25:46.930','2018-02-13 11:25:46.930','admin','admin')
GO
IF NOT EXISTS(SELECT * FROM Version)
INSERT INTO Version(DbVersion) Values(64)
GO
-- Sales & round off direct values data bacth script
-- -------------------------------------------------
GO
UPDATE SI SET SI.SellingPrice = PS.SellingPrice FROM SalesItem SI JOIN ProductStock PS ON SI.ProductStockId = PS.Id WHERE SI.SellingPrice IS NULL
GO
UPDATE SI SET SI.VAT = ISNULL(PS.VAT,0) FROM Sales S JOIN SalesItem SI ON S.Id = SI.SalesId JOIN ProductStock PS ON SI.ProductStockId = PS.Id WHERE ISNULL(S.TaxRefNo,0) = 0 AND SI.VAT IS NULL
GO
UPDATE SI SET SI.GstTotal = ISNULL(PS.GstTotal,0), SI.Igst = ISNULL(PS.GstTotal,0), SI.Cgst = CAST(ISNULL(PS.GstTotal,0)/2 AS DECIMAL(9,2)), SI.Sgst = CAST(ISNULL(PS.GstTotal,0)/2 AS DECIMAL(9,2)) FROM Sales S JOIN SalesItem SI ON S.Id = SI.SalesId JOIN ProductStock PS ON SI.ProductStockId = PS.Id WHERE ISNULL(S.TaxRefNo,0) = 1 AND (SI.GstTotal IS NULL OR ISNULL(SI.GstTotal,0) != ISNULL(PS.GstTotal,SI.GstTotal))
GO
UPDATE SI SET ItemAmount = d.ItemAmount FROM SalesItem SI
CROSS APPLY (SELECT CAST((Quantity*SellingPrice) AS DECIMAL(18, 6)) AS ItemAmount) d
WHERE ISNULL(SI.ItemAmount, 0) != d.ItemAmount
GO
UPDATE SI SET SI.DiscountAmount = ISNULL(d.SIDiscountValue,0) FROM Sales S JOIN SalesItem SI ON S.Id = SI.SalesId 
CROSS APPLY (SELECT CAST((SI.ItemAmount * CASE WHEN ISNULL(SI.Discount,0) > 0 THEN ISNULL(SI.Discount,0) ELSE ISNULL(S.Discount,0) END) / 100 AS DECIMAL(18, 6)) AS SIDiscountValue) d
WHERE (ISNULL(S.Discount,0) > 0 OR ISNULL(SI.Discount,0) > 0) AND ISNULL(SI.DiscountAmount,0) != ISNULL(d.SIDiscountValue,0)
GO
UPDATE SI SET SI.TotalAmount = ISNULL(d.TotalAmount,0) FROM SalesItem SI 
CROSS APPLY (SELECT CAST(SI.ItemAmount - ISNULL(SI.DiscountAmount,0) AS DECIMAL(18,6)) AS TotalAmount) d
WHERE ISNULL(SI.TotalAmount,0) != ISNULL(d.TotalAmount,0)
GO
UPDATE SI SET SI.VatAmount = ISNULL(d.VatAmount,0) FROM Sales S JOIN SalesItem SI ON S.Id = SI.SalesId 
CROSS APPLY (SELECT CAST((SI.TotalAmount - ((SI.TotalAmount / (100 + SI.VAT)) * 100)) AS DECIMAL(18, 6)) AS VatAmount) d
WHERE ISNULL(S.TaxRefNo,0) = 0 AND ISNULL(SI.VatAmount,0) != ISNULL(d.VatAmount,0)
GO
UPDATE SI SET SI.GstAmount = ISNULL(d.GstAmount,0) FROM Sales S JOIN SalesItem SI ON S.Id = SI.SalesId 
CROSS APPLY (SELECT CAST((SI.TotalAmount - ((SI.TotalAmount / (100 + SI.GstTotal)) * 100)) AS DECIMAL(18, 6)) AS GstAmount) d
WHERE ISNULL(S.TaxRefNo,0) = 1 AND ISNULL(SI.GstAmount,0) != ISNULL(d.GstAmount,0)
GO
UPDATE S SET S.GstAmount = ISNULL(SI.GstAmount,0) FROM Sales S JOIN 
(
	SELECT SI.SalesId,CAST(SUM(ISNULL(SI.GstAmount,0)) AS DECIMAL(18, 6)) AS GstAmount FROM Sales S 
	JOIN SalesItem SI ON S.Id = SI.SalesId WHERE ISNULL(S.TaxRefNo,0) = 1 GROUP BY SI.SalesId
) SI ON S.Id = SI.SalesId 
WHERE ISNULL(S.GstAmount,0) != ISNULL(SI.GstAmount,0)
GO
UPDATE S SET S.VatAmount = ISNULL(SI.VatAmount,0) FROM Sales S JOIN 
(
	SELECT SI.SalesId,CAST(SUM(ISNULL(SI.VatAmount,0)) AS DECIMAL(18, 6)) AS VatAmount FROM Sales S 
	JOIN SalesItem SI ON S.Id = SI.SalesId WHERE ISNULL(S.TaxRefNo,0) = 0 GROUP BY SI.SalesId
) SI ON S.Id = SI.SalesId 
WHERE ISNULL(S.VatAmount,0) != ISNULL(SI.VatAmount,0)
GO
UPDATE S SET S.SalesItemAmount = SI.SalesItemAmount FROM Sales S 
JOIN (SELECT SI.SalesId,CAST(SUM(SI.ItemAmount) AS DECIMAL(18, 6)) AS SalesItemAmount FROM SalesItem SI GROUP BY SI.SalesId) 
SI ON S.Id = SI.SalesId WHERE ISNULL(S.SalesItemAmount,0) != SI.SalesItemAmount
GO
UPDATE S SET S.TotalDiscountValue = ISNULL(SI.TotalDiscountValue,0) FROM Sales S 
JOIN (SELECT SI.SalesId,CAST(SUM(ISNULL(SI.DiscountAmount,0)) AS DECIMAL(18, 6)) AS TotalDiscountValue FROM SalesItem SI GROUP BY SI.SalesId) 
SI ON S.Id = SI.SalesId WHERE ISNULL(S.TotalDiscountValue,0) != ISNULL(SI.TotalDiscountValue,0)
GO
UPDATE S SET S.SaleAmount = ISNULL(d.SaleAmount, 0) FROM Sales S
CROSS APPLY (SELECT ROUND(ROUND((SalesItemAmount-ISNULL(TotalDiscountValue,0)), 2),0) AS SaleAmount) d
WHERE ISNULL(S.SaleAmount, 0) != ISNULL(d.SaleAmount, 0)
GO
UPDATE S SET S.RoundoffSaleAmount = ISNULL(d.RoundoffSaleAmount, 0) FROM Sales S
CROSS APPLY (SELECT CAST(SaleAmount-(SalesItemAmount-ISNULL(TotalDiscountValue,0)) AS DECIMAL(5, 2)) AS RoundoffSaleAmount) d
WHERE ISNULL(S.RoundoffSaleAmount, 0) != ISNULL(d.RoundoffSaleAmount, 0)
GO
UPDATE Sales SET IsRoundOff = 1 WHERE IsRoundOff IS NULL
GO
UPDATE SaleSettings SET IsEnableRoundOff = 1 WHERE IsEnableRoundOff IS NULL
GO
UPDATE Sales SET SaleAmount = 0 WHERE SaleAmount IS NULL
GO
UPDATE Sales SET RoundoffSaleAmount = 0 WHERE RoundoffSaleAmount IS NULL
GO
UPDATE Sales SET TotalDiscountValue = 0 WHERE TotalDiscountValue IS NULL
GO
UPDATE SRI SET SRI.MrpSellingPrice = PS.SellingPrice FROM SalesReturnItem SRI JOIN ProductStock PS ON SRI.ProductStockId = PS.Id WHERE SRI.MrpSellingPrice IS NULL
GO
UPDATE SRI SET SRI.GstTotal = ISNULL(PS.GstTotal,0), SRI.Igst = ISNULL(PS.GstTotal,0), SRI.Cgst = CAST(ISNULL(PS.GstTotal,0)/2 AS DECIMAL(9,2)), SRI.Sgst = CAST(ISNULL(PS.GstTotal,0)/2 AS DECIMAL(9,2)) FROM SalesReturn SR JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId JOIN ProductStock PS ON SRI.ProductStockId = PS.Id WHERE ISNULL(SR.TaxRefNo,0) = 1 AND (SRI.GstTotal IS NULL OR ISNULL(SRI.GstTotal,0) != ISNULL(PS.GstTotal,SRI.GstTotal))
GO
---- Should not run this query more than once and separately & below query should run with this
UPDATE SRI SET SRI.Discount = ISNULL(d.TotalDiscount,0) FROM SalesReturn SR 
JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId 
JOIN Sales S ON SR.SalesId = S.Id
CROSS APPLY (SELECT ISNULL(S.Discount,0)+ISNULL(SRI.Discount,0) AS TotalDiscount) d
WHERE ISNULL(S.Discount,0) > 0 AND ISNULL(SR.Discount,0) = 0 AND ISNULL(SRI.Discount,0) != ISNULL(d.TotalDiscount,0)
UPDATE SR SET SR.Discount = ISNULL(S.Discount,0) FROM SalesReturn SR 
JOIN Sales S ON SR.SalesId = S.Id
WHERE ISNULL(S.Discount,0) > 0 AND ISNULL(SR.Discount,0) != ISNULL(S.Discount,0)
GO
UPDATE SRI SET SRI.DiscountAmount = ISNULL(d.DiscountAmount,0) FROM SalesReturnItem SRI 
CROSS APPLY (SELECT CAST((SRI.Quantity*SRI.MrpSellingPrice)*ISNULL(SRI.Discount,0)/100 AS DECIMAL(18,6)) AS DiscountAmount) d
WHERE ISNULL(SRI.Discount,0) > 0 AND ISNULL(SRI.DiscountAmount,0) != ISNULL(d.DiscountAmount,0)
GO
UPDATE SRI SET SRI.TotalAmount = ISNULL(d.TotalAmount,0) FROM SalesReturnItem SRI 
CROSS APPLY (SELECT CAST((SRI.Quantity*SRI.MrpSellingPrice) - ISNULL(SRI.DiscountAmount,0) AS DECIMAL(18,6)) AS TotalAmount) d
WHERE ISNULL(SRI.TotalAmount,0) != ISNULL(d.TotalAmount,0)
GO
UPDATE SRI SET SRI.GstAmount = ISNULL(d.GstAmount,0) FROM SalesReturn SR JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId 
CROSS APPLY (SELECT CAST((SRI.TotalAmount - ((SRI.TotalAmount / (100 + SRI.GstTotal)) * 100)) AS DECIMAL(18, 6)) AS GstAmount) d
WHERE ISNULL(SR.TaxRefNo,0) = 1 AND ISNULL(SRI.GstAmount,0) != ISNULL(d.GstAmount,0)
GO
UPDATE SR SET SR.GstAmount = ISNULL(SRI.GstAmount,0) FROM SalesReturn SR JOIN 
(
	SELECT SRI.SalesReturnId,CAST(SUM(ISNULL(SRI.GstAmount,0)) AS DECIMAL(18, 6)) AS GstAmount FROM SalesReturn SR 
	JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId WHERE ISNULL(SR.TaxRefNo,0) = 1 AND ISNULL(SRI.IsDeleted,0) = 0 GROUP BY SRI.SalesReturnId
) SRI ON SR.Id = SRI.SalesReturnId 
WHERE ISNULL(SR.GstAmount,0) != ISNULL(SRI.GstAmount,0)
GO
UPDATE SR SET SR.ReturnItemAmount = ISNULL(SRI.ReturnItemAmount,0) FROM SalesReturn SR JOIN 
(SELECT SRI.SalesReturnId,CAST(SUM(SRI.Quantity*SRI.MrpSellingPrice) AS DECIMAL(18, 6)) AS ReturnItemAmount FROM SalesReturnItem SRI WHERE ISNULL(SRI.IsDeleted,0) = 0 GROUP BY SRI.SalesReturnId) 
SRI ON SR.Id = SRI.SalesReturnId WHERE ISNULL(SR.ReturnItemAmount,0) != ISNULL(SRI.ReturnItemAmount,0)
GO
UPDATE SR SET SR.TotalDiscountValue = ISNULL(SRI.TotalDiscountValue,0) FROM SalesReturn SR JOIN 
(SELECT SRI.SalesReturnId,CAST(SUM(ISNULL(SRI.DiscountAmount,0)) AS DECIMAL(18, 6)) AS TotalDiscountValue FROM SalesReturnItem SRI WHERE ISNULL(SRI.IsDeleted,0) = 0 GROUP BY SRI.SalesReturnId) 
SRI ON SR.Id = SRI.SalesReturnId WHERE ISNULL(SR.TotalDiscountValue,0) != ISNULL(SRI.TotalDiscountValue,0)
GO
UPDATE SR SET SR.DiscountValue = ISNULL(d.DiscountValue,0) FROM SalesReturn SR 
CROSS APPLY (SELECT CAST(SR.ReturnItemAmount*ISNULL(SR.Discount,0)/100 AS DECIMAL(18,6)) AS DiscountValue) d
WHERE ISNULL(SR.Discount,0) > 0 AND ISNULL(SR.DiscountValue,0) != ISNULL(d.DiscountValue,0)
GO
UPDATE SR SET SR.NetAmount = ISNULL(d.NetAmount, 0), SR.RoundOffNetAmount = 0 FROM SalesReturn SR
CROSS APPLY (SELECT CAST((SR.ReturnItemAmount-ISNULL(SR.TotalDiscountValue,0)) AS DECIMAL(18,6)) AS NetAmount) d
WHERE ISNULL(SR.NetAmount,0) != ISNULL(d.NetAmount, 0)
GO
UPDATE S SET S.NetAmount = ISNULL(S.SaleAmount,0), S.RoundOffNetAmount = ISNULL(S.RoundoffSaleAmount,0) FROM Sales S 
LEFT JOIN (SELECT * FROM SalesReturn SR WHERE ISNULL(SR.IsAlongWithSale,0) = 1) SR ON S.Id = SR.SalesId
WHERE SR.SalesId IS NULL AND ISNULL(S.NetAmount,0) ! = ISNULL(S.SaleAmount,0)
GO
UPDATE S SET S.NetAmount = ISNULL(d1.NetAmount,0), S.RoundOffNetAmount = ISNULL(d1.RoundOffNetAmount,0) FROM Sales S 
JOIN (SELECT * FROM SalesReturn SR WHERE ISNULL(SR.IsAlongWithSale,0) = 1)  SR ON S.Id = SR.SalesId
CROSS APPLY (SELECT (ISNULL(S.SaleAmount,0)-ISNULL(S.RoundoffSaleAmount,0))-(ISNULL(SR.NetAmount,0)-ISNULL(SR.RoundOffNetAmount,0)) AS BillAmount) d
CROSS APPLY (SELECT ROUND(ISNULL(d.BillAmount,0), 0) AS NetAmount,CAST(ISNULL(d.BillAmount,0)-ROUND(ISNULL(d.BillAmount,0), 0) AS DECIMAL(5,2)) AS RoundOffNetAmount) d1
WHERE ISNULL(S.NetAmount,0) != ISNULL(d1.NetAmount,0)
GO
UPDATE SalesReturn SET RoundoffNetAmount = 0 WHERE RoundoffNetAmount IS NULL
GO
UPDATE SalesReturn SET IsRoundOff = 1 WHERE ISNULL(RoundoffNetAmount,0) != 0
GO
DELETE FROM CustomSettings WHERE GroupId = 3
GO
INSERT INTO CustomSettings([Id], [GroupId], [GroupName], [CreatedAt], [UpdatedAt], [CreatedBy], [UpdatedBy])
VALUES(NEWID(), 3, 'Enable Sync Data From Online To Offline', GETDATE(), GETDATE(), 'admin', 'admin')
GO
DELETE FROM CustomSettings WHERE GroupId = 4
GO
INSERT INTO CustomSettings([Id], [GroupId], [GroupName], [CreatedAt], [UpdatedAt], [CreatedBy], [UpdatedBy])
VALUES(NEWID(), 4, 'Loyalty points', GETDATE(), GETDATE(), 'admin', 'admin')
GO

------Instance Type Id in Domain Master and Domain Values


DELETE FROM DomainMaster WHERE Id = 6
GO
INSERT INTO DomainMaster(Id,Code,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(6, 6,'InstanceTypeId', GETDATE(), GETDATE(), 'admin', 'admin')
GO
DELETE FROM DomainValues WHERE DomainId = 6
GO
INSERT INTO DomainValues(Id,DomainId,DomainValue,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(17, 6, 1, 'Only Offline', GETDATE(), GETDATE(), 'admin', 'admin')

INSERT INTO DomainValues(Id,DomainId,DomainValue,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(18, 6, 2, 'Both Online and Offline', GETDATE(), GETDATE(), 'admin', 'admin')

INSERT INTO DomainValues(Id,DomainId,DomainValue,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(19, 6, 3, 'Only Online', GETDATE(), GETDATE(), 'admin', 'admin')
GO

DELETE FROM DomainMaster WHERE Id = 7
GO
INSERT INTO DomainMaster(Id,Code,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(7, 7,'RegisterType', GETDATE(), GETDATE(), 'admin', 'admin')
GO
DELETE FROM DomainValues WHERE DomainId = 7
GO
INSERT INTO DomainValues(Id,DomainId,DomainValue,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(20, 7, 2, 'Live user', GETDATE(), GETDATE(), 'admin', 'admin')

INSERT INTO DomainValues(Id,DomainId,DomainValue,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(21, 7, 3, 'Test user', GETDATE(), GETDATE(), 'admin', 'admin')

INSERT INTO DomainValues(Id,DomainId,DomainValue,DisplayText,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy) VALUES(22, 7, 4, 'Trail user', GETDATE(), GETDATE(), 'admin', 'admin')
GO
