﻿--used
--drop index [XI_CommunicationLog_AccountIdInstanceIdCreatedAt] on [CommunicationLog];
--drop index [XI_InventoryOrder_ProductID] on [InventoryReOrder];
--drop index [XI_InventoryOrder_InstanceReorder] on [InventoryReOrder];
--drop index [XI_ProductStock_AccountIdInstanceIDProductExpiryDate] on [ProductStock];
--drop index [XI_ProductStock_InstanceID] on [ProductStock];
--drop index [XI_ProductStock_InstanceIDExpiryDate] on [ProductStock];
--drop index [XI_Sales_AccountIdInstanceIdInvoiceDate] on [Sales];
--drop index [XI_SalesItem_SalesID] on [SalesItem];
--drop index [XI_SalesItem_CreatedAt] on [SalesItem];
--drop index [XI_SalesItem_CreatedAt_SalesProductStockId] on [SalesItem];
--drop index [XI_SalesItem_InstanceID] on [SalesItem];
--drop index [XI_VendorPurchaseItem_CreatedAt] on [VendorPurchaseItem];
--drop index [XI_VendorPurchaseItem_VendorPurchaseId] on [VendorPurchaseItem];
--drop index [XI_VendorPurchase_InvoiceDate] on [VendorPurchase];
--drop index [XI_VendorPurchase_VendorIdInvoiceDate] on [VendorPurchase];
--drop index [XI_VendorPurchase_ProductStockId] on [VendorPurchaseItem];
--drop index [XI_Vendor_AccountIdInstanceId] on [Vendor]
--drop index [XI_Vendor_EmailAllowView] on [Vendor]


--GO
------------------used

CREATE NONCLUSTERED INDEX [XI_CommunicationLog_AccountIdInstanceIdCreatedAt]
ON [dbo].[CommunicationLog] ([AccountId],[InstanceId],[CreatedAt])
GO
CREATE NONCLUSTERED INDEX [XI_InventoryOrder_ProductID]
ON [dbo].[InventoryReOrder] ([ProductId])
INCLUDE ([Id],[ReOrder],[Status])
GO
CREATE NONCLUSTERED INDEX [XI_InventoryOrder_InstanceReorder]
ON [dbo].[InventoryReOrder] ([InstanceId],[ReOrder])
Go
CREATE NONCLUSTERED INDEX [XI_ProductStock_AccountIdInstanceIDProductExpiryDate]
ON [dbo].[ProductStock] ([AccountId],[InstanceId],[ProductId],[ExpireDate])
INCLUDE ([VendorId],[Stock])
GO
CREATE NONCLUSTERED INDEX [XI_ProductStock_InstanceID]
ON [dbo].[ProductStock] ([InstanceId])
INCLUDE ([ProductId],[Stock])
GO
CREATE NONCLUSTERED INDEX [XI_ProductStock_InstanceIDExpiryDate]
ON [dbo].[ProductStock] ([InstanceId],[ExpireDate])
INCLUDE ([Id],[ProductId],[VendorId],[Stock])
GO
CREATE NONCLUSTERED INDEX [XI_Sales_AccountIdInstanceIdInvoiceDate]
ON [dbo].[Sales] ([AccountId],[InstanceId],[InvoiceDate])
INCLUDE ([Id],[InvoiceNo],[Name],[Mobile],[Email],[PaymentType],[DeliveryType],[Discount],[CreatedBy])
GO
CREATE NONCLUSTERED INDEX [XI_SalesItem_SalesID]
ON [dbo].[SalesItem] ([SalesId])
INCLUDE ([ProductStockId],[Quantity],[Discount],[SellingPrice])
GO
CREATE NONCLUSTERED INDEX [XI_SalesItem_CreatedAt]
ON [dbo].[SalesItem] ([CreatedAt])
INCLUDE ([InstanceId],[ProductStockId],[Quantity],[SellingPrice])
GO
CREATE NONCLUSTERED INDEX [XI_SalesItem_CreatedAt_SalesProductStockId]
ON [dbo].[SalesItem] ([CreatedAt])
INCLUDE ([SalesId],[ProductStockId])
GO
CREATE NONCLUSTERED INDEX [XI_SalesItem_InstanceID]
ON [dbo].[SalesItem] ([InstanceId])
INCLUDE ([ProductStockId],[CreatedAt])
GO
CREATE NONCLUSTERED INDEX [XI_SalesReturnItem_SalesReturnId]
ON [dbo].[SalesReturnItem] ([SalesReturnId])
INCLUDE ([ProductStockId],[Quantity],[Discount],[MrpSellingPrice])
GO
CREATE NONCLUSTERED INDEX [XI_VendorPurchaseItem_CreatedAt]
ON [dbo].[VendorPurchaseItem] ([CreatedAt])
INCLUDE ([InstanceId],[VendorPurchaseId],[ProductStockId],[PackageQty],[PackagePurchasePrice],[Discount])
GO
 CREATE NONCLUSTERED INDEX [XI_VendorPurchaseItem_VendorPurchaseId]
ON [dbo].[VendorPurchaseItem] ([VendorPurchaseId])
INCLUDE ([ProductStockId],[PackageQty],[PackagePurchasePrice],[FreeQty],[Discount])
GO  
CREATE NONCLUSTERED INDEX [XI_VendorPurchase_InvoiceDate]
ON [dbo].[VendorPurchase] ([InvoiceDate])
INCLUDE ([Id],[VendorId],[InvoiceNo],[GoodsRcvNo])
GO 
CREATE NONCLUSTERED INDEX [XI_VendorPurchase_VendorIdInvoiceDate]
ON [dbo].[VendorPurchase] ([VendorId],[InvoiceDate])
INCLUDE ([Id],[InvoiceNo],[GoodsRcvNo])
 GO
CREATE NONCLUSTERED INDEX [XI_VendorPurchase_ProductStockId]
ON [dbo].[VendorPurchaseItem] ([ProductStockId])
INCLUDE ([VendorPurchaseId],[PurchasePrice])
GO

CREATE NONCLUSTERED INDEX [XI_Vendor_AccountIdInstanceId]
ON [dbo].[Vendor] ([AccountId],[InstanceId])
GO

 
CREATE NONCLUSTERED INDEX [XI_Vendor_EmailAllowView]
ON [dbo].[Vendor] ([Email],[AllowViewStock])
GO
 

------------------used