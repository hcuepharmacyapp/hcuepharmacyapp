﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--Transfer permission
--INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
--SELECT NEWID(),Id,AccountId,InstanceId,14 FROM HQueUser WHERE InstanceId IS NULL AND UserType = 2 
--AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 14) 

--SalesEdit permission
INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
SELECT NEWID(),Id,AccountId,InstanceId,15 FROM HQueUser WHERE InstanceId IS NULL AND UserType = 2 
AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 15) 

--SalesCancel permission
INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
SELECT NEWID(),Id,AccountId,InstanceId,16 FROM HQueUser WHERE InstanceId IS NULL AND UserType = 2 
AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 16) 

--Sales EditBill  permission
INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
SELECT NEWID(),Id,AccountId,InstanceId,17 FROM HQueUser WHERE InstanceId IS NULL AND UserType = 2 
AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 17) 

--Estimate  permission
INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
SELECT NEWID(),Id,AccountId,InstanceId,18 FROM HQueUser WHERE InstanceId IS NULL AND UserType = 2 
AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 18) 

--Estimate EditBill  permission
INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
SELECT NEWID(),Id,AccountId,InstanceId,19 FROM HQueUser WHERE InstanceId IS NULL AND UserType = 2 
AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 19) 


--AddProduct permission  // By San 
INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
SELECT NEWID(),Id,AccountId,InstanceId,26 FROM HQueUser WHERE UserType = 2 
AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 26) 

--StockAdjustMent permission  // By San 
INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
SELECT NEWID(),Id,AccountId,InstanceId,29 FROM HQueUser WHERE UserType = 2 
AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 29) 

--Vendor permission  // By Lawrence 
INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
SELECT NEWID(),Id,AccountId,InstanceId,30 FROM HQueUser WHERE UserType = 2  
AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 30) 

--Settings Page permission  // By Lawrence 
INSERT INTO UserAccess (Id,UserId,AccountId,InstanceId,AccessRight)
SELECT NEWID(),Id,AccountId,InstanceId,31 FROM HQueUser WHERE UserType = 2  
AND NOT EXISTS( SELECT 1 FROM  UserAccess WHERE UserId = HQueUser.Id AND AccessRight = 31)
