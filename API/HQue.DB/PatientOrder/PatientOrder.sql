﻿CREATE TABLE [dbo].[PatientOrder]
(
	[Id] CHAR(36) NOT NULL,
	[PatientExtId] VARCHAR(50) NOT NULL,
	[OrderId] VARCHAR(50) NOT NULL,
	[OrderStatus] INT NOT NULL,
	[Lat] VARCHAR(20) NOT NULL, 
    [Lon] VARCHAR(20) NOT NULL,
	[Prescription] Text NULL,
	[AccountId] CHAR(36) NULL,
	[InstanceId] CHAR(36) NULL,
	[Amount] decimal(10, 2) NULL,
	[DeliveryBoyId] CHAR(36) NULL,
	[DeliveredTime] DATETIME NULL,
	[WithPharmaId] int NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_PatientOrder] PRIMARY KEY ([Id])
)
