﻿CREATE TABLE [dbo].[DeliveryBoy]
(
	[Id] CHAR(36) NOT NULL,
	[AccountId] CHAR(36) NOT NULL,
	[InstanceId] CHAR(36) NOT NULL,
	[Name] VARCHAR(50) NOT NULL,
	[ImagePath] VARCHAR(100) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_DeliveryBoy] PRIMARY KEY ([Id]),
)
