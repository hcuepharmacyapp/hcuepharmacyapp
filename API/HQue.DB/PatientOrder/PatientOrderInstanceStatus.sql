﻿CREATE TABLE [dbo].[PatientOrderInstanceStatus]
(
	[Id] CHAR(36) NOT NULL,
	[PatientOrderId] CHAR(36) NOT NULL,
	[AcceptanceStatus] int NOT NULL,
	[AccountId] CHAR(36) NOT NULL,
	[InstanceId] CHAR(36) NOT NULL,
	[Duration] INT NULL,
	[MessageCode] VARCHAR(50) NOT NULL,
	[PushSentTime] DATETIME NOT NULL,
	[Distance] INT NOT NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_PatientOrderInstanceStatus] PRIMARY KEY ([Id]) 
    
)
