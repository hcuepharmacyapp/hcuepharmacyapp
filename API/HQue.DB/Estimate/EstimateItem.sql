﻿CREATE TABLE [dbo].[EstimateItem]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[EstimateId] CHAR(36) ,
	[ProductStockId] CHAR(36), 
	[ProductId] CHAR(36), 
	[ProductName]	VARCHAR(1000),
	[ReminderFrequency] TINYINT NULL DEFAULT 0, 
	[ReminderDate] DATE NULL,
    [Quantity] DECIMAL(18, 2) NULL, 
    [Discount] DECIMAL(5, 2) NULL ,
    [SellingPrice] DECIMAL(18, 2) NULL, 
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_EstimateItem] PRIMARY KEY ([Id]) 
)

GO

CREATE INDEX [IX_EstimateItem_EstimateId] ON [dbo].[EstimateItem] ([EstimateId])
