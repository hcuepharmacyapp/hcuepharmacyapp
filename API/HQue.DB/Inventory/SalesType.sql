﻿CREATE TABLE [dbo].[SalesType]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) NOT NULL,
	[Name] CHAR(50) NOT NULL,
	[Status] BIT NULL DEFAULT 1, 		
	[IsActive] BIT NULL DEFAULT 1,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_SalesType] PRIMARY KEY ([Id]), 
)
