﻿CREATE TABLE [dbo].[SelfConsumption]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) NOT NULL ,
	[InvoiceNo] VARCHAR(50) NULL,
	[ProductStockId] CHAR(36)  NULL, 
	[Consumption]	DECIMAL(18,2)  NULL,
	[ConsumptionNotes] VARCHAR(1000) NULL,
	[ConsumedBy] CHAR(36)  NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 	 
    CONSTRAINT [PK_SelfConsumption] PRIMARY KEY ([Id]) ,
)
