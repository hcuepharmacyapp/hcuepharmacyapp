﻿CREATE TABLE [dbo].[TaxValues]
(
	[Id] CHAR(36) NOT NULL  ,
	[AccountId] CHAR(36)  Not null ,
	[TaxGroupId] INT,
[Tax] DECIMAL(9, 2),
[IsActive] TINYINT NULL, 
CreatedAt datetime default getdate(),
UpdatedAt datetime default getdate(),
CreatedBy Char(36) default 'admin',
UpdatedBy Char(36) default 'admin',
 CONSTRAINT [PK_TaxValues] PRIMARY KEY ([Id]),
)
