﻿CREATE TABLE [dbo].[SalesPayment]
(
	Id char(36) Primary Key,
[AccountId] char(36) NOT NULL,
InstanceId char(36) NOT NULL,
SalesId char(36) NOT NULL,
PaymentInd int NOT NULL,
SubPaymentInd int NULL,
Amount DECIMAL(18, 2) NOT NULL,
isActive BIT NOT NULL default 1,
[CardNo] VARCHAR(50) NULL, 
 [CardDigits] VARCHAR(15) NULL, 
[CardName] VARCHAR(500) NULL,
    [CardDate] DATE NULL, 
	[CardTransId] VARCHAR(100) NULL,
	[ChequeNo] VARCHAR(50) NULL,
	[ChequeDate] DATE NULL,
	[WalletTransId] VARCHAR(100) NULL,
CreatedAt	datetime default getdate(),
UpdatedAt	datetime default getdate(),
CreatedBy	char(36) DEFAULT 'admin',
UpdatedBy	char(36) DEFAULT 'admin',
[OfflineStatus] BIT NULL DEFAULT 0
)
