﻿CREATE TABLE [dbo].[TemplatePurchaseChildMaster]
(
	[Id] CHAR(36) NOT NULL ,
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[TemplateColumnId] CHAR(36) NOT NULL,
	[HeaderIndex] SMALLINT NOT NULL, 
	[HeaderName] VARCHAR(200) ,
	[VendorId] CHAR(36),
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_TemplatePurchaseChildMaster] PRIMARY KEY ([Id]), 
)
