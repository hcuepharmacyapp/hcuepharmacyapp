﻿CREATE TABLE [dbo].[SalesTemplate]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) NOT NULL,
	[Prefix] Varchar(3) NULL,
	[TemplateNo] VARCHAR(15) ,
	[TemplateDate] DATE ,
	[TemplateName] VARCHAR(100) NULL,
	[IsActive] TINYINT NULL, 
	[FinyearId] varchar(36) NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_SalesTemplate] PRIMARY KEY ([Id]), 
)
