﻿CREATE TABLE [dbo].[TemplatePurchaseChildMasterSettings]
(
	[Id] CHAR(36) NOT NULL ,
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[HeaderOption] BIT NOT NULL,
	[VendorRowPos] SMALLINT NOT NULL,
	[ProductRowPos] SMALLINT NOT NULL,
	[DateFormat] VARCHAR(20) ,
	[VendorId] CHAR(36),
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_TemplatePurchaseChildMasterSettings] PRIMARY KEY ([Id]), 

)
