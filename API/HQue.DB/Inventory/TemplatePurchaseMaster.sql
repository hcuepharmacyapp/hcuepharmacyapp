﻿CREATE TABLE [dbo].[TemplatePurchaseMaster]
(
	[Id] CHAR(36) NOT NULL ,
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[VendorHeaderName] VARCHAR(200) NOT NULL,
	[TemplateColumnId] CHAR(36) NOT NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
	 CONSTRAINT [PK_TemplatePurchaseMaster] PRIMARY KEY ([Id]), 
)
