﻿CREATE TABLE [dbo].[PhysicalStockHistory]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[PopupNo] INT,
    [ProductId] CHAR(36) ,
	[CurrentStock] DECIMAL(18,2),
	[PhysicalStock] DECIMAL(18,2),
	[OpenedAt] DATETIME,
	[ClosedAt] DATETIME,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_PhysicalStockHistory] PRIMARY KEY ([Id]) 
)
