﻿CREATE TABLE [dbo].[DiscountRules]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,   
	[Amount] DECIMAL(18,2),
	[ToAmount] DECIMAL(18,2),	
	[Discount] DECIMAL(5,2),
    [FromDate] DATE NULL, 
	[ToDate] DATE NULL, 
	[BillAmountType] VARCHAR(50) NULL,
    [OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	 CONSTRAINT [PK_DiscountRules] PRIMARY KEY ([Id]), 
)
