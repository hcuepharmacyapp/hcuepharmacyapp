﻿CREATE TABLE [dbo].[StockAdjustment]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) NOT NULL ,
	[ProductStockId] CHAR(36)  NULL, 
	[AdjustedStock]	DECIMAL(18,2)  NULL,
	[AdjustedBy] CHAR(36)  NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[TaxRefNo] tinyint NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_StockAdjustment] PRIMARY KEY ([Id]) ,
)
