﻿
Create table SchedulerLog(Id char(36) Not null primary key, 
AccountId char(36) ,
InstanceId char(36),
Task Varchar(50),
 ExecutedDate datetime ,
IsOffline bit,
IsSynctoOnline bit default null, 
CreatedAt datetime default getdate(), 
CreatedBy varchar(36), 
UpdatedAt datetime default getdate(), 
UpdatedBy varchar(36)
)