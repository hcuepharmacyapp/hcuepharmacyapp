﻿
Create   table ClosingStock(
Id char(36) Not null primary key,
AccountId char(36),
InstanceId char(36),
ProductStockId char(36),
 TransDate datetime,
ClosingStock  decimal(18,2),
StockValue decimal(18,6),
GstTotal Decimal(8,2) ,
IsOffline bit,
IsSynctoOnline bit default null,
CreatedAt datetime default getdate(),
CreatedBy char(36)    
)