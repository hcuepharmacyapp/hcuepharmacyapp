﻿CREATE TABLE [dbo].[SalesTemplateItem]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) NOT NULL,
	[TemplateId] CHAR(36) NOT NULL,
	[ProductId] CHAR(36) NOT NULL,
	[Quantity] DECIMAL(18, 2) NULL, 
	[IsDeleted] BIT null DEFAULT 0, 
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_SalesTemplateItem] PRIMARY KEY ([Id]), 
)
