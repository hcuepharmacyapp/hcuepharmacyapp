﻿CREATE TABLE [dbo].[SalesOrderEstimate]
(
	[Id] CHAR(36) NOT NULL,
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) NOT NULL,
	[PatientId] CHAR(36)  NULL,
	[Prefix] Varchar(3) NULL,
	[OrderEstimateId] VARCHAR(15) ,
	[OrderEstimateDate] DATE ,
	[SalesOrderEstimateType] TINYINT NULL, 
	[IsEstimateActive] TINYINT NULL, 
	[FinyearId] varchar(36) NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
	CONSTRAINT [PK_SalesOrderEstimate] PRIMARY KEY ([Id]), 
)
