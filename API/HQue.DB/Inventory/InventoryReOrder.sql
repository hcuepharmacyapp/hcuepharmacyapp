﻿CREATE TABLE [dbo].[InventoryReOrder]
(
	[Id] CHAR(36) NOT NULL PRIMARY KEY, 
    [AccountId] CHAR(36) NULL, 
    [InstanceId] CHAR(36) NULL, 
	[ProductId] CHAR(36) NULL, 
    [ReOrder] DECIMAL(18, 2) NULL, 
	[OfflineStatus] BIT NULL DEFAULT 0,
    [CreatedAt] DATETIME NULL DEFAULT GetDate(), 
    [UpdatedAt] DATETIME NULL DEFAULT GetDate(), 
    [CreatedBy] CHAR(36) NULL DEFAULT 'admin', 
    [UpdatedBy] CHAR(36) NULL DEFAULT 'admin', 
    [Status] INT NULL
)
