﻿CREATE TABLE [dbo].[CommunicationLog]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[SmsLogId] VARCHAR(36) NULL,
    [UserId] VARCHAR(36) NULL, 
    [Mobile] VARCHAR(15) NULL, 
	[Message] VARCHAR(max) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
	CONSTRAINT [PK_CommunicationLog] PRIMARY KEY ([Id]), 
)
