﻿CREATE TABLE [dbo].[InventorySmsSettings]
(
	[Id] CHAR(36) NOT NULL,
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,   
	[SmsType] VARCHAR(50) NULL,
	[SmsOption] VARCHAR(50) NULL,
	[SmsContent] VARCHAR(500) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	CONSTRAINT [PK_InventorySmsSettings] PRIMARY KEY ([Id]), 
)