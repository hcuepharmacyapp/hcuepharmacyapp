﻿CREATE TABLE [dbo].[Account]
(
	[Id] CHAR(36) NOT NULL, 
	[Code] VARCHAR(15),
	[Name] VARCHAR(150),
	[RegistrationDate] DATE,
	[InstanceCount] INT NOT NULL,
	[Phone]	VARCHAR(15),
	[Email]	VARCHAR(150),
	[Address]	VARCHAR(300),
	[Area]	VARCHAR(100) NULL,
    [City] VARCHAR(100) NULL, 
    [State] VARCHAR(100) NULL, 
    [Pincode] VARCHAR(10) NULL, 
	[RegisterType] SMALLINT NULL,
	[isChain] BIT NULL, 
	[LicensePeriod] TINYINT NULL, 
	[LicenseStartDate] DATE NULL,
	[LicenseEndDate] DATE NULL,
	[LicenseCount] INT NULL, 
    	[IsAMC] BIT NULL, 
	[IsApproved] BIT NULL, 
	[Network] CHAR(36) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    [IsBillNumberReset] bit default NULL,
	[InstanceTypeId] INT NULL,
	[VerticalTypeId] INT NULL,
	[SubVerticalTypeId] INT NULL,
    CONSTRAINT [PK_Account] PRIMARY KEY ([Id]) 
)
