﻿CREATE TABLE [dbo].[InstanceClient]
(
    [Id] CHAR(36) NOT NULL, 
    [InstanceId] CHAR(36) NOT NULL, 
    [ClientId] VARCHAR(300) NOT NULL, 
    [LastSyncedAt] DATETIME2 NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
    CONSTRAINT [PK_InstanceClient] PRIMARY KEY ([Id])
)
