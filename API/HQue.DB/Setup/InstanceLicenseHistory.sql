﻿CREATE TABLE [dbo].[InstanceLicenseHistory]
(
	[Id] CHAR(36) NOT NULL, 
	[AccountId] CHAR(36) NOT NULL,
	[InstanceId] CHAR(36) NOT NULL,	
	[LicenseStartDate] DATE,
	[LicenseEndDate] DATE,
	[LastLoggedinDate] DATE,
	[IsActive] BIT,	
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',    
    CONSTRAINT [PK_InstanceLicenseHistory] PRIMARY KEY ([Id]) 
)
