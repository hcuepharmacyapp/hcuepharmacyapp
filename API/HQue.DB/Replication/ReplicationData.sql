﻿CREATE TABLE [dbo].[ReplicationData]
(
	[Id] CHAR(36) NOT NULL, 
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) NULL ,
    [ClientId] VARCHAR(300) NOT NULL, 
    [TableName] VARCHAR(100) NOT NULL, 
	[Action] TINYINT NOT NULL, 
    [ActionTime] DATETIME2 NOT NULL, 
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    [TransactionID] VARCHAR(50) NULL, 
    CONSTRAINT [PK_Replication] PRIMARY KEY ([Id])
	)
