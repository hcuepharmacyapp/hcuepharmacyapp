﻿CREATE TRIGGER TriggerImportProductMaster
ON [ImportProcessActivity]  
AFTER INSERT   
AS   
DECLARE @AccountID AS [VARCHAR](36)
DECLARE @InstanceID AS [VARCHAR](36)
DECLARE @FileName AS [VARCHAR](500)
begin

SELECT @AccountID = accountId, @InstanceID = instanceId, @FileName = [fileName]  FROM INSERTED
EXEC sp_importProductMaster @AccountID, @InstanceID, @FileName, 1

End
