﻿/*
Deployment script for hcueloyalpoint

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "hcueloyalpoint"
:setvar DefaultFilePrefix "hcueloyalpoint"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Altering [dbo].[usp_GetTransferStockList]...';


GO
/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 02/05/2017	Poongodi R	   BatchNo, Expiredate, VAT, Selling pricetaken from stock transfer item - For Sync
** 18/05/2017   bala           [ProductStockVendorId] and [ProductStockPackageSize] getting data from stocktransferitem 
** 16/06/2007	Poongodi R		Accepted Qty, Size and Strip , No of strip col added
** 04/07/2007	Poongodi R		GST total taken from stock transferitem
** 25/06/2018  Poongodi R             Productid Validation removed 
** 28/11/2018  Sumathi             LEFT JoIN added in Product stock table
** 28/11/2018  Sarubala            LEFT JoIN added in Product table
*******************************************************************************/
ALTER PROCEDURE [dbo].[usp_GetTransferStockList](@TransferID varchar(max),
@InstanceId char(36))
 AS
 BEGIN
   SET NOCOUNT ON


SELECT StockTransferItem.Id as Id,StockTransferItem.AccountId as AccountId,
StockTransferItem.InstanceId as InstanceId,
StockTransferItem.TransferId as TransferId,
StockTransferItem.ProductStockId as ProductStockId,
 StockTransferItem.Quantity  ,
case ST.ToInstanceId when @InstanceId then isnull(StockTransferItem.AcceptedUnits , StockTransferItem.Quantity ) else StockTransferItem.Quantity end  as Quantity1,
StockTransferItem.Discount as Discount,StockTransferItem.OfflineStatus as OfflineStatus, StockTransferItem.CreatedAt as CreatedAt,StockTransferItem.UpdatedAt as UpdatedAt,StockTransferItem.CreatedBy as CreatedBy,StockTransferItem.UpdatedBy as UpdatedBy,
ProductStock.Id 
AS [ProductStockId],ProductStock.AccountId AS [ProductStockAccountId],ProductStock.InstanceId AS [ProductStockInstanceId],
ISNULL(ProductStock.ProductId, Product.Id) AS [ProductStockProductId],ISNULL(StockTransferItem.VendorId, VendorOrder.VendorId) AS [ProductStockVendorId],
isnull(StockTransferItem.BatchNo, ProductStock.BatchNo) AS [ProductStockBatchNo],  isnull(StockTransferItem.ExpireDate,( ISNULL(ProductStock.ExpireDate, VendorOrderItem.ExpireDate))) 

AS [ProductStockExpireDate], isnull(StockTransferItem.VAT, ISNULL(ProductStock.VAT, VendorOrderItem.VAT) )
AS [ProductStockVAT],ProductStock.TaxType AS [ProductStockTaxType],
isnull(StockTransferItem.SellingPrice, ISNULL(ProductStock.SellingPrice, VendorOrderItem.SellingPrice)) AS [ProductStockSellingPrice],
isnull(StockTransferItem.MRP, ISNULL(ProductStock.MRP, VendorOrderItem.SellingPrice)) AS [ProductStockMRP],
ProductStock.PurchaseBarcode AS 
[ProductStockPurchaseBarcode],ProductStock.Stock AS [ProductStockStock],
case ST.ToInstanceId when @InstanceId then isnull(StockTransferItem.AcceptedPackageSize ,  ISNULL(StockTransferItem.PackageSize, ISNULL(VendorOrderItem.PackageSize,1))) else 
 ISNULL(StockTransferItem.PackageSize, ISNULL(VendorOrderItem.PackageSize,1)) end  AS [StockTransferPackageSize], 
  ISNULL(StockTransferItem.PackageSize, ISNULL(VendorOrderItem.PackageSize,1))  [ProductStockPackageSize],
ProductStock.PackagePurchasePrice AS 
[ProductStockPackagePurchasePrice], isnull(StockTransferItem.PurchasePrice, ISNULL(ProductStock.PurchasePrice, VendorOrderItem.PurchasePrice)) AS [ProductStockPurchasePrice],
ProductStock.OfflineStatus AS [ProductStockOfflineStatus],ProductStock.CreatedAt AS 
[ProductStockCreatedAt],ProductStock.UpdatedAt AS [ProductStockUpdatedAt],ProductStock.CreatedBy AS [ProductStockCreatedBy],ProductStock.UpdatedBy AS 
[ProductStockUpdatedBy],ProductStock.CST AS [ProductStockCST],isnull(StockTransferItem.GstTotal, isnull(ProductStock.GstTotal ,0)) AS [ProductStockGstTotal],ProductStock.IsMovingStock AS [ProductStockIsMovingStock],
ProductStock.ReOrderQty AS [ProductStockReOrderQty],ProductStock.Status AS [ProductStockStatus],ProductStock.IsMovingStockExpire AS [ProductStockIsMovingStockExpire],ProductStock.NewOpenedStock AS 
[ProductStockNewOpenedStock],ProductStock.NewStockInvoiceNo AS [ProductStockNewStockInvoiceNo],ProductStock.NewStockQty AS [ProductStockNewStockQty],ProductStock.StockImport AS 
[ProductStockStockImport],VendorOrderItem.Id AS [VendorOrderItemId],VendorOrderItem.AccountId AS [VendorOrderItemAccountId],VendorOrderItem.InstanceId AS 
[VendorOrderItemInstanceId],VendorOrderItem.VendorOrderId AS [VendorOrderItemVendorOrderId],VendorOrderItem.ProductId AS [VendorOrderItemProductId],VendorOrderItem.Quantity AS 
[VendorOrderItemQuantity],VendorOrderItem.VAT AS [VendorOrderItemVAT],VendorOrderItem.PackageSize AS [VendorOrderItemPackageSize],VendorOrderItem.PackageQty AS 
[VendorOrderItemPackageQty],VendorOrderItem.PurchasePrice AS [VendorOrderItemPurchasePrice],VendorOrderItem.OfflineStatus AS [VendorOrderItemOfflineStatus],VendorOrderItem.CreatedAt AS 
[VendorOrderItemCreatedAt],VendorOrderItem.UpdatedAt AS [VendorOrderItemUpdatedAt],VendorOrderItem.CreatedBy AS [VendorOrderItemCreatedBy],VendorOrderItem.UpdatedBy AS 
[VendorOrderItemUpdatedBy],VendorOrderItem.VendorPurchaseId AS [VendorOrderItemVendorPurchaseId],VendorOrderItem.StockTransferId AS [VendorOrderItemStockTransferId],Product.Id AS 
[ProductId],Product.AccountId AS [ProductAccountId],Product.InstanceId AS [ProductInstanceId],Product.Code AS [ProductCode],Product.Name AS [ProductName],Product.Manufacturer AS 
[ProductManufacturer],Product.KindName AS [ProductKindName],Product.StrengthName AS [ProductStrengthName],Product.Type AS [ProductType],Product.Schedule AS [ProductSchedule],Product.Category AS [ProductCategory],Product.GenericName AS [ProductGenericName]

,Product.CommodityCode AS [ProductCommodityCode],Product.Packing AS [ProductPacking],Product.OfflineStatus AS [ProductOfflineStatus],Product.CreatedAt AS [ProductCreatedAt],Product.UpdatedAt AS [ProductUpdatedAt],Product.CreatedBy AS [ProductCreatedBy], 



Product.UpdatedBy AS [ProductUpdatedBy],Product.PackageSize AS [ProductPackageSize],Product.VAT AS [ProductVAT],Product.Price AS [ProductPrice],Product.Status AS [ProductStatus],Product.RackNo AS [ProductRackNo],Product.ProductMasterID AS [ProductProductM
asterID],
Product.ProductOrgID AS [ProductProductOrgID],Product.ReOrderLevel AS [ProductReOrderLevel],Product.ReOrderQty AS [ProductReOrderQty],Product.Discount AS [ProductDiscount] ,
cast(StockTransferItem.Quantity  / ISNULL(StockTransferItem.PackageSize, ISNULL(VendorOrderItem.PackageSize,1)) as numeric(10,1))[Noofstrips]
 FROM StockTransferItem WITH(NOLOCK)
 inner join stockTransfer ST on ST.Id = StockTransferItem.TransferId
LEFT JOIN ProductStock WITH(NOLOCK) ON ProductStock.Id = StockTransferItem.ProductStockId
LEFT JOIN Product WITH(NOLOCK) ON Product.Id = StockTransferItem.ProductId
LEFT JOIN VendorOrderItem WITH(NOLOCK) ON VendorOrderItem.StockTransferId = StockTransferItem.Id 
LEFT JOIN VendorPurchaseItem WITH(NOLOCK) ON VendorPurchaseItem.id = VendorOrderItem.VendorPurchaseId
LEFT JOIN VendorOrder WITH(NOLOCK) ON VendorOrder.id = VendorOrderItem.VendorOrderId
/*INNER JOIN Product WITH(NOLOCK) ON (  Product.Id = ProductStock.ProductId OR Product.Id = VendorOrderItem.ProductId) */
/* Changed by Sumathi on 28-11-18 */
/*LEFT JOIN Product WITH(NOLOCK) ON (  Product.Id = ProductStock.ProductId OR Product.Id = VendorOrderItem.ProductId)  */
WHERE StockTransferItem.TransferId in (select a.id from dbo.udf_Split(@TransferID ) a)
           --Product.Id = StockTransferItem.ProductId OR commented by poongodi 
 END
GO
PRINT N'Update complete.';


GO
