﻿/*
Deployment script for hcuepharmaDev

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "hcuepharmaDev"
:setvar DefaultFilePrefix "hcuepharmaDev"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Creating [dbo].[LoyaltyProductPoints]...';


GO
CREATE TABLE [dbo].[LoyaltyProductPoints] (
    [Id]               CHAR (36)       NOT NULL,
    [AccountId]        CHAR (36)       NULL,
    [InstanceId]       CHAR (36)       NULL,
    [LoyaltyId]        CHAR (36)       NULL,
    [KindName]         VARCHAR (250)   NULL,
    [KindOfProductPts] DECIMAL (18, 6) NULL,
    [CreatedAt]        DATETIME        NOT NULL,
    [UpdatedAt]        DATETIME        NOT NULL,
    [CreatedBy]        CHAR (36)       NOT NULL,
    [UpdatedBy]        CHAR (36)       NOT NULL,
    CONSTRAINT [PK_LoyaltyProductPoints] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Creating unnamed constraint on [dbo].[LoyaltyProductPoints]...';


GO
ALTER TABLE [dbo].[LoyaltyProductPoints]
    ADD DEFAULT GetDate() FOR [CreatedAt];


GO
PRINT N'Creating unnamed constraint on [dbo].[LoyaltyProductPoints]...';


GO
ALTER TABLE [dbo].[LoyaltyProductPoints]
    ADD DEFAULT GetDate() FOR [UpdatedAt];


GO
PRINT N'Creating unnamed constraint on [dbo].[LoyaltyProductPoints]...';


GO
ALTER TABLE [dbo].[LoyaltyProductPoints]
    ADD DEFAULT 'admin' FOR [CreatedBy];


GO
PRINT N'Creating unnamed constraint on [dbo].[LoyaltyProductPoints]...';


GO
ALTER TABLE [dbo].[LoyaltyProductPoints]
    ADD DEFAULT 'admin' FOR [UpdatedBy];


GO
PRINT N'Update complete.';


GO
