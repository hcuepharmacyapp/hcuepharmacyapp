﻿/*
Deployment script for hcueloyalpoint

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "hcueloyalpoint"
:setvar DefaultFilePrefix "hcueloyalpoint"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Altering [dbo].[SaleSettings]...';


GO
ALTER TABLE [dbo].[SaleSettings]
    ADD [IsEnableNewSalesScreen] BIT NULL;


GO
PRINT N'Refreshing [dbo].[sp_syncinstancedata]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[sp_syncinstancedata]';


GO
PRINT N'Refreshing [dbo].[usp_FullSyncData]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[usp_FullSyncData]';


GO
PRINT N'Refreshing [dbo].[usp_GetDoctorList]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[usp_GetDoctorList]';


GO
PRINT N'Update complete.';


GO
