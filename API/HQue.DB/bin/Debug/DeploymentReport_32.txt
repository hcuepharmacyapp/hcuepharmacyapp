﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       [dbo].[Reverse_SyncData_Seed] (Table)
       [dbo].[usp_Reverse_SyncData_Seed_Insert] (Procedure)
       [dbo].[usp_SyncDataRemove_Scheduler] (Procedure)

** Supporting actions
