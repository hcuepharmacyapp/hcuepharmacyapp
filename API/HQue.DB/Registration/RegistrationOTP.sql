﻿CREATE TABLE [dbo].[RegistrationOTP]
(
[Id] CHAR(36) NOT NULL , 
   [AccountId] CHAR(36) NOT NULL, 
[UserId] CHAR(36) NOT NULL,
   [OTPType] TINYINT NOT NULL, 
   [OTPNumber] CHAR(4) NOT NULL
CONSTRAINT [PK_Registration] PRIMARY KEY ([Id]), 
   [CreatedAt] DATETIME NOT NULL DEFAULT GetDate()
   
)