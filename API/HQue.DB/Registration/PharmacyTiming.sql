﻿CREATE TABLE [dbo].[PharmacyTiming]
(
	[Id] CHAR(36) NOT NULL PRIMARY KEY, 
    [AccountId] CHAR(36) NOT NULL, 
    [InstanceId] CHAR(36) NOT NULL, 
    [DayOfWeek] INT NOT NULL, 
    [Timing] VARCHAR(55) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin'
)
