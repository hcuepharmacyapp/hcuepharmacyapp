﻿CREATE TABLE [dbo].[PharmacyUploads]
(
[Id] CHAR(36) NOT NULL , 
   [AccountId] CHAR(36) NOT NULL, 
   [InstanceId] CHAR(36) NOT NULL, 
   [FileName] VARCHAR(500) NOT NULL, 
   [OfflineStatus] BIT NULL DEFAULT 0,
[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
   [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
   [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
   [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
[FileNumber] SMALLINT NOT NULL, 
   CONSTRAINT [PK_PharmacyUploads] PRIMARY KEY ([Id]) 
)