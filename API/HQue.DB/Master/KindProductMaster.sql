﻿CREATE TABLE [dbo].[KindProductMaster]
(
[Id] CHAR(36) NOT NULL  ,
[AccountId] CHAR(36)  Not null ,
[KindName] Varchar(200),
[IsActive] TINYINT NULL, 
CreatedAt datetime default getdate(),
UpdatedAt datetime default getdate(),
CreatedBy Char(36) default 'admin',
UpdatedBy Char(36) default 'admin',
CONSTRAINT [PK_KindProductMaster] PRIMARY KEY ([Id]),
)
