﻿CREATE TABLE [dbo].[VendorPurchaseFormula]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36),
	[InstanceId] CHAR(36),   
	[FormulaName] VARCHAR(100),	
	[FormulaJSON] VARCHAR(1000),
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME NOT NULL DEFAULT GetDate(),
	[UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(),
	[CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	[UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	CONSTRAINT [PK_VendorPurchaseFormula] PRIMARY KEY ([Id]), 
)
