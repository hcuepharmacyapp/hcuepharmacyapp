﻿CREATE TABLE [dbo].[Product]
(
	[Id] CHAR(36) NOT NULL DEFAULT NEWID(),  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
    [Code] VARCHAR(15) NULL, 
    [Name] VARCHAR(1000) NOT NULL, 
    [Manufacturer] VARCHAR(300) NULL, 
	[KindName] VARCHAR(250) NULL,
	[StrengthName] VARCHAR(500) NULL,
	[Type] VARCHAR(250) NULL,
	[Schedule] VARCHAR(250) NULL,
	[Category] VARCHAR(250) NULL,
	[GenericName] VARCHAR(250) NULL,
	[CommodityCode] VARCHAR(250) NULL,
	[Packing] VARCHAR(250) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	Ext_RefId Varchar(36) NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    [PackageSize] INT NULL, 
    [VAT] DECIMAL(9, 2) NULL, 
    [Price] DECIMAL(18, 2) NULL, 
    [Status] TINYINT NULL, 
    [RackNo] VARCHAR(50) NULL, 
    [ProductMasterID] CHAR(36) NULL,
	[ProductOrgID] [char](36) NULL,
    [ReOrderLevel] DECIMAL(18, 2) NULL, 
    [ReOrderQty] DECIMAL(18, 2) NULL, 
    [Discount] DECIMAL(18, 2) NULL, 
	[Eancode] VARCHAR(50) NULL, 
	[IsHidden] bit null default 0,
	[HsnCode] VARCHAR(50),
	[Igst] DECIMAL(9, 2) NULL, 
	[Cgst] DECIMAL(9, 2) NULL, 
	[Sgst] DECIMAL(9, 2) NULL, 
	[GstTotal] DECIMAL(9, 2) NULL, 
    [Subcategory] VARCHAR(250) NULL, 
    CONSTRAINT [PK_Product] PRIMARY KEY ([Id]), 
     
)
