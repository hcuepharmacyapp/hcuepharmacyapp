﻿CREATE TABLE [dbo].[FinYearMaster]
(
	[Id] CHAR(36) NOT NULL,
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) NULL ,
	[FinYearStartDate] DATETIME NULL, 
	[FinYearEndDate] DATETIME NULL, 
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
	CONSTRAINT [PK_FinYearMaster] PRIMARY KEY ([Id]) 
)
