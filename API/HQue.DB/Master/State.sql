﻿CREATE TABLE [dbo].[State]
(
	[Id] CHAR(36) NOT NULL PRIMARY KEY, 
    [StateName] VARCHAR(100) NULL, 
    [StateCode] VARCHAR(3) NULL, 
    [CountryId] CHAR(36) NULL, 
    [OfflineStatus] BIT NULL, 
	[isUnionTerritory] BIT NULL, 
    [CreatedAt] DATETIME NOT NULL DEFAULT GetDate(), 
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(), 
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin'
)
