﻿CREATE TABLE [dbo].[ImportProcessActivity](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountId] [char](36) NOT NULL,
	[InstanceId] [char](36) NOT NULL,
	[FileName] [varchar](500) NOT NULL,
	[IsCloud] [int] NOT NULL,
	[ActivityDesc] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[CreatedAt] [datetime] NULL DEFAULT (getdate()),
	[CreatedBy] [char](36) NULL,
    CONSTRAINT [PK_ImportProcessActivity] PRIMARY KEY ([Id]),
     
)
