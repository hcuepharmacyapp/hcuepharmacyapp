﻿CREATE TABLE [dbo].[AlternateVendorProduct]
(
 [Id] CHAR(36) NOT NULL DEFAULT NEWID(),  
 [AccountId] CHAR(36) ,
 [InstanceId] CHAR(36) ,
 [ProductId] CHAR(36) NOT NULL,
 [VendorId] CHAR(36) NOT NULL,
 [AlternateProductName] VARCHAR(1000) NOT NULL,
 [AlternatePackageSize] VARCHAR(100)  NULL,
    [UpdatedPackageSize] INT NULL, 
 [OfflineStatus] BIT NULL DEFAULT 0,
 [CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_AlternateVendorProduct] PRIMARY KEY ([Id]), 
     
)