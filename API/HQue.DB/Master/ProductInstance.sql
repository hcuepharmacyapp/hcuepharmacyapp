﻿
Create table ProductInstance(
Id char(36) Primary Key,
[AccountId] char(36) NOT NULL,
InstanceId char(36) NOT NULL,
ProductId char(36),
ReOrderLevel decimal(18,2),
ReOrderQty decimal(18,2),
ReOrderModified BIT NULL,
RackNo varchar(50),
BoxNo varchar(15) NULL,
Ext_RefId Varchar(36) NULL,
OfflineStatus BIT NULL DEFAULT 0,
CreatedAt	datetime default getdate(),
UpdatedAt	datetime default getdate(),
CreatedBy	char(36),
UpdatedBy	char(36)
)