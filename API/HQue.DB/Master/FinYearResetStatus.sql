﻿CREATE TABLE [dbo].[FinYearResetStatus]
(
	[Id] CHAR(36) NOT NULL,
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) NULL ,
	[FinYearMasterId] CHAR(36),
	[FinancialYear] Datetime null,
	[ResetStatus] tinyint null,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
	CONSTRAINT [PK_FinYearResetStatus] PRIMARY KEY ([Id]) 
)
