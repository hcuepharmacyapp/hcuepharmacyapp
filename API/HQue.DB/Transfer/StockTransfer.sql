﻿CREATE TABLE [dbo].[StockTransfer]
(
	[Id] CHAR(36) NOT NULL ,
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[FromInstanceId] CHAR(36) NOT NULL ,
	[ToInstanceId] CHAR(36) NOT NULL ,
	[Prefix] Varchar(3) NULL,
	[TransferNo] VARCHAR(50) NOT NULL, 
    [TransferDate] DATETIME NOT NULL, 
	[FileName]	VARCHAR(500),
	[Comments]  VARCHAR(MAX),
	[TransferStatus]	TINYINT,
	[OfflineStatus] BIT NULL DEFAULT 0,
	TransferBy varchar(150) NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] VARCHAR(150) NULL DEFAULT 'admin', 
	FinyearId varchar(36) NULL,
	[TaxRefNo] tinyint NULL,
	[TransferItemCount] INT NULL,
    CONSTRAINT [PK_StockTransfer] PRIMARY KEY ([Id]) 
)
