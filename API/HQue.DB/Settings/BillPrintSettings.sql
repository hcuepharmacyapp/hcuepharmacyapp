﻿CREATE TABLE [dbo].[BillPrintSettings]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,   
	[Status] VARCHAR(50) NULL,	
	[FooterNote] VARCHAR(150) NULL,	
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	CONSTRAINT [PK_BillPrintSettings] PRIMARY KEY ([Id]), 
)