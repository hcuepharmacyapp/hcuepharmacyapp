﻿CREATE TABLE [dbo].[Settings]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[Enable_GlobalProdut] bit NULL ,
	[Pur_SortByLowestPrice] BIT NULL,
	[SalesPriceSetting] int null,
	[SaleBillToCSV] bit null,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin' 
)
