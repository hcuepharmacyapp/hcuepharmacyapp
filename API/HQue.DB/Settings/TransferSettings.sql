﻿CREATE TABLE [dbo].[TransferSettings]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL,
	[InstanceId] CHAR(36) NOT NULL,	
	[ProductBatchDisplayType] VARCHAR(5) NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	[TransferQtyType] BIT NULL DEFAULT 1, 
	[TransferZeroStock] BIT NULL DEFAULT 0,
    [ShowExpiredQty] INT NULL, 
    CONSTRAINT [PK_TransferSettings] PRIMARY KEY ([Id]), 
)
