﻿CREATE TABLE [dbo].[OrderSettings]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NULL ,
	[InstanceId] CHAR(36) ,   
	[NoOfDaysFromDate] int NULL,	
	[DateSettingType] tinyint NULL,	
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	CONSTRAINT [PK_OrderSettings] PRIMARY KEY ([Id]), 
)
