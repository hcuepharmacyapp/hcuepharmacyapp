﻿CREATE TABLE [dbo].[StockLedgerUpdateHistoryItem]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) NOT NULL ,
	[StockLedgerUpdateId] CHAR(36) NOT NULL, 
	[ProductId] CHAR(36) NOT NULL,
	[ProductStockId] CHAR(36) NULL, 
	[BatchNo] VARCHAR(100) NOT NULL, 
    [ExpireDate] DATE NOT NULL, 
	[CurrentStock] Decimal(18,6) DEFAULT 0,
	[ActualStock] Decimal(18,6) DEFAULT 0,
	[TransactionType] VARCHAR(50) NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_StockLedgerUpdateHistoryItem] PRIMARY KEY ([Id]) ,
)
