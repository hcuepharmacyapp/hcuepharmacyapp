﻿CREATE TABLE [dbo].[Tool]
(
	[Id] CHAR(36) NOT NULL, 
    [AccountId] CHAR(36) NULL, 
    [InstanceId] CHAR(36) NULL, 
	[ToolName] VARCHAR(50) NULL, 
	[Description] VARCHAR(250) NULL,
	[Image] VARCHAR(100) NULL,
	[Status] Int NULL DEFAULT 0,
	[OfflineStatus] BIT NULL DEFAULT 0,
    [CreatedAt] DATETIME NULL DEFAULT GetDate(), 
    [UpdatedAt] DATETIME NULL DEFAULT GetDate(), 
    [CreatedBy] CHAR(36) NULL DEFAULT 'admin', 
    [UpdatedBy] CHAR(36) NULL DEFAULT 'admin',
	  CONSTRAINT [PK_Tool] PRIMARY KEY ([Id]), 
)
