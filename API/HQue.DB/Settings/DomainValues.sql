﻿CREATE TABLE [dbo].[DomainValues]
(
	[Id] INT  NOT NULL ,
	DomainId varchar(36) Not null ,
DomainValue varchar(50)  Not null ,
DisplayText varchar(50),
HasSubdomain bit Null,
SubId tinyint null,
AccountId varchar(36),
InstanceId varchar(36),
CreatedAt datetime  default getdate(),
UpdatedAt datetime default getdate(),
CreatedBy Char(36) default 'admin',
UpdatedBy Char(36) default 'admin',
Constraint PK_DomainValues PRIMARY KEY(Id)
)
