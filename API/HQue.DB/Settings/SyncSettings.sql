﻿CREATE TABLE [dbo].[SyncSettings]
(
	[Id] Char(36) NOT NULL ,
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) , 
	IsProductSync bit NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	CONSTRAINT [PK_SyncSettings] PRIMARY KEY ([Id]), 
)
