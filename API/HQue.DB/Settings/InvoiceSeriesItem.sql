﻿CREATE TABLE [dbo].[InvoiceSeriesItem]
(
	[Id] CHAR(36) NOT NULL, 
    [AccountId] CHAR(36) NULL, 
    [InstanceId] CHAR(36) NULL, 
	[SeriesName] VARCHAR(5) NULL,
	[InvoiceseriesType] tinyint NULL, 
	[ActiveStatus] VARCHAR(5) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
    [CreatedAt] DATETIME NULL DEFAULT GetDate(), 
    [UpdatedAt] DATETIME NULL DEFAULT GetDate(), 
    [CreatedBy] CHAR(36) NULL DEFAULT 'admin', 
    [UpdatedBy] CHAR(36) NULL DEFAULT 'admin',
	  CONSTRAINT [PK_InvoiceSeriesItem] PRIMARY KEY ([Id]), 
)
