﻿CREATE TABLE [dbo].[InventorySettings]
(
	[Id] CHAR(36) NOT NULL PRIMARY KEY, 
    [AccountId] CHAR(36) NULL, 
    [InstanceId] CHAR(36) NULL, 
    [NoOfDays] INT NULL, 
	[NoOfMonth] INT NULL, 
	[PopupStartDate] DATE,
	[PopupEndDate] DATE,
	[PopupStartTime] VARCHAR(10),
	[PopupEndTime] VARCHAR(10),
	[PopupInterval] INT,
	[PopupEnabled] BIT,
	[LastPopupAt] DATETIME,
	[OpenedPopup] BIT,
	[OfflineStatus] BIT NULL DEFAULT 0,
    [CreatedAt] DATETIME NULL DEFAULT GetDate(), 
    [UpdatedAt] DATETIME NULL DEFAULT GetDate(), 
    [CreatedBy] CHAR(36) NULL DEFAULT 'admin', 
    [UpdatedBy] CHAR(36) NULL DEFAULT 'admin'
)
