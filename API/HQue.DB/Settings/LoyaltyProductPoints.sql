﻿CREATE TABLE [dbo].[LoyaltyProductPoints]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[LoyaltyId] CHAR(36) ,
	[KindName] VARCHAR(250) NULL,
	[KindOfProductPts] DECIMAL(18,6) NULL,
	[IsActive] BIT NULL DEFAULT 0,
	[OfflineStatus] BIT NULL DEFAULT 0,
   	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin'
    CONSTRAINT [PK_LoyaltyProductPoints] PRIMARY KEY ([Id]), 
)
