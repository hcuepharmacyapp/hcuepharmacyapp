﻿CREATE TABLE [dbo].[Requirement]
(
	[Id] CHAR(36) NOT NULL, 
    [AccountId] CHAR(36) NULL, 
    [InstanceId] CHAR(36) NULL, 
	[GatheredBy] VARCHAR(50) NOT NULL, 
	[RequirementNo] VARCHAR(50) NOT NULL, 
	[Description] VARCHAR(500) NOT NULL,
	[Type] SMALLINT NOT NULL,
	[SolvedBy] CHAR(36) NULL, 
	[SolvedDate] DATE null,
	[Remarks] VARCHAR(500) NULL,
	[Status] VARCHAR(50) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
    [CreatedAt] DATETIME NULL DEFAULT GetDate(), 
    [UpdatedAt] DATETIME NULL DEFAULT GetDate(), 
    [CreatedBy] CHAR(36) NULL DEFAULT 'admin', 
    [UpdatedBy] CHAR(36) NULL DEFAULT 'admin',
	  CONSTRAINT [PK_Requirement] PRIMARY KEY ([Id]), 
)
