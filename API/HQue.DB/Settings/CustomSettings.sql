﻿CREATE TABLE [dbo].[CustomSettings]
(
	[Id] CHAR(36) NOT NULL,  
	[GroupId] INT,
	[GroupName] VARCHAR(200),
	[CreatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_CustomSettings] PRIMARY KEY ([Id]) 
)