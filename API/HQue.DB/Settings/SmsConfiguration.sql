﻿CREATE TABLE [dbo].[SmsConfiguration]
(
	[Id] CHAR(36) NOT NULL, 
    [AccountId] CHAR(36) NULL, 
    [InstanceId] CHAR(36) NULL, 
	[Date] DATETIME NULL DEFAULT GetDate(), 	
	[SmsCount] BIGINT NULL ,
	[Remarks] VARCHAR(1000) NULL,
	[IsActive] BIT NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,	
    [CreatedAt] DATETIME NOT NULL DEFAULT GetDate(), 
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(), 
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	  CONSTRAINT [PK_SmsConfiguration] PRIMARY KEY ([Id]), 
)
