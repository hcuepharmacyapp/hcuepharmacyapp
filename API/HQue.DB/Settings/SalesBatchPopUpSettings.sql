﻿CREATE TABLE [dbo].[SalesBatchPopUpSettings]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,  
	[Quantity] BIT NULL,
	[Batch] BIT NULL, 
	[Expiry] BIT NULL,
	[Vat] BIT NULL,
	[GST] BIT NULL,
	[UnitMrp] BIT NULL,
	[StripMrp] BIT NULL,
	[UnitsPerStrip] BIT NULL,
	[Profit] BIT NULL,
	[RackNo] BIT NULL,
	[Generic] BIT NULL,
	[Mfg] BIT NULL,
	[Category] BIT NULL,
    [Age] BIT NULL,
	[Supplier] BIT NULL,
	[CreatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	CONSTRAINT [PK_SalesBatchPopUpSettings] PRIMARY KEY ([Id])
 
)
