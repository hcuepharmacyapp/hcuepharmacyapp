﻿CREATE TABLE [dbo].[BarcodePrnDesign]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36),
	[InstanceId] CHAR(36),
	[PrnName] VARCHAR(100),
	[PrnFileData] TEXT,
	[PrnFileName] VARCHAR(100),
	[PrnFieldJson] TEXT,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    CONSTRAINT [PK_BarcodePrnDesign] PRIMARY KEY ([Id])
)
