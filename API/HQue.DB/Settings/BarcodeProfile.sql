﻿CREATE TABLE [dbo].[BarcodeProfile]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36),
	[InstanceId] CHAR(36),
	[ProfileName] VARCHAR(100),
	[ProfileJson] TEXT,
	[IsDefaultProfile] BIT,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_BarcodeProfile] PRIMARY KEY ([Id]) 
)
