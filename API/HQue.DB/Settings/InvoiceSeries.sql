﻿CREATE TABLE [dbo].[InvoiceSeries]
(
	[Id] CHAR(36) NOT NULL, 
    [AccountId] CHAR(36) NULL, 
    [InstanceId] CHAR(36) NULL, 
	[InvoiceseriesType] tinyint NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
    [CreatedAt] DATETIME NULL DEFAULT GetDate(), 
    [UpdatedAt] DATETIME NULL DEFAULT GetDate(), 
    [CreatedBy] CHAR(36) NULL DEFAULT 'admin', 
    [UpdatedBy] CHAR(36) NULL DEFAULT 'admin',
	  CONSTRAINT [PK_InvoiceSeries] PRIMARY KEY ([Id]), 
)
