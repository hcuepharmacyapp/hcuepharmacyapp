﻿CREATE TABLE [dbo].[DomainMaster]
(
	[Id] INT NOT NULL  ,
	Code Varchar(7)  Not null ,
DisplayText varchar(50),
AccountId varchar(36),
InstanceId varchar(36),
ParentId varchar(36),
CreatedAt datetime default getdate(),
UpdatedAt datetime default getdate(),
CreatedBy Char(36) default 'admin',
UpdatedBy Char(36) default 'admin',
 CONSTRAINT [PK_DomainMaster] PRIMARY KEY ([Id]),
)
