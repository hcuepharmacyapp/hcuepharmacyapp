﻿CREATE TABLE [dbo].[TransactionOrder]
(
	[Id] INT NOT NULL IDENTITY(1,1),
	[TableName] VARCHAR(100),
	[LockQuery] VARCHAR(1000),
	CONSTRAINT [PK_TransactionOrder] PRIMARY KEY ([Id]), 
)
