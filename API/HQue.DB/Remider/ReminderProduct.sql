﻿CREATE TABLE [dbo].[ReminderProduct]
(
	[Id] CHAR(36) NOT NULL, 
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[UserReminderId] CHAR(36) ,
	[ProductStockId] CHAR(36),
	[ProductName] VARCHAR(1000) NULL, 
	[Quantity] DECIMAL NULL, 
	[ReminderFrequency] TINYINT NULL, 
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_ReminderProduct] PRIMARY KEY ([Id]), 
)
