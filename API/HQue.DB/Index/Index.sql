﻿--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_CommunicationLog_AccountIdInstanceIdCreatedAt' AND OBJECT_ID = OBJECT_ID('CommunicationLog'))
--DROP INDEX [XI_CommunicationLog_AccountIdInstanceIdCreatedAt] ON [CommunicationLog]
GO
CREATE NONCLUSTERED INDEX [XI_CommunicationLog_AccountIdInstanceIdCreatedAt]
ON [dbo].[CommunicationLog] ([AccountId],[InstanceId],[CreatedAt])
GO
--PRINT 1
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_InventoryOrder_ProductID' AND OBJECT_ID = OBJECT_ID('InventoryReOrder'))
--DROP INDEX [XI_InventoryOrder_ProductID] ON [InventoryReOrder]
GO
CREATE NONCLUSTERED INDEX [XI_InventoryOrder_ProductID]
ON [dbo].[InventoryReOrder] ([ProductId])
INCLUDE ([Id],[ReOrder],[Status])
GO
--PRINT 2
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_InventoryOrder_InstanceReorder' AND OBJECT_ID = OBJECT_ID('InventoryReOrder'))
--DROP INDEX [XI_InventoryOrder_InstanceReorder] ON [InventoryReOrder]
GO
CREATE NONCLUSTERED INDEX [XI_InventoryOrder_InstanceReorder]
ON [dbo].[InventoryReOrder] ([InstanceId],[ReOrder])
GO
--PRINT 3
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_ProductStock_AccountIdInstanceIDProductExpiryDate' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [XI_ProductStock_AccountIdInstanceIDProductExpiryDate] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [XI_ProductStock_AccountIdInstanceIDProductExpiryDate]
ON [dbo].[ProductStock] ([AccountId],[InstanceId],[ProductId],[ExpireDate],[Status])
INCLUDE ([VendorId],[Stock],[BatchNo])
GO
--PRINT 4
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_ProductStock_InstanceID' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [XI_ProductStock_InstanceID] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [XI_ProductStock_InstanceID]
ON [dbo].[ProductStock] ([InstanceId],[ProductId])
INCLUDE ([Stock])
GO	
--PRINT 5
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_ProductStock_InstanceIDExpiryDate' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [XI_ProductStock_InstanceIDExpiryDate] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [XI_ProductStock_InstanceIDExpiryDate]
ON [dbo].[ProductStock] ([InstanceId],[ExpireDate])
INCLUDE ([Id],[ProductId],[VendorId],[Stock])
GO
--PRINT 6
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_Sales_AccountIdInstanceIdInvoiceDate' AND OBJECT_ID = OBJECT_ID('Sales'))
--DROP INDEX [XI_Sales_AccountIdInstanceIdInvoiceDate] ON [Sales]
GO
CREATE NONCLUSTERED INDEX [XI_Sales_AccountIdInstanceIdInvoiceDate]
ON [dbo].[Sales] ([AccountId],[InstanceId],[InvoiceDate])
INCLUDE ([Id],[InvoiceNo],[Name],[Mobile],[Email],[PaymentType],[DeliveryType],[Discount],[CreatedBy])
GO
--PRINT 7
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_SalesItem_SalesID' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [XI_SalesItem_SalesID] ON [SalesItem]
GO
CREATE NONCLUSTERED INDEX [XI_SalesItem_SalesID]
ON [dbo].[SalesItem] ( [SalesId])
INCLUDE ([ProductStockId],[Quantity],[Discount],[SellingPrice])
GO
--PRINT 8
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_SalesItem_CreatedAt' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [XI_SalesItem_CreatedAt] ON [SalesItem]
GO
CREATE NONCLUSTERED INDEX [XI_SalesItem_CreatedAt]
ON [dbo].[SalesItem] (InstanceId,[CreatedAt])
INCLUDE ( [ProductStockId],[Quantity],[SellingPrice])
GO
--PRINT 9
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_SalesItem_CreatedAt_SalesProductStockId' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [XI_SalesItem_CreatedAt_SalesProductStockId] ON [SalesItem]
GO
CREATE NONCLUSTERED INDEX [XI_SalesItem_CreatedAt_SalesProductStockId]
ON [dbo].[SalesItem] (InstanceId,[CreatedAt])
INCLUDE ([SalesId],[ProductStockId])
GO
--PRINT 10
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_SalesItem_InstanceID' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [XI_SalesItem_InstanceID] ON [SalesItem]
GO
CREATE NONCLUSTERED INDEX [XI_SalesItem_InstanceID]
ON [dbo].[SalesItem] ([InstanceId],[ProductStockId])
INCLUDE ([CreatedAt],[SalesId],[Quantity])
GO
--PRINT 11
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_SalesReturnItem_SalesReturnId' AND OBJECT_ID = OBJECT_ID('SalesReturnItem'))
--DROP INDEX [XI_SalesReturnItem_SalesReturnId] ON [SalesReturnItem]
GO
CREATE NONCLUSTERED INDEX [XI_SalesReturnItem_SalesReturnId]
ON [dbo].[SalesReturnItem] (InstanceId,[SalesReturnId])
INCLUDE ([ProductStockId],[Quantity],[Discount],[MrpSellingPrice])
GO
--PRINT 12
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_VendorPurchaseItem_CreatedAt' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [XI_VendorPurchaseItem_CreatedAt] ON [VendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [XI_VendorPurchaseItem_CreatedAt]
ON [dbo].[VendorPurchaseItem] ([CreatedAt])
INCLUDE ([InstanceId],[VendorPurchaseId],[ProductStockId],[PackageQty],[PackagePurchasePrice],[Discount])
GO
--PRINT 13
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_VendorPurchaseItem_VendorPurchaseId' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [XI_VendorPurchaseItem_VendorPurchaseId] ON [VendorPurchaseItem]
GO
 CREATE NONCLUSTERED INDEX [XI_VendorPurchaseItem_VendorPurchaseId]
ON [dbo].[VendorPurchaseItem] ([VendorPurchaseId])
INCLUDE ([ProductStockId],[PackageQty],[PackagePurchasePrice],[PackageSellingPrice],[Quantity],[PurchasePrice],[FreeQty],[Discount])
GO
--PRINT 14
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_VendorPurchase_InvoiceDate' AND OBJECT_ID = OBJECT_ID('VendorPurchase'))
--DROP INDEX [XI_VendorPurchase_InvoiceDate] ON [VendorPurchase]
GO
CREATE NONCLUSTERED INDEX [XI_VendorPurchase_InvoiceDate]
ON [dbo].[VendorPurchase] ([InvoiceDate])
INCLUDE ([Id],[VendorId],[InvoiceNo],[GoodsRcvNo])
GO 
--PRINT 15
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_VendorPurchase_VendorIdInvoiceDate' AND OBJECT_ID = OBJECT_ID('VendorPurchase'))
--DROP INDEX [XI_VendorPurchase_VendorIdInvoiceDate] ON [VendorPurchase]
GO
CREATE NONCLUSTERED INDEX [XI_VendorPurchase_VendorIdInvoiceDate]
ON [dbo].[VendorPurchase] ([VendorId],[InvoiceDate])
INCLUDE ([Id],[InvoiceNo],[GoodsRcvNo])
 GO
 --PRINT 16
GO
 --IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_VendorPurchase_ProductStockId' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [XI_VendorPurchase_ProductStockId] ON [VendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [XI_VendorPurchase_ProductStockId]
ON [dbo].[VendorPurchaseItem] ([ProductStockId])
INCLUDE ([VendorPurchaseId],[PurchasePrice])
GO
--PRINT 17
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_Vendor_AccountIdInstanceId' AND OBJECT_ID = OBJECT_ID('Vendor'))
--DROP INDEX [XI_Vendor_AccountIdInstanceId] ON [Vendor]
GO
CREATE NONCLUSTERED INDEX [XI_Vendor_AccountIdInstanceId]
ON [dbo].[Vendor] ([AccountId],[InstanceId])
GO
--PRINT 18
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_Vendor_EmailAllowView' AND OBJECT_ID = OBJECT_ID('Vendor'))
--DROP INDEX [XI_Vendor_EmailAllowView] ON [Vendor]
GO
CREATE NONCLUSTERED INDEX [XI_Vendor_EmailAllowView]
ON [dbo].[Vendor] ([Email],[AllowViewStock])
GO
--PRINT 19
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_ProductStock_AccountIdInstanceIDProductCreatedAt' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [XI_ProductStock_AccountIdInstanceIDProductCreatedAt] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [XI_ProductStock_AccountIdInstanceIDProductCreatedAt]
ON [dbo].[ProductStock] ([AccountId],[InstanceId],[ProductId],[CreatedAt])
INCLUDE ([VendorId],[Stock])
GO
--PRINT 20
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='<Idx_ProductStock_AccountId' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [<Idx_ProductStock_AccountId] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [<Idx_ProductStock_AccountId]
ON [dbo].[ProductStock] ([AccountId],[InstanceId],[ExpireDate])
INCLUDE ([ProductId],[VendorId],[BatchNo],[Stock])
go
--PRINT 21
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Sales' AND OBJECT_ID = OBJECT_ID('Sales'))
--DROP INDEX [IDX_Sales] ON [Sales]
GO
CREATE NONCLUSTERED INDEX IDX_Sales
ON [dbo].[Sales] (InstanceId,[CreatedAt])
INCLUDE ([Id],[Discount])
GO
--PRINT 22
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VendorPurchase' AND OBJECT_ID = OBJECT_ID('VendorPurchase'))
--DROP INDEX [IDX_VendorPurchase] ON [VendorPurchase]
GO
 CREATE NONCLUSTERED INDEX [IDX_VendorPurchase]
ON [dbo].[VendorPurchase] ([CreatedAt])
INCLUDE ([Id],[Discount])
go
--PRINT 23
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_PO_AccountId' AND OBJECT_ID = OBJECT_ID('VendorPurchase'))
--DROP INDEX [IDX_PO_AccountId] ON [VendorPurchase]
GO
CREATE NONCLUSTERED INDEX [IDX_PO_AccountId]
ON [dbo].[VendorPurchase] ([AccountId],[InstanceId],[InvoiceDate])
INCLUDE ([Id],[VendorId],[InvoiceNo],[Discount],[CreatedAt])
GO
--PRINT 24
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_salename' AND OBJECT_ID = OBJECT_ID('Sales'))
--DROP INDEX [idx_salename] ON [Sales]
GO 
CREATE NONCLUSTERED INDEX [idx_salename]
ON [dbo].[Sales] ([Name],[Mobile])
INCLUDE ([PatientId])
GO
--PRINT 25
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductStock' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_ProductStock] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductStock]
ON [dbo].[ProductStock] ([InstanceId],[ProductId] )
INCLUDE ([Id],[VendorId],[BatchNo],[ExpireDate],[VAT],[SellingPrice],[Stock],[PurchasePrice],[CreatedAt])
GO 
--PRINT 26
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_StockAdjustment_Account' AND OBJECT_ID = OBJECT_ID('StockAdjustment'))
--DROP INDEX [IDX_StockAdjustment_Account] ON [StockAdjustment]
GO
CREATE NONCLUSTERED INDEX [IDX_StockAdjustment_Account]
ON [dbo].[StockAdjustment] ([AccountId],[InstanceId])
INCLUDE ([Id],[ProductStockId],[AdjustedStock],[AdjustedBy],[CreatedAt])
go
--PRINT 27
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='<IDX_VendorPOItem_Instance' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [<IDX_VendorPOItem_Instance] ON [VendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [<IDX_VendorPOItem_Instance]
ON [dbo].[VendorPurchaseItem] ([InstanceId])
INCLUDE ([VendorPurchaseId],[ProductStockId],[PurchasePrice])
go
--PRINT 28
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='Idx_Payment_Account' AND OBJECT_ID = OBJECT_ID('Payment'))
--DROP INDEX [Idx_Payment_Account] ON [Payment]
GO
CREATE NONCLUSTERED INDEX [Idx_Payment_Account]
ON [dbo].[Payment] ([AccountId],[InstanceId],[VendorPurchaseId])
INCLUDE ([VendorId],[Debit],[PaymentMode],[ChequeNo],[ChequeDate] ,[CreatedAt])
GO
--PRINT 29
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Vendorpurchase_VendorId' AND OBJECT_ID = OBJECT_ID('VendorPurchase'))
--DROP INDEX [IDX_Vendorpurchase_VendorId] ON [VendorPurchase]
GO
CREATE NONCLUSTERED INDEX [IDX_Vendorpurchase_VendorId]
ON [dbo].[VendorPurchase] ([VendorId])
INCLUDE ([Id])
GO
--PRINT 30
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_Payment_VendorPurchase' AND OBJECT_ID = OBJECT_ID('Payment'))
--DROP INDEX [idx_Payment_VendorPurchase] ON [Payment]
GO
CREATE NONCLUSTERED INDEX [idx_Payment_VendorPurchase]
ON [dbo].[Payment] ([VendorPurchaseId])
INCLUDE ([Debit])
go
--PRINT 31
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_Vendor_Instance' AND OBJECT_ID = OBJECT_ID('Vendor'))
--DROP INDEX [idx_Vendor_Instance] ON [Vendor]
GO
CREATE NONCLUSTERED INDEX [idx_Vendor_Instance]
ON [dbo].[Vendor] ([InstanceId])
GO
--PRINT 32
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_DOCTOR_ACCT' AND OBJECT_ID = OBJECT_ID('Doctor'))
--DROP INDEX [IDX_DOCTOR_ACCT] ON [Doctor]
GO
CREATE NONCLUSTERED INDEX [IDX_DOCTOR_ACCT]
ON [dbo].[Doctor] ([AccountId],[InstanceId])
INCLUDE ([Name],[Mobile])
GO
--PRINT 33
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_saleitem_Accountid' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [idx_saleitem_Accountid] ON [SalesItem]
GO 
CREATE NONCLUSTERED INDEX [idx_saleitem_Accountid]
ON [dbo].[SalesItem] ([AccountId],[InstanceId],[CreatedAt])
INCLUDE ([SalesId],[ProductStockId],[Quantity])
GO
--PRINT 34
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Productstock_Product' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_Productstock_Product] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [IDX_Productstock_Product]
ON [dbo].[ProductStock] ([ProductId],[CreatedAt])
INCLUDE ([VendorId],[VAT],[PackageSize],[PurchasePrice])
go
--PRINT 35
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductInstance_InstanceId' AND OBJECT_ID = OBJECT_ID('ProductInstance'))
--DROP INDEX [IDX_ProductInstance_InstanceId] ON [ProductInstance]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductInstance_InstanceId]
ON [dbo].[ProductInstance] ([AccountID],[InstanceId],[ProductId])
INCLUDE ([Id],[RackNo])
go
--PRINT 36
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductAcct' AND OBJECT_ID = OBJECT_ID('Product'))
--DROP INDEX [IDX_ProductAcct] ON [Product]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductAcct]
ON [dbo].[Product] ([AccountId],[Name])
GO
--PRINT 37
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Vendorpurchase_Instance' AND OBJECT_ID = OBJECT_ID('VendorPurchase'))
--DROP INDEX [IDX_Vendorpurchase_Instance] ON [VendorPurchase]
GO  
CREATE NONCLUSTERED INDEX [IDX_Vendorpurchase_Instance]
ON [dbo].[VendorPurchase] ([AccountId],[InstanceId],[InvoiceNo])
INCLUDE ([Id],[VendorId],[InvoiceDate],[Discount],[Credit],[PaymentType],[ChequeNo],[ChequeDate],[CreatedAt],[GoodsRcvNo],[Comments],[FileName],[CreditNoOfDays],[NoteType],[NoteAmount],[CancelStatus])
GO
--PRINT 38
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VendorPurchaseItem_Account' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [IDX_VendorPurchaseItem_Account] ON [VendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [IDX_VendorPurchaseItem_Account]
ON [dbo].[VendorPurchaseItem] ([AccountId],[InstanceId])
INCLUDE ([VendorPurchaseId],[ProductStockId])
go
--PRINT 39
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductStock_InstanceExpire' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_ProductStock_InstanceExpire] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductStock_InstanceExpire]
ON [dbo].[ProductStock] ([ExpireDate],[Stock],[Status],[InstanceId],[ProductId])
GO
--PRINT 40
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='Idx_ProductStockAccount' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [Idx_ProductStockAccount] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [Idx_ProductStockAccount]
ON [dbo].[ProductStock] ([AccountId],[InstanceId],[Status])
INCLUDE ([Id],[ProductId],[VendorId],[BatchNo],[ExpireDate],[Stock])
GO
--PRINT 41
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_SyncData_InstanceID' AND OBJECT_ID = OBJECT_ID('SyncData'))
--DROP INDEX [XI_SyncData_InstanceID] ON [SyncData]
GO
CREATE NONCLUSTERED INDEX [XI_SyncData_InstanceID]
ON [dbo].[SyncData] ([InstanceId],[Id])
GO
--PRINT 42
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='Idx_Saleitem_ProductStock' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [Idx_Saleitem_ProductStock] ON [SalesItem]
GO 
CREATE NONCLUSTERED INDEX [Idx_Saleitem_ProductStock]
ON [dbo].[SalesItem] ([ProductStockId])
GO
--PRINT 43
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductStock_InstanceID' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_ProductStock_InstanceID] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductStock_InstanceID]
ON [dbo].[ProductStock] ([InstanceId],[Stock],[UpdatedAt])
INCLUDE ([Id],[ProductId],[VendorId],[BatchNo],[ExpireDate],[VAT],[SellingPrice],[CreatedAt])
GO
--PRINT 44
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VendorPOItem' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [IDX_VendorPOItem] ON [VendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [IDX_VendorPOItem]
ON [dbo].[VendorPurchaseItem] ([AccountId],[InstanceId],[Status])
INCLUDE ([VendorPurchaseId],[ProductStockId],[PackageSize],[PackageQty],[PackagePurchasePrice],[PackageSellingPrice],[PackageMRP],[Quantity],[PurchasePrice],[CreatedAt],[FreeQty],[Discount])
GO
--PRINT 45
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Sales_AccountId' AND OBJECT_ID = OBJECT_ID('Sales'))
--DROP INDEX [IDX_Sales_AccountId] ON [Sales]
GO
CREATE NONCLUSTERED INDEX [IDX_Sales_AccountId]
ON [dbo].[Sales] ([AccountId],[InstanceId] )
INCLUDE ([Cancelstatus],[InvoiceDate],[NetAmount])
go
--PRINT 46
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Leads_Account' AND OBJECT_ID = OBJECT_ID('Leads'))
--DROP INDEX [IDX_Leads_Account] ON [Leads]
GO
CREATE NONCLUSTERED INDEX [IDX_Leads_Account]
ON [dbo].[Leads] ([AccountId],[InstanceId],[LeadStatus])
INCLUDE ([LeadDate])
GO
--PRINT 47
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Sale_Createdat' AND OBJECT_ID = OBJECT_ID('Sales'))
--DROP INDEX [IDX_Sale_Createdat] ON [Sales]
GO 
CREATE NONCLUSTERED INDEX [IDX_Sale_Createdat]
ON [dbo].[Sales] ([CreatedAt], [AccountId],[InstanceId])
GO
--PRINT 48
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Sales_InvDt' AND OBJECT_ID = OBJECT_ID('Sales'))
--DROP INDEX [IDX_Sales_InvDt] ON [Sales]
GO 
CREATE NONCLUSTERED INDEX IDX_Sales_InvDt
ON [dbo].[Sales] ( InvoiceDate)
INCLUDE ([Id] )
go
--PRINT 49
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VendorPOItem_PurchaseId' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [IDX_VendorPOItem_PurchaseId] ON [VendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [IDX_VendorPOItem_PurchaseId]
ON [dbo].[VendorPurchaseItem] ([ProductStockId],[VendorPurchaseId],[Status])
INCLUDE ([PurchasePrice])
GO
--PRINT 50
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductStock_Status' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_ProductStock_Status] ON [ProductStock]
GO  
CREATE NONCLUSTERED INDEX [IDX_ProductStock_Status]
ON [dbo].[ProductStock] ([Stock],[Status])
INCLUDE ([Id],[ProductId])
GO
--PRINT 51
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SalesReturn_Instance' AND OBJECT_ID = OBJECT_ID('SalesReturn'))
--DROP INDEX [IDX_SalesReturn_Instance] ON [SalesReturn]
GO
CREATE NONCLUSTERED INDEX [IDX_SalesReturn_Instance]
ON [dbo].[SalesReturn] ([AccountId],InstanceId,[ReturnDate])
GO
--PRINT 52
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SalesItem_AccountId' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [IDX_SalesItem_AccountId] ON [SalesItem]
GO 
CREATE NONCLUSTERED INDEX [IDX_SalesItem_AccountId]
ON [dbo].[SalesItem] ([AccountId],InstanceId, [SalesId])
INCLUDE ([ProductStockId],[Quantity],[Discount],[SellingPrice])
GO
--PRINT 53
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Sales_Account_Invoice' AND OBJECT_ID = OBJECT_ID('Sales'))
--DROP INDEX [IDX_Sales_Account_Invoice] ON [Sales]
GO 
CREATE NONCLUSTERED INDEX [IDX_Sales_Account_Invoice]
ON [dbo].[Sales] ([AccountId],[InstanceId],[InvoiceSeries],[Cancelstatus],[InvoiceDate])
INCLUDE ([Id],[InvoiceNo],[Name],[Discount],[DiscountValue],[CreatedAt],[SalesType])
GO
--PRINT 54
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Payment_Account_Debit' AND OBJECT_ID = OBJECT_ID('Payment'))
--DROP INDEX [IDX_Payment_Account_Debit] ON [Payment]
GO
CREATE NONCLUSTERED INDEX [IDX_Payment_Account_Debit]
ON [dbo].[Payment] ([AccountId],[InstanceId],[Debit])
INCLUDE ([VendorId],[PaymentMode],[BankName],[BankBranchName],[IfscCode],[ChequeNo],[ChequeDate],[VendorPurchaseId],[CreatedAt])
GO
--PRINT 55
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_Patient_AccountIdInstanceIdName' AND OBJECT_ID = OBJECT_ID('Patient'))
--DROP INDEX [XI_Patient_AccountIdInstanceIdName] ON [Patient]
GO
CREATE NONCLUSTERED INDEX [XI_Patient_AccountIdInstanceIdName]
ON [dbo].[Patient] ([AccountId],[InstanceId],[Name])
INCLUDE ([Id],[ShortName],[Mobile],[Email],[DOB],[Age],[Gender],[Address],[Area],[City],[State],[Pincode],[EmpID],[Discount],[PatientType])
GO
--PRINT 56
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_Patient_AccountIdInstanceIdMobile' AND OBJECT_ID = OBJECT_ID('Patient'))
--DROP INDEX [XI_Patient_AccountIdInstanceIdMobile] ON [Patient]
GO
CREATE NONCLUSTERED INDEX [XI_Patient_AccountIdInstanceIdMobile]
ON [dbo].[Patient] ([AccountId],[InstanceId],[Mobile])
INCLUDE ([Id],[ShortName],[Name],[Email],[DOB],[Age],[Gender],[Address],[Area],[City],[State],[Pincode],[EmpID],[Discount],[PatientType])
GO
--PRINT 57
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_Patient_AccountIdInstanceIdEmpID' AND OBJECT_ID = OBJECT_ID('Patient'))
--DROP INDEX [XI_Patient_AccountIdInstanceIdEmpID] ON [Patient]
GO
CREATE NONCLUSTERED INDEX [XI_Patient_AccountIdInstanceIdEmpID]
ON [dbo].[Patient] ([AccountId],[InstanceId],[EmpID])
INCLUDE ([Id],[ShortName],[Name],[Mobile],[Email],[DOB],[Age],[Gender],[Address],[Area],[City],[State],[Pincode],[Discount],[PatientType])
GO
--PRINT 58
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductStock_InstanceExpireStock' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_ProductStock_InstanceExpireStock] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductStock_InstanceExpireStock]
ON [dbo].[ProductStock] ([InstanceId],[ExpireDate],[Stock])
INCLUDE ([ProductId],[Status])
GO
--PRINT 59
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductInstance_Instance' AND OBJECT_ID = OBJECT_ID('ProductInstance'))
--DROP INDEX [IDX_ProductInstance_Instance] ON [ProductInstance]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductInstance_Instance]
ON [dbo].[ProductInstance] ([InstanceId],[ProductId])
GO
--PRINT 60
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_TempVendorPurchaseItem_Account' AND OBJECT_ID = OBJECT_ID('TempVendorPurchaseItem'))
--DROP INDEX [IDX_TempVendorPurchaseItem_Account] ON [TempVendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [IDX_TempVendorPurchaseItem_Account]
ON [dbo].[TempVendorPurchaseItem] ([AccountId],[InstanceId],[ProductStockId])
GO
--PRINT 61
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_Doctor_AccountIdInstanceIdName' AND OBJECT_ID = OBJECT_ID('Doctor'))
--DROP INDEX [XI_Doctor_AccountIdInstanceIdName] ON [Doctor]
GO
CREATE NONCLUSTERED INDEX [XI_Doctor_AccountIdInstanceIdName]
ON [dbo].[Doctor] ([AccountId],[InstanceId],[Name])
WHERE AccountId IS NOT NULL
GO
--PRINT 62
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_Doctor_AccountIdInstanceIdMobile' AND OBJECT_ID = OBJECT_ID('Doctor'))
--DROP INDEX [XI_Doctor_AccountIdInstanceIdMobile] ON [Doctor]
GO
CREATE NONCLUSTERED INDEX [XI_Doctor_AccountIdInstanceIdMobile]
ON [dbo].[Doctor] ([AccountId],[InstanceId],[Mobile])
WHERE AccountId IS NOT NULL
GO
--PRINT 63
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductAcctStatus' AND OBJECT_ID = OBJECT_ID('Product'))
--DROP INDEX [IDX_ProductAcctStatus] ON [Product]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductAcctStatus]
ON [dbo].[Product] ([AccountId],[Status])
INCLUDE ([Id],[Name],[Manufacturer],[Category],[GenericName])
GO
--PRINT 64
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductStock_Stock' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_ProductStock_Stock] ON [ProductStock]
GO  
CREATE NONCLUSTERED INDEX IDX_ProductStock_Stock
ON [dbo].[ProductStock] ([Stock])
INCLUDE ([Id],[InstanceId],[ProductId],[VendorId],[BatchNo],[ExpireDate],[ReOrderQty],[Status])
GO
--PRINT 65
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductMaster_GenericName' AND OBJECT_ID = OBJECT_ID('ProductMaster'))
--DROP INDEX [IDX_ProductMaster_GenericName] ON [ProductMaster]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductMaster_GenericName]
ON [dbo].[ProductMaster] ([GenericName],[Name])
GO
--PRINT 66
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductMaster_UpdatedAt' AND OBJECT_ID = OBJECT_ID('ProductMaster'))
--DROP INDEX [IDX_ProductMaster_UpdatedAt] ON [ProductMaster]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductMaster_UpdatedAt]
ON [dbo].[ProductMaster] ([UpdatedAt])
GO
--PRINT 67
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_PDTSTK_AcctID' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_PDTSTK_AcctID] ON [ProductStock]
GO 
CREATE NONCLUSTERED INDEX [IDX_PDTSTK_AcctID]
ON [dbo].[ProductStock] ([AccountId],[InstanceId],[StockImport],[CreatedAt])
INCLUDE ([ProductId],[NewOpenedStock],[ImportQty])
GO
--PRINT 68
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SI_AcctID' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [IDX_SI_AcctID] ON [SalesItem]
GO
CREATE NONCLUSTERED INDEX [IDX_SI_AcctID]
ON [dbo].[SalesItem] ([AccountId],[InstanceId],[UpdatedAt])
INCLUDE ([SalesId],[ProductStockId],[Quantity])
GO
--PRINT 69
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_STK_Transfer' AND OBJECT_ID = OBJECT_ID('StockTransferItem'))
--DROP INDEX [IDX_STK_Transfer] ON [StockTransferItem]
GO
CREATE NONCLUSTERED INDEX [IDX_STK_Transfer]
ON [dbo].[StockTransferItem] ([AccountId],[InstanceId],[UpdatedAt])
INCLUDE ([TransferId],[ProductStockId],[Quantity])
GO
--PRINT 70
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_STK_Toinstance' AND OBJECT_ID = OBJECT_ID('StockTransferItem'))
--DROP INDEX [IDX_STK_Toinstance] ON [StockTransferItem]
GO
CREATE NONCLUSTERED INDEX [IDX_STK_Toinstance]
ON [dbo].[StockTransferItem] ([AccountId],[TransferId],[ToInstanceId],[UpdatedAt])
INCLUDE ([ProductStockId],[Quantity])
GO
--PRINT 71
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_stk_transferitem_toproduct' AND OBJECT_ID = OBJECT_ID('StockTransferItem'))
--DROP INDEX [idx_stk_transferitem_toproduct] ON [StockTransferItem]
GO
CREATE NONCLUSTERED INDEX [idx_stk_transferitem_toproduct]
ON [dbo].[StockTransferItem] ([ToInstanceId],[UpdatedAt])
INCLUDE ([TransferId],[Quantity],[ToProductStockId])
GO
--PRINT 72
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_StockTransferItem_instance' AND OBJECT_ID = OBJECT_ID('StockTransferItem'))
--DROP INDEX [idx_StockTransferItem_instance] ON [StockTransferItem]
GO
CREATE NONCLUSTERED INDEX idx_StockTransferItem_instance
ON [dbo].[StockTransferItem] ([InstanceId],[UpdatedAt])
INCLUDE ([TransferId],[ProductStockId],[Quantity])
GO
--PRINT 73
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_STK_Transfer_ToInstance' AND OBJECT_ID = OBJECT_ID('StockTransferItem'))
--DROP INDEX [IDX_STK_Transfer_ToInstance] ON [StockTransferItem]
GO
CREATE NONCLUSTERED INDEX [IDX_STK_Transfer_ToInstance]
ON [dbo].[StockTransferItem] ([ToProductStockId],[ToInstanceId],[UpdatedAt])
INCLUDE ([TransferId],[Quantity])
GO
--PRINT 74
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_STKADJ_Movement' AND OBJECT_ID = OBJECT_ID('StockAdjustment'))
--DROP INDEX [IDX_STKADJ_Movement] ON [StockAdjustment]
GO 
CREATE NONCLUSTERED INDEX [IDX_STKADJ_Movement]
ON [dbo].[StockAdjustment] ([InstanceId],[AdjustedStock],[UpdatedAt] )
INCLUDE ([ProductStockId])
GO
--PRINT 75
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VendorReturn_Movement' AND OBJECT_ID = OBJECT_ID('VendorReturnItem'))
--DROP INDEX [IDX_VendorReturn_Movement] ON [VendorReturnItem]
GO
CREATE NONCLUSTERED INDEX [IDX_VendorReturn_Movement]
ON [dbo].[VendorReturnItem] ([InstanceId],[UpdatedAt])
GO
--PRINT 76
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Vendor_Movement' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [IDX_Vendor_Movement] ON [VendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [IDX_Vendor_Movement]
ON [dbo].[vendorpurchaseitem] ([InstanceId],[UpdatedAt])
GO
--PRINT 77
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Sales_Movement' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [IDX_Sales_Movement] ON [SalesItem]
GO
CREATE NONCLUSTERED INDEX [IDX_Sales_Movement]
ON [dbo].[SalesItem] ([InstanceId],[UpdatedAt])
GO
--PRINT 78
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SalesReturn_Movement' AND OBJECT_ID = OBJECT_ID('SalesReturnItem'))
--DROP INDEX [IDX_SalesReturn_Movement] ON [SalesReturnItem]
GO
CREATE NONCLUSTERED INDEX [IDX_SalesReturn_Movement]
ON [dbo].[SalesReturnItem] ([InstanceId],[UpdatedAt])
go
--PRINT 79
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Temp_Movement' AND OBJECT_ID = OBJECT_ID('TempVendorPurchaseItem'))
--DROP INDEX [IDX_Temp_Movement] ON [TempVendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [IDX_Temp_Movement]
ON [dbo].[TempVendorPurchaseItem] ([InstanceId],[ProductStockId],[isActive],[UpdatedAt])
GO
--PRINT 80
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_DC_Movement' AND OBJECT_ID = OBJECT_ID('DCVendorPurchaseItem'))
--DROP INDEX [IDX_DC_Movement] ON [DCVendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [IDX_DC_Movement]
ON [dbo].[DCVendorPurchaseItem] ([InstanceId],[ProductStockId],[isActive],[UpdatedAt])
GO
--PRINT 81
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VendorItem_StockMovement' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [IDX_VendorItem_StockMovement] ON [VendorPurchaseItem]
GO
CREATE NONCLUSTERED INDEX [IDX_VendorItem_StockMovement]
ON [dbo].[VendorPurchaseItem] ([InstanceId],[UpdatedAt])
INCLUDE ([ProductStockId],[Quantity])
GO
--PRINT 82
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VEndorOrderItem_Account' AND OBJECT_ID = OBJECT_ID('VendorOrderItem'))
--DROP INDEX [IDX_VEndorOrderItem_Account] ON [VendorOrderItem]
GO 
CREATE NONCLUSTERED INDEX [IDX_VEndorOrderItem_Account]
ON [dbo].[VendorOrderItem] ([AccountId],[InstanceId],[VendorPurchaseId],[StockTransferId])
GO
--PRINT 83
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Selfcons_Movement' AND OBJECT_ID = OBJECT_ID('SelfConsumption'))
--DROP INDEX [IDX_Selfcons_Movement] ON [SelfConsumption]
GO
CREATE NONCLUSTERED INDEX [IDX_Selfcons_Movement]
ON [dbo].[SelfConsumption] ([InstanceId],[ProductStockId],[UpdatedAt])
go
--PRINT 84
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductStock_MOvement' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_ProductStock_MOvement] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductStock_MOvement]
ON [dbo].[ProductStock] ([InstanceId],[UpdatedAt])
INCLUDE ([ProductId],[Stock],[NewOpenedStock],[NewStockQty],[StockImport],[ImportQty])
go
--PRINT 85
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_STKADJ_Movement1' AND OBJECT_ID = OBJECT_ID('StockAdjustment'))
--DROP INDEX [IDX_STKADJ_Movement1] ON [StockAdjustment]
GO 
CREATE NONCLUSTERED   INDEX [IDX_STKADJ_Movement1]
ON [dbo].[StockAdjustment] ([InstanceId],[ProductStockId],[AdjustedStock],[UpdatedAt])
GO
--PRINT 86
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SaleItem1_Movement' AND OBJECT_ID = OBJECT_ID('SalesItem'))
--DROP INDEX [IDX_SaleItem1_Movement] ON [SalesItem]
GO
CREATE NONCLUSTERED INDEX [IDX_SaleItem1_Movement]
ON [dbo].[SalesItem] ([InstanceId],[UpdatedAt])
INCLUDE ([ProductStockId],[Quantity])
go
--PRINT 87
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='Idx_pdt_stock_vat' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [Idx_pdt_stock_vat] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [Idx_pdt_stock_vat]
ON [dbo].[ProductStock] ([InstanceId])
INCLUDE ([Id],[ProductId],[BatchNo],[ExpireDate],[VAT],[SellingPrice],[GstTotal])
GO
--PRINT 88
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_Prodcutstock_CreatedAt' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [idx_Prodcutstock_CreatedAt] ON [ProductStock]
GO 
CREATE NONCLUSTERED INDEX [idx_Prodcutstock_CreatedAt]
ON [dbo].[ProductStock] ([AccountId],[InstanceId],[CreatedAt])
INCLUDE ([PurchasePrice],[NewOpenedStock],[StockImport],[ImportQty])
GO
--PRINT 89
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SalesReturnDate' AND OBJECT_ID = OBJECT_ID('SalesReturn'))
--DROP INDEX [IDX_SalesReturnDate] ON [SalesReturn]
GO 
CREATE NONCLUSTERED INDEX [IDX_SalesReturnDate]
ON [dbo].[SalesReturn] ([AccountId],[ReturnDate])
GO
--PRINT 90
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='Idx_StockTransferItem_price' AND OBJECT_ID = OBJECT_ID('StockTransferItem'))
--DROP INDEX [Idx_StockTransferItem_price] ON [StockTransferItem]
GO 
CREATE NONCLUSTERED INDEX [Idx_StockTransferItem_price]
ON [dbo].[StockTransferItem] ([InstanceId])
INCLUDE ([TransferId],[Quantity],[PurchasePrice],[GstTotal])
GO
--PRINT 91
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_StockTransfer_Instance' AND OBJECT_ID = OBJECT_ID('StockTransfer'))
--DROP INDEX [IDX_StockTransfer_Instance] ON [StockTransfer]
GO 
CREATE NONCLUSTERED INDEX [IDX_StockTransfer_Instance]
ON [dbo].[StockTransfer] ([InstanceId],[TransferDate],[TransferStatus])
GO
--PRINT 92
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VendorPurchaseItem_Status' AND OBJECT_ID = OBJECT_ID('VendorPurchaseItem'))
--DROP INDEX [IDX_VendorPurchaseItem_Status] ON [VendorPurchaseItem]
GO 
CREATE NONCLUSTERED INDEX [IDX_VendorPurchaseItem_Status]
ON [dbo].[VendorPurchaseItem] ([AccountId],[InstanceId],[Status])
INCLUDE ([VendorPurchaseId],[ProductStockId],[PackageQty],[PackagePurchasePrice],[FreeQty],[Discount],[GstTotal])
GO
--PRINT 93
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SalesReturnItem_Instance' AND OBJECT_ID = OBJECT_ID('SalesReturnItem'))
--DROP INDEX [IDX_SalesReturnItem_Instance] ON [SalesReturnItem]
GO 
CREATE NONCLUSTERED INDEX [IDX_SalesReturnItem_Instance]
ON [dbo].[SalesReturnItem] ([InstanceId])
INCLUDE ([SalesReturnId],[ProductStockId],[Quantity],[Discount],[MrpSellingPrice],[IsDeleted],[GstTotal])
GO
--PRINT 94
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_StockTransferItem_ToInstance' AND OBJECT_ID = OBJECT_ID('StockTransferItem'))
--DROP INDEX [IDX_StockTransferItem_ToInstance] ON [StockTransferItem]
GO
CREATE NONCLUSTERED INDEX [IDX_StockTransferItem_ToInstance]
ON [dbo].[StockTransferItem] ([ToInstanceId])
INCLUDE ([TransferId],[Quantity],[PurchasePrice],[GstTotal])
GO
--PRINT 95
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_TempVendorPurchaseItem_Instance' AND OBJECT_ID = OBJECT_ID('TempVendorPurchaseItem'))
--DROP INDEX [IDX_TempVendorPurchaseItem_Instance] ON [TempVendorPurchaseItem]
GO  
CREATE NONCLUSTERED INDEX [IDX_TempVendorPurchaseItem_Instance]
ON [dbo].[TempVendorPurchaseItem] ([InstanceId],[isActive],[UpdatedAt])
INCLUDE ([ProductStockId],[Quantity],[IsDeleted])
GO
--PRINT 96
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='Idx_customerPay' AND OBJECT_ID = OBJECT_ID('CustomerPayment'))
--DROP INDEX [Idx_customerPay] ON [CustomerPayment]
GO 
CREATE NONCLUSTERED INDEX [Idx_customerPay]
ON [dbo].[CustomerPayment] ([AccountId])
INCLUDE ([InstanceId],[SalesId],[Credit])
GO
--PRINT 97
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SAlesReturnItem' AND OBJECT_ID = OBJECT_ID('SalesReturnItem'))
--DROP INDEX [IDX_SAlesReturnItem] ON [SalesReturnItem]
GO
CREATE NONCLUSTERED INDEX [IDX_SAlesReturnItem]
ON [dbo].[SalesReturnItem] ([SalesReturnId])
INCLUDE ([ProductStockId],[Quantity],[Discount],[MrpSellingPrice])
GO
--PRINT 98
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_HqueUer_Account' AND OBJECT_ID = OBJECT_ID('HQueUser'))
--DROP INDEX [IDX_HqueUer_Account] ON [HQueUser]
GO
CREATE NONCLUSTERED INDEX [IDX_HqueUer_Account]
ON [dbo].[HQueUser] ([AccountId],[InstanceId],[UserId])
go
--PRINT 99
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_Instance' AND OBJECT_ID = OBJECT_ID('Instance'))
--DROP INDEX [IDX_Instance] ON [Instance]
GO
CREATE NONCLUSTERED INDEX [IDX_Instance]
ON [dbo].[Instance] ([AccountId])
GO
--PRINT 100
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VendorReturnItem' AND OBJECT_ID = OBJECT_ID('VendorReturnItem'))
--DROP INDEX [IDX_VendorReturnItem] ON [VendorReturnItem]
GO
CREATE NONCLUSTERED INDEX [IDX_VendorReturnItem]
ON [dbo].[VendorReturnItem] ([AccountId])
INCLUDE ([InstanceId],[VendorReturnId],[ProductStockId],[Quantity],[ReturnPurchasePrice])
go
--PRINT 101
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='Idx_VendorPurchase_Instance1' AND OBJECT_ID = OBJECT_ID('VendorPurchase'))
--DROP INDEX [Idx_VendorPurchase_Instance1] ON [VendorPurchase]
GO 
CREATE NONCLUSTERED INDEX [Idx_VendorPurchase_Instance1]
ON [dbo].[VendorPurchase] ([InstanceId])
INCLUDE ([CancelStatus])
GO
--PRINT 102
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_productstockinsid' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_productstockinsid] ON [ProductStock]
GO 
CREATE NONCLUSTERED INDEX [IDX_productstockinsid]
ON [dbo].[ProductStock] ([InstanceId],[Stock])
INCLUDE ([ProductId],[VAT],[SellingPrice],[PackageSize],[PackagePurchasePrice],[PurchasePrice],[CST],[GstTotal],[TaxRefNo])
GO
--PRINT 103
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_Closing_Transdate' AND OBJECT_ID = OBJECT_ID('ClosingStock'))
--DROP INDEX [idx_Closing_Transdate] ON [ClosingStock]
GO
CREATE NONCLUSTERED INDEX [idx_Closing_Transdate]
ON [dbo].[ClosingStock] ([InstanceId],[TransDate])
INCLUDE ([AccountId] ,[ProductStockId],[ClosingStock],[StockValue],[GstTotal],[IsOffline],[IsSynctoOnline],[CreatedAt],[CreatedBy])
GO
--PRINT 104
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='Idx_Productstock_Updtat' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [Idx_Productstock_Updtat] ON [ProductStock]
GO 
CREATE NONCLUSTERED INDEX [Idx_Productstock_Updtat]
ON [dbo].[ProductStock] ([InstanceId],[UpdatedAt])
INCLUDE ([VAT],[Stock],[PurchasePrice],[GstTotal],[TaxRefNo])
GO
--PRINT 105
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_VoucherCustomerPayment' AND OBJECT_ID = OBJECT_ID('Voucher'))
--DROP INDEX [IDX_VoucherCustomerPayment] ON [Voucher]
GO
CREATE NONCLUSTERED INDEX [IDX_VoucherCustomerPayment]
ON [dbo].[Voucher] ([InstanceId],[TransactionType],[Amount])
GO
--PRINT 106
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_CustomerReceipt' AND OBJECT_ID = OBJECT_ID('CustomerPayment'))
--DROP INDEX [IDX_CustomerReceipt] ON [CustomerPayment]
GO
CREATE NONCLUSTERED INDEX [IDX_CustomerReceipt]
ON [dbo].[CustomerPayment] ([SalesId],[AccountId],[InstanceId])
INCLUDE ([Credit],[Debit])
GO
--PRINT 107
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_InstanceGST' AND OBJECT_ID = OBJECT_ID('Instance'))
--DROP INDEX [IDX_InstanceGST] ON [Instance]
GO 
CREATE NONCLUSTERED INDEX [IDX_InstanceGST]
ON [dbo].[Instance] ([GsTinNo])
GO
--PRINT 108
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductAccountId' AND OBJECT_ID = OBJECT_ID('Product'))
--DROP INDEX [IDX_ProductAccountId] ON [Product]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductAccountId]
ON [dbo].[Product] ([AccountId])
GO
--PRINT 109
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductProductStockcount' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_ProductProductStockcount] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductProductStockcount]
ON [dbo].[ProductStock] ([productId],[GstTotal])
GO 
--PRINT 110
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductProductStockcountGSTNotNull' AND OBJECT_ID = OBJECT_ID('ProductStock'))
--DROP INDEX [IDX_ProductProductStockcountGSTNotNull] ON [ProductStock]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductProductStockcountGSTNotNull]
ON [dbo].[ProductStock] ([productId])
Where GstTotal IS NOT NULL
GO 
--PRINT 111
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductGSTNull' AND OBJECT_ID = OBJECT_ID('Product'))
--DROP INDEX [IDX_ProductGSTNull] ON [Product]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductGSTNull]
ON [dbo].[Product] ([Id])
Where GstTotal IS NULL
GO
--PRINT 112
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_VendorReturnDate' AND OBJECT_ID = OBJECT_ID('VendorReturn'))
--DROP INDEX [idx_VendorReturnDate] ON [VendorReturn]
GO
CREATE NONCLUSTERED INDEX [idx_VendorReturnDate]
ON [dbo].[VendorReturn] ([ReturnDate])
GO
--PRINT 113
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_endorReturnItemID' AND OBJECT_ID = OBJECT_ID('VendorReturnItem'))
--DROP INDEX [idx_endorReturnItemID] ON [VendorReturnItem]
GO
CREATE NONCLUSTERED INDEX [idx_endorReturnItemID]
ON [dbo].[VendorReturnItem] ([VendorReturnId])
INCLUDE ([AccountId],[InstanceId],[ProductStockId],[Quantity],[ReturnPurchasePrice])
GO
--PRINT 114
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SalesReturnsDate' AND OBJECT_ID = OBJECT_ID('SalesReturn'))
--DROP INDEX [IDX_SalesReturnsDate] ON [SalesReturn]
GO 
CREATE NONCLUSTERED INDEX [IDX_SalesReturnsDate]
ON [dbo].[SalesReturn] ([ReturnDate])
INCLUDE ([AccountId],[InstanceId],[SalesId],[ReturnNo])
GO
--PRINT 115
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_SalesInvdt' AND OBJECT_ID = OBJECT_ID('Sales'))
--DROP INDEX [IDX_SalesInvdt] ON [Sales]
GO 
CREATE NONCLUSTERED INDEX [IDX_SalesInvdt]
ON [dbo].[Sales] ([AccountId],[InstanceId],[Cancelstatus],[InvoiceDate])
INCLUDE ([PaymentType],[Discount],[CashType])
GO
--PRINT 116
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_vendorpuchaseIns' AND OBJECT_ID = OBJECT_ID('VendorPurchase'))
--DROP INDEX [IDX_vendorpuchaseIns] ON [VendorPurchase]
GO
CREATE NONCLUSTERED INDEX [IDX_vendorpuchaseIns]
ON [dbo].[VendorPurchase] ([InstanceId],[CreatedAt])
INCLUDE ([PaymentType],[CancelStatus],[TaxRefNo])
GO
--PRINT 117
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_VendorReturnItem_Insid' AND OBJECT_ID = OBJECT_ID('VendorReturnItem'))
--DROP INDEX [idx_VendorReturnItem_Insid] ON [VendorReturnItem]
GO
CREATE NONCLUSTERED INDEX [idx_VendorReturnItem_Insid]
ON [dbo].[VendorReturnItem] ([InstanceId],[UpdatedAt])
INCLUDE ([ProductStockId],[Quantity])
GO
--PRINT 118
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_vendorreturn_Retdt' AND OBJECT_ID = OBJECT_ID('VendorReturn'))
--DROP INDEX [IDX_vendorreturn_Retdt] ON [VendorReturn]
GO
CREATE NONCLUSTERED INDEX [IDX_vendorreturn_Retdt]
ON [dbo].[VendorReturn] ([InstanceId],[ReturnDate])
GO
--PRINT 119
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_Stocktransfer_toins' AND OBJECT_ID = OBJECT_ID('StockTransfer'))
--DROP INDEX [idx_Stocktransfer_toins] ON [StockTransfer]
GO
CREATE NONCLUSTERED INDEX [idx_Stocktransfer_toins]
ON [dbo].[StockTransfer] ([ToInstanceId],[TransferStatus],[TransferDate])
GO
--PRINT 120
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductGstTotal' AND OBJECT_ID = OBJECT_ID('Product'))
--DROP INDEX [IDX_ProductGstTotal] ON [Product]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductGstTotal]
ON [dbo].[Product] ([GstTotal])
Go
--PRINT 121
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='idx_SalesPay_Instance' AND OBJECT_ID = OBJECT_ID('SalesPayment'))
--DROP INDEX [idx_SalesPay_Instance] ON [SalesPayment]
GO
CREATE NONCLUSTERED INDEX [idx_SalesPay_Instance]
ON [dbo].[SalesPayment] ([InstanceId],[isActive])
INCLUDE ([SalesId],[PaymentInd],[SubPaymentInd],[Amount])
GO
--PRINT 122
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductAccountIdCode' AND OBJECT_ID = OBJECT_ID('Product'))
--DROP INDEX [IDX_ProductAccountIdCode] ON [Product]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductAccountIdCode]
ON [dbo].[Product] ([AccountId],[Code])
GO
--PRINT 123
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='IDX_ProductIdCode' AND OBJECT_ID = OBJECT_ID('Product'))
--DROP INDEX [IDX_ProductIdCode] ON [Product]
GO
CREATE NONCLUSTERED INDEX [IDX_ProductIdCode]
ON [dbo].[Product] ([Id],[Code])
GO
--PRINT 124
GO
--IF EXISTS (SELECT * FROM SYS.INDEXES WHERE Name='XI_Sales_AccountIdInstanceIdNameMobile' AND OBJECT_ID = OBJECT_ID('Sales'))
--DROP INDEX [XI_Sales_AccountIdInstanceIdNameMobile] ON [Sales]
GO
CREATE NONCLUSTERED INDEX [XI_Sales_AccountIdInstanceIdNameMobile]
ON [dbo].[Sales] ([AccountId],[InstanceId],[Name],[Mobile])
GO
--PRINT 125
GO
CREATE NONCLUSTERED INDEX [IDX_Leads] ON [dbo].[Leads] ([LeadStatus]) INCLUDE ([AccountId], [Address], [Age], [CreatedAt], [CreatedBy], [DeliveryMode], [DOB], [DoctorId], [DoctorMobile], [DoctorName], [DoctorSpecialization], [Email], [ExternalId], [Gender], [InstanceId], [Lat], [LeadDate], [Lng], [LocalLeadStatus], [Mobile], [Name], [OfflineStatus], [PatientId], [Pincode], [UpdatedAt], [UpdatedBy])  
GO
CREATE NONCLUSTERED INDEX [IDX_SalesReturnItemStock] ON [dbo].SalesReturnItem ([ProductStockId], [IsDeleted]) INCLUDE([Quantity], [SalesReturnId])
GO