﻿CREATE TABLE [dbo].[SalesReturnItemAudit]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[SalesReturnId] CHAR(36) ,
	[ProductStockId] CHAR(36), 
    [Quantity] DECIMAL(18, 2) NULL,
	[CancelType] TINYINT NULL,
	[Discount] DECIMAL(5, 2) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0, 
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    [MrpSellingPrice] DECIMAL(18, 2) NULL, 
	[MRP] DECIMAL(18, 2) NULL,
	[IsDeleted] BIT null DEFAULT 0, 
	[Igst] DECIMAL(9, 2) NULL, 
	[Cgst] DECIMAL(9, 2) NULL, 
	[Sgst] DECIMAL(9, 2) NULL, 
	[GstTotal] DECIMAL(9, 2) NULL,
	[Action] CHAR(1) NULL DEFAULT 'U', 
    CONSTRAINT [PK_SalesReturnItemAudit] PRIMARY KEY ([Id]), 
)
