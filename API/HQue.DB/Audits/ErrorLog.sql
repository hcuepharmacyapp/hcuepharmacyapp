﻿CREATE TABLE [dbo].[ErrorLog]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[ErrorMessage] VARCHAR(MAX),
	[ErrorStackTrace] TEXT,
	[UnSavedData] TEXT,
	[Data] TEXT,
	[OfflineStatus] BIT NULL DEFAULT 0, 
	[CreatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_ErrorLog] PRIMARY KEY ([Id]), 
)
