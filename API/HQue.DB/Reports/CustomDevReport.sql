﻿CREATE TABLE [dbo].[CustomDevReport]
(
	[Id] CHAR(36) NOT NULL, 
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[ReportName] VARCHAR(150) ,
	[ReportType] CHAR(1) NULL, 
	[Query] VARCHAR(max) NULL,
	[FilterJSON] VARCHAR(MAX) NULL,
	[ConfigJSON] VARCHAR(MAX) NULL, 
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_CustomDevReport] PRIMARY KEY ([Id]) 
)