﻿CREATE TABLE [dbo].[CustomReport]
(
	[Id] CHAR(36) NOT NULL, 
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[ReportName] VARCHAR(150) ,
	[ReportType] CHAR(1) NULL, 
	[Query] VARCHAR(2500) NULL,
	[ConfigJSON] VARCHAR(MAX) NULL, 
    [Remarks] VARCHAR(250) NULL, 
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_CustomReports] PRIMARY KEY ([Id]) 
)
