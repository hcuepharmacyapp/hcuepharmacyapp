﻿CREATE TABLE [dbo].[UserAccess]
(
	[Id] CHAR(36) NOT NULL, 
    [UserId] CHAR(36) NOT NULL, 
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
    [AccessRight] TINYINT NOT NULL, 
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    CONSTRAINT [PK_UserAccess] PRIMARY KEY ([Id]) 
)
