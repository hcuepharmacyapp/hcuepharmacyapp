﻿CREATE TABLE [dbo].[LoginHistory]
(
	[Id] CHAR(36) NOT NULL, 
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
    [LogInUserId] VARCHAR(150)  NULL, 
	[LogOutUserId] VARCHAR(150)  NULL, 
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
	[LoggedInDate] DATETIME     NOT NULL DEFAULT GetDate(),
	[LoggedOutDate] DATETIME      NULL DEFAULT GetDate(),
	[LastTransctionDate] DATETIME      NULL DEFAULT GetDate(),
	[IsSentsms]BIT NULL,
	[IsSentemail]BIT NULL,
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_LoginHistory] PRIMARY KEY ([Id]) 
)
