﻿CREATE TABLE [dbo].[Voucher]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL,
	[InstanceId] CHAR(36),
	[ReturnId] CHAR(36) NULL,
	[ParticipantId] CHAR(36),
	[TransactionType] TINYINT,
	[VoucherType] TINYINT,
	[OriginalAmount] DECIMAL(18,2),
	[Factor] INT,
	[Amount] DECIMAL(18,2),
	[ReturnAmount] DECIMAL(18,2),
	[Status] BIT,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',   
    CONSTRAINT [PK_Voucher] PRIMARY KEY ([Id]) 
)