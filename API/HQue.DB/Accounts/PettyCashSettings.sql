﻿CREATE TABLE [dbo].[PettyCashSettings]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[OpeningBalance] DECIMAL(18, 2) NOT NULL, 
    [Amount] DECIMAL(18, 2) NOT NULL, 
	[CreateHead] VARCHAR(50)     NOT NULL ,
	[CreditOrDebit] VARCHAR(20) NOT NULL DEFAULT 0,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
   
    CONSTRAINT [PK_PettyCashSettings] PRIMARY KEY ([Id])
)
