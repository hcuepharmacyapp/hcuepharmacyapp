﻿CREATE TABLE [dbo].[CustomerPayment]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) ,
	 [SalesId] CHAR(36) NULL,
	  [CustomerId] CHAR(36) NULL,
	[TransactionDate] DATE NULL, 
    [Debit] DECIMAL(18, 2), 
	[Credit] DECIMAL(18, 2),
	[PaymentType] VARCHAR(50) NULL, 
	[ChequeNo] VARCHAR(50) NULL,
	[ChequeDate] DATE NULL,
	[CardNo] VARCHAR(50) NULL,
	[CardDate] DATE NULL, 
	[Remarks] VARCHAR (500) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',   
		[BankDeposited] BIT  NULL DEFAULT 0,
		[AmountCredited] BIT  NULL DEFAULT 0, 
    CONSTRAINT [PK_CustomerPayment] PRIMARY KEY ([Id]) 
)