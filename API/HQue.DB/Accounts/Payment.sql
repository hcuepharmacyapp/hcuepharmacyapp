﻿CREATE TABLE [dbo].[Payment]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL ,
	[InstanceId] CHAR(36) ,
	[TransactionDate] DATE NULL, 
	[VendorId] CHAR(36) ,
    [Debit] DECIMAL(18, 2), 
	[Credit] DECIMAL(18, 2),
	[PaymentType] VARCHAR(50) NULL,
	[PaymentMode] VARCHAR(50) NULL, 
	[BankName] VARCHAR (200) NULL,
	[BankBranchName] VARCHAR (200) NULL,
	[IfscCode] VARCHAR (50) NULL,
	[ChequeNo] VARCHAR(50) NULL,
	[ChequeDate] DATE NULL, 
	[VendorPurchaseId] CHAR(36) ,
	[Remarks] VARCHAR (500) NULL,
	[OfflineStatus] BIT NULL DEFAULT 0,
	[PaymentHistoryStatus] VARCHAR(50) NULL,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
	
	[Status] INT NULL,  
   
    CONSTRAINT [PK_Payment] PRIMARY KEY ([Id]) 
)