﻿CREATE TABLE [dbo].[Settlements]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) NOT NULL,
	[InstanceId] CHAR(36),
	[VoucherId] CHAR(36) NULL,	
	[TransactionType] TINYINT,
	[TransactionId] CHAR(36),
	[AdjustedAmount] DECIMAL(18,2),
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin',   
    CONSTRAINT [PK_Settlements] PRIMARY KEY ([Id]) 
)