﻿CREATE TABLE [dbo].[PettyCash]
(
	[Id] CHAR(36) NOT NULL,  
	[AccountId] CHAR(36) ,
	[InstanceId] CHAR(36) ,
	[UserId] CHAR(36) ,
	[Title] varchar(200),
	[CashType] varchar(50),
	[Description] VARCHAR(500) NOT NULL, 
    [Amount] DECIMAL(18, 2) NOT NULL, 
	[TransactionDate] DATETIME     NOT NULL DEFAULT GetDate(),
	[OfflineStatus] BIT NULL DEFAULT 0,
	[CreatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [UpdatedAt] DATETIME     NOT NULL DEFAULT GetDate(),
    [CreatedBy] CHAR(36) NOT NULL DEFAULT 'admin',
    [UpdatedBy] CHAR(36) NOT NULL DEFAULT 'admin', 
    CONSTRAINT [PK_PettyCash] PRIMARY KEY ([Id]), 
)
