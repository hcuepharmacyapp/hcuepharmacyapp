/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 29/04/2017	Poongodi R	  COnvert to int added in order by section
** 15/06/2017	Poongodi R	  Prefix added
** 08/08/2017	Santhiyagu    PaymentType condition added.     
** 03/10/2017   Senthil.S     Added Union Condition for Vendor OB
*******************************************************************************/
Create PROCEDURE [dbo].[usp_GetVendorPayments](@InstanceId varchar(36),@AccountId varchar(36),@VendorId varchar(36),@Mobile varchar(15),@CreditDate date,@Operator varchar(10))
 AS
 BEGIN
 SET NOCOUNT ON  
  
  select @VendorId  = isnull(@VendorId ,''),@Mobile  = isnull(@Mobile ,''),@CreditDate = ISNULL(@CreditDate,''), @Operator = ISNULL(@Operator,'')
  
  if(@Operator is null or @Operator ='')
  begin

	SELECT vp.InvoiceNo As InvoiceNo ,isnull(vp.GoodsRcvNo,'') As GoodsRcvNo,vp.InvoiceDate As InvoiceDate, isnull(vp.PaymentType,'') As PaymentType,
	isnull(vp.prefix,'')+ ISNULL(vp.BillSeries,'') BillSeries,isnull(VendorPurchaseId,'') VendorPurchaseId ,isnull(SUM(Debit),0) AS	Debit,
	cast(CAST(dbo.CreditCalculate(VendorPurchaseId) as Numeric(20,0)) as numeric(20,2)) AS Credit,
	isnull(v.Name,'') Name,v.Id,isnull(vp.CreditNoOfDays,0) CreditNoOfDays,
	DATEADD(day,isnull(vp.CreditNoOfDays,0),cast(vp.CreatedAt as date)) as CreditDate
 	FROM Payment P
	INNER JOIN VendorPurchase vp ON p.VendorPurchaseId = vp.Id
	inner join Vendor v on v.Id=vp.VendorId
	WHERE vp.InstanceId = @InstanceId and vp.AccountId = @AccountId 
	and isnull(vp.CancelStatus ,0)= 0
	and (vp.VendorId = @VendorId or @VendorId='')
	and (v.Mobile=@Mobile or @Mobile='')
	and vp.PaymentType='Credit'
	GROUP BY vp.InvoiceNo,vp.GoodsRcvNo,vp.InvoiceDate, VendorPurchaseId, vp.PaymentType, vp.CreatedAt, v.Name,v.Id,vp.CreditNoOfDays,
	isnull(vp.prefix,'')+ ISNULL(vp.BillSeries,'')
	HAVING cast(CAST(dbo.CreditCalculate(VendorPurchaseId) as Numeric(20,0)) as numeric(20,2)) - SUM(Debit) > 0 
	--Order by cast(vp.GoodsRcvNo as int) asc
	UNION
	Select a.* from 
	(SELECT 'NA' as 'InvoiceNo', 'NA' as 'GoodsRcvNo', Max( p.TransactionDate) as InvoiceDate,
	'Credit' as  'Paymenttype', 'OB' as 'BillSeries', null as 'VendorPurchaseId',Sum(p.debit) as  Debit,
	Sum(p.credit) as Credit,
	V.Name as  Name, V.Id as Id, 
	0 as CreditNoOfdays,Max(p.CreatedAt) as CreditDate
	From Payment p inner join Vendor V
	ON p.VendorId = v.Id
	Where (p.VendorId = @VendorId or @VendorId='')
	And p.VendorPurchaseId is null
	Group by Name, v.id) as a
	Group by a.InvoiceNo,a.GoodsRcvNo,a.InvoiceDate, a.Paymenttype,a.BillSeries, a.VendorPurchaseId,
	  a.Debit, a.Credit,a.Name,a.Id,a.CreditDate,a.CreditNoOfdays
	Having (a.Credit - a.Debit) > 0
	
  end

  else
  begin
	
	declare @fromdate date = null, @todate date = null

	if(@Operator = 'equal')
		begin
			select @fromdate = @CreditDate, @todate = @CreditDate
		end
	else if(@Operator = 'less')
		begin
			select @fromdate = null, @todate = DATEADD(DAY,-1,@CreditDate)
		end
	else if(@Operator = 'greater')
		begin
			select @fromdate = DATEADD(DAY,1,@CreditDate) , @todate = null
		end
				
		SELECT vp.InvoiceNo As InvoiceNo ,isnull(vp.GoodsRcvNo,'') As GoodsRcvNo,vp.InvoiceDate As InvoiceDate,
		isnull(vp.prefix,'')+ISNULL(vp.BillSeries,'') BillSeries, isnull(vp.PaymentType,'') As PaymentType,isnull(VendorPurchaseId,'') VendorPurchaseId ,
		isnull(SUM(Debit),0) AS	Debit,cast(CAST(dbo.CreditCalculate(VendorPurchaseId) as Numeric(20,0)) as numeric(20,2)) AS Credit,
		isnull(v.Name,'') Name,v.Id,isnull(vp.CreditNoOfDays,0) CreditNoOfDays,DATEADD(day,isnull(vp.CreditNoOfDays,0),
		cast(vp.CreatedAt as date)) as CreditDate
 
		FROM Payment P
		INNER JOIN VendorPurchase vp ON p.VendorPurchaseId = vp.Id
		inner join Vendor v on v.Id=vp.VendorId
		WHERE vp.InstanceId = @InstanceId and vp.AccountId = @AccountId 
		and isnull(vp.CancelStatus ,0)= 0
		and (vp.VendorId = @VendorId or @VendorId='')
		and (v.Mobile=@Mobile or @Mobile='')
		and vp.PaymentType='Credit'
		and DATEADD(DAY,isnull(vp.CreditNoOfDays,0),cast(vp.CreatedAt as date)) between ISNULL(@fromdate,DATEADD(DAY,isnull(vp.CreditNoOfDays,0),
		cast (vp.CreatedAt as date))) and ISNULL(@todate, DATEADD(DAY,isnull(vp.CreditNoOfDays,0),cast(vp.CreatedAt as date)))
		GROUP BY vp.InvoiceNo,vp.GoodsRcvNo,vp.InvoiceDate, VendorPurchaseId, vp.PaymentType, vp.CreatedAt, v.Name,
		v.Id,vp.CreditNoOfDays,isnull(vp.prefix,'')+ ISNULL(vp.BillSeries,'')
		HAVING cast(CAST(dbo.CreditCalculate(VendorPurchaseId) as Numeric(20,0)) as numeric(20,2)) - SUM(Debit) > 0 
		--Order by  cast(vp.GoodsRcvNo as int)  asc
		UNION
		Select a.* from 
	(  SELECT 'NA' as 'InvoiceNo', 'NA' as 'GoodsRcvNo',Max( p.TransactionDate) as InvoiceDate,
	   'Credit' as  'Paymenttype', 'OB' as 'BillSeries', null as 'VendorPurchaseId', Sum(p.debit) as  Debit,
	   Sum(p.credit) as Credit,
	   V.Name as  Name, V.Id as Id, 
	   0 as CreditNoOfdays,Max(p.CreatedAt) as CreditDate
	  From Payment p inner join Vendor V
	  ON p.VendorId = v.Id
	  Where  (p.VendorId = @VendorId or @VendorId='')
	  And p.VendorPurchaseId is null
	  Group by Name, v.id) as a
	  Group by a.InvoiceNo,a.GoodsRcvNo,a.InvoiceDate, a.Paymenttype,a.BillSeries, a.VendorPurchaseId,
	  a.Debit, a.Credit,a.Name,a.Id,a.CreditDate,a.CreditNoOfdays
	  Having (a.Credit - a.Debit) > 0

  end

END