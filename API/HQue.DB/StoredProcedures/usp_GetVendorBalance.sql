 

/*                              
******************************************************************************                            
** File: [usp_GetVendorBalance]  
** Name: [usp_GetVendorBalance]                              
** Description: To Get supllier as on balance
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author: Poongodi R   
** Created Date:   08/02/2017 
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 **08/03/2017  Poongodi     Po cancel status validation added
   **03/05/2017  sabarish    status checking added 

*******************************************************************************/ 
 
CREATE PROCEDURE [dbo].[usp_GetVendorBalance](@InstanceId VARCHAR(36) ,
@AccountId varchar(36)
                                              ) 
AS 
begin
 SELECT payment.VendorId, Vendor.Name as VendorName, round( sum(isnull(payment.Credit,0)) - sum(isnull(debit,0)) ,0) as Balance from payment Inner join Vendor on Vendor.Id = Payment.VendorId 
 inner join VendorPurchase VP on VP.id = Payment.VendorPurchaseId 
 and isnull(vp.CancelStatus ,0) =0 and (payment.Status is null or payment.Status = 1)
                        WHERE payment.accountId= @AccountId and payment.instanceId = @InstanceId

                        group by payment.VendorId, Vendor.Name


						end 