-- exec usp_GetCustomerWisePaymentReceiptCancellationList '2d5abd54-89a9-4b31-aa76-8ec70b9125dd','c3473243-ad71-4883-8f21-57e196c76e96','Chithra','08-Jan-2017','08-Mar-2017'
CREATE PROCEDURE [dbo].[usp_GetCustomerWisePaymentReceiptCancellationList](@AccountId varchar(36),@InstanceId varchar(36),@Name varchar(500),@StartDate datetime,@EndDate datetime)
  AS
 BEGIN
   SET NOCOUNT ON
	
select id,PaymentDate,BillNo,BillDate,BillAmount,sum(PaidAmount) as PaidAmount,sum(BillAmount)-sum(PaidAmount) as BalanceAmount,ChequeNo,ChequeDate,CardNo,CardDate,PaymentType
from (SELECT S.Id,CP.createdat AS PaymentDate,
S.InvoiceNo AS BillNo,S.InvoiceDate AS BillDate,		  
Convert(decimal(18, 2),sum((CASE  WHEN SI.SellingPrice > 0 THEN SI.SellingPrice ELSE PS.SellingPrice END) * SI.Quantity - ((CASE 
WHEN SI.SellingPrice > 0 THEN SI.SellingPrice ELSE PS.SellingPrice END ) * SI.Quantity * (CASE 
WHEN SI.Discount > 0 THEN SI.Discount ELSE S.Discount END) /100) )) AS BillAmount,			                       
CP.debit as PaidAmount, CP.Credit as BalanceAmount,CP.ChequeNo,CP.ChequeDate,CP.CardNo,CP.CardDate,CP.PaymentType
FROM SalesItem as SI 
inner join Sales as S on SI.SalesId = S.Id
inner join ProductStock as PS on SI.ProductStockId  = PS.Id
inner join CustomerPayment as CP on  CP.SalesId = S.Id
WHERE CP.Accountid=@AccountId and CP.InstanceId=@InstanceId and S.Name = @Name
and cast(CP.createdat as date )between isnull(@StartDate, CP.createdat) and isnull(@EndDate, CP.createdat)
AND (S.Cancelstatus IS NULL) AND S.Name IS NOT NULL AND S.Credit IS NOT NULL AND CP.PaymentType IS NOT NULL
GROUP BY S.Id,CP.createdat,S.InvoiceNo,S.InvoiceDate,CP.debit,CP.Credit,CP.ChequeNo,CP.ChequeDate,CP.CardNo,CP.CardDate,CP.PaymentType) a 
group by id,PaymentDate,BillNo,BillDate,BillAmount,ChequeNo,ChequeDate,CardNo,CardDate,PaymentType
--HAVING sum(BalanceAmount)-sum(PaidAmount)>0
Order by a.PaymentDate desc
  
 END



