﻿/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 25/04/2018	Gavaskar S	  ProductStock offline to online data update
*******************************************************************************/
CREATE PROCEDURE [dbo].[usp_UpdateProductStockToOnline](@AccountId CHAR(36),@InstanceId CHAR(36),@ProductStockIdsVsStock xml)
AS
BEGIN
	--DECLARE @xml XML 
	--SET @xml = CAST('<ProductStock><ProductStockId>'+REPLACE(REPLACE(REPLACE(ISNULL(@ProductStockIdsVsStock,''),'~','</ProductStockId><Stock>'),'''',''),',',
	--'</Stock></ProductStock><ProductStock><ProductStockId>')+'</Stock></ProductStock>' AS XML)

	--UPDATE PS SET PS.Stock = tmp.Stock FROM ProductStock PS
	--JOIN
	--(SELECT Tab.Col.value('ProductStockId[1]', 'CHAR(36)') AS ProductStockId,
	--Tab.Col.value('Stock[1]', 'decimal(18,6)') AS Stock FROM @xml.nodes('ProductStock') AS Tab(Col)) tmp ON tmp.ProductStockId = PS.Id WHERE PS.AccountId=@AccountId 
	--and PS.InstanceId = @InstanceId

	--SELECT PS.Id, PS.Stock FROM ProductStock PS
	--JOIN
	--(SELECT Tab.Col.value('ProductStockId[1]', 'CHAR(36)') AS ProductStockId,
	--Tab.Col.value('Stock[1]', 'decimal(18,6)') AS Stock FROM @xml.nodes('ProductStock') AS Tab(Col)) tmp ON tmp.ProductStockId = PS.Id AND tmp.Stock = PS.Stock


	UPDATE PS SET PS.Stock = tmp.Stock FROM ProductStock PS
	JOIN
	(SELECT Tab.Col.value('ID[1]', 'CHAR(36)') AS ProductStockId,
	Tab.Col.value('STOCK[1]', 'decimal(18,6)') AS Stock FROM @ProductStockIdsVsStock.nodes('/INSSTOCK/PRODUCTSTOCK')  AS Tab(Col)) tmp ON tmp.ProductStockId = PS.Id WHERE PS.AccountId=@AccountId 
	and PS.InstanceId = @InstanceId


	--SELECT Tab.Col.value('ID[1]', 'CHAR(36)') AS ProductStockId,
	--Tab.Col.value('STOCK[1]', 'decimal(18,6)') AS Stock FROM @ProductStockIdsVsStock.nodes('/INSSTOCK/PRODUCTSTOCK')  AS Tab(Col) 

END