  --Usage : -- usp_GetSupplierWiseBalanceList '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93'
/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 27/04/2017	Poongodi R	  Vendor InstanceId mpping removed and Instanceid map to Vendor purchase
** 13-10-2017   Senthil.S   Union Query Added
**11/11/2017   Poongodi			Accountid and instanceid filter added in Union section
*******************************************************************************/
Create PROCEDURE [dbo].[usp_GetSupplierWiseBalanceList](@InstanceId varchar(36),@AccountId varchar(36))
AS
	BEGIN
	SET NOCOUNT ON
SELECT Name,Mobile,Sum(Debit) as Debit,Sum(Credit) as Credit,Sum(Credit)-Sum(Debit) As TotalDue FROM 
(SELECT v.Name,v.Mobile,VendorPurchaseId ,SUM(Debit) AS Debit,ROUND(dbo.CreditCalculate(VendorPurchaseId),0) AS Credit FROM Payment 
				INNER JOIN VendorPurchase ON VendorPurchase.Id =Payment.VendorPurchaseId
				and VendorPurchase.instanceid = payment.InstanceId
				inner join vendor v on v.id=VendorPurchase.VendorId 
				WHERE v.AccountId=@AccountId
				and isnull(VendorPurchase.CancelStatus ,0) =0
				and VendorPurchase.InstanceId =@InstanceId and VendorPurchase.PaymentType='Credit'
				GROUP BY v.Name,v.Mobile,VendorPurchaseId
				HAVING dbo.CreditCalculate(VendorPurchaseId) - SUM(Debit) > 0
				UNION
				Select v.Name, v.mobile, '' as VendorPurchaseId, Sum(p.Debit) as Debit, Sum(p.Credit) as Credit
				From   Payment p inner join (Select * from Vendor where  AccountId=@AccountId)  V
				ON p.VendorId = v.Id
				Where p.VendorPurchaseId is null
				and P.InstanceId =@InstanceId 
				Group by Name, mobile
				Having Sum(p.Credit)-Sum(p.Debit) > 0

				) Supplier GROUP BY Name,Mobile

END