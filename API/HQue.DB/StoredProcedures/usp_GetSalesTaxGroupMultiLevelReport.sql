--Usage : -- usp_GetSalesTaxGroupMultiLevelReport '88670cde-b028-4918-a171-529663955eb5','18204879-99ff-4efd-b076-f85b4a0da0a3', 3 ,'24-Jul-2017','25-Jul-2017', -1, '', '', ''
/** Date        Author          Description                              
*******************************************************************************        
 ** 20/07/2017	Manivannan		Created 
 ** 30/08/2017  Sarubala        GST calculation changes done
*******************************************************************************/ 

 CREATE PROCEDURE [dbo].[usp_GetSalesTaxGroupMultiLevelReport](
 @InstanceIds varchar(1000),@AccountId varchar(36), @Level smallint, @StartDate datetime,@EndDate datetime, @GstTotal decimal(9,2), 
 @SearchColName VARCHAR(50), @SearchOption VARCHAR(50), @SearchValue VARCHAR(50)
)
 AS
 BEGIN
	SET NOCOUNT ON

	DECLARE @billNo VARCHAR(100) = NULL, 
	@PatientId VARCHAR(100) = NULL, 
	@from_gstTotal NUMERIC(10,2) = NULL,
	@to_gstTotal NUMERIC(10,2) = NULL,
	@from_profitCost NUMERIC(10,2) = NULL,
	@to_profitCost NUMERIC(10,2) = NULL,
	@from_profitMrp NUMERIC(10,2) = NULL,
	@to_profitMrp NUMERIC(10,2) = NULL

	IF @SearchColName IS NULL OR @SearchColName = ''
		SELECT @SearchColName = NULL, @SearchOption = NULL, @SearchValue = NULL
	IF @SearchOption = ''
		SELECT @SearchOption = NULL
	IF @SearchValue = '' 
		SELECT @SearchValue = NULL

	DECLARE @InvoiceSeries VARCHAR(500) = @SearchOption
	IF (ISNULL(@SearchColName,'') != 'billNo') 
		SELECT @InvoiceSeries = 'ALL'

	DECLARE @XmlList XML
	SET @XmlList = CAST(('<A>'+REPLACE(ISNULL(@InvoiceSeries,''),',','</A><A>')+'</A>') AS XML)
	
	IF (@SearchColName = 'billNo') 
		BEGIN
			SELECT @billNo = @SearchValue
		END
	Else IF (@SearchColName ='gstTotal') 
		BEGIN 
		  IF (@SearchOption='equal') 
		  SELECT @from_gstTotal = Cast (@SearchValue AS NUMERIC (10,2)) , 
				 @to_gstTotal = Cast (@SearchValue AS   NUMERIC (10,2)) 
		  ELSE 
		  IF (@SearchOption='greater') 
		  SELECT @from_gstTotal = Cast (@SearchValue AS NUMERIC (10,2))+0.01 , 
				 @to_gstTotal =                                         NULL 
		  ELSE 
		  IF (@SearchOption='less') 
		  SELECT @from_gstTotal =                                     0.00, 
				 @to_gstTotal = Cast (@SearchValue AS NUMERIC (10,2))-0.01 
		END
	Else IF (@SearchColName ='customerName') 
		Select @PatientId = @SearchValue

	IF IsNull(@GstTotal, -1) != -1
	BEGIN
		Select @from_gstTotal = @GstTotal,
		 @to_gstTotal  = @GstTotal
	END

	IF @Level = 1
	BEGIN
		;With temp_instances (InstanceId)
		As 
		(
			select * from udf_Split(@InstanceIds) a
		)

		SELECT 
		GstTotal, 
		(Sum(SalesCount) - Sum(ReturnCount)) SalesCount, 
		(SUM(InvoiceAmount)-SUM(ReturnInvoiceAmount)) InvoiceAmount,
		(SUM(InvoiceAmountNoTaxNoDiscount)-SUM(ReturnInvoiceAmountNoTaxNoDiscount)) InvoiceAmountNoTaxNoDiscount, 
		(SUM(TaxAmount)-SUM(ReturnTaxAmount)) TaxAmount
		FROM (
			SELECT 
			SI.GstTotal AS GstTotal, 
			Count(DISTINCT S.Id) SalesCount, 
			0 As ReturnCount, 
			--SUM(CONVERT(decimal(18,2), (SI.Quantity) * (ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))))) As InvoiceAmount, 
			SUM(SI.TotalAmount) As InvoiceAmount, 
			0 as ReturnInvoiceAmount,
			--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) / (1 + (IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)) /100))) as InvoiceAmountNoTaxNoDiscount, 
			SUM(SI.TotalAmount-SI.GstAmount) as InvoiceAmountNoTaxNoDiscount,
			0 as ReturnInvoiceAmountNoTaxNoDiscount,
			--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) / (100 + (IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)))) * IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)) ) as TaxAmount,
			SUM(SI.GstAmount) as TaxAmount,
			0 as ReturnTaxAmount			
			FROM Sales S WITH(NOLOCK)
			INNER JOIN Instance I WITH(NOLOCK) On I.Id = S.InstanceId
			INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
			INNER JOIN ProductStock PS WITH(NOLOCK) on PS.Id=SI.productstockid
			INNER JOIN temp_instances TempIns On S.InstanceId = TempIns.InstanceId
			LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
			WHERE 
			S.AccountId = @AccountId 
			AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
			AND S.Cancelstatus is NULL
			AND 
			(
				ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
				ISNULL(P.Name, '') Like ISNULL(@PatientId, ISNULL(P.Name, '')) + '%'
			)
			AND ISNULL(S.InvoiceSeries,'') = 
			(
				CASE 
				WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
				WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
				END
			)
			AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
			AND SI.GstTotal BETWEEN Isnull(@from_gstTotal,SI.GstTotal) AND Isnull(@to_gstTotal,SI.GstTotal)
			GROUP BY SI.GstTotal

			UNION ALL

			SELECT 
			sri.GstTotal AS GstTotal, 
			0 As SalesCount, 
			Count(SR.Id) ReturnCount, 
			0 as InvoiceAmount, 
			--SUM(CONVERT(decimal(18,2), (SRI.Quantity) * (ISNULL(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - ((ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100))))) As ReturnInvoiceAmount,			
			SUM(SRI.TotalAmount) As ReturnInvoiceAmount,			
			0 as InvoiceAmountNoTaxNoDiscount, 
			--SUM((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100)) / (1 + (case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end /100))) as ReturnInvoiceAmountNoTaxNoDiscount,
			SUM(SRI.TotalAmount-SRI.GstAmount) as ReturnInvoiceAmountNoTaxNoDiscount,
			0 as TaxAmount, 
			--SUM((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100)) / (100 + (case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end)) * case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end ) as ReturnTaxAmount
			SUM(SRI.GstAmount) as ReturnTaxAmount
			FROM SalesReturn SR WITH(NOLOCK)
			INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
			INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
			INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId
			INNER JOIN temp_instances TempIns On SR.InstanceId = TempIns.InstanceId
			LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
			LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
			WHERE SR.AccountId = @AccountId
			AND Convert(date,SR.returndate) BETWEEN @StartDate AND @EndDate
			AND S.Cancelstatus is NULL
			and ISNULL(sri.IsDeleted,0) != 1
			and ISNULL(sr.CancelType,0) != 2			
			AND 
			(
				ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
				ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
				or ISNULL(SR.PatientId,'') = ISNULL(@PatientId,ISNULL(SR.PatientId,''))
			)
			AND ISNULL(S.InvoiceSeries,'') = 
			(
				CASE 
				WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
				WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
				END
			)
			AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
			AND sri.GstTotal BETWEEN Isnull(@from_gstTotal, sri.GstTotal) AND Isnull(@to_gstTotal, sri.GstTotal)
			GROUP BY sri.GstTotal 
		) A
		GROUP BY GstTotal
		ORDER BY GstTotal
	End
	Else If @Level = 2
	Begin
		;With temp_instances (InstanceId)
		As 
		(
			select * from udf_Split(@InstanceIds) a
		)
		SELECT 
		GstTotal, 
		InstanceId, 
		InstanceName, 
		(Sum(SalesCount) - Sum(ReturnCount)) SalesCount, 
		(SUM(InvoiceAmount)-SUM(ReturnInvoiceAmount)) InvoiceAmount,		 
		(SUM(InvoiceAmountNoTaxNoDiscount)-SUM(ReturnInvoiceAmountNoTaxNoDiscount)) InvoiceAmountNoTaxNoDiscount, 
		(SUM(TaxAmount)-SUM(ReturnTaxAmount)) TaxAmount
		FROM (
			SELECT 
			SI.GstTotal AS GstTotal,
			I.Id InstanceId, 
			case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName,
			Count(DISTINCT S.Id) SalesCount, 
			0 As ReturnCount,
			--SUM(CONVERT(decimal(18,2), (SI.Quantity) * (ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))))) As InvoiceAmount, 
			SUM(SI.TotalAmount) As InvoiceAmount, 
			0 as ReturnInvoiceAmount,			
			--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) / (1 + (IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)) /100))) as InvoiceAmountNoTaxNoDiscount, 
			SUM(SI.TotalAmount-SI.GstAmount) as InvoiceAmountNoTaxNoDiscount, 
			0 as ReturnInvoiceAmountNoTaxNoDiscount,
			--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) / (100 + (IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)))) * IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0))) as TaxAmount, 
			SUM(SI.GstAmount) as TaxAmount, 
			0 as ReturnTaxAmount
			FROM Sales S WITH(NOLOCK)
			INNER JOIN Instance I WITH(NOLOCK) On I.Id = S.InstanceId
			INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
			INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SI.productstockid
			INNER JOIN temp_instances TempIns On S.InstanceId = TempIns.InstanceId
			LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
			WHERE S.AccountId = @AccountId 
			AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
			and S.Cancelstatus is NULL
			AND 
			(
				ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
				ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
			)
			AND ISNULL(S.InvoiceSeries,'') = 
			(
				CASE 
				WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
				WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
				END
			)
			AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
			AND SI.GstTotal BETWEEN Isnull(@from_gstTotal, SI.GstTotal) AND Isnull(@to_gstTotal, SI.GstTotal)
			GROUP BY SI.GstTotal,I.Id,case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end

			UNION ALL

			SELECT 
			sri.GstTotal AS GstTotal, 
			I.Id InstanceId, 
			case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName,
			0 As SalesCount, 
			Count(SR.Id) ReturnCount,
			0 as InvoiceAmount, 
			--SUM(CONVERT(decimal(18,2), (SRI.Quantity) * (ISNULL(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - ((ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100))))) As ReturnInvoiceAmount,	
			SUM(SRI.TotalAmount) As ReturnInvoiceAmount,		
			0 as InvoiceAmountNoTaxNoDiscount, 
			--SUM((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100)) / (1 + (case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end /100))) as ReturnInvoiceAmountNoTaxNoDiscount,
			SUM(SRI.TotalAmount-SRI.GstAmount) as ReturnInvoiceAmountNoTaxNoDiscount,
			0 as TaxAmount, 
			--SUM((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0))/ 100)) / (100 + (case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end)) * case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end ) as ReturnTaxAmount
			SUM(SRI.GstAmount) as ReturnTaxAmount
			FROM SalesReturn SR WITH(NOLOCK)
			INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
			INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
			INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId
			INNER JOIN temp_instances TempIns On SR.InstanceId = TempIns.InstanceId
			LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
			LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
			WHERE SR.AccountId = @AccountId
			AND Convert(date,SR.returndate) BETWEEN @StartDate AND @EndDate
			AND S.Cancelstatus is NULL
			and ISNULL(sri.IsDeleted,0) != 1
			and ISNULL(sr.CancelType,0) != 2			
			AND 
			(
				ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
				ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
				or ISNULL(SR.PatientId,'') = ISNULL(@PatientId,ISNULL(SR.PatientId,''))
			)
			AND ISNULL(S.InvoiceSeries,'') = 
			(
				CASE 
				WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
				WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
				END
			)
			AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
			AND sri.GstTotal BETWEEN Isnull(@from_gstTotal, sri.GstTotal) AND Isnull(@to_gstTotal, sri.GstTotal)
			GROUP BY sri.GstTotal, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end
		) A
		GROUP BY GstTotal, InstanceId, InstanceName
		ORDER BY GstTotal, InstanceName
	End
	Else If @Level = 3
	Begin
		SELECT 
		SI.GstTotal AS GstTotal, 
		S.InvoiceDate, 
		I.Id InstanceId, 
		case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, 
		S.InvoiceNo,
		LTRIM(ISNULL(s.prefix,'') + ISNULL(s.InvoiceSeries, '')) as InvoiceSeries, 
		P.Name PatientName,
		--SUM((SI.Quantity) * ISNULL(PS.PurchasePrice,0)/(1+(ISNULL(PS.VAT, 0)/100))*(1+(ISNULL(PS.GstTotal, 0)/100))) as PurchasePrice,
		--SUM(CONVERT(decimal(18,2), (SI.Quantity) * IsNull(SI.SellingPrice, PS.SellingPrice))) As MRPAmount,
		--SUM(CONVERT(decimal(18,2), (SI.Quantity) * (ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))))) As InvoiceAmount,
		SUM(SI.TotalAmount) As InvoiceAmount,
		--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) / (1 + (IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)) /100))) as InvoiceAmountNoTaxNoDiscount,
		SUM(SI.TotalAmount-SI.GstAmount) as InvoiceAmountNoTaxNoDiscount,
		--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) / (100 + (IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)))) * IsNull(PS.GstTotal,ISNULL(SI.GstTotal,0)) ) as TaxAmount,
		SUM(SI.GstAmount) as TaxAmount,
		--SUM(((SI.Quantity) * ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100)) as DiscountValue,
		0 As IsReturn
		FROM Sales S WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = S.InstanceId
		INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.SalesId
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SI.ProductStockId
		LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
		WHERE S.AccountId = @AccountId AND S.InstanceId = @InstanceIds		 
		AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
		and S.Cancelstatus is NULL
		AND 
		(
			ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
			ISNULL(P.Name, '') Like ISNULL(@PatientId, P.Name) + '%'
		)
		AND ISNULL(S.InvoiceSeries,'') = 
		(
			CASE 
			WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
			WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
			END
		)
		AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
		AND SI.GstTotal BETWEEN Isnull(@from_gstTotal, SI.GstTotal) AND Isnull(@to_gstTotal, SI.GstTotal)
		GROUP BY SI.GstTotal, S.InvoiceDate, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end, S.InvoiceNo, LTRIM(ISNULL(s.prefix,'') + ISNULL(s.InvoiceSeries, '')),P.Name
		
		UNION ALL
		
		SELECT 
		sri.GstTotal, 
		SR.ReturnDate as InvoiceDate, 
		I.Id InstanceId, 
		case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, 
		SR.ReturnNo as InvoiceNo,
		LTRIM(ISNULL(SR.prefix,'') + ISNULL(SR.InvoiceSeries, '')) as InvoiceSeries, 
		isNull(P2.Name, P.Name) PatientName,
		--SUM((SRI.Quantity) * ISNULL(PS.PurchasePrice,0)/(1+(ISNULL(PS.VAT, 0)/100))*(1+(ISNULL(PS.GstTotal, 0)/100))) as PurchasePrice,
		--SUM(CONVERT(decimal(18,2), (SRI.Quantity) * ISNULL(SRI.MrpSellingPrice, PS.SellingPrice))) As MRPAmount,
		--SUM(CONVERT(decimal(18,2), (SRI.Quantity) * (ISNULL(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1 - (ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) / 100)))) As InvoiceAmount,
		SUM(SRI.TotalAmount) As InvoiceAmount,
		--SUM((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (ISNULL(s.Discount,0)+ISNULL(sri.Discount,0))/ 100)) / (1 + (case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end /100))) as InvoiceAmountNoTaxNoDiscount,
		SUM(SRI.TotalAmount-SRI.GstAmount) as InvoiceAmountNoTaxNoDiscount,
		--SUM((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (1- (ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) / 100)) / (100 + (case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end)) * case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end ) as TaxAmount,
		SUM(SRI.GstAmount) as TaxAmount,
		--SUM(((SRI.Quantity) * ISNULL(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0)) * (ISNULL(s.Discount,0)+ISNULL(SRI.Discount,0)) / 100)) as DiscountValue,
		1 As IsReturn
		FROM SalesReturn SR WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
		INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId
		LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
		LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
		LEFT JOIN Patient P2 WITH(NOLOCK) ON P2.Id = SR.PatientId
		WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceIds		
		AND Convert(date,SR.returndate) BETWEEN @StartDate AND @EndDate
		and S.Cancelstatus is NULL
		and ISNULL(sri.IsDeleted,0) != 1
		and ISNULL(sr.CancelType,0) != 2	
		AND 
		(
			ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
			ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
			or ISNULL(SR.PatientId,'') = ISNULL(@PatientId,ISNULL(SR.PatientId,''))
		)
		AND ISNULL(S.InvoiceSeries,'') = 
		(
			CASE 
			WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
			WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
			END
		)
		AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
		AND sri.GstTotal BETWEEN Isnull(@from_gstTotal, sri.GstTotal) AND Isnull(@to_gstTotal, sri.GstTotal)
		GROUP BY sri.GstTotal, SR.ReturnDate, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end, SR.ReturnNo, LTRIM(ISNULL(SR.prefix,'') + ISNULL(SR.InvoiceSeries, '')),isNull(P2.Name, P.Name)
		ORDER BY InvoiceSeries desc, InvoiceNo desc
	End
 END