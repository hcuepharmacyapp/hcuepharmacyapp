﻿/*                               
******************************************************************************                            
** File: [usp_GetSalesOrderEstimateHistoryItemList]   
** Name: [usp_GetSalesOrderEstimateHistoryItemList]                               
** Description: To Get Purchase Audit details  
**   
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 31/10/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
  
*******************************************************************************/
 
  -- exec usp_GetCustomerOrderEstimateList 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4','26f2eddc-0784-42c6-aa6b-310447cd8a41'
 CREATE PROCEDURE [dbo].[usp_GetCustomerOrderEstimateList](@AccountId varchar(36),@InstanceId varchar(36),@PatientId varchar(36))
 AS
 BEGIN
   SET NOCOUNT ON

SELECT SOE.Id,isnull(SOE.Prefix,'') as Prefix,SOE.OrderEstimateId,SOE.OrderEstimateDate,SOE.SalesOrderEstimateType,
Patient.Id [PatientId] ,isnull(Patient.Name,'') as [PatientName] ,isnull(Patient.Email,'') as [PatientEmail],isnull(Patient.Mobile,'') as [PatientMobile], 
SOE.IsEstimateActive FROM SalesOrderEstimate SOE (nolock) 
INNER JOIN Patient (nolock) ON Patient.Id = SOE.PatientId  
WHERE SOE.AccountId  =  @AccountId AND SOE.InstanceId  =  @InstanceId AND SOE.PatientId = @PatientId AND ISNULL(SOE.IsEstimateActive,0)=0
--ORDER BY  (SOE.OrderEstimateId  )  desc  
ORDER BY SOE.CreatedAt desc 
           
END
           
 
