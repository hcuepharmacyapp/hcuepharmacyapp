﻿CREATE PROCEDURE [dbo].[usp_GetCustomerPaymentValue](
@AccountId VARCHAR(36), 
@InstanceId VARCHAR(36), 
@fromdate datetime, 
@todate datetime, 
@UserId VARCHAR(36),
@offlineStatus bit)
 AS
 BEGIN
   SET NOCOUNT ON
   
	select sum(debit) ReturnValue from CustomerPayment
	WHERE PaymentType = 'Cash' And 
		COALESCE(UpdatedAt,CreatedAt)  between (@fromdate) and (@todate)
		And AccountId=@AccountId and InstanceId=@InstanceId and CreatedBy=@UserId
		and offlineStatus = @offlineStatus

END