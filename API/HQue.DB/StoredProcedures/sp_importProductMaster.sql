CREATE PROCEDURE sp_importProductMaster (@AccountID as varchar(36), @InstanceID as varchar(36), @FilePath as varchar(1000), @IsCloud as int) AS
BEGIN

IF OBJECT_ID(N'tempdb..#tmpProductMaster', N'U') IS NOT NULL
	DROP TABLE #tmpProductMaster

IF OBJECT_ID(N'dbo.tmpProductMaster', N'U') IS NULL
BEGIN
PRINT 'Creating Table tmpProductMaster'
	CREATE TABLE tmpProductMaster
	(
	[RowREF]		int null,
	[ProductCode]	varchar(50) NOT NULL,
	[ProductName]	varchar(100) NOT NULL,
	[Manufacturer]	varchar(100) NOT NULL,
	[ScheduleCode]	varchar(200) NOT NULL,
	[Category]		varchar(200) NOT NULL,
	[Purchaseprice] varchar(16) NOT NULL,
	[PurchaseUnit]	varchar(16) NOT NULL,
	[PurchaseTax]	varchar(16) NOT NULL, 
	[HsnCode] varchar(50),
	[SGST] decimal(9,2),
	[CGST] decimal(9,2),
	[IGST] decimal(9,2))
END	

--SELECT * INTO #tmpProductMaster FROM tmpProductMaster
TRUNCATE TABLE tmpProductMaster 

DECLARE @bulk_cmd varchar(1000);
IF @IsCloud = 1  
	SET @bulk_cmd = 'BULK INSERT tmpProductMaster FROM '''+ @FilePath + ''' WITH (DATA_SOURCE=''MyAzureInvoicesContainer'', FORMAT=''CSV'', FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  
ELSE
	SET @bulk_cmd = 'BULK INSERT tmpProductMaster FROM '''+ @FilePath + ''' WITH (FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  

EXEC(@bulk_cmd);  

SELECT * INTO #tmpProductMaster FROM tmpProductMaster

UPDATE #tmpProductMaster SET ScheduleCode = 'NON-SCHEDULE' WHERE ScheduleCode = '0'
UPDATE #tmpProductMaster SET ScheduleCode = 'H' WHERE ScheduleCode = '1'


UPDATE p
   SET [Code] = t.ProductCode
      ,[Name] = t.productname
      ,[Manufacturer] = t.manufacturer
      ,[Schedule] = t.ScheduleCode
      ,[Category] = t.category
      ,[PackageSize] = t.PurchaseUnit
      ,[VAT] = t.PurchaseTax
      ,[Price] = t.Purchaseprice
	  ,[HsnCode] = t.HsnCode
	  ,[Igst] = t.Igst
	  ,[Cgst] = t.Cgst
	  ,[Sgst] = t.Sgst
	  ,[GstTotal] = ISNULL(t.[Sgst],0)+ISNULL(t.[Cgst],0)
FROM tmpProductMaster t INNER JOIN (SELECT * FROM product WHERE accountid = @AccountID) p ON p.name = t.productName 
WHERE accountid = @AccountID


INSERT INTO [dbo].[Product]
           ([Id],[AccountId],[InstanceId],[Code],[Name],[Manufacturer],[Schedule],[Category],[PackageSize],[VAT],[Price])
			SELECT NEWID(), @AccountID, null, LTRIM(RTRIM(t.ProductCode)), LTRIM(RTRIM(t.ProductName)), LTRIM(RTRIM(t.Manufacturer)), t.ScheduleCode, LTRIM(RTRIM(t.Category)), CAST(t.PurchaseUnit AS int), CAST(t.PurchaseTax AS decimal(9,2)), CAST(t.Purchaseprice AS decimal(18,2)) FROM #tmpProductMaster t WHERE productname NOT IN (SELECT name FROM product WHERE accountid = @AccountID)


update ps set ps.[HsnCode] = p.HsnCode,ps.[Igst] = p.Igst,ps.[Cgst] = p.Cgst,ps.[Sgst] = p.Sgst,ps.[GstTotal] = ISNULL(p.[Sgst],0)+ISNULL(p.[Cgst],0) from productstock ps inner join (select p.id,p.name,p.cgst,p.sgst,p.igst,p.HsnCode FROM tmpProductMaster t INNER JOIN (SELECT * FROM product WHERE accountid = @AccountID) p ON p.name = t.productName 
WHERE accountid = @AccountID) p on ps.productid=p.id where ps.accountId=@AccountID

END
	