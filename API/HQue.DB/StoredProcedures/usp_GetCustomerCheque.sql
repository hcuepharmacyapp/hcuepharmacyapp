
/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 **15/06/2007	Poongodi R		Prefix added
 **09/10/2017	Sarubala V		Multiple payment cheque included
 **02/11/2017	Sarubala V		Multiple payment cheque issue fix
*******************************************************************************/
 
Create PROCEDURE [dbo].[usp_GetCustomerCheque] (@accountid char(36),@instanceid char(36),@paymentType varchar(50),@PatientId varchar(100)) AS BEGIN
SELECT s.Id,
       max(ISNULL(s.Prefix,'')+ s.InvoiceSeries) AS InvoiceSeries,
       max(s.InvoiceNo) AS InvoiceNo,
       max(s.InvoiceDate) AS InvoiceDate,
       max(s.Name) AS Name,
       max(s.Mobile) AS Mobile,
       max(s.PaymentType) AS PaymentType,
       max(s.ChequeNo) AS ChequeNo,
       max(s.ChequeDate) AS ChequeDate,
       s.BankDeposited,
       s.AmountCredited,
       Sum(Isnull(InvoiceAmount, 0) - Isnull(DiscountVal, 0)) AS InvoiceAmount,
       'Sales' [Type]
FROM sales AS s
INNER JOIN SalesItem AS si ON si.SalesId = s.Id
INNER JOIN ProductStock AS ps ON si.ProductStockId = ps.Id CROSS apply
  (SELECT ((si.quantity *(Isnull(si.sellingprice,ps.sellingprice)) *Isnull(si.discount, 0)/100) + (Isnull(si.sellingprice,ps.sellingprice) *si.quantity * Isnull(s.discount, 0) / 100)) AS DiscountVal, CONVERT(DECIMAL(18, 2),(Isnull(si.sellingprice, ps.sellingprice)* si.quantity))AS InvoiceAmount) AS b
WHERE s.InstanceId=@instanceid
  AND s.AccountId=@accountid
  AND s.PaymentType =@paymentType
  AND s.PatientId=@PatientId
  AND (isnull(s.BankDeposited ,0) != 1 OR isnull(s.AmountCredited ,0) != 1)
GROUP BY s.Id,
         s.BankDeposited,
         s.AmountCredited


		 
		 
		 UNION all


SELECT cp.Id,
       max(ISNULL(s.Prefix,'')+s.InvoiceSeries) AS InvoiceSeries,
       max(s.InvoiceNo) InvoiceNo,
       max(s.InvoiceDate) AS InvoiceDate,
       max(s.Name) AS Name,
       max(s.Mobile) AS Mobile,
       max(cp.PaymentType) AS PaymentType,
       max(cp.ChequeNo) AS ChequeNo,
       max(cp.ChequeDate) AS ChequeDate,
       cp.BankDeposited,
       cp.AmountCredited,
       max(cp.Debit) AS InvoiceAmount,
       'CustomerReceipt' [Type]
FROM CustomerPayment as cp
INNER JOIN sales as s ON s.id = cp.SalesId
WHERE cp.PaymentType =@paymentType
 
  AND s.InstanceId= @instanceid
  AND s.AccountId= @accountid   
  AND s.PatientId=@PatientId
  AND (isnull(cp.BankDeposited ,0) != 1 OR isnull(cp.AmountCredited ,0) != 1)
GROUP BY cp.Id,
         cp.BankDeposited,
         cp.AmountCredited 
		 
		 union all


SELECT s.Id,
       max(ISNULL(s.Prefix,'')+ s.InvoiceSeries) AS InvoiceSeries,
       max(s.InvoiceNo) AS InvoiceNo,
       max(s.InvoiceDate) AS InvoiceDate,
       max(s.Name) AS Name,
       max(s.Mobile) AS Mobile,
       'Cheque' AS PaymentType,
       max(sp.ChequeNo) AS ChequeNo,
       max(sp.ChequeDate) AS ChequeDate,
       s.BankDeposited,
       s.AmountCredited,
       Sum(Isnull(sp.Amount, 0)) AS InvoiceAmount,
       'Sales' [Type]
FROM sales AS s
join SalesPayment sp on s.Id = sp.SalesId
WHERE s.InstanceId=@instanceid
  AND s.AccountId=@accountid
  AND s.PaymentType = 'Multiple'
  AND s.PatientId=@PatientId
  and sp.PaymentInd = 3
  and sp.isActive = 1
  AND (isnull(s.BankDeposited ,0) != 1 OR isnull(s.AmountCredited ,0) != 1)
GROUP BY s.Id,
         s.BankDeposited,
         s.AmountCredited
		 
		 END


   




  
                       