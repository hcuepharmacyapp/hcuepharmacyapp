/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 04/09/2017  Poongodi		Created
** 11/10/2017  nandhini		invoicedate1 for csv
** 06/03/2018  nandhini		return value roundoff added
*******************************************************************************/ 
create PROC [dbo].[Usp_gstr1_salesReturn](@Accountid  CHAR(36), 
                               @InstanceId VARCHAR(36), 
                               @Startdate  DATE, 
                               @EndDate    DATE, 
                               @GstinNo    VARCHAR(50), 
                               @FilterType VARCHAR(10),
							   @IsSummary bit=0) 
AS 
  BEGIN 
  declare
   @invoiceDate1 datetime
   DECLARE @Instance TABLE 
        ( 
           id CHAR(36) 
        ) 

      IF ( @FilterType = 'branch' ) 
        BEGIN 
            INSERT INTO @instance 
            SELECT @Instanceid 
			set @GstinNo = nULL
        END 
      ELSE 
        BEGIN 
            INSERT INTO @instance 
            SELECT id 
            FROM   instance 
            WHERE  gstinno = @GstinNo 
        END 

		SELECT   
		branch [BranchName],
		gstinno [BranchGSTin],
		max(returnno) AS [InvoiceNumber],
		max(Returndate) AS [InvoiceDate], 
		max(convert(varchar,Returndate,103)) [InvoiceDate1], 			
		max(retno) [Retno],
		max(Isnull(salesNAME, '')) AS [RecipientName], 
		productNAME AS [ProductName] ,
		sum(Isnull(Quantity, 0)) [Quantity], 
		vat AS [GSTTotal], 
		sum(isnull([VatValue],0)) [TaxAmount],
		Round(sum(isnull([VatValue],0)/2) ,3) [CGSTAmount],
		Round(sum(isnull([VatValue],0)/2) ,3) [SGSTAmount],
		0 [IGSTAmount],			
		
		case when (isRoundOff = 1) then 
			round(sum(isnull([ReturnValue],0)- isnull(ReturnCharges,0)),0) 
		else 
			sum(isnull([ReturnValue],0) - isnull(ReturnCharges,0)) 
		end
		[InvoiceValue],
		
		sum(isnull([ReturnValue],0) -isnull([VatValue],0)) [ValueWithoutTax],
		max([PatientGSTin]) [RecipientGSTin],
		max([PlaceOfSupply]) [PlaceOfSupply],
		sum(isnull(ReturnCharges,0)) [ReturnCharges] 
		from (
			SELECT 
			salesreturn.IsRoundOff as isRoundOff,
			salesreturnitem.productstockid,  
			i.name +' - ' + i.area [branch],
			i.gstinno,
            Isnull(salesreturnitem.quantity, 0) Quantity, 
            Isnull(salesreturnitem.mrpsellingprice, 0) MrpSellingPrice, 
            Isnull(salesreturnitem.discount, 0) Discount, 
            salesreturn.id AS [SalesReturnId], 
			salesreturn.ReturnNo retno,
			isnull(salesreturn.InvoiceSeries,'')+isnull(salesreturn.prefix,'')+salesreturn.ReturnNo AS [ReturnNo], 
            salesreturn.returndate AS [ReturnDate], 
            isnull(sales.name, Isnull(Patient.NAME, '')) AS [SalesName], 
			isnull(Patient.GsTin,'') [PatientGSTin],
            Isnull(sales.email, '') AS [SalesEmail], 
            Isnull(sales.mobile, '') AS [SalesMobile], 
            sales.discount AS [SalesDiscount], 
            productstock.sellingprice AS [ProductStockSellingPrice], 
            --case isnull(salesreturn.TaxRefNo,0) when 1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0)) else isnull(productstock.vat,0) end AS [VAT], 
			case isnull(salesreturn.TaxRefNo,0) when 1 then isnull(salesreturnitem.GstTotal,0) else isnull(productstock.vat,0) end AS [VAT],
            productstock.batchno AS [BatchNo], 
            productstock.expiredate AS [ExpireDate], 
            case @IsSummary when 1 then '' else product.NAME end AS [ProductName],
			--((sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100)) /(100+  case isnull(salesreturn.TaxRefNo,0) when 1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0))  else isnull(productstock.vat,0) end)*100 ) *  case isnull(salesreturn.TaxRefNo,0) when 1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0))  else isnull(productstock.vat,0) end / 100  [VatValue] ,
			salesreturnitem.GstAmount AS [VatValue] ,
			--((sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100)) /(100+  case isnull(salesreturn.TaxRefNo,0) when 1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0))  else isnull(productstock.vat,0) end)*100 )  [WithoutVAT],
			--round( sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100),6) [ReturnValue],
			salesreturnitem.TotalAmount AS [ReturnValue],
			--round(( sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100)) * SalesReturn.ReturnChargePercent /100,6) [ReturnCharges],
			round(salesreturnitem.TotalAmount * ISNULL(SalesReturn.ReturnChargePercent,0)/100,6) [ReturnCharges],
			left(ltrim(isnull(Patient.GsTin,'')),2) [PlaceOfSupply]
		
			FROM (Select * from salesreturnitem WITH(NOLOCK) where AccountId = @Accountid AND instanceid in (Select Id from @Instance) and (IsDeleted is null or IsDeleted != 1)) salesreturnitem
			INNER JOIN (Select * from salesreturn WITH(NOLOCK) where AccountId = @Accountid AND instanceid in (Select Id from @Instance) and cast(salesreturn.returndate as date) BETWEEN @StartDate AND @EndDate) salesreturn ON salesreturn.id = salesreturnitem.salesreturnid 
			LEFT JOIN (Select * from sales WITH(NOLOCK) where AccountId = @Accountid AND instanceid in (Select Id from @Instance)) sales ON sales.id = salesreturn.salesid 
			LEFT JOIN (Select * from patient WITH(NOLOCK) where Accountid = @Accountid) patient ON patient.id = salesreturn.patientid
			INNER JOIN (Select * from productstock WITH(NOLOCK) where AccountId = @Accountid AND instanceid in (Select Id from @Instance)) productstock ON productstock.id = salesreturnitem.productstockid 
			Left JOIN (Select * from product WITH(NOLOCK) where Accountid = @Accountid) product ON product.id = productstock.productid 
			inner join(Select * from Instance WITH(NOLOCK) where Accountid = @Accountid and id in (Select Id from @Instance)) i on i.Id=salesreturn.InstanceId
			
			--cross apply (select (Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice) * Isnull(salesreturnitem.quantity, 0)) [ItemValue],	((Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) * Isnull(salesreturnitem.discount, 0) /100) [DiscountValue]) SR
			  
			WHERE salesreturn.AccountId = @Accountid
			AND isnull(sales.cancelstatus,0) = 0 
		) as one 
		group by salesreturnid, vat,productNAME, branch, gstinno, isRoundOff         
		order by branch, max(Returndate) desc, cast(max(retno) as bigint) desc  
		 
 end