/** Date        Author          Description                              
*******************************************************************************        
 **21/06/2017   Violet			Prefix Added 
 ** 06/07/2017	Poongodi		GST % Added 
 ** 13/09/2017   Lawrence		All branch condition Added 
 ** 21/09/2017   Lawrence		bill discount not showing issue fixed and Branch Name Added
 ** 03/10/2017   Lawrence		item and bill wise discount issue fixed
*******************************************************************************
--exec usp_GetSalesUserWiseList 'c3473243-ad71-4883-8f21-57e196c76e96' ,'2d5abd54-89a9-4b31-aa76-8ec70b9125dd','2017-01-01','2017-03-14','userName','HCUE PHARMACY 2','9884430520','Detail'
--exec usp_GetSalesUserWiseList '013513b1-ea8c-4ea8-9fed-054b260ee197' ,'18204879-99ff-4efd-b076-f85b4a0da0a3','2017-03-18','2017-03-18','userName','SARAVANAN','','Summary'
*/ 
Create PROCEDURE [dbo].[usp_GetSalesUserWiseList](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime, @SearchOption varchar(50),
 @SearchValue varchar(200), @Mobile varchar(50), @FilterValue varchar(50))
AS
 BEGIN
   SET NOCOUNT ON

    Declare @UserName varchar(150) = '', @UserId varchar(150) = ''

	if (@SearchOption ='') select @SearchOption  = null
	if (@SearchValue ='') select @SearchValue  = null
	if (@Mobile ='') select @Mobile  = null
	if (@FilterValue ='') select @FilterValue  = null
	
	

	if (@SearchOption ='userName')
	select  @UserName = isnull(@SearchValue,'')
	if (@UserName ='') select @UserName  = null

	if (@SearchOption ='userId')
	select  @UserId = isnull(@SearchValue,'')
	if (@UserId ='') select @UserId  = null

IF (@SearchOption ='userName' or @SearchOption ='' or @SearchOption  = null) 
BEGIN

IF (@FilterValue ='Summary')
BEGIN
print 1
SELECT SalesDate,Name,count(TotalSales) as TotalSales,SUM(isnull(TotalPurchase,0)) as TotalPurchase,SUM(TotalMRP) AS TotalMRP,SUM(TotalDiscount) as TotalDiscount
, InstanceName as InstanceName
from(SELECT  convert(date,S.invoicedate) AS [SalesDate],S.invoiceno As[InvoiceNo],U.Name, count(distinct S.Id) AS TotalSales,
Convert(decimal(18,2),sum((VPI.PurchasePrice * SI.Quantity))) AS TotalPurchase,
Convert(decimal(18,2),sum((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * 
SI.Quantity - ((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity * isnull(S.Discount ,0)/ 100) ))  AS TotalMRP,
CONVERT(decimal(18,2),sum((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity * isnull(SI.Discount,0) / 100)) as TotalDiscount
, I.Name as InstanceName		
FROM Sales S (NOLOCK)
INNER JOIN SalesItem SI  (NOLOCK) ON S.Id = SI.salesid
INNER JOIN ProductStock PS  (NOLOCK) ON SI.productstockid = PS.id
LEFT OUTER JOIN VendorPurchaseItem VPI  (NOLOCK) ON PS.id=VPI.ProductStockId
LEFT OUTER JOIN HQueUser U  (NOLOCK) ON U.id = S.CreatedBy
INNER JOIN Instance I ON I.Id = ISNULL(@InstanceId,S.InstanceId)
WHERE S.InstanceId = ISNULL(@InstanceId,S.InstanceId) AND S.AccountId = @AccountId and S.Cancelstatus is NULL
--AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
AND CAST(S.invoicedate AS date) BETWEEN CAST(isnull(@StartDate  , S.invoicedate) AS date) and CAST(isnull(@EndDate,S.invoicedate) AS date)
and U.Name =isnull(@UserName , U.Name)
--and isnull(U.Name ,'')  like isnull(@UserName,'') +'%'
--and isnull(U.Mobile ,'')  like isnull(@Mobile,'') +'%'
GROUP BY S.invoicedate,S.invoiceno,U.Name, I.Name) as A GROUP BY A.SalesDate,A.Name, A.InstanceName  order by A.SalesDate desc 

END
ELSE IF (@FilterValue ='Invoice')
BEGIN
Print 2
select sales.invoicedate AS InvoiceDate,sales.invoiceno as InvoiceNo,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,U.Name, convert(decimal(18,2), sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) *salesitem.quantity)) As InvoiceAmount
,convert(decimal(18,2),Case when isnull(sales.Discount ,0)>0 then isnull(sales.Discount ,0) else isnull(salesitem.Discount,0) end) as Discount
,sum(salesitem.Quantity * (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100) as DiscountSum,sales.Name As [PatientName]
, I.Name as InstanceName
from sales sales 
join salesitem salesitem on sales.id= salesitem.salesid
join productstock productstock on productstock.id=salesitem.productstockid
LEFT OUTER JOIN HQueUser U  (NOLOCK) ON U.id = sales.CreatedBy
INNER JOIN Instance I ON I.Id = ISNULL(@InstanceId,sales.InstanceId)
WHERE sales.InstanceId = ISNULL(@InstanceId,sales.InstanceId) AND sales.AccountId = @AccountId
and U.Name =isnull(@UserName , U.Name)
--and isnull(U.Name ,'')  like isnull(@UserName,'') +'%'
--and sales.Mobile='9884430520' 
and sales.Cancelstatus is NULL
--AND Convert(date,sales.invoicedate) BETWEEN @StartDate AND @EndDate
AND CAST(sales.invoicedate AS date) BETWEEN CAST(isnull(@StartDate  , sales.invoicedate) AS date) and CAST(isnull(@EndDate,sales.invoicedate) AS date)
group by sales.invoicedate,sales.invoiceno,sales.InvoiceSeries,sales.Prefix,U.Name,isnull(sales.discount,0),sales.CreatedAt,sales.Name, I.Name ,salesitem.Discount
ORDER BY sales.CreatedAt desc

END
ELSE IF (@FilterValue ='Detail')
BEGIN
PRINT 3
/*
select sales.invoicedate AS InvoiceDate,sales.invoiceno as InvoiceNo,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,U.Name ,isnull(sales.Discount,0)   as  SalesDiscount,sales.Credit as  Credit,
sales.Name As [PatientName],
sales.DoctorName,sales.Address,salesitem.quantity as Quantity,convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END)) As SellingPrice,
convert(decimal(18,2),case when isnull(sales.Discount,0)>0 then isnull(sales.Discount,0) else isnull(salesitem.Discount,0) end) as Discount,
--isnull(salesitem.Discount,0) Discount,
 ps.BatchNo,ps.ExpireDate,case isnull(sales.TaxRefNo,0) when 
			 1 then isnull(salesitem.GstTotal, isnull(ps.GstTotal,0)) else isnull(ps.VAT,0) end VAT,ps.Stock,ps.PurchasePrice,p.Name as ProductName, p.Manufacturer, p.Schedule,p.Type,
convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) *salesitem.quantity) As InvoiceAmount, isnull(sales.Discount ,0) as Discount,ISNULL(ISNULL(vpi.PurchasePrice,ps.PurchasePrice) * (ps.Stock),0)  As CostPrice,(salesitem.Quantity * (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) * case isnull(salesitem.Discount,0) when 0 then isnull(sales.Discount,0) else salesitem.Discount end / 100) as DiscountSum
, I.Name as InstanceName

from sales(nolock) sales 
join salesitem (nolock) salesitem on sales.id= salesitem.salesid
join productstock (nolock) ps on ps.id=salesitem.productstockid
inner join Product (nolock) p on p.Id=ps.ProductId
left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem (nolock) group by productstockId) vpi on ps.id=vpi.productstockid 
LEFT OUTER JOIN HQueUser  U  (NOLOCK) ON U.id = sales.CreatedBy		
INNER JOIN Instance (nolock) I ON I.Id = ISNULL(@InstanceId,sales.InstanceId)	*/		
select sales.invoicedate AS InvoiceDate,sales.invoiceno as InvoiceNo,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,U.Name ,isnull(sales.Discount,0)   as  SalesDiscount,sales.Credit as  Credit,
sales.Name As [PatientName],

sales.DoctorName,sales.Address,salesitem.quantity as Quantity,convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END)) As SellingPrice,
convert(decimal(18,2),case when isnull(sales.Discount,0)>0 then isnull(sales.Discount,0) else isnull(salesitem.Discount,0) end) as Discount,
--isnull(salesitem.Discount,0) Discount,
 ps.BatchNo,ps.ExpireDate,case isnull(sales.TaxRefNo,0) when 
			 1 then isnull(salesitem.GstTotal, isnull(ps.GstTotal,0)) else isnull(ps.VAT,0) end VAT,ps.Stock,ps.PurchasePrice,p.Name as ProductName, p.Manufacturer, p.Schedule,p.Type,
			 inv.InvoiceAmount,
--convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) *salesitem.quantity) As InvoiceAmount, 
inv.InvoiceAmount  - (inv.InvoiceAmount * (isnull(salesitem.Discount ,0) + isnull(sales.discount,0) ) )/100 [FinalValue],
(inv.InvoiceAmount  - (inv.InvoiceAmount * (isnull(salesitem.Discount ,0) + isnull(sales.discount,0) ) )/100 ) *100 /(100+isnull(inv.vat,0)) AmountwithoutTax,
(inv.InvoiceAmount  - (inv.InvoiceAmount * (isnull(salesitem.Discount ,0) + isnull(sales.discount,0) ) )/100 ) - (inv.InvoiceAmount  - (inv.InvoiceAmount * (isnull(salesitem.Discount ,0) + isnull(sales.discount,0) ) )/100 ) *100 /(100+isnull(inv.vat,0))  [Tax],
 ISNULL(ISNULL(vpi.PurchasePrice,ps.PurchasePrice) * (ps.Stock),0)  As CostPrice,(salesitem.Quantity * (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) * case isnull(salesitem.Discount,0) when 0 then isnull(sales.Discount,0) else salesitem.Discount end / 100) as DiscountSum
, I.Name as InstanceName
from sales(nolock) sales 
join salesitem (nolock) salesitem on sales.id= salesitem.salesid
join productstock (nolock) ps on ps.id=salesitem.productstockid
inner join Product (nolock) p on p.Id=ps.ProductId
left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem (nolock) group by productstockId) vpi on ps.id=vpi.productstockid 
LEFT OUTER JOIN HQueUser  U  (NOLOCK) ON U.id = sales.CreatedBy		
INNER JOIN Instance (nolock) I ON I.Id = ISNULL(@InstanceId,sales.InstanceId)			
cross apply (select convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) *salesitem.quantity) As InvoiceAmount,
case isnull(sales.TaxRefNo,0) when 
			 1 then isnull(salesitem.GstTotal, isnull(ps.GstTotal,0)) else isnull(ps.VAT,0) end VAT) inv
WHERE sales.InstanceId = ISNULL(@InstanceId,sales.InstanceId) AND sales.AccountId = @AccountId
and U.Name =isnull(@UserName , U.Name) 
and sales.Cancelstatus is NULL
--AND Convert(date,sales.invoicedate) BETWEEN @StartDate AND @EndDate
AND CAST(sales.invoicedate AS date) BETWEEN CAST(isnull(@StartDate  , sales.invoicedate) AS date) and CAST(isnull(@EndDate,sales.invoicedate) AS date)
ORDER BY sales.CreatedAt desc

END

END

ELSE
BEGIN

IF (@FilterValue ='Summary')
BEGIN
print 11
SELECT SalesDate,Name,count(TotalSales) as TotalSales,SUM(isnull(TotalPurchase,0)) as TotalPurchase,SUM(TotalMRP) AS TotalMRP,SUM(TotalDiscount) as TotalDiscount, InstanceName 
from(
SELECT  convert(date,S.invoicedate) AS [SalesDate],S.invoiceno As[InvoiceNo],U.Name, count(distinct S.Id) AS TotalSales,
Convert(decimal(18,2),sum((VPI.PurchasePrice * SI.Quantity))) AS TotalPurchase,
Convert(decimal(18,2),sum((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * 
SI.Quantity - ((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity * isnull(S.Discount ,0) / 100) ))  AS TotalMRP,
CONVERT(decimal(18,2),sum((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity * isnull(SI.Discount,0) / 100)) as TotalDiscount  
, I.Name as InstanceName		
FROM Sales S (NOLOCK)
INNER JOIN SalesItem SI  (NOLOCK) ON S.Id = SI.salesid
INNER JOIN ProductStock PS  (NOLOCK) ON SI.productstockid = PS.id
LEFT OUTER JOIN VendorPurchaseItem VPI  (NOLOCK) ON PS.id=VPI.ProductStockId
LEFT OUTER JOIN HQueUser U  (NOLOCK) ON U.id = S.CreatedBy
INNER JOIN Instance I ON I.Id = ISNULL(@InstanceId,S.InstanceId)			
WHERE S.InstanceId = ISNULL(@InstanceId,S.InstanceId) AND S.AccountId = @AccountId and S.Cancelstatus is NULL
AND CAST(S.invoicedate AS date) BETWEEN CAST(isnull(@StartDate  , S.invoicedate) AS date) and CAST(isnull(@EndDate,S.invoicedate) AS date)
and U.UserId =isnull(@UserId , U.UserId) 
GROUP BY S.invoicedate,S.invoiceno,U.Name, I.Name ) as A GROUP BY A.SalesDate,A.Name, A.InstanceName
order by A.SalesDate desc

END
ELSE IF (@FilterValue ='Invoice')
BEGIN
Print 22
select sales.invoicedate AS InvoiceDate,sales.invoiceno as InvoiceNo,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,U.Name, convert(decimal(18,2), sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) *salesitem.quantity)) As InvoiceAmount
,convert(decimal(18,2),Case when isnull(sales.Discount ,0)>0 then isnull(sales.Discount ,0) else isnull(salesitem.Discount,0) end) as Discount
,sum(salesitem.Quantity * (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100) as DiscountSum,sales.Name As [PatientName]
, I.Name as InstanceName
from sales sales 
join salesitem salesitem on sales.id= salesitem.salesid
join productstock productstock on productstock.id=salesitem.productstockid
LEFT OUTER JOIN HQueUser U  (NOLOCK) ON U.id = sales.CreatedBy
INNER JOIN Instance I ON I.Id = ISNULL(@InstanceId,sales.InstanceId)
WHERE sales.InstanceId = ISNULL(@InstanceId,sales.InstanceId) AND sales.AccountId = @AccountId
and U.UserId =isnull(@UserId , U.UserId) 
and sales.Cancelstatus is NULL
AND CAST(sales.invoicedate AS date) BETWEEN CAST(isnull(@StartDate  , sales.invoicedate) AS date) and CAST(isnull(@EndDate,sales.invoicedate) AS date)
group by sales.invoicedate,sales.invoiceno,sales.InvoiceSeries,sales.Prefix,U.Name,isnull(sales.discount,0) ,sales.CreatedAt,sales.Name, I.Name, salesitem.Discount
ORDER BY sales.CreatedAt desc

END
ELSE IF (@FilterValue ='Detail')
BEGIN
PRINT 33
/*
select sales.invoicedate AS InvoiceDate,sales.invoiceno as InvoiceNo,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,U.Name ,isnull(sales.Discount,0)   as  SalesDiscount,sales.Credit as  Credit,
sales.Name As [PatientName],
sales.DoctorName,sales.Address,salesitem.quantity as Quantity,convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END)) As SellingPrice, 
--salesitem.Discount ,
convert(decimal(18,2),case when isnull(sales.Discount,0)>0 then isnull(sales.Discount,0) else isnull(salesitem.Discount,0) end) as Discount,
ps.BatchNo,ps.ExpireDate,
case isnull(sales.TaxRefNo,0) when 
			 1 then isnull(salesitem.GstTotal, isnull(ps.GstTotal,0)) else isnull(ps.VAT,0) end VAT,ps.Stock,ps.PurchasePrice,p.Name as ProductName, p.Manufacturer, p.Schedule,p.Type,
convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) *salesitem.quantity) As InvoiceAmount, ISNULL(ISNULL(vpi.PurchasePrice,ps.PurchasePrice) * (ps.Stock),0)  As CostPrice,(salesitem.Quantity * (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) * case isnull(salesitem.Discount,0) when 0 then isnull(sales.Discount,0) else isnull(salesitem.Discount,0) end / 100) as DiscountSum
, I.Name as InstanceName
from sales (nolock)sales 
join salesitem  (nolock)salesitem on sales.id= salesitem.salesid
join productstock (nolock) ps on ps.id=salesitem.productstockid
inner join Product (nolock) p on p.Id=ps.ProductId
left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem (nolock) group by productstockId) vpi on ps.id=vpi.productstockid 
LEFT OUTER JOIN HQueUser (nolock) U  ON U.id = sales.CreatedBy					
INNER JOIN Instance (nolock) I ON I.Id = ISNULL(@InstanceId,sales.InstanceId)*/
select sales.invoicedate AS InvoiceDate,sales.invoiceno as InvoiceNo,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,U.Name ,isnull(sales.Discount,0)   as  SalesDiscount,sales.Credit as  Credit,
sales.Name As [PatientName],

sales.DoctorName,sales.Address,salesitem.quantity as Quantity,convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END)) As SellingPrice,
convert(decimal(18,2),case when isnull(sales.Discount,0)>0 then isnull(sales.Discount,0) else isnull(salesitem.Discount,0) end) as Discount,
--isnull(salesitem.Discount,0) Discount,
 ps.BatchNo,ps.ExpireDate,case isnull(sales.TaxRefNo,0) when 
			 1 then isnull(salesitem.GstTotal, isnull(ps.GstTotal,0)) else isnull(ps.VAT,0) end VAT,ps.Stock,ps.PurchasePrice,p.Name as ProductName, p.Manufacturer, p.Schedule,p.Type,
			 inv.InvoiceAmount,
--convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) *salesitem.quantity) As InvoiceAmount, 
inv.InvoiceAmount  - (inv.InvoiceAmount * (isnull(salesitem.Discount ,0) + isnull(sales.discount,0) ) )/100 [FinalValue],
(inv.InvoiceAmount  - (inv.InvoiceAmount * (isnull(salesitem.Discount ,0) + isnull(sales.discount,0) ) )/100 ) *100 /(100+isnull(inv.vat,0)) AmountwithoutTax,
(inv.InvoiceAmount  - (inv.InvoiceAmount * (isnull(salesitem.Discount ,0) + isnull(sales.discount,0) ) )/100 ) - (inv.InvoiceAmount  - (inv.InvoiceAmount * (isnull(salesitem.Discount ,0) + isnull(sales.discount,0) ) )/100 ) *100 /(100+isnull(inv.vat,0))  [Tax],
 ISNULL(ISNULL(vpi.PurchasePrice,ps.PurchasePrice) * (ps.Stock),0)  As CostPrice,(salesitem.Quantity * (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) * case isnull(salesitem.Discount,0) when 0 then isnull(sales.Discount,0) else salesitem.Discount end / 100) as DiscountSum
, I.Name as InstanceName
from sales(nolock) sales 
join salesitem (nolock) salesitem on sales.id= salesitem.salesid
join productstock (nolock) ps on ps.id=salesitem.productstockid
inner join Product (nolock) p on p.Id=ps.ProductId
left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem (nolock) group by productstockId) vpi on ps.id=vpi.productstockid 
LEFT OUTER JOIN HQueUser  U  (NOLOCK) ON U.id = sales.CreatedBy		
INNER JOIN Instance (nolock) I ON I.Id = ISNULL(@InstanceId,sales.InstanceId)			
cross apply (select convert(decimal(18,2),(CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else ps.SellingPrice END) *salesitem.quantity) As InvoiceAmount,
case isnull(sales.TaxRefNo,0) when 
			 1 then isnull(salesitem.GstTotal, isnull(ps.GstTotal,0)) else isnull(ps.VAT,0) end VAT) inv
WHERE sales.InstanceId = ISNULL(@InstanceId,sales.InstanceId) AND sales.AccountId = @AccountId
and U.UserId =isnull(@UserId , U.UserId) 
and sales.Cancelstatus is NULL
--AND Convert(date,sales.invoicedate) BETWEEN @StartDate AND @EndDate
AND CAST(sales.invoicedate AS date) BETWEEN CAST(isnull(@StartDate  , sales.invoicedate) AS date) and CAST(isnull(@EndDate,sales.invoicedate) AS date)
ORDER BY sales.CreatedAt desc

END

END

END