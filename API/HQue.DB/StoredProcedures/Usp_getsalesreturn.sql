 

/*                             
******************************************************************************                            
** File: [Usp_getsalesreturn] 
** Name: [Usp_getsalesreturn]                             
** Description: To Get Sales Return details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author:Poongodi R 
** Created Date: 02/02/2017 
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
 **12/02/2017	Poongodi	 Discount forumula corrected 
 **13/06/2017	Violet  	 sales Name corrected 
 **21/06/2017   Violet		 Prefix Added 
 ** 06/07/2017	Poongodi	 GST % Added 
 ** 11/07/2017	Sarubala	 Return Invoice Series Added 
 ** 06/09/2017	San	         HsnCode Added 
 ** 13/09/2017  Lawrence	 All branch condition Added 
 ** 13/09/2017	Poongodi R		Instance Name added
 ** 12/10/2017	Poongodi R	Sales Return Discount Issue fixed
   ** 02/01/2018  nandhini		kindname  field added
*******************************************************************************/ 
 
create PROC [dbo].[Usp_getsalesreturn](@InstanceId VARCHAR(36), 
                                      @AccountId  VARCHAR(36), 
                                      @StartDate  DATETIME, 
                                      @EndDate    DATETIME,
									  @IsSummary int =0,
									  @FromReturnNo int,
									  @ToReturnNo int,
									  @SearchType int,
									  @Kind varchar(36)) 
AS 
  BEGIN 

/** 
@SearchType =1 is 'ReturnNo'
@SearchType =2 is 'BillDate'
@SearchType =0 is 'All'
**/

if (@SearchType  =1)
begin 
SElect @StartDate = NULL , @EndDate = NULL
end
 
if(@FromReturnNo=0  OR @FromReturnNo='')
select  @FromReturnNo =null

if(@ToReturnNo=0 OR @ToReturnNo='')
select  @ToReturnNo =null

if(@StartDate=0  OR @StartDate='')
select  @StartDate =null

if(@EndDate=0 OR @EndDate='')
select  @EndDate =null

      SET nocount ON 
if (isnull(@IsSummary,0) = 0)
Begin

      SELECT salesreturnitem.productstockid, 
             salesreturnitem.quantity AS Quantity, 
             salesreturnitem.mrpsellingprice AS MrpSellingPrice, 
             --Isnull(salesreturnitem.discount, 0)  + isnull( sales.discount   ,0)       Discount, 
			 Isnull(salesreturnitem.discount, 0) AS Discount,
             salesreturn.id                             AS [SalesReturnId], 
             ltrim(isnull(salesreturn.Prefix,'')+' '+isnull(salesreturn.InvoiceSeries,'')+' '+ salesreturn.ReturnNo)  AS [ReturnNo], 
             salesreturn.returndate                     AS [ReturnDate], 
			  salesreturn.CreatedAt                     AS [ReturnTime], 
             isnull(sales.name, Isnull(Patient.NAME, ''))  AS [SalesName], 
			 ltrim(isnull(sales.Prefix,'')+' '+isnull(sales.InvoiceSeries,'')+' '+ sales.InvoiceNo)  AS [BillNo],
             Isnull(sales.email, '')                    AS [SalesEmail], 
             Isnull(sales.mobile, '')                   AS [SalesMobile], 
             ISNULL(salesreturn.discount,0)             AS [SalesDiscount], 
             case isnull(salesreturn.TaxRefNo,0) when 
			 1 then isnull(salesreturnitem.GstTotal,0) else isnull(productstock.vat,0) end AS [VAT], 
             productstock.batchno                       AS [BatchNo], 
             productstock.expiredate                    AS [ExpireDate], 
             product.NAME                               AS [ProductName] ,
			 product.KindName as [KindName],
		/*round(	(Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) - ((Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) * Isnull(salesretur
nitem.discount, 0) /100)-
			  ((Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) * Isnull(sales.discount, 0) /100),0) [ReturnValue]*/
     			 --round( sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100),0)  [ReturnValue],
			  salesreturnitem.TotalAmount AS [ReturnValue],
			 -- ((sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100)) /(100+  case isnull(salesreturn.TaxRefNo,0) when 
			 --1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0))  else isnull(productstock.vat,0) end)*100 ) *  case isnull(salesreturn.TaxRefNo,0) when 
			 --1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0)) else isnull(productstock.vat,0) end / 
    --               100  [VatValue] ,
				d.TaxAmount AS [VatValue],
				--      ((sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100)) /(100+  case isnull(salesreturn.TaxRefNo,0) when 
			 --1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0))  else isnull(productstock.vat,0) end)*100 )  [WithoutVAT],
			 salesreturnitem.TotalAmount-d.TaxAmount AS [WithoutVAT],
			 ISNULL(salesreturnitem.DiscountAmount,0) AS DiscountAmount,
			 ISNULL(salesreturn.ReturnCharges,0) AS ReturnCharges,
			 isnull(product.HsnCode,ProductStock.HsnCode) As [HsnCode], Ins.Name [Branch],
			 hqUser.Name [UserName]
			 
	 FROM   salesreturnitem(nolock) 
             INNER JOIN salesreturn(nolock) 
                     ON salesreturn.id = salesreturnitem.salesreturnid 
			Inner join (Select * from instance (nolock) where accountid= @AccountId ) Ins on Ins.id = salesreturn.InstanceId
			Inner join (Select * from HQueUser (nolock) where accountid= @AccountId ) hqUser on hqUser.id = salesreturn.CreatedBy
		
             LEFT JOIN sales (nolock)
                     ON sales.id = salesreturn.salesid
		     LEFT JOIN patient(nolock)
					 ON patient.id = salesreturn.patientid 
             INNER JOIN productstock (nolock)
                     ON productstock.id = salesreturnitem.productstockid 
             INNER JOIN product (nolock)
                     ON product.id = productstock.productid 
					
    --  	 cross apply (select (Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) [ItemValue],
			 --((Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) * Isnull(salesreturnitem.discount, 0) /100) [DiscountValue]
			 -- )   SR
			 CROSS APPLY (SELECT CASE ISNULL(salesreturn.TaxRefNo,0) WHEN 1 THEN salesreturnitem.GstAmount ELSE 			 
			 (salesreturnitem.mrpsellingprice*salesreturnitem.quantity-ISNULL(salesreturnitem.DiscountAmount,0)/(100+isnull(productstock.vat,0))*100)*isnull(productstock.vat,0)/100			 
			 END AS TaxAmount) AS d
	  WHERE salesreturn.accountid = @AccountId 
             AND salesreturn.instanceid = isnull(@InstanceId,salesreturn.instanceid)
             AND Isnull(salesreturn.returndate,'') BETWEEN isnull(@StartDate,Isnull(salesreturn.returndate,'')) AND isnull(@EndDate,Isnull(salesreturn.returndate,''))  
	  and ISNULL(product.KindName,'')= isnull(@kind,ISNULL(product.KindName,''))
             AND isnull(sales.cancelstatus,0) =0
			 AND  Isnull(salesreturn.returnno,'') BETWEEN isnull(@FromReturnNo,Isnull(salesreturn.returnno,'')) AND isnull(@ToReturnNo,Isnull(salesreturn.returnno,''))
			  and salesreturnitem.quantity>0      
			 and (salesreturnitem.IsDeleted is null or salesreturnitem.IsDeleted != 1)
      ORDER  BY salesreturn.returndate DESC 
end 
else
begin
 SELECT productstockid, 
            sum(Quantity) AS Quantity, 
             MrpSellingPrice AS MrpSellingPrice, 
             Discount AS Discount, 
             salesreturnid                             AS [SalesReturnId], 
              returnno                       AS [ReturnNo], 
             Returndate                     AS [ReturnDate], 
             salesNAME                     AS [SalesName], 
			 ReturnTime         as[ReturnTime],
			 BillNo							as[BillNo],
             salesemail                    AS [SalesEmail], 
             salesmobile                   AS [SalesMobile], 
             salesdiscount                             AS [SalesDiscount], 
            vat                           AS [VAT], 
            ''                       AS [BatchNo], 
            NULL                   AS [ExpireDate], 
             productNAME                               AS [ProductName] ,
			 sum(ReturnValue) [ReturnValue],
			 sum(VatValue) [VatValue],
			 sum(WithoutVAT) [WithoutVAT]
			 ,Max(branch) [Branch],
			 max(UserName) [UserName]
			  from(
 SELECT salesreturnitem.productstockid, 
             salesreturnitem.quantity AS Quantity, 
             salesreturnitem.mrpsellingprice AS MrpSellingPrice, 
             --Isnull(salesreturnitem.discount, 0) + isnull(sales.discount,0)         Discount, 
			 Isnull(salesreturnitem.discount, 0) AS Discount, 
             salesreturn.id                             AS [SalesReturnId], 
             ltrim(isnull(salesreturn.Prefix,'')+' '+isnull(salesreturn.InvoiceSeries,'')+' '+ salesreturn.ReturnNo)   AS [ReturnNo], 
             salesreturn.returndate                     AS [ReturnDate], 
			   salesreturn.CreatedAt                     AS [ReturnTime], 
             isnull(sales.name, Isnull(Patient.NAME, ''))  AS [SalesName], 
		ltrim(isnull(sales.Prefix,'')+' '+isnull(sales.InvoiceSeries,'')+' '+ sales.InvoiceNo)  AS [BillNo],
             Isnull(sales.email, '')                    AS [SalesEmail], 
             Isnull(sales.mobile, '')                   AS [SalesMobile], 
             ISNULL(salesreturn.discount,0)             AS [SalesDiscount], 
             --productstock.sellingprice                  AS 
             --[ProductStockSellingPrice], 
              case isnull(salesreturn.TaxRefNo,0)
				 when 1 then 
				 isnull(salesreturnitem.GstTotal,0) 
				  else isnull(productstock.vat,0) end AS [VAT], 
             productstock.batchno                       AS [BatchNo], 
             productstock.expiredate                    AS [ExpireDate], 
             product.NAME                               AS [ProductName] ,
			 --	  ((sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100)) /(100+  case isnull(salesreturn.TaxRefNo,0) when 
			 --1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0))  else isnull(productstock.vat,0) end)*100 ) *  case isnull(salesreturn.TaxRefNo,0) when 
			 --1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0))  else isnull(productstock.vat,0) end / 
    --               100  [VatValue] ,
				d.TaxAmount AS [VatValue],
				--      ((sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100)) /(100+  case isnull(salesreturn.TaxRefNo,0) when 
			 --1 then isnull(productstock.GstTotal, isnull(salesreturnitem.GstTotal,0))  else isnull(productstock.vat,0) end)*100 )  [WithoutVAT],
			 salesreturnitem.TotalAmount-d.TaxAmount AS [WithoutVAT],
		/*round(	(Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) - ((Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) * Isnull(salesretu
rnitem.discount, 0) /100)-
			  ((Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) * Isnull(sales.discount, 0) /100),0) [ReturrnValue],*/
			 --round( sr.ItemValue - sr.DiscountValue -(sr.ItemValue *Isnull(sales.discount, 0) /100),0)  [ReturnValue],
			 salesreturnitem.TotalAmount AS [ReturnValue],
			 ISNULL(salesreturnitem.DiscountAmount,0) AS DiscountAmount,
			 ISNULL(salesreturn.ReturnCharges,0) AS ReturnCharges,
			isnull(product.HsnCode,ProductStock.HsnCode) As [HsnCode], Ins.Name [Branch],
			 hqUser.Name [UserName]
      FROM   salesreturnitem (nolock)
             INNER JOIN salesreturn (nolock)
                     ON salesreturn.id = salesreturnitem.salesreturnid 
					 	Inner join (Select * from instance (nolock) where accountid= @AccountId ) Ins on Ins.id = salesreturn.InstanceId
							Inner join (Select * from HQueUser (nolock) where accountid= @AccountId ) hqUser on hqUser.id = salesreturn.CreatedBy
             LEFT JOIN sales (nolock)
                     ON sales.id = salesreturn.salesid 
		     LEFT JOIN patient(nolock)
					 ON patient.id = salesreturn.patientid
             INNER JOIN productstock (nolock)
                     ON productstock.id = salesreturnitem.productstockid 
             INNER JOIN product (nolock)
                     ON product.id = productstock.productid 
					  inner join HQueUser(nolock)
					 on HQueUser.id=SalesReturn.CreatedBy
			 --cross apply (select (Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) [ItemValue],
			 --((Isnull(salesreturnitem.mrpsellingprice, productstock.sellingprice)  *  Isnull(salesreturnitem.quantity, 0)  ) * Isnull(salesreturnitem.discount, 0) /100) [DiscountValue]
			 -- )   SR
			 CROSS APPLY (SELECT CASE ISNULL(salesreturn.TaxRefNo,0) WHEN 1 THEN salesreturnitem.GstAmount ELSE 			 
			 (salesreturnitem.mrpsellingprice*salesreturnitem.quantity-ISNULL(salesreturnitem.DiscountAmount,0)/(100+isnull(productstock.vat,0))*100)*isnull(productstock.vat,0)/100			 
			 END AS TaxAmount) AS d
      WHERE salesreturn.accountid = @AccountId 
             AND salesreturn.instanceid = isnull(@InstanceId,salesreturn.instanceid)
             AND Isnull(salesreturn.returndate,' ') BETWEEN isnull(@StartDate,Isnull(salesreturn.returndate,'')) AND isnull(@EndDate,Isnull(salesreturn.returndate,''))  
	    and ISNULL(product.KindName,'')= isnull(@kind,ISNULL(product.KindName,''))
             AND isnull(sales.cancelstatus,0) =0
			 AND  Isnull(salesreturn.returnno,'') BETWEEN isnull(@FromReturnNo,Isnull(salesreturn.returnno,'')) AND isnull(@ToReturnNo,Isnull(salesreturn.returnno,''))
			 and (salesreturnitem.IsDeleted is null or salesreturnitem.IsDeleted != 1)
			 and salesreturnitem.quantity>0
			       
			 )as one group by   productstockid,  MrpSellingPrice, 
                 Discount, 
             salesreturnid  ,                
              returnno                       ,
             Returndate                  ,
             salesNAME             ,
             salesemail              ,
             salesmobile              ,
             salesdiscount                            ,
             --productstocksellingprice               ,
            vat                          ,
                       productNAME    ,BillNo    ,ReturnTime      
					    
      ORDER  BY returndate DESC 

end 
  END