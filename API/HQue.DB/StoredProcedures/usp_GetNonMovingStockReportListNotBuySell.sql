/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 04/05/2017	   Poongodi		   Duplicate removed 
 23/06/2017    Violet		   Handled case condtion for purchaseprice 
 11/07/2017    Sarubala		   GST selection Included 
*******************************************************************************/
CREATE PROCEDURE [dbo].[usp_GetNonMovingStockReportListNotBuySell] (@InstanceId varchar(36), @from datetime, @to datetime)
 AS
 BEGIN
   SET NOCOUNT ON
   Declare @dt datetime;
   Set @dt = getdate();
    Select ProductName, max(CreatedAt) [CreatedAt], max(UpdatedAt)  [UpdatedAt] , max(LastSaleDate)  [LastSaleDate],
	max( Type) [Type] 
	,max(Category) [Category]
	 ,max(Schedule) [Schedule]
	 ,max(Id) [Id]
	 ,max(BatchNo) [BatchNo]
	 ,max( Quantity) as Quantity
	 ,max(ExpireDate) [ExpireDate]
	 ,max(VAT) [VAT]
	 ,max(GstTotal) [GstTotal]
	 ,max(Cgst) [Cgst]
	 ,max(Sgst) [Sgst]
	 ,max(Igst) [Igst]
	 ,max(InvoiceNo) [InvoiceNo]
	 ,max(CostPrice) [CostPrice]
	 ,max(MRP) [MRP]
	 ,max(Age) [Age]
	 ,max(PurchaseDate) [PurchaseDate]
	 , VendorName from (
	 SELECT DISTINCT P.Name as ProductName
	 ,isNull(ps.CreatedAt, '01/01/1900') as CreatedAt
	 ,isNull(ps.UpdatedAt, '01/01/1900') as UpdatedAt
	 ,case when OSI.CreatedAt is not 
null then Convert(Varchar(15), OSI.CreatedAt,103) else 'Not Sold' end as LastSaleDate
	 ,P.Type
	 ,P.Category
	 ,P.Schedule
	 ,PS.Id
	 ,PS.BatchNo
	 ,ABS(PS.Stock) as Quantity
	 ,PS.ExpireDate
	 ,PS.VAT
	 ,case when ISNULL(vpi.GstTotal,0) > 0 then vpi.GstTotal else 
		case when ISNULL(PS.GstTotal,0) > 0 then PS.GstTotal else PS.VAT end
	  end GstTotal
	 ,case when ISNULL(vpi.Cgst,0) > 0 then vpi.Cgst else ISNULL(PS.Cgst,0) end Cgst
	 ,case when ISNULL(vpi.Sgst,0) > 0 then vpi.Sgst else ISNULL(PS.Sgst,0) end Sgst
	 ,case when ISNULL(vpi.Igst,0) > 0 then vpi.Igst else ISNULL(PS.Igst,0) end Igst
	 ,S.InvoiceNo
	 ,case when VPI.Purchaseprice is not null then ISNULL((VPI.Purchaseprice) * (PS.Stock),0)  else ISNULL((PS.PurchasePrice) * (PS.Stock),0) end  As CostPrice
	 ,(PS.Stock * PS.Sellingprice) as MRP
	 , DATEDIFF(day,VP.InvoiceDate,@dt) as Age
	 ,Isnull(VP.InvoiceDate,PS.CreatedAt) as PurchaseDate
	 ,V.Name as VendorName
	FROM ProductStock PS WITH(NOLOCK)
	INNER JOIN Product P WITH(NOLOCK) on P.Id = PS.ProductId
	LEFT JOIN SalesItem OSI WITH(NOLOCK) on OSI.ProductStockId = PS.Id  --and OSI.CreatedAt = SI.CreatedAt
	LEFT JOIN sales S WITH(NOLOCK) on S.Id = OSI.SalesId
	LEFT JOIN VendorPurchaseItem VPI WITH(NOLOCK) on VPI.ProductStockId = PS.Id
	LEFT JOIN Vendor V WITH(NOLOCK) on v.Id = PS.VendorId
	LEFT JOIN VendorPurchase VP WITH(NOLOCK) on VP.Id = VPI.VendorPurchaseId
	and isnull(vp.CancelStatus ,0)= 0
	 WHERE PS.InstanceId IN (@InstanceId)
	 and (PS.Stock > 0) AND (Convert(date, PS.UpdatedAt) < Convert(date,@from) AND Convert(date, PS.UpdatedAt) < Convert(date,@to)) 	) as one
	 group by ProductName,vendorname
	ORDER BY 
CreatedAt desc
	 
END



