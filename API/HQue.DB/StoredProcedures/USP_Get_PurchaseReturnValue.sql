﻿ 
/*                               
******************************************************************************                            
** File: [USP_Get_PurchaseReturnValue]   
** Name: [USP_Get_PurchaseReturnValue]                               
** Description: To Get Purchase Return value against Purchase based on the flag(Return 
created with Purchase)
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 18/07/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 
*******************************************************************************/ 
 
CREATE Proc dbo.USP_Get_PurchaseReturnValue (@PurchaseId char(36))
as
begin
-- select VR.Id AS PurchaseReturnId,  ROUND(SUM(isnull(VR.TotalReturnedAmount,0)),2 ) [ReturnValue]
 select VR.Id AS PurchaseReturnId, isnull(VR.TotalReturnedAmount,0) [ReturnValue]
 from VendorReturnItem VRI 
 inner join VendorReturn VR on VR.id =VRI.VendorReturnId and isnull(VR.IsAlongWithPurchase ,0) =1 
 where  VR.VendorPurchaseId =@PurchaseId  
 group by  VR.Id,VR.TotalReturnedAmount

  end 


