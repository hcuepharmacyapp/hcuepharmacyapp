﻿/*                               
******************************************************************************                            
** File: [usp_GetSalesTemplateHistoryItemList]   
** Name: [usp_GetSalesTemplateHistoryItemList]                               
** Description: To Get Sales Template Details
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 09/10/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
  
*******************************************************************************/
 
  -- exec usp_GetSalesTemplateHistoryItemList 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4','26f2eddc-0784-42c6-aa6b-310447cd8a41'
 CREATE PROCEDURE [dbo].[usp_GetSalesTemplateHistoryItemList](@AccountId varchar(36),@InstanceId varchar(36),@SalesTemplateId varchar(max))
 AS
 BEGIN
   SET NOCOUNT ON
   
SELECT STI.ProductId as ProductId,STI.Quantity,STI.TemplateId,STI.CreatedAt,ISNULL(P.Name,PM.Name) as [ProductName],STI.IsDeleted,P.Status 
FROM SalesTemplateItem STI WITH(NOLOCK)
LEFT JOIN Product P WITH(NOLOCK) ON P.Id = STI.ProductId 
LEFT JOIN ProductMaster PM WITH(NOLOCK) ON PM.Id = STI.ProductId 
WHERE STI.InstanceId  = @InstanceId  AND STI.AccountId  =  @AccountId
and STI.TemplateId in (select a.id from dbo.udf_Split(@SalesTemplateId ) a)
--ORDER BY STI.CreatedAt desc  
ORDER BY ISNULL(P.Name,PM.Name)  asc  
           
 END
     