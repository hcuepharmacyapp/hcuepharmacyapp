/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**02/08/17     Poongodi R	SP Created to  import stock
**10/08/17     Poongodi R	Table changed to Temp table 
**22/08/17     Poongodi R	Deadlock Priority Added
**04/10/17     Poongodi R	Product Instance duplicate issue fixed
**22/02/19     Sumathi	 COLLATE database_default Added 
*******************************************************************************/
CREATE PROCEDURE [dbo].[usp_importproductstock] (@AccountID as varchar(36), @InstanceID as varchar(36), @FilePath as varchar(1000), @Createdby varchar(36), @IsCloud as int) AS
BEGIN

 SET DEADLOCK_PRIORITY LOW
 
 IF OBJECT_ID(N'tempdb..#tmpStockImport', N'U') IS NOT NULL
	DROP TABLE #tmpStockImport

 /*IF OBJECT_ID(N'dbo.tmpStockImport', N'U') IS NULL
 BEGIN
  PRINT 'Creating Table tmpStockImport'*/
	 CREATE   TABLE #tmpStockImport
	 (
	 
		ProductName Varchar(1000) COLLATE database_default,
		HsnCode varchar(50),
		GstTotal decimal(9,2),
		[ExpireDate(dd-MMM-yyyy)] Date ,
		BatchNo varchar(100),
		[Units/Strip] int,
		[Price/Strip] decimal(20,2),
		[MRP/Strip] decimal(20,2),
		Barcode varchar(50),
		Stock Int,
		FreeQty Int ,
		[Vendor Name] Varchar(100) COLLATE database_default,
		Category varchar(250) ,
		[Generic Name] varchar(250) ,
		Schedule varchar(250) ,
		[Type] varchar(250) ,
		Manufacturer varchar(300) ,
		RackNo varchar(50) COLLATE database_default,
		BoxNo varchar(15) COLLATE database_default

		) 
/* END*/	
 
--SELECT * INTO #tmpStockImport FROM tmpVendor TRUNCATE TABLE #tmpStockImport

 
Declare @EXTRefId char(36) = Newid() 
DECLARE @bulk_cmd varchar(1000);  
IF @IsCloud = 1 
	SET @bulk_cmd = 'BULK INSERT #tmpStockImport FROM '''+ @FilePath + ''' WITH (MAXERRORS = 0, DATA_SOURCE=''MyAzureInvoicesContainer'', FORMAT=''CSV'', FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  
ELSE
	SET @bulk_cmd = 'BULK INSERT #tmpStockImport FROM '''+ @FilePath + ''' WITH (MAXERRORS = 0, FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  

EXEC(@bulk_cmd);  
-- create table ##output (id int identity(1,1), output nvarchar(255) null)
-- declare @rc int 
--insert ##output (output) exec @rc = master..xp_cmdshell 'ErrorLog'
 

--SELECT * INTO #tmpStockImport FROM tmpStockImport
Alter table #tmpStockImport Add VendorId Char(36) COLLATE database_default
Alter table #tmpStockImport Add ProductId Char(36) COLLATE database_default

/*Vendor*/
INSERT INTO [dbo].[Vendor]
           ([Id],[AccountId],[InstanceId],[Code],[Name],CreatedBy,UpdatedBy,Ext_RefId)
		   SELECT  NEWID(), @AccountID, null, 'Vendor000', RTRIM(LTRIM(t.[Vendor Name])) ,@Createdby,@Createdby,@EXTRefId from #tmpStockImport t 
			where  replace(RTRIM(LTRIM(t.[Vendor Name])) ,' ','') not in (select replace(name,' ','') from vendor(nolock) where Accountid =@accountid)
			group by  RTRIM(LTRIM(t.[Vendor Name]))
			update t set vendorid = v.id from #tmpStockImport t   inner join (Select * from vendor  where Accountid =@accountid) v on replace(v.name,' ','') =replace(RTRIM(LTRIM(t.[Vendor Name])) ,' ','')
/*Product Insert*/
INSERT INTO [dbo].[product]
           (Id,Name,AccountId,InstanceId,Code,Manufacturer,Type,Schedule,Category,[GenericName],CreatedAt,UpdatedAt,CreatedBy,UpdatedBy,PackageSize,HsnCode,Igst,Cgst,Sgst,GstTotal,Ext_RefId)
		   SELECT NEWID(),RTRIM(LTRIM(t.ProductName)),  @AccountID, null, '',  RTRIM(LTRIM(t.Manufacturer)),type, Schedule,Category, t.[Generic Name], getdate(),getdate(),@Createdby,@Createdby,[Units/Strip],HsnCode,GstTotal,GstTotal/2,GstTotal/2,GstTotal ,@EXTRefId from #tmpStockImport t 
			where  replace(RTRIM(LTRIM(t.ProductName)) ,' ','') not in (select replace(name,' ','') from product (nolock) where Accountid =@accountid)
			group by RTRIM(LTRIM(t.ProductName)), RTRIM(LTRIM(t.Manufacturer)),type, Schedule,Category, t.[Generic Name],[Units/Strip],HsnCode,GstTotal

			update t set productid = p.id from #tmpStockImport t   inner join (Select * from product  where Accountid =@accountid) p on replace(p.name,' ','') =replace(RTRIM(LTRIM(t.ProductName)) ,' ','')

/*Product Instance*/
Insert into productinstance(Id,AccountId,InstanceId,ProductId,ReOrderLevel,ReOrderQty,ReOrderModified,RackNo,BoxNo,Ext_RefId,OfflineStatus,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy)
Select   Newid(),@Accountid,@InstanceId,Productid, NULL,NULL, NULL, max(Rackno),max(boxno),@EXTRefId,0,getdate(),getdate(),@createdby,@createdby from #tmpStockImport where isnull(Rackno,'')+ isnull(Boxno,'')<> ''  
and Productid not in (select productid from productinstance(nolock) where Accountid =@accountid and instanceid =@InstanceId)
group  by Productid

/*Productstock*/
Insert into ProductStock (Id,AccountId,InstanceId,ProductId,VendorId,BatchNo,ExpireDate,VAT,TaxType,SellingPrice,MRP,PurchaseBarcode,Stock,PackageSize,PackagePurchasePrice,PurchasePrice,
OfflineStatus, CreatedBy,UpdatedBy,CST,IsMovingStock,ReOrderQty,Status,IsMovingStockExpire,NewOpenedStock,NewStockInvoiceNo,NewStockQty,StockImport,ImportQty,Eancode,
HsnCode,Igst,Cgst,Sgst,GstTotal,ImportDate,Ext_RefId,TaxRefNo,PrvStockQty)
Select NewId() Id,@AccountId,@InstanceId,t.ProductId,VendorId,Isnull(t.BatchNo,'0') ,Cast(t.[ExpireDate(dd-MMM-yyyy)] as date) ,0 VAT,Null ,t.[MRP/Strip]/t.[Units/Strip] SellingPrice,
t.[MRP/Strip]/t.[Units/Strip] MRP,Null ,t.Stock,t.[Units/Strip],t.[Price/Strip] ,((t.[Price/Strip]/t.[Units/Strip])*t.GstTotal)/100+(t.[Price/Strip]/t.[Units/Strip]) PurchasePrice,
0 , @createdby ,@createdby ,0 ,null ,NULL,1 ,null ,null ,null ,null ,1 ,t.stock ,Barcode 
,HsnCode,t.GstTotal,t.GstTotal/2 Cgst,t.GstTotal/2 ,t.GstTotal  ,getdate()  , @EXTRefId,1,0 from #tmpStockImport t


 SELECT 'SUCCESS'
 
		   
END