﻿Create PROCEDURE [dbo].[usp_InsertCustomerPaymentOB]
	(@InstanceId varchar(36),
	@AccountId varchar(36),
	@CustomerId char(36),
	@credit NUMERIC(18,2),
	@offlineStatus bit,
	@createdBy CHAR(36),
	@id CHAR(36),
	@updatedDate Datetime)
AS
 BEGIN
   SET NOCOUNT ON 

		Declare @status varchar(15)
		set @status = '0'
			If (@credit > 0)
			Begin
			IF Not Exists(Select 1 from CustomerPayment where CustomerID = @CustomerId and AccountId = @AccountId 
			and InstanceId = @InstanceId and SalesId is null  )
			Begin
			INSERT INTO CustomerPayment
			(Id,AccountID,InstanceID,SalesID,CustomerID,TransactionDate,Debit,Credit,offlinestatus,createdat,updatedat,createdby,updatedby) Values
			(@id, @AccountId, @InstanceId,NULL,  @CustomerId, @updatedDate,0, @credit, @offlineStatus,@updatedDate,@updatedDate,
			@createdBy,@createdBy)
			set @status = '1'
			end
			end

			Select @status as status

END

