/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 13/08/2017 Poongodi R	  Created
*******************************************************************************/ 
CREATE proc dbo.USP_Product_Update( 
@Id char(36),
 @Manufacturer varchar(300),
@Schedule Varchar(250),
@GenericName Varchar(250),
@Category Varchar(250),
@Packing Varchar(250),
@CommodityCode  Varchar(250),
@Type  Varchar(250),
@RackNo  Varchar (50)
 )
as 
begin
SET DEADLOCK_PRIORITY LOW
UPDATE Product SET Manufacturer = @Manufacturer,Schedule = @Schedule,GenericName = @GenericName,Category = @Category,Packing = @Packing,CommodityCode = @CommodityCode,Type = @Type,RackNo = @RackNo WHERE Product.Id  =  @Id
 select 'success'
end