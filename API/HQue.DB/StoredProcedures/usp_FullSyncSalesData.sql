/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 29/04/2017	Sarubala V	  SP Created

*******************************************************************************/
CREATE PROCEDURE [dbo].[usp_FullSyncSalesData] @AccountID char(36),@InstanceID VARCHAR(40)
AS
BEGIN

IF @InstanceID != '' and @AccountID != ''
BEGIN

DECLARE @TempExportTable TABLE
(
  Sales XML,
  SalesItem XML
)

INSERT INTO @TempExportTable VALUES
(   
  (select * from Sales where instanceid = @InstanceID for xml Auto, Root('Sales')),
   (select * from SalesItem where instanceid = @InstanceID for xml Auto, Root('SalesItem'))
)

SELECT        
  Sales as '*',
  SalesItem as '*' 
from @TempExportTable 
FOR XML PATH('HcueData1')
END

END
