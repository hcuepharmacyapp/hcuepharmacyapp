/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**19/10/17     Poongodi R		SP Created to Get Closing STock
**10/Jan/19   Sumathi S			replaced NOT IN WITH NOT EXISTS
*******************************************************************************/

CREATE Proc dbo.Usp_ClosingStockToOnline(@Accountid char(36),
@InstanceId char(36),
@xmlData xml
)
as
begin

IF ISNULL(@Accountid,'') = ''	
	SELECT TOP 1 @Accountid = Accountid FROM Instance WHERE Id = @InstanceId


SELECT x.col.value('Id[1]','varchar(50)') Id,
 x.col.value('AccountId[1]', 'char(36)') AccountId,
x.col.value('InstanceId[1]', ' char(36)') InstanceId,
x.col.value('ProductStockId[1]', ' char(36)') ProductStockId,
 x.col.value('TransDate[1]', ' datetime') TransDate,
 x.col.value('ClosingStock[1]', ' decimal(18,2)') ClosingStock,
 x.col.value('StockValue[1]', ' decimal(18,6)') StockValue,
 x.col.value('GstTotal[1]', ' Decimal(8,2)') GstTotal,
 x.col.value('IsOffline[1]', 'bit') IsOffline,
 x.col.value('IsSynctoOnline[1]', ' bit') IsSynctoOnline, 
 x.col.value('CreatedAt[1]', 'datetime') CreatedAt,
 x.col.value('CreatedBy[1]', 'char(36)') CreatedBy
FROM @xmlData.nodes('/ClosingStocksLog/ClosingStocks/ClosingStock') x(col)
--where x.col.value('Id[1]','varchar(50)') NOT IN (select Id  from ClosingStock(nolock) where Accountid=@Accountid and instanceid =@InstanceId
where NOT EXISTS (select Id  from ClosingStock(nolock) where Accountid=@Accountid and instanceid =@InstanceId
AND id = x.col.value('Id[1]','varchar(50)'))



SELECT x.col.value('Id[1]','varchar(50)') Id,
 x.col.value('AccountId[1]', 'char(36)') AccountId,
x.col.value('InstanceId[1]', ' char(36)') InstanceId,
x.col.value('Task[1]', ' varchar(50)') Task,
 x.col.value('ExecutedDate[1]', ' datetime') ExecutedDate,
 x.col.value('IsOffline[1]', 'bit') IsOffline,
 x.col.value('IsSynctoOnline[1]', ' bit') IsSynctoOnline, 
 x.col.value('CreatedAt[1]', 'datetime') CreatedAt,
 x.col.value('CreatedBy[1]', 'char(36)') CreatedBy,
  x.col.value('UpdatedAt[1]', 'datetime') UpdatedAt,
 x.col.value('UpdatedBy[1]', 'char(36)') UpdatedBy
FROM @xmlData.nodes('/ClosingStocksLog/Log/SchedulerLog') x(col)
--where x.col.value('Id[1]','varchar(50)') NOT IN (select Id  from SchedulerLog(nolock) where Accountid=@Accountid and instanceid =@InstanceId)
where NOT EXISTS (select Id  from SchedulerLog(nolock) where Accountid=@Accountid and instanceid =@InstanceId
AND id = x.col.value('Id[1]','varchar(50)'))

--select 'success'
end