﻿/*******************************************************************************                            
** create History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 **21/09/2017  Arun			

*******************************************************************************/ 
CREATE PROCEDURE [dbo].[Usp_Get_StockItemExpiry](@AccountId  CHAR(36), 
@InstanceId CHAR(36),
@productId char(36),
@ShowExpiredQty int
) 
AS 
BEGIN 


  SELECT 
ProductStock.Id,ProductStock.AccountId,ProductStock.InstanceId,ProductStock.ProductId,ProductStock.VendorId,ProductStock.BatchNo,ProductStock.ExpireDate,
ProductStock.VAT,isnull(ProductStock.Igst,0) as Igst,isnull(ProductStock.Cgst,0) as Cgst,isnull(ProductStock.Sgst,0) as Sgst,isnull(ProductStock.GstTotal,0) as GstTotal,Isnull(Product.HsnCode,'') [HsnCode],ProductStock.TaxType,ProductStock.SellingPrice,ProductStock.MRP,ProductStock.PurchaseBarcode,ProductStock.Stock,ISNULL(ProductStock.PackageSize, 1) AS PackageSize,ProductStock.PackagePurchasePrice,ProductStock.PurchasePrice,ProductStock.OfflineStatus,ProductStock.CreatedAt,ProductStock.UpdatedAt,ProductStock.CreatedBy,ProductStock.UpdatedBy,ProductStock.CST,ProductStock.IsMovingStock,ProductStock.ReOrderQty,ProductStock.Status,ProductStock.IsMovingStockExpire,ProductStock.NewOpenedStock,ProductStock.NewStockInvoiceNo,ProductStock.NewStockQty,ProductStock.StockImport,ProductStock.Eancode,Product.Id 
AS [Product.Id],Product.AccountId AS [Product.AccountId],Product.InstanceId AS [Product.InstanceId],Product.Code AS [Product.Code],Product.Name AS 
[Product.Name],Product.Manufacturer AS [Product.Manufacturer],Product.KindName AS [Product.KindName],Product.StrengthName AS [Product.StrengthName],Product.Type AS [Product.Type],
Product.Schedule AS [Product.Schedule],Product.Category AS [Product.Category],Product.GenericName AS [Product.GenericName],Product.CommodityCode AS [Product.CommodityCode],
Product.Packing AS [Product.Packing],Product.OfflineStatus AS [Product.OfflineStatus],Product.CreatedAt AS [Product.CreatedAt],Product.UpdatedAt AS [Product.UpdatedAt],
Product.CreatedBy AS [Product.CreatedBy],Product.UpdatedBy AS [Product.UpdatedBy],Product.PackageSize AS [Product.PackageSize],Product.VAT AS [Product.VAT],
Product.Price AS [Product.Price],Product.Status AS [Product.Status],ProductInstance.RackNo AS [Product.RackNo],ProductInstance.BoxNo AS [Product.BoxNo],Product.ProductMasterID AS [Product.ProductMasterID],
Product.ProductOrgID AS [Product.ProductOrgID],ProductInstance.ReOrderLevel AS [Product.ReOrderLevel],ProductInstance.ReOrderQty AS [Product.ReOrderQty],
isnull(Product.Igst,0) as [Product.Igst],isnull(Product.Cgst,0) as [Product.Cgst],isnull(Product.Sgst,0) as [Product.Sgst],isnull(Product.GstTotal,0) as [Product.GstTotal],
Product.Discount AS [Product.Discount],Vendor.Name AS [Vendor.Name] FROM ProductStock (nolock) INNER JOIN Product (nolock) ON Product.Id = ProductStock.ProductId
 LEFT JOIN Vendor ON ProductStock.VendorId = Vendor.Id LEFT JOIN ProductInstance ON ProductInstance.ProductId = ProductStock.ProductId
  And ProductInstance.InstanceId = ProductStock.InstanceId  WHERE ProductStock.Stock  >  0 AND 
   ((@ShowExpiredQty=0 and cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date)) OR 
   ((@ShowExpiredQty=1 and cast(ProductStock.ExpireDate  as date)<=cast(getdate() as date))))
		
  AND (ProductStock.Status  =  1 Or (ProductStock.Status IS NULL))  AND Product.Id =@productId  AND ProductStock.AccountId  =@AccountId 
  AND ProductStock.InstanceId  = @InstanceId ORDER BY ProductStock.ExpireDate asc 

  END