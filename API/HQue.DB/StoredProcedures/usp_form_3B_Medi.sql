/*************************************
******************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**14/08/17     Poongodi R	   SP Created 
 
*******************************************************************************/
Create proc dbo.usp_form_3B_Medi(@InstanceId  VARCHAR(36),@AccountId CHAR(36), @StartDate date,@EndDate date,@Searchtype varchar(10),@Gstin varchar(50) )
as
begin
 
 Declare @Instance table (ID char(36))
 if (@SearchType ='branch')
 begin
	insert into @instance  
	select @Instanceid
	select @GSTin = isnull(GsTinNo,'') from instance where id =@InstanceId -- +' ('+ name +' - '+ area +')'
 end 
else
begin
 insert into @instance 
 select id from instance where gstinno =@GSTin
 end
 
--select  @InstanceId=N'013513b1-ea8c-4ea8-9fed-054b260ee197',@AccountId=N'18204879-99ff-4efd-b076-f85b4a0da0a3',@StartDate=N'01-Jul-2017',@EndDate=N'31-Jul-2017'
declare @CGSTVal decimal(18,2), @CGST  decimal(18,2)
Declare @PO table (PoValue decimal(18,2),IGSTPoValue decimal(18,2),[TaxAmount] decimal(18,2), [IGST] decimal(18,2), [CGST] decimal(18,2),[SGST] decimal(18,2), vendorType int, locationtype int)
Declare @Sales table (SaleValue decimal(18,2),[TaxAmount] decimal(18,2), [IGST] decimal(18,2), [CGST] decimal(18,2),[SGST] decimal(18,2), gstType int )


insert into @PO (Povalue, IGSTPoValue, TaxAmount,IGST,CGST,SGST,vendorType,locationtype)
   Select case [locationType] when 1 then cast( isnull( sum( ReportTotal-[TaxValue] ),0.00) as decimal(18,2)) else 0.00 end  [POValue],
        case [locationType] when 2 then cast( isnull(sum( ReportTotal-[TaxValue] ) ,0.00) as decimal(18,2)) else 0.00 end  [IGSTValue],
      sum(round([TaxValue],2)) [TaxAmount] ,
 case [locationType] when 2 then   isnull( sum(round([TaxValue],2)),0.00) else 0.00 end [IGST],
 case [locationType] when 1 then    isnull(sum(round([TaxValue]/2,3)) ,0.00)else 0.00 end [CGST],
  case [locationType] when 1 then   isnull( sum(round([TaxValue]/2,3)),0.00) else 0.00 end [SGST],
	
	   [VendorType],[locationType]
	   from(
	   SELECT  sum(pocal.discount) [discount], vendorpurchase.id ,isnull(vendortype,1) [VendorType], isnull(locationType,1)  [locationType],
		sum(  Round( (Isnull(pocal.Poval, 0)) * 
       (case when VendorPurchase.TaxRefNo = 1 then ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0))					
	  else 0 end) / 100, 2) ) [TaxValue],    
         sum(    ( ( Isnull(vendorpurchaseitem.packageqty, 0) - Isnull(vendorpurchaseitem.freeqty, 0) ) * 
               vendorpurchaseitem.packagepurchaseprice ) - ( ( ( Isnull( vendorpurchaseitem.packageqty, 0) - 
			   Isnull( vendorpurchaseitem.freeqty, 0) ) * vendorpurchaseitem.packagepurchaseprice ) * 
             Isnull(vendorpurchaseitem.discount, 0) / 100 ) + ( ( ( ( Isnull( vendorpurchaseitem.packageqty, 0) - Isnull(vendorpurchaseitem.freeqty, 0) ) * 
             vendorpurchaseitem.packagepurchaseprice ) - ( ( ( Isnull(vendorpurchaseitem.packageqty, 0) - 
		  Isnull(vendorpurchaseitem.freeqty, 0) ) * vendorpurchaseitem.packagepurchaseprice ) * Isnull(vendorpurchaseitem.discount, 0) / 100 ) ) * ( 
 		   (case when VendorPurchase.TaxRefNo = 1 then ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0)) else 0 end) / 100)		  ) )
		    ReportTotal
	 	  FROM   vendorpurchaseitem (nolock)
		  INNER JOIN (select * from vendorpurchase  (nolock) where cast(createdat as date ) BETWEEN @StartDate AND @EndDate  
		  and instanceid in(select id from @instance)   and isnull(CancelStatus ,0)= 0) vendorpurchase 
				  ON vendorpurchase.id = vendorpurchaseitem.vendorpurchaseid 
		  INNER JOIN (select * from vendor (nolock) where accountid =@AccountId  and isnull(locationType,1) in (1,2))  vendor
				  ON vendor.id = vendorpurchase.vendorid 
		  INNER JOIN (select * from productstock (nolock) where instanceid in(select id from @instance) )productstock
				  ON productstock.id = vendorpurchaseitem.productstockid 
		  Left JOIN (select * from product  where accountid  =@AccountId)product
				  ON product.id = productstock.productid 
		CRoss apply (select  Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * ( 
							 Isnull ( vendorpurchaseitem.packageqty, 0) - Isnull ( vendorpurchaseitem.freeqty, 0) ) - ( 
							 Isnull( vendorpurchaseitem.packagepurchaseprice, 0) * ( Isnull ( vendorpurchaseitem.packageqty, 0) - 
							 Isnull ( vendorpurchaseitem.freeqty, 0) ) * Isnull ( vendorpurchaseitem.discount, 0) / 100 )  [Poval],
							( Isnull( vendorpurchaseitem.packagepurchaseprice, 0) * ( Isnull ( vendorpurchaseitem.packageqty, 0) - 
							Isnull ( vendorpurchaseitem.freeqty, 0) ) * Isnull ( vendorpurchaseitem.discount, 0) / 100 )  [Discount]) as pocal
		  WHERE   vendorpurchaseitem.accountid = @AccountId 
		  AND vendorpurchaseitem.instanceid in(select id from @instance)  
		  and (vendorpurchaseitem.Status is null or vendorpurchaseitem.Status = 1)
 group by vendorpurchase.id,isnull(vendortype,1) ,isnull([locationType],1))
 A  group by [VendorType],[locationType]
 

 /*Get Sales Value */
Insert into @Sales   (SaleValue ,[TaxAmount] , [IGST]  , [CGST]  ,[SGST]  , gstType   )
 SElect 
  cast(isnull( sum(transtotal-[TaxAmount]),0.00) as decimal(18,2))[TotalSaleValue],   
 
    '0.00' [TaxAmount], 
	0.00,
    cast( isnull( sum(taxamount) ,0.00)/2   as decimal(18,2))[CGSTTaxAmount],
	 cast( isnull( sum(taxamount) ,0.00) /2  as decimal(18,2)) [SGSTTaxAmount] ,
	 1
	    from (
 SELECT   S.Id   ,
 	 round( ( case right(cast(sum(a.itemval - a.discount) as decimal(18,2))  ,2) when  50 then round( sum(a.itemval - a.discount)+0.5 ,0)  
	 else round( sum(a.itemval - a.discount) ,0) end )  ,2) [TransTotal],
		--Round(sum(a.itemval - a.discount) ,0)   [TransTotal],
	Round(SUM(a.Discount) ,2) as DiscountValue ,
	Round(sum ((a.itemval - a.discount ) *100/(100+ISNULL(ps.GstTotal, isnull(SI.Gsttotal,0)))),2)  [Basic],
	Round( sum((a.itemval - a.discount ) - ((a.itemval - a.discount ) *100/(100+ISNULL(ps.GstTotal,  isnull(SI.Gsttotal,0) )))),2) [TaxAmount] ,
		round( ( case right(cast(sum(a.itemval - a.discount) as decimal(18,2))  ,2) when  50 then round( sum(a.itemval - a.discount)+0.5 ,0)  
	 else round( sum(a.itemval - a.discount) ,0) end )- round(sum(a.itemval - a.discount) ,2) ,2)
	  [RoundOff]
	 
	FROM Sales S WITH(NOLOCK) 
		INNER JOIN (Select * from SalesItem WITH(NOLOCK) where InstanceId in(select id from @instance) ) SI  on S.id= SI.salesid
		INNER JOIN (Select * from ProductStock WITH(NOLOCK) where InstanceId in(select id from @instance)  ) PS  on PS.Id=SI.productstockid
 		 cross apply (Select (SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0))) [ItemVal],
		 ((SI.Quantity) * ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100) [Discount] ) as  A
		WHERE 
			S.AccountId = @AccountId  
			AND  Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
			and  S.InstanceId in(select id from @instance) and s.cancelstatus is null
				and (ISNULL(ps.GstTotal, 0) > 0  or isnull(SI.Gsttotal,0) >0)
group by  s.id 
 
 )a
 /*Sale 0 GST */
 Insert into @Sales   (SaleValue ,[TaxAmount] , [IGST]  , [CGST]  ,[SGST]  , gstType   )
 Select cast( isnull( sum(transtotal),0.00) as decimal(18,2)) [TotalSaleValue],   
 
   0.00 [TaxAmount], 
   0.00,
   0.00  [CGSTTaxAmount],
	 0.00 [SGSTTaxAmount] ,
	 2  from (
 SELECT   S.Id   , 
	 round( ( case right(cast(sum(a.itemval - a.discount) as decimal(18,2))  ,2) when  50 then round( sum(a.itemval - a.discount)+0.5 ,0)  
	 else round( sum(a.itemval - a.discount) ,0) end )  ,2) [TransTotal],
		--Round(sum(a.itemval - a.discount) ,0)   [TransTotal],
	Round(SUM(a.Discount) ,2) as DiscountValue ,
  	round( ( case right(cast(sum(a.itemval - a.discount) as decimal(18,2))  ,2) when  50 then round( sum(a.itemval - a.discount)+0.5 ,0)  
	 else round( sum(a.itemval - a.discount) ,0) end )- round(sum(a.itemval - a.discount) ,2) ,2)
	  [RoundOff]
	 
	FROM Sales S WITH(NOLOCK) 
		INNER JOIN (Select * from SalesItem WITH(NOLOCK) where InstanceId in(select id from @instance) ) SI  on S.id= SI.salesid
		INNER JOIN (Select * from ProductStock WITH(NOLOCK) where InstanceId in(select id from @instance)  ) PS  on PS.Id=SI.productstockid
 		 cross apply (Select (SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0))) [ItemVal],
		 ((SI.Quantity) * ISNULL(SI.SellingPrice, ISNULL(PS.SellingPrice,0)) * case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100) [Discount] ) as  A
		WHERE 
			S.AccountId = @AccountId  
			AND  Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
			and  S.InstanceId in(select id from @instance) and s.cancelstatus is null 
				and (ISNULL(ps.GstTotal, 0) = 0  and isnull(SI.Gsttotal,0) =0)
group by  s.id 
 
 )a
 /*Taxable Sales Return*/
 insert into @Sales (SaleValue ,[TaxAmount] , [IGST]  , [CGST]  ,[SGST]  , gstType   )
 
 SELECT	    cast(isnull( sum(transtotal-[TaxAmount]),0.00) as decimal(18,2))  *(-1)[TotalSaleValue],   
 
    '0.00' [TaxAmount], 
	0.00,
    cast( isnull( sum(taxamount) ,0.00)/2   as decimal(18,2))  *(-1)[CGSTTaxAmount],
	 cast( isnull( sum(taxamount) ,0.00) /2  as decimal(18,2))   *(-1) [SGSTTaxAmount]  ,
	 1
	 
      
FROM   (SELECT   SR.id,  
               Round(Sum(a.itemval - a.discount), 2) [TransTotal], 
               Round(Sum(a.discount), 2) AS DiscountValue, 
               Round(Sum (( a.itemval - a.discount ) * 100 / ( 100 + ( isnull(ps.GstTotal, isnull(sri.GstTotal,0))   ) )), 2) [Basic], 
			   Round(Sum(( a.itemval - a.discount ) - ( ( a.itemval - a.discount ) * 100 / ( 100 + (  isnull(ps.GstTotal, isnull(sri.GstTotal,0))    ) ) )), 2)        [TaxAmount] 
FROM   salesreturn SR WITH(nolock) INNER JOIN 
(SELECT * FROM   salesreturnitem (nolock) WHERE  instanceid  in(select id from @instance) AND  (IsDeleted is null or IsDeleted != 1)    ) SRI ON SRI.salesreturnid = SR.id INNER JOIN 
(SELECT * FROM   productstock(nolock) WHERE  instanceid  in(select id from @instance)) PS ON PS.id = SRI.productstockid LEFT JOIN 
--(SELECT * FROM   patient (nolock) WHERE  accountid = @accountid) SP ON SP.id = Sr.patientid LEFT JOIN
 (SELECT * FROM   sales WHERE  instanceid  in(select id from @instance)) S ON SR.salesid = S.id 
CROSS apply (SELECT ( SRI.quantity * Isnull(SRI.mrpsellingprice,  Isnull(PS.sellingprice, 0)) ) [ItemVal], 
	( ( SRI.quantity ) * Isnull(SRI.mrpsellingprice, Isnull(PS.sellingprice, 0)) * CASE Isnull(s.discount, 0) WHEN   0 THEN Isnull(sri.discount, 0) else s.discount    END / 100 ) [Discount]) A 
WHERE  SR.accountid = @AccountId AND Isnull(sr.canceltype, 0) != 2 
and (sri.IsDeleted is null or sri.IsDeleted != 1)  

AND CONVERT(DATE, SR.returndate) BETWEEN @StartDate AND @EndDate  AND isnull(S.cancelstatus,0) =0   -- Modified cancelstatus
GROUP  BY    SR.id  ) A 
 
 
 /*Non Taxable Sales Return*/
 insert into @Sales (SaleValue ,[TaxAmount] , [IGST]  , [CGST]  ,[SGST]  , gstType   )
 
 SELECT	    cast(isnull( sum(transtotal-[TaxAmount]),0.00) as decimal(18,2))  *(-1)[TotalSaleValue],   
 
    '0.00' [TaxAmount], 
	0.00,
     0.00 [CGSTTaxAmount],
 0.00 [SGSTTaxAmount]  ,
	2
 
FROM   (SELECT   SR.id,  
               Round(Sum(a.itemval - a.discount), 2) [TransTotal], 
               Round(Sum(a.discount), 2) AS DiscountValue, 
               Round(Sum (( a.itemval - a.discount ) * 100 / ( 100 + (   Isnull(SRI.gsttotal, 0) ) )), 2) [Basic], 
			   Round(Sum(( a.itemval - a.discount ) - ( ( a.itemval - a.discount ) * 100 / ( 100 + (   Isnull(SRI.gsttotal, 0)   ) ) )), 2)        [TaxAmount] 
FROM   salesreturn SR WITH(nolock) INNER JOIN 
(SELECT * FROM   salesreturnitem (nolock) WHERE  instanceid in(select id from @instance) AND  (IsDeleted is null or IsDeleted != 1) and Isnull(gsttotal, 0) = 0 ) SRI ON SRI.salesreturnid = SR.id INNER JOIN 
(SELECT * FROM   productstock(nolock) WHERE  instanceid in(select id from @instance) ) PS ON PS.id = SRI.productstockid LEFT JOIN 
--(SELECT * FROM   patient (nolock) WHERE  accountid = @accountid) SP ON SP.id = Sr.patientid LEFT JOIN
 (SELECT * FROM   sales WHERE  instanceid  in(select id from @instance)) S ON SR.salesid = S.id 
CROSS apply (SELECT ( SRI.quantity * Isnull(SRI.mrpsellingprice,  Isnull(PS.sellingprice, 0)) ) [ItemVal], 
	( ( SRI.quantity ) * Isnull(SRI.mrpsellingprice, Isnull(PS.sellingprice, 0)) * CASE Isnull(s.discount, 0) WHEN   0 THEN Isnull(sri.discount, 0) else s.discount    END / 100 ) [Discount]) A 
WHERE  SR.accountid = @AccountId AND Isnull(sr.canceltype, 0) != 2 
and (sri.IsDeleted is null or sri.IsDeleted != 1)
AND CONVERT(DATE, SR.returndate) BETWEEN @StartDate AND @EndDate  AND S.cancelstatus IS NULL -- Modified cancelstatus
GROUP  BY   SR.id ) A 

 Create  table #gst3b  (Col0 varchar(200),Col1 varchar(200), col2 varchar(200), col3 varchar(200), col4 varchar(200), col5 varchar(200), col6 varchar(200),IsHeader int)
  insert into #gst3b (col0,isheader)
 values ('1. GSTIN: '+ @GSTin ,3),
		('2. Legal name of the registered person',3),
		('',3)

 insert into #gst3b (col0,isheader)
 values ('3.1 Detail of Outward Supplies and Inward supplies liable to reverse charges' ,3)
  insert into #gst3b
 values('','','','','','','',3),
 ('','Nature of Supplies','Total Taxable Value','Integrated Tax','Central Tax','State/ UT Tax','Cess',1), 
 ('','1','2','3','4','5','6',2)
 
 /*> 0 GST Sales */
 insert into #gst3b(col1,col2,col3,col4,col5)
 Select '(a) Outward taxable supplies (other than zero rated, nil rated and exempted)',
sum(isnull(SaleValue,0)),sum(isnull(IGST,0)),sum(isnull(CGST,0)),sum(isnull(SGST,0)) from  @Sales where gsttype =1 
 
  insert into #gst3b(col1,col2,col3,col4,col5)
 select '(b) Outward taxable supplies (zero rated)', '0.00','0.00','0.00','0.00'

  insert into #gst3b(col1,col2,col3,col4,col5)
 Select  '(c) Other outward supplies, (Nil rated, exempted)' , sum(isnull(SaleValue,0)),   
 
   0.00 [TaxAmount], 
   0.00  [CGSTTaxAmount],
	 0.00 [SGSTTaxAmount]   from  @Sales where gsttype =2 

 
 insert into #gst3b(col1,col2,col3,col4,col5)
 --select '(d) Inward supplies (liable to reverse charge)', PoValue, IGST,CGST,SGST from @PO where vendortype =2 
  select '(d) Inward supplies (liable to reverse charge)',  '0.00','0.00','0.00','0.00'

 
 insert into #gst3b(col1,col2,col3,col4,col5)
 select '(e) Non GST outward supplies', '0.00','0.00','0.00','0.00'

  insert into #gst3b(col0,IsHeader)
 Values(' ',null),
 ('3.2 Of the supplies shown in 3.1 (a) above, details of inter-State supplies made to unregistered persons, composition taxable persons and UIN holders',3),
 (' ',null)
  insert into #gst3b(col1,col2,col3,col4,col5,isheader)
 Values( '', 'Place of Supply (State/UT)','Total Taxable Value','Amount of Integrated Tax','',1),
		('1','2','3','4','',2),
		('Supplies made to Unregistered Persons','0.00','0.00','0.00','0.00',0),
		('Supplies made to Composition Taxable Persons','0.00','0.00','0.00','0.00',0),
		('Supplies made to UIN Holders','0.00','0.00','0.00','0.00',0)

  insert into #gst3b(col0,col2,col3,col4,col5,IsHeader)
 Values	(' ','','','','',3),
 ('4. Eligible ITC','','','','',3) ,
 (' ','','','','',3)

  insert into #gst3b 
 values ('','Details','Total Taxable Value','Integrated Tax','Central Tax','State/ UT Tax','Cess',1)

  insert into #gst3b(col1,col2,col3,col4,col5)
	Values	('(A) ITC Available (whether in full or part)','','','',''),
('(1) Import of goods','','','',''),
('(2) Import of services','','','',''),
('(3) Inward supplies liable to reverse charge (other than 1 & 2 above)','','','',''),
 ('(4) Inward supplies from ISD','','','','') 
 /*Regitered vendor Tax value*/
  insert into #gst3b(col1,col2,col3,col4,col5)
--SElect  '(5) All other ITC',sum(isnull(povalue,0)+isnull(igstpovalue,0)),sum(isnull(IGST,0)),sum(isnull(CGST,0)),sum(isnull(SGST,0))    from @po where vendortype =1 
SElect  '(5) All other ITC',sum(isnull(IGST,0)+isnull(CGST,0) + isnull(SGST,0)),sum(isnull(IGST,0)),sum(isnull(CGST,0)),sum(isnull(SGST,0))    from @po where vendortype =1 

 insert into #gst3b(col1,col2,col3,col4,col5)
Values('(B) ITC Reversed','','','',''),
('(1) As per rules 42 & 43 of CGST Rules','','','',''),
('(2) Others','','','',''),
('(C) Net ITC Available (A) ? (B)','','','',''),
('(D) Ineligible ITC','','','',''),
('(1) As per section 17(5)','','','',''),
('(2) Others','','','','') 


  insert into #gst3b(col0,col2,col3,col4,col5,IsHeader)
 Values	(' ','','','','',3),
 ('5.Values of exempt, nil-rated and non-GST inward supplies','','','','',3) ,
 (' ','','','','',3)

   insert into #gst3b
 values('','','','','','','',3),
 ('','Nature of Supplies','Inter-State supplies',' Intra-State supplies','','','',1),
  ('','1','2','3','','','',2)


  /*composition Vendor Tax details*/
    insert into #gst3b(col1,col2,col3 )
SElect  'From a supplier under composition scheme, Exempt and Nil rated supply',sum(isnull(IGST,0)),sum(isnull(CGST,0))+sum(isnull(SGST,0))   from @po where vendortype in(2,3)  

    insert into #gst3b(col1,col2,col3 )
	values('Non GST supply','','')

	  insert into #gst3b(col0,col2,col3,col4,col5,IsHeader)
 Values	(' ','','','','',3),
 ('6. Payment of Tax','','','','',3) ,
 (' ','','','','',3)
	  insert into #gst3b 
 values ('','Description','Tax payable','Integrated Tax','Central Tax','State/ UT Tax','Cess',1),
   ('','1','2','3','4','5','6',2)


 
 insert into #gst3b(col1,col2,col3)
--select 'Integrated Tax',SUM(igstpovalue) *(-1) , sum(igst) * (-1)  from @po -- Commented 17-08-2017

select 'Integrated Tax',(select SUM(igst) from @sales) igst , (select sum(igst) from @po where vendortype =1) igst -- Modified 17-08-2017
--Select @CGSTVal =  sum(cgstsaleval), @CGST = sum(cgst) from ( -- Commented 17-08-2017

Select @CGSTVal =  sum(cgst), @CGST =  (select sum(cgst) from @po where vendortype =1 ) from ( -- Modified 17-08-2017
SElect sum(salevalue -sgst) [CGSTSaleVal], sum(cgst)  [CGST] from @sales -- Modified 17-08-2017
--union all  -- Commented 17-08-2017
--select SUM(povalue) *(-1) , sum(cgst) * (-1)  from @po  -- Commented 17-08-2017
) one 

insert into #gst3b(col1, col2,col4,COL5)
Values('Central Tax',@CGSTVal, @CGST ,0.00 ),
('State/UT Tax',@CGSTVal,0.00, @CGST  ),
('Cess',0.00,0.00,0.00 )
 select * from #gst3b

 end