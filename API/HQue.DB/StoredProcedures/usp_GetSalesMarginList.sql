﻿/*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 31/05/2017	Poongodi		Null discount value eliminated
** 06/07/2017	Poongodi		GST % Added 
** 14/09/2017	Lawrence		All branch added
** 15/09/2017	nandhini		billno added for invoice all 
*******************************************************************************/ 
create PROCEDURE [dbo].[usp_GetSalesMarginList](@Filter varchar(15),@InstanceId VARCHAR(36),@AccountId  VARCHAR(36),@StartDate  DATETIME,@EndDate DATETIME) 
AS 
BEGIN 
	SET NOCOUNT ON
	IF @Filter = 'InvoiceWise' 
	BEGIN
	SELECT Convert(date,invoicedate) AS InvoiceDate,
	InvoiceNo as InvoiceNo,
		sum(MRP) As MRP, 
		SUM(CostPrice) As CostPrice,
		SUM(MRP-CostPrice) AS SalesProfit,
		SUM(MRP-CostPrice)/SUM(MRP)*100 AS ProfitPerc,
		InstanceName, Name
		FROM (
		SELECT 
		S.InvoiceNo AS InvoiceNo,
		ltrim(isnull(S.prefix,'')+isnull(S.InvoiceSeries, '')) as InvoiceSeries,
		S.invoicedate AS InvoiceDate,
		S.Name As Name,			  
		SUM(SI.ItemAmount) As InvoiceAmount, 
		SUM(d.Cost) As CostPrice,
		s.saleamount As MRP,	
		Inst.Name as InstanceName
		FROM Sales S WITH(NOLOCK)
		INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SI.productstockid					
		INNER JOIN Instance Inst WITH(NOLOCK) ON Inst.Id = ISNULL(@InstanceId,S.InstanceId)
		CROSS APPLY (SELECT ISNULL(PS.PurchasePrice,0) * SI.Quantity AS Cost) AS d
		WHERE S.AccountId = @AccountId AND S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
		AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
		AND S.Cancelstatus is NULL
		GROUP BY  S.InvoiceNo,S.Invoicedate,S.Name,S.InvoiceSeries,S.prefix,Inst.Name,s.saleamount,s.id
		) z
		GROUP BY InvoiceNo,Invoicedate,Name,InvoiceSeries,InstanceName
		ORDER BY Convert(date,invoicedate) desc
	END

	ELSE IF @Filter = 'ProductWise' 
	BEGIN
SELECT 
		S.InvoiceDate,
		S.InvoiceNo,
		ltrim(isnull(S.prefix,'')+isnull(S.InvoiceSeries, '')) as InvoiceSeries, 
		S.Name,
		PS.BatchNo,
		PS.ExpireDate,
		case isnull(S.TaxRefNo,0) when 1 then isnull(SI.GstTotal, 0) else isnull(SI.VAT,0) end VAT,
		PS.Stock,
		ISNULL(PS.PurchasePrice,0) AS PurchasePrice,
		P.Name as ProductName, 
		P.Schedule,
		d.Cost As CostPrice,
		SI.TotalAmount,
		SI.TotalAmount-d.Cost AS SalesProfit,
		(SI.TotalAmount-d.Cost)/SI.TotalAmount*100 AS ProfitPerc,
		Inst.Name as InstanceName
		FROM Sales S WITH(NOLOCK)
		INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SI.productstockid
		INNER JOIN  Product p WITH(NOLOCK) on p.Id=ps.ProductId				
		INNER JOIN Instance Inst WITH(NOLOCK) ON Inst.Id = ISNULL(@InstanceId,S.InstanceId)
		CROSS APPLY (SELECT ISNULL(PS.PurchasePrice,0)*SI.Quantity AS Cost) AS d
		WHERE S.AccountId = @AccountId and S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
		AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
		AND S.Cancelstatus is NULL
		
		ORDER BY S.CreatedAt desc
	END
	ELSE
	BEGIN	
	SELECT Convert(date,invoicedate) AS InvoiceDate,
		sum(MRP) As MRP, 
		SUM(CostPrice) As CostPrice,
		SUM(MRP-CostPrice) AS SalesProfit,
		SUM(MRP-CostPrice)/SUM(MRP)*100 AS ProfitPerc,
		InstanceName
		FROM (
		SELECT  
		S.invoicedate AS InvoiceDate,
		s.saleamount As MRP, 
		SUM(d.Cost) As CostPrice,
		Inst.Name as InstanceName	
		FROM Sales S WITH(NOLOCK)
		INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SI.productstockid
		INNER JOIN  Product p WITH(NOLOCK) on p.Id=ps.ProductId
		INNER JOIN Instance Inst WITH(NOLOCK) ON Inst.Id = ISNULL(@InstanceId,S.InstanceId)		
		CROSS APPLY (SELECT ISNULL(PS.PurchasePrice,0)*SI.Quantity AS Cost) AS d			
		WHERE  S.AccountId = @AccountId and S.InstanceId =ISNULL(@InstanceId,S.InstanceId) 
		AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
		AND S.Cancelstatus is NULL
		GROUP BY InvoiceDate,Inst.Name,s.id,s.saleamount
		) z
		GROUP BY Convert(date,invoicedate),InstanceName
		ORDER BY Convert(date,invoicedate) desc
	END
	SET NOCOUNT OFF
END