
CREATE PROCEDURE [dbo].[usp_GetDoctorList](@AccountId varchar(36),@InstanceId varchar(36), @Id varchar(36), @Mobile varchar(15), @PageNo int, @PageSize int)
 AS
 BEGIN
 SET NOCOUNT ON  

select @Id=ISNULL(@Id,''), @Mobile=ISNULL(@Mobile,'')

declare @DoctorSearch varchar(5),@AccId varchar(36),@InstId varchar(36)

select @DoctorSearch=DoctorSearchType from SaleSettings where AccountId=@AccountId and InstanceId=@InstanceId
select @AccId = @AccountId,@InstId = @InstanceId

if(@DoctorSearch = '2')
begin
	select @AccId = '', @InstId = ''
end

select *,COUNT(1) over () DoctorCount from 
(
select D.Id,isnull(D.Name,'') Name,isnull(D.ShortName,'') ShortName,isnull(D.Mobile,'') Mobile,isnull(D.Area,'') Area,isnull(D.City,'') City,
isnull(D.State,'') [State],isnull(D.Pincode,'') Pincode,isnull(Commission,0) Commission,D.GsTin from DefaultDoctor DD join Doctor D on DD.DoctorId = D.Id
where DD.AccountId=@AccountId and DD.InstanceId=@InstanceId
and (D.Id=@Id or @Id='')
and (D.Mobile=@Mobile or @Mobile='')

union

select Id,isnull(Name,'') Name,isnull(ShortName,'') ShortName,isnull(Mobile,'') Mobile,isnull(Area,'') Area,isnull(City,'') City,
isnull([State],'') [State],isnull(Pincode,'') Pincode,isnull(Commission,0) Commission,GsTin from Doctor
where (AccountId=@AccountId )
and (InstanceId=@InstanceId )
and (Id=@Id or @Id='')
and (Mobile=@Mobile or @Mobile='')

union 

select Id,isnull(Name,'') Name,isnull(ShortName,'') ShortName,isnull(Mobile,'') Mobile,isnull(Area,'') Area,isnull(City,'') City,
isnull([State],'') [State],isnull(Pincode,'') Pincode,isnull(Commission,0) Commission,GsTin from Doctor D1
where (AccountId=@AccId or @AccId ='' )
and (InstanceId=@InstId or @InstId = '' )
and (Id=@Id or @Id='')
and (Mobile=@Mobile or @Mobile='')
and not exists (select  isnull(Name,'') Name,isnull(Mobile,'') Mobile from Doctor D2
where (AccountId=@AccountId )
and (InstanceId=@InstanceId )
and (Id=@Id or @Id='')
and (Mobile=@Mobile or @Mobile='')
and isnull(D1.Name,'') = isnull(D2.Name,'')
and isnull(D1.Mobile,'') = ISNULL(D2.Mobile,''))
) A
order by A.Name offset @PageNo rows
FETCH next @PageSize rows only
  

END