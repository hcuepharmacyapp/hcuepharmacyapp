CREATE PROC [dbo].[Usp_ProductMerge](@AccountId CHAR(36), @productName VARCHAR(2000), @productId CHAR(36), @mergeProductId CHAR(36), @mergeType TINYINT)
-- @mergeType - 1 by product name
-- @mergeType - 2 by product id 
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @tableName VARCHAR(200)
	DECLARE @columnName VARCHAR(100)='ProductId'
	DECLARE @sqlQuery VARCHAR(MAX)

	IF @mergeType = 1
	BEGIN
		DECLARE product_cursor CURSOR FOR  
		SELECT MAX(p.Id) AS ProductId,RTRIM(LTRIM(p.Name)) AS ProductName FROM Product p WHERE AccountId=@AccountId
		AND RTRIM(LTRIM(p.Name)) = RTRIM(LTRIM(ISNULL(@productName,p.Name))) AND ISNULL(p.Status,1)=1
		GROUP BY RTRIM(LTRIM(p.Name)) HAVING COUNT(RTRIM(LTRIM(p.Name)))>1

		OPEN product_cursor   
		FETCH NEXT FROM product_cursor INTO @productId,@productName   
		WHILE @@FETCH_STATUS = 0   
		BEGIN   

			DECLARE table_cursor CURSOR FOR  
			SELECT OBJECT_NAME(sc.OBJECT_ID) AS TableName,sc.Name AS ColumnName FROM SYS.COLUMNS sc JOIN SYS.TABLES st on sc.OBJECT_ID=st.OBJECT_ID 
			WHERE st.Type='U'AND sc.Name = @columnName
			AND OBJECT_NAME(sc.OBJECT_ID) NOT IN('FinalParas','Mediplus_ClosingStock','Productstock_DataUpdate','ProductStock_imp','purchasetemp_paras','salesrettemp_paras','salestemp_paras','sndProductStock','tmpStockImport')
			AND OBJECT_NAME(sc.OBJECT_ID) NOT LIKE 'z%'
			GROUP BY OBJECT_NAME(sc.OBJECT_ID),sc.Name

			OPEN table_cursor   
			FETCH NEXT FROM table_cursor INTO @tableName,@columnName   
			WHILE @@FETCH_STATUS = 0   
			BEGIN   
				SET @sqlQuery = 'UPDATE tmp SET tmp.' + @columnName + ' = ''' + @productId + ''' FROM Product p JOIN ' + @tableName + ' tmp ON p.Id = tmp.'+ @columnName +' WHERE p.AccountId = ''' + @AccountId + ''' AND p.Id != ''' + @productId + ''' AND RTRIM(LTRIM(p.Name)) = ''' + @productName + ''''
				PRINT @sqlQuery
				EXEC(@sqlQuery)

				FETCH NEXT FROM table_cursor INTO @tableName,@columnName
			END   

			CLOSE table_cursor   
			DEALLOCATE table_cursor

			UPDATE Product SET Status = 2 WHERE AccountId = @AccountId AND Id != @productId AND Name = @productName

			FETCH NEXT FROM product_cursor INTO @productId,@productName
		END   

		CLOSE product_cursor   
		DEALLOCATE product_cursor
	END
	ELSE IF @mergeType = 2 
	BEGIN
		DECLARE table_cursor CURSOR FOR  
		SELECT OBJECT_NAME(sc.OBJECT_ID) AS TableName,sc.Name AS ColumnName FROM SYS.COLUMNS sc JOIN SYS.TABLES st on sc.OBJECT_ID=st.OBJECT_ID 
		WHERE st.Type='U'AND sc.Name = @columnName
		AND OBJECT_NAME(sc.OBJECT_ID) NOT IN('FinalParas','Mediplus_ClosingStock','Productstock_DataUpdate','ProductStock_imp','purchasetemp_paras','salesrettemp_paras','salestemp_paras','sndProductStock','tmpStockImport')
		AND OBJECT_NAME(sc.OBJECT_ID) NOT LIKE 'z%'
		GROUP BY OBJECT_NAME(sc.OBJECT_ID),sc.Name

		OPEN table_cursor   
		FETCH NEXT FROM table_cursor INTO @tableName,@columnName   
		WHILE @@FETCH_STATUS = 0   
		BEGIN   
			SET @sqlQuery = 'UPDATE tmp SET tmp.' + @columnName + ' = ''' + @productId + ''' FROM Product p JOIN ' + @tableName + ' tmp ON p.Id = tmp.'+ @columnName +' WHERE p.AccountId = ''' + @AccountId + ''' AND p.Id = ''' + @mergeProductId + ''''
			PRINT @sqlQuery
			EXEC(@sqlQuery)

			FETCH NEXT FROM table_cursor INTO @tableName,@columnName
		END   

		CLOSE table_cursor   
		DEALLOCATE table_cursor

		UPDATE Product SET Status = 2 WHERE AccountId = @AccountId AND Id = @mergeProductId
	END
	SET NOCOUNT OFF
END