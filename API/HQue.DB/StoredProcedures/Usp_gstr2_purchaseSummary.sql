/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 30/08/2017  Poongodi		Created
**12/10/2017   Sarubala		Implemented the stored values instead of calculation
**13/10/2017   nandhini		invoiceDate1 added for csv
*******************************************************************************/ 
create PROC [dbo].[Usp_gstr2_purchaseSummary](@Accountid  CHAR(36), 
                                  @InstanceId VARCHAR(36), 
                                  @Startdate  DATE, 
                                  @EndDate    DATE, 
                                  @GstinNo    VARCHAR(50), 
                                  @FilterType VARCHAR(10),
								  @IsRegister bit,
								  @IsInvDtSearch bit) 
AS 
  BEGIN 
	Declare @InvStartDt date, @InvEndDt date, @GrnStartDt date, @GrnEndDt Date
	 declare
   @invoiceDate1 datetime
      DECLARE @Instance TABLE 
        ( 
           id CHAR(36) 
        ) 

      IF ( @FilterType = 'branch' ) 
        BEGIN 
            INSERT INTO @instance 
            SELECT @Instanceid 
			set @GstinNo =NULL
        END 
      ELSE 
        BEGIN 
            INSERT INTO @instance 
            SELECT id 
            FROM   instance 
            WHERE  gstinno = @GstinNo 
        END 
		declare @LocType table (loctype int)
		declare @vendortype table (ventype int)
		
		declare @LocType1 table (loctype int)
		declare @vendortype1 table (ventype int)
		if (@IsRegister =1)
			begin
			insert into @LocType 
			values (1),(2)
			insert into @vendortype 
			values (1) 
			end
		else
			begin
			insert into @LocType 
			values (3)
			insert into @vendortype 
			values(1) , (2),(3) 
			insert into @LocType1
			values (1),(2)
			insert into @vendortype1 
			values  (2),(3) 
			end
		 
		 if (@IsInvDtSearch =1)
			Select  @InvStartDt =@Startdate, @InvEndDt =@EndDate , @GrnStartDt = NULL, @GrnEndDt = NULL
		 else
		 Select  @GrnStartDt =@Startdate, @GrnEndDt =@EndDate , @InvStartDt = NULL, @InvEndDt = NULL

		Select [PurchaseId], max([BranchName]) [BranchName],max([BranchGSTin]) [BranchGSTin],max([InvoiceDate]) [InvoiceDate],max([InvoiceNumber]) [InvoiceNumber],max([InvoiceDate1])[InvoiceDate1],
		max([RecipientName]) [RecipientName],Max([RecipientGSTin]) [RecipientGSTin],max([PlaceOfSupply]) [PlaceOfSupply],'Goods' 
             [IsGoods] ,
             'No' 
             [IsReverseCharge], 
              sum([Quantity]) [Quantity],
			  sum([FreeQty]) [FreeQty], max([SupplyType]) [SupplyType],[TaxRate],sum([ValueWithoutTax]) [ValueWithoutTax], [TaxRate] [GSTTotal]	 ,
			  sum([IGSTAmount]) [IGSTAmount],
			  sum([CGSTAmount]) [CGSTAmount],
			  sum([SGSTAmount]) [SGSTAmount],
			  sum([TaxAmount]) [TaxAmount],
			  sum([InvoiceValue]) [InvoiceValue]
			  from (
      SELECT instance.NAME + ' - ' + instance.area 
             AS 
             [BranchName],
             Instance.gstinno 
             [BranchGSTin], 
             vendor.gstinno 
             [RecipientGSTin], 
             Cast(vendorpurchase.invoicedate AS DATE) 
             AS 
             [InvoiceDate], 
			 	  convert(varchar,vendorpurchase.invoicedate,103)                    [InvoiceDate1], 
             vendorpurchase.invoiceno 
             AS 
             [InvoiceNumber], 
             vendor.NAME 
             AS 
             [RecipientName], 
              
             isnull(s.statecode, left(isnull(vendor.gstinno,''),2))  [PlaceOfSupply], 
             
             vendorpurchaseitem.packageqty 
             AS 
             [Quantity], 
             vendorpurchaseitem.freeqty 
             [FreeQty], 
             
             CASE Isnull(vendorpurchase.taxrefno, 0) 
               WHEN 1 THEN 
                 CASE Isnull(productstock.gsttotal, 
                      Isnull(vendorpurchaseitem.gsttotal, 0)) 
                   WHEN 0 THEN 'Zero Rated Supplies' 
                   ELSE 'Regular GST Supply' 
                 END 
             END 
             [SupplyType], 
          
             Isnull(ProductStock.gsttotal, 
             Isnull(VendorPurchaseItem.gsttotal, 0)) 
             AS 
             [TaxRate], 
             Round(Isnull(pocal.poval, 0), 3) 
             [ValueWithoutTax], 
             Isnull(ProductStock.gsttotal, 
             Isnull(VendorPurchaseItem.gsttotal, 0)) 
             AS 
             [GSTTotal], 
             CASE Isnull(vendor.locationtype, 1) 
               WHEN 2 THEN Tax.taxvalue 
               ELSE 0 
             END 
             [IGSTAmount], 
             CASE Isnull(vendor.locationtype, 1) 
               WHEN 1 THEN Round(Tax.taxvalue / 2, 3) 
               ELSE 0 
             END 
             [CGSTAmount], 
             CASE Isnull(vendor.locationtype, 1) 
               WHEN 1 THEN Round(Tax.taxvalue / 2, 3) 
               ELSE 0 
             END 
             [SGSTAmount], 
             
             Tax.taxvalue 
             [TaxAmount], 
             0.00 
             [Cess], 
             isnull(vendorpurchaseitem.Quantity,0) * ISNULL(vendorpurchaseitem.PurchasePrice,0)
             [InvoiceValue] ,
      vendorpurchase.id [PurchaseId]
      FROM   vendorpurchase 
             INNER JOIN (SELECT * 
                         FROM   @Instance) I 
                     ON i.id = vendorpurchase.instanceid 
             INNER JOIN (SELECT * 
                         FROM   vendorpurchaseitem (nolock) 
                         WHERE  accountid = @Accountid 
                                AND instanceid IN (SELECT id 
                                                   FROM   @Instance)) 
                        vendorpurchaseitem 
                     ON vendorpurchase.id = vendorpurchaseitem.vendorpurchaseid 
             INNER JOIN  (SELECT * 
                         FROM   vendor (nolock) 
                         WHERE  accountid = @Accountid and ( isnull(locationtype,1) in(select loctype from @LocType)
						 and isnull(vendortype,1) in (select ventype from @vendortype) 
						 or  isnull(locationtype,1) in(select loctype from @LocType1)
						 and isnull(vendortype,1) in (select ventype from @vendortype1)) ) vendor     
                     ON vendor.id = vendorpurchase.vendorid 
			 left join (select * from  state (nolock) ) s on s.id = vendor.stateid
             INNER JOIN (SELECT * 
                         FROM   productstock (nolock) 
                         WHERE  accountid = @Accountid 
                                AND instanceid IN (SELECT id 
                                                   FROM   @Instance)) 
                        productstock 
                     ON productstock.id = vendorpurchaseitem.productstockid 
             LEFT JOIN (SELECT * 
                        FROM   product (nolock) 
                        WHERE  accountid = @Accountid) product 
                    ON product.id = productstock.productid 
             INNER JOIN (SELECT * 
                         FROM   instance (nolock) 
                         WHERE  accountid = @Accountid 
                                AND id IN (SELECT id 
                                           FROM   @Instance)) Instance 
                     ON Instance.id = vendorpurchaseitem.instanceid 

             CROSS apply (SELECT (isnull(vendorpurchaseitem.Quantity,0) * isnull(vendorpurchaseitem.PurchasePrice,0)) - case							when vendorpurchase.TaxRefNo = 1 then ISNULL(vendorpurchaseitem.GstValue,0) else ISNULL										(vendorpurchaseitem.VatValue,0) end  [Poval]) AS pocal 

             CROSS apply (SELECT Round(case when vendorpurchase.TaxRefNo = 1 then ISNULL(vendorpurchaseitem.GstValue,0) else							ISNULL (vendorpurchaseitem.VatValue,0) end ,2) [TaxValue]) Tax 

      WHERE  Cast(vendorpurchase.createdat AS DATE) BETWEEN 
              Cast(isnull(@GrnStartDt,vendorpurchase.createdat) AS DATE)  AND  Cast(isnull(@GrnEndDt,vendorpurchase.createdat) AS DATE) 
			  and Cast(vendorpurchase.InvoiceDate AS DATE) BETWEEN 
              Cast(isnull(@InvStartDt,vendorpurchase.invoiceDate) AS DATE)  AND  Cast(isnull(@InvEndDt,vendorpurchase.invoiceDate) AS DATE) 
             AND vendorpurchase.accountid = @Accountid 
             AND Isnull(vendorpurchase.cancelstatus, 0) = 0 
             AND ( vendorpurchaseitem.status IS NULL 
                    OR vendorpurchaseitem.status = 1 ) 
					) one Group by [PurchaseId],[TaxRate]
					order by [BranchName], [InvoiceDate],[InvoiceDate1] desc,[InvoiceNumber] desc
  END