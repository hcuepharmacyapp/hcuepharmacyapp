
--usp_get_sale_history @AccountId=N'18204879-99ff-4efd-b076-f85b4a0da0a3',@InstanceId=N'013513b1-ea8c-4ea8-9fed-054b260ee197'
Create proc dbo.usp_get_sale_history_Count  (
 @AccountId nvarchar(36),@InstanceId nvarchar(36),
 @SearchColName varchar(50),
 @SearchOption varchar(50),
 @SearchValue varchar(50),
 @fromDate varchar(15),
 @Todate varchar(15)  )
 as 
  begin
   declare @isDetail bit = 0
  Declare @From_BillDt date = null, @To_BillDt date = null,@From_ExpiryDt date = null, @To_ExpiryDt date = null,
  @BtchNo varchar(50) = '',@customerName varchar(200) = '',@customerMobile varchar(100) = '',
   @doctorName  varchar(100) = null,
  @doctorMobile varchar(100) = null,
  @product varchar(100) = null , 
  @billNo varchar(100) = '',
  @from_Qty numeric(10,2)  =null,
  @to_Qty numeric(10,2)  =null,
  @from_Vat numeric(10,2)  =null,
  @to_Vat numeric(10,2)  =null,
   @from_mrp numeric(10,2)  =null,
  @to_mrp numeric(10,2)  =null,
   @from_discount numeric(10,2)  =null,
  @to_discount numeric(10,2)  =null 

  if (@SearchColName ='batchNo')
		select  @BtchNo = isnull(@SearchValue,''), @isDetail  =1

   else  if (@SearchColName ='customerName')
		set @customerName = isnull(@SearchValue,'')

  else   if (@SearchColName ='billNo')
		select  @billNo = isnull(@SearchOption ,'')+ isnull(@SearchValue,'') 
 
 else  if (@SearchColName ='customerMobile')
		set @customerMobile = isnull(@SearchValue,'')
 else  if (@SearchColName ='doctorName')
		set @doctorName = isnull(@SearchValue,'')

 else  if (@SearchColName ='doctorMobile')
		set @doctorMobile = isnull(@SearchValue,'')
else if (@SearchColName ='product')
		select @product = isnull(@SearchValue,''), @isDetail =1
else if (@SearchColName ='billDate')
		select @From_BillDt =  @fromDate ,@To_BillDt = @Todate
else if (@SearchColName ='expiry')
		select @From_ExpiryDt =  @fromDate ,@To_ExpiryDt = @Todate, @isDetail =1
else if (@SearchColName ='quantity')
begin 
	if (@SearchOption='equal')
		select @from_Qty =  cast (@SearchValue as numeric (10,2))    ,@to_Qty =  cast (@SearchValue as numeric (10,2)), @isDetail =1
	else if (@SearchOption='greater')
			select @from_Qty =  cast (@SearchValue as numeric (10,2))+0.01    ,@to_Qty =  NULL, @isDetail =1
	else if (@SearchOption='less')
			select @from_Qty =  NULL, @to_Qty =  cast (@SearchValue as numeric (10,2))-0.01   ,@isDetail =1
end 
 else if (@SearchColName ='vat')
begin 
	if (@SearchOption='equal')
		select @from_vat =  cast (@SearchValue as numeric (10,2))    ,@to_vat =  cast (@SearchValue as numeric (10,2)), @isDetail =1
	else if (@SearchOption='greater')
			select @from_vat =  cast (@SearchValue as numeric (10,2))+0.01    ,@to_vat =  NULL, @isDetail =1
	else if (@SearchOption='less')
			select @from_vat =  0.01, @to_vat =  cast (@SearchValue as numeric (10,2))-0.01   ,@isDetail =1
end 
 else if (@SearchColName ='mrp')
begin 
	if (@SearchOption='equal')
		select @from_Mrp =  cast (@SearchValue as numeric (10,2))    ,@to_Mrp =  cast (@SearchValue as numeric (10,2)), @isDetail =1
	else if (@SearchOption='greater')
			select @from_Mrp =  cast (@SearchValue as numeric (10,2))+0.01    ,@to_Mrp =  NULL, @isDetail =1
	else if (@SearchOption='less')
			select @from_Mrp =  NULL, @to_Mrp =  cast (@SearchValue as numeric (10,2))-0.01   ,@isDetail =1
end 

else if (@SearchColName ='discount')
begin 
	if (@SearchOption='equal')
		select @from_discount =  cast (@SearchValue as numeric (10,2))    ,@to_discount =  cast (@SearchValue as numeric (10,2)) 
	else if (@SearchOption='greater')
			select @from_discount =  cast (@SearchValue as numeric (10,2))+0.01    ,@to_discount =  NULL
	else if (@SearchOption='less')
			select @from_discount =  0.01, @to_discount =  cast (@SearchValue as numeric (10,2))-0.01   
end 

--select @InvoiceNo= '121',@AccountId=N'18204879-99ff-4efd-b076-f85b4a0da0a3',@InstanceId=N'013513b1-ea8c-4ea8-9fed-054b260ee197'
if (@isDetail =0)
begin
SELECT count(1) [SalesCount] FROM Sales WHERE   Sales.AccountId  =  @AccountId AND Sales.InstanceId  =  @InstanceId
and isnull(sales.Name ,'')  like isnull(@customerName,'') +'%'
and isnull(sales.Mobile ,'') like isnull(@customerMobile,'') +'%'
and isnull( sales.DoctorName,'')  like isnull(@doctorName,'') +'%'
and  isnull(sales.DoctorMobile,'')  like isnull(@doctorMobile,'') +'%'
and isnull(sales.InvoiceSeries,'')+isnull( sales.InvoiceNo,'') like isnull(@billNo,'')+'%'
 and cast(sales.InvoiceDate as date )between isnull(@From_BillDt, InvoiceDate) and isnull(@To_BillDt, InvoiceDate)
  and sales.Discount  between  isnull(@from_discount,sales.Discount) and isnull(@to_discount,sales.Discount)
 end 
 else
 begin
SELECT count(1) [SalesCount]  FROM Sales WHERE   Sales.AccountId  =  @AccountId AND Sales.InstanceId  =  @InstanceId
and sales.id in (SELECT SalesItem.SalesId 
 
 FROM SalesItem INNER JOIN ProductStock ON ProductStock.Id = SalesItem.ProductStockId 
 --INNER JOIN Product ON Product.Id = ProductStock.ProductId  
 WHERE SalesItem.InstanceId  =  @InstanceId AND SalesItem.AccountId  =  @AccountId  
 --and isnull(product.Name,'') like isnull(@product,'') +'%'
 and ProductStock.ProductId like isnull(@product, '')+'%'
  and isnull(productstock.BatchNo ,'')like isnull(@BtchNo,'') +'%'
  and productstock.ExpireDate between isnull(@From_ExpiryDt, productstock.ExpireDate) and isnull(@To_ExpiryDt, productstock.ExpireDate)
  and SalesItem.Quantity between isnull(@from_qty,SalesItem.Quantity) and isnull(@to_Qty,SalesItem.Quantity) 
   and SalesItem.SellingPrice between isnull(@from_mrp,SalesItem.SellingPrice) and isnull(@to_mrp,SalesItem.SellingPrice) 
     and productstock.VAT between isnull(@from_Vat,productstock.VAT) and isnull(@to_Vat,productstock.VAT) 
	 )
 
 end
 select @From_ExpiryDt,@from_qty,@from_mrp,@to_mrp,@product,@BtchNo,@From_ExpiryDt,@To_ExpiryDt,@from_qty,@to_Qty,@billNo    
  
 end