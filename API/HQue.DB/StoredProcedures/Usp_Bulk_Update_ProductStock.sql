﻿/*
---- Usage
DECLARE @StockListWithQty VARCHAR(MAX) = '000012a9-e5f1-4ca2-9e87-e61e5ddb03a6~10,0000520f-d8a8-4978-bfd7-5f76953de527~1'
DECLARE @AccountId CHAR(36) = 'af1f88a4-3ff0-48b9-b18a-c7497c6cd4a9'
DECLARE @InstanceId CHAR(36) = 'b1edce81-dfce-4f75-ba69-d8d7af26cbd2'
DECLARE @UpdatedBy VARCHAR(MAX) = 'admin'
*/
CREATE PROCEDURE [dbo].[Usp_Bulk_Update_ProductStock](@AccountId CHAR(36), @InstanceId CHAR(36), @StockListWithQty VARCHAR(MAX), @UpdatedBy CHAR(36))
AS
BEGIN
	SET DEADLOCK_PRIORITY LOW

	DECLARE @xml XML 
	SET @xml = CAST('<ProductStock><ProductStockId>'+REPLACE(REPLACE(REPLACE(ISNULL(@StockListWithQty,''),'~','</ProductStockId><RequiredStock>'),'''',''),',','</RequiredStock></ProductStock><ProductStock><ProductStockId>')+'</RequiredStock></ProductStock>' AS XML)

	UPDATE PS SET PS.Stock = PS.Stock - (R.TransStock), PS.UpdatedAt = GETDATE(), PS.UpdatedBy = @UpdatedBy, PS.TransactionId = NEWID() FROM 
	(
		SELECT Tab.Col.value('ProductStockId[1]', 'CHAR(36)') AS ProductStockId,
		Tab.Col.value('RequiredStock[1]', 'DECIMAL(18,2)') AS TransStock
		FROM @xml.nodes('ProductStock') AS Tab(Col)
	) R
	INNER JOIN (SELECT * FROM ProductStock WHERE AccountId = @AccountId AND InstanceId = @InstanceId) PS ON PS.Id = R.ProductStockId
	WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId

	SELECT PS.ProductId,ProductStockId,UpdatedStock,PS.Stock,PS.BatchNo,PS.ExpireDate,PS.TransactionId,PS.UpdatedAt FROM 
	(
		SELECT Tab.Col.value('ProductStockId[1]', 'CHAR(36)') AS ProductStockId,
		Tab.Col.value('RequiredStock[1]', 'DECIMAL(18,2)') AS UpdatedStock
		FROM @xml.nodes('ProductStock') AS Tab(Col)
	) R
	INNER JOIN (SELECT * FROM ProductStock WHERE AccountId = @AccountId AND InstanceId = @InstanceId) PS ON PS.Id = R.ProductStockId
	WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId
END