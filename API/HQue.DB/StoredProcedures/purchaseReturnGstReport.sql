
Create PROCEDURE [dbo].[purchaseReturnGstReport](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime )
 AS

select S.ReturnDate, S.ReturnNo, sum(S.GrossAmount) ReturnAmount, sum(S.GstAmount) GstAmount, sum(S.Gst0_PurchaseReturnValue) Gst0_PurchaseReturnValue, sum(S.Gst0_Value) Gst0_Value, sum(S.Gst0_PurchaseReturnValue)+sum(S.Gst0_Value) Gst0_TotalPurchaseReturnValue, sum(S.Gst5_PurchaseReturnValue) Gst5_PurchaseReturnValue, sum(S.Gst5_Value) Gst5_Value, sum(S.Gst5_PurchaseReturnValue)+sum(S.Gst5_Value) Gst5_TotalPurchaseReturnValue, sum(S.Gst12_PurchaseReturnValue) Gst12_PurchaseReturnValue, sum(S.Gst12_Value) Gst12_Value, sum(S.Gst12_PurchaseReturnValue)+sum(S.Gst12_Value) Gst12_TotalPurchaseReturnValue, sum(S.Gst18_PurchaseReturnValue) Gst18_PurchaseReturnValue, sum(S.Gst18_Value) Gst18_Value, sum(S.Gst18_PurchaseReturnValue)+sum(S.Gst18_Value) Gst18_TotalPurchaseReturnValue, sum(S.Gst28_PurchaseReturnValue) Gst28_PurchaseReturnValue, sum(S.Gst28_Value) Gst28_Value, sum(S.Gst28_PurchaseReturnValue)+sum(S.Gst28_Value) Gst28_TotalPurchaseReturnValue, sum(S.GstOther_PurchaseReturnValue) GstOther_PurchaseReturnValue, sum(S.GstOther_Value) GstOther_Value, sum(S.GstOther_PurchaseReturnValue)+sum(S.GstOther_Value) GstOther_TotalPurchaseReturnValue from

(select vr.ReturnDate, (isnull(vr.Prefix,'')+vr.ReturnNo) ReturnNo, Purchase.PurchaseReturnValue as PurchaseAmount,vri.GstAmount,Purchase.GrossValue as GrossAmount,
case isnull(vri.GstTotal,0) when 0 then Purchase.PurchaseReturnValue else 0 end Gst0_PurchaseReturnValue,
case isnull(vri.GstTotal,0) when 0 then vri.GstAmount else 0 end Gst0_Value,
case isnull(vri.GstTotal,0) when 5 then Purchase.PurchaseReturnValue else 0 end Gst5_PurchaseReturnValue,
case isnull(vri.GstTotal,0) when 5 then vri.GstAmount else 0 end Gst5_Value,
case isnull(vri.GstTotal,0) when 12 then Purchase.PurchaseReturnValue else 0 end Gst12_PurchaseReturnValue,
case isnull(vri.GstTotal,0) when 12 then vri.GstAmount else 0 end Gst12_Value,
case isnull(vri.GstTotal,0) when 18 then Purchase.PurchaseReturnValue else 0 end Gst18_PurchaseReturnValue,
case isnull(vri.GstTotal,0) when 18 then vri.GstAmount else 0 end Gst18_Value,
case isnull(vri.GstTotal,0) when 28 then Purchase.PurchaseReturnValue else 0 end Gst28_PurchaseReturnValue,
case isnull(vri.GstTotal,0) when 28 then vri.GstAmount else 0 end Gst28_Value,
case when isnull(vri.GstTotal,0) not in (0,5,12,18,28) then  Purchase.PurchaseReturnValue else 0 end GstOther_PurchaseReturnValue,
case when isnull(vri.GstTotal,0) not in (0,5,12,18,28) then vri.GstAmount else 0 end GstOther_Value
from VendorReturn vr with(nolock) inner join
VendorReturnItem vri with(nolock) on vr.Id=vri.VendorReturnId
cross apply (select ((isnull(vri.Quantity,0) * isnull(vri.ReturnPurchasePrice,0))-isnull(vri.GstAmount,0)) [PurchaseReturnValue],
					((isnull(vri.Quantity,0) * isnull(vri.ReturnPurchasePrice,0))) [GrossValue]	) as Purchase
where vr.AccountId=@AccountId and vr.InstanceId=@InstanceId and vr.CreatedAt between @StartDate and @EndDate) S
group by S.ReturnDate, S.ReturnNo
order by S.ReturnDate desc

