 /*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**09/05/17     Poongodi R		Product Instance Join changed from Inner to left
**27/07/17     Poongodi R		Distinct Productstock taken from Transfer and Vendor Return 
**10/08/17     Poongodi R		Productstock TaxRefNo added by POongodi
**17/08/17	   Settu			GstTotal column added
**13/09/17	   Sarubala			Item product name added
**16/10/17	   Settu			GstTotal referred from purchase instead of stock
**05/04/17	   Poongodi			Productinstance table select Qry changed to avoid duplicates 
*******************************************************************************/
create procedure [dbo].[usp_GetVendorPurchaseItemsHistory] (@VendorPurchaseId char(36))
as
begin

declare @Instanceid char(36)
select @instanceid = Instanceid from vendorpurchase where id =@VendorPurchaseId

SELECT                              vpi.Id as vpiid,
									isnull(vpi.ProductName,'') as vpiProductName,
                                    vpi.ProductStockId as vpiProductStockId,
	                                vpi.PackageSize as vpiPackageSize,
	                                vpi.PackageQty as vpiPackageQty,
	                                vpi.PackagePurchasePrice as vpiPackagePurchasePrice,
	                                vpi.PackageSellingPrice as vpiPackageSellingPrice,
                                    vpi.PackageMRP as vpiPackageMRP,
	                                vpi.VendorPurchaseId as vpiVendorPurchaseId,
	                                vpi.Quantity as vpiQuantity,
                                    vpi.Quantity as vpiActualQty,
	                                vpi.PurchasePrice as vpiPurchasePrice,
	                                vpi.AccountId as vpiAccountId,
	                                vpi.InstanceId as vpiInstanceId,
	                                vpi.CreatedBy as vpiCreatedBy,
                                    vpi.UpdatedBy as vpiUpdatedBy,
	                                vpi.FreeQty as vpiFreeQty,
	                                vpi.Discount as vpiDiscount,
									vpi.MarkupPerc as vpiMarkupPerc,
									vpi.SchemeDiscountPerc as vpiSchemeDiscountPerc,
	                                vpi.fromDcId as vpifromDcId,
	                                vpi.fromTempId as vppifromTempId,
                                    isnull(vpi.IsPoItem,0) as vpiIsPoItem,	
									vpi.HsnCode,		
									vpi.Igst,
									vpi.Cgst,
									vpi.Sgst,		
									vpi.GstTotal,					
                                    ps.SellingPrice AS psSellingPrice,
                                    ps.MRP AS psMRP,
	                                ps.VAT AS psVAT,

									vpi.Igst AS psIgst,
									vpi.Cgst AS psCgst,
									vpi.Sgst AS psSgst,
									vpi.GstTotal AS psGstTotal,
									ps.HsnCode AS psHsnCode,

	                                ps.CST AS psCST,
                                    ps.ProductId AS psProductId,
	                                ps.BatchNo AS psBatchNo,
	                                ps.ExpireDate AS psExpireDate, 
                                    ps.Stock AS psStock,
	                                ps.AccountId AS psAccountId,
	                                ps.InstanceId AS psInstanceId,
                                    ps.CreatedBy AS psCreatedBy,
	                                ps.UpdatedBy AS psUpdatedBy,
	                                ps.Id AS psId,
                                    ps.PurchaseBarcode AS psPurchaseBarcode,
				    isnull(ps.TaxRefNo,0) psTaxRefno,
	                                p.Name AS pName,
	                                p.Id AS pId,
                                    pin.RackNo AS pRackNo,
									pin.BoxNo AS pBoxNo,
	                                p.Eancode AS pEancode,
	                                isnull(ps.Eancode,null) AS psEancode,
									Pa.Debit as PaDebit,
									returnItem.returnProductStockId as vpiReturnProductStockId,
									StockTransfer.Id as TransferId



	                                             FROM VendorPurchaseItem as vpi INNER JOIN ProductStock ps ON ps.Id = vpi.ProductStockId and (vpi.Status is null or vpi.Status = 1) 
                                                 INNER JOIN Product as p ON p.Id = ps.ProductId 
		  					                      Left JOIN (select  Productid , instanceid, max(isnull(rackno,'') ) rackno, max(isnull(BoxNo,'') ) BoxNo
												  from ProductInstance where instanceid =@instanceid group by  Productid , instanceid)  pin ON pin.ProductId = ps.ProductId and pin.InstanceId = vpi.InstanceId


												 --For Payment Checking
												 left join (												
                                                 select sum(p.Debit) as Debit,p.VendorPurchaseId,p.PaymentType from Payment p 
												 inner join VendorPurchase as vp on p.VendorPurchaseId = vp.Id 
												 where p.VendorPurchaseId =  @VendorPurchaseId and p.PaymentType ='PurchasePaid'
												 group by p.VendorPurchaseId,p.PaymentType) as Pa on Pa.VendorPurchaseId = vpi.VendorPurchaseId

												 --For Return Checking
												 --left join (
												 --select distinct vpi.Id,vri.ProductStockId from VendorPurchaseItem as vpi
             --                                    inner join VendorReturn as vr on vr.VendorPurchaseId = vpi.VendorPurchaseId
             --                                    left join VendorReturnItem as vri on vr.id = vri.VendorReturnId and vri.ProductStockId = vpi.ProductStockId
             --                                    where (vpi.Status is null or vpi.Status = 1) and vpi.VendorPurchaseId = @VendorPurchaseId) as returnItem 
												 --on  returnItem.Id = vpi.Id

												 left join(
												 select ProductStockId as returnProductStockId,max(Id)  as venodrpurchaseitemid from VendorPurchaseItem where 
												  VendorPurchaseId=@VendorPurchaseId and ProductStockId in (select vri.ProductStockId from VendorReturn as vr inner join	
															 VendorReturnItem as vri on vr.Id = vri.VendorReturnId where vr.VendorPurchaseId =@VendorPurchaseId)  group by ProductStockId)
												  as returnItem  on  returnItem.venodrpurchaseitemid = vpi.Id

												 --For Transfer Checking
												left join (
												select sti.ProductStockId, max(sti.Id) id  
												from StockTransferItem as sti inner join vendorpurchaseitem as vpi on sti.ProductStockId = vpi.ProductStockId 
												and vpi.VendorPurchaseId = @VendorPurchaseId  	group by sti.ProductStockId		)
												as StockTransfer on StockTransfer.ProductStockId = vpi.ProductStockId

												 
												  
		  					                     and pin.InstanceId = ps.InstanceId 		  					                    
		     				                     WHERE  vpi.VendorPurchaseId = @VendorPurchaseId  												
												 ORDER BY vpi.VendorPurchaseItemSno asc
end








