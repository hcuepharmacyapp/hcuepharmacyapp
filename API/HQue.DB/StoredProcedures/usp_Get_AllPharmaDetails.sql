  --usp_Get_AllPharmaDetails 0,'2017-02-07','2017-02-07','2017-01-01','2017-01-31','chennai'
Create Proc dbo.usp_Get_AllPharmaDetails(@RegisterType int,
@FromDate Datetime,
@Todate Datetime,
@LastMonth_FromDate Datetime,
@LastMonth_Todate Datetime,
@City varchAR(500) =''
)
As 
begin
 
 set @City = isnull(@City,'')
---get pharma DETAILS
Create table #PharmaDetail    (Pharmaid char(50), SalesCount Int, SalesAmountBeforeDisocunt Numeric(20,2),
SalesDisocuntedAmount Numeric(20,2),POCount int, PurchaseAmountBeforeDiscount Numeric(20,2),
PurchaseDisocuntedAmount Numeric(20,2),
saleslastmonthcount int,
salesLastmonthAmountBeforeDiscount  Numeric(20,2),
salesLastMonthDiscountedAmount  Numeric(20,2),
purchasetotalmonthCount  int,

purchaseLastMonthBeforeDisocunt  Numeric(20,2),
pucrhaseLastMonthDiscountedAmount  Numeric(20,2))
Create Nonclustered index idx_Pharmatemp
on #PharmaDetail (Pharmaid )
include(SalesAmountBeforeDisocunt,SalesDisocuntedAmount,PurchaseAmountBeforeDiscount)
						--SALES
 
insert into #PharmaDetail (pharmaid,SalesCount,SalesAmountBeforeDisocunt,SalesDisocuntedAmount)
select   DISTINCT
                      SI.instanceId as PharmaId,
					
					  count(si.salescount) as SalesInvoiceCount,                    
					  sum(isnull(si.salesamountBeforeDiscount,0)) as SalesAmountBeforeDisocunt,
					  sum(isnull(si.salesdiscountedAmount,0)) as SalesDisocuntedAmount              
               from  
                    (select S.InstanceId,count(s.InvoiceNo) as salescount,
			   sum(  case  isNull(sii.SellingPrice,0)
                                when 0 
	                            then (productstock.SellingPrice)*sii.Quantity
	                            else (sii.SellingPrice)*sii.Quantity
	                            end
                               ) as salesamountBeforeDiscount,
			       sum(case
									    when  ISNULL(s.Discount,0) <> 0 then 
										      case 
											        when sii.SellingPrice is not null 
													then ((sii.SellingPrice*s.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*s.Discount)/100)*sii.Quantity
											  end
                                      when isnull(sii.Discount,0) <> 0 then
									          case
											         when sii.SellingPrice is not null 
													then ((sii.SellingPrice*sii.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*sii.Discount)/100)*sii.Quantity
											  end
                                       else								         
											         0											 
									 end								     
								) as salesdiscountedAmount  
                from Sales as  s inner join  Instance as i  
				     ON S.InstanceId = I.ID AND S.accountid =i.accountid 
				 inner join Account as a on a.Id = i.AccountId
						 inner join SalesItem as  sii on s.Id = sii.SalesId
				                 inner join productstock as productstock on productstock.id=sii.productstockid
				                 where  cast(s.CreatedAt as date)  between @FromDate and @Todate
								  and  (((a.RegisterType = @RegisterType) and (@RegisterType >0)) 
								 or ((a.RegisterType in (1,2,4))  and (@RegisterType =0)))	
								 and (((i.city =@City) and (@city <>'')) or (@city =''))
				                 group by sii.SalesId,s.InstanceId
                 ) as  SI
			 
                       group by SI.InstanceId


					   --PURCHASE
 
insert into #PharmaDetail (pharmaid,POCount,PurchaseAmountBeforeDiscount,PurchaseDisocuntedAmount)
					   select   DISTINCT
                     i.id as PharmaId,
					 
					  count(vpi.purchaseCount) as PurchaseInvoiceCount,                  
					  sum(isnull(vpi.purchasebeforedisocunt,0)) as PurchaseAmountBeforeDiscount,
					  sum(isnull(vpi.pucrhasediscountedAmount,0)) as PurchaseDisocuntedAmount
               from Instance as i  
				   inner join 
                 (select count(vpp.InvoiceNo) as purchaseCount,vpii.InstanceId,         
                       sum(case
									    when  ISNULL(vpp.Discount,0) <> 0 then 										     
											       ((vpii.PurchasePrice * vpp.Discount)/100)*vpii.Quantity
												   else (vpii.PurchasePrice * vpii.Discount/100)* vpii.Quantity                    	 
									 end								     
								) as pucrhasediscountedAmount,
								sum(vpii.PurchasePrice * vpii.Quantity) as purchasebeforedisocunt 
           from VendorPurchase as vpp inner join VendorPurchaseItem as vpii on vpp.Id = vpii.VendorPurchaseId 
		   where  cast(vpp.CreatedAt as date)   between @FromDate and @Todate
		   group by vpii.VendorPurchaseId,vpii.InstanceId 
                 )as vpi   on i.id = vpi.InstanceId
				   inner join Account as a on a.Id = i.AccountId
				   where (((a.RegisterType = @RegisterType) and (@RegisterType >0)) 
				  or ((a.RegisterType in (1,2,4))  and (@RegisterType =0)))	
				  and (((i.city =@City) and (@city <>'')) or (@city =''))
                       group by i.id 

					   --SALES LAST MONTH
					   
insert into #PharmaDetail (pharmaid,saleslastmonthcount,salesLastmonthAmountBeforeDiscount,salesLastMonthDiscountedAmount)
					   select   DISTINCT
                      i.Id as PharmaId,
					 
					   count(sis.saleslastmonthcount) as saleslastmonthcount,					
					  sum(isnull(sis.saleslastmonthamountBeforeDiscount,0)) as salesLastmonthAmountBeforeDiscount,
					  sum(isnull(sis.saleslastmonthdiscountedAmount,0)) as salesLastMonthDiscountedAmount
               from 
                   Instance as i  
				   inner join (select sii.InstanceId,count(s.InvoiceNo) as saleslastmonthcount,			
			   sum(    
                                 isNull(sii.SellingPrice,productstock.SellingPrice)*sii.Quantity
	                            
                               ) as saleslastmonthamountBeforeDiscount,
			       sum(case
									    when  ISNULL(s.Discount,0) <> 0 then 
										      case 
											        when sii.SellingPrice is not null 
													then ((sii.SellingPrice*s.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*s.Discount)/100)*sii.Quantity
											  end
                                      when isnull(sii.Discount,0) <> 0 then
									          case
											         when sii.SellingPrice is not null 
													then ((sii.SellingPrice*sii.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*sii.Discount)/100)*sii.Quantity
											  end
                                       else     							         
											         0											 
									 end								     
								) as saleslastmonthdiscountedAmount  
                from Sales as  s inner join SalesItem as  sii on s.Id = sii.SalesId
								 inner join productstock as productstock on productstock.id=sii.productstockid
				                 where  cast(s.CreatedAt as date) between @LastMonth_FromDate and @LastMonth_Todate
				                 group by sii.SalesId,sii.InstanceId				
                 ) as sis on i.id = sis.InstanceId
				  inner join Account as a on a.Id = i.AccountId
				   where (((a.RegisterType = @RegisterType) and (@RegisterType >0)) 
				  or ((a.RegisterType in (1,2,4))  and (@RegisterType =0)))	
				    and (((i.city =@City) and (@city <>'')) or (@city =''))
                       group by i.Id,i.Name

					   --pURCHASE LAST MONTH

insert into #PharmaDetail (pharmaid,purchasetotalmonthCount,purchaseLastMonthBeforeDisocunt,pucrhaseLastMonthDiscountedAmount)
					   select   DISTINCT
                      i.Id as PharmaId,
				 
					     count(vpis.purchasetotalmonthCount) as purchasetotalmonthCount,                     
					  sum(isnull(vpis.purchaselastmonthbeforedisocunt,0)) as purchaseLastMonthBeforeDisocunt,
					  sum(isnull(vpis.pucrhaselastmonthdiscountedAmount,0)) as pucrhaseLastMonthDiscountedAmount
               from 
                   Instance as i  
				     inner join (select count(vpp.InvoiceNo) as purchasetotalmonthCount,vpii.InstanceId,           
                       sum(case
									    when  ISNULL(vpp.Discount,0) <> 0 then 										     
											       ((vpii.PurchasePrice * vpp.Discount)/100)*vpii.Quantity
												   else (vpii.PurchasePrice * vpii.Discount/100)* vpii.Quantity                    	 
									 end								     
								) as pucrhaselastmonthdiscountedAmount,

								sum(vpii.PurchasePrice * vpii.Quantity) as purchaselastmonthbeforedisocunt 
           from VendorPurchase as vpp inner join VendorPurchaseItem as vpii on vpp.Id = vpii.VendorPurchaseId 
		   where  cast(vpp.CreatedAt as date)  between @LastMonth_FromDate and @LastMonth_Todate
		   group by vpii.VendorPurchaseId,vpii.InstanceId                            
                 ) as vpis on i.id = vpis.InstanceId
				  inner join Account as a on a.Id = i.AccountId
				   where (((a.RegisterType = @RegisterType) and (@RegisterType >0)) 
				  or ((a.RegisterType in (1,2,4))  and (@RegisterType =0)))	
				    and (((i.city =@City) and (@city <>'')) or (@city =''))
                       group by i.Id,i.Name


select 
 /*,  pay.paymentDate paiddate, pay.cost, pay.quantity, pay.discount, pay.discountType, pay.tax, maxpay.paidAmt, pay.BalanceReason */
                 sum(Isnull(SalesCount,0)) [SalesInvoiceCount],
sum(Isnull(SalesAmountBeforeDisocunt,0)-Isnull(SalesDisocuntedAmount,0)) [SalesTotalValue],
sum(Isnull(POCount,0)) [PurchaseInvoiceCount],

sum(Isnull(SalesAmountBeforeDisocunt,0)-Isnull(PurchaseDisocuntedAmount,0)) [PurchaseTotalValue],
sum(Isnull(saleslastmonthcount,0)) [Saleslastmonthcount],
sum(Isnull(salesLastmonthAmountBeforeDiscount,0)-Isnull(salesLastMonthDiscountedAmount,0)) [SaleslastmonthTotalValue],
sum(Isnull(salesLastMonthDiscountedAmount,0)) [salesLastMonthDiscountedAmount],
sum(Isnull(purchasetotalmonthCount,0)) [purchasetotalmonthCount],

sum(Isnull(purchaseLastMonthBeforeDisocunt,0)-Isnull(pucrhaseLastMonthDiscountedAmount,0)) [PurchaseTotalmonthValue],
sum(Isnull(pucrhaseLastMonthDiscountedAmount,0)) [pucrhaseLastMonthDiscountedAmount],
sum(Isnull(SalesAmountBeforeDisocunt,0)) [SalesAmountBeforeDisocunt],
sum(Isnull(PurchaseAmountBeforeDiscount,0)) [PurchaseAmountBeforeDiscount] ,
 0 as count, isnull(i.id,'') PharmaId,  isnull(a.Name,'') as GroupName,isnull(i.Name,'') as PharmaName, 
isnull(i.City,'')City ,isnull(i.Area,'') as PharmaArea,a.RegisterType ,
 i.bdoName BdoName, i.rmName RmName, i.contactName ContactName, i.ContactMobile ContactMobile, i.contactEmail ContactEmail,
  i.secondaryName SecondaryName, i.secondaryMobile SecondaryMobile , i.secondaryEmail SecondaryEmail, 
 a.RegisterType, i.lastLoggedOn , '' LastLoggedOnText, 0 as InvoiceValue, null PaidDateText ,''  IsFullPaid ,'' BalanceReasonText ,
convert(varchar(10), max( a.RegistrationDate) ,103 )  RegistrationDate   from account a left join instance i on a.id = i.accountId
 
                     
						 left join #PharmaDetail  pd on pd.PharmaId = i.ID  
						  where (((a.RegisterType = @RegisterType) and (@RegisterType >0)) 
				  or ((a.RegisterType in (1,2,4))  and (@RegisterType =0)))	
				    and (((i.city =@City) and (@city <>'')) or (@city =''))
group by  i.id  ,  a.Name ,i.Name  ,i.City,i.Area  ,a.RegisterType , i.bdoName, i.rmName, i.contactName, i.ContactMobile, i.contactEmail, 
i.secondaryName, i.secondaryMobile, i.secondaryEmail, a.RegisterType, i.lastLoggedOn
order by 2 desc
/* ,pay.paymentDate  , pay.cost, pay.quantity, pay.discount, pay.discountType, pay.tax, maxpay.paidAmt, pay.BalanceReason */
          
 drop table #PharmaDetail
-----eDGE DETAILS
 end 