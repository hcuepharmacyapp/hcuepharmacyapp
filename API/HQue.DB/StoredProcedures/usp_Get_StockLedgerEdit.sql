﻿CREATE Proc dbo.usp_Get_StockLedgerEdit(@AccountId char(36),@InstanceId char(36), @ProductName varchar(2000))
as
begin
Declare @product varchar(2000) = null
print @ProductName
print '123'
print @product
if (@ProductName=null or @ProductName = '')
begin
print '124'
select @product = isnull(@ProductName,'')
end
else
begin
print '125'
select @product = @ProductName
end

Select max(psid) [stockid], Name, productId , Sum(ImportQty) ImportQty, Sum(CurrentStock) CurrentStock, Sum(OpeningStock) OpeningStock, Sum(AdjustedStock_In) AdjustedStockIn, Sum(PurchaseQty) PurchaseQty, Sum(SaleReturnQty) SaleReturnQty, Sum(DcQty) DcQty, Sum(TempQty) TempQty, Sum(TransferInQty) TransferInAccQty, Sum(isnull(psqty,0)) TransferInNotAccQty, 
Sum(AdjustedStock_Out) AdjustedStockOut, Sum(SaleQty) SaleQty, Sum(PurchaseDelQty) PurchaseDelQty, Sum(TransferOutQty) TransferOutQty, Sum(ConsumptionQty) ConsumptionQty, Sum(ReturnQty) ReturnQty
--, Sum(TotalIn) TotalIn, Sum(TotalOut) TotalOut, Sum(Diff) Diff
, Sum(TotalIn+isnull(psqty,0)) As FinalIn, Sum(TotalOut) As FinalOut, Sum(TotalIn+isnull(psqty,0)-TotalOut-CurrentStock) As DiffQty 
--,a.psid 
from (
Select 
p.name--,Max(ps.Id) psId
,Sum( case isnull(stockimport,0) when 1 then isnull(ps.importQty,0) else 0 end ) ImportQty
,Sum(ps.stock) as CurrentStock
,Sum(isnull(ps.newStockQty,0)) as OpeningStock
,sum(case when isnull(sa.AdjustedStock,0) > 0 then sa.AdjustedStock else 0 end) AdjustedStock_In
,sum(case when isnull(sa.AdjustedStock,0) < 0 then ABS(sa.AdjustedStock) else 0 end) AdjustedStock_Out
,Sum(Isnull(sips.SaleQty,0)) SaleQty
,Sum(Isnull(srips.SaleReturnQty,0)) SaleReturnQty
,Sum(Isnull(dcps.DcQty,0)) DcQty
,Sum(Isnull(tps.TempQty,0)) TempQty
,Sum(isnull(stito.TransferInQty,0)) TransferInQty
,Sum(isnull(sti.quantity,0)) TransferOutQty
,Sum(isnull(vi.PurchaseQty,0)) PurchaseQty
,Sum(isnull(vdi.PurchaseDelQty,0)) PurchaseDelQty
,Sum(isnull(scn.ConsumptionQty,0)) ConsumptionQty
,Sum(isnull(vr.ReturnQty,0)) ReturnQty
,Sum( case isnull(stockimport,0) when 1 then isnull(ps.importQty,0) else 0 end)+Sum(isnull(ps.newStockQty,0))+Sum(isnull(vi.PurchaseQty,0))+Sum(isnull(stito.TransferInQty,0))+Sum(Isnull(dcps.DcQty,0))+Sum(Isnull(srips.SaleReturnQty,0))+Sum(Isnull(tps.TempQty,0))+sum(case when isnull(sa.AdjustedStock,0) > 0 then sa.AdjustedStock else 0 end) TotalIn
,Sum(isnull(vdi.PurchaseDelQty,0))--+Sum(isnull(ps.stock,0)) 
+Sum(isnull(sti.quantity,0))+Sum(isnull(scn.ConsumptionQty,0))+Sum(isnull(vr.ReturnQty,0))+Sum(Isnull(sips.SaleQty,0))+sum(case when isnull(sa.AdjustedStock,0) < 0 then ABS(sa.AdjustedStock) else 0 end) TotalOut
,(Sum( case isnull(stockimport,0) when 1 then isnull(ps.importQty,0) else 0 end)+Sum(isnull(ps.newStockQty,0))+Sum(isnull(vi.PurchaseQty,0))+Sum(isnull(stito.TransferInQty,0))+Sum(Isnull(srips.SaleReturnQty,0))+Sum(Isnull(tps.TempQty,0)) +sum(case when isnull(sa.AdjustedStock,0) > 0 then sa.AdjustedStock else 0 end))-
(Sum(isnull(vdi.PurchaseDelQty,0))--+Sum(isnull(ps.stock,0))
+Sum(isnull(sti.quantity,0))+Sum(isnull(scn.ConsumptionQty,0))+Sum(isnull(vr.ReturnQty,0))+Sum(Isnull(sips.SaleQty,0))+sum(case when isnull(sa.AdjustedStock,0) < 0 then ABS(sa.AdjustedStock) else 0 end)) Diff
,ps.productid productid
,ps.id psid
,0 psqty
from productStock(nolock) ps 

left join (
Select st.productStockid,sum(st.quantity) quantity from productStock(nolock) ps1 
inner join stocktransferitem st on st.productStockId=ps1.id 
inner join stocktransfer tr on tr.Id=st.transferid 
where ps1.instanceid=@InstanceId and TransferStatus!=2  group by st.productStockid) sti on sti.productStockId=ps.id

left join (Select st.toproductStockid,sum(st.quantity) TransferInQty from productStock(nolock) ps1 
inner join stocktransferitem st on st.toproductStockId=ps1.id
inner join stocktransfer tr on tr.Id=st.transferid 
where st.toinstanceid=@InstanceId and st.accountid= @AccountId and TransferStatus!=2  group by st.toproductStockid
) stito on stito.toproductStockId=ps.id

left join (Select st.productStockid,sum(st.AdjustedStock) AdjustedStock from productStock(nolock) ps1 inner join StockAdjustment st on st.productStockId=ps1.id 
where ps1.instanceid=@InstanceId group by st.productStockid) sa on sa.productStockid=ps.id

left join (Select st.productStockid,sum(st.quantity) PurchaseQty from productStock(nolock) ps1 inner join Vendorpurchaseitem st on st.productStockId=ps1.id 
where ps1.instanceid=@InstanceId and st.vendorpurchaseid in (Select id from vendorpurchase(nolock) where isnull(cancelstatus,0) =0 and instanceid =@InstanceId) group by st.productStockid) vi on vi.productStockid=ps.id

left join (Select vpi.productStockid,sum(vpi.quantity) PurchaseDelQty from productStock(nolock) ps1 inner join Vendorpurchaseitem vpi on vpi.productStockId=ps1.id 
where ps1.instanceid=@InstanceId and vpi.status=2 and vpi.vendorpurchaseId in (Select id from vendorpurchase(nolock) where isnull(cancelstatus,0) =0 and instanceid =@InstanceId) group by vpi.productStockid) vdi on vdi.productStockid=ps.id

left join (Select sc.productStockid,sum(sc.Consumption) ConsumptionQty from productStock(nolock) ps1 inner join SelfConsumption sc on sc.productStockId=ps1.id 
where ps1.instanceid=@InstanceId group by sc.productStockid) scn on scn.productStockid=ps.id

left join (Select si.productStockid,sum(si.Quantity) SaleQty from productStock(nolock) ps1 inner join SalesItem si on si.productStockId=ps1.id 
where si.instanceid=@InstanceId group by si.productStockid) sips on sips.productStockid=ps.id

left join (Select sri.productStockid,sum(sri.Quantity) SaleReturnQty from productStock(nolock) ps1 inner join SalesReturnItem sri on sri.productStockId=ps1.id 
where sri.instanceid=@InstanceId and isnull(sri.IsDeleted,0) !=1 group by sri.productStockid) srips on srips.productStockid=ps.id  

left join (Select dc.productStockid,sum(isnull(dc.Quantity,0) + (isnull(dc.PackageQty,0)  * isnull(dc.FreeQty,0))) DcQty from productStock(nolock) ps1 inner join DCVendorPurchaseItem dc on dc.productStockId=ps1.id 
where dc.instanceid=@InstanceId and isactive =1  group by dc.productStockid) dcps on dcps.productStockid=ps.id

left join (Select t.productStockid,sum(t.Quantity) TempQty from productStock(nolock) ps1 inner join TempVendorpurchaseItem t on t.productStockId=ps1.id 
where t.instanceid=@InstanceId  and isactive =1  group by t.productStockid) tps on tps.productStockid=ps.id


left join (Select vri.productStockid,sum(vri.Quantity) ReturnQty from productStock(nolock) ps1 inner join vendorreturnitem vri on vri.productStockId=ps1.id 
where ps1.instanceid=@InstanceId group by vri.productStockid) vr on vr.productStockid=ps.id

inner join product p on p.id=ps.productid
where ps.instanceid=@InstanceId --and p.name = @ProductName
and p.name like isnull(@product, '')+'%'
 
 group by  ps.productid,p.name
 ,ps.id
 
union all

Select p.name, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,p.id,ps.id,Sum(isnull(strpsnull.quantity,0)) psqty from stockTransferItem strpsnull inner join productStock(nolock) ps  on ps.Id=strpsnull.productStockid inner join product p on p.id = ps.productId
where strpsnull.transferid in(Select id from stockTransfer where toInstanceId=@InstanceId and TransferStatus=3) and strpsnull.toproductStockid is null and strpsnull.accountId=@AccountId and strpsnull.instanceid=@InstanceId
group by p.name, p.id, ps.id

) a 
--Where -- ((TotalIn+isnull(psqty,0))-TotalOut)!=0  
--a.productid = 'A170452B-8C62-49AA-81B5-3D45D8D82068'
Group by name, productid 
having Sum(TotalIn+isnull(psqty,0)-TotalOut-CurrentStock)  <> 0 
 

END