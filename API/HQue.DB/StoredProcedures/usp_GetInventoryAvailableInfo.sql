 

/*                               
******************************************************************************                            
** File: [usp_GetInventoryAvailableInfo]   
** Name: [usp_GetInventoryAvailableInfo]                               
** Description: To Get Inventory Available Stock Details
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 15/03/2017 Poongodi			Rack Number taken from Product Instance   
** 17/04/2017 Poongodi			Inner Query Removed
** 19/05/2017 Settu				BoxNo included
** 02/06/2017 Poongodi			ProductInstance Join changed
** 22/06/2017 Annadurai			Add Expired Date check
** 13/07/2017 Martin			Sp Optimizied
** 31/07/2017 Poongodi			Instanceid validation removed from Product and Vendor
** 18/08/2017 Gavaskar			Instanceid validation added from ProductInstance
** 18/08/2017 Lawrence			product name search with exact match
** 27/09/2017 Pooongodi			Condition included 
*******************************************************************************/ 
CREATE PROC [dbo].[usp_GetInventoryAvailableInfo]( @InstanceId VARCHAR(36), 
													@AccountId   VARCHAR(36), 
													@NameType    INT, 
													@Name        VARCHAR(50), 
													@PageNo      INT=0, 
													@PageSize    INT=10) 
AS 
  BEGIN 
    SET nocount ON 
  
  Declare @PName varchar(250),
		  @GenericName varchar(250)

		  	if (@NameType =1)
			BEGIN
				SET @PName = case when @Name = '' then NULL ELSE @Name END
				SET @GenericName = ''
			END
			else
			BEGIN
				SET @GenericName = CASE WHEN @Name IS NULL THEN '' ELSE @Name END
				SET @PName = NULL
			END
			 

SELECT          ps.productid [ProductId], 
                    --(ps.stock) [Stock], 
                    --ps.expiredate [ExpireDate], 
                    --ps.batchno, 
                    p.NAME ProductName, 
                    p.genericname , 
                    p.code ProductCode, 
                    p.manufacturer Manufacturer, 
                    ip.rackno RackNo, 
					ip.boxNo BoxNo, 
                    p.status Status, 
                    MAX(v.NAME)          VendorName, 
                    Count(1) OVER() TotalRecordCount 
    FROM            
	(
		SELECT * FROM productstock WITH(nolock) WHERE accountid=@AccountId AND instanceid=@InstanceId 
		and cast(ExpireDate as date) > cast(getdate() as date) and stock != 0 and (Status is null or Status = 1) 
	) PS 
    INNER JOIN      (SELECT * FROM product WITH(nolock) WHERE accountid=@AccountId ) P  
    ON              p.id = ps.productid 
	Left JOIN		(SELECT * FROM ProductInstance WITH(nolock) WHERE accountid=@AccountId AND instanceid=@InstanceId ) IP on IP.ProductId = P.id
    LEFT OUTER JOIN (SELECT * FROM vendor WITH(nolock) WHERE accountid=@AccountId  ) V
    ON              v.id = ps.vendorid 
    WHERE (P.Name =  CASE WHEN @PName IS NULL THEN p.Name ELSE @PName END
	OR P.Name LIKE CASE WHEN @PName IS NOT NULL AND LEN(@PName)=1 THEN @PName +'%' ELSE @PName END)
	AND      Isnull(p.genericname,'') LIKE @GenericName + '%' 	
	GROUP BY ps.productid,p.NAME,p.genericname,p.code,p.manufacturer,ip.rackno,ip.boxNo,p.status
    ORDER BY        p.NAME  offset @PageNo rows 
    FETCH next @PageSize rows only
  END