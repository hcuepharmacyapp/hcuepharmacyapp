
Create PROCEDURE [dbo].[purchaseGstReport](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime )
 AS

select S.InvoiceDate, S.InvoiceNo, sum(S.GrossAmount) SalesAmount, sum(S.GstValue) GstAmount, sum(S.Gst0_PurchaseValue) Gst0_PurchaseValue, sum(S.Gst0_Value) Gst0_Value, sum(S.Gst0_PurchaseValue)+sum(S.Gst0_Value) Gst0_TotalPurchaseValue, sum(S.Gst5_PurchaseValue) Gst5_PurchaseValue, sum(S.Gst5_Value) Gst5_Value, sum(S.Gst5_PurchaseValue)+sum(S.Gst5_Value) Gst5_TotalPurchaseValue, sum(S.Gst12_PurchaseValue) Gst12_PurchaseValue, sum(S.Gst12_Value) Gst12_Value, sum(S.Gst12_PurchaseValue)+sum(S.Gst12_Value) Gst12_TotalPurchaseValue, sum(S.Gst18_PurchaseValue) Gst18_PurchaseValue, sum(S.Gst18_Value) Gst18_Value, sum(S.Gst18_PurchaseValue)+sum(S.Gst18_Value) Gst18_TotalPurchaseValue, sum(S.Gst28_PurchaseValue) Gst28_PurchaseValue, sum(S.Gst28_Value) Gst28_Value, sum(S.Gst28_PurchaseValue)+sum(S.Gst28_Value) Gst28_TotalPurchaseValue, sum(S.GstOther_PurchaseValue) GstOther_PurchaseValue, sum(S.GstOther_Value) GstOther_Value, sum(S.GstOther_PurchaseValue)+sum(S.GstOther_Value) GstOther_TotalPurchaseValue from 

(select vp.InvoiceDate, (isnull(vp.Prefix,'')+isnull(vp.BillSeries,'')+vp.InvoiceNo) InvoiceNo, Purchase.PurchaseValue as PurchaseAmount,vpi.GstValue,Purchase.GrossValue as GrossAmount,
case isnull(vpi.GstTotal,0) when 0 then Purchase.PurchaseValue else 0 end Gst0_PurchaseValue,
case isnull(vpi.GstTotal,0) when 0 then vpi.GstValue else 0 end Gst0_Value,
case isnull(vpi.GstTotal,0) when 5 then Purchase.PurchaseValue else 0 end Gst5_PurchaseValue,
case isnull(vpi.GstTotal,0) when 5 then vpi.GstValue else 0 end Gst5_Value,
case isnull(vpi.GstTotal,0) when 12 then Purchase.PurchaseValue else 0 end Gst12_PurchaseValue,
case isnull(vpi.GstTotal,0) when 12 then vpi.GstValue else 0 end Gst12_Value,
case isnull(vpi.GstTotal,0) when 18 then Purchase.PurchaseValue else 0 end Gst18_PurchaseValue,
case isnull(vpi.GstTotal,0) when 18 then  vpi.GstValue else 0 end Gst18_Value,
case isnull(vpi.GstTotal,0) when 28 then Purchase.PurchaseValue else 0 end Gst28_PurchaseValue,
case isnull(vpi.GstTotal,0) when 28 then vpi.GstValue else 0 end Gst28_Value,
case when isnull(vpi.GstTotal,0) not in (0,5,12,18,28) then  Purchase.PurchaseValue else 0 end GstOther_PurchaseValue,
case when isnull(vpi.GstTotal,0) not in (0,5,12,18,28) then vpi.GstValue else 0 end GstOther_Value
from VendorPurchase vp with(nolock) inner join
VendorPurchaseItem vpi with(nolock) on vp.Id=vpi.VendorPurchaseId
cross apply (select ((isnull(vpi.Quantity,0) * isnull(vpi.PurchasePrice,0))-isnull(vpi.GstValue,0)) [PurchaseValue],
					((isnull(vpi.Quantity,0) * isnull(vpi.PurchasePrice,0))) [GrossValue]	) as Purchase
where vp.AccountId=@AccountId and vp.InstanceId=@InstanceId and vp.CreatedAt between @StartDate and @EndDate and isnull(vp.Cancelstatus,0)= 0 ) S
group by S.InvoiceDate, S.InvoiceNo
order by S.InvoiceDate desc