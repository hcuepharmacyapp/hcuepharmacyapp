/*                              
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 06/09/2017 Poongodi			Created
** 11/10/2017  nandhini		invoicedate1,expirydate1 for csv
*******************************************************************************/ 
 create PROCEDURE [dbo].[Usp_GSTR2_PurchaseReturn](@Accountid  CHAR(36), 
                                  @InstanceId VARCHAR(36), 
                                  @Startdate  DATE, 
                                  @EndDate    DATE, 
                                  @GstinNo    VARCHAR(50), 
                                  @FilterType VARCHAR(10),
								  @IsRegister bit,
								  @IsInvDtSearch bit) 
AS 
  BEGIN 
      Declare @InvStartDt date,  @InvEndDt date,
	  @GRNStartDt date, @GRNEndDt date
	   declare
   @invoiceDate1 datetime

	    DECLARE @Instance TABLE 
        ( 
           id CHAR(36) 
        ) 

      IF ( @FilterType = 'branch' ) 
        BEGIN 
            INSERT INTO @instance 
            SELECT @Instanceid 
			set @GstinNo =NULL
        END 
      ELSE 
        BEGIN 
            INSERT INTO @instance 
            SELECT id 
            FROM   instance 
            WHERE  gstinno = @GstinNo 
        END 
		declare @LocType table (loctype int)
		declare @vendortype table (ventype int)
		
		declare @LocType1 table (loctype int)
		declare @vendortype1 table (ventype int)
		if (@IsRegister =1)
			begin
			insert into @LocType 
			values (1),(2)
			insert into @vendortype 
			values (1) 
			end
		else
			begin
			insert into @LocType 
			values (3)
			insert into @vendortype 
			values(1) , (2),(3) 
			insert into @LocType1
			values (1),(2)
			insert into @vendortype1 
			values  (2),(3) 
			end
		   

	  IF @IsInvDtSearch=1
	  Begin
		select @InvStartDt = @StartDate, @InvEndDt = @EndDate, @GRNStartDt = NULL, @GRNEndDt = NULL
	  End
	  Else
	  Begin
		select @InvStartDt = NULL, @InvEndDt = NULL, @GRNStartDt = @StartDate, @GRNEndDt = @EndDate
	  End
	  Select vendorreturnId, max([BranchName]) [BranchName],max([BranchGSTin]) [BranchGSTin],max([InvoiceDate]) [InvoiceDate],max([InvoiceNumber]) [InvoiceNumber],max([InvoiceDate1]) [InvoiceDate1],
		max([RecipientName]) [RecipientName],Max([RecipientGSTin]) [RecipientGSTin],max([PlaceOfSupply]) [PlaceOfSupply],'Goods' 
             [IsGoods] ,
             'No' 
             [IsReverseCharge], 
              sum([Quantity]) [Quantity],
			  [TaxRate],sum([ValueWithoutTax]) [ValueWithoutTax], [TaxRate] [GSTTotal]	 ,
			  sum([IGSTAmount]) [IGSTAmount],
			  sum([CGSTAmount]) [CGSTAmount],
			  sum([SGSTAmount]) [SGSTAmount],
			  sum([TaxAmount]) [TaxAmount],
			  sum([InvoiceValue]) [InvoiceValue]
			  from (
	  SELECT instance.NAME + ' - ' + instance.area 
             AS 
             [BranchName],
             Instance.gstinno [BranchGSTin], 
             vendor.gstinno 
             [RecipientGSTin], 
			 vendorreturnitem.productstockid [Productstockid], 
             vendorreturnitem.quantity		[Quantity], 
             vendorreturn.id                  AS [VendorReturnId], 
             ltrim(isnull(vendorreturn.Prefix,'')+' '+vendorreturn.returnno)            AS [InvoiceNumber], 
             vendorreturn.returndate          AS [InvoiceDate], 
			   convert(varchar,vendorreturn.returndate ,103)         AS [InvoiceDate1], 
			   
             vendor.NAME 
             AS 
             [RecipientName], 
              isnull(s.statecode, left(isnull(vendor.gstinno,''),2))  [PlaceOfSupply], 
			 Isnull(ProductStock.gsttotal, 
             Isnull(VendorPurchaseItem.gsttotal, 0)) 
             AS 
             [TaxRate], 
			  Round(Isnull(pocal.InvoiceValue, 0)-isnull(Tax.taxvalue ,0) , 3) 
             [ValueWithoutTax], 
			  CASE Isnull(vendor.locationtype, 1) 
               WHEN 2 THEN Tax.taxvalue 
               ELSE 0 
             END 
             [IGSTAmount], 
             CASE Isnull(vendor.locationtype, 1) 
               WHEN 1 THEN Round(Tax.taxvalue / 2, 3) 
               ELSE 0 
             END 
             [CGSTAmount], 
             CASE Isnull(vendor.locationtype, 1) 
               WHEN 1 THEN Round(Tax.taxvalue / 2, 3) 
               ELSE 0 
             END 
             [SGSTAmount], 
             
             Tax.taxvalue 
             [TaxAmount], 
			 ISNULL(pocal.Taxper,0)        AS [GstTotal], 
			pocal.InvoiceValue [InvoiceValue]
      FROM    vendorreturn
	    INNER JOIN (SELECT * 
                         FROM   @Instance) I 
                     ON i.id = vendorreturn.instanceid 
             INNER JOIN (Select * from  vendorreturnitem  where Instanceid IN (SELECT id 
                                           FROM   @Instance))vendorreturnitem
                     ON vendorreturn.id = vendorreturnitem.vendorreturnid 
             LEFT JOIN ( SElect * from vendorpurchase  where Instanceid IN (SELECT id 
                                           FROM   @Instance)) vendorpurchase
                     ON vendorpurchase.id = vendorreturn.vendorpurchaseid 
             INNER JOIN  (SELECT * 
                         FROM   vendor (nolock) 
                         WHERE  accountid = @Accountid and ( isnull(locationtype,1) in(select loctype from @LocType)
						 and isnull(vendortype,1) in (select ventype from @vendortype) 
						 or  isnull(locationtype,1) in(select loctype from @LocType1)
						 and isnull(vendortype,1) in (select ventype from @vendortype1)) ) vendor  
                     ON vendor.id = vendorreturn.vendorid 
			 left join (select * from  state (nolock) ) s on s.id = vendor.stateid
             INNER JOIN  (SELECT * 
                         FROM   productstock (nolock) 
                         WHERE  accountid = @Accountid 
                                AND instanceid IN (SELECT id 
                                                   FROM   @Instance)) productstock 
                     ON productstock.id = vendorreturnitem.productstockid 
             LEFT JOIN (SELECT * 
                         FROM   vendorpurchaseitem (nolock) 
                         WHERE  accountid = @Accountid 
                                AND instanceid IN (SELECT id 
                                                   FROM   @Instance)) vendorpurchaseitem 
                     ON vendorpurchaseitem.productstockid = productstock.id 
					 and VendorPurchaseItem.VendorPurchaseId = VendorReturn.VendorPurchaseId
             LEFT JOIN (SELECT * 
                        FROM   product (nolock) 
                        WHERE  accountid = @Accountid) product 
                     ON product.id = productstock.productid 
			 INNER JOIN (SELECT * 
                         FROM   instance (nolock) 
                         WHERE  accountid = @Accountid 
                                AND id IN (SELECT id 
                                           FROM   @Instance)) Instance 
                     ON Instance.id = vendorreturn.instanceid 
			 cross apply (select isnull(vendorreturnitem.quantity,0) * isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) [InvoiceValue],
			 ISNULL(VendorReturnItem.GstTotal,productstock.GstTotal) Taxper) pocal
			  cross apply (Select case isnull( pocal.Taxper,0) when 0 then 0 else pocal.InvoiceValue -( pocal.InvoiceValue  * 100 /(100+ pocal.Taxper) )end [TaxValue]) Tax
      WHERE  
	  /* vendorreturn.returndate BETWEEN @StartDate AND @EndDate
	  and */
	   (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0) and  -- Condition added by Gavaskar 15-09-2017 
	  cast(vendorreturn.returndate as date) BETWEEN isnull(@InvStartDt,cast(vendorreturn.returndate as date)) AND isnull(@InvEndDt,cast(vendorreturn.returndate as date))
	  AND cast(vendorreturn.CreatedAt as date)  BETWEEN isnull(@GRNStartDt,cast(vendorreturn.CreatedAt as date)) AND isnull(@GRNEndDt,cast(vendorreturn.CreatedAt as date))
             AND vendorreturn.accountid = @AccountId 
            
			 ) as one Group by vendorreturnId,[TaxRate]
			 ORDER BY [BranchName], INVOICEDATE DESC, INVOICENUMBER DESC
  
  END