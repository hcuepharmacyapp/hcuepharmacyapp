﻿CREATE PROCEDURE [dbo].[usp_Get_PhysicalStockHistory](@AccountId CHAR(36), @InstanceId CHAR(36), @FromDate AS DATE, @ToDate AS DATE, @ProductId VARCHAR(500), @UserName VARCHAR(100), @UserId VARCHAR(100))
AS
BEGIN

	IF @FromDate = ''
		SET @FromDate = NULL
	IF @ToDate = ''
		SET @ToDate = NULL
	IF @ProductId = ''
		SET @ProductId = NULL
	IF @UserName = ''
		SET @UserName = NULL
	IF @UserId = ''
		SET @UserId = ''

	SELECT P.Id AS [ProductId],P.Name AS [ProductName],PSH.PhysicalStock,PSH.CurrentStock,
	(ISNULL(PSH.CurrentStock,0)-ISNULL(PSH.PhysicalStock,0)) AS StockDifference,
	PSH.CreatedAt,PSH.OpenedAt,PSH.ClosedAt,U.Name AS [UserName],U.UserId AS [UserId]
	FROM PhysicalStockHistory PSH
	INNER JOIN Product P ON P.Id = PSH.ProductId
	INNER JOIN HQueUser U ON U.Id = PSH.CreatedBy
	WHERE PSH.AccountId = @AccountId AND PSH.InstanceId = @InstanceId
	AND CAST(PSH.CreatedAt AS DATE) BETWEEN ISNULL(@FromDate,PSH.CreatedAt) AND ISNULL(@ToDate,PSH.CreatedAt)
	AND (PSH.ProductId = ISNULL(@ProductId,PSH.ProductId) OR P.Name like ''+@ProductId+'%') 
	AND (U.Mobile = ISNULL(@UserName,U.Mobile) OR U.Name like ''+@UserName+'%') 
	AND (U.UserId = ISNULL(@UserId,U.UserId) OR U.UserId like ''+@UserId+'%') 
	ORDER BY PSH.CreatedAt DESC
END