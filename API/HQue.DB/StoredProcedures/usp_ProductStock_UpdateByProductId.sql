﻿/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 11-Nov2017 Manivannan	  Created
 ** 20-Feb-2019 Sumathi	  Status Update Included
*******************************************************************************/ 
CREATE Proc usp_ProductStock_UpdateByProductId(@GstTotal decimal (18,2),
								    @Igst char(36),
									@Cgst bit,
									@Sgst decimal(18,6),
									@UpdatedBy varchar(36),
									@ProductId char(36),
									@AccountId char(36),
									@InstanceId char(36)=null,
									@UpdatedAt datetime,
									@Status INT = NULL
)
as
begin
SET DEADLOCK_PRIORITY LOW

Update ProductStock Set GstTotal =  @GstTotal, Igst = @Igst, Cgst = @Cgst, Sgst = @Sgst, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy Where ProductId = @ProductId And GstTotal is NULL And AccountId = @AccountId

IF(@Status IS NOT NULL)
	Update ProductStock Set Status = @Status Where ProductId = @ProductId And AccountId = @AccountId 
 
 select 'success'
 end