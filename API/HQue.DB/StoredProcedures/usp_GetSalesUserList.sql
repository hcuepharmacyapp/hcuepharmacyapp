  --Usage : -- usp_GetSalesUserList '0020ccad-e432-4ec5-8927-e9302db2340a','d5baa673-d6e4-4e3e-a593-cdd6313be32c'
  
CREATE PROCEDURE [dbo].[usp_GetSalesUserList](@InstanceId varchar(36),@AccountId varchar(36))
AS
 BEGIN
   SET NOCOUNT ON
 SELECT U.Name, count(distinct S.Id) AS TotalSales,
        Convert(decimal(18,2),sum((VPI.PurchasePrice * SI.Quantity))) AS TotalPurchase,
		Convert(decimal(18,2),sum((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity - ((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity * S.Discount / 100) ))  AS TotalMRP,
	    CONVERT(decimal(18,2),sum((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity * isnull(SI.Discount,0) / 100)) as TotalDiscount  
		
		--CONVERT(decimal(18,2),SUM((VPI.PurchasePrice * SI.Quantity))) AS TotalPurchase,
		--CONVERT(decimal(18,2),SUM( ISNULL(SI.SellingPrice, PS.SellingPrice) * SI.Quantity * (1 - S.Discount / 100))) AS TotalMRP,
	    --CONVERT(decimal(18,2),SUM(ISNULL(SI.SellingPrice, PS.SellingPrice) * SI.Quantity * ISNULL(SI.Discount,0) / 100)) as TotalDiscount 
	  ----CONVERT(decimal(18,2),sum((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity - ((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity * S.Discount / 100) ))  AS TotalMRP,
	  ----,CONVERT(decimal(18,2),sum((CASE WHEN SI.SellingPrice > 0 then SI.SellingPrice else PS.SellingPrice END) * SI.Quantity * isnull(SI.Discount,0) / 100)) as TotalDiscount
	   
FROM Sales S WITH(NOLOCK)
INNER JOIN SalesItem SI  WITH(NOLOCK) ON S.Id = SI.salesid
INNER JOIN ProductStock PS  WITH(NOLOCK) ON SI.productstockid = PS.id
LEFT OUTER JOIN VendorPurchaseItem VPI  WITH(NOLOCK) ON PS.id=VPI.ProductStockId
LEFT OUTER JOIN HQueUser U  WITH(NOLOCK) ON U.id = S.CreatedBy
 WHERE S.InstanceId = @InstanceId AND S.AccountId = @AccountId and S.Cancelstatus is NULL
GROUP BY U.Name
ORDER BY U.Name DESC

END
 


