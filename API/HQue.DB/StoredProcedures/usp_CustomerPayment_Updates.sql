/*************************************
******************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**04/04/18     Poongodi R	   SP Created 
 
*******************************************************************************/
 create PROC [dbo].[usp_CustomerPayment_Updates](@AccountId char(36),
 @InstanceId char(36),
 @SalesId char(36)

 )

 as 
 begin

 SET DEADLOCK_PRIORITY LOW 
	--UPDATE customerpayment SET DEBIT =0,CREDIT =0 WHERE SALESID =@SalesId
	DELETE FROM customerpayment WHERE SALESID =@SalesId

	  select 'success' 

 end 