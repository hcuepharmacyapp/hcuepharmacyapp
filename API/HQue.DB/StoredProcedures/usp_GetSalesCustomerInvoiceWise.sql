﻿CREATE PROCEDURE [dbo].[usp_GetSalesCustomerInvoiceWise](@AccountId varchar(36),@InstanceId varchar(36),
@StartDate date,@EndDate date,@PatientName VARCHAR(200),@UserName VARCHAR(200),@UserId VARCHAR(100),@Type VARCHAR(20))
AS
BEGIN
SET NOCOUNT ON 

if @InstanceId = ''
SET @InstanceId = NULL
if @StartDate = ''
SET @StartDate = NULL
if @EndDate = ''
SET @EndDate = NULL

if @PatientName = ''
SET @PatientName = NULL
if @UserName = ''
SET @UserName = NULL
if @UserId = ''
SET @UserId = NULL

select 
sales.invoicedate AS InvoiceDate,
sales.invoiceno as InvoiceNo,
ltrim(isnull(sales.prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries, 
sales.SalesItemAmount As InvoiceAmount,
sales.TotalDiscountValue,
sales.SaleAmount,
sales.RoundoffSaleAmount, 
Inst.Name as InstanceName,
CASE WHEN @Type = 'USERWISE' THEN ISNULL(U.Name,'') ELSE '' END AS UserName,
CASE WHEN @Type = 'CUSTOMERWISE' THEN ISNULL(sales.Name,'') ELSE '' END AS PatientName
from sales sales WITH(NOLOCK) 
join salesitem salesitem WITH(NOLOCK) on sales.id= salesitem.salesid
join Instance Inst WITH(NOLOCK) on Inst.Id = ISNULL(@InstanceId, sales.InstanceId)
LEFT OUTER JOIN HQueUser U WITH(NOLOCK) ON U.id = sales.CreatedBy
WHERE sales.AccountId = @AccountId and sales.InstanceId = ISNULL(@InstanceId,sales.InstanceId)
AND Convert(date,sales.invoicedate) BETWEEN ISNULL(@StartDate,Convert(date,sales.invoicedate)) AND ISNULL(@EndDate,Convert(date,sales.invoicedate))
AND ISNULL(sales.Name,'')= ISNULL(@PatientName,ISNULL(sales.Name,'')) and sales.Cancelstatus is NULL
AND ISNULL(U.Name,'') = isnull(@UserName, ISNULL(U.Name,''))
and ISNULL(U.UserId,'') = isnull(@UserId, ISNULL(U.UserId,'')) 
group by sales.invoicedate,sales.invoiceno,ltrim(isnull(sales.prefix,'')+isnull(sales.InvoiceSeries, '')),
sales.SalesItemAmount,sales.TotalDiscountValue,sales.SaleAmount,sales.RoundoffSaleAmount,Inst.Name,
CASE WHEN @Type = 'USERWISE' THEN ISNULL(U.Name,'') ELSE '' END ,
CASE WHEN @Type = 'CUSTOMERWISE' THEN ISNULL(sales.Name,'') ELSE '' END 
ORDER BY Inst.Name,sales.invoicedate

SET NOCOUNT OFF
END