﻿/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 23/04/2018	Gavaskar S	  Stock Correction in ProductStock offline to online data update
*******************************************************************************/
CREATE PROCEDURE [dbo].[usp_DataCorrection_UpdateProductStock](@AccountId CHAR(36), @InstanceId CHAR(36), @ProductStockId CHAR(36),@Stock decimal (18,6),@UpdatedAt datetime)
AS
BEGIN
	UPDATE ProductStock set Stock = @Stock,UpdatedAt = @UpdatedAt WHERE AccountId = @AccountId AND InstanceId = @InstanceId and Id = @ProductStockId
END