 /*                               
******************************************************************************                            
** File: [usp_GetBranchwiseStock]   
** Name: [usp_GetBranchwiseStock]                               
** Description: To GetBranchWise stock
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date:06/06/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 10/08/2017  Poongodi R		Area Name inlcuded
*******************************************************************************/ 
 Create PROC [dbo].[usp_GetBranchwiseStock]( @AccountId  VARCHAR(36), 
												@ProductId VARCHAR(36))

As 
Begin
 SELECT 
ProductStock.Id,ProductStock.AccountId,ProductStock.InstanceId,ProductStock.ProductId,ProductStock.VendorId,ProductStock.BatchNo,ProductStock.ExpireDate,ProductStock.VAT,ProductStock.TaxType,ProductStock.SellingPrice,ProductStock.MRP,ProductStock.PurchaseBarcode,ProductStock.Stock,ISNULL(ProductStock.PackageSize, 1) AS PackageSize,ProductStock.PackagePurchasePrice,ProductStock.PurchasePrice,ProductStock.OfflineStatus,ProductStock.CreatedAt,ProductStock.UpdatedAt,ProductStock.CreatedBy,ProductStock.UpdatedBy,ProductStock.CST,ProductStock.IsMovingStock,ProductStock.ReOrderQty,ProductStock.Status,ProductStock.IsMovingStockExpire,ProductStock.NewOpenedStock,ProductStock.NewStockInvoiceNo,ProductStock.NewStockQty,ProductStock.StockImport,ProductStock.Eancode,Product.Id 
AS [Product.Id],Product.AccountId AS [Product.AccountId],Product.InstanceId AS [Product.InstanceId],Product.Code AS [Product.Code],Product.Name AS 
[Product.Name],Product.Manufacturer AS [Product.Manufacturer],Product.KindName AS [Product.KindName],Product.StrengthName AS [Product.StrengthName],Product.Type AS [Product.Type],
Product.Schedule AS [Product.Schedule],Product.Category AS [Product.Category],Product.GenericName AS [Product.GenericName],Product.CommodityCode AS [Product.CommodityCode],
Product.Packing AS [Product.Packing],Product.OfflineStatus AS [Product.OfflineStatus],Product.CreatedAt AS [Product.CreatedAt],Product.UpdatedAt AS [Product.UpdatedAt],
Product.CreatedBy AS [Product.CreatedBy],Product.UpdatedBy AS [Product.UpdatedBy],Product.PackageSize AS [Product.PackageSize],Product.VAT AS [Product.VAT],
Product.Price AS [Product.Price],Product.Status AS [Product.Status],ProductInstance.RackNo AS [Product.RackNo],Product.ProductMasterID AS [Product.ProductMasterID],
Product.ProductOrgID AS [Product.ProductOrgID],ProductInstance.ReOrderLevel AS [Product.ReOrderLevel],ProductInstance.ReOrderQty AS [Product.ReOrderQty],
Product.Discount AS [Product.Discount],Vendor.Name AS [Vendor.Name], Instance.Name + case isnull (instance.Area,'')   when '' then '' else  ',' + isnull (instance.Area,'') end As [InstanceName], ISNULL(Instance.Phone,ISNULL(Instance.Mobile,'')) as [Phone] FROM ProductStock INNER JOIN Product ON Product.Id = ProductStock.ProductId
 LEFT JOIN Vendor ON ProductStock.VendorId = Vendor.Id 
 LEFT JOIN ProductInstance ON ProductInstance.ProductId = ProductStock.ProductId
  And ProductInstance.InstanceId = ProductStock.InstanceId 
   INNER JOIN Instance On Instance.Id = ProductStock.InstanceId WHERE ProductStock.Stock  >  0 AND cast(ProductStock.ExpireDate  as date) > cast(getdate() as date)
  AND (ProductStock.Status  =  1 Or (ProductStock.Status IS NULL))    AND ProductStock.AccountId  =  @AccountId 
  and  ProductStock.ProductId =@ProductId
   ORDER BY ProductStock.ExpireDate asc 
   End 