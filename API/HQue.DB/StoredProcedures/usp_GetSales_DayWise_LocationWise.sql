 --Usage : -- usp_GetSales_DayWise_LocationWise '013513b1-ea8c-4ea8-9fed-054b260ee197','18204879-99ff-4efd-b076-f85b4a0da0a3','01 Nov 2016','30 Nov 2016'

 CREATE PROCEDURE [dbo].[usp_GetSales_DayWise_LocationWise] (@AccountId varchar(36), @StartDate datetime, @EndDate datetime, @InstanceInd bit, @InstanceIds varchar(36) = '')
 AS
 BEGIN
   SET NOCOUNT ON

   --Declaring Table Variables
  DECLARE @Instances TABLE (InstanceId VARCHAR(36), InstanceName VARCHAR(150));
  DECLARE @TmpReport TABLE (InstanceId VARCHAR(36), InstanceName VARCHAR(150), Dt DATE, TotalBills DECIMAL(18,2), TotalCash DECIMAL(18,2), TotalCC DECIMAL(18,2), 
			TotalCredit DECIMAL(18,2), TotalSales DECIMAL(18,2), PrevMonthDt DATE, PrevMonthSales DECIMAL(18,2), AvgSales DECIMAL(18,2), DataType INT);
  DECLARE @TmpSales TABLE (InstanceId VARCHAR(36), InvoiceDate DATE, SalesId VARCHAR(36), PaymentType VARCHAR(20), SellAmt DECIMAL(18,2), Credit DECIMAL(18,2));
  DECLARE @InstanceAvgSales TABLE (InstanceId VARCHAR(36), AvgSellAmt DECIMAL(18,2));

  -- Inserting All the Instances for the Account if the Instance Indicator is 0
  IF @InstanceInd = 0
  BEGIN
		INSERT INTO @Instances SELECT Id, Name FROM Instance WITH(NOLOCK) WHERE AccountId = @AccountId
  END;

  --All dates within the date range
  WITH dateRange AS
  (
  SELECT dt = DATEADD(dd,0,@StartDate) WHERE DATEADD(dd,0,@StartDate) <= @EndDate
  UNION ALL
  SELECT DATEADD(dd, 1, Dt) from dateRange WHERE DATEADD(dd, 1, Dt) <= @EndDate
  )
  
  --All dates within the date range are inserted into @TmpReport table and updated with its Previous month's date
  INSERT INTO @TmpReport(InstanceId, InstanceName, Dt, DataType) SELECT I.InstanceId, I.InstanceName, DR.Dt, 1 FROM DateRange DR CROSS JOIN @Instances I
  OPTION (maxrecursion 0);
  UPDATE @TmpReport SET PrevMonthDt = DateAdd(month, -1, Dt);

  --All dates within the date range in the previous month are inserted into @TmpReport table
  With dateRange2 as
  (
  SELECT dt = DATEADD(dd,0,DateAdd(month,-1,@StartDate)) WHERE DATEADD(dd,0,@StartDate) <= @EndDate
  UNION ALL
  SELECT DATEADD(dd, 1, dt) from dateRange2 WHERE DATEADD(dd, 1, dt) <= @EndDate
  )
  INSERT INTO @TmpReport(InstanceId, InstanceName, Dt, DataType) SELECT I.InstanceId, I.InstanceName, DR.Dt, 2 FROM DateRange2 DR CROSS JOIN @Instances I
  OPTION (maxrecursion 0);
  
  -- Loading the Sales Values for the instance and invoice date to the @TmpSales

  INSERT INTO @TmpSales
  SELECT s.InstanceId, s.InvoiceDate, s.Id as SalesId, s.PaymentType, (isNull(si.SellingPrice, ps.SellingPrice) * si.Quantity) as SellAmt, isNull(s.Credit,0)
  FROM Sales s WITH(NOLOCK) JOIN SalesItem si WITH(NOLOCK) on s.Id = si.SalesId join ProductStock ps WITH(NOLOCK) on si.ProductStockId = ps.Id 
  WHERE s.AccountId = @AccountId and s.instanceId IN (SELECT InstanceId from @Instances)
  and ((s.invoiceDate between @StartDate and DATEADD(dd,1,@EndDate)) or (s.invoiceDate between DateAdd(month, -1, @StartDate) and DateAdd(month, -1, DATEADD(dd,1,@EndDate))))

  -- Updating the Aggregate values to the @TmpReport
  UPDATE TR SET TotalBills = OQ.TotalBills, TotalCash = OQ.TotalCash, TotalCC = OQ.TotalCC, TotalCredit = OQ.TotalCredit, TotalSales = (OQ.TotalCash+OQ.TotalCC+OQ.TotalCredit) FROM @TmpReport TR JOIN (
  SELECT IQ.InstanceId, IQ.InvoiceDate, COUNT(IQ.SalesId) as TotalBills, SUM(IQ.Cash) as TotalCash, SUM(IQ.Credit) as TotalCredit, SUM(IQ.Card) as TotalCC FROM (
  SELECT InstanceId, InvoiceDate, SalesId, 
  CASE WHEN (paymentType != 'Card') THEN (SellAmt-Credit) ELSE 0 END AS Cash, 
  CASE WHEN (paymentType != 'Card') THEN (Credit) ELSE 0 END AS Credit, 
  CASE WHEN (paymentType = 'Card') THEN (SellAmt) ELSE 0 END AS Card
  from @TmpSales --where InvoiceDate between @StartDate and @EndDate
  ) IQ 
  GROUP BY IQ.InstanceId, IQ.InvoiceDate
  ) OQ ON OQ.InvoiceDate = tr.Dt AND OQ.InstanceId = TR.InstanceId

  --Deleting Temporary Sales values from @TmpSales
	DELETE from @TmpSales;

  --Updating the previous month sales value in the seperate rows in the current months' rows' fields.
  UPDATE TR1 SET TR1.PrevMonthSales = TR2.TotalSales FROM @TmpReport TR1 JOIN @TmpReport TR2 ON TR1.PrevMonthDt = TR2.Dt AND TR1.InstanceId = TR2.InstanceId WHERE TR1.DataType = 1;

  --Deleting the seperate rows for previous month
  DELETE from @TmpReport WHERE DataType = 2;

  --Updating temp table with the Average sales value
  
  INSERT INTO @InstanceAvgSales
   SELECT s.InstanceId, AVG(ISNULL(si.SellingPrice, ps.SellingPrice) * si.Quantity) AS AvgSellAmt FROM sales s WITH(NOLOCK) 
   JOIN SalesItem si WITH(NOLOCK) ON s.Id = si.SalesId 
	JOIN ProductStock ps WITH(NOLOCK) ON si.ProductStockId = ps.Id 
	WHERE s.AccountId = @AccountId
	GROUP BY s.InstanceId

	UPDATE TR SET TR.AvgSales = IA.AvgSellAmt FROM @TmpReport TR JOIN
	@InstanceAvgSales IA ON TR.InstanceId = IA.InstanceId
	
  --select * into TmpReport from @TmpReport


  SELECT InstanceName, Dt SalesDate, isNull(TotalBills, 0) TotalBills, isNull(TotalCash, 0) TotalCash, isNull(TotalCC, 0) TotalCC, isNull(TotalCredit, 0) TotalCredit, isNull(TotalSales, 0) TotalSales, PrevMonthDt, isNull(PrevMonthSales, 0) PrevMonthSales, isNull(AvgSales, 0) AvgSales FROM @TmpReport
  
 END
  

