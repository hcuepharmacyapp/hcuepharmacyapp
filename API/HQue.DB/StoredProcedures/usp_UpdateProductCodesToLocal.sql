﻿CREATE PROCEDURE [dbo].[usp_UpdateProductCodesToLocal](@ProductIdsVsCodes VARCHAR(MAX))
AS
BEGIN
	DECLARE @xml XML 
	SET @xml = CAST('<Product><ProductId>'+REPLACE(REPLACE(REPLACE(ISNULL(@ProductIdsVsCodes,''),'~','</ProductId><Code>'),'''',''),',','</Code></Product><Product><ProductId>')+'</Code></Product>' AS XML)

	UPDATE P SET P.Code = tmp.ProductCode FROM Product P
	JOIN
	(SELECT Tab.Col.value('ProductId[1]', 'CHAR(36)') AS ProductId,
	Tab.Col.value('Code[1]', 'VARCHAR(20)') AS ProductCode FROM @xml.nodes('Product') AS Tab(Col)) tmp ON tmp.ProductId = P.Id WHERE P.Code IS NULL

	SELECT P.Id, P.Code FROM Product P
	JOIN
	(SELECT Tab.Col.value('ProductId[1]', 'CHAR(36)') AS ProductId,
	Tab.Col.value('Code[1]', 'VARCHAR(20)') AS ProductCode FROM @xml.nodes('Product') AS Tab(Col)) tmp ON tmp.ProductId = P.Id AND tmp.ProductCode = P.Code

END