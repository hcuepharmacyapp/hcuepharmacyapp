﻿-- Usage: exec usp_Get_CustomerPaymentList @AccountId=N'2d5abd54-89a9-4b31-aa76-8ec70b9125dd',@InstanceId=N'c3473243-ad71-4883-8f21-57e196c76e96',@customername=NULL,@mobile=NULL
CREATE PROCEDURE [dbo].[usp_Get_CustomerPaymentList](@AccountId CHAR(36), @InstanceId CHAR(36), @customername VARCHAR(150), @mobile VARCHAR(15))
AS
BEGIN
IF @customername = ''
SET @customername = NULL
IF @mobile = ''
SET @mobile = NULL
Declare @CustomerId CHAR(36)
	    DECLARE @table1 TABLE
				(
				 PatientId CHAR(36),
				 CustomerName varchar(150),
				 CustomerMobile varchar(100),
				 CustomerCredit numeric(18,2),
				 CustomerDebit numeric(18,2),
				 BalanceAmount numeric(18,2),
				 SalesCredit numeric(18,2),
				 CustomerStatus int
				)

		 DECLARE @table2 TABLE
				(
				 PatientId CHAR(36),
				  CustomerName varchar(150),
				 CustomerMobile varchar(100),
				 CustomerCredit numeric(18,2),
				 CustomerDebit numeric(18,2),
				 BalanceAmount numeric(18,2),
				 SalesCredit numeric(18,2),
				 CustomerStatus int
				)

				 DECLARE @table3 TABLE
				(
				 PatientId CHAR(36),
				  CustomerName varchar(150),
				 CustomerMobile varchar(100),
				 CustomerCredit numeric(18,2),
				 CustomerDebit numeric(18,2),
				 BalanceAmount numeric(18,2),
				 SalesCredit numeric(18,2),
				 CustomerStatus int
				)

	Insert into  @table1  
	SELECT a.* FROM
	(SELECT P.Id AS PatientId, P.Name AS CustomerName, P.Mobile AS CustomerMobile, R2.CustomerCredit, R2.CustomerDebit, R2.BalanceAmount, R2.SalesCredit
	, ISNULL(R2.CustomerStatus,1) CustomerStatus
	  FROM (
		SELECT R.PatientId, R.[CustomerPayment.Credit] AS CustomerCredit, R.[CustomerPayment.Debit] AS CustomerDebit,
		R.TotalSalesCredit AS SalesCredit,isnull(R1.BalanceAmount,0) as BalanceAmount, ISNULL(R1.CustomerStatus,1) CustomerStatus FROM 
		(
			SELECT Sales.PatientId, SUM(Sales.Credit) AS TotalSalesCredit , SUM(CustomerPayment.Debit)  AS [CustomerPayment.Debit], 
			SUM(CustomerPayment.Credit)  AS [CustomerPayment.Credit] FROM Sales INNER JOIN CustomerPayment ON CustomerPayment.SalesId = Sales.Id  
			WHERE Sales.AccountId  =  @AccountId AND Sales.InstanceId  =  @InstanceId 
			AND ISNULL(Sales.Name,'') = ISNULL(@customername,ISNULL(Sales.Name,'')) AND ISNULL(Sales.Mobile,'') = ISNULL(@mobile,ISNULL(Sales.Mobile,''))
			AND (Sales.Cancelstatus IS NULL) 
			GROUP BY Sales.PatientId
		) R LEFT JOIN 
		(
			SELECT V.ParticipantId,SUM(V.Amount) AS BalanceAmount, ISNULL(P.Status,1) CustomerStatus FROM Voucher V
			INNER JOIN Patient P ON V.ParticipantId = P.Id
			WHERE V.AccountId  =  @AccountId AND V.InstanceId  =  @InstanceId 
			AND ISNULL(P.Name,'') = ISNULL(@customername,ISNULL(P.Name,'')) AND ISNULL(P.Mobile,'') = ISNULL(@mobile,ISNULL(P.Mobile,''))
			AND V.TransactionType = 1 AND V.Amount > 0
			GROUP BY V.ParticipantId, P.Status
		) R1 ON R.PatientId = R1.ParticipantId
		WHERE (R.[CustomerPayment.Credit] != R.[CustomerPayment.Debit] OR R1.BalanceAmount > 0)
	) R2
	INNER JOIN Patient P ON P.Id = R2.PatientId
	) as a  

			
			Insert into @table2
			SELECT b.* FROM
			(Select a.id as PatientId, a.Name as CustomerName, a.Mobile as CustomerMobile,  SUM(b.Credit) as CustomerCredit ,
			SUM(b.Debit) as CustomerDebit, 0 as BalanceAmount , 0 as SalesCredit, ISNULL(a.Status,1) CustomerStatus
			From CustomerPayment b   Inner Join Patient a
			On a.id  = b.CustomerId 
			where b.CustomerId is not null and a.Status <> 2 --and a.Status is not null
			And b.AccountId  =  @AccountId AND b.InstanceId  =  @InstanceId 
			Group by a.id,a.Name,a.Mobile,a.Status) as b
			Group by PatientId,CustomerName,CustomerMobile,CustomerCredit,CustomerDebit,BalanceAmount,SalesCredit,CustomerStatus
			Having (CustomerCredit - CustomerDebit) > 0

			Insert into @table3
			Select a.* from 
			(Select * from @table1
			union 
			Select * from @table2) as a


			Select DISTINCT a.PatientId,
					a. CustomerName,
					a. CustomerMobile,
					Sum(a.CustomerCredit) as CustomerCredit ,
					Sum(a.CustomerDebit) as CustomerDebit ,
					Sum(a.BalanceAmount) as BalanceAmount,
					Sum(a.SalesCredit) as SalesCredit
					,MAX(a.CustomerStatus) as CustomerStatus
			from @table3 a --where a.CustomerStatus is not null
			Group by a.PatientId,a.CustomerName,a.CustomerMobile --,a.CustomerStatus

END