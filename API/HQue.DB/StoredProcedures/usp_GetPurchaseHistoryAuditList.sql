 
/*                               
******************************************************************************                            
** File: [usp_GetPurchaseHistoryAuditList]   
** Name: [usp_GetPurchaseHistoryAuditList]                               
** Description: To Get Purchase Audit details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 **04/05/2017  Poongodi R		ProductId filter added 
*******************************************************************************/

 -- exec usp_GetPurchaseHistoryAuditList '8adafeb8-ebed-454b-90c7-2c65a7a94f06'
 create PROCEDURE [dbo].[usp_GetPurchaseHistoryAuditList](@VendorPurchaseId varchar(max), @Product varchar(50) ='')
 AS
 BEGIN
   SET NOCOUNT ON

  
 SELECT VendorPurchaseItemAudit.ProductStockId,VendorPurchaseItemAudit.PackageSize,VendorPurchaseItemAudit.PackageQty,VendorPurchaseItemAudit.PackagePurchasePrice,
VendorPurchaseItemAudit.PackageSellingPrice,VendorPurchaseItemAudit.PackageMRP,VendorPurchaseItemAudit.VendorPurchaseId,VendorPurchaseItemAudit.Quantity,VendorPurchaseItemAudit.PurchasePrice,
VendorPurchaseItemAudit.FreeQty,VendorPurchaseItemAudit.Discount,VendorPurchaseItemAudit.CreatedAt,VendorPurchaseItemAudit.Action,
ProductStock.SellingPrice,ProductStock.MRP ,ProductStock.VAT,ProductStock.CST,
-- GST related fields 
ProductStock.Igst,ProductStock.Cgst,ProductStock.Sgst,ProductStock.GstTotal,
ProductStock.ExpireDate AS [ExpireDate],ProductStock.BatchNo,Product.Name AS [ProductName],HQueUser.Name AS [UpdatedBy] FROM 
VendorPurchaseItemAudit INNER JOIN ProductStock ON ProductStock.Id = VendorPurchaseItemAudit.ProductStockId INNER JOIN Product ON Product.Id = 
ProductStock.ProductId INNER JOIN HQueUser ON HQueUser.Id = VendorPurchaseItemAudit.UpdatedBy  WHERE 
ProductStock.ProductId  like isnull(@product, '')+'%' and
VendorPurchaseItemAudit.VendorPurchaseId in (select a.id from dbo.udf_Split(@VendorPurchaseId ) a) ORDER BY VendorPurchaseItemAudit.CreatedAt desc 
           
 END
 