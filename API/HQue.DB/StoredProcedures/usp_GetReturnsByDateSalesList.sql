/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
**  31/05/2017  Poongodi R		Sales Inner join removed 
** 20/06/2017   Poongodi		Invoice Series Filter commented 
** 21/06/2017   Violet			Prefix Addded
** 10/07/2017   Sarubala		Custom Series Addded
** 13/09/2017	Poongodi R		Instance Name added
** 12/10/2017	Poongodi R		Sales Return Discount Issue fixed
** 13/10/2017   Sarubala V		Sales Return return charges included
*******************************************************************************/ 
-- usp_GetReturnsByDateSalesList '3e2ec066-1551-4527-be53-5aa3c5b7fb7d','c503ca6e-f418-4807-a847-b6886378cf0b','09-Mar-2017 00:00:00','09-Mar-2017 23:59:59','',''
-- usp_GetReturnsByDateSalesList 'c3473243-ad71-4883-8f21-57e196c76e96','c503ca6e-f418-4807-a847-b6886378cf0b','09-Mar-2017 00:00:00','09-Mar-2017 23:59:59','',''
CREATE Proc [dbo].[usp_GetReturnsByDateSalesList]( @InstanceId varchar(36),@AccountId varchar(36), @StartDate datetime,@EndDate datetime, @SearchOption varchar(50),
 @SearchValue varchar(50))
 AS
 BEGIN 
 
declare @InvoiceSeries varchar(100) = ''

if (@SearchOption ='1')

select  @InvoiceSeries = '' 

else  if (@SearchOption ='2')

select  @InvoiceSeries = isnull(@SearchValue,'') 

else  if (@SearchOption ='3')

select  @InvoiceSeries = 'MAN'

SET NOCOUNT ON
select @SearchOption  =1
IF (@SearchOption ='1')
BEGIN

SELECT ltrim(isnull(SR.Prefix,'')+' '+ISNULL(sr.InvoiceSeries,'')+' '+ SR.ReturnNo)as ReturnNo,SR.ReturnDate,isnull(S.Name,isnull(cust.name,'')) [Name], ltrim(isnull(S.Prefix,'')+isnull(S.InvoiceSeries,'') +' '+ isnull(S.InvoiceNo,'')) [InvoiceNo], isnull
(S.Mobile,isnull(cust.Mobile,'')) Mobile,S.Email, U.Name as CreatedBy,
--sum(case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end ) as AmountBeforeDiscount,
SR.ReturnItemAmount as AmountBeforeDiscount,
--sum(case when isNull(sri.MrpSellingPrice,0)=0 
--then (((ps.SellingPrice*sri.Quantity) * (isnull(s.Discount,0) + isnull(sri.Discount,0)))/100)
--else (((sri.MrpSellingPrice*sri.Quantity) * (isnull(s.Discount,0) + isnull(sri.Discount,0)))/100) end)   as DiscountedAmount,   
SR.TotalDiscountValue as DiscountedAmount,
--sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity
--else ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end) as ReturnedItemDiscount,
--sum(case when isNull(s.Discount, 0) =0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity
--end else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity
--else (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) as ReturnedPrice,
SR.NetAmount-ISNULL(sr.ReturnCharges,0) as NetValue,
SR.NetAmount AS ReturnedPrice,
SR.RoundOffNetAmount,
SR.IsRoundOff,
Ins.Name [Branch],
isnull(sr.ReturnCharges,0) ReturnCharges
 
FROM SalesReturn SR (NOLOCK)
Inner join (Select * from instance where accountid= @AccountId ) Ins on Ins.id = sR.InstanceId
Left join Patient cust on cust.id  = sr.PatientId 
Left JOIN Sales S  (NOLOCK) ON s.Id = sr.SalesId
 
INNER JOIN SalesReturnItem SRI  (NOLOCK) on sri.SalesReturnId =  sr.Id
INNER JOIN ProductStock PS  (NOLOCK) on ps.id = sri.ProductStockId
INNER JOIN Product P  (NOLOCK) on p.Id = ps.ProductId
LEFT OUTER JOIN HQUEUSER U  (NOLOCK) on U.Id = s.CreatedBy
WHERE SR.AccountId  =  @AccountId AND SR.InstanceId  = ISNULL(@InstanceId,SR.InstanceId) AND ISNULL(   S.Cancelstatus,0) =0  
and SR.ReturnDate BETWEEN @StartDate and @EndDate 
and sri.Quantity >0
--AND isnull(S.InvoiceSeries,'') =''
and (sri.IsDeleted is null or sri.IsDeleted != 1)
GROUP BY SR.ReturnNo,SR.Prefix,sr.InvoiceSeries,SR.ReturnDate,isnull(S.Name,isnull(cust.name,'')) ,
ltrim(isnull(S.Prefix,'')+isnull(S.InvoiceSeries,'') +' '+ isnull(S.InvoiceNo,'')),isnull(S.Mobile,isnull(cust.Mobile,'')),
S.Email,U.Name, Ins.Name,isnull(sr.ReturnCharges,0),SR.ReturnItemAmount,SR.TotalDiscountValue,
SR.RoundOffNetAmount,SR.IsRoundOff,SR.NetAmount
									 
ORDER BY SR.ReturnNo DESC

END

ELSE IF (@SearchOption ='2')
BEGIN

SELECT ltrim(isnull(SR.Prefix,'')+' '+ISNULL(sr.InvoiceSeries,'')+' '+ SR.ReturnNo)as ReturnNo,SR.ReturnDate,isnull(S.Name,isnull(cust.name,''))  Name,ltrim(isnull(S.Prefix,'')+isnull(S.InvoiceSeries,'') +' '+ isnull(S.InvoiceNo,'')) [InvoiceNo], isnull(S
.Mobile,isnull(cust.Mobile,''))Mobile,S.Email, U.Name as CreatedBy,
--sum(case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri
--.MrpSellingPrice)*sri.Quantity end ) as AmountBeforeDiscount,
SR.ReturnItemAmount as AmountBeforeDiscount,
--sum(case when isNull(sri.MrpSellingPrice,0)=0 
--then (((ps.SellingPrice*sri.Quantity) * (isnull(s.Discount,0) + isnull(sri.Discount,0)))/100)
--else (((sri.MrpSellingPrice*sri.Quantity) * (isnull(s.Discount,0) + isnull(sri.Discount,0)))/100) end)   as DiscountedAmount, 
SR.TotalDiscountValue as DiscountedAmount,  
--sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity
--else ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end) as ReturnedItemDiscount,
--sum(case when isNull(s.Discount, 0) =0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity
--end else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity
--else (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) as ReturnedPrice, 
SR.NetAmount-ISNULL(sr.ReturnCharges,0) as NetValue,
SR.NetAmount AS ReturnedPrice,
SR.RoundOffNetAmount,
SR.IsRoundOff,
Ins.Name [Branch], isnull(sr.ReturnCharges,0) ReturnCharges
 
FROM SalesReturn SR (NOLOCK)
Inner join (Select * from instance where accountid= @AccountId ) Ins on Ins.id = sR.InstanceId
Left join Patient cust on sr.PatientId = cust.id 
Left JOIN Sales S  (NOLOCK) ON s.Id = sr.SalesId
 
 AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%' 
AND isnull(S.InvoiceSeries,'') !='' AND isnull(S.InvoiceSeries,'') !='MAN'
INNER JOIN SalesReturnItem SRI  (NOLOCK) on sri.SalesReturnId =  sr.Id
INNER JOIN ProductStock PS  (NOLOCK) on ps.id = sri.ProductStockId
INNER JOIN Product P  (NOLOCK) on p.Id = ps.ProductId
LEFT OUTER JOIN HQUEUSER U  (NOLOCK) on U.Id = s.CreatedBy
WHERE SR.AccountId  =  @AccountId AND SR.InstanceId  = ISNULL(@InstanceId,SR.InstanceId) AND ISNULL(   S.Cancelstatus,0) =0  
and SR.ReturnDate BETWEEN @StartDate and @EndDate
and (sri.IsDeleted is null or sri.IsDeleted != 1)
and sri.Quantity >0
GROUP BY SR.ReturnNo,SR.Prefix,sr.InvoiceSeries,SR.ReturnDate,isnull(S.Name,isnull(cust.name,'')),
ltrim(isnull(S.Prefix,'')+isnull(S.InvoiceSeries,'') +' '+ isnull(S.InvoiceNo,'')) ,isnull(S.Mobile,isnull(cust.Mobile,'')),
S.Email,U.Name, Ins.Name,isnull(sr.ReturnCharges,0),SR.ReturnItemAmount,SR.TotalDiscountValue,
SR.RoundOffNetAmount,SR.IsRoundOff,SR.NetAmount										 
ORDER BY SR.ReturnNo DESC

END

ELSE 
BEGIN

SELECT ltrim(isnull(SR.Prefix,'')+' '+ISNULL(sr.InvoiceSeries,'')+' '+ SR.ReturnNo)as ReturnNo,SR.ReturnDate,isnull(S.Name,isnull(cust.name,''))  Name, ltrim(isnull(S.Prefix,'')+isnull(S.InvoiceSeries,'') +' '+ isnull(S.InvoiceNo,'')) [InvoiceNo], isnull(
S.Mobile,isnull(cust.Mobile,'')) Mobile,S.Email, U.Name as CreatedBy,
--sum(case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end ) as AmountBeforeDiscount,
SR.ReturnItemAmount as AmountBeforeDiscount,
--sum(case when isNull(sri.MrpSellingPrice,0)=0 
--then (((ps.SellingPrice*sri.Quantity) * (isnull(s.Discount,0) + isnull(sri.Discount,0)))/100)
--else (((sri.MrpSellingPrice*sri.Quantity) * (isnull(s.Discount,0) + isnull(sri.Discount,0)))/100) end)   as DiscountedAmount,
SR.TotalDiscountValue as DiscountedAmount,     
--sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity
--else ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end) as ReturnedItemDiscount,
--sum(case when isNull(s.Discount, 0) =0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity
--end else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity
--else (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) as ReturnedPrice,
SR.NetAmount-ISNULL(sr.ReturnCharges,0) as NetValue,
SR.NetAmount AS ReturnedPrice,
SR.RoundOffNetAmount,
SR.IsRoundOff,
Ins.Name [Branch], isnull(sr.ReturnCharges,0) ReturnCharges
 
FROM SalesReturn SR (NOLOCK)
Inner join (Select * from instance where accountid= @AccountId ) Ins on Ins.id = sR.InstanceId
Left join Patient cust on sr.PatientId = cust.id 
Left JOIN Sales S  (NOLOCK) ON s.Id = sr.SalesId
 
INNER JOIN SalesReturnItem SRI  (NOLOCK) on sri.SalesReturnId =  sr.Id
INNER JOIN ProductStock PS  (NOLOCK) on ps.id = sri.ProductStockId
INNER JOIN Product P  (NOLOCK) on p.Id = ps.ProductId
LEFT OUTER JOIN HQUEUSER U  (NOLOCK) on U.Id = s.CreatedBy
WHERE SR.AccountId  =  @AccountId AND SR.InstanceId  = ISNULL(@InstanceId,SR.InstanceId) AND ISNULL(   S.Cancelstatus,0) =0  
and SR.ReturnDate BETWEEN @StartDate and @EndDate AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%'
and (sri.IsDeleted is null or sri.IsDeleted != 1)
and sri.Quantity >0
GROUP BY SR.ReturnNo,SR.Prefix,sr.InvoiceSeries,SR.ReturnDate,isnull(S.Name,isnull(cust.name,'')),
ltrim(isnull(S.Prefix,'')+isnull(S.InvoiceSeries,'') +' '+ isnull(S.InvoiceNo,'')),isnull(S.Mobile,isnull(cust.Mobile,'')),
S.Email,U.Name	, Ins.Name,isnull(sr.ReturnCharges,0),SR.ReturnItemAmount,SR.TotalDiscountValue,
SR.RoundOffNetAmount,SR.IsRoundOff,SR.NetAmount							 
ORDER BY SR.ReturnNo DESC

END


END