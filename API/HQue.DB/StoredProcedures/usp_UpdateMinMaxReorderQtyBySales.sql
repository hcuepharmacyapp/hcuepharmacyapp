﻿CREATE PROCEDURE [dbo].[usp_UpdateMinMaxReorderQtyBySales](@AccountId CHAR(36), @InstanceId CHAR(36), @fromDate DATE, @toDate DATE, @MinReorderFactor INT, @MaxReorderFactor INT, @OfflineStatus BIT, @updatedBy CHAR(36))
AS
BEGIN
	SET NOCOUNT ON

	UPDATE PINS SET PINS.ReOrderLevel = ROUND(R.AvgSoldQty * @MinReorderFactor, 0),
	PINS.ReOrderQty = ROUND(CASE WHEN ISNULL(R.AvgSoldQty,0) <> 0 THEN R.AvgSoldQty * @MaxReorderFactor
	ELSE ISNULL(RR.PackageSize,ISNULL(R.PackageSize,0)) END, 0),
	PINS.OfflineStatus = @OfflineStatus,
	PINS.UpdatedAt = getdate(),
	PINS.UpdatedBy = @updatedBy
	FROM 
	(
		SELECT P.Id,P.Name,P.PackageSize,
		ISNULL(R1.SoldQty,0) AS SoldQty,
		CAST((ISNULL(R1.SoldQty,0)/(SELECT DATEDIFF(DAY, CAST(@fromDate AS DATE), CAST(@toDate AS DATE)) + 1)) AS MONEY) AS AvgSoldQty
		FROM Product P 
		LEFT JOIN 
		(
			SELECT PS.ProductId,SUM(SI.Quantity-ISNULL(SR.ReturnQty,0)) AS SoldQty FROM ProductStock PS
			INNER JOIN SalesItem SI ON SI.ProductStockId = PS.Id
			INNER JOIN Sales S ON S.Id = SI.SalesId
			LEFT JOIN 
			(
				SELECT SR.SalesId,SRI.ProductStockId,SUM(SRI.Quantity) AS ReturnQty FROM SalesReturn SR 
				INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
				WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceId
				AND SR.ReturnDate BETWEEN CAST(@fromDate AS DATE) AND CAST(@toDate AS DATE)
				GROUP BY SR.SalesId,SRI.ProductStockId 
			) SR 
			ON SR.SalesId = SI.SalesId AND SR.ProductStockId = SI.ProductStockId		
			WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId
			AND S.InvoiceDate BETWEEN CAST(@fromDate AS DATE) AND CAST(@toDate AS DATE)
			AND S.Cancelstatus IS NULL
			GROUP BY PS.ProductId
		) R1 ON R1.ProductId = P.Id
		GROUP BY P.Id,P.Name,P.PackageSize,ISNULL(R1.SoldQty,0)
	) R 
	INNER JOIN ProductInstance PINS ON PINS.ProductId = R.Id
	LEFT JOIN
	(	
		SELECT PS1.ProductId,PS1.PackageSize 		
		FROM ProductStock PS1 
		INNER JOIN 
		(
			SELECT PS2.ProductId,MAX(PS2.CreatedAt) AS CreatedAt FROM ProductStock PS2
			WHERE PS2.AccountId = @AccountId AND PS2.InstanceId = @InstanceId
			GROUP BY PS2.ProductId
		) R2 ON R2.ProductId = PS1.ProductId AND R2.CreatedAt = PS1.CreatedAt
		WHERE PS1.AccountId = @AccountId AND PS1.InstanceId = @InstanceId
	) RR ON RR.ProductId = PINS.ProductId
	WHERE PINS.AccountId = @AccountId AND PINS.InstanceId = @InstanceId
	AND ISNULL(PINS.ReOrderModified,0) = 0 

	SELECT CAST(1 AS BIT) AS Result 
END
