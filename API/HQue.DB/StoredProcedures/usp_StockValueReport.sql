/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         

** 11/10/2017	nandhini		gst column added

** 13/10/2017	Poongodi		Formula changed
*******************************************************************************/
 
 
 Create PROCEDURE [dbo].[usp_StockValueReport](@InstanceId varchar(36),@AccountId varchar(36))
 AS
 BEGIN
 SET NOCOUNT ON  

select A.Category,
cast(sum(A.Vat0_Price) as numeric(20,2)) Vat0_PriceTotal,
cast(sum(A.Vat145_Price) as numeric(20,2)) Vat145_PriceTotal,
cast(sum(A.Vat5_Price) as numeric(20,2)) Vat5_PriceTotal,
cast(sum(A.VatOther_Price) as numeric(20,2)) VatOther_PriceTotal,
cast(sum(A.Gst0_Price) as numeric(20,2)) Gst0_PriceTotal,
cast(sum(A.Gst5_Price) as numeric(20,2)) Gst5_PriceTotal,
cast(sum(A.Gst12_Price) as numeric(20,2)) Gst12_PriceTotal,
cast(sum(A.Gst18_Price) as numeric(20,2)) Gst18_PriceTotal,
cast(sum(A.Gst28_Price) as numeric(20,2)) Gst28_PriceTotal,
cast(sum(A.GstOther_Price) as numeric(20,2)) GstOther_PriceTotal,
cast(sum(A.MRP) as numeric(20,2)) MrpTotal, cast(COUNT(A.Name) as decimal) ProductCount,
--cast(sum(A.Vat0_Price)+sum(A.Vat145_Price)+sum(A.Vat5_Price)+sum(A.VatOther_Price) as numeric(20,2)) 'PriceTotal',
cast(sum(A.Gst0_Price)+sum(A.Gst5_Price)+sum(A.Gst12_Price)+sum(A.Gst18_Price) +sum(A.Gst28_Price)+sum(A.GstOther_Price)as numeric(20,2)) 'PriceTotal'
--,cast((SUM(A.MRP)-(sum(A.Vat0_Price)+sum(A.Vat145_Price)+sum(A.Vat5_Price)+sum(A.VatOther_Price)))*100/case when (sum(A.Vat0_Price)+sum(A.Vat145_Price)+sum(A.Vat5_Price)+sum(A.VatOther_Price)) > 0 then (sum(A.Vat0_Price)+sum(A.Vat145_Price)+sum(A.Vat5_Price)+sum(A.VatOther_Price)) else 1 end as numeric(20,2)) 'ProfitPercentage'  

--,cast((SUM(A.MRP)-(sum(A.Gst0_Price)+sum(A.Gst5_Price)+sum(A.Gst12_Price)+sum(A.Gst18_Price) +sum(A.Gst28_Price)+sum(A.GstOther_Price)))*100/case when (sum(A.Gst0_Price)+sum(A.Gst5_Price)+sum(A.Gst12_Price)+sum(A.Gst18_Price) +sum(A.Gst28_Price)+sum(A.GstOther_Price)) > 0 then (sum(A.Gst0_Price)+sum(A.Gst5_Price)+sum(A.Gst12_Price)+sum(A.Gst18_Price) +sum(A.Gst28_Price)+sum(A.GstOther_Price)) else 1 end as numeric(20,2)) 'ProfitPercentage'  
,cast((SUM(A.MRP)-(sum(A.Gst0_Price)+sum(A.Gst5_Price)+sum(A.Gst12_Price)+sum(A.Gst18_Price) +sum(A.Gst28_Price)+sum(A.GstOther_Price)))*100 /case when sum(a.mrp)>0 then sum(a.mrp) end as numeric(20,2)) 'ProfitPercentage'

from
(
select p.Name,isnull(p.Category,'') Category,ps.Stock, isnull(ps.PurchasePrice,0)  PurchasePrice,sum(vat.PoPrice) as 'vat.PoPrice',(gst.PoPrice)as 'gst.PoPrice',isnull(gst.gst,0) 'GST',
isnull(vat.vat,0)  'VAT',
sum(case isnull(vat.vat,0)  when 0 then 
(vat.poprice)
else 0 end ) [Vat0_Price],
sum(case isnull(vat.vat,0)  when 5 then 
(vat.poprice)
else 0 end )[Vat5_Price],
sum(case isnull(vat.vat,0)  when 14.5 then 
(vat.poprice)
else 0 end) [Vat145_Price],
sum(case   when isnull(vat.vat,0) in (5,14.5,0) then  0 else
(vat.poprice)
end )[VatOther_Price],


sum(case isnull(gst.gst,0)  when 0 then 
(gst.PoPrice)
else 0 end ) [Gst0_Price],
sum(case isnull(gst.gst,0)  when 5 then 
(gst.PoPrice)
else 0 end ) [Gst5_Price],
sum(case isnull(gst.gst,0)  when 12 then 
(gst.PoPrice)
else 0 end ) [Gst12_Price],
sum(case isnull(gst.gst,0)  when 18 then 
(gst.PoPrice)
else 0 end ) [Gst18_Price],
sum(case isnull(gst.gst,0)  when 28 then 
(gst.PoPrice)
else 0 end ) [Gst28_Price],
sum(case   when isnull(gst.gst,0) in (0,5,12,18,28) then  0 else
(gst.poprice)
end )[GstOther_Price],
sum(isnull(ps.Stock,0)*isnull(ps.SellingPrice,0)) 'MRP' from 
( SElect * from Product (nolock)   where accountid =@AccountId) p join 
(Select * from ProductStock (nolock)  where instanceid =@Instanceid and  accountid =@AccountId and  Stock > 0)ps on ps.ProductId=p.Id
-- LEFT   JOIN (
--(select distinct vendorpurchaseid,instanceid, productstockid, PurchasePrice from vendorpurchaseitem (nolock) where InstanceId = @InstanceId and 
--VendorPurchaseid in (select id from vendorpurchase (nolock)  vp where InstanceId = @InstanceId 	and isnull(CancelStatus ,0) =0  ))
--) vpi ON (PS.Id = vpi.productstockid) 
cross apply (select case isnull(ps.cst,0) when 0 then isnull(ps.VAT,0) else  ps.CST  end as VAT,
--(isnull(ps.Stock,0) *isnull(vpi.PurchasePrice,isnull(ps.PurchasePrice,0)))
isnull(ps.Stock,0) *(	 CASE ISNULL(ps.PackageSize,0) WHEN 0 THEN	ISNULL(ps.PurchasePrice,0)  
	   ELSE  case ISNULL(PackagePurchasePrice,0)  when 0 then ISNULL(ps.PurchasePrice,0) 
	    else   (ISNULL(PackagePurchasePrice,0)/ISNULL(ps.PackageSize,0)  ) + ((ISNULL(PackagePurchasePrice,0)/ISNULL(ps.PackageSize,0) )* (ISNULL(ps.VAT,0)  /100) ) END  end  ) [PoPrice]

 ) as vat
 cross apply (select  isnull(ps.GstTotal,0) as GST,
----(isnull(ps.Stock,0) *isnull(vpi.PurchasePrice,isnull(ps.PurchasePrice,0))) ,
-- isnull(ps.Stock,0) *(CASE WHEN ((ISNULL(ps.VAT,0) + isnull(ps.gsttotal,0)  =  0 ) or (ISNULL(ps.VAT,0) =0 and isnull(ps.gsttotal,0) >0)) THEN  ISNULL(ps.PurchasePrice,0)  
-- else (isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0))) +((isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0)))* isnull(ps.gsttotal,0) /100)    end)
  isnull(ps.Stock,0) *(CASE isnull(taxrefno,0)  WHEN 1 THEN  ISNULL(ps.PurchasePrice,0)  
 else (isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0))) +((isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0)))* isnull(ps.gsttotal,0) /100)    end)
		 [PoPrice]
 ) as gst
where ps.InstanceId=@InstanceId
group by p.Category,p.Name,ps.Stock,ps.PurchasePrice,ps.SellingPrice,ps.VAT,ps.CST, 
isnull(vat.vat,0),isnull(gst.GST,0),gst.PoPrice
)A
group by A.Category
order by A.Category

END
