﻿CREATE PROCEDURE [dbo].[usp_CheckCustomerVendorOBPaid](@InstanceId varchar(36),@AccountId varchar(36),@CustomerId char(36), @type varchar(15))
AS
 BEGIN
		
		IF(@type = 'CUS')
		BEGIN
		select Isnull(sum(debit),0) Debit from customerpayment 
		Where customerid = @CustomerId
		and accountid = @AccountId
		and instanceid = @InstanceId
		and salesid is null 
		Having sum(debit) > 0
		END
		ELSE IF(@type = 'VEN')
		BEGIN
		select  Isnull(sum(debit),0) Debit from payment 
		Where vendorid = @CustomerId
		and accountid = @AccountId
		and instanceid = @InstanceId
		and vendorpurchaseid is null 
		Having sum(debit) > 0

		END
 END