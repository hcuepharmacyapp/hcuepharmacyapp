  --Usage : -- usp_GetDistributorBranchList 'saravanakumarkb@gmail.com'--,'4edd19c2-3fe3-4ba2-8083-83a06ccd6c88'
CREATE PROCEDURE [dbo].[usp_GetDistributorBranchList](@EmailId varchar(100))
AS
 BEGIN
   SET NOCOUNT ON
   SELECT Id,Name,Phone,Address,COUNT(*) AS [Count] 
   FROM (
        SELECT I.Id ,I.Name,I.Phone ,
		     I.Address,Product.Id AS ProductId,ABS(SUM(PS.Stock)) AS [Sum] 
		FROM Vendor V WITH(NOLOCK)
        INNER JOIN Instance  I  WITH(NOLOCK) ON I.Id = V.InstanceId
        INNER JOIN VendorPurchase VP  WITH(NOLOCK) ON V.Id = VP.VendorId
        INNER JOIN VendorPurchaseItem VPI  WITH(NOLOCK) ON VPI .VendorPurchaseId = VP.Id
        INNER JOIN ProductStock PS  WITH(NOLOCK) ON VPI.ProductStockId = PS.Id
        INNER JOIN Product  WITH(NOLOCK) ON Product.Id = PS.ProductId
        WHERE V.Email = @EmailId AND V.AllowViewStock = 1
        GROUP BY I.Id,I.Name,I.Phone,I.Address,Product.Id
        HAVING ABS(SUM(PS.Stock)) < 50
   ) TMP
   GROUP BY Id,Name,Phone,Address
   ORDER BY [Count] DESC

END
 


