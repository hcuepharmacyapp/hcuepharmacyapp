﻿/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 27/07/2017	Poongodi		 Vendor Location Type validation added 
** 13/09/2017   Lawrence		 All branch condition Added 
** 26/04/2018   Sarubala		 Included DCNo in report 
** 07/05/2018   nandhini		 Status Column added in report 
******************************************************************************* 
--usp_GetDCList 'c6b7fc38-8e3a-48af-b39d-06267b4785b4', 'c503ca6e-f418-4807-a847-b6886378cf0b', '01-Sep-2017', '14-Sep-2017', null, null
--usp_GetDCList null, 'c503ca6e-f418-4807-a847-b6886378cf0b', '01-Sep-2017', '14-Sep-2017', null, null

*/
create PROCEDURE [dbo].[usp_GetDCList](@InstanceId VARCHAR(36), 
                                       @AccountId  VARCHAR(36), 
                                       @StartDate  DATETIME, 
                                       @EndDate    DATETIME, 
                                       @ProductId  NVARCHAR(36) ='', 
                                       @VendorId   NVARCHAR(36) ='') 
AS 
  BEGIN 
      SET nocount ON 

      SELECT @ProductId = Isnull(@ProductId, ''), 
             @VendorId = Isnull(@VendorId, '') 

      SELECT dc.productstockid,
             productstock.productid,
			 dc.DCDate,
			 dc.DCNo,				
		 Status = CASE 
        WHEN dc.isActive = '0' and dc.isPurchased is null THEN 'Deleted'
		 WHEN dc.isActive = '0' and dc.isPurchased='1' THEN 'Converted'
		  WHEN dc.isActive = '1' and dc.isPurchased='1' THEN 'Converted'
		  WHEN dc.isActive = '1' and dc.isPurchased is null THEN 'Pending'
       
		 ELSE 'Other' END,

             dc.quantity As Quantity, 
             dc.purchaseprice Purchaseprice, 
             dc.packagepurchaseprice [PackagePurchasePrice], 
             dc.packageqty as PackageQty, 
             dc.packagesellingprice [PackageSellingPrice], 
             Isnull(dc.packageqty, 0) [ActualQty], Round((Isnull(pocal.Poval, 0)) * 
                   (case when isnull(dc.TaxRefNo,0) = 1 then isnull(productstock.GstTotal,0) else Isnull(productstock.VAT, 0) end)  / 100, 2)  [VatValue],    
         ((( Isnull(dc.packageqty, 0)) * dc.packagepurchaseprice ) - ( ( (Isnull(dc.packageqty, 0)) * dc.packagepurchaseprice ) * Isnull(dc.discount, 0) / 100 ) + ( (( ( Isnull(dc.packageqty, 0)) * dc.packagepurchaseprice ) - ( ( (Isnull(dc.packageqty, 0)) * dc.packagepurchaseprice ) * Isnull(dc.discount, 0) / 100 ) ) * ((case when isnull(dc.TaxRefNo,0) = 1 then isnull(productstock.GstTotal,0) else Isnull(productstock.VAT, 0) end) / 100 ))) AS ReportTotal, 
  dc.freeqty [FreeQty], 
  dc.discount [Discount], 
   cast(isnull(dc.TaxRefNo,0) as int) [TaxRefNo],
  vendor.id                             AS [Vendor.Id], 
  vendor.NAME                           AS [VendorName], 
  vendor.mobile                         AS [VendorMobile], 
  vendor.email                          AS [VendorEmail], 
  vendor.address                        AS [VendorAddress], 
  vendor.tinno                          AS [VendorTinNo], 
  isnull(Vendor.EnableCST,0)			AS [VendorEnableCST],
  productstock.sellingprice             AS [ProductStockSellingPrice], 
  productstock.vat                      AS [VAT], 
  productstock.cst                      AS [CST], 
  case isnull (vendor.LocationType,1)
	 when 1 then  productstock.GstTotal 
	 when 0 then  productstock.GstTotal 
	 else 0 end                 AS [GstTotal], 
   case isnull (vendor.LocationType,1)
	 when 1 then productstock.Cgst  
	  when 0 then productstock.Cgst  
	 else 0 end                    AS [Cgst], 
    case isnull (vendor.LocationType,1)
	 when 1 then productstock.Sgst   
	  when 0 then productstock.Sgst  
	 else 0 end                  AS [Sgst], 
  case isnull (vendor.LocationType,1)
	 when 2 then productstock.Igst 
	  else 0 end             AS [Igst], 
  product.NAME                          AS [ProductName] 
  ,Inst.Name AS InstanceName
  FROM DCVendorPurchaseItem dc with(nolock)
  
  INNER JOIN vendor with(nolock) 
          ON vendor.id = dc.vendorid 
  INNER JOIN productstock with(nolock) 
          ON productstock.id = dc.productstockid 
  INNER JOIN product with(nolock) 
          ON product.id = productstock.productid 
  INNER JOIN Instance Inst on (Inst.Id = ISNULL(@InstanceId,dc.InstanceId) and Inst.AccountId = @AccountId)
CRoss apply (select  Isnull(dc.packagepurchaseprice, 0) * ( Isnull( dc.packageqty, 0)) - (Isnull(dc.packagepurchaseprice, 0) * ( Isnull  (dc.packageqty, 0)) * Isnull(dc.discount, 0) /100) [Poval]) as pocal
  WHERE  dc.DCDate BETWEEN @StartDate AND @EndDate 
  AND dc.accountid = @AccountId 
  AND dc.instanceid = ISNULL(@InstanceId,dc.InstanceId) 
  AND ( ( dc.vendorid = @VendorId ) 
         OR ( @VendorId = '' ) ) 
  AND ( ( productstock.productid = @ProductId ) 
         OR ( @ProductId = '' ) ) 
  ORDER  BY dc.createdat DESC 
  END