﻿/*
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 10/08/2017 Poongodi R		 Opening stock function changed
 
*******************************************************************************/ 
-- exec usp_GetStockTransferList_Rpt 'c6b7fc38-8e3a-48af-b39d-06267b4785b4','c503ca6e-f418-4807-a847-b6886378cf0b','05-Jul-2017','05-Jul-2017'
Create PROCEDURE [dbo].[usp_GetStockTransferList_Rpt](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime)
AS
 BEGIN
   SET NOCOUNT ON
	declare @condition nvarchar(max)	
	Declare @qry nvarchar(max)
	SET @condition=''

 

Select ps.productid, '' TransferNo,Max(ST.TransferDate)TransferDate ,P.Name as ProductName,P.Manufacturer as Mfg,SUM(STI.Quantity) as TransferQty,
 max(isnull(os.OpeningStock ,0)) [OpeningStock], 
 cast( ( max(isnull(os.OpeningStock ,0)) +max(isnull(trans.inQty,0)) +max(isnull(trans.adjqty,0))) - max(isnull(trans.outqty,0)) as decimal) [CurrentStock],
MAX(V.Name) As   VendorName,/* Max(isnull(PoQty,0)) as PurchaseQty,*/
 ''  [TransferStatus],
'' TransferBy,	'' [ToTransfer], '' Name ,
	max(isnull(trans.inQty,0)) [InwardQty], max(isnull(trans.outqty,0)) OutwardQty, max(isnull(trans.adjqty,0)) [AdjQty]
	from StockTransfer ST 
	Inner Join StockTransferItem STI ON STI.TransferId=ST.Id

	Left join (SELECT Productid,sum(stock)  stock, max(vendorid) [VendorId] from ProductStock  
	where   ProductStock.AccountId = @AccountId and ProductStock.InstanceId =@InstanceId 
	group by  Productid) as PS on ps.ProductId = sti.ProductId

	Inner Join Product P ON P.id=PS.ProductId And P.AccountId= @AccountId
	Inner Join Vendor V ON V.id=PS.VendorId And V.AccountId= @AccountId

	Inner Join Instance I ON I.id=ST.ToInstanceId And I.AccountId= st.AccountId
	left join dbo.Udf_GetTransStock(@InstanceId,CAST(@StartDate AS date ) , CAST(@EndDate AS date)) as Trans
		on Trans.Productid = ps.ProductId
	Left join  (Select productid, ClosingQty [OpeningStock]  from dbo.Udf_GetOpeningStock(@InstanceId, @AccountId, CAST(@StartDate AS date ) ))  os on os.Productid = ps.productid

	--cross apply (select  dbo.Udf_GetOpeningStock(@InstanceId, ps.productid,  CAST(@StartDate AS date ) )[OpeningStock] ) OS
	 Where ST.InstanceId=  @InstanceId  AND ST.AccountId  = @AccountId 
	 AND CAST(ST.CreatedAt AS date) BETWEEN CAST(@StartDate AS date ) and CAST(@EndDate AS date)
	 and isnull(st.TransferStatus,0) in (1,3)
	  group by ps.productid, P.Name,P.Manufacturer  
 
 
 END

 