



/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 **30/05/2017  Nandhini			
 **22/06/2017   Violet			Prefix Added 
  **14/09/2017   nandhini		branch name Added 
  **14/09/2017   Sarubala 		isnull added in Gsttotal 
*******************************************************************************/ 
-- [Usp_Get_Consolidated_Sales_Return_Report] '852eaefa-f670-4a6a-a0e7-db3fa252d576','c0d8da9d-b797-4243-b691-68365d3c42de','SB','01-Apr-2017','25-May-2017'
CREATE PROCEDURE [dbo].[Usp_Get_Consolidated_Sales_Return_Report](@AccountId  CHAR(36), 
@InstanceId CHAR(36),
@SearchOption varchar(50), 
@CashType varchar(20), 
@StartDate  DATETIME, 
@EndDate    DATETIME) 
AS 
BEGIN 
	declare @InvoiceSeries varchar(100) = ''
	select  @InvoiceSeries = isnull(@SearchOption,'') 
	
	select @CashType = isNull(@CashType, '')
	
	SELECT   
	Branch,          
	TempSales2.returnno	AS [ReturnNo], 
	TempSales2.returndate AS [ReturnDate],
	TempSales2.Name AS [Name],
	RoundOffNetAmount,
	NetAmount,
	SUM(Gross52Amt) as Gross52Amt,
	SUM(vat52Amount) as vat52Amount,
	SUM(Gross131Amt) as Gross131Amt,
	SUM(vat131Amount) as vat131Amount,
	SUM(isnull(OtherVATAmt,0)) as OtherVATAmt ,
	SUM(isnull(OtherGrossAmt,0)) as OtherGrossAmt 
	from 
	(
		select  
		TempSales1.returnno, 
		TempSales1.returndate,
		TempSales1.Name,
		TempSales1.GstTotal,
		Branch,
		RoundOffNetAmount,
		NetAmount,
		case isnull(TempSales1.GstTotal,0) when 5.2 then SUM(isnull(VatAmount,0)) else 0 end as vat52Amount,
		case isnull(TempSales1.GstTotal,0) when 5.2 then sum(isnull(GrossAmount,0)) else 0 end as Gross52Amt,
		case isnull(TempSales1.GstTotal,0) when 13.1 then SUM(isnull(VatAmount,0)) else 0 end  as vat131Amount, 
		case isnull(TempSales1.GstTotal,0) when 13.1 then sum(isnull(GrossAmount,0)) else 0 end as Gross131Amt  ,
		case when isnull(TempSales1.GstTotal,0) not in (5.2, 13.1 ) then sum(isnull(VatAmount,0)) else 0 end as OtherVATAmt  ,
		case when isnull(TempSales1.GstTotal,0) not in (5.2, 13.1 ) then sum(isnull(GrossAmount,0)) else 0 end as OtherGrossAmt  
		from 
		(
			select 
			ltrim(isnull(SR.Prefix,'')+' '+isnull(SR.InvoiceSeries,'')+' '+ SR.ReturnNo) as ReturnNo,
			isnull(s.Name,p.Name) Name,
			sr.returndate ,
			sr.RoundOffNetAmount AS RoundOffNetAmount,
			sr.NetAmount AS NetAmount,
			isnull(ps.GstTotal,0) GstTotal,
			sri.TotalAmount,
			sri.TotalAmount-d.TaxAmount AS GrossAmount,
			d.TaxAmount AS VatAmount,
			ins.Name as Branch
			from	
			salesreturnitem sri(nolock)
			Inner join (Select * from instance(nolock) where accountid= @AccountId ) Ins on Ins.id = sri.InstanceId
			INNER JOIN salesreturn sr(nolock) ON sr.id = sri.salesreturnid 
			LEFT JOIN sales s (nolock) ON s.id = sr.salesid 
			INNER JOIN ProductStock ps(nolock) on sri.ProductStockId=ps.Id
			left join patient p(nolock) on p.id=sr.PatientId
			left join voucher v(nolock) on v.ReturnId = sr.id And v.AccountId = sr.AccountId And v.InstanceId = sr.InstanceId
			CROSS APPLY (SELECT CASE WHEN ISNULL(sr.TaxRefNo,0) = 1 THEN ISNULL(sri.GstAmount,0) 
			ELSE (sri.mrpsellingprice*sri.quantity-ISNULL(sri.DiscountAmount,0)/(100+isnull(ps.vat,0))*100)*isnull(ps.vat,0)/100 END AS TaxAmount) AS d
			where sr.AccountId = @AccountId AND sr.InstanceId = ISNULL(@InstanceId,sr.InstanceId) AND sr.returndate between @StartDate and @EndDate 
			AND isnull(s.InvoiceSeries,'')+isnull(s.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%' 
			and s.Cancelstatus is NULL
			AND case when s.id is null then case when v.id is null then 'Full' Else 'Credit' end else isNull(s.CashType, '') end like isnull(@CashType,'')+'%'
			and (sri.IsDeleted is null or sri.IsDeleted != 1)
			and sri.Quantity>0
		) as TempSales1 group by TempSales1.returnno,TempSales1.returndate,TempSales1.Name,TempSales1.GstTotal,Branch,NetAmount,RoundOffNetAmount
	) as TempSales2 group by TempSales2.returnno,TempSales2.Name,TempSales2.returndate,Branch,NetAmount,RoundOffNetAmount 	  
	ORDER BY TempSales2.ReturnNo,TempSales2.ReturnDate asc

END