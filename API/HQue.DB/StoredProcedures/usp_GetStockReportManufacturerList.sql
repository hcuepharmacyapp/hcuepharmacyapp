﻿ 
/*******************************************************************************                            
** ManufacturerList List for fill the auto complete textbox in stock report                 
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 13/03/2017	Lawrence	   Manufacture
*******************************************************************************/ 
 --Usage : -- usp_GetStockReportList '28c860a2-21c4-4642-9b14-40d29f411878'--,'4edd19c2-3fe3-4ba2-8083-83a06ccd6c88'
 --[usp_GetStockReportManufacturerList] '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','nonzero','TABLET'
 --[usp_GetStockReportManufacturerList] '3e2ec066-1551-4527-be53-5aa3c5b7fb7d','nonzero','a'
 --usp_GetStockReportManufacturerList '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','zero'
 --usp_GetStockReportManufacturerList '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','expire'
 --usp_GetStockReportManufacturerList '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','all'
CREATE PROCEDURE [dbo].[usp_GetStockReportManufacturerList](@InstanceId varchar(36), @StockType varchar(10), @manufacturer varchar(10))
AS
 BEGIN
  Declare @From_Stock int=0, @To_Stock int =0, @From_Expiry date =NULL, @To_Expiry date =NULL 
 declare @Status table (stockstatus int )
 if (right(@StockType,1) ='2')
 begin
 insert into @Status 	values (2)
 select @StockType  = replace (@StockType,2,'')
 end 
 else
 begin
 insert into @Status 	values (1),(0)
 end 

 if (@StockType ='nonzero')
	begin
	select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  = cast(getdate () as date), @To_Expiry  = NULL
	 
	end 
 else if (@StockType ='expire')
	begin
	select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  =  NULL, @To_Expiry  =cast(getdate () as date)
	
	end

  else if (@StockType ='all')
	begin
	select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  =  NULL, @To_Expiry  = NULL	
	end  

   SELECT distinct P.Manufacturer FROM ProductStock PS WITH(NOLOCK)
    INNER JOIN Product P  WITH(NOLOCK) ON P.Id = PS.ProductId 
    LEFT JOIN Vendor  WITH(NOLOCK) ON Vendor.Id = PS.VendorId 
     WHERE PS.InstanceId = @InstanceId 
	 	 and ps.Stock between isnull(@From_Stock , ps.stock) and isnull(@To_Stock , ps.stock) 
		 and P.Manufacturer like ''+@manufacturer+'%'
		  
	 and cast(ps.ExpireDate  as date)  between isnull(@From_Expiry ,cast(ps.ExpireDate  as date) ) and isnull(@To_Expiry,cast(ps.ExpireDate  as date) )
    ORDER BY P.Manufacturer 

END