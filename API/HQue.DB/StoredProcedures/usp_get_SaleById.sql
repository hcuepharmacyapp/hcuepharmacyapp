/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 27/07/2017	Poongodi		SP Optimized  
** 17/08/2017   Settu           ProductStock.GstTotal NULL values handled
** 08/09/2017   Sarubala		SalesItem product name added
** 24/10/2017 Settu				PurchaseBarcode field added
** 06/10/2017 Settu				Cgst/Igst/Sgst should not picked from product stock always
** 17/05/2018 nandhini			Item order changed by SalesItemSno
*******************************************************************************/ 
Create Proc usp_get_SaleById (@SaleId char(36),@InstanceId char(36),@Accountid Char(36))
AS
Begin
SELECT SalesItem.Id,SalesItem.AccountId,SalesItem.InstanceId,SalesItem.SalesId,SalesItem.SaleProductName,
SalesItem.ProductStockId,SalesItem.ReminderFrequency,SalesItem.ReminderDate,
SalesItem.Quantity,SalesItem.ItemAmount,SalesItem.VAT,SalesItem.VatAmount,SalesItem.DiscountAmount,
SalesItem.Discount,SalesItem.SellingPrice,SalesItem.MRP,SalesItem.OfflineStatus,SalesItem.Igst,SalesItem.Cgst,
SalesItem.Sgst,SalesItem.GstTotal,SalesItem.GstAmount,SalesItem.CreatedAt,SalesItem.UpdatedAt,SalesItem.CreatedBy,
SalesItem.UpdatedBy,SalesItem.SalesOrderEstimateId,SalesItem.SalesOrderEstimateType,SalesItem.LoyaltyProductPts,
Sales.Id AS [Sales.Id],Sales.AccountId AS [Sales.AccountId],Sales.InstanceId AS [Sales.InstanceId],
Sales.PatientId AS [Sales.PatientId],Sales.Prefix AS [Sales.Prefix],Sales.InvoiceSeries AS [Sales.InvoiceSeries],Sales.InvoiceNo AS [Sales.InvoiceNo],
Sales.InvoiceDate AS [Sales.InvoiceDate],Sales.Name AS [Sales.Name],Sales.Mobile AS [Sales.Mobile],Sales.Email AS [Sales.Email],Sales.DOB AS [Sales.DOB],
Sales.Age AS [Sales.Age],Sales.Gender AS [Sales.Gender],Sales.Address AS [Sales.Address],ISNULL(ISNULL(Sales.Pincode,Patient.Pincode),'') AS [Sales.Pincode],Sales.DoctorName AS [Sales.DoctorName],
Sales.DoctorMobile AS [Sales.DoctorMobile],Sales.PaymentType AS [Sales.PaymentType],Sales.DeliveryType AS [Sales.DeliveryType],Sales.BillPrint AS [Sales.BillPrint],
Sales.SendSms AS [Sales.SendSms],Sales.SendEmail AS [Sales.SendEmail],Sales.Discount AS [Sales.Discount],Sales.DiscountValue AS [Sales.DiscountValue],
Sales.DiscountType AS [Sales.DiscountType],Sales.RoundoffNetAmount AS [Sales.RoundoffNetAmount],Sales.Credit AS [Sales.Credit],Sales.FileName AS [Sales.FileName],
Sales.OfflineStatus AS [Sales.OfflineStatus],Sales.CreatedAt AS [Sales.CreatedAt],Sales.UpdatedAt AS [Sales.UpdatedAt],Sales.CreatedBy AS [Sales.CreatedBy],
Sales.UpdatedBy AS [Sales.UpdatedBy],Sales.CashType AS [Sales.CashType],Sales.CardNo AS [Sales.CardNo],Sales.CardDate AS [Sales.CardDate],Sales.ChequeNo AS [Sales.ChequeNo],
Sales.ChequeDate AS [Sales.ChequeDate],Sales.CardDigits AS [Sales.CardDigits],Sales.CardName AS [Sales.CardName],Sales.Cancelstatus AS [Sales.Cancelstatus],
Sales.SalesType AS [Sales.SalesType],Sales.NetAmount AS [Sales.NetAmount],Sales.BankDeposited AS [Sales.BankDeposited],Sales.AmountCredited AS [Sales.AmountCredited],
Sales.FinyearId AS [Sales.FinyearId],Sales.DoctorId AS [Sales.DoctorId],Sales.VatAmount AS [Sales.VatAmount],Sales.SalesItemAmount AS [Sales.SalesItemAmount],
Sales.LoyaltyPts AS [Sales.LoyaltyPts],Sales.RedeemPts AS [Sales.RedeemPts],Sales.RedeemAmt AS [Sales.RedeemAmt],Sales.DoorDeliveryStatus AS [Sales.DoorDeliveryStatus],
Sales.GstAmount AS [Sales.GstAmount],Sales.TaxRefNo AS [Sales.TaxRefNo],Sales.LoyaltyId AS [Sales.LoyaltyId],Sales.RedeemPercent AS [Sales.RedeemPercent],Sales.TotalDiscountValue AS [Sales.TotalDiscountValue],Sales.SaleAmount AS [Sales.SaleAmount],
Sales.RoundoffSaleAmount AS [Sales.RoundoffSaleAmount],
ProductStock.Id AS [ProductStock.Id],ProductStock.AccountId AS [ProductStock.AccountId],
ProductStock.InstanceId AS [ProductStock.InstanceId],ProductStock.ProductId AS [ProductStock.ProductId],ProductStock.VendorId AS [ProductStock.VendorId],
ProductStock.BatchNo AS [ProductStock.BatchNo],ProductStock.ExpireDate AS [ProductStock.ExpireDate],ProductStock.VAT AS [ProductStock.VAT],
ProductStock.TaxType AS [ProductStock.TaxType],ProductStock.SellingPrice AS [ProductStock.SellingPrice],ProductStock.MRP AS [ProductStock.MRP],
ProductStock.PurchaseBarcode AS [ProductStock.PurchaseBarcode],ProductStock.BarcodeProfileId AS [ProductStock.BarcodeProfileId],ProductStock.Stock AS [ProductStock.Stock],ProductStock.PackageSize AS [ProductStock.PackageSize],
ProductStock.PackagePurchasePrice AS [ProductStock.PackagePurchasePrice],ProductStock.PurchasePrice AS [ProductStock.PurchasePrice],
ProductStock.OfflineStatus AS [ProductStock.OfflineStatus],ProductStock.CreatedAt AS [ProductStock.CreatedAt],ProductStock.UpdatedAt AS [ProductStock.UpdatedAt],
ProductStock.CreatedBy AS [ProductStock.CreatedBy],ProductStock.UpdatedBy AS [ProductStock.UpdatedBy],ProductStock.CST AS [ProductStock.CST],
ProductStock.IsMovingStock AS [ProductStock.IsMovingStock],ProductStock.ReOrderQty AS [ProductStock.ReOrderQty],
ProductStock.Status AS [ProductStock.Status],ProductStock.IsMovingStockExpire AS [ProductStock.IsMovingStockExpire],
ProductStock.NewOpenedStock AS [ProductStock.NewOpenedStock],ProductStock.NewStockInvoiceNo AS [ProductStock.NewStockInvoiceNo],
ProductStock.NewStockQty AS [ProductStock.NewStockQty],ProductStock.StockImport AS [ProductStock.StockImport],ProductStock.ImportQty AS [ProductStock.ImportQty],
ProductStock.Eancode AS [ProductStock.Eancode],ProductStock.HsnCode AS [ProductStock.HsnCode],
CASE WHEN SalesItem.Cgst IS NULL THEN SalesItem.Igst END AS [ProductStock.Igst],
CASE WHEN SalesItem.Igst IS NULL THEN ISNULL(SalesItem.Cgst, ProductStock.Cgst) ELSE SalesItem.Cgst END AS [ProductStock.Cgst],
CASE WHEN SalesItem.Igst IS NULL THEN ISNULL(SalesItem.Sgst, ProductStock.Sgst) ELSE SalesItem.Sgst END AS [ProductStock.Sgst],
ISNULL(SalesItem.GstTotal ,ProductStock.GstTotal) AS [ProductStock.GstTotal],
ProductStock.ImportDate AS [ProductStock.ImportDate],Product.Id AS [Product.Id],
Product.AccountId AS [Product.AccountId],Product.InstanceId AS [Product.InstanceId],Product.Code AS [Product.Code],  /*Replace( Replace(replace(replace(Product.Name,'{','(') ,'}',')'),'@',' '),'*',' ') AS [Product.Name],*/
Product.Name AS [Product.Name],
Product.Manufacturer AS [Product.Manufacturer],Product.KindName AS [Product.KindName],Product.StrengthName AS [Product.StrengthName],Product.Type AS [Product.Type],
Product.Schedule AS [Product.Schedule],Product.Category AS [Product.Category],Product.GenericName AS [Product.GenericName],Product.CommodityCode AS [Product.CommodityCode],
Product.Packing AS [Product.Packing],Product.OfflineStatus AS [Product.OfflineStatus],Product.CreatedAt AS [Product.CreatedAt],Product.UpdatedAt AS [Product.UpdatedAt],
Product.CreatedBy AS [Product.CreatedBy],Product.UpdatedBy AS [Product.UpdatedBy],Product.PackageSize AS [Product.PackageSize],Product.VAT AS [Product.VAT],
Product.Price AS [Product.Price],Product.Status AS [Product.Status],Product.RackNo AS [Product.RackNo],Product.ProductMasterID AS [Product.ProductMasterID],
Product.ProductOrgID AS [Product.ProductOrgID],Product.ReOrderLevel AS [Product.ReOrderLevel],Product.ReOrderQty AS [Product.ReOrderQty],
Product.Discount AS [Product.Discount],Product.Eancode AS [Product.Eancode],Product.IsHidden AS [Product.IsHidden],Product.HsnCode AS [Product.HsnCode],
Product.Igst AS [Product.Igst],Product.Cgst AS [Product.Cgst],Product.Sgst AS [Product.Sgst],Product.GstTotal AS [Product.GstTotal],
Instance.Id AS [Instance.Id],Instance.AccountId AS [Instance.AccountId],Instance.ExternalId AS [Instance.ExternalId],
Instance.Code AS [Instance.Code],Instance.Name AS [Instance.Name],Instance.RegistrationDate AS [Instance.RegistrationDate],
Instance.Phone AS [Instance.Phone],Instance.Mobile AS [Instance.Mobile],Instance.DrugLicenseNo AS [Instance.DrugLicenseNo],
Instance.TinNo AS [Instance.TinNo],Instance.ContactName AS [Instance.ContactName],Instance.ContactMobile AS [Instance.ContactMobile],
Instance.ContactEmail AS [Instance.ContactEmail],Instance.SecondaryName AS [Instance.SecondaryName],Instance.SecondaryMobile AS [Instance.SecondaryMobile],
Instance.SecondaryEmail AS [Instance.SecondaryEmail],Instance.ContactDesignation AS [Instance.ContactDesignation],Instance.BDOId AS [Instance.BDOId],
Instance.BDODesignation AS [Instance.BDODesignation],Instance.BDOName AS [Instance.BDOName],Instance.RMName AS [Instance.RMName],
Instance.Address AS [Instance.Address],Instance.Area AS [Instance.Area],Instance.City AS [Instance.City],Instance.State AS [Instance.State],
Instance.Pincode AS [Instance.Pincode],Instance.Landmark AS [Instance.Landmark],Instance.Status AS [Instance.Status],
Instance.DrugLicenseExpDate AS [Instance.DrugLicenseExpDate],Instance.BuildingName AS [Instance.BuildingName],
Instance.StreetName AS [Instance.StreetName],Instance.Country AS [Instance.Country],
Instance.Lat AS [Instance.Lat],Instance.Lon AS [Instance.Lon],Instance.isDoorDelivery AS [Instance.isDoorDelivery],
Instance.isVerified AS [Instance.isVerified],Instance.isUnionTerritory AS [Instance.isUnionTerritory],Instance.isLicensed AS [Instance.isLicensed],
Instance.OfflineStatus AS [Instance.OfflineStatus],Instance.IsOfflineInstalled AS [Instance.IsOfflineInstalled],Instance.LastLoggedOn AS [Instance.LastLoggedOn],Instance.CreatedAt AS [Instance.CreatedAt],
Instance.UpdatedAt AS [Instance.UpdatedAt],Instance.CreatedBy AS [Instance.CreatedBy],Instance.UpdatedBy AS [Instance.UpdatedBy],Instance.Gstselect AS [Instance.Gstselect],
Instance.DistanceCover AS [Instance.DistanceCover],Instance.isHeadOffice AS [Instance.isHeadOffice],Instance.OfflineVersionNo AS [Instance.OfflineVersionNo],
Instance.GsTinNo AS [Instance.GsTinNo],HQueUser.Name AS [HQueUser.Name],ProductInstance.RackNo AS [ProductInstance.RackNo],ProductInstance.BoxNo AS [ProductInstance.BoxNo],
ISNULL(Patient.Gstin,'') [Patient.Gstin],
ISNULL(Patient.DrugLicenseNo,'') [Patient.DrugLicenseNo],
ISNULL(Patient.Pan,'') [Patient.Pan],
ISNULL(Patient.ShortName,'') [Patient.ShortName],
ISNULL(Patient.City,'') [Patient.City],
ISNULL(Patient.EmpID,'') [Patient.EmpID]
FROM SalesItem INNER JOIN Sales ON Sales.Id = SalesItem.SalesId 
INNER JOIN (select * from ProductStock where  InstanceId = @InstanceId)ProductStock  ON ProductStock.Id = SalesItem.ProductStockId 
INNER JOIN(Select * from Product Where Accountid =@Accountid) Product ON Product.Id = ProductStock.ProductId 
INNER JOIN Instance ON Instance.Id = Sales.InstanceId INNER JOIN HQueUser ON HQueUser.Id = Sales.CreatedBy 
LEFT JOIN (SELECT Productid,InstanceId, MAx(isnull(RackNo,'')) [RackNo] , max (isnull(Boxno,'') ) [BoxNo] from ProductInstance Where InstanceId = @InstanceId Group by Productid,InstanceId ) ProductInstance ON ProductInstance.ProductId = Product.Id And ProductInstance.InstanceId = ProductStock.InstanceId
LEFT JOIN Patient ON Patient.id=Sales.PatientId
 WHERE Sales.Id  =  @SaleId ORDER BY SalesItem.SalesItemSno asc 
END