/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 20/07/2017	Poongodi		Created
** 26/12/2017	nandhini		altered
*******************************************************************************/ 
-- exec usp_GetStockTransferList_Rpt 'c6b7fc38-8e3a-48af-b39d-06267b4785b4','c503ca6e-f418-4807-a847-b6886378cf0b','05-Jul-2017','05-Jul-2017' 
create PROCEDURE [dbo].[Usp_transfer_rpt](@InstanceId VARCHAR(36), 
                                          @AccountId  VARCHAR(36), 
                                          @FromDate  DATE, 
                                          @ToDate    DATE) 
AS 
  BEGIN 
      SELECT Isnull(prefix, '') + Isnull( transferno, '') [TransferNo], 
             transferdate [TransferDate], 
             Fi.Area                                      [TranferFrom], 
             TI.Area                                      [TransferTo],
             St.transferby [TransferBy], 
              isnull(sti.gstvalue ,0) [GSTValue], 
            isnull( sti.vatvalue ,0) [VatValue],  
             isnull(sti.transfervalue  ,0) [TransferAmount],
			 case isnull(st.TransferStatus,0)  when 3 then 'Accepted' when 2 then 'Rejected' else 'Pending' end [TransferStatus]
      FROM   stocktransfer st 
             INNER JOIN (SELECT * 
                         FROM   instance 
                         WHERE   accountid = @AccountId) FI 
                     ON FI.id = St.frominstanceid 
             LEFT JOIN (SELECT * 
                        FROM   instance 
                        WHERE    accountid = @AccountId) TI 
                    ON TI.id = St.toinstanceid 
             Left JOIN (SELECT transferid, 
                                Sum(item.itemval) 
                                [TransferValue], 
                                Sum(item.itemval - ( item.itemval * 100 / ( 
                                                     100 + Isnull(gsttotal, 0) ) 
                                                   )) 
                                [GstValue], 
                                Sum(item.itemval - ( item.itemval * 100 / ( 
                                                     100 + Isnull(vat, 0) ) )) 
                                                      [VatValue] 
                         FROM   stocktransferitem 
                                CROSS apply (SELECT ( quantity * Isnull( 
                                                      purchaseprice, 
                                                                 0) ) 
                                                    [ItemVal]) 
                                            Item 
                         WHERE  accountid = @AccountId 
                                AND (instanceid = @InstanceId 
								or ToInstanceId =@InstanceId)
                         GROUP  BY transferid) STI 
                     ON sti.transferid = ST.id 
      WHERE  ST.accountid = @accountId 
             AND ( ST.frominstanceid = @InstanceId 
                    OR ST.toinstanceid = @InstanceId ) 
             AND cast(transferdate as date) BETWEEN @FromDate AND @ToDate 
  END 