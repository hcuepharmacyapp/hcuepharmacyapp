/*
 *******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************    

** 13/09/2017   Lawrence	All branch condition Added 
*/
CREATE PROCEDURE [dbo].[usp_BillCancelList](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime,@CancelNo varchar(50),
 @Name varchar(100),@Mobile varchar(15),@ProdId varchar(36),@InvoiceNo varchar(50),@Batch varchar(100))
 AS
 BEGIN
 SET NOCOUNT ON  
  
  select @CancelNo  = isnull(@CancelNo ,''),@Name  = isnull(@Name ,''),@Mobile  = isnull(@Mobile ,''),@ProdId  = isnull(@ProdId ,''),
  @InvoiceNo  = isnull(@InvoiceNo ,''),@StartDate = ISNULL(@StartDate,''),@EndDate = ISNULL(@EndDate,''), @Batch = ISNULL(@Batch,'')
  
select 
sr.Id,
sr.ReturnDate,
ltrim(isnull(sr.Prefix,'')+ sr.ReturnNo) AS ReturnNo,
cast(sr.CreatedAt as datetime) AS CreatedAt,
CONVERT(VARCHAR(10),sr.CreatedAt, 103) + ' ' + LTRIM(RIGHT(CONVERT(CHAR(20),cast(sr.CreatedAt as datetime), 22), 11)) AS CancelDtTime,
sa.InvoiceDate,
isnull(sa.Prefix,'')+isnull(sa.InvoiceSeries,'')+' '+sa.InvoiceNo AS InvoiceNo,
isnull(sa.Name,'') AS CustomerName,
isnull(sa.Mobile,'') AS Mobile,
isnull(p.Name,'') AS ProductName,
isnull(ps.BatchNo,'') AS BatchNo,
ps.ExpireDate,
isnull(sri.Quantity,0) AS Quantity, 
isnull(hu.Name,'') AS Name, 
ISNULL(sri.MrpSellingPrice,0) AS MrpSellingPrice,
ISNULL(sri.TotalAmount,0) AS TotalAmount,
Inst.Name AS InstanceName
from SalesReturn sr (nolock)
join SalesReturnItem sri(nolock) on sr.Id=sri.SalesReturnId
join Sales SA(nolock) on sa.Id=sr.SalesId
join ProductStock ps(nolock) on ps.Id=sri.ProductStockId
join Product P(nolock) on p.Id=ps.ProductId
join HQueUser hu(nolock) on hu.Id=sr.CreatedBy
join Instance Inst(nolock) on Inst.Id = ISNULL(@InstanceId, sr.InstanceId)
where sr.AccountId=@AccountId
and sr.InstanceId=isnull(@InstanceId,sr.InstanceId)
--and sr.CreatedAt between @StartDate and @EndDate
and ((((cast(sr.CreatedAt as date)) >= cast( @StartDate as date)) or @StartDate='') and (((cast( sr.CreatedAt as date)) <=cast( @EndDate as date)) or @EndDate=''))
and sr.CancelType = 1
and (sr.ReturnNo=@CancelNo or @CancelNo='')
and (sa.Name=@Name or @Name = '')
and (sa.Mobile=@Mobile or @Mobile = '')
and (p.Id=@ProdId or @ProdId = '')
and (sa.InvoiceNo=@InvoiceNo or @InvoiceNo = '')
and (ps.BatchNo=@Batch or @Batch = '')
order by sr.CreatedAt desc

END