 
/*                               
******************************************************************************                            
** File: [usp_GetInventoryReOrderInfo]   
** Name: [usp_GetInventoryReOrderInfo]                               
** Description: To Get Sale details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
  ** 15/03/2017 Poongodi			Rack Number taken from Product Instance  
  ** 19/05/2017 Settu				BoxNo included
  ** 02/06/2017 Poongodi			ProductInstance Join changed
  ** 28/08/2017 Lawrence			product name search with exact match
*******************************************************************************/ 
CREATE PROC [dbo].[usp_GetInventoryReOrderInfo]( @InstanceId VARCHAR(36), 
@AccountId                                                   VARCHAR(36), 
@NameType                                                    INT, 
@Name                                                        VARCHAR(50), 
@PageNo                                                      INT=0, 
@PageSize                                                    INT=10) 
AS 
  BEGIN 
    SET nocount ON 

	  Declare @PName varchar(250),
		  @GenericName varchar(250)

			if (@NameType =1)
			BEGIN
				SET @PName = case when @Name = '' then NULL ELSE @Name END
				SET @GenericName = ''
			END
			else
			BEGIN
				SET @GenericName = CASE WHEN @Name IS NULL THEN '' ELSE @Name END
				SET @PName = NULL
			END


    SELECT          i.instanceid [InstanceId], 
                    i.accountid, 
                    ps.productid [ProductId], 
                    p.id          AS [Product.Id], 
                    --Sum(ps.stock) [Stock] , 
                    --ps.expiredate [ExpireDate], 
                    p.NAME [ProductName], 
                    p.genericname  [GenericName], 
                    p.code [ProductCode], 
                    p.manufacturer [Manufacturer], 
                    Max(ip.rackno) [RackNo], 
					Max(ip.boxNo) [BoxNo], 
                    p.status [Status], 
                    MAX(v.NAME) [VendorName], 
                    --v.id   VendorId, 
                    ip.reorderqty [ReOrder], 
                    i.id            AS [ReorderId], 
                    i.status           [ReOrderStatus] , 
                    Count(1) OVER()    [TotalRecordCount] 
    FROM 
	( 
        SELECT * FROM ProductStock PST WITH(nolock) 
        WHERE  pst.accountid=@AccountId AND pst.instanceid=@InstanceId
		AND pst.Stock != 0 AND (pst.Status is null or pst.Status = 1)
	) PS 
    INNER JOIN      product P WITH(nolock) 
    ON ps.accountid=@AccountId 
    AND ps.instanceid=@InstanceId 
	AND  p.id = ps.productid 
	Left JOIN ProductInstance IP WITH(nolock) on 
	IP.accountid=@AccountId  
	AND IP.instanceid=@InstanceId 
	AND IP.ProductId = P.id
    LEFT OUTER JOIN vendor V WITH(nolock) 
    ON              v.id = ps.vendorid 
    LEFT OUTER JOIN inventoryreorder I WITH(nolock) 
    ON i.instanceid =@InstanceId AND i.productid =p.id 
	 AND             i.reorder > 0 
    WHERE (P.Name =  CASE WHEN @PName IS NULL THEN p.Name ELSE @PName END
	OR P.Name LIKE CASE WHEN @PName IS NOT NULL AND LEN(@PName)=1 THEN @PName +'%' ELSE @PName END)
	AND      Isnull(p.genericname,'') LIKE @GenericName + '%'
	AND             i.reorder > 0  
    GROUP BY        i.instanceid, 
                    i.accountid, 
                    ps.productid, 
                    p.id , 
                    --ps.expiredate, 
                    p.NAME , 
                    p.genericname , 
                    p.code , 
                    p.manufacturer, 
                    p.status, 
                    --v.NAME , 
                    --v.id , 
                    ip.reorderqty, 
                    i.id , 
                    i.status 
    ORDER BY        p.NAME ASC offset @PageNo rows 
    FETCH next @PageSize rows only 
  END
 