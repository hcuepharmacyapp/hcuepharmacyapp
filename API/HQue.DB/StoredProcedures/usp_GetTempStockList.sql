-- [usp_GetTempStockList] '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','','',''
-- [usp_GetTempStockList] '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','4E4E702B-A369-4606-9A96-6FB6B867E84B','',''
-- usp_GetTempStockList '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','c94169f2-397b-44a8-ba67-d0b7d3d81b6d','01-Sep-2016','21-Sep-2016'
CREATE PROCEDURE [dbo].[usp_GetTempStockList](@InstanceId varchar(36),@AccountId varchar(36),@ProductId varchar(36),@StartDate datetime,@EndDate datetime)
AS
 BEGIN
   SET NOCOUNT ON
	--declare @condition nvarchar(max)	
	--Declare @qry nvarchar(max)
	--SET @condition=''

	--if(@ProductId is not null AND @ProductId!='')
	--begin
	--	set @condition=@condition+' and p.id= '+''''+@ProductId +''''
	--end

	--if(@StartDate is not null AND @StartDate!='')
	--begin
	--	set @condition=@condition+' AND CAST(T.CreatedAt AS date) BETWEEN '+''''+CAST(@StartDate AS varchar(11))+'''' +' AND '+''''+CAST(@EndDate AS varchar(11))+''''
	--end
	
	--SET @qry='Select p.Name,ps.BatchNo,ISNULL(ps.VAT,0) VAT,ps.ExpireDate,T.PackageSize,Case When T.isActive=0 Then vp.PackageQty Else T.PackageQty End As [PackageQty],T.PackagePurchasePrice,T.PackageSellingPrice,
	--Case When T.isActive=0 Then ''Converted'' Else ''Pending'' End As [Status],ISNULL(T.TaxRefNo,0) as TaxRefNo,ISNULL(ps.GstTotal,0) as GstTotal from TempVendorPurchaseItem T Inner Join ProductStock ps ON T.ProductStockId=ps.id
 --Inner Join Product p ON p.id=ps.ProductId
 --left join vendorpurchaseItem vp On vp.fromtempId=T.id
 --Where T.InstanceId=''' +@InstanceId +''''+' AND T.AccountId  ='''+@AccountId+''''+ @condition+' ORDER BY T.CreatedAt asc'
	
	----print @qry
	--EXEC sp_executesql @qry
		
		
if(@ProductId=null OR @ProductId='')
select  @ProductId = isnull(@ProductId,'')

Select p.Name,ps.BatchNo,ISNULL(ps.VAT,0) VAT,ps.ExpireDate,T.PackageSize,Case When T.isActive=0 Then vp.PackageQty Else T.PackageQty End As
[PackageQty],T.PackagePurchasePrice,T.PackageSellingPrice,
Case  When T.isActive=0 Then 'Converted' Else 'Pending' End As [Status],ISNULL(T.TaxRefNo,0) as TaxRefNo,ISNULL(ps.GstTotal,0) as GstTotal 
from TempVendorPurchaseItem  (nolock) T 
Inner Join ProductStock (nolock) ps ON T.ProductStockId=ps.id
Inner Join Product (nolock) p ON p.id=ps.ProductId
left join vendorpurchaseItem (nolock) vp On vp.fromtempId=T.id
Where T.InstanceId=@InstanceId  AND T.AccountId  =@AccountId AND CAST(T.CreatedAt AS date) BETWEEN CAST(@StartDate AS varchar(11)) AND CAST(@EndDate AS varchar(11))
AND isnull(p.id ,'') like isnull(@ProductId,'') +'%'
  
 END