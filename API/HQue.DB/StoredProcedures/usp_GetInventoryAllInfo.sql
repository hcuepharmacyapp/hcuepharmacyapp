 /*                               
******************************************************************************                            
** File: [usp_GetInventoryAllInfo]   
** Name: [usp_GetInventoryAllInfo]                               
** Description: To Get All Product details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 15/03/2017 Poongodi			Rack Number taken from Product Instance 
** 17/04/2017 Poongodi			Inner Query Removed
** 26/04/2017 Poongodi			Active Status Removed 
** 19/05/2017 Settu				BoxNo included
** 02/06/2017 Poongodi			ProductInstance Join changed
** 13/07/2017 Poongodi			SP Optimized
** 20/07/2017 Poongodi			InstanceId removed and isnulladded
** 28/08/2017 Lawrence			product name search with exact match
*******************************************************************************/ 
 CREATE PROC [dbo].[usp_GetInventoryAllInfo]( @InstanceId VARCHAR(36), 
												@AccountId VARCHAR(36), 
												@NameType  INT, 
												@Name      VARCHAR(50) , 
												@PageNo    INT=0, 
												@PageSize  INT=10) 
AS 
  BEGIN 
    SET nocount ON 
	 
	  Declare @PName varchar(250),
			  @GenericName varchar(250)

		  	if (@NameType =1)
			BEGIN
				SET @PName = case when @Name = '' then NULL ELSE @Name END
				SET @GenericName = ''
			END
			else
			BEGIN
				SET @GenericName = CASE WHEN @Name IS NULL THEN '' ELSE @Name END
				SET @PName = NULL
			END
			 
    SELECT          ps.productid [ProductId], 
                    --ps.stock [Stock], 
                    --ps.expiredate [ExpireDate], 
                    --ps.batchno [Barchno], 
                    p.NAME [ProductName], 
                    p.genericname [GenericName] , 
                    p.code [ProductCode], 
                    p.manufacturer [Manufacturer], 
					iP.ReOrderQty [ReOrder],
                   Isnull( IP.rackno ,'') [RackNo],
					Isnull(IP.boxNo ,'') [BoxNo],  
                    p.status [Status], 
                    MAX(v.NAME)          [VendorName], 
                    Count(1) OVER() [TotalRecordCount] 
	FROM            
	(
		SELECT * FROM productstock WITH(nolock) WHERE accountid=@AccountId AND instanceid=@InstanceId 
		AND (ISNULL(Status,1)=1 OR ProductId IN(SELECT ProductId FROM ProductStock 
		WHERE accountid=@AccountId AND instanceid=@InstanceId AND Stock != 0 AND ISNULL(Status,1)!=1))
	) PS 
    INNER JOIN      (SELECT * FROM product WITH(nolock) WHERE accountid=@AccountId   ) P  
    ON              p.id = ps.productid 
	Left JOIN		(SELECT * FROM ProductInstance WITH(nolock) WHERE accountid=@AccountId AND instanceid=@InstanceId ) IP on IP.ProductId = P.id
    LEFT OUTER JOIN (SELECT * FROM vendor WITH(nolock) WHERE accountid=@AccountId AND instanceid=@InstanceId) V
    ON              v.id = ps.vendorid 
  
    where (P.Name =  CASE WHEN @PName IS NULL THEN p.Name ELSE @PName END
	OR P.Name LIKE CASE WHEN @PName IS NOT NULL AND LEN(@PName)=1 THEN @PName +'%' ELSE @PName END)
	AND  Isnull(p.genericname,'') LIKE @GenericName + '%'      
	GROUP BY ps.productid,p.NAME,p.genericname,p.code,p.manufacturer,iP.ReOrderQty,
	Isnull( IP.rackno ,''),Isnull(IP.boxNo ,''),p.status
    ORDER BY        p.NAME ASC offset @PageNo rows 
    FETCH next @PageSize rows only 
  END
 