﻿ -- Exec usp_GetDashboardDailyPlusPharmacyCardDetailsList '852eaefa-f670-4a6a-a0e7-db3fa252d576'
 ---- Admin Email Id added by Gavaskar 05-10-2017 
 CREATE PROCEDURE [dbo].[usp_GetDashboardDailyPlusPharmacyCardDetailsList] (@AccountId varchar(36),@EmailId varchar(200))
 AS
 BEGIN
   SET NOCOUNT ON
  
        SELECT isnull(Branch,'') as Branch,Convert(decimal(18,2),SUM(TOTALSALESCOUNT),0) AS TOTALSALESCOUNT,isnull(PharmacyName,'') as PharmacyName,isnull(OwnerName,'') as OwnerName,isnull(CITY,'') as CITY,isnull(AREA,'') as AREA,SUM(CASH - ReturnedAmount)TotalCash,SUM(CARD)TotalCard,SUM(CREDIT)TotalCredit FROM (
        select Branch,count(totalsales) TOTALSALESCOUNT,PHARMACYNAME,OWNERNAME,CITY,AREA,ROUND(Convert(decimal(18,2),sum(Newtotalmrp1)),0) Cash,ROUND(Convert(decimal(18,2),sum(Newtotalmrp2)),0) Card ,ROUND(Convert(decimal(18,2),sum(Newtotalmrp3)),0) Credit
        ,'0.00' as ReturnedAmount from (select *, case when diff>=0.50 then Roundedvalue+1 else roundedvalue end Newtotalmrp,case when diff1>=0.50 then Roundedvalue1+1 else roundedvalue1 end Newtotalmrp1,
        case when diff2>=0.50 then Roundedvalue2+1 else roundedvalue2 end Newtotalmrp2,case when diff3>=0.50 then Roundedvalue3+1 else roundedvalue3 end Newtotalmrp3   from 
        (select *,((totalMrp-TotalDiscount)-roundedvalue) Diff,((TotalCash-CashTotalDiscount)-roundedvalue1) Diff1,((TotalCard-CardTotalDiscount)-roundedvalue2) Diff2,
        ((TotalCredit-CreditTotalDiscount)-roundedvalue3) Diff3 from 
        (select *,floor(TotalMrp-TotalDiscount) Roundedvalue,floor(TotalCash-CashTotalDiscount) Roundedvalue1,floor(TotalCard-CardTotalDiscount) Roundedvalue2,
        floor(TotalCredit-CreditTotalDiscount) Roundedvalue3 from 
        (select Instance.Name AS Branch,sales.invoiceno,count(distinct sales.id) AS TotalSales,Account.Name As PharmacyName,HQueUser.Name as OwnerName,Instance.City AS City,Instance.Area AS Area,Convert(decimal(18,2),sum((ISNULL(vpi.PurchasePrice,ProductStock.PurchasePrice) * salesitem.Quantity))) AS TotalPurchase,
        Convert(decimal(18,2),sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END)  * 
        salesitem.Quantity -((CASE WHEN salesitem.SellingPrice > 0 then
        salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount,0) / 100) ))  AS TotalMRP
        ,CONVERT(decimal(18,2),sum(SalesItem.Quantity * (CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(SalesItem.Discount,0) / 100)) AS TotalDiscount
        ,CONVERT(decimal(18,2),sum(case when sales.paymenttype='cash' then (SalesItem.Quantity * (CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(SalesItem.Discount,0) / 100) else 0 end)) AS CashTotalDiscount
        ,CONVERT(decimal(18,2),sum(case when sales.paymenttype='card' then (SalesItem.Quantity * (CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(SalesItem.Discount,0) / 100) else 0 end)) AS CardTotalDiscount
        ,CONVERT(decimal(18,2),sum(case when sales.paymenttype='credit' then (SalesItem.Quantity * (CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(SalesItem.Discount,0) / 100) else 0 end))AS CreditTotalDiscount
        ,Convert(decimal(18,2),sum(case when sales.paymenttype='cash' then (CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END)  * salesitem.Quantity - 
        ((CASE WHEN salesitem.SellingPrice > 0 then
        salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount,0) / 100) else 0 end)) as TotalCash
        ,Convert(decimal(18,2),sum(case when sales.paymenttype='card' then (CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END)  * salesitem.Quantity - 
        ((CASE WHEN salesitem.SellingPrice > 0 then
        salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount,0) / 100) else 0 end)) as TotalCard,
        Convert(decimal(18,2),sum(case when sales.paymenttype='credit' then (CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END)  * salesitem.Quantity - 
        ((CASE WHEN salesitem.SellingPrice > 0 then
        salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount,0) / 100) else 0 end)) as TotalCredit
        from Account
        INNER JOIN HQueUser on HQueUser.Accountid=Account.id
        INNER JOIN Instance on Instance.AccountId = HQueUser.AccountId AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''
        INNER JOIN sales on sales.AccountId = HQueUser.AccountId AND SALES.InstanceId=INSTANCE.ID 
        INNER join salesitem on sales.id = salesitem.salesid 
        INNER join productstock on salesitem.productstockid = productstock.id AND productstock.InstanceId=INSTANCE.ID
        LEFT join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem  group by productstockId) vpi on productstock.id=vpi.productstockid 
        WHERE  HQueUser.AccountId = @AccountId and sales.Cancelstatus is NULL  AND HQueUser.UserId=@EmailId AND 
        Convert(date,sales.invoicedate) = dateadd(day,datediff(day,1,GETDATE()),0) group by sales.invoiceno,Instance.Id,Instance.Name,Account.Name,HQueUser.Name,Instance.City,Instance.Area) a) a) a) a 
        group by Branch,PharmacyName,OwnerName,City,Area
        UNION

		select Branch,'0' As TOTALSALESCOUNT,PharmacyName,OwnerName,City,Area,'0.00' As Cash,'0.00' as [Card],'0.00' as credit,SUM(ReturnedAmount) as ReturnedAmount from (
        select Instance.Name AS Branch,'0' AS TOTALSALESCOUNT,Account.Name As PharmacyName,HQueUser.Name as OwnerName,Instance.City AS City,Instance.Area AS Area,'0.00' As Cash,'0.00' as [Card],'0.00' as credit,
        round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
        else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * isnull(s.Discount,0))/100)*sri.Quantity else 
        (sri.MrpSellingPrice-(sri.MrpSellingPrice * isnull(s.Discount,0))/100)*sri.Quantity end end) -( sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * isnull(sri.Discount,0))/100)*sri.Quantity else
 
        ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end)))),0) as ReturnedAmount
        from Account
        INNER JOIN HQueUser on HQueUser.Accountid=Account.id
        INNER JOIN Instance on Instance.AccountId = HQueUser.AccountId AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''
        INNER JOIN SalesReturn as sr on sr.AccountId = HQueUser.AccountId AND sr.InstanceId=INSTANCE.ID 
        left JOIN Sales as s ON s.Id = sr.SalesId
        inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
        inner join ProductStock as ps on ps.id = sri.ProductStockId
        inner join Product as p on p.Id = ps.ProductId
        WHERE  sr.AccountId =@AccountId AND HQueUser.UserId=@EmailId
        AND Convert(date,sr.ReturnDate) =dateadd(day,datediff(day,1,GETDATE()),0) and s.Cancelstatus is NULL
        group by Instance.Name,Account.Name,HQueUser.Name,Instance.City,Instance.Area,sr.ReturnNo ) as b  group by Branch,PharmacyName,OwnerName,City,Area

        UNION
        select Instance.Name AS Branch,'0' As TOTALSALESCOUNT,Account.Name As PharmacyName,HQueUser.Name as OwnerName,Instance.City AS City,Instance.Area AS Area,'0.00' As Cash,'0.00' as [Card],'0.00' as credit,'0.00' as ReturnedAmount
        from Account
        INNER JOIN HQueUser on HQueUser.Accountid=Account.id
        INNER JOIN Instance on Instance.AccountId = HQueUser.AccountId AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''
        WHERE Account.id =@AccountId AND HQueUser.UserId=@EmailId and Instance.Id not in('8e18d49d-3a9e-4d0f-9bef-cba296ed4191','6ac9369b-467f-45df-af8b-5909b5314bc6')
 group by Instance.Name,Account.Name,HQueUser.Name,Instance.City,Instance.Area) AS CARDDETAILS GROUP BY BRANCH,PHARMACYNAME,OWNERNAME,CITY,AREA
	ORDER BY SUM(CASH - ReturnedAmount) DESC,SUM(CARD) DESC,SUM(CREDIT) DESC,BRANCH ASC

 END
  



