﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author			Description                              
*******************************************************************************        
**07/02/18     Settu P			SP Created  
*******************************************************************************/
CREATE PROC [dbo].[usp_Get_GstNullProducts] (@AccountId CHAR(36), @InstanceId CHAR(36))
AS
BEGIN
	SET NOCOUNT ON

	SELECT COUNT(DISTINCT p.Id) PrdCount FROM 
	(
		SELECT Id,AccountId,GstTotal FROM Product WITH(NOLOCK) WHERE AccountId = @AccountId and  GstTotal IS NULL
	) p 
	LEFT JOIN 
	(
		SELECT Id,ProductId FROM ProductStock WITH(NOLOCK) WHERE AccountId = @AccountId AND GstTotal IS NOT NULL
	) ps ON p.Id = ps.ProductId 
	WHERE p.AccountId = @AccountId and p.GstTotal IS NULL AND ps.Id IS NULL

	SET NOCOUNT OFF
END