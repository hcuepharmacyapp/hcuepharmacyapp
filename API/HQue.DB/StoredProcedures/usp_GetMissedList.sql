/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 22/09/2017  Poongodi		Created
*******************************************************************************/ 
Create proc dbo.usp_GetMissedList(@Accountid Char(36),
@InstanceId char(36),
@Type varchar(10))
as
begin
if (@Type ='sales')
begin
Select    Id [Id], InvoiceNo from Sales (nolock) Where AccountId =@Accountid  and instanceid =@InstanceId
and Id not in (SElect salesid from SalesItem(nolock) Where AccountId =@Accountid  and instanceid =@InstanceId)
 
 end
 else if (@Type ='purchase')
begin
Select    Id [Id], GoodsRcvNo InvoiceNo from VendorPurchase (nolock) Where AccountId =@Accountid  and instanceid =@InstanceId
and Id not in (SElect VendorPurchaseId from VendorPurchaseItem(nolock) Where AccountId =@Accountid  and instanceid =@InstanceId)
 
 end
end 

 