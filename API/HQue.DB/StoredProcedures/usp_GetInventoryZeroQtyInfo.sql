 
/*                               
******************************************************************************                            
** File: [usp_GetInventoryZeroQtyInfo]   
** Name: [usp_GetInventoryZeroQtyInfo]                               
** Description: To Get Inventory Zero Qty
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 15/03/2017 Poongodi			Rack Number taken from Product Instance    
 ** 17/04/2017 Poongodi			Inner Query Removed
 ** 19/05/2017 Settu			BoxNo included
 ** 02/06/2017 Poongodi			ProductInstance Join changed
  ** 31/07/2017 Poongodi		Product Stock Inner qry removed validation added in Outer qry
 ** 28/08/2017 Lawrence		product name search with exact match
 ** 29/11/2017 Poongodi			Batchwise listing removed and Qty grouped by Product 
*******************************************************************************/ 

-- [usp_GetInventoryZeroQtyInfo]  '5395667b-aab9-40aa-873b-7e7dc135eeec','4edd19c2-3fe3-4ba2-8083-83a06ccd6c88',1,'', 0, 20 --TRYPSIN
 CREATE PROC [dbo].[usp_GetInventoryZeroQtyInfo]( @InstanceId VARCHAR(36), 
												 @AccountId  VARCHAR(36), 
												@NameType INT, 
												@Name     VARCHAR(1000), 
												@PageNo   INT=0, 
												@PageSize INT=10) 
AS 
  BEGIN 
    SET nocount ON 
		  Declare @PName varchar(250),
			  @GenericName varchar(250)

		  	if (@NameType =1)
			BEGIN
				SET @PName = case when @Name = '' then NULL ELSE @Name END
				SET @GenericName = ''
			END
			else
			BEGIN
				SET @GenericName = CASE WHEN @Name IS NULL THEN '' ELSE @Name END
				SET @PName = NULL
			END

    SELECT          ps.productid [ProductId], 
                    --ps.stock [Stock], 
                    --ps.expiredate 
					--NULL [ExpireDate], 
                    --ps.batchno, 
					--'' batchno, 
                    p.NAME [ProductName], 
                    p.genericname [GenericName], 
                    p.code [ProductCode], 
                    p.manufacturer [Manufacturer], 
                    ip.rackno [RackNo], 
					ip.boxNo [BoxNo],
                    p.status [Status], 
                    MAX(v.NAME)          [VendorName], 
                    Count(1) OVER() [TotalRecordCount] 
    FROM           
	(
		Select max(vendorid) vendorid  ,productid,instanceid,accountid, 0 [stock]  from productstock PS WITH(NOLOCK) where --stock = 0 and
		ps.AccountId = @AccountId AND ps.instanceid = @InstanceId 
		and (PS.Status is null or PS.Status = 1) group by accountid,instanceid,productid 
		having sum(stock) = 0
	) PS
    INNER JOIN      (Select * from product  WITH(nolock) where accountid=@AccountId ) P
    ON              p.id = ps.productid 
	AND             ps.accountid=@AccountId 
    AND             ps.instanceid=@InstanceId    
	Left JOIN (Select productid, instanceid,accountid, max(isnull(rackno,'')) [rackno],  max(isnull(boxNo,'')) [boxNo] from ProductInstance WITH(nolock) 
		 group by accountid,instanceid,productid) IP on IP.ProductId = P.id
	AND             IP.accountid=@AccountId
	AND             IP.instanceid=@InstanceId      
    LEFT OUTER JOIN (Select * from vendor WITH(nolock)  where accountid=@AccountId ) V
    ON              v.id = ps.vendorid 
    WHERE (P.Name =  CASE WHEN @PName IS NULL THEN p.Name ELSE @PName END
	OR P.Name LIKE CASE WHEN @PName IS NOT NULL AND LEN(@PName)=1 THEN @PName +'%' ELSE @PName END)
	AND      Isnull(p.genericname,'') LIKE @GenericName + '%' 
	GROUP BY ps.productid,p.NAME,p.genericname,p.code,p.manufacturer,ip.rackno,ip.boxNo,p.status
    ORDER BY        p.NAME ASC offset @PageNo rows 
    FETCH next @PageSize rows only 
  END

 