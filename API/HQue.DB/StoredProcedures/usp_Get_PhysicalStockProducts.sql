﻿CREATE PROCEDURE [dbo].[usp_Get_PhysicalStockProducts](@AccountId CHAR(36), @InstanceId CHAR(36))
AS
BEGIN
	DECLARE @Product TABLE(Productid CHAR(36), tsid INT IDENTITY(1,1))
	DECLARE @FromDate DATE, @PhyFromDate DATE
	DECLARE @ToDate DATE, @PhyToDate DATE
	SELECT @FromDate = ISNULL(@FromDate,GETDATE()-1), @ToDate = ISNULL(@ToDate,GETDATE()-1)
	SELECT @PhyFromDate = ISNULL(@PhyFromDate,GETDATE()-7), @PhyToDate = ISNULL(@PhyToDate,GETDATE())
	DECLARE @FetchRows INT = 5, @DefaultRows INT = 5, @InsertRowCount INT = 0
	
	INSERT INTO @Product(Productid)		
	SELECT PS.ProductId  
			FROM Sales S 
			INNER JOIN SalesItem SI ON S.Id = SI.SalesId
			INNER JOIN ProductStock PS ON PS.Id = SI.ProductStockId
			LEFT JOIN 
			(
				SELECT PS.ProductId,SUM(SRI.Quantity) AS ReturnQty,
				SUM(SRI.Quantity * ISNULL(SRI.MrpSellingPrice,PS.SellingPrice)) AS ReturnAmt FROM SalesReturn SR 
				INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
				INNER JOIN ProductStock PS ON PS.Id = SRI.ProductStockId
				WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceId
				AND CAST(SR.CreatedAt AS DATE) BETWEEN @FromDate AND @ToDate GROUP BY PS.ProductId
			) SR 
			ON SR.ProductId = PS.ProductId
			WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId
			AND CAST(S.CreatedAt AS DATE) BETWEEN @FromDate AND @ToDate AND S.Cancelstatus IS NULL 
			AND PS.ProductId NOT IN(SELECT PSH.ProductId FROM PhysicalStockHistory PSH WHERE PSH.PhysicalStock IS NOT NULL
			AND CAST(PSH.CreatedAt AS DATE) BETWEEN @PhyFromDate AND @PhyToDate)
			GROUP BY PS.ProductId HAVING SUM(SI.Quantity)-MAX(ISNULL(SR.ReturnQty,0)) > 0
			ORDER BY SUM(SI.Quantity)-MAX(ISNULL(SR.ReturnQty,0)) DESC
			OFFSET 0 ROWS FETCH NEXT @DefaultRows ROWS ONLY

	SELECT @InsertRowCount = @@ROWCOUNT   
	SET @FetchRows = @DefaultRows + (@FetchRows - @InsertRowCount)

	INSERT INTO @Product(Productid)					
	SELECT PS.ProductId   
			FROM Sales S 
			INNER JOIN SalesItem SI ON S.Id = SI.SalesId
			INNER JOIN ProductStock  PS ON PS.Id = SI.ProductStockId
			AND PS.ProductId NOT IN (SELECT Productid FROM @Product)
			LEFT JOIN 
			(
				SELECT PS.ProductId,SUM(SRI.Quantity) AS ReturnQty,
				SUM(SRI.Quantity * ISNULL(SRI.MrpSellingPrice,PS.SellingPrice)) AS ReturnAmt FROM SalesReturn SR 
				INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
				INNER JOIN ProductStock  PS ON PS.Id = SRI.ProductStockId
				AND PS.ProductId NOT IN (SELECT Productid FROM @Product)
				WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceId
				AND CAST(SR.CreatedAt AS DATE) BETWEEN @FromDate AND @ToDate GROUP BY PS.ProductId
			) SR 
			ON SR.ProductId = PS.ProductId
			CROSS APPLY(SELECT (SI.Quantity * ISNULL(SI.SellingPrice,PS.SellingPrice)) AS SALEAMOUNT1) AS T
			WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId
			AND CAST(S.CreatedAt AS DATE) BETWEEN @FromDate AND @ToDate AND S.Cancelstatus IS NULL 
			AND PS.ProductId NOT IN(SELECT PSH.ProductId FROM PhysicalStockHistory PSH WHERE PSH.PhysicalStock IS NOT NULL
			AND CAST(PSH.CreatedAt AS DATE) BETWEEN @PhyFromDate AND @PhyToDate)		
			GROUP BY PS.ProductId HAVING SUM(SALEAMOUNT1)-MAX(ISNULL(SR.ReturnAmt,0)) > 0
			ORDER BY SUM(SALEAMOUNT1)-MAX(ISNULL(SR.ReturnAmt,0)) DESC 
			OFFSET 0 ROWS FETCH NEXT @FetchRows ROWS ONLY

	SELECT @InsertRowCount = @@ROWCOUNT   
	SET @FetchRows = @DefaultRows + (@FetchRows - @InsertRowCount)
	
	INSERT INTO @Product(Productid)		
	SELECT PS.ProductId FROM VendorPurchase VP
			INNER JOIN VendorPurchaseItem VPI ON VP.Id = VPI.VendorPurchaseId
			INNER JOIN ProductStock  PS ON PS.Id = VPI.ProductStockId
			AND PS.ProductId NOT IN (SELECT Productid FROM @Product)
			LEFT JOIN
			(
				SELECT PS.ProductId,SUM(VRI.Quantity) AS ReturnQty,
				SUM(VRI.Quantity * ISNULL(VRI.ReturnPurchasePrice,PS.PurchasePrice)) AS ReturnAmt FROM VendorReturn VR 
				INNER JOIN VendorReturnItem VRI ON VR.Id = VRI.VendorReturnId
				INNER JOIN ProductStock  PS ON PS.Id = VRI.ProductStockId
				AND PS.ProductId NOT IN (SELECT Productid FROM @Product)
				WHERE VR.AccountId = @AccountId AND VR.InstanceId = @InstanceId
				AND CAST(VR.CreatedAt AS DATE) BETWEEN @FromDate AND @ToDate GROUP BY PS.ProductId
			) VR1
			ON VR1.ProductId = PS.ProductId
			WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId
			AND CAST(VP.CreatedAt AS DATE) BETWEEN @FromDate AND @ToDate AND VP.CancelStatus IS NULL
			AND PS.ProductId NOT IN(SELECT PSH.ProductId FROM PhysicalStockHistory PSH WHERE PSH.PhysicalStock IS NOT NULL
			AND CAST(PSH.CreatedAt AS DATE) BETWEEN @PhyFromDate AND @PhyToDate)
			GROUP BY PS.ProductId HAVING SUM(VPI.Quantity)-MAX(ISNULL(VR1.ReturnQty,0)) > 0
			ORDER BY SUM(VPI.Quantity)-MAX(ISNULL(VR1.ReturnQty,0)) DESC 
			OFFSET 0 ROWS FETCH NEXT @FetchRows ROWS ONLY 
	   
	SELECT @InsertRowCount = @@ROWCOUNT   
	SET @FetchRows = @DefaultRows + (@FetchRows - @InsertRowCount)
	
	INSERT INTO @Product(Productid)	
	SELECT PS.ProductId FROM ProductStock  PS
			WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId 
			AND PS.ProductId NOT IN (SELECT Productid FROM @Product)
			AND ISNULL(PS.Stock,0) > 0 
			AND PS.ProductId NOT IN(SELECT PSH.ProductId FROM PhysicalStockHistory PSH WHERE PSH.PhysicalStock IS NOT NULL
			AND CAST(PSH.CreatedAt AS DATE) BETWEEN @PhyFromDate AND @PhyToDate)
			GROUP BY PS.ProductId HAVING SUM(ISNULL(PS.Stock,0)) > 0 
			ORDER BY SUM(ISNULL(PS.Stock,0)) DESC 
			OFFSET 0 ROWS FETCH NEXT @FetchRows ROWS ONLY 
	   

		SELECT P.Id, P.Name, R1.RackNo, R1.BoxNo, SUM(PS.Stock) AS Stock FROM @Product R	
		INNER JOIN ProductStock PS ON PS.ProductId = R.ProductId
		INNER JOIN Product P ON PS.ProductId = P.Id
		LEFT JOIN 
		(
			SELECT PINS.ProductId, ISNULL(PINS.RackNo,'') AS RackNo, ISNULL(PINS.BoxNo,'') AS BoxNo FROM ProductInstance PINS 
			WHERE PINS.AccountId = @AccountId AND PINS.InstanceId = @InstanceId
			GROUP BY PINS.ProductId, ISNULL(PINS.RackNo,''), ISNULL(PINS.BoxNo,'')
		) R1 ON R1.ProductId = P.Id
		GROUP BY R.tsid, P.Id, P.Name, R1.RackNo, R1.BoxNo
		ORDER BY R.tsid
END