﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 14/12/2017  Gavaskar			Creted
*******************************************************************************/ 
-- usp_Get_KindOfProductMaster 'c503ca6e-f418-4807-a847-b6886378cf0b',
create procedure [dbo].[usp_Get_KindOfProductMaster](
@Accountid CHAR(36),
@Type  VARCHAR(36)
)
as
begin
	if @Type ='all'
	(SELECT id,AccountId,KindName,IsActive FROM KindProductMaster (nolock) where AccountId  =@Accountid) order by KindName asc
	else
	(SELECT id,AccountId,KindName,IsActive FROM KindProductMaster (nolock) where AccountId  =@Accountid and isnull(IsActive,0)=0) order by KindName asc
end
