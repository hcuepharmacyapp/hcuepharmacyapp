 
/*                               
******************************************************************************                            
** File: [usp_get_saleItem_Details]   
** Name: [usp_get_saleItem_Details]                               
** Description: To Get Sale details   based on SaleId 
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
**05/04/17     Poongodi R		Return item details 
**09/05/17     Poongodi R		Nolock added 
**18/05/17     Violet			Function removed and SaleId mapped Directly
**17/08/17     Settu			ProductStock.GstTotal NULL values handled
**20/09/17     Sarubala			sales item table condition added in join 
**05/11/17     nandhini			GstTotal value handled
** 17/05/2018 nandhini			Item order changed by SalesItemSno
*******************************************************************************/ 
Create PROC [dbo].[Usp_get_saleitem_details] (@AccountId     NVARCHAR(36), 
                                          @InstanceId    NVARCHAR(36), 
                                          @SaleId        NVARCHAR(max), 
                                          @SearchColName VARCHAR(50), 
                                          @SearchOption  VARCHAR(50), 
                                          @SearchValue   VARCHAR(50), 
                                          @fromDate      VARCHAR(15), 
                                          @Todate        VARCHAR(15)) 
AS 
  BEGIN 

/*  SalesItemType Definition:
1 - Sales Item
2 - Sales Item Audit
3 - Return Along with Sales
4 - Sales History Return */


     /* DECLARE @From_BillDt    DATE = NULL, 
              @To_BillDt      DATE = NULL, 
              @From_ExpiryDt  DATE = NULL, 
              @To_ExpiryDt    DATE = NULL, 
              @BtchNo         VARCHAR(50) = '', 
              @customerName   VARCHAR(200) = '', 
              @customerMobile VARCHAR(100) = '', 
              @doctorName     VARCHAR(100) = NULL, 
              @doctorMobile   VARCHAR(100) = NULL, 
              @product        VARCHAR(100) = '', 
              @billNo         VARCHAR(100) = '', 
              @from_Qty       NUMERIC(10, 2) =NULL, 
              @to_Qty         NUMERIC(10, 2) =NULL, 
              @from_Vat       NUMERIC(10, 2) =NULL, 
              @to_Vat         NUMERIC(10, 2) =NULL, 
              @from_mrp       NUMERIC(10, 2) =NULL, 
              @to_mrp         NUMERIC(10, 2) =NULL, 
              @from_discount  NUMERIC(10, 2) =NULL, 
              @to_discount    NUMERIC(10, 2) =NULL 

      IF ( @SearchColName = 'batchNo' ) 
        SELECT @BtchNo = Isnull(@SearchValue, '') 
      ELSE IF ( @SearchColName = 'product' ) 
        SELECT @product = Isnull(@SearchValue, '') 
      ELSE IF ( @SearchColName = 'expiry' ) 
        SELECT @From_ExpiryDt = @fromDate, 
               @To_ExpiryDt = @Todate 
      ELSE IF ( @SearchColName = 'quantity' ) 
        BEGIN 
            IF ( @SearchOption = 'equal' ) 
              SELECT @from_Qty = Cast (@SearchValue AS NUMERIC (10, 2)), 
                     @to_Qty = Cast (@SearchValue AS NUMERIC (10, 2)) 
            ELSE IF ( @SearchOption = 'greater' ) 
              SELECT @from_Qty = Cast (@SearchValue AS NUMERIC (10, 2)) + 0.01, 
                     @to_Qty = NULL 
            ELSE IF ( @SearchOption = 'less' ) 
              SELECT @from_Qty = NULL, 
      @to_Qty = Cast (@SearchValue AS NUMERIC (10, 2)) - 0.01 
        END 
      ELSE IF ( @SearchColName = 'vat' ) 
        BEGIN 
          IF ( @SearchOption = 'equal' ) 
              SELECT @from_vat = Cast (@SearchValue AS NUMERIC (10, 2)), 
                     @to_vat = Cast (@SearchValue AS NUMERIC (10, 2)) 
            ELSE IF ( @SearchOption = 'greater' ) 
              SELECT @from_vat = Cast (@SearchValue AS NUMERIC (10, 2)) + 0.01, 
                     @to_vat = NULL 
            ELSE IF ( @SearchOption = 'less' ) 
              SELECT @from_vat = NULL, 
                     @to_vat = Cast (@SearchValue AS NUMERIC (10, 2)) - 0.01 
        END 
      ELSE IF ( @SearchColName = 'mrp' ) 
        BEGIN 
            IF ( @SearchOption = 'equal' ) 
              SELECT @from_Mrp = Cast (@SearchValue AS NUMERIC (10, 2)), 
                     @to_Mrp = Cast (@SearchValue AS NUMERIC (10, 2)) 
            ELSE IF ( @SearchOption = 'greater' ) 
              SELECT @from_Mrp = Cast (@SearchValue AS NUMERIC (10, 2)) + 0.01, 
                     @to_Mrp = NULL 
            ELSE IF ( @SearchOption = 'less' ) 
              SELECT @from_Mrp = NULL, 
                     @to_Mrp = Cast (@SearchValue AS NUMERIC (10, 2)) - 0.01 
        END 
		*/
      SELECT 1 
             [SaleItemType], 
             salesitem.id, 
             salesitem.accountid, 
             salesitem.instanceid, 
             salesitem.salesid [SalesId],		
			 salesitem.SalesItemSno,	
             product.NAME 
             AS 
             [ProductName], 
             productstock.batchno 
             [BatchNo], 
             productstock.expiredate 
             AS 
             [ExpireDate], 
             productstock.vat 
             AS 
             [Vat], 
	 ISNULL(SalesItem.GstTotal,productstock.GstTotal)
			   AS 
             [GstTotal],
             salesitem.discount 
             [Discount], 
             salesitem.quantity 
             [Qty], 
			Isnull(salesitem.sellingprice, Isnull(productstock.sellingprice, 0)) 
			[SellingPrice], 
			Isnull(salesitem.MRP, Isnull(productstock.MRP, 0)) 
			[MRP],
			Isnull(salesitem.quantity, 0) * 
			Isnull(salesitem.sellingprice, Isnull(productstock.sellingprice, 0)) 
			[Total], 
			'' 
			[HQueUserName], 
			salesitem.createdat 
			[CreatedAt] ,
			ISNULL(null,'') [Action]
			FROM   salesitem  (nolock)
			INNER JOIN productstock  (nolock)
					ON productstock.id = salesitem.productstockid 
			INNER JOIN product  (nolock)
					ON product.id = productstock.productid 
			CROSS apply (SELECT 
						Isnull(salesitem.sellingprice, Isnull( 
						productstock.sellingprice, 0)) 
									SELLPRICE) AS Sell 
			WHERE  salesitem.salesid  =  @saleid
			/*salesitem.instanceid = @InstanceId 
			AND salesitem.accountid = @AccountId 
			/*AND salesitem.salesid IN (SELECT a.id 
										FROM   Udf_split(@saleid) a) */
			AND AND productstock.productid LIKE Isnull(@product, '') + '%' 
			AND productstock.batchno LIKE Isnull(@BtchNo, '') + '%' 
			AND productstock.expiredate BETWEEN Isnull(@From_ExpiryDt, 
												productstock.expiredate) 
												AND 
					Isnull(@To_ExpiryDt, productstock.expiredate) 
			AND salesitem.quantity BETWEEN Isnull(@from_qty, salesitem.quantity) 
											AND 
											Isnull( 
											@to_Qty, salesitem.quantity) 
			AND Isnull(Sell.sellprice, 0) BETWEEN Isnull(@from_mrp, 
													Isnull(Sell.sellprice, 0 
													)) 
													AND Isnull(@to_mrp, 
														Isnull(Sell.sellprice, 0)) 
			AND productstock.vat BETWEEN Isnull(@from_Vat, productstock.vat) AND 
											Isnull(@to_Vat, productstock.vat) */
			UNION ALL 
			SELECT DISTINCT 2 
			[SaleItemType], 
			SalesItem.id, 
			SalesItem.accountid, 
			SalesItem.instanceid, 
			SalesItem.salesid,		
			si.SalesItemSno,	
			product.NAME 
			AS 
			[ProductName], 
			productstock.batchno 
			[BatchNo], 
			productstock.expiredate 
			AS 
			[ExpireDate], 
			productstock.vat 
			AS 
			[Vat], 
			ISNULL(SI.GstTotal,productstock.GstTotal)
			   AS 
             [GstTotal],
			SalesItem.discount 
			[Discount], 
			SalesItem.quantity 
			[Qty], 
			Isnull(SalesItem.sellingprice, Isnull(productstock.sellingprice, 0)) 
			[SellingPrice], 
			Isnull(SalesItem.MRP, Isnull(productstock.MRP, 0)) 
			[MRP],
			Isnull(SalesItem.quantity, 0) * 
			Isnull(SalesItem.sellingprice, Isnull(productstock.sellingprice, 0)) 
			[Total], 			
			hqueuser.NAME 
			AS 
			[HQueUserName], 
			Salesitem.createdat 
			[CreatedAt] ,
			ISNULL([SalesItem].Action,'') [Action]
			FROM   salesitemaudit   (nolock)[SalesItem] 
			LEFT JOIN SalesItem (NOLOCK) [SI] ON SalesItem.SalesId = SI.SalesId
			and si.ProductStockId = SalesItem.ProductStockId
			INNER JOIN productstock  (nolock)
					ON productstock.id = SalesItem.productstockid 
			INNER JOIN product  (nolock)
					ON product.id = productstock.productid 
			INNER JOIN hqueuser  (nolock)
					ON hqueuser.id = SalesItem.updatedby 
			CROSS apply (SELECT 
						Isnull(SalesItem.sellingprice, Isnull( 
						productstock.sellingprice, 0)) 
									SELLPRICE) AS Sell 
			WHERE    salesitem.salesid  =  @saleid
			/*AND productstock.productid LIKE Isnull(@product, '') + '%' 
			AND Isnull(productstock.batchno, '')LIKE Isnull(@BtchNo, '') + '%' 
			AND productstock.expiredate BETWEEN Isnull(@From_ExpiryDt, 
												productstock.expiredate) 
												AND 
					Isnull(@To_ExpiryDt, productstock.expiredate) 
			AND SalesItem.quantity BETWEEN Isnull(@from_qty, SalesItem.quantity) 
											AND 
											Isnull( 
											@to_Qty, SalesItem.quantity) 
			AND Isnull(Sell.sellprice, 0) BETWEEN Isnull(@from_mrp, 
													Isnull(Sell.sellprice, 0 
													)) 
													AND Isnull(@to_mrp, 
														Isnull(Sell.sellprice, 0)) 
			AND productstock.vat BETWEEN Isnull(@from_Vat, productstock.vat) AND 
											Isnull(@to_Vat, productstock.vat) */


			Union all

			 SELECT 3
             [SaleItemType], 
             salesitem.id, 
             salesitem.accountid, 
             salesitem.instanceid, 
             SR.salesid [SalesId], 
			 salesitem.Cgst,
             product.NAME 
             AS 
             [ProductName], 
             productstock.batchno 
             [BatchNo], 
             productstock.expiredate 
             AS 
             [ExpireDate], 
             productstock.vat 
             AS 
             [Vat], 
			 ISNULL(SalesItem.GstTotal,productstock.GstTotal)
			   AS 
             [GstTotal],
             salesitem.discount 
             [Discount], 
             salesitem.quantity 
             [Qty], 
			Isnull(salesitem.MrpSellingPrice, Isnull(productstock.sellingprice, 0)) 
			[SellingPrice], 
			0 [MRP], 
			Isnull(salesitem.quantity, 0) * 
			Isnull(salesitem.MrpSellingPrice, Isnull(productstock.sellingprice, 0)) 
			[Total], 
			'' 
			[HQueUserName], 
			salesitem.createdat 
			[CreatedAt] ,
			ISNULL(null,'') [Action]
			FROM   SalesReturnItem (nolock) salesitem 
			Inner join SalesReturn SR on SR.id = salesitem.SalesReturnId
			 and isnull(SR.IsAlongWithSale ,0)   = 1
  			Inner JOIN productstock  (nolock)
					ON productstock.id = salesitem.productstockid 
			Inner JOIN product  (nolock)
					ON product.id = productstock.productid 
			CROSS apply (SELECT 
						Isnull(salesitem.MrpSellingPrice, Isnull( 
						productstock.sellingprice, 0)) 
									SELLPRICE) AS Sell 
			WHERE SR.salesid  =  @saleid and isnull(salesitem.IsDeleted,'') != 1     

			/*
			AND salesitem.instanceid = @InstanceId 
			AND salesitem.accountid = @AccountId 
			 AND productstock.productid LIKE Isnull(@product, '') + '%' 
			AND productstock.batchno LIKE Isnull(@BtchNo, '') + '%' 
			AND productstock.expiredate BETWEEN Isnull(@From_ExpiryDt, 
												productstock.expiredate) 
												AND 
					Isnull(@To_ExpiryDt, productstock.expiredate) 
			AND salesitem.quantity BETWEEN Isnull(@from_qty, salesitem.quantity) 
											AND 
											Isnull( 
											@to_Qty, salesitem.quantity) 
			AND Isnull(Sell.sellprice, 0) BETWEEN Isnull(@from_mrp, 
													Isnull(Sell.sellprice, 0 
													)) 
													AND Isnull(@to_mrp, 
														Isnull(Sell.sellprice, 0)) 
			AND productstock.vat BETWEEN Isnull(@from_Vat, productstock.vat) AND 
											Isnull(@to_Vat, productstock.vat) */

			UNION ALL 
			SELECT 4 
			[SaleItemType], 
			sri.id, 
			sri.accountid, 
			sri.instanceid, 
			sr.salesid,
			sr.CancelType,
			product.NAME 
			AS 
			[ProductName], 
			productstock.batchno 
			[BatchNo], 
			productstock.expiredate 
			AS 
			[ExpireDate], 
			productstock.vat 
			AS 
			[Vat], 
			ISNULL(sri.GstTotal,productstock.GstTotal)
			   AS 
             [GstTotal],
			sri.discount 
			[Discount], 
			sri.quantity 
			[Qty], 
			Isnull(sri.MrpSellingPrice, Isnull(productstock.sellingprice, 0)) 
			[SellingPrice], 
			Isnull(sri.MRP, Isnull(productstock.MRP, 0)) 
			[MRP],
			Isnull(sri.quantity, 0) * 
			Isnull(sri.MrpSellingPrice, Isnull(productstock.sellingprice, 0)) 
			[Total], 			
			hqueuser.NAME 
			AS 
			[HQueUserName], 
			sri.createdat 
			[CreatedAt] ,
			'R' [Action]
			FROM   SalesReturn (nolock) [sr] 
			INNER JOIN SalesReturnItem (nolock) [sri]
					ON sr.Id = sri.SalesReturnId
			INNER JOIN productstock  (nolock)
					ON productstock.id = sri.productstockid 
			INNER JOIN product  (nolock)
					ON product.id = productstock.productid 
			INNER JOIN hqueuser  (nolock)
					ON hqueuser.id = sri.updatedby 
			CROSS apply (SELECT 
						Isnull(sri.MrpSellingPrice, Isnull( 
						productstock.sellingprice, 0)) 
									SELLPRICE) AS Sell 
			WHERE    sr.salesid  =  @saleid
			and ISNULL(sr.IsAlongWithSale,'') != 1
			and ISNULL(sr.CancelType,0) < 1
END