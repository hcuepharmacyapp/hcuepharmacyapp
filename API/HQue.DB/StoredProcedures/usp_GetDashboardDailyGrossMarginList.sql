 -- Exec [usp_GetDashboardDailyGrossMarginList] '18204879-99ff-4efd-b076-f85b4a0da0a3'
 CREATE PROCEDURE [dbo].[usp_GetDashboardDailyGrossMarginList] (@AccountId varchar(36))
 AS
 BEGIN
   SET NOCOUNT ON
  
	        SELECT Branch,SUM(SALESNET) AS SALESNET,SUM(PURCHASENET)AS PURCHASENET,SUM(PROFIT) Profit  FROM (
            SELECT BRANCH,sum(AMOUNT - RETURNEDAMOUNT) SALESNET,SUM(PROFIT) PROFIT,'0.00' AS PURCHASENET FROM (
            SELECT Branch,sum(Amount) Amount,round(Convert(decimal(18,2),sum(Amount - TotalPurchase-ProfitReturnAmount)),0) Profit,'0.00' as ReturnedAmount,sum(ProfitReturnAmount) ProfitReturnAmount FROM (

            select Branch,sum(Amount) Amount,sum(TotalPurchase) TotalPurchase,'0.00' as ReturnedAmount,'0.00'As ProfitReturnAmount from (
            select Instance.Name AS Branch,round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity - 
            ((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount,0) / 100))-sum(salesitem.Quantity * 
            (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount, 
            Convert(decimal(18,2),sum((ISNULL(vpi.PurchasePrice,ProductStock.PurchasePrice) * salesitem.Quantity))) AS TotalPurchase,'0.00' as ReturnedAmount ,'0.00'As ProfitReturnAmount
            from Account
            --INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            INNER JOIN Instance on Instance.AccountId = Account.id 
            INNER JOIN sales on sales.AccountId = Account.id AND SALES.InstanceId=INSTANCE.ID  
            INNER JOIN salesitem on sales.id = salesitem.salesid 
            INNER JOIN productstock on salesitem.productstockid = productstock.id AND productstock.InstanceId=INSTANCE.ID
            left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem  group by productstockId) vpi on productstock.id=vpi.productstockid   
            WHERE sales.AccountId =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
            AND Convert(date,sales.invoicedate) =dateadd(day,datediff(day,1,GETDATE()),0) and sales.Cancelstatus is NULL 
            group by sales.InvoiceNo,Instance.Name ) a group by a.Branch

            UNION

            select branch,'0.00' As TotalPurchase,'0.00' As Amount,'0.00' as ReturnedAmount,sum(Amount - TotalPurchase) as ProfitReturnAmount from (
            select Instance.Name AS Branch,round(Convert(decimal(18,2),(sum((CASE WHEN sri.MrpSellingPrice > 0 then sri.MrpSellingPrice else productstock.SellingPrice END) * sri.Quantity - 
            ((CASE WHEN sri.MrpSellingPrice > 0 then sri.MrpSellingPrice else productstock.SellingPrice END) * sri.Quantity * isnull(sri.Discount,0) / 100))-sum(sri.Quantity * 
            (case when sri.MrpSellingPrice > 0 then sri.MrpSellingPrice else productstock.SellingPrice END) * isnull(sri.Discount,0) / 100))),0)  As Amount,
            Convert(decimal(18,2),sum((ISNULL(vpi.PurchasePrice,ProductStock.PurchasePrice) * sri.Quantity)))  AS TotalPurchase
            from Account
            --INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            INNER JOIN Instance on Instance.AccountId = Account.id 
            INNER JOIN SalesReturn as sr on sr.AccountId = Account.id AND sr.InstanceId=INSTANCE.ID 
            inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
            INNER JOIN productstock on sri.productstockid = productstock.id 
            left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem  group by productstockId) vpi on productstock.id=vpi.productstockid   
            WHERE sr.AccountId =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
            AND Convert(date,sr.CreatedAt) =dateadd(day,datediff(day,1,GETDATE()),0) and  sri.CancelType is NULL
            group by Instance.Name,sr.ReturnNo ) as a group by a.Branch

            UNION 

            select Instance.Name AS Branch,'0.00' As TotalPurchase,'0.00' As Amount,'0.00' as ReturnedAmount ,'0.00'As ProfitReturnAmount
            from Account
            --INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            INNER JOIN Instance on Instance.AccountId = Account.id
            WHERE Account.id =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
            group by Instance.Name) AS a GROUP BY Branch
            UNION 
            select Branch,'0.00'As Amount,'0.00'As Profit,sum(ReturnedAmount)ReturnedAmount,'0.00'As ProfitReturnAmount from (
            select Instance.Name AS Branch,'0.00'  As Amount,'0.00'As Profit,
            round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
            else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * isnull(s.Discount,0))/100)*sri.Quantity else 
            (sri.MrpSellingPrice-(sri.MrpSellingPrice * isnull(s.Discount,0))/100)*sri.Quantity end end) -( sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * isnull(sri.Discount,0))/100)*sri.Quantity else


 
            ((sri.MrpSellingPrice * isnull(sri.Discount,0))/100)*sri.Quantity end end)))),0) as ReturnedAmount
            from Account
            --INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            INNER JOIN Instance on Instance.AccountId = Account.id
            INNER JOIN SalesReturn as sr on sr.AccountId = Account.id AND sr.InstanceId=INSTANCE.ID 
            inner JOIN Sales as s ON s.Id = sr.SalesId
            inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
            inner join ProductStock as ps on ps.id = sri.ProductStockId
            inner join Product as p on p.Id = ps.ProductId
            WHERE  sr.AccountId =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
            AND Convert(date,sr.ReturnDate) =dateadd(day,datediff(day,1,GETDATE()),0) and s.Cancelstatus is NULL
            group by Instance.Name ,s.InvoiceNo) b group by b.Branch ) AS SALE GROUP BY SALE.BRANCH
            UNION 
            SELECT BRANCH,'0.00' AS SALESNET,'0.00' AS PROFIT,SUM(BUYAMOUNT - BUYRETURNAMOUNT) AS PURCHASENET FROM (
            select Branch,sum(BuyAmount) BuyAmount,'0.00' as BuyReturnAmount from (
            select Branch,sum(BuyAmount-BuyCreditAmount+BuyDebitAmount) BuyAmount,'0.00' as BuyReturnAmount from (
            SELECT Branch,SUM(BuyAmount)as BuyAmount,'0.00' as ReturnAmount,'0.00' as BuyCreditAmount,'0.00' as BuyDebitAmount FROM(
            SELECT Instance.Name AS Branch,Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
            * (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end ) )) )),0) as BuyAmount,
            '0.00' as ReturnAmount,'0.00' as BuyCreditAmount,'0.00' as BuyDebitAmount
            FROM Account
            --INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            INNER JOIN Instance on Instance.AccountId = Account.id
            INNER JOIN VendorPurchase v on v.AccountId = Account.id AND v.InstanceId=INSTANCE.ID
            inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
            inner join ProductStock ps
            on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
            where  v.AccountId =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
            AND Convert(date,v.CreatedAt)  = dateadd(day,datediff(day,1,GETDATE()),0) 
            group by Instance.Name ,v.InvoiceNo ) as a GROUP BY Branch
            UNION
            SELECT BRANCH,'0.00' as BuyAmount,'0.00' as BuyReturnAmount,Round(Convert(decimal(18,2),(SUM(BuyCreditAmount))),0) as BuyCreditAmount,'0.00' as BuyDebitAmount FROM(
            select Instance.Name AS BRANCH,'0.00' as BuyAmount,'0.00' as BuyReturnAmount,(v.NoteAmount) as BuyCreditAmount 
            FROM Account
            --INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            INNER JOIN Instance on Instance.AccountId = Account.id
            INNER JOIN VendorPurchase v on v.AccountId = Account.id AND v.InstanceId=INSTANCE.ID
            inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
            inner join ProductStock ps
            on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
            where  v.AccountId =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
            AND Convert(date,v.CreatedAt)  = dateadd(day,datediff(day,1,GETDATE()),0) and v.NoteType = 'credit'
            group by Instance.Name,v.NoteAmount
            ) AS BuyCredit GROUP BY BuyCredit.BRANCH
            UNION
            SELECT BRANCH,'0.00' as BuyAmount,'0.00' as BuyReturnAmount,'0.00' as BuyCreditAmount,Round(Convert(decimal(18,2),(SUM(BuyDebitAmount))),0) as BuyDebitAmount FROM(
            select Instance.Name AS BRANCH,'0.00' as BuyAmount,'0.00' as BuyReturnAmount,(v.NoteAmount) as BuyDebitAmount 
            FROM Account
            --INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            INNER JOIN Instance on Instance.AccountId = Account.id
            INNER JOIN VendorPurchase v on v.AccountId = Account.id AND v.InstanceId=INSTANCE.ID
            inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
            inner join ProductStock ps
            on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
            where  v.AccountId =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
            AND Convert(date,v.CreatedAt)  = dateadd(day,datediff(day,1,GETDATE()),0) and v.NoteType = 'debit'
            group by Instance.Name,v.NoteAmount
            ) AS BuyDebit GROUP BY BuyDebit.BRANCH
            ) a group by a.Branch
            UNION 
            select Instance.Name AS Branch,'0.00' As BuyAmount,'0.00' as BuyReturnAmount
            from Account
            --INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            INNER JOIN Instance on Instance.AccountId = Account.id 
            WHERE Account.id =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
            group by Instance.Name ) b group by b.Branch
            UNION
            SELECT Branch,'0.00' as BuyAmount,SUM(BuyReturnAmount) as BuyReturnAmount FROM (
            SELECT Instance.Name AS Branch,'0.00' as BuyAmount,
            Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) * VendorReturnItem.Quantity)))),0)  as BuyReturnAmount
            FROM Account
            --INNER JOIN HQueUser on HQueUser.Accountid=Account.id
            INNER JOIN Instance on Instance.AccountId = Account.id
            INNER JOIN VendorReturnItem on VendorReturnItem.AccountId = Account.id AND VendorReturnItem.InstanceId=INSTANCE.ID 
            INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
            INNER JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
            INNER JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
            INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
            INNER JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
            INNER JOIN Product ON Product.Id = ProductStock.ProductId
            where Account.id =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
            AND Convert(date,VendorReturn.ReturnDate)  = dateadd(day,datediff(day,1,GETDATE()),0) 
            GROUP BY Instance.Name,VendorPurchase.InvoiceNo) C GROUP BY C.BRANCH ) AS PURCHASE GROUP BY PURCHASE.BRANCH) TOTALMARGIN GROUP BY TOTALMARGIN.BRANCH

 END
