
 /*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 31/05/2017	Poongodi		Sales  mapping changed from inner to left 
*******************************************************************************/ 
--Usage : -- usp_GetDashboardSales '18204879-99ff-4efd-b076-f85b4a0da0a3', '013513b1-ea8c-4ea8-9fed-054b260ee197'
CREATE PROCEDURE [dbo].[usp_GetDashboardSales](@AccountId VARCHAR(36), @InstanceId VARCHAR(36) = '')
 AS
 BEGIN
   SET NOCOUNT ON

   IF @InstanceId != ''
   BEGIN

		SELECT Name, 
			sum(Amount) Amount,
			sum(SalesReturnAmount) SalesReturnAmount 
		FROM (

                SELECT 'Today' Name,
					sum(Amount) Amount,'0.00' AS SalesReturnAmount
			    FROM (
						SELECT 'Today' AS Name,
							sum(isnull(sales.SaleAmount ,0))  AS Amount,
							'0.00' AS SalesReturnAmount
						FROM sales 				 
						WHERE sales.AccountId =@AccountId and sales.InstanceId = @InstanceId 
						AND Convert(date,sales.invoicedate) =dateadd(day,datediff(day,0,GETDATE()),0) and sales.Cancelstatus is NULL
					) a GROUP BY a.name

                UNION  

                SELECT 'Today' AS Name, 
					   '0.00' AS Amount, 
					   '0.00' AS SalesReturnAmount
                UNION 

                SELECT Name,
					  '0.00'  AS Amount,
					   SUM(SalesReturnAmount) SalesReturnAmount 
				FROM (

					SELECT 'Today' AS Name,
					'0.00'  AS Amount, 
					sr.NetAmount AS SalesReturnAmount
					FROM SalesReturn AS sr
						Left JOIN Sales AS s ON s.Id = sr.SalesId  
						INNER JOIN SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
						INNER JOIN  ProductStock as ps on ps.id = sri.ProductStockId
						INNER JOIN  Product as p on p.Id = ps.ProductId
					WHERE sr.AccountId =@AccountId AND sr.InstanceId = @InstanceId and ISNULL(   S.Cancelstatus,0) =0  
					AND CONVERT(DATE,sr.ReturnDate) =DATEADD(day,datediff(day,0,GETDATE()),0) 
					AND (sri.IsDeleted IS NULL OR  sri.IsDeleted != 1)
					GROUP BY ISNULL(S.InvoiceSeries, '') +' '+ ISNULL(s.InvoiceNo ,''),sr.NetAmount) e GROUP BY e.name
                ) Today 
				GROUP BY Name

 
                UNION  ALL

                select Name, sum(Amount)Amount, sum(SalesReturnAmount)SalesReturnAmount from (

                select 'Yesterday' Name,sum(Amount) Amount,'0.00' as SalesReturnAmount from (
                select 'Yesterday' As Name,
 
				sum(isnull(sales.SaleAmount ,0)) As Amount,
                '0.00' as SalesReturnAmount
                from sales
                 WHERE sales.AccountId =@AccountId AND sales.InstanceId = @InstanceId  
                AND Convert(date,sales.invoicedate) =dateadd(day,datediff(day,1,GETDATE()),0) and sales.Cancelstatus is NULL
             
				) b group by b.name

                union 
                select 'Yesterday' As Name, '0.00' as Amount, '0.00' as SalesReturnAmount

                union 

                select 'Yesterday' As Name,'0.00'  As Amount , sum(SalesReturnAmount) from (
                select 'Yesterday' As Name,
                '0.00'  As Amount,

				sr.NetAmount AS SalesReturnAmount
                from SalesReturn as sr Left JOIN Sales as s ON s.Id = sr.SalesId  
                inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
                inner join ProductStock as ps on ps.id = sri.ProductStockId
                inner join Product as p on p.Id = ps.ProductId
                WHERE sr.AccountId =@AccountId AND sr.InstanceId = @InstanceId and ISNULL(   S.Cancelstatus,0) =0  
                AND Convert(date,sr.ReturnDate) =dateadd(day,datediff(day,1,GETDATE()),0)  
				and (sri.IsDeleted is null or sri.IsDeleted != 1)
                group by ISNULL(S.InvoiceSeries, '') +' '+ isnull(s.InvoiceNo ,''),sr.NetAmount) f group by f.name
                ) Yesterday group by Name

                union all

                select Name, sum(Amount)Amount, sum(SalesReturnAmount)SalesReturnAmount from (
                select  'Last Month' As Name,sum(Amount) Amount,'0.00' as SalesReturnAmount from (
                select 'Last Month' As Name,

				sum(isnull(sales.SaleAmount ,0)) As Amount,
                '0.00' as SalesReturnAmount
                from sales

                WHERE sales.AccountId =@AccountId and sales.InstanceId = @InstanceId  
                AND Convert(date,sales.invoicedate) between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0))) and sales.Cancelstatus is NULL

				) c group by c.name
                union 
                select 'Last Month' As Name, '0.00' as Amount, '0.00' as SalesReturnAmount
                union 

                select 'Last Month' As Name,'0.00'  As Amount,sum(SalesReturnAmount) from (
                select 'Last Month' As Name,
                '0.00'  As Amount,

				sr.NetAmount as SalesReturnAmount
                from SalesReturn as sr Left JOIN Sales as s ON s.Id = sr.SalesId  
                inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
                inner join ProductStock as ps on ps.id = sri.ProductStockId
                inner join Product as p on p.Id = ps.ProductId
                WHERE sr.AccountId =@AccountId AND sr.InstanceId = @InstanceId and ISNULL(S.Cancelstatus,0) =0  
                AND Convert(date,sr.ReturnDate) between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))  
				and (sri.IsDeleted is null or sri.IsDeleted != 1)
                group by ISNULL(S.InvoiceSeries, '') +' '+ isnull(s.InvoiceNo ,''),sr.NetAmount) g group by g.name
                ) LastMonth group by Name

                union all

                select Name, sum(Amount)Amount, sum(SalesReturnAmount)SalesReturnAmount from (
                select Name,sum(Amount)Amount,'0.00' SalesReturnAmount from (
				--select 'Financial Year' As Name, 0.00 Amount
                 select 'Financial Year' As Name,
 
				sum(isnull(sales.SaleAmount ,0)) As Amount,
                '0.00' as SalesReturnAmount
                from sales 
 
                WHERE sales.AccountId =@AccountId AND sales.InstanceId = @InstanceId 
                AND Convert(date,sales.invoicedate) between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )



))
                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ))))) and sales.Cancelstatus is NULL
 
				) d group by d.name

                union 
                select 'Financial Year' As Name, '0.00' as Amount, '0.00' as SalesReturnAmount 
                union 

                select Name,'0.00' Amount,sum(SalesReturnAmount)SalesReturnAmount from (
                select 'Financial Year' As Name,
                '0.00'  As Amount,
				 
				sr.NetAmount as SalesReturnAmount
                from SalesReturn as sr left JOIN Sales as s ON s.Id = sr.SalesId  
                inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
                inner join ProductStock as ps on ps.id = sri.ProductStockId
                inner join Product as p on p.Id = ps.ProductId
                WHERE sr.AccountId =@AccountId AND sr.InstanceId = @InstanceId and ISNULL(   S.Cancelstatus,0) =0  
                AND Convert(date,sr.ReturnDate) between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))
                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))))
				and (sri.IsDeleted is null or sri.IsDeleted != 1)  
                group by ISNULL(S.InvoiceSeries, '') +' '+ isnull(s.InvoiceNo ,''),sr.NetAmount) h group by h.name
                ) Financial group by Name
	END
	ELSE
	BEGIN
		select Name, sum(Amount) Amount, sum(SalesReturnAmount) SalesReturnAmount from (

                select 'Today' Name,sum(Amount) Amount,'0.00' as SalesReturnAmount from (
                select 'Today' As Name,
 --                round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) 
	--			* salesitem.Quantity - 
	--((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount ,0) / 100))-sum(salesitem.Quantity * 
	--(case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount, 
				sales.SaleAmount As Amount,
                '0.00' as SalesReturnAmount
                from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id
                --left join vendorpurchaseitem on productstock.id=vendorpurchaseitem.productstockid 
                WHERE sales.AccountId =@AccountId
                AND Convert(date,sales.invoicedate) =dateadd(day,datediff(day,0,GETDATE()),0) and sales.Cancelstatus is NULL
                --group by ISNULL(sales.InvoiceSeries, '') +' '+sales.InvoiceNo 
				group by sales.id,sales.SaleAmount 
				) a group by a.name

                union 
                select 'Today' As Name, '0.00' as Amount, '0.00' as SalesReturnAmount
                union 

                select Name,'0.00'  As Amount,sum(SalesReturnAmount)SalesReturnAmount from (

                select 'Today' As Name,
                '0.00'  As Amount,
--                round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
--                else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * isnull(s.Discount ,0))/100)*sri.Quantity else 
--                (sri.MrpSellingPrice-(sri.MrpSellingPrice * isnull(s.Discount ,0))/100)*sri.Quantity end end) -( sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * isnull(sri.Discount,0))/100)*sri.Quantity 


--else 
--                ((sri.MrpSellingPrice * isnull(sri.Discount ,0))/100)*sri.Quantity end end)))),0) as SalesReturnAmount
				sr.NetAmount as SalesReturnAmount
                from SalesReturn as sr Left JOIN Sales as s ON s.Id = sr.SalesId  
                inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
                inner join ProductStock as ps on ps.id = sri.ProductStockId
                inner join Product as p on p.Id = ps.ProductId
				and (sri.IsDeleted is null or sri.IsDeleted != 1)
                WHERE sr.AccountId =@AccountId
                AND Convert(date,sr.ReturnDate) =dateadd(day,datediff(day,0,GETDATE()),0)   AND ISNULL(   S.Cancelstatus,0) =0  
                group by ISNULL(S.InvoiceSeries, '') +' '+ isnull(s.InvoiceNo ,''),sr.NetAmount) e group by e.name
                ) Today group by Name

 
                union all

                select Name, sum(Amount)Amount, sum(SalesReturnAmount)SalesReturnAmount from (

                select 'Yesterday' Name,sum(Amount) Amount,'0.00' as SalesReturnAmount from (
                select 'Yesterday' As Name,
 --                round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) 
	--			* salesitem.Quantity - 
	--((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount ,0) / 100))-sum(salesitem.Quantity * 
	--(case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount,
				sales.SaleAmount As Amount,
                '0.00' as SalesReturnAmount
                from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id 
                --left join vendorpurchaseitem on productstock.id=vendorpurchaseitem.productstockid 
                WHERE sales.AccountId =@AccountId
                AND Convert(date,sales.invoicedate) =dateadd(day,datediff(day,1,GETDATE()),0) and sales.Cancelstatus is NULL
                --group by ISNULL(sales.InvoiceSeries, '') +' '+sales.InvoiceNo 
				group by sales.id,sales.SaleAmount 
				) b group by b.name

                union 
                select 'Yesterday' As Name, '0.00' as Amount, '0.00' as SalesReturnAmount

                union 

                select 'Yesterday' As Name,'0.00'  As Amount , sum(SalesReturnAmount) from (
                select 'Yesterday' As Name,
                '0.00'  As Amount,

 --               round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
 --               else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * isnull(s.Discount ,0))/100)*sri.Quantity else 
 --               (sri.MrpSellingPrice-(sri.MrpSellingPrice * isnull(s.Discount ,0))/100)*sri.Quantity end end) -( sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * isnull(sri.Discount,0))/100)*sri.Quantity




 --else 
 --               ((sri.MrpSellingPrice * isnull(sri.Discount,0))/100)*sri.Quantity end end)))),0) as SalesReturnAmount
				sr.NetAmount as SalesReturnAmount
                from SalesReturn as sr left JOIN Sales as s ON s.Id = sr.SalesId  
                inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
                inner join ProductStock as ps on ps.id = sri.ProductStockId
                inner join Product as p on p.Id = ps.ProductId
                WHERE sr.AccountId =@AccountId
                AND Convert(date,sr.ReturnDate) =dateadd(day,datediff(day,1,GETDATE()),0)   AND ISNULL(   S.Cancelstatus,0) =0  
				and (sri.IsDeleted is null or sri.IsDeleted != 1)
                group by ISNULL(S.InvoiceSeries, '') +' '+ isnull(s.InvoiceNo ,''),sr.NetAmount) f group by f.name
                ) Yesterday group by Name

                union all

                select Name, sum(Amount)Amount, sum(SalesReturnAmount)SalesReturnAmount from (
                select  'Last Month' As Name,sum(Amount) Amount,'0.00' as SalesReturnAmount from (
                select 'Last Month' As Name,
 --                round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) 
	--			* salesitem.Quantity - 
	--((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount ,0) / 100))-sum(salesitem.Quantity * 
	--(case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount,
				sales.SaleAmount As Amount,
                '0.00' as SalesReturnAmount
                from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id 
                --left join vendorpurchaseitem on productstock.id=vendorpurchaseitem.productstockid 
                WHERE sales.AccountId =@AccountId
                AND Convert(date,sales.invoicedate) between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0))) and sales.Cancelstatus is NULL
                --between  CONVERT(varchar,dateadd(d,-(day(dateadd(m,-1,getdate()-2))),dateadd(m,-1,getdate()-1)),106) and CONVERT(varchar,dateadd(d,-(day(getdate())),getdate()),106) 
                --group by ISNULL(sales.InvoiceSeries, '') +' '+sales.InvoiceNo
				group by sales.id,sales.SaleAmount 
				) c group by c.name
                union 
    select 'Last Month' As Name, '0.00' as Amount, '0.00' as SalesReturnAmount
                union 

                select 'Last Month' As Name,'0.00'  As Amount,sum(SalesReturnAmount) from (
                select 'Last Month' As Name,
                '0.00'  As Amount,
--                round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
--                else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * isnull(s.Discount ,0))/100)*sri.Quantity else 
--                (sri.MrpSellingPrice-(sri.MrpSellingPrice * isnull(s.Discount ,0))/100)*sri.Quantity end end) -(sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * isnull(sri.Discount ,0))/100)*sri.Quantity 
--else 
--                ((sri.MrpSellingPrice * isnull(sri.Discount ,0))/100)*sri.Quantity end end)))),0) as SalesReturnAmount
				sr.NetAmount as SalesReturnAmount
                from SalesReturn as sr Left JOIN Sales as s ON s.Id = sr.SalesId  
                inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
                inner join ProductStock as ps on ps.id = sri.ProductStockId
                inner join Product as p on p.Id = ps.ProductId
                WHERE sr.AccountId =@AccountId AND ISNULL(   S.Cancelstatus,0) =0  
                AND Convert(date,sr.ReturnDate) between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))  and s.Cancelstatus is NULL
				and (sri.IsDeleted is null or sri.IsDeleted != 1)
                --between  CONVERT(varchar,dateadd(d,-(day(dateadd(m,-1,getdate()-2))),dateadd(m,-1,getdate()-1)),106) and CONVERT(varchar,dateadd(d,-(day(getdate())),getdate()),106)
                group by ISNULL(S.InvoiceSeries, '') +' '+ isnull(s.InvoiceNo ,''),sr.NetAmount) g group by g.name
                ) LastMonth group by Name

                union all

                select Name, sum(Amount)Amount, sum(SalesReturnAmount)SalesReturnAmount from (
                select Name,sum(Amount)Amount,'0.00' SalesReturnAmount from (
                select 'Financial Year' As Name,
 --                round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) 
	--			* salesitem.Quantity - 
	--((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount ,0) / 100))-sum(salesitem.Quantity * 
	--(case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount,
				sales.SaleAmount As Amount,
                '0.00' as SalesReturnAmount
                from sales inner join salesitem on sales.id = salesitem.salesid inner join productstock on salesitem.productstockid = productstock.id 
                --left join vendorpurchaseitem on productstock.id=vendorpurchaseitem.productstockid 
                WHERE sales.AccountId =@AccountId
                AND Convert(date,sales.invoicedate) between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 



)

))
                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ))))) and sales.Cancelstatus is NULL
                --group by ISNULL(sales.InvoiceSeries, '') +' '+sales.InvoiceNo 
				group by sales.id,sales.SaleAmount 
				) d group by d.name
				
     union 
                select 'Financial Year' As Name, '0.00' as Amount, '0.00' as SalesReturnAmount 
                union 

                select Name,'0.00' Amount,sum(SalesReturnAmount)SalesReturnAmount from (
                select 'Financial Year' As Name,
                '0.00'  As Amount,
--                round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
--                else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * isnull(s.Discount ,0))/100)*sri.Quantity else 
--                (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) - (sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity 


--else 
--                ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end)))),0) as SalesReturnAmount
				sr.NetAmount as SalesReturnAmount
                from SalesReturn as sr Left JOIN Sales as s ON s.Id = sr.SalesId  
                inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
                inner join ProductStock as ps on ps.id = sri.ProductStockId
                inner join Product as p on p.Id = ps.ProductId
                WHERE sr.AccountId =@AccountId AND ISNULL(   S.Cancelstatus,0) =0  
                AND Convert(date,sr.ReturnDate) between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))
                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ))))) and s.Cancelstatus is NULL
				and (sri.IsDeleted is null or sri.IsDeleted != 1)
                group by ISNULL(S.InvoiceSeries, '') +' '+ isnull(s.InvoiceNo ,''),sr.NetAmount) h group by h.name
                ) Financial group by Name
	END
END
