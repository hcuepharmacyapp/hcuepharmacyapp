﻿/*                               
******************************************************************************                            
 --usp_get_patient_details 'c503ca6e-f418-4807-a847-b6886378cf0b','508ddc99-7147-47be-94a3-113e0bbdb81f','dep',0
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************    
 27-05-2017     Violet        Patient detils
 18-07-2017     Poongodi	  Top 10 Added
*******************************************************************************/ 
Create PROC [dbo].[usp_get_patient_details] ( 
@AccountId  NVARCHAR(36), 
@InstanceId NVARCHAR(36), 
@Name       VARCHAR(50), 
@Dept       INT,
@IsActive   int)
AS 
  BEGIN 
   
	--When dept 1
	IF @Dept = 1
	BEGIN
	
		SELECT Top 10 Patient.Id,Patient.AccountId,Patient.InstanceId,Patient.Code,Patient.Name,Patient.ShortName,Patient.Mobile,
		Patient.Email,Patient.DOB,Patient.Age,Patient.Gender,Patient.Address,Patient.Area,Patient.City,Patient.State,
		Patient.Pincode,Patient.OfflineStatus,Patient.CreatedAt,Patient.UpdatedAt,Patient.CreatedBy,Patient.UpdatedBy,
		Patient.EmpID,Patient.Discount,Patient.IsSync,Patient.CustomerPaymentType,Patient.PatientType,Patient.Status FROM Patient 
		WHERE Patient.Name  Like  @Name+'%' AND Patient.AccountId  =  @AccountId and (PatientType =1) 
		and Patient.Status = case when @IsActive = 1 then @IsActive else Patient.Status  end
		union
		SELECT  Top 10 Patient.Id,Patient.AccountId,Patient.InstanceId,Patient.Code,Patient.Name,Patient.ShortName,Patient.Mobile,
		Patient.Email,Patient.DOB,Patient.Age,Patient.Gender,Patient.Address,Patient.Area,Patient.City,Patient.State,
		Patient.Pincode,Patient.OfflineStatus,Patient.CreatedAt,Patient.UpdatedAt,Patient.CreatedBy,Patient.UpdatedBy,
		Patient.EmpID,Patient.Discount,Patient.IsSync,Patient.CustomerPaymentType,Patient.PatientType,Patient.Status FROM Patient 
		WHERE Patient.Name  Like  @Name+'%' AND Patient.AccountId  =  @AccountId AND Patient.InstanceId  =  @InstanceId 
		and (PatientType =2) and Patient.Status = case when @IsActive = 1 then @IsActive else Patient.Status  end
	END

--When dept 0
IF @Dept = 0
	BEGIN
		SELECT  Top 10 Patient.Id,Patient.AccountId,Patient.InstanceId,Patient.Code,Patient.Name,Patient.ShortName,Patient.Mobile,
		Patient.Email,Patient.DOB,Patient.Age,Patient.Gender,Patient.Address,Patient.Area,Patient.City,Patient.State,
		Patient.Pincode,Patient.OfflineStatus,Patient.CreatedAt,Patient.UpdatedAt,Patient.CreatedBy,Patient.UpdatedBy,
		Patient.EmpID,Patient.Discount,Patient.IsSync,Patient.CustomerPaymentType,Patient.PatientType,Patient.Status FROM Patient 
		WHERE Patient.Name  Like  @Name+'%' AND Patient.AccountId  =  @AccountId --AND Patient.InstanceId  =  @InstanceId 
		and (PatientType <> 2 or PatientType is null) and Patient.Status = case when @IsActive = 1 then @IsActive else Patient.Status  end
	END
  END