 
/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 13/03/2017	Poongodi	   Stock type parameter added
*******************************************************************************/ 
 --Usage : -- usp_GetStockReportList '28c860a2-21c4-4642-9b14-40d29f411878'--,'4edd19c2-3fe3-4ba2-8083-83a06ccd6c88'
 --[usp_GetStockReportCategoryList] '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','nonzero','TABLET'
 --[usp_GetStockReportCategoryList] '3e2ec066-1551-4527-be53-5aa3c5b7fb7d','nonzero','t'
 --usp_GetStockReportCategoryList '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','zero'
 --usp_GetStockReportCategoryList '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','expire'
 --usp_GetStockReportCategoryList '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','all'
CREATE PROCEDURE [dbo].[usp_GetStockReportCategoryList](@InstanceId varchar(36), @StockType varchar(10), @category varchar(10))
AS
 BEGIN
  Declare @From_Stock decimal(28,6)=0, @To_Stock decimal(28,6) =0,@From_Expiry date =NULL, @To_Expiry date =NULL 
 declare @Status table (stockstatus int )
 if (right(@StockType,1) ='2')
 begin
 insert into @Status 	values (2)
 select @StockType  = replace (@StockType,2,'')
 end 
 else
 begin
 insert into @Status 	values (1),(0)
 end 

 if (@StockType ='nonzero')
	begin
	select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  = cast(getdate () as date), @To_Expiry  = NULL
	 
	end 
 else if (@StockType ='expire')
	begin
	select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  =  NULL, @To_Expiry  =cast(getdate () as date)
	
	end
-- All Stock List
  else if (@StockType ='all')
	begin
	select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  =  NULL, @To_Expiry  = NULL	
	end  

   SELECT distinct --P.Name, P.GenericName, PS.BatchNo, ABS(PS.Stock) AS Quantity, p.id,
         --PS.ExpireDate, ISNULL(PS.VAT,0) as VAT,
		 --ISNULL(ISNULL((select top 1 purchaseprice from vendorpurchaseitem   inner join vendorpurchase   vp on vp.id =vendorpurchaseitem.vendorpurchaseid
		 --and vp.instanceid = vendorpurchaseitem.instanceid  where ProductStockId=ps.id and isnull(CancelStatus ,0) =0  and vendorpurchaseitem.instanceid=@InstanceId
		 -- order by vendorpurchaseitem.CreatedAt desc),PS.PurchasePrice) * (PS.Stock),0)  As CostPrice,
         --ISNULL((PS.Stock * PS.Sellingprice),0) as MRP,Vendor.Name AS Vendor, 
         P.Category --, 
		 --P.Schedule, 
		 --P.Type , case isnull(ps.status,0) when 2 then 'Inactive' else 'Active' end [Status]
    FROM ProductStock PS WITH(NOLOCK)
    INNER JOIN Product P  WITH(NOLOCK) ON P.Id = PS.ProductId 
    LEFT JOIN Vendor  WITH(NOLOCK) ON Vendor.Id = PS.VendorId 
     WHERE PS.InstanceId = @InstanceId 
	 	 and ps.Stock between isnull(@From_Stock , ps.stock) and isnull(@To_Stock , ps.stock) 
		 and P.Category like ''+@category+'%'
		  
	 and cast(ps.ExpireDate  as date)  between isnull(@From_Expiry ,cast(ps.ExpireDate  as date) ) and isnull(@To_Expiry,cast(ps.ExpireDate  as date) )
    ORDER BY P.Category 

END