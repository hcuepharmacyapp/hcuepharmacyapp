 
/*                               
******************************************************************************                            
** File: [Usp_Get_SalesReturnLoyaltyPts]   
** Name: [Usp_Get_SalesReturnLoyaltyPts]                               
** Description: To Get Sales Return Loyalty Pts against sales
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Sarubala V   
** Created Date: 28/06/2018   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 
*******************************************************************************/ 
 
Create Proc dbo.Usp_Get_SalesReturnLoyaltyPts (@SaleId char(36))
as
begin
 select SR.SalesId SaleId,  ROUND(SUM(isnull(SRI.LoyaltyProductPts,0)) ,3 ) [LoyaltyPts]
 from SalesReturn SR inner join SalesReturnItem SRI on SR.id =sri.SalesReturnId
 where  SR.SalesId =@SaleId  and ISNULL(sri.IsDeleted,'') != 1 and ISNULL(sr.CancelType,0) = 0
 group by  SR.SalesId

  end 