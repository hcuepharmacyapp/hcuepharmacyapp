 
 --Usage : -- usp_GetPurchaseDetailMultiLevelReport '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93',1 ,'01-Jul-2017','23-Jul-2017',null,null,null,'01-Jul-2017'
/** Date        Author          Description                              
*******************************************************************************        
 ** 20/07/2017	Sarubala		Created 
*******************************************************************************/ 

CREATE PROCEDURE [dbo].[usp_GetPurchaseDetailMultiLevelReport](@InstanceIds varchar(1000),@AccountId varchar(36), @Level smallint, @StartDate datetime,@EndDate datetime, @SearchColName VARCHAR(50), @SearchOption VARCHAR(50), @SearchValue VARCHAR(50), @InvoiceDate datetime)
 AS
 BEGIN
	SET NOCOUNT ON

	DECLARE  @billNo VARCHAR(100) = NULL, 
	@VendorId VARCHAR(200) = NULL,
	@from_gstTotal NUMERIC(10,2) = NULL,
	@to_gstTotal NUMERIC(10,2) = NULL,
	@from_profitCost NUMERIC(10,2) = NULL,
	@to_profitCost NUMERIC(10,2) = NULL,
	@from_profitMrp NUMERIC(10,2) = NULL,
	@to_profitMrp NUMERIC(10,2) = NULL

	IF @SearchColName IS NULL OR @SearchColName = ''
		SELECT @SearchColName = NULL, @SearchOption = NULL, @SearchValue = NULL
	IF @SearchOption = ''
		SELECT @SearchOption = NULL
	IF @SearchValue = '' 
		SELECT @SearchValue = NULL

	DECLARE @InvoiceSeries VARCHAR(500) = @SearchOption
	IF (ISNULL(@SearchColName,'') != 'billNo') 
		SELECT @InvoiceSeries = 'ALL'

	DECLARE @XmlList XML
	SET @XmlList = CAST(('<A>'+REPLACE(ISNULL(@InvoiceSeries,''),',','</A><A>')+'</A>') AS XML)
	
	IF @InvoiceDate is not null 
		SELECT @InvoiceDate = CONVERT(date,@InvoiceDate)

	IF (@SearchColName = 'billNo') 
		BEGIN
			SELECT @billNo = @SearchValue
		END
	Else IF (@SearchColName ='gstTotal') 
		BEGIN 
		  IF (@SearchOption = 'equal') 
		  SELECT @from_gstTotal = Cast (@SearchValue AS NUMERIC(10,2)) , 
				 @to_gstTotal = Cast(@SearchValue AS NUMERIC(10,2)) 
		  ELSE 
		  IF (@SearchOption = 'greater') 
		  SELECT @from_gstTotal = Cast(@SearchValue AS NUMERIC(10,2))+0.01, 
				 @to_gstTotal = NULL 
		  ELSE 
		  IF (@SearchOption = 'less') 
		  SELECT @from_gstTotal = 0.00, 
				 @to_gstTotal = Cast(@SearchValue AS NUMERIC(10,2))-0.01 
		END
	Else IF (@SearchColName ='vendorName') 
		Select @VendorId = @SearchValue

			 
	If @Level = 1
	Begin
		;With temp_instances (InstanceId)
		As (
		select * from udf_Split(@InstanceIds) a
		)
		
		SELECT 
		InvoiceDate, 
		'' InstanceId, 
		'' InstanceName, 
		'' InvoiceNo, 
		'' Prefix, 
		'' VendorName,
		(SUM(PurchaseDiscountAmount)-SUM(ReturnDiscountAmount)) AS PurchaseDiscountAmount,
		(SUM(SellingPrice)-SUM(ReturnSellingPrice)) AS SellingPrice,
		(SUM(CostPriceWithoutTax)-SUM(ReturnCostPriceWithoutTax)) AS CostPriceWithoutTax,
		(SUM(TaxAmount)-SUM(ReturnTaxAmount)) AS TaxAmount,
		(SUM(RoundOffNetAmount)) AS RoundOffNetAmount,
		(SUM(NetPurchasePrice)-SUM(ReturnNetPurchasePrice)) AS NetPurchasePrice,
		0 ReturnFlag 
		,SUM(Credit) Credit, sum(Debit) Debit
		FROM
		(SELECT 
		InvoiceDate, 
		'' InstanceId, 
		'' InstanceName, 
		'' InvoiceNo, 
		'' Prefix, 
		'' VendorName,
		SUM(PurchaseDiscountAmount) PurchaseDiscountAmount,SUM(ReturnDiscountAmount) ReturnDiscountAmount,
		SUM(SellingPrice) SellingPrice,SUM(ReturnSellingPrice) ReturnSellingPrice,
		SUM(CostPriceWithoutTax) CostPriceWithoutTax,SUM(ReturnCostPriceWithoutTax) ReturnCostPriceWithoutTax,
		SUM(TaxAmount) TaxAmount,SUM(ReturnTaxAmount) ReturnTaxAmount,
		SUM(RoundOffNetAmount) AS RoundOffNetAmount,
		SUM(Round(Round(NetPurchasePrice,2),0))- sum(Credit) + SUM(Debit) NetPurchasePrice,SUM(Round(Round(ReturnNetPurchasePrice,2),0)) AS	ReturnNetPurchasePrice,
		0 ReturnFlag ,
		SUM(Credit) Credit, sum(Debit) Debit
		FROM
		(
			SELECT Convert(date,VP.InvoiceDate) InvoiceDate,
			'' InstanceId,
			'' InstanceName,
			'' InvoiceNo,
			'' Prefix,
			'' VendorName
			,VP.Id as VendorPurhaseId
			--, SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0) )) * ISNULL(VPI.Discount,0) / 100 ) PurchaseDiscountAmount	
			, SUM(ISNULL(VPI.DiscountValue, 0)) AS PurchaseDiscountAmount	
			, 0 AS ReturnDiscountAmount	
					
			--, SUM(CONVERT(decimal(18,2), ISNULL(PS.SellingPrice, isnull(VPI.PackageSellingPrice,0)/isnull(VPI.PackageSize,1))) * (isnull(VPI.Quantity,0) )) As SellingPrice
			, SUM(CONVERT(decimal(18,6), isnull(VPI.PackageSellingPrice,0)/isnull(VPI.PackageSize,1)) * (isnull(VPI.Quantity,0) )) As SellingPrice
			, 0 ReturnSellingPrice	
						
			--, SUM(ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (1- ISNULL(VPI.Discount,0) / 100)  * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0) )) as CostPriceWithoutTax		
			, SUM((VPI.PurchasePrice*VPI.Quantity)-VPI.GstValue) as CostPriceWithoutTax		
			, 0 AS ReturnCostPriceWithoutTax
				
			--, SUM((ISNULL(VPI.PackageQty, 0) - ISNULL(VPI.FreeQty,0) )*(isNull(VPI.PackagePurchasePrice, PS.PackagePurchasePrice)) * (1- ISNULL(VPI.Discount,0) / 100) * (CASE ISNULL(VP.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END) / (100)) as TaxAmount	
			, SUM(VPI.GstValue) as TaxAmount	
			, 0 AS ReturnTaxAmount		
			, SUM(ISNULL(VP.RoundOffValue, 0)) RoundOffNetAmount			
			--, SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0) ) * (1 - (ISNULL(VPI.Discount,0) / 100))) * (1 + (CASE ISNULL(VP.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(VPI.GstTotal,0) != 0 then VPI.GstTotal else ISNULL(PS.Gsttotal,0) end
			-- ELSE PS.Vat END / 100))) - SUM(Case When NoteType='credit' Then IsNull(NoteAmount, 0) Else 0 End) + SUM(Case When NoteType='debit' Then IsNull(NoteAmount, 0) Else 0 End) NetPurchasePrice
			--, SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0) ) * (1 - (ISNULL(VPI.Discount,0) / 100))) * (1 + (CASE ISNULL(VP.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END / 100))) NetPurchasePrice
			, SUM(VPI.PurchasePrice*VPI.Quantity) AS NetPurchasePrice
			, 0 AS ReturnNetPurchasePrice
			, 0 ReturnFlag
			,max(Case When NoteType='credit' Then IsNull(NoteAmount, 0) Else 0 End) Credit
			,max(Case When NoteType='debit' Then IsNull(NoteAmount, 0) Else 0 End) Debit

		FROM VendorPurchase VP WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = VP.InstanceId
		INNER JOIN VendorPurchaseItem VPI  WITH(NOLOCK) on VP.id= VPI.VendorPurchaseId
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=VPI.productstockid
		INNER JOIN temp_instances TempIns On VP.InstanceId = TempIns.InstanceId
		LEFT JOIN Vendor V ON V.Id = VP.VendorId
		--LEFT JOIN VendorReturnItem VRI ON VRI.AccountId = PS.AccountId AND VRI.InstanceId = PS.InstanceId AND VRI.ProductStockId = PS.Id
				
		WHERE VP.AccountId = @AccountId and (VP.Cancelstatus is NULL or VP.CancelStatus != 1) 
			AND ISNULL(VPI.[Status],0) != 2
			AND  Convert(date,VP.CreatedAt) BETWEEN @StartDate AND @EndDate

			AND 
			(
				ISNULL(VP.VendorId ,'') = isnull(@VendorId,ISNULL(VP.VendorId ,'') ) 
				OR ISNULL(V.Name, '') Like ISNULL(@VendorId, ISNULL(V.Name, '')) + '%'
			)
			AND ISNULL(VP.BillSeries,'') = 
			(
				CASE 
					WHEN ISNULL(VP.BillSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(VP.BillSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(VP.BillSeries,'')
				END
			)
			AND ISNULL(VP.GoodsRcvNo,'') = ISNULL(@billNo,ISNULL(VP.GoodsRcvNo,'')) 
			AND case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end BETWEEN Isnull(@from_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end) AND Isnull(@to_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end)

		GROUP BY Convert(date,VP.InvoiceDate),VP.Id
		) A GROUP BY InvoiceDate
		
		UNION ALL

		SELECT 
		InvoiceDate, 
		'' InstanceId, 
		'' InstanceName, 
		'' InvoiceNo, 
		'' Prefix, 
		'' VendorName,
		SUM(PurchaseDiscountAmount) PurchaseDiscountAmount,SUM(ReturnDiscountAmount) ReturnDiscountAmount,
		SUM(SellingPrice) SellingPrice,SUM(ReturnSellingPrice) ReturnSellingPrice,
		SUM(CostPriceWithoutTax) CostPriceWithoutTax,SUM(ReturnCostPriceWithoutTax) ReturnCostPriceWithoutTax,
		SUM(TaxAmount) TaxAmount,SUM(ReturnTaxAmount) ReturnTaxAmount,
		SUM(RoundOffNetAmount) AS RoundOffNetAmount,
		SUM(Round(Round(NetPurchasePrice,2),0)) NetPurchasePrice,
		--SUM(Round(Round(ReturnNetPurchasePrice,2),0)) AS ReturnNetPurchasePrice,
		SUM(ReturnNetPurchasePrice) AS ReturnNetPurchasePrice,
		0 ReturnFlag 
		,SUM(Credit) Credit, sum(Debit) Debit
		FROM
		(
		SELECT CONVERT(DATE,VR.ReturnDate) InvoiceDate, 
			'' InstanceId, 
			'' InstanceName,
			'' InvoiceNo,
			'' Prefix,
			'' VendorName		
							
			--, SUM(((ISNULL(VRI.Quantity,0) / isnull(PS.PackageSize,ISNULL(VPI.PackageSize,1))) - case when isnull(VRI.Quantity,0) > 0 then  ISNULL(VPI.FreeQty,0) else 0 end) * ISNULL(PS.PackagePurchasePrice,ISNULL(VPI.PackagePurchasePrice,0)) * ISNULL(VPI.Discount,0) / 100 ) PurchaseDiscountAmount	
			,0 AS PurchaseDiscountAmount
			--, SUM(((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0))) * ISNULL(VPI.Discount,0) / 100) / ISNULL(VPI.Quantity,1) * ISNULL(VRI.Quantity,0)) ReturnDiscountAmount
			, SUM(ISNULL(VRI.DiscountValue, 0)) AS ReturnDiscountAmount
			, 0 AS SellingPrice
			, SUM(CONVERT(decimal(18,6), ISNULL(PS.SellingPrice, isnull(VPI.PackageSellingPrice,0)/isnull(VPI.PackageSize,1))) * (isnull(VRI.Quantity,0))) As ReturnSellingPrice
			, 0 AS CostPriceWithoutTax
			--, SUM(ISNULL(PS.PackagePurchasePrice,ISNULL(VPI.PackagePurchasePrice,0)) * ((ISNULL(VRI.Quantity,0) / isnull(PS.PackageSize,ISNULL(VPI.PackageSize,1))) - case when isnull(VRI.Quantity,0) > 0 then  ISNULL(VPI.FreeQty,0) else 0 end)) as CostPriceWithoutTax	
			--,SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (1- ISNULL(VPI.Discount,0) / 100) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0)))/ISNULL(VPI.Quantity,1) * ISNULL(VRI.Quantity,0)) ReturnCostPriceWithoutTax
			--,SUM((ISNULL(VRI.quantity, 0))*((isNull(VRI.ReturnPurchasePrice, PS.PurchasePrice)) - (isNull(VRI.ReturnPurchasePrice, PS.PurchasePrice)) * (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END) / ((100 + (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			-- case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			--  ELSE PS.Vat END))))) ReturnCostPriceWithoutTax
			,SUM((ISNULL(VRI.Quantity,0) * ISNULL(VRI.ReturnPurchasePrice,0))-ISNULL(VRI.GstAmount, 0)) ReturnCostPriceWithoutTax
			, 0 AS TaxAmount
			--, SUM((ISNULL(VRI.quantity, 0))*(isNull(VRI.ReturnPurchasePrice, PS.PurchasePrice)) * (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END) / ((100 + (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			-- case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			--  ELSE PS.Vat END)))) as ReturnTaxAmount			
			, SUM(ISNULL(VRI.GstAmount, 0)) as ReturnTaxAmount	
			, 0 RoundOffNetAmount			
			--,SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * ((ISNULL(VRI.Quantity,0)/ISNULL(VPI.PackageSize,1)) - ISNULL(VPI.FreeQty,0)) * (1 - (ISNULL(VPI.Discount,0) / 100))) * (1 + (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN ISNULL(VRI.GstTotal, ISNULL(PS.Gsttotal,0)) ELSE PS.Vat END / 100))) NetPurchasePrice
			, 0 AS NetPurchasePrice
			, SUM(ISNULL(VRI.Quantity,0) * ISNULL(VRI.ReturnPurchasePrice,0)) ReturnNetPurchasePrice
			, 1 ReturnFlag
			,0 Credit, 0 Debit

		FROM VendorReturn VR WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = VR.InstanceId
		INNER JOIN VendorReturnItem VRI  WITH(NOLOCK) on VR.id= VRI.VendorReturnId
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=VRI.productstockid
		INNER JOIN temp_instances TempIns On VR.InstanceId = TempIns.InstanceId
		LEFT JOIN Vendor V ON V.Id = VR.VendorId
		LEFT JOIN VendorPurchase VP on VP.Id = VR.VendorPurchaseId
		LEFT JOIN VendorPurchaseItem VPI on VPI.ProductStockId = VRI.ProductStockId and VPI.InstanceId = VR.InstanceId
		and VPI.VendorPurchaseId = VP.Id
--		left JOIN VendorPurchaseItem vpi ON vpi.ProductStockId = ps.Id
--and vpi.VendorPurchaseId = vp.Id
					
		WHERE --VR.InstanceId = @InstanceIds AND 
			VR.AccountId = @AccountId 
			AND  Convert(date,VR.CreatedAt) BETWEEN @StartDate AND @EndDate
			and (VRI.IsDeleted is null or VRI.IsDeleted=0)  -- Condition added by Gavaskar 15-09-2017
			AND 
			(
				ISNULL(VP.VendorId ,'') = isnull(@VendorId,ISNULL(VP.VendorId ,'') ) 
				OR ISNULL(V.Name, '') Like ISNULL(@VendorId, ISNULL(V.Name, '')) + '%'
				or ISNULL(VR.VendorId,'') = ISNULL(@VendorId,ISNULL(VR.VendorId ,'') ) 
			)
			AND ISNULL(VP.BillSeries,'') = 
			(
				CASE 
					WHEN ISNULL(VP.BillSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(VP.BillSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(VP.BillSeries,'')
				END
			)
			AND ISNULL(VP.GoodsRcvNo,'') = ISNULL(@billNo,ISNULL(VP.GoodsRcvNo,'')) 
			AND case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end BETWEEN Isnull(@from_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end) AND Isnull(@to_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end)

		GROUP BY CONVERT(DATE,VR.ReturnDate)) A GROUP BY InvoiceDate

		) A
		GROUP BY Convert(date,invoicedate)
		ORDER BY Convert(date,invoicedate) DESC

	End
	Else If @Level = 2
	Begin
		;With temp_instances (InstanceId)
		As (
		select * from udf_Split(@InstanceIds) a
		)

		SELECT 
		InvoiceDate, 
		InstanceId, 
		InstanceName, 
		'' InvoiceNo, 
		'' Prefix, 
		'' VendorName,
		(SUM(PurchaseDiscountAmount)-SUM(ReturnDiscountAmount)) AS PurchaseDiscountAmount,
		(SUM(SellingPrice)-SUM(ReturnSellingPrice)) AS SellingPrice,
		(SUM(CostPriceWithoutTax)-SUM(ReturnCostPriceWithoutTax)) AS CostPriceWithoutTax,
		(SUM(TaxAmount)-SUM(ReturnTaxAmount)) AS TaxAmount,
		(SUM(RoundOffNetAmount)) AS RoundOffNetAmount,
		(SUM(NetPurchasePrice)-SUM(ReturnNetPurchasePrice)) AS NetPurchasePrice,
		0 ReturnFlag
		,SUM(Credit) Credit, sum(Debit) Debit
		FROM 
		(
		SELECT 
		InvoiceDate, 
		InstanceId, 
		InstanceName, 
		'' InvoiceNo, 
		'' Prefix, 
		'' VendorName,		
		SUM(PurchaseDiscountAmount) PurchaseDiscountAmount,SUM(ReturnDiscountAmount) AS ReturnDiscountAmount,
		SUM(SellingPrice) SellingPrice,SUM(ReturnSellingPrice) AS ReturnSellingPrice,
		SUM(CostPriceWithoutTax) CostPriceWithoutTax,SUM(ReturnCostPriceWithoutTax) AS ReturnCostPriceWithoutTax,
		SUM(TaxAmount) TaxAmount,SUM(ReturnTaxAmount) AS ReturnTaxAmount,
		SUM(RoundOffNetAmount) AS RoundOffNetAmount,
		SUM(Round(Round(NetPurchasePrice,2),0)) - SUM(Credit) + sum(Debit) NetPurchasePrice,SUM(Round(Round(ReturnNetPurchasePrice,2),0)) AS				ReturnNetPurchasePrice,
		0 ReturnFlag
		,SUM(Credit) Credit, sum(Debit) Debit
		FROM 
		(
		SELECT Convert(date,VP.InvoiceDate) InvoiceDate, I.Id InstanceId, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, --VP.InvoiceDate, 			
			'' InvoiceNo,
			'' Prefix,
			'' VendorName	
			,VP.Id VendorPurchaseId
			--, SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0) )) * ISNULL(VPI.Discount,0) / 100 ) PurchaseDiscountAmount	
			, SUM(ISNULL(VPI.DiscountValue, 0)) AS PurchaseDiscountAmount	
			, 0 AS ReturnDiscountAmount
			--, SUM(CONVERT(decimal(18,2), ISNULL(PS.SellingPrice, isnull(VPI.PackageSellingPrice,0)/isnull(VPI.PackageSize,1))) * (isnull(VPI.Quantity,0) )) As SellingPrice
			, SUM(CONVERT(decimal(18,6), isnull(VPI.PackageSellingPrice,0)/isnull(VPI.PackageSize,1)) * (isnull(VPI.Quantity,0) )) As SellingPrice
			, 0 ReturnSellingPrice						
			--, SUM(ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (1- ISNULL(VPI.Discount,0) / 100) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0) )) as CostPriceWithoutTax	
			, SUM((VPI.PurchasePrice*VPI.Quantity)-VPI.GstValue) as CostPriceWithoutTax	
			, 0 AS ReturnCostPriceWithoutTax		
			--, SUM((ISNULL(VPI.PackageQty, 0) - ISNULL(VPI.FreeQty,0) )*(isNull(VPI.PackagePurchasePrice, PS.PackagePurchasePrice)) * (1- ISNULL(VPI.Discount,0) / 100) * (CASE ISNULL(VP.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END) / (100 )) as TaxAmount	
			, SUM(VPI.GstValue) as TaxAmount	
			, 0 AS ReturnTaxAmount		
			, SUM(ISNULL(VP.RoundOffValue, 0)) RoundOffNetAmount			
			--, SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0) ) * (1 - (ISNULL(VPI.Discount,0) / 100))) * (1 + (CASE ISNULL(VP.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(VPI.GstTotal,0) != 0 then VPI.GstTotal else ISNULL(PS.Gsttotal,0) end
			-- ELSE PS.Vat END / 100))) - SUM(Case When NoteType='credit' Then IsNull(NoteAmount, 0) Else 0 End) + SUM(Case When NoteType='debit' Then IsNull(NoteAmount, 0) Else 0 End) NetPurchasePrice
			--, SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0) ) * (1 - (ISNULL(VPI.Discount,0) / 100))) * (1 + (CASE ISNULL(VP.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END / 100))) NetPurchasePrice
			, SUM(VPI.PurchasePrice*VPI.Quantity) AS NetPurchasePrice
			, 0 AS ReturnNetPurchasePrice
			, 0 ReturnFlag
			,max(Case When NoteType='credit' Then IsNull(NoteAmount, 0) Else 0 End) Credit
			,max(Case When NoteType='debit' Then IsNull(NoteAmount, 0) Else 0 End) Debit

		FROM VendorPurchase VP WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = VP.InstanceId
		INNER JOIN VendorPurchaseItem VPI  WITH(NOLOCK) on VP.id= VPI.VendorPurchaseId
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=VPI.productstockid
		INNER JOIN temp_instances TempIns On VP.InstanceId = TempIns.InstanceId
		LEFT JOIN Vendor V ON V.Id = VP.VendorId
		--LEFT JOIN VendorReturnItem VRI ON VRI.AccountId = PS.AccountId AND VRI.InstanceId = PS.InstanceId AND VRI.ProductStockId = PS.Id

		WHERE VP.AccountId = @AccountId and (VP.Cancelstatus is NULL or VP.CancelStatus != 1)
			AND ISNULL(VPI.[Status],0) != 2
			AND  Convert(date,VP.CreatedAt) BETWEEN @StartDate AND @EndDate
			and CONVERT(date,vp.InvoiceDate) = @InvoiceDate
			AND 
			(
				ISNULL(VP.VendorId ,'') = isnull(@VendorId,ISNULL(VP.VendorId ,'') ) 
				OR ISNULL(V.Name, '') Like ISNULL(@VendorId, ISNULL(V.Name, '')) + '%'
			)
			AND ISNULL(VP.BillSeries,'') = 
			(
				CASE 
					WHEN ISNULL(VP.BillSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(VP.BillSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(VP.BillSeries,'')
				END
			)
			AND ISNULL(VP.GoodsRcvNo,'') = ISNULL(@billNo,ISNULL(VP.GoodsRcvNo,'')) 
			AND case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end BETWEEN Isnull(@from_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end) AND Isnull(@to_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end)

		GROUP BY Convert(date,VP.InvoiceDate),VP.Id, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end --, VP.InvoiceDate
		) A group by InvoiceDate, InstanceId , InstanceName
		

		UNION ALL

		SELECT 
		InvoiceDate, 
		InstanceId, 
		InstanceName, 
		'' InvoiceNo, 
		'' Prefix, 
		'' VendorName,
		SUM(PurchaseDiscountAmount) PurchaseDiscountAmount,SUM(ReturnDiscountAmount) AS ReturnDiscountAmount,
		SUM(SellingPrice) SellingPrice,SUM(ReturnSellingPrice) AS ReturnSellingPrice,
		SUM(CostPriceWithoutTax) CostPriceWithoutTax,SUM(ReturnCostPriceWithoutTax) AS ReturnCostPriceWithoutTax,
		SUM(TaxAmount) TaxAmount,SUM(ReturnTaxAmount) AS ReturnTaxAmount,
		SUM(RoundOffNetAmount) AS RoundOffNetAmount,
		SUM(Round(Round(NetPurchasePrice,2),0)) NetPurchasePrice,
		--SUM(Round(Round(ReturnNetPurchasePrice,2),0)) AS				ReturnNetPurchasePrice,
		SUM(ReturnNetPurchasePrice) AS ReturnNetPurchasePrice,
		0 ReturnFlag
		,SUM(Credit) Credit, sum(Debit) Debit
		FROM 
		(
		SELECT CONVERT(DATE,VR.ReturnDate) InvoiceDate, I.Id InstanceId, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName,
			'' InvoiceNo,
			'' Prefix,
			'' VendorName		
							
			--, SUM(((ISNULL(VRI.Quantity,0) / isnull(PS.PackageSize,ISNULL(VPI.PackageSize,1))) - case when isnull(VRI.Quantity,0) > 0 then  ISNULL(VPI.FreeQty,0) else 0 end) * ISNULL(PS.PackagePurchasePrice,ISNULL(VPI.PackagePurchasePrice,0)) * ISNULL(VPI.Discount,0) / 100 ) PurchaseDiscountAmount	
			,0 AS PurchaseDiscountAmount
			--, SUM(((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0))) * ISNULL(VPI.Discount,0) / 100) / ISNULL(VPI.Quantity,1) * ISNULL(VRI.Quantity,0)) ReturnDiscountAmount
			, SUM(ISNULL(VRI.DiscountValue, 0)) ReturnDiscountAmount
			, 0 AS SellingPrice
			, SUM(CONVERT(decimal(18,6), ISNULL(PS.SellingPrice, isnull(VPI.PackageSellingPrice,0)/isnull(VPI.PackageSize,1))) * (isnull(VRI.Quantity,0))) As ReturnSellingPrice
			, 0 AS CostPriceWithoutTax
			--, SUM(ISNULL(PS.PackagePurchasePrice,ISNULL(VPI.PackagePurchasePrice,0)) * ((ISNULL(VRI.Quantity,0) / isnull(PS.PackageSize,ISNULL(VPI.PackageSize,1))) - case when isnull(VRI.Quantity,0) > 0 then  ISNULL(VPI.FreeQty,0) else 0 end)) as CostPriceWithoutTax	
			--,SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (1- ISNULL(VPI.Discount,0) / 100) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0)))/ISNULL(VPI.Quantity,1) * ISNULL(VRI.Quantity,0)) ReturnCostPriceWithoutTax
			--,SUM((ISNULL(VRI.quantity, 0))*((isNull(VRI.ReturnPurchasePrice, PS.PurchasePrice)) - (isNull(VRI.ReturnPurchasePrice, PS.PurchasePrice)) * (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END) / ((100 + (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			-- case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			--  ELSE PS.Vat END))))) ReturnCostPriceWithoutTax
			,SUM((ISNULL(VRI.Quantity,0) * ISNULL(VRI.ReturnPurchasePrice,0))-ISNULL(VRI.GstAmount, 0)) ReturnCostPriceWithoutTax
			, 0 AS TaxAmount
			--, SUM((ISNULL(VRI.quantity, 0))*(isNull(VRI.ReturnPurchasePrice, PS.PurchasePrice)) * (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END) / ((100 + (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			-- case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			--  ELSE PS.Vat END)))) as ReturnTaxAmount	
			, SUM(ISNULL(VRI.GstAmount, 0)) as ReturnTaxAmount		
			, 0 RoundOffNetAmount			
			--,SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * ((ISNULL(VRI.Quantity,0)/ISNULL(VPI.PackageSize,1)) - ISNULL(VPI.FreeQty,0)) * (1 - (ISNULL(VPI.Discount,0) / 100))) * (1 + (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN ISNULL(VRI.GstTotal, ISNULL(PS.Gsttotal,0)) ELSE PS.Vat END / 100))) NetPurchasePrice
			, 0 AS NetPurchasePrice
			, SUM(ISNULL(VRI.Quantity,0) * ISNULL(VRI.ReturnPurchasePrice,0)) ReturnNetPurchasePrice
			, 1 ReturnFlag
			,0 Credit, 0 Debit

		FROM VendorReturn VR WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = VR.InstanceId
		INNER JOIN VendorReturnItem VRI  WITH(NOLOCK) on VR.id= VRI.VendorReturnId
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=VRI.productstockid
		INNER JOIN temp_instances TempIns On VR.InstanceId = TempIns.InstanceId
		LEFT JOIN Vendor V ON V.Id = VR.VendorId
		LEFT JOIN VendorPurchase VP on VP.Id = VR.VendorPurchaseId
		LEFT JOIN VendorPurchaseItem VPI on VPI.ProductStockId = VRI.ProductStockId and VPI.InstanceId = VR.InstanceId
		and VPI.VendorPurchaseId = VP.Id
		--left JOIN VendorPurchaseItem vpi ON vpi.ProductStockId = ps.Id
--and vpi.VendorPurchaseId = vp.Id
					
		WHERE --VR.InstanceId = @InstanceIds AND 
			VR.AccountId = @AccountId and (VRI.IsDeleted is null or VRI.IsDeleted=0)  -- Condition added by Gavaskar 15-09-2017
			AND  Convert(date,VR.CreatedAt) BETWEEN @StartDate AND @EndDate
			and CONVERT(date,VR.ReturnDate) = @InvoiceDate
			
			AND 
			(
				ISNULL(VP.VendorId ,'') = isnull(@VendorId,ISNULL(VP.VendorId ,'') ) 
				OR ISNULL(V.Name, '') Like ISNULL(@VendorId, ISNULL(V.Name, '')) + '%'
				or ISNULL(VR.VendorId,'') = ISNULL(@VendorId,ISNULL(VR.VendorId ,'') ) 
			)
			AND ISNULL(VP.BillSeries,'') = 
			(
				CASE 
					WHEN ISNULL(VP.BillSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(VP.BillSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(VP.BillSeries,'')
				END
			)
			AND ISNULL(VP.GoodsRcvNo,'') = ISNULL(@billNo,ISNULL(VP.GoodsRcvNo,'')) 
			AND case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end BETWEEN Isnull(@from_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end) AND Isnull(@to_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end)

		GROUP BY CONVERT(DATE,VR.ReturnDate), I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end ) A group by InvoiceDate, InstanceId , InstanceName

		) A
		GROUP BY Convert(date,invoicedate), InstanceId, InstanceName
		ORDER BY Convert(date,invoicedate) DESC, InstanceName ASC
	End
		
	Else If @Level = 3
	Begin

	select InvoiceDate,InstanceId,InstanceName,InvoiceNo,Prefix,VendorName,PurchaseDiscountAmount,SellingPrice,CostPriceWithoutTax, TaxAmount,RoundOffNetAmount,NetPurchasePrice - Credit + Debit as NetPurchasePrice,ReturnFlag, Credit ,Debit  from
	(
		SELECT convert(date,VP.InvoiceDate) InvoiceDate, I.Id InstanceId, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, VP.GoodsRcvNo AS InvoiceNo
		, LTRIM(ISNULL(VP.Prefix,'')+ISNULL(VP.BillSeries,'')) as Prefix, V.Name VendorName			
							
			--, SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0))) * ISNULL(VPI.Discount,0) / 100 ) PurchaseDiscountAmount	
			, SUM(ISNULL(VPI.DiscountValue, 0)) AS PurchaseDiscountAmount	

			--, SUM(CONVERT(decimal(18,2), ISNULL(PS.SellingPrice, isnull(VPI.PackageSellingPrice,0)/isnull(VPI.PackageSize,1))) * (isnull(VPI.Quantity,0))) As SellingPrice
			, SUM(CONVERT(decimal(18,6), isnull(VPI.PackageSellingPrice,0)/isnull(VPI.PackageSize,1)) * (isnull(VPI.Quantity,0))) As SellingPrice

			--, SUM(ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (1- ISNULL(VPI.Discount,0) / 100) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0))) as CostPriceWithoutTax		
			, SUM((VPI.PurchasePrice*VPI.Quantity)-VPI.GstValue) as CostPriceWithoutTax		
				
			--, SUM((ISNULL(VPI.PackageQty, 0) - ISNULL(VPI.FreeQty,0))*(isNull(VPI.PackagePurchasePrice, PS.PackagePurchasePrice)) * (1- ISNULL(VPI.Discount,0) / 100) * (CASE ISNULL(VP.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END) / (100 )) as TaxAmount
			, SUM(VPI.GstValue) as TaxAmount
						
			, SUM(ISNULL(VP.RoundOffValue, 0)) RoundOffNetAmount			
			--, SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0)) * (1 - (ISNULL(VPI.Discount,0) / 100))) * (1 + (CASE ISNULL(VP.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(VPI.GstTotal,0) != 0 then VPI.GstTotal else ISNULL(PS.Gsttotal,0) end
			-- ELSE PS.Vat END / 100))) - SUM(Case When NoteType='credit' Then IsNull(NoteAmount, 0) Else 0 End) + SUM(Case When NoteType='debit' Then IsNull(NoteAmount, 0) Else 0 End) NetPurchasePrice
			--, SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0)) * (1 - (ISNULL(VPI.Discount,0) / 100))) * (1 + (CASE ISNULL(VP.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END / 100))) NetPurchasePrice
			, SUM(VPI.PurchasePrice*VPI.Quantity) AS NetPurchasePrice
			, 0 ReturnFlag
			, MAX(case when NoteType='credit' Then IsNull(NoteAmount, 0) Else 0 End) Credit
			, MAX(Case When NoteType='debit' Then IsNull(NoteAmount, 0) Else 0 End) Debit
		FROM VendorPurchase VP WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = VP.InstanceId
		INNER JOIN VendorPurchaseItem VPI  WITH(NOLOCK) on VP.id= VPI.VendorPurchaseId
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=VPI.productstockid
		LEFT JOIN Vendor V ON V.Id = VP.VendorId
					
		WHERE VP.InstanceId = @InstanceIds
			AND VP.AccountId = @AccountId and (VP.Cancelstatus is NULL or VP.CancelStatus != 1)
			AND ISNULL(VPI.[Status],0) != 2
			AND  Convert(date,VP.CreatedAt) BETWEEN @StartDate AND @EndDate
			and CONVERT(date,vp.InvoiceDate) = @InvoiceDate
			
			AND 
			(
				ISNULL(VP.VendorId ,'') = isnull(@VendorId,ISNULL(VP.VendorId ,'') ) 
				OR ISNULL(V.Name, '') Like ISNULL(@VendorId, ISNULL(V.Name, '')) + '%'
			)
			AND ISNULL(VP.BillSeries,'') = 
			(
				CASE 
					WHEN ISNULL(VP.BillSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(VP.BillSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(VP.BillSeries,'')
				END
			)
			AND ISNULL(VP.GoodsRcvNo,'') = ISNULL(@billNo,ISNULL(VP.GoodsRcvNo,'')) 
			AND case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end BETWEEN Isnull(@from_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end) AND Isnull(@to_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end)

		GROUP BY CONVERT(DATE,VP.InvoiceDate), I.Id,case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end , VP.GoodsRcvNo, LTRIM(ISNULL(VP.Prefix,'')+ISNULL(VP.BillSeries,''))
			, V.Name, ISNULL(VP.RoundOffValue, 0)
		) A
		UNION ALL

		SELECT CONVERT(DATE,VR.ReturnDate) InvoiceDate, I.Id InstanceId, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, VR.ReturnNo
		, LTRIM(ISNULL(VR.prefix,'')) as Prefix, V.Name VendorName			
							
			--, SUM(((ISNULL(VRI.Quantity,0) / isnull(PS.PackageSize,ISNULL(VPI.PackageSize,1))) - case when isnull(VRI.Quantity,0) > 0 then  ISNULL(VPI.FreeQty,0) else 0 end) * ISNULL(PS.PackagePurchasePrice,ISNULL(VPI.PackagePurchasePrice,0)) * ISNULL(VPI.Discount,0) / 100 ) PurchaseDiscountAmount	

			--, SUM(((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0))) * ISNULL(VPI.Discount,0) / 100) / ISNULL(VPI.Quantity,1) * ISNULL(VRI.Quantity,0)) PurchaseDiscountAmount
			, SUM(ISNULL(VRI.DiscountValue, 0)) AS PurchaseDiscountAmount

			, SUM(CONVERT(decimal(18,6), ISNULL(PS.SellingPrice, isnull(VPI.PackageSellingPrice,0)/isnull(VPI.PackageSize,1))) * (isnull(VRI.Quantity,0))) As SellingPrice

			--, SUM(ISNULL(PS.PackagePurchasePrice,ISNULL(VPI.PackagePurchasePrice,0)) * ((ISNULL(VRI.Quantity,0) / isnull(PS.PackageSize,ISNULL(VPI.PackageSize,1))) - case when isnull(VRI.Quantity,0) > 0 then  ISNULL(VPI.FreeQty,0) else 0 end)) as CostPriceWithoutTax	
			--,SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * (1- ISNULL(VPI.Discount,0) / 100) * (ISNULL(VPI.PackageQty,0) - ISNULL(VPI.FreeQty,0)))/ISNULL(VPI.Quantity,1) * ISNULL(VRI.Quantity,0)) CostPriceWithoutTax
			--	,SUM((ISNULL(VRI.quantity, 0))*((isNull(VRI.ReturnPurchasePrice, PS.PurchasePrice)) - (isNull(VRI.ReturnPurchasePrice, PS.PurchasePrice)) * (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END) / ((100 + (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			-- case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			--  ELSE PS.Vat END))))) CostPriceWithoutTax
			,SUM((ISNULL(VRI.Quantity,0) * ISNULL(VRI.ReturnPurchasePrice,0))-ISNULL(VRI.GstAmount, 0)) AS CostPriceWithoutTax
			--, SUM((ISNULL(VRI.quantity, 0))*(isNull(VRI.ReturnPurchasePrice, PS.PurchasePrice)) * (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			--case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			-- ELSE PS.Vat END) / ((100 + (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN 
			-- case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end
			--  ELSE PS.Vat END)))) as TaxAmount	
			, SUM(ISNULL(VRI.GstAmount, 0)) AS TaxAmount			  	
			, 0 RoundOffNetAmount			
			--,SUM((ISNULL(VPI.PackagePurchasePrice,ISNULL(PS.PackagePurchasePrice,0)) * ((ISNULL(VRI.Quantity,0)/ISNULL(VPI.PackageSize,1)) - ISNULL(VPI.FreeQty,0)) * (1 - (ISNULL(VPI.Discount,0) / 100))) * (1 + (CASE ISNULL(VR.TaxRefNo,0) WHEN 1 THEN ISNULL(VRI.GstTotal, ISNULL(PS.Gsttotal,0)) ELSE PS.Vat END / 100))) NetPurchasePrice
			,SUM(ISNULL(VRI.Quantity,0) * ISNULL(VRI.ReturnPurchasePrice,0)) NetPurchasePrice
			, 1 ReturnFlag
			,0 Credit ,0 Debit 

		FROM VendorReturn VR WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = VR.InstanceId
		INNER JOIN VendorReturnItem VRI  WITH(NOLOCK) on VR.id= VRI.VendorReturnId
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=VRI.productstockid
		LEFT JOIN Vendor V ON V.Id = VR.VendorId
		LEFT JOIN VendorPurchase VP on VP.Id = VR.VendorPurchaseId
		LEFT JOIN VendorPurchaseItem VPI on VPI.ProductStockId = VRI.ProductStockId and VPI.InstanceId = VR.InstanceId
		and VPI.VendorPurchaseId = VP.Id
		--left JOIN VendorPurchaseItem vpi ON vpi.ProductStockId = ps.Id
--and vpi.VendorPurchaseId = vp.Id
					
		WHERE VR.InstanceId = @InstanceIds
			AND VR.AccountId = @AccountId and (VRI.IsDeleted is null or VRI.IsDeleted=0)  -- Condition added by Gavaskar 15-09-2017
			AND  Convert(date,VR.CreatedAt) BETWEEN @StartDate AND @EndDate
			and CONVERT(date,VR.ReturnDate) = @InvoiceDate
			
			AND 
			(
				ISNULL(VP.VendorId ,'') = isnull(@VendorId,ISNULL(VP.VendorId ,'') ) 
				OR ISNULL(V.Name, '') Like ISNULL(@VendorId, ISNULL(V.Name, '')) + '%'
				or ISNULL(VR.VendorId,'') = ISNULL(@VendorId,ISNULL(VR.VendorId ,'') ) 
			)
			AND ISNULL(VP.BillSeries,'') = 
			(
				CASE 
					WHEN ISNULL(VP.BillSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(VP.BillSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(VP.BillSeries,'')
				END
			)
			AND ISNULL(VP.GoodsRcvNo,'') = ISNULL(@billNo,ISNULL(VP.GoodsRcvNo,'')) 
			AND case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end BETWEEN Isnull(@from_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end) AND Isnull(@to_gstTotal, case when ISNULL(PS.GstTotal,0) != 0 then PS.GstTotal else ISNULL(VPI.Gsttotal,0) end)

		GROUP BY CONVERT(DATE,VR.ReturnDate), I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end , VR.ReturnNo, LTRIM(ISNULL(VR.prefix,'')) 
			, V.Name

		--ORDER BY CONVERT(DATE,VP.InvoiceDate) desc
		ORDER BY InvoiceNo desc

	End
 END

