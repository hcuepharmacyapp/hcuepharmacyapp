
 
  /*************************************
******************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**28/08/17     Poongodi R	   SP Created 
 **28/08/17   nandhini			UOM field added
 **30/08/17   nandhini			productnanme field added
*******************************************************************************/


 Create Proc dbo.Usp_Get_HSN(@AccountId char(36))
 As
 Begin
 Select Code, name [ProductName],isnull(hsncode,'') [HSN] ,'' [HSNDesc], 'Nos' [UOM],isnull(GstTotal,0) [GSTTotal] from Product where accountid =@AccountId and isnull(status ,0) in (0,1)
 order by name asc
 End