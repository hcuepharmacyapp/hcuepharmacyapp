 

/*                              
******************************************************************************                            
** File: [Usp_vat_po_annx8_rpt]  
** Name: [Usp_vat_po_annx8_rpt]                              
** Description: To Get Puchase and Return  details  
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author:Poongodi R  
** Created Date: 09/02/2017  
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 30/03/2017  Poongodi			Invoice date changed to created at 
** 29/04/2017  Sabarish			VendorPO Item status validation added (block the deleted item) 
** 30/06/2017  Poongodi			TaxRefNovalidation added
*******************************************************************************/ 
CREATE PROC dbo.Usp_vat_po_annx8_rpt(@AccountId  CHAR(36), 
                                           @InstanceId CHAR(36), 
                                           @StartDate  DATETIME, 
                                           @EndDate    DATETIME) 
AS 
  BEGIN 
 /*
  SELECT VendorPurchaseItem.ProductStockId [ProductStockId],VendorPurchaseItem.Quantity [Quantity],VendorPurchaseItem.PurchasePrice [PurchasePrice],
VendorPurchaseItem.PackagePurchasePrice [PackagePurchasePrice],VendorPurchaseItem.PackageQty [PackageQty],
isnull(VendorPurchaseItem.PackageQty ,0)  - isnull(VendorPurchaseItem.FreeQty,0) [POQty],
VendorPurchaseItem.PackageSellingPrice [PackageSellingPrice],
VendorPurchaseItem.FreeQty [FreeQty],VendorPurchaseItem.Discount [Discount],VendorPurchase.Id AS [VendorPurchaseId],
VendorPurchase.InvoiceNo AS [InvoiceNo] ,VendorPurchase.InvoiceDate AS [InvoiceDate],
VendorPurchase.Discount AS [VendorPODiscount]
,Vendor.Id AS [Vendor.Id], isnull(Vendor.Name ,'')AS [VendorName],
Vendor.TinNo AS [VendorTinNo],isnull(Vendor.Address ,'') AS [VendorAddress],
isnull(Vendor.Area ,'') AS [VendorArea],ProductStock.SellingPrice AS [SellingPrice],
ProductStock.CST AS [CST],--Product.CommodityCode 
'2004'  AS [CommodityCode] FROM VendorPurchaseItem
 INNER JOIN VendorPurchase ON VendorPurchase.Id = VendorPurchaseItem.VendorPurchaseId 
 INNER JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId 
 INNER JOIN ProductStock ON ProductStock.Id = VendorPurchaseItem.ProductStockId 
 INNER JOIN Product ON Product.Id = ProductStock.ProductId  
 WHERE VendorPurchase.InvoiceDate  Between  @StartDate AND @EndDate AND VendorPurchase.AccountId  =  @AccountId 
 AND VendorPurchase.InstanceId  =  @InstanceId 
 ORDER BY VendorPurchase.CreatedAt desc
 */
  declare @GstDate date ='2017-06-30'
  SELECT isnull(Vendor.Name ,'')AS [VendorName],
Vendor.TinNo AS [VendorTinNo],max(isnull(Vendor.Address ,'')) AS [VendorAddress],
max(isnull(Vendor.Area ,'')) AS [VendorArea],
             CASE Isnull([productstock.CST], 0) 
               WHEN 5 THEN '2044' 
               WHEN 14.5 THEN '301' 
               ELSE '752' 
             END                                          [Commoditycode], 
			 sum(isnull(Quantity,0)) [Quantity],
             [vendorpurchase.invoiceno]                   [InvoiceNo], 
             [vendorpurchase.invoicedate]                 [InvoiceDate], 
             Sum(Isnull([vendorpurchaseitem.povalue], 0) + ( 
                 Isnull([vendorpurchaseitem.povalue], 0) * 
                 Isnull([productstock.CST], 0) / 
                 100 
                   ))                                     InvoiceValue, 
             Sum(Isnull([vendorpurchaseitem.povalue], 0)) POValue, 
             [productstock.CST]                           [CST], 
             Round(Sum(Isnull([vendorpurchaseitem.povalue], 0)) * 
                   Isnull([productstock.CST], 0) / 
                   100, 2)                                [CSTValue], 
             'R'                                          [Category]  ,
			 max(isnull(Vendor.State ,''))[VendorState],
			 max(GRNNo) GRNNo,
			  Max(PODate)			     [PoDate],
			 'C Form' [FormType],
			  'For Resale' [SalePurpose],
			  '' [Transport],
			  '' [AggreementOrderDt],
			   '' [AggreementOrderNo],
			   'Nos' [UOM],
			   sum(Isnull([vendorpurchaseitem.povalue], 0) +  (Isnull([vendorpurchaseitem.povalue], 0)  * 
                   Isnull([productstock.CST], 0) / 
                   100 ) )  [TotalPO]
      FROM   (SELECT vendorpurchaseitem.productstockid, 
                     vendorpurchaseitem.quantity, 
                     vendorpurchaseitem.purchaseprice, 
                     vendorpurchaseitem.packagepurchaseprice, 
                     vendorpurchaseitem.packageqty, 
                     vendorpurchaseitem.packagesellingprice, 
                     vendorpurchaseitem.freeqty, 
                     vendorpurchaseitem.discount, 
					 VendorPurchase.GoodsRcvNo [GRNNo],
					 cast(VendorPurchase.createdat as date)[PODate],
                     vendorpurchase.id          AS [VendorPurchase.Id], 
                     vendorpurchase.invoiceno   AS [VendorPurchase.InvoiceNo], 
                     vendorpurchase.invoicedate AS [VendorPurchase.InvoiceDate], 
                     vendorpurchase.discount    AS [VendorPurchase.Discount], 
                     vendorpurchase.VendorId                  AS [VendorId], 
                  
                     productstock.sellingprice  AS [ProductStock.SellingPrice], 
                     productstock.CST           AS [ProductStock.CST], 
                     Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * ( 
                     Isnull 
                     ( 
                     vendorpurchaseitem.packageqty, 0) 
                     - 
                                          Isnull 
                     ( 
                     vendorpurchaseitem.freeqty, 0) ) - ( 
                     Isnull( 
                     vendorpurchaseitem.packagepurchaseprice, 0) * 
     ( 
                     Isnull 
                     ( 
                     vendorpurchaseitem.packageqty, 0) 
                     - 
                                          Isnull 
                     ( 
                     vendorpurchaseitem.freeqty, 0) )  * 
      Isnull 
      ( 
             vendorpurchaseitem.discount, 0) / 
      100 ) 
      [VendorPurchaseItem.POValue] 
      FROM   vendorpurchaseitem 
      INNER JOIN vendorpurchase 
      ON vendorpurchase.id = vendorpurchaseitem.vendorpurchaseid 
      and (vendorpurchaseitem.Status is null or vendorpurchaseitem.Status = 1)
      INNER JOIN vendor 
      ON vendor.id = vendorpurchase.vendorid 
      INNER JOIN productstock 
      ON productstock.id = vendorpurchaseitem.productstockid 
      INNER JOIN product 
      ON product.id = productstock.productid 
      WHERE  cast(vendorpurchase.CreatedAt as date) BETWEEN @StartDate AND @EndDate 
	   and CONVERT(DATE, vendorpurchase.createdat) <=@GstDate
      AND vendorpurchase.accountid = @AccountId 
      AND vendorpurchase.instanceid = @InstanceId 
      and isnull(EnableCST ,0) =1 
      and isnull(vendorpurchase.TaxRefNo,0) =0
       
     )AS one left join Vendor  on Vendor.id = VendorId
      GROUP  BY vendor.name, 
                vendor.tinno, 
                [vendorpurchase.invoiceno], 
                [vendorpurchase.invoicedate], 
                [productstock.CST] 
      ORDER  BY [vendorpurchase.invoicedate],
				 vendor.name 
 end
 

 