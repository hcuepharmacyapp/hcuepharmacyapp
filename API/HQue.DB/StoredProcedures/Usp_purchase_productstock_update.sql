/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 26/08/2017 Poongodi R	   Created
** 07/09/2017 Poongodi R	   Package purchase price length increased
*******************************************************************************/ 
Create PROC dbo.Usp_purchase_productstock_update(
@Id char(36),
@AccountId char(36),
@InstanceId char(36),
@ProductId char(36),
@VendorId char(36),
@BatchNo varchar(100),
@ExpireDate date,
@VAT decimal(5,2),
@TaxType varchar(10),
@SellingPrice decimal(18,6),
@MRP decimal(18,6),
@PurchaseBarcode varchar(100),
@Stock decimal(9,2),
@PackageSize decimal(9,2),
@PackagePurchasePrice decimal(18,6),
@PurchasePrice decimal(18,6),
@OfflineStatus bit,
@UpdatedAt datetime,
@UpdatedBy char(36),
@CST decimal(9,2),
@Eancode varchar(50),
@HsnCode varchar(50),
@Igst decimal(9,2),
@Cgst decimal(9,2),
@Sgst decimal(9,2),
@GstTotal decimal(9,2) ) 
AS 
  BEGIN 
 SET DEADLOCK_PRIORITY LOW
      UPDATE productstock 
      SET    productid = @ProductId,  
             vendorid = @VendorId, 
             batchno = @BatchNo, 
             expiredate = @ExpireDate, 
             vat = @VAT, 
             taxtype = @TaxType, 
             sellingprice = @SellingPrice, 
             mrp = @MRP, 
             purchasebarcode = @PurchaseBarcode, 
             stock = @Stock, 
             packagesize = @PackageSize, 
             packagepurchaseprice = @PackagePurchasePrice, 
             purchaseprice = @PurchasePrice, 
             offlinestatus = @OfflineStatus, 
             updatedat = @UpdatedAt, 
             updatedby = @UpdatedBy, 
             cst = @CST,  
             eancode = @Eancode, 
             hsncode = @HsnCode, 
             igst = @Igst, 
             cgst = @Cgst, 
             sgst = @Sgst, 
             gsttotal = @GstTotal 
      WHERE  productstock.id = @Id 

 
 select 'success'
  END 