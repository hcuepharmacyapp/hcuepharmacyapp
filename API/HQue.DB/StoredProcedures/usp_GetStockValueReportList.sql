 

 --Usage : -- usp_GetStockValueReportList '013513b1-ea8c-4ea8-9fed-054b260ee197','18204879-99ff-4efd-b076-f85b4a0da0a3'
  

 CREATE PROCEDURE [dbo].[usp_GetStockValueReportList](@InstanceId varchar(36),@AccountId varchar(36))
 AS
 BEGIN
 SET NOCOUNT ON  

select ReportNo,CONVERT(char(10),CreatedAt,126) as CreatedAt from 
StockValueReport
where AccountId = @AccountId and InstanceId=@InstanceId
group by ReportNo ,CONVERT(char(10),CreatedAt,126)
order by ReportNo desc,CONVERT(char(10),CreatedAt,126) desc

END
 