/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
16/12/2017	  Poongodi		Created
*******************************************************************************/ 
 
Create Proc usp_Sales_Productstock_Updates(@InstanceId char(36), @Accountid char(36), @Id char(36),
									@UpdatedAt datetime, @UpdatedBy char(36), @Stock decimal (18,2), @TransactionId varchar(36) ,@Quantity decimal(18,2) = 0 
)
as
begin
SET DEADLOCK_PRIORITY LOW
 
Update ProductStock Set Stock = @Stock, UpdatedAt = @UpdatedAt, UpdatedBy = @UpdatedBy, TransactionId = @TransactionId where AccountId = @AccountId And InstanceId = @InstanceId And  id = @Id
 
  
  select 'success' 
 
 end