﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************   
** 18/09/2017  CharuBala	Original Author of this SP   
** 28/09/2017  Poongodi		Createddate added in where condition
** 03/10/2017  Poongodi		Credit and cash payment taken
** 05/10/2017  Senthil	    Added the offline check
** 02/11/2017  Sarubala	    Multiple payment included
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[usp_GetSalesValueforAccount](
@AccountId VARCHAR(36),
@InstanceId VARCHAR(36),
@fromdate datetime, 
@todate datetime, 
@UserId VARCHAR(36),
@offlineStatus bit )
AS
BEGIN
	SET NOCOUNT ON
   
	select 
	isnull(sum(A.SaleValue) - sum(A.ReturnValue),0) Cash, 
	sum(A.SaleValue) SaleValue, 
	sum(A.ReturnValue) ReturnValue 
	from (
		select 
		s.id, 
		--round(Convert(decimal(18,6),(sum(((isnull(si.SellingPrice,ps.SellingPrice) ) * si.Quantity - ((isnull(si.SellingPrice,ps.SellingPrice) ) * si.Quantity * isnull(s.Discount ,0) / 100)) )-sum( (si.Quantity * (isnull(si.SellingPrice,ps.SellingPrice) ) * isnull(si.Discount,0) / 100)  )) -(max(isnull(cp.Credit,0)))+ max(isnull(RoundoffNetAmount,0))) ,0)   SaleValue , 
		s.SaleAmount - max(isnull(cp.Credit,0)) AS SaleValue , 
		0 ReturnValue	
		from Sales S WITH(NOLOCK) join SalesItem si WITH(NOLOCK) on s.Id=si.SalesId
		join ProductStock ps WITH(NOLOCK) on ps.Id=si.ProductStockId
		left join (select salesid, sum(isnull(credit,0)) credit from customerpayment WITH(NOLOCK) where AccountId=@AccountId AND instanceid =@instanceid group by salesid) cp on cp.salesid = s.id 
		where S.AccountId=@AccountId and S.InstanceId=@InstanceId 
		and convert(datetime,S.createdat) between (@fromdate) and (@todate)
		and (s.CreatedBy=@UserId OR S.CreatedBy in(Select id from HQueUser where accountId=@AccountId and InstanceId=@InstanceId and UserType=6))
		and s.PaymentType in('cash','Credit','Multiple')
		and s.offlineStatus = @offlineStatus
		and ISNULL(s.Cancelstatus,0) = 0
		group by s.Id,s.SaleAmount

		union all

		select 
		sr.id, 
		0 SaleValue, 
		--round(Convert(decimal(18,6),sum((ISNULL(sri.MrpSellingPrice,ps.SellingPrice) * sri.Quantity) - (ISNULL(sri.MrpSellingPrice,ps.SellingPrice) * sri.Quantity * ISNULL((isnull(sri.Discount,0)+isnull(s.Discount,0)),0)/100))) ,0) ReturnValue 
		SR.NetAmount AS ReturnValue 
		from SalesReturn sr WITH(NOLOCK) join SalesReturnItem sri WITH(NOLOCK) on sr.Id=sri.SalesReturnId
		join ProductStock ps WITH(NOLOCK) on sri.ProductStockId=ps.Id
		left join sales s WITH(NOLOCK) on sr.salesid=s.id
		where Sr.AccountId=@AccountId and Sr.InstanceId=@InstanceId and convert(datetime,Sr.createdat) between (@fromdate) and (@todate)
		and sr.CreatedBy=@UserId
		and ISNULL(sri.IsDeleted,0) != 1 and ISNULL(sr.CancelType,0) != 1
		and sr.offlineStatus = @offlineStatus
		group by sr.id,SR.NetAmount

	) A

	SET NOCOUNT OFF
END