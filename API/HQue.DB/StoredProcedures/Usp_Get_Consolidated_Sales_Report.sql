/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 **30/05/2017  Poongodi			Other than 5 and 13.1 Vat Details Taken 
 **22/06/2017   Violet			Prefix Added 
 **21/07/2017   Manivannan		CashType Filter Added 
 **14/09/2017   nandhini		branch name Added 
 **14/09/2017   Sarubala 		isnull added in Gsttotal 
*******************************************************************************/ 
-- [Usp_Get_Consolidated_Sales_Report] '852eaefa-f670-4a6a-a0e7-db3fa252d576','cca95924-b9a4-45fd-9fda-9b64a714a67d','SH',Null,'15-Jul-2017','23-Jul-2017'
CREATE PROCEDURE [dbo].[Usp_Get_Consolidated_Sales_Report](@AccountId  CHAR(36), 
@InstanceId CHAR(36),
@SearchOption varchar(50), 
@CashType varchar(20), 
@StartDate  DATETIME, 
@EndDate    DATETIME) 
AS 
BEGIN 
	declare @InvoiceSeries varchar(100) = ''
	select  @InvoiceSeries = isnull(@SearchOption,'') 

	select @CashType = isNull(@CashType, '')

	select  
	Branch,
	InvoiceDate,
	invoiceNo,
	Name,
	RoundoffSaleAmount,
	SaleAmount,
	SUM(Gross52Amt) as Gross52Amt,
	SUM(vat52Amount) as vat52Amount,
	SUM(Gross131Amt) as Gross131Amt,
	SUM(vat131Amount) as vat131Amount,
	SUM(isnull(OtherVATAmt,0)) as OtherVATAmt ,
	SUM(isnull(OtherGrossAmt,0)) as OtherGrossAmt 
	from 
	(
		select 
		invoiceNo,
		InvoiceDate,
		Name,
		GstTotal,
		Branch,
		RoundoffSaleAmount,
		SaleAmount,
		case isnull(GstTotal,0) when 5.2 then SUM(isnull(VatAmount,0)) else 0 end as vat52Amount,
		case isnull(GstTotal,0) when 5.2 then sum(isnull(GrossAmount,0)) else 0 end as Gross52Amt,
		case  isnull(GstTotal,0) when 13.1 then SUM(isnull(VatAmount,0)) else 0 end  as vat131Amount, 
		case isnull(GstTotal,0) when 13.1 then sum(isnull(GrossAmount,0)) else 0 end as Gross131Amt  ,

		case  when isnull(GstTotal,0) not in (5.2, 13.1 ) then sum(isnull(VatAmount,0)) else 0 end as OtherVATAmt  ,
		case  when isnull(GstTotal,0) not in (5.2, 13.1 ) then sum(isnull(GrossAmount,0)) else 0 end as OtherGrossAmt  from 
		(
			select s.InvoiceDate,
			ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, ''))+' '+ s.invoiceno AS invoiceNo, 
			ISNULL(s.Name,'') AS Name,
			s.RoundoffSaleAmount AS RoundoffSaleAmount,
			s.SaleAmount AS SaleAmount,
			ins.Name AS Branch,
			ISNULL(si.GstTotal,0) AS GstTotal,
			si.TotalAmount,
			si.TotalAmount-d.TaxAmount AS GrossAmount,
			d.TaxAmount AS VatAmount
			from Sales as s (NOLOCK)
			Inner join (Select * from instance(NOLOCK) where accountid= @AccountId ) Ins on Ins.id = s.InstanceId
			inner join SalesItem as si(NOLOCK) on s.Id = si.SalesId 
			CROSS APPLY (SELECT CASE WHEN ISNULL(s.TaxRefNo,0) = 1 THEN ISNULL(si.GstAmount,0) ELSE ISNULL(si.VatAmount,0) END AS TaxAmount) AS d
			where s.AccountId = @AccountId AND s.InstanceId = ISNULL(@InstanceId,s.InstanceId) AND s.InvoiceDate between @StartDate and @EndDate  
			AND isnull(s.InvoiceSeries,'')+isnull(s.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%' 
			AND isnull(s.CashType,'') like isnull(@CashType,'')+'%'
			and s.Cancelstatus is NULL
		) as TempSales1 group by invoiceNo,InvoiceDate,GstTotal,Name,Branch,RoundoffSaleAmount,SaleAmount
	) as TempSales2 group by invoiceNo,Name,InvoiceDate,Branch,RoundoffSaleAmount,SaleAmount 
	Order by InvoiceDate,cast(substring(invoiceNo,CHARINDEX(' ', invoiceNo),10) as numeric) asc

END