﻿ -- usp_GetSelectedTemplateNameList 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4','b7f8ee03-7f94-4061-b4b1-21cceabfb826'
 CREATE PROCEDURE [dbo].[usp_GetSelectedTemplateNameList](@AccountId varchar(36),@InstanceId varchar(36),@TemplateId varchar(36))
 AS
 BEGIN
 SET NOCOUNT ON  
  
  select STI.Id as Id,STI.TemplateId,ST.TemplateName,STI.ProductId,STI.Quantity,ISNULL(P.Name,PM.Name) as Name,isnull(P.GenericName,'') as GenericName,
  isnull(P.Manufacturer,'') as Manufacturer,
  isnull(P.Category,'') as Category,isnull(P.Subcategory,'') as Subcategory,isnull(P.Schedule,'') as Schedule,isnull(P.GstTotal,0) as GstTotal ,
  ST.AccountId,ST.InstanceId from SalesTemplate ST WITH(NOLOCK) 
  INNER JOIN SalesTemplateItem STI WITH(NOLOCK)  on STI.TemplateId=ST.Id
  LEFT JOIN Product P WITH(NOLOCK) ON P.Id = STI.ProductId 
  LEFT JOIN ProductMaster PM WITH(NOLOCK) ON PM.Id = STI.ProductId 
  WHERE ST.AccountId=@AccountId AND ST.InstanceId=@InstanceId AND ST.Id=@TemplateId and (ST.IsActive!=1 or ST.IsActive is null)
  and (STI.IsDeleted =0 or STI.IsDeleted is null) --ORDER BY ST.CreatedAt ASC
  ORDER BY ISNULL(P.Name,PM.Name) ASC

END