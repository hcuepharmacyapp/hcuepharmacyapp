 
/*                             
******************************************************************************                            
** File: [Usp_vat_Sales_annx21_rpt] 
** Name: [Usp_vat_Sales_annx21_rpt]                             
** Description: To Get Sales details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author:Poongodi R 
** Created Date: 11/02/2017 
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 13/02/2017	Poongodi		Invoiceseries col added along with invoicenumber
** 30/03/2017	Poongodi		Invoice date changed to created at
** 19/06/2017   Violet			Patient Name added in only return 
** 30/06/2017  Poongodi			TaxRefNovalidation added
*******************************************************************************/ 
Create PROC [dbo].[Usp_vat_Sales_annx21_rpt](@AccountId  CHAR(36), 
                                  @InstanceId CHAR(36), 
                                  @StartDate  DATETIME, 
                                  @EndDate    DATETIME) 
AS 
  BEGIN 
 

	  /*
	  Commodity code hard coded based on VAT %
	  VAT		Commoditycode
	  5			2044
	  14.5		301
	  0 or other 752
	  
	  Category
	  Purchare - R for all vat %

	  	1141213551
	  */
	    declare @GstDate date ='2017-06-30'
	  	Select isnull(Name,'-') [BuyerName] ,'-' BuyerTin,  Invoicedate [Invoicedate], InvoiceNo [InvoiceNo], 
					 CASE Isnull(vat, 0) 
               WHEN 5 THEN '2044' 
               WHEN 14.5 THEN '301' 
               ELSE '752' 
             END						                      [Commoditycode],
			Sum(isnull(InvoiceAmount,0) -  isnull(DiscountVal,0)) - ( round((Sum(isnull(InvoiceAmount,0) -  isnull(DiscountVal,0)) /(100+ Isnull(Vat, 0))*100 )   * 
                   Isnull(Vat, 0) / 
                   100, 2) ) [SalesValue],
			Isnull(vat, 0) VAT,
			      0      [VatValue], 
				      Category,max(TranType) [TranType],'By notification for Goods' [ExemptType] from (
					select sales.invoicedate AS InvoiceDate,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries,'')+' ' +sales.invoiceno) as InvoiceNo,sales.Discount as  salesDiscount,
					sales.Name ,salesitem.quantity,
		 
					salesitem.Discount [salesitem.Discount],
					 
					ps.VAT, p.Name as ProductName,  'F' Category ,'Sales' [TranType],
					
convert(decimal(18,2),(CASE isnull( salesitem.SellingPrice,0) WHEN    0 then ps.SellingPrice  else salesitem.SellingPrice   END) * salesitem.quantity) As InvoiceAmount
                    , sales.Discount as Discount 
                    ,(salesitem.Quantity *
					(CASE isnull( salesitem.SellingPrice,0) WHEN    0 then ps.SellingPrice  else salesitem.SellingPrice   END) * isnull(salesitem.Discount,0) / 100) +
					(isnull(salesitem.SellingPrice  ,ps.SellingPrice )* salesitem.quantity * isnull(sales.Discount,0) /100)  as DiscountVal
                    from sales sales 
                    join salesitem salesitem on sales.id= salesitem.salesid
                    join productstock ps on ps.id=salesitem.productstockid
                    inner join Product p on p.Id=ps.ProductId
					
                    WHERE sales.InstanceId = @InstanceId AND sales.AccountId =@AccountId and isnull(ps.VAT,0)=0
					AND Convert(date,sales.Createdat) BETWEEN @StartDate AND @EndDate
					and isnull(sales.cancelstatus ,0) =0 
						 and salesitem.quantity>0
					 	  and isnull(sales.TaxRefNo,0) =0
					   and CONVERT(DATE, sales.createdat) <=@GstDate
					union all 
					select SalesReturn.ReturnDate AS InvoiceDate,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries,'')+' ' +sales.invoiceno) as InvoiceNo,0 as  salesDiscount,
					isnull(sales.name, Isnull(Patient.NAME, '-')) ,SalesReturnItem.quantity,
		 
					SalesReturnItem.Discount [SalesReturnItem.Discount],
					 
					ps.VAT, p.Name as ProductName,  'R' Category ,'Sales Return',
convert(decimal(18,2),(CASE isnull( SalesReturnItem.MrpSellingPrice,0) WHEN    0 then ps.SellingPrice  else SalesReturnItem.MrpSellingPrice   END) * SalesReturnItem.quantity) As InvoiceAmount
                    , Sales.Discount as Discount 
                    ,(SalesReturnItem.Quantity *
					(CASE isnull( SalesReturnItem.MrpSellingPrice,0) WHEN    0 then ps.SellingPrice  else SalesReturnItem.MrpSellingPrice   END) * isnull(SalesReturnItem.Discount,0) / 100) + 
					(isnull(SalesReturnItem.MrpSellingPrice  ,ps.SellingPrice )* SalesReturnItem.quantity * isnull(sales.Discount,0) /100) as DiscountVal
                    from SalesReturn SalesReturn 
                    join SalesReturnItem SalesReturnItem on SalesReturn.id= SalesReturnItem.SalesReturnId
                    join productstock ps on ps.id=SalesReturnItem.productstockid
                    inner join Product p on p.Id=ps.ProductId
					 left join sales on sales.id = salesreturn.salesid 
					 left join patient on patient.id = salesreturn.PatientId 
                    WHERE SalesReturn.InstanceId = @InstanceId AND SalesReturn.AccountId =@AccountId and isnull(ps.VAT,0)=0
					AND Convert(date,SalesReturn.CreatedAt) BETWEEN @StartDate AND @EndDate
					and isnull(sales.cancelstatus ,0) =0 
					and (SalesReturnItem.IsDeleted is null or SalesReturnItem.IsDeleted != 1)
						 and SalesReturnItem.quantity>0
					  and isnull(SalesReturn.TaxRefNo,0) =0
					   and CONVERT(DATE, SalesReturn.createdat) <=@GstDate
                   ) as one   group by [Name] ,  Category, Invoicedate, InvoiceNo,Isnull(vat, 0)
                    ORDER BY  Category,vat,Invoiceno   
					 
					END  --12773