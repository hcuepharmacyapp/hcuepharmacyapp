﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 05/10/2017  Poongodi		   GSTin number added in Validation section
** 13/10/2017  Poongodi		  Prefix added
** 30/12/2017  nandhini		 area name added
*******************************************************************************/ 
Create PROCEDURE [dbo].[usp_Get_ActiveIndentList](@AccountId CHAR(36), @InstanceId CHAR(36), @OrderId CHAR(36), @Type TINYINT,@FromDate Datetime,@ToDate Datetime)
AS
BEGIN
	IF @OrderId = ''
		SET @OrderId = NULL

	IF @Type = 1
	BEGIN
		SELECT R1.Id,R1.OrderId,R1.InstanceId,R1.InstanceName,R1.OrderDate,R1.VendorId,R1.VendorName,R1.VendorOrderItemId,
		R1.ProductId,R1.ProductName,R1.ProductSchedule,R1.PackageSize,R1.PackageQty,R1.Quantity,R1.ReceivedQty,R1.Stock,R1.TransferQtyType,
		R1.VendorPurchaseId,R1.StockTransferId,R1.OrderItemSno,R1.FreeQty FROM 
		(
			SELECT VO.Id,isnull(VO.prefix,'')+ VO.OrderId [OrderId],VO.InstanceId,
			(SELECT Area  FROM Instance WHERE Id = VO.InstanceId) AS InstanceName,
			VO.OrderDate,VO.VendorId,V.Name AS VendorName,VOI.Id AS VendorOrderItemId,
			VOI.ProductId,P.Name AS ProductName,P.Schedule AS ProductSchedule,VOI.PackageSize,VOI.PackageQty,VOI.Quantity,ISNULL(VOI.ReceivedQty,0) AS ReceivedQty,ISNULL(R.Stock,0) AS Stock,
			VOI.VendorPurchaseId,VOI.StockTransferId,VO.CreatedAt,V.TinNo,isnull(V.GsTinNo,'') [GsTinNo],ISNULL(TS.TransferQtyType,1) AS TransferQtyType,
			VOI.OrderItemSno,
			VOI.FreeQty
			FROM VendorOrder VO
			INNER JOIN VendorOrderItem VOI ON VO.Id = VOI.VendorOrderId
			INNER JOIN Product P ON P.Id = VOI.ProductId
			INNER JOIN Vendor V ON V.Id = VO.VendorId
			LEFT JOIN TransferSettings TS ON VO.AccountId = @AccountId AND VO.InstanceId = @InstanceId
			LEFT JOIN 
			(
				SELECT AccountId,InstanceId,ProductId,SUM(Stock) AS Stock FROM ProductStock PS
				WHERE AccountId = @AccountId AND InstanceId = @InstanceId
				AND PS.Stock > 0 AND PS.ExpireDate > CAST(GETDATE() AS DATE)
				AND ISNULL(PS.Status,1) = 1
				GROUP BY AccountId,InstanceId,ProductId
			) R ON R.AccountId = VOI.AccountId AND R.ProductId = VOI.ProductId
			WHERE VO.AccountId = @AccountId AND VO.InstanceId != @InstanceId AND VO.Id = ISNULL(@OrderId,VO.Id) AND 
			ISNULL(VOI.IsDeleted,0) = 0 AND VO.Id IN(SELECT VOI1.VendorOrderId FROM VendorOrderItem VOI1
			WHERE VOI1.AccountId = @AccountId AND VOI1.InstanceId != @InstanceId 
			AND VOI1.VendorOrderId = ISNULL(@OrderId,VOI1.VendorOrderId) AND ISNULL(VOI1.Quantity,0) != ISNULL(VOI1.ReceivedQty,0))
			AND ISNULL(P.Status,1) = 1 AND Convert(date,VO.OrderDate) between @FromDate and @ToDate
		) R1
		INNER JOIN Instance I ON I.GsTinNo = R1.GsTinNo
		WHERE I.AccountId = @AccountId AND I.Id = @InstanceId AND I.isHeadOffice = 1
		ORDER BY R1.CreatedAt DESC 
	END
	ELSE IF @Type = 2 AND @OrderId IS NOT NULL
	BEGIN
		SELECT R1.Id,R1.OrderId,R1.InstanceId,R1.InstanceName,R1.OrderDate,R1.VendorId,R1.VendorName,R1.VendorOrderItemId,
		R1.ProductId,R1.ProductName,R1.ProductSchedule,R1.PackageSize,R1.PackageQty,R1.Quantity,R1.ReceivedQty,R1.Stock,R1.TransferQtyType,
		R1.VendorPurchaseId,R1.StockTransferId,R1.ProductStockId,R1.VendorPurchaseItemId FROM 
		(
			SELECT VP.Id,ISNULL(VP.Prefix,'')+ISNULL(VP.BillSeries,'')+ISNULL(VP.GoodsRcvNo,'') AS OrderId,
			NULL AS InstanceId,NULL AS InstanceName,
			CAST(VP.InvoiceDate AS DATE) AS OrderDate,NULL AS VendorId,
			NULL AS VendorName,NULL AS VendorOrderItemId,VPI.Id AS VendorPurchaseItemId,
			PS.ProductId,P.Name AS ProductName,P.Schedule AS ProductSchedule,
			NULL AS PackageSize,NULL AS PackageQty,ISNULL(VPI.Quantity,0)-ISNULL(STI.Quantity,0) AS Quantity,0 AS ReceivedQty,
			ISNULL(PS.Stock,0) AS Stock,NULL AS VendorPurchaseId,NULL AS StockTransferId,
			ISNULL(TS.TransferQtyType,1) AS TransferQtyType,VPI.ProductStockId
			FROM VendorPurchase VP
			INNER JOIN VendorPurchaseItem VPI ON VP.Id = VPI.VendorPurchaseId
			INNER JOIN ProductStock PS ON PS.Id = VPI.ProductStockId
			INNER JOIN Product P ON P.Id = PS.ProductId
			LEFT JOIN StockTransferItem STI ON STI.VendorPurchaseItemId = VPI.Id
			LEFT JOIN TransferSettings TS ON VP.AccountId = TS.AccountId AND VP.InstanceId = TS.InstanceId
			WHERE VP.AccountId = @AccountId AND VP.InstanceId = @InstanceId 			
			AND VP.Id = @OrderId
			AND VP.CancelStatus IS NULL AND ISNULL(VPI.Status,1) != 2
			AND ISNULL(PS.Stock,0) > 0 AND PS.ExpireDate > CAST(GETDATE() AS DATE) 
			AND ISNULL(PS.Status,1) = 1 AND ISNULL(P.Status,1) = 1
			AND ISNULL(VPI.Quantity,0)-ISNULL(STI.Quantity,0) > 0
		) R1
	END
END