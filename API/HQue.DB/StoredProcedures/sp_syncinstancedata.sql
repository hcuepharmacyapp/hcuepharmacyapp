/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 29/04/2017	Poongodi R	  Order settings table included
** 02/06/2017	Siva		  User Access details taken Based on user and Account 
** 10/08/2017	San		      State table added
*******************************************************************************/
CREATE PROCEDURE [dbo].[sp_syncinstancedata] 
	@InstanceID VARCHAR(40)
AS
BEGIN

IF @InstanceID != ''
BEGIN

Declare @AccountID char(36)
set @AccountID=(select top 1 accountid from Instance where AccountId=(select AccountId from Account inner join Instance on Instance.AccountId = Account.Id where Instance.Id = @InstanceID))
select * from Product where AccountId=(select AccountId from Account inner join Instance on Instance.AccountId = Account.Id where Instance.Id = @InstanceID) for xml Auto, Root('Product')


IF Not Exists(Select instanceId from SyncPreDataCorrection where Instanceid=@InstanceID)

Begin
	exec usp_DataCorrection_Patient_Accountwise @AccountID,@InstanceID
	Insert into SyncPreDataCorrection values(newid(),@AccountID,@InstanceID,GETDATE())
End


Select * from ProductInstance where instanceid = @InstanceID for xml Auto, Root('ProductInstance')
select * from ProductStock where instanceid = @InstanceID for xml Auto, Root('ProductStock')
select * from AlternateVendorProduct where instanceid = @InstanceID for xml Auto, Root('AlternateVendorProduct')
select * from CardTypeSettings where instanceid = @InstanceID for xml Auto, Root('CardTypeSettings')
select * from CommunicationLog where instanceid = @InstanceID for xml Auto, Root('CommunicationLog')
select * from CustomerPayment where instanceid = @InstanceID for xml Auto, Root('CustomerPayment')
select * from CustomReport where instanceid = @InstanceID for xml Auto, Root('CustomReport')
select * from CustomDevReport for xml Auto, Root('CustomDevReport')
select * from DiscountRules where instanceid = @InstanceID for xml Auto, Root('DiscountRules')
select * from InstanceClient where instanceid = @InstanceID for xml Auto, Root('InstanceClient')
select * from InventoryReOrder where instanceid = @InstanceID for xml Auto, Root('InventoryReOrder')
select * from InventorySettings where instanceid = @InstanceID for xml Auto, Root('InventorySettings')
select * from Leads where instanceid = @InstanceID for xml Auto, Root('Leads')
select * from LeadsProduct where instanceid = @InstanceID for xml Auto, Root('LeadsProduct')
select * from Payment where instanceid = @InstanceID for xml Auto, Root('Payment')
select * from PettyCash where instanceid = @InstanceID for xml Auto, Root('PettyCash')
select * from PettyCashSettings where instanceid = @InstanceID for xml Auto, Root('PettyCashSettings')
select * from ReminderProduct where instanceid = @InstanceID for xml Auto, Root('ReminderProduct')
select * from ReminderRemark where instanceid = @InstanceID for xml Auto, Root('ReminderRemark')
select * from Sales where instanceid = @InstanceID for xml Auto, Root('Sales')
select * from SalesItem where instanceid = @InstanceID for xml Auto, Root('SalesItem')
select * from SalesReturn where instanceid = @InstanceID for xml Auto, Root('SalesReturn')
select * from SalesReturnItem where instanceid = @InstanceID for xml Auto, Root('SalesReturnItem')

-- By San
select * from BatchListSettings where instanceid = @InstanceID for xml Auto, Root('BatchListSettings')
select * from DCVendorPurchaseItem where instanceid = @InstanceID for xml Auto, Root('DCVendorPurchaseItem')
select * from Doctor where AccountID = @AccountID for xml Auto, Root('Doctor')

/*Added by Poongodi*/
select * from FinYearMaster where AccountId=(select AccountId from Account inner join Instance on Instance.AccountId = Account.Id where Instance.Id = @InstanceID) for xml Auto, Root('FinYearMaster')
select * from OrderSettings  where instanceid = @InstanceID for xml Auto, Root('OrderSettings')

select * from InvoiceSeries where instanceid = @InstanceID for xml Auto, Root('InvoiceSeries')
select * from InvoiceSeriesItem where instanceid = @InstanceID for xml Auto, Root('InvoiceSeriesItem')
select * from InventorySmsSettings where instanceid = @InstanceID for xml Auto, Root('InventorySmsSettings')
select * from MissedOrder where instanceid = @InstanceID for xml Auto, Root('MissedOrder')
select * from PurchaseSettings where instanceid = @InstanceID for xml Auto, Root('PurchaseSettings')
select * from Patient where AccountID = @AccountID for xml Auto, Root('Patient')
select * from SalesType where instanceid = @InstanceID for xml Auto, Root('SalesType')
select * from SalesItemAudit where instanceid = @InstanceID for xml Auto, Root('SalesItemAudit')
select * from SelfConsumption where instanceid = @InstanceID for xml Auto, Root('SelfConsumption')
select * from SaleSettings where instanceid = @InstanceID for xml Auto, Root('SaleSettings')
select * from TaxSeriesItem where instanceid = @InstanceID for xml Auto, Root('TaxSeriesItem')


select * from StockAdjustment where instanceid = @InstanceID for xml Auto, Root('StockAdjustment')
select * from StockTransfer where AccountID = @AccountID for xml Auto, Root('StockTransfer')
select * from StockTransferItem where AccountID = @AccountID for xml Auto, Root('StockTransferItem')
select * from TransferSettings where instanceid = @InstanceID for xml Auto, Root('TransferSettings')
select * from TempVendorPurchaseItem where instanceid = @InstanceID for xml Auto, Root('TempVendorPurchaseItem')

select * from TemplatePurchaseMaster for xml Auto, Root('TemplatePurchaseMaster')
select * from TemplatePurchaseChildMaster where instanceid = @InstanceID for xml Auto, Root('TemplatePurchaseChildMaster')
select * from TemplatePurchaseChildMasterSettings where instanceid = @InstanceID for xml Auto, Root('TemplatePurchaseChildMasterSettings')

select * from UserReminder where instanceid = @InstanceID for xml Auto, Root('UserReminder')
select * from Vendor where AccountID = @AccountID for xml Auto, Root('Vendor')
select * from VendorOrder where AccountID = @AccountID for xml Auto, Root('VendorOrder')
select * from VendorOrderItem where AccountID = @AccountID for xml Auto, Root('VendorOrderItem')
select * from VendorPurchase where instanceid = @InstanceID for xml Auto, Root('VendorPurchase')
select * from VendorPurchaseItem where instanceid = @InstanceID for xml Auto, Root('VendorPurchaseItem')
select * from VendorPurchaseItemAudit where instanceid = @InstanceID for xml Auto, Root('VendorPurchaseItemAudit')
select * from VendorReturn where instanceid = @InstanceID for xml Auto, Root('VendorReturn')
select * from VendorReturnItem where instanceid = @InstanceID for xml Auto, Root('VendorReturnItem')

/* Support User Included */
select * from HQueUser where AccountId=(select AccountId from Account inner join Instance on Instance.AccountId = Account.Id where Instance.Id = @InstanceID  and instanceId is null) OR instanceid = @InstanceID union
select * from HQueUser where AccountId='8b5b203d-7036-4c1c-b9b1-872f7e2ea4d6'
for xml Auto, Root('HQueUser')

select * from UserAccess where userid in(
select id from HQueUser where AccountId=(select AccountId from Account inner join Instance on Instance.AccountId = Account.Id where Instance.Id = @InstanceID  and instanceId is null) OR instanceid = @InstanceID) for xml Auto, Root('UserAccess')

select Id
,AccountId
,ExternalId
,Code
,Name
,RegistrationDate
,Phone
,Mobile
,DrugLicenseNo
,TinNo
,ContactName
,ContactMobile
,ContactEmail
,SecondaryName
,SecondaryMobile
,SecondaryEmail
,ContactDesignation
,BDOId
,BDODesignation
,BDOName
,RMName
,Address
,Isnull(Area,'') Area
,City
,State
,Pincode
,Landmark
,Status
,DrugLicenseExpDate
,BuildingName
,StreetName
,Country
,Lat
,Lon
,isDoorDelivery
,isVerified
,isUnionTerritory
,isLicensed
,0 as OfflineStatus
,IsOfflineInstalled
,LastLoggedOn
,CreatedAt
,UpdatedAt
,CreatedBy
,UpdatedBy
,DistanceCover
,isHeadOffice,OfflineVersionNo,GsTinNo,InstanceTypeId,SmsUserName,SmsPassword,SmsUrl,TotalSmsCount,SentSmsCount from Instance where AccountId=(select AccountId from Account inner join Instance on Instance.AccountId = Account.Id where Instance.Id = @InstanceID) for xml Auto, Root('Instance')

/* Support Tool*/
select * from Account where Id=@AccountID 
Union
select * from Account where Id='8b5b203d-7036-4c1c-b9b1-872f7e2ea4d6'
 for xml Auto, Root('Account')


SELECT @AccountID AccountID, @InstanceID InstanceID, isnull(max(id),0) SeedIndex,getdate() LastSynedAt FROM  SyncData SyncDataSeed where instanceid = @InstanceID for xml Auto, Root('SyncDataSeed')

Select * from Voucher where instanceid = @InstanceID for xml Auto, Root('Voucher')
select * from PhysicalStockHistory where instanceid = @InstanceID for xml Auto, Root('PhysicalStockHistory')
select * from SalesBatchPopUpSettings where instanceid = @InstanceID for xml Auto, Root('SalesBatchPopUpSettings')
select * from Settlements where instanceid = @InstanceID for xml Auto, Root('Settlements')
select * from SalesReturnItemAudit where instanceid = @InstanceID for xml Auto, Root('SalesReturnItemAudit')
select * from SalesPayment where instanceid = @InstanceID for xml Auto, Root('SalesPayment')
select * from InventoryUpdateHistory where instanceid = @InstanceID for xml Auto, Root('InventoryUpdateHistory')
select * from Billprintsettings where instanceid = @InstanceID for xml Auto, Root('Billprintsettings')
select * from PharmacyTiming where instanceid = @InstanceID for xml Auto, Root('PharmacyTiming')
Select * from State for xml Auto, Root('State')
select * from DomainMaster for xml Auto, Root('DomainMaster')
select * from DomainValues for xml Auto, Root('DomainValues')
select * from PettyCashHdr where instanceid = @InstanceID for xml Auto, Root('PettyCashHdr')
select * from PettyCashDtl where instanceid = @InstanceID for xml Auto, Root('PettyCashDtl')
select * from Settings where instanceid = @InstanceID for xml Auto, Root('Settings')
select * from VendorPurchaseFormula where AccountId = @AccountID for xml Auto, Root('VendorPurchaseFormula')
select * from ClosingStock where instanceid = @InstanceID for xml Auto, Root('ClosingStock')
select * from SchedulerLog where instanceid = @InstanceID for xml Auto, Root('SchedulerLog')
------------------- Version 62 Start

select * from BarcodeProfile where  AccountId = @AccountID for xml Auto, Root('BarcodeProfile')
select * from BarcodePrnDesign where AccountId = @AccountID for xml Auto, Root('BarcodePrnDesign')
select * from CustomSettingsDetail where instanceid = @InstanceID for xml Auto, Root('CustomSettingsDetail')
select * from CustomSettings  for xml Auto, Root('CustomSettings')
select * from SmsSettings where instanceid = @InstanceID for xml Auto, Root('SmsSettings')
select * from SmsConfiguration where instanceid = @InstanceID for xml Auto, Root('SmsConfiguration')
select * from SalesTemplate where instanceid = @InstanceID for xml Auto, Root('SalesTemplate')
select * from SalesTemplateItem where instanceid = @InstanceID for xml Auto, Root('SalesTemplateItem')
select * from SalesOrderEstimate where instanceid = @InstanceID for xml Auto, Root('SalesOrderEstimate')
select * from SalesOrderEstimateItem where instanceid = @InstanceID for xml Auto, Root('SalesOrderEstimateItem')
select * from TaxValues where  AccountId = @AccountID for xml Auto, Root('TaxValues')
select * from SmsLog where  instanceid = @InstanceID  for xml Auto, Root('SmsLog')
select * from LoginHistory where  instanceid = @InstanceID  for xml Auto, Root('LoginHistory')
------------------- Version 62 End

------------------- Version 64 Start
select * from Version for xml Auto, Root('Version')
------------------- Version 64 End
------------------- Version 65 Start
select * from StockLedgerUpdateHistory where instanceid = @InstanceID for xml Auto, Root('StockLedgerUpdateHistory')
select * from StockLedgerUpdateHistoryItem where instanceid = @InstanceID for xml Auto, Root('StockLedgerUpdateHistoryItem')
------------------- Version 65 End
END

END