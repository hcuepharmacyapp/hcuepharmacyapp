 
/*                              
******************************************************************************                            
** File: [usp_GetPOList]  
** Name: [usp_GetPOList]                              
** Description: To Purchase Order Details 
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author: Poongodi R   
** Created Date:   06/02/2017 
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 **07/02/2017  Poongodi     Productid and vendorid parameter added for Purchase 
 **02/03/2017  Poongodi     date validation Changed from Invocie date  to created date
 **08/03/2017  Poongodi     Po cancel status validation added 
 **05/07/2017  Sarubala     GST calculation added 
 **23/08/2017  Lawrence		Added for 'Invoice Date Wise' filter added
 **06/09/2017  Poongodi		Left Join Added in Productmaster 
 ** 13/09/2017 Lawrence		All branch condition Added 
 ** 12/10/2017 Gavaskar		Vatvalue and invoice final amount direct db column value fetched and Markup related field added
*******************************************************************************/ 
--Usage : -- usp_GetPOList  @StartDate='01-Jan-2017',@EndDate=N'06-Feb-2017',@AccountId=N'18204879-99ff-4efd-b076-f85b4a0da0a3',@InstanceId=N'013513b1-ea8c-4ea8-9fed-054b260ee197' 
CREATE PROCEDURE [dbo].[Usp_getpolist](@InstanceId VARCHAR(36), 
                                       @AccountId  VARCHAR(36), 
                                       @StartDate  DATETIME, 
                                       @EndDate    DATETIME, 
                                       @ProductId  NVARCHAR(36) ='', 
                                       @VendorId   NVARCHAR(36) ='',
									   @IsInvoiceDate BIT) 
AS 
  BEGIN 
      SET nocount ON 

      SELECT @ProductId = Isnull(@ProductId, ''), 
             @VendorId = Isnull(@VendorId, '') 
	  
	  select @InstanceId = case when @InstanceId='' then NULL else @InstanceId end

	  	Declare @InvStartDt date,
			@InvEndDt date,
			@GRNStartDt date,
			@GRNEndDt date

		if (@IsInvoiceDate =1)
			begin
			Select @InvStartDt = @StartDate,@InvEndDt =@EndDate, @GRNStartDt = NULL, @GRNEndDt = NULL
			end
		else
			begin
			Select @InvStartDt = NULL,@InvEndDt =NULL, @GRNStartDt = @StartDate, @GRNEndDt = @EndDate
			end

      SELECT vendorpurchaseitem.productstockid, Inst.Name AS InstanceName,
			 CAST(isnull(VendorPurchase.TaxRefNo,0) as int) TaxRefNo,
             productstock.productid, 
             vendorpurchaseitem.quantity, 
             vendorpurchaseitem.purchaseprice, 
             vendorpurchaseitem.packagepurchaseprice [PackagePurchasePrice], 
             vendorpurchaseitem.packageqty, 
             vendorpurchaseitem.packagesellingprice [PackageSellingPrice], 
             Isnull(vendorpurchaseitem.packageqty, 0) - Isnull( 
             vendorpurchaseitem.freeqty, 0) [ActualQty],
			 /* 
			Round( (Isnull(pocal.Poval, 0)) * 
			--(case when VendorPurchase.TaxRefNo = 1 then ISNULL(VendorPurchaseItem.GstTotal,isnull(ProductStock.GstTotal,0)) else Isnull(productstock.VAT, 0) end) / 100, 2) [VatValue],    
			(case when VendorPurchase.TaxRefNo = 1 then ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0))					
			else Isnull(productstock.VAT, 0) end) / 100, 2) [VatValue], */
			 
			 
			 (case when VendorPurchase.TaxRefNo = 1 then ISNULL(VendorPurchaseItem.GstValue,0)				
			else Isnull(VendorPurchaseItem.VatValue, 0) end) [VatValue],   
     /* 
             ( ( Isnull(vendorpurchaseitem.packageqty, 0) - 
                   Isnull(vendorpurchaseitem.freeqty, 0) ) * 
               vendorpurchaseitem.packagepurchaseprice ) - 
             ( ( ( 
             Isnull( 
             vendorpurchaseitem.packageqty, 0) - 
             Isnull( 
             vendorpurchaseitem.freeqty, 0) ) * 
             vendorpurchaseitem.packagepurchaseprice ) * 
             Isnull(vendorpurchaseitem.discount, 0) / 100 ) + ( ( 
             ( 
             ( Isnull( 
               vendorpurchaseitem.packageqty, 0) - 
             Isnull(vendorpurchaseitem.freeqty, 0) ) * 
             vendorpurchaseitem.packagepurchaseprice ) - 
		  ( ( ( 
		  Isnull(vendorpurchaseitem.packageqty, 0) 
			- 
		  Isnull(vendorpurchaseitem.freeqty, 0) ) * 
		  vendorpurchaseitem.packagepurchaseprice ) * 
		  Isnull(vendorpurchaseitem.discount, 0) / 100 ) ) * ( 
		  --(case when VendorPurchase.TaxRefNo = 1 then ISNULL(VendorPurchaseItem.GstTotal,isnull(ProductStock.GstTotal,0)) else Isnull(productstock.vat, 0) end) / 100		  ) ) ReportTotal, 
		   (case when VendorPurchase.TaxRefNo = 1 then ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0)) else Isnull(productstock.vat, 0) end) / 100		  ) ) ReportTotal, */

		  vendorpurchaseitem.PurchasePrice * vendorpurchaseitem.Quantity as [ReportTotal],
		  vendorpurchaseitem.freeqty [FreeQty], 
		  vendorpurchaseitem.discount [Discount], 
		  vendorpurchase.id                     AS [VendorPurchaseId], 
		  vendorpurchase.invoiceno              AS [VendorPONo], 
		  vendorpurchase.invoicedate            AS [InvoiceDate], 
		  vendorpurchase.discount               AS [PODiscount], 
		  vendor.id                             AS [Vendor.Id], 
		  vendor.NAME                           AS [VendorName], 
		  vendor.mobile                         AS [VendorMobile], 
		  vendor.email                          AS [VendorEmail], 
		  vendor.address                        AS [VendorAddress], 
		  vendor.GsTinNo                        AS [VendorTinNo], 
		  productstock.sellingprice             AS [ProductStockSellingPrice], 
		  productstock.vat                      AS [VAT], 
		  productstock.cst                      AS [CST], 
		  /* --ISNULL(VendorPurchaseItem.GstTotal,isnull(ProductStock.GstTotal,0))	AS [GSTTotal],
		  --ISNULL(VendorPurchaseItem.Cgst,isnull(ProductStock.Cgst,0))			AS [Cgst],
		  --ISNULL(VendorPurchaseItem.Sgst,isnull(ProductStock.Sgst,0))			AS [Sgst],
		  --ISNULL(VendorPurchaseItem.Igst,isnull(ProductStock.Igst,0))			AS [Igst],
		  */
		  ISNULL(ProductStock.GstTotal,isnull(VendorPurchaseItem.GstTotal,0))	AS [GSTTotal],
		  ISNULL(ProductStock.Cgst,isnull(VendorPurchaseItem.Cgst,0))			AS [Cgst],
		  ISNULL(ProductStock.Sgst,isnull(VendorPurchaseItem.Sgst,0))			AS [Sgst],
		  ISNULL(ProductStock.Igst,isnull(VendorPurchaseItem.Igst,0))			AS [Igst],
		  product.NAME                          AS [ProductName],
		  isnull(product.HsnCode,ProductStock.HsnCode) As [HsnCode],
		  vendorpurchaseitem.MarkupPerc,
		  vendorpurchaseitem.SchemeDiscountPerc,
		  vendorpurchaseitem.FreeQtyTaxValue
		  FROM   vendorpurchaseitem (nolock) 
		  INNER JOIN vendorpurchase (nolock) 
				  ON vendorpurchase.id = vendorpurchaseitem.vendorpurchaseid 
		  INNER JOIN vendor (nolock) 
				  ON vendor.id = vendorpurchase.vendorid 
		  INNER JOIN productstock (nolock) 
				  ON productstock.id = vendorpurchaseitem.productstockid 
		  Left JOIN product (nolock) 
				  ON product.id = productstock.productid 
		  INNER JOIN Instance Inst on Inst.AccountId = @AccountId
		  and (Inst.AccountId = @AccountId and Inst.Id = ISNULL(@InstanceId,VendorPurchase.InstanceId))
		/*
		CRoss apply (select  Isnull(vendorpurchaseitem.packagepurchaseprice, 0) * (Isnull (vendorpurchaseitem.packageqty, 0)- Isnull ( 
							 vendorpurchaseitem.freeqty, 0) ) - ( Isnull( vendorpurchaseitem.packagepurchaseprice, 0) * ( 
							 Isnull(vendorpurchaseitem.packageqty, 0)- Isnull (vendorpurchaseitem.freeqty, 0) ) * 
			                 Isnull(vendorpurchaseitem.discount, 0) / 100 )  [Poval]) as pocal */
		  WHERE  cast(vendorpurchase.CreatedAt as date) BETWEEN isnull(@GRNStartDt,cast(vendorpurchase.CreatedAt as date)) AND isnull(@GRNEndDt,cast(vendorpurchase.CreatedAt as date))
		  AND cast(vendorpurchase.InvoiceDate as date) BETWEEN isnull(@InvStartDt,cast(vendorpurchase.InvoiceDate as date) ) AND isnull(@InvEndDt,cast(vendorpurchase.InvoiceDate as date))
		  AND vendorpurchase.accountid = @AccountId 
		  AND vendorpurchase.instanceid = ISNULL(@InstanceId,vendorpurchase.InstanceId)
		  AND ( ( vendorpurchase.vendorid = @VendorId ) 
				 OR ( @VendorId = '' ) ) 
		  AND ( ( productstock.productid = @ProductId ) 
				 OR ( @ProductId = '' ) ) 
		  and isnull(VendorPurchase.CancelStatus ,0)= 0
		  and (vendorpurchaseitem.Status is null or vendorpurchaseitem.Status = 1)
		  ORDER  BY vendorpurchase.createdat DESC 
		  END