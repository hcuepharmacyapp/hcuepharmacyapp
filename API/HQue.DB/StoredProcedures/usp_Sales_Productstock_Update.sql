/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
16/12/2017	  Poongodi		Created
*******************************************************************************/ 
Create Proc usp_Sales_Productstock_Update(@InstanceId char(36), @Accountid char(36), @Id char(36),
									@UpdatedAt datetime, @UpdatedBy char(36), @Quantity decimal (18,2), @TransactionId varchar(36) =''
)
as
begin
SET DEADLOCK_PRIORITY LOW
 
 
 if (isnull(@TransactionId,'') ='')
 begin
  Update ProductStock Set Stock = Stock - @Quantity, UpdatedAt = @UpdatedAt, UpdatedBy=@UpdatedBy 
 where AccountId = @AccountId And InstanceId = @InstanceId And Id = @Id 
 end  
else
begin
Update ProductStock Set Stock = Stock - @Quantity, UpdatedAt = @UpdatedAt, UpdatedBy=@UpdatedBy, TransactionId = @TransactionId 
 where AccountId = @AccountId And InstanceId = @InstanceId And Id = @Id  


 end
 
  
  select 'success' 
 
 end