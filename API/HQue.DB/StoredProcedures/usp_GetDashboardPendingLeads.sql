 
--Usage : -- usp_GetDashboardPendingLeads '013513b1-ea8c-4ea8-9fed-054b260ee197', '18204879-99ff-4efd-b076-f85b4a0da0a3'
 CREATE PROCEDURE [dbo].[usp_GetDashboardPendingLeads](@AccountId VARCHAR(36), @InstanceId VARCHAR(36) = '')
 AS
 BEGIN
   SET NOCOUNT ON

   IF @InstanceId != ''
   BEGIN
		SELECT REPLACE(CONVERT(VARCHAR(32),LeadDate,106),' ','-') AS LeadDate, COUNT(*) AS Count FROM Leads
		WHERE AccountId = @AccountId AND InstanceId  =  @InstanceId AND (LeadStatus IS NULL) 
		GROUP BY REPLACE(CONVERT(VARCHAR(32),LeadDate,106),' ','-') ORDER BY 1

	END
	ELSE
	BEGIN
		SELECT REPLACE(CONVERT(VARCHAR(32),LeadDate,106),' ','-') AS LeadDate, COUNT(*) AS Count FROM Leads
		WHERE AccountId = @AccountId AND InstanceId  =  @InstanceId AND (LeadStatus IS NULL) 
		GROUP BY REPLACE(CONVERT(VARCHAR(32),LeadDate,106),' ','-') ORDER BY 1
	END
END


 