
 /*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 13/08/2017 Poongodi R	  Created
 ** 25/01/2017 Poongodi R	  default value specified
*******************************************************************************/ 
Create proc dbo.usp_product_HSN_update(@Id char(36),
@UpdatedAt datetime,
@UpdatedBy char(36),
@HsnCode  varchar(50),
@Igst decimal(9,2) =null,
@Cgst decimal(9,2) =null,
@Sgst decimal(9,2) =null,
@GstTotal decimal(9,2) =null
)
as
begin
SET DEADLOCK_PRIORITY LOW
if ( @GstTotal is null)
begin
UPDATE Product SET UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,HsnCode = @HsnCode  WHERE Product.Id  =  @Id
end 
else
	UPDATE Product SET UpdatedAt = @UpdatedAt,UpdatedBy = @UpdatedBy,HsnCode = @HsnCode,Igst = @Igst,Cgst = @Cgst,Sgst = @Sgst,GstTotal = @GstTotal WHERE Product.Id  =  @Id

 select 'success'
end 