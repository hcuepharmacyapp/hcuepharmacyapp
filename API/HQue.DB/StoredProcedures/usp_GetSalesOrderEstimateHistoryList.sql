﻿/*                               
******************************************************************************                            
** File: [usp_GetSalesOrderEstimateHistoryList]   
** Name: [usp_GetSalesOrderEstimateHistoryList]                               
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 09/10/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
*******************************************************************************/ 
 
 -- exec usp_GetSalesOrderEstimateHistoryList 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4',0,30
 CREATE  PROCEDURE [dbo].[usp_GetSalesOrderEstimateHistoryList](@AccountId varchar(36),@InstanceId varchar(36),@PageNo int,@PageSize int)
 AS
 BEGIN
   SET NOCOUNT ON

SELECT COUNT(1) OVER() SalesOrderEstimateCount, SalesOrderEstimate.Id,isnull(SalesOrderEstimate.Prefix,'') as Prefix,SalesOrderEstimate.OrderEstimateId,SalesOrderEstimate.OrderEstimateDate,SalesOrderEstimate.SalesOrderEstimateType,
Patient.Id [PatientId] ,isnull(Patient.Name,'') as [PatientName] ,isnull(Patient.Email,'') as [PatientEmail],isnull(Patient.Mobile,'') as [PatientMobile], 
SalesOrderEstimate.IsEstimateActive,Patient.PatientType,SalesOrderEstimate.CreatedAt FROM SalesOrderEstimate (nolock) 
LEFT JOIN Patient (nolock) ON Patient.Id = SalesOrderEstimate.PatientId  
WHERE SalesOrderEstimate.AccountId  =  @AccountId AND SalesOrderEstimate.InstanceId  =  @InstanceId
ORDER BY  (SalesOrderEstimate.createdat  )  desc  OFFSET @PageNo ROWS FETCH NEXT @PageSize ROWS ONLY


           
END
