/*                               
******************************************************************************                            
** File: [usp_GetPurchaseCancelList]   
** Name: [usp_GetPurchaseCancelList]                               
** Description: To Get Purchase Audit details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R  
** Created Date: 07/03/2017
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 01/04/2017	Poongodi		Search Issue fixed  
** 14/06/2017	Poongodi R		Prefix added
 ** 22/06/2017	Poongodi R		Prefix added in Filtercondition
 ** 14/12/2017	nandhini n		exact search filteration
  ** 25/12/2017	nandhini n		netvalue added
 *******************************************************************************/ 

 
 -- exec usp_GetPurchaseCancelList '18204879-99ff-4efd-b076-f85b4a0da0a3','013513b1-ea8c-4ea8-9fed-054b260ee197'
 create PROCEDURE [dbo].[usp_GetPurchaseCancelList](@AccountId varchar(36),@InstanceId varchar(36),@PageNo int,@PageSize int,@SearchColName varchar(50), @SearchOption varchar(50),
 @SearchValue varchar(50), @fromDate varchar(15),@Todate varchar(15),@fromCancelDate date, @ToCancelDate date)
 AS
 BEGIN
   SET NOCOUNT ON

  Declare @isDetail bit = 0

  Declare @BtchNo varchar(150) = null,  @invoiceNo varchar(150) = null, @grNo varchar(150) = null,@vendorName varchar(150) = '',@From_ExpiryDt date = null, @To_ExpiryDt date = null, 
  @billDate date = null, @product varchar(150) = null,@expiry date = null,@email varchar(150) = '',@mobile varchar(150) = null,@From_BillDt date = null, @To_BillDt date = null


  if (@SearchColName ='batchNo')
	select  @BtchNo = isnull(@SearchValue,''), @isDetail  =1

else  if (@SearchColName ='invoiceNo')
	select  @invoiceNo =   isnull(@SearchValue,'') 
  
  else   if (@SearchColName ='grNo')
	select  @grNo =  isnull(@SearchValue,'') 

  else   if (@SearchColName ='vendorName')
	select  @vendorName =   isnull(@SearchValue,'') 
  else   if (@SearchColName ='email')
	select  @email =   isnull(@SearchValue,'') 
    else   if (@SearchColName ='mobile')
	select  @mobile =   isnull(@SearchValue,'') 
	else if (@SearchColName ='product')
		
		select @product = isnull(@SearchValue,''), @isDetail =1

	else if (@SearchColName ='expiry')

	select @From_ExpiryDt =  @fromDate ,@To_ExpiryDt = @Todate , @isDetail =1
 
	else if (@SearchColName ='billDate')

	select @From_BillDt =  @fromDate ,@To_BillDt = @Todate

	

IF (@isDetail =0)
BEGIN
 

SELECT COUNT(1) OVER() PurchaseCount, VendorPurchase.Id,VendorPurchase.InvoiceNo,VendorPurchase.InvoiceDate,VendorPurchase.Discount,VendorPurchase.GoodsRcvNo,VendorPurchase.Comments,VendorPurchase.NetValue,
VendorPurchase.GoodsRcvNo,VendorPurchase.FileName,VendorPurchase.PaymentType,VendorPurchase.ChequeNo,VendorPurchase.ChequeDate,VendorPurchase.CreditNoOfDays,
isnull(VendorPurchase.prefix,'')+ISNULL(VendorPurchase.BillSeries,'') BillSeries,
VendorPurchase.Credit,VendorPurchase.CreatedAt,VendorPurchase.NoteAmount,VendorPurchase.NoteType,Vendor.Id [VendorId] ,Vendor.Name [VendorName] ,Vendor.Email,
Vendor.Mobile  ,Vendor.EnableCST ,
isnull(VendorPurchase.CancelStatus ,0) [CancelStatus],
caST(VendorPurchase.UpdatedAt as date)  [CancelDate] ,isnull(VendorPurchase.TaxRefNo,0) as TaxRefNo
 FROM VendorPurchase LEFT JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId  
WHERE cast(VendorPurchase.UpdatedAt as date) between @fromCancelDate and @ToCancelDate
and VendorPurchase.AccountId  =  @AccountId AND VendorPurchase.InstanceId  =  @InstanceId
and VendorPurchase.CancelStatus =1
and isnull(Vendor.Name ,'')  like isnull(@vendorName,'') +'%'
and isnull(Vendor.Mobile ,'') = isnull(@mobile,isnull(Vendor.Mobile ,'')) 
and isnull( Vendor.Email,'')  like isnull(@email,'') +'%'
--and ((ISNULL(VendorPurchase.Prefix,'') + ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,''))  like isnull(@grNo,'') +'%'
--or
-- (ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,''))  like isnull(@grNo,'') +'%')
and ((ISNULL(VendorPurchase.Prefix,'') + ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,''))  = isnull(@grNo,(ISNULL(VendorPurchase.Prefix,'') + ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,''))) 
or
 (ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,''))  = isnull(@grNo,(ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,''))) )

--and isnull(VendorPurchase.InvoiceNo,'') like isnull(@invoiceNo,'')+'%'
and isnull(VendorPurchase.InvoiceNo,'') = isnull(@invoiceNo,isnull(VendorPurchase.InvoiceNo,''))
and cast(VendorPurchase.CreatedAt as date )between isnull(@From_BillDt, cast(VendorPurchase.CreatedAt as date )) and isnull(@To_BillDt, cast(VendorPurchase.CreatedAt as date ))

ORDER BY VendorPurchase.CreatedAt desc OFFSET @PageNo ROWS FETCH NEXT @PageSize ROWS ONLY

END

ELSE
BEGIN
 

SELECT COUNT(1) OVER() PurchaseCount, VendorPurchase.Id,VendorPurchase.InvoiceNo,VendorPurchase.InvoiceDate,VendorPurchase.Discount,VendorPurchase.GoodsRcvNo,VendorPurchase.Comments,VendorPurchase.NetValue,
VendorPurchase.GoodsRcvNo,VendorPurchase.FileName,VendorPurchase.PaymentType,VendorPurchase.ChequeNo,VendorPurchase.ChequeDate,VendorPurchase.CreditNoOfDays,
isnull(VendorPurchase.prefix,'')+ISNULL(VendorPurchase.BillSeries,'') BillSeries,
VendorPurchase.Credit,VendorPurchase.CreatedAt,VendorPurchase.NoteAmount,VendorPurchase.NoteType,Vendor.Id [VendorId] ,Vendor.Name [VendorName] ,Vendor.Email,
Vendor.Mobile ,Vendor.EnableCST ,isnull(VendorPurchase.CancelStatus ,0) [CancelStatus],
caST(VendorPurchase.UpdatedAt as date) [CancelDate],isnull(VendorPurchase.TaxRefNo,0) as TaxRefNo FROM VendorPurchase LEFT JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId  
WHERE VendorPurchase.AccountId  =  @AccountId AND VendorPurchase.InstanceId  =  @InstanceId
and VendorPurchase.id in (Select VendorPurchaseid from VendorPurchaseItem INNER JOIN ProductStock ON ProductStock.Id = VendorPurchaseItem.ProductStockId 
where VendorPurchaseItem.InstanceId  =  @InstanceId AND VendorPurchaseItem.AccountId  =  @AccountId 

and cast(VendorPurchase.UpdatedAt as date) between @fromCancelDate and @ToCancelDate and VendorPurchase.CancelStatus =1
 
and ProductStock.ProductId = isnull(@product, ProductStock.ProductId)
--and isnull(productstock.BatchNo ,'') like isnull(@BtchNo,'') +'%'
and isnull(productstock.BatchNo ,'') = isnull(@BtchNo,isnull(productstock.BatchNo ,'')) 
and productstock.ExpireDate between isnull(@From_ExpiryDt, productstock.ExpireDate) and isnull(@To_ExpiryDt, productstock.ExpireDate))

ORDER BY VendorPurchase.CreatedAt desc OFFSET @PageNo ROWS FETCH NEXT @PageSize ROWS ONLY

END
           
END