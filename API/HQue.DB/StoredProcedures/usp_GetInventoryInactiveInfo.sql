﻿/*                               
******************************************************************************                            
** File: [usp_GetInventoryInactiveInfo]   
** Name: [usp_GetInventoryInactiveInfo]                               
** Description: To Get Inventory Inactive Stock Details
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------           -----------                               
**   
** Author: Settu 
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 29/12/2017 Settu			Created 
*******************************************************************************/ 
CREATE PROC [dbo].[usp_GetInventoryInactiveInfo]( @InstanceId VARCHAR(36), 
													@AccountId   VARCHAR(36), 
													@NameType    INT, 
													@Name        VARCHAR(50), 
													@PageNo      INT=0, 
													@PageSize    INT=10) 
AS 
  BEGIN 
    SET nocount ON 
  
  Declare @PName varchar(250),
		  @GenericName varchar(250)

		  	if (@NameType =1)
			BEGIN
				SET @PName = case when @Name = '' then NULL ELSE @Name END
				SET @GenericName = ''
			END
			else
			BEGIN
				SET @GenericName = CASE WHEN @Name IS NULL THEN '' ELSE @Name END
				SET @PName = NULL
			END
			 

SELECT          ps.productid [ProductId], 
                    p.NAME ProductName, 
                    p.genericname , 
                    p.code ProductCode, 
                    p.manufacturer Manufacturer, 
                    ip.rackno RackNo, 
					ip.boxNo BoxNo, 
                    p.status Status, 
                    MAX(v.NAME)          VendorName, 
                    Count(1) OVER() TotalRecordCount 
    FROM            
	(
		SELECT * FROM productstock WITH(nolock) WHERE accountid=@AccountId 
		AND instanceid=@InstanceId 
		AND ProductId NOT IN(SELECT ProductId FROM ProductStock WHERE AccountId=@AccountId AND InstanceId=@InstanceId AND ISNULL(Status,1)=1)
		and stock != 0 and ISNULL(Status,1)!=1
	) PS 
    INNER JOIN      (SELECT * FROM product WITH(nolock) WHERE accountid=@AccountId ) P  
    ON              p.id = ps.productid 
	Left JOIN		(SELECT * FROM ProductInstance WITH(nolock) WHERE accountid=@AccountId AND instanceid=@InstanceId ) IP on IP.ProductId = P.id
    LEFT OUTER JOIN (SELECT * FROM vendor WITH(nolock) WHERE accountid=@AccountId  ) V
    ON              v.id = ps.vendorid 
    WHERE (P.Name =  CASE WHEN @PName IS NULL THEN p.Name ELSE @PName END
	OR P.Name LIKE CASE WHEN @PName IS NOT NULL AND LEN(@PName)=1 THEN @PName +'%' ELSE @PName END)
	AND      Isnull(p.genericname,'') LIKE @GenericName + '%' 	
	GROUP BY ps.productid,p.NAME,p.genericname,p.code,p.manufacturer,ip.rackno,ip.boxNo,p.status
    ORDER BY        p.NAME  offset @PageNo rows 
    FETCH next @PageSize rows only
  END