 
/*                               
******************************************************************************                            
** File: [usp_get_saleItem_Details]   
** Name: [usp_get_saleItem_Details]                               
** Description: To Get Sale details   based on SaleId 
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
  
*******************************************************************************/ 
CREATE PROC dbo.Usp_get_saleReturnitem_details (@AccountId     NVARCHAR(36), 
                                          @InstanceId    NVARCHAR(36), 
                                          @SaleId        NVARCHAR(max), 
                                          @SearchColName VARCHAR(50), 
                                          @SearchOption  VARCHAR(50), 
                                          @SearchValue   VARCHAR(50), 
                                          @fromDate      VARCHAR(15), 
                                          @Todate        VARCHAR(15)) 
AS 
  BEGIN 
      DECLARE @From_BillDt    DATE = NULL, 
              @To_BillDt      DATE = NULL, 
              @From_ExpiryDt  DATE = NULL, 
              @To_ExpiryDt    DATE = NULL, 
              @BtchNo         VARCHAR(50) = '', 
              @customerName   VARCHAR(200) = '', 
              @customerMobile VARCHAR(100) = '', 
              @doctorName     VARCHAR(100) = NULL, 
              @doctorMobile   VARCHAR(100) = NULL, 
              @product        VARCHAR(100) = '', 
              @billNo         VARCHAR(100) = '', 
              @from_Qty       NUMERIC(10, 2) =NULL, 
              @to_Qty         NUMERIC(10, 2) =NULL, 
              @from_Vat       NUMERIC(10, 2) =NULL, 
              @to_Vat         NUMERIC(10, 2) =NULL, 
              @from_mrp       NUMERIC(10, 2) =NULL, 
              @to_mrp         NUMERIC(10, 2) =NULL, 
              @from_discount  NUMERIC(10, 2) =NULL, 
              @to_discount    NUMERIC(10, 2) =NULL 

      IF ( @SearchColName = 'batchNo' ) 
        SELECT @BtchNo = Isnull(@SearchValue, '') 
      ELSE IF ( @SearchColName = 'product' ) 
        SELECT @product = Isnull(@SearchValue, '') 
      ELSE IF ( @SearchColName = 'expiry' ) 
        SELECT @From_ExpiryDt = @fromDate, 
               @To_ExpiryDt = @Todate 
      ELSE IF ( @SearchColName = 'quantity' ) 
        BEGIN 
            IF ( @SearchOption = 'equal' ) 
              SELECT @from_Qty = Cast (@SearchValue AS NUMERIC (10, 2)), 
                     @to_Qty = Cast (@SearchValue AS NUMERIC (10, 2)) 
            ELSE IF ( @SearchOption = 'greater' ) 
              SELECT @from_Qty = Cast (@SearchValue AS NUMERIC (10, 2)) + 0.01, 
                     @to_Qty = NULL 
            ELSE IF ( @SearchOption = 'less' ) 
              SELECT @from_Qty = NULL, 
                     @to_Qty = Cast (@SearchValue AS NUMERIC (10, 2)) - 0.01 
        END 
      ELSE IF ( @SearchColName = 'vat' ) 
        BEGIN 
            IF ( @SearchOption = 'equal' ) 
              SELECT @from_vat = Cast (@SearchValue AS NUMERIC (10, 2)), 
                     @to_vat = Cast (@SearchValue AS NUMERIC (10, 2)) 
            ELSE IF ( @SearchOption = 'greater' ) 
              SELECT @from_vat = Cast (@SearchValue AS NUMERIC (10, 2)) + 0.01, 
                     @to_vat = NULL 
            ELSE IF ( @SearchOption = 'less' ) 
              SELECT @from_vat = NULL, 
                     @to_vat = Cast (@SearchValue AS NUMERIC (10, 2)) - 0.01 
        END 
      ELSE IF ( @SearchColName = 'mrp' ) 
        BEGIN 
            IF ( @SearchOption = 'equal' ) 
              SELECT @from_Mrp = Cast (@SearchValue AS NUMERIC (10, 2)), 
                     @to_Mrp = Cast (@SearchValue AS NUMERIC (10, 2)) 
            ELSE IF ( @SearchOption = 'greater' ) 
              SELECT @from_Mrp = Cast (@SearchValue AS NUMERIC (10, 2)) + 0.01, 
                     @to_Mrp = NULL 
            ELSE IF ( @SearchOption = 'less' ) 
              SELECT @from_Mrp = NULL, 
                     @to_Mrp = Cast (@SearchValue AS NUMERIC (10, 2)) - 0.01 
        END 

      SELECT 3 
             [SaleItemType], 
             salesitem.id, 
             salesitem.accountid, 
             salesitem.instanceid, 
             SR.salesid [SalesId], 
             product.NAME 
             AS 
             [ProductName], 
             productstock.batchno 
             [BatchNo], 
             productstock.expiredate 
             AS 
             [ExpireDate], 
             productstock.vat 
             AS 
             [Vat], 
             salesitem.discount 
             [Discount], 
             salesitem.quantity 
             [Qty], 
			Isnull(SI.sellingprice, Isnull(productstock.sellingprice, 0)) 
			[SellingPrice], 
			Isnull(salesitem.quantity, 0) * 
			Isnull(SI.sellingprice, Isnull(productstock.sellingprice, 0)) 
			[Total], 
			'' 
			[HQueUserName], 
			salesitem.createdat 
			[CreatedAt] 
			FROM   SalesReturnItem salesitem 
			Inner join SalesReturn SR on SR.id = salesitem.SalesReturnId
			and isnull(SR.IsAlongWithSale ,0)   = 1
			inner join SalesItem SI on SI.ProductStockId = salesitem.ProductStockId
			and si.SalesId = sr.SalesId
			INNER JOIN productstock 
					ON productstock.id = salesitem.productstockid 
			INNER JOIN product 
					ON product.id = productstock.productid 
			CROSS apply (SELECT 
						Isnull(si.sellingprice, Isnull( 
						productstock.sellingprice, 0)) 
									SELLPRICE) AS Sell 
			WHERE  salesitem.instanceid = @InstanceId 
			AND salesitem.accountid = @AccountId 
			AND SR.salesid IN (SELECT a.id 
										FROM   Udf_split(@saleid) a) 
			AND productstock.productid LIKE Isnull(@product, '') + '%' 
			AND productstock.batchno LIKE Isnull(@BtchNo, '') + '%' 
			AND productstock.expiredate BETWEEN Isnull(@From_ExpiryDt, 
												productstock.expiredate) 
												AND 
					Isnull(@To_ExpiryDt, productstock.expiredate) 
			AND salesitem.quantity BETWEEN Isnull(@from_qty, salesitem.quantity) 
											AND 
											Isnull( 
											@to_Qty, salesitem.quantity) 
			AND Isnull(Sell.sellprice, 0) BETWEEN Isnull(@from_mrp, 
													Isnull(Sell.sellprice, 0 
													)) 
													AND Isnull(@to_mrp, 
														Isnull(Sell.sellprice, 0)) 
			AND productstock.vat BETWEEN Isnull(@from_Vat, productstock.vat) AND 	Isnull(@to_Vat, productstock.vat) 
		 
END 
 