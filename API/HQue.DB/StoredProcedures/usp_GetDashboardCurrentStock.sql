 /******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 15/03/2017	Poongodi	   Status removed and stock>0 added (to match the value from stock report )
 ** 09/05/2017	Poongodi	   Query Tunned
 ** 23/09/2017	Poongodi	   GST% included 
 ** 04/10/2017 Poongodi		   Costprice formula 
  ** 16/10/2017 Poongodi		Product Table added in join to match the values to the other reports
*******************************************************************************/ 
--Usage : -- usp_GetDashboardCurrentStock '18204879-99ff-4efd-b076-f85b4a0da0a3','a9e978e5-cba5-48d3-a8cc-7975e24352bf'
Create PROCEDURE [dbo].[usp_GetDashboardCurrentStock](@AccountId VARCHAR(36), @InstanceId VARCHAR(36) = '')
 AS
 BEGIN
   SET NOCOUNT ON
   if (isnull( @InstanceId ,'') ='')
   SET  @InstanceId  =NULL

   --Select Name, Sum(isnull(CurrentStockAmount,0)) as CurrentStockAmount from (
		select 'WITH TAX' as Name, 
		
		Round(sum(Convert(decimal(18,2),(CASE isnull(taxrefno,0)  WHEN 1 THEN  ISNULL(ProductStock.PurchasePrice,0)  
 else (isnull(ProductStock.PurchasePrice,0) *100 /(100+ISNULL(ProductStock.VAT,0))) +((isnull(ProductStock.PurchasePrice,0) *100 /(100+ISNULL(ProductStock.VAT,0)))* isnull(ProductStock.gsttotal,0) /100)    end) * isnull(ProductStock.Stock,0)) ),0)  as CurrentStockAmount
				  from ProductStock (NOLOCK )	 ProductStock 
				    INNER JOIN(Select * from Product  WITH(NOLOCK) where accountid =@AccountId) P ON P.Id = ProductStock.ProductId 
	 	WHERE ProductStock.AccountId = @AccountId AND ProductStock.InstanceId = isnull(@InstanceId,ProductStock.InstanceId)
	 	and  ProductStock.Stock > 0 
		 
		UNION ALL
		 
		select 'WITHOUT TAX' as Name, 
		Round(sum(Convert(decimal(18,2),
		(isnull(ProductStock.PurchasePrice,0) * isnull(ProductStock.Stock,0))-
		(isnull(ProductStock.PurchasePrice,0) * isnull(ProductStock.Stock,0))* ( tax.taxper
  		 /(100 + tax.taxper)))
		) ,0) as CurrentStockAmount from ProductStock (NOLOCK)
  
		INNER JOIN(Select * from Product  WITH(NOLOCK) where accountid =@AccountId) P ON P.Id = ProductStock.ProductId 
		 cross apply (select (CASE isnull(taxrefno,0)  WHEN 0 THEN   	isnull(ProductStock.VAT ,0) else isnull(ProductStock.gsttotal,0) end ) taxper) tax
 		WHERE ProductStock.AccountId = @AccountId AND ProductStock.InstanceId = isnull(@InstanceId,ProductStock.InstanceId)
	 		and  ProductStock.Stock > 0 
		--) a group by Name

 /* 		 
   IF @InstanceId != ''
   BEGIN


   Select Name, Sum(CurrentStockAmount) as CurrentStockAmount from (
		select 'WITH TAX' as Name, 

		Convert(decimal(18,2),((ISNULL((select top 1 purchaseprice from vendorpurchaseitem   inner join vendorpurchase   vp on vp.id =vendorpurchaseitem.vendorpurchaseid
	and vp.instanceid = vendorpurchaseitem.instanceid  where ProductStockId=ProductStock.id and isnull(CancelStatus ,0) =0  and vendorpurchaseitem.instanceid=@InstanceId
	 order by vendorpurchaseitem.CreatedAt desc),ProductStock.PurchasePrice) * ProductStock.Stock)),0) 
				as CurrentStockAmount from ProductStock 
		INNER JOIN Product P  WITH(NOLOCK) ON P.Id = ProductStock.ProductId  
		--left outer JOIN (select distinct productstockid, PurchasePrice from vendorpurchaseitem where AccountId = @AccountId AND InstanceId = @InstanceId) vpi			ON (ProductStock.Id = vpi.productstockid)  
		WHERE ProductStock.AccountId = @AccountId AND ProductStock.InstanceId = @InstanceId
		/*AND (ProductStock.Status is NULL or ProductStock.Status = 1)
		AND (P.Status is NULL or P.Status = 1)*/
		and  ProductStock.Stock > 0 
		) a group by Name
		UNION ALL
		Select Name, Sum(CurrentStockAmount) as CurrentStockAmount from (
		select 'WITHOUT TAX' as Name, Convert(decimal(18,2),((ISNULL((select top 1 purchaseprice from vendorpurchaseitem where ProductStockId=ProductStock.id order by CreatedAt desc),ProductStock.PurchasePrice) * ProductStock.Stock) - 
			((ISNULL((select top 1 purchaseprice from vendorpurchaseitem where ProductStockId=ProductStock.id order by CreatedAt desc),ProductStock.PurchasePrice) * ProductStock.Stock) * (ProductStock.VAT/(100 + ProductStock.VAT)))),0) 
		as CurrentStockAmount from ProductStock 
		INNER JOIN Product P  WITH(NOLOCK) ON P.Id = ProductStock.ProductId  
		--left outer JOIN (select distinct productstockid, PurchasePrice from vendorpurchaseitem where AccountId = @AccountId AND InstanceId = @InstanceId) vpi				ON (ProductStock.Id = vpi.productstockid) 
		WHERE ProductStock.AccountId = @AccountId AND ProductStock.InstanceId = @InstanceId
		/*AND (ProductStock.Status is NULL or ProductStock.Status = 1)
		AND (P.Status is NULL or P.Status = 1)*/
			and  ProductStock.Stock > 0 
		) a group by Name
	END
	ELSE
	BEGIN
	Select Name, Sum(CurrentStockAmount) as CurrentStockAmount from (
		select 'WITH TAX' as Name, Convert(decimal(18,2),((ISNULL((select top 1 purchaseprice from vendorpurchaseitem where ProductStockId=ProductStock.id order by CreatedAt desc),ProductStock.PurchasePrice) * ProductStock.Stock)),0) 
				as CurrentStockAmount from ProductStock 
		INNER JOIN Product P  WITH(NOLOCK) ON P.Id = ProductStock.ProductId  
		--left outer JOIN (select distinct productstockid, PurchasePrice from vendorpurchaseitem where AccountId = @AccountId) vpi ON (ProductStock.Id = vpi.productstockid) 
		WHERE ProductStock.AccountId = @AccountId
		/*AND (ProductStock.Status is NULL or ProductStock.Status = 1)
		AND (P.Status is NULL or P.Status = 1)*/
			and  ProductStock.Stock > 0 
		) a group by Name
		UNION ALL
		Select Name, Sum(CurrentStockAmount) as CurrentStockAmount from (
		select 'WITHOUT TAX' as Name, Convert(decimal(18,2),((ISNULL((select top 1 purchaseprice from vendorpurchaseitem where ProductStockId=ProductStock.id order by CreatedAt desc),ProductStock.PurchasePrice) * ProductStock.Stock) - 
			((ISNULL((select top 1 purchaseprice from vendorpurchaseitem where ProductStockId=ProductStock.id order by CreatedAt desc),ProductStock.PurchasePrice) * ProductStock.Stock) * (ProductStock.VAT/(100 + ProductStock.VAT)))),0) 
		as CurrentStockAmount from ProductStock 
		INNER JOIN Product P  WITH(NOLOCK) ON P.Id = ProductStock.ProductId  
		--left outer JOIN (select distinct productstockid, PurchasePrice from vendorpurchaseitem where AccountId = @AccountId) vpi ON (ProductStock.Id = vpi.productstockid) 
		WHERE ProductStock.AccountId = @AccountId
		/*AND (ProductStock.Status is NULL or ProductStock.Status = 1)
		AND (P.Status is NULL or P.Status = 1)*/
			and  ProductStock.Stock > 0 
		) a group by Name
	END
*/

END
 