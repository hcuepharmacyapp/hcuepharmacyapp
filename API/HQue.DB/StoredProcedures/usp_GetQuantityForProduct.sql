﻿CREATE procedure [dbo].[usp_GetQuantityForProduct](@productid char(36),@instanceid char(36))
as
begin

declare @Tmp table (productid char(36),nonExpiredQuantity int,expiredQuantity int,purchaseprice decimal(18,3),vat decimal(9,2),Igst decimal(9,2),Cgst decimal(9,2),Sgst decimal(9,2),GstTotal decimal(9,2),packagesize decimal(18,2))

--get nonExpiredQuantity
insert into @Tmp(productid, nonExpiredQuantity)
select p.Id,sum(ps.Stock) as nonExpiredQuantity
from ProductStock as ps inner join product as p on ps.ProductId = p.Id
where p.Id = @productid and ps.InstanceId = @instanceid and ps.ExpireDate > GETDATE()
group by p.Id 


--get expiredQuantity
update @Tmp set expiredQuantity = (select sum(ps.Stock) as expiredQuantity
from ProductStock as ps inner join product as p on ps.ProductId = p.Id
where p.Id = @productid and ps.InstanceId = @instanceid and ps.ExpireDate < GETDATE()
group by p.Id )
where productid = @productid

--get purchaseprice,vat,packagesize
update @Tmp set purchaseprice = a.price, vat =a.VAT,igst=a.igst,cgst=a.cgst,sgst=a.sgst,gstTotal=a.gstTotal,packagesize = a.PackageSize from 
 ( select max(vpi.createdat) as date, max(vpi.PackagePurchasePrice) as price,max(vpi.PackageSize) as PackageSize, max(ps.vat) as VAT , max(ps.Igst) as Igst, max(ps.Cgst) as Cgst, max(ps.Sgst) as Sgst, max(ps.GstTotal) as GstTotal from ProductStock as ps left join vendorpurchaseitem as vpi on vpi.ProductStockId = ps.Id where ps.ProductId =@productid and ps.InstanceId=@instanceid group by ps.ProductId) as a 
where productid = @productid

select * from @Tmp     
end