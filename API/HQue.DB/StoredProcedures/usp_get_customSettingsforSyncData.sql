CREATE PROCEDURE [dbo].[usp_get_customSettingsforSyncData](@AccountId CHAR(36), @InstanceId CHAR(36),@customSettingsGroupId int)
AS
BEGIN
	SELECT CS.GroupId, CS.GroupName, CSD.Id, CSD.AccountId, CSD.InstanceId, CSD.IsActive FROM CustomSettings CS
	LEFT JOIN CustomSettingsDetail CSD ON CSD.CustomSettingsGroupId = CS.GroupId AND CSD.AccountId = @AccountId AND CSD.InstanceId = @InstanceId where cs.GroupId=@customSettingsGroupId
	ORDER BY CS.GroupId ASC
END
