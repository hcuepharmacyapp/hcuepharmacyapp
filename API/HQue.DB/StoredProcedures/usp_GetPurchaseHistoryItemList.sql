 

/*                               
******************************************************************************                            
** File: [usp_GetPurchaseHistoryItemList]   
** Name: [usp_GetPurchaseHistoryItemList]                               
** Description: To Get Purchase Audit details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 16/10/2017 Poongodi			GST% taken from Vendorpurchase  
** 24/10/2017 Settu				PurchaseBarcode field added
*******************************************************************************/
 
  -- exec usp_GetPurchaseHistoryItemList '18204879-99ff-4efd-b076-f85b4a0da0a3','013513b1-ea8c-4ea8-9fed-054b260ee197','8adafeb8-ebed-454b-90c7-2c65a7a94f06'
 Create PROCEDURE [dbo].[usp_GetPurchaseHistoryItemList](@AccountId varchar(36),@InstanceId varchar(36),@VendorPurchaseId varchar(max),
  @SearchColName varchar(50),
 @SearchOption varchar(50),
 @SearchValue varchar(50),@fromDate varchar(15),@Todate varchar(15))
 AS
 BEGIN
   SET NOCOUNT ON
    Declare @BtchNo varchar(150) = '',@From_ExpiryDt date = null, @To_ExpiryDt date = null, @product varchar(150) = ''
   if (@SearchColName ='batchNo')
		select  @BtchNo = isnull(@SearchValue,'') 
	else if (@SearchColName ='product')
		select @product = isnull(@SearchValue,'') 
	else if (@SearchColName ='expiry')

	select @From_ExpiryDt =  @fromDate ,@To_ExpiryDt = @Todate 

	--begin
	--if (@SearchOption='equal')
	--	select @From_ExpiryDt =  cast (@SearchValue as date)    ,@To_ExpiryDt =  cast (@SearchValue as date ) 
	--else if (@SearchOption='greater')
	--		select @From_ExpiryDt =  dateadd(d,1, cast (@SearchValue as date))     ,@To_ExpiryDt =  NULL
	--else if (@SearchOption='less')
	--		select @From_ExpiryDt =  null     ,@To_ExpiryDt =  dateadd(d,-1, cast (@SearchValue as date)) 
	--end 

SELECT VP.ProductStockId as ProductStockId,VP.PackageSize,VP.PackageQty,VP.PackagePurchasePrice,VP.PackageSellingPrice,VP.PackageMRP
,VP.VendorPurchaseId,VP.Quantity,VP.PurchasePrice,VP.FreeQty,VP.Discount,
VP.CreatedAt,PS.SellingPrice AS SellingPrice,PS.MRP AS MRP,PS.VAT AS VAT,PS.CST AS CST,PS.BatchNo AS BatchNo,PS.ExpireDate AS [ExpireDate],isnull(PS.TaxType,0) AS TaxType,
PS.PurchaseBarcode AS PurchaseBarcode, PS.Stock AS ProductStock, PS.CreatedAt AS psCreatedAt,
P.Name AS [ProductName],P.Code AS [ProductCode],P.Id AS [ProductId],
VP.Igst, VP.Cgst, VP.Sgst, VP.GstTotal
FROM VendorPurchaseItem VP WITH(NOLOCK)
INNER JOIN ProductStock PS  WITH(NOLOCK) ON  PS.Id = VP.ProductStockId 
INNER JOIN Product P WITH(NOLOCK) ON P.Id = PS.ProductId  
WHERE VP.InstanceId  = @InstanceId  AND VP.AccountId  =  @AccountId
 and PS.ProductId like isnull(@product, '')+'%'
  and isnull(PS.BatchNo ,'') like isnull(@BtchNo,'') +'%'
  and PS.ExpireDate between isnull(@From_ExpiryDt, PS.ExpireDate) and isnull(@To_ExpiryDt, PS.ExpireDate)
and VP.VendorPurchaseId in (select a.id from dbo.udf_Split(@VendorPurchaseId ) a)
and (VP.Status is null or VP.Status = 1)
 ORDER BY VP.CreatedAt asc

           
 END
           
 
