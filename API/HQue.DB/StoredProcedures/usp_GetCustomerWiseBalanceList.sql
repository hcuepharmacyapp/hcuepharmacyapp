﻿  --Usage : -- usp_GetSupplierWiseBalanceList '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93'
/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
**11/11/2017   Poongodi			Instanceid validate from customerpayment 
*******************************************************************************/
Create PROCEDURE [dbo].[usp_GetCustomerWiseBalanceList](@InstanceId varchar(36),@AccountId varchar(36))
AS
 BEGIN
   SET NOCOUNT ON

   Select a.Name, a.Mobile, SUM(a.TotalSalesCredit) as TotalSalesCredit, SUM(a.[CustomerPayment.Debit]) as [CustomerPayment.Debit], 
   SUM(a.[CustomerPayment.Credit]) as [CustomerPayment.Credit],Sum(a.TotalDue) as TotalDue
  from 
(SELECT Sales.Name,Sales.Mobile, ISNULL(SUM(Sales.Credit),0) AS TotalSalesCredit , ISNULL(SUM(CustomerPayment.Debit),0)  AS [CustomerPayment.Debit], ISNULL(SUM(CustomerPayment.Credit),0)  AS [CustomerPayment.Credit],
ISNULL(SUM(
CustomerPayment.Credit)-SUM(CustomerPayment.Debit),0) As TotalDue
FROM Sales 
INNER JOIN CustomerPayment ON CustomerPayment.SalesId = Sales.Id  
WHERE Sales.InstanceId  =  @InstanceId AND Sales.AccountId  =  @AccountId AND (Sales.Cancelstatus IS NULL) AND


 Sales.Name IS NOT NULL
GROUP BY Sales.Name,Sales.Mobile having ISNULL(SUM(CustomerPayment.Credit)-SUM(CustomerPayment.Debit),0)>0
UNION
Select P.Name, P.Mobile, ISNULL(SUM(CP.Credit),0) AS TotalSalesCredit ,ISNULL(SUM(CP.Debit),0)  AS [CP.Debit],
ISNULL(

SUM(CP.Credit),0)  AS [CP.Credit],
ISNULL(SUM(CP.Credit)-SUM(CP.Debit),0) As TotalDue
From Patient P
Inner Join CustomerPayment CP on CP.CustomerId = P.Id
WHERE CP.InstanceId  =  @InstanceId AND P.AccountId  =  @AccountId 
And CP.SalesId is null
GROUP BY P.Name,P.Mobile having ISNULL(SUM(CP.Credit)-SUM(CP.Debit),0)>0) as a
GROUP BY a.Name,a.Mobile
ORDER BY a.Name 

END
