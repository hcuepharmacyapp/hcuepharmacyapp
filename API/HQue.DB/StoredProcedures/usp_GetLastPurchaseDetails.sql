﻿CREATE PROCEDURE [dbo].[usp_GetLastPurchaseDetails](@AccountId CHAR(36), @InstanceId CHAR(36), @ProductId CHAR(36), @SortByPrice BIT)
AS
BEGIN
	SELECT Top 1 ProductStock.BatchNo, ProductStock.ExpireDate, ProductStock.VAT, ISNULL(Product.HsnCode, ProductStock.HsnCode) HsnCode, ISNULL(ProductStock.Igst,Product.Igst) Igst, 
	ISNULL(ProductStock.Cgst,Product.Cgst) Cgst, ISNULL(ProductStock.Sgst,Product.Sgst) Sgst, ISNULL(ProductStock.GstTotal,Product.GstTotal) GstTotal, ProductStock.CST,
	ProductStock.PurchasePrice, ProductStock.TaxRefNo, Product.Eancode, isnull(ProductStock.PackageSize, Product.PackageSize) AS [PackageSize], 
	ProductStock.PackageQty, ProductStock.PackagePurchasePrice,
	ProductStock.PackageSellingPrice, ProductStock.PackageMRP,
	ProductStock.Discount, ProductStock.FreeQty, ProductInstance.RackNo,
	ProductInstance.BoxNo FROM Product 
	LEFT JOIN
	(
		SELECT ProductStock.Id, ProductStock.ProductId, ISNULL(VendorPurchaseItem.Sgst,ProductStock.Sgst) [Sgst], ISNULL(VendorPurchaseItem.GstTotal,ProductStock.GstTotal) [GstTotal], ProductStock.CST, ProductStock.BatchNo, ProductStock.ExpireDate, ProductStock.VAT,
		ProductStock.HsnCode, ISNULL(VendorPurchaseItem.Igst,ProductStock.Igst)[Igst], ISNULL(VendorPurchaseItem.Cgst,ProductStock.Cgst) [Cgst], ProductStock.PurchasePrice, VendorPurchaseItem.PackageSize, VendorPurchaseItem.PackageQty,
		VendorPurchaseItem.PackagePurchasePrice, VendorPurchaseItem.Discount, VendorPurchaseItem.FreeQty, VendorPurchaseItem.PackageSellingPrice, VendorPurchaseItem.PackageMRP,
		vendorpurchaseitem.CreatedAt, VP.TaxRefNo FROM ProductStock 
		INNER JOIN VendorPurchaseItem ON ProductStock.Id = VendorPurchaseItem.ProductStockId
		INNER JOIN VendorPurchase VP ON VP.Id = VendorPurchaseItem.VendorPurchaseId 
		WHERE productstock.AccountId = @AccountId AND productstock.InstanceId = @InstanceId
		
		AND ISNULL(VP.Taxrefno,0) = (CASE @SortByPrice WHEN 1 THEN 1 ELSE ISNULL(VP.Taxrefno,0) END) 
		AND ISNULL(ProductStock.PurchasePrice,0) > (CASE @SortByPrice WHEN 1 THEN 0 ELSE ISNULL(ProductStock.PurchasePrice,0)-1 END)


		AND ProductId = @ProductId AND (ProductStock.Status = 1 Or(ProductStock.Status IS NULL))
	) ProductStock On Product.Id = ProductStock.ProductId
	LEFT JOIN (Select * FROM ProductInstance(NOLOCK) WHERE AccountId = @AccountId AND InstanceId = @InstanceId and ProductId = @ProductId) ProductInstance
	ON ProductInstance.ProductId = Product.id WHERE Product.AccountId = @AccountId AND Product.Id = @ProductId 
	ORDER BY (CASE @SortByPrice WHEN 1 THEN ProductStock.PurchasePrice END) ASC, 
	(CASE @SortByPrice WHEN 0 THEN ProductStock.CreatedAt END) DESC 

END
