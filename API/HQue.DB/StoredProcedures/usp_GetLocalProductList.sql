 --Usage : -- usp_GetLocalProductList 'mat','013513b1-ea8c-4ea8-9fed-054b260ee197','18204879-99ff-4efd-b076-f85b4a0da0a3'


 CREATE PROCEDURE [dbo].[usp_GetLocalProductList](@ProdName varchar(50),@InstanceId varchar(36),@AccountId varchar(36))
 AS
 BEGIN
   SET NOCOUNT ON


select * from 
(select DISTINCT top 20 p.Name,p.Id,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,p.RackNo, -1 as Totalstock 
from Product p where p.InstanceId=@InstanceId and p.AccountId=@AccountId and p.Name like @ProdName+'%' 
and p.Id not in (select p1.Id from Product p1 join ProductStock ps on p1.Id=ps.ProductId 
where p1.Name like @ProdName+'%' and ps.InstanceId=@InstanceId and ps.AccountId=@AccountId) 

union all 

select DISTINCT top 20 p.Name,p.Id, ps.AccountId, ps.InstanceId, p.Code, p.Manufacturer, p.KindName, p.StrengthName, p.Type,p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,p.RackNo, isNull(sum(ps.stock),-1) as Totalstock  from product p join ProductStock ps on p.id = ps.ProductId and ps.InstanceId = @InstanceId and ps.AccountId=@AccountId 
where p.Name like @ProdName+'%' group by p.Id,p.Name,ps.AccountId,ps.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,p.Schedule,p.Category, p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,p.RackNo order by p.name asc) as PD group by Id,Name,AccountId,InstanceId,Code,Manufacturer,KindName,StrengthName,Type,Schedule,Category,Packing,CreatedAt,UpdatedAt,   CreatedBy,UpdatedBy,VAT,Price,Status,RackNo,Totalstock order by name asc



END
