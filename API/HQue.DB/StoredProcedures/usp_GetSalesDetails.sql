﻿CREATE PROCEDURE [dbo].[usp_GetSalesDetails](@AccountId varchar(36),@InstanceId varchar(36),
@StartDate date,@EndDate date,@PatientName VARCHAR(200), @PatientMobile VARCHAR(20), 
@ProductId VARCHAR(36),@UserName VARCHAR(200),@UserId VARCHAR(100))
AS
BEGIN
SET NOCOUNT ON 

IF @InstanceId = ''
SET @InstanceId = NULL
IF @StartDate = ''
SET @StartDate = NULL
IF @EndDate = ''
SET @EndDate = NULL

IF @PatientName = ''
SET @PatientName = NULL
IF @PatientMobile = ''
SET @PatientMobile = NULL
IF @ProductId = ''
SET @ProductId = NULL
if @UserName = ''
SET @UserName = NULL
if @UserId = ''
SET @UserId = NULL

select 
sales.invoicedate AS InvoiceDate,
sales.invoiceno as InvoiceNo,
ltrim(isnull(sales.prefix,'')+isnull(sales.InvoiceSeries, '')) as InvoiceSeries,
ISNULL(U.Name,'') AS UserName,
ISNULL(sales.Name,'') AS Name,
sales.DoctorName,
sales.Address,
salesitem.quantity,
salesitem.SellingPrice,
ps.BatchNo,
ps.ExpireDate,
case isnull(sales.TaxRefNo,0) when 1 then isnull(salesitem.GstTotal,0) else isnull(ps.VAT,0) end VAT,
ps.Stock,
ps.PurchasePrice,
p.Name as ProductName, 
p.Manufacturer, 
p.Schedule,
p.Type,
salesitem.TotalAmount,
d.TotalCostPrice AS TotalCostPrice,
ISNULL(salesitem.DiscountAmount,0) AS DiscountAmount,
CASE WHEN d.TotalCostPrice > 0 THEN (salesitem.TotalAmount-d.TotalCostPrice)/d.TotalCostPrice ELSE 0 END*100 AS SalesProfit,
d.TaxAmount AS TaxAmount,
salesitem.TotalAmount-d.TaxAmount AS AmountWOTTax,
Inst.Name AS InstanceName,
sales.Credit as Credit
from sales sales WITH(NOLOCK)
join salesitem salesitem WITH(NOLOCK) on sales.id= salesitem.salesid
join productstock ps WITH(NOLOCK) on ps.id=salesitem.productstockid
inner join Product p WITH(NOLOCK) on p.Id=ps.ProductId
join Instance Inst WITH(NOLOCK) on Inst.Id = ISNULL(@InstanceId, sales.InstanceId)
LEFT OUTER JOIN HQueUser U WITH(NOLOCK) ON U.id = sales.CreatedBy
CROSS APPLY (SELECT ps.PurchasePrice*salesitem.quantity AS TotalCostPrice,
CASE ISNULL(sales.taxrefno,0) WHEN 1 THEN ISNULL(salesitem.GstAmount,0) ELSE ISNULL(salesitem.VatAmount,0) END AS TaxAmount) AS d
WHERE sales.AccountId = @AccountId AND sales.InstanceId = ISNULL(@InstanceId, sales.InstanceId)   
AND Convert(date,sales.invoicedate) BETWEEN ISNULL(@StartDate,Convert(date,sales.invoicedate)) AND ISNULL(@EndDate,Convert(date,sales.invoicedate)) 
AND ISNULL(sales.Name,'') = ISNULL(@PatientName,ISNULL(sales.Name,''))
AND ISNULL(sales.Mobile,'') = ISNULL(@PatientMobile,ISNULL(sales.Mobile,''))
AND p.Id = ISNULL(@ProductId,p.Id)
AND ISNULL(U.Name,'') = isnull(@UserName, ISNULL(U.Name,''))
and ISNULL(U.UserId,'') = isnull(@UserId, ISNULL(U.UserId,'')) 
AND sales.Cancelstatus is NULL
ORDER BY InstanceName, sales.CreatedAt desc

SET NOCOUNT OFF
END