 
/*                             
******************************************************************************                            
** File: [usp_GetReturnsByDate] 
** Name: [usp_GetReturnsByDate]                             
** Description: To Get Sales Return details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author:Poongodi R 
** Created Date: 02/02/2017 
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 03/02/2017	Poongodi		InvoiceSeries added    
** 31/05/2017	Poongodi		Sales Mapping changed from Inner to left
** 19/06/2017   Violet			Invoice Series condition Added 
** 20/06/2017   Poongodi		Invoice Series Filter commented 
** 21/06/2017   Violet			Prefix Addded
** 10/07/2017   Sarubala		Custom Series Addded
** 17/08/2017   Poongodi		Cancel status position changed
 ** 13/09/2017	Poongodi R		Instance Name added
 ** 14/09/2017	Sarubala V		Default Series added
 ** 12/10/2017	Sarubala V		Return Charges included
*******************************************************************************/ 
CREATE  Proc [dbo].[usp_GetReturnsByDate]( @InstanceId varchar(36),@AccountId varchar(36), @StartDate datetime,@EndDate datetime,@SearchOption varchar(50),
 @SearchValue varchar(50),@FromInvoice int, @ToInvoice int)
 AS
 BEGIN 

 
declare @InvoiceSeries varchar(100) = ''

if (@SearchOption ='1')

select  @InvoiceSeries = '' 

else  if (@SearchOption ='2')

select  @InvoiceSeries = isnull(@SearchValue,'') 

else  if (@SearchOption ='3')

select  @InvoiceSeries = 'MAN'

if(@FromInvoice=0  OR @FromInvoice='')
select  @FromInvoice =null, @ToInvoice=null

if(@ToInvoice=0 OR @ToInvoice='')
select  @ToInvoice =null,  @FromInvoice =null

--select  @SearchOption ='1'
 SET NOCOUNT ON

 IF (@SearchOption ='1')
BEGIN

 SELECT ltrim(isnull(SR.Prefix,'')+' '+ISNULL(sr.InvoiceSeries,'')+' '+ SR.ReturnNo)as ReturnNo,SR.ReturnDate,isnull(S.Name,PT.Name) as Name, ltrim(isnull(s.Prefix,'')+isnull(s.InvoiceSeries,'') +' ' + isnull(S.InvoiceNo,'')) [InvoiceNo],  isnull(S.Mobile,PT.Mobile) as Mobile,S.Email, U.Name as CreatedBy,
	    --sum(case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end ) as AmountBeforeDiscount,
		SR.ReturnItemAmount as AmountBeforeDiscount,
        /*sum(case when  isnull(s.Discount,0) <> 0 then case when sri.MrpSellingPrice is not null then ((sri.MrpSellingPrice*s.Discount)/100)*sri.Quantity 
        else ((ps.SellingPrice*s.Discount)/100)*sri.Quantity end when isnull(sri.Discount,0) <> 0 then case when sri.MrpSellingPrice is not null 
        then ((sri.MrpSellingPrice*sri.Discount)/100)*sri.Quantity else ((ps.SellingPrice*sri.Discount)/100)*sri.Quantity end else 0 end) as DiscountedAmount,   
		sum(case when isNull(sri.Discount, 0) = 0 then
 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity
        else ((((sri.MrpSellingPrice)) * sri.Discount)/100)*sri.Quantity end end) as ReturnedItemDiscount,*/
		SR.TotalDiscountValue as DiscountedAmount,


 --       sum(case when isNull(s.Discount, 0) =
 --0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity
 --       end else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity 
 --       else (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) as ReturnedPrice, 
		SR.NetAmount-ISNULL(sr.ReturnCharges,0) as NetValue,
		SR.NetAmount-SR.RoundOffNetAmount AS ReturnedPrice,
		SR.RoundOffNetAmount,
		Ins.Name [Branch],
		Ins.Area, 
		ISNULL(sr.ReturnCharges,0) ReturnCharges
 
FROM SalesReturn SR (NOLOCK)	
Inner join (Select * from instance where accountid= @AccountId ) Ins on Ins.id = sR.InstanceId
LEFT JOIN Sales S  (NOLOCK) ON s.Id = sr.SalesId 
 
INNER JOIN SalesReturnItem SRI  (NOLOCK) on sri.SalesReturnId =  sr.Id
INNER JOIN ProductStock PS  (NOLOCK) on ps.id = sri.ProductStockId
INNER JOIN Product P  (NOLOCK) on p.Id = ps.ProductId
LEFT OUTER JOIN HQUEUSER U  (NOLOCK) on U.Id = SR.CreatedBy
LEFT JOIN Patient PT  (NOLOCK) on PT.Id = SR.PatientId
WHERE SR.AccountId  =  @AccountId AND SR.InstanceId = ISNULL(@InstanceId,SR.InstanceId)
and SR.ReturnDate BETWEEN @StartDate and @EndDate  AND ISNULL(   S.Cancelstatus,0) =0  
and sri.Quantity >0
and isnull(S.InvoiceSeries,'') =''
AND  Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,''))
and (sri.IsDeleted is null or sri.IsDeleted != 1)
GROUP BY SR.ReturnNo,SR.Prefix,sr.InvoiceSeries,SR.ReturnDate,isnull(S.Name,PT.Name),(isnull(s.Prefix,'')+isnull(s.InvoiceSeries,'') +' ' + isnull(S.InvoiceNo,'')),isnull(S.Mobile,PT.Mobile),
S.Email,U.Name, Ins.Name	,Ins.Area, ISNULL(sr.ReturnCharges,0),SR.ReturnItemAmount,SR.TotalDiscountValue,
SR.RoundOffNetAmount,SR.IsRoundOff,SR.NetAmount										 
ORDER BY SR.ReturnNo DESC
END
ELSE IF (@SearchOption ='2')
BEGIN

SELECT ltrim(isnull(SR.Prefix,'')+' '+ISNULL(sr.InvoiceSeries,'')+' '+ SR.ReturnNo)as ReturnNo,SR.ReturnDate,isnull(S.Name,PT.Name) as Name, ltrim(isnull(s.Prefix,'')+isnull(s.InvoiceSeries,'') +' ' + isnull(S.InvoiceNo,'')) [InvoiceNo],  isnull(S.Mobile,PT.Mobile) as Mobile,S.Email, U.Name as CreatedBy,
	    --sum(case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end ) as AmountBeforeDiscount,
		SR.ReturnItemAmount as AmountBeforeDiscount,
        /*sum(case when  isnull(s.Discount,0) <> 0 then case when sri.MrpSellingPrice is not null then ((sri.MrpSellingPrice*s.Discount)/100)*sri.Quantity 
        else ((ps.SellingPrice*s.Discount)/100)*sri.Quantity end when isnull(sri.Discount,0) <> 0 then case when sri.MrpSellingPrice is not null 
        then ((sri.MrpSellingPrice*sri.Discount)/100)*sri.Quantity else ((ps.SellingPrice*sri.Discount)/100)*sri.Quantity end else 0 end) as DiscountedAmount,   
		sum(case when isNull(sri.Discount, 0) = 0 then
 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity
        else ((((sri.MrpSellingPrice)) * sri.Discount)/100)*sri.Quantity end end) as ReturnedItemDiscount,*/
		SR.TotalDiscountValue as DiscountedAmount,

 --       sum(case when isNull(s.Discount, 0) =
 --0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity
 --       end else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity 
 --       else (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) as ReturnedPrice, 
		SR.NetAmount-ISNULL(sr.ReturnCharges,0) as NetValue,
		SR.NetAmount-SR.RoundOffNetAmount AS ReturnedPrice,
		SR.RoundOffNetAmount,
		Ins.Name [Branch],
		Ins.Area,  
		ISNULL(sr.ReturnCharges,0) ReturnCharges
 
FROM SalesReturn SR (NOLOCK)
Inner join (Select * from instance where accountid= @AccountId ) Ins on Ins.id = sR.InstanceId
LEFT JOIN Sales S  (NOLOCK) ON s.Id = sr.SalesId 
 
INNER JOIN SalesReturnItem SRI (NOLOCK) on sri.SalesReturnId =  sr.Id
INNER JOIN ProductStock PS (NOLOCK) on ps.id = sri.ProductStockId
INNER JOIN Product P (NOLOCK) on p.Id = ps.ProductId
LEFT OUTER JOIN HQUEUSER U (NOLOCK) on U.Id = SR.CreatedBy
LEFT JOIN Patient PT (NOLOCK) on PT.Id = SR.PatientId
WHERE SR.AccountId  =  @AccountId AND SR.InstanceId = ISNULL(@InstanceId,SR.InstanceId)
and SR.ReturnDate BETWEEN @StartDate and @EndDate  AND ISNULL(   S.Cancelstatus,0) =0 
and sri.Quantity >0 
AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%' 
AND isnull(S.InvoiceSeries,'') !='' AND isnull(S.InvoiceSeries,'') !='MAN'
and (sri.IsDeleted is null or sri.IsDeleted != 1)
AND  Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,''))
GROUP BY SR.ReturnNo,SR.Prefix,sr.InvoiceSeries,SR.ReturnDate,isnull(S.Name,PT.Name),(isnull(s.Prefix,'')+isnull(s.InvoiceSeries,'') +' ' + isnull(S.InvoiceNo,'')),isnull(S.Mobile,PT.Mobile),
S.Email,U.Name, Ins.Name, Ins.Area, ISNULL(sr.ReturnCharges,0),SR.ReturnItemAmount,SR.TotalDiscountValue,
SR.RoundOffNetAmount,SR.IsRoundOff,SR.NetAmount											 
ORDER BY SR.ReturnNo DESC

END


ELSE 
BEGIN

SELECT ltrim(isnull(SR.Prefix,'')+' '+ISNULL(sr.InvoiceSeries,'')+' '+ SR.ReturnNo)as ReturnNo,SR.ReturnDate,isnull(S.Name,PT.Name) as Name, 
ltrim(isnull(s.Prefix,'')+isnull(s.InvoiceSeries,'') +' ' + isnull(S.InvoiceNo,'')) [InvoiceNo],  isnull(S.Mobile,PT.Mobile) as Mobile,S.Email, U.Name as CreatedBy,
	    --sum(case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end ) as AmountBeforeDiscount,
		SR.ReturnItemAmount as AmountBeforeDiscount,
        /*sum(case when  isnull(s.Discount,0) <> 0 then case when sri.MrpSellingPrice is not null then ((sri.MrpSellingPrice*s.Discount)/100)*sri.Quantity 
        else ((ps.SellingPrice*s.Discount)/100)*sri.Quantity end when isnull(sri.Discount,0) <> 0 then case when sri.MrpSellingPrice is not null 
        then ((sri.MrpSellingPrice*sri.Discount)/100)*sri.Quantity else ((ps.SellingPrice*sri.Discount)/100)*sri.Quantity end else 0 end) as DiscountedAmount,   
		sum(case when isNull(sri.Discount, 0) = 0 then
 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity
        else ((((sri.MrpSellingPrice)) * sri.Discount)/100)*sri.Quantity end end) as ReturnedItemDiscount,*/
		SR.TotalDiscountValue as DiscountedAmount,

	--	sum(case when isNull(sri.Discount, 0) = 0 then
 --0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity
 --       else ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end) as ReturnedItemDiscount,

        --sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity
        --end else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity 
        --else (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity end end) as ReturnedPrice, 
		SR.NetAmount-ISNULL(sr.ReturnCharges,0) as NetValue,
		SR.NetAmount-SR.RoundOffNetAmount AS ReturnedPrice,
		SR.RoundOffNetAmount,
		Ins.Name [Branch],
		Ins.Area,  
		ISNULL(sr.ReturnCharges,0) ReturnCharges
 
FROM SalesReturn SR (NOLOCK)
Inner join (Select * from instance where accountid= @AccountId ) Ins on Ins.id = sR.InstanceId
LEFT JOIN Sales S (NOLOCK) ON s.Id = sr.SalesId 
INNER JOIN SalesReturnItem SRI (NOLOCK) on sri.SalesReturnId =  sr.Id
INNER JOIN ProductStock PS (NOLOCK) on ps.id = sri.ProductStockId
INNER JOIN Product P (NOLOCK) on p.Id = ps.ProductId
LEFT OUTER JOIN HQUEUSER U (NOLOCK) on U.Id = SR.CreatedBy
LEFT JOIN Patient PT (NOLOCK) on PT.Id = SR.PatientId
WHERE SR.AccountId  =  @AccountId AND SR.InstanceId = ISNULL(@InstanceId,SR.InstanceId)
and SR.ReturnDate BETWEEN @StartDate and @EndDate  AND ISNULL(   S.Cancelstatus,0) =0  
and sri.Quantity >0
AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%'
AND  Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,'')) 
and (sri.IsDeleted is null or sri.IsDeleted != 1)
GROUP BY SR.ReturnNo,SR.Prefix,sr.InvoiceSeries,SR.ReturnDate,isnull(S.Name,PT.Name),(isnull(s.Prefix,'')+isnull(s.InvoiceSeries,'') +' ' + isnull(S.InvoiceNo,'')),isnull(S.Mobile,PT.Mobile),
S.Email,U.Name, Ins.Name, Ins.Area, ISNULL(sr.ReturnCharges,0),SR.ReturnItemAmount,SR.TotalDiscountValue,
SR.RoundOffNetAmount,SR.IsRoundOff,SR.NetAmount									 
ORDER BY SR.ReturnNo DESC

END
END