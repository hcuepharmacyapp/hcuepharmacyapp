﻿-- exec usp_SearchSales @InstanceId=N'3e2ec066-1551-4527-be53-5aa3c5b7fb7d',@AccountId=N'c503ca6e-f418-4807-a847-b6886378cf0b',@fromDate='2017-08-07 00:00:00',@ToDate='2017-08-07 00:00:00',@multiplyfactor=1,@type=3
create procedure [dbo].[usp_SearchSales](@accountid char(36),@instanceid char(36),@fromDate date,@Todate date,@multiplyfactor decimal(18,2),@type tinyint)
as
begin	

declare @Tmp table (productid char(36),productname varchar(1000),soldqty decimal(18,2),TransQty decimal(18,2),packageSize decimal(18,2),packageQty decimal(18,2),purchasePrice decimal(18,3),vat decimal(9,2),Igst decimal(9,2),Cgst decimal(9,2),Sgst decimal(9,2),GstTotal decimal(9,2),
createdat datetime,vendorid char(36), stock decimal(18,2), pendingPOQty decimal(18,2), SellingPrice decimal(18,6))

declare @salesTmp table (productid char(36),soldqty decimal(18,2),TransQty decimal(18,2))
declare @transferTmp table (productid char(36),soldqty decimal(18,2),TransQty decimal(18,2))
declare @missedSalesTmp table (productid char(36),soldqty decimal(18,2),TransQty decimal(18,2))
declare @indentTmp table (productid char(36),soldqty decimal(18,2),TransQty decimal(18,2))

/*
-- Order generation done by various option - Settu
-- -----------------------------------------------
@type = 1 - By Sales
@type = 2 - By Transfer
@type = 3 - By Missed Sales
@type = 4 - By Sales & By Transfer
@type = 5 - By Sales & By Missed Sales
@type = 6 - By Transfer & By Missed Sales
@type = 7 - By Sales, By Transfer & By Missed Sales
@type = 8 - By Indent Qty
@type = 9 - By Sales, By Transfer, By Indent Qty & By Missed Sales
@type = 10 - By Sales, By Transfer & By Indent Qty 
@type = 11 - By Sales, By Indent Qty & By Missed Sales
@type = 12 - By Transfer, By Indent Qty & By Missed Sales
@type = 13 - By Sales & By Indent Qty
@type = 14 - By Transfer & By Indent Qty
@type = 15 - By Indent Qty & By Missed Sales
*/
If @type = 1 OR @type = 4 OR @type = 5 OR @type = 7 OR @type = 9 OR @type = 10 OR @type = 11 OR @type = 13
Begin
	insert into @salesTmp(productid,soldqty,TransQty)
	select ps.ProductId,sum(si.Quantity-ISNULL(SR.ReturnQty,0)),0

	from Sales s inner join SalesItem as si 
		on s.id = si.SalesId
		inner join ProductStock as ps on si.ProductStockId = ps.Id
		LEFT JOIN 
		(
			SELECT SR.SalesId,SRI.ProductStockId,SUM(SRI.Quantity) AS ReturnQty FROM SalesReturn SR 
			INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
			WHERE SR.AccountId = @accountid AND SR.InstanceId = @instanceid
			AND cast(SR.CreatedAt as date) BETWEEN CAST(@fromDate AS DATE) AND CAST(@Todate AS DATE)
			GROUP BY SR.SalesId,SRI.ProductStockId 
		) SR 
		ON SR.SalesId = SI.SalesId AND SR.ProductStockId = SI.ProductStockId
		where si.AccountId = @accountid and si.InstanceId = @instanceid
		and cast(si.CreatedAt as date) between cast(@fromDate as date) and cast(@Todate  as date) 	
		AND s.Cancelstatus IS NULL						  
		group by ps.ProductId 
		/*
		insert into @salesTmp(productid,soldqty,Transqty)
		SELECT  ps.ProductId,0,sum(si.Quantity-ISNULL(SR.ReturnQty,0))
		from Sales s inner join SalesItem as si 
		on s.id = si.SalesId
		inner join ProductStock as ps on si.ProductStockId = ps.Id
		LEFT JOIN 
		(
			SELECT SR.SalesId,SRI.ProductStockId,SUM(SRI.Quantity) AS ReturnQty FROM SalesReturn SR 
			INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
			WHERE SR.AccountId = @accountid AND SR.InstanceId = @instanceid
			AND DATEPART(m, SR.CreatedAt) = DATEPART(m, DATEADD(m, -1, GETDATE()))
			GROUP BY SR.SalesId,SRI.ProductStockId 
		) SR 
		ON SR.SalesId = SI.SalesId AND SR.ProductStockId = SI.ProductStockId
		where si.AccountId = @accountid and si.InstanceId = @instanceid
		and  DATEPART(m, SI.CreatedAt) = DATEPART(m, DATEADD(m, -1, GETDATE())) 	
		AND s.Cancelstatus IS NULL						  
		group by ps.ProductId 
		*/
End	
		
If @type = 2 OR @type = 4 OR @type = 6 OR @type = 7 OR @type = 9 OR @type = 10 OR @type = 12 OR @type = 14
Begin
	insert into @transferTmp(productid,soldqty,TransQty)
	select si.ProductId,sum(si.Quantity),0
		from StockTransfer s inner join StockTransferItem as si 
		on s.id = si.TransferId	   
		where s.AccountId = @accountid and s.InstanceId = @instanceid
		and cast(si.CreatedAt as date) between cast(@fromDate as date) and cast(@Todate  as date) 	
		and s.transferstatus = 3	  
		group by si.ProductId		
		/*
		insert into @transferTmp(productid,soldqty,Transqty)
		SELECT  ps.ProductId,0,sum(si.Quantity-ISNULL(SR.ReturnQty,0))
from Sales s inner join SalesItem as si 
		on s.id = si.SalesId
		inner join ProductStock as ps on si.ProductStockId = ps.Id
		LEFT JOIN 
		(
			SELECT SR.SalesId,SRI.ProductStockId,SUM(SRI.Quantity) AS ReturnQty FROM SalesReturn SR 
			INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
			WHERE SR.AccountId = @accountid AND SR.InstanceId = @instanceid
			AND DATEPART(m, SR.CreatedAt) = DATEPART(m, DATEADD(m, -1, GETDATE()))
			GROUP BY SR.SalesId,SRI.ProductStockId 
		) SR 
		ON SR.SalesId = SI.SalesId AND SR.ProductStockId = SI.ProductStockId
		where si.AccountId = @accountid and si.InstanceId = @instanceid
		and  DATEPART(m, SI.CreatedAt) = DATEPART(m, DATEADD(m, -1, GETDATE()))	
		AND s.Cancelstatus IS NULL						  
		group by ps.ProductId 
		*/
End

If @type = 3 OR @type = 5 OR @type = 6 OR @type = 7 OR @type = 9 OR @type = 11 OR @type = 12 OR @type = 15
Begin
	insert into @missedSalesTmp(productid,soldqty,TransQty)
	select m.ProductId,sum(m.Quantity),0
		from MissedOrder m   
		where m.AccountId = @accountid and m.InstanceId = @instanceid
		and cast(m.CreatedAt as date) between cast(@fromDate as date) and cast(@Todate  as date) 	 
		group by m.ProductId

		/*
			insert into @missedSalesTmp(productid,soldqty,Transqty)
		SELECT  ps.ProductId,0,sum(si.Quantity-ISNULL(SR.ReturnQty,0))
from Sales s inner join SalesItem as si 
		on s.id = si.SalesId
		inner join ProductStock as ps on si.ProductStockId = ps.Id
		LEFT JOIN 
		(
			SELECT SR.SalesId,SRI.ProductStockId,SUM(SRI.Quantity) AS ReturnQty FROM SalesReturn SR 
			INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
			WHERE SR.AccountId = @accountid AND SR.InstanceId = @instanceid
			AND DATEPART(m, SR.CreatedAt) = DATEPART(m, DATEADD(m, -1, GETDATE()))
			GROUP BY SR.SalesId,SRI.ProductStockId 
		) SR 
		ON SR.SalesId = SI.SalesId AND SR.ProductStockId = SI.ProductStockId
		where si.AccountId = @accountid and si.InstanceId = @instanceid
		and  DATEPART(m, SI.CreatedAt) = DATEPART(m, DATEADD(m, -1, GETDATE()))	
		AND s.Cancelstatus IS NULL						  
		group by ps.ProductId 
		*/
End

IF @type = 8 OR @type = 9 OR @type = 10 OR @type = 11 OR @type = 12 OR @type = 13 OR @type = 14 OR @type = 15
BEGIN
	insert into @indentTmp(productid,soldqty,TransQty)
	SELECT R1.ProductId,SUM(ISNULL(R1.Quantity,0)-ISNULL(R1.ReceivedQty,0)),0
	  FROM 
		(
			SELECT VOI.ProductId,VOI.Quantity,ISNULL(VOI.ReceivedQty,0) AS ReceivedQty,V.TinNo,v.GsTinNo
			FROM VendorOrder VO
			INNER JOIN VendorOrderItem VOI ON VO.Id = VOI.VendorOrderId
			INNER JOIN Product P ON P.Id = VOI.ProductId
			INNER JOIN Vendor V ON V.Id = VO.VendorId
			WHERE VO.AccountId = @AccountId AND VO.InstanceId != @InstanceId 
			AND CAST(VO.OrderDate AS DATE) BETWEEN CAST(@fromDate AS DATE) AND CAST(@TODATE AS DATE) 
			AND ISNULL(VOI.IsDeleted,0) = 0 AND (ISNULL(VOI.Quantity,0)-ISNULL(VOI.ReceivedQty,0)) > 0
			AND ISNULL(P.Status,1) = 1
		) R1
		INNER JOIN Instance I ON I.TinNo = R1.TinNo or i.GsTinNo=r1.GsTinNo
		WHERE I.AccountId = @AccountId AND I.Id = @InstanceId AND I.isHeadOffice = 1
		GROUP BY R1.ProductId
		/*
		insert into @indentTmp(productid,soldqty,Transqty)
		SELECT  ps.ProductId,0,sum(si.Quantity-ISNULL(SR.ReturnQty,0))
from Sales s inner join SalesItem as si 
		on s.id = si.SalesId
		inner join ProductStock as ps on si.ProductStockId = ps.Id
		LEFT JOIN 
		(
			SELECT SR.SalesId,SRI.ProductStockId,SUM(SRI.Quantity) AS ReturnQty FROM SalesReturn SR 
			INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
			WHERE SR.AccountId = @accountid AND SR.InstanceId = @instanceid
			AND DATEPART(m, SR.CreatedAt) = DATEPART(m, DATEADD(m, -1, GETDATE()))
			GROUP BY SR.SalesId,SRI.ProductStockId 
		) SR 
		ON SR.SalesId = SI.SalesId AND SR.ProductStockId = SI.ProductStockId
		where si.AccountId = @accountid and si.InstanceId = @instanceid
		and  DATEPART(m, SI.CreatedAt) = DATEPART(m, DATEADD(m, -1, GETDATE()))	
		AND s.Cancelstatus IS NULL						  
		group by ps.ProductId 
		*/
END
	
	-- Final value
	insert into @Tmp(productid,productname,soldqty,TransQty,createdat)
	SELECT R.productid,P.Name,SUM(R.soldqty),SUM(R.TransQty),
	(SELECT max(PS.createdat) FROM ProductStock PS WHERE PS.ProductId = (R.productid)) 
	FROM (
	select productid,soldqty,TransQty from @salesTmp
	UNION ALL
	select productid,soldqty,TransQty from @transferTmp
	UNION ALL
	select productid,soldqty,TransQty from @missedSalesTmp
	UNION ALL 
	SELECT productid,soldqty,TransQty from @indentTmp
	) R 
	INNER JOIN Product P ON P.Id = R.productid
	GROUP BY R.productid,P.Name


--Get Stock 
update @Tmp set stock = b.stock from @Tmp a inner join (select sum(isnull(Stock,0)) as stock,ProductId from ProductStock PS
where PS.InstanceId = @instanceid and PS.AccountId = @accountid 
AND PS.Stock > 0 AND PS.ExpireDate > CAST(GETDATE() AS DATE) AND ISNULL(PS.Status,1) = 1
group by PS.ProductId) as b on a.productid = b.ProductId

--To get pending po qty 
update @Tmp set pendingPOQty = b.pendingPOQty from @Tmp a inner join (select sum(quantity) as pendingPOQty,ProductId from vendororderitem where  InstanceId=@instanceid and AccountId=@accountid and vendorpurchaseid is null 
and StockTransferId is null and isnull(IsDeleted,0) = 0 group by ProductId) as b on a.productid = b.ProductId
 
--Get Last purchasePrice,vat,packageSize,vendorid 
if(@AccountId='67067991-0e68-4779-825e-8e1d724cd68b' and @InstanceId!='5d9a5071-3db9-44e0-9921-c226f34c3616' and @type = 1 )
Begin
update @Tmp  set purchasePrice = isnull(ps.PackagePurchasePrice,0), vat  = isnull(ps.vat,0),Igst  = isnull(ps.Igst,0),Cgst  = isnull(ps.Cgst,0),Sgst  = isnull(ps.Sgst,0),
GstTotal  = isnull(ps.GstTotal,0),packageSize = PS.PackageSize, vendorid = 'E5F3A722-94A0-4ABF-A097-E5155C51402B',SellingPrice = ISNULL(ps.MRP,0)*ISNULL(ps.PackageSize,1) from @Tmp a inner join  ProductStock ps 
on ps.ProductId = A.productid  and A.createdat = ps.createdat
End
else
begin
	update @Tmp  set purchasePrice = isnull(ps.PackagePurchasePrice,0), vat  = isnull(ps.vat,0),Igst  = isnull(ps.Igst,0),Cgst  = isnull(ps.Cgst,0),Sgst  = isnull(ps.Sgst,0),GstTotal  = isnull(ps.GstTotal,0),packageSize = PS.PackageSize, vendorid = PS.VendorId,SellingPrice = ISNULL(ps.MRP,0)*ISNULL(ps.PackageSize,1) from @Tmp a inner join  ProductStock ps 
	on ps.ProductId = A.productid  and A.createdat = ps.createdat
end
select productname,
       Productid,
	   vendorid,
	   Purchaseprice,
	   SellingPrice,
	   ISNULL(vat,0) AS vat,
	   igst,
	   cgst,
	   sgst,
	   gstTotal,
	   isnull(packageSize,1) as packageSize,
	   soldqty,
	   ISNULL(TransQty,0)TransQty,
	
		cast(ROUND(soldqty * @multiplyfactor,0) as decimal(18,2))as ReqQty,
	   isnull(stock,0) As stock,
	  cast( a.orderQty as decimal(18,2)) as  orderQty,
	   cast(ISNULL( a.orderQty/packageSize,0 ) as decimal(18,2)) as  packageQty,
	   pendingPOQty
	  
	    from @tmp  CROSS APPLY
			   (SELECT (case soldqty%packageSize when 0 
	                             then soldqty  
								 else soldqty+packageSize-(soldqty%packageSize) 
	   end) * @multiplyfactor as [orderQty] ) AS a
	   where soldqty IS NOT NULL
	  Group by  productname,
       Productid,
	   vendorid,
	   Purchaseprice,
	   SellingPrice,
	   vat,
	   igst,
	   cgst,
	   sgst,
	   gstTotal,
	   packageSize,
	   soldqty,
	   TransQty,
	   stock,
	   orderQty,
	   pendingPOQty
	  order by pendingPOQty  
	  	    
end
 
