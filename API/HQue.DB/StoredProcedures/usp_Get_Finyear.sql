create proc dbo.usp_Get_Finyear (@Accountid char(36),
@InstanceId char(36),
 
@Billdate date
)
as 
begin
 
declare @FinyearId char(36) ='', @BillReset bit,@BillNo Numeric(10,0)
if (isnull(@Accountid,'') ='') Select @Accountid  = Accountid from Instance where id = @InstanceId
Select @BillReset = IsBillNumberReset  from Account where id =@Accountid
 if (isnull(@BillReset,0) =1)
begin
		Select @FinyearId  =id  from FinYearMaster where cast(@Billdate as date) between FinYearStartDate and  FinYearEndDate and Accountid =@Accountid
		
end 
 
 select isnull(@FinyearId,'') FinYearId
end 