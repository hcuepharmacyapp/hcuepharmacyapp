﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 06/10/2017  Poongodi			Cancel Status added
*******************************************************************************/ 
-- usp_CheckInvoiceExist '1446042d-0208-409a-95db-272324af07bf','4edd19c2-3fe3-4ba2-8083-83a06ccd6c88','54545','52087f46-8043-44fa-b20f-5a2e3bcc3afa'
Create procedure [dbo].[usp_CheckInvoiceExist](@Accountid CHAR(36),@instanceid char(36),@invoiceno varchar(50),@vendorid char(36),@purchaseId  char(36),@BillDate datetime)
as
begin
declare @FinyearId char(36), @BillReset bit,@BillNo Numeric(10,0)
Select @BillReset = IsBillNumberReset  from Account where id =@Accountid
--exec usp_GetBillNumber @InstanceId=N'013513b1-ea8c-4ea8-9fed-054b260ee197',@AccountId=N'18204879-99ff-4efd-b076-f85b4a0da0a3',@BillDate='2017-03-25 13:03:35.950',@TableName=0
if (isnull(@BillReset,0) =1)
begin
		Select @FinyearId  =id  from FinYearMaster where   cast(@Billdate as date) between FinYearStartDate and  FinYearEndDate and Accountid =@Accountid
end 
	select @FinyearId = isnull(@FinyearId,'')
		SELECT 
		--count(*) as count 
		(ISNULL(BillSeries,'')+GoodsRcvNo) as GoodsRcvNo
		FROM VendorPurchase where InstanceId = @instanceid and InvoiceNo = @invoiceno  and VendorId = @vendorid and isnull(id ,'')<> isnull(@purchaseId ,'') and isnull(Finyear,'') =@FinyearId
		and CancelStatus is null 
		GROUP BY GoodsRcvNo, BillSeries
 



end
 