﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 24/10/2017  Gavaskar			Creted
*******************************************************************************/ 
-- usp_CheckSalesTemplateExist 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4','Welcome'
Create procedure [dbo].[usp_CheckSalesTemplateExist](@Accountid CHAR(36),@Instanceid char(36),@TemplateName varchar(100))
as
begin
		SELECT TemplateNo
		FROM SalesTemplate where AccountId  =@Accountid and InstanceId = @Instanceid and TemplateName = @TemplateName 

end
 