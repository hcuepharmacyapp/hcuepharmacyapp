﻿/*                             
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date			Author			Description                             
*******************************************************************************       
** 27/05/2017	Settu			Created  
** 13/09/2017   Lawrence		All branch condition Added 
** 14/09/2017   Sarubala		RackNo,BoxNo max applied 
******************************************************************************

--declare @StartDate date = getdate()-10
--declare @EndDate date = getdate()
--EXEC [usp_Get_DrugWiseReportList] 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4',@StartDate,@EndDate,'',0,0,'Drug'
-- usp_Get_DrugWiseReportList 'c503ca6e-f418-4807-a847-b6886378cf0b', '3e2ec066-1551-4527-be53-5aa3c5b7fb7d', '01-Sep-2017', '14-Sep-2017', 'ALL', 0, 0, null
-- usp_Get_DrugWiseReportList 'c503ca6e-f418-4807-a847-b6886378cf0b', NULL, '01-Sep-2017', '14-Sep-2017', 'ALL', 0, 0, null
*/ 
CREATE PROCEDURE [dbo].[usp_Get_DrugWiseReportList](@AccountId CHAR(36), @InstanceId CHAR(36), @StartDate DATETIME, @EndDate DATETIME, 
@InvoiceSeries varchar(100), @FromInvoice VARCHAR(50), @ToInvoice VARCHAR(50), @productVal VARCHAR(500))
AS
BEGIN
	SET NOCOUNT ON
	SELECT @InvoiceSeries = ISNULL(@InvoiceSeries,''), @productVal = ISNULL(@productVal,'') 

	DECLARE @XmlList XML
	SET @XmlList = CAST(('<A>'+REPLACE(@InvoiceSeries,',','</A><A>')+'</A>') AS XML)

	IF @FromInvoice = '' OR @FromInvoice = 0
	SET @FromInvoice = NULL
	IF @ToInvoice = '' OR @ToInvoice = 0
	SET @ToInvoice = NULL

	SELECT Name,RackNo,BoxNo,Stock,Quantity,VendorName, InstanceName FROM 
	(	
		SELECT P.Id,P.Name,PINS.RackNo,PINS.BoxNo,SUM(PS.Stock) AS Stock,
		(
			SELECT TOP 1 V.Name FROM ProductStock PS1(nolock)
			INNER JOIN Vendor V(nolock) ON V.Id = PS1.VendorId
			WHERE PS1.ProductId = P.Id AND PS1.CreatedAt = MAX(PS.CreatedAt)
		) 
		AS VendorName
		FROM Product P INNER JOIN ProductStock PS ON PS.ProductId = P.Id
		LEFT JOIN 
		(
			SELECT PC.ProductId,max(PC.RackNo) RackNo, max(PC.BoxNo) BoxNo  FROM ProductInstance PC (nolock)
			WHERE PC.AccountId = @AccountId AND PC.InstanceId = ISNULL(@InstanceId,PC.InstanceId)
			GROUP BY PC.ProductId
		) 
		PINS ON PINS.ProductId = PS.ProductId		
		WHERE PS.AccountId = @AccountId AND PS.InstanceId = ISNULL(@InstanceId,PS.InstanceId)
		AND (P.Name like ''+@productVal+'%' OR P.Id = @productVal)
		GROUP BY P.Id,P.Name,PINS.RackNo,PINS.BoxNo
	) R1
	INNER JOIN 
	(	
		SELECT PS.ProductId,SUM(SI.Quantity-ISNULL(SR.Quantity,0)) AS Quantity, I.Name AS InstanceName FROM ProductStock PS 
		INNER JOIN SalesItem SI(nolock) ON PS.Id = SI.ProductStockId
		INNER JOIN Sales S(nolock) ON S.Id = SI.SalesId
		LEFT JOIN 
		(
			SELECT SR.SalesId,SRI.ProductStockId,SUM(SRI.Quantity) AS Quantity FROM SalesReturn SR (nolock)
			INNER JOIN SalesReturnItem SRI(nolock) ON SR.Id = SRI.SalesReturnId
			WHERE SR.AccountId = @AccountId AND SR.InstanceId = ISNULL(@InstanceId,SR.InstanceId)
			GROUP BY SR.SalesId,SRI.ProductStockId 
		) SR 
		ON SR.SalesId = SI.SalesId AND SR.ProductStockId = SI.ProductStockId
		INNER JOIN Instance I on I.Id = isnull(@InstanceId, S.InstanceId)
		WHERE S.AccountId = @AccountId AND S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
		AND ISNULL(S.InvoiceSeries,'') = 
		(
			CASE 
				WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
				WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
			END
		)
		AND S.InvoiceNo BETWEEN ISNULL(@FromInvoice,S.InvoiceNo) AND ISNULL(@ToInvoice,S.InvoiceNo)
		AND S.InvoiceDate BETWEEN @StartDate AND @EndDate
		AND S.Cancelstatus IS NULL
		GROUP BY PS.ProductId, I.Name
	) 
	R2 ON R2.ProductId=R1.Id 
	WHERE R2.Quantity > 0

	SET NOCOUNT OFF
END