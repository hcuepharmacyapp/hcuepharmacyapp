 --Usage : -- usp_GetDashboardAboutToExpire '25-Mar-2017', '013513b1-ea8c-4ea8-9fed-054b260ee197', '18204879-99ff-4efd-b076-f85b4a0da0a3'
 create PROCEDURE [dbo].[usp_GetDashboardAboutToExpire](@FromExpDate Date, @AccountId VARCHAR(36), @InstanceId VARCHAR(36) = '')
 AS
 BEGIN
   SET NOCOUNT ON

   IF @InstanceId != ''
   BEGIN
		SELECT DISTINCT TOP 10 P.Name, SUM(PS.Stock) Stock, PS.ExpireDate FROM ProductStock PS
		INNER JOIN Product P ON P.Id = PS.ProductId 
		WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId AND cast(PS.ExpireDate as date) between cast(@FromExpDate  as date) and dateadd(m,3,cast(@FromExpDate  as date))
		AND (PS.Status is NULL or PS.Status = 1)
		AND (P.Status is NULL or P.Status = 1)
		GROUP BY P.Name, PS.ExpireDate
		HAVING SUM(PS.Stock) > 0
		ORDER BY PS.ExpireDate asc, SUM(PS.Stock) desc
	END
	ELSE
	BEGIN
		SELECT DISTINCT TOP 10 P.Name, SUM(PS.Stock) Stock, PS.ExpireDate FROM ProductStock PS
		INNER JOIN Product P ON P.Id = PS.ProductId 
		WHERE PS.AccountId = @AccountId AND PS.ExpireDate  between cast(@FromExpDate  as date) and dateadd(m,3,cast(@FromExpDate  as date))
		AND (PS.Status is NULL or PS.Status = 1)
		AND (P.Status is NULL or P.Status = 1)
		GROUP BY P.Name, PS.ExpireDate
		HAVING SUM(PS.Stock) > 0
		ORDER BY PS.ExpireDate asc, SUM(PS.Stock) desc
	END
END
 