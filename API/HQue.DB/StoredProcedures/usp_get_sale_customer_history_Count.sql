﻿/**   
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 19/05/2017 Gavaskar	
*******************************************************************************/ 

Create proc dbo.usp_get_sale_customer_history_Count  (
@AccountId nvarchar(36),@InstanceId nvarchar(36),
@CustomerId varchar(100))
as 
 begin
 

SELECT count(1) [SalesCount] FROM Sales WHERE   Sales.AccountId  =  @AccountId AND Sales.InstanceId  =  @InstanceId
and isnull(sales.PatientId ,'')  = isnull(@CustomerId,'')
 
end