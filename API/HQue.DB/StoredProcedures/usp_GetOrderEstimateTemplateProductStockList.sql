﻿ /*                               
******************************************************************************                            
** File: [usp_GetOrderEstimateTemplateProductStockList]   
** Name: [usp_GetOrderEstimateTemplateProductStockList]                               
** Description: To Get Purchase Audit details  
**   
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 31/10/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
  
*******************************************************************************/
 
 -- usp_GetOrderEstimateTemplateProductStockList 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4','b7f8ee03-7f94-4061-b4b1-21cceabfb826'
 CREATE PROCEDURE [dbo].[usp_GetOrderEstimateTemplateProductStockList](@AccountId varchar(36),@InstanceId varchar(36),@ProductId varchar(36))
 AS
 BEGIN
 SET NOCOUNT ON  
  
select top 1 PS.SellingPrice AS SellingPrice,PS.MRP AS MRP,
PS.VAT AS VAT,PS.CST AS CST,PS.BatchNo AS BatchNo,PS.ExpireDate AS [ExpireDate],PS.Stock,PS.PackageSize,PS.PackagePurchasePrice,PS.PurchasePrice,PS.CreatedAt ,
PS.Igst, PS.Cgst, PS.Sgst, PS.GstTotal,ISNULL(P.Name,PM.Name) as Name,PS.ProductId,P.ProductMasterID,PI.RackNo,PI.BoxNo,PS.Id from ProductStock PS  WITH(NOLOCK)
LEFT JOIN Product P WITH(NOLOCK) ON P.Id = PS.ProductId 
LEFT JOIN ProductMaster PM WITH(NOLOCK) ON PM.Id = PS.ProductId  
INNER JOIN (select productid, max(isnull(rackno,''))  [RackNo],  max(isnull(BoxNo,''))  [BoxNo] from ProductInstance WITH(NOLOCK) where instanceid = @InstanceID group by productid) PI  ON PI.ProductId = P.Id 
where PS.AccountId = @AccountId and  PS.InstanceId = @InstanceId and PS.ProductId in (select a.id from dbo.udf_Split(@ProductId ) a) and ps.Stock > 0 ORDER BY PS.CreatedAt ASC

END