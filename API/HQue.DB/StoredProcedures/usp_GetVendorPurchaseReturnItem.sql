﻿--usp_GetVendorPurchaseReturnItem '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','Crocin'
CREATE procedure usp_GetVendorPurchaseReturnItem (@AccountId char(36), @InstanceId char(36), @Name VARCHAR(50)) -- @VendorId char(36),
AS
  Begin
SELECT  distinct(p.Id), P.Name,	
		 ps.PackagePurchasePrice,
		 ps.PurchasePrice,
		 ps.Stock as Quantity,
		 ps.Id as productstockid,
		 ps.ProductId,
		 ps.BatchNo,
		 ps.ExpireDate,
		 ps.VAT,
		 ps.Igst,
		 ps.Sgst,
		 ps.Cgst,
		 ps.GstTotal,
		 ps.SellingPrice,
		 ps.MRP,
		 ps.Stock,
		 ps.CST,
		 ps.PackageSize,
		 CAST(((ISNULL(vpi.PurchasePrice, ps.PurchasePrice) *100/(100+ISNULL(vpi.gsttotal, ISNULL(ps.GstTotal, 0))))*ISNULL(vpi.PackageSize, ISNULL(ps.PackageSize, 1))) AS DECIMAL(18, 6)) as pkgPurPrice
   from ProductStock as ps    
   JOIN Product as P on ps.ProductId = P.Id
   LEFT JOIN VendorPurchaseItem as vpi ON vpi.ProductStockId =ps.Id 
        where 
		ps.AccountId = @AccountId AND
		ps.InstanceId = @InstanceId
		AND Name LIKE ''+@Name+'%' AND PS.Stock>0	 

order by P.Name

END