
/*                             
******************************************************************************                            
** File: [usp_tallySaleReport] 
** Name: [TallySaleReport]                             
** Description: To Get Sales   details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author: 
** Created Date:  
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
** 04/07/2018 Bikas		Columns added and some columns removed.   
*******************************************************************************/ 

Create PROCEDURE [dbo].[TallySaleReport](
@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime )
 AS
 BEGIN
 if (@AccountId='6137e806-7a11-43ab-9f33-23ad4e3d71a6')
 BEGIN
select sa.InvoiceDate,
(isnull(sa.Prefix,'') + isnull(sa.InvoiceSeries,'') + sa.InvoiceNo)InvoiceNo,a.CustomerPaymentType,sa.Name As CustomerName,si.SaleProductName,a.GsTin,productstock.HsnCode,si.Cgst ,si.Sgst,si.Igst,si.GstTotal,si.SellingPrice,si.Quantity,
si.GstAmount/2 AS CGSTAmount,(SI.GstAmount/2) AS SGSTAmount,SI.GstAmount,si.DiscountAmount AS DiscountValue,sa.RoundoffSaleAmount AS RoundOff,si.TotalAmount,Product.Code,V.Name	,'22' As StateId,V.VendorType ,'1001' As salecode,Case  WHEN V.VendorType = 1 THEN 'R'
When V.VendorType = 2 THEN 'UR'
When V.VendorType = 3 THEN 'C'
END as VendorTypeDesc
from Sales sa with(nolock) inner join 
SalesItem si with(nolock) on sa.Id=si.SalesId
INNER JOIN productstock (nolock) 
				  ON productstock.id = si.productstockid 
				   Left JOIN product (nolock) 
				  ON product.id = productstock.productid 
INNER JOIN Vendor V (nolock) on productstock.VendorId = V.Id
left join CustomerPayment as cp WITH(NOLOCK) on  cp.SalesId = sa.Id
inner Join Patient a On a.id  = sa.PatientId
where sa.AccountId=@AccountId and sa.InstanceId=@InstanceId and sa.CreatedAt between @StartDate and @EndDate and isnull(sa.Cancelstatus,0)= 0 

order by InvoiceDate ,sa.CreatedAt desc
END
else
BEGIN
select sa.Id,sa.InvoiceDate,
(isnull(sa.Prefix,'') + isnull(sa.InvoiceSeries,'') + sa.InvoiceNo)InvoiceNo,a.CustomerPaymentType,sa.Name As CustomerName,si.SaleProductName,a.GsTin,productstock.HsnCode,si.Cgst ,si.Sgst,si.Igst,si.GstTotal,si.SellingPrice,si.Quantity,
si.GstAmount/2 AS CGSTAmount,(SI.GstAmount/2) AS SGSTAmount,SI.GstAmount,si.DiscountAmount AS DiscountValue,sa.RoundoffSaleAmount AS RoundOff,si.TotalAmount,Product.Code,V.Name	,V.StateId,V.VendorType ,'' As salecode,Case  WHEN V.VendorType = 1 THEN 'R'
When V.VendorType = 2 THEN 'U'
When V.VendorType = 3 THEN 'C'
END as VendorTypeDesc
from Sales sa with(nolock) inner join 
SalesItem si with(nolock) on sa.Id=si.SalesId
INNER JOIN productstock (nolock) 
				  ON productstock.id = si.productstockid 
				   Left JOIN product (nolock) 
				  ON product.id = productstock.productid 
INNER JOIN Vendor V (nolock) on productstock.VendorId = V.Id
left join CustomerPayment as cp WITH(NOLOCK) on  cp.SalesId = sa.Id
LEFT Join Patient a On a.id  = sa.PatientId
where sa.AccountId=@AccountId and sa.InstanceId=@InstanceId and sa.CreatedAt between @StartDate and @EndDate and isnull(sa.Cancelstatus,0)= 0 

order by InvoiceDate ,sa.CreatedAt desc

END
END
