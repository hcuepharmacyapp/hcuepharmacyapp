﻿/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 21/11/2017 Manivannan	  Created
 ** 23/11/2017 Manivannan	  Added TransactionId
*******************************************************************************/ 
Create Proc usp_Productstock_UpdateStock(@InstanceId char(36), @Accountid char(36), @Id char(36),
									@UpdatedAt datetime, @UpdatedBy char(36), @Quantity decimal (18,2), @TransactionId char(36)
)
as
begin
SET DEADLOCK_PRIORITY LOW
 
 --DECLARE @InsertedIDs table(ID char(36));

 Update ProductStock Set Stock = Stock - @Quantity, UpdatedAt = @UpdatedAt, UpdatedBy=@UpdatedBy, TransactionId = @TransactionId 
 --OUTPUT INSERTED.Id INTO @InsertedIDs
 where AccountId = @AccountId And InstanceId = @InstanceId And Id = @Id And Stock >= @Quantity

 
 --select 'success'

 select @@RowCount
 --Select Count(*) From @InsertedIDs

 end