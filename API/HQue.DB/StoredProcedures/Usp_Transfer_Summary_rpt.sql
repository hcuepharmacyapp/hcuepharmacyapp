/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 20/07/2017	Poongodi		Created
** 26/07/2017	Poongodi		Rejected value displayed for both the instacnes
*******************************************************************************/ 
-- exec usp_GetStockTransferList_Rpt 'c6b7fc38-8e3a-48af-b39d-06267b4785b4','c503ca6e-f418-4807-a847-b6886378cf0b','05-Jul-2017','05-Jul-2017' 
Create PROCEDURE [dbo].[Usp_transfer_Summary_rpt](@InstanceId VARCHAR(36), 
                                          @AccountId  VARCHAR(36), 
                                          @FromDate  DATE, 
                                          @ToDate    DATE) 
AS 
  BEGIN 
  SElect TransferDate, sum(isnull(t.AcceptedValue,0)) [AcceptedValue] , sum(isnull(t.transfervalue,0)) [TransferValue] , sum(isnull(t.AcceptedPending,0)) [PendingValue]
  , sum(isnull(t.RejectedValue,0)) [RejectedValue] from(
      SELECT Isnull(prefix, '') + Isnull( transferno, '') [TransferNo], 
             cast(transferdate as date)[TransferDate], 
             Fi.NAME                                      [TranferFrom], 
             TI.NAME                                      [TransferTo],
             St.transferby [TransferBy], 
              isnull(sti.gstvalue ,0) [GSTValue], 
            isnull( sti.vatvalue ,0) [VatValue],  
             case isnull(st.TransferStatus,0)  when 3 then   case ST.toinstanceid when @InstanceId  then isnull(sti.transfervalue  ,0)  else 0 end else 0 end  [AcceptedValue],
			 case isnull(st.TransferStatus,0)  when 2 then   isnull(sti.transfervalue  ,0)    else 0 end  [RejectedValue],
			 case isnull(st.TransferStatus,0)  when 2 then  0 else case ST.FromInstanceId when @InstanceId  then isnull(sti.transfervalue  ,0)  else 0 end end  [TransferValue],
			  case isnull(st.TransferStatus,0)  when 1 then   case ST.toinstanceid when @InstanceId  then isnull(sti.transfervalue  ,0)  else 0 end else 0 end  [AcceptedPending],
			 case isnull(st.TransferStatus,0)  when 3 then 'Accepted' when 2 then 'Rejected' else 'Pending' end [TransferStatus]
      FROM   stocktransfer st 
             INNER JOIN (SELECT * 
                         FROM   instance 
                         WHERE    accountid = @AccountId) FI 
                     ON FI.id = St.frominstanceid 
             LEFT JOIN (SELECT * 
                        FROM   instance 
                        WHERE    accountid = @AccountId) TI 
                    ON TI.id = St.toinstanceid 
             Left JOIN (SELECT transferid, 
                                Sum(item.itemval) 
                                [TransferValue], 
                                Sum(item.itemval - ( item.itemval * 100 / ( 
                                                     100 + Isnull(gsttotal, 0) ) 
                                                   )) 
                                [GstValue], 
                                Sum(item.itemval - ( item.itemval * 100 / ( 
                                                     100 + Isnull(vat, 0) ) )) 
                                                      [VatValue] 
                         FROM   stocktransferitem 
                                CROSS apply (SELECT ( quantity * Isnull( 
                                                      purchaseprice, 
                                                                 0) ) 
                                                    [ItemVal]) 
                                            Item 
                         WHERE  accountid = @AccountId 
                                AND (instanceid = @InstanceId 
								or ToInstanceId =@InstanceId)
                         GROUP  BY transferid) STI 
                     ON sti.transferid = ST.id 
      WHERE  ST.accountid = @accountId 
             AND ( ST.frominstanceid = @InstanceId 
                    OR ST.toinstanceid = @InstanceId ) 
             AND cast(transferdate as date) BETWEEN @FromDate AND @ToDate ) t
			 group by TransferDate
  END 