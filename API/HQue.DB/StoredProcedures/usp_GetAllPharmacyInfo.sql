 --usp_GetAllPharmacyInfo '1900-01-01','2016-11-01','1900-01-01','2016-11-01',4
 CREATE PROCEDURE [dbo].[usp_GetAllPharmacyInfo](@StartDate datetime,@EndDate datetime,@FirstDate datetime,@LastDate datetime,@RegisterType int=null)
 AS
 BEGIN
   SET NOCOUNT ON
 
   Declare @tblSales table(InstanceId varchar(36),SalesCount int null,SalesTotalValue decimal(18,4) null)
   INSERT INTO @tblSales(InstanceId,SalesCount,SalesTotalValue)
   SELECT SI.InstanceId,  Count(*) as SaleCount,
		   ROUND(SUM( ISNULL(SI.SellingPrice,PS.SellingPrice)*SI.quantity),0) 
	--FROM Sales S WITH(NOLOCK)
 	--INNER JOIN SalesItem SI WITH(NOLOCK) ON S.Id = SI.SalesId
	FROM SalesItem SI WITH(NOLOCK)
	INNER JOIN ProductStock PS WITH(NOLOCK) on PS.id=SI.ProductStockId
    WHERE SI.CreatedAt BETWEEN @StartDate and @EndDate 
	GROUP BY SI.InstanceId
	 
	  Declare @tblSalesFrom table(InstanceId varchar(36),SalesCount int null,SalesTotalValue decimal(18,4) null)
	 INSERT INTO @tblSalesFrom(InstanceId,SalesCount,SalesTotalValue)
   SELECT SI.InstanceId,  Count(*) as SaleCount,
		   ROUND(SUM( ISNULL(SI.SellingPrice,PS.SellingPrice)*SI.quantity),0) SalesTotalValue
	--FROM Sales S WITH(NOLOCK)
 	--INNER JOIN SalesItem SI WITH(NOLOCK) ON S.Id = SI.SalesId
	FROM SalesItem SI WITH(NOLOCK)
	INNER JOIN ProductStock PS WITH(NOLOCK) on PS.id=SI.ProductStockId
    WHERE SI.CreatedAt BETWEEN @FirstDate and @LastDate 
	GROUP BY SI.InstanceId

	Declare @tblVendorSales table(InstanceId varchar(36),PurchaseCount int null,PurchaseTotalValue decimal(18,4) null)
	INSERT INTO @tblVendorSales(InstanceId,PurchaseCount,PurchaseTotalValue)
	select vpi.InstanceId, count(vp.InvoiceNo) as purchaseCount,
                     ROUND(sum(((vpi.PackageQty*vpi.PackagePurchasePrice)-(((vpi.PackageQty*(vpi.PackagePurchasePrice)*vpi.Discount)/100)))+(((vpi.PackageQty*vpi.PackagePurchasePrice) - (((vpi.PackageQty*vpi.PackagePurchasePrice)*vpi.Discount)/100))*ps.VAT/100)),0) as  PurchaseTotalValue
    FROM VendorPurchaseItem as VPI WITH(NOLOCK)
	INNER JOIN VendorPurchase VP WITH(NOLOCK) on VP.id = VPI.vendorPurchaseId
    INNER JOIN ProductStock as PS WITH(NOLOCK) on VPI.ProductStockId = PS.Id
	 WHERE VPI.CreatedAt BETWEEN @StartDate and @EndDate  
	GROUP BY VPI.InstanceId  
	 
   Declare @tblVendorSalesFrom table(InstanceId varchar(36),PurchaseCount int null,PurchaseTotalValue decimal(18,4) null)
	INSERT INTO @tblVendorSalesFrom(InstanceId,PurchaseCount,PurchaseTotalValue)
	select vpi.InstanceId, count(vp.InvoiceNo) as purchaseCount,
                     ROUND(sum(((vpi.PackageQty*vpi.PackagePurchasePrice)-(((vpi.PackageQty*(vpi.PackagePurchasePrice)*vpi.Discount)/100)))+(((vpi.PackageQty*vpi.PackagePurchasePrice) - (((vpi.PackageQty*vpi.PackagePurchasePrice)*vpi.Discount)/100))*ps.VAT/100)),0) as  PurchaseTotalValue
    FROM VendorPurchaseItem as VPI WITH(NOLOCK)
	INNER JOIN VendorPurchase VP WITH(NOLOCK) on VP.id = VPI.vendorPurchaseId
    INNER JOIN ProductStock as PS WITH(NOLOCK) on VPI.ProductStockId = PS.Id
	 WHERE VPI.CreatedAt BETWEEN @FirstDate and @LastDate
	GROUP BY VPI.InstanceId 

   SELECT  i.Id as PharmaId, 
           I.Name as PharmaName,
           ISNULL(I.Area,'') as PharmaArea,
		   A.RegisterType,
			ISNULL(sum(S.SalesCount),0) as SalesInvoiceCount,
			ISNULL(sum(S.SalesTotalValue),0) as SalesTotalValue,
			ISNULL(sum(VS.PurchaseCount),0) as PurchaseInvoiceCount,
			ISNULL(sum(VS.PurchaseTotalValue),0) as PurchaseTotalValue,
			ISNULL(sum(SF.SalesCount),0) as saleslastmonthcount,
			ISNULL(sum(SF.SalesTotalValue),0) as SaleslastmonthTotalValue,
			ISNULL(sum(VSF.PurchaseCount),0) as purchasetotalmonthCount,
			ISNULL(sum(VSF.PurchaseTotalValue),0) as PurchaseTotalmonthValue    
FROM Instance I WITH(NOLOCK)
INNER JOIN Account as A on A.Id = I.AccountId  

LEFT OUTER JOIN @tblSales S  ON I.Id = S.InstanceId
LEFT OUTER JOIN @tblVendorSales VS ON I.Id = S.InstanceId
LEFT OUTER JOIN @tblSalesFrom SF  ON I.Id = SF.InstanceId
LEFT OUTER JOIN @tblVendorSalesFrom VSF ON I.Id = SF.InstanceId

WHERE A.RegisterType =ISNULL(@RegisterType,A.RegisterType)
 GROUP BY  i.Id,i.Name, i.Area,A.RegisterType ;
  
 END
  


