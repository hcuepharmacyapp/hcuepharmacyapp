 /*                              
******************************************************************************                            
** File: [usp_GetProduct_PurchaseSalesList]  
** Name: [usp_GetProduct_PurchaseSalesList]                              
** Description: Product wise Purchase Sales details for a period
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author: Manivannan
** Created Date:   25/05/2017 
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 
*******************************************************************************/ 
--Usage : -- usp_GetProduct_PurchaseSalesList  @AccountId=N'18204879-99ff-4efd-b076-f85b4a0da0a3', @InstanceId=N'013513b1-ea8c-4ea8-9fed-054b260ee197', @GRNNo = '12'
CREATE PROCEDURE [dbo].[usp_GetProduct_PurchaseSalesList](@AccountId  VARCHAR(36), @InstanceId VARCHAR(36), @GRNNo VARCHAR(50)) 
AS 
  BEGIN 
      SET nocount ON 

-- Temp table declaration
DECLARE @TmpPurchaseSale TABLE (ProductName varchar(1000), PurchaseQuantity decimal(18,6), FreeQty decimal(18,6), PurchasePrice decimal(18,6)
, PS_SellingPrice decimal(18,6), PS_MRP decimal(18,6), PS_Purchase_Amount decimal(18,6), S_InvoiceDate date, S_InvoiceNo varchar(50), 
CustomerName varchar(100), SI_Quantity decimal(18,6), SI_SellingPrice decimal(18,6), SI_MRP decimal(18,6), SI_Selling_Amount decimal(18,6), SI_MRP_Amount decimal(18,6), SellingPrice_Profit decimal(10,6), MRP_Profit decimal(10,6), PS_Stock decimal(18,6), ProductStockId Varchar(36), SalesId Varchar(36))

-- Populating the temp table
INSERT INTO @TmpPurchaseSale (ProductName, PurchaseQuantity, FreeQty, PurchasePrice, PS_SellingPrice, PS_MRP, S_InvoiceDate, S_InvoiceNo, CustomerName, SI_Quantity, SI_SellingPrice, SI_MRP, PS_Stock, ProductStockId, SalesId)
select P.Name, VPI.Quantity as PurchaseQuantity, VPI.FreeQty, VPI.PurchasePrice, isNull(PS.SellingPrice, 0), isNull(PS.MRP, 0)
, S.InvoiceDate, S.InvoiceNo, isNull(S.Name, '') CustomerName, SI.Quantity, isNull(SI.SellingPrice, 0), isNull(SI.MRP, 0), PS.Stock, PS.Id, S.Id
 From VendorPurchase VP 
 Join VendorPurchaseItem VPI On VPI.VendorPurchaseId = VP.Id
 Join ProductStock PS On PS.Id = VPI.ProductStockId
 Join Product P On P.Id = PS.ProductId
 Left Join SalesItem SI On SI.ProductStockId = PS.Id
 Join Sales S On S.Id = SI.SalesId
  where VP.GoodsRcvNo = @GRNNo
  And VP.AccountId = @AccountId And VP.InstanceId = @InstanceId

  -- Calculating the Amount (Price * Quantity) for Purchase, Selling and MRP amount fields
  Update @TmpPurchaseSale Set PS_Purchase_Amount = ((PurchaseQuantity+FreeQty) * PurchasePrice)
  ,  SI_Selling_Amount = (SI_Quantity * SI_SellingPrice), SI_MRP_Amount = (SI_Quantity * SI_MRP)
  , SellingPrice_Profit = ((SI_SellingPrice - PurchasePrice) / PurchasePrice * 100)
  , MRP_Profit = ((SI_MRP - PurchasePrice) / PurchasePrice * 100)

  -- Subtracting the Sales Return Quantity from the Sales Quantity
  Update temp set SI_Quantity = SI_Quantity - sri.Quantity from @TmpPurchaseSale temp 
  Join SalesReturn sr On sr.SalesId = temp.SalesId
  Join SalesReturnItem sri On sri.SalesReturnId = sr.Id And sri.ProductStockId = temp.ProductStockId

  Select * from @TmpPurchaseSale

END  
 