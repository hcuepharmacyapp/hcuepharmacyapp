/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 13/08/2017 Poongodi R	  Created
*******************************************************************************/ 
Create Proc usp_Productstock_Update(@Stock decimal (18,2),
								    @VendorId char(36),
									@IsMovingStock bit,
									@SellingPrice decimal(18,6),
									@Eancode varchar (50),
									@OfflineStatus bit,
									@MRP  decimal(18,6),
									@Id char(36),
									@Accountid char(36),
									@Instanceid char(36),
									@UpdatedAt datetime 
)
as
begin
SET DEADLOCK_PRIORITY LOW
 
 UPDATE ProductStock SET Stock = @Stock,VendorId = @VendorId,UpdatedAt = @UpdatedAt,IsMovingStock = @IsMovingStock,
							SellingPrice = @SellingPrice,Eancode = @Eancode,OfflineStatus = @OfflineStatus,MRP = @MRP, Status = NULL 
							WHERE ProductStock.Id  =  @Id
								  and Productstock.AccountId =@Accountid and ProductStock.InstanceId =@Instanceid 

 
 select 'success'
 end