﻿/*                               
******************************************************************************                            
** File: [usp_GetSalesLoyaltyPointSettings]   
** Name: [usp_GetSalesLoyaltyPointSettings]                               
**   
** This Loyalty Points can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 25/05/2018  
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
*******************************************************************************/ 
 
 -- exec usp_GetSalesLoyaltyPointSettings '956d7fe7-d39b-4a9c-91b6-552fb7d1481e','87c13de7-eda0-4567-8439-8bb078f71059','e46a39f5-0325-47fe-9d71-1bdc6606df88'
 CREATE  PROCEDURE [dbo].[usp_GetSalesLoyaltyPointSettings](@AccountId varchar(36),@LoyaltyId varchar(36),@SalesId varchar(36))
 AS
 BEGIN

SELECT LoyaltyPointSettings.Id,Sales.Id as SalesId,LoyaltyPointSettings.LoyaltySaleValue,LoyaltyPointSettings.LoyaltyPoint,
LoyaltyPointSettings.StartDate,LoyaltyPointSettings.EndDate,LoyaltyPointSettings.MinimumSalesAmt,LoyaltyPointSettings.RedeemPoint,
LoyaltyPointSettings.RedeemValue,LoyaltyPointSettings.LoyaltyOption,LoyaltyPointSettings.IsMinimumSale,LoyaltyPointSettings.IsEndDate,
LoyaltyPointSettings.MaximumRedeemPoint, LoyaltyPointSettings.IsActive, LoyaltyPointSettings.IsMaximumRedeem
FROM LoyaltyPointSettings  (nolock)  INNER JOIN Sales (nolock) ON Sales.LoyaltyId = LoyaltyPointSettings.Id  WHERE 
LoyaltyPointSettings.AccountId  =  @AccountId AND Sales.LoyaltyId  =  @LoyaltyId
 AND Sales.Id  =  @SalesId
           
END
