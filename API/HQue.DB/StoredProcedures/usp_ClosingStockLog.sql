/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************/

Create PROCEDURE [dbo].usp_ClosingStockLog(@InstanceId VARCHAR(36), 
                                                 @AccountId  VARCHAR(36)  
                                                 )
AS 
  BEGIN 
 select  Top 1 'Opening/Closing Stock Available only from' + CONVERT (varchar(10), ExecutedDate ,103)  displaytext,* from schedulerlog (nolock) where InstanceId =@InstanceId and AccountId =@AccountId  order by ExecutedDate asc 
 
  
 end
  