﻿/*                               
******************************************************************************                            
** File: [usp_GetLoyaltyPointSettings]   
** Name: [usp_GetLoyaltyPointSettings]                               
**   
** This Loyalty Points can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 13/12/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
*******************************************************************************/ 
 
 -- exec usp_GetLoyaltyPointSettings 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4'
 CREATE  PROCEDURE [dbo].[usp_GetLoyaltyPointSettings](@AccountId varchar(36),@InstanceId varchar(36))
 AS
 BEGIN

--SELECT loyaltyPts.Id,loyaltyPts.LoyaltyPoint,loyaltyPts.StartDate,loyaltyPts.EndDate,loyaltyPts.MinimumSalesAmt,loyaltyPts.RedeemPoint,loyaltyPts.RedeemValue,
--loyaltyPts.MaximumRedeemPoint,loyaltyPts.IsActive,loyaltyPts.LoyaltyOption,loyaltyPts.IsMinimumSale,loyaltyPts.IsMaximumRedeem,loyaltyPts.IsEndDate, 
--loyaltyProductPts.Id,loyaltyProductPts.LoyaltyId,loyaltyProductPts.KindName,loyaltyProductPts.KindOfProductPts from LoyaltyPointSettings (nolock) loyaltyPts
--inner Join LoyaltyProductPoints loyaltyProductPts (nolock) on loyaltyProductPts.LoyaltyId = loyaltyPts.Id
--where loyaltyPts.AccountId = @AccountId and loyaltyPts.InstanceId = @InstanceId and loyaltyPts.IsActive =1 and loyaltyPts.LoyaltyOption =1

SELECT loyaltyPts.Id,loyaltyPts.LoyaltySaleValue,loyaltyPts.LoyaltyPoint,loyaltyPts.StartDate,loyaltyPts.EndDate,loyaltyPts.MinimumSalesAmt,loyaltyPts.RedeemPoint,loyaltyPts.RedeemValue,
loyaltyPts.MaximumRedeemPoint,loyaltyPts.IsActive,loyaltyPts.LoyaltyOption,loyaltyPts.IsMinimumSale,loyaltyPts.IsMaximumRedeem,loyaltyPts.IsEndDate
 from LoyaltyPointSettings (nolock) loyaltyPts
where loyaltyPts.AccountId = @AccountId and loyaltyPts.InstanceId = @InstanceId and loyaltyPts.IsActive =1 and loyaltyPts.LoyaltyOption =1
           
END
