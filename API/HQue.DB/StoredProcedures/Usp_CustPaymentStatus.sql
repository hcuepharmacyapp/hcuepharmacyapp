﻿--Usp_CustPaymentStatus 'c503ca6e-f418-4807-a847-b6886378cf0b', '3e2ec066-1551-4527-be53-5aa3c5b7fb7d', '99448423', 'Lawrence Test cust'
--Usp_CustPaymentStatus 'c503ca6e-f418-4807-a847-b6886378cf0b', '3e2ec066-1551-4527-be53-5aa3c5b7fb7d', '', 'Lawrence Test cust1'

CREATE Proc Usp_CustPaymentStatus(@AccountId char(36), @InstanceId char(36), @Mobile nvarchar(15), @Name varchar(100)) -- @CustId char(36)
AS
Begin

select @Name = ISNULL(@Name,'') select @Mobile = ISNULL(@Mobile,'')

select SUM(a.[CustomerPayment.Debit]) Debit, SUM(a.[CustomerPayment.Credit]) Credit from  --a.Name, a.Mobile,
(
SELECT Sales.Name,Sales.Mobile, ISNULL(SUM(CustomerPayment.Debit),0)  AS [CustomerPayment.Debit], ISNULL(SUM(CustomerPayment.Credit),0) AS [CustomerPayment.Credit] FROM Sales(nolock)
LEFT JOIN CustomerPayment(nolock) ON CustomerPayment.SalesId = Sales.Id  
WHERE Sales.InstanceId=@InstanceId AND Sales.AccountId=@AccountId 
/*AND Sales.Name = @Name
AND Sales.Mobile = @Mobile*/
AND Sales.Name = CASE WHEN @Name = '' THEN Sales.Name ELSE @Name END
AND Sales.Mobile = CASE WHEN @Mobile = '' THEN Sales.Mobile ELSE @Mobile END 
AND (Sales.Cancelstatus IS NULL) 
GROUP BY Sales.Name,Sales.Mobile
union ALL
SELECT Patient.Name,Patient.Mobile, ISNULL(SUM(CustomerPayment.Debit),0) AS [CustomerPayment.Debit], ISNULL(SUM(CustomerPayment.Credit),0) AS [CustomerPayment.Credit] FROM Patient(nolock) 
LEFT JOIN CustomerPayment(nolock) ON CustomerPayment.CustomerId = Patient.Id  
WHERE Patient.InstanceId  =  @InstanceId AND Patient.AccountId  =  @AccountId 
/*AND Patient.Name = @Name
AND Patient.Mobile = @Mobile*/
AND Patient.Name = CASE WHEN @Name = '' THEN Patient.Name ELSE @Name END
AND Patient.Mobile = CASE WHEN @Mobile = '' THEN Patient.Mobile ELSE @Mobile END 
GROUP BY Patient.Name,Patient.Mobile) as a
--group by a.Name, a.Mobile
End