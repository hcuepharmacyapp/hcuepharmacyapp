CREATE PROCEDURE [dbo].[usp_move_syncDataToTemp](@AccountID CHAR(36), @InstanceID CHAR(36),@SeedIndex int)
AS
BEGIN
SET IDENTITY_INSERT dbo.SyncData_Backup ON;  


INSERT INTO SyncData_Backup(Id,AccountId, InstanceId, Action,Data,CreatedAt)
SELECT Id,AccountId, InstanceId, Action,Data,CreatedAt FROM SyncData WHERE accountId=@AccountID AND instanceId=@InstanceID AND instanceId !='' AND Action='I' AND Id <= @SeedIndex

DELETE FROM SyncData WHERE accountId=@AccountID AND instanceId=@InstanceID AND instanceId !='' AND Action='I' AND Id <= @SeedIndex

END