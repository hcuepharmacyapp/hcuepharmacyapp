﻿Create PROCEDURE [dbo].[usp_GetPettyCashDetails](@pettyCashId Varchar(36))
  AS
 BEGIN
   SET NOCOUNT ON
   Declare @timeSlot datetime, @InstanceId CHAR(36),  @UserId CHAR(36), @preId CHAR(36), @preDayBalAmount numeric(18,2), @userName varchar(75)
   Declare @InsName Varchar(150), @DrugLicenseNo varchar(150), @TinNo varchar(150), @GsTinNo varchar(150), @Address varchar(500)


    Select @timeSlot = transactionDate ,
	@InstanceId = InstanceId,
	@UserId = CreatedBy
	 From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId 

		   select top 1 @preId = Id from pettycashhdr 
		   where instanceId = @InstanceId And 
		   CreatedBy = @UserId And
			TransactionDate < @timeSlot
			order by TransactionDate desc

			Select @preDayBalAmount = Isnull([clbalance],0) 
			From [dbo].[PettyCashHdr]
		    Where Id = @preId 

			Select @InsName = Instance.Name ,
					@DrugLicenseNo =	Instance.DrugLicenseNo,
					@TinNo =	Instance.TinNo,
                     @GsTinNo =   Instance.GsTinNo,
                     @Address =   Instance.Address 
			From Instance where Id = @InstanceId

	SELECT @userName = name from hQueUser where id = @UserId

    DECLARE @table1 TABLE
				(
				 SlNo int,
				 Details varchar(150),
				 Inward numeric(18,2),
				 Outward numeric(18,2),
				 Denamination varchar(150),
				 Amount numeric(18,2)

				)

   INSERT into @table1 
   SELECT 1, 'PREVIOUS CLOSING BALANCE', Isnull(@preDayBalAmount,0),NULL, Cast([noof2000] as varchar(50)) + ' X 2000', ([noof2000] * 2000)
   From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId

  
   INSERT into @table1 
   SELECT 2, 'FLOATING AMOUNT', [FloatingAmount],NULL,  Cast([noof1000] as varchar(50)) + ' X 1000', ([noof1000] * 1000)
   From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId

    INSERT into @table1 
   SELECT 3, 'SALES', [TotalSales],NULL, Cast([noof500] as varchar(50)) + ' X 500', ([noof500] * 500)
   From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId

	 INSERT into @table1 
   SELECT 4, 'CUSTOMER RECEIPTS', [CustomerPayment],NULL, Cast([noof200] as varchar(50)) + ' X 200', ([noof200] * 200)
   From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId	

    INSERT into @table1 
   SELECT 5, 'VENDOR PAYMENTS', NULL,[VendorPayment],Cast([noof100] as varchar(50)) + ' X 100', ([noof100] * 100)
   From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId	

    INSERT into @table1 
   SELECT 6, 'EXPENSES', NULL,[TotalExpense],Cast([noof50] as varchar(50)) + ' X 50', ([noof50] * 50)
     From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId	
   

	  INSERT into @table1 
   SELECT NULL, NULL, NULL,NULL,Cast([noof20] as varchar(50)) + ' X 20', ([noof20] * 20)
     From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId	

	  INSERT into @table1 
   SELECT NULL, NULL, NULL,NULL, Cast([noof10] as varchar(50)) + ' X 10', ([noof10] * 10)
     From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId	

    INSERT into @table1 
   SELECT NULL, NULL, NULL,NULL,'Coins', ([coins] )
     From [dbo].[PettyCashHdr]
		Where Id = @pettyCashId
	
	Declare @sumValue Numeric(18,2), @cashOnHand Numeric(18,2)
	
	Select @sumValue =  SUM(Amount) - (SUM(Inward) - SUM(Outward)), @cashOnHand = (SUM(Inward) - SUM(Outward))   From @table1
		
	SELECT SlNo, Details, Isnull(Inward,0) as Inward, Isnull(Outward,0) as Outward,Isnull(Denamination,'') as Denamination, isnull(Amount,0) as  Amount, Isnull(@cashOnHand,0) as Total, Isnull(ABS(@sumValue),0) as difftotal,
	 @InsName as insname, @DrugLicenseNo as drugLicenseno, @TinNo as tinno, @GsTinNo as gstinno, @Address as address,
	CASE WHEN @sumValue < 0 then 'SHORTAGE' ELSE
	CASE  WHEN @sumValue > 0 then  'EXCESS'  ELSE 'TALLIED'  END END as Status, @userName as username

	FROM 	@table1


END