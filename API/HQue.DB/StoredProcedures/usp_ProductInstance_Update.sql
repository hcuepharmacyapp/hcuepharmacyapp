 create proc dbo.usp_ProductInstance_Update(@AccountId char(36),
@InstanceId char(36),
@ProductId char(36),
@ReorderLevel  decimal (18,2),
@ReorderQry decimal (18,2)
 )
 as 
 begin
 update productinstance set ReorderLevel = @ReorderLevel, ReOrderQty = @ReorderQry, UpdatedAt = getdate() where productId = @ProductId
 and AccountID =@AccountId and InstanceId = @InstanceId
  select @ProductId ProductId
 end 

