/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**14/06/17     Mani SP Created to get the generic from sales
**26/06/2017   Sandy		Plus pharmacy InstanceId hardcoded
*******************************************************************************/
CREATE Proc dbo.usp_GetStock_Generic(@AccountId char(36), @InstanceId char(36), @GenericName varchar(100), @ExpDate as Date, @IncludeZeroStock bit)
as
begin
	IF @InstanceId='b7b607a2-e72a-4003-b4b6-ca85123513e7'
		set @IncludeZeroStock=1
	IF @IncludeZeroStock=1
	BEGIN
		SELECT TOP 10 * FROM (
		SELECT 1 AS GROUP_NO, SUM(ProductStock.Stock) AS Stock, Product.GenericName AS [GenericName]
		FROM ProductStock(nolock) INNER JOIN Product (nolock)  ON Product.Id = ProductStock.ProductId  
		WHERE ProductStock.Stock > 0 AND 
		cast(ProductStock.ExpireDate  as date) > @ExpDate
		AND isnull(ProductStock.Status, 1) = (1)
		AND Product.GenericName  Like  @GenericName+ '%'
		 AND (isNull(Product.Status, 1) =  1)
		 AND ProductStock.InstanceId  =  @InstanceId
		 GROUP BY  Product.GenericName  --ORDER BY Product.Name asc 
		 --HAVING SUM(ProductStock.Stock) > 0
		  UNION
		 SELECT 2 AS GROUP_NO, SUM(ProductStock.Stock) AS Stock, Product.GenericName AS [GenericName]
		FROM ProductStock(nolock) INNER JOIN Product (nolock)  ON Product.Id = ProductStock.ProductId  
		WHERE ProductStock.Stock = 0 AND 
		cast(ProductStock.ExpireDate  as date) >  @ExpDate
		AND isnull(ProductStock.Status, 1) = (1)
		AND Product.GenericName Like @GenericName+ '%'
		 AND (isNull(Product.Status, 1) =  1)
		 AND ProductStock.InstanceId  = @InstanceId
		 GROUP BY  Product.GenericName  --ORDER BY Product.Name asc 
		 --HAVING SUM(ProductStock.Stock) = 0
		 ) A ORDER BY GROUP_NO, GenericName
	END
	ELSE
	BEGIN
		
		SELECT TOP 10 SUM(ProductStock.Stock) AS Stock, Product.GenericName AS [GenericName]
		FROM ProductStock(nolock) INNER JOIN Product (nolock)  ON Product.Id = ProductStock.ProductId  
		WHERE ProductStock.Stock > 0 AND 
		cast(ProductStock.ExpireDate  as date) > @ExpDate
		AND isnull(ProductStock.Status, 1) = (1)
		AND Product.GenericName  Like  @GenericName+ '%'
		 AND (isNull(Product.Status, 1) =  1)
		 AND ProductStock.InstanceId  =  @InstanceId
		 GROUP BY  Product.GenericName  --ORDER BY Product.Name asc 
		 --HAVING SUM(ProductStock.Stock) > 0
		 ORDER BY GenericName
	END
 END
