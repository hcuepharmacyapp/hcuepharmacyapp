﻿
/*--======================================================
-- Created By : Lawrence on 23-05-2017
-- Get the NonSaleCustomers List
-- Updated By : Settu on 25-05-2017
--========================================================*/

--usp_Get_NonSaleCustomer 'c503ca6e-f418-4807-a847-b6886378cf0b','3e2ec066-1551-4527-be53-5aa3c5b7fb7d','2017-09-01','2017-09-14'
--usp_Get_NonSaleCustomer 'c503ca6e-f418-4807-a847-b6886378cf0b','3e2ec066-1551-4527-be53-5aa3c5b7fb7d','2017-09-14','2017-09-14'

CREATE PROC usp_Get_NonSaleCustomer(@AccountId VARCHAR(36), @InstanceId VARCHAR(36), @From DATE, @To DATE)
AS
BEGIN

SET NOCOUNT ON

DECLARE @TmpSale TABLE (saleId char(36),patientName VARCHAR(100),patientMobile VARCHAR(15),saleLastDate DATETIME,
InvoiceSeries VARCHAR(15),Prefix VARCHAR(15),InvoiceNo VARCHAR(50),SaleAmount DECIMAL(18,6), InstanceId CHAR(36))

INSERT INTO @TmpSale(patientName,patientMobile,saleLastDate, InstanceId)
SELECT ISNULL(Name,'') Name,ISNULL(Mobile,'') Mobile,MAX(CreatedAt) CreatedAt, InstanceId from Sales (nolock)
WHERE Accountid=@AccountId and Instanceid=ISNULL(@InstanceId,InstanceId)
AND Invoicedate NOT BETWEEN  @From AND @To
GROUP BY ISNULL(Name,''),ISNULL(Mobile,''), InstanceId

UPDATE t SET t.saleId=s.Id,t.InvoiceSeries=s.InvoiceSeries,t.Prefix = s.Prefix,t.InvoiceNo=s.InvoiceNo,
t.SaleAmount=s.SaleAmount FROM @TmpSale t 
INNER JOIN Sales s(nolock) ON ISNULL(Name,'')=patientName AND ISNULL(Mobile,'')=patientMobile AND CreatedAt=saleLastDate

SELECT I.Name as InstanceName, isNull(P.Name, '') as Name, MAX(P.ShortName) ShortName,MAX(P.Area) Area,MAX(isNull(P.City, '')) City,MAX(isNull(P.Address, '')) Address,isNull(P.Mobile, '') as Mobile,MAX(P.Email) Email,
MAX(saleLastDate) AS CreatedAt,S.SaleAmount,
MAX(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')+' '+isNull(InvoiceNo,'')) AS InvoiceNo 
FROM Patient P(nolock)
INNER JOIN @TmpSale S ON patientName=ISNULL(P.Name,'') AND patientMobile=ISNULL(P.Mobile,'')
INNER JOIN Instance I (NOLOCK) ON I.Id = ISNULL(@InstanceId,S.InstanceId)
WHERE P.Accountid=@AccountId AND P.Instanceid=ISNULL(@InstanceId,P.InstanceId)

AND ISNULL(P.Name,'')+ISNULL(P.Mobile,'') NOT IN 
(SELECT DISTINCT ISNULL(Name,'')+ISNULL(Mobile,'') FROM Sales(nolock)
WHERE Accountid=@AccountId AND Instanceid=ISNULL(@InstanceId,InstanceId)
AND Invoicedate BETWEEN @From AND @To) 

AND ISNULL(P.Name,'')<>''
GROUP BY P.Name,P.Mobile,S.saleId,S.SaleAmount, I.Name
ORDER BY P.Name

SET NOCOUNT OFF
END