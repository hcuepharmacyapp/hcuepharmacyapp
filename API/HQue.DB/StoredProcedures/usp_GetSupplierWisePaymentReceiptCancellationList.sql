-- exec usp_GetSupplierWisePaymentReceiptCancellationList '2d5abd54-89a9-4b31-aa76-8ec70b9125dd','c3473243-ad71-4883-8f21-57e196c76e96','e6385f2e-9c9f-4071-8996-915ccc7f5198','01-Mar-2017','08-Mar-2017'
/** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
 22/06/2017    Violet			Prefix Added
*******************************************************************************/ 
Create PROCEDURE [dbo].[usp_GetSupplierWisePaymentReceiptCancellationList](@AccountId varchar(36),@InstanceId varchar(36),@VendorId char(36),@StartDate datetime,@EndDate datetime)
  AS
 BEGIN
   SET NOCOUNT ON

   if (@VendorId ='') select @VendorId  = null
	
	Select a.* from 
(SELECT P.TransactionDate AS PaymentDate,VP.GoodsRcvNo As GRNNo,VP.InvoiceNo As InvoiceNo ,VP.InvoiceDate As InvoiceDate,Ltrim(ISNULL(VP.Prefix,'')+ISNULL(VP.BillSeries,'')) BillSeries,
CAST(Round(Isnull(dbo.CreditCalculate(p.VendorPurchaseId),0),0) As Decimal(10, 2))  As [InvoiceAmount],
Case When P.PaymentType='PurchasePaid' Then P.Debit else 0.00 End  AS [PaidAmount],
CAST(Isnull(Round(dbo.CreditCalculate(P.VendorPurchaseId),0) - P.Debit,0) As Decimal(10, 2)) AS Balance,
ISNULL(P.PaymentMode,'cash') As PaymentMode,P.ChequeNo As [ChequeNo],P.ChequeDate As [ChequeDate], P.PaymentHistoryStatus As PaymentType,P.VendorPurchaseId,
P.BankName,P.BankBranchName,P.IfscCode  FROM Payment P
INNER JOIN VendorPurchase VP ON P.VendorPurchaseId = VP.Id
WHERE P.Accountid=@AccountId and P.InstanceId=@InstanceId and P.VendorId = isnull(@VendorId , P.VendorId) 
and CAST(P.TransactionDate AS date) BETWEEN CAST(isnull(@StartDate  , P.TransactionDate) AS date) and CAST(isnull(@EndDate,P.TransactionDate) AS date)
--cast(P.TransactionDate as date )between isnull(@StartDate, P.TransactionDate) and isnull(@EndDate, P.TransactionDate)
AND P.PaymentHistoryStatus in('Modified','Cancelled')
GROUP BY P.TransactionDate,VP.GoodsRcvNo,VP.InvoiceNo,VP.InvoiceDate,VP.Prefix,VP.BillSeries, P.VendorPurchaseId,P.Debit,P.PaymentMode,P.ChequeNo,P.ChequeDate,P.PaymentType,P.PaymentHistoryStatus,
P.BankName,P.BankBranchName,P.IfscCode
UNION
 SELECT  Max(p.TransactionDate) as PaymentDate, v.Name as GRNNo, 'Op Bal' as InvoiceNo,
				Max(p.TransactionDate) as InvoiceDate,
				'' as BillSeries,  SUM(v.BalanceAmount) as [InvoiceAmount],
				Sum(p.Debit) as [PaidAmount],
				(SUM(p.Credit)- SUM(p.Debit)) as Balance,
				 'cash' as PaymentMode,
				'' as [ChequeNo], null as [ChequeDate], '' as PaymentType,
				'' as VendorPurchaseId,
				'' as BankName, '' as BankBranchName,
				'' as IfscCode
				From Payment p inner join Vendor V ON p.VendorId = v.Id
                Where  p.VendorId = isnull(@VendorId,p.VendorId)
				and CAST(P.TransactionDate AS date) BETWEEN CAST(isnull(@StartDate  , P.TransactionDate) AS date) and CAST(isnull(@EndDate,P.TransactionDate) AS date)
                AND  p.InstanceId = @InstanceId
				AND  p.AccountId = @AccountId
				AND P.PaymentHistoryStatus in('Modified','Cancelled')
                And p.VendorPurchaseId is null
				Group by v.name
				Having (SUM(p.Credit)- SUM(p.Debit)) is not null) as a
Order by a.PaymentDate desc


--HAVING dbo.CreditCalculate(P.VendorPurchaseId) - SUM(P.Debit) > 0 

  
 END

