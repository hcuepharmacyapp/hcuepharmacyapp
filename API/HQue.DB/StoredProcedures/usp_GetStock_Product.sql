
/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**09/05/17     Poongodi R SP Created to get the products from sales
**15/05/17     Mani SP altered to include ProductId in the search for Barcode feature
**13/06/17     Mani SP altered to include zero stocks also
**16/06/17     Mani SP altered to include zero stocks or not based on parameter @IncludeZeroStock
**26/06/2017   Sandy		Plus pharmacy InstanceId hardcoded
**14/07/2017   Poongodi		Union all block removed  (avoid  to display duplicate Product)
**21/07/2017   Poongodi		Max Productid removed, Product code added  all block removed  (avoid  to display duplicate Product)
**26/07/2017   Poongodi		Accountid Validation removed 
**02/10/2017   Arun		Add new parameter@ShowExpiredQty
*******************************************************************************/
CREATE Proc dbo.usp_GetStock_Product(@InstanceId char(36), @ProductName varchar(100), @IncludeZeroStock bit, @Accountid char(36),@ShowExpiredQty int)
as
begin
 set @IncludeZeroStock= 0
	IF @IncludeZeroStock=1
	BEGIN
		SELECT TOP 10 * FROM (
		SELECT 1 AS GROUP_NO, SUM(isnull(ProductStock.Stock,0)) AS Stock , (Product.Id) AS [ProductId], Product.Name [ProductName],
		 isnull(Product.Code,'')  AS [ProductCode],
		max(Product.Schedule) AS [ProductSchedule]
		FROM Product (nolock)     Left JOIN ( SELECT * FROM ProductStock(nolock) WHERE    
		(@ShowExpiredQty=0 and cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date) or 
		@ShowExpiredQty=1 and cast(ProductStock.ExpireDate  as date)<=cast(getdate() as date))
		AND isnull(ProductStock.Status, 1) = (1)  AND ProductStock.InstanceId  =  @InstanceId ) 
		ProductStock ON Product.Id = ProductStock.ProductId  
		 AND (isNull(Product.Status, 1) =  1) 
		WHERE  Product.Accountid =@Accountid
		
		and  ((Product.Id  =  @ProductName) OR (Product.Name  Like  @ProductName+ '%'))
		
		
		 GROUP BY  Product.Name,Product.Id,Product.Code  --ORDER BY Product.Name asc 
		 --HAVING SUM(ProductStock.Stock) > 0
		/*  UNION
		 SELECT 2 AS GROUP_NO, SUM(ProductStock.Stock) AS Stock ,max(Product.Id ) AS [ProductId],Product.Name AS [ProductName],
		max(Product.Schedule) AS [ProductSchedule]
		FROM ProductStock(nolock) INNER JOIN Product (nolock)  ON Product.Id = ProductStock.ProductId  
		WHERE ProductStock.Stock = 0 AND 
		cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date)
		AND isnull(ProductStock.Status, 1) = (1)
		AND ((Product.Id  =  @ProductName) OR (Product.Name  Like  @ProductName+ '%'))
		 AND (isNull(Product.Status, 1) =  1)
		 AND ProductStock.InstanceId  = @InstanceId
		 GROUP BY  Product.Name  --ORDER BY Product.Name asc 
		 --HAVING SUM(ProductStock.Stock) = 0*/
		 ) A ORDER BY GROUP_NO, ProductName,ProductCode
	 END
	 ELSE
	 BEGIN
		SELECT TOP 10 SUM(ProductStock.Stock) AS Stock , (Product.Id) AS [ProductId],Product.Name AS [ProductName],
		max(Product.Schedule) AS [ProductSchedule]
		FROM ProductStock(nolock) INNER JOIN Product (nolock)  ON Product.Id = ProductStock.ProductId
		WHERE ProductStock.Stock > 0 AND 
			(@ShowExpiredQty=0 and cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date) or 
		@ShowExpiredQty=1 and cast(ProductStock.ExpireDate  as date)<=cast(getdate() as date))
		AND isnull(ProductStock.Status, 1) = (1)
		AND ((Product.Id  =  @ProductName) OR (Product.Name  Like  @ProductName+ '%'))
		 AND (isNull(Product.Status, 1) =  1)
		 AND ProductStock.InstanceId  =  @InstanceId
		 GROUP BY  Product.Name ,Product.Id --ORDER BY Product.Name asc 
		 --HAVING SUM(ProductStock.Stock) > 0
		 ORDER BY PRODUCTNAME
	 END
 END