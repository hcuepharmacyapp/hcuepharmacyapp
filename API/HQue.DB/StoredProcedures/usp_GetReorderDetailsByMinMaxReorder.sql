﻿CREATE PROCEDURE [dbo].[usp_GetReorderDetailsByMinMaxReorder](@AccountId CHAR(36), @InstanceId CHAR(36))
AS
BEGIN
	SELECT R5.Id,R5.Name,R5.VendorId,R5.VAT,R5.Igst,R5.Cgst,R5.Sgst,R5.GstTotal,R5.PackageSize,R5.PackagePurchasePrice,R5.PackageMRP,
	R4.ReOrderQty,R4.Stock,R4.MinReorder,R4.MaxReorder,ISNULL(R6.pendingPOQty,0) AS pendingPOQty
	FROM 
	(
		SELECT PINS.ProductId,ISNULL(R1.Stock,0) AS Stock,ISNULL(PINS.ReOrderQty,0)-ISNULL(R1.Stock,0) AS ReOrderQty,
		ISNULL(PINS.ReOrderLevel,0) AS MinReorder,ISNULL(PINS.ReOrderQty,0) AS MaxReorder
		FROM ProductInstance PINS 
		LEFT JOIN
		(	
			SELECT PS.ProductId,SUM(ISNULL(PS.Stock,0)) AS Stock FROM ProductStock PS 
			WHERE PS.AccountId = @AccountId AND PS.InstanceId = @InstanceId	 
			GROUP BY PS.ProductId
		) R1 
		ON R1.ProductId = PINS.ProductId
		WHERE PINS.AccountId = @AccountId AND PINS.InstanceId = @InstanceId	 
		AND ISNULL(R1.Stock,0) <= ISNULL(PINS.ReOrderLevel,0)
		AND (ISNULL(PINS.ReOrderQty,0)-ISNULL(R1.Stock,0)) > 0
	) R4
	INNER JOIN
	(
		SELECT 
		P1.Id,P1.Name,
		ISNULL(R3.VAT,ISNULL(P1.VAT,0)) AS VAT,
		ISNULL(R3.Igst,ISNULL(P1.Igst,0)) AS Igst,
		ISNULL(R3.Cgst,ISNULL(P1.Cgst,0)) AS Cgst,
		ISNULL(R3.Sgst,ISNULL(P1.Sgst,0)) AS Sgst,
		ISNULL(R3.GstTotal,ISNULL(P1.GstTotal,0)) AS GstTotal,
		ISNULL(R3.PackageSize,ISNULL(CASE WHEN P1.PackageSize=0 THEN 1 ELSE P1.PackageSize END,1)) AS PackageSize,
		ISNULL(R3.PackagePurchasePrice,0) AS PackagePurchasePrice,
		ISNULL(R3.PackageMRP,0) AS PackageMRP,
		R3.VendorId
		FROM Product P1
		LEFT JOIN
		(
			SELECT PS1.ProductId,CASE WHEN ISNULL(PS1.CST,0) = 0 THEN ISNULL(PS1.VAT,0) ELSE PS1.CST END AS VAT,
			ISNULL(PS1.Igst,0) AS Igst,	ISNULL(PS1.Cgst,0) AS Cgst,	ISNULL(PS1.Sgst,0) AS Sgst,	ISNULL(PS1.GstTotal,0) AS GstTotal,
			PS1.PackageSize,
			PS1.PackagePurchasePrice,		
			ROUND(PS1.MRP * PS1.PackageSize, 2) AS PackageMRP,
			PS1.VendorId
			FROM ProductStock PS1 
			INNER JOIN 
			(
				SELECT PS2.ProductId,MAX(PS2.CreatedAt) AS CreatedAt FROM ProductStock PS2
				WHERE PS2.AccountId = @AccountId AND PS2.InstanceId = @InstanceId
				GROUP BY PS2.ProductId
			) R2
			ON R2.ProductId = PS1.ProductId AND R2.CreatedAt = PS1.CreatedAt	
			WHERE PS1.AccountId = @AccountId AND PS1.InstanceId = @InstanceId
		) R3 ON R3.ProductId = P1.Id
		WHERE P1.AccountId = @AccountId AND ISNULL(P1.Status,1) = 1 AND ISNULL(P1.IsHidden,0) = 0
	) R5 ON R4.ProductId = R5.Id
	LEFT JOIN
	(
		SELECT SUM(quantity) AS pendingPOQty,ProductId FROM vendororderitem 
		WHERE AccountId = @AccountId AND InstanceId = @InstanceId AND vendorpurchaseid is null 
		and StockTransferId is null and ISNULL(IsDeleted,0) = 0 GROUP BY ProductId
	) R6 ON R6.ProductId = R5.Id
	ORDER BY ISNULL(R6.pendingPOQty,0)
END