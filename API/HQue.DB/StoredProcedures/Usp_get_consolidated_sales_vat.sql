 
     
/*                              
******************************************************************************                            
** File: [Usp_Get_Consolidated_Sales_VAT]  
** Name: [Usp_Get_Consolidated_Sales_VAT]                              
** Description: To Purchase Order Details 
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author: Poongodi R   
** Created Date:   06/02/2017 
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 **07/02/2017  Poongodi     Productid and vendorid parameter added for Purchase 
 **02/03/2017  Poongodi     date validation Changed from Invocie date  to created date
 **21/06/2017   Violet			Prefix Added 
 ** 30/06/2017  Poongodi			TaxRefNovalidation added
 **13/09/2017   Lawrence	All branch condition Added 
*******************************************************************************/ 
 
CREATE PROCEDURE [dbo].[Usp_get_consolidated_sales_vat](@AccountId  CHAR(36), 
                                                        @InstanceId CHAR(36), 
                                                        @StartDate  DATETIME, 
                                                        @EndDate    DATETIME) 
AS 
  BEGIN 
 
 declare @GstDate date ='2017-06-30'
      SELECT Cast (invoicedate AS DATE)            [Invoicedate], 
	  	 (select ltrim(isnull(s1.prefix,'')+isnull(s1.InvoiceSeries, ''))+' '+ s1.invoiceno  from sales s1(nolock) where s1.createdat = min(dt1)   ) +'-'+
					 (select    ltrim(isnull(s2.prefix,'')+isnull(s2.InvoiceSeries, ''))+' '+ s2.invoiceno  from sales s2(nolock) where s2.createdat = max(dt1 ) ) [InvoiceNo],
           
             Sum(Isnull([vat5_salesvalue], 0))     [Vat5_SalesValue], 
             Sum(Isnull([vat5_value], 0))          [Vat5_Value], 
             Sum(Isnull([vat5_salesvalue], 0) 
                 + Isnull([vat5_value], 0))        [Vat5_TotalValue], 
             Sum(Isnull([vat145_salesvalue], 0))   [Vat145_SalesValue], 
             Sum(Isnull([vat145_value], 0))        [Vat145_Value], 
             Sum(Isnull([vat145_salesvalue], 0) 
                 + Isnull([vat145_value], 0))      [Vat145_TotalValue], 
             Sum(Isnull([vat0_salesvalue], 0))     [Vat0_SalesValue], 
             Sum(Isnull(discountval, 0))           [DiscountVal], 
             Sum(Isnull(salevalue, 0))             [SalesValue], 
             Sum(Isnull(vatvalue, 0))              [VatValue] 
      FROM   (SELECT invoicedate, 
                      
					 max(createdat) dt1,
                     Sum(Isnull(invoiceamount, 0) - Isnull(discountval, 0)) 
                     [SaleValue], 
                     Sum(Isnull(invoiceamount, 0) - Isnull(discountval, 0)) - ( 
                     Round( 
                                                                              ( 
                     Sum(Isnull(invoiceamount, 0) - Isnull(discountval, 0)) / 
                               ( 
                     100 
                       + 
                             Isnull( 
                       vat, 0 
                               ) ) * 
                       100 ) 
                           * Isnull(vat, 0) / 100, 2) ) 
                     [VatValue] 
                     , 
                     CASE Isnull(vat, 0) 
                       WHEN 5 THEN Sum(Isnull(invoiceamount, 0) - 
                                       Isnull(discountval, 0)) - 
                                   ( Round(( Sum(Isnull( 
                                             invoiceamount 
                     , 0) - Isnull(discountval, 0)) / 
                                             ( 
                                                 100 + Isnull(vat, 0) ) * 100 ) 
                                           * 
                                           Isnull(vat, 0) / 
                                   100, 2) ) 
                       ELSE 0 
                     END 
                     [Vat5_SalesValue] 
                            , 
                     CASE Isnull(vat, 0) 
                       WHEN 14.5 THEN Sum(Isnull(invoiceamount, 0) - Isnull( 
                                          discountval, 0)) 
                                      - ( 
                                      Round(( Sum(Isnull( 
                                              invoiceamount 
                     , 0) - Isnull(discountval, 0)) / 
                                      ( 
                                                       100 + Isnull(vat, 0) ) * 
                                              100 ) 
                                            * 
                                      Isnull(vat, 0) / 100, 2) ) 
                       ELSE 0 
                     END 
                            [Vat145_SalesValue], 
                     CASE Isnull(vat, 0) 
                       WHEN 0 THEN Sum(Isnull(invoiceamount, 0) - 
                                       Isnull(discountval, 0)) - 
                                   ( Round(( Sum(Isnull( 
                                             invoiceamount 
                     , 0) - Isnull(discountval, 0)) / 
                                             ( 
                                                 100 + Isnull(vat, 0) ) * 100 ) 
                                           * 
                                           Isnull(vat, 0) / 
                                   100, 2) ) 
                       ELSE 0 
                     END 
                     [Vat0_SalesValue] 
                            , 
                     Isnull(vat, 0) 
                            VAT, 
                     CASE Isnull(vat, 0) 
                       WHEN 5 THEN Round(( Sum(Isnull(invoiceamount, 0) - 
                                               Isnull(discountval, 0)) / 
                                                                 ( 
                                           100 + Isnull(vat, 0) ) * 
                                           100 ) * 
                                         Isnull(vat, 0) 
                                                       / 100, 2) 
                       ELSE 0 
                     END 
                     [Vat5_Value], 
                     CASE Isnull(vat, 0) 
                       WHEN 14.5 THEN Round(( Sum(Isnull(invoiceamount, 0) 
                                                  - Isnull( 
                                                  discountval, 0)) 
                                              / 
                                              ( 100 + Isnull(vat, 0) ) * 100 
                                            ) * 
                                                             Isnull(vat, 0) / 
                                            100, 
                                      2) 
                       ELSE 0 
                     END 
                     [Vat145_Value], 
                     Sum(Isnull(discountval, 0)) 
                     [DiscountVal] 
              FROM   (SELECT sales.invoicedate  AS InvoiceDate, 
                             ltrim(isnull(sales.prefix,'')+isnull(sales.InvoiceSeries, ''))  invoiceseries,
                               sales.invoiceno  AS InvoiceNo, 
							 sales.CreatedAt CreatedAt,
							 
                             

                             sales.discount     AS salesDiscount, 
                             sales.NAME, 
                             salesitem.quantity, 
                             salesitem.discount [salesitem.Discount], 
                             ps.vat, 
                             p.NAME             AS ProductName, 
                             'F'                Category, 
                             b.invoiceamount, 
                             sales.discount     AS Discount, 
                             b.discountval 
                      FROM   sales sales (nolock)
                             JOIN salesitem salesitem (nolock)
                               ON sales.id = salesitem.salesid 
                             JOIN productstock ps (nolock)
                               ON ps.id = salesitem.productstockid 
                             INNER JOIN product p (nolock)
                                     ON p.id = ps.productid 
                             CROSS apply (SELECT ( salesitem.quantity * 
                                                   ( 
                                                   Isnull( 
                                                   salesitem.sellingprice, 
                                                   ps.sellingprice) 
                                                      ) * 
                                                   Isnull(salesitem.discount, 0) 
                                                   / 
                                                   100 
                                                 ) + ( 
                                                 Isnull( 
                                                 salesitem.sellingprice 
                             , ps.sellingprice) * 
                             salesitem.quantity * Isnull( 
                             sales.discount, 0) / 100 ) 
                             AS 
                             DiscountVal, 
                             CONVERT(DECIMAL(18, 2), 
                             Isnull(salesitem.sellingprice, ps.sellingprice) 
                             * 
                             salesitem.quantity) 
                             AS InvoiceAmount) AS b
                      WHERE  sales.instanceid = ISNULL(@InstanceId,sales.InstanceId)
                             AND sales.accountid = @AccountId 
                             and isnull(sales.TaxRefNo,0) =0
							 and CONVERT(DATE, sales.createdat) <=@GstDate
                             AND CONVERT(DATE, sales.createdat) BETWEEN 
                                 @StartDate AND @EndDate 
                             AND Isnull(sales.cancelstatus, 0) = 0) AS one 
              GROUP  BY invoicedate ,
                      
                        Isnull(vat, 0)) AS One 
      GROUP  BY Cast(invoicedate AS DATE) 
      ORDER  BY invoicedate 
  END 