﻿/*                               
******************************************************************************                            
** File: [usp_GetSalesOrderEstimateHistoryItemList]   
** Name: [usp_GetSalesOrderEstimateHistoryItemList]                               
** Description: To Get Purchase Audit details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 09/10/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
  
*******************************************************************************/
 
  -- exec usp_GetSalesOrderEstimateHistoryItemList 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4','26f2eddc-0784-42c6-aa6b-310447cd8a41'
 CREATE PROCEDURE [dbo].[usp_GetSalesOrderEstimateHistoryItemList](@AccountId varchar(36),@InstanceId varchar(36),@SalesOrderEstimateId varchar(max))
 AS
 BEGIN
   SET NOCOUNT ON
   

----SELECT SOEI.ProductId as ProductId,SOEI.Quantity,SOEI.SellingPrice,SOEI.SalesOrderEstimateId,SOEI.CreatedAt,PS.SellingPrice AS SellingPrice,PS.MRP AS MRP,
----PS.VAT AS VAT,PS.CST AS CST,PS.BatchNo AS BatchNo,PS.ExpireDate AS [ExpireDate],PS.Stock,PS.PackageSize,PS.PackagePurchasePrice,PS.PurchasePrice,PS.CreatedAt ,
----isnull(PS.TaxType,0) AS TaxType,P.Name[ProductName],
----PS.Igst, PS.Cgst, PS.Sgst, PS.GstTotal
----FROM SalesOrderEstimateitem SOEI WITH(NOLOCK)
----INNER JOIN Product P WITH(NOLOCK) ON P.Id = SOEI.ProductId  
----INNER JOIN ProductStock PS  WITH(NOLOCK) ON  PS.ProductId = P.Id
----WHERE SOEI.InstanceId  = @InstanceId  AND SOEI.AccountId  =  @AccountId
----and SOEI.SalesOrderEstimateId in (select a.id from dbo.udf_Split(@SalesOrderEstimateId ) a)
----ORDER BY SOEI.CreatedAt desc , PS.CreatedAt desc

SELECT SOEI.ProductId as ProductId,SOEI.Quantity,SOEI.SellingPrice,SOEI.SalesOrderEstimateId,SOEI.CreatedAt,SOEI.IsDeleted,ISNULL(P.Name,PM.Name) as [ProductName],
SOEI.BatchNo,SOEI.Discount,SOEI.ExpireDate,SOEI.IsOrder,P.Status,ISNULL(SOEI.ReceivedQty,0) as ReceivedQty FROM SalesOrderEstimateitem SOEI WITH(NOLOCK)
LEFT JOIN Product P WITH(NOLOCK) ON P.Id = SOEI.ProductId 
LEFT JOIN ProductMaster PM WITH(NOLOCK) ON PM.Id = SOEI.ProductId  
WHERE SOEI.InstanceId  = @InstanceId  AND SOEI.AccountId  =  @AccountId
and SOEI.SalesOrderEstimateId in (select a.id from dbo.udf_Split(@SalesOrderEstimateId ) a)
--ORDER BY SOEI.CreatedAt desc 
ORDER BY ISNULL(P.Name,PM.Name)  ASC

           
 END
           
 
