
 /*                              
******************************************************************************                            
** File: [usp_GetInventory_Details]  
** Name: [usp_GetInventory_Details]                              
** Description: To Get Product Stock details   based on ProductId
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author:Poongodi R  
** Created Date: 28/02/2017  
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 **10/05/2017  Poongodi R		Nolock added
 **27/05/2017  Poongodi R		Type parameter added
 **22/06/2017  Annadurai C		Add purchasePrice in the Select and Add the Expired Date check
  ** 29/11/2017 Poongodi			Batchwise listing removed and Qty grouped by Product 
*******************************************************************************/ 
Create Proc usp_GetInventory_Details( @InstanceId nvarchar(36),
@ProductId nvarchar(max),
@Type varchar(100)
)
as
begin
 
if (@Type ='Available')
begin
SELECT (ProductStock.PurchasePrice * ProductStock.Stock) as purchasePrice, ProductStock.Id,ProductStock.ProductId,ProductStock.BatchNo,ProductStock.ExpireDate,ProductStock.Stock,isnull(ProductStock.ReOrderQty,0) [ReOrderQty] , isnull(ProductStock.Status,'1') [Status],ProductStock.InstanceId,Vendor.Name 
AS [VendorName],Vendor.Id AS [Vendor.Id] FROM ProductStock(nolock) LEFT JOIN Vendor (nolock) ON Vendor.Id = ProductStock.VendorId  WHERE ProductStock.InstanceId  =  @InstanceId 
and ProductStock.ProductId in (select id from dbo.udf_Split (@Productid)) and ProductStock.Stock >0 
end 
if (@Type ='Inactive')
begin
SELECT (ProductStock.PurchasePrice * ProductStock.Stock) as purchasePrice, ProductStock.Id,ProductStock.ProductId,ProductStock.BatchNo,ProductStock.ExpireDate,ProductStock.Stock,isnull(ProductStock.ReOrderQty,0) [ReOrderQty] , isnull(ProductStock.Status,'1') [Status],ProductStock.InstanceId,Vendor.Name 
AS [VendorName],Vendor.Id AS [Vendor.Id] FROM ProductStock(nolock) LEFT JOIN Vendor (nolock) ON Vendor.Id = ProductStock.VendorId  WHERE ProductStock.InstanceId  =  @InstanceId 
and ProductStock.ProductId in (select id from dbo.udf_Split (@Productid)) and ProductStock.Stock >0 
end 
else if (@Type ='Zero')
begin
--SELECT (0) as purchasePrice, '' Id ,ProductStock.ProductId, '' BatchNo, ProductStock.ExpireDate [ExpireDate],ProductStock.Stock,
--isnull(ProductStock.ReOrderQty,0) [ReOrderQty] , isnull(ProductStock.Status,'1') [Status],ProductStock.InstanceId,isnull(Vendor.Name ,'') 
--AS [VendorName],isnull(Vendor.Id ,'') AS [Vendor.Id] from (Select max(ExpireDate) [ExpireDate], max(vendorid) vendorid  ,productid,instanceid,accountid, 0 [stock] ,max(Status) [Status] , max(ReOrderQty) ReOrderQty
-- FROM ProductStock(nolock)  where InstanceId  =  @InstanceId  and ProductStock.ProductId in (select id from dbo.udf_Split (@Productid))   group by ProductStock.productid, ProductStock.instanceid,ProductStock.accountid 
-- ) ProductStock
--LEFT JOIN Vendor (nolock) ON Vendor.Id = ProductStock.VendorId  WHERE ProductStock.InstanceId  =  @InstanceId 
--and ProductStock.ProductId in (select id from dbo.udf_Split (@Productid)) and ProductStock.Stock =0

SELECT (ProductStock.PurchasePrice * ProductStock.Stock) as purchasePrice, ProductStock.Id,ProductStock.ProductId,
ProductStock.BatchNo,ProductStock.ExpireDate,ProductStock.Stock,isnull(ProductStock.ReOrderQty,0) [ReOrderQty] , isnull(ProductStock.Status,'1') [Status],ProductStock.InstanceId,Vendor.Name 
AS [VendorName],Vendor.Id AS [Vendor.Id] FROM ProductStock(nolock) LEFT JOIN Vendor (nolock) ON Vendor.Id = ProductStock.VendorId  WHERE ProductStock.InstanceId  =  @InstanceId 
and ProductStock.ProductId in (select id from dbo.udf_Split (@Productid)) and ProductStock.Stock =0

end 

else if (@Type ='Expiry')
begin
SELECT (ProductStock.PurchasePrice * ProductStock.Stock) as purchasePrice, ProductStock.Id,ProductStock.ProductId,ProductStock.BatchNo,ProductStock.ExpireDate,ProductStock.Stock,isnull(ProductStock.ReOrderQty,0) [ReOrderQty] , isnull(ProductStock.Status,'1') [Status],ProductStock.InstanceId,Vendor.Name 
AS [VendorName],Vendor.Id AS [Vendor.Id] FROM ProductStock(nolock) LEFT JOIN Vendor (nolock) ON Vendor.Id = ProductStock.VendorId  WHERE ProductStock.InstanceId  =  @InstanceId 
and ProductStock.ProductId in (select id from dbo.udf_Split (@Productid)) and cast(ProductStock.ExpireDate as date) <= cast(getdate() as date)
end 
else
begin
SELECT (ProductStock.PurchasePrice * ProductStock.Stock) as purchasePrice,
ProductStock.Id,ProductStock.ProductId,ProductStock.BatchNo,ProductStock.ExpireDate,ProductStock.Stock,isnull(ProductStock.ReOrderQty,0) [ReOrderQty] , isnull(ProductStock.Status,'1') [Status],ProductStock.InstanceId,Vendor.Name 
AS [VendorName],Vendor.Id AS [Vendor.Id] FROM ProductStock(nolock) LEFT JOIN Vendor (nolock) ON Vendor.Id = ProductStock.VendorId  WHERE ProductStock.InstanceId  =  @InstanceId 
and ProductStock.ProductId in (select id from dbo.udf_Split (@Productid))
end
end 
 
 