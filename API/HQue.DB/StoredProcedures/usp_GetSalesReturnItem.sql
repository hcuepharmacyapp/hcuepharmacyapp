/****** Object:  StoredProcedure [dbo].[usp_GetSalesReturnItem]    Script Date: 30-Jun-18 2:50:39 PM ******/

/*                               
******************************************************************************                            
** File: [usp_GetSalesReturnItem]   
** Name: [usp_GetSalesReturnItem]                               
** Description: To Get SaleReturnItem details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Sarubala V   
** Created Date: 29/06/2018   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         

 *******************************************************************************/ 
CREATE PROC [dbo].[usp_GetSalesReturnItem] (@SalesReturnId nvarchar(36))
AS 
  BEGIN

  SELECT SalesReturnItem.Id,SalesReturnItem.AccountId,SalesReturnItem.InstanceId,SalesReturnItem.SalesReturnId,SalesReturnItem.ProductStockId,SalesReturnItem.Quantity,SalesReturnItem.CancelType,SalesReturnItem.Discount,SalesReturnItem.DiscountAmount,SalesReturnItem.OfflineStatus,SalesReturnItem.CreatedAt,SalesReturnItem.UpdatedAt,SalesReturnItem.CreatedBy,SalesReturnItem.UpdatedBy,SalesReturnItem.MrpSellingPrice,SalesReturnItem.MRP,SalesReturnItem.IsDeleted,SalesReturnItem.Igst,SalesReturnItem.Cgst,SalesReturnItem.Sgst,SalesReturnItem.GstTotal,SalesReturnItem.GstAmount,SalesReturnItem.TotalAmount,SalesReturnItem.LoyaltyProductPts,ProductStock.Id AS [ProductStock.Id],ProductStock.AccountId AS [ProductStock.AccountId],ProductStock.InstanceId AS [ProductStock.InstanceId],ProductStock.ProductId AS 
[ProductStock.ProductId],ProductStock.VendorId AS [ProductStock.VendorId],ProductStock.BatchNo AS [ProductStock.BatchNo],ProductStock.ExpireDate AS 
[ProductStock.ExpireDate],ProductStock.VAT AS [ProductStock.VAT],ProductStock.TaxType AS [ProductStock.TaxType],ProductStock.SellingPrice AS 
[ProductStock.SellingPrice],ProductStock.MRP AS [ProductStock.MRP],ProductStock.PurchaseBarcode AS [ProductStock.PurchaseBarcode],ProductStock.BarcodeProfileId AS 
[ProductStock.BarcodeProfileId],ProductStock.Stock AS [ProductStock.Stock],ProductStock.PackageSize AS [ProductStock.PackageSize],ProductStock.PackagePurchasePrice AS 
[ProductStock.PackagePurchasePrice],ProductStock.PurchasePrice AS [ProductStock.PurchasePrice],ProductStock.OfflineStatus AS [ProductStock.OfflineStatus],ProductStock.CreatedAt 
AS [ProductStock.CreatedAt],ProductStock.UpdatedAt AS [ProductStock.UpdatedAt],ProductStock.CreatedBy AS [ProductStock.CreatedBy],ProductStock.UpdatedBy AS 
[ProductStock.UpdatedBy],ProductStock.CST AS [ProductStock.CST],ProductStock.IsMovingStock AS [ProductStock.IsMovingStock],ProductStock.ReOrderQty AS 
[ProductStock.ReOrderQty],ProductStock.Status AS [ProductStock.Status],ProductStock.IsMovingStockExpire AS [ProductStock.IsMovingStockExpire],ProductStock.NewOpenedStock AS 
[ProductStock.NewOpenedStock],ProductStock.NewStockInvoiceNo AS [ProductStock.NewStockInvoiceNo],ProductStock.NewStockQty AS [ProductStock.NewStockQty],ProductStock.StockImport 
AS [ProductStock.StockImport],ProductStock.ImportQty AS [ProductStock.ImportQty],ProductStock.Eancode AS [ProductStock.Eancode],ProductStock.HsnCode AS 
[ProductStock.HsnCode],ProductStock.Igst AS [ProductStock.Igst],ProductStock.Cgst AS [ProductStock.Cgst],ProductStock.Sgst AS [ProductStock.Sgst],ProductStock.GstTotal AS 
[ProductStock.GstTotal],ProductStock.ImportDate AS [ProductStock.ImportDate],ProductStock.Ext_RefId AS [ProductStock.Ext_RefId],ProductStock.TaxRefNo AS 
[ProductStock.TaxRefNo],ProductStock.PrvStockQty AS [ProductStock.PrvStockQty],ProductStock.TransactionId AS [ProductStock.TransactionId],Product.Id AS [Product.Id],Product.AccountId AS [Product.AccountId],Product.InstanceId AS [Product.InstanceId],Product.Code AS [Product.Code],Product.Name AS [Product.Name],Product.Manufacturer AS [Product.Manufacturer],Product.KindName AS [Product.KindName],Product.StrengthName AS [Product.StrengthName],Product.Type AS [Product.Type],Product.Schedule AS [Product.Schedule],Product.Category AS [Product.Category],Product.GenericName AS [Product.GenericName],Product.CommodityCode AS [Product.CommodityCode],Product.Packing AS [Product.Packing],Product.OfflineStatus AS [Product.OfflineStatus],Product.Ext_RefId AS [Product.Ext_RefId],Product.CreatedAt AS [Product.CreatedAt],Product.UpdatedAt AS [Product.UpdatedAt],Product.CreatedBy AS [Product.CreatedBy],Product.UpdatedBy AS [Product.UpdatedBy],Product.PackageSize AS [Product.PackageSize],Product.VAT AS [Product.VAT],Product.Price AS [Product.Price],Product.Status AS [Product.Status],Product.RackNo AS [Product.RackNo],Product.ProductMasterID AS [Product.ProductMasterID],Product.ProductOrgID AS [Product.ProductOrgID],Product.ReOrderLevel AS [Product.ReOrderLevel],Product.ReOrderQty AS [Product.ReOrderQty],Product.Discount AS [Product.Discount],Product.Eancode AS [Product.Eancode],Product.IsHidden AS [Product.IsHidden],Product.HsnCode AS [Product.HsnCode],Product.Igst AS [Product.Igst],Product.Cgst AS [Product.Cgst],Product.Sgst AS [Product.Sgst],Product.GstTotal AS [Product.GstTotal],Product.Subcategory AS [Product.Subcategory] FROM SalesReturnItem  (nolock)  INNER JOIN ProductStock (nolock) ON ProductStock.Id = SalesReturnItem.ProductStockId INNER JOIN Product (nolock) ON Product.Id = ProductStock.ProductId  WHERE SalesReturnItem.SalesReturnId  =  @SalesReturnId ORDER BY 1

  END