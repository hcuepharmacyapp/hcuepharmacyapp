/** Date        Author          Description                              
*******************************************************************************        
  **27/10/2017   Sarubala		SP created
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[usp_GetOpeningStockHistory](@InstanceId varchar(36), @AccountId varchar(36), @ProductId varchar(36))

AS
 BEGIN
   SET NOCOUNT ON

   SELECT ps.Id, ps.ProductId, isnull(ps.BatchNo,'') BatchNo, ps.ExpireDate, isnull(ps.VAT,0) VAT, isnull(ps.GstTotal,0) GstTotal, isnull(ps.SellingPrice,0) SellingPrice, isnull(ps.MRP,0) MRP, isnull(ps.Stock,0) Stock, isnull(ps.PackageSize,0) PackageSize, isnull(ps.PackagePurchasePrice,0) PackagePurchasePrice, isnull(ps.PurchasePrice,0) PurchasePrice, isnull(ps.NewStockInvoiceNo,'') NewStockInvoiceNo, isnull(ps.NewStockQty,0) NewStockQty, isnull(p.Name,'') Name,count(si.Id) SalesItemCount,count(sia.Id) SalesItemAuditCount FROM ProductStock ps (nolock)  LEFT JOIN Product p (nolock) ON ps.ProductId = p.Id 
left join SalesItem si on si.ProductStockId=ps.Id and si.AccountId=@AccountId and si.InstanceId=@InstanceId
left join SalesItemAudit sia on sia.ProductStockId=ps.Id and si.SalesId=sia.SalesId and sia.AccountId=@AccountId and sia.InstanceId=@InstanceId
WHERE ps.InstanceId  =  @InstanceId AND ps.AccountId  = @AccountId AND ps.NewOpenedStock  = 1 
AND ps.ProductId = ISNULL(@ProductId, ps.ProductId)

group by ps.Id, ps.ProductId, ps.BatchNo, ps.ExpireDate, ps.VAT, ps.GstTotal, ps.SellingPrice, ps.MRP, ps.Stock, ps.PackageSize, ps.PackagePurchasePrice, ps.PurchasePrice, ps.NewStockInvoiceNo, ps.NewStockQty, p.Id,p.Name,ps.CreatedAt
ORDER BY ps.CreatedAt desc


END



