/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**02/08/17     Poongodi R	 Createdby Parameter,ExternalId added 
 
*******************************************************************************/
Create PROCEDURE sp_importVendorMaster (@AccountID as varchar(36), @InstanceID as varchar(36), @FilePath as varchar(1000), @IsCloud as int,@Createdby char(36)) AS
BEGIN
 
 IF OBJECT_ID(N'tempdb..#tmpVendor', N'U') IS NOT NULL
	DROP TABLE #tmpVendor

 IF OBJECT_ID(N'dbo.tmpVendor', N'U') IS NULL
 BEGIN
  PRINT 'Creating Table tmpVendor'
	 CREATE TABLE tmpVendor
	 (
		[RowREF]		int null,
		[VendorCode]	varchar(50) NOT NULL,
		[VendorName]	varchar(100) NOT NULL,
		[TYPE]			varchar(100) NOT NULL,
		[Address1]		varchar(200) NOT NULL,
		[Address2]		varchar(200) NOT NULL,
		[Pincode]		varchar(20) NOT NULL,
		[City]			varchar(50) NOT NULL,
		[State]			varchar(50) NOT NULL,
		[Country]		varchar(50) NOT NULL,	
		[Phone]			varchar(100) NOT NULL,
		[Mobile]		varchar(50) NOT NULL, 
		[DLNumber]		varchar(50) NOT NULL,
		[TINNumber]		varchar(200) NOT NULL
		) 
 END	

--SELECT * INTO #tmpVendor FROM tmpVendor
TRUNCATE TABLE tmpVendor
Declare @EXTRefId char(36)= Newid()
DECLARE @bulk_cmd varchar(1000);  
IF @IsCloud = 1 
	SET @bulk_cmd = 'BULK INSERT tmpVendor FROM '''+ @FilePath + ''' WITH (DATA_SOURCE=''MyAzureInvoicesContainer'', FORMAT=''CSV'', FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  
ELSE
	SET @bulk_cmd = 'BULK INSERT tmpVendor FROM '''+ @FilePath + ''' WITH (FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  

EXEC(@bulk_cmd);  

SELECT * INTO #tmpVendor FROM tmpVendor

INSERT INTO [dbo].[Vendor]
           ([Id],[AccountId],[InstanceId],[Code],[Name],[Mobile],[Phone],[Address],[Area],[City],[State],[Pincode],[DrugLicenseNo],[TinNo],createdby,UpdatedBy,CreatedAt,UpdatedAt,Ext_RefId)
		   SELECT NEWID(), @AccountID, null, t.VendorCode, RTRIM(LTRIM(t.VendorName)), SUBSTRING(t.Mobile,0,14), SUBSTRING(t.Phone,0,14), t.Address1, t.Address2, t.City, t.State, t.Pincode, SUBSTRING(t.DLNumber,0,49), SUBSTRING(t.TINNumber,0,49),@Createdby,@Createdby,getdate(),getdate(),@EXTRefId from #tmpVendor t

SELECT 'SUCCESS'
		   
END