 

/*                              
******************************************************************************                            
** File: [Usp_vat_poreturn_annx9_rpt]  
** Name: [Usp_vat_poreturn_annx9_rpt]                              
** Description: To Get Puchase and Return  details  
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author:Poongodi R  
** Created Date: 28/01/2017  
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 28/03/2017	Poongodi		Taxtype validation added  
** 30/03/2017  Poongodi			Invoice date changed to created at
** 30/06/2017  Poongodi			TaxRefNovalidation added

*******************************************************************************/ 
CREATE PROC dbo.Usp_vat_poreturn_annx9_rpt(@AccountId  CHAR(36), 
                                           @InstanceId CHAR(36), 
                                           @StartDate  DATETIME, 
                                           @EndDate    DATETIME) 
AS 
  BEGIN 
      /* 
      Commodity code hard coded based on VAT % 
      VAT    Commoditycode 
      5      2044 
      14.5    301 
      0 or other 752 
       
      Category 
      Purchare - R for all vat % 
       
         
      */ 
	  declare @GstDate date ='2017-06-30'
      SELECT [returnno] ReturnNo, 
             [returndate] ReturnDate, 
             [invoicedate] InvoiceDate, 
             [invoiceno] InvoiceNo, 
             [VendorName], 
			  max(isnull(Vendor.State ,''))[VendorState],
			  max(isnull(Vendor.Address ,'')) AS [VendorAddress],
             [tinno] as TinNo, 
             [CST] CST, 
             Sum(Isnull(returnvalue, 0) - Isnull([returnCST], 0)) ReturnValue, 
             Sum(Isnull([returnCST], 0))                          [ReturnCST], 
             Sum(Isnull([povalue], 0) - Isnull ([podiscount], 0)) [POValue], 
             Sum(Isnull([poCST], 0))                              [POCST], 
             CASE Isnull(CST, 0) 
               WHEN 5 THEN '2044' 
               WHEN 14.5 THEN '301' 
               ELSE '752' 
             END 
             [Commoditycode] ,
			  'C Form' [FormType],
			  '' [FormNo]
      FROM   (SELECT vendorreturnitem.quantity, 
                     Isnull(vendorreturnitem.quantity, 0) * Isnull( 
                     vendorreturnitem.returnpurchaseprice, 0)    [ReturnValue], 
                     Isnull(vendorreturnitem.quantity, 0) * Isnull( 
                     vendorreturnitem.returnpurchaseprice, 0) * Isnull( 
                     vendorpurchase.discount, 0) / 
                     100 
                     [ReturnDiscount], 
                     ( ( Isnull(vendorreturnitem.quantity, 0) * Isnull( 
                           vendorreturnitem.returnpurchaseprice, 0) ) / ( 
                         100 + Isnull( productstock.cst, 0) ) * 100 ) * Isnull( 
                     productstock.CST, 0) / 
                     100                                         [ReturnCST], 
                     ( Isnull(vendorpurchaseitem.packageqty, vendorreturnitem.Quantity) - 
                       Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
                     vendorpurchaseitem.packagepurchaseprice, vendorreturnitem.ReturnPurchasePrice) [POValue], 
                     ( Isnull(vendorpurchaseitem.packageqty, vendorreturnitem.Quantity) - 
                       Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
                     vendorpurchaseitem.packagepurchaseprice, vendorreturnitem.ReturnPurchasePrice) * Isnull( 
                     vendorpurchaseitem.discount, 0) 
                     / 100                                       [PODiscount], 
                     ( ( Isnull(vendorpurchaseitem.packageqty, 0) - 
                           Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
                         vendorpurchaseitem.packagepurchaseprice, 0) - 
                     ( ( 
                     Isnull( 
                     vendorpurchaseitem.packageqty, 0) 
                     - 
                       Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
                       vendorpurchaseitem.packagepurchaseprice, 0) * 
                       Isnull(vendorpurchaseitem.discount, 0) / 100 ) ) * 
                     Isnull(productstock.cst, 0) / 
                     100                                         [POCST], 
                     vendorreturn.returnno                       AS [ReturnNo], 
                     vendorreturn.returndate                     AS [ReturnDate] 
                     , 
                     vendorpurchase.vendorid 
                     AS 
                     [VendorPurchase.VendorId] 
                            , 
                     vendorpurchase.invoicedate                  AS 
                            [InvoiceDate], 
                     vendorpurchase.invoiceno                    AS [InvoiceNo], 
                     vendor.NAME                                 AS [VendorName] 
                     , 
                      vendor.Id                                 AS [VendorId] ,
                     productstock.vat                            AS [VAT], 
                     productstock.cst                            AS 
                     [CST] 
              FROM   vendorreturn 
                     INNER JOIN vendorreturnitem 
                             ON vendorreturnitem.vendorreturnid = vendorreturn.id 
                     LEFT JOIN vendorpurchase 
                             ON vendorpurchase.id = vendorreturn.vendorpurchaseid 
                     LEFT JOIN VendorPurchaseItem 
                             ON vendorpurchaseitem.vendorpurchaseid = vendorpurchase.id 
                                AND vendorpurchaseitem.productstockid = vendorreturnitem.productstockid 
                     INNER JOIN vendor 
                             ON vendor.id = vendorreturn.vendorid 
                     INNER JOIN productstock 
                             ON productstock.id = vendorreturnitem.productstockid 
                     INNER JOIN product 
                             ON product.id = productstock.productid 
              WHERE  cast(vendorreturn.CreatedAt as date) BETWEEN @StartDate AND @EndDate 
                     AND vendorreturn.accountid = @AccountId 
                     AND vendorreturn.instanceid = @InstanceId 
					  and isnull(Vendor.TaxType,'Tax')= 'Tax'
                     and isnull(EnableCST ,0) =1 
					  and isnull(vendorreturn.TaxRefNo,0) =0
					   and CONVERT(DATE, vendorreturn.createdat) <=@GstDate
					 ) AS one left join Vendor  on Vendor.id = VendorId
      GROUP  BY [returnno], 
                [returndate], 
                [invoicedate], 
                [invoiceno], 
                [vendorname], 
                [tinno], 
                [cst] 
      ORDER  BY cst, 
                [returnno] 
  END 