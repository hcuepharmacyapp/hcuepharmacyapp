/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 25/10/2017	Sarubala		SP created
** 09/11/2017	Sarubala		Sales count value not coming issue fixed
*******************************************************************************  
--usp_GetSalesSummary 'c6b7fc38-8e3a-48af-b39d-06267b4785b4', 'c503ca6e-f418-4807-a847-b6886378cf0b', '01-Sep-2017', '18-Sep-2017', 'ALL', 0, 0, 0
 */

CREATE PROCEDURE [dbo].[usp_GetSalesSummary](@InstanceId varchar(36),@AccountId varchar(36),@StartDate date,@EndDate date,
 @InvoiceSeries varchar(100), @FromInvoice VARCHAR(50), @ToInvoice VARCHAR(50), @paymentMode int)
 AS
 BEGIN
   SET NOCOUNT ON 
   IF @InstanceId = ''
	SET @InstanceId = NULL

	IF @StartDate = ''
	SET @StartDate = NULL
	IF @EndDate = ''
	SET @EndDate = NULL

	SELECT @InvoiceSeries = ISNULL(@InvoiceSeries,'')
	DECLARE @XmlList XML
	SET @XmlList = CAST(('<A>'+REPLACE(@InvoiceSeries,',','</A><A>')+'</A>') AS XML)

	IF @FromInvoice = '' OR @FromInvoice = 0
	SET @FromInvoice = NULL
	IF @ToInvoice = '' OR @ToInvoice = 0
	SET @ToInvoice = NULL

select InstanceName,convert(date,InvoiceDate) invoicedate,count(id) salescount,
sum([cash]) [cash], 
sum([card]) [card], 
sum([cheque]) [cheque], 
sum([credit]) [credit],
sum([ewallet]) [ewallet],
sum(sellingprice) sellingprice, sum(purchaseprice) purchaseprice,
sum(returnedPrice) returnedPrice, sum(ReturnPurchasePrice) ReturnPurchasePrice,
SUM(d.ProfitAmount) AS ProfitAmount,
SUM(d.NetValue) AS NetValue,
SUM(d.ProfitAmount)/SUM(d.NetValue)*100 AS ProfitPerc
 from 
 (select
 i.Name InstanceName,s.InvoiceDate,s.id,
case s.PaymentType when 'Multiple' then  f1.cash else 
  (case s.PaymentType when 'Cash' then salesitem.sellingprice else 0 end) end [cash], 
case s.PaymentType when 'Multiple' then f1.[card] else
 (case s.PaymentType when 'Card' then salesitem.sellingprice else 0 end) end [card], 
case s.PaymentType when 'Multiple' then f1.cheque else
 (case s.PaymentType when 'Cheque' then salesitem.sellingprice else 0 end) end [cheque], 
case s.PaymentType when 'Multiple' then f1.credit else isnull(customerpayment.credit,0) end [credit],
isnull(f1.ewallet,0) [ewallet] ,
salesitem.sellingprice AS sellingprice, purchaseprice, 0 returnedPrice, 0 ReturnPurchasePrice
from
Sales s(nolock)
inner join (
select si.salesid,
--sum(sprice.price1) as sellingprice,sum(sprice.purchaseprice) purchaseprice
s.SaleAmount as sellingprice,sum(sprice.purchaseprice) purchaseprice

from  SalesItem as si(nolock) 
inner join (Select * from ProductStock (nolock) where AccountId = @AccountId and InstanceId = ISNULL(@InstanceId,InstanceId)) as ps on ps.Id = si.ProductStockId 
inner join (select * from sales(nolock) where AccountId = @AccountId and InstanceId = ISNULL(@InstanceId,InstanceId)) as s on s.Id = si.SalesId
 
--cross apply (select (isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) -((isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) * (isnull(s.Discount,0) + ISNULL(si.Discount,0))/100) price1, (isnull(ps.PurchasePrice,0) * si.Quantity) purchaseprice) as sprice 
cross apply (select (ps.PurchasePrice * si.Quantity) purchaseprice) as sprice 

group by si.SalesId,s.SaleAmount) as salesitem on salesitem.SalesId = s.id 
inner join (select * from Instance (nolock) where accountid= @AccountId) i on i.Id=s.InstanceId
left join dbo.udf_getPayments(@AccountId,@InstanceId,@StartDate,@EndDate) f1 on f1.SalesId=s.Id
left join ( 
select cp.SalesId,sum(cp.Credit) as credit from CustomerPayment as cp(nolock) left join Sales as s on s.id = cp.SalesId group by cp.SalesId	) as customerpayment on customerpayment.SalesId = s.Id
where s.InvoiceDate between ISNULL(@StartDate,s.InvoiceDate) AND ISNULL(@EndDate,s.InvoiceDate) 
and isnull(s.Cancelstatus,0) = 0
and s.InstanceId =ISNULL(@InstanceId,s.InstanceId) 
AND ISNULL(s.InvoiceSeries,'') = 
(
	CASE 
		WHEN ISNULL(s.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(s.InvoiceSeries,'')
		WHEN @InvoiceSeries = 'ALL' THEN ISNULL(s.InvoiceSeries,'')
	END
)
AND s.InvoiceNo BETWEEN ISNULL(@FromInvoice,s.InvoiceNo) AND ISNULL(@ToInvoice,s.InvoiceNo)

union all

select
 i.Name InstanceName,sr.ReturnDate InvoiceDate,sr.id,
0 [cash], 0 [card], 0 [cheque], 0 [credit], 0 [ewallet] , 0 sellingprice,0 purchaseprice, 
--round(Convert(decimal(18,2), ((ISNULL(sri.MrpSellingPrice,SellingPrice)*sri.Quantity) -					  						  
--				(((ISNULL(sri.MrpSellingPrice,SellingPrice) * (isnull(s.Discount ,0)+isnull(sri.Discount,0)))/100)*sri.Quantity ))),0) returnedPrice, 
				sr.NetAmount-ISNULL(sr.ReturnCharges,0) AS returnedPrice,
				sum(ps.PurchasePrice * sri.Quantity) AS ReturnPurchasePrice
from
SalesReturn sr(nolock)
left join (select * from Sales(nolock) where AccountId = @AccountId and InstanceId = ISNULL(@InstanceId,InstanceId)) s on s.Id=sr.SalesId 
inner join (select * from SalesReturnItem(nolock) where AccountId = @AccountId and InstanceId = ISNULL(@InstanceId,InstanceId)) sri on sri.SalesReturnId=sr.Id
inner join (select * from ProductStock(nolock) where AccountId = @AccountId and InstanceId = ISNULL(@InstanceId,InstanceId)) ps on ps.Id=sri.ProductStockId
inner join (select * from Instance(nolock) where AccountId = @AccountId) i on i.Id=sr.InstanceId
where convert(date,sr.ReturnDate) between ISNULL(@StartDate,ReturnDate) AND ISNULL(@EndDate,ReturnDate) 
and sr.InstanceId = ISNULL(@InstanceId,sr.InstanceId) and sr.AccountId =@AccountId 
and ISNULL(s.Cancelstatus,0) = 0 and (sri.IsDeleted is null or sri.IsDeleted != 1)
GROUP BY i.Name,sr.id,sr.ReturnDate,sr.NetAmount,ISNULL(sr.ReturnCharges,0)
) A
CROSS APPLY (SELECT (sellingprice-returnedPrice) AS NetValue,(sellingprice-purchaseprice)-(returnedPrice-ReturnPurchasePrice) AS ProfitAmount) d
group by invoicedate,InstanceName
order by InstanceName, invoicedate desc

END