/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**27/10/17     Poongodi R		Invoice series and Prefix added
**01/11/17     Sarubala V		Multiple payment included
*******************************************************************************/
Create PROCEDURE [dbo].[usp_GetCustomerPayment](@InstanceId varchar(36),@AccountId varchar(36),@Name varchar(500),@Mobile varchar(50),@StartDate datetime,@EndDate datetime)
AS
BEGIN
	SET NOCOUNT ON
	
	if (@StartDate ='') select @StartDate  = null
	if (@EndDate ='') select @EndDate  = null
	if (@Name ='') select @Name  = null
	if (@Mobile ='') select @Mobile  = null
	if (@InstanceId ='') select @InstanceId  = null

	Select a.* from
	(
		select id,
		PaymentDate,
		Name,
		Mobile,
		BillNo,
		BillDate,
		BillAmount as BillAmount,
		sum(PaidAmount) as PaidAmount,
		sum(BillAmount)-sum(PaidAmount) as BalanceAmount,
		ChequeNo,
		ChequeDate,
		CardNo,
		CardDate,
		PaymentType
		from 
		(
			SELECT S.Id,
			CAST(CP.createdat AS DATE) AS PaymentDate,
			ISNULL(S.Name,'') AS Name,
			ISNULL(S.Mobile,'') AS Mobile,
			isnull(S.Prefix,'')+Isnull(S.InvoiceSeries,'')+ S.InvoiceNo AS BillNo,
			CAST(S.InvoiceDate AS DATE) AS BillDate,
			--Convert(decimal(18, 2),sum((CASE  WHEN SI.SellingPrice > 0 THEN SI.SellingPrice ELSE PS.SellingPrice END) * SI.Quantity - ((CASE WHEN SI.SellingPrice > 0 THEN SI.SellingPrice ELSE PS.SellingPrice END ) * SI.Quantity * (CASE WHEN SI.Discount > 0 THEN SI.Discount ELSE S.Discount END) /100) )) AS BillAmount,
			S.SaleAmount AS BillAmount,
			CP.debit as PaidAmount, 
			CP.Credit as BalanceAmount,
			CP.ChequeNo,
			CP.ChequeDate,
			CP.CardNo,
			CP.CardDate,
			CP.PaymentType
			FROM SalesItem as SI WITH(NOLOCK) 
			inner join Sales as S WITH(NOLOCK) on SI.SalesId = S.Id
			inner join ProductStock as PS WITH(NOLOCK) on SI.ProductStockId  = PS.Id
			inner join CustomerPayment as CP WITH(NOLOCK) on CP.SalesId = S.Id
			left join (select * from SalesPayment WITH(NOLOCK) where AccountId=@AccountId and InstanceId = ISNULL(@InstanceId,InstanceId) and PaymentInd = 4 and IsActive = 1) sp on sp.SalesId=s.Id
			WHERE CP.Accountid=@AccountId and CP.InstanceId=ISNULL(@InstanceId,CP.InstanceId) 
			AND CAST(CP.createdat AS date) BETWEEN CAST(isnull(@StartDate,CP.createdat) AS date) and CAST(isnull(@EndDate,CP.createdat) AS date)
			AND ISNULL(S.Name,'')=isnull(@Name, ISNULL(S.Name,'')) AND ISNULL(S.Mobile,'')=isnull(@Mobile,ISNULL(S.Mobile,''))
			AND (S.Cancelstatus IS NULL) AND S.Name IS NOT NULL 
			AND (S.Credit IS NOT NULL or (s.PaymentType = 'Multiple' and sp.PaymentInd= 4)) AND CP.debit>0 
			GROUP BY S.Id,S.SaleAmount,CAST(CP.createdat AS DATE),CAST(S.InvoiceDate AS date),ISNULL(S.Name,''),ISNULL(S.Mobile,''),isnull(S.Prefix,'')+Isnull(S.InvoiceSeries,'')+ S.InvoiceNo,CP.debit,CP.Credit,CP.ChequeNo,CP.ChequeDate,CP.CardNo,CP.CardDate,CP.PaymentType
		) a 
		group by id,PaymentDate,Name,Mobile,BillNo,BillDate,BillAmount,ChequeNo,ChequeDate,CardNo,CardDate,PaymentType

		UNION

		Select 
		P.Id, 
		CAST(CP.createdat AS DATE) as PaymentDate, 
		ISNULL(P.Name,'') AS Name,
		ISNULL(P.Mobile,'') AS Mobile, 
		'Op Bal' as BillNo, 
		CP.TransactionDate as BillDate, 
		sum(CP.Credit) as BillAmount, 
		sum(CP.Debit) as PaidAmount,
		sum(CP.Credit)-sum(CP.Debit) as BalanceAmount,
		'' as ChequeNo, 
		Null as ChequeDate,
		'' as CardNo,
		Null as CardDate,
		'' as PaymentType
		From CustomerPayment CP WITH(NOLOCK) INNER JOIN Patient P WITH(NOLOCK) ON CP.CustomerId = P.Id
		WHERE CP.Accountid=@AccountId and CP.InstanceId=ISNULL(@InstanceId,CP.InstanceId) AND ISNULL(P.Name,'')=isnull(@Name,ISNULL(P.Name,'')) AND ISNULL(P.Mobile,'')=isnull(@Mobile,ISNULL(P.Mobile,''))
		AND CP.SalesId Is NULL AND CP.debit>0
		Group By P.ID, CP.TransactionDate,ISNULL(P.Name,''),ISNULL(P.Mobile,''),CAST(CP.createdat AS DATE)	
	) as a 
	Order by a.BillDate desc

	SET NOCOUNT OFF
END