/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**02/08/17     Poongodi R	 Createdby Parameter,ExternalId added 
 
*******************************************************************************/
Create PROCEDURE sp_importpurchasehistory (@AccountID as varchar(36), @InstanceID as varchar(36), @FilePath as varchar(1000), @IsCloud as int,@Createdby char(36)) AS
BEGIN

IF OBJECT_ID(N'tempdb..tmpPurHist', N'U') IS NOT NULL
 DROP TABLE #tmpPurHist

 IF OBJECT_ID(N'dbo.tmpPurHist', N'U') IS NULL
 BEGIN
  PRINT 'Creating Table tmpPurHist'
	 CREATE TABLE tmpPurHist
	 (
		[RowREF]		int null,
		[VendorName]	varchar(100) NOT NULL ,
		[GoodsRcvNo]	varchar(50) NULL,
		[InvoiceNo]		varchar(50) NOT NULL,
		[InvoiceDate]	varchar(16) NULL,
		[Discount]		decimal(5, 2) NULL,		
		[NetAmount]		decimal(18, 3) NULL,
		[ProductName]	varchar(200) NOT NULL,
		[PackageSize]   decimal(10, 2) NULL,
		[PackagePurchasePrice] decimal(18, 2) NULL,
		[FreeQty]		int NULL,
		[VAT]           decimal(9, 2) NULL,
		[CST]           decimal(9, 2) NULL,
		[ExpireDate]	varchar(16) NULL,
		[BatchNo]		varchar(100) NOT NULL,
		[MRP]			decimal(18, 2) NOT NULL,
		[Discount2]		decimal(5, 2) NULL,
		[Qty]			decimal(18, 2) NOT NULL
	 ) 
 END	

--SELECT * INTO #tmpPurHist from tmpPurHist

TRUNCATE TABLE tmpPurHist

Declare @EXTRefId char(36)= Newid()

BEGIN TRANSACTION

DECLARE @bulk_cmd varchar(1000);  
IF @IsCloud = 1 
	SET @bulk_cmd = 'BULK INSERT tmpPurHist FROM '''+ @FilePath + ''' WITH (DATA_SOURCE=''MyAzureInvoicesContainer'', FORMAT=''CSV'',FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  
ELSE
	SET @bulk_cmd = 'BULK INSERT tmpPurHist FROM '''+ @FilePath + ''' WITH (FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  

EXEC(@bulk_cmd);  

SELECT * INTO #tmpPurHist from tmpPurHist

--Products 
INSERT INTO product (id, accountID, name,createdby,updatedby) SELECT NEWID(), @AccountID, ProductName,@Createdby,@Createdby FROM (SELECT distinct productname from #tmpPurHist WHERE ProductName NOT IN (SELECT name from PRODUCT WHERE accountid = @AccountID)) t

--Vendors
INSERT INTO vendor (id, accountID, code, name,Createdby,UpdatedBy) SELECT NEWID(), @AccountID, 'Vendor000', vendorname,@Createdby,@Createdby from #tmpPurHist where vendorname NOT IN (select name from vendor WHERE accountId = @AccountID) group by vendorname


--VendorPurchase
INSERT INTO [dbo].[VendorPurchase]
           ([Id],[AccountId],[InstanceId],[VendorId],[InvoiceNo],[InvoiceDate],[Discount],[GoodsRcvNo],[TotalPurchaseValue],[NetValue],CreatedBy,UpdatedBy)
	 SELECT  NEWID(), @AccountID, @InstanceID, id VendorId, InvoiceNo, InvoiceDate, t.Discount, GoodsRcvNo, NetAmount, NetAmount,@Createdby,@Createdby   FROM  (select distinct InvoiceNo, InvoiceDate, Discount, NetAmount, vendorname, GoodsRcvNo  from #tmpPurHist) t INNER JOIN  vendor ON t.VendorName = Name 
	 WHERE vendor.accountId = @AccountID 
	 order by GoodsRcvNo

--ProductStock
INSERT INTO [dbo].[ProductStock]
           ([Id],[AccountId],[InstanceId],[ProductId],[VendorId],[BatchNo],[ExpireDate],[PackageSize],[PackagePurchasePrice],[PurchasePrice],[VAT],[CST],[SellingPrice],[Stock],[StockImport],[Eancode],CreatedBy, UpdatedBy,Ext_RefId)
			SELECT NEWID(), @AccountID, @InstanceID, p.id productid, v.id VendorId, t.BatchNo, ISNull(t.ExpireDate, getdate()), t.PackageSize, t.PackagePurchasePrice, CAST(isnull(t.PackagePurchasePrice,1)/ isnull(t.PackageSize,1) AS decimal(18,3)), t.VAT, t.CST, CAST(t.MRP/ISNULL(t.PackageSize,1) AS decimal(18,3)), 0, 1,RowREF ,@Createdby,@Createdby,@EXTRefId
			FROM #tmpPurHist t INNER JOIN vendor v ON v.Name = t.VendorName INNER JOIN product p ON p.Name = t.ProductName WHERE p.accountId = @AccountID
			

INSERT INTO [dbo].[VendorPurchaseItem]
           ([Id],[AccountId],[InstanceId],[VendorPurchaseId],[ProductStockId],[PackageSize],[PackageQty],[PackagePurchasePrice],[PackageSellingPrice],[Quantity],[PurchasePrice],[FreeQty],[Discount],CreatedBy, UpdatedBy)
	SELECT  NEWID(), @AccountID, @InstanceID, vp.id, ps.id, t.PackageSize, t.Qty + t.FreeQty, t.PackagePurchasePrice, t.MRP, t.PackageSize * (t.Qty + t.FreeQty), CAST(isnull(t.PackagePurchasePrice,1)/ isnull(t.PackageSize,1) AS decimal(18,3)), t.FreeQty, t.Discount + t.Discount2,@Createdby,@Createdby
	FROM #tmpPurHist t INNER JOIN vendorpurchase vp ON vp.InvoiceNo = t.InvoiceNo INNER JOIN productstock ps ON ps.Eancode = t.RowREF WHERE ps.accountId = @AccountID AND ps.instanceId = @InstanceID

COMMIT TRANSACTION

SELECT 'SUCCESS'

END

