﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 02/12/2017  Gavaskar			Creted
** 05/12/2017  nandhini			modified
*******************************************************************************/ 
-- usp_Get_TaxValues 'c503ca6e-f418-4807-a847-b6886378cf0b',
create procedure [dbo].[usp_Get_TaxValues](
@Accountid CHAR(36),
@Type  VARCHAR(36)
)
as
begin
	if @Type ='all'
	(SELECT id,AccountId,TaxGroupId,Tax,IsActive FROM TaxValues (nolock) where AccountId  =@Accountid) 
	order by Tax

	else

(SELECT id,AccountId,TaxGroupId,Tax,IsActive FROM TaxValues (nolock) where AccountId  =@Accountid and 
isnull(IsActive,0)=0)

order by Tax
end
