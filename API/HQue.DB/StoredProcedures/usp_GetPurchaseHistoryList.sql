/*                               
******************************************************************************                            
** File: [usp_GetPurchaseHistoryList]   
** Name: [usp_GetPurchaseHistoryList]                               
** Description: To Get Purchase Audit details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 09/03/20017	Poongodi R		Top 10 Selection Implemented  
** 10/03/20017	Poongodi R		Replaced Createdat filter to Invoice date (Input given by Siva)
** 10/04/20017	Poongodi R		Order by changed from Created at to GRN Number 
** 13/06/2017	Poongodi R		Numeric changed to Bigint
** 14/06/2017	Poongodi R		Prefix added
 ** 22/06/2017	Poongodi R		Prefix added in Filtercondition
  ** 4/09/2017	Nandhini	exact Filter search
  ** 25/09/2017	Nandhini	Vendor Id Filter search
 *******************************************************************************/ 

 
 -- exec usp_GetPurchaseHistoryList '18204879-99ff-4efd-b076-f85b4a0da0a3','013513b1-ea8c-4ea8-9fed-054b260ee197'
 CREATE  PROCEDURE [dbo].[usp_GetPurchaseHistoryList](@AccountId varchar(36),@InstanceId varchar(36),@PageNo int,@PageSize int,@SearchColName varchar(50), @SearchOption varchar(50),
 @SearchValue varchar(50), @fromDate varchar(15),@Todate varchar(15))
 AS
 BEGIN
   SET NOCOUNT ON

  Declare @isDetail int = 0

  Declare @BtchNo varchar(150) = null, 
   @invoiceNo varchar(150) = null
   , @grNo varchar(150) = null
   ,@vendorId varchar(150) = null
   ,@From_ExpiryDt date = null
   , @To_ExpiryDt date = null, 
  @billDate date = null, 
  @product varchar(150) = '',
  @expiry date = null,
  @email varchar(150) = null,
  @mobile varchar(150) = null,
  @From_BillDt date = null, 
  @To_BillDt date = null

  IF (@SearchOption='top10') 
	begin 

	 set @isDetail  = 3
	end 
 else
 begin
  if (@SearchColName ='batchNo')
	select  @BtchNo = isnull(@SearchValue,''), @isDetail  =1

else  if (@SearchColName ='invoiceNo')
	select  @invoiceNo =   isnull(@SearchValue,'') 
  
  else   if (@SearchColName ='grNo')
	select  @grNo =  isnull(@SearchValue,'') 

  else   if (@SearchColName ='vendorName')
	select  @vendorId =   isnull(@SearchValue,'') 
  else   if (@SearchColName ='email')
	select  @email =   isnull(@SearchValue,'') 
    else   if (@SearchColName ='mobile')
	select  @mobile =   isnull(@SearchValue,'') 
	else if (@SearchColName ='product')
		
		select @product = isnull(@SearchValue,''), @isDetail =1

	else if (@SearchColName ='expiry')

	select @From_ExpiryDt =  @fromDate ,@To_ExpiryDt = @Todate , @isDetail =1

	--begin
	--if (@SearchOption='equal')
	--	select @From_ExpiryDt =  cast (@SearchValue as date)    ,@To_ExpiryDt =  cast (@SearchValue as date ), @isDetail =1
	--else if (@SearchOption='greater')
	--		select @From_ExpiryDt =  dateadd(d,1, cast (@SearchValue as date))     ,@To_ExpiryDt =  NULL, @isDetail =1
	--else if (@SearchOption='less')
	--		select @From_ExpiryDt =  null     ,@To_ExpiryDt =  dateadd(d,-1, cast (@SearchValue as date)), @isDetail =1
	--end 

	else if (@SearchColName ='billDate')

	select @From_BillDt =  @fromDate ,@To_BillDt = @Todate

end
	--begin
	--if (@SearchOption='equal')
	--	select @From_BillDt =  cast (@SearchValue as date)    ,@To_BillDt =  cast (@SearchValue as date )
	--else if (@SearchOption='greater')
	--		select @From_BillDt =  dateadd(d,1, cast (@SearchValue as date))     ,@To_BillDt =  NULL
	--else if (@SearchOption='less')
	--		select @From_BillDt =  null     ,@To_BillDt =  dateadd(d,-1, cast (@SearchValue as date))
	--end  

IF (@isDetail =0)
BEGIN
print 1

SELECT COUNT(1) OVER() PurchaseCount, VendorPurchase.Id,VendorPurchase.InvoiceNo,VendorPurchase.InvoiceDate,VendorPurchase.Discount,VendorPurchase.GoodsRcvNo,VendorPurchase.Comments,
VendorPurchase.GoodsRcvNo,VendorPurchase.FileName,VendorPurchase.PaymentType,VendorPurchase.ChequeNo,VendorPurchase.ChequeDate,VendorPurchase.CreditNoOfDays, isnull(VendorPurchase.prefix,'')+ isnull(VendorPurchase.BillSeries,'') BillSeries,
VendorPurchase.Credit,VendorPurchase.CreatedAt,VendorPurchase.NoteAmount,VendorPurchase.NoteType,Vendor.Id [VendorId] ,Vendor.Name [VendorName], Vendor.Status, Vendor.Email,
Vendor.Mobile  ,Vendor.EnableCST ,
isnull(VendorPurchase.CancelStatus ,0) [CancelStatus],ISNULL(VendorPurchase.TaxRefNo,0) AS TaxRefNo,VendorPurchase.NetValue FROM VendorPurchase LEFT JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId  
WHERE VendorPurchase.AccountId  =  @AccountId AND VendorPurchase.InstanceId  =  @InstanceId
and isnull(Vendor.Id ,'')  = isnull(@vendorId,isnull(Vendor.Id ,'') ) 
and isnull(Vendor.Mobile ,'') = isnull(@mobile,isnull(Vendor.Mobile ,''))
and isnull( Vendor.Email,'')  = isnull(@email,isnull( Vendor.Email,'') ) 
and ( (ISNULL(VendorPurchase.Prefix,'') +  ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,''))  = isnull(@grNo,(ISNULL(VendorPurchase.Prefix,'') +  ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,'')) ) 
or
 (ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,''))  = isnull(@grNo, (ISNULL(VendorPurchase.BillSeries,'') + isnull(VendorPurchase.GoodsRcvNo,'')) ) )
and isnull(VendorPurchase.InvoiceNo,'') = isnull(@invoiceNo,isnull(VendorPurchase.InvoiceNo,''))
and cast(VendorPurchase.InvoiceDate as date )between isnull(@From_BillDt, InvoiceDate) and isnull(@To_BillDt, InvoiceDate)
ORDER BY  (VendorPurchase.createdat  )  desc  , cast(VendorPurchase.GoodsRcvNo as numeric(10,0)) desc OFFSET @PageNo ROWS FETCH NEXT @PageSize ROWS ONLY

END
else  IF (@isDetail =3)
BEGIN
print 1

SELECT 10  PurchaseCount, VendorPurchase.Id,VendorPurchase.InvoiceNo,VendorPurchase.InvoiceDate,VendorPurchase.Discount,VendorPurchase.GoodsRcvNo,VendorPurchase.Comments,
VendorPurchase.GoodsRcvNo,VendorPurchase.FileName,VendorPurchase.PaymentType,VendorPurchase.ChequeNo,VendorPurchase.ChequeDate,VendorPurchase.CreditNoOfDays,isnull(VendorPurchase.prefix,'')+ isnull(VendorPurchase.BillSeries,'') BillSeries,
VendorPurchase.Credit,VendorPurchase.CreatedAt,VendorPurchase.NoteAmount,VendorPurchase.NoteType,Vendor.Id [VendorId] ,Vendor.Name [VendorName], Vendor.Status, Vendor.Email,
Vendor.Mobile  ,Vendor.EnableCST ,
isnull(VendorPurchase.CancelStatus ,0) [CancelStatus],ISNULL(VendorPurchase.TaxRefNo,0) AS TaxRefNo,VendorPurchase.NetValue FROM VendorPurchase LEFT JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId  
WHERE VendorPurchase.AccountId  =  @AccountId AND VendorPurchase.InstanceId  =  @InstanceId
ORDER BY     (VendorPurchase.createdat  )  desc , cast(VendorPurchase.GoodsRcvNo as bigint) desc OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY

END
ELSE
BEGIN
print 2

SELECT COUNT(1) OVER() PurchaseCount, VendorPurchase.Id,VendorPurchase.InvoiceNo,VendorPurchase.InvoiceDate,VendorPurchase.Discount,VendorPurchase.GoodsRcvNo,VendorPurchase.Comments,
VendorPurchase.GoodsRcvNo,VendorPurchase.FileName,VendorPurchase.PaymentType,VendorPurchase.ChequeNo,VendorPurchase.ChequeDate,VendorPurchase.CreditNoOfDays,isnull(VendorPurchase.prefix,'')+ isnull(VendorPurchase.BillSeries,'') BillSeries,
VendorPurchase.Credit,VendorPurchase.CreatedAt,VendorPurchase.NoteAmount,VendorPurchase.NoteType,Vendor.Id [VendorId] ,Vendor.Name [VendorName], Vendor.Status, Vendor.Email,
Vendor.Mobile ,Vendor.EnableCST ,isnull(VendorPurchase.CancelStatus ,0) [CancelStatus],ISNULL(VendorPurchase.TaxRefNo,0) AS TaxRefNo,VendorPurchase.NetValue FROM VendorPurchase LEFT JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId  
WHERE VendorPurchase.AccountId  =  @AccountId AND VendorPurchase.InstanceId  =  @InstanceId
and VendorPurchase.id in (Select VendorPurchaseid from VendorPurchaseItem INNER JOIN ProductStock ON ProductStock.Id = VendorPurchaseItem.ProductStockId 
where VendorPurchaseItem.InstanceId  =  @InstanceId AND VendorPurchaseItem.AccountId  =  @AccountId 
and ProductStock.ProductId like isnull(@product, '')+'%'
and isnull(productstock.BatchNo ,'') = isnull(@BtchNo, isnull(productstock.BatchNo ,'') )
 
and productstock.ExpireDate between isnull(@From_ExpiryDt, productstock.ExpireDate) and isnull(@To_ExpiryDt, productstock.ExpireDate))

ORDER BY     (VendorPurchase.createdat  )  desc   , cast(VendorPurchase.GoodsRcvNo as bigint) desc OFFSET @PageNo ROWS FETCH NEXT @PageSize ROWS ONLY

END
           
END
