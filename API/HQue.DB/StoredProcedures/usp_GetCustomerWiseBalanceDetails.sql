﻿/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**01/11/17     Sarubala V		Multiple payment included
*******************************************************************************/
CREATE PROCEDURE [dbo].[usp_GetCustomerWiseBalanceDetails](@InstanceId varchar(36),@AccountId varchar(36),@Name varchar(50),@Mobile varchar(20),@StartDate datetime,@EndDate datetime)
AS
BEGIN
	SET NOCOUNT ON
	
	if(@Name = '')
	begin
		SET @Name = NULL
	end
	if(@Mobile = '')
	begin
		SET @Mobile = NULL
	end
	if(@StartDate = '')
	begin
		SET @StartDate = NULL
	end
	if(@EndDate = '')
	begin
		SET @EndDate = NULL
	end
	if(@InstanceId = '')
	begin
		SET @InstanceId = NULL
	end
		 
	select 
	id,
	InvoiceNo,
	InvoiceDate,
	InvoiceValue, 
	Age as Age,  
	InvoiceValue-(sum(Credit)-sum(debit)) as Debit,
	sum(Credit)-sum(debit) as Credit 
	from 
	(
		SELECT 
		s.Id,
		s.InvoiceNo,
		s.InvoiceDate,	
		--Convert(decimal(18, 2),sum((CASE  WHEN SI.SellingPrice > 0 THEN SI.SellingPrice ELSE PS.SellingPrice END) * SI.Quantity - ((CASE WHEN SI.SellingPrice > 0 THEN SI.SellingPrice ELSE PS.SellingPrice END ) * SI.Quantity * (CASE WHEN SI.Discount > 0 THEN SI.Discount ELSE S.Discount END) /100) )) AS InvoiceValue,
		s.SaleAmount AS InvoiceValue,
		cp.debit as Debit,   
		cp.Credit,		
		DATEDIFF(day,CAST(cp.CreatedAt  AS date),Getdate()) As Age
		FROM SalesItem as si WITH(NOLOCK) inner join Sales as s WITH(NOLOCK) on si.SalesId = s.Id
		inner join ProductStock as ps WITH(NOLOCK) on si.ProductStockId = ps.Id
		inner join CustomerPayment as cp WITH(NOLOCK) on  cp.SalesId = s.Id
		left join (select * from SalesPayment WITH(NOLOCK) where AccountId = @AccountId and InstanceId = ISNULL(@InstanceId,InstanceId) and PaymentInd = 4 and isActive = 1)sp on sp.SalesId=s.Id
		WHERE s.AccountId = @AccountId AND s.InstanceId = ISNULL(@InstanceId,s.InstanceId)
		AND CAST(S.invoicedate AS DATE) BETWEEN CAST(ISNULL(@StartDate,S.invoicedate) AS DATE) AND CAST(ISNULL(@EndDate,S.invoicedate) AS DATE)
		AND ISNULL(s.Name,'') = ISNULL(@Name,ISNULL(s.Name,'')) AND ISNULL(s.Mobile,'') = ISNULL(@Mobile,ISNULL(s.Mobile,''))
		AND (s.Cancelstatus IS NULL) AND s.Name IS NOT NULL AND (S.Credit IS NOT NULL or (s.PaymentType = 'Multiple' and sp.PaymentInd=4))
		GROUP BY s.Id,s.InvoiceNo,s.InvoiceDate,cp.debit,cp.Credit,CAST(cp.CreatedAt  AS date),s.PaymentType,s.SaleAmount
	) a group by id,InvoiceNo,InvoiceDate,InvoiceValue,Age having sum(Credit)-sum(debit)>0
	
	UNION 

	Select 
	S.Id, 
	'Op Bal' as InvoiceNo, 
	CP.transactionDate as InvoiceDate, 
	sum(CP.Credit) as InvoiceValue, 
	DATEDIFF(day,CAST(cp.CreatedAt AS date),Getdate()) As Age,
	sum(CP.debit) as Debit, 
	sum(CP.Credit)-sum(CP.debit) as Credit
	From CustomerPayment CP WITH(NOLOCK) INNER JOIN Patient S WITH(NOLOCK) ON CP.CustomerId = S.Id 
	WHERE CP.AccountId = @AccountId AND CP.InstanceId =ISNULL(@InstanceId,CP.InstanceId)
	AND CP.SalesId is null AND ISNULL(S.Name,'') = ISNULL(@Name,ISNULL(S.Name,'')) AND ISNULL(S.Mobile,'') = ISNULL(@Mobile,ISNULL(S.Mobile,''))					
	Group by S.Id,CP.transactionDate,CAST(cp.CreatedAt  AS date) having sum(CP.Credit)-sum(CP.debit)>0 

	SET NOCOUNT OFF	
END