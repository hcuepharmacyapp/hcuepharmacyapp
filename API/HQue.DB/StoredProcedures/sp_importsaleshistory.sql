/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**02/08/17     Poongodi R	 Createdby Parameter,ExternalId added 
 
*******************************************************************************/
Create PROCEDURE sp_importsaleshistory (@AccountID as varchar(36), @InstanceID as varchar(36), @FilePath as varchar(1000), @IsCloud as int,@Createdby char(36)) AS
BEGIN
 
 IF OBJECT_ID(N'tempdb..tmpSaleHist', N'U') IS NOT NULL
	DROP TABLE #tmpSaleHist

 IF OBJECT_ID(N'dbo.tmpSaleHist', N'U') IS NOT NULL
	DROP TABLE tmpSaleHist

 IF OBJECT_ID(N'dbo.tmpSaleHist', N'U') IS NULL
 BEGIN
  PRINT 'Creating Table tmpSaleHist'
	 CREATE TABLE tmpSaleHist
	 (
		[RowREF]		int null,
		[InvoiceNo]		varchar(50) NOT NULL,
		[InvoiceDate]	varchar(16) NULL,
		[PatientName]	varchar(200) NOT NULL,
		[DoctorName]	varchar(200) NOT NULL,
		[NetAmount]		decimal(18, 2) NULL,
		[ProductName]	varchar(200) NOT NULL,
		[PackageSize]   decimal(10, 2) NULL,
		[PackageQty]	decimal(10, 2) NULL,
		[PackagePurchasePrice] decimal(18, 2) NULL,
		[FreeQty]		int NULL,
		[VAT]           decimal(9, 2) NULL,
		[CST]           decimal(9, 2) NULL,
		[ExpireDate]	varchar(16) NULL,
		[BatchNo]		varchar(100) NOT NULL,
		[MRP]			decimal(18, 2) NOT NULL,
		[Discount]		decimal(5, 2) NULL,
		[Qty]			decimal(18, 2) NOT NULL,
		[BillAmount]	decimal(18, 2) NOT NULL,
		[UnitRate]		decimal(18, 2) NOT NULL
	 ) 
 END	

--SELECT * INTO #tmpSaleHist FROM tmpSaleHist
TRUNCATE TABLE tmpSaleHist
Declare @EXTRefId char(36)= Newid()
DECLARE @bulk_cmd varchar(1000);  
IF @IsCloud = 1 
	SET @bulk_cmd = 'BULK INSERT tmpSaleHist FROM '''+ @FilePath + ''' WITH (DATA_SOURCE=''MyAzureInvoicesContainer'', FORMAT=''CSV'', FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  
ELSE
	SET @bulk_cmd = 'BULK INSERT tmpSaleHist FROM '''+ @FilePath + ''' WITH (FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  


EXEC(@bulk_cmd);  

SELECT * into #tmpSaleHist from tmpSaleHist 

--Doctor
INSERT INTO Doctor (id, AccountId, Name,CreatedBy,UpdatedBy) SELECT NEWID(), @AccountID, doctorName,@Createdby,@Createdby    FROM (SELECT distinct DoctorName FROM #tmpSaleHist where ISNULL(DoctorName,'')!='') t WHERE DoctorName NOT IN (SELECT name FROM Doctor WHERE accountId = @AccountID)

--Patient
INSERT INTO Patient (id, AccountId, Name,CreatedBy,UpdatedBy) SELECT NEWID(), @AccountID, PatientName,@Createdby,@Createdby  FROM (SELECT distinct PatientName FROM #tmpSaleHist where ISNULL(PatientName,'')!='') t WHERE PatientName NOT IN (SELECT name FROM Patient WHERE accountId = @AccountID)

--Products 
INSERT INTO product (id, AccountId, name, Eancode,CreatedBy,UpdatedBy,Ext_RefId) SELECT NEWID(), @AccountID, ProductName, 'SH',@Createdby,@Createdby ,@EXTRefId FROM (SELECT distinct productname from #tmpSaleHist WHERE ProductName NOT IN (SELECT name from PRODUCT WHERE accountId = @AccountID)) t

--ProductStock
INSERT INTO [dbo].[ProductStock]
           ([Id],[AccountId],[InstanceId],[ProductId],[VendorId],[BatchNo],[ExpireDate],[PackageSize],[PackagePurchasePrice],[PurchasePrice],[VAT],[CST],[SellingPrice],[Stock],[StockImport],[Eancode],CreatedBy,UpdatedBy,Ext_RefId)
			SELECT NEWID(), @AccountID, @InstanceID, p.id productid, '117856B6-709C-4665-8243-AD3711F317ED' VendorId, t.BatchNo, ISNull(t.ExpireDate, getdate()), t.PackageSize, t.PackagePurchasePrice, CAST(isnull(t.PackagePurchasePrice,1)/ isnull(t.PackageSize,1) AS decimal(18,3)), t.VAT, t.CST, CAST(t.MRP/ISNULL(t.PackageSize,1) AS decimal(18,3)), 0, 1,RowREF,@Createdby,@Createdby,@EXTRefId
			FROM #tmpSaleHist t INNER JOIN product p ON p.Name = t.ProductName AND p.Eancode = 'SH' WHERE p.accountId = @AccountID

INSERT INTO [dbo].[InvoiceSeriesItem] 
			([Id],[AccountId],[InstanceId],[SeriesName],[InvoiceseriesType],[ActiveStatus])
			SELECT NEWID(), @AccountID, @InstanceID, series, 2, 'Y' FROM
			(SELECT distinct SUBSTRING(invoiceno,1,2) series from #tmpSaleHist) t

/*DECLARE @UserID AS VARCHAR(36)
SELECT Top 1 @UserID = Id From HQueUser WHERE AccountId = @AccountID */
--Sales
INSERT INTO [dbo].[Sales]
       ([Id],[AccountId],[InstanceId], [InvoiceSeries],[InvoiceNo],[InvoiceDate],[DoctorId],[PatientId],[Name],[DoctorName],[Discount],[NetAmount],[createdBy],PaymentType,DeliveryType,CashType,BillPrint,SendSms,SendEmail,DiscountType,DiscountValue,createdat,UpdatedBy)
		SELECT NEWID(), @AccountID, @InstanceID, '', t.InvoiceNo, t.InvoiceDate, d.id doctorid, p.id patientid, t.patientname, t.doctorname, 0, t.NetAmount, @Createdby,'Cash','Counter','Full',0,0,0,1,0,t.invoiceDate ,@Createdby FROM
		(SELECT InvoiceNo, InvoiceNo invno, InvoiceDate, sum(BillAmount) NetAmount, max(doctorname) doctorname, max(patientname) patientname, max(discount) discount FROM #tmpSaleHist group by InvoiceNo, InvoiceDate) t
		LEFT OUTER JOIN Doctor d ON d.Name = t.doctorname LEFT OUTER JOIN Patient p ON p.Name = t.patientname WHERE p.accountId = @AccountID AND d.accountId = @AccountID ORDER BY t.invoiceDate

--SalesItem
INSERT INTO [dbo].[SalesItem]
	 ([Id],[AccountId],[InstanceId],[SalesId],[ProductStockId],[Quantity],[VAT],[Discount],[SellingPrice],[MRP],CreatedBy,UpdatedBy)
	 SELECT NEWID(), @AccountID, @InstanceID, s.id, ps.id, t.Qty, t.VAT, t.Discount, t.UnitRate, t.UnitRate,@Createdby,@Createdby  FROM #tmpSaleHist t INNER JOIN
	 (SELECT max(ps.id) id, p.Name, ps.BatchNo from ProductStock ps INNER JOIN product p ON p.id = ps.ProductId WHERE ps.accountId = @AccountID AND ps.instanceId = @InstanceID  group by p.Name, ps.BatchNo) ps ON ps.Name = t.ProductName AND ps.BatchNo = t.BatchNo
	 INNER JOIN sales s ON s.InvoiceNo = t.InvoiceNo WHERE s.accountId = @AccountID AND s.instanceId = @InstanceID


UPDATE sales SET InvoiceSeries = SUBSTRING(InvoiceNo,1, 2), InvoiceNo = SUBSTRING(InvoiceNo,3, LEN(InvoiceNo)-2) WHERE accountId = @AccountID AND instanceId = @InstanceID

SELECT 'SUCCESS'

END

