/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 30/08/2017  Poongodi		Created
** 04/092017  Sarubala		Added order by filter
** 11/10/2017  nandhini		invoicedate1,expirydate1 for csv
** 23/05/2018  Sarubala		order by CreatedAt incluced

*******************************************************************************/ 
CREATE PROC [dbo].[Usp_gstr1_sales](@Accountid  CHAR(36), 
                               @InstanceId VARCHAR(36), 
                               @Startdate  DATE, 
                               @EndDate    DATE, 
                               @GstinNo    VARCHAR(50), 
                               @FilterType VARCHAR(10),
							   @IsSummary bit=0)   
AS 
  BEGIN 
		if (@Accountid ='67067991-0e68-4779-825e-8e1d724cd68b' and month(@enddate)= 7)
		  begin
			exec Usp_gstr1_sales_medi @Accountid,@InstanceId,@Startdate,@EndDate,@GstinNo,@FilterType,@IsSummary
			Return
		  end 
		declare @invoiceDate1 datetime
		declare @ExipryDate1 datetime
      
		Declare @Instance TABLE   
        ( 
           id CHAR(36) primary key
        ) 

		IF ( @FilterType = 'branch' ) 
			BEGIN 
				INSERT INTO @Instance 
				SELECT @Instanceid 
				set @GstinNo = nULL
			END 
		ELSE 
			BEGIN 
				INSERT INTO @Instance 
				SELECT id 
				FROM   instance (nolock)
				WHERE  gstinno = @GstinNo 
			END 

		Select 
		SalesId, 
		max(BranchName) [BranchName], 
		max([BranchGSTin]) [BranchGSTin], 
		max([InvoiceNumber]) [InvoiceNumber],
		([InvoiceDate]) [InvoiceDate],
		([CreatedAt]) [CreatedAt],
		max([RecipientName]) [RecipientName],
		max([RecipientGSTin]) [RecipientGSTin],
		max([PartyType]) [PartyType],
		[Code],
		[HSN],
		[ProductName],
		([InvoiceDate1]) [InvoiceDate1],	
		sum(isnull([Quantity],0)) [Quantity], 
		sum([InvoiceValue]) [InvoiceValue], 
		max([PlaceOfSupply]) [PlaceOfSupply],
		'Goods' [IsGoods], 
        'No' [IsReverseCharge], 
		max([SupplyType]) [SupplyType],
		[GSTTotal], 
		sum([DiscountValue]) [DiscountValue],
		sum([ValueWithoutTax]) [ValueWithoutTax],
		sum([CGSTAmount]) [CGSTAmount], 
		sum([SGSTAmount]) [SGSTAmount],
		0.00 [IGSTAmount], 
        0.00 [CESS], 
		sum([TaxAmount]) [TaxAmount],
		[BatchNo],
		[ExipryDate],
		[ExipryDate1] 
		from (
			SELECT 
			i.NAME + ' - ' + i.area AS BranchName, 
            i.gstinno [BranchGSTin], 
            Ltrim(Isnull(s.prefix, '') + Isnull(s.invoiceseries, '')) AS InvoiceSeries, 
            S.invoiceno, 
            Ltrim(Isnull(s.prefix, '')+Isnull(s.invoiceseries, '')) + Isnull(S.invoiceno, '') [InvoiceNumber], 
            S.invoicedate [InvoiceDate], 
			S.CreatedAt [CreatedAt],
			convert(varchar,S.invoicedate,103) [InvoiceDate1], 			  
            Isnull(S.NAME, '') AS [RecipientName], 
			ltrim(isnull(pat.GsTin,'') ) [RecipientGSTin],
            case ltrim(isnull(pat.GsTin,'')) when '' then 'UnRegistered' else 'Registered' end [PartyType], 
            case @IsSummary when 1 then '' else Isnull(p.code, '')   end [Code], 
            case @IsSummary when 1 then '' else Isnull(P.hsncode, '') end [HSN], 
            case @IsSummary when 1 then '' else Isnull(P.NAME, 'Not Available') end AS [ProductName], 
            SI.quantity [Quantity], 
            --SIP.finalvalue  - (Cast(( SIP.finalvalue * Isnull(SIp.discount, 0) / 100 ) AS DECIMAL(18, 6))) [InvoiceValue], 
			SI.TotalAmount AS [InvoiceValue],
            left(ltrim(isnull(pat.GsTin,'')),2) [PlaceOfSupply], 
			--CONVERT(DECIMAL(18, 6), Isnull(SI.sellingprice, PS.sellingprice) * SI.quantity) AS SellingPrice, 
			SI.ItemAmount AS SellingPrice, 
            --CASE Isnull(s.taxrefno, 0) WHEN 1 THEN CASE Isnull(ps.gsttotal, Isnull(SI.gsttotal, 0)) WHEN 0 THEN 'Zero Rated Supplies' ELSE 'Regular GST Supply' END END [SupplyType], 
			CASE Isnull(s.taxrefno, 0) WHEN 1 THEN CASE Isnull(SI.gsttotal, 0) WHEN 0 THEN 'Zero Rated Supplies' ELSE 'Regular GST Supply' END END [SupplyType], 
            --CASE Isnull(s.taxrefno, 0) WHEN 1 THEN Isnull(ps.gsttotal, Isnull(SI.gsttotal, 0)) ELSE PS.vat END [GSTTotal], 
			CASE Isnull(s.taxrefno, 0) WHEN 1 THEN Isnull(SI.gsttotal, 0) ELSE PS.vat END [GSTTotal], 
            Isnull(SI.discount, 0) Discount, 
            --Cast(( SIP.finalvalue * Isnull(SIp.discount, 0) / 100 ) AS DECIMAL(18, 6)) [DiscountValue], 
			SI.DiscountAmount AS [DiscountValue], 
			--round(SIP.finalvalue ,6) - round(tax.taxvalue,6) - (Cast(( round(SIP.finalvalue ,6)* Isnull(SIp.discount, 0) / 100 ) AS DECIMAL(18, 6))) [ValueWithoutTax], 
			SI.TotalAmount-SI.GstAmount AS [ValueWithoutTax], 
            --Round(tax.taxvalue / 2, 6) [CGSTAmount], 
			SI.GstAmount/2 AS [CGSTAmount],
            --Round(tax.taxvalue / 2, 6) [SGSTAmount],    
			SI.GstAmount/2 AS [SGSTAmount],      
            --Round(tax.taxvalue, 6) [TaxAmount], 
			SI.GstAmount AS [TaxAmount], 
            case @IsSummary when 1 then '' else  PS.batchno  end [BatchNo], 
            case @IsSummary when 1 then NULL else PS.expiredate  end [ExipryDate] ,
			case @IsSummary when 1 then NULL else convert(varchar,PS.expiredate,103)  end [ExipryDate1] ,
			s.id [SalesId]
			FROM sales S WITH(nolock) 
			inner join @Instance I1 on I1.id = S.instanceid
            INNER JOIN (SELECT SI.* FROM salesitem (nolock) SI inner join @Instance I on I.id = si.instanceid) SI ON S.id = SI.salesid 
            INNER JOIN (SELECT PS.* FROM productstock (nolock) PS inner join @Instance I on I.id = PS.instanceid) PS ON PS.id = SI.productstockid 
			LEFT JOIN (SELECT * FROM PATIENT WHERE  accountid = @Accountid) pat on pat.id = S.PatientId
            LEFT JOIN (SELECT * FROM product (nolock) WHERE  accountid = @Accountid) p ON p.id = ps.productid 
            INNER JOIN (SELECT I1.* FROM instance (nolock) I1 inner join @Instance I on I.id = I1.id) i ON i.id = ps.instanceid 
            --CROSS apply (SELECT CONVERT(DECIMAL(18, 6), Isnull(SI.sellingprice, PS.sellingprice) * SI.quantity) AS FinalValue, (Isnull(SI.discount, 0) + Isnull(s.discount, 0)) discount) SIP
			--CROSS apply (SELECT Cast(( SIP.finalvalue - ( SIP.finalvalue * Isnull(SIp.discount, 0) / 100 )) - (SIP.finalvalue - ( SIP.finalvalue * Isnull(SIp.discount, 0) / 100 )) * (100 / ( 100 + Isnull(ps.gsttotal, Isnull(SI.gsttotal, 0)) ) ) AS DECIMAL(18, 6)) [TaxValue]) AS Tax 
			WHERE S.accountid = @Accountid              
            AND CONVERT(DATE, S.invoicedate) BETWEEN @Startdate AND @EndDate 
			AND S.cancelstatus IS NULL 
            AND i.gstinno = Isnull(@GstinNo, i.gstinno)
		) one 
		group by BranchName,InvoiceDate,CreatedAt,InvoiceNumber,SalesId,ProductName,GSTTotal,[BatchNo],[ExipryDate],[Code],[HSN],[ExipryDate1],[InvoiceDate1]
order by BranchName,InvoiceDate,CreatedAt

  END