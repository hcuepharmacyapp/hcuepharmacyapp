  --Usage : -- usp_GetSmsUserList '003cc71f-26e0-49d4-a1e0-e0a2efd3ebbd','0bb2d317-bdbc-4d99-9e71-b554d78b661a'
  create PROCEDURE [dbo].[usp_GetSmsUserList](@InstanceId varchar(36),@AccountId varchar(36))
 AS
 BEGIN
 declare @Startdate varchar(15),@Enddate varchar(15)
 if (month(getdate()) between 4 and 12)
 begin
 select @Startdate = cast(year(Getdate()) as varchar(4)) +'-'+'04-01', @Enddate = cast(year(Getdate()) + 1 as varchar(4))  +'-'+'03-31'
 end
 else if (month(getdate()) between 1 and 3)
 begin
 select @Startdate = cast(year(Getdate())-1 as varchar(4)) +'-'+'04-01', @Enddate = cast(year(Getdate()) as varchar(4))  +'-'+'03-31'
 end
 --select @Startdate, @Enddate
   SET NOCOUNT ON
     SELECT U.Name, COUNT(DISTINCT CL.id) AS TotalSms
    FROM CommunicationLog CL WITH(NOLOCK)
    LEFT OUTER JOIN HQueUser U WITH(NOLOCK) on U.id = CL.createdby
     WHERE CL.InstanceId = @InstanceId AND CL.AccountId = @AccountId
	 and cast(cl.CreatedAt as date) between cast(@Startdate as date) and cast(@enddate as date)
    GROUP BY U.Name
    ORDER BY U.Name DESC
END
 


