 --Usage : -- [usp_GetSalesDetailReturnsByDate] '0020ccad-e432-4ec5-8927-e9302db2340a','d5baa673-d6e4-4e3e-a593-cdd6313be32c','1900-01-01','2016-11-01'
 CREATE PROCEDURE [dbo].[usp_GetSalesDetailReturnsByDate](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime)
 AS
 BEGIN
   SET NOCOUNT ON
  
  SELECT DISTINCT SR.ReturnNo,SR.ReturnDate, S.Name,S.InvoiceNo,P.Name as ProductName,
			PS.BatchNo,PS.ExpireDate,S.Discount,sum(SRI.Quantity) as Quantity,
			ISNULL(SRI.MrpSellingPrice,ps.SellingPrice) as SellingPrice,ps.VAT,
			sum(case when isNull(s.Discount, 0) = 0 then 
            case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end else case 
			when isNull(sri.MrpSellingPrice,0)=0 
            then (ps.SellingPrice-(ps.SellingPrice * s.Discount)/100)*sri.Quantity else (sri.MrpSellingPrice-(sri.MrpSellingPrice * s.Discount)/100)*sri.Quantity 
			end end) as ReturnedPrice 

	FROM  SalesReturn SR WITH(NOLOCK) inner JOIN Sales as s ON s.Id = sr.SalesId
	INNER JOIN SalesReturnItem SRI WITH(NOLOCK) on sri.SalesReturnId =  sr.Id
	INNER JOIN ProductStock PS WITH(NOLOCK) on ps.id = sri.ProductStockId
	INNER JOIN Product P WITH(NOLOCK) on p.Id = ps.ProductId
	WHERE sr.AccountId  =  @AccountId AND sr.InstanceId  =  @InstanceId and S.Cancelstatus is NULL
	and sr.ReturnDate between @StartDate and @EndDate
	GROUP BY SR.ReturnNo, SR.ReturnDate, S.Name, S.InvoiceNo,P.Name,PS.VAT,PS.BatchNo,
			PS.ExpireDate,S.Discount,SRI.MrpSellingPrice,PS.SellingPrice							 
	ORDER BY SR.ReturnNo DESC

END
  


