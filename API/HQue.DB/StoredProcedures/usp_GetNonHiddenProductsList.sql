﻿/**   
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 19/05/2017 Gavaskar			
 ** 02/06/2017 Poongodi			ProductInstance Join changed
 ** 25/11/2018 Bikas			Added Package Size 
*******************************************************************************/ 
CREATE procedure [dbo].[usp_GetNonHiddenProductsList](@productname varchar(1000),@accountid char(36),@instanceid char(36))
as
begin

--DECLARE @NOofRecord int =10000000


--if (LEN(@productname) <= 2) 
--set @NOofRecord =20

 
 IF EXISTS ( SELECT Top 1 1 FROM  Settings where InstanceId=@instanceid and Enable_GlobalProdut=1)
 Begin
 SELECT top 10 *
FROM
  (SELECT p.Name,
          p.Id,
          p.AccountId,
          p.InstanceId,
          p.Code,
          p.Manufacturer,
          p.KindName,
          p.StrengthName,
          p.Type,
          p.Schedule,
          p.Category,
          p.Packing,
          p.CreatedAt,
          p.UpdatedAt,
          p.CreatedBy,
          p.UpdatedBy,
          p.VAT,
          p.Price,
		  p.HsnCode,
		  p.Igst,
		  p.Cgst,
		  p.Sgst,
		  p.GstTotal,
		  p.PackageSize,
		  
         

 p.Status,
          ip.RackNo RackNo,
		  ip.BoxNo BoxNo,
		  p.GenericName,
		  p.Eancode,
          isNull(sum(ps.stock),0) AS Totalstock
   FROM product p
   Left join productinstance ip on ip.accountid = p.accountid 
   and ip.instanceid = @instanceid
   and ip.productid = p.id
   LEFT JOIN ProductStock ps ON p.id = ps.ProductId
   AND (ps.Status IS NULL OR ps.Status = 1)
   AND ps.AccountId=@accountid
   AND ps.InstanceId = @instanceid
   WHERE (p.Name LIKE ''+@productname+'%' or p.Id=@productname) and p.AccountId=@accountid and (p.Status is null or p.Status = 1) --and (p.IsHidden!=1 or p.IsHidden is null)
   and isNull(p.IsHidden, 0) = 0
    group by p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,    
	       p.Schedule,p.Category,p.Packing,p.PackageSize,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,ip.RackNo,ip.BoxNo,p.GenericName,p.Eancode,
   p.HsnCode,
		  p.Igst,
		  p.Cgst,
		  p.Sgst,
		  p.GstTotal
     ) a 
         ORDER BY a.Name ASC,
         a.accountId,
 
        a.Totalstock  

 End
 Else
 Begin
SELECT top 10 *
FROM
  (SELECT p.Name,
          p.Id,
          p.AccountId,
          p.InstanceId,
          p.Code,
          p.Manufacturer,
          p.KindName,
          p.StrengthName,
          p.Type,
          p.Schedule,
          p.Category,
          p.Packing,
          p.CreatedAt,
          p.UpdatedAt,
          p.CreatedBy,
          p.UpdatedBy,
          p.VAT,
          p.Price,
		  p.HsnCode,
		  p.Igst,
		  p.Cgst,
		  p.Sgst,
		  p.GstTotal,
		  p.PackageSize,
         

 p.Status,
          ip.RackNo RackNo,
		  ip.BoxNo BoxNo,
		  p.GenericName,
		  p.Eancode,
          isNull(sum(ps.stock),0) AS Totalstock
   FROM product p
   Left join productinstance ip on ip.accountid = p.accountid 
   and ip.instanceid = @instanceid
   and ip.productid = p.id
   LEFT JOIN ProductStock ps ON p.id = ps.ProductId
   AND (ps.Status IS NULL OR ps.Status = 1)
   AND ps.AccountId=@accountid
   AND ps.InstanceId = @instanceid
   WHERE (p.Name LIKE ''+@productname+'%' or p.Id=@productname) and p.AccountId=@accountid and (p.Status is null or p.Status = 1) --and (p.IsHidden!=1 or p.IsHidden is null)
   and isNull(p.IsHidden, 0) = 0
    group by p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,    
	       p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,ip.RackNo,ip.BoxNo,p.GenericName,p.Eancode,
   p.HsnCode,
		  p.Igst,
		  p.Cgst,
		  p.Sgst,
		  p.GstTotal,
		  p.PackageSize
					
union
	 
	  select p.Name,
	         p.Id,
			 p.AccountId,
			 p.InstanceId,
			 p.Code,
			 p.Manufacturer,
			 p.KindName,
			 p.StrengthName,
			 p.Type,
	
		 p.Schedule,
			 p.Category,
			 p.Packing,
			 p.CreatedAt,
			 p.UpdatedAt,
			 p.CreatedBy,
			 p.UpdatedBy,
			 p.VAT,
			 p.Price,
			  p.HsnCode,
		  p.Igst,
		  p.Cgst,
		  p.Sgst,
		  p.GstTotal,
		  p.PackageSize,
			 p.Status,
			 p.RackNo,
			 '' AS BoxNo,
			 p.GenericName,
			'' as Eancode,
			 isNull(-1,-1) as Totalstock  
	  from ProductMaster p where (p.Name LIKE ''+@productname+'%' or p.Id=@productname) 
     AND (p.Status IS NULL OR p.Status = 1)
     AND p.Name NOT IN
       (SELECT p.Name
        FROM product p
        LEFT JOIN ProductStock ps ON p.id = ps.ProductId
        AND ps.AccountId = @accountid
        AND ps.InstanceId = @instanceid
        WHERE (p.Name LIKE ''+@productname+'%' or p.Id=@productname) and p.AccountId = @accountid --and (p.IsHidden!=1 or p.IsHidden is null) 
		and isNull(p.IsHidden, 0) = 0
        GROUP BY p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,p.Schedule,p.Category,p.Packing,p.PackageSize,p.CreatedAt,p.UpdatedAt,p.CreatedBy, 
		                p.UpdatedBy,p.VAT,p.Price,p.Status,p.RackNo,p.GenericName,p.Eancode)) a 
         ORDER BY a.Name ASC,
         a.accountId,
 
        a.Totalstock  

End
		 
end