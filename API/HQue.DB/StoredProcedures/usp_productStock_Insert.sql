/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 25/09/2017  Poongodi		Created
*******************************************************************************/ 
Create proc dbo.usp_productStock_Insert(
						@Id char(36),
						@AccountId char(36),
						@InstanceId char(36),
						@ProductId char(36),
						@VendorId char(36),
						@BatchNo varchar(100),
						@ExpireDate date,
						@VAT decimal(5),
						@TaxType varchar(10),
						@SellingPrice decimal(18,6),
						@MRP decimal(18,6),
						@PurchaseBarcode varchar(100),
						@Stock decimal(9,2),
						@PackageSize decimal(9,2),
						@PackagePurchasePrice decimal(18,6),
						@PurchasePrice decimal(18,6),
						@OfflineStatus bit,
						@CreatedAt datetime,
						@UpdatedAt datetime,
						@CreatedBy char(36),
						@UpdatedBy char(36),
						@CST decimal(9,2),
						@IsMovingStock bit,
						@ReOrderQty decimal(9,2),
						@Status int,
						@IsMovingStockExpire bit,
						@NewOpenedStock bit,
						@NewStockInvoiceNo varchar(50),
						@NewStockQty decimal(9,2),
						@StockImport bit,
						@ImportQty decimal(9,2),
						@Eancode varchar(50),
						@HsnCode varchar(50),
						@Igst decimal(9,2),
						@Cgst decimal(9,2),
						@Sgst decimal(9,2),
						@GstTotal decimal(9,2),
						@ImportDate date,
						@Ext_RefId varchar(36),
						@TaxRefNo tinyint 
)
as 
begin
if (not exists (select id from ProductStock (nolock) where id =@Id))
begin
	INSERT INTO ProductStock (Id,AccountId,InstanceId,ProductId,VendorId,BatchNo,ExpireDate,VAT,TaxType,SellingPrice,MRP,
	PurchaseBarcode,Stock,PackageSize,PackagePurchasePrice,PurchasePrice,OfflineStatus,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy,CST,
	IsMovingStock,ReOrderQty,Status,IsMovingStockExpire,NewOpenedStock,NewStockInvoiceNo,NewStockQty,StockImport,ImportQty,Eancode,HsnCode,
	Igst,Cgst,Sgst,GstTotal,ImportDate,Ext_RefId,TaxRefNo)
	VALUES(@Id,@AccountId,@InstanceId,@ProductId,@VendorId,@BatchNo,@ExpireDate,@VAT,@TaxType,@SellingPrice,@MRP,
	@PurchaseBarcode,@Stock,@PackageSize,@PackagePurchasePrice,@PurchasePrice,@OfflineStatus,@CreatedAt,@UpdatedAt,@CreatedBy,@UpdatedBy,@CST,
	@IsMovingStock,@ReOrderQty,@Status,@IsMovingStockExpire,@NewOpenedStock,@NewStockInvoiceNo,@NewStockQty,@StockImport,@ImportQty,@Eancode,@HsnCode,
	@Igst,@Cgst,@Sgst,@GstTotal,@ImportDate,@Ext_RefId,@TaxRefNo)
end 
select 'success' 
end

