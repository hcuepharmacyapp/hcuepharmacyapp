﻿/*                               
******************************************************************************                            
** File: [usp_GetSalesTemplateHistoryList]   
** Name: [usp_GetSalesTemplateHistoryList]                               
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 09/10/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
*******************************************************************************/ 
 
 -- exec usp_GetSalesTemplateHistoryList 'c503ca6e-f418-4807-a847-b6886378cf0b','c6b7fc38-8e3a-48af-b39d-06267b4785b4',0,30
 Create  PROCEDURE [dbo].[usp_GetSalesTemplateHistoryList](@AccountId varchar(36),@InstanceId varchar(36),@PageNo int,@PageSize int)
 AS
 BEGIN
   SET NOCOUNT ON

SELECT COUNT(1) OVER() SalesTemplateCount, SalesTemplate.Id,isnull(SalesTemplate.Prefix,'') as Prefix,SalesTemplate.TemplateNo,SalesTemplate.TemplateDate,
SalesTemplate.TemplateName,SalesTemplate.IsActive
FROM SalesTemplate (nolock) 
WHERE SalesTemplate.AccountId  =  @AccountId AND SalesTemplate.InstanceId  =  @InstanceId
ORDER BY  (SalesTemplate.createdat  )  desc  OFFSET @PageNo ROWS FETCH NEXT @PageSize ROWS ONLY
           
END
