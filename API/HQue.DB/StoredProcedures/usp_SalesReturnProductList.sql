
/*
 *******************************************************************************                            
** newly created for Sales return product search                   
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************    

** 22/09/2017   nandhini	Sales return product search
*/
CREATE PROCEDURE [dbo].[usp_SalesReturnProductList](@InstanceId varchar(36),@ProductName varchar(150))
 AS
 BEGIN
 SET NOCOUNT ON  
  
  if @ProductName = ''
  select @ProductName = null

  select top 20 result.Id,result.Name from (Select distinct(p.Id),p.name from Product p(nolock)
   join ProductStock ps on p.Id = ps.ProductId 
   join SalesReturnItem sri on ps.Id = sri.ProductStockId 
   where ps.InstanceId = @InstanceId  
   and p.Name like isnull(@ProductName,p.Name) +'%') result
   order by result.Name asc

END