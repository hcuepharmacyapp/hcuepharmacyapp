-- usp_GetSupplierLedger '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','19e60d7b-9c7c-479e-a6b9-5b8c53451c6b','',''
-- usp_GetSupplierLedger '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','66221b7f-f0ae-4a3d-bfc8-144f75c0ad17','',''
-- usp_GetSupplierLedger '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','c94169f2-397b-44a8-ba67-d0b7d3d81b6d','01-Sep-2016','21-Sep-2016'
  
  /** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
 22/06/2017    Violet			Prefix Added
 13-10-2017    Senthil          Union added
*******************************************************************************/ 
Create PROCEDURE [dbo].[usp_GetSupplierLedger](@InstanceId varchar(36),@AccountId varchar(36),@VendorId char(36),@StartDate datetime,@EndDate datetime)
AS
 BEGIN
   SET NOCOUNT ON
declare @condition nvarchar(max)	
	Declare @qry nvarchar(max)
	SET @condition=''
	

	/*if(@StartDate is not null AND @StartDate!='')
	begin
		set @condition=@condition+' AND CAST(VendorPurchase.invoicedate AS date) BETWEEN '+''''+CAST(@StartDate AS varchar(11))+'''' +' AND '+''''+CAST(@EndDate AS varchar(11))+''''
	end
	
	SET @qry='SELECT VendorPurchase.GoodsRcvNo As GRN	,CAST(VendorPurchase.CreatedAt AS date) As [GRNDate],VendorPurchase.InvoiceNo As InvoiceNo ,
	VendorPurchase.InvoiceDate As InvoiceDate,Payment.PaymentType AS TransactionType,V.Name, VendorPurchase.PaymentType As PaymentType,
Case When Payment.PaymentType=''PurchasePaid'' Then DATEDIFF(day,CAST(VendorPurchase.CreatedAt AS date),Getdate()) else Null End As [Payabledays],
CAST(Round(Isnull(dbo.CreditCalculate(VendorPurchaseId),0),0) As Decimal(10, 2))  As [InvoiceAmount],
 ISNULL(Payment.PaymentMode,''cash'') As PaymentMode,
 Payment.ChequeNo As [ChequeNo],
 Payment.ChequeDate As [ChequeDate],
 VendorPurchaseId ,
  Case When Payment.PaymentType=''PurchasePaid'' Then Debit else 0.00 End  AS [PaidAmount],
 Case When Payment.PaymentType=''Return'' Then Debit else 0.00 End AS [ReturnAmount],
CAST(Isnull(Round(dbo.CreditCalculate(VendorPurchaseId),0) -Debit,0) As Decimal(10, 2)) AS Balance

FROM Payment 
INNER JOIN VendorPurchase ON Payment.VendorPurchaseId = VendorPurchase.Id
INNER JOIN Vendor V ON V.Id= Payment.VendorId
 WHERE VendorPurchase.VendorId = ''' +@VendorId +''''+' and VendorPurchase.InstanceId=''' +@InstanceId +''''+@condition+' 
 Order by Convert(int,VendorPurchase.GoodsRcvNo),Payment.CreatedAt asc'

	
	 print @qry
	EXEC sp_executesql @qry */
	 if (@StartDate ='') select @StartDate  = null
	 if (@EndDate ='') select @EndDate  = null
	 if (@VendorId ='') select @VendorId  = null
	 SELECT b.* from 
	(SELECT VendorPurchase.GoodsRcvNo As GRN	,CAST(VendorPurchase.CreatedAt AS date) As [GRNDate],VendorPurchase.InvoiceNo As InvoiceNo ,VendorPurchase.InvoiceDate As InvoiceDate,ltrim(ISNULL(VendorPurchase.Prefix,'')+ISNULL(VendorPurchase.BillSeries,'')) BillSeries,
Payment.PaymentType AS TransactionType,V.Name, VendorPurchase.PaymentType As PaymentType,
Case When Payment.PaymentType='PurchasePaid' Then DATEDIFF(day,CAST(VendorPurchase.CreatedAt AS date),Getdate()) else Null End As [Payabledays],
CAST(Round(Isnull(dbo.CreditCalculate(VendorPurchaseId),0),0) As Decimal(10, 2))  As [InvoiceAmount],
 ISNULL(Payment.PaymentMode,'cash') As PaymentMode,
 Payment.ChequeNo As [ChequeNo],
 Payment.ChequeDate As [ChequeDate],
 VendorPurchaseId ,
  Case When Payment.PaymentType='PurchasePaid' Then Debit else 0.00 End  AS [PaidAmount],
 Case When Payment.PaymentType='Return' Then Debit else 0.00 End AS [ReturnAmount],
CAST(Isnull(Round(dbo.CreditCalculate(VendorPurchaseId),0) -Debit,0) As Decimal(10, 2)) AS Balance ,Payment.BankName,Payment.BankBranchName,Payment.IfscCode

FROM Payment 
INNER JOIN VendorPurchase ON Payment.VendorPurchaseId = VendorPurchase.Id
INNER JOIN Vendor V ON V.Id= Payment.VendorId
 WHERE   VendorPurchase.InstanceId=@InstanceId
   and isnull(VendorPurchase.cancelstatus ,0)  =0
  AND CAST(VendorPurchase.invoicedate  AS date) BETWEEN CAST(isnull(@StartDate  ,VendorPurchase.invoicedate ) AS date) and CAST(isnull(@EndDate,VendorPurchase.invoicedate ) AS date)
AND VendorPurchase.VendorId=isnull(@VendorId,VendorPurchase.VendorId)
UNION
SELECT '0' as GRN, Max(p.TransactionDate) as [GRNDate],'' as InvoiceNo,
				Max(p.TransactionDate) as InvoiceDate,
				'' as BillSeries,'Op Bal' as TransactionType, v.name, '' as PaymentType,
				0 as [Payabledays],  SUM(p.Credit) as [InvoiceAmount], 'cash' as PaymentMode,
				'' as [ChequeNo], null as [ChequeDate], '' as VendorPurchaseId,
				Sum(p.Debit) as [PaidAmount], 0.00 as [ReturnAmount], (SUM(p.Credit)- SUM(p.Debit)) as Balance,
				'' as BankName, '' as BankBranchName, '' as IfscCode
				From Payment p inner join Vendor V ON p.VendorId = v.Id
                Where  p.VendorId = isnull(@VendorId,p.VendorId)
                AND  p.InstanceId = @InstanceId
				AND  p.AccountId = @AccountId
                And p.VendorPurchaseId is null
                Group by v.Name) as b
				order by Convert(int,b.GRN),b.[GRNDate] asc

				
END




