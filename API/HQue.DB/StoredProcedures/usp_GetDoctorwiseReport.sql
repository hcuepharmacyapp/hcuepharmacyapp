/**  
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 28/08/2017	Sarubala		Created
** 31/08/2017	Lawrence		Net Value, Return Qty Added, All instance value loaded
** 18/09/2017	Sarubala		Sales Discount value calculated
*******************************************************************************/ 
 --Usage : -- usp_GetDoctorwiseReport '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','01 Aug 2017','30 Aug 2017','b990385e-301f-4c0b-8868-b7ac178f1986','0fb017bb-da20-44b1-892e-a36512647c66'
 
Create PROCEDURE [dbo].[usp_GetDoctorwiseReport](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime, @DoctorId varchar(36), @ProductId varchar(36))
AS
BEGIN
	SET NOCOUNT ON 

	if @DoctorId = ''
		select @DoctorId = null
	if @ProductId = ''
		select @ProductId = null

	select 
	BranchName,
	ProductName, 
	DoctorName, 
	SUM(Quantity) SalesQty, 
	SUM(ReturnQty) ReturnQty, 
	SUM(Quantity) - SUM(ReturnQty) NetQuantity, 
	SUM(A.SalesNetvalue - A.ReturnNetvalue) as Netvalue ,
	--SUM(MRP) As MRP, 
	--SUM(CostPrice) As CostPrice,
	--SUM(MRP-CostPrice) AS SalesProfit,
	SUM(MRP-CostPrice)/SUM(MRP)*100 AS ProfitPerc
	from
	(
		select 
		i.Name As BranchName,
		ISNULL(p.Name,'') ProductName, 
		ISNULL(d.Name, ISNULL(s.DoctorName,'')) DoctorName , 
		SUM(si.Quantity) Quantity, 
		0 ReturnQty,
		0 NetQuantity, 
		--sum(Round(si.Quantity * (si.SellingPrice * (1 - ((isnull(s.Discount,0) + isnull(si.Discount,0)) / 100))),0)) SalesNetvalue
		SUM(si.TotalAmount) SalesNetvalue, 
		SUM(PP.Cost) As CostPrice,
		s.saleamount As MRP,	
		0 ReturnNetvalue
		from 
		sales s(nolock) join SalesItem si(nolock) on s.Id=si.SalesId
		join ProductStock ps(nolock) on ps.Id=si.ProductStockId
		join Product p(nolock) on p.Id=ps.ProductId
		left join Doctor d(nolock) on d.Id=s.DoctorId
		inner join Instance i(nolock) on i.AccountId=@AccountId AND i.Id=ISNULL(@InstanceId,s.InstanceId) 
		CROSS APPLY (SELECT ISNULL(PS.PurchasePrice,0) * SI.Quantity AS Cost) AS PP
		where s.AccountId=@AccountId and s.InstanceId=ISNULL(@InstanceId,s.InstanceId) and CONVERT(date,s.InvoiceDate) between @StartDate  and @EndDate
		and ISNULL(ps.ProductId,'') = ISNULL(@ProductId,ISNULL(ps.ProductId,''))
		and 
		(
			ISNULL(s.DoctorId,'') = ISNULL(@DoctorId,ISNULL(s.DoctorId,'')) or
			ISNULL(s.DoctorName,'') like ISNULL(@DoctorId,ISNULL(s.DoctorName,'')) + '%' 
		)
		and s.Cancelstatus is null
		group by ISNULL(d.Name, ISNULL(s.DoctorName,'')),ISNULL(p.Name,''),i.Name,s.saleamount

		union all

		select 
		i.Name BranchName,
		ISNULL(p.Name,'') ProductName, 
		ISNULL(d.Name, ISNULL(s.DoctorName,'')) DoctorName , 
		0 SalesQty, 
		SUM(sri.Quantity) ReturnQty, 
		0 NetQuantity, 
		0 SalesNetvalue,
		SUM(sri.TotalAmount) AS ReturnNetvalue,
		SUM(PP.Cost) As CostPrice,
		s.saleamount As MRP
		--round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * isnull(s.Discount ,0))/100)*sri.Quantity else 
		--(sri.MrpSellingPrice-(sri.MrpSellingPrice * isnull(s.Discount ,0))/100)*sri.Quantity end end) - (sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * isnull(sri.Discount,0))/100)*sri.Quantity else ((sri.MrpSellingPrice * isnull(sri.Discount,0))/100)*sri.Quantity end end)))),0) as ReturnNetvalue
		from SalesReturn sr(nolock) 
		join SalesReturnItem sri on sr.Id=sri.SalesReturnId
		join ProductStock ps(nolock) on ps.Id=sri.ProductStockId
		join Product p(nolock) on p.Id=ps.ProductId
		left join Sales s(nolock) on s.Id=sr.SalesId
		left join Doctor d(nolock) on d.Id=s.DoctorId
		inner join Instance i(nolock) on i.AccountId=@AccountId and i.Id=sr.InstanceId
		CROSS APPLY (SELECT ISNULL(PS.PurchasePrice,0) * sri.Quantity AS Cost) AS PP
		where sr.AccountId=@AccountId and sr.InstanceId=ISNULL(@InstanceId,sr.InstanceId) and CONVERT(date,sr.ReturnDate) between @StartDate  and @EndDate
		and ISNULL(ps.ProductId,'') = ISNULL(@ProductId,ISNULL(ps.ProductId,''))
		and 
		(
			ISNULL(s.DoctorId,'') = ISNULL(@DoctorId,ISNULL(s.DoctorId,'')) or
			ISNULL(s.DoctorName,'') like ISNULL(@DoctorId,ISNULL(s.DoctorName,'')) + '%' 
		)
		and s.Cancelstatus is null
		and ISNULL(sri.IsDeleted,0) != 1
		and ISNULL(sr.CancelType,0) != 2
		group by ISNULL(d.Name, ISNULL(s.DoctorName,'')),ISNULL(p.Name,''),i.Name,s.saleamount
	)A
	group by DoctorName,ProductName,BranchName

	SET NOCOUNT OFF
END