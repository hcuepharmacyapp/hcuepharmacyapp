/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 23/11/2017 Manivannan	  Created
*******************************************************************************/ 
CREATE Proc usp_Sales_Create(@Id char(36), @AccountId char(36), @InstanceId char(36), @PatientId varchar(36), @InvoiceSeries varchar(15), @Prefix varchar(3)
, @InvoiceNo varchar(50), @InvoiceDate datetime, @Name varchar(100), @Mobile varchar(15), @Email varchar(100), @DOB date, @Age tinyint, @Gender varchar(15)
, @Address varchar(300), @Pincode varchar(10), @DoctorName varchar(100), @DoctorMobile varchar(100), @PaymentType varchar(50), @DeliveryType varchar(50)
, @BillPrint bit, @SendSms bit, @SendEmail bit, @Discount decimal(10,6), @DiscountValue decimal(18,6), @DiscountType tinyint, @RoundoffNetAmount decimal(5,2)
, @Credit decimal(18,2), @FileName varchar(500), @OfflineStatus bit, @CreatedAt datetime, @UpdatedAt datetime, @CreatedBy char(36), @UpdatedBy char(36)
, @CashType varchar(50), @CardNo varchar(50), @CardDate date, @ChequeNo varchar(50), @ChequeDate date, @CardDigits varchar(15), @CardName varchar(500)
, @Cancelstatus tinyint, @SalesType char(36), @NetAmount decimal(18,6), @BankDeposited bit, @AmountCredited bit, @FinyearId varchar(36), @DoctorId char(36)
, @VatAmount decimal(18,6), @SalesItemAmount decimal(18,6), @LoyaltyPts decimal(18,6), @RedeemPts decimal(18,6), @RedeemAmt decimal(18,6), @DoorDeliveryStatus varchar(15)
, @GstAmount decimal(18,6),@IsRoundOff bit, @TaxRefNo tinyint,@SaleAmount DECIMAL(18,6),@RoundoffSaleAmount DECIMAL(5,2),@TotalDiscountValue DECIMAL(18, 6)
, @LoyaltyId char(36),@RedeemPercent decimal(18,6)
)
AS
BEGIN

	INSERT INTO Sales (Id,AccountId,InstanceId,PatientId,InvoiceSeries,Prefix,InvoiceNo,InvoiceDate,Name,Mobile,Email,DOB,Age,Gender,
	Address,Pincode,DoctorName,DoctorMobile,PaymentType,DeliveryType,BillPrint,SendSms,SendEmail,Discount,DiscountValue,DiscountType,
	TotalDiscountValue,RoundoffNetAmount,Credit,FileName,OfflineStatus,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy,CashType,CardNo,CardDate,
	ChequeNo,ChequeDate,CardDigits,CardName,Cancelstatus,SalesType,NetAmount,SaleAmount,RoundoffSaleAmount,BankDeposited,AmountCredited,
	FinyearId,DoctorId,VatAmount,SalesItemAmount,LoyaltyPts,RedeemPts,RedeemAmt,DoorDeliveryStatus,GstAmount,IsRoundOff,TaxRefNo,LoyaltyId,
	RedeemPercent)
	VALUES(@Id,@AccountId,@InstanceId,@PatientId,@InvoiceSeries,@Prefix,
	dbo.GetSalesInvoiceNo(@Accountid, @InstanceId, @InvoiceDate, @InvoiceSeries, @Prefix, @InvoiceNo),
	@InvoiceDate,@Name,@Mobile,@Email,@DOB,@Age,@Gender,@Address,@Pincode,@DoctorName,@DoctorMobile,@PaymentType,@DeliveryType,@BillPrint,
	@SendSms,@SendEmail,@Discount,@DiscountValue,@DiscountType,@TotalDiscountValue,@RoundoffNetAmount,@Credit,@FileName,@OfflineStatus,
	@CreatedAt,@UpdatedAt,@CreatedBy,@UpdatedBy,@CashType,@CardNo,@CardDate,@ChequeNo,@ChequeDate,@CardDigits,@CardName,@Cancelstatus,
	@SalesType,@NetAmount,@SaleAmount,@RoundoffSaleAmount,@BankDeposited,@AmountCredited,@FinyearId,@DoctorId,@VatAmount,@SalesItemAmount,
	@LoyaltyPts,@RedeemPts,@RedeemAmt,@DoorDeliveryStatus,@GstAmount,@IsRoundOff,@TaxRefNo,@LoyaltyId,@RedeemPercent)

	SELECT * FROM Sales WHERE Id = @Id
 
END
