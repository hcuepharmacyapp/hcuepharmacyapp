/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 **09/05/17     Poongodi R		Query optimized (address the Performance Issue)
*******************************************************************************/  
--Usage : -- usp_GetDashboardFinYearPurchase '013513b1-ea8c-4ea8-9fed-054b260ee197', '18204879-99ff-4efd-b076-f85b4a0da0a3'
 Create PROCEDURE [dbo].[usp_GetDashboardFinYearPurchase](@AccountId VARCHAR(36), @InstanceId VARCHAR(36) = '')
 AS
 BEGIN
   SET NOCOUNT ON
   Declare @FinyearStartDt date ,@Date date = getdate(),
   @MinDate date 
   if (MONTH (@date) between 4 and 12)
   begin
   Select @FinyearStartDt= cast((cast( year(@date) as varchar)+'-04-01') as date) 
		 

   end
   else if (MONTH (@date) between 1 and 3)
   begin
   Select @FinyearStartDt= cast((cast( year(@date) -1 as varchar)+'-04-01') as date) 
	 

   end
      IF @InstanceId != ''
   BEGIN

   select @MinDate= min(createdat) from VendorPurchase where accountid =@AccountId AND instanceid = @InstanceId 

   end 
   else
   begin
    select @MinDate= min(createdat) from VendorPurchase where accountid =@AccountId  
   end
 select datediff(d, @FinyearStartDt, @Date) FinancialYear
 
 /*  IF @InstanceId != ''
   BEGIN
		select sum(FinancialYear) as FinancialYear from (
            SELECT distinct DATEDIFF(day, (SELECT top 1 CONVERT(date, createdat)
            as date from VendorPurchase where convert(date, createdat) >=
            ( select CONVERT(date, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())
            - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))
            and convert(date, createdat)<=
            (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
            getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))
            order by CreatedAt), (select top 1 case when(select convert(date, GETDATE())) <= (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(
            dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
            getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))
            then (select  convert(date, GETDATE())) end from VendorPurchase)) as FinancialYear
            from VendorPurchase where accountid =@AccountId AND instanceid = @InstanceId 

            UNION ALL

            SELECT '0' as FinancialYear ) as Financial
	END
	ELSE
	BEGIN
		select sum(FinancialYear) as FinancialYear from (
            SELECT distinct DATEDIFF(day, (SELECT top 1 CONVERT(date, createdat)
            as date from VendorPurchase where convert(date, createdat) >=
            ( select CONVERT(date, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())
            - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))
            and convert(date, createdat)<=
            (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
            getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))
            order by CreatedAt), (select top 1 case when(select convert(date, GETDATE())) <= (select convert(date, DATEADD(SS, -1, DATEADD(mm, 12, DATEADD(
            dd, 0, DATEDIFF(dd, 0, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12),
            getDate()) - datePart(d, DATEADD(mm, -(((12 + DATEPART(m, getDate())) - 4) % 12), getDate())) + 1))))))
            then (select  convert(date, GETDATE())) end from VendorPurchase)) as FinancialYear
            from VendorPurchase where accountid = @AccountId

            UNION ALL

            SELECT '0' as FinancialYear ) as Financial
	END

*/
END
 