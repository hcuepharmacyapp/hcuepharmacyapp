/*                               
******************************************************************************                            
** File: [usp_get_sale_history]   
** Name: [usp_get_sale_history]                               
** Description: To Get Sale details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 04/03/2017 Poongodi R		Invoice amount section ADDED for the cr:#PR22 
 ** 07/03/2017 Poongodi R		Invoice date changed to Created at 
 ** 09/03/20017	Poongodi R		Top 10 Selection Implemented  
 ** 10/03/2017  sabarish		PaymentType Added
 ** 13/03/2017  Poongodi R		Created at changed to Invoice date(Input given by Siva)
 ** 28/03/2017  Poongodi R		Isnull added in Sales discount value section
 ** 07/04/2017  Poongodi R		Customer name display issue fixed
 ** 09/05/2017  Poongodi R		Nolock added
 ** 29/05/2017  Violet			PatientId Parameter added and Implemented
 ** 31/05/2017  Poongodi R		DiscountIssue fixed
 ** 07/06/2017  Poongodi R		Isnull addedin Patiendid section
 ** 08/06/2017  Poongodi R		Billamount search issue fixed
 ** 14/06/2017	Poongodi R		Prefix added
 ** 22/06/2017	Poongodi R		Prefix added in Filtercondition
 ** 28/06/2017  Settu           Voucher adjustment/Settlements history inclded for blocking edit/cancel/return
 ** 22/07/2017	Poongodi R		Arithmetic Overflow issue fixed
 ** 07/08/2017	Nandhini N		Customer series - >ALL->selection of Bill number not working
 ** 16/08/2017  Santhiyagu      Modified From and To MRP Data Type
  ** 28/08/2017  Nandhini N      Modified For filter exact search
  ** 25/09/2017  Lawrence M      filter Modified For exact bill no search
  ** 09/11/2017  Sarubala V      Multiple payment added
  ** 28/11/2017  Lawrence M		 gstSelect added for composite
  ** 28/12/2017  nandhini	     username added 
  ** 18/12/2017  Sarubala V		 Redeem percent isnull check implemented
 *******************************************************************************/ 
create PROC [dbo].[usp_get_sale_history] (@AccountId NVARCHAR(36), 
@InstanceId                                       NVARCHAR(36),
@PatientId                                        NVARCHAR(36),
@SearchColName                                    VARCHAR(50), 
@SearchOption                                     VARCHAR(50), 
@SearchValue                                      VARCHAR(50), 
@fromDate                                         VARCHAR(15), 
@Todate                                           VARCHAR(15), 
@PageNo                                           INT, 
@PageSize                                         INT ) 
AS 
  BEGIN 
    DECLARE @isDetail    int = 0 
    DECLARE @From_BillDt DATE = NULL, 
      @To_BillDt         DATE = NULL, 
      @From_ExpiryDt     DATE = NULL, 
      @To_ExpiryDt       DATE = NULL, 
      @BtchNo            VARCHAR(50) = NULL, 
      @customerName      VARCHAR(200) = NULL, 
      @customerMobile    VARCHAR(100) = NULL, 
      @doctorName        VARCHAR(100) = NULL, 
      @doctorMobile      VARCHAR(100) = NULL, 
      @product           VARCHAR(100) = NULL , 
      @billNo            VARCHAR(100) = '', 
      @discountType      tinyint = NULL,  
      @from_Qty          NUMERIC(10,2) =NULL, 
      @to_Qty            NUMERIC(10,2) =NULL, 
      @from_Vat          NUMERIC(10,2) =NULL, 
      @to_Vat            NUMERIC(10,2) =NULL, 
      @from_mrp          decimal(18,6) =NULL, 
      @to_mrp            decimal(18,6) =NULL, 
      @from_discount      decimal(30,6)  =NULL, 
      @to_discount        decimal(30,6)  =NULL ,
      @from_discountValue     decimal(30,6) =NULL, 
      @to_discountValue       decimal(30,6) =NULL ,
      @from_InvAmt         NUMERIC(20,2) =NULL ,
      @to_InvAmt             NUMERIC(20,2) =NULL 
    IF (@SearchOption='top10') 
    begin 

     set @isDetail  = 3
    end 
 else
 begin
        Select @PatientId = case  isnull(@PatientId,'') when '' then NULL else isnull(@PatientId,'')  end 
        IF (@SearchColName ='batchNo') 
        SELECT @BtchNo = Isnull(@SearchValue,''), 
               @isDetail =1 
        ELSE 
        IF (@SearchColName ='customerName') 
        SET @customerName = Isnull(@SearchValue,'') 
        ELSE 
        IF (@SearchColName ='billNo') 
        BEGIN
            IF(@SearchOption='All')        
            --SELECT @billNo = '' 
            SELECT @billNo = Isnull(@SearchValue,'')
            ELSE IF(@SearchOption='Default') -- Added default invoice no load issue 16-06-2017
            SELECT @billNo = Isnull(@SearchValue,'') 
            ELSE
            SELECT @billNo = Isnull(@SearchOption ,'') + Isnull(@SearchValue,'') 
        END
        ELSE 
        IF (@SearchColName ='customerMobile') 
        SET @customerMobile = Isnull(@SearchValue,'') 
        ELSE 
        IF (@SearchColName ='doctorName') 
        SET @doctorName = Isnull(@SearchValue,'') 
        ELSE 
        IF (@SearchColName ='doctorMobile') 
        SET @doctorMobile = Isnull(@SearchValue,'') 
        ELSE 
        IF (@SearchColName ='product') 
        SELECT @product = Isnull(@SearchValue,''), 
               @isDetail =1 
        ELSE 
        IF (@SearchColName ='billDate') 
        SELECT @From_BillDt = @fromDate , 
               @To_BillDt =   @Todate 
        ELSE 
        IF (@SearchColName ='expiry') 
        SELECT @From_ExpiryDt = @fromDate , 
               @To_ExpiryDt =   @Todate, 
               @isDetail =      1 
        ELSE 
        IF (@SearchColName ='quantity') 
        BEGIN 
          IF (@SearchOption='equal') 
          SELECT @from_Qty = Cast (@SearchValue AS NUMERIC (10,2)) , 
                 @to_Qty = Cast (@SearchValue AS   NUMERIC (10,2)), 
                 @isDetail =1 
          ELSE 
          IF (@SearchOption='greater') 
          SELECT @from_Qty = Cast (@SearchValue AS NUMERIC (10,2))+0.01 , 
                 @to_Qty =                                         NULL, 
                 @isDetail =                                       1 
          ELSE 
          IF (@SearchOption='less') 
          SELECT @from_Qty =                                     NULL, 
                 @to_Qty = Cast (@SearchValue AS NUMERIC (10,2))-0.01 , 
                 @isDetail =                                     1 
        END 
        ELSE 
        IF (@SearchColName ='vat') 
        BEGIN 
          IF (@SearchOption='equal') 
          SELECT @from_vat = Cast (@SearchValue AS NUMERIC (10,2)) , 
                 @to_vat = Cast (@SearchValue AS   NUMERIC (10,2)), 
                 @isDetail =1 
          ELSE 
          IF (@SearchOption='greater') 
          SELECT @from_vat = Cast (@SearchValue AS NUMERIC (10,2))+0.01 , 
                 @to_vat =                                         NULL, 
                 @isDetail =                                       1 
          ELSE 
          IF (@SearchOption='less') 
          SELECT @from_vat =                                     0.01 , 
                 @to_vat = Cast (@SearchValue AS NUMERIC (10,2))-0.01 , 
                 @isDetail =                                     1 
        END 
        ELSE 
        IF (@SearchColName ='mrp') 
        BEGIN 
          IF (@SearchOption='equal') 
          SELECT @from_Mrp = Cast (@SearchValue AS NUMERIC (10,2)) , 
                 @to_Mrp = Cast (@SearchValue AS   NUMERIC (10,2)), 
                 @isDetail =1 
          ELSE 
          IF (@SearchOption='greater') 
          SELECT @from_Mrp = Cast (@SearchValue AS NUMERIC (10,2))+0.01 , 
                 @to_Mrp =                                         NULL, 
                 @isDetail =                                       1 
          ELSE 
          IF (@SearchOption='less') 
          SELECT @from_Mrp =                                     NULL, 
                 @to_Mrp = Cast (@SearchValue AS NUMERIC (10,2))-0.01 , 
                 @isDetail =                                     1 
        END 
        ELSE 
        IF (@SearchColName ='discount') 
        BEGIN 
        set @discountType=1
          IF (@SearchOption='equal') 
          SELECT @from_discount = Cast (@SearchValue AS decimal(30,6)) , 
                 @to_discount = Cast (@SearchValue AS  decimal(30,6)) 
          ELSE 
          IF (@SearchOption='greater') 
          SELECT @from_discount = Cast (@SearchValue AS decimal(30,6))+0.01 , 
                 @to_discount =                                         NULL 
          ELSE 
          IF (@SearchOption='less') 
          SELECT @from_discount =                                     0.01, 
                 @to_discount = Cast (@SearchValue AS decimal(30,6))-0.01 
        END 
        ELSE 
        IF (@SearchColName ='discountValue') 
        BEGIN 
        set @discountType=2
          IF (@SearchOption='equal') 
          SELECT @from_discountValue = Cast (@SearchValue AS decimal(30,6)) , 
                 @to_discountValue = Cast (@SearchValue AS  decimal(30,6)) 
          ELSE 
          IF (@SearchOption='greater') 
          SELECT @from_discountValue = Cast (@SearchValue AS decimal(30,6))+0.01 , 
                 @to_discountValue =                                         NULL 
          ELSE 
          IF (@SearchOption='less') 
          SELECT @from_discountValue =                                     0.01, 
                 @to_discountValue = Cast (@SearchValue AS decimal(30,6))-0.01 
        END 
        ELSE 
        IF (@SearchColName ='invValue') 
        BEGIN 

          IF (@SearchOption='equal') 
          SELECT @from_InvAmt = Cast (@SearchValue AS NUMERIC (10,2)) , 
                 @to_InvAmt = Cast (@SearchValue AS   NUMERIC (10,2)), 
                 @isDetail =2 
          ELSE 
          IF (@SearchOption='greater') 
          SELECT @from_InvAmt = Cast (@SearchValue AS NUMERIC (10,2))+0.01 , 
                 @to_InvAmt =                                         NULL, 
                 @isDetail = 2
          ELSE 
          IF (@SearchOption='less') 
          SELECT @from_InvAmt =                                    NULL , 
                 @to_InvAmt = Cast (@SearchValue AS NUMERIC (10,2))-0.01 , 
                 @isDetail =  2
        END 
end 
         if (isnull(@customerName,'') ='')
             select @customerName =NULL

    IF (@isDetail =0) 
    BEGIN 

        IF(@SearchOption='All') 
        BEGIN
        SELECT  
        Count(1) OVER() salescount, 
               sales.id [Id], 
               isnull(sales.prefix,'') + sales.invoiceseries [InvoiceSeries], 
               sales.invoiceno [InvoiceNo], 
               sales.invoicedate [InvoiceDate], 
               sales.NAME [Name], 
               sales.email [Email], 
               sales.mobile [Mobile], 
               sales.filename [FileName], 
               isnull(sales.discount,0)-isnull(sales.RedeemPercent,0) [Discount],
               sales.discountvalue [DiscountValue], 
               isnull(sales.discountType,1) [DiscountType], 
               sales.RoundoffNetAmount [RoundoffNetAmount], 
               sales.credit [Credit], 
               sales.doctorname [DoctorName], 
               sales.createdat [CreatedAt], 
               sales.doctormobile [DoctorMobile], 
               sales.cancelstatus [Cancelstatus], 
               sales.doorDeliveryStatus [doorDeliveryStatus],
               sales.DeliveryType [DeliveryType],
               isnull(sales.TaxRefNo ,0) [TaxRefNo],
               sales.cashtype,
               --case isnull(sales.PaymentType ,'') when 'cheque' then  case when (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )then 'Cheque' else 'cash' end 
			   case when ((isnull(sales.PaymentType ,'')='cheque' or isnull(sales.PaymentType ,'')='Multiple') and (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )) then 'Cheque'
				when (isnull(sales.PaymentType ,'')='cheque' and isnull(BankDeposited,0) =  0  and isnull(AmountCredited,0)  =0) then 'cash'		
			   else  isnull(sales.PaymentType ,'') end [PaymentType], sales.PaymentType as [PaymentTypeOrig],
               0 ReturnAmount ,
               sales.NetAmount [NetAmount],
                (CASE WHEN ISNULL(R.TransactionId,ISNULL(R1.SalesID,'')) <> '' THEN 1 ELSE 0 END) AS IsReturnCreditAdjusted,
				ISNULL(I.Gstselect,0) AS Gstselect,
				isnull(U.Name,'')as Username
      FROM sales  (nolock)
	  JOIN Instance(nolock) I on I.Id = @InstanceId
        LEFT JOIN 
        (
            SELECT TransactionId FROM Settlements SM WHERE SM.AccountId = @AccountId AND SM.InstanceId = @InstanceId AND SM.TransactionType = 1
            GROUP BY TransactionId
        ) R ON R.TransactionId = sales.Id
        LEFT JOIN 
        (
        SELECT SR.SalesID FROM SalesReturn SR INNER JOIN Voucher V ON V.ReturnId = SR.Id WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceId AND V.TransactionType = 1
        AND V.OriginalAmount != V.Amount
        ) R1 ON R1.SalesID = sales.Id
		left join HQueUser U on U.id=Sales.CreatedBy
      WHERE    sales.accountid = @AccountId 
      AND      sales.instanceid = @InstanceId 
      /*AND      Isnull(sales.NAME ,'') = Isnull(@customerName, Isnull(sales.NAME ,'') )*/
      AND ISNULL(Sales.PatientId ,'') = isnull(@PatientId,ISNULL(Sales.PatientId ,'') )
      AND      Isnull(sales.mobile ,'') = Isnull(@customerMobile, Isnull(sales.mobile ,''))   
      AND      Isnull( sales.doctorname,'') = Isnull(@doctorName, Isnull( sales.doctorname,''))    
      AND      Isnull(sales.doctormobile,'') = Isnull(@doctorMobile,  Isnull(sales.doctormobile,'')) 
       /* --And       Isnull(sales.invoiceseries,'')!='' And Isnull(sales.invoiceseries,'')!='MAN'*/
      And    (Isnull(sales.invoiceseries,'')!='' And Isnull(sales.invoiceseries,'')!='MAN'    
      and Isnull( sales.invoiceno,'') LIKE  Isnull(@billNo,'')+'%')

      AND      Cast(sales.InvoiceDate AS DATE )BETWEEN Isnull(@From_BillDt, invoicedate) AND      Isnull(@To_BillDt, invoicedate)
       AND      isnull(sales.discount ,0) BETWEEN Isnull(@from_discount, isnull(sales.discount ,0) ) AND      Isnull(@to_discount, isnull(sales.discount ,0) )
      AND     isnull( sales.discountValue,0) BETWEEN Isnull(@from_discountValue,isnull( sales.discountValue,0)) AND      Isnull(@to_discountValue,isnull( sales.discountValue,0))
         AND isnull(sales.discountType,0) = isnull (@discountType,isnull(sales.discountType,0) )

    ORDER BY sales.createdat DESC offset @PageNo rows 
      FETCH next @PageSize rows only 
        END

        ELSE IF (@SearchOption='Default') -- Added default invoice no load issue 16-06-2017
        BEGIN
        print '456' + 'Default'
                  SELECT   Count(1) OVER() salescount, 
                           sales.id [Id], 
                           isnull(sales.prefix,'') + sales.invoiceseries [InvoiceSeries], 
                           sales.invoiceno [InvoiceNo], 
                           sales.invoicedate [InvoiceDate], 
                           sales.NAME [Name], 
                           sales.email [Email], 
                           sales.mobile [Mobile], 
                           sales.filename [FileName], 
                           isnull(sales.discount,0)-isnull(sales.RedeemPercent,0) [Discount],
                           sales.discountvalue [DiscountValue], 
                           isnull(sales.discountType,1) [DiscountType], 
                           sales.RoundoffNetAmount [RoundoffNetAmount], 
                           sales.credit [Credit], 
                           sales.doctorname [DoctorName], 
                           sales.createdat [CreatedAt], 
                           sales.doctormobile [DoctorMobile], 
                           sales.cancelstatus [Cancelstatus], 
                            sales.doorDeliveryStatus [doorDeliveryStatus],
                            sales.DeliveryType [DeliveryType],
                              isnull(sales.TaxRefNo ,0) [TaxRefNo],
                           sales.cashtype,
                           
                           --case isnull(sales.PaymentType ,'') when 'cheque' then  case when (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )then 'Cheque' else 'cash' end 
						   case when ((isnull(sales.PaymentType ,'')='cheque' or isnull(sales.PaymentType ,'')='Multiple') and (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )) then 'Cheque'
				when (isnull(sales.PaymentType ,'')='cheque' and isnull(BankDeposited,0) =  0  and isnull(AmountCredited,0)  =0) then 'cash'		
						   else  isnull(sales.PaymentType ,'') end [PaymentType], sales.PaymentType as [PaymentTypeOrig],
                           0 ReturnAmount ,
                           sales.NetAmount [NetAmount], 
                (CASE WHEN ISNULL(R.TransactionId,ISNULL(R1.SalesID,'')) <> '' THEN 1 ELSE 0 END) AS IsReturnCreditAdjusted,
				isnull(I.Gstselect,0) AS Gstselect,
					isnull(U.Name,'')as Username
      FROM sales  (nolock)
	  JOIN Instance(nolock) I ON I.Id = @InstanceId
        LEFT JOIN 
        (
            SELECT TransactionId FROM Settlements SM WHERE SM.AccountId = @AccountId AND SM.InstanceId = @InstanceId AND SM.TransactionType = 1
            GROUP BY TransactionId
        ) R ON R.TransactionId = sales.Id
        LEFT JOIN 
        (
        SELECT SR.SalesID FROM SalesReturn SR INNER JOIN Voucher V ON V.ReturnId = SR.Id WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceId AND V.TransactionType = 1
        AND V.OriginalAmount != V.Amount
        ) R1 ON R1.SalesID = sales.Id
		left join HQueUser U on U.id=Sales.CreatedBy
                  WHERE    sales.accountid = @AccountId 
                  AND      sales.instanceid = @InstanceId 
                  /*AND      Isnull(sales.NAME ,'') = Isnull(@customerName, Isnull(sales.NAME ,'') )*/
                  AND ISNULL(Sales.PatientId ,'') = isnull(@PatientId,ISNULL(Sales.PatientId ,'') )
                  AND      Isnull(sales.mobile ,'') = Isnull(@customerMobile,  Isnull(sales.mobile ,''))   
                  AND      Isnull( sales.doctorname,'') = Isnull(@doctorName,   Isnull( sales.doctorname,''))   
                  AND      Isnull(sales.doctormobile,'') = Isnull(@doctorMobile, Isnull(sales.doctormobile,'')) 
                  /* AND      (Isnull(sales.Prefix,'') +Isnull(sales.invoiceseries,'') + Isnull( sales.invoiceno,'') LIKE Isnull(@billNo,'')+'%'
					or Isnull(sales.invoiceseries,'') + Isnull( sales.invoiceno,'') LIKE Isnull(@billNo,'')+'%') */
                 AND (Isnull(sales.Prefix,'') +Isnull(sales.invoiceseries,'') + Isnull( sales.invoiceno,'')= Isnull(@billNo,'')
                  or Isnull(sales.invoiceseries,'') + Isnull( sales.invoiceno,'') = case when Isnull(@billNo,'') = '' then Isnull(sales.invoiceseries,'')  + Isnull							(sales.invoiceno,'') else @billNo end
				  ) 
                  AND isnull(sales.InvoiceSeries,'') =''
                  AND      Cast(sales.InvoiceDate AS DATE )BETWEEN Isnull(@From_BillDt, invoicedate) AND      Isnull(@To_BillDt, invoicedate)
                   AND      isnull(sales.discount ,0) BETWEEN Isnull(@from_discount, isnull(sales.discount ,0) ) AND      Isnull(@to_discount, isnull(sales.discount ,0) )
                  AND     isnull( sales.discountValue,0) BETWEEN Isnull(@from_discountValue,isnull( sales.discountValue,0)) AND      Isnull(@to_discountValue,isnull( sales.discountValue,0))
                 AND isnull(sales.discountType,0) = isnull (@discountType,isnull(sales.discountType,0) )

                ORDER BY sales.createdat DESC offset @PageNo rows 
                  FETCH next @PageSize rows only 
                END

        ELSE 
            BEGIN
        
		PRINT 'Custom/MAN' + ''''+ @billNo +''''

                  SELECT   Count(1) OVER() salescount, 
                           sales.id [Id], 
                           isnull(sales.prefix,'') + sales.invoiceseries [InvoiceSeries], 
                           sales.invoiceno [InvoiceNo], 
                           sales.invoicedate [InvoiceDate], 
                           sales.NAME [Name], 
                           sales.email [Email], 
                           sales.mobile [Mobile], 
                           sales.filename [FileName], 
                           isnull(sales.discount,0)-isnull(sales.RedeemPercent,0) [Discount],
                           sales.discountvalue [DiscountValue], 
                           isnull(sales.discountType,1) [DiscountType], 
                           sales.RoundoffNetAmount [RoundoffNetAmount], 
                           sales.credit [Credit], 
                           sales.doctorname [DoctorName], 
                           sales.createdat [CreatedAt], 
                           sales.doctormobile [DoctorMobile], 
                           sales.cancelstatus [Cancelstatus], 
                            sales.doorDeliveryStatus [doorDeliveryStatus],
                            sales.DeliveryType [DeliveryType],
                             isnull(sales.TaxRefNo ,0) [TaxRefNo],
                           sales.cashtype,
                           --case isnull(sales.PaymentType ,'') when 'cheque' then  case when (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )then 'Cheque' else 'cash' end 
						   case when ((isnull(sales.PaymentType ,'')='cheque' or isnull(sales.PaymentType ,'')='Multiple') and (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )) then 'Cheque'
				when (isnull(sales.PaymentType ,'')='cheque' and isnull(BankDeposited,0) =  0  and isnull(AmountCredited,0)  =0) then 'cash'		
						   else  isnull(sales.PaymentType ,'') end [PaymentType], sales.PaymentType as [PaymentTypeOrig],
                           0 ReturnAmount ,
                           sales.NetAmount [NetAmount], 
                (CASE WHEN ISNULL(R.TransactionId,ISNULL(R1.SalesID,'')) <> '' THEN 1 ELSE 0 END) AS IsReturnCreditAdjusted,
				ISNULL(I.Gstselect,0) AS Gstselect,
					isnull(U.Name,'')as Username
      FROM sales  (nolock)
        JOIN Instance(nolock) I ON I.Id = @InstanceId
		LEFT JOIN 
        (
            SELECT TransactionId FROM Settlements SM WHERE SM.AccountId = @AccountId AND SM.InstanceId = @InstanceId AND SM.TransactionType = 1
            GROUP BY TransactionId
        ) R ON R.TransactionId = sales.Id
        LEFT JOIN 
        (
        SELECT SR.SalesID FROM SalesReturn SR INNER JOIN Voucher V ON V.ReturnId = SR.Id WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceId AND V.TransactionType = 1
        AND V.OriginalAmount != V.Amount
        ) R1 ON R1.SalesID = sales.Id
			left join HQueUser U on U.id=Sales.CreatedBy
                  WHERE    sales.accountid = @AccountId 
                  AND      sales.instanceid = @InstanceId 
                  /*AND      Isnull(sales.NAME ,'') = Isnull(@customerName, Isnull(sales.NAME ,'') )*/
                  AND ISNULL(Sales.PatientId ,'') = isnull(@PatientId,ISNULL(Sales.PatientId ,'') )
                  AND      Isnull(sales.mobile ,'') = Isnull(@customerMobile,Isnull(sales.mobile ,''))   
                  AND      Isnull( sales.doctorname,'') = Isnull(@doctorName,Isnull( sales.doctorname,''))  
                  AND      Isnull(sales.doctormobile,'') = Isnull(@doctorMobile,  Isnull(sales.doctormobile,''))
                  /* --AND      ( Isnull(sales.invoiceseries,'') +Isnull(sales.Prefix,'')+ Isnull( sales.invoiceno,'') LIKE  Isnull(@billNo,'')+'%'
                  --or  Isnull(sales.invoiceseries,'')  + Isnull( sales.invoiceno,'') LIKE  Isnull(@billNo,'')+'%') */

                  AND ( Isnull(sales.invoiceseries,'') +Isnull(sales.Prefix,'')+ Isnull( sales.invoiceno,'')=Isnull(@billNo,'')
                  OR Isnull(sales.invoiceseries,'')  + Isnull( sales.invoiceno,'') =  case when Isnull(@billNo,'') = '' then Isnull(sales.invoiceseries,'')  + Isnull							(sales.invoiceno,'') else @billNo end OR Isnull(sales.invoiceseries,'') = ISNULL(@billNo,''))

                  AND      Cast(sales.InvoiceDate AS DATE )BETWEEN Isnull(@From_BillDt, invoicedate) AND      Isnull(@To_BillDt, invoicedate)
                   AND      isnull(sales.discount ,0) BETWEEN Isnull(@from_discount, isnull(sales.discount ,0) ) AND      Isnull(@to_discount, isnull(sales.discount ,0) )
                  AND     isnull( sales.discountValue,0) BETWEEN Isnull(@from_discountValue,isnull( sales.discountValue,0)) AND      Isnull(@to_discountValue,isnull( sales.discountValue,0))
                 AND isnull(sales.discountType,0) = isnull (@discountType,isnull(sales.discountType,0) )

                ORDER BY sales.createdat DESC offset @PageNo rows 
                  FETCH next @PageSize rows only 
                END
    END 
    else if (@isDetail =2)
    begin

      SELECT   Count(1) OVER() salescount, 
               sales.id [Id], 
               max(isnull(sales.prefix,'') + sales.invoiceseries) [InvoiceSeries], 
                max(sales.invoiceno) [InvoiceNo], 
                max(sales.invoicedate) [InvoiceDate], 
                max(sales.NAME) [Name], 
                max(sales.email) [Email], 
                max(sales.mobile) [Mobile], 
                max(sales.filename) [FileName], 
			    max(sales.discount)- max(isnull(sales.RedeemPercent,0)) [Discount],
                max(sales.discountvalue) [DiscountValue],
                max(isnull(sales.discountType,1)) [DiscountType], 
                max(sales.RoundoffNetAmount) [RoundoffNetAmount],
                max(sales.credit) [Credit], 
                max(sales.doctorname) [DoctorName], 
                 sales.createdat  [CreatedAt], 
                max(sales.doctormobile) [DoctorMobile], 
                max(sales.cancelstatus) [Cancelstatus],
                 max(sales.doorDeliveryStatus) [doorDeliveryStatus], 
                   max(sales.DeliveryType) [DeliveryType],
                     max(isnull(sales.TaxRefNo ,0)) [TaxRefNo],
                max(sales.cashtype)  [cashtype],
                Sum(Isnull(invoiceamount, 0) - Isnull(discountval, 0))  [SaleValue],
                max( case when ((isnull(sales.PaymentType ,'')='cheque' or isnull(sales.PaymentType ,'')='Multiple') and (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )) then 'Cheque'
				when (isnull(sales.PaymentType ,'')='cheque' and isnull(BankDeposited,0) =  0  and isnull(AmountCredited,0)  =0) then 'cash'		
				else  isnull(sales.PaymentType ,'') end )PaymentType, max(sales.PaymentType) as [PaymentTypeOrig],
				0 ReturnAmount,
               max(sales.NetAmount) [NetAmount] ,
               (CASE WHEN ISNULL(R.TransactionId,ISNULL(R1.SalesID,'')) <> '' THEN 1 ELSE 0 END) AS IsReturnCreditAdjusted,
			    max(cast(ISNULL(I.Gstselect,0) as int)) AS Gstselect,
					max(isnull(U.Name,'')) [Username]
			FROM   sales(nolock) 
			JOIN Instance(nolock) I ON I.Id = @InstanceId
			Inner JOIN salesitem (nolock) salesitem 
                               ON sales.id = salesitem.SalesID 
                             JOIN productstock  (nolock) ps 
                               ON ps.id = salesitem.productstockid                             
                            LEFT JOIN 
                            (
                                SELECT TransactionId FROM Settlements SM WHERE SM.AccountId = @AccountId AND SM.InstanceId = @InstanceId AND SM.TransactionType = 1
                                GROUP BY TransactionId
                            ) R ON R.TransactionId = sales.Id
                            LEFT JOIN 
                            (
                            SELECT SR.SalesID FROM SalesReturn SR INNER JOIN Voucher V ON V.ReturnId = SR.Id WHERE SR.AccountId = @AccountId 
							AND SR.InstanceId = @InstanceId AND V.TransactionType = 1
                            AND V.OriginalAmount != V.Amount
                            ) R1 ON R1.SalesID = sales.Id    
								                  
                             CROSS apply (SELECT  ( ( salesitem.quantity * 
                                                   ( 
                                                   Isnull( 
                                                   salesitem.sellingprice, 
                                                   ps.sellingprice) 
                                                      ) * 
                                                   Isnull(salesitem.discount, 0) 
                                                   / 
                                                   100 
                                                 ) + ( 
                                                 Isnull( 
                                                 salesitem.sellingprice 
                             , ps.sellingprice) * 
                             salesitem.quantity * Isnull( 
                             sales.discount, 0) / 100 ) )
                             AS 
                             DiscountVal, 
                             CONVERT(DECIMAL(18, 2), 
                              (Isnull(salesitem.sellingprice, ps.sellingprice) 
                             * 
                             salesitem.quantity) ) AS InvoiceAmount) AS b 
							 left join HQueUser U on U.id=Sales.CreatedBy    
      WHERE    sales.accountid = @AccountId 
      AND      sales.instanceid = @InstanceId 
          AND isnull(sales.discountType,0) = isnull (@discountType,isnull(sales.discountType,0) )
         and round(isnull(sales.NetAmount ,0),2)  between isnull(@from_InvAmt ,  Isnull(sales.NetAmount, 0)) and  isnull(@to_InvAmt ,  Isnull(sales.NetAmount, 0)) 
    group by sales.id,ISNULL(R.TransactionId,ISNULL(R1.SalesID,'')) , sales.createdat
	/* having Round( Sum(Isnull(invoiceamount, 0) - Isnull(discountval, 0)),0)  between isnull(@from_InvAmt ,Sum(Isnull(invoiceamount, 0) - Isnull(discountval, 0))) 
    and isnull(@to_InvAmt,Sum(Isnull(invoiceamount, 0) - Isnull(discountval, 0))) */

      ORDER BY sales.createdat DESC offset @PageNo rows 
      FETCH next @PageSize rows only        
    end
   else if (@isDetail =3)
   begin 
        SELECT  10 salescount, 
               sales.id [Id], 
               isnull(sales.prefix,'') + sales.invoiceseries [InvoiceSeries], 
               sales.invoiceno [InvoiceNo], 
               sales.invoicedate [InvoiceDate], 
               sales.NAME [Name], 
               sales.email [Email], 
               sales.mobile [Mobile], 
               sales.filename [FileName], 
               isnull(sales.discount,0) - isnull(sales.RedeemPercent,0) [Discount], 
               sales.discountvalue [DiscountValue],
               isnull(sales.discountType,1) [DiscountType], 
               sales.RoundoffNetAmount [RoundoffNetAmount],
               sales.credit [Credit], 
               sales.doctorname [DoctorName], 
               sales.createdat [CreatedAt], 
               sales.doctormobile [DoctorMobile], 
               sales.cancelstatus [Cancelstatus], 
                sales.doorDeliveryStatus [doorDeliveryStatus],
                  sales.DeliveryType [DeliveryType],
                  isnull(sales.TaxRefNo ,0) [TaxRefNo],
               sales.cashtype ,
                --case isnull(sales.PaymentType ,'') when 'cheque' then  case when (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )then  'Cheque' else 'cash' end 
				case when ((isnull(sales.PaymentType ,'')='cheque' or isnull(sales.PaymentType ,'')='Multiple') and (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )) then 'Cheque'
				when (isnull(sales.PaymentType ,'')='cheque' and isnull(BankDeposited,0) =  0  and isnull(AmountCredited,0)  =0) then 'cash'		
				else  isnull(sales.PaymentType ,'') end [PaymentType], sales.PaymentType as [PaymentTypeOrig]
                ,  0 ReturnAmount,
                sales.NetAmount  [NetAmount] ,
                (CASE WHEN ISNULL(R.TransactionId,ISNULL(R1.SalesID,'')) <> '' THEN 1 ELSE 0 END) AS IsReturnCreditAdjusted,
				ISNULL(I.Gstselect,0) as Gstselect,
					isnull(U.Name,'')as Username
      FROM sales  (nolock)
	  JOIN Instance(nolock) I ON I.Id = @InstanceId
        LEFT JOIN 
        (
            SELECT TransactionId FROM Settlements SM WHERE SM.AccountId = @AccountId AND SM.InstanceId = @InstanceId AND SM.TransactionType = 1
            GROUP BY TransactionId
        ) R ON R.TransactionId = sales.Id
        LEFT JOIN 
        (
        SELECT SR.SalesID FROM SalesReturn SR INNER JOIN Voucher V ON V.ReturnId = SR.Id WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceId AND V.TransactionType = 1
        AND V.OriginalAmount != V.Amount
        ) R1 ON R1.SalesID = sales.Id
		left join HQueUser U on U.id=Sales.CreatedBy          
      WHERE    sales.accountid = @AccountId 
      AND      sales.instanceid = @InstanceId 
     
      ORDER BY sales.createdat DESC offset 0 rows 
      FETCH next 10 rows only  
   end
    ELSE 
    BEGIN 
      SELECT   Count(1) OVER() salescount, 
               sales.id [Id], 
               isnull(sales.prefix,'') + sales.invoiceseries [InvoiceSeries], 
sales.invoiceno [InvoiceNo], 
               sales.invoicedate [InvoiceDate], 
               sales.NAME [Name], 
               sales.email [Email], 
               sales.mobile [Mobile], 
               sales.filename [FileName], 
               isnull(sales.discount,0) - isnull(sales.RedeemPercent,0) [Discount], 
               sales.discountvalue [DiscountValue],
               isnull(sales.discountType,1) [DiscountType], 
               sales.RoundoffNetAmount [RoundoffNetAmount],
               sales.credit [Credit], 
               sales.doctorname [DoctorName], 
               sales.createdat [CreatedAt], 
               sales.doctormobile [DoctorMobile], 
               sales.cancelstatus [Cancelstatus], 
                sales.doorDeliveryStatus [doorDeliveryStatus],
                  sales.DeliveryType [DeliveryType],
                    isnull(sales.TaxRefNo ,0) [TaxRefNo],
               sales.cashtype ,
     --          case isnull(sales.PaymentType ,'') when 'cheque' then  
					--case when (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )then  'Cheque' else 'cash' end 
				case when ((isnull(sales.PaymentType ,'')='cheque' or isnull(sales.PaymentType ,'')='Multiple') and (isnull(BankDeposited,0) =  1  or isnull(AmountCredited,0)  =1 )) then 'Cheque'
				when (isnull(sales.PaymentType ,'')='cheque' and isnull(BankDeposited,0) =  0  and isnull(AmountCredited,0)  =0) then 'cash'			  		   
			   else  isnull(sales.PaymentType ,'') end [PaymentType], 
			   sales.PaymentType as [PaymentTypeOrig],
                 0 ReturnAmount,
                (sales.NetAmount) [NetAmount] ,
      (CASE WHEN ISNULL(R.TransactionId,ISNULL(R1.SalesID,'')) <> '' THEN 1 ELSE 0 END) AS IsReturnCreditAdjusted
	  ,ISNULL(I.Gstselect,0) as Gstselect,
	  	isnull(U.Name,'')as Username
      FROM sales  (nolock)  
	  join Instance(nolock) I on I.Id = @InstanceId
        LEFT JOIN 
        (
            SELECT TransactionId FROM Settlements SM WHERE SM.AccountId = @AccountId AND SM.InstanceId = @InstanceId AND SM.TransactionType = 1
            GROUP BY TransactionId
        ) R ON R.TransactionId = sales.Id
        LEFT JOIN 
        (
        SELECT SR.SalesID FROM SalesReturn SR INNER JOIN Voucher V ON V.ReturnId = SR.Id WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceId AND V.TransactionType = 1
        AND V.OriginalAmount != V.Amount
        ) R1 ON R1.SalesID = sales.Id
		left join HQueUser U on U.id=Sales.CreatedBy          
      WHERE    sales.accountid = @AccountId 
      AND      sales.instanceid = @InstanceId 
      AND      sales.id IN 
               ( 
                          SELECT     salesitem.salesid 
                          FROM       salesitem  (nolock)
                          INNER JOIN productstock  (nolock)
                          ON         productstock.id = salesitem.productstockid 
                          WHERE      salesitem.instanceid = @InstanceId 
                          AND        salesitem.accountid = @AccountId 
                          AND        productstock.productid LIKE Isnull(@product, '')         +'%'
                          AND        Isnull(productstock.batchno ,'') = Isnull(@BtchNo, Isnull(productstock.batchno ,''))
                          AND        productstock.expiredate BETWEEN Isnull(@From_ExpiryDt, productstock.expiredate) AND        Isnull(@To_ExpiryDt, productstock.expiredate)
                          AND        salesitem.quantity BETWEEN Isnull(@from_qty,salesitem.quantity) AND        Isnull(@to_Qty,salesitem.quantity)
                          AND        salesitem.sellingprice BETWEEN Isnull(@from_mrp,salesitem.sellingprice) AND        Isnull(@to_mrp,salesitem.sellingprice)
                          AND    productstock.vat BETWEEN Isnull(@from_Vat,productstock.vat) AND        Isnull(@to_Vat,productstock.vat) )
                              AND isnull(sales.discountType,0) = isnull (@discountType,isnull(sales.discountType,0) )
      ORDER BY sales.createdat DESC offset @PageNo rows 
      FETCH next @PageSize rows only 
    END 

  END