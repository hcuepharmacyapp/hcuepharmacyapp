/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 **09/05/17     Poongodi R		Query optimized (address the Performance Issue)
*******************************************************************************/ 
--Usage : -- usp_GetDashboardTrendingSales '013513b1-ea8c-4ea8-9fed-054b260ee197', '18204879-99ff-4efd-b076-f85b4a0da0a3'
 create PROCEDURE [dbo].[usp_GetDashboardTrendingSales](@AccountId VARCHAR(36), @InstanceId VARCHAR(36) = '')
 AS
 BEGIN
   SET NOCOUNT ON

   IF @InstanceId != ''
   BEGIN
	
		Select top 10 Name, Sum(SalesQuantity - ReturnQuantity) as Stock from (
SELECT Product.Name, ISNULL(SalesItem.Quantity,0) SalesQuantity, 0 AS ReturnQuantity FROM SalesItem (nolock)
		INNER JOIN ProductStock(nolock) ON SalesItem.ProductStockId = ProductStock.Id INNER JOIN Product (nolock) ON Product.Id = ProductStock.ProductId 
		INNER JOIN Sales (nolock) ON Sales.Id = SalesItem.SalesId 
		WHERE SalesItem.AccountId  =  @AccountId AND SalesItem.InstanceId  =  @InstanceId AND Convert(date,Sales.InvoiceDate)
		 Between  dateadd(day,datediff(day,0,GETDATE()),-10) AND dateadd(day,datediff(day,0,GETDATE()),0)
		AND (ProductStock.Status is NULL or ProductStock.Status = 1)
		AND (Product.Status is NULL or Product.Status = 1)
		UNION ALL 
		SELECT Product.Name, 0 SalesQuantity, ISNULL(SalesReturnItem.Quantity,0) AS ReturnQuantity FROM SalesReturnItem (nolock)
		INNER JOIN ProductStock (nolock) ON SalesReturnItem.ProductStockId = ProductStock.Id INNER JOIN Product (nolock) ON Product.Id = ProductStock.ProductId 
		INNER JOIN SalesReturn (nolock) ON SalesReturn.Id = SalesReturnItem.SalesReturnId 
		WHERE SalesReturnItem.AccountId  =  @AccountId AND SalesReturnItem.InstanceId  =  @InstanceId AND Convert(date,SalesReturn.ReturnDate) 
		Between  dateadd(day,datediff(day,0,GETDATE()),-10) AND dateadd(day,datediff(day,0,GETDATE()),0)
		AND (ProductStock.Status is NULL or ProductStock.Status = 1)
		AND (Product.Status is NULL or Product.Status = 1)
		) a 
		GROUP BY Name 
		having Sum(SalesQuantity - ReturnQuantity) > 0
		ORDER BY Sum(SalesQuantity - ReturnQuantity) desc

	END
	ELSE
	BEGIN
		Select top 10 Name, Sum(SalesQuantity - ReturnQuantity) as Stock from (
SELECT  Product.Name, ISNULL(SalesItem.Quantity,0) SalesQuantity, 0 AS ReturnQuantity FROM SalesItem (nolock)
		INNER JOIN ProductStock (nolock) ON SalesItem.ProductStockId = ProductStock.Id INNER JOIN Product  (nolock) ON Product.Id = ProductStock.ProductId 
		INNER JOIN Sales (nolock) ON Sales.Id = SalesItem.SalesId 
		WHERE SalesItem.AccountId  =  @AccountId AND Convert(date,Sales.InvoiceDate) Between  dateadd(day,datediff(day,0,GETDATE()),-10) AND dateadd(day,datediff(day,0,GETDATE()),0)
		AND (ProductStock.Status is NULL or ProductStock.Status = 1)
		AND (Product.Status is NULL or Product.Status = 1)
		UNION ALL 
		SELECT Product.Name, 0 SalesQuantity, ISNULL(SalesReturnItem.Quantity,0) AS ReturnQuantity FROM SalesReturnItem (nolock)
		INNER JOIN ProductStock (nolock) ON SalesReturnItem.ProductStockId = ProductStock.Id INNER JOIN Product (nolock) ON Product.Id = ProductStock.ProductId 
		INNER JOIN SalesReturn (nolock) ON SalesReturn.Id = SalesReturnItem.SalesReturnId 
		WHERE SalesReturnItem.AccountId  =  @AccountId AND Convert(date,SalesReturn.ReturnDate) Between  dateadd(day,datediff(day,0,GETDATE()),-10) AND dateadd(day,datediff(day,0,GETDATE()),0)
		AND (ProductStock.Status is NULL or ProductStock.Status = 1)
		AND (Product.Status is NULL or Product.Status = 1)
		) a 
		GROUP BY Name 
		having Sum(SalesQuantity - ReturnQuantity) > 0
		ORDER BY Sum(SalesQuantity - ReturnQuantity) desc
	END
END
 