/** Date        Author          Description                              
*******************************************************************************        
 **21/06/2017   Violet			Prefix Added 
 **25/10/2017   Sarubala		Multiple payment included
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[usp_GetDoorDeliveryList](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime)
AS
BEGIN
	SET NOCOUNT ON 

	select s.Id as SalesId,u.Name as CreatedBy,s.Name,s.Mobile,s.Email,s.InvoiceDate,s.InvoiceNo,s.DeliveryType,s.DoorDeliveryStatus,s.PaymentType,ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries,
	case s.PaymentType when 'Multiple' then  f1.cash else 
	(case s.PaymentType when 'Cash' then s.SaleAmount  else 0 end) end [cash], 
	case s.PaymentType when 'Multiple' then f1.[card] else
	(case s.PaymentType when 'Card' then s.SaleAmount  else 0 end) end [card], 
	case s.PaymentType when 'Multiple' then f1.cheque else
	(case s.PaymentType when 'Cheque' then s.SaleAmount  else 0 end) end [cheque], 
	case s.PaymentType when 'Multiple' then f1.credit else isnull(customerpayment.credit,0) end [credit],
	isnull(f1.ewallet,0) [ewallet],isnull(f1.subPayment,'') subPayment,
	s.SaleAmount,s.RoundoffSaleAmount, Inst.Name as InstanceName 
	from sales as s(nolock) 
	left join ( 
	select cp.SalesId,sum(cp.Credit) as credit from CustomerPayment as cp(nolock) left join 
	(select * from Sales (nolock) where AccountId=@AccountId AND instanceid = isnull(@instanceid,instanceid)) as s on s.id = cp.SalesId group by cp.SalesId	) 
	as customerpayment on customerpayment.SalesId = s.Id
	left join (Select * from HQueUser(nolock) where AccountId =@AccountId) as u on u.id = s.CreatedBy
	join (Select * from Instance(nolock) where AccountId =@AccountId) Inst on Inst.Id = ISNULL(@InstanceId,s.InstanceId)
	left join dbo.udf_getPayments(@AccountId,@InstanceId,@StartDate,@EndDate) f1 on f1.salesid = s.Id

	WHERE S.AccountId = @AccountId AND S.InstanceId =ISNULL(@InstanceId,S.InstanceId) 
	AND Convert(date,S.InvoiceDate) BETWEEN @StartDate AND @EndDate 
	AND s.Cancelstatus is NULL
	and s.DeliveryType='Home Delivery'
	and ((s.PaymentType='Cash'and s.DeliveryType='Home Delivery')
	or (s.PaymentType='Card'and s.DeliveryType='Home Delivery')
	or (s.PaymentType='Cheque'and s.DeliveryType='Home Delivery')
	or (s.PaymentType='Multiple'and s.DeliveryType='Home Delivery'))
	and isnull(s.DoorDeliveryStatus,'') <>''
	order by s.InvoiceDate desc,s.InvoiceNo desc
  
	SET NOCOUNT OFF
END