﻿/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 29/05/2017 Bala			Account wise product stock data  
*******************************************************************************/ 
Create procedure [dbo].[usp_GetProductsAccountWise](@productname varchar(1000),@accountid char(36),@instanceid char(36))

as
begin

DECLARE @NOofRecord int =10000000


if (LEN(@productname) <= 2) 
set @NOofRecord =20

 


SELECT    *
FROM
  (SELECT p.Name,
          p.Id,
          p.AccountId,
          p.InstanceId,
          p.Code,
          p.Manufacturer,
          p.KindName,
          p.StrengthName,
          p.Type,
          p.Schedule,
          p.Category,
          p.Packing,
          p.CreatedAt,
          p.UpdatedAt,
          p.CreatedBy,
          p.UpdatedBy,
          p.VAT,
          p.Price,
          p.Status,
          ip.RackNo RackNo,
		  p.GenericName,
		  p.Eancode,
          isNull(sum(ps.stock),0) AS Totalstock
   FROM product p
   Left join productinstance ip on ip.accountid = p.accountid 
   and ip.instanceid = @instanceid
   and ip.productid = p.id
   LEFT JOIN ProductStock ps ON p.id = ps.ProductId
   AND (ps.Status IS NULL OR ps.Status = 1)
   AND ps.AccountId=@accountid
   AND ps.InstanceId = @instanceid
   WHERE (p.Name LIKE ''+@productname+'%' or p.Id=@productname) and p.AccountId=@accountid and (p.Status is null or p.Status = 1)
    group by p.Id,p.Name,p.AccountId,p.InstanceId,p.Code,p.Manufacturer,p.KindName,p.StrengthName,p.Type,    
	       p.Schedule,p.Category,p.Packing,p.CreatedAt,p.UpdatedAt,p.CreatedBy,p.UpdatedBy,p.VAT,p.Price,p.Status,ip.RackNo,p.GenericName,p.Eancode
     ) a 
ORDER BY a.Name ASC,
         a.accountId,
         a.Totalstock DESC offset 0 rows 
    FETCH next @NOofRecord rows only 
		 
end