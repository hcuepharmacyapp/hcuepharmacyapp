 -- Exec usp_GetDashboardDailyPatientVisitList '18204879-99ff-4efd-b076-f85b4a0da0a3'
 CREATE PROCEDURE [dbo].[usp_GetDashboardDailyPatientVisitList] (@AccountId varchar(36))
 AS
 BEGIN
   SET NOCOUNT ON
  
	select Branch,PatientTypeName, PatientVisitCount from (select Instance.Name AS Branch,LOWER(SalesType.Name) as PatientTypeName,count(Sales.SalesType)as PatientVisitCount
	from Account
	--INNER JOIN HQueUser on HQueUser.Accountid=Account.id
	--INNER JOIN Instance on Instance.AccountId = HQueUser.AccountId AND HQueUser.InstanceId is NULL AND HQueUser.Userid!=''
	INNER JOIN Instance on Instance.AccountId = Account.id
	INNER join SalesType SalesType on SalesType.AccountId = Account.id and SalesType.InstanceId =Instance.id
	INNER JOIN Sales Sales ON Sales.SalesType = SalesType.ID  and sales.AccountId=Account.id
	WHERE Convert(date,InvoiceDate)  = dateadd(day,datediff(day,1,GETDATE()),0) and sales.Cancelstatus is NULL
	and Account.id =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
	group by Instance.Name,SalesType.Name
	UNION
	select Instance.Name AS Branch,'-' As PatientTypeName,'-' as PatientVisitCount
	from Account
	--INNER JOIN HQueUser on HQueUser.Accountid=Account.id
	INNER JOIN Instance on Instance.AccountId = Account.id          
	WHERE Account.id =@AccountId --and HQueUser.UserId='saravanan@goclinix.com'
	group by Instance.Name ) as a group by a.Branch,a.PatientTypeName,a.PatientVisitCount order by a.Branch,a.PatientTypeName desc

 END
  



