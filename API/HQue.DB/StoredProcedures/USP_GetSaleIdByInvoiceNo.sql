﻿/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
**  26/08/2017  lawrence		Get Sales/Return Id By Invoice/Return No.
*******************************************************************************
--USP_GetSaleIdByInvoiceNo @Type = 0, @TypeValue='', @Value='62', @accountid='c503ca6e-f418-4807-a847-b6886378cf0b', @instanceid='3e2ec066-1551-4527-be53-5aa3c5b7fb7d'
--USP_GetSaleIdByInvoiceNo @Type = 1, @TypeValue=null, @Value='240', @accountid='c503ca6e-f418-4807-a847-b6886378cf0b', @instanceid='c6b7fc38-8e3a-48af-b39d-06267b4785b4',@returnStatus=false
*/ 

CREATE PROC USP_GetSaleIdByInvoiceNo(@Type INT, @TypeValue varchar(20), @Value varchar(5), @accountid char(36), @instanceid char(36), @returnStatus bit)
AS
BEGIN
print @returnStatus
IF (@returnStatus=0)
BEGIN
IF @Type = 1
BEGIN

select s.Id from Sales s inner join Patient p on s.patientId=p.Id
WHERE (s.InvoiceSeries = @TypeValue OR s.InvoiceSeries = '') AND s.InvoiceNo = @Value 
AND s.AccountId = @accountid and s.InstanceId=@instanceid and p.PatientType=2 and s.cancelstatus is null
order by s.InvoiceDate desc
END

ELSE IF (@Type=2 OR @Type=3)
BEGIN
select s.Id from Sales s inner join Patient p on s.patientId=p.Id 
WHERE s.InvoiceSeries = @TypeValue AND s.InvoiceNo = @Value  
AND s.AccountId = @accountid and s.InstanceId=@instanceid and p.PatientType=2 and s.cancelstatus is null
order by s.InvoiceDate desc
END
END
ELSE 
	IF @Type = 1
	BEGIN
		select sr.Id from SalesReturn sr
		WHERE (sr.InvoiceSeries = @TypeValue OR sr.InvoiceSeries = '') and sr.AccountId = @accountid and sr.InstanceId=@instanceid 
		and sr.ReturnNo= @Value
		order by sr.CreatedAt desc
	END
	ELSE IF (@Type=2 OR @Type=3)
	BEGIN
		select sr.Id from SalesReturn sr 
		WHERE sr.InvoiceSeries = @TypeValue and sr.AccountId = @accountid and sr.InstanceId=@instanceid 
		and sr.ReturnNo= @Value
		order by sr.CreatedAt desc
	END

END