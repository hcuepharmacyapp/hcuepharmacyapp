﻿/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
**04/05/2017  Poongodi R		ProductId filter added 
*******************************************************************************/ 
--usp_GetPurchaseHistoryDeletedItems 'de02dedc-d205-4ba9-9edd-9196e64b3050'
 Create PROCEDURE [dbo].[usp_GetPurchaseHistoryDeletedItems](@VendorPurchaseId varchar(max), @Product varchar(50))
 as
 BEGIN
   SET NOCOUNT ON

  
 SELECT vpi.Id,
        vpi.AccountId,
		vpi.InstanceId,
		vpi.VendorPurchaseId,
		vpi.ProductStockId,
		vpi.PackageSize,
		vpi.PackageQty,
		vpi.PackagePurchasePrice,
		vpi.PackageSellingPrice,
		vpi.PackageMRP,
		vpi.Quantity,
		vpi.PurchasePrice,
		vpi.OfflineStatus,
		vpi.CreatedAt,
		vpi.UpdatedAt,
		vpi.CreatedBy,
		vpi.UpdatedBy,
		vpi.FreeQty,
		vpi.Discount,
		vpi.Status,
		vpi.VAT,
		vpi.VatValue,
		vpi.DiscountValue,
		vpi.fromTempId,
		vpi.fromDcId,

		ps.SellingPrice  as productStockSellingPrice,
		ps.MRP as productStockMRP,
		ps.VAT as productStockvat,
		ps.CST as productStockcst,
		ps.BatchNo as productStockbatchno,
		ps.ExpireDate as productStockexpiredate,
		ps.TaxType as productStocktaxtype,
		

		p.Name as productname,

		u.Name as username

		  FROM VendorPurchaseItem  as vpi inner join ProductStock as ps on vpi.ProductStockId = ps.Id
		                                  inner join Product as p on p.Id = ps.ProductId
										  inner join HQueUser as u on u.Id = vpi.UpdatedBy

									   WHERE vpi.VendorPurchaseId in (select a.id from dbo.udf_Split(@VendorPurchaseId ) a) 
									         and vpi.Status = 2
											and ps.ProductId  like isnull(@product, '')+'%'  
									   ORDER BY vpi.CreatedAt desc 
           
 END
