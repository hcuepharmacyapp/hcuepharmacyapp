 
/*                             
******************************************************************************                            
** File: [Usp_vat_Sales_annx15_rpt] 
** Name: [Usp_vat_Sales_annx15_rpt]                             
** Description: To Get Sales details 
** 
** This template can be customized:                             
**                              
** Called by:                              
**                              
**  Parameters:                             
**  Input                Output                             
**  ----------              -----------                             
** 
** Author:Poongodi R 
** Created Date: 28/01/2017 
** 
*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
**03/02/2017	Poongodi		VAT calculation issue fixed
**04/02/2017	Poongodi		Sales Return Section Removed as per Sundar input   
** 13/02/2017	Poongodi		Invoiceseries col added along with invoicenumber
** 30/03/2017  Poongodi			Invoice date changed to created at
** 30/06/2017  Poongodi			TaxRefNovalidation added
*******************************************************************************/ 
CREATE PROC dbo.Usp_vat_Sales_annx15_rpt(@AccountId  CHAR(36), 
                                  @InstanceId CHAR(36), 
                                  @StartDate  DATETIME ='2016-01-01', 
                                  @EndDate    DATETIME='2016-03-01') 
AS 
  BEGIN 
 

	  /*
	  Commodity code hard coded based on VAT %
	  VAT		Commoditycode
	  5			2044
	  14.5		301
	  0 or other 752
	  
	  Category
	  Purchare - R for all vat %

	  	1141213551
	  */
	    declare @GstDate date ='2017-06-30'

	  	Select isnull(Name,'-') [BuyerName] ,'-' BuyerTin,  Invoicedate, InvoiceNo, 
					 CASE Isnull(vat, 0) 
               WHEN 5 THEN '2044' 
               WHEN 14.5 THEN '301' 
               ELSE '752' 
             END						                      [Commoditycode],
			Sum(isnull(InvoiceAmount,0) -  isnull(DiscountVal,0)) - ( round((Sum(isnull(InvoiceAmount,0) -  isnull(DiscountVal,0)) /(100+ Isnull(Vat, 0))*100 )   * 
                   Isnull(Vat, 0) / 
                   100, 2) ) [SalesValue],
			Isnull(vat, 0) VAT,
			      round((Sum(isnull(InvoiceAmount,0) -  isnull(DiscountVal,0)) /(100+ Isnull(Vat, 0))*100 )   * 
                   Isnull(Vat, 0) / 
                   100, 2)                                [VatValue], 
				      Category from (
					select sales.invoicedate AS InvoiceDate,ltrim(isnull(sales.Prefix,'')+isnull(sales.InvoiceSeries,'')+ ' '+  sales.invoiceno) as InvoiceNo,sales.Discount as  salesDiscount,
					sales.Name ,salesitem.quantity,
		 
					salesitem.Discount [salesitem.Discount],
					 
					ps.VAT, p.Name as ProductName,  'F' Category ,
convert(decimal(18,2),(CASE isnull( salesitem.SellingPrice,0) WHEN    0 then ps.SellingPrice  else salesitem.SellingPrice   END) * salesitem.quantity) As InvoiceAmount
                    , sales.Discount as Discount 
                    ,(salesitem.Quantity *
					(CASE isnull( salesitem.SellingPrice,0) WHEN    0 then ps.SellingPrice  else salesitem.SellingPrice   END) * isnull(salesitem.Discount,0) / 100) +
					(isnull(salesitem.SellingPrice  ,ps.SellingPrice )* salesitem.quantity * isnull(sales.Discount,0) /100)  as DiscountVal
                    from sales sales 
                    join salesitem salesitem on sales.id= salesitem.salesid
                    join productstock ps on ps.id=salesitem.productstockid
                    inner join Product p on p.Id=ps.ProductId
					
                    WHERE sales.InstanceId = @InstanceId AND sales.AccountId =@AccountId and isnull(ps.VAT,0)>0
					AND Convert(date,sales.CreatedAt) BETWEEN @StartDate AND @EndDate
					and isnull(sales.cancelstatus ,0) =0 
					and isnull(sales.TaxRefNo,0) =0
					and CONVERT(DATE, sales.createdat) <=@GstDate
				/*	union all 
					select SalesReturn.ReturnDate AS InvoiceDate,Sales.InvoiceNo as InvoiceNo,0 as  salesDiscount,
					Sales.Name ,SalesReturnItem.quantity,
		 
					SalesReturnItem.Discount [SalesReturnItem.Discount],
					 
					ps.VAT, p.Name as ProductName,  'R' Category ,
convert(decimal(18,2),(CASE isnull( SalesReturnItem.MrpSellingPrice,0) WHEN    0 then ps.SellingPrice  else SalesReturnItem.MrpSellingPrice   END) * SalesReturnItem.quantity) As InvoiceAmount
                    , Sales.Discount as Discount 
                    ,(SalesReturnItem.Quantity *
					(CASE isnull( SalesReturnItem.MrpSellingPrice,0) WHEN    0 then ps.SellingPrice  else SalesReturnItem.MrpSellingPrice   END) * isnull(SalesReturnItem.Discount,0) / 100) + 
					(isnull(SalesReturnItem.MrpSellingPrice  ,ps.SellingPrice )* SalesReturnItem.quantity * isnull(sales.Discount,0) /100) as DiscountVal
                    from SalesReturn SalesReturn 
                    join SalesReturnItem SalesReturnItem on SalesReturn.id= SalesReturnItem.SalesReturnId
                    join productstock ps on ps.id=SalesReturnItem.productstockid
                    inner join Product p on p.Id=ps.ProductId
					 inner join sales on sales.id = salesreturn.salesid 
                    WHERE SalesReturn.InstanceId = @InstanceId AND SalesReturn.AccountId =@AccountId and isnull(ps.VAT,0)>0
					AND Convert(date,SalesReturn.ReturnDate) BETWEEN @StartDate AND @EndDate
					and isnull(sales.cancelstatus ,0) =0 */
                   ) as one   group by [Name] ,  Category, Invoicedate, InvoiceNo,Isnull(vat, 0)
                    ORDER BY  Category,vat,Invoiceno   
					 
					END  --12773