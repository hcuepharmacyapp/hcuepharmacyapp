create PROCEDURE dbo.usp_GetJournalReport(@Filter varchar(20),@InstanceId NVARCHAR(36), @AccountId NVARCHAR(36), @StartDate  DATETIME,@EndDate DATETIME)
AS
BEGIN	
IF @Filter = 'CustomerReceipts' 
BEGIN
SELECT 
	s.Id,
	s.InvoiceNo,
	Inst.Name as InstanceName,
	ISNULL(s.Name,'') AS Name,
	isnull(s.prefix,'') + s.InvoiceSeries [InvoiceSeries],
	s.InvoiceDate,		  
	s.SaleAmount AS InvoiceValue,		                       
	cp.PaymentType,
	cp.ChequeNo,
	cp.ChequeDate,
	cp.CardNo,
	cp.CardDate,
	cp.CreatedAt,
	cp.Remarks,
	cp.Debit,   
	cp.Credit,
	cp.CustomerId
	--a.id,
	--a.Name,
	--a.Status
	FROM SalesItem as SI WITH(NOLOCK) 
			inner join Sales as S WITH(NOLOCK) on SI.SalesId = S.Id
			inner JOIN Instance Inst WITH(NOLOCK) ON Inst.Id = @InstanceId
			inner join ProductStock as PS WITH(NOLOCK) on SI.ProductStockId  = PS.Id
			inner join CustomerPayment as CP WITH(NOLOCK) on CP.SalesId = S.Id
			left join (select * from SalesPayment WITH(NOLOCK) where AccountId=@AccountId and InstanceId = ISNULL(@InstanceId,InstanceId) and PaymentInd = 4 and IsActive = 1) sp on sp.SalesId=s.Id
			WHERE CP.Accountid=@AccountId and CP.InstanceId=ISNULL(@InstanceId,CP.InstanceId) 
			AND CAST(CP.createdat AS date) BETWEEN CAST(isnull(@StartDate,CP.createdat) AS date) and CAST(isnull(@EndDate,CP.createdat) AS date)
	group by  s.Id,s.InvoiceNo, isnull(s.prefix,'') +s.InvoiceSeries,s.InvoiceDate, s.SaleAmount,cp.customerID,cp.PaymentType,cp.CreatedAt,cp.Remarks,cp.Credit,cp.ChequeNo,cp.Debit,cp.ChequeDate,cp.CardNo,
	cp.CardDate,Inst.Name,s.Name
END

	
ELSE IF @Filter = 'VendorPayment'   
BEGIN
   SET NOCOUNT ON
	declare @condition nvarchar(max)	
	Declare @qry nvarchar(max)
	Declare @VendorId char(36) = Null
	SET @condition=''

 	 if (@StartDate ='') select @StartDate  = null
	 if (@EndDate ='') select @EndDate  = null


	 DECLARE @table1 TABLE
				(
				 InstanceName varchar(150),
				 Name varchar(150),
				 CreatedAt datetime,
				 InvoiceNo varchar(150),
				 GoodsRcvNo varchar(150),
				 InvoiceDate datetime,
				 BillSeries varchar(150),
				 VendorPurchaseId char(36),
				 InvoiceAmount decimal(10,2),
				 PaymentMode varchar(150),
				 [ChequeNo] varchar(150),
				 [ChequeDate] datetime,
				 Debit decimal(10,2),
				 Balance decimal(10,2),
				 BankName varchar(150),
				 BankBranchName varchar(150),
				 ifscCode varchar(150)
				)


			 DECLARE @table2 TABLE
				(
				 InstanceName varchar(150),
				 Name varchar(150),
				 CreatedAt datetime,
				 InvoiceNo varchar(150),
				 GoodsRcvNo varchar(150),
				 InvoiceDate datetime,
				 BillSeries varchar(150),
				 VendorPurchaseId char(36),
				 InvoiceAmount decimal(10,2),
				 PaymentMode varchar(150),
				 [ChequeNo] varchar(150),
				 [ChequeDate] datetime,
				 Debit decimal(10,2),
				 Balance decimal(10,2),
				 BankName varchar(150),
				 BankBranchName varchar(150),
				 ifscCode varchar(150)
				)


				 DECLARE @table3 TABLE
				(
				 InstanceName varchar(150),
				 Name varchar(150),
				 CreatedAt datetime,
				 InvoiceNo varchar(150),
				 GoodsRcvNo varchar(150),
				 InvoiceDate datetime,
				 BillSeries varchar(150),
				 VendorPurchaseId char(36),
				 InvoiceAmount decimal(10,2),
				 PaymentMode varchar(150),
				 [ChequeNo] varchar(150),
				 [ChequeDate] datetime,
				 Debit decimal(10,2),
				 Balance decimal(10,2),
				 BankName varchar(150),
				 BankBranchName varchar(150),
				 ifscCode varchar(150)
				)

				  Insert into @table1	
				  
				  SELECT  Inst.Name InstanceName, MAX(Vendor.Name) Name,
				  CAST(Payment.CreatedAt AS DATE) as CreatedAt,
				 mAX(  VendorPurchase.InvoiceNo ) InvoiceNo ,  
				 max(isnull(VendorPurchase.GoodsRcvNo ,'')) As GoodsRcvNo,
					 mAX(VendorPurchase.InvoiceDate )As InvoiceDate,
					  MAX( ISNULL(VendorPurchase.Prefix,'')+ISNULL(VendorPurchase.BillSeries,'') )BillSeries,
				VendorPurchaseId,
				CAST(max(po.InvoiceAmount) As Decimal(10, 2)) InvoiceAmount,
				case  (isnull(Payment.PaymentType,'')) when 'cash' then  (isnull(Payment.PaymentType,'')) when 'cheque' then  (isnull(Payment.PaymentType,'')) else  (isnull						(Payment.PaymentMode,'Cash')) end [PaymentMode],max(Payment.ChequeNo) [ChequeNo],max(Payment.ChequeDate) [ChequeDate],sum(Isnull(Payment.Debit,0)) as Debit,
				case when CAST( (Round(max(po.InvoiceAmount),0) - max(isnull(DebitAmt.TotalDebit,0))) As Decimal(10, 2)) < 0 then 0 else  
				CAST( (Round(max(po.InvoiceAmount),0) - max(isnull(DebitAmt.TotalDebit,0))) As Decimal(10, 2))  end 
				As Balance,
				max(isnull(Payment.BankName,'')) [BankName],
				max(isnull(Payment.BankBranchName,'')) [BankBranchName],
				max(isnull(Payment.IfscCode,'')) IfscCode FROM Payment 
				INNER JOIN VendorPurchase ON Payment.VendorPurchaseId = VendorPurchase.Id
				and isnull(VendorPurchase.cancelstatus ,0)  = 0
				INNER JOIN Vendor ON Payment.VendorId=Vendor.Id
				inner JOIN Instance Inst WITH(NOLOCK) ON Inst.Id = @InstanceId
				CROSS APPLY  (select Round(Isnull(dbo.CreditCalculate(VendorPurchaseId),0),0)   AS InvoiceAmount ) As [PO]
				CROSS APPLY  ((select  sum(isnull(debit,0)) TotalDebit from payment where VendorPurchaseId = VendorPurchase.id ) )As [DebitAmt]
				WHERE  Payment.InstanceId=@InstanceId AND Payment.AccountId  = @AccountId
				AND CAST(Payment.CreatedAt AS date) BETWEEN CAST(isnull(@StartDate, Payment.CreatedAt) AS date) and CAST(isnull(@EndDate,Payment.CreatedAt) AS date)
				AND VendorPurchase.VendorId=isnull(@VendorId,VendorPurchase.VendorId)
				 and isnull(Payment.Debit ,0) > 0 
				 group by Inst.Name,VendorPurchaseId, CAST(Payment.CreatedAt AS DATE),po.InvoiceAmount,isnull(Payment.PaymentMode,'Cash'), isnull(Payment.PaymentType,'')
 
 ORDER BY CAST(Payment.CreatedAt AS DATE)  desc 
 
 Insert into @table2
 Select Inst.Name InstanceName, v.Name, Max(p.CreatedAt) as CreatedAt, '' as InvoiceNo, '' as GoodsRcvNo, Max(p.transactiondate) as InvoiceDate, '' as BillSeries, null as vendorpurchaseId, 0 as InvoiceAmount,
 'Cash' as [PaymentMode], '' as [ChequeNo], null as chequeDate, sum(Isnull(p.Debit,0)) as Debit,
 CAST( sum(p.credit) - sum(p.debit) As Decimal(10, 2)) as balance,
 '' as [BankName],
 '' as [BankBranchName],
 '' as IfscCode
 From Payment P inner join Vendor v on p.VendorId = v.id
 inner JOIN Instance Inst WITH(NOLOCK) ON Inst.Id = @InstanceId
 Where p.VendorId = Isnull (@VendorId, p.VendorId)
 and  p.VendorPurchaseId is null   
 and p.InstanceId=@InstanceId AND p.AccountId  = @AccountId
 Group by Inst.Name,v.name

 Insert into @table3
			Select a.* from 
			(Select * from @table1
			union 
			Select * from @table2) as a

 --select * from @table3


 select InstanceName, Name , CreatedAt, InvoiceNo, GoodsRcvNo, InvoiceDate ,BillSeries,vendorpurchaseId,InvoiceAmount,
 [PaymentMode], [ChequeNo], chequeDate, sum(Debit) as Debit, sum(balance) as Balance,
 [BankName],[BankBranchName], ifsccode 
 From @table3
 Group by InstanceName, Name , CreatedAt, InvoiceNo, GoodsRcvNo, InvoiceDate,BillSeries,vendorpurchaseId,InvoiceAmount,
 [PaymentMode], [ChequeNo], chequeDate, [BankName],[BankBranchName], ifsccode 


 END
END


