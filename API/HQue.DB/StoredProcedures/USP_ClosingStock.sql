/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**19/10/17     Poongodi R		SP Created to Get Closing STock
*******************************************************************************/
Create proc usp_closingStock_Scheduler(@AccountId char(36),@InstanceId  char(36),@TransDate date,@CreatedBy char(36),@Offline bit )
as
begin
set @TransDate = dateadd(d,-1,@TransDate)
declare @Tran bit =0, @InsCount int =0,
@LastExecutedDate date
 declare @Output nvarchar(max) =''
 select @InsCount = Count(1) from instance where id =@InstanceId and isnull(IsOfflineInstalled,0) = @Offline
 if (   Isnull(@InsCount,0) = 0 )
 begin
	return
 end
if Not exists(SElect * from SchedulerLog (nolock) where instanceid =@InstanceId and AccountId =@AccountId and Task='Closing Stock')
begin
	if (isnull((select count(1) from productstock (nolock) where  instanceid =@InstanceId and AccountId =@AccountId),0) >0 )
	begin
		Insert into ClosingStock (Id,AccountId,InstanceId, ProductStockId,TransDate,ClosingStock,StockValue,GstTotal,IsOffline,createdby)
		select NEWID(), @AccountId,@InstanceId,Id, @TransDate,Stock, (CASE isnull(taxrefno,0)  WHEN 1 THEN  ISNULL(ps.PurchasePrice,0)  
		else (isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0))) +((isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0)))* isnull(ps.gsttotal,0) /100) end ) * Stock, gsttotal
		,@Offline,@CreatedBy 
		from ProductStock(nolock) ps where  cast(updatedat as date ) <=@TransDate 
		 and  InstanceId =@InstanceId 

		select @Tran  =1
  end
end 
else if Not exists(SElect * from SchedulerLog where instanceid =@InstanceId and AccountId =@AccountId and Task='Closing Stock' and cast(ExecutedDate as date) =@TransDate)
begin
	 SElect top 1 @LastExecutedDate = ExecutedDate   from SchedulerLog where instanceid =@InstanceId and AccountId =@AccountId and Task='Closing Stock'  order by ExecutedDate desc 
	/*Insert current date stock into closing stock table */
	Insert into ClosingStock (Id,AccountId,InstanceId, ProductStockId,TransDate,ClosingStock,StockValue,GstTotal,IsOffline, CreatedBy)
		select NEWID(),@AccountId,@InstanceId, Id , @TransDate,Stock, (CASE isnull(taxrefno,0)  WHEN 1 THEN  ISNULL(ps.PurchasePrice,0)  
		else (isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0))) +((isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0)))* isnull(ps.gsttotal,0) /100) end ) * Stock,
		 gsttotal ,@Offline,@CreatedBy
		from ProductStock ps where cast(updatedat as date ) between isnull(@LastExecutedDate,cast(updatedat as date )) and @TransDate 
		and InstanceId =@InstanceId 

select @Tran  =1
 end

 if (@Tran =1)
 begin
 	Insert into SchedulerLog (Id,AccountId,InstanceId,Task,ExecutedDate,IsOffline,CreatedBy,UpdatedBy)
	Values (NEWID(), @AccountId,@InstanceId,'Closing Stock', @TransDate,@Offline,@CreatedBy,@CreatedBy)

	set @Output= (  SELECT
	 ( select * from ClosingStock  where TransDate  = @TransDate  and Instanceid =@InstanceId for xml  path ('ClosingStock'),type)  AS 'ClosingStocks',
	  ( select * from SchedulerLog  where ExecutedDate  = @TransDate  and Instanceid =@InstanceId  for xml  path ('SchedulerLog'),type) AS 'Log'
	FOR XML PATH(''), ROOT('ClosingStocksLog') )
 
 end
	select @Output [stockxml]
 end
