/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 21/08/2017	Poongodi		Created 
*******************************************************************************/
Create PROCEDURE usp_syncproduct (@AccountID as varchar(36), @FilePath as varchar(1000), @Createdby varchar(36), @IsCloud as int) AS
BEGIN
 
 IF OBJECT_ID(N'tempdb..#tmpproduct', N'U') IS NOT NULL
	DROP TABLE #tmpVendor

 /*IF OBJECT_ID(N'dbo.tmpStockImport', N'U') IS NULL
 BEGIN
  PRINT 'Creating Table tmpStockImport'*/
	 CREATE   TABLE #tmpproduct
	 (
	 
		[RowREF]		int null,
		id char(36),
	[ProductCode]	varchar(50) NOT NULL,
	[ProductName]	varchar(100) NOT NULL,
	[Manufacturer]	varchar(100) NOT NULL,
	[ScheduleCode]	varchar(200) NOT NULL,
	[Category]		varchar(200) NOT NULL,
	[Purchaseprice] varchar(16) NOT NULL,
	[PurchaseUnit]	varchar(16) NOT NULL,
	[PurchaseTax]	varchar(16) NOT NULL, 
	[HsnCode] varchar(50),
	[SGST] decimal(9,2),
	[CGST] decimal(9,2),
	[IGST] decimal(9,2),
	gstTotal  decimal(9,2),
	InstanceId char(36),
	KindName varchar (250),
	StrengthName varchar(500),
	[Type] varchar (250),
	GenericName varchar(250),
	CommodityCode varchar(250),
	[Status] tinyint,
	ProductMasterID char(36),
	Discount decimal(18,2), 
	Eancode varchar(50),
	IsHidden varchar(10) 
	)

	 

 
Declare @EXTRefId char(36)= Newid()
DECLARE @bulk_cmd varchar(1000);  
IF @IsCloud = 1 
	SET @bulk_cmd = 'BULK INSERT #tmpproduct FROM '''+ @FilePath + ''' WITH (MAXERRORS = 0, DATA_SOURCE=''MyAzureInvoicesContainer'', FORMAT=''CSV'', FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  
ELSE
	SET @bulk_cmd = 'BULK INSERT #tmpproduct FROM '''+ @FilePath + ''' WITH (MAXERRORS = 0, FIRSTROW = 2, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')';  

EXEC(@bulk_cmd);  
 
  /*Product Insert*/
INSERT INTO [dbo].[product]
           (Id,Name,AccountId,InstanceId,Code,Manufacturer,Type,Schedule,Category,CreatedAt,UpdatedAt,CreatedBy,UpdatedBy,PackageSize,Price,VAT,HsnCode,Igst,Cgst,Sgst,GstTotal,Ext_RefId,OfflineStatus,KindName,StrengthName, GenericName,CommodityCode,[Status],ProductMasterID,Discount, Eancode,IsHidden)
		   SELECT id,RTRIM(LTRIM(t.ProductName)),  @AccountID, InstanceId, [ProductCode],  RTRIM(LTRIM(t.Manufacturer)),[Type], ScheduleCode,Category, getdate(),getdate(),@Createdby,@Createdby,PurchaseUnit,[Purchaseprice],[PurchaseTax],HsnCode,Igst,cgst,sgst, isnull(gsttotal,0) ,@EXTRefId ,1,KindName,StrengthName, GenericName,CommodityCode,[Status],ProductMasterID,Discount, Eancode,case isnull(IsHidden,'') when 'FALSE' then 0 else 1 end  from #tmpproduct t 
			where  id not in (select id from product (nolock) where Accountid =@accountid)
 
 SELECT 'SUCCESS'
 
		   
END
 