 --Usage : -- usp_GetItemWiseSaleMultiLevelReport '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93',1 ,'01-Jul-2017','23-Jul-2017',null,null,null,null
/** Date        Author          Description                              
*******************************************************************************        
 ** 25/07/2017	Sarubala		Created  
 ** 30/08/2017  Sarubala        GST calculation changes done
*******************************************************************************/ 

 CREATE PROCEDURE [dbo].[usp_GetItemWiseSaleMultiLevelReport](
 @InstanceIds VARCHAR(1000),@AccountId CHAR(36), @Level smallint, @StartDate datetime,@EndDate datetime, 
 @SearchColName VARCHAR(50), @SearchOption VARCHAR(500), @SearchValue VARCHAR(500), @ProdId VARCHAR(100)
)
 AS
 BEGIN
	SET NOCOUNT ON

	DECLARE @billNo VARCHAR(100) = NULL, 
	@PatientId VARCHAR(100) = NULL, 
	@ProductId VARCHAR(100) = NULL, 
	@from_gstTotal NUMERIC(10,2) = NULL,
	@to_gstTotal NUMERIC(10,2) = NULL,
	@from_profitCost NUMERIC(10,2) = NULL,
	@to_profitCost NUMERIC(10,2) = NULL,
	@from_profitMrp NUMERIC(10,2) = NULL,
	@to_profitMrp NUMERIC(10,2) = NULL
	
	IF @SearchColName IS NULL OR @SearchColName = ''
		SELECT @SearchColName = NULL, @SearchOption = NULL, @SearchValue = NULL
	IF @SearchOption = ''
		SELECT @SearchOption = NULL
	IF @SearchValue = '' 
		SELECT @SearchValue = NULL
	IF @ProdId = ''
		SELECT @ProdId = NULL

	DECLARE @InvoiceSeries VARCHAR(500) = @SearchOption
	IF (ISNULL(@SearchColName,'') != 'billNo') 
		SELECT @InvoiceSeries = 'ALL'

	DECLARE @XmlList XML
	SET @XmlList = CAST(('<A>'+REPLACE(ISNULL(@InvoiceSeries,''),',','</A><A>')+'</A>') AS XML)
		
	IF (@SearchColName = 'billNo') 
		BEGIN
			SELECT @billNo = @SearchValue
		END
	Else IF (@SearchColName ='gstTotal') 
		BEGIN 
		  IF (@SearchOption = 'equal') 
		  SELECT @from_gstTotal = Cast (@SearchValue AS NUMERIC(10,2)) , 
				 @to_gstTotal = Cast(@SearchValue AS NUMERIC(10,2)) 
		  ELSE 
		  IF (@SearchOption = 'greater') 
		  SELECT @from_gstTotal = Cast(@SearchValue AS NUMERIC(10,2))+0.01, 
				 @to_gstTotal = NULL 
		  ELSE 
		  IF (@SearchOption = 'less') 
		  SELECT @from_gstTotal = 0.00, 
				 @to_gstTotal = Cast(@SearchValue AS NUMERIC(10,2))-0.01 
		END
	Else IF (@SearchColName ='customerName') 
		Select @PatientId = @SearchValue

	Else IF (@SearchColName ='productName') 
		Select @ProductId = @SearchValue

	
	IF isnull(@ProdId,'') != ''
		Select @ProductId = @ProdId
	
	IF @Level = 1
	BEGIN
		;With temp_instances (InstanceId)
		As 
		(
			select * from udf_Split(@InstanceIds) a
		)

		select 
		--'' InvoiceDate,
		--'' InstanceId, 
		--'' InstanceName, 
		--'' InvoiceNo,
		--'' InvoiceSeries,
		--'' PatientName, 
		--0 SalesDiscount,
		ProductId,
		Name, 
		SUM(Quantity) - SUM (ReturnQuantity) Quantity, 
		--SUM(DiscountAmount) - SUM(ReturnDiscountAmount) DiscountAmount,
		--SUM(InvoiceAmountNoTaxNoDiscount) - SUM(ReturnInvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount,
		--SUM(GrossSaleWithoutTax) - SUM(ReturnGrossSaleWithoutTax) GrossSaleWithoutTax, 
		--SUM(TaxAmount) - SUM(ReturnTaxAmount) TaxAmount, 
		SUM(TotalNetAmount) - SUM(ReturnTotalNetAmount) TotalNetAmount, 
		SUM(TotalPurchasePrice) - SUM(ReturnTotalPurchasePrice) TotalPurchasePrice
		--0 ReturnFlag, 
		--0 MRP
		from (
			select 
			--'' InvoiceDate,
			--'' InstanceId, 
			--'' InstanceName, 
			--'' InvoiceNo,
			--'' InvoiceSeries,
			--'' PatientName, 
			--0 SalesDiscount,
			ProductId,
			Name, 
			SUM(Quantity) Quantity, 
			SUM (ReturnQuantity) ReturnQuantity, 
			--SUM(DiscountAmount) DiscountAmount, 
			--SUM(ReturnDiscountAmount) ReturnDiscountAmount, 
			--SUM(InvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount, 
			--SUM(ReturnInvoiceAmountNoTaxNoDiscount) ReturnInvoiceAmountNoTaxNoDiscount, 
			--SUM(GrossSaleWithoutTax) GrossSaleWithoutTax, 
			--SUM(ReturnGrossSaleWithoutTax) ReturnGrossSaleWithoutTax, 
			--SUM(TaxAmount) TaxAmount, 
			--SUM(ReturnTaxAmount) ReturnTaxAmount, 
			SUM(TotalNetAmount) TotalNetAmount, 
			SUM(ReturnTotalNetAmount) ReturnTotalNetAmount, 
			SUM(TotalPurchasePrice) TotalPurchasePrice, 
			SUM(ReturnTotalPurchasePrice) ReturnTotalPurchasePrice
			--0 ReturnFlag, 
			--0 MRP
			from (
				select 
				--'' InvoiceDate,
				--'' InstanceId, 
				--'' InstanceName,
				--'' InvoiceNo,
				----'' InvoiceSeries,
				--'' PatientName, 
				--0 SalesDiscount,
				p.Id ProductId, 
				p.Name,
				sum(si.Quantity) Quantity,
				0 ReturnQuantity,
				--sum((si.Quantity * ISNULL(si.SellingPrice,isnull(ps.SellingPrice,0)) * case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100)) DiscountAmount,
				--0 ReturnDiscountAmount,
				--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0))) / (1 + (case ISNULL(S.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else ps.VAT end/(100 + (case ISNULL(S.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else ps.VAT end))))) as InvoiceAmountNoTaxNoDiscount,
				--0 ReturnInvoiceAmountNoTaxNoDiscount,
				--sum(((si.Quantity * ISNULL(si.SellingPrice,isnull(ps.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100)))) * (1 - (case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end / (100 + case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end))))  GrossSaleWithoutTax,
				--0 ReturnGrossSaleWithoutTax,
				--sum(((si.Quantity * ISNULL(si.SellingPrice,isnull(ps.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) * case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end ) / (100 + case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end)) TaxAmount,
				--0 ReturnTaxAmount,
				--sum((isnull(si.Quantity,0) * ISNULL(si.SellingPrice,ISNULL(ps.SellingPrice,0))) * (1 - ((case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end)/100))) TotalNetAmount,
				sum(SI.TotalAmount) AS TotalNetAmount,
				0 ReturnTotalNetAmount,
				--SUM((SI.Quantity) * (Case When IsNull(PS.Vat, 0) > 0 Then ISNULL(PS.PurchasePrice,0)/(1+(ISNULL( PS.VAT, 0)/100))*(1+(ISNULL(PS.GstTotal, 0)/100)) Else ISNULL(PS.PurchasePrice,0) End)) TotalPurchasePrice,
				SUM((SI.Quantity) * ISNULL(PS.PurchasePrice,0)) AS TotalPurchasePrice,
				0 ReturnTotalPurchasePrice
				--0 ReturnFlag
				from Sales S WITH(NOLOCK) 
				join SalesItem si WITH(NOLOCK) on s.Id = si.SalesId
				join Instance i WITH(NOLOCK) on i.Id=s.InstanceId
				join ProductStock ps WITH(NOLOCK) on si.ProductStockId=ps.Id
				INNER JOIN temp_instances TempIns On S.InstanceId = TempIns.InstanceId
				join Product p WITH(NOLOCK) on p.Id=ps.ProductId
				left join Patient PA WITH(NOLOCK) on PA.Id = S.PatientId
				WHERE S.AccountId = @AccountId 
				AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
				and (S.Cancelstatus is NULL or s.Cancelstatus != 1)
				AND 
				(
					ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
					ISNULL(PA.Name, '') Like ISNULL(@PatientId, ISNULL(PA.Name, '')) + '%'
				)
				AND 
				(
					ISNULL(p.Id,'') = ISNULL(@ProductId,ISNULL(ps.ProductId,'')) OR
					ISNULL(P.Name, '') Like ISNULL(@ProductId, ISNULL(P.Name, '')) + '%'
				)
				AND ISNULL(S.InvoiceSeries,'') = 
				(
					CASE 
					WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
					END
				)
				AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 			
				AND SI.GstTotal BETWEEN Isnull(@from_gstTotal, SI.GstTotal) AND Isnull(@to_gstTotal, SI.GstTotal)			
				GROUP BY p.Name,p.Id
			) A group by Name,ProductId

			UNION ALL
		
			select 
			--'' InvoiceDate,
			--'' InstanceId, 
			--'' InstanceName, 
			--'' InvoiceNo,
			--'' InvoiceSeries,
			--'' PatientName, 
			--0 SalesDiscount,
			ProductId,
			Name, 
			SUM(Quantity) Quantity, 
			SUM(ReturnQuantity) ReturnQuantity, 
			--SUM(DiscountAmount) DiscountAmount, 
			--SUM(ReturnDiscountAmount) ReturnDiscountAmount, 
			--SUM(InvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount, 
			--SUM(ReturnInvoiceAmountNoTaxNoDiscount) ReturnInvoiceAmountNoTaxNoDiscount, 
			--SUM(GrossSaleWithoutTax) GrossSaleWithoutTax, 
			--SUM(ReturnGrossSaleWithoutTax) ReturnGrossSaleWithoutTax, 
			--SUM(TaxAmount) TaxAmount, 
			--SUM(ReturnTaxAmount) ReturnTaxAmount, 
			SUM(TotalNetAmount) TotalNetAmount, 
			SUM(ReturnTotalNetAmount) ReturnTotalNetAmount, 
			SUM(TotalPurchasePrice) TotalPurchasePrice, 
			SUM(ReturnTotalPurchasePrice) ReturnTotalPurchasePrice
			--0 ReturnFlag, 
			--0 MRP
			from (
				SELECT 
				--'' InvoiceDate,
				--'' InstanceId, 
				--'' InstanceName, 
				--'' InvoiceNo,
				--'' InvoiceSeries,
				--'' PatientName, 
				--0 SalesDiscount,
				p.Id ProductId,
				p.Name,
				0 Quantity,
				sum(SRI.Quantity) ReturnQuantity,
				--0 DiscountAmount,
				--sum((SRI.Quantity * ISNULL(SRI.MrpSellingPrice,isnull(ps.SellingPrice,0)) * (ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) / 100)) ReturnDiscountAmount,
				--0 InvoiceAmountNoTaxNoDiscount,
				--sum((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0))) / (1 + (case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else PS.VAT end/(100 + (case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else ps.VAT end))))) as ReturnInvoiceAmountNoTaxNoDiscount,
				--0 GrossSaleWithoutTax,
				--sum(((SRI.Quantity * ISNULL(SRI.MrpSellingPrice,isnull(ps.SellingPrice,0)) * (1 - ((ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) / 100)))) * (1 - (case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else PS.VAT end / (100 + case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else PS.VAT end))))  ReturnGrossSaleWithoutTax,
				--0 TaxAmount,
				--sum(((SRI.Quantity * ISNULL(SRI.MrpSellingPrice,isnull(ps.SellingPrice,0)) * (1 - ((ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) / 100))) * case ISNULL(sr.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else ps.VAT end ) / (100 + case ISNULL(sr.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SRI.GstTotal,0)) else ps.VAT end)) ReturnTaxAmount,
				0 TotalNetAmount,
				--sum((isnull(sri.Quantity,0) * ISNULL(sri.MrpSellingPrice,ISNULL(ps.SellingPrice,0))) * (1 - ((ISNULL(s.Discount,0)+ISNULL(sri.Discount,0))/100))) ReturnTotalNetAmount,
				sum(SRI.TotalAmount) ReturnTotalNetAmount,
				0 TotalPurchasePrice,
				--SUM((SRI.Quantity) * (Case When IsNull(PS.Vat, 0) > 0 Then ISNULL(PS.PurchasePrice,0)/(1+(ISNULL(PS.VAT, 0)/100))*(1+(ISNULL(PS.GstTotal, 0)/100)) Else ISNULL(PS.PurchasePrice,0) End)) ReturnTotalPurchasePrice,
				SUM((SRI.Quantity) * ISNULL(PS.PurchasePrice,0)) ReturnTotalPurchasePrice
				--1 ReturnFlag
				FROM SalesReturn SR WITH(NOLOCK)
				INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
				INNER JOIN SalesReturnItem SRI ON SRI.SalesReturnId = SR.Id
				INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId	
				INNER JOIN temp_instances TempIns On SR.InstanceId = TempIns.InstanceId	
				LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
				JOIN Product p on p.Id=ps.ProductId
				left join Patient PA on PA.Id = S.PatientId
				WHERE SR.AccountId = @AccountId 
					AND Convert(date,SR.returndate) BETWEEN @StartDate AND @EndDate
					and S.Cancelstatus is NULL
					and ISNULL(sri.IsDeleted,0) != 1
					and ISNULL(sr.CancelType,0) != 2	
					AND 
					(
						ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
						ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
						or ISNULL(SR.PatientId,'') = ISNULL(@PatientId,'')
					)
					AND 
					(
						ISNULL(p.Id,'') = ISNULL(@ProductId,ISNULL(ps.ProductId,'')) OR
						ISNULL(P.Name, '') Like ISNULL(@ProductId, ISNULL(P.Name, '')) + '%'
					)
					AND ISNULL(S.InvoiceSeries,'') = 
					(
						CASE 
							WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
							WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
						END
					)
					AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
					AND sri.GstTotal BETWEEN Isnull(@from_gstTotal, sri.GstTotal) AND Isnull(@to_gstTotal, sri.GstTotal)
				GROUP BY p.Name,p.Id 
			) A Group by Name, ProductId
		)A
		GROUP BY Name,ProductId
		ORDER BY Name
	End
	Else If @Level = 2
	Begin
		;With temp_instances (InstanceId)
		As 
		(
			select * from udf_Split(@InstanceIds) a
		)

		select 
		--'' InvoiceDate,
		InstanceId, 
		InstanceName, 
		--'' InvoiceNo,
		--'' InvoiceSeries,
		--'' PatientName,
		--0 SalesDiscount,
		ProductId,
		Name, 
		SUM(Quantity) - SUM (ReturnQuantity) Quantity, 
		--SUM(DiscountAmount) - SUM(ReturnDiscountAmount) DiscountAmount,
		--SUM(InvoiceAmountNoTaxNoDiscount) - SUM(ReturnInvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount,
		--SUM(GrossSaleWithoutTax) - SUM (ReturnGrossSaleWithoutTax) GrossSaleWithoutTax, 
		--SUM(TaxAmount) - SUM (ReturnTaxAmount) TaxAmount, 
		SUM(TotalNetAmount) - SUM (ReturnTotalNetAmount) TotalNetAmount, 
		SUM(TotalPurchasePrice) - SUM (ReturnTotalPurchasePrice) TotalPurchasePrice
		--0 ReturnFlag , 
		--0 MRP
		from (
			select 
			--'' InvoiceDate,
			InstanceId, 
			InstanceName, 
			--'' InvoiceNo,
			--'' InvoiceSeries,
			--'' PatientName, 
			--0 SalesDiscount,
			ProductId,
			Name, 
			SUM(Quantity) Quantity, 
			SUM(ReturnQuantity) ReturnQuantity, 
			--SUM(DiscountAmount) DiscountAmount, 
			--SUM(ReturnDiscountAmount) ReturnDiscountAmount, 
			--SUM(InvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount, 
			--SUM(ReturnInvoiceAmountNoTaxNoDiscount) ReturnInvoiceAmountNoTaxNoDiscount, 
			--SUM(GrossSaleWithoutTax) GrossSaleWithoutTax, 
			--SUM(ReturnGrossSaleWithoutTax) ReturnGrossSaleWithoutTax, 
			--SUM(TaxAmount) TaxAmount, 
			--SUM(ReturnTaxAmount) ReturnTaxAmount, 
			SUM(TotalNetAmount) TotalNetAmount, 
			SUM(ReturnTotalNetAmount) ReturnTotalNetAmount, 
			SUM(TotalPurchasePrice) TotalPurchasePrice, 
			SUM(ReturnTotalPurchasePrice) ReturnTotalPurchasePrice
			--0 ReturnFlag, 
			--0 MRP
			from (
				select 
				--'' InvoiceDate, 
				I.Id InstanceId, 
				case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName,
				--'' InvoiceNo,
				--'' InvoiceSeries,
				--'' PatientName, 
				--0 SalesDiscount,
				p.Id ProductId,
				p.Name,
				sum(si.Quantity) Quantity,
				0 ReturnQuantity,
				--sum((si.Quantity * ISNULL(si.SellingPrice,isnull(ps.SellingPrice,0)) * case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100)) DiscountAmount,
				--0 ReturnDiscountAmount,
				--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0))) / (1 + (case ISNULL(S.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else ps.VAT end/(100 + (case ISNULL(S.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else ps.VAT end))))) as InvoiceAmountNoTaxNoDiscount,
				--0 ReturnInvoiceAmountNoTaxNoDiscount,
				--sum(((si.Quantity * ISNULL(si.SellingPrice,isnull(ps.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100)))) * (1 - (case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end / (100 + case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end))))  GrossSaleWithoutTax,
				--0 ReturnGrossSaleWithoutTax,
				--sum(((si.Quantity * ISNULL(si.SellingPrice,isnull(ps.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) * case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end ) / (100 + case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end)) TaxAmount,
				--0 ReturnTaxAmount,
				--sum((isnull(si.Quantity,0) * ISNULL(si.SellingPrice,ISNULL(ps.SellingPrice,0))) * (1 - ((case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end)/100))) TotalNetAmount,
				sum(SI.TotalAmount) TotalNetAmount,
				0 ReturnTotalNetAmount,
				--SUM((SI.Quantity) * (Case When IsNull(PS.Vat, 0) > 0 Then ISNULL(PS.PurchasePrice,0)/(1+(ISNULL( PS.VAT, 0)/100))*(1+(ISNULL(PS.GstTotal, 0)/100)) Else ISNULL(PS.PurchasePrice,0) End)) TotalPurchasePrice
				SUM((SI.Quantity) * ISNULL(PS.PurchasePrice,0)) TotalPurchasePrice,
				0 ReturnTotalPurchasePrice
				--0 ReturnFlag
				from Sales S WITH(NOLOCK)
				join SalesItem si WITH(NOLOCK) on s.Id = si.SalesId
				join Instance i WITH(NOLOCK) on i.Id=s.InstanceId
				join ProductStock ps WITH(NOLOCK) on si.ProductStockId=ps.Id
				INNER JOIN temp_instances TempIns On S.InstanceId = TempIns.InstanceId
				join Product p WITH(NOLOCK) on p.Id=ps.ProductId
				left join Patient PA WITH(NOLOCK) on PA.Id = S.PatientId
				WHERE S.AccountId = @AccountId 		
				AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
				and (S.Cancelstatus is NULL or s.Cancelstatus != 1)	
				AND 
				(
					ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
					ISNULL(PA.Name, '') Like ISNULL(@PatientId, ISNULL(PA.Name, '')) + '%'
				)
				AND 
				(
					ISNULL(p.Id,'') = ISNULL(@ProductId,ISNULL(ps.ProductId,'')) OR
					ISNULL(P.Name, '') Like ISNULL(@ProductId, ISNULL(P.Name, '')) + '%'
				)
				AND ISNULL(S.InvoiceSeries,'') = 
				(
					CASE 
					WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
					END
				)
				AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
				AND SI.GstTotal BETWEEN Isnull(@from_gstTotal, SI.GstTotal) AND Isnull(@to_gstTotal, SI.GstTotal)
				GROUP BY I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end,p.Name,p.Id
			) A group by InstanceId,InstanceName,Name,ProductId

			UNION ALL
		
			select 
			--'' InvoiceDate,
			InstanceId, 
			InstanceName, 
			--'' InvoiceNo,
			--'' InvoiceSeries,
			--'' PatientName, 
			--0 SalesDiscount,
			ProductId,
			Name, 
			SUM(Quantity) Quantity, 
			SUM(ReturnQuantity) ReturnQuantity, 
			--SUM(DiscountAmount) DiscountAmount, 
			--SUM(ReturnDiscountAmount) ReturnDiscountAmount, 
			--SUM(InvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount, 
			--SUM(ReturnInvoiceAmountNoTaxNoDiscount) ReturnInvoiceAmountNoTaxNoDiscount, 
			--SUM(GrossSaleWithoutTax) GrossSaleWithoutTax, 
			--SUM(ReturnGrossSaleWithoutTax) ReturnGrossSaleWithoutTax, 
			--SUM(TaxAmount) TaxAmount, 
			--SUM(ReturnTaxAmount) ReturnTaxAmount, 
			SUM(TotalNetAmount) TotalNetAmount, 
			SUM(ReturnTotalNetAmount) ReturnTotalNetAmount, 
			SUM(TotalPurchasePrice) TotalPurchasePrice, 
			SUM(ReturnTotalPurchasePrice) ReturnTotalPurchasePrice
			--0 ReturnFlag, 
			--0 MRP
			from (
				SELECT 
				--'' InvoiceDate, 
				I.Id InstanceId, 
				case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName,
				--'' InvoiceNo,
				--'' InvoiceSeries,
				--'' PatientName,
				--0 SalesDiscount,
				p.Id ProductId,
				p.Name,
				0 Quantity,
				sum(SRI.Quantity) ReturnQuantity,
				--0 DiscountAmount,
				--sum((SRI.Quantity * ISNULL(SRI.MrpSellingPrice,isnull(ps.SellingPrice,0)) * (ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) / 100)) ReturnDiscountAmount,
				--0 InvoiceAmountNoTaxNoDiscount,
				--sum((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0))) / (1 + (case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else PS.VAT end/(100 + (case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else ps.VAT end))))) as ReturnInvoiceAmountNoTaxNoDiscount,
				--0 GrossSaleWithoutTax,
				--sum(((SRI.Quantity * ISNULL(SRI.MrpSellingPrice,isnull(ps.SellingPrice,0)) * (1 - ((ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) / 100)))) * (1 - (case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end	else PS.VAT end / (100 + case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else PS.VAT end))))  ReturnGrossSaleWithoutTax,
				--0 TaxAmount,
				--sum(((SRI.Quantity * ISNULL(SRI.MrpSellingPrice,isnull(ps.SellingPrice,0)) * (1 - ((ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) / 100))) * case ISNULL(sr.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else ps.VAT end ) / (100 + case ISNULL(sr.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else ps.VAT end)) ReturnTaxAmount,
				0 TotalNetAmount,
				--sum((isnull(sri.Quantity,0) * ISNULL(sri.MrpSellingPrice,ISNULL(ps.SellingPrice,0))) * (1 - ((ISNULL(s.Discount,0)+ISNULL(sri.Discount,0))/100))) ReturnTotalNetAmount,
				sum(SRI.TotalAmount) AS ReturnTotalNetAmount,
				0 TotalPurchasePrice,
				--SUM((SRI.Quantity) * (Case When IsNull(PS.Vat, 0) > 0 Then ISNULL(PS.PurchasePrice,0)/(1+(ISNULL(PS.VAT, 0)/100))*(1+(ISNULL(PS.GstTotal, 0)/100)) Else ISNULL(PS.PurchasePrice,0) End)) ReturnTotalPurchasePrice
				SUM((SRI.Quantity) * ISNULL(PS.PurchasePrice,0)) ReturnTotalPurchasePrice
				--1 ReturnReturnFlag
				FROM SalesReturn SR WITH(NOLOCK)
				INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
				INNER JOIN SalesReturnItem SRI ON SRI.SalesReturnId = SR.Id
				INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId
				INNER JOIN temp_instances TempIns On SR.InstanceId = TempIns.InstanceId			
				LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
				JOIN Product p WITH(NOLOCK) on p.Id=ps.ProductId
				left join Patient PA WITH(NOLOCK) on PA.Id = S.PatientId
				WHERE SR.AccountId = @AccountId 
				AND Convert(date,SR.returndate) BETWEEN @StartDate AND @EndDate
				and S.Cancelstatus is NULL
				and ISNULL(sri.IsDeleted,0) != 1
				and ISNULL(sr.CancelType,0) != 2
				AND 
				(
					ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
					ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
					or ISNULL(SR.PatientId,'') = ISNULL(@PatientId,ISNULL(SR.PatientId,''))
				)
				AND 
				(
					ISNULL(p.Id,'') = ISNULL(@ProductId,ISNULL(ps.ProductId,'')) OR
					ISNULL(P.Name, '') Like ISNULL(@ProductId, ISNULL(P.Name, '')) + '%'
				)
				AND ISNULL(S.InvoiceSeries,'') = 
				(
					CASE 
					WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
					END
				)
				AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
				AND sri.GstTotal BETWEEN Isnull(@from_gstTotal, sri.GstTotal) AND Isnull(@to_gstTotal, sri.GstTotal)
				GROUP BY I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end,p.Name, p.Id
			) A group by InstanceId,InstanceName,Name,ProductId
		) A
		GROUP BY InstanceId, InstanceName, Name, ProductId
		ORDER BY Name, InstanceName ASC		
	End
	Else If @Level = 3
	Begin
		SELECT 
		S.InvoiceDate, 
		I.Id InstanceId, 
		case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName,
		S.InvoiceNo, 
		LTRIM(ISNULL(s.prefix,'') + ISNULL(s.InvoiceSeries, '')) as InvoiceSeries, 
		PA.Name PatientName, 
		case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end SalesDiscount,
		p.Id ProductId,
		p.Name,
		sum(si.Quantity) Quantity,
		--sum((si.Quantity * ISNULL(si.SellingPrice,isnull(ps.SellingPrice,0)) * case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100)) DiscountAmount,
		--SUM((SI.Quantity) * (isNull(SI.SellingPrice, ISNULL(PS.SellingPrice,0))) / (1 + (case ISNULL(S.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else ps.VAT end/(100 + (case ISNULL(S.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else ps.VAT end))))) as InvoiceAmountNoTaxNoDiscount,
		--sum(((si.Quantity * ISNULL(si.SellingPrice,isnull(ps.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100)))) * (1 - (case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end / (100 + case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end))))  GrossSaleWithoutTax,
		SUM(SI.TotalAmount-SI.GstAmount) AS GrossSaleWithoutTax,
		--sum(((si.Quantity * ISNULL(si.SellingPrice,isnull(ps.SellingPrice,0)) * (1 - (case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end / 100))) * case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end ) / (100 + case ISNULL(s.TaxRefNo,0) when 1 then ISNULL(ps.GstTotal,isnull(SI.GstTotal,0)) else si.VAT end)) TaxAmount,
		SUM(SI.GstAmount) AS TaxAmount,
		--sum((isnull(si.Quantity,0) * ISNULL(si.SellingPrice,ISNULL(ps.SellingPrice,0))) * (1 - ((case when ISNULL(s.Discount,0) > 0 then s.Discount else ISNULL(si.Discount,0) end)/100))) TotalNetAmount,
		SUM(SI.TotalAmount) AS TotalNetAmount,
		--SUM((SI.Quantity) * (Case When IsNull(PS.Vat, 0) > 0 Then ISNULL(PS.PurchasePrice,0)/(1+(ISNULL( PS.VAT, 0)/100))*(1+(ISNULL(PS.GstTotal, 0)/100)) Else ISNULL(PS.PurchasePrice,0) End)) TotalPurchasePrice,
		SUM((SI.Quantity) * ISNULL(PS.PurchasePrice,0)) AS TotalPurchasePrice,
		0 ReturnFlag,
		max(si.SellingPrice) AS MRP
		from Sales S WITH(NOLOCK)
		join SalesItem si WITH(NOLOCK) on s.Id = si.SalesId
		join Instance i WITH(NOLOCK) on i.Id=s.InstanceId
		join ProductStock ps WITH(NOLOCK) on si.ProductStockId=ps.Id
		join Product p WITH(NOLOCK) on p.Id=ps.ProductId
		left join Patient PA WITH(NOLOCK) on PA.Id = S.PatientId
		WHERE S.AccountId = @AccountId 
		and S.InstanceId = @InstanceIds
		AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
		and (S.Cancelstatus is NULL or s.Cancelstatus != 1)
		AND 
		(
			ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
			ISNULL(PA.Name, '') Like ISNULL(@PatientId, ISNULL(PA.Name, '')) + '%'
		)
		AND 
		(
			ISNULL(p.Id,'') = ISNULL(@ProductId,ISNULL(ps.ProductId,'')) OR
			ISNULL(P.Name, '') Like ISNULL(@ProductId, ISNULL(P.Name, '')) + '%'
		)
		AND ISNULL(S.InvoiceSeries,'') = 
		(
			CASE 
			WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
			WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
			END
		)
		AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
		AND SI.GstTotal BETWEEN Isnull(@from_gstTotal, SI.GstTotal) AND Isnull(@to_gstTotal, SI.GstTotal)
		GROUP BY S.InvoiceDate, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end , S.InvoiceNo, LTRIM(ISNULL(s.prefix,'') + ISNULL(s.InvoiceSeries, '')), 
		P.Name,p.Id, ISNULL(S.Discount, 0), PA.Name, ISNULL(SI.Discount,0),S.Discount
		
		UNION ALL
		
		SELECT 
		SR.ReturnDate, 
		I.Id InstanceId, 
		case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, 
		SR.ReturnNo, 
		LTRIM(ISNULL(SR.prefix,'') + ISNULL(SR.InvoiceSeries, '')) as InvoiceSeries, 
		isNull(P2.Name, PA.Name) PatientName,
		(ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) SalesDiscount, 
		p.Id ProductId,
		p.Name,
		sum(SRI.Quantity) Quantity,
		--sum((SRI.Quantity * ISNULL(SRI.MrpSellingPrice,isnull(ps.SellingPrice,0)) * (ISNULL(s.Discount,0)+ISNULL(sri.Discount,0))/ 100)) DiscountAmount,
		--sum((SRI.Quantity) * (isNull(SRI.MrpSellingPrice, ISNULL(PS.SellingPrice,0))) / (1 + (case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else PS.VAT end/(100 + (case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end	else ps.VAT end))))) as InvoiceAmountNoTaxNoDiscount,
		--sum(((SRI.Quantity * ISNULL(SRI.MrpSellingPrice,isnull(ps.SellingPrice,0)) * (1 - (ISNULL(s.Discount,0)+ISNULL(sri.Discount,0)) / 100))) * (1 - (case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else PS.VAT end / (100 + case ISNULL(SR.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else PS.VAT end))))  GrossSaleWithoutTax,
		SUM(SRI.TotalAmount-SRI.GstAmount) AS GrossSaleWithoutTax,
		--sum(((SRI.Quantity * ISNULL(SRI.MrpSellingPrice,isnull(ps.SellingPrice,0)) * (1 - (ISNULL(s.Discount,0)+ISNULL(sri.Discount,0) ) / 100)) * case ISNULL(sr.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else ps.VAT end ) / (100 + case ISNULL(sr.TaxRefNo,0) when 1 then case when isNull(ps.GstTotal,0) !=0 then ps.GstTotal else isNull(sri.GstTotal, 0) end else ps.VAT end)) TaxAmount,
		SUM(SRI.GstAmount) AS TaxAmount,
		--sum((isnull(sri.Quantity,0) * ISNULL(sri.MrpSellingPrice,ISNULL(ps.SellingPrice,0))) * (1 - ((ISNULL(s.Discount,0)+ISNULL(sri.Discount,0))/100))) TotalNetAmount,
		SUM(SRI.TotalAmount) AS TotalNetAmount,
		--SUM((SRI.Quantity) * (Case When IsNull(PS.Vat, 0) > 0 Then ISNULL(PS.PurchasePrice,0)/(1+(ISNULL(PS.VAT, 0)/100))*(1+(ISNULL(PS.GstTotal, 0)/100)) Else ISNULL(PS.PurchasePrice,0) End)) ReturnTotalPurchasePrice,
		SUM((SRI.Quantity) * ISNULL(PS.PurchasePrice,0)) AS ReturnTotalPurchasePrice,
		1 ReturnFlag,
		max(sri.MrpSellingPrice) AS MRP
		FROM SalesReturn SR WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
		INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId		
		LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
		JOIN Product p WITH(NOLOCK) on p.Id=ps.ProductId
		left join Patient PA WITH(NOLOCK) on PA.Id = S.PatientId
		LEFT JOIN Patient P2 WITH(NOLOCK) ON P2.Id = SR.PatientId
		WHERE SR.AccountId = @AccountId and SR.InstanceId = @InstanceIds
		AND Convert(date,SR.returndate) BETWEEN @StartDate AND @EndDate
		AND S.Cancelstatus is NULL
		and ISNULL(sri.IsDeleted,0) != 1
		and ISNULL(sr.CancelType,0) != 2
		AND 
		(
			ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
			ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
			or ISNULL(SR.PatientId,'') = ISNULL(@PatientId,ISNULL(SR.PatientId,''))
		)
		AND 
		(
			ISNULL(p.Id,'') = ISNULL(@ProductId,ISNULL(ps.ProductId,'')) OR
			ISNULL(P.Name, '') Like ISNULL(@ProductId, ISNULL(P.Name, '')) + '%'
		)
		AND ISNULL(S.InvoiceSeries,'') = 
		(
			CASE 
			WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
			WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
			END
		)
		AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
		AND sri.GstTotal BETWEEN Isnull(@from_gstTotal, sri.GstTotal) AND Isnull(@to_gstTotal, sri.GstTotal)
		GROUP BY SR.ReturnDate, I.Id,case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end , SR.ReturnNo, LTRIM(ISNULL(SR.prefix,'') + ISNULL(SR.InvoiceSeries, '')), 
		P.Name,p.Id, (ISNULL(s.Discount,0)+ ISNULL(sri.Discount,0) ),isNull(P2.Name, PA.Name)
		ORDER BY InvoiceSeries desc, InvoiceNo desc
	End
 END