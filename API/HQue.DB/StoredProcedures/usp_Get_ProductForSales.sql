
/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**09/05/17     Poongodi R	SP Created to get the products for sales
**02/08/17     Poongodi R	Pudukottai - Product stock zero qty taken
**14/08/17     Poongodi R	Zero Productstock Enabled for Suguna medicals
**31/08/17     Poongodi R	Top 100 added for mediplus
**12/09/17     Poongodi R	Inventory Zero stock included in UNITED MEDICALS
**04/12/17     Poongodi R	Trim  added in product section
**16/12/17     nandhini n	negative stock removed for mediplus
*******************************************************************************/
create Proc dbo.usp_Get_ProductForSales(@InstanceId char(36), @ProductName varchar(100), @Accountid char(36))
as
begin
declare @IncludeZeroStock bit =0
set @ProductName = ltrim( rtrim(@ProductName))
	IF (@Accountid  in( '67067991-0e68-4779-825e-8e1d724cd68b','ae13cb7f-991b-42d6-8dec-8de153e4526a','63ED272D-1122-4E04-AFE8-C5A0F13B4F0E','af51d266-1981-45ed-9c14-f9882e6e8656',
		'caf5e8be-a169-460f-a672-ccdad4f33cc0','862c8efa-7ad2-4f9c-82ec-b2842e4b5643','976bfbd1-fe57-45d5-a99f-4e01eed868ae','4d07d298-d8cf-425b-8983-a08a6fe4c397', '46e9dc5e-e543-430b-9a51-b4d02e22657a','d189fa3c-e0f2-4ffb-83ad-a4f7515c07e0','1c2dae41-ef29-49fe-8813-ea7d41be4f1a',
		'852eaefa-f670-4a6a-a0e7-db3fa252d576','444f0122-6321-48fb-bc4d-123addde4b8e','c2b3ac8a-8547-438c-8496-8c7ee675283f','2a552458-a1dd-4d10-9fa3-6724b4fbec90'))
		 set @IncludeZeroStock=1
   IF (@IncludeZeroStock=1 and @instanceid  in('5d9a5071-3db9-44e0-9921-c226f34c3616','552bcb81-4996-4dcd-ac73-e709761372a2','686c3b68-73f2-4582-9f2e-5527ab3ce070',
   '1f1a7cbe-65d5-4416-ada4-70c17ef704b5','17e75a1a-0e19-4042-9449-f266a84862f6','39c28212-7272-4c21-9e4e-b2319e77ba9c','686ae6f4-beba-4418-a68f-635d9bdd2d0b',
   '860e4874-953f-4d08-9eea-8bd091e4b473','b31de0c8-8cfb-45c4-a26f-80674e0e6824','ce9e8c12-7ce5-4058-a458-c68bbb4e7e00','dac499a1-3357-481b-b382-2e6c4c58921c',
   '17e75a1a-0e19-4042-9449-f266a84862f6','49e653fd-dc4f-4a48-801d-ce1fa6ca8ba0'))
	BEGIN
		SELECT  * FROM (
		SELECT  TOP 10  1 AS GROUP_NO, SUM(isnull(ProductStock.Stock,0)) AS Stock , (Product.Id) AS [ProductId], Product.Name [ProductName],
		 isnull(Product.Code,'')  AS [ProductCode],
		max(Product.Schedule) AS [ProductSchedule]
		FROM Product (nolock)     Inner JOIN ( SELECT * FROM ProductStock(nolock) WHERE    cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date)
		AND isnull(ProductStock.Status, 1) = (1)  AND ProductStock.InstanceId  =  @InstanceId ) ProductStock ON Product.Id = ProductStock.ProductId  
		 AND (isNull(Product.Status, 1) =  1)
		WHERE  Product.Accountid =@Accountid
		
		and  ((Product.Id  =  @ProductName) OR (ltrim(rtrim(Product.Name ))  Like  @ProductName+ '%'))
		and ProductStock.Stock >= 0
	 	
		 GROUP BY  Product.Name,Product.Id,Product.Code  
		 
		 ) A ORDER BY GROUP_NO, ProductName,Stock desc,ProductCode
	 END
	else IF (@IncludeZeroStock=1 and @instanceid Not in ('5d9a5071-3db9-44e0-9921-c226f34c3616','8eb43784-374b-4832-90c3-4a4014979248','a628d299-7415-484c-b758-c5d7e645c231'))
	BEGIN
		SELECT  * FROM (
		SELECT   Top 100 1 AS GROUP_NO, SUM(isnull(ProductStock.Stock,0)) AS Stock , (Product.Id) AS [ProductId], Product.Name [ProductName],
		 isnull(Product.Code,'')  AS [ProductCode],
		max(Product.Schedule) AS [ProductSchedule]
		FROM Product (nolock)     Left JOIN ( SELECT * FROM ProductStock(nolock) WHERE    cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date)
		AND isnull(ProductStock.Status, 1) = (1)  AND ProductStock.InstanceId  =  @InstanceId ) ProductStock ON Product.Id = ProductStock.ProductId  
		 AND (isNull(Product.Status, 1) =  1)
		WHERE  Product.Accountid =@Accountid
		
		and  ((Product.Id  =  @ProductName) OR (replace(Product.Name  ,' ','')Like  replace(@ProductName,' ' ,'')+ '%'))
	 	and ProductStock.Stock >= 0
		 GROUP BY  Product.Name,Product.Id,Product.Code  
		 
		 ) A ORDER BY GROUP_NO,  ProductName,Stock desc,ProductCode
	 END
	
	 else IF (@IncludeZeroStock=1 and @instanceid  in('8eb43784-374b-4832-90c3-4a4014979248'))
	BEGIN
		SELECT  * FROM (
		SELECT  1 AS GROUP_NO, SUM(isnull(ProductStock.Stock,0)) AS Stock , (Product.Id) AS [ProductId], Product.Name [ProductName],
		 isnull(Product.Code,'')  AS [ProductCode],
		max(Product.Schedule) AS [ProductSchedule]
		FROM Product (nolock)     Inner JOIN ( SELECT * FROM ProductStock(nolock) WHERE    cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date)
		AND isnull(ProductStock.Status, 1) = (1)  AND ProductStock.InstanceId  =  @InstanceId ) ProductStock ON Product.Id = ProductStock.ProductId  
		 AND (isNull(Product.Status, 1) =  1)
		WHERE  Product.Accountid =@Accountid
		
		and  ((Product.Id  =  @ProductName) OR (ltrim(rtrim(Product.Name ))  Like  @ProductName+ '%'))
	 	and ProductStock.Stock >= 0
		 GROUP BY  Product.Name,Product.Id,Product.Code  
		 
		 ) A ORDER BY GROUP_NO, ProductName,Stock desc,ProductCode
	 END
	  else IF (@IncludeZeroStock=1 and @instanceid  in('a628d299-7415-484c-b758-c5d7e645c231','a839411a-344c-467b-85f9-9eb570b7b6ce','0a4079e8-aa89-449c-8641-574336a73813','8320c592-70ac-4746-babd-9d52246916ef'))
	BEGIN
		SELECT  * FROM (
		SELECT  TOP 10  1 AS GROUP_NO, SUM(isnull(ProductStock.Stock,0)) AS Stock , (Product.Id) AS [ProductId], Product.Name [ProductName],
		 ''  AS [ProductCode],
		max(Product.Schedule) AS [ProductSchedule]
		FROM Product (nolock)     Inner JOIN ( SELECT * FROM ProductStock(nolock) WHERE    cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date)
		AND isnull(ProductStock.Status, 1) = (1)  AND ProductStock.InstanceId  =  @InstanceId ) ProductStock ON Product.Id = ProductStock.ProductId  
		 AND (isNull(Product.Status, 1) =  1)
		WHERE  Product.Accountid =@Accountid
		
		and  ((Product.Id  =  @ProductName) OR (ltrim(rtrim(Product.Name ))  Like  @ProductName+ '%'))
	 	and ProductStock.Stock >= 0
		 GROUP BY  Product.Name,Product.Id
		 
		 ) A ORDER BY GROUP_NO, ProductName,Stock desc,ProductCode
	 END
	 ELSE
	 BEGIN
		SELECT TOP 10 SUM(ProductStock.Stock) AS Stock , (Product.Id) AS [ProductId],Product.Name AS [ProductName],
		max(Product.Schedule) AS [ProductSchedule]
		FROM ProductStock(nolock) INNER JOIN Product (nolock)  ON Product.Id = ProductStock.ProductId
		WHERE ProductStock.Stock > 0 AND 
		cast(ProductStock.ExpireDate  as date) >  cast(getdate() as date)
		AND isnull(ProductStock.Status, 1) = (1)
		AND ((Product.Id  =  @ProductName) OR (ltrim(rtrim(Product.Name )) Like  @ProductName+ '%'))
		 AND (isNull(Product.Status, 1) =  1)
		 AND ProductStock.InstanceId  =  @InstanceId
		 GROUP BY  Product.Name ,Product.Id  
		 ORDER BY PRODUCTNAME
	 END
 END
