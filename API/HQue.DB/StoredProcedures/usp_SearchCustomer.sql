﻿/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 **09/10/2017	Sarubala V		Multiple payment cheque included
*******************************************************************************/

CREATE PROCEDURE [dbo].[usp_SearchCustomer](@accountid char(36),@instanceid char(36),@PaymentType varchar(50),@name varchar(100),@mobile varchar(15)) AS BEGIN


select distinct  * from(

SELECT distinct top 10  s.Name,s.Mobile,s.PatientId
FROM sales as s
join Patient p on p.Id = s.PatientId
WHERE s.InstanceId=@instanceid
  AND s.AccountId=@accountid
  AND s.PaymentType =@PaymentType
  AND s.Name LIKE '%'+ISNULL(@name,'')+'%'
  AND s.Mobile LIKE '%'+ISNULL(@mobile,'')+'%'
  AND (isnull(s.BankDeposited,0) != 1
       OR isnull(s.AmountCredited,0) != 1)
  AND ISNULL(p.Status,0) <> 2 




UNION

 
SELECT distinct top 10   s.Name,s.Mobile,s.PatientId
FROM sales AS s
join Patient p on p.Id = s.PatientId
INNER JOIN CustomerPayment AS cp ON s.Id = cp.SalesId
WHERE cp.InstanceId =@instanceid
  AND cp.AccountId = @accountid
  AND cp.PaymentType = @PaymentType
  AND s.Name LIKE '%'+ISNULL(@name,'')+'%'
  AND s.Mobile LIKE '%'+ISNULL(@mobile,'')+'%'
  AND (isnull(cp.BankDeposited,0) !=1
       OR isnull(cp.AmountCredited,0) != 1)
  AND ISNULL(p.Status,0) <> 2 

UNION 

SELECT distinct top 10 s.Name,s.Mobile,s.PatientId from 
sales as s
join Patient p on p.Id = s.PatientId
left join SalesPayment sp on s.Id=sp.SalesId
WHERE s.InstanceId=@instanceid
  AND s.AccountId=@accountid
  AND s.PaymentType = 'Multiple'
  AND s.Name LIKE '%'+ISNULL(@name,'')+'%'
  AND s.Mobile LIKE '%'+ISNULL(@mobile,'')+'%'
  AND (isnull(s.BankDeposited,0) != 1
       OR isnull(s.AmountCredited,0) != 1)
	   AND sp.PaymentInd = 3
	    and sp.isActive = 1
  AND ISNULL(p.Status,0) <> 2 
		 ) as a
		 
		 
		 
		 END

	