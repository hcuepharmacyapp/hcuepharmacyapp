

Create PROCEDURE [dbo].[salesGstReport](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime )
 AS

select a.InvoiceDate, a.InvoiceNo, sum(a.GrossAmount) SalesAmount, sum(a.GstAmount) GstAmount, sum(a.Gst0_SaleValue) Gst0_SaleValue, sum(a.Gst0_Value) Gst0_Value, sum(a.Gst0_SaleValue)+sum(a.Gst0_Value) Gst0_TotalSaleValue, sum(a.Gst5_SaleValue) Gst5_SaleValue, sum(a.Gst5_Value) Gst5_Value, sum(a.Gst5_SaleValue)+sum(a.Gst5_Value) Gst5_TotalSaleValue, sum(a.Gst12_SaleValue) Gst12_SaleValue, sum(a.Gst12_Value) Gst12_Value, sum(a.Gst12_SaleValue)+sum(a.Gst12_Value) Gst12_TotalSaleValue, sum(a.Gst18_SaleValue) Gst18_SaleValue, sum(a.Gst18_Value) Gst18_Value, sum(a.Gst18_SaleValue)+sum(a.Gst18_Value) Gst18_TotalSaleValue, sum(a.Gst28_SaleValue) Gst28_SaleValue, sum(a.Gst28_Value) Gst28_Value, sum(a.Gst28_SaleValue)+sum(a.Gst28_Value) Gst28_TotalSaleValue, sum(a.GstOther_SaleValue) GstOther_SaleValue, sum(a.GstOther_Value) GstOther_Value, sum(a.GstOther_SaleValue)+sum(a.GstOther_Value) GstOther_TotalSaleValue from 

(select sa.InvoiceDate,(isnull(sa.Prefix,'') + isnull(sa.InvoiceSeries,'') + sa.InvoiceNo) InvoiceNo, (si.ItemAmount-si.DiscountAmount-si.GstAmount) SalesAmount, si.GstAmount,(si.ItemAmount-si.DiscountAmount) GrossAmount,
case isnull(si.GstTotal,0) when 0 then (si.ItemAmount-si.DiscountAmount-si.GstAmount) else 0 end Gst0_SaleValue,
case isnull(si.GstTotal,0) when 0 then si.GstAmount else 0 end Gst0_Value,
case isnull(si.GstTotal,0) when 5 then (si.ItemAmount-si.DiscountAmount-si.GstAmount) else 0 end Gst5_SaleValue,
case isnull(si.GstTotal,0) when 5 then si.GstAmount else 0 end Gst5_Value,
case isnull(si.GstTotal,0) when 12 then (si.ItemAmount-si.DiscountAmount-si.GstAmount) else 0 end Gst12_SaleValue,
case isnull(si.GstTotal,0) when 12 then si.GstAmount else 0 end Gst12_Value,
case isnull(si.GstTotal,0) when 18 then (si.ItemAmount-si.DiscountAmount-si.GstAmount) else 0 end Gst18_SaleValue,
case isnull(si.GstTotal,0) when 18 then si.GstAmount else 0 end Gst18_Value,
case isnull(si.GstTotal,0) when 28 then (si.ItemAmount-si.DiscountAmount-si.GstAmount) else 0 end Gst28_SaleValue,
case isnull(si.GstTotal,0) when 28 then si.GstAmount else 0 end Gst28_Value,
case when isnull(si.GstTotal,0) not in (0,5,12,18,28) then (si.ItemAmount-si.DiscountAmount-si.GstAmount) else 0 end GstOther_SaleValue,
case when isnull(si.GstTotal,0) not in (0,5,12,18,28) then si.GstAmount else 0 end GstOther_Value
from Sales sa with(nolock) inner join 
SalesItem si with(nolock) on sa.Id=si.SalesId
where sa.AccountId=@AccountId and sa.InstanceId=@InstanceId and sa.CreatedAt between @StartDate and @EndDate and isnull(sa.Cancelstatus,0)= 0 ) a
group by a.InvoiceDate, a.InvoiceNo
order by a.InvoiceDate desc


