﻿/*******************************************************************************                                   
** Date        Author          Description                               
*******************************************************************************         
 ** 24/10/2017	Lawrence	   Stock Report Print
*******************************************************************************/  
-- usp_GetStockRptLstForPrint '3e2ec066-1551-4527-be53-5aa3c5b7fb7d', 'c503ca6e-f418-4807-a847-b6886378cf0b', 'nonzero', 1, '', '', '', '', '','','',''
-- usp_GetStockRptLstForPrint '3e2ec066-1551-4527-be53-5aa3c5b7fb7d', 'c503ca6e-f418-4807-a847-b6886378cf0b', 'nonzero', 0, '', '', '', '', '','','',''
CREATE PROCEDURE [dbo].[usp_GetStockRptLstForPrint] (@InstanceId varchar(36),@AccountId varchar(36), @StockType varchar(10), @IsProductWise bit=0, @Category varchar(250), @Manufacturer varchar(250),@RackNo Varchar(100),@BoxNo Varchar(100), @Alphabet1 char(1), @Alphabet2 char(1), @fromDate date, @toDate date)
AS
 BEGIN
  Declare @From_Stock decimal(18,2)=0, @To_Stock decimal(18,2) =0,@From_Expiry date =NULL, @To_Expiry date =NULL 
 declare @Status table (stockstatus int )
 if (right(@StockType,1) ='2')
 begin
 insert into @Status 	values (2)
 select @StockType  = replace (@StockType,2,'')
 end 
 else
 begin
 insert into @Status 	values (1),(0)
 end 
 if (@fromDate ='') set @fromDate = NULL
 if (@toDate ='') set @toDate = NULL

 if (@StockType ='nonzero')
	begin
		select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  = cast(getdate () as date), @To_Expiry  = NULL	 
	end 
 else if (@StockType ='expiry')
	begin
		select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  =  NULL, @To_Expiry  =cast(getdate () as date)	
	end
	
	else if (@StockType ='all')
	begin
		select @From_Stock = null , @To_Stock = NULL,@From_Expiry  =  NULL, @To_Expiry  =Null	
	end
	
	if (@Category ='' Or @Category='null')
	begin
	select @Category  = NULL	 
	end

   if (@Manufacturer ='' Or @Manufacturer='null')
	begin
	select @Manufacturer  = NULL	 
	end
	   if (@RackNo ='' Or @RackNo='null')
	begin
	select @RackNo  = NULL	 
	end
  if (@BoxNo ='' Or @BoxNo='null')
	begin
	select @BoxNo  = NULL	 
	end	
		if(@IsProductWise=1)
	Begin
		
	select @Alphabet1 = isnull(@Alphabet1,'')
	select @Alphabet2 = isnull(@Alphabet2,'')

  SELECT A1.*, I.Name as [InstName], I.DrugLicenseNo, I.TinNo, I.GsTinNo, I.Address as [InstAddress]
  , 0 CostPriceGst FROM (

  SELECT P.Name, Sum(ABS(PS.Stock)) AS Quantity, p.id, Sum(ISNULL((PS.Stock * PS.Sellingprice),0)) as MRP, SUM(PS.PackageSize) AS PackageSize
  --,Max(isnull(Vendor.Name ,'')) AS Vendor
  ,sum(isnull(PS.SellingPrice,0)) as SellingPrice
  ,P.Category, P.Manufacturer, P.Schedule, P.Type,Max(isnull(pis.rackno,'') ) [RackNo],  Max(isnull(pis.BoxNo,'')) [BoxNo]
    FROM (Select * from ProductStock ps WITH(NOLOCK) where PS.InstanceId = @InstanceId
	 	and ps.Stock between isnull(@From_Stock , ps.stock) and isnull(@To_Stock , ps.stock)  
		 and cast(ps.ExpireDate  as date)  between isnull(@From_Expiry ,cast(ps.ExpireDate as date)) and isnull(@To_Expiry,cast(ps.ExpireDate  as date))
		  and  ps.Stock > = 0 ) PS 		
    INNER JOIN (Select * from Product  WITH(NOLOCK) where accountid =@AccountId) P ON P.Id = PS.ProductId 
    LEFT JOIN (Select * from Vendor  WITH(NOLOCK) where accountid =@AccountId) vendor ON Vendor.Id = PS.VendorId 
	Left Join (Select Productid,  max(isnull(pis.rackno,'') ) [RackNo],  max(isnull(pis.BoxNo,'') ) [BoxNo] from  ProductInstance (NOLOCK) pis
	where pis.InstanceId = @InstanceId and  isnull(pis.rackno,'')  = isnull(@rackNo,  isnull(pis.rackno,'')) 
	and  isnull(pis.BoxNo,'')  = isnull(@BoxNo,  isnull(pis.BoxNo,'')) group by Productid ) PIS on pis.ProductId = ps.ProductId	
	left join (select ProductStockId,  Max(isnull(purchaseprice ,0))  [purchaseprice] from vendorpurchaseitem(NOLOCK) 
	inner join vendorpurchase (NOLOCK)  vp on vp.id =vendorpurchaseitem.vendorpurchaseid
	and vp.instanceid = vendorpurchaseitem.instanceid  	
	where isnull(CancelStatus ,0) =0  and vendorpurchaseitem.instanceid=@InstanceId
	and (isnull(vendorpurchaseitem.Status  ,0) in (1,1))
	 Group by vendorpurchaseitem.ProductStockId) as VP on VP.ProductStockId=ps.id   	 	 
     WHERE PS.InstanceId = @InstanceId 
	and ps.Stock between isnull(@From_Stock , ps.stock) and isnull(@To_Stock , ps.stock) 	
	and isnull(P.Category,'')=isnull(@Category,isnull(P.Category,''))		  
	and isnull(P.Manufacturer,'')=isnull(@Manufacturer,isnull(P.Manufacturer,''))
	-- Newly added for product filter
	and (isnull(P.Name,'') like case when ''+@Alphabet1+'' = '' then  isnull(replace(P.Name,'[','\['),'') else '['+@Alphabet1+'-'+@Alphabet2+']%' end ESCAPE '\')
	And cast(ps.ExpireDate  as date)  between  isnull(@fromDate,cast(ps.ExpireDate  as date) ) and isnull(@toDate, cast(ps.ExpireDate  as date))  
	and  isnull(pis.rackno,'') = isnull(@rackNo,  isnull(pis.rackno,'')) 
	and  isnull(pis.BoxNo,'') = isnull(@BoxNo,  isnull(pis.BoxNo,''))
	group by P.Name,P.GenericName,p.id,P.Category, P.Manufacturer, P.Schedule, P.Type
	 ) A1
	 JOIN Instance I ON I.Id = @InstanceId
	 ORDER BY A1.Name 
	 
	End
	
	else
	Begin
	
  SELECT P.Name, PS.BatchNo, ABS(PS.Stock) AS Quantity, p.id,
         PS.ExpireDate, ISNULL(PS.PurchasePrice,isnull( Vp.purchaseprice,0)) * (PS.Stock)  As CostPrice,isnull(ps.Poprice ,0) * (PS.Stock) as CostPriceGst,
	     ISNULL((PS.Stock * PS.SellingPrice),0) as MRP, PS.SellingPrice as SellingPrice, P.Category, P.Manufacturer, (isnull(pis.rackno,'')) [RackNo], (isnull(pis.BoxNo,'')) [BoxNo]
		 ,I.Name as [InstName], I.DrugLicenseNo, I.TinNo, I.GsTinNo, I.Address as [InstAddress]
		 , case when ISNULL(ABS(ps.PackageSize),0) > 0 then ISNULL(ABS(ps.PackageSize),0) else ISNULL(p.PackageSize,0) end as PackageSize
    FROM (Select (CASE isnull(taxrefno,0)  WHEN 1 THEN  ISNULL(ps.PurchasePrice,0)
 else (isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0))) +((isnull(ps.PurchasePrice,0) *100 /(100+ISNULL(ps.VAT,0)))* isnull(ps.gsttotal,0) /100) end) [Poprice] , * from ProductStock ps WITH(NOLOCK) where PS.InstanceId = @InstanceId 
	 	and ps.Stock between isnull(@From_Stock , ps.stock) and isnull(@To_Stock , ps.stock)  
		 and cast(ps.ExpireDate  as date)  between isnull(@From_Expiry ,cast(ps.ExpireDate  as date) ) and isnull(@To_Expiry,cast(ps.ExpireDate  as date))
		 	 and  ps.Stock > = 0) PS 
    INNER JOIN(Select * from Product  WITH(NOLOCK) where accountid =@AccountId) P ON P.Id = PS.ProductId 
    LEFT JOIN( Select * from Vendor  WITH(NOLOCK) where accountid =@AccountId) vendor ON Vendor.Id = PS.VendorId 
	Left Join (Select Productid,  max(isnull(pis.rackno,'') ) [RackNo],  max(isnull(pis.BoxNo,'') ) [BoxNo] from  ProductInstance (NOLOCK) pis
	where pis.InstanceId = @InstanceId and  isnull(pis.rackno,'')  = isnull(@rackNo,  isnull(pis.rackno,'')) 
	and  isnull(pis.BoxNo,'')  = isnull(@BoxNo,  isnull(pis.BoxNo,'')) group by Productid ) PIS on pis.ProductId = ps.ProductId
	left join (select ProductStockId,  Max(isnull(purchaseprice ,0))  [purchaseprice] from vendorpurchaseitem(NOLOCK)   where  instanceid=@InstanceId
	and vendorpurchaseid in (Select id from vendorpurchase (NOLOCK)  vp where instanceid=@InstanceId and isnull(CancelStatus ,0) =0 ) 
	and (isnull(vendorpurchaseitem.Status,0) in (1))
 	 Group by vendorpurchaseitem.ProductStockId)  as VP on VP.ProductStockId=ps.id   
	 join Instance I ON I.Id = @InstanceId
     WHERE PS.InstanceId = @InstanceId 
	 	and ps.Stock between isnull(@From_Stock , ps.stock) and isnull(@To_Stock , ps.stock) 	
		and isnull(P.Category,'')= isnull(@Category,isnull(P.Category,''))		  
		and isnull(P.Manufacturer,'')= isnull(@Manufacturer,isnull(P.Manufacturer,''))		  
		-- Newly added for product filter
		and (isnull(P.Name,'') like case when ''+@Alphabet1+'' = '' then  isnull(replace(P.Name,'[','\['),'') else '['+@Alphabet1+'-'+@Alphabet2+']%' end ESCAPE '\')

		And cast(ps.ExpireDate  as date)  between  isnull(@fromDate,cast(ps.ExpireDate  as date) ) and isnull(@toDate, cast(ps.ExpireDate  as date))
		and  isnull(pis.rackno,'')  = isnull(@rackNo,  isnull(pis.rackno,''))
	and  isnull(pis.BoxNo,'')  = isnull(@BoxNo,  isnull(pis.BoxNo,''))
	 ORDER BY P.Name 
	End

End