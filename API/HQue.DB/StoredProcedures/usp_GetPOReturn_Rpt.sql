/*     /                        
******************************************************************************                            
** File: [usp_GetPOReturn_Rpt]  
** Name: [usp_GetPOReturn_Rpt]                              
** Description: To Purchase Retrun Details 
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author: Poongodi R   
** Created Date:   08/02/2017 
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 ** 18/04/2017 Poongodi R		Return Item duplicate issue fixed and Price taken from PR Return table
 ** 22/06/2017   Violet			Prefix Added 
 ** 05/07/2017   Sarubala		GST Calculation Added 
 **23/08/2017   Lawrence		Return Date filter added 
 ** 07/09/2017	Poongodi		Vendorpurchase grn date changed to return created date
 ** 13/09/2017   Lawrence		All branch condition Added 
*******************************************************************************/ 
--Usage : -- usp_GetPOReturn_Rpt  @StartDate='01-Jan-2017',@EndDate=N'06-Feb-2017',@AccountId=N'18204879-99ff-4efd-b076-f85b4a0da0a3',@InstanceId=N'013513b1-ea8c-4ea8-9fed-054b260ee197' 
Create PROCEDURE [dbo].[Usp_getporeturn_rpt](@InstanceId VARCHAR(36), 
                                             @AccountId  VARCHAR(36), 
                                             @StartDate  DATETIME, 
                                             @EndDate    DATETIME,
											 @IsInvoiceDate bit=0) 
AS 
  BEGIN 
      Declare @InvStartDt date,  @InvEndDt date,
	  @GRNStartDt date, @GRNEndDt date
	  IF @IsInvoiceDate=1
	  Begin
		select @InvStartDt = @StartDate, @InvEndDt = @EndDate, @GRNStartDt = NULL, @GRNEndDt = NULL
	  End
	  Else
	  Begin
		select @InvStartDt = NULL, @InvEndDt = NULL, @GRNStartDt = @StartDate, @GRNEndDt = @EndDate
	  End
	  SELECT vendorreturnitem.productstockid [Productstockid], Inst.Name AS InstanceName,
             vendorreturnitem.quantity		[Quantity], 
             vendorreturn.id                  AS [VendorReturnId], 
             ltrim(isnull(vendorreturn.Prefix,'')+' '+vendorreturn.returnno)            AS [ReturnNo], 
             vendorreturn.returndate          AS [ReturnDate], 
             vendorpurchase.id                AS [VendorPurchaseId], 
             vendorpurchase.vendorid          AS [VendorId], 
             vendorpurchase.discount          AS [VendorPurchaseDiscount], 
             vendorpurchase.invoiceno         AS [InvoiceNo], 
			 CAST(isnull(vendorreturn.TaxRefNo,0) as int) TaxRefNo,
             vendor.NAME                      AS [VendorName], 
             vendor.mobile                    AS [VendorMobile], 
             isnull(vendor.email ,'')         AS [VendorEmail], 
             vendor.address                   AS [VendorAddress], 
             vendor.GsTinNo                   AS [VendorTinNo], 
             productstock.sellingprice        AS [SellingPrice], 
             productstock.vat                 AS [VAT], 			 
			 ISNULL(VendorReturnItem.GstTotal,productstock.GstTotal)        AS [GstTotal], 
			 ISNULL(VendorReturnItem.Cgst,productstock.Cgst)                AS [Cgst], 
			 ISNULL(VendorReturnItem.Sgst,productstock.Sgst)                AS [Sgst], 
			 ISNULL(VendorReturnItem.Igst,productstock.Igst)                AS [Igst], 
             vendorpurchaseitem.id            AS [VendorPurchaseItemId], 
             isnull(vendorpurchaseitem.purchaseprice,VendorReturnItem.ReturnPurchasePrice) AS [PurchasePrice], 
			 
             product.NAME                     AS [ProductName] ,
			 isnull(vendorreturnitem.quantity,0) * isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) [ReturnTotal]
      FROM   vendorreturnitem WITH(NOLOCK) 
             INNER JOIN vendorreturn WITH(NOLOCK)
                     ON vendorreturn.id = vendorreturnitem.vendorreturnid 
             LEFT JOIN vendorpurchase WITH(NOLOCK) --INNER JOIN vendorpurchase 
                     ON vendorpurchase.id = vendorreturn.vendorpurchaseid 
             INNER JOIN vendor WITH(NOLOCK)
                     ON vendor.id = vendorreturn.vendorid 
             INNER JOIN productstock WITH(NOLOCK)
                     ON productstock.id = vendorreturnitem.productstockid 
             LEFT JOIN vendorpurchaseitem WITH(NOLOCK)
                     ON vendorpurchaseitem.productstockid = productstock.id 
					 and VendorPurchaseItem.VendorPurchaseId = VendorReturn.VendorPurchaseId
             INNER JOIN product WITH(NOLOCK)
                     ON product.id = productstock.productid 
			INNER JOIN Instance Inst on Inst.AccountId = @AccountId 
			and (Inst.AccountId = @AccountId and Inst.Id = ISNULL(@InstanceId,vendorreturn.InstanceId))

      WHERE  (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0) and -- Condition added by Gavaskar 12-09-2017
	  /* vendorreturn.returndate BETWEEN @StartDate AND @EndDate
	  and */
	  cast(vendorreturn.returndate as date) BETWEEN isnull(@InvStartDt,cast(vendorreturn.returndate as date)) AND isnull(@InvEndDt,cast(vendorreturn.returndate as date))
	  AND cast(vendorreturn.CreatedAt as date)  BETWEEN isnull(@GRNStartDt,cast(vendorreturn.CreatedAt as date)) AND isnull(@GRNEndDt,cast(vendorreturn.CreatedAt as date))
             AND vendorreturn.accountid = @AccountId 
             AND vendorreturn.instanceid = ISNULL(@InstanceId,vendorreturn.InstanceId) 
      ORDER  BY vendorreturn.createdat DESC 
  END 
   
