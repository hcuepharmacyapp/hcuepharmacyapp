﻿/*                               
******************************************************************************                            
** File: [usp_GetKindOfProductList]   
** Name: [usp_GetKindOfProductList]                               
** Description: To Get Kind Of Product List
**   
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 11/11/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
  
*******************************************************************************/
 
 CREATE PROCEDURE [dbo].[usp_GetKindOfProductList](@AccountId varchar(36),@InstanceId varchar(36))
 AS
 BEGIN
   SET NOCOUNT ON

select distinct KindName from product (nolock)  where AccountId= @AccountId AND KindName in ('OTC','F&B','FMCG','Prescription','Generics','Surgicals') ORDER BY KindName ASC
           
END
           
 
