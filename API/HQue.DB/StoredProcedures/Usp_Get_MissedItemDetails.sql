/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 22/09/2017  Poongodi		Created
*******************************************************************************/ 
Create Proc dbo.Usp_Get_MissedItemDetails(@Id char(36),
@Type varchar(20) ,
@AccountId char(36),
@InstanceId char(36)
)
as
begin
 
if (@Type='Sale')
begin
	select   * from SalesItem(nolock)  where SalesId =@Id
end 
else if (@Type='Purchase')
begin
	 select   * from VendorPurchaseItem(nolock)  where VendorPurchaseId =@Id
 
end 
else if (@Type='Purchasestock')
begin
 
	select   ps.* from  (SElect * from VendorPurchaseItem (nolock) where  Id =@Id ) vpi  
	INner join (SElect * from ProductStock (nolock) where AccountId =@AccountId and Instanceid =@InstanceId) Ps on ps.id = vpi.ProductStockId 
end 
end
 
