/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 17/04/2017 Poongodi			Account Id and Instance id validated from Vendorpurchase table
** 21/04/2017 Poongodi			Credit Note and Debit note amount removed from PO Value calculation
**04/05/2017 Poongodi			Credit Note Value taken
**22/06/2017  Violet			Prefix Added 
**05/07/2017  Sarubala			GST Calculation Added 
**23/08/2017  Lawrence			Added for 'Invoice Date Wise' filter added
** 13/09/2017   Lawrence		All branch condition Added 
** 12/10/2017   Gavaskar		Purchase and invoice final amount direct db column value fetched
** 19/10/2017   Poongodi		Vendorpurchase id added in Group by section
*******************************************************************************/ 
 --Usage : -- usp_GetBuySummaryList '0020ccad-e432-4ec5-8927-e9302db2340a','d5baa673-d6e4-4e3e-a593-cdd6313be32c','1900-01-01','2016-11-01'
 -- usp_GetBuySummaryList null, 'c503ca6e-f418-4807-a847-b6886378cf0b',  '01-Jan-2017', '14-Sep-2017',0
Create PROCEDURE [dbo].[usp_GetBuySummaryList](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime, @IsInvoiceDate bit=0)
AS
 BEGIN
   SET NOCOUNT ON
      	  Declare @InvStartDt date,  @InvEndDt date,
	  @GRNStartDt date, @GRNEndDt date

	  IF @IsInvoiceDate=1
	  Begin
		select @InvStartDt = @StartDate, @InvEndDt = @EndDate, @GRNStartDt = NULL, @GRNEndDt = NULL
	  End
	  Else
	  Begin
		select @InvStartDt = NULL, @InvEndDt = NULL, @GRNStartDt = @StartDate, @GRNEndDt = @EndDate
	  End
SELECT  VP.id, VP.InvoiceDate,VP.InvoiceNo,VP.GoodsRcvNo,V.Name,0.00 NoteAmount,VP.NoteType,isnull(VP.Prefix,'') +ISNULL(VP.BillSeries,'') BillSeries,

/* SUM( (vi.PackagePurchasePrice*(vi.PackageQty-ISNULL(vi.FreeQty,0))*(1- ISNULL(vi.Discount,0) / 100))
      *(1 + 0.01*(CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END))) AS   InvoiceValue */

/* 
sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))-((vi.PackagePurchasePrice * 
(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * (
(CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))+
(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)* 
(CASE WHEN VP.TaxRefNo = 1 THEN isnull(PS.GstTotal,VI.GstTotal) ELSE (CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) END)))
/*+(CASE WHEN VP.NoteType='credit' THEN -(VP.NoteAmount) WHEN VP.NoteType='debit' THEN VP.NoteAmount else 0 END)*/ as InvoiceValue, */

(ISNULL(VP.TotalPurchaseValue,0) + ISNULL(VP.VatValue,0))as InvoiceValue,

mAX(CASE isnull(VP.NoteType,'') WHEN 'credit' THEN  isnull(VP.NoteAmount ,0) else 0 END) NoteValue , 
mAX(CASE isnull(VP.NoteType,'') WHEN 'debit' THEN  isnull(VP.NoteAmount ,0) else 0 END) DebitNoteValue ,

/* 
sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))-((vi.PackagePurchasePrice * 
(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * (
(CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))+
(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)* 
(CASE WHEN VP.TaxRefNo = 1 THEN isnull(PS.GstTotal,VI.GstTotal) ELSE (CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) END)))
 +(CASE WHEN VP.NoteType='credit' THEN -(VP.NoteAmount) WHEN VP.NoteType='debit' THEN VP.NoteAmount else 0 END)  as FinalValue, */

(ISNULL(VP.TotalPurchaseValue,0) + ISNULL(VP.VatValue,0)) + (CASE WHEN VP.NoteType='credit' THEN -(ISNULL(VP.NoteAmount,0)) 
WHEN VP.NoteType='debit' THEN ISNULL(VP.NoteAmount,0) else 0 END) as FinalValue,

Ins.Name AS InstanceName 
FROM VendorPurchase VP WITH(NOLOCK)
INNER JOIN VendorPurchaseItem VI  WITH(NOLOCK) ON VP.Id=vi.VendorPurchaseId 
INNER JOIN ProductStock PS  WITH(NOLOCK) ON vi.ProductStockId=ps.id 
LEFT OUTER JOIN Vendor V  WITH(NOLOCK) ON V.Id=VP.VendorId 
INNER JOIN Instance Ins on Ins.Id = ISNULL(@InstanceId, VP.InstanceId)
WHERE vp.InstanceId=ISNULL(@InstanceId,vp.InstanceId) and vp.AccountId=@AccountId 
AND cast(VP.CreatedAt as date) BETWEEN isnull(@GRNStartDt,cast(VP.CreatedAt as date)) AND isnull(@GRNEndDt,cast(VP.CreatedAt as date))
and cast(VP.InvoiceDate as date)  BETWEEN isnull(@InvStartDt,cast(VP.InvoiceDate as date) ) AND isnull(@InvEndDt,cast(VP.InvoiceDate as date))
and isnull(vp.CancelStatus ,0)= 0
and (VI.Status is null or VI.Status = 1)
GROUP BY  VP.id, VP.InvoiceNo,VP.InvoiceDate,VP.GoodsRcvNo,V.Name,VP.NoteAmount,VP.NoteType,VP.BillSeries,VP.Prefix, Ins.Name,VP.TotalPurchaseValue,VP.VatValue
ORDER BY VP.InvoiceDate DESC 
           
 END