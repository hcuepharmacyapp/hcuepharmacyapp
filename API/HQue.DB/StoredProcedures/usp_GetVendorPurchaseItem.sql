﻿ Create procedure [dbo].[usp_GetVendorPurchaseItem] (@VendorPurchaseId char(36))
  as
  begin

  select vpi.Quantity ,
         vpi.ProductStockId,
		 vpi.PurchasePrice,
		 vpi.PackageSellingPrice,
		 vpi.PackageMRP,
		 vpi.PackagePurchasePrice,
		 vpi.PackageQty,
		 vpi.PackageSize,
		 vpi.FreeQty,
		 vpi.Discount,
		 vpi.DiscountValue,
		 --vpi.Igst,
		 --vpi.Cgst,
		 --vpi.Sgst,
		 --vpi.GstTotal,
		 ps.Id as productstockid,
		 ps.ProductId,
		 ps.BatchNo,
		 ps.ExpireDate,
		 ps.VAT,
		 vpi.Igst,
		 vpi.Cgst,
		 vpi.Sgst,
		 isnull(vpi.GstTotal,0) AS GstTotal,
		 
		 ps.SellingPrice,
		 ps.MRP,
		 ps.Stock,
		 ps.CST,
		 ps.PackageSize,
		 CAST(((vpi.PurchasePrice * 100/(100+ISNULL(vpi.gsttotal, 0))) * vpi.PackageSize) AS DECIMAL(18, 6)) AS pkgPurPrice
   from VendorPurchaseItem as vpi inner join ProductStock as ps on vpi.ProductStockId =ps.Id
        where vpi.VendorPurchaseId = @VendorPurchaseId 
		      and (vpi.Status is null or vpi.Status = 1) 
  end