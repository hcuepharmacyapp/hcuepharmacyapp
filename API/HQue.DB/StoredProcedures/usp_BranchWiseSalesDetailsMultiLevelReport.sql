
Create PROCEDURE [dbo].[usp_BranchWiseSalesDetailMultiLevelReport](
 @InstanceIds VARCHAR(1000),@AccountId CHAR(36), @Level smallint, @StartDate datetime,@EndDate datetime, 
 @SearchColName VARCHAR(50), @SearchOption VARCHAR(500), @SearchValue VARCHAR(500)
)
 AS
 BEGIN
	SET NOCOUNT ON

	DECLARE @billNo VARCHAR(100) = NULL, 
	@PatientId VARCHAR(100) = NULL, 
	@from_gstTotal NUMERIC(10,2) = NULL,
	@to_gstTotal NUMERIC(10,2) = NULL,
	@from_profitCost NUMERIC(10,2) = NULL,
	@to_profitCost NUMERIC(10,2) = NULL,
	@from_profitMrp NUMERIC(10,2) = NULL,
	@to_profitMrp NUMERIC(10,2) = NULL
	
	IF @SearchColName IS NULL OR @SearchColName = ''
		SELECT @SearchColName = NULL, @SearchOption = NULL, @SearchValue = NULL
	IF @SearchOption = ''
		SELECT @SearchOption = NULL
	IF @SearchValue = '' 
		SELECT @SearchValue = NULL

	DECLARE @InvoiceSeries VARCHAR(500) = @SearchOption
	IF (ISNULL(@SearchColName,'') != 'billNo') 
		SELECT @InvoiceSeries = 'ALL'

	DECLARE @XmlList XML
	SET @XmlList = CAST(('<A>'+REPLACE(ISNULL(@InvoiceSeries,''),',','</A><A>')+'</A>') AS XML)
		
	IF (@SearchColName = 'billNo') 
		BEGIN
			SELECT @billNo = @SearchValue
		END
	Else IF (@SearchColName ='gstTotal') 
		BEGIN 
		  IF (@SearchOption = 'equal') 
		  SELECT @from_gstTotal = Cast (@SearchValue AS NUMERIC(10,2)) , 
				 @to_gstTotal = Cast(@SearchValue AS NUMERIC(10,2)) 
		  ELSE 
		  IF (@SearchOption = 'greater') 
		  SELECT @from_gstTotal = Cast(@SearchValue AS NUMERIC(10,2))+0.01, 
				 @to_gstTotal = NULL 
		  ELSE 
		  IF (@SearchOption = 'less') 
		  SELECT @from_gstTotal = 0.00, 
				 @to_gstTotal = Cast(@SearchValue AS NUMERIC(10,2))-0.01 
		END
	Else IF (@SearchColName ='customerName') 
		Select @PatientId = @SearchValue

	
	
	IF @Level = 1
	Begin
		;With temp_instances (InstanceId)
		As 
		(
			select * from udf_Split(@InstanceIds) a
		)
		SELECT 
		--InvoiceDate, 
		InstanceId, 
		InstanceName, 
		(Sum(SalesCount) - Sum(ReturnCount)) SalesCount, 
		(SUM(PurchasePrice) - SUM(ReturnPurchasePrice)) PurchasePrice, 
		(SUM(MRPAmount)-SUM(ReturnMRPAmount)) MRPAmount, 
		(SUM(InvoiceAmount)-SUM(ReturnInvoiceAmount)) InvoiceAmount, 
		(SUM(InvoiceAmountNoTaxNoDiscount)-SUM(ReturnInvoiceAmountNoTaxNoDiscount)) InvoiceAmountNoTaxNoDiscount, 
		(SUM(TaxAmount)-SUM(ReturnTaxAmount)) TaxAmount, 
		(SUM(DiscountValue)-SUM(ReturnDiscountValue)) DiscountValue, 
		SUM(RoundOffNetAmount) RoundOffNetAmount 
		FROM (
			SELECT 
			--InvoiceDate, 
			InstanceId, 
			InstanceName,
			Sum(SalesCount) SalesCount, 
			Sum(ReturnCount) ReturnCount, 
			SUM(PurchasePrice) PurchasePrice, 
			SUM(ReturnPurchasePrice) ReturnPurchasePrice, 
			SUM(MRPAmount) MRPAmount, 
			SUM(ReturnMRPAmount) ReturnMRPAmount, 
			SUM(InvoiceAmount) InvoiceAmount, 
			SUM(ReturnInvoiceAmount) ReturnInvoiceAmount, 
			SUM(InvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount, 
			SUM(ReturnInvoiceAmountNoTaxNoDiscount) ReturnInvoiceAmountNoTaxNoDiscount, 
			SUM(TaxAmount) TaxAmount, SUM(ReturnTaxAmount) ReturnTaxAmount, 
			SUM(DiscountValue) DiscountValue, 
			SUM(ReturnDiscountValue) ReturnDiscountValue, 
			SUM(RoundOffNetAmount) RoundOffNetAmount 
			FROM (
				SELECT 
			--	Convert(date,S.InvoiceDate) InvoiceDate, 
				S.Id, 
				I.Id InstanceId, 
				case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, 
				Count(DISTINCT S.Id) SalesCount, 0 As ReturnCount, 
				SUM((SI.Quantity) * ISNULL(PS.PurchasePrice,0)) as PurchasePrice, 
				0 as ReturnPurchasePrice, 
				S.SalesItemAmount As MRPAmount, 
				0 as ReturnMRPAmount, 
				S.SaleAmount As InvoiceAmount,
				0 as ReturnInvoiceAmount, 
				(S.SalesItemAmount-ISNULL(S.TotalDiscountValue,0))-S.GstAmount as InvoiceAmountNoTaxNoDiscount,
				0 as ReturnInvoiceAmountNoTaxNoDiscount, 
				S.GstAmount as TaxAmount, 
				0 as ReturnTaxAmount, 
				ISNULL(S.TotalDiscountValue,0) as DiscountValue, 
				0 as ReturnDiscountValue, 
				S.RoundoffSaleAmount AS RoundOffNetAmount
				FROM Sales S WITH(NOLOCK)
				INNER JOIN Instance I WITH(NOLOCK) On I.Id = S.InstanceId
				INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
				INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SI.productstockid
				INNER JOIN temp_instances TempIns On S.InstanceId = TempIns.InstanceId
				LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
				WHERE S.AccountId = @AccountId 
				AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
				and S.Cancelstatus is NULL
				AND 
				(
					ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
					ISNULL(P.Name, '') Like ISNULL(@PatientId, P.Name) + '%'
				)
				AND ISNULL(S.InvoiceSeries,'') = 
				(
					CASE 
					WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
					END
				)
				AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,''))  
				AND SI.GstTotal BETWEEN Isnull(@from_gstTotal, SI.GstTotal) AND Isnull(@to_gstTotal, SI.GstTotal)
				GROUP BY Convert(date,S.invoicedate), S.Id, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end,S.SalesItemAmount,
				S.SaleAmount,ISNULL(S.TotalDiscountValue,0),S.GstAmount,S.RoundoffSaleAmount
			) A group by  InstanceId, InstanceName

			UNION ALL

			SELECT 
		--	InvoiceDate, 
			InstanceId, 
			InstanceName, 
			Sum(SalesCount) SalesCount, 
			Sum(ReturnCount) ReturnCount, 
			SUM(PurchasePrice) PurchasePrice, 
			SUM(ReturnPurchasePrice) ReturnPurchasePrice,
			SUM(MRPAmount) MRPAmount, 
			SUM(ReturnMRPAmount) ReturnMRPAmount,
			SUM(InvoiceAmount) InvoiceAmount, 
			SUM(ReturnInvoiceAmount) ReturnInvoiceAmount, 
			SUM(InvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount, 
			SUM(ReturnInvoiceAmountNoTaxNoDiscount) ReturnInvoiceAmountNoTaxNoDiscount, 
			SUM(TaxAmount) TaxAmount, 
			SUM(ReturnTaxAmount) ReturnTaxAmount,
			SUM(DiscountValue) DiscountValue, 
			SUM(ReturnDiscountValue) ReturnDiscountValue, 
			SUM(RoundOffNetAmount) RoundOffNetAmount 
			FROM (
				SELECT 
			--	Convert(date,SR.ReturnDate) InvoiceDate, 
				SR.Id, 
				I.Id InstanceId, 
				case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, 
				0 As SalesCount, 
				Count(SR.Id) ReturnCount, 
				0 as PurchasePrice, 
				SUM((SRI.Quantity) * ISNULL(PS.PurchasePrice,0)) as ReturnPurchasePrice, 
				0 as MRPAmount, 
				SR.ReturnItemAmount As ReturnMRPAmount, 
				0 as InvoiceAmount, 
				SR.NetAmount As ReturnInvoiceAmount, 
				0 as InvoiceAmountNoTaxNoDiscount, 
				(SR.ReturnItemAmount-ISNULL(SR.TotalDiscountValue,0))-SR.GstAmount as ReturnInvoiceAmountNoTaxNoDiscount, 
				0 as TaxAmount, 
				SR.GstAmount as ReturnTaxAmount,
				0 as DiscountValue, 
				ISNULL(SR.TotalDiscountValue,0) as ReturnDiscountValue, 
				SR.RoundOffNetAmount AS RoundOffNetAmount
				FROM SalesReturn SR WITH(NOLOCK)
				INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
				INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
				INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId
				INNER JOIN temp_instances TempIns On SR.InstanceId = TempIns.InstanceId
				LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
				LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
				WHERE SR.AccountId = @AccountId
				AND  Convert(date,SR.returndate) BETWEEN @StartDate AND @EndDate
				AND S.Cancelstatus is NULL
				and ISNULL(sri.IsDeleted,0) != 1
				and ISNULL(sr.CancelType,0) != 2				
				AND 
				(
					ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
					ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
					or ISNULL(SR.PatientId,'') = ISNULL(@PatientId,'')
				)
				AND ISNULL(S.InvoiceSeries,'') = 
				(
					CASE 
					WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
					END
				)
				AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
				AND sri.GstTotal BETWEEN Isnull(@from_gstTotal, sri.GstTotal) AND Isnull(@to_gstTotal, sri.GstTotal)
				GROUP BY Convert(date,SR.ReturnDate), SR.Id, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end,SR.ReturnItemAmount,
				SR.NetAmount,ISNULL(SR.TotalDiscountValue,0),SR.GstAmount,SR.RoundOffNetAmount 
			) A group by  InstanceId, InstanceName
		) A
		GROUP BY  InstanceId, InstanceName
		ORDER BY InstanceName ASC
	End
	Else If @Level = 2
	Begin
		;With temp_instances (InstanceId)
		As 
		(
			select * from udf_Split(@InstanceIds) a
		)
		SELECT 
		InvoiceDate, 
		InstanceId, 
		InstanceName, 
		(Sum(SalesCount) - Sum(ReturnCount)) SalesCount, 
		(SUM(PurchasePrice) - SUM(ReturnPurchasePrice)) PurchasePrice, 
		(SUM(MRPAmount)-SUM(ReturnMRPAmount)) MRPAmount, 
		(SUM(InvoiceAmount)-SUM(ReturnInvoiceAmount)) InvoiceAmount, 
		(SUM(InvoiceAmountNoTaxNoDiscount)-SUM(ReturnInvoiceAmountNoTaxNoDiscount)) InvoiceAmountNoTaxNoDiscount, 
		(SUM(TaxAmount)-SUM(ReturnTaxAmount)) TaxAmount, 
		(SUM(DiscountValue)-SUM(ReturnDiscountValue)) DiscountValue, 
		SUM(RoundOffNetAmount) RoundOffNetAmount 
		FROM (
			SELECT 
			InvoiceDate, 
			InstanceId, 
			InstanceName, 
			Sum(SalesCount) SalesCount, 
			Sum(ReturnCount) ReturnCount, 
			SUM(PurchasePrice) PurchasePrice, 
			SUM(ReturnPurchasePrice) ReturnPurchasePrice, 
			SUM(MRPAmount) MRPAmount, 
			SUM(ReturnMRPAmount) ReturnMRPAmount, 
			SUM(InvoiceAmount) InvoiceAmount, 
			SUM(ReturnInvoiceAmount) ReturnInvoiceAmount, 
			SUM(InvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount, 
			SUM(ReturnInvoiceAmountNoTaxNoDiscount) ReturnInvoiceAmountNoTaxNoDiscount, 
			SUM(TaxAmount) TaxAmount, SUM(ReturnTaxAmount) ReturnTaxAmount, 
			SUM(DiscountValue) DiscountValue, 
			SUM(ReturnDiscountValue) ReturnDiscountValue, 
			SUM(RoundOffNetAmount) RoundOffNetAmount 
			FROM (
				SELECT 
				Convert(date,S.InvoiceDate) InvoiceDate, 
				S.Id, 
				I.Id InstanceId, 
				case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, 
				Count(DISTINCT S.Id) SalesCount, 0 As ReturnCount, 
				SUM((SI.Quantity) * ISNULL(PS.PurchasePrice,0)) as PurchasePrice, 
				0 as ReturnPurchasePrice, 
				S.SalesItemAmount As MRPAmount, 
				0 as ReturnMRPAmount, 
				S.SaleAmount As InvoiceAmount,
				0 as ReturnInvoiceAmount, 
				(S.SalesItemAmount-ISNULL(S.TotalDiscountValue,0))-S.GstAmount as InvoiceAmountNoTaxNoDiscount,
				0 as ReturnInvoiceAmountNoTaxNoDiscount, 
				S.GstAmount as TaxAmount, 
				0 as ReturnTaxAmount, 
				ISNULL(S.TotalDiscountValue,0) as DiscountValue, 
				0 as ReturnDiscountValue, 
				S.RoundoffSaleAmount AS RoundOffNetAmount
				FROM Sales S WITH(NOLOCK)
				INNER JOIN Instance I WITH(NOLOCK) On I.Id = S.InstanceId
				INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.salesid
				INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SI.productstockid
				INNER JOIN temp_instances TempIns On S.InstanceId = TempIns.InstanceId
				LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
				WHERE S.AccountId = @AccountId AND  S.InstanceId = @InstanceIds
				AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
				and S.Cancelstatus is NULL
				AND 
				(
					ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
					ISNULL(P.Name, '') Like ISNULL(@PatientId, P.Name) + '%'
				)
				AND ISNULL(S.InvoiceSeries,'') = 
				(
					CASE 
					WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
					END
				)
				AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,''))  
				AND SI.GstTotal BETWEEN Isnull(@from_gstTotal, SI.GstTotal) AND Isnull(@to_gstTotal, SI.GstTotal)
				GROUP BY Convert(date,S.invoicedate), S.Id, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end,S.SalesItemAmount,
				S.SaleAmount,ISNULL(S.TotalDiscountValue,0),S.GstAmount,S.RoundoffSaleAmount
			) A group by invoicedate, InstanceId, InstanceName

			UNION ALL

			SELECT 
			InvoiceDate, 
			InstanceId, 
			InstanceName, 
			Sum(SalesCount) SalesCount, 
			Sum(ReturnCount) ReturnCount, 
			SUM(PurchasePrice) PurchasePrice, 
			SUM(ReturnPurchasePrice) ReturnPurchasePrice,
			SUM(MRPAmount) MRPAmount, 
			SUM(ReturnMRPAmount) ReturnMRPAmount,
			SUM(InvoiceAmount) InvoiceAmount, 
			SUM(ReturnInvoiceAmount) ReturnInvoiceAmount, 
			SUM(InvoiceAmountNoTaxNoDiscount) InvoiceAmountNoTaxNoDiscount, 
			SUM(ReturnInvoiceAmountNoTaxNoDiscount) ReturnInvoiceAmountNoTaxNoDiscount, 
			SUM(TaxAmount) TaxAmount, 
			SUM(ReturnTaxAmount) ReturnTaxAmount,
			SUM(DiscountValue) DiscountValue, 
			SUM(ReturnDiscountValue) ReturnDiscountValue, 
			SUM(RoundOffNetAmount) RoundOffNetAmount 
			FROM (
				SELECT 
				Convert(date,SR.ReturnDate) InvoiceDate, 
				SR.Id, 
				I.Id InstanceId, 
				case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, 
				0 As SalesCount, 
				Count(SR.Id) ReturnCount, 
				0 as PurchasePrice, 
				SUM((SRI.Quantity) * ISNULL(PS.PurchasePrice,0)) as ReturnPurchasePrice, 
				0 as MRPAmount, 
				SR.ReturnItemAmount As ReturnMRPAmount, 
				0 as InvoiceAmount, 
				SR.NetAmount As ReturnInvoiceAmount, 
				0 as InvoiceAmountNoTaxNoDiscount, 
				(SR.ReturnItemAmount-ISNULL(SR.TotalDiscountValue,0))-SR.GstAmount as ReturnInvoiceAmountNoTaxNoDiscount, 
				0 as TaxAmount, 
				SR.GstAmount as ReturnTaxAmount,
				0 as DiscountValue, 
				ISNULL(SR.TotalDiscountValue,0) as ReturnDiscountValue, 
				SR.RoundOffNetAmount AS RoundOffNetAmount
				FROM SalesReturn SR WITH(NOLOCK)
				INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
				INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
				INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SRI.ProductStockId
				INNER JOIN temp_instances TempIns On SR.InstanceId = TempIns.InstanceId
				LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
				LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
				WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceIds
				AND  Convert(date,SR.returndate) BETWEEN @StartDate AND @EndDate
				AND S.Cancelstatus is NULL
				and ISNULL(sri.IsDeleted,0) != 1
				and ISNULL(sr.CancelType,0) != 2				
				AND 
				(
					ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
					ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
					or ISNULL(SR.PatientId,'') = ISNULL(@PatientId,'')
				)
				AND ISNULL(S.InvoiceSeries,'') = 
				(
					CASE 
					WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
					WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
					END
				)
				AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
				AND sri.GstTotal BETWEEN Isnull(@from_gstTotal, sri.GstTotal) AND Isnull(@to_gstTotal, sri.GstTotal)
				GROUP BY Convert(date,SR.ReturnDate), SR.Id, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end,SR.ReturnItemAmount,
				SR.NetAmount,ISNULL(SR.TotalDiscountValue,0),SR.GstAmount,SR.RoundOffNetAmount 
			) A group by invoicedate, InstanceId, InstanceName
		) A
		GROUP BY Convert(date,invoicedate),InstanceId, InstanceName
		ORDER BY Convert(date,invoicedate) DESC
	End
	Else If @Level = 3
	Begin
		SELECT 
		S.InvoiceDate, 
		I.Id InstanceId, 
		case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, 
		S.InvoiceNo, 
		LTRIM(ISNULL(s.prefix,'') + ISNULL(s.InvoiceSeries, '')) as InvoiceSeries, 
		P.Name PatientName, 
		SUM((SI.Quantity) * ISNULL(PS.PurchasePrice,0)) as PurchasePrice, 
		S.SalesItemAmount As MRPAmount, 
		S.SaleAmount As InvoiceAmount,
		(S.SalesItemAmount-ISNULL(S.TotalDiscountValue,0))-S.GstAmount as InvoiceAmountNoTaxNoDiscount,
		S.GstAmount as TaxAmount,
		ISNULL(S.TotalDiscountValue,0) as DiscountValue,
		0 As IsReturn,
		S.RoundoffSaleAmount AS RoundOffNetAmount
		FROM Sales S WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = S.InstanceId
		INNER JOIN SalesItem SI  WITH(NOLOCK) on S.id= SI.SalesId
		INNER JOIN ProductStock PS  WITH(NOLOCK) on PS.Id=SI.ProductStockId
		LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
		WHERE S.AccountId = @AccountId and S.InstanceId = @InstanceIds
		AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate
		AND S.Cancelstatus is NULL		
		AND 
		(
			ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
			ISNULL(P.Name, '') Like ISNULL(@PatientId, P.Name) + '%'
		)
		AND ISNULL(S.InvoiceSeries,'') = 
		(
			CASE 
			WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
			WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
			END
		)
		AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
		AND SI.GstTotal BETWEEN Isnull(@from_gstTotal, SI.GstTotal) AND Isnull(@to_gstTotal, SI.GstTotal)
		GROUP BY S.InvoiceDate, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end, S.InvoiceNo, LTRIM(ISNULL(s.prefix,'') + ISNULL(s.InvoiceSeries, '')),
		P.Name,S.SalesItemAmount,S.SaleAmount,ISNULL(S.TotalDiscountValue,0),S.GstAmount,S.RoundoffSaleAmount

		UNION ALL
		
		SELECT 
		SR.ReturnDate as InvoiceDate, 
		I.Id InstanceId, 
		case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end InstanceName, 
		SR.ReturnNo as InvoiceNo, 
		LTRIM(ISNULL(SR.prefix,'') + ISNULL(SR.InvoiceSeries, '')) as InvoiceSeries, 
		isNull(P2.Name, P.Name) PatientName, 
		SUM((SRI.Quantity) * ISNULL(PS.PurchasePrice,0)) as PurchasePrice,
		SR.ReturnItemAmount As MRPAmount,
		SR.NetAmount As InvoiceAmount,
		(SR.ReturnItemAmount-ISNULL(SR.TotalDiscountValue,0))-SR.GstAmount as InvoiceAmountNoTaxNoDiscount, 
		SR.GstAmount as TaxAmount,
		ISNULL(SR.TotalDiscountValue,0) as DiscountValue,
		1 As IsReturn,
		SR.RoundOffNetAmount
		FROM SalesReturn SR WITH(NOLOCK)
		INNER JOIN Instance I WITH(NOLOCK) On I.Id = SR.InstanceId
		INNER JOIN SalesReturnItem SRI WITH(NOLOCK) ON SRI.SalesReturnId = SR.Id
		INNER JOIN ProductStock PS WITH(NOLOCK) on PS.Id=SRI.ProductStockId
		LEFT JOIN Sales S WITH(NOLOCK) On SR.SalesId = S.Id
		LEFT JOIN Patient P WITH(NOLOCK) ON P.Id = S.PatientId
		LEFT JOIN Patient P2 WITH(NOLOCK) ON P2.Id = SR.PatientId
		WHERE SR.AccountId = @AccountId AND SR.InstanceId = @InstanceIds
		AND Convert(date,SR.returndate) BETWEEN @StartDate AND @EndDate
		and S.Cancelstatus is NULL
		and ISNULL(sri.IsDeleted,0) != 1
		and ISNULL(sr.CancelType,0) != 2
		AND 
		(
			ISNULL(S.PatientId,'') = ISNULL(@PatientId,ISNULL(S.PatientId,'')) OR
			ISNULL(P.Name, '') Like ISNULL(@PatientId, '') + '%'
			or ISNULL(SR.PatientId,'') = ISNULL(@PatientId,'')
		)
		AND ISNULL(S.InvoiceSeries,'') = 
		(
			CASE 
			WHEN ISNULL(S.InvoiceSeries,'') IN (SELECT A.value('.', 'VARCHAR(20)') FROM @XmlList.nodes('A') AS FN(A)) THEN ISNULL(S.InvoiceSeries,'')
			WHEN @InvoiceSeries = 'ALL' THEN ISNULL(S.InvoiceSeries,'')
			END
		)
		AND ISNULL(S.InvoiceNo,'') = ISNULL(@billNo,ISNULL(S.InvoiceNo,'')) 
		AND sri.GstTotal BETWEEN Isnull(@from_gstTotal, sri.GstTotal) AND Isnull(@to_gstTotal, sri.GstTotal)
		GROUP BY SR.ReturnDate, I.Id, case when ltrim(rtrim(isNull(i.area, ''))) !='' then i.name+','+i.area else i.name end, SR.ReturnNo, LTRIM(ISNULL(SR.prefix,'') + ISNULL(SR.InvoiceSeries, '')),
		isNull(P2.Name, P.Name),SR.ReturnItemAmount,SR.NetAmount,ISNULL(SR.TotalDiscountValue,0),SR.GstAmount,SR.RoundOffNetAmount
		ORDER BY InvoiceSeries desc, InvoiceNo desc
	End
 END