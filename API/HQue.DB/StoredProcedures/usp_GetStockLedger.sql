 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*******************************************************************************                            
** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
**02/06/2017	Poongodi		Import qty taken in 'Import section'
** 16/06/2017	Poongodi		Accepted Qty added for To instance in "In" section (transtype:6)
** 23/06/2017   Violet			Prefix Added
** 13/07/2017   Sarubala		Sales Return Invoice Series Added
** 21/07/2017   Poongodi		ToProductstockid added 
** 12/09/2017   nandhini		current stock code added
** 11/10/2017   Poongodi		As per sundar suggestion Cancel Purchase details removed 
** 26/10/2017   Poongodi		FreeQty added in DC section
** 05/12/2017   Poongodi		Admin Adjusted stock details diplayed
** 25/01/2018   nandhini		date changed invoice to createdat
** 24/04/2018   nandhini		in dc quantity calculation changed
** 23/05/2018   nandhini		type 11(stock adjustment) changed
** 23/10/2018   bikas			Last month Opening Stock Added
** 21/02/2019   Sarubala		Vendor Purchase delete date changed
*******************************************************************************/ 
 --Usage : -- usp_GetStockLedger '003cc71f-26e0-49d4-a1e0-e0a2efd3ebbd','0bb2d317-bdbc-4d99-9e71-b554d78b661a','1900-01-01','2017-01-17','47dd47ae-cb7a-45a8-865e-d22d14a629df'
Create  PROCEDURE [dbo].[usp_GetStockLedger](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime,@ProductId varchar(36))
 AS
 BEGIN
   SET NOCOUNT ON
   select TransactionType, Id, TransactionDate, TransactionNo, Name, ToInstance, Quantity,Quantity, GoodsRcvNo, BillSeries, BatchNo, ExpireDate, Stock, ProductName, StockType,OpeningStock1  from(
	select 1 as 'TransactionType',sa.Id,sa.CreatedAt as 'TransactionDate',ltrim(isnull(SA.Prefix,'')+isnull(SA.InvoiceSeries,'') +' '+ isnull(SA.InvoiceNo,'')) 'TransactionNo',
	sa.Name,null 'ToInstance',si.Quantity,null 'GoodsRcvNo',null 'BillSeries',
	ps.BatchNo 'BatchNo',ps.ExpireDate,0 as Stock,p.Name as 'ProductName',1 as 'StockType',0 as OpeningStock1 from Sales SA
	join SalesItem SI on
	sa.Id=si.SalesId
	join ProductStock ps on ps.Id=si.ProductStockId
	join Product p on p.Id=ps.ProductId
	where sa.AccountId=@AccountId
	and sa.InstanceId=@InstanceId
	and p.id=@ProductId
	and CONVERT(char(10), sa.CreatedAt,126) between @StartDate and @EndDate	

union all

select 2,sr.Id,sr.CreatedAt,isnull(sr.Prefix,'')+' '+isnull(sr.InvoiceSeries,'')+' '+sr.ReturnNo,s.Name,null 'ToInstance',sri.Quantity,null 'GoodsRcvNo',null 'BillSeries',ps.BatchNo 'BatchNo',
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',2 as 'StockType',0 as OpeningStock1 from SalesReturn sr
join SalesReturnItem sri on
sr.Id=sri.SalesReturnId
left join Sales s on s.Id=sr.SalesId
join ProductStock ps on ps.Id=sri.ProductStockId
join Product p on p.Id=ps.ProductId
where sr.AccountId=@AccountId
and sr.InstanceId=@InstanceId
and (sr.CancelType is null or sr.CancelType = 2)
and p.id=@ProductId
and CONVERT(char(10), sr.CreatedAt,126) between @StartDate and @EndDate

union all

select 3,sr.Id,sr.CreatedAt,isnull(sr.Prefix,'')+' '+isnull(sr.InvoiceSeries,'')+' '+sr.ReturnNo,s.Name,null 'ToInstance',sri.Quantity,null 'GoodsRcvNo',null 'BillSeries',ps.BatchNo 'BatchNo',
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',2 as 'StockType',0 as OpeningStock1 from SalesReturn sr
join SalesReturnItem sri on
sr.Id=sri.SalesReturnId
join Sales s on s.Id=sr.SalesId
join ProductStock ps on ps.Id=sri.ProductStockId
join Product p on p.Id=ps.ProductId
where sr.AccountId=@AccountId
and sr.InstanceId=@InstanceId
and sr.CancelType=1
and p.id=@ProductId
and CONVERT(char(10), sr.CreatedAt,126) between @StartDate and @EndDate

union all

select
4 as 'TransactionType',
vp.Id,
vp.CreatedAt as 'TransactionDate',
ltrim(isnull(vp.Prefix,'')+isnull(vp.BillSeries,''))+vp.InvoiceNo 'TransactionNo',
v.Name,
null 'ToInstance',
vpi.Quantity,
vp.GoodsRcvNo,
isnull(vp.BillSeries,'') BillSeries,
ps.BatchNo 'BatchNo',
ps.ExpireDate,0 as Stock,


p.Name as 'ProductName',
2 as 'StockType',0 as OpeningStock1

 from VendorPurchase vp
join VendorPurchaseItem vpi on
vp.Id=vpi.VendorPurchaseId
join ProductStock ps on ps.Id=vpi.ProductStockId
join Product p on p.Id=ps.ProductId
join Vendor v on v.Id=vp.VendorId
where vp.AccountId=@AccountId
and vp.InstanceId=@InstanceId
and p.id=@ProductId
 and isnull(vp.CancelStatus ,0)= 0
and CONVERT(char(10), vp.CreatedAt,126) between @StartDate and @EndDate

union all
select 5 as 'TransactionType',vr.Id,vr.CreatedAt,isnull(vr.Prefix,'')+' '+vr.ReturnNo,v.Name,null 'ToInstance',vri.Quantity,vp.GoodsRcvNo,isnull(vp.BillSeries,'') BillSeries ,ps.BatchNo,ps.ExpireDate,0 as Stock,p.Name as 'ProductName',
1 as 'StockType',0 as OpeningStock1 from VendorReturn vr
join VendorReturnItem vri on
vr.Id = vri.VendorReturnId
join ProductStock ps on ps.Id=vri.ProductStockId
join Product p on p.Id=ps.ProductId
left join VendorPurchase vp on vp.Id=vr.VendorPurchaseId
join Vendor v on v.Id=vr.VendorId
where vr.AccountId=@AccountId
and vr.InstanceId=@InstanceId
and p.id=@ProductId
and isnull(vp.CancelStatus ,0)= 0
and CONVERT(char(10), vr.CreatedAt,126) between @StartDate and @EndDate

union all

select 6 as 'TransactionType',st.id,st.UpdatedAt 'TransferDate',st.TransferNo,ins.Name,null 'ToInstance',
case ST.ToInstanceId when @InstanceId then isnull(sti.AcceptedUnits , sti.Quantity ) else  sti.Quantity end [Quantity]
,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',2 as 'StockType',0 as OpeningStock1 from StockTransfer st
join StockTransferItem sti on st.Id=sti.TransferId
join ProductStock ps on ps.Id= isnull(sti.ToProductStockId,sti.ProductStockId)
join Product p on p.Id=ps.ProductId
join Instance ins on ins.Id=st.FromInstanceId
join Instance ins1 on ins1.Id = st.ToInstanceId
where st.AccountId=@AccountId
--and st.InstanceId=@InstanceId
and p.id=@ProductId
and st.TransferStatus=3
and st.ToInstanceId=@InstanceId
and CONVERT(char(10), st.UpdatedAt,126) between @StartDate and @EndDate

union all

select 7 as 'TransactionType',st.id,st.CreatedAt,st.TransferNo,ins1.Name,null 'ToInstance',sti.Quantity,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',1 as 'StockType',0 as OpeningStock1 from StockTransfer st
join StockTransferItem sti on st.Id=sti.TransferId
join ProductStock ps on ps.Id=sti.ProductStockId
join Product p on p.Id=ps.ProductId
join Instance ins on ins.Id=st.FromInstanceId
join Instance ins1 on ins1.Id = st.ToInstanceId
where st.AccountId=@AccountId
and st.InstanceId=@InstanceId
and p.id=@ProductId
and st.TransferStatus=3
and st.FromInstanceId=@InstanceId
and CONVERT(char(10), st.CreatedAt,126) between @StartDate and @EndDate

union all
select 8 as 'TransactionType',dc.Id,dc.CreatedAt,dc.DCNo 'DcNo',v.Name,null 'ToInstance',( (isnull(dc.PackageQty,0)  + isnull(dc.FreeQty,0))*isnull(dc.PackageSize,0) ) Quantity ,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',2 as 'StockType',0 as OpeningStock1 from DCVendorPurchaseItem dc
join ProductStock ps on ps.Id=dc.ProductStockId
join Product p on p.Id=ps.ProductId
join Vendor v on v.Id = dc.VendorId
where dc.AccountId=@AccountId
and dc.InstanceId=@InstanceId
and p.id=@ProductId
and CONVERT(char(10), dc.CreatedAt,126) between @StartDate and @EndDate
and dc.isActive=1

union all

select 9 as 'TransactionType',tp.Id,tp.CreatedAt,null 'TmpNo',null 'Name',null 'ToInstance',tp.Quantity,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',2 as 'StockType',0 as OpeningStock1 from TempVendorPurchaseItem tp 
join ProductStock ps on ps.Id=tp.ProductStockId
join Product p on p.Id=ps.ProductId
where tp.AccountId=@AccountId
and tp.InstanceId=@InstanceId
and p.id=@ProductId
and CONVERT(char(10), tp.CreatedAt,126) between @StartDate and @EndDate
and tp.isActive=1

union all

select 10 as 'TransactionType',sc.Id,sc.CreatedAt,sc.InvoiceNo,hq.Name,null 'ToInstance',sc.Consumption,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',1 as 'StockType',0 as OpeningStock1 from SelfConsumption sc 
join ProductStock ps on ps.Id=sc.ProductStockId
join Product p on p.Id=ps.ProductId
join HQueUser hq on hq.Id=sc.ConsumedBy
where sc.AccountId=@AccountId
and sc.InstanceId=@InstanceId
and p.id=@ProductId
and CONVERT(char(10), sc.CreatedAt,126) between @StartDate and @EndDate

union all

select 11 as 'TransactionType',sa.Id,sa.CreatedAt,null 'InvoiceNo',hq.Name,null 'ToInstance',ABS(sa.AdjustedStock) as AdjustedStock,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',case when sa.AdjustedStock >= 0 then 2 else 1 end as 'StockType',0 as OpeningStock1  from StockAdjustment sa
join ProductStock ps on sa.ProductStockId=ps.Id
join Product p on p.Id=ps.ProductId
join HQueUser hq on hq.Id=sa.AdjustedBy
where sa.AccountId=@AccountId
and sa.InstanceId=@InstanceId
and p.id=@ProductId
and CONVERT(char(10), sa.CreatedAt,126) between @StartDate and @EndDate


union all

select 12 as 'TransactionType',ps.Id,ps.CreatedAt,ps.NewStockInvoiceNo,null 'Name',null 'ToInstance',ps.NewStockQty,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',2 as 'StockType',0 as OpeningStock1 from ProductStock ps
join Product p on ps.ProductId=p.id
where ps.AccountId=@AccountId
and ps.InstanceId=@InstanceId
and p.id=@ProductId
and ps.NewOpenedStock=1
and CONVERT(char(10), ps.CreatedAt,126) between @StartDate and @EndDate

union all

select 13 as 'TransactionType',ps.Id,ps.CreatedAt,null,null 'Name',null 'ToInstance',isnull(ps.ImportQty,0),null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',2 as 'StockType',0 as OpeningStock1 from ProductStock ps
join Product p on ps.ProductId=p.id
where ps.AccountId=@AccountId
and ps.InstanceId=@InstanceId
and p.id=@ProductId
and ps.StockImport=1
and CONVERT(char(10), ps.CreatedAt,126) between @StartDate and @EndDate

union all

select 14 as 'TransactionType',st.id,st.CreatedAt,st.TransferNo,ins.Name,ins1.Name as 'ToInstance',sti.Quantity,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',3 as 'StockType',0 as OpeningStock1 from StockTransfer st
join StockTransferItem sti on st.Id=sti.TransferId
join ProductStock ps on ps.Id=sti.ProductStockId
join Product p on p.Id=ps.ProductId
join Instance ins on ins.Id=st.FromInstanceId
join Instance ins1 on ins1.Id = st.ToInstanceId
where st.AccountId=@AccountId
--and st.InstanceId=@InstanceId
and p.id=@ProductId
and (st.FromInstanceId=@InstanceId or st.ToInstanceId=@InstanceId)
and st.TransferStatus=1
and CONVERT(char(10), st.CreatedAt,126) between @StartDate and @EndDate

union all

select 15 as 'TransactionType',st.id,st.CreatedAt,st.TransferNo,ins.Name,ins1.Name as 'ToInstance',sti.Quantity,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',4 as 'StockType',0 as OpeningStock1 from StockTransfer st
join StockTransferItem sti on st.Id=sti.TransferId
join ProductStock ps on ps.Id=sti.ProductStockId
join Product p on p.Id=ps.ProductId
join Instance ins on ins.Id=st.FromInstanceId
join Instance ins1 on ins1.Id = st.ToInstanceId
where st.AccountId=@AccountId
--and st.InstanceId=@InstanceId
and p.id=@ProductId
and (st.FromInstanceId=@InstanceId or st.ToInstanceId=@InstanceId)
and st.TransferStatus=2
and CONVERT(char(10), st.CreatedAt,126) between @StartDate and @EndDate
/*
union all
Po cancelled details taken
select 16 as 'TransactionType',vp.Id,vp.InvoiceDate as 'TransactionDate',ltrim(isnull(vp.Prefix,'')+isnull(vp.BillSeries,''))+ vp.InvoiceNo 'TransactionNo',v.Name,null 'ToInstance',vpi.Quantity,vp.GoodsRcvNo,ISNULL(vp.BillSeries,'') BillSeries,
ps.BatchNo 'BatchNo',ps.ExpireDate,0 as Stock,p.Name as 'ProductName',1 as 'StockType' from VendorPurchase vp
join VendorPurchaseItem vpi on
vp.Id=vpi.VendorPurchaseId
join ProductStock ps on ps.Id=vpi.ProductStockId
join Product p on p.Id=ps.ProductId
join Vendor v on v.Id=vp.VendorId
where vp.AccountId=@AccountId
and vp.InstanceId=@InstanceId
and p.id=@ProductId
and isnull(vp.CancelStatus ,0)= 1
and isnull(vpi.Status ,0) <> 2
and CONVERT(char(10), vp.InvoiceDate,126) between @StartDate and @EndDate*/


union all
select     17 as 'TransactionType',
           vp.Id,
           vp.UpdatedAt as 'TransactionDate',
           ltrim(isnull(vp.Prefix,'')+isnull(vp.BillSeries,''))+vp.InvoiceNo 'TransactionNo',
           v.Name,
           null 'ToInstance',
           vpi.Quantity,
           vp.GoodsRcvNo,
		   isnull(vp.BillSeries,'') BillSeries,
           ps.BatchNo 'BatchNo',
           ps.ExpireDate,0 as Stock,
		
           p.Name as 'ProductName', 
           1 as 'StockType' ,0 as OpeningStock1
 
from VendorPurchase vp
join VendorPurchaseItem vpi on vp.Id=vpi.VendorPurchaseId
join ProductStock ps on ps.Id=vpi.ProductStockId
join Product p on p.Id=ps.ProductId
join Vendor v on v.Id=vp.VendorId
where vp.AccountId=@AccountId
and vp.InstanceId=@InstanceId
and p.id=@ProductId
and CONVERT(char(10), vp.CreatedAt,126) between @StartDate and @EndDate
and isnull(vp.CancelStatus ,0)= 0
and vpi.Status = 2


union all

select 18,sr.Id,sr.CreatedAt,isnull(sr.Prefix,'')+' '+isnull(sr.InvoiceSeries,'')+' '+sr.ReturnNo,s.Name,null 'ToInstance',sri.Quantity,null 'GoodsRcvNo',null 'BillSeries',ps.BatchNo 'BatchNo',
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',1 as 'StockType',0 as OpeningStock1 from SalesReturn sr
join SalesReturnItem sri on
sr.Id=sri.SalesReturnId
left join Sales s on s.Id=sr.SalesId
join ProductStock ps on ps.Id=sri.ProductStockId
join Product p on p.Id=ps.ProductId
where sr.AccountId=@AccountId
and sr.InstanceId=@InstanceId
and sr.CancelType is null
and sri.IsDeleted = 1
and p.id=@ProductId
and CONVERT(char(10), sr.CreatedAt,126) between @StartDate and @EndDate

union all

select 19,sr.Id,sr.CreatedAt,isnull(sr.Prefix,'')+' '+isnull(sr.InvoiceSeries,'')+' '+sr.ReturnNo,s.Name,null 'ToInstance',sri.Quantity,null 'GoodsRcvNo',null 'BillSeries',ps.BatchNo 'BatchNo',
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',1 as 'StockType',0 as OpeningStock1 from SalesReturn sr
join SalesReturnItem sri on
sr.Id=sri.SalesReturnId
join Sales s on s.Id=sr.SalesId
join ProductStock ps on ps.Id=sri.ProductStockId
join Product p on p.Id=ps.ProductId
where sr.AccountId=@AccountId

and sr.InstanceId=@InstanceId
and sr.CancelType = 2
and p.id=@ProductId
and CONVERT(char(10), sr.CreatedAt,126) between @StartDate and @EndDate

 
union all

select 11 as 'TransactionType',sa.Id,sa.CreatedAt,null 'InvoiceNo',sa.adjustedby +'by admin'  Name,null 'ToInstance',ABS(sa.AdjustedStock) as AdjustedStock,null 'GoodsRcvNo',null 'BillSeries', ps.BatchNo,
ps.ExpireDate,0 as Stock,p.Name as 'ProductName',case when sa.AdjustedStock >= 0 then 2 else 1 end as 'StockType',0 as OpeningStock1  from StockAdjustment sa
join ProductStock ps on sa.ProductStockId=ps.Id
join Product p on p.Id=ps.ProductId
 and sa.updatedby ='admin'
where sa.AccountId=@AccountId
and sa.InstanceId=@InstanceId
and p.id=@ProductId
and CONVERT(char(10), sa.CreatedAt,126) between @StartDate and @EndDate


union all 
select 20 TransactionType, '' Id, GETDATE() TransactionDate, '' TransactionNo, '' Name, null ToInstance, 0 Quantity, null GoodsRcvNo, null BillSeries, '' BatchNo, GETDATE() ExpireDate, sum(Stock) Stock,'' ProductName, 0 StockType,(
select  sum(stock) from ProductStock where ProductId=@ProductId AND CreatedAt between CreatedAt and dateadd(dd,-1, cast(@StartDate as date))) as OpeningStock1
from ProductStock where ProductId=@ProductId and AccountId=@AccountId and InstanceId=@InstanceId

) as SalesTran order by TransactionDate asc

END
