
/**   
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 19/05/2017 Poongodi			Hardcoded filter value removed			
 
*******************************************************************************/ 



Create   PROCEDURE [dbo].[usp_GetProductList]
(@InstanceId varchar(36),
@AccountId varchar(36),
@type varchar(10))
AS
 BEGIN
  
   SET NOCOUNT ON

 
     IF (@type ='All') 
		Select @InstanceId  =NULL 
		 
  

    Select distinct isnull(p.Name,'')Name,isnull(p.Manufacturer,'')Manufacturer,isnull(p.GenericName,'')GenericName,isnull(p.Category,'')Category,
isnull(isnull((select top 1 pss1.packagesize from productstock pss1 where pss1.productid=p.id order by pss1.createdat desc),
(select top 1 vpi.PackageSize from VendorPurchaseItem vpi inner join productstock pss on vpi.ProductStockId=pss.id where pss.productid=p.id order by pss.createdat desc)),1)
PackageSize
from product as p


inner join ProductStock as ps on p.id=ps.ProductId and ps.AccountId=@AccountId
Where p.AccountId  =@AccountId
 and  ps.instanceid = isnull(@InstanceId, ps.instanceid )
and isnull(p.Status ,0) in (0,1)
order by name asc
END
GO


