 
/**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 18/04/2017	Poongodi		Return Item duplicate Issue fixed
** 23/06/2017	Poongodi		Purchase Return Join changed 
** 03/07/2017	Sarubala		GST calculation changed
*******************************************************************************/ 
--Usage : -- usp_GetDashboardPurchase '013513b1-ea8c-4ea8-9fed-054b260ee197', '18204879-99ff-4efd-b076-f85b4a0da0a3'
CREATE PROCEDURE [dbo].[usp_GetDashboardPurchase](@AccountId VARCHAR(36), @InstanceId VARCHAR(36) = '')
 AS
 BEGIN
   SET NOCOUNT ON

   IF @InstanceId != ''
   BEGIN

select Name, sum(Amount)Amount, sum(PurchaseReturnAmount)PurchaseReturnAmount from (
                SELECT Name,Sum(Amount) as Amount,0 as PurchaseReturnAmount from (SELECT 'Today' as Name,
				--Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
    --            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
    --            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
    --            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
    --            * (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end ))) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
				ROUND(SUM(vi.PurchasePrice*vi.Quantity) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END),0) as Amount,
                '0.00' as PurchaseReturnAmount
                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId
				 inner join ProductStock ps on vi.ProductStockId=ps.id 
				 left join Vendor on Vendor.Id=v.VendorId 
                where v.AccountId =@AccountId AND v.InstanceId=@InstanceId
				and isnull(v.CancelStatus ,0) =0
                AND Convert(date,v.CreatedAt)  = dateadd(day,datediff(day,0,GETDATE()),0)
				and (vi.Status is null or vi.Status = 1)
				 group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a group by a.name



	
                union




	
                SELECT 'Today' as Name,'0.00' as Amount,
                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,isnull(VendorPurchaseItem.PurchasePrice,0)) * VendorReturnItem.Quantity)))),0)  as PurchaseReturnAmount
                FROM VendorReturnItem
				INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
				Left JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
				Left JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
				INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
				left JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
				and VendorPurchaseItem.VendorPurchaseId = VendorPurchase.Id
				INNER JOIN Product ON Product.Id = ProductStock.ProductId
                where VendorReturn.AccountId =@AccountId AND VendorReturn.InstanceId=@InstanceId
					and isnull(VendorPurchase.CancelStatus ,0) =0 and (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0)  -- Condition added by Gavaskar 14-09-2017
                AND Convert(date,VendorReturn.ReturnDate)  = dateadd(day,datediff(day,0,GETDATE()),0) 
				and isnull(VendorPurchaseItem.Status,0) in (0,1) 
                ) Today group by Name    




                union all



                select Name, sum(Amount)Amount, sum(PurchaseReturnAmount)PurchaseReturnAmount from (
                SELECT Name,Sum(Amount) as Amount,0 as PurchaseReturnAmount from (SELECT 'Yesterday' as Name,
				--Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
    --            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
    --            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
    --            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
    --            *  (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end )))+ (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
				ROUND(SUM(vi.PurchasePrice*vi.Quantity) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END),0) as Amount,
                '0.00' as PurchaseReturnAmount
                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
				inner join ProductStock ps on vi.ProductStockId=ps.id 
				left join Vendor on Vendor.Id=v.VendorId 
                where v.AccountId =@AccountId AND v.InstanceId=@InstanceId 
				and isnull(v.CancelStatus ,0) = 0
                AND Convert(date,v.CreatedAt)  = dateadd(day,datediff(day,1,GETDATE()),0)
				and (vi.Status is null or vi.Status = 1)
				 group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a 
				 group by a.name
	
                union
	
                SELECT 'Yesterday' as Name,'0.00' as Amount,
                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,isnull(VendorPurchaseItem.PurchasePrice,0)) * VendorReturnItem.Quantity)))),0)  as PurchaseReturnAmount
               FROM VendorReturnItem
INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
Left JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
Left JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
left JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
and VendorPurchaseItem.VendorPurchaseId = VendorPurchase.Id
INNER JOIN Product ON Product.Id = ProductStock.ProductId
                where VendorReturn.AccountId =@AccountId AND VendorReturn.InstanceId=@InstanceId
					and isnull(VendorPurchase.CancelStatus ,0) =0 and (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0)  -- Condition added by Gavaskar 14-09-2017
                AND Convert(date,VendorReturn.ReturnDate)  = dateadd(day,datediff(day,1,GETDATE()),0) 
				and isnull(VendorPurchaseItem.Status,0) in (0,1) 
                ) Yesterday group by Name  

                union all

                select Name, sum(Amount)Amount, sum(PurchaseReturnAmount)PurchaseReturnAmount from (
                SELECT Name,Sum(Amount) as Amount,0 as PurchaseReturnAmount from (SELECT 'Last Month' as Name,
				--Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
    --            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
    --            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
    --            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
    --            *  (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end )))+ (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
				ROUND(SUM(vi.PurchasePrice*vi.Quantity) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END),0) as Amount,
                '0.00' as PurchaseReturnAmount
                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
				inner join ProductStock ps on vi.ProductStockId=ps.id 
				left join Vendor on Vendor.Id=v.VendorId 
                where v.AccountId =@AccountId AND v.InstanceId=@InstanceId
					and isnull(v.CancelStatus ,0) =0
                AND Convert(date,v.CreatedAt)  between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))
				and (vi.Status is null or vi.Status = 1) 
				group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) 
a group by a.name
	
                union
	
                SELECT 'Last Month' as Name,'0.00' as Amount,
               Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,isnull(VendorPurchaseItem.PurchasePrice,0)) * VendorReturnItem.Quantity)))),0)  as PurchaseReturnAmount
                FROM VendorReturnItem
 INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
Left JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
Left JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
left JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
and VendorPurchaseItem.VendorPurchaseId = VendorPurchase.Id
INNER JOIN Product ON Product.Id = ProductStock.ProductId
                where VendorReturn.AccountId =@AccountId AND VendorReturn.InstanceId=@InstanceId 
					and isnull(VendorPurchase.CancelStatus ,0) =0 and (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0)  -- Condition added by Gavaskar 14-09-2017
                AND Convert(date,VendorReturn.ReturnDate)   between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))
				and isnull(VendorPurchaseItem.Status,0) in (0,1) 
                ) LastMonth group by Name  

                union all

                select Name, sum(Amount)Amount, sum(PurchaseReturnAmount)PurchaseReturnAmount from (
                SELECT Name,Sum(Amount) as Amount,0 as PurchaseReturnAmount from (SELECT 'Financial Year' as Name,
				--Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
    --            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
    --            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
    --            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
    --            *  (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end )))+ (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
				ROUND(SUM(vi.PurchasePrice*vi.Quantity) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END),0) as Amount,
                '0.00' as PurchaseReturnAmount
                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
				inner join ProductStock ps on vi.ProductStockId=ps.id 
				left join Vendor on Vendor.Id=v.VendorId 
                where v.AccountId =@AccountId AND v.InstanceId=@InstanceId 
					and isnull(v.CancelStatus ,0) =0
                AND Convert(date,v.CreatedAt)  between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))
                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))))

				and (vi.Status is null or vi.Status = 1)


				 group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a 
 group by a.name
	
                union
	
                SELECT 'Financial Year' as Name,'0.00' as Amount,
                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,isnull(VendorPurchaseItem.PurchasePrice,0)) * VendorReturnItem.Quantity)))),0)  as PurchaseReturnAmount
                FROM VendorReturnItem
 INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
Left JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
Left JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
left JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
and VendorPurchaseItem.VendorPurchaseId = VendorPurchase.Id
INNER JOIN Product ON Product.Id = ProductStock.ProductId
                where VendorReturn.AccountId =@AccountId AND VendorReturn.InstanceId=@InstanceId  
					and isnull(VendorPurchase.CancelStatus ,0) =0 and (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0)  -- Condition added by Gavaskar 14-09-2017
                AND Convert(date,VendorReturn.ReturnDate)  between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate()
 ))+1 )))
                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))))


					and isnull(VendorPurchaseItem.Status,0) in (0,1) 

                ) FinancialYear
				 group by Name
	END
	ELSE
	BEGIN
	select Name, sum(Amount)Amount, sum(PurchaseReturnAmount)PurchaseReturnAmount from (
                SELECT Name,Sum(Amount) as Amount,0 as PurchaseReturnAmount from (SELECT 'Today' as Name,
				--Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
    --            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
    --            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
    --            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
    --            *  (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end ))) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
				ROUND(SUM(vi.PurchasePrice*vi.Quantity) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END),0) as Amount,
                '0.00' as PurchaseReturnAmount
                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
				inner join ProductStock ps on vi.ProductStockId=ps.id 
				left join Vendor on Vendor.Id=v.VendorId 
                where v.AccountId =@AccountId
					and isnull(v.CancelStatus ,0) =0
                AND Convert(date,v.CreatedAt)  = dateadd(day,datediff(day,0,GETDATE()),0)


				and(vi.Status is null or vi.Status = 1)
				
				 group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a 
				 group by a.name
	
                union
	
                SELECT 'Today' as Name,'0.00' as Amount,
                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,isnull(VendorPurchaseItem.PurchasePrice,0)) * VendorReturnItem.Quantity)))),0)  as PurchaseReturnAmount
                FROM VendorReturnItem
				INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
				Left JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
				Left JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
				INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
				left JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
				and VendorPurchaseItem.VendorPurchaseId = VendorPurchase.Id
				INNER JOIN Product ON Product.Id = ProductStock.ProductId
                where VendorPurchase.AccountId =@AccountId
					and isnull(VendorPurchase.CancelStatus ,0) =0 and (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0)  -- Condition added by Gavaskar 14-09-2017
                AND Convert(date,VendorReturn.ReturnDate)  = dateadd(day,datediff(day,0,GETDATE()),0) 

				and isnull(VendorPurchaseItem.Status,0) in (0,1) 


                ) Today group by Name    

                union all

                select Name, sum(Amount)Amount, sum(PurchaseReturnAmount)PurchaseReturnAmount from (
                SELECT Name,Sum(Amount) as Amount,0 as PurchaseReturnAmount from (SELECT 'Yesterday' as Name,
				--Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
    --            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
    --            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
    --            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
    --            *  (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end )))+ (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
				ROUND(SUM(vi.PurchasePrice*vi.Quantity) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END),0) as Amount,
                '0.00' as PurchaseReturnAmount
                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId inner join ProductStock ps
                on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
                where v.AccountId =@AccountId
					and isnull(v.CancelStatus ,0) =0
                AND Convert(date,v.CreatedAt)  = dateadd(day,datediff(day,1,GETDATE()),0) 

				and (vi.Status is null or vi.Status = 1)

				group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a 
				group by a.name
	
                union
	
                SELECT 'Yesterday' as Name,'0.00' as Amount,
                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,isnull(VendorPurchaseItem.PurchasePrice,0)) * VendorReturnItem.Quantity)))),0)  as PurchaseReturnAmount
               FROM VendorReturnItem
				INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
				Left JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
				Left JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
				INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
				left JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
				and VendorPurchaseItem.VendorPurchaseId = VendorPurchase.Id
				INNER JOIN Product ON Product.Id = ProductStock.ProductId
                where VendorPurchase.AccountId =@AccountId
					and isnull(VendorPurchase.CancelStatus ,0) =0 and (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0)  -- Condition added by Gavaskar 14-09-2017
                AND Convert(date,VendorReturn.ReturnDate)  = dateadd(day,datediff(day,1,GETDATE()),0)
				
				
				and isnull(VendorPurchaseItem.Status,0) in (0,1) 
				 
                ) Yesterday group by Name  

                union all

                select Name, sum(Amount)Amount, sum(PurchaseReturnAmount)PurchaseReturnAmount from (
                SELECT Name,Sum(Amount) as Amount,0 as PurchaseReturnAmount from (SELECT 'Last Month' as Name,
				--Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
    --            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
    --            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
    --            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
    --            *  (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end )))+ (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
				ROUND(SUM(vi.PurchasePrice*vi.Quantity) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END),0) as Amount,
                '0.00' as PurchaseReturnAmount
                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId inner join ProductStock ps
                on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
                where v.AccountId =@AccountId
					and isnull(v.CancelStatus ,0) =0
                AND Convert(date,v.CreatedAt)  between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))
				
				
				and (vi.Status is null or vi.Status = 1)
				 
				group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a
			    group by a.name
	
                union
	
                SELECT 'Last Month' as Name,'0.00' as Amount,
               Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,isnull(VendorPurchaseItem.PurchasePrice,0)) * VendorReturnItem.Quantity)))),0)  as PurchaseReturnAmount
                FROM VendorReturnItem
				INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
				Left JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
				Left JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
				INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
				left JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
				and VendorPurchaseItem.VendorPurchaseId = VendorPurchase.Id
				INNER JOIN Product ON Product.Id = ProductStock.ProductId
                where VendorPurchase.AccountId =@AccountId
					and isnull(VendorPurchase.CancelStatus ,0) =0 and (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0)  -- Condition added by Gavaskar 14-09-2017
                AND Convert(date,VendorReturn.ReturnDate)   between  CONVERT(date,DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()), 0))) and CONVERT(date,DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))

					and isnull(VendorPurchaseItem.Status,0) in (0,1) 


                ) LastMonth group by Name  

                union all

                select Name, sum(Amount)Amount, sum(PurchaseReturnAmount)PurchaseReturnAmount from (
                SELECT Name,Sum(Amount) as Amount,0 as PurchaseReturnAmount from (SELECT 'Financial Year' as Name,
				--Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
    --            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
    --            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
    --            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
    --            *  (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end )))+ (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END))),0) as Amount,
				ROUND(SUM(vi.PurchasePrice*vi.Quantity) + (CASE WHEN v.NoteType='credit' THEN -(v.NoteAmount) WHEN v.NoteType='debit' THEN v.NoteAmount else 0 END),0) as Amount,
                '0.00' as PurchaseReturnAmount
                FROM VendorPurchase v inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId inner join ProductStock ps
                on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
                where v.AccountId =@AccountId
					and isnull(v.CancelStatus ,0) =0
                AND Convert(date,v.CreatedAt)  between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))
                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))))


				and (vi.Status is null or vi.Status = 1)



				 group by v.InvoiceNo,v.InvoiceDate,v.NoteAmount,v.NoteType) a
				 group by a.name
	
                union
	
                SELECT 'Financial Year' as Name,'0.00' as Amount,
                Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,isnull(VendorPurchaseItem.PurchasePrice,0)) * VendorReturnItem.Quantity)))),0)  as PurchaseReturnAmount
                FROM VendorReturnItem
				INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
				Left JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
				Left JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
				INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
				left JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
				and VendorPurchaseItem.VendorPurchaseId = VendorPurchase.Id
				INNER JOIN Product ON Product.Id = ProductStock.ProductId
                where VendorPurchase.AccountId =@AccountId
					and isnull(VendorPurchase.CancelStatus ,0) =0 and (vendorreturnitem.IsDeleted is null or vendorreturnitem.IsDeleted=0)  -- Condition added by Gavaskar 14-09-2017
                AND Convert(date,VendorReturn.ReturnDate)  between  CONVERT(date,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate()
 ))+1 )))
                and convert(date,DATEADD(SS,-1,DATEADD(mm,12,DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 )))))



				and isnull(VendorPurchaseItem.Status,0) in (0,1) 


                ) FinancialYear group by Name
	END
END
 
