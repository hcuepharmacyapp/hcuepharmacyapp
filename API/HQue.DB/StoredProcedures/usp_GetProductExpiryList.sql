  --Usage : -- usp_GetProductExpiryList '11e9fe4e-6dc8-46ec-bd7b-637670508a4b','1900-01-01','2016-11-01'
  CREATE PROCEDURE [dbo].[usp_GetProductExpiryList](@InstanceId varchar(36),@StartDate datetime,@EndDate datetime)
 AS
 BEGIN
   SET NOCOUNT ON
     SELECT P.Name Product,V.Name AS Vendor,ABS(SUM(PS.Stock)) AS Stock,
	 PS.ExpireDate AS ExpireDate,PS.Id,PS.InstanceId 
	 --,@InstanceId idchk
	 FROM ProductStock PS WITH(NOLOCK)
    INNER JOIN Product P WITH(NOLOCK) ON P.Id = PS.ProductId 
	LEFT OUTER JOIN Vendor V WITH(NOLOCK) ON V.Id = PS.VendorId
    WHERE PS.InstanceId = @InstanceId
	 --AND PS.ExpireDate<=Getdate() 
	 and cast(ps.ExpireDate as date) between cast( @StartDate as date) and cast(@EndDate as date)
	 and PS.Stock>0
    GROUP BY P.Name,V.Name,ExpireDate,PS.Id,PS.InstanceId
    ORDER BY ExpireDate
END
 


