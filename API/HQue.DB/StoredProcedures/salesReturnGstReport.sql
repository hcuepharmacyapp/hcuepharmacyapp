
Create PROCEDURE [dbo].[salesReturnGstReport](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime )
 AS

select a.ReturnDate, a.ReturnNo, sum(a.GrossAmount) ReturnAmount, sum(a.GstAmount) GstAmount, sum(a.Gst0_ReturnValue) Gst0_ReturnValue, sum(a.Gst0_Value) Gst0_Value, sum(a.Gst0_TotalReturnValue) Gst0_TotalReturnValue, sum(a.Gst5_ReturnValue) Gst5_ReturnValue, sum(a.Gst5_Value) Gst5_Value, sum(a.Gst5_TotalReturnValue) Gst5_TotalReturnValue, sum(a.Gst12_ReturnValue) Gst12_ReturnValue, sum(a.Gst12_Value) Gst12_Value, sum(a.Gst12_TotalReturnValue) Gst12_TotalReturnValue, sum(a.Gst18_ReturnValue) Gst18_ReturnValue, sum(a.Gst18_Value) Gst18_Value, sum(a.Gst18_TotalReturnValue) Gst18_TotalReturnValue, sum(a.Gst28_ReturnValue) Gst28_ReturnValue, sum(a.Gst28_Value) Gst28_Value, sum(a.Gst28_TotalReturnValue) Gst28_TotalReturnValue, sum(a.GstOther_ReturnValue) GstOther_ReturnValue, sum(a.GstOther_Value) GstOther_Value, sum(a.GstOther_TotalReturnValue) GstOther_TotalReturnValue from

(select sr.ReturnDate, (isnull(sr.Prefix,'') + isnull(sr.InvoiceSeries,'') + sr.ReturnNo) ReturnNo, (sri.TotalAmount-sri.GstAmount) ReturnAmount, sri.GstAmount,(sri.TotalAmount) GrossAmount,
case isnull(sri.GstTotal,0) when 0 then ((sri.TotalAmount-sri.GstAmount)) else 0 end Gst0_ReturnValue,
case isnull(sri.GstTotal,0) when 0 then sri.GstAmount else 0 end Gst0_Value,
case isnull(sri.GstTotal,0) when 0 then sri.TotalAmount else 0 end Gst0_TotalReturnValue,
case isnull(sri.GstTotal,0) when 5 then ((sri.TotalAmount-sri.GstAmount)) else 0 end Gst5_ReturnValue,
case isnull(sri.GstTotal,0) when 5 then sri.GstAmount else 0 end Gst5_Value,
case isnull(sri.GstTotal,0) when 5 then sri.TotalAmount else 0 end Gst5_TotalReturnValue,
case isnull(sri.GstTotal,0) when 12 then ((sri.TotalAmount-sri.GstAmount)) else 0 end Gst12_ReturnValue,
case isnull(sri.GstTotal,0) when 12 then sri.GstAmount else 0 end Gst12_Value,
case isnull(sri.GstTotal,0) when 12 then sri.TotalAmount else 0 end Gst12_TotalReturnValue,
case isnull(sri.GstTotal,0) when 18 then ((sri.TotalAmount-sri.GstAmount)) else 0 end Gst18_ReturnValue,
case isnull(sri.GstTotal,0) when 18 then sri.GstAmount else 0 end Gst18_Value,
case isnull(sri.GstTotal,0) when 18 then sri.TotalAmount else 0 end Gst18_TotalReturnValue,
case isnull(sri.GstTotal,0) when 28 then ((sri.TotalAmount-sri.GstAmount)) else 0 end Gst28_ReturnValue,
case isnull(sri.GstTotal,0) when 28 then sri.GstAmount else 0 end Gst28_Value,
case isnull(sri.GstTotal,0) when 28 then sri.TotalAmount else 0 end Gst28_TotalReturnValue,
case when isnull(sri.GstTotal,0) not in (0,5,12,18,28) then ((sri.TotalAmount-sri.GstAmount)) else 0 end GstOther_ReturnValue,
case when isnull(sri.GstTotal,0) not in (0,5,12,18,28) then sri.GstAmount else 0 end GstOther_Value,
case when isnull(sri.GstTotal,0) not in (0,5,12,18,28) then sri.TotalAmount else 0 end GstOther_TotalReturnValue

from SalesReturn sr with(nolock) inner join 
SalesReturnItem sri with(nolock) on sr.Id=sri.SalesReturnId
where sr.AccountId=@AccountId and sr.InstanceId=@InstanceId and sr.CreatedAt between @StartDate and @EndDate and isnull(sr.CancelType,0) != 1 ) a
group by a.ReturnDate, a.ReturnNo
order by a.ReturnDate desc

