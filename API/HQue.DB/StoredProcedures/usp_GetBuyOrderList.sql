/**   
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 21/06/2017 Poongodi			Prefix added
 ** 05/07/2017 Sarubala			GST Calculation added
 ** 13/09/2017   Lawrence		All branch condition Added 
*******************************************************************************/ 
 --Usage : -- usp_GetBuyOrderList '0020ccad-e432-4ec5-8927-e9302db2340a','d5baa673-d6e4-4e3e-a593-cdd6313be32c','1900-01-01','2016-11-01'
CREATE PROCEDURE [dbo].[usp_GetBuyOrderList](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime)
 AS
 BEGIN
   SET NOCOUNT ON
   IF @InstanceId != ''
   BEGIN
select distinct i.Name as InstanceName, vo.Id,vo.VendorId,isnull(vo.Prefix,'')+ vo.OrderId [OrderId],vo.OrderDate,
CAST(isnull(vo.TaxRefNo,0) as int) TaxRefNo,voi.ProductId,voi.Quantity
,ISNULL(v.Name,'') Name
,ISNULL(v.Address,'') Address
,ISNULL(v.Area,'') Area
,ISNULL(v.City,'') City,ISNULL(v.DrugLicenseNo,'') DrugLicenseNo,ISNULL(v.TinNo,'') TinNo,
ISNULL(v.Mobile,'') Mobile,ISNULL(v.Email,'') Email
,ISNULL(p.Name,'') 'ProductName'
,case isnull(ps.Id,'') when '' then null else cast( ISNULL(voi.FreeQty,0.00) as numeric(20,2)) end FreeQty
,case isnull(ps.Id,'') when '' then null else ISNULL(VPIDiscount,0) end Discount
,case isnull(ps.Id,'') when '' then null else ISNULL(ps.VAT,0) end VAT
,case isnull(ps.Id,'') when '' then null else ISNULL(ps.CST,0) end CST
,case isnull(voi.Id,'') when '' then null else ISNULL(voi.GstTotal,isnull(ps.GstTotal,0)) end GstTotal
,case isnull(voi.Id,'') when '' then null else ISNULL(voi.Cgst,isnull(ps.Cgst,0)) end Cgst
,case isnull(voi.Id,'') when '' then null else ISNULL(voi.Sgst,isnull(ps.Sgst,0)) end Sgst
,case isnull(voi.Id,'') when '' then null else ISNULL(voi.Igst,isnull(ps.Igst,0)) end Igst
,case isnull(ps.Id,'') when '' then null else ISNULL(ps.PackagePurchasePrice,isnull(VPIPackagePurchasePrice,0)) end PackagePurchasePrice
,case isnull(ps.Id,'') when '' then null else ISNULL(PackageSellingPrice,isnull(VPIPackageSellingPrice,0)) end PackageSellingPrice
,case isnull(ps.Id,'') when '' then null else ISNULL(ps.PackageSize,isnull(VPIPackageSize,0)) end PackageSize,
case  isnull(ps.Id,'') when '' then 2 else 1 end as Status 
from VendorOrder vo (NOLOCK) 
join VendorOrderItem voi (NOLOCK) on vo.Id=voi.VendorOrderId
join Vendor v (NOLOCK) on vo.VendorId=v.Id
join Product p (NOLOCK) on voi.ProductId=p.Id

left join (select ps.CreatedAt,ps.VAT,ps.CST,ps.ProductId,ps.Id,ps.PackagePurchasePrice,ps.PackageSize,
ISNULL(ps.PackageSize,0) * ISNULL(ps.SellingPrice,0) 'PackageSellingPrice',ps.GstTotal,ps.Cgst,ps.Sgst,ps.Igst  from ProductStock ps 
inner join (select max(CreatedAt) CreatedAt ,ProductId from 
ProductStock (NOLOCK)
where InstanceId=ISNULL(@InstanceId,InstanceId)
and AccountId=@AccountId
group by ProductId
) As pdt on pdt.CreatedAt =ps.CreatedAt and pdt.ProductId=ps.ProductId where  ps.InstanceId=ISNULL(@InstanceId,ps.InstanceId) and ps.AccountId=@AccountId)as PS on PS.ProductId=p.Id
Join Instance i (NOLOCK) on i.id = vo.InstanceId
left join (select cast(MAX(FreeQty) as numeric(20,2))FreeQty,max(Discount) VPIDiscount,cast(CreatedAt as date) as CreatedAt,ProductStockId,MAX(PackagePurchasePrice) 'VPIPackagePurchasePrice',MAX(PackageSize) 'VPIPackageSize',MAX(PackageSellingPrice) 'VPIPackageSellingPrice' from 
VendorPurchaseItem (NOLOCK)
where InstanceId=ISNULL(@InstanceId,InstanceId) 
and AccountId=@AccountId and ISNULL(FreeQty,0)>0
group by cast(CreatedAt as date),ProductStockId) as VPI on vpi.ProductStockId=PS.Id

where vo.InstanceId=ISNULL(@InstanceId,vo.InstanceId)
and vo.AccountId=@AccountId
and Convert(date,vo.OrderDate) between @StartDate and @EndDate

order by I.Name, [Status] asc,vo.OrderDate,isnull(vo.Prefix,'')+ vo.OrderId  desc

END
		   ELSE
	BEGIN
		select distinct i.Name as InstanceName, vo.Id,vo.VendorId,isnull(vo.Prefix,'')+ vo.OrderId [OrderId],vo.OrderDate,CAST(isnull(vo.TaxRefNo,0) as int) TaxRefNo,voi.ProductId,voi.Quantity
		,ISNULL(v.Name,'') Name
		,ISNULL(v.Address,'') Address
		,ISNULL(v.Area,'') Area
		,ISNULL(v.City,'') City,ISNULL(v.DrugLicenseNo,'') DrugLicenseNo,ISNULL(v.TinNo,'') TinNo,
		ISNULL(v.Mobile,'') Mobile,ISNULL(v.Email,'') Email
		,ISNULL(p.Name,'') 'ProductName'
		,case isnull(ps.Id,'') when '' then null else cast( ISNULL(voi.FreeQty,0.00) as numeric(20,2)) end FreeQty
		,case isnull(ps.Id,'') when '' then null else ISNULL(VPIDiscount,0) end Discount
		,case isnull(ps.Id,'') when '' then null else ISNULL(ps.VAT,0) end VAT
		,case isnull(ps.Id,'') when '' then null else ISNULL(ps.CST,0) end CST
		,case isnull(voi.Id,'') when '' then null else ISNULL(voi.GstTotal,isnull(ps.GstTotal,0)) end GstTotal
		,case isnull(voi.Id,'') when '' then null else ISNULL(voi.Cgst,isnull(ps.Cgst,0)) end Cgst
		,case isnull(voi.Id,'') when '' then null else ISNULL(voi.Sgst,isnull(ps.Sgst,0)) end Sgst
		,case isnull(voi.Id,'') when '' then null else ISNULL(voi.Igst,isnull(ps.Igst,0)) end Igst
		,case isnull(ps.Id,'') when '' then null else ISNULL(ps.PackagePurchasePrice,isnull(VPIPackagePurchasePrice,0)) end PackagePurchasePrice
		,case isnull(ps.Id,'') when '' then null else ISNULL(PackageSellingPrice,isnull(VPIPackageSellingPrice,0)) end PackageSellingPrice
		,case isnull(ps.Id,'') when '' then null else ISNULL(ps.PackageSize,isnull(VPIPackageSize,0)) end PackageSize,
		case  isnull(ps.Id,'') when '' then 2 else 1 end as Status 
		from VendorOrder vo (NOLOCK)
		join VendorOrderItem voi (NOLOCK) on vo.Id=voi.VendorOrderId
		join Vendor v (NOLOCK) on vo.VendorId=v.Id
		join Product p (NOLOCK) on voi.ProductId=p.Id

		left join (select ps.CreatedAt,ps.VAT,ps.CST,ps.ProductId,ps.Id,ps.PackagePurchasePrice,ps.PackageSize,
		ISNULL(ps.PackageSize,0) * ISNULL(ps.SellingPrice,0) 'PackageSellingPrice',ps.GstTotal,ps.Cgst,ps.Sgst,ps.Igst from				ProductStock ps (NOLOCK)
		inner join (select max(CreatedAt) CreatedAt ,ProductId from 
		ProductStock (NOLOCK) 
		where AccountId=@AccountId
		group by ProductId
		) As pdt on pdt.CreatedAt =ps.CreatedAt and pdt.ProductId=ps.ProductId where  ps.AccountId=@AccountId)as PS on PS.ProductId=p.Id
		Join Instance i (NOLOCK) on i.Id = vo.InstanceId
		left join (select cast(MAX(FreeQty) as numeric(20,2))FreeQty,max(Discount) VPIDiscount,cast(CreatedAt as date) as CreatedAt,ProductStockId,MAX(PackagePurchasePrice) 'VPIPackagePurchasePrice',MAX(PackageSize) 'VPIPackageSize',MAX(PackageSellingPrice) 'VPIPackageSellingPrice' from 
		VendorPurchaseItem (NOLOCK)
		where AccountId=@AccountId and ISNULL(FreeQty,0)>0
		group by cast(CreatedAt as date),ProductStockId) as VPI on vpi.ProductStockId=PS.Id

		where vo.AccountId=@AccountId
		and Convert(date,vo.OrderDate) between @StartDate and @EndDate

		order by I.Name, [Status] asc,vo.OrderDate,isnull(vo.Prefix,'')+ vo.OrderId desc
	END
           
 END
 