﻿--exec usp_RefreshOrderDraft 
--@InstanceId=N'3e2ec066-1551-4527-be53-5aa3c5b7fb7d',
--@AccountId=N'c503ca6e-f418-4807-a847-b6886378cf0b',
--@fromDate=N'2017-08-07',
--@ToDate=N'2017-08-07',
--@ProductIds=N'''40598DEF-7A9C-41DE-A6DB-E5A9FA5F277D'',''1800d91d-8561-4ad1-80cf-dc10255ebb17'',''86487ed6-42b5-43db-b598-7806e8a932e0''',
--@type=7
CREATE procedure [dbo].[usp_RefreshOrderDraft](@accountid char(36),@instanceid char(36),@fromDate date,@Todate date,@ProductIds VARCHAR(MAX),@type tinyint)
as
begin	

declare @Tmp table(productid char(36),soldqty decimal(18,2),stock decimal(18,2))

declare @salesTmp table (productid char(36),soldqty decimal(18,2))
declare @transferTmp table (productid char(36),soldqty decimal(18,2))
declare @missedSalesTmp table (productid char(36),soldqty decimal(18,2))
declare @indentTmp table (productid char(36),soldqty decimal(18,2))

DECLARE @XmlList XML
SET @XmlList = CAST(('<A>'+REPLACE(REPLACE(ISNULL(@ProductIds,''),',','</A><A>'),'''','')+'</A>') AS XML)

/*
-- Order generation done by various option - Settu
-- -----------------------------------------------
@type = 1 - By Sales
@type = 2 - By Transfer
@type = 3 - By Missed Sales
@type = 4 - By Sales & By Transfer
@type = 5 - By Sales & By Missed Sales
@type = 6 - By Transfer & By Missed Sales
@type = 7 - By Sales, By Transfer & By Missed Sales
@type = 8 - By Indent Qty
@type = 9 - By Sales, By Transfer, By Indent Qty & By Missed Sales
@type = 10 - By Sales, By Transfer & By Indent Qty 
@type = 11 - By Sales, By Indent Qty & By Missed Sales
@type = 12 - By Transfer, By Indent Qty & By Missed Sales
@type = 13 - By Sales & By Indent Qty
@type = 14 - By Transfer & By Indent Qty
@type = 15 - By Indent Qty & By Missed Sales
*/
If @type = 1 OR @type = 4 OR @type = 5 OR @type = 7 OR @type = 9 OR @type = 10 OR @type = 11 OR @type = 13
Begin
	insert into @salesTmp(productid,soldqty)
	select ps.ProductId,sum(si.Quantity-ISNULL(SR.ReturnQty,0))
	from Sales s inner join SalesItem as si 
		on s.id = si.SalesId
		inner join ProductStock as ps on si.ProductStockId = ps.Id
		LEFT JOIN 
		(
			SELECT SR.SalesId,SRI.ProductStockId,SUM(SRI.Quantity) AS ReturnQty FROM SalesReturn SR 
			INNER JOIN SalesReturnItem SRI ON SR.Id = SRI.SalesReturnId
			WHERE SR.AccountId = @accountid AND SR.InstanceId = @instanceid
			AND cast(SR.CreatedAt as date) BETWEEN CAST(@fromDate AS DATE) AND CAST(@Todate AS DATE)
			GROUP BY SR.SalesId,SRI.ProductStockId 
		) SR 
		ON SR.SalesId = SI.SalesId AND SR.ProductStockId = SI.ProductStockId
		where si.AccountId = @accountid and si.InstanceId = @instanceid
		AND ps.ProductId IN(SELECT A.value('.', 'VARCHAR(100)') FROM @XmlList.nodes('A') AS FN(A))
		and cast(si.CreatedAt as date) between cast(@fromDate as date) and cast(@Todate  as date) 	
		AND s.Cancelstatus IS NULL						  
		group by ps.ProductId 
End	
		
If @type = 2 OR @type = 4 OR @type = 6 OR @type = 7 OR @type = 9 OR @type = 10 OR @type = 12 OR @type = 14
Begin
	insert into @transferTmp(productid,soldqty)
	select si.ProductId,sum(si.Quantity)
		from StockTransfer s inner join StockTransferItem as si 
		on s.id = si.TransferId	   
		where s.AccountId = @accountid and s.InstanceId = @instanceid
		and si.ProductId IN(SELECT A.value('.', 'VARCHAR(100)') FROM @XmlList.nodes('A') AS FN(A))
		and cast(si.CreatedAt as date) between cast(@fromDate as date) and cast(@Todate  as date) 	
		and s.transferstatus = 3	  
		group by si.ProductId
End

If @type = 3 OR @type = 5 OR @type = 6 OR @type = 7 OR @type = 9 OR @type = 11 OR @type = 12 OR @type = 15
Begin
	insert into @missedSalesTmp(productid,soldqty)
	select m.ProductId,sum(m.Quantity)
		from MissedOrder m   
		where m.AccountId = @accountid and m.InstanceId = @instanceid
		and m.ProductId IN(SELECT A.value('.', 'VARCHAR(100)') FROM @XmlList.nodes('A') AS FN(A))
		and cast(m.CreatedAt as date) between cast(@fromDate as date) and cast(@Todate  as date) 	 
		group by m.ProductId
End

IF @type = 8 OR @type = 9 OR @type = 10 OR @type = 11 OR @type = 12 OR @type = 13 OR @type = 14 OR @type = 15
BEGIN
	insert into @indentTmp(productid,soldqty)
	SELECT R1.ProductId,SUM(ISNULL(R1.Quantity,0)-ISNULL(R1.ReceivedQty,0))  FROM 
		(
			SELECT VOI.ProductId,VOI.Quantity,ISNULL(VOI.ReceivedQty,0) AS ReceivedQty,V.TinNo
			FROM VendorOrder VO
			INNER JOIN VendorOrderItem VOI ON VO.Id = VOI.VendorOrderId
			INNER JOIN Product P ON P.Id = VOI.ProductId
			INNER JOIN Vendor V ON V.Id = VO.VendorId
			WHERE VO.AccountId = @AccountId AND VO.InstanceId != @InstanceId 
			AND VOI.ProductId IN(SELECT A.value('.', 'VARCHAR(100)') FROM @XmlList.nodes('A') AS FN(A))
			AND CAST(VO.OrderDate AS DATE) BETWEEN CAST(@fromDate AS DATE) AND CAST(@TODATE AS DATE) 
			AND ISNULL(VOI.IsDeleted,0) = 0 AND (ISNULL(VOI.Quantity,0)-ISNULL(VOI.ReceivedQty,0)) > 0
			AND ISNULL(P.Status,1) = 1
		) R1
		INNER JOIN Instance I ON I.TinNo = R1.TinNo
		WHERE I.AccountId = @AccountId AND I.Id = @InstanceId AND I.isHeadOffice = 1
		GROUP BY R1.ProductId
END
	
	-- Final value
	insert into @Tmp(productid,soldqty)
	SELECT R.productid,SUM(R.soldqty)
	FROM (
	select productid,soldqty from @salesTmp
	UNION ALL
	select productid,soldqty from @transferTmp
	UNION ALL
	select productid,soldqty from @missedSalesTmp
	UNION ALL 
	SELECT productid,soldqty from @indentTmp
	) R 
	GROUP BY R.productid


--Get Stock 
update @Tmp set stock = b.stock from @Tmp a inner join (select sum(isnull(Stock,0)) as stock,ProductId from ProductStock PS
where PS.InstanceId = @instanceid and PS.AccountId = @accountid 
AND PS.Stock > 0 AND PS.ExpireDate > CAST(GETDATE() AS DATE) AND ISNULL(PS.Status,1) = 1
group by PS.ProductId) as b on a.productid = b.ProductId


select ProductId,
		soldqty,
		isnull(stock,0) AS Stock
		from @tmp  	 
		
end
