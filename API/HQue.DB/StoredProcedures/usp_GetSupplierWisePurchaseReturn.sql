
/** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
**22/06/2017   Violet			Prefix Added 
**05/07/2017   Sarubala			GST Calculation Added 
** 23/08/2017   Lawrence	    Added for 'Return Date Wise' filter
** 13/09/2017   Lawrence		All branch condition Added 
** 15/09/2017   Gavaskar		Return deleted condition Added
** 17/09/2017   Gavaskar		Return Price condition Added
**19/07/2018	Sarubala		Issue in only return, value not coming fixed
*******************************************************************************
--usp_GetSupplierWisePurchaseReturn_Test '3e2ec066-1551-4527-be53-5aa3c5b7fb7d','c503ca6e-f418-4807-a847-b6886378cf0b','1118c5de-0186-40d0-873d-6aa3c448f10f',null, null, 0
-- [usp_GetSupplierWisePurchaseReturn] '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','d6d25ebf-6868-4682-9e1c-db935da84835','',''
-- usp_GetSupplierWisePurchaseReturn '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','c94169f2-397b-44a8-ba67-d0b7d3d81b6d','01-Sep-2016','21-Sep-2016'

*/

CREATE PROCEDURE [dbo].[usp_GetSupplierWisePurchaseReturn](@InstanceId varchar(36),@AccountId varchar(36),@VendorId char(36),@StartDate datetime,@EndDate datetime, @IsInvoiceDate bit=0)
AS
 BEGIN
   SET NOCOUNT ON

	/* if(@StartDate is not null AND @StartDate!='') begin */
	Declare @InvStartDt date,  @InvEndDt date,
	  @GRNStartDt date, @GRNEndDt date

	  if @StartDate = ''
	  begin
		select @StartDate = null
	  end
	  if @EndDate = ''
	  begin
		select @EndDate = null
	  end

	  IF @IsInvoiceDate=1
	  Begin
		select @InvStartDt = @StartDate, @InvEndDt = @EndDate, @GRNStartDt = NULL, @GRNEndDt = NULL
	  End
	  Else
	  Begin
		select @InvStartDt = NULL, @InvEndDt = NULL, @GRNStartDt = @StartDate, @GRNEndDt = @EndDate
	  End	 

SELECT ltrim(isnull(r.Prefix,'')+' '+ r.ReturnNo) as ReturnNo,r.ReturnDate,r.Reason,CAST(isnull(r.TaxRefNo,0) as int) TaxRefNo,isnull(VendorPurchase.InvoiceNo,'') AS [InvoiceNo], isnull(VendorPurchase.InvoiceDate,'')  AS [InvoiceDate],isnull(VendorPurchase.GoodsRcvNo,'') AS [GoodsRcvNo],(Isnull(VendorPurchase.Prefix,'')+ISNULL(VendorPurchase.BillSeries,'')) BillSeries,
	p.Name,ri.Quantity,ISNULL(vi.PurchasePrice,ps.PurchasePrice) AS PurchasePrice,ps.VAT,isnull(ri.GstTotal,ps.GstTotal) GstTotal,isnull(ri.Cgst,ps.Cgst) Cgst,isnull(ri.Igst,ps.Igst) Igst,isnull(ri.Sgst,ps.Sgst) Sgst,(ISNULL(vi.PurchasePrice,ps.PurchasePrice)*ri.Quantity)*((case when r.TaxRefNo = 1 then isnull(ri.GstTotal,isnull(ps.GstTotal,0)) else ps.VAT end)/(100+ (case when r.TaxRefNo = 1 then isnull(ri.GstTotal,isnull(ps.GstTotal,0)) else ps.VAT end))) AS VatAmount	,
	--ISNULL(vi.PurchasePrice,ps.PurchasePrice)*ri.Quantity AS Total -- Commented by Gavaskar 17-09-2017
	 isnull(ri.quantity,0) * isnull(ri.ReturnPurchasePrice,vi.PurchasePrice) as Total -- Modified by Gavaskar 17-09-2017
	,Ins.Name AS InstanceName
FROM VendorReturn r with(nolock)
LEFT JOIN VendorPurchase with(nolock) ON VendorPurchase.Id = r.VendorPurchaseId
INNER JOIN Vendor with(nolock) ON Vendor.Id = r.VendorId
INNER JOIN VendorReturnItem ri with(nolock) ON ri.VendorReturnId=r.id
LEFT JOIN VendorPurchaseItem vi with(nolock) ON vi.ProductStockId=ri.ProductStockId and vi.VendorPurchaseId=r.VendorPurchaseId
INNER JOIN ProductStock ps with(nolock) ON ps.id=ri.ProductStockId
INNER JOIN Instance Ins on (Ins.Id = ISNULL(@InstanceId,r.InstanceId) and Ins.AccountId = @AccountId)
INNER JOIN Product p with(nolock) ON p.id=ps.ProductId WHERE Vendor.Id=@VendorId and r.InstanceId=ISNULL(@InstanceId,r.InstanceId) AND r.AccountId=@AccountId 
AND CAST(r.ReturnDate AS date) BETWEEN isnull(@InvStartDt,cast(r.ReturnDate as date)) AND isnull(@InvEndDt,cast(r.ReturnDate as date)) 
/* BETWEEN @StartDate AND @EndDate */
and (ri.IsDeleted is null or ri.IsDeleted=0) -- Condition added by Gavaskar 12-09-2017

AND isnull(CAST(VendorPurchase.CreatedAt AS date),GETDATE()) BETWEEN isnull(@GRNStartDt,isnull(CAST(VendorPurchase.CreatedAt AS date),GETDATE())) AND isnull(@GRNEndDt,isnull(CAST(VendorPurchase.CreatedAt AS date),GETDATE()))

--AND CAST(VendorPurchase.CreatedAt AS date) BETWEEN isnull(@GRNStartDt,cast(VendorPurchase.CreatedAt as date)) AND isnull(@GRNEndDt,cast(VendorPurchase.CreatedAt as date))
ORDER BY r.CreatedAt desc

--end

End