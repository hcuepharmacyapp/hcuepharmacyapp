-- [usp_GetSupplierWiseExpiryReturn] '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','d6d25ebf-6868-4682-9e1c-db935da84835','',''
-- usp_GetSupplierWiseExpiryReturn '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','c94169f2-397b-44a8-ba67-d0b7d3d81b6d','01-Sep-2016','21-Sep-2016'
CREATE PROCEDURE [dbo].[usp_GetSupplierWiseExpiryReturn] (@InstanceId varchar(36),@AccountId varchar(36),@VendorId char(36),@StartDate datetime,@EndDate datetime)
AS
 BEGIN
   SET NOCOUNT ON
	declare @condition nvarchar(max)	
	Declare @qry nvarchar(max)
	SET @condition=''

	if(@StartDate is not null AND @StartDate!='')
	begin
		set @condition=@condition+' AND CAST(VendorPurchase.invoicedate AS date) BETWEEN '+''''+CAST(@StartDate AS varchar(11))+'''' +' AND '+''''+CAST(@EndDate AS varchar(11))+''''
	end
	
	SET @qry='SELECT p.Name,ps.batchNo AS BatchNo,ps.VAT,ri.Quantity,vi.PurchasePrice,ps.SellingPrice,(ps.SellingPrice*ri.Quantity)*(ps.VAT/100) AS VatAmount,ps.SellingPrice*ri.Quantity AS Total
,r.ReturnNo,r.ReturnDate,r.Reason
FROM VendorReturn r
INNER JOIN VendorPurchase ON VendorPurchase.Id = r.VendorPurchaseId
INNER JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
INNER JOIN VendorReturnItem ri ON ri.VendorReturnId=r.id
INNER JOIN VendorPurchaseItem vi ON vi.ProductStockId=ri.ProductStockId and vi.VendorPurchaseId=r.VendorPurchaseId
INNER JOIN ProductStock ps ON ps.id=ri.ProductStockId and (ps.IsMovingStockExpire=0 OR ps.IsMovingStockExpire=1)
INNER JOIN Product p ON p.id=ps.ProductId WHERE Vendor.Id=''' +@VendorId +''''+' and r.InstanceId=''' +@InstanceId +''''+' AND r.AccountId  ='''+@AccountId+''''+ @condition+' ORDER BY r.CreatedAt desc'
	
	--print @qry
	EXEC sp_executesql @qry
  
 END