
/*                            
******************************************************************************                            
** File: [usp_Get_AllPharmaSummary]
** Name: [usp_Get_AllPharmaSummary]                            
** Description: To Get the All pharmacy sales details
**
** This template can be customized:                            
**                             
** Called by:                             
**                             
**	Parameters:                            
**	Input								Output                            
**	----------							-----------                            
**
** Author:Poongodi R
** Created Date: 17/01/2017
**
*******************************************************************************                            
** Change History                            
*******************************************************************************                            
** Date				Author					Description                            
*******************************************************************************      
** 25/01/2017	   Poongodi					 Sales Discounted value included in Sales amount calculation
** 27/01/2017		POongodi				Register type  changed from <>3 to (1,2,4)  
*******************************************************************************/
--exec usp_Get_AllPharmaSummary @RegisterType=0,@FromDate=N'18-Jan-2017',@Todate=N'18-Jan-2017'
 
Create Proc dbo.usp_Get_AllPharmaSummary(@RegisterType int,
@FromDate Datetime,
@Todate Datetime
)
As 
begin

Select @RegisterType  = isnull (@RegisterType,0),
		@FromDate = cast(@FromDate as date),
		@Todate = cast(@Todate as date)
 
	 select    
   
					 lower(isnull(i.City,'') ) [city],
					 'Summary' [GroupName],
					  count(S.salescount) as SalesCount,  
					  sum(isnull(S.salesamountBeforeDiscount,0) - isnull(s.salesdiscountedAmount,0)) as SalesAmount,
					  0.00 POCount,
					  0.00 POAmount
               from    
 Instance as i  
  inner join Account as a on a.Id = i.AccountId
				 
				   LEFT JOIN (
	 select sii.InstanceId,count(s.InvoiceNo) as salescount, 
			   sum(  case 
                                when isNull(sii.SellingPrice,0)=0 
	                            then (productstock.SellingPrice)*sii.Quantity
	                            else (sii.SellingPrice)*sii.Quantity
	                            end
                               ) as salesamountBeforeDiscount,
			       sum(case
									    when  ISNULL(s.Discount,0) <> 0 then 
										      case 
											        when sii.SellingPrice is not null 
													then ((sii.SellingPrice*s.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*s.Discount)/100)*sii.Quantity
											  end
                                      when isnull(sii.Discount,0) <> 0 then
									          case
											         when sii.SellingPrice is not null 
													then ((sii.SellingPrice*sii.Discount)/100)*sii.Quantity 
													else ((productstock.SellingPrice*sii.Discount)/100)*sii.Quantity
											  end
                                       else								         
											         0											 
									 end								     
								) as salesdiscountedAmount  
								  from Sales as  s inner join SalesItem as  sii on s.Id = sii.SalesId
				                 inner join productstock as productstock on productstock.id=sii.productstockid
								
				                  and  cast(S.CreatedAt as date) between @FromDate and @Todate
				                 group by sii.SalesId,sii.InstanceId
								 ) as S   on i.id = S.InstanceId
		 		 where (((a.RegisterType = @RegisterType) and (@RegisterType >0)) 
				  or ((a.RegisterType in (1,2,4))  and (@RegisterType =0)))						 
group by  
					isnull(i.City,'')
		

end 

