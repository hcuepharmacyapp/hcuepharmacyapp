 /*                               
******************************************************************************                            
** File: [usp_GetInventoryExpiryInfo]   
** Name: [usp_GetInventoryExpiryInfo]                               
** Description: To Get Expiry Product Details
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 28/02/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 15/03/2017 Poongodi			Rack Number taken from Product Instance      
 ** 19/05/2017 Settu			BoxNo included
 ** 02/06/2017 Poongodi			ProductInstance Join changed
  ** 31/07/2017 Poongodi		Zero stock expiry included 
  ** 28/08/2017 Lawrence		product name search with exact match
*******************************************************************************/ 
 CREATE PROC [dbo].[usp_GetInventoryExpiryInfo]( @InstanceId VARCHAR(36), 
												@AccountId  VARCHAR(36), 
												@NameType   INT, 
												@Name       VARCHAR(1000), 
												@PageNo     INT=0, 
												@PageSize   INT=10) 
AS 
  BEGIN 
    SET nocount ON 
	Declare @PName varchar(250),
			  @GenericName varchar(250)

			if (@NameType =1)
			BEGIN
				SET @PName = case when @Name = '' then NULL ELSE @Name END
				SET @GenericName = ''
			END
			else
			BEGIN
				SET @GenericName = CASE WHEN @Name IS NULL THEN '' ELSE @Name END
				SET @PName = NULL
			END

    SELECT          ps.productid [ProductId], 
                    --ps.stock [Stock], 
                    --ps.expiredate [ExpireDate], 
                    p.NAME [ProductName], 
                    p.genericname [GenericName], 
                    p.code [ProductCode], 
                    p.manufacturer [Manufacturer], 
                    ip.rackno [RackNo], 
					ip.boxno [BoxNo], 
                    p.status [Status], 
                    MAX(v.NAME)          [VendorName], 
                    Count(1) OVER() TotalRecordCount 
    FROM  
	( 
        SELECT * FROM ProductStock PST WITH(nolock) 
        WHERE pst.accountid=@AccountId AND pst.instanceid=@InstanceId 
		AND Cast(PST.expiredate AS DATE) <= Cast(Getdate() AS DATE) 
		AND PST.Stock != 0 AND (PST.Status is null or PST.Status = 1) 
	) PS
	INNER JOIN      product P WITH(nolock) 
    ON              p.id = ps.productid 
	AND             ps.accountid=@AccountId
    AND             ps.instanceid=@InstanceId      
	LEFT JOIN ProductInstance IP on IP.ProductId = P.id
	AND             IP.accountid=@AccountId
	AND             IP.instanceid=@InstanceId   
    LEFT OUTER JOIN vendor V WITH(nolock) 
    ON              v.id = ps.vendorid 
    WHERE (P.Name =  CASE WHEN @PName IS NULL THEN p.Name ELSE @PName END
	OR P.Name LIKE CASE WHEN @PName IS NOT NULL AND LEN(@PName)=1 THEN @PName +'%' ELSE @PName END)
	AND      Isnull(p.genericname,'') LIKE @GenericName + '%' 
	GROUP BY ps.productid,p.NAME,p.genericname,p.code,p.manufacturer,ip.rackno,ip.boxno,p.status
    ORDER BY        p.NAME ASC offset @PageNo rows 
    FETCH next @PageSize rows only 
  END
 