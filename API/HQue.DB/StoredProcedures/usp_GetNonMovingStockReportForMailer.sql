CREATE PROCEDURE [dbo].[usp_GetNonMovingStockReportForMailer](@InstanceId varchar(36))
 AS
 BEGIN
   SET NOCOUNT ON
   Declare @dt datetime;
   Set @dt = getdate();
	 SELECT DISTINCT top 15 P.Name as ProductName,
	                 
	                 case when PS.CreatedAt is not null then Convert(Varchar(15), SI.CreatedAt,103) else  'Not Sold' end as LastSaleDate,	
					 ABS(PS.Stock) as AvailableStock, 							
					 S.InvoiceNo,
                     DATEDIFF(day,VP.InvoiceDate,@dt) as Age
					
	FROM ProductStock PS WITH(NOLOCK)
	--LEFT JOIN below is changed to inner join
	INNER JOIN ( SELECT --COUNT(Quantity) as Quantity,
				ProductStockId,MAX(CreatedAt) as CreatedAt 
				FROM SalesItem SI
				 WHERE SI.InstanceId= @InstanceId
				GROUP BY ProductStockId 
				 HAVING MAX(CreatedAt) < DATEADD(DAY,-90,@dt) 
				) as SI  on  SI.ProductStockId = PS.Id 


	INNER JOIN Product P WITH(NOLOCK) on P.Id = PS.ProductId
	INNER JOIN SalesItem OSI WITH(NOLOCK) on OSI.ProductStockId = SI.ProductStockId and OSI.CreatedAt = si.CreatedAt
	INNER JOIN sales S WITH(NOLOCK) on S.Id = OSI.SalesId
	LEFT OUTER JOIN VendorPurchaseItem VPI WITH(NOLOCK) on VPI.ProductStockId = PS.Id
	LEFT OUTER JOIN VendorPurchase VP WITH(NOLOCK) on VP.Id = VPI.VendorPurchaseId
	 WHERE PS.InstanceId = @InstanceId and PS.Stock > 0 
	ORDER BY AvailableStock desc


	 
END

