  --Usage : -- usp_GetSmsBranchList '003cc71f-26e0-49d4-a1e0-e0a2efd3ebbd','0bb2d317-bdbc-4d99-9e71-b554d78b661a','1900-01-01','2016-11-01'
  CREATE PROCEDURE [dbo].[usp_GetSmsBranchList](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime)
 AS
 BEGIN
   SET NOCOUNT ON
	SELECT CONVERT(date,createdAt) AS SmsDate,COUNT(DISTINCT CL.Id) AS TotalSms
	FROM CommunicationLog CL WITH(NOLOCK)
	WHERE CL.InstanceId = @InstanceId AND CL.AccountId = @AccountId
	AND CONVERT(date,CL.CreatedAt) BETWEEN @StartDate AND @EndDate
	GROUP BY CONVERT(date,CreatedAt)
	ORDER BY CONVERT(date,CreatedAt) DESC
END
 
 


