
  --Usage : -- exec [usp_GetLowProductStockInfo] '1446042d-0208-409a-95db-272324af07bf' --,'d5baa673-d6e4-4e3e-a593-cdd6313be32c','1900-01-01','2016-11-01'
 CREATE PROCEDURE [dbo].[usp_GetLowProductStockInfo](@InstanceId varchar(36))
 AS
 BEGIN
   SET NOCOUNT ON

select osi.Productid, p.Name as Productname, osi.AvgSold, p.Manufacturer as ManufacturerName, isnull(P.ReOrderQty, 0) as OrderQty
, ps.VendorId, isNull(ps.purchaseprice, 0) purchasePrice, isNull(ps.vat, 0) vat, ps.BatchNo, sum(ps.stock) Stock
from Product p inner join 
(
select p.Id as ProductId, osi.instanceId, CEILING(sum(osi.Quantity)/30) AvgSold, sum(isNull(ps.Stock, 0)) as Stock from productstock ps 
	Join Product P on P.id = ps.productId And ps.AccountId = P.AccountId
	inner join
			(
			select ps.productId, si.instanceId,  sum(si.Quantity) Quantity FROM salesitem (nolock) si 
			Join productStock ps On ps.id = si.ProductStockId
			WHERE Convert(date, si.CreatedAt) between dateadd(day,datediff(day,0,GETDATE()),-30) and dateadd(day,datediff(day,0,GETDATE()),0)
				AND si.InstanceId  =  @InstanceId
				--And si.productstockid in ('19629596-5708-485a-8b51-c8a6ff227916','2115cc8a-c105-42ba-806d-fdd7230c6ce9','2fa3728f-986e-4526-93e0-89f402520f56','f6ee9399-8fca-4a38-a7d1-123501793910')
				Group By ps.productId, si.instanceId
			) osi on p.id = osi.productId
	Group By p.Id, osi.instanceId
	having  sum(isnull( ps.Stock ,0))  < CEILING(sum(osi.Quantity)/30) And 
	sum(isnull( ps.Stock ,0)) > 0
	) osi on p.Id = osi.ProductId
	Join ProductStock ps On ps.ProductId = p.Id and ps.InstanceId = osi.InstanceId
--where p.InstanceId = @InstanceId
group by osi.Productid, p.Name, osi.AvgSold, p.Manufacturer, isnull(P.ReOrderQty, 0)
, ps.VendorId, isNull(ps.purchaseprice, 0), isNull(ps.vat, 0), ps.BatchNo
ORDER BY sum(ps.Stock) DESC

--   select osi.Productid, p.Name as Productname, osi.AvgSold, isNull(osi.Stock, 0) as Stock, p.Manufacturer as ManufacturerName, isnull(P.ReOrderQty, 0) as OrderQty 
--from Product p inner join  
--(
--	select p.Id as ProductId, osi.instanceId, CEILING(sum(osi.Quantity)/30) AvgSold, sum(isNull(ps.Stock, 0)) as Stock from productstock ps 
--	inner join  
--			(
--			select si.productstockid, si.instanceId,  sum(si.Quantity) Quantity FROM salesitem (nolock) si 
--			WHERE Convert(date, si.CreatedAt) between dateadd(day,datediff(day,0,GETDATE()),-30) and dateadd(day,datediff(day,0,GETDATE()),0)
--				AND si.InstanceId  =  @InstanceId 
--				Group By si.productstockid, si.instanceId
--			) osi on ps.id = osi.productStockId
--	Join Product P on P.id = ps.productId
--	Group By p.Id, osi.instanceId
--	having  sum(isnull( ps.Stock ,0))  < CEILING(sum(osi.Quantity)/30) And 
--	sum(isnull( ps.Stock ,0)) > 0
--	) osi on p.Id = osi.ProductId 
----where p.InstanceId = @InstanceId
--ORDER BY isNull(osi.Stock, 0) DESC

--	declare @Tmp table (
--Productid char(36),
--Stock decimal, 
--createdat datetime,
--VendorId char(36),
--Productname varchar(1000),
--ManufacturerName varchar(300),
--OrderQty int,
--vat decimal(9,2),
--purchasePrice decimal(18,3)

--)
-- declare @Nodays int 
-- Select @Nodays = isnull(Noofdays,0)  from InventorySettings where InstanceId = @InstanceId


--insert into @Tmp(stock,Productid, createdat,Productname,OrderQty,ManufacturerName)

--SELECT  
--       ABS(SUM(PS.Stock)) AS Stock ,
--	   ps.productid ,
--	  max(ps.createdat) as  createdat,
--	  p.name,
--	  max(isnull(P.ReOrderQty ,isnull(A.ReorderQty,0))),isnull(p.Manufacturer,'') as Manufacturer
	  
--	FROM ProductStock PS WITH(NOLOCK)
--	inner join product p on p.id = ps.productid  
--	left join (Select ps.ProductId as Id, ceiling((sum(si.Quantity)/90)*@Nodays )as ReorderQty,sum(distinct ps.Stock) as TotalStock
-- from Sales s inner join SalesItem si on s.Id=si.SalesId inner join ProductStock ps on ps.Id=si.ProductStockId inner join Product p1 on p1.id=ps.ProductId 
-- where    s.InvoiceDate>=DATEADD(month,-3,GETDATE()) 
-- and isnull(p1.ReOrderQty ,0)  = 0 
-- and s.InstanceId = @InstanceId
-- group by ps.ProductId) as A on A.Id =P.Id
--  	WHERE PS.InstanceId = @InstanceId
	
--	GROUP BY  ps.productid, p.name ,p.Manufacturer
--	HAVING ABS(SUM(PS.Stock)) between 1 and  30
--	ORDER BY Stock 

--	update @Tmp set VendorId = p.VendorId,purchasePrice = p.PurchasePrice, vat = p.VAT from @tmp a inner join productstock p on p.productid = a.productid and p.createdat = a.createdat 
--	where p.InstanceId = @InstanceId



--	select * from @tmp 
            
 END