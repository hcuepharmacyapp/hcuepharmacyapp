/** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
 23/05/2017    Poongodi R		Date wise payment grouped up and sp optimizied, current balance taken
 24/05/2017    Poongodi R		Payment mode group by added
 22/06/2017    Violet			Prefix Added
 13-10-2017    Senthil.S        @table introduced 
********************************************************************************/ 

Create PROCEDURE [dbo].[usp_GetSupplierPayment](@InstanceId varchar(36),@AccountId varchar(36),@VendorId char(36),@StartDate datetime,@EndDate datetime)
AS
 BEGIN
   SET NOCOUNT ON
	declare @condition nvarchar(max)	
	Declare @qry nvarchar(max)
	SET @condition=''

	/*if(@VendorId is not null AND @VendorId!='')
	begin
		set @condition=@condition+' AND VendorPurchase.VendorId='+''''+@VendorId +''''
	end
	if(@StartDate is not null AND @StartDate!='')
	begin
		set @condition=@condition+' AND CAST(Payment.CreatedAt AS date) BETWEEN '+''''+CAST(@StartDate AS varchar(11))+'''' +' AND '+''''+CAST(@EndDate AS varchar(11))+''''
	end
	
	SET @qry='SELECT Vendor.Name,Payment.CreatedAt as CreatedAt,VendorPurchase.InvoiceNo As InvoiceNo ,VendorPurchase.GoodsRcvNo As GoodsRcvNo,VendorPurchase.InvoiceDate As InvoiceDate, 
VendorPurchaseId,CAST(Round(Isnull(dbo.CreditCalculate(VendorPurchaseId),0),0) As Decimal(10, 2)) InvoiceAmount,Payment.PaymentMode,Payment.ChequeNo,Payment.ChequeDate,Isnull(Payment.Debit,0) as Debit,CAST(Isnull(Round(dbo.CreditCalculate(VendorPurchaseId





),0) -Debit,0) As Decimal(10, 2)) As Balance FROM Payment 
INNER JOIN VendorPurchase ON Payment.VendorPurchaseId = VendorPurchase.Id
and isnull(VendorPurchase.cancelstatus ,0)  =0
INNER JOIN Vendor ON Payment.VendorId=Vendor.Id
WHERE  Payment.InstanceId=''' +@InstanceId +''''+' AND Payment.AccountId  ='''+@AccountId+''''+ @condition+' ORDER BY Payment.CreatedAt asc'
		EXEC sp_executesql @qry
	 print @qry */

	/* SELECT Vendor.Name,Payment.CreatedAt as CreatedAt,VendorPurchase.InvoiceNo As InvoiceNo ,VendorPurchase.GoodsRcvNo As GoodsRcvNo,
	 VendorPurchase.InvoiceDate As InvoiceDate, ISNULL(VendorPurchase.BillSeries,'') BillSeries,
VendorPurchaseId,CAST(Round(Isnull(dbo.CreditCalculate(VendorPurchaseId),0),0) As Decimal(10, 2)) InvoiceAmount,Payment.PaymentMode,Payment.ChequeNo,Payment.ChequeDate,Isnull(Payment.Debit,0) as Debit,CAST(Isnull(Round(dbo.CreditCalculate(VendorPurchaseId




),0) -Debit,0) As Decimal(10, 2)) As Balance,Payment.BankName,Payment.BankBranchName,Payment.IfscCode FROM Payment 
INNER JOIN VendorPurchase ON Payment.VendorPurchaseId = VendorPurchase.Id
and isnull(VendorPurchase.cancelstatus ,0)  = 0
INNER JOIN Vendor ON Payment.VendorId=Vendor.Id
WHERE  Payment.InstanceId=@InstanceId AND Payment.AccountId  = @AccountId
AND CAST(Payment.CreatedAt AS date) BETWEEN CAST(isnull(@StartDate, Payment.CreatedAt) AS date) and CAST(isnull(@EndDate,Payment.CreatedAt) AS date)
AND VendorPurchase.VendorId=isnull(@VendorId,VendorPurchase.VendorId)
 and Payment.Debit != 0
 ORDER BY Payment.CreatedAt asc  */

 	 if (@StartDate ='') select @StartDate  = null
	 if (@EndDate ='') select @EndDate  = null
	 if (@VendorId ='') select @VendorId  = null


	 DECLARE @table1 TABLE
				(
				 Name varchar(150),
				 CreatedAt datetime,
				 InvoiceNo varchar(150),
				 GoodsRcvNo varchar(150),
				 InvoiceDate datetime,
				 BillSeries varchar(150),
				 VendorPurchaseId char(36),
				 InvoiceAmount decimal(10,2),
				 PaymentMode varchar(150),
				 [ChequeNo] varchar(150),
				 [ChequeDate] datetime,
				 Debit decimal(10,2),
				 Balance decimal(10,2),
				 BankName varchar(150),
				 BankBranchName varchar(150),
				 ifscCode varchar(150)
				)


			 DECLARE @table2 TABLE
				(
				 Name varchar(150),
				 CreatedAt datetime,
				 InvoiceNo varchar(150),
				 GoodsRcvNo varchar(150),
				 InvoiceDate datetime,
				 BillSeries varchar(150),
				 VendorPurchaseId char(36),
				 InvoiceAmount decimal(10,2),
				 PaymentMode varchar(150),
				 [ChequeNo] varchar(150),
				 [ChequeDate] datetime,
				 Debit decimal(10,2),
				 Balance decimal(10,2),
				 BankName varchar(150),
				 BankBranchName varchar(150),
				 ifscCode varchar(150)
				)


				 DECLARE @table3 TABLE
				(
				 Name varchar(150),
				 CreatedAt datetime,
				 InvoiceNo varchar(150),
				 GoodsRcvNo varchar(150),
				 InvoiceDate datetime,
				 BillSeries varchar(150),
				 VendorPurchaseId char(36),
				 InvoiceAmount decimal(10,2),
				 PaymentMode varchar(150),
				 [ChequeNo] varchar(150),
				 [ChequeDate] datetime,
				 Debit decimal(10,2),
				 Balance decimal(10,2),
				 BankName varchar(150),
				 BankBranchName varchar(150),
				 ifscCode varchar(150)
				)

  Insert into @table1	
  SELECT MAX(Vendor.Name) Name,
  CAST(Payment.CreatedAt AS DATE) as CreatedAt,
 mAX(  VendorPurchase.InvoiceNo ) InvoiceNo ,  
 max(isnull(VendorPurchase.GoodsRcvNo ,'')) As GoodsRcvNo,
	 mAX(VendorPurchase.InvoiceDate )As InvoiceDate,
	  MAX( ISNULL(VendorPurchase.Prefix,'')+ISNULL(VendorPurchase.BillSeries,'') )BillSeries,
VendorPurchaseId,
CAST(max(po.InvoiceAmount) As Decimal(10, 2)) InvoiceAmount,
case  (isnull(Payment.PaymentType,'')) when 'cash' then  (isnull(Payment.PaymentType,'')) when 'cheque' then  (isnull(Payment.PaymentType,'')) else  (isnull(Payment.PaymentMode,'Cash')) end [PaymentMode],max(Payment.ChequeNo) [ChequeNo],max(Payment.ChequeDate) [ChequeDate],sum(Isnull(Payment.Debit,0)) as Debit,
case when CAST( (Round(max(po.InvoiceAmount),0) - max(isnull(DebitAmt.TotalDebit,0))) As Decimal(10, 2)) < 0 then 0 else  
CAST( (Round(max(po.InvoiceAmount),0) - max(isnull(DebitAmt.TotalDebit,0))) As Decimal(10, 2))  end 
As Balance,
max(isnull(Payment.BankName,'')) [BankName],
max(isnull(Payment.BankBranchName,'')) [BankBranchName],
max(isnull(Payment.IfscCode,'')) IfscCode FROM Payment 
INNER JOIN VendorPurchase ON Payment.VendorPurchaseId = VendorPurchase.Id
and isnull(VendorPurchase.cancelstatus ,0)  = 0
INNER JOIN Vendor ON Payment.VendorId=Vendor.Id
CROSS APPLY  (select Round(Isnull(dbo.CreditCalculate(VendorPurchaseId),0),0)   AS InvoiceAmount ) As [PO]
CROSS APPLY  ((select  sum(isnull(debit,0)) TotalDebit from payment where VendorPurchaseId = VendorPurchase.id ) )As [DebitAmt]
WHERE  Payment.InstanceId=@InstanceId AND Payment.AccountId  = @AccountId
AND CAST(Payment.CreatedAt AS date) BETWEEN CAST(isnull(@StartDate, Payment.CreatedAt) AS date) and CAST(isnull(@EndDate,Payment.CreatedAt) AS date)
AND VendorPurchase.VendorId=isnull(@VendorId,VendorPurchase.VendorId)
 and isnull(Payment.Debit ,0) > 0 
 group by VendorPurchaseId, CAST(Payment.CreatedAt AS DATE),po.InvoiceAmount,isnull(Payment.PaymentMode,'Cash'), isnull(Payment.PaymentType,'')
 
 ORDER BY CAST(Payment.CreatedAt AS DATE)  desc 
 
 Insert into @table2
 Select v.Name, Max(p.CreatedAt) as CreatedAt, '' as InvoiceNo, '' as GoodsRcvNo, Max(p.transactiondate) as InvoiceDate, '' as BillSeries, null as vendorpurchaseId, 0 as InvoiceAmount,
 'Cash' as [PaymentMode], '' as [ChequeNo], null as chequeDate, sum(Isnull(p.Debit,0)) as Debit,
 CAST( sum(p.credit) - sum(p.debit) As Decimal(10, 2)) as balance,
 '' as [BankName],
 '' as [BankBranchName],
 '' as IfscCode
 From Payment P inner join Vendor v on p.VendorId = v.id
 Where p.VendorId = Isnull (@VendorId, p.VendorId)
 and  p.VendorPurchaseId is null   
 and p.InstanceId=@InstanceId AND p.AccountId  = @AccountId
 Group by v.name

 Insert into @table3
			Select a.* from 
			(Select * from @table1
			union 
			Select * from @table2) as a

 --select * from @table3


 select Name , CreatedAt, InvoiceNo, GoodsRcvNo, InvoiceDate ,BillSeries,vendorpurchaseId,InvoiceAmount,
 [PaymentMode], [ChequeNo], chequeDate, sum(Debit) as Debit, sum(balance) as Balance,
 [BankName],[BankBranchName], ifsccode 
 From @table3
 Group by Name , CreatedAt, InvoiceNo, GoodsRcvNo, InvoiceDate,BillSeries,vendorpurchaseId,InvoiceAmount,
 [PaymentMode], [ChequeNo], chequeDate, [BankName],[BankBranchName], ifsccode 


 END


 