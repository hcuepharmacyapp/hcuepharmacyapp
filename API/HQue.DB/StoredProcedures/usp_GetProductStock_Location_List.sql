 
/*******************************************************************************                            
** Rack NO and Bok no  List for fill the auto complete textbox in stock report                 
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 21/07/2017  Poongodi		Newly created
*******************************************************************************/ 
 
CREATE PROCEDURE [dbo].usp_GetProductStock_Location_List(@InstanceId varchar(36), @StockType varchar(10), @SearchType varchar(10),@SearchText varchar(100))
AS
 BEGIN
  Declare @From_Stock int=0, @To_Stock int =0, @From_Expiry date =NULL, @To_Expiry date =NULL 
 declare @Status table (stockstatus int )
 declare @RackNo varchar(100) ='',
		@BoxNO varchar(100)  =''
		if (@SearchType= 'boxno') 
			select @BoxNO =@SearchText ,@RackNo =''
		else
		select @BoxNO ='' ,@RackNo =@SearchText
			
 if (right(@StockType,1) ='2')
 begin
 insert into @Status 	values (2)
 select @StockType  = replace (@StockType,2,'')
 end 
 else
 begin
 insert into @Status 	values (1),(0)
 end 

 if (@StockType ='nonzero')
	begin
	select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  = cast(getdate () as date), @To_Expiry  = NULL
	 
	end 
 else if (@StockType ='expire')
	begin
	select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  =  NULL, @To_Expiry  =cast(getdate () as date)
	
	end

  else if (@StockType ='all')
	begin
	select @From_Stock = 1 , @To_Stock = NULL,@From_Expiry  =  NULL, @To_Expiry  = NULL	
	end  

	if (@SearchType='rackno')
	begin
   SELECT distinct isnull(P.RackNo,'')   [RackNo],'' BoxNo FROM ProductStock PS WITH(NOLOCK) Inner join(
    SElect ProductId, Max(isnull(RAckNo,'')) [RackNo] from ProductInstance where Instanceid =@InstanceId and isnull(RackNo ,'')  like  @SearchText+'%' group by ProductId) P
	on P.ProductId = Ps.ProductId 
     WHERE PS.InstanceId = @InstanceId 
	 	 and ps.Stock between isnull(@From_Stock , ps.stock) and isnull(@To_Stock , ps.stock) 
 	 and cast(ps.ExpireDate  as date)  between isnull(@From_Expiry ,cast(ps.ExpireDate  as date) ) and isnull(@To_Expiry,cast(ps.ExpireDate  as date) )
    ORDER BY RackNo 
	end 

	else if (@SearchType='boxno')
	begin
   SELECT distinct  '' RackNo ,isnull(P.BoxNo,'') [BoxNo] FROM ProductStock PS WITH(NOLOCK) Inner join(
    SElect ProductId, Max(isnull(BoxNo,'')) [BoxNo] from ProductInstance where Instanceid =@InstanceId and isnull(BoxNo ,'')  like  @SearchText+'%' group by ProductId) P
	on P.ProductId = Ps.ProductId 
     WHERE PS.InstanceId = @InstanceId 
	 	 and ps.Stock between isnull(@From_Stock , ps.stock) and isnull(@To_Stock , ps.stock) 
 	 and cast(ps.ExpireDate  as date)  between isnull(@From_Expiry ,cast(ps.ExpireDate  as date) ) and isnull(@To_Expiry,cast(ps.ExpireDate  as date) )
    ORDER BY  BoxNo 
	end 
END