﻿/** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
**22/06/2017   Violet			Prefix Added 
**13/09/2017   Poongodi			InstanceId join condition added
** 15/09/2017   Gavaskar		Return deleted condition Added
*******************************************************************************/ 
-- usp_GetBuyReturnSummaryList null,'c503ca6e-f418-4807-a847-b6886378cf0b','13-Jan-2017','13-Sep-2017',1
-- usp_GetBuyReturnSummaryList '3e2ec066-1551-4527-be53-5aa3c5b7fb7d','c503ca6e-f418-4807-a847-b6886378cf0b','13-Jan-2017','13-Sep-2017',1
--'3e2ec066-1551-4527-be53-5aa3c5b7fb7d'
CREATE PROCEDURE [dbo].[usp_GetBuyReturnSummaryList](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime, @IsInvoiceDate bit=0)
 AS
 BEGIN
   SET NOCOUNT ON
    
	Declare @InvStartDt date,  @InvEndDt date,
	 @GRNStartDt date, @GRNEndDt date

	 IF @IsInvoiceDate=1
	 Begin
		select @InvStartDt = @StartDate, @InvEndDt = @EndDate, @GRNStartDt = NULL, @GRNEndDt = NULL
	 End
	 Else
	 Begin
		select @InvStartDt = NULL, @InvEndDt = NULL, @GRNStartDt = @StartDate, @GRNEndDt = @EndDate
	 End

SELECT ltrim(isnull(r.Prefix,'')+' '+r.ReturnNo) as ReturnNo,r.ReturnDate,VendorPurchase.InvoiceNo AS [InvoiceNo], VendorPurchase.InvoiceDate AS [InvoiceDate]
,Vendor.Name,Count(ri.VendorReturnId) AS NOOfItem,sum(ISNULL(vi.PurchasePrice,ri.ReturnPurchasePrice) * ri.Quantity) AS Total, Inst.Name AS InstanceName
FROM VendorReturn r
LEFT JOIN VendorPurchase ON VendorPurchase.Id = r.VendorPurchaseId --INNER JOIN VendorPurchase ON VendorPurchase.Id = r.VendorPurchaseId
INNER JOIN Vendor ON Vendor.Id = r.VendorId --VendorPurchase.VendorId
INNER JOIN VendorReturnItem ri ON ri.VendorReturnId=r.id 
LEFT JOIN VendorPurchaseItem vi ON vi.ProductStockId=ri.ProductStockId and vi.VendorPurchaseId=r.VendorPurchaseId
INNER JOIN ProductStock ps ON ps.id=ri.ProductStockId
INNER JOIN Instance Inst on Inst.id=ISNULL(@InstanceId,r.instanceid)
WHERE r.InstanceId=isnull(@InstanceId,r.InstanceId) and r.AccountId=@AccountId 
AND r.ReturnDate BETWEEN @StartDate AND @EndDate and (ri.IsDeleted is null or ri.IsDeleted=0) -- Condition added by Gavaskar 12-09-2017
GROUP BY r.ReturnNo,r.Prefix,r.ReturnDate,VendorPurchase.InvoiceNo,VendorPurchase.InvoiceDate,Vendor.Name, Inst.Name
ORDER BY r.ReturnDate desc
           
END