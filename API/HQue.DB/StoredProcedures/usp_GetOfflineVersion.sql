/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 05/10/2017  nandhini		   offline version
*******************************************************************************/ 
create Proc [dbo].[usp_GetOfflineVersion]
as
Select 
I.Area
,I.OfflineVersionNo
,I.Name,I.Id
,I.AccountId
,I.RegistrationDate
,I.Phone
,I.Mobile
,I.DrugLicenseNo
,I.TinNo
,I.ContactName
,I.ContactMobile
,I.ContactEmail
,I.SecondaryName
,I.SecondaryMobile
,I.SecondaryEmail
,I.ContactDesignation
,I.Address
,I.City
,I.State
,I.Pincode
,I.Landmark
,I.Status
,I.DrugLicenseExpDate
,I.BuildingName
,I.StreetName
,I.Country
,I.OfflineStatus
,I.IsOfflineInstalled
,I.isHeadOffice
,I.GsTinNo from Instance I with(nolock) inner Join Account A with(nolock) On I.AccountId=A.Id where a.registertype =2 
and isnull(i.IsOfflineInstalled,0) = 1
order by 1


