﻿CREATE PROCEDURE [dbo].[usp_GetProductAgeAnalysis](@InstanceId varchar(36),@AccountId varchar(36),@ProductId varchar(36),@StartDate datetime,@EndDate datetime)
AS
 BEGIN
   SET NOCOUNT ON
	declare @condition nvarchar(max)	
	Declare @qry nvarchar(max)
	SET @condition=''

	if(@ProductId is not null AND @ProductId!='')
	begin
		set @condition=@condition+' and p.id= '+''''+@ProductId +''''
	end

	if(@StartDate is not null AND @StartDate!='')
	begin
		set @condition=@condition+' AND CAST(ps.CreatedAt AS date) BETWEEN '+''''+CAST(@StartDate AS varchar(11))+'''' +' AND '+''''+CAST(@EndDate AS varchar(11))+''''
	end
	
	SET @qry='Select ps.Id,p.Name,ps.BatchNo,ps.ExpireDate,
	Case When DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate())>0 AND DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate())<30 Then ps.Stock Else Null End AS [Lessthan30days],
	Case When DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate())>30 AND DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate())<60  Then ps.Stock Else Null End  AS [Lessthan60days],
	Case When DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate())>60 AND DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate())<120  Then ps.Stock Else Null End AS [Lessthan120days],
	Case When DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate())>120 AND DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate())<180  Then ps.Stock Else Null End AS [Lessthan180days],
	Case When DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate())>180 Then ps.Stock Else Null End AS [Lessthan360days],
	DATEDIFF(day,CAST(ps.CreatedAt AS date),Getdate()) AS Age,(Select top 1 CAST(CreatedAt AS date) from SalesItem where ProductStockId=ps.Id order by CreatedAt desc) AS LastSaleDate from ProductStock ps
Inner Join Product P ON ps.ProductId=p.Id
 Where ps.InstanceId=''' +@InstanceId +''''+' AND ps.AccountId  ='''+@AccountId+''''+ @condition+' and (ps.Status is null or ps.Status = 1) order by ps.CreatedAt desc'
	
	--print @qry
	EXEC sp_executesql @qry
  
 END