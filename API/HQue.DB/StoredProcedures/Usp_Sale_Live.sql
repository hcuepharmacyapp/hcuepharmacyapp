 /*************************************
******************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**16/10/17     nandhini	   SP Created 

*******************************************************************************/
 create PROCEDURE [dbo].[Usp_Sale_Live](@Fromdate date)
 AS
 BEGIN
 declare @Correctdate int
 select
 @Correctdate= (DATEDIFF(DAY, @FromDate, GETDATE()))


 
 
select @Fromdate = dateadd(day,datediff(day,@Correctdate,GETDATE()),0)

   SET NOCOUNT ON
 

			 SELECT (isnull(GroupName,'')) as GroupName,(isnull(PharmaName,''))as PharmaName,isnull(City,'') as City,isnull(Area,'') AS Area,SUM(SALESNET) AS SALESNET,SUM(TOTALSALES) as TOTALSALES,SUM(TOTALPURCHASECOUNT) as TOTALPURCHASECOUNT,SUM(PURCHASENET)AS PURCHASENET  FROM ( 

            ---- SALES DETAILS START

            SELECT GroupName,PharmaName,City,Area,sum(AMOUNT - RETURNEDAMOUNT) SALESNET,SUM(PROFIT) PROFIT,SUM(TotalSales) as TotalSales,'0'as TotalPurchaseCount,'0.00' AS PURCHASENET FROM (
            SELECT GroupName,PharmaName,City,Area,sum(Amount) Amount,round(Convert(decimal(18,2),sum(Amount - TotalPurchase-ProfitReturnAmount)),0) Profit,'0.00' as ReturnedAmount,sum(ProfitReturnAmount) ProfitReturnAmount, sum(TotalSales)as TotalSales,'0



'as TotalPurchaseCount FROM (
            select GroupName,PharmaName,City,Area,sum(Amount) Amount,sum(TotalPurchase) TotalPurchase,'0.00' as ReturnedAmount,'0.00'As ProfitReturnAmount,count(TotalSales) as TotalSales,'0'as TotalPurchaseCount from (
            select Account.Name as GroupName,Instance.Name AS PharmaName,Instance.City AS City,Instance.Area AS Area,round(Convert(decimal(18,2),(sum((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity - 
            ((CASE WHEN salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * salesitem.Quantity * isnull(sales.Discount,0) / 100))-sum(salesitem.Quantity * 
            (case when salesitem.SellingPrice > 0 then salesitem.SellingPrice else productstock.SellingPrice END) * isnull(salesitem.Discount,0) / 100))),0)  As Amount, 
            Convert(decimal(18,2),sum((ISNULL(vpi.PurchasePrice,ProductStock.PurchasePrice) * salesitem.Quantity))) AS TotalPurchase,'0.00' as ReturnedAmount ,'0.00'As ProfitReturnAmount,count(distinct sales.id) AS TotalSales,'0'as TotalPurchaseCount
 from Account
            INNER JOIN Instance on Instance.AccountId = Account.Id 
            INNER JOIN sales on sales.AccountId = Account.Id AND SALES.InstanceId=INSTANCE.ID  
            INNER JOIN salesitem on sales.id = salesitem.salesid 
            INNER JOIN productstock on salesitem.productstockid = productstock.id AND productstock.InstanceId=INSTANCE.ID
            left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem  group by productstockId) vpi on productstock.id=vpi.productstockid   
            WHERE  Convert(date,sales.invoicedate) =@Fromdate and sales.Cancelstatus is NULL and account.RegisterType in(2)
            group by sales.InvoiceNo,Account.Name,Instance.Name,Instance.City,Instance.Area) a group by a.GroupName,a.PharmaName,a.City,a.Area,a.TotalSales

            UNION

            select GroupName,PharmaName,City,Area,'0.00' As TotalPurchase,'0.00' As Amount,'0.00' as ReturnedAmount,sum(Amount - TotalPurchase) as ProfitReturnAmount,'0'as TotalSales,'0'as TotalPurchaseCount from (
            select Account.Name as GroupName,Instance.Name AS PharmaName,Instance.City AS City,Instance.Area AS Area,
			----round(Convert(decimal(18,2),(sum((CASE WHEN sri.MrpSellingPrice > 0 then sri.MrpSellingPrice else productstock.SellingPrice END) * sri.Quantity - 
   ----         ((CASE WHEN sri.MrpSellingPrice > 0 then sri.MrpSellingPrice else productstock.SellingPrice END) * sri.Quantity * sri.Discount / 100))-sum(sri.Quantity * 
   ----         (case when sri.MrpSellingPrice > 0 then sri.MrpSellingPrice else productstock.SellingPrice END) * isnull(sri.Discount,0) / 100))),0)  As Amount,
   round(Convert(decimal(18,2),(sum(
			((CASE WHEN sri.MrpSellingPrice > 0 then sri.MrpSellingPrice else productstock.SellingPrice END) * sri.Quantity)-((sri.Quantity * 
            (case when sri.MrpSellingPrice > 0 then sri.MrpSellingPrice else productstock.SellingPrice END) * isnull(sri.Discount,0) / 100))
			))),0)  As Amount,
            Convert(decimal(18,2),sum((ISNULL(vpi.PurchasePrice,ProductStock.PurchasePrice) * sri.Quantity)))  AS TotalPurchase,'0'as TotalSales,'0'as TotalPurchaseCount
            from Account
            INNER JOIN Instance on Instance.AccountId = Account.Id 
            INNER JOIN SalesReturn as sr on sr.AccountId = Account.Id AND sr.InstanceId=INSTANCE.ID 
            inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
            INNER JOIN productstock on sri.productstockid = productstock.id 
            left join (Select distinct productstockId, min(PurchasePrice) as PurchasePrice from vendorpurchaseitem  group by productstockId) vpi on productstock.id=vpi.productstockid   
            WHERE  Convert(date,sr.ReturnDate) =@Fromdate and  sri.CancelType is NULL and account.RegisterType in(2)
            group by Account.Name,Instance.Name,Instance.City,Instance.Area,sr.ReturnNo ) as a group by a.GroupName,a.PharmaName,a.City,a.Area,a.TotalSales

            UNION all

            select Account.Name as GroupName, Instance.Name AS PharmaName,Instance.City AS City,Instance.Area AS Area,'0.00' As TotalPurchase,'0.00' As Amount,'0.00' as ReturnedAmount ,'0.00'As ProfitReturnAmount,'0'as TotalSales,'0'as TotalPurchaseCount






            from Account
            INNER JOIN Instance on Instance.AccountId = Account.Id 
            WHERE Account.RegisterType in(2)
            group by Account.Name,Instance.Name,Instance.City,Instance.Area
            ) AS a GROUP BY GroupName,PharmaName,City,Area,TotalSales


            UNION  all
            select GroupName,PharmaName,City,Area,'0.00'As Amount,'0.00'As Profit,sum(ReturnedAmount)ReturnedAmount,'0.00'As ProfitReturnAmount,'0'as TotalSales,'0'as TotalPurchaseCount from (
            select Account.Name as GroupName,Instance.Name AS PharmaName,Instance.City AS City,Instance.Area AS Area,'0.00'  As Amount,'0.00'As Profit,
            round(Convert(decimal(18,2),(sum(case when isNull(s.Discount, 0) = 0 then case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice)*sri.Quantity else (sri.MrpSellingPrice)*sri.Quantity end
            else case when isNull(sri.MrpSellingPrice,0)=0 then (ps.SellingPrice-(ps.SellingPrice * isNull(s.Discount,0))/100)*sri.Quantity else 
       (sri.MrpSellingPrice-(sri.MrpSellingPrice * isNull(s.Discount,0))/100)*sri.Quantity end end) -( sum(case when isNull(sri.Discount, 0) = 0 then 0 else case when isNull(sri.MrpSellingPrice,0)=0 then ((ps.SellingPrice * sri.Discount)/100)*sri.Quantity
 else


 
            ((sri.MrpSellingPrice * sri.Discount)/100)*sri.Quantity end end)))),0) as ReturnedAmount,'0'as TotalSales,'0'as TotalPurchase
            from Account
            INNER JOIN Instance on Instance.AccountId = Account.Id 
            INNER JOIN SalesReturn as sr on sr.AccountId = Account.Id AND sr.InstanceId=INSTANCE.ID 
            left JOIN Sales as s ON s.Id = sr.SalesId
            inner join SalesReturnItem as sri on sri.SalesReturnId =  sr.Id
            inner join ProductStock as ps on ps.id = sri.ProductStockId
            inner join Product as p on p.Id = ps.ProductId
            WHERE  Convert(date,sr.ReturnDate) =@Fromdate and s.Cancelstatus is NULL and account.RegisterType in(2)
            group by Account.Name,Instance.Name,Instance.City,Instance.Area ,sr.ReturnNo ) b group by b.GroupName,b.PharmaName,b.City,b.Area ) AS SALE GROUP BY SALE.GroupName,SALE.PharmaName,SALE.City,SALE.Area

            ---- SALES DETAILS END
            ---- PURCHASE DETAILS START

            UNION  all

            SELECT GroupName,PharmaName,City,Area,'0.00' AS SALESNET,'0.00' AS PROFIT,'0'as TotalSales,sum(TotalPurchaseCount)as TotalPurchaseCount,SUM(BUYAMOUNT - BUYRETURNAMOUNT) AS PURCHASENET FROM (
            SELECT GroupName,PharmaName,City,Area,sum(BuyAmount) BuyAmount,'0.00' as BuyReturnAmount,'0'as TotalSales,sum(TotalPurchaseCount)as TotalPurchaseCount from (

            SELECT GroupName,PharmaName,City,Area,sum(BuyAmount-BuyCreditAmount+BuyDebitAmount) BuyAmount,'0.00' as BuyReturnAmount,'0'as TotalSales,sum(TotalPurchaseCount)as TotalPurchaseCount from (

            SELECT GroupName,PharmaName,City,Area,SUM(BuyAmount)as BuyAmount,'0.00' as ReturnAmount,'0.00' as BuyCreditAmount,'0.00' as BuyDebitAmount,'0'as TotalSales,sum(TotalPurchaseCount)as TotalPurchaseCount FROM(
            SELECT Account.Name as GroupName,Instance.Name AS PharmaName,Instance.City AS City,Instance.Area AS Area,Round(Convert(decimal(18,2),(sum((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END)))
            -((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100))
            +(((vi.PackagePurchasePrice*(vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))-
            ((vi.PackagePurchasePrice * (vi.PackageQty-(CASE WHEN vi.FreeQty>=0 THEN vi.FreeQty else '0' END))) * ((CASE WHEN vi.Discount>=0 THEN vi.Discount else '0' END) / 100)))/100)
            * (case when(isnull(v.TaxRefNo,0)=1) then ps.GstTotal else( CASE WHEN ps.CST>0 THEN ps.CST else ps.VAT END) end ) )) )),0) as BuyAmount,
            '0.00' as ReturnAmount,'0.00' as BuyCreditAmount,'0.00' as BuyDebitAmount,'0'as TotalSales,count(distinct v.id) AS TotalPurchaseCount
            FROM Account
            INNER JOIN Instance on Instance.AccountId = Account.Id 
            INNER JOIN VendorPurchase v on v.AccountId = Account.Id AND v.InstanceId=INSTANCE.ID
            inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
            inner join ProductStock ps
            on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
            where  Convert(date,v.CreatedAt)  = @Fromdate  and account.RegisterType in(2)
      group by Account.Name,Instance.Name,Instance.City,Instance.Area ,v.InvoiceNo ) as a GROUP BY GroupName,PharmaName,City,Area ,TotalPurchaseCount

            UNION all
            SELECT GroupName,PharmaName,City,Area ,'0.00' as BuyAmount,'0.00' as BuyReturnAmount,Round(Convert(decimal(18,2),(SUM(BuyCreditAmount))),0) as BuyCreditAmount,'0.00' as BuyDebitAmount,'0'as TotalSales,'0'as TotalPurchaseCount FROM(
            select Account.Name as GroupName,Instance.Name AS PharmaName,Instance.City AS City,Instance.Area AS Area,'0.00' as BuyAmount,'0.00' as BuyReturnAmount,(v.NoteAmount) as BuyCreditAmount ,'0'as TotalSales,'0'as TotalPurchaseCount
            FROM Account
            INNER JOIN Instance on Instance.AccountId = Account.Id 
            INNER JOIN VendorPurchase v on v.AccountId = Account.Id AND v.InstanceId=INSTANCE.ID
            inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
            inner join ProductStock ps
            on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
            where  Convert(date,v.CreatedAt)  = @Fromdate and v.NoteType = 'credit' and account.RegisterType in(2)
            group by Account.Name,Instance.Name,Instance.City,Instance.Area ,v.NoteAmount
            ) AS BuyCredit GROUP BY BuyCredit.GroupName,BuyCredit.PharmaName,BuyCredit.City,BuyCredit.Area

            UNION all
            SELECT GroupName,PharmaName,City,Area,'0.00' as BuyAmount,'0.00' as BuyReturnAmount,'0.00' as BuyCreditAmount,Round(Convert(decimal(18,2),(SUM(BuyDebitAmount))),0) as BuyDebitAmount,'0'as TotalSales,'0'as TotalPurchaseCount FROM(
            select Account.Name as GroupName,Instance.Name AS PharmaName,Instance.City AS City,Instance.Area AS Area,'0.00' as BuyAmount,'0.00' as BuyReturnAmount,(v.NoteAmount) as BuyDebitAmount ,'0'as TotalSales,'0'as TotalPurchaseCount
            FROM Account
            INNER JOIN Instance on Instance.AccountId = Account.Id 
            INNER JOIN VendorPurchase v on v.AccountId = Account.Id AND v.InstanceId=INSTANCE.ID
            inner join VendorPurchaseItem vi on v.Id=vi.VendorPurchaseId 
            inner join ProductStock ps
            on vi.ProductStockId=ps.id left join Vendor on Vendor.Id=v.VendorId 
            where  Convert(date,v.CreatedAt)  = @Fromdate and v.NoteType = 'debit' and account.RegisterType in(2)
            group by Account.Name,Instance.Name,Instance.City,Instance.Area ,v.NoteAmount
            ) AS BuyDebit GROUP BY BuyDebit.GroupName,BuyDebit.PharmaName,BuyDebit.City,BuyDebit.Area
            ) a group by a.GroupName, a.PharmaName,a.City,a.Area
            UNION  all
            select Account.Name as GroupName,Instance.Name AS PharmaName,Instance.City AS City,Instance.Area AS Area,'0.00' As BuyAmount,'0.00' as BuyReturnAmount,'0'as TotalSales,'0'as TotalPurchaseCount
            from Account
            INNER JOIN Instance on Instance.AccountId = Account.Id 
            where  account.RegisterType in(2)
            group by Account.Name,Instance.Name,Instance.City, Instance.Area) b group by b.GroupName,b.PharmaName,b.City,b.Area
            UNION all
            SELECT GroupName,PharmaName,City,Area,'0.00' as BuyAmount,SUM(BuyReturnAmount) as BuyReturnAmount,'0'as TotalSales,'0'as TotalPurchaseCount FROM (
            SELECT Account.Name as GroupName,Instance.Name AS PharmaName,Instance.City AS City,Instance.Area AS Area,'0.00' as BuyAmount,
            Round(Convert(decimal(18,2),sum(((isnull(VendorReturnItem.ReturnPurchasePrice,VendorPurchaseItem.PurchasePrice) * VendorReturnItem.Quantity)))),0)  as BuyReturnAmount,
            '0'as TotalSales,'0'as TotalPurchase
            FROM Account
            INNER JOIN Instance on Instance.AccountId = Account.Id 
            INNER JOIN VendorReturnItem on VendorReturnItem.AccountId = Account.Id AND VendorReturnItem.InstanceId=INSTANCE.ID 
            INNER JOIN VendorReturn ON VendorReturn.Id = VendorReturnItem.VendorReturnId  
            INNER JOIN VendorPurchase ON VendorPurchase.Id = VendorReturn.VendorPurchaseId
            INNER JOIN Vendor ON Vendor.Id = VendorPurchase.VendorId
            INNER JOIN ProductStock ON ProductStock.Id = VendorReturnItem.ProductStockId
            INNER JOIN VendorPurchaseItem ON VendorPurchaseItem.ProductStockId = ProductStock.Id
            INNER JOIN Product ON Product.Id = ProductStock.ProductId
            where  Convert(date,VendorReturn.ReturnDate)  = @Fromdate and account.RegisterType in(2)
            GROUP BY Account.Name,Instance.Name,Instance.City,Instance.Area,VendorPurchase.InvoiceNo) C GROUP BY C.GroupName,C.PharmaName,C.City,C.Area ) AS PURCHASE GROUP BY PURCHASE.GroupName,PURCHASE.PharmaName,PURCHASE.City,PURCHASE.Area

            ---- PURCHASE DETAILS END

            ) TOTALMARGIN  GROUP BY TOTALMARGIN.GroupName,TOTALMARGIN.PharmaName,TOTALMARGIN.City,TOTALMARGIN.Area ORDER BY SUM(SALESNET) DESC,SUM(PURCHASENET) DESC,
			TOTALMARGIN.PharmaName ASC

 END