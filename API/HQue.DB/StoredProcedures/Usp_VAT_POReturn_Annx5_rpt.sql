 

/*                              
******************************************************************************                            
** File: [Usp_VAT_POReturn_Annx5_rpt]  
** Name: [Usp_VAT_POReturn_Annx5_rpt]                              
** Description: To Get Puchase and Return  details  
**  
** This template can be customized:                              
**                               
** Called by:                               
**                               
**  Parameters:                              
**  Input                Output                              
**  ----------              -----------                              
**  
** Author:Poongodi R  
** Created Date: 28/01/2017  
**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 03/02/2017  Poongodi   Calculation modified    
** 28/03/2017	Poongodi		Taxtype validation added
** 30/03/2017  Poongodi			Invoice date changed to created at
** 22/06/2017   Violet			Prefix Added
** 30/06/2017  Poongodi			TaxRefNovalidation added
*******************************************************************************/ 


--Usp_vat_poreturn_annx5_rpt '18204879-99ff-4efd-b076-f85b4a0da0a3','013513b1-ea8c-4ea8-9fed-054b260ee197','6/20/2017','6/20/2017'
CREATE PROC dbo.Usp_vat_poreturn_annx5_rpt(@AccountId  CHAR(36), 
                                           @InstanceId CHAR(36), 
                                           @StartDate  DATETIME, 
                                           @EndDate    DATETIME) 
AS 
  BEGIN 
      /* 
      Commodity code hard coded based on VAT % 
      VAT    Commoditycode 
      5      2044 
      14.5    301 
      0 or other 752 
       
      Category 
      Purchare - R for all vat % 
       
         
      */ 
	   	  declare @GstDate date ='2017-06-30'
      SELECT [returnno] ReturnNo, 
             [returndate] ReturnDate, 
             [invoicedate] InvoiceDate, 
             [invoiceno] InvoiceNo, 
             [VendorName], 
             [tinno] as TinNo, 
             [vat] VAT, 
             Sum(Isnull(returnvalue, 0) - Isnull([returnvat], 0)) ReturnValue, 
             Sum(Isnull([returnvat], 0))                          ReturnVat, 
             Sum(Isnull([povalue], 0) - Isnull ([podiscount], 0)) [POValue], 
             Sum(Isnull([povat], 0))                              [POVat], 
             CASE Isnull(vat, 0) 
               WHEN 5 THEN '2044' 
               WHEN 14.5 THEN '301' 
               ELSE '752' 
             END 
             [Commoditycode] 
      FROM   (SELECT vendorreturnitem.quantity, 
                     Isnull(vendorreturnitem.quantity, 0) * Isnull( 
                     vendorreturnitem.returnpurchaseprice, 0)    [ReturnValue], 
                     Isnull(vendorreturnitem.quantity, 0) * Isnull( 
                     vendorreturnitem.returnpurchaseprice, 0) * Isnull( 
                     vendorpurchase.discount, 0) / 
                     100 
                     [ReturnDiscount], 
                     ( ( Isnull(vendorreturnitem.quantity, 0) * Isnull( 
                           vendorreturnitem.returnpurchaseprice, 0) ) / ( 
                         100 + Isnull( productstock.vat, 0) ) * 100 ) * Isnull( 
                     productstock.vat, 0) / 
                     100                                         [ReturnVat], 
                     ( Isnull(vendorpurchaseitem.packageqty, vendorreturnitem.Quantity) - 
                       Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
                     vendorpurchaseitem.packagepurchaseprice, (vendorreturnitem.ReturnPurchasePrice-((vendorreturnitem.ReturnPurchasePrice*Isnull( productstock.vat, 0))/(100+Isnull( productstock.vat, 0))))) [POValue], 
                     ( Isnull(vendorpurchaseitem.packageqty, vendorreturnitem.Quantity) - 
                       Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
                     vendorpurchaseitem.packagepurchaseprice, vendorreturnitem.ReturnPurchasePrice) * Isnull( 
                     vendorpurchaseitem.discount, 0) 
                     / 100                                       [PODiscount], 
                     ( ( Isnull(vendorpurchaseitem.packageqty, vendorreturnitem.Quantity) - 
                           Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
                         vendorpurchaseitem.packagepurchaseprice, (vendorreturnitem.ReturnPurchasePrice-((vendorreturnitem.ReturnPurchasePrice*Isnull( productstock.vat, 0))/(100+Isnull( productstock.vat, 0))))) - 
                     ( ( 
                     Isnull( 
                     vendorpurchaseitem.packageqty, vendorreturnitem.Quantity) 
                     - 
                       Isnull(vendorpurchaseitem.freeqty, 0) ) * Isnull( 
                       vendorpurchaseitem.packagepurchaseprice, (vendorreturnitem.ReturnPurchasePrice-((vendorreturnitem.ReturnPurchasePrice*Isnull( productstock.vat, 0))/(100+Isnull( productstock.vat, 0))))) * 
                       Isnull(vendorpurchaseitem.discount, 0) / 100 ) ) * 
                     Isnull(productstock.vat, 0) / 
                     100                                         [POVat], 
                     vendorreturn.returnno                       AS [ReturnNo], 
                     vendorreturn.returndate                     AS [ReturnDate] 
                     , 
                     vendorpurchase.vendorid 
                     AS 
                     [VendorPurchase.VendorId] 
                            , 
                     vendorpurchase.invoicedate                  AS 
                            [InvoiceDate], 
                     Ltrim(Isnull(vendorpurchase.Prefix,'')+' '+vendorpurchase.invoiceno)                    AS [InvoiceNo], 
                     vendor.NAME                                 AS [VendorName] 
                     , 
                     vendor.tinno 
                     AS [TinNo], 
                     productstock.vat                            AS [VAT], 
                     productstock.cst                            AS 
                     [ProductStock.CST] 
              FROM   vendorreturn 
                     INNER JOIN vendorreturnitem 
                             ON vendorreturnitem.vendorreturnid = 
                                vendorreturn.id 
                     LEFT JOIN vendorpurchase 
                             ON vendorpurchase.id = vendorreturn.vendorpurchaseid 
                     LEFT JOIN vendorpurchaseitem 
                             ON vendorpurchaseitem.vendorpurchaseid = vendorpurchase.id 
                                AND vendorpurchaseitem.productstockid = vendorreturnitem.productstockid 
                     INNER JOIN vendor 
                             ON vendor.id = vendorreturn.vendorid 
                     INNER JOIN productstock 
                             ON productstock.id = vendorreturnitem.productstockid 
                     INNER JOIN product 
                             ON product.id = productstock.productid 
              WHERE  cast(vendorreturn.CreatedAt as date) BETWEEN @StartDate AND @EndDate 
					 and isnull(Vendor.TaxType,'Tax') in ( 'Tax','')
                     AND vendorreturn.accountid = @AccountId 
                     AND vendorreturn.instanceid = @InstanceId 
                     AND Isnull(productstock.vat, 0) > 0
					 and isnull(vendorreturn.TaxRefNo,0) =0
					  and CONVERT(DATE, vendorreturn.createdat) <=@GstDate) AS one 
      GROUP  BY [returnno], 
                [returndate], 
                [invoicedate], 
                [invoiceno], 
                [vendorname], 
                [tinno], 
                [vat] 
      ORDER  BY vat, 
                [returnno] 
  END