CREATE Procedure SP_GrnNumberCorrection
as
Begin
	declare @InstanceId char(36)
	declare @id char(36)
	declare @maxgoodsrcvno int
	declare Cur_Grnnumberupdate cursor for 
	select distinct instanceid from vendorpurchase where createdat >='2017-03-03'
	Open Cur_Grnnumberupdate    
	fetch  Cur_Grnnumberupdate into @InstanceId                            
	while (@@fetch_status=0)    
	Begin
		set @maxgoodsrcvno=(select max(cast(goodsrcvno as int)) from vendorpurchase where createdat <'2017-03-03' and instanceid=@InstanceId)
		declare Cur_Grnnumberupdate1 cursor for 
		select id from vendorpurchase where createdat >='2017-03-03' and instanceid=@InstanceId order by createdat
		Open Cur_Grnnumberupdate1    
		fetch  Cur_Grnnumberupdate1 into @id                            
		while (@@fetch_status=0)    
		Begin
			set @maxgoodsrcvno=@maxgoodsrcvno+1
			insert into zGrnnumberupdateverfication
			select id,Accountid,instanceid,goodsrcvno,@maxgoodsrcvno,createdat from vendorpurchase where id=@id and createdat >='2017-03-03' and instanceid=@InstanceId
			update vendorpurchase set goodsrcvno=@maxgoodsrcvno where id=@id and createdat >='2017-03-03' and instanceid=@InstanceId
			fetch  NEXT FROM Cur_Grnnumberupdate1 into @id
		End                            
		close Cur_Grnnumberupdate1                             
		deallocate Cur_Grnnumberupdate1
		fetch  NEXT FROM Cur_Grnnumberupdate into  @InstanceId
	End                            
	close Cur_Grnnumberupdate                             
	deallocate Cur_Grnnumberupdate
End
