﻿CREATE PROCEDURE [dbo].[usp_GetSalesSummaryWise](@AccountId varchar(36),@InstanceId varchar(36),
@StartDate date,@EndDate date,
@PatientName VARCHAR(200),@PatientMobile VARCHAR(20),@PatientId VARCHAR(100),
@UserName VARCHAR(200),@UserId VARCHAR(100),@Type VARCHAR(20))
AS
BEGIN
SET NOCOUNT ON 

if @InstanceId = ''
	SET @InstanceId = NULL
if @StartDate = ''
	SET @StartDate = NULL
if @EndDate = ''
	SET @EndDate = NULL

if @PatientName = ''
	SET @PatientName = NULL
if @PatientMobile = ''
	SET @PatientMobile = NULL
if @PatientId = ''
	SET @PatientId = NULL
if @UserName = ''
	SET @UserName = NULL
if @UserId = ''
	SET @UserId = NULL

select convert(date,sales.invoicedate) AS SalesDate, 
count(distinct sales.id) AS TotalSales,
sum(d.TotalPurchase) AS TotalPurchase,
sum(salesitem.TotalAmount)  AS TotalMRP,
sum(ISNULL(SalesItem.DiscountAmount,0)) AS TotalDiscount, 
CAST(sum(salesitem.TotalAmount)-sum(d.TotalPurchase) AS DECIMAL(18,2)) AS Profit,
Inst.Name AS InstanceName,
CASE WHEN @Type = 'USERWISE' THEN ISNULL(U.Name,'') ELSE '' END AS UserName,
CASE WHEN @Type = 'CUSTOMERWISE' THEN ISNULL(sales.Name,'') ELSE '' END AS PatientName
from sales (NOLOCK)
inner join salesitem (NOLOCK) on sales.id = salesitem.salesid 
inner join productstock (NOLOCK) on salesitem.productstockid = productstock.id 
join Instance Inst (NOLOCK) on Inst.Id = ISNULL(@InstanceId, sales.InstanceId) 
LEFT OUTER JOIN HQueUser U (NOLOCK) ON U.id = sales.CreatedBy
CROSS APPLY (SELECT CAST(ProductStock.PurchasePrice*salesitem.Quantity AS DECIMAL(18,6)) AS TotalPurchase) AS d
WHERE sales.AccountId = @AccountId AND sales.InstanceId = ISNULL(@InstanceId,sales.InstanceId) 
AND Convert(date,sales.invoicedate) BETWEEN ISNULL(@StartDate,Convert(date,sales.invoicedate)) AND ISNULL(@EndDate,Convert(date,sales.invoicedate)) 
AND ISNULL(sales.PatientId,'') = ISNULL(@PatientId, ISNULL(sales.PatientId,'')) 
AND ISNULL(sales.Name,'') = ISNULL(@PatientName, ISNULL(sales.Name,'')) 
AND ISNULL(sales.Mobile,'') = ISNULL(@PatientMobile, ISNULL(sales.Mobile,'')) 
and ISNULL(U.Name,'') = isnull(@UserName, ISNULL(U.Name,''))
and ISNULL(U.UserId,'') = isnull(@UserId, ISNULL(U.UserId,'')) 
and sales.Cancelstatus is NULL
group by convert(date,invoicedate),Inst.Name,
CASE WHEN @Type = 'USERWISE' THEN ISNULL(U.Name,'') ELSE '' END ,
CASE WHEN @Type = 'CUSTOMERWISE' THEN ISNULL(sales.Name,'') ELSE '' END
order by InstanceName, salesdate desc

SET NOCOUNT OFF
END