/**  
*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
 **21/04/2017  Poongodi     Order by Invoice Number Added
 **19/06/2017   Violet			Invoice Series condition Added 
 **21/06/2017   Violet			Prefix Added 
 ** 13/09/2017	Poongodi R		Instance Name added
 ** 14/09/2017	Poongodi		Int changed to Bigint
 ** 24/10/2017	Sarubala V		Multiple payment implemented
*******************************************************************************/ 
 --Usage : -- usp_GetHomeDeliveryList '013513b1-ea8c-4ea8-9fed-054b260ee197','18204879-99ff-4efd-b076-f85b4a0da0a3','23-Dec-2016','23-Dec-2016'
CREATE PROCEDURE [dbo].[usp_GetHomeDeliveryList](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime, @SearchOption varchar(50),
 @SearchValue varchar(50), @FromInvoice bigint, @ToInvoice bigint)
 AS
 BEGIN

 declare @InvoiceSeries varchar(100) = ''

if (@SearchOption ='1')

select  @InvoiceSeries = '' 

else  if (@SearchOption ='2')

select  @InvoiceSeries = isnull(@SearchValue,'') 
else  if (@SearchOption ='4')

select  @InvoiceSeries = isnull(@SearchValue,'') 

else  if (@SearchOption ='3')

select  @InvoiceSeries = 'MAN'

if(@FromInvoice=0  OR @FromInvoice='')
select  @FromInvoice =null, @ToInvoice=null

if(@ToInvoice=0 OR @ToInvoice='')
select  @ToInvoice =null, @FromInvoice =null

   SET NOCOUNT ON
 
	
IF (@SearchOption ='1')
BEGIN

 select s.Id as SalesId,u.Name as CreatedBy,s.Name,s.Mobile,s.Email,s.InvoiceDate,s.InvoiceNo,s.DeliveryType,s.PaymentType, ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries ,
 case s.PaymentType when 'Multiple' then  f1.cash else 
  (case s.PaymentType when 'Cash' then salesitem.sellingprice else 0 end) end [cash], 
 case s.PaymentType when 'Multiple' then f1.[card] else
 (case s.PaymentType when 'Card' then salesitem.sellingprice else 0 end) end [card], 
 case s.PaymentType when 'Multiple' then f1.cheque else
 (case s.PaymentType when 'Cheque' then salesitem.sellingprice else 0 end) end [cheque], 
 case s.PaymentType when 'Multiple' then f1.credit else isnull(customerpayment.credit,0) end [credit],
 isnull(f1.ewallet,0) [ewallet],isnull(f1.subPayment,'') subPayment,
salesitem.SaleAmount as SellingPrice,
salesitem.RoundoffSaleAmount,
Ins.Name [Branch],
Ins.Area[Area] 

from sales as s(NOLOCK) inner join (
--salesitemtable join
select si.salesid,
--sum(sprice.price1) as sellingprice
s.SaleAmount, 
s.RoundoffSaleAmount,
s.SaleAmount-s.RoundoffSaleAmount as sellingprice 

from  SalesItem as si(NOLOCK) 
--Inner join (select * from ProductStock(NOLOCK) where InstanceId=isnull(@InstanceId,InstanceId) and AccountId=@AccountId) as ps on ps.Id = si.ProductStockId 
left join (select * from sales(NOLOCK) where InstanceId=isnull(@InstanceId,InstanceId) and AccountId=@AccountId)as s on s.Id = si.SalesId 

--cross apply (select (isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) -((isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) * (isnull(s.Discount,0) + ISNULL(si.Discount,0))/100) price1) as sprice

group by si.SalesId,s.SaleAmount,s.RoundoffSaleAmount) as salesitem on salesitem.SalesId = s.id
Inner join (Select * from instance(NOLOCK) where accountid= @AccountId ) Ins on Ins.id = s.InstanceId
left join (
 
select cp.SalesId,sum(cp.Credit) as credit from CustomerPayment as cp(NOLOCK) left join Sales as s(NOLOCK) on s.id = cp.SalesId group by cp.SalesId	) 
as customerpayment on customerpayment.SalesId = s.Id
 left join (select * from HQueUser(NOLOCK) where AccountId=@AccountId) as u on u.id = s.CreatedBy
 left join dbo.udf_getPayments(@AccountId,@InstanceId,@StartDate,@EndDate) f1 on f1.salesid = s.Id

WHERE S.AccountId = @AccountId and S.InstanceId =ISNULL(@InstanceId,s.InstanceId) 
AND Convert(date,S.InvoiceDate) BETWEEN @StartDate AND @EndDate 
AND Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,''))
AND isnull(S.InvoiceSeries,'') =''
AND s.Cancelstatus is NULL
ORDER BY s.CreatedAt desc
 END

 ELSE IF (@SearchOption ='2')
BEGIN

 select s.Id as SalesId,u.Name as CreatedBy,s.Name,s.Mobile,s.Email,s.InvoiceDate,s.InvoiceNo,s.DeliveryType,s.PaymentType,ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries,
 case s.PaymentType when 'Multiple' then  f1.cash else 
  (case s.PaymentType when 'Cash' then salesitem.sellingprice else 0 end) end [cash], 
 case s.PaymentType when 'Multiple' then f1.[card] else
 (case s.PaymentType when 'Card' then salesitem.sellingprice else 0 end) end [card], 
 case s.PaymentType when 'Multiple' then f1.cheque else
 (case s.PaymentType when 'Cheque' then salesitem.sellingprice else 0 end) end [cheque], 
 case s.PaymentType when 'Multiple' then f1.credit else isnull(customerpayment.credit,0) end [credit],
 isnull(f1.ewallet,0) [ewallet],isnull(f1.subPayment,'') subPayment,
salesitem.SaleAmount as SellingPrice,
salesitem.RoundoffSaleAmount,
Ins.Name [Branch],
Ins.Area[Area]  

from sales as s(NOLOCK) inner join (
--salesitemtable join
select si.salesid,
--sum(sprice.price1) as sellingprice
s.SaleAmount, 
s.RoundoffSaleAmount,
s.SaleAmount-s.RoundoffSaleAmount as sellingprice 

from  SalesItem as si(NOLOCK) 
--Inner join (select * from ProductStock(NOLOCK) where InstanceId=isnull(@InstanceId,InstanceId) and AccountId=@AccountId) as ps on ps.Id = si.ProductStockId 
left join (select * from sales(NOLOCK) where InstanceId=isnull(@InstanceId,InstanceId) and AccountId=@AccountId)as s on s.Id = si.SalesId 

--cross apply (select (isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) -((isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) * (isnull(s.Discount,0) + ISNULL(si.Discount,0))/100) price1) as sprice

group by si.SalesId,s.SaleAmount,s.RoundoffSaleAmount) as salesitem on salesitem.SalesId = s.id
Inner join (Select * from instance(NOLOCK) where accountid= @AccountId ) Ins on Ins.id = s.InstanceId
left join (
 
select cp.SalesId,sum(cp.Credit) as credit from CustomerPayment as cp(NOLOCK) left join Sales as s(NOLOCK) on s.id = cp.SalesId group by cp.SalesId	) 
as customerpayment on customerpayment.SalesId = s.Id
 left join (Select * from HQueUser(nolock) where accountid= @AccountId ) as u on u.id = s.CreatedBy
 left join dbo.udf_getPayments(@AccountId,@InstanceId,@StartDate,@EndDate) f1 on f1.salesid = s.Id

WHERE S.AccountId = @AccountId AND S.InstanceId =ISNULL(@InstanceId,s.InstanceId)
AND Convert(date,S.InvoiceDate) BETWEEN @StartDate AND @EndDate 
AND Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,''))
AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%' 
AND isnull(S.InvoiceSeries,'') !='' AND isnull(S.InvoiceSeries,'') !='MAN'
and s.Cancelstatus is NULL
ORDER BY s.CreatedAt desc
 END
 
 ELSE IF (@SearchOption ='4')
BEGIN

 select s.Id as SalesId,u.Name as CreatedBy,s.Name,s.Mobile,s.Email,s.InvoiceDate,s.InvoiceNo,s.DeliveryType,s.PaymentType,ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries,
 case s.PaymentType when 'Multiple' then  f1.cash else 
  (case s.PaymentType when 'Cash' then salesitem.sellingprice else 0 end) end [cash], 
 case s.PaymentType when 'Multiple' then f1.[card] else
 (case s.PaymentType when 'Card' then salesitem.sellingprice else 0 end) end [card], 
 case s.PaymentType when 'Multiple' then f1.cheque else
 (case s.PaymentType when 'Cheque' then salesitem.sellingprice else 0 end) end [cheque], 
 case s.PaymentType when 'Multiple' then f1.credit else isnull(customerpayment.credit,0) end [credit],
 isnull(f1.ewallet,0) [ewallet],isnull(f1.subPayment,'') subPayment,
salesitem.SaleAmount as SellingPrice,
salesitem.RoundoffSaleAmount,
Ins.Name [Branch],
Ins.Area[Area]  

from sales as s(NOLOCK) inner join (
--salesitemtable join
select si.salesid,
--sum(sprice.price1) as sellingprice
s.SaleAmount, 
s.RoundoffSaleAmount,
s.SaleAmount-s.RoundoffSaleAmount as sellingprice

from  SalesItem as si(NOLOCK) 
--Inner join (select * from ProductStock(NOLOCK) where InstanceId=isnull(@InstanceId,InstanceId) and AccountId=@AccountId) as ps on ps.Id = si.ProductStockId 
left join (select * from sales(NOLOCK) where InstanceId=isnull(@InstanceId,InstanceId) and AccountId=@AccountId) as s on s.Id = si.SalesId 

--cross apply (select (isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) -((isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) * (isnull(s.Discount,0) + ISNULL(si.Discount,0))/100) price1) as sprice

group by si.SalesId,s.SaleAmount,s.RoundoffSaleAmount) as salesitem on salesitem.SalesId = s.id
Inner join (Select * from instance(nolock) where accountid= @AccountId ) Ins on Ins.id = s.InstanceId
left join (
 
select cp.SalesId,sum(cp.Credit) as credit from CustomerPayment as cp(NOLOCK) left join Sales as s(NOLOCK) on s.id = cp.SalesId group by cp.SalesId	) 
as customerpayment on customerpayment.SalesId = s.Id
 left join (Select * from HQueUser(nolock) where accountid= @AccountId ) as u on u.id = s.CreatedBy
 left join dbo.udf_getPayments(@AccountId,@InstanceId,@StartDate,@EndDate) f1 on f1.salesid = s.Id

WHERE S.AccountId = @AccountId and S.InstanceId = ISNULL(@InstanceId,s.InstanceId) 
AND Convert(date,S.InvoiceDate) BETWEEN @StartDate AND @EndDate 
AND Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,'')) 
AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%' 
AND isnull(S.InvoiceSeries,'') !='' AND isnull(S.InvoiceSeries,'') !='MAN'
AND s.Cancelstatus is NULL
ORDER BY s.CreatedAt desc
 END

 ELSE 
BEGIN

 select s.Id as SalesId,u.Name as CreatedBy,s.Name,s.Mobile,s.Email,s.InvoiceDate,s.InvoiceNo,s.DeliveryType,s.PaymentType,ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries , 
 case s.PaymentType when 'Multiple' then  f1.cash else 
  (case s.PaymentType when 'Cash' then salesitem.sellingprice else 0 end) end [cash], 
 case s.PaymentType when 'Multiple' then f1.[card] else
 (case s.PaymentType when 'Card' then salesitem.sellingprice else 0 end) end [card], 
 case s.PaymentType when 'Multiple' then f1.cheque else
 (case s.PaymentType when 'Cheque' then salesitem.sellingprice else 0 end) end [cheque], 
 case s.PaymentType when 'Multiple' then f1.credit else isnull(customerpayment.credit,0) end [credit],
 isnull(f1.ewallet,0) [ewallet],isnull(f1.subPayment,'') subPayment,
salesitem.SaleAmount as SellingPrice,
salesitem.RoundoffSaleAmount,
Ins.Name [Branch],
Ins.Area[Area]  

from sales as s(NOLOCK) inner join (
--salesitemtable join
select si.salesid,
--sum(sprice.price1) as sellingprice 
s.SaleAmount, 
s.RoundoffSaleAmount,
s.SaleAmount-s.RoundoffSaleAmount as sellingprice

from  SalesItem(NOLOCK) as si 
--Inner join (select * from ProductStock(NOLOCK) where InstanceId=isnull(@InstanceId,InstanceId) and AccountId=@AccountId) as ps on ps.Id = si.ProductStockId 
left join (select * from sales(NOLOCK) where AccountId=@AccountId AND InstanceId=isnull(@InstanceId,InstanceId)) as s on s.Id = si.SalesId 

--cross apply (select (isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) -((isnull(si.SellingPrice,ps.SellingPrice) * si.Quantity) * (isnull(s.Discount,0) + ISNULL(si.Discount,0))/100) price1) as sprice

group by si.SalesId,s.SaleAmount,s.RoundoffSaleAmount) as salesitem on salesitem.SalesId = s.id
Inner join (Select * from instance(NOLOCK) where accountid= @AccountId ) Ins on Ins.id = s.InstanceId
left join (select cp.SalesId,sum(cp.Credit) as credit from CustomerPayment as cp(NOLOCK) left join Sales as s(NOLOCK) on s.id = cp.SalesId group by cp.SalesId) 
as customerpayment on customerpayment.SalesId = s.Id
left join (Select * from HQueUser(NOLOCK) where accountid= @AccountId ) as u on u.id = s.CreatedBy
left join dbo.udf_getPayments(@AccountId,@InstanceId,@StartDate,@EndDate) f1 on f1.salesid = s.Id

WHERE S.AccountId = @AccountId and S.InstanceId = ISNULL(@InstanceId,s.InstanceId) 
AND Convert(date,S.InvoiceDate) BETWEEN @StartDate AND @EndDate 
AND Isnull(S.InvoiceNo,'') BETWEEN isnull(@FromInvoice,Isnull(S.InvoiceNo,'')) AND isnull(@ToInvoice,Isnull(S.InvoiceNo,''))
AND isnull(S.InvoiceSeries,'')+isnull( S.InvoiceNo,'') like isnull(@InvoiceSeries,'')+'%'
AND s.Cancelstatus is NULL 
ORDER BY s.CreatedAt desc
 END

 END
