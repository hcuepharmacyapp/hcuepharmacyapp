 --Usage : -- usp_GetSalesReportList '013513b1-ea8c-4ea8-9fed-054b260ee197','18204879-99ff-4efd-b076-f85b4a0da0a3','01-Dec-2016','23-Dec-2016'
/** Date        Author          Description                              
*******************************************************************************        
 **21/06/2017   Violet			Prefix Added 
 ** 06/07/2017	Poongodi		GST % Added 
 ** 31/07/2017	Poongodi		Inward date added 
 ** 13/09/2017	Poongodi R		Instance Name added
 ** 21/09/2017   Lawrence		bill discount not showing issue fixed 
 ** 03/10/2017   Lawrence		Item and Bill wise discount issue fixed
  ** 02/01/2018  nandhini		KindName  field added
*******************************************************************************/ 
 create PROCEDURE [dbo].[usp_GetSalesReportList](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime,@Kind varchar(36))
 AS
 BEGIN

   SET NOCOUNT ON

  SELECT S.InvoiceDate,S.InvoiceNo,ltrim(isnull(s.prefix,'')+isnull(s.InvoiceSeries, '')) as InvoiceSeries,
  --case when isnull(S.Discount,0) > 0 then S.Discount else SI.Discount end 
   --0.00 SalesDiscount,
   S.Name,S.DoctorName,S.Address,
  SI.Quantity,  
  Case when isnull(SI.Discount ,0)>0 then isnull(SI.Discount ,0) else isnull(S.Discount,0) end as Discount,
  --CONVERT(decimal(18,2),ISNULL(SI.SellingPrice, PS.SellingPrice)) As SellingPrice, --SI.Discount,
  SI.SellingPrice,
 
  PS.BatchNo,PS.ExpireDate,
  --case isnull(s.taxrefno,0)  when 1 then isnull(ps.GstTotal,isnull(SI.Gsttotal,0)) else  PS.VAT end VAT,
  case isnull(s.taxrefno,0)  when 1 then isnull(SI.Gsttotal,0) else PS.VAT end VAT,
  PS.Stock,PS.PurchasePrice,
  P.Name as ProductName, P.Manufacturer, P.Schedule,P.Type,isnull(P.KindName,'')as KindName ,
  --CONVERT(decimal(18,2),ISNULL(SI.SellingPrice, PS.SellingPrice) *SI.quantity) As InvoiceAmount,
  --convert(decimal(18,2),Case when isnull(S.Discount ,0)>0 then isnull(S.Discount ,0) else isnull(SI.Discount,0) end) as Discount, 
   --ISNULL(ISNULL(VPI.PurchasePrice,PS.PurchasePrice) * (PS.Stock),0)  As CostPrice,
  --(SI.Quantity * ISNULL(SI.SellingPrice, PS.SellingPrice) * case isnull(SI.Discount,0) when 0 then isnull(S.Discount,0) else SI.Discount end / 100) as DiscountSum,
  ISNULL(SI.DiscountAmount,0) as DiscountSum,
  cast(ps.CreatedAt   as date) [InwardDate],
  isnull(p.HsnCode,ps.HsnCode) As [HsnCode] , Ins.Name [Branch],
  SI.TotalAmount,
  d.TotalCostPrice AS TotalCostPrice,
  d.TaxAmount AS TaxAmount,
  SI.TotalAmount-d.TaxAmount AS AmountWOTTax,
  CASE WHEN d.TotalCostPrice > 0 THEN (SI.TotalAmount-d.TotalCostPrice)/d.TotalCostPrice ELSE 0 END*100 AS SalesProfit
FROM Sales S WITH(NOLOCK)
Inner join (Select * from instance WITH(NOLOCK) where accountid= @AccountId ) Ins on Ins.id = s.InstanceId
INNER JOIN SalesItem SI WITH(NOLOCK) on S.id= SI.salesid
INNER JOIN ProductStock PS WITH(NOLOCK) on PS.Id=SI.productstockid
INNER JOIN  Product p WITH(NOLOCK) on p.Id=ps.ProductId
--LEFT OUTER JOIN (SELECT  ProductStockId, min(PurchasePrice) as PurchasePrice 
--				FROM  VendorPurchaseItem (NOLOCK)
--				GROUP BY ProductStockId) VPI on PS.id=VPI.ProductStockId 	
CROSS APPLY (SELECT CASE ISNULL(s.taxrefno,0) WHEN 1 THEN ISNULL(SI.GstAmount,0) ELSE ISNULL(SI.VatAmount,0) END AS TaxAmount, SI.Quantity*PS.PurchasePrice AS TotalCostPrice) d				
WHERE S.AccountId = @AccountId and S.InstanceId = ISNULL(@InstanceId,S.InstanceId) 
AND Convert(date,S.invoicedate) BETWEEN @StartDate AND @EndDate 
AND S.Cancelstatus is NULL
and ISNULL(P.KindName,'')= isnull(@Kind,ISNULL(P.KindName,''))
 	ORDER BY S.CreatedAt desc

 END