/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 25/07/2017	Poongodi		SP Optimized  
** 06/09/2017	Poongodi		Import Qty taken from productstock based on created date
** 15/11/2017	Poongodi		OPening and Closing stock taken from closing stock table  
** 06/12/2017	Poongodi		Deleted qty reduced in Purchase qty as per sundar suggestion
*******************************************************************************/ 
Create PROCEDURE [dbo].[Usp_getstockmovement_rpt](@InstanceId VARCHAR(36), 
                                                 @AccountId  VARCHAR(36), 
                                                 @FromDate   DATE, 
                                                 @ToDate     DATE,
												 @SysDate date  )
AS 
  BEGIN 
   Declare @IsDisplayCurStock int =0 
  
 if not exists (select * from schedulerlog (nolock) where InstanceId =@InstanceId and  (ExecutedDate  <=@FromDate or ExecutedDate <=@ToDate)  )
 begin
 if exists( select * from schedulerlog (nolock) where InstanceId =@InstanceId and AccountId =@AccountId)
 begin
  select top 1 -1 StockExecuteStatus,  'Opening/Closing Stock Available only from ' + CONVERT (varchar(10), ExecutedDate ,103)  ClosingStockMessge from schedulerlog (nolock) where InstanceId =@InstanceId and AccountId =@AccountId  order by ExecutedDate asc 
 end 
 else
 begin
   select  -1 StockExecuteStatus,  'Opening/Closing Stock not synced / generated for the selected Branch'   ClosingStockMessge  

 end 
 return 
 end 
  
 if DATEDIFF(d,@todate,@SysDate) >= 0 set @IsDisplayCurStock =1

  SELECT 0 StockExecuteStatus, t.productid [ProductId], p.name [ProductName] ,   
  max(isnull(opn.ClosingQty,0)) OpeningQty,
  --dbo.Udf_getopeningstock(@InstanceId, t.productid, 
  --           Cast(@FromDate AS DATE)) ,
	sum(isnull(importqty,0)) [ImportQty], sum(isnull(newstockqty,0)) [CreatedOpeningStockQty],	
	sum(isnull(si,0)) [SalesQty],	sum(isnull(sr,0)) [SalesReturnQty],	sum(isnull(vp,0)) [PurchaseQty],
		sum(isnull(vr,0)) [PurchaseReturnQty],	sum(isnull(st1,0)) [TransferOutQty],	
		sum(isnull(st,0)) [TransferInQty],	sum(isnull(adjOut,0)) [AdjustmentOutQty],	
		sum(isnull(adjin,0)) [AdjustmentInQty],	
		sum(isnull(dc,0)) [DCQty],
		sum(isnull(temp,0)) [TempStockQty],
		sum(isnull(consumption,0)) [SelfConsumption],
		sum( isnull(temp ,0) + isnull(dc ,0) + isnull(importqty,0)+isnull(st,0) +isnull( newstockqty,0) +isnull(vp,0)+isnull(sr,0)+isnull(adjin,0) )[TotalInQty],
	   sum(isnull(si,0)+isnull(vr,0)+isnull(st1,0)+isnull( adjOut,0)+  isnull(consumption,0)) [TotalOutQty] ,
	--sum(isnull(TotalInQty,0)) [TotalInQty],
	--sum(isnull(TotalOutQty,0)) [TotalOutQty],
		--sum(isnull(actual,0)) [ClosingQty],
		case @IsDisplayCurStock when 1 then max(isnull(ps.stock,0)) else  max(isnull(Closing.ClosingQty,0)) end ClosingQty,
		max(isnull(ps.stock,0)) [CurrentStock],
		sum(isnull(actual,0))  - sum(isnull(ps.stock,0)) diff
 FROM (
SELECT ps.id,ps.BatchNo,ps.ExpireDate,ps.productid,imp.importqty,qty.newstockqty, 
 ps.stock, si.quantity si, sr.quantity sr, (isnull(vp.quantity ,0) - isnull(vpd.quantity,0))  vp, isnull(vr.quantity ,0)  vr, st.quantity st, st1.quantity st1,
 ad.adjustedstock adjOut,
  ad1.adjustedstock adjin,
 dc.quantity dc ,
 temp.quantity temp,
 isnull(consumption.quantity,0) consumption,
  /* isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(qty.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)+isnull(ad1.adjustedstock,0) [TotalInQty],
   isnull(si.quantity,0)-isnull(vr.quantity,0)+isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0) [TotalOutQty] ,*/
  isnull(temp.quantity ,0) + isnull(dc.quantity ,0) + isnull(imp.importqty,0) +isnull( qty.newstockqty,0) +isnull(vp.quantity,0)+isnull(sr.quantity,0)-isnull(si.quantity,0)-isnull(vr.quantity,0)-isnull(st1.quantity,0)+isnull(st.quantity,0)+isnull(ad.adjustedstock,0)+isnull(ad1.adjustedstock,0)  -isnull(consumption.quantity,0) actual FROM
(select * from productstock (nolock) where instanceid = @InstanceId and cast(updatedat as date) between @FromDate and @ToDate ) ps LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) between @FromDate and @ToDate  group by productstockid) si ON si.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from salesreturnitem (nolock) where instanceid = @InstanceId  and cast(updatedat as date) between @FromDate and @ToDate group by productstockid) sr ON sr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorpurchaseitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) between @FromDate and @ToDate  and vendorpurchaseid in (select id from vendorpurchase(nolock) where instanceid = @InstanceId and cast(updatedat as date) between @FromDate and @ToDate  and isnull(cancelstatus,0) =0)  group by productstockid) vp ON vp.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorpurchaseitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) between @FromDate and @ToDate   and status=2    group by productstockid) vpd ON vpd.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from vendorreturnitem (nolock) where instanceid = @InstanceId and cast(updatedat as date) between @FromDate and @ToDate  group by productstockid) vr ON vr.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus in(1,3) AND s1.instanceid = @InstanceId  and cast(s1.updatedat as date) between @FromDate and @ToDate  group by s1.productstockid) st1 ON st1.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, toproductstockid from stocktransferitem (nolock) s1 INNER JOIN stocktransfer (nolock) s2 ON s2.id = s1.transferid where transferstatus = 3 AND s1.toinstanceid = @InstanceId  and cast(s1.updatedat as date) between @FromDate and @ToDate  group by s1.toproductstockid) st ON st.toproductstockid = ps.id LEFT OUTER JOIN 
(select sum(abs(adjustedstock)) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) between @FromDate and @ToDate and adjustedstock<0  group by productstockid) ad ON ad.productstockid = ps.id LEFT OUTER JOIN 
(select sum(adjustedstock) adjustedstock, productstockid from stockadjustment (nolock) where instanceid = @InstanceId  and cast(updatedat as date) between @FromDate and @ToDate  and adjustedstock>0 group by productstockid) ad1 ON ad1.productstockid = ps.id LEFT OUTER JOIN 
(select sum(quantity) quantity, productstockid from dcvendorpurchaseitem (nolock) where instanceid = @InstanceId    AND Isnull(ispurchased, 0) = 0  and cast(updatedat as date) between @FromDate and @ToDate  group by productstockid) dc ON dc.productstockid = ps.id LEFT OUTER JOIN  
(select sum(quantity) quantity, productstockid from tempvendorpurchaseitem (nolock) where instanceid = @InstanceId  AND isactive = 1   and cast(updatedat as date) between @FromDate and @ToDate  group by productstockid) temp ON temp.productstockid = ps.id  LEFT OUTER JOIN
(select sum(Consumption) quantity, productstockid from SelfConsumption (nolock) where instanceid = @InstanceId   and cast(updatedat as date)  between @FromDate and @ToDate    group by productstockid) consumption ON consumption.productstockid = ps.id  LEFT OUTER JOIN
(select sum(importqty) importqty, id productstockid from productstock (nolock) where instanceid = @InstanceId   and cast(Createdat as date) between @FromDate and @ToDate  and isnull(StockImport,0) =1  group by id) imp ON imp.productstockid = ps.id  
/* and  Isnull(newopenedstock, 0)  = 0 */
cross apply (select case  Isnull(newopenedstock, 0) when 1   then Isnull(newstockqty, 0) else 0 end  newstockqty) [qty]
) t  inner join (select * from product where accountid =@accountid) p on p.id = t.productid
inner join (Select productid, sum(stock) stock from productstock(nolock) where instanceid =@InstanceId and stock >0 group by productid) ps on ps.ProductId = p.id
 Left join (Select * from Udf_GetOpeningStock(@InstanceId,@AccountId ,@FromDate) ) Opn on Opn.Productid = t.ProductId
  Left join (Select * from Udf_GetClosingStock(@InstanceId,@AccountId ,@ToDate) ) Closing on Closing.Productid = t.ProductId
group by t.productid,  p.name   order by 3

  /*
      Create TABLE #Outuput  
        ( 
           ProductId              CHAR(36), 
           OpeningQty             INT, 
           ImportQty              INT, 
           CreatedOpeningStockQty INT, 
           PurchaseQty            INT, 
           SalesQty               INT, 
           PurchaseReturnQty      INT, 
           SalesReturnQty         INT, 
           AdjustmentOutQty       INT, 
           AdjustmentInQty        INT, 
           TransferOutQty         INT, 
           TransferInQty          INT, 
           DCQty                  INT, 
           TempStockQty           INT 
        )  
      Create TABLE #Trans
        ( 
           ProductId              CHAR(36), 
           OpeningQty             INT, 
           ImportQty              INT, 
           CreatedOpeningStockQty INT, 
           PurchaseQty            INT, 
           SalesQty               INT, 
           PurchaseReturnQty      INT, 
           SalesReturnQty         INT, 
           AdjustmentOutQty       INT, 
           AdjustmentInQty        INT, 
           TransferOutQty         INT, 
           TransferInQty          INT, 
           DCQty                  INT, 
           TempStockQty           INT 
        ) 
	 
	 
	  
	  insert into #Trans(ProductId,ImportQty)
	  
	  SELECT productid,
                     Sum(importqty) ImportQty 
                     
              FROM productstock (nolock)  
              WHERE  instanceid = @Instanceid 
			  AND accountid =@AccountId 
                     AND stockimport = 1 
					    AND Isnull(newopenedstock, 0) = 0
                     AND Cast(createdat AS DATE) BETWEEN @FromDate AND @ToDate 
              GROUP  BY productid 
			  /*Opening */
                insert into #Trans(ProductId,CreatedOpeningStockQty)
              SELECT productid ,
                     Sum(Isnull(newstockqty, 0)) [CreatedStock]
                    
              FROM   productstock (nolock)
              WHERE  instanceid = @Instanceid 
			   AND accountid =@AccountId 
                     AND Isnull(newopenedstock, 0) = 1 
                     AND Cast(createdat AS DATE) BETWEEN @FromDate AND @ToDate 
              GROUP  BY productid 
           	  /*PO */
			  insert into #Trans(ProductId,PurchaseQty)
              SELECT productid, 
                     Sum(quantity) POQty  
              FROM  (select * from  vendorpurchaseitem  (nolock)   WHERE  instanceid = @Instanceid 
			   AND accountid =@AccountId  AND Isnull(status, 0) = 0 
                     AND Cast(updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate ) vp
                     INNER JOIN (select * from vendorpurchase(nolock) WHERE  instanceid = @Instanceid 
			   AND accountid =@AccountId     AND Isnull(cancelstatus, 0) = 0 ) v 
                             ON v.id = vp.vendorpurchaseid  inner join
                (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = vp.ProductStockId
              GROUP  BY productid 
			  	  /*Purchase Return */
       insert into #Trans(ProductId,PurchaseReturnQty)
              SELECT productid, 
                     Sum(quantity) PRQty
              FROM  (select * from  vendorreturnitem  (nolock) WHERE  instanceid = @Instanceid 
			   AND accountid =@AccountId  AND Cast(updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate ) vp
                     INNER JOIN (select * from vendorreturn(nolock)  WHERE  instanceid = @Instanceid 
			   AND accountid =@AccountId  )v 
                             ON v.id = vp.vendorreturnid 
           inner join
                (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = vp.ProductStockId
              GROUP  BY productid 
             /*sales*/
			    insert into #Trans(ProductId,SalesQty)
              SELECT productid, 
                   Sum(quantity) SaleQty  
                     
              FROM   (select * from salesitem  (nolock) WHERE  instanceid = @Instanceid 
			   AND accountid =@AccountId   AND Cast(updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate ) si INNER JOIN 
				(select * from sales (nolock)WHERE  instanceid = @Instanceid 
			   AND accountid =@AccountId     AND Isnull(cancelstatus, 0) = 0 ) s
                             ON s.id = si.salesid 
                             
               inner join
                (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = si.ProductStockId
              GROUP  BY productid 
                    
       
               /*sales Return*/
			  insert into #Trans(ProductId,SalesReturnQty)
              SELECT productid, 
                     Sum(quantity) SRQty
              FROM   (select * from salesreturnitem (nolock)  where instanceid = @Instanceid 
			   AND accountid =@AccountId  AND Isnull(isdeleted, 0) = 0     AND Cast(updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate )  si 
                     INNER JOIN (select * from salesreturn  (nolock) where   instanceid = @Instanceid 
			   AND accountid =@AccountId and isnull(canceltype,0)  = 0) s
                             ON s.id = si.salesreturnid 
			inner join
                (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = si.ProductStockId
              GROUP  BY productid 

              insert into #Trans(ProductId,TransferOutQty)
              SELECT ps.productid,
                      Sum(quantity) TransferQty

              FROM  (select * from stocktransferitem (nolock)  where instanceid = @Instanceid 
			   AND accountid =@AccountId AND Cast(updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate )si 
                     INNER JOIN (select * from stocktransfer(nolock) where instanceid = @Instanceid 
			   AND accountid =@AccountId  AND Isnull(transferstatus, 0) IN ( 1, 3 ) ) s  
                             ON s.id = si.transferid 
                inner join
                (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = si.ProductStockId
              GROUP  BY ps.productid 
               
 /*Transfer in */
              insert into #Trans(ProductId,TransferInQty)
              SELECT  ps.productid,
                   Sum(quantity) [AcceptQty] 
           
              FROM  (select * from stocktransferitem (nolock)  where toinstanceid = @Instanceid 
			   AND accountid =@AccountId AND Cast(updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate )si 
                     INNER JOIN (select * from stocktransfer(nolock) where instanceid = @Instanceid 
			   AND accountid =@AccountId  AND Isnull(transferstatus, 0) IN ( 3 ) ) s  
                             ON s.id = si.transferid 
              inner join
                (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = si.ProductStockId
              GROUP  BY ps.productid 
            
                 insert into #Trans(ProductId,adjustmentinqty)
              SELECT  ps.productid, 
                     Sum(adjustedstock) AdjQty 
                   
              FROM (select * from  stockadjustment  (nolock) where instanceid = @Instanceid 
			   AND accountid =@AccountId  AND Isnull(adjustedstock, 0) > 0 
                     AND Cast(updatedat AS DATE) BETWEEN @FromDate AND @ToDate  ) s
                  inner join
                (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = s.ProductStockId
              GROUP  BY ps.productid 
            
           
                 insert into #Trans(ProductId,AdjustmentOutQty)
              SELECT  ps.productid,Sum(Abs(adjustedstock)) AdjOutQty 
              FROM (select * from  stockadjustment  (nolock) where instanceid = @Instanceid 
			   AND accountid =@AccountId  AND Isnull(adjustedstock, 0) <0 
                     AND Cast(updatedat AS DATE) BETWEEN @FromDate AND @ToDate  ) s
                  inner join
                (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = s.ProductStockId
              GROUP  BY ps.productid 

               insert into #Trans(ProductId,DCQty)
              SELECT  ps.productid, 
                     Sum(quantity) DCQty 
              FROM   (select * from dcvendorpurchaseitem (nolock) where instanceid = @Instanceid 
			   AND accountid =@AccountId   AND isactive = 1 
                     AND Isnull(ispurchased, 0) = 0 
                     AND Cast(updatedat AS DATE) BETWEEN @FromDate AND @ToDate ) d 
					 inner join      (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = d.ProductStockId
              GROUP  BY ps.productid 

			 insert into #Trans(ProductId,TempStockQty)
              SELECT productid, 
                     Sum(quantity) TempQty 
              FROM   (select * from tempvendorpurchaseitem (nolock) where instanceid = @Instanceid 
			   AND accountid =@AccountId  AND isactive = 1 
                     AND Cast(updatedat AS DATE) BETWEEN @FromDate AND @ToDate ) t inner join
			 (select * from productstock where instanceid = @Instanceid 
			   AND accountid =@AccountId ) ps on ps.id = t.ProductStockId
              GROUP  BY ps.productid 

   
  
      INSERT INTO #Outuput 
                  (productid, 
                   openingqty, 
                   importqty, 
                   createdopeningstockqty, 
                   purchaseqty, 
                   salesqty, 
                   purchasereturnqty, 
                   salesreturnqty, 
                   adjustmentinqty, 
                   transferoutqty, 
                   transferinqty, 
                   dcqty, 
                   tempstockqty, 
                   adjustmentoutqty) 
      SELECT productid, 
             dbo.Udf_getopeningstock(@InstanceId, ps.productid, 
             Cast(@FromDate AS DATE)) 
             [OpeningStock], 
          
sum(Isnull( importqty ,0)),
 
sum(Isnull( createdopeningstockqty ,0)),
sum(Isnull( purchaseqty ,0)),
sum(Isnull( salesqty ,0)),
sum(Isnull( purchasereturnqty ,0)),
sum(Isnull( salesreturnqty ,0)),
sum(Isnull( adjustmentinqty ,0)),
sum(Isnull( transferoutqty ,0)),
sum(Isnull( transferinqty ,0)),
sum(Isnull( dcqty ,0)),
sum(Isnull( tempstockqty ,0)),
sum(Isnull( adjustmentoutqty ,0))
      FROM       #Trans Ps   group by productid


	  

      SELECT P.NAME                           [ProductName], 
             Ps.*, 
             stk.totalinqty TotalInQty, 
             stk.totaloutqty TotalOutQty, 
             stk.totalinqty - stk.totaloutqty ClosingQty, 
             cur.currentstock  CurrentStock
      FROM   #Outuput Ps 
             INNER JOIN product P (nolock)
                     ON P.id COLLATE DATABASE_DEFAULT = Ps.productid  COLLATE DATABASE_DEFAULT
                        AND P.accountid = @AccountId 
             LEFT JOIN dbo.Udf_getcurrentstock(@instanceid, @AccountId) cur 
                    ON cur.productid COLLATE DATABASE_DEFAULT = ps.productid  COLLATE DATABASE_DEFAULT
             CROSS apply (SELECT ( openingqty + createdopeningstockqty 
                                   + importqty + purchaseqty + salesreturnqty 
                                   + adjustmentinqty + transferinqty + dcqty 
                                   + tempstockqty ) 
                                 [TotalInQty], 
                                 ( salesqty + purchasereturnqty 
                                   + adjustmentoutqty + transferoutqty ) 
                                 TotalOutQty) 
                         Stk  
     Create TABLE #Outuput  
        ( 
           ProductId              CHAR(36), 
           OpeningQty             INT, 
           ImportQty              INT, 
           CreatedOpeningStockQty INT, 
           PurchaseQty            INT, 
           SalesQty               INT, 
           PurchaseReturnQty      INT, 
           SalesReturnQty         INT, 
           AdjustmentOutQty       INT, 
           AdjustmentInQty        INT, 
           TransferOutQty         INT, 
           TransferInQty          INT, 
           DCQty                  INT, 
           TempStockQty           INT 
        ) 
	 
      INSERT INTO #Outuput 
                  (productid, 
                   openingqty, 
                   importqty, 
                   createdopeningstockqty, 
                   purchaseqty, 
                   salesqty, 
                   purchasereturnqty, 
                   salesreturnqty, 
                   adjustmentinqty, 
                   transferoutqty, 
                   transferinqty, 
                   dcqty, 
                   tempstockqty, 
                   adjustmentoutqty) 
      SELECT productid, 
             dbo.Udf_getopeningstock(@InstanceId, ps.productid, 
             Cast(@FromDate AS DATE)) 
             [OpeningStock], 
             Sum (isnull(a.importqty,0)), 
             Sum (isnull(a.createdstock,0)), 
             Sum(isnull(poqty,0)), 
             Sum(isnull(saleqty,0)),  
             Sum(isnull(prqty,0)), 
             Sum(isnull(srqty,0)), 
             Sum(isnull(adjinqty,0)), 
             Sum(isnull(transferqty,0)), 
             Sum(isnull(acceptqty,0)), 
             Sum(isnull(dcqty,0)), 
             Sum(isnull(tempqty,0)), 
             Sum(isnull(adjoutqty,0))
      FROM       productstock Ps  
	  Left JOIN (SELECT id             [ProductStockId], 
                     Sum(importqty) ImportQty, 
                     0              [CreatedStock], 
                     0              POQty, 
                     0              SaleQty, 
                     0              PRQty, 
                     0              SRQty, 
                     0              AdjInQty, 
                     0              TransferQty, 
                     0              AcceptQty, 
                     0              DCQty, 
                     0              TempQty, 
                     0              AdjOutQty 
              FROM   productstock (nolock)
              WHERE  instanceid = @Instanceid 
                     AND stockimport = 1 
					    AND Isnull(newopenedstock, 0) = 0
                     AND Cast(createdat AS DATE) BETWEEN @FromDate AND @ToDate 
              GROUP  BY id 
              UNION ALL 
              SELECT id                          [ProductStockId], 
                     0                           ImportQty, 
                     Sum(Isnull(newstockqty, 0)) [CreatedStock], 
                     0                           POQty, 
                     0                           SaleQty, 
                     0                           PRQty, 
                     0                           SRQty, 
                     0                           AdjInQty, 
                     0                           TransferQty, 
                     0                           AcceptQty, 
                     0                           DCQty, 
                     0                           TempQty, 
                     0                           AdjOutQty 
              FROM   productstock (nolock)
              WHERE  instanceid = @Instanceid 
                     AND Isnull(newopenedstock, 0) = 1 
                     AND Cast(createdat AS DATE) BETWEEN @FromDate AND @ToDate 
              GROUP  BY id 
              UNION ALL 
              SELECT productstockid, 
                     0             ImportQty, 
                     0             [CreatedStock], 
                     Sum(quantity) POQty, 
                     0             SaleQty, 
                     0             PRQty, 
                     0             SRQty, 
                     0             AdjQty, 
                     0             TransferQty, 
                     0             AcceptQty, 
                     0             DCQty, 
                     0             TempQty, 
                     0             AdjOutQty 
              FROM   vendorpurchaseitem vp (nolock)
                     INNER JOIN vendorpurchase v (nolock)
                             ON v.id = vp.vendorpurchaseid 
                                AND Isnull(v.cancelstatus, 0) = 0 
              WHERE  vp.instanceid = @Instanceid 
                     AND Isnull(vp.status, 0) = 0 
                     AND Cast(vp.updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate 
              GROUP  BY productstockid 
              UNION ALL 
              SELECT productstockid, 
                     0             ImportQty, 
                     0             [CreatedStock], 
                     0             POQty, 
                     0             SaleQty, 
                     Sum(quantity) PRQty, 
                     0             SRQty, 
                     0             AdjQty, 
                     0             TransferQty, 
                     0             AcceptQty, 
                     0             DCQty, 
                     0             TempQty, 
                     0             AdjOutQty 
              FROM   vendorreturnitem vp (nolock)
                     INNER JOIN vendorreturn v (nolock)
                             ON v.id = vp.vendorreturnid 
              WHERE  vp.instanceid = @Instanceid 
                     AND Cast(vp.updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate 
              GROUP  BY productstockid 
              UNION ALL 
              SELECT productstockid, 
                     0             ImportQty, 
                     0             [CreatedStock], 
                     0             POQty, 
                     Sum(quantity) SaleQty, 
                     0             PRQty, 
                     0             SRQty, 
                     0             AdjQty, 
                     0             TransferQty, 
                     0             AcceptQty, 
                     0             DCQty, 
                     0             TempQty, 
                     0             AdjOutQty 
              FROM   salesitem si (nolock)
                     INNER JOIN sales s (nolock)
                             ON s.id = si.salesid 
                                AND Isnull(s.cancelstatus, 0) = 0 
              WHERE  si.instanceid = @Instanceid 
                     AND Cast(si.updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate 
              GROUP  BY productstockid 
              UNION ALL 
              SELECT productstockid, 
                     0             ImportQty, 
                     0             [CreatedStock], 
                     0             POQty, 
                     0             SaleQty, 
                     0             PRQty, 
                     Sum(quantity) SRQty, 
                     0             AdjQty, 
                     0             TransferQty, 
                     0             AcceptQty, 
                     0             DCQty, 
                     0             TempQty, 
                     0             AdjOutQty 
              FROM   salesreturnitem si (nolock)
                     INNER JOIN salesreturn s (nolock)
                             ON s.id = si.salesreturnid 
              WHERE  si.instanceid = @Instanceid 
                     AND Isnull(si.isdeleted, 0) = 0 
					 and isnull(s.canceltype,0)  = 0
                     AND Cast(si.updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate 
              GROUP  BY productstockid 
              UNION ALL 
              SELECT productstockid, 
                     0             ImportQty, 
                     0             [CreatedStock], 
                     0             POQty, 
                     0             SaleQty, 
                     0             PRQty, 
                     0             SRQty, 
                     0             AdjQty, 
                     Sum(quantity) TransferQty, 
                     0             AcceptQty, 
                     0             DCQty, 
                     0             TempQty, 
                     0             AdjOutQty 
              FROM   stocktransferitem si (nolock)
                     INNER JOIN stocktransfer s (nolock)
                             ON s.id = si.transferid 
                                AND Isnull(s.transferstatus, 0) IN ( 1, 3 ) 
              WHERE  si.instanceid = @Instanceid 
                     AND Cast(si.updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate 
              GROUP  BY productstockid 
              UNION ALL 
              SELECT si.toproductstockid, 
                     0             ImportQty, 
                     0             [CreatedStock], 
                     0             POQty, 
                     0             SaleQty, 
                     0             PRQty, 
                     0             SRQty, 
                     0             AdjQty, 
                     0             TransferQty, 
                     Sum(quantity) [AcceptQty], 
                     0             DCQty, 
                     0             TempQty, 
                     0             AdjOutQty 
              FROM   stocktransferitem si (nolock)
                     INNER JOIN stocktransfer s (nolock)
                             ON s.id = si.transferid 
                                AND Isnull(s.transferstatus, 0) IN ( 3 ) 
              WHERE  si.toinstanceid = @Instanceid 
                     AND Cast(si.updatedat AS DATE) BETWEEN 
                         @FromDate AND @ToDate 
              GROUP  BY si.toproductstockid 
              UNION ALL 
              SELECT productstockid, 
                     0                  ImportQty, 
                     0                  [CreatedStock], 
                     0                  POQty, 
                     0                  SaleQty, 
                     0                  PRQty, 
                     0                  SRQty, 
                     Sum(adjustedstock) AdjQty, 
                     0                  TransferQty, 
                     0                  AcceptQty, 
                     0                  DCQty, 
                     0                  TempQty, 
                     0                  AdjOutQty 
              FROM   stockadjustment s (nolock)
              WHERE  s.instanceid = @Instanceid 
                     AND Isnull(adjustedstock, 0) > 0 
                     AND Cast(s.updatedat AS DATE) BETWEEN @FromDate AND @ToDate 
              GROUP  BY productstockid 
              UNION ALL 
              SELECT productstockid, 
                     0                       ImportQty, 
                     0                       [CreatedStock], 
                     0                       POQty, 
                     0                       SaleQty, 
                     0                       PRQty, 
                     0                       SRQty, 
                     0                       AdjQty, 
                     0                       TransferQty, 
                     0                       AcceptQty, 
                     0                       DCQty, 
                     0                       TempQty, 
                     Sum(Abs(adjustedstock)) AdjOutQty 
              FROM   stockadjustment s (nolock)
              WHERE  s.instanceid = @Instanceid 
                     AND Isnull(adjustedstock, 0) < 0 
                     AND Cast(s.updatedat AS DATE) BETWEEN @FromDate AND @ToDate 
              GROUP  BY productstockid 
              UNION ALL 
              SELECT productstockid, 
                     0             ImportQty, 
                     0             [CreatedStock], 
                     0             POQty, 
                     0             SaleQty, 
                     0             PRQty, 
                     0             SRQty, 
                     0             AdjQty, 
                     0             TransferQty, 
                     0             AcceptQty, 
                     Sum(quantity) DCQty, 
                     0             TempQty, 
                     0             AdjOutQty 
              FROM   dcvendorpurchaseitem (nolock)
              WHERE  instanceid = @Instanceid 
                     AND isactive = 1 
                     AND Isnull(ispurchased, 0) = 0 
                     AND Cast(updatedat AS DATE) BETWEEN @FromDate AND @ToDate 
              GROUP  BY productstockid 
              UNION ALL 
              SELECT productstockid, 
                     0             ImportQty, 
                     0             [CreatedStock], 
                     0             POQty, 
                     0             SaleQty, 
                     0             PRQty, 
                     0             SRQty, 
                     0             AdjQty, 
                     0             TransferQty, 
                     0             AcceptQty, 
                     0             DCQty, 
                     Sum(quantity) TempQty, 
                     0             AdjOutQty 
              FROM   tempvendorpurchaseitem (nolock)
              WHERE  instanceid = @Instanceid 
                     AND isactive = 1 
                     AND Cast(updatedat AS DATE) BETWEEN @FromDate AND @ToDate 
              GROUP  BY productstockid) a 
        
                     ON Ps.id = a.productstockid 
                        AND ps.instanceid = @InstanceId 
      GROUP  BY productid 

      SELECT P.NAME                           [ProductName], 
             Ps.*, 
             stk.totalinqty TotalInQty, 
             stk.totaloutqty TotalOutQty, 
             stk.totalinqty - stk.totaloutqty ClosingQty, 
             cur.currentstock  CurrentStock
      FROM   #Outuput Ps 
             INNER JOIN product P (nolock)
                     ON P.id COLLATE DATABASE_DEFAULT = Ps.productid  COLLATE DATABASE_DEFAULT
                        AND P.accountid = @AccountId 
             LEFT JOIN dbo.Udf_getcurrentstock(@instanceid, @AccountId) cur 
                    ON cur.productid COLLATE DATABASE_DEFAULT = ps.productid COLLATE DATABASE_DEFAULT
             CROSS apply (SELECT ( openingqty + createdopeningstockqty 
                                   + importqty + purchaseqty + salesreturnqty 
                                   + adjustmentinqty + transferinqty + dcqty 
                                   + tempstockqty ) 
                                 [TotalInQty], 
                                 ( salesqty + purchasereturnqty 
                                   + adjustmentoutqty + transferoutqty ) 
                                 TotalOutQty) 
                         Stk */
  END 