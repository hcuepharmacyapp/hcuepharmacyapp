-- usp_GetSupplierWiseBalanceDetails '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','d6d25ebf-6868-4682-9e1c-db935da84835','',''
-- usp_GetSupplierWiseBalanceDetails '85d3fdc9-1426-4d3e-8a96-16c78088eaa8','756efa5e-66e1-46ff-a3b2-3f282d746d93','c94169f2-397b-44a8-ba67-d0b7d3d81b6d','01-Sep-2016','21-Sep-2016'
/** Change History                             
*******************************************************************************                            
** Date        Author          Description                             
*******************************************************************************       
 22/06/2017    Violet			Prefix Added
 06/09/2017	   Settu            Optimized to compiled qry instead of dynamic query
 13-10-2017    Senthil.S        Union Query added
*******************************************************************************/ 
Create PROCEDURE [dbo].[usp_GetSupplierWiseBalanceDetails](@InstanceId varchar(36),@AccountId varchar(36),@VendorId char(36),@StartDate datetime,@EndDate datetime)
  AS
 BEGIN
   SET NOCOUNT ON
	declare @condition nvarchar(max)	
	Declare @qry nvarchar(max)
	SET @condition=''
	
	--if(@VendorId is not null AND @VendorId!='')
	--begin
	--	set @condition=@condition+' AND s.Name='+''''+@Name +''''
	--end
	

	--if(@Mobile is not null AND @Mobile!='')
	--begin
	--	set @condition=@condition+' AND s.Mobile='+''''+@Mobile +''''
	--end
	

	IF(@StartDate IS NULL OR @StartDate='')
	BEGIN
		SET @StartDate = NULL
	END
	IF(@EndDate IS NULL OR @EndDate='')
	BEGIN
		SET @EndDate = NULL
	END
	IF(@VendorId IS NULL OR @VendorId='')
	BEGIN
		SET @VendorId = NULL
	END
	SELECT b.* from
	(SELECT VendorPurchase.InvoiceNo As InvoiceNo ,VendorPurchase.GoodsRcvNo As GoodsRcvNo,VendorPurchase.InvoiceDate As InvoiceDate, VendorPurchase.PaymentType As PaymentType,
	(ISNULL(VendorPurchase.Prefix,'')+ISNULL(VendorPurchase.BillSeries,'')) AS BillSeries,
	VendorPurchaseId ,SUM(Debit) AS Debit,dbo.CreditCalculate(VendorPurchaseId) AS Credit,ROUND(dbo.CreditCalculate(VendorPurchaseId),0)-SUM(Debit) as TotalDue,
	VendorPurchase.CreditNoOfDays as[PayableDays],DATEADD(day,VendorPurchase.CreditNoOfDays,VendorPurchase.Createdat) AS [CreditDate],
	DATEDIFF(day,CAST(MIN(Payment.createdat) AS date),Getdate()) as Age,V.Name AS VendorName FROM Payment (nolock)
	INNER JOIN VendorPurchase(nolock) ON Payment.VendorPurchaseId = VendorPurchase.Id
	INNER JOIN Vendor(nolock) V ON V.Id = VendorPurchase.VendorId AND V.Id = Payment.VendorId
	WHERE VendorPurchase.AccountId=@AccountId AND VendorPurchase.InstanceId=@InstanceId
	AND CAST(VendorPurchase.invoicedate AS date) BETWEEN CAST(ISNULL(@StartDate,VendorPurchase.invoicedate) AS DATE) AND CAST(ISNULL(@EndDate,VendorPurchase.invoicedate) AS DATE)
	and VendorPurchase.VendorId = ISNULL(@VendorId,VendorPurchase.VendorId) and isnull(VendorPurchase.CancelStatus ,0)=0 
	and VendorPurchase.PaymentType='Credit'

	GROUP BY V.Name,VendorPurchase.InvoiceNo,VendorPurchase.GoodsRcvNo,VendorPurchase.InvoiceDate, VendorPurchaseId, VendorPurchase.BillSeries,VendorPurchase.Prefix, 
	VendorPurchase.PaymentType,VendorPurchase.CreditNoOfDays,
	VendorPurchase.Createdat--,Convert(date,Payment.createdat)
	HAVING dbo.CreditCalculate(VendorPurchaseId) - SUM(Debit) > 0 
	--Order by V.Name,Convert(int,VendorPurchase.GoodsRcvNo) asc
	UNION
	SELECT '' as InvoiceNo, '' as GoodsRcvNo, Max(p.TransactionDate) as InvoiceDate,
				'' as PaymentType, '' as BillSeries, '' as VendorPurchaseId,
				SUM(p.Debit) AS Debit, SUM(p.Credit) as Credit, (SUM(p.Credit)- SUM(p.Debit)) as TotalDue,
				0  as[PayableDays], Max(p.Createdat) as [CreditDate],
				DATEDIFF(day,CAST(MIN(p.createdat) AS date),Getdate()) as Age,
				V.Name AS VendorName
                From Payment p inner join Vendor V ON p.VendorId = v.Id
                Where  p.VendorId = isnull(@VendorId,p.VendorId)
                AND  p.InstanceId = @InstanceId
				AND  p.AccountId = @AccountId
                And p.VendorPurchaseId is null
                Group by v.Name
				Having (SUM(p.Credit)- SUM(p.Debit)) > 0) as b
				Order by b.VendorName,Convert(int,b.GoodsRcvNo) asc
	--print @qry
	--EXEC sp_executesql @qry
  

 END


