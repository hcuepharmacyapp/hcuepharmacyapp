/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
** 10/10/2017  Poongodi		   Prefix Added
** 24/07/2018	Bikas			Customer series for customer
*******************************************************************************/ 
Create proc [dbo].[usp_GetBillNumber] (@Accountid char(36),
@InstanceId char(36),
@TableName varchar(30),
@Billdate date,
@InvoiceSeries varchar(15),
@Prefix varchar(5) =''
)
as 
begin
--Alter table Account add IsBillNumberReset bit default 1
--update Account set IsBillNumberReset =1
declare @FinyearId char(36), @BillReset bit,@BillNo Numeric(10,0),@BillSeriesType tinyint,@CustomSeriesName varchar(15)
if (isnull(@Accountid,'') ='') Select @Accountid  = Accountid from Instance where id = @InstanceId

Select @BillReset = IsBillNumberReset  from Account where id =@Accountid

if (@TableName ='VendorPurchase')
 begin
 select @BillSeriesType = isnull(BillSeriesType,1),@CustomSeriesName = ISNULL(CustomSeriesName,'') from PurchaseSettings where AccountId = @Accountid and InstanceId = @InstanceId
 end

--exec usp_GetBillNumber @InstanceId=N'013513b1-ea8c-4ea8-9fed-054b260ee197',@AccountId=N'18204879-99ff-4efd-b076-f85b4a0da0a3',@BillDate='2017-03-25 13:03:35.950',@TableName=0
if (isnull(@BillReset,0) =1)
begin
		Select @FinyearId  =id  from FinYearMaster where   cast(@Billdate as date) between FinYearStartDate and  FinYearEndDate and Accountid =@Accountid 
		select @FinyearId  = isnull(@FinyearId,'')
	if (@TableName ='VendorPurchase')
	begin
		if(@BillSeriesType = 2)
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, goodsRcvNo) , 0))    FROM VendorPurchase WHERE InstanceId = @InstanceId and isnull(Finyear,'') =@FinyearId and Accountid =@Accountid and ISNULL(BillSeries,'') = @CustomSeriesName
			 and isnull(Prefix ,'') =@Prefix
		end
		else
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, goodsRcvNo) , 0))    FROM VendorPurchase WHERE InstanceId = @InstanceId and isnull(Finyear,'') =@FinyearId and Accountid =@Accountid and isnull(Prefix ,'') =@Prefix
		end
		select isnull(@BillNo,0) +1  as BillNo
		 
		return
	end 
 
		if (@TableName ='PurchaseReturn')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, ReturnNo) , 0))    FROM VendorReturn WHERE InstanceId = @InstanceId and  isnull(FinyearId,'') =@FinyearId and isnull(Prefix ,'') =@Prefix
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 

		if (@TableName ='Sales')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, InvoiceNo) , 0))    FROM Sales WHERE InstanceId = @InstanceId and isnull(FinyearId,'') =@FinyearId and isnull(Prefix ,'') =@Prefix
			AND isnull(InvoiceSeries,'') = isnull(@InvoiceSeries,'')
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
		
		if (@TableName ='SalesCancel')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, ReturnNo) , 0))    FROM SalesReturn WHERE InstanceId = @InstanceId and  isnull(FinyearId,'') =@FinyearId and isnull(Prefix ,'') =@Prefix
			AND isnull(CancelType,'') = '1' AND ISNULL(InvoiceSeries,'') = isnull(@InvoiceSeries,'')
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 

		if (@TableName ='SalesReturn')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, ReturnNo) , 0))    FROM SalesReturn WHERE InstanceId = @InstanceId and  isnull(FinyearId,'') =@FinyearId and isnull(Prefix ,'') =@Prefix
			AND isnull(CancelType,'') = '' AND ISNULL(InvoiceSeries,'') = isnull(@InvoiceSeries,'')
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
		
		if (@TableName ='Transfer')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, TransferNo) , 0))    FROM StockTransfer WHERE InstanceId = @InstanceId and isnull(FinyearId,'') =@FinyearId and isnull(Prefix ,'') =@Prefix
		 
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
		
		if (@TableName ='Order')
		begin
		 
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, OrderId) , 0))    FROM VendorOrder WHERE InstanceId = @InstanceId and isnull(FinyearId,'') =@FinyearId and isnull(Prefix ,'') =@Prefix
			 
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 

		-- Added By Gavaskar 09-10-2017 Start
		if (@TableName ='SalesTemplate')
		begin
		 
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, TemplateNo) , 0))    FROM SalesTemplate WHERE InstanceId = @InstanceId and isnull(FinyearId,'') =@FinyearId and isnull(Prefix ,'') =@Prefix
			 
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end

		if (@TableName ='SalesOrderEstimate')
		begin
		 
			--SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, OrderEstimateId) , 0))    FROM SalesOrderEstimate WHERE InstanceId = @InstanceId and isnull(FinyearId,'') =@FinyearId and isnull(Prefix ,'') =@Prefix and SalesOrderEstimateType = @InvoiceSeries
			SELECT @BillNo =  ISNULL(MAX(CONVERT(INT, OrderEstimateId)),0) + 1     FROM SalesOrderEstimate WHERE InstanceId = @InstanceId and isnull(FinyearId,'') =@FinyearId and isnull(Prefix ,'') =@Prefix and SalesOrderEstimateType = @InvoiceSeries

			--SELECT ISNULL(MAX(CONVERT(INT, OrderEstimateId)),0) + 1 AS OrderEstimateId FROM SalesOrderEstimate where InstanceId = '{instanceid}' and SalesOrderEstimateType = '{SalesOrderEstimateType}'
			 select isnull(@BillNo,0) as BillNo
			--select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end
		-- Added By Gavaskar 09-10-2017 End

		
		--Added by BikAS 03-07-2018 sTART
		if (@TableName ='Patient')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, CustomerSeriesId) , 0))    FROM Patient WHERE AccountId = @Accountid  and isnull(Prefix ,'') =isnull(@Prefix,'')
			select isnull(@BillNo,0) +1  as BillNo
		
			return
		end 
		 --Added by BikAS 03-07-2018 End
end 
ELSE
BEGIN

if (@TableName ='VendorPurchase')
	begin
		if(@BillSeriesType = 2)
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, goodsRcvNo) , 0))    FROM VendorPurchase WHERE InstanceId = @InstanceId and ISNULL (BillSeries,'') = @CustomSeriesName
			 and isnull(Prefix ,'') =@Prefix
		end
		else
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, goodsRcvNo) , 0))    FROM VendorPurchase WHERE InstanceId = @InstanceId and isnull(Prefix ,'') =@Prefix
		end
		select isnull(@BillNo,0) +1  as BillNo
		  
		return
	end 
	
if (@TableName ='PurchaseReturn')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, ReturnNo) , 0))    FROM VendorReturn WHERE InstanceId = @InstanceId   and isnull(Prefix ,'') =@Prefix
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
 	if (@TableName ='Sales')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, InvoiceNo) , 0))    FROM Sales WHERE InstanceId = @InstanceId   and isnull(Prefix ,'') =@Prefix
			AND isnull(InvoiceSeries,'') = isnull(@InvoiceSeries,'')
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
		
		if (@TableName ='SalesCancel')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, ReturnNo) , 0))    FROM SalesReturn WHERE InstanceId = @InstanceId  and isnull(Prefix ,'') =@Prefix
			AND isnull(CancelType,'') = '1' AND ISNULL(InvoiceSeries,'') = isnull(@InvoiceSeries,'')
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 

		if (@TableName ='SalesReturn')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, ReturnNo) , 0))    FROM SalesReturn WHERE InstanceId = @InstanceId  and isnull(Prefix ,'') =@Prefix
			AND isnull(CancelType,'') = '' AND ISNULL(InvoiceSeries,'') = isnull(@InvoiceSeries,'')
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
		
		if (@TableName ='Transfer')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, TransferNo) , 0))    FROM StockTransfer WHERE InstanceId = @InstanceId   and isnull(Prefix ,'') =@Prefix
		 
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
		
		if (@TableName ='Order')
		begin
		 
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, OrderId) , 0))    FROM VendorOrder WHERE InstanceId = @InstanceId   and isnull(Prefix ,'') =@Prefix
			 
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
		-- Added By Gavaskar 09-10-2017 Start
		if (@TableName ='SalesTemplate')
		begin
		 
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, TemplateNo) , 0))    FROM SalesTemplate WHERE InstanceId = @InstanceId and isnull(Prefix ,'') =@Prefix 
			 
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
		if (@TableName ='SalesOrderEstimate')
		begin
		 
			--SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, SalesOrderEstimateType) , 0))    FROM SalesOrderEstimate WHERE InstanceId = @InstanceId and isnull(Prefix ,'') =@Prefix and SalesOrderEstimateType = @InvoiceSeries
			 
			--select isnull(@BillNo,0) +1  as BillNo
			SELECT @BillNo =  ISNULL(MAX(CONVERT(INT, OrderEstimateId)),0) + 1    FROM SalesOrderEstimate WHERE InstanceId = @InstanceId and isnull(Prefix ,'') =@Prefix and SalesOrderEstimateType = @InvoiceSeries
			 
			select isnull(@BillNo,0)   as BillNo
		 
			return
		end 
		-- Added By Gavaskar 09-10-2017 End

		--Added By Bikas 02-07-2018
		if (@TableName ='Patient')
		begin
			SELECT @BillNo =  MAX(ISNULL(CONVERT(INT, CustomerSeriesId) , 0))    FROM Patient WHERE InstanceId = @InstanceId   and isnull(Prefix ,'') =@Prefix
			AND isnull(empid,'') = isnull(@InvoiceSeries,'')
			select isnull(@BillNo,0) +1  as BillNo
		 
			return
		end 
END
 
end