﻿/** Date        Author          Description                              
*******************************************************************************        
  **27/10/2017   Sarubala		DeletedStatus null issue fix
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[usp_GetPettyCashDates](@AccountId varchar(36),@InstanceId varchar(36), @UserId varchar(36), @tDate Datetime)
  AS
 BEGIN
   SET NOCOUNT ON
   
		Select Id, TransactionDate
		From pettycashHdr
		Where AccountId = @AccountId
		And InstanceId = @InstanceId
		And UserId = @UserId
		And cast(TransactionDate as date) = @tDate
		and isnull(DeletedStatus,0) = 0
		order by TransactionDate desc

END 