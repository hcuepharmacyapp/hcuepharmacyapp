/*******************************************************************************                            
** Change History                              
*******************************************************************************                            
** Date        Author          Description                              
*******************************************************************************        
**29/11/18     Sumathi 		SP Created to Get Last Seed Index
*******************************************************************************/
CREATE procedure [dbo].[usp_SyncDataRemove_Scheduler](@AccountId char(36),@InstanceId  char(36),@TransDate date,@CreatedBy char(36),@Offline bit )
AS
BEGIN	
declare @SeedIndex int =0, @LastExecutedDate date,@Tran bit =0
 
	IF Not EXISTS(SELECT * FROM SchedulerLog (nolock) WHERE instanceid =@InstanceId AND AccountId =@AccountId AND Task='Sync Data Remove')
		BEGIN			
				SELECT @Tran  =1
		 END
	
	ELSE IF NOT EXISTS(SELECT * FROM SchedulerLog WHERE instanceid =@InstanceId AND AccountId =@AccountId AND Task='Sync Data Remove' AND CAST(ExecutedDate AS date) =@TransDate)
	BEGIN
		 SElect top 1 @LastExecutedDate = ExecutedDate   from SchedulerLog where instanceid =@InstanceId and AccountId =@AccountId and Task='Sync Data Remove'  order by ExecutedDate desc 	
		SELECT @Tran  =1	
	END
	 if (@Tran =1)
	 BEGIN
		Insert into SchedulerLog (Id,AccountId,InstanceId,Task,ExecutedDate,IsOffline,CreatedBy,UpdatedBy)
		Values (NEWID(), @AccountId,@InstanceId,'Sync Data Remove', @TransDate,@Offline,@CreatedBy,@CreatedBy)
	 
	 SET @SeedIndex  = (SELECT TOP 1 seedindex From SyncDataSeed WHERE accountid = @AccountId AND instanceid = @InstanceId)
	 END
		SELECT @SeedIndex 
 end