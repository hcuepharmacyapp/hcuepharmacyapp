﻿/*                               
******************************************************************************                            
** File: [usp_GetPurchaseHistoryIsAlongWithPurchaseReturnList]   
** Name: [usp_GetPurchaseHistoryIsAlongWithPurchaseReturnList]                               
** Description: To Get Purchase IsAlongWithPurchase details  
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Gavaskar S   
** Created Date: 07/09/2017   
*/

 -- exec usp_GetPurchaseHistoryIsAlongWithPurchaseReturnList '8adafeb8-ebed-454b-90c7-2c65a7a94f06'
 CREATE PROCEDURE [dbo].[usp_GetReturnListAlongPurchase](@VendorPurchaseId varchar(max), @Product varchar(50))
 AS
 BEGIN
   SET NOCOUNT ON

SELECT VendorReturn.Id,isnull(VendorReturn.Prefix,'')+ VendorReturn.ReturnNo [ReturnNo],VendorReturn.ReturnDate,VendorReturn.Reason,VendorReturn.CreatedAt,VendorPurchase.Id AS [VendorPurchaseId],
VendorPurchase.VendorId ,VendorPurchase.InvoiceNo ,VendorPurchase.GoodsRcvNo,HQueUser.Name AS [CreatedBy] FROM VendorReturn 
INNER JOIN VendorPurchase  ON VendorPurchase.Id = VendorReturn.VendorPurchaseId 
INNER JOIN HQueUser ON HQueUser.Id = VendorReturn.CreatedBy  
WHERE VendorReturn.IsAlongWithPurchase = 1 and VendorReturn.VendorPurchaseId  in (select a.id from dbo.udf_Split(@VendorPurchaseId ) a) ORDER BY 1
      
 END