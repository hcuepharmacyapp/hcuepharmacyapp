 
/*                               
******************************************************************************                            
** File: [USP_Get_SalesReturnValue]   
** Name: [USP_Get_SalesReturnValue]                               
** Description: To Get Sales Return value against sales based on the flag(Return 
created with sale)
**   
** This template can be customized:                               
**                                
** Called by:                                
**                                
**  Parameters:                               
**  Input                Output                               
**  ----------              -----------                               
**   
** Author:Poongodi R   
** Created Date: 06/04/2017   
**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 
*******************************************************************************/ 
 
Create Proc dbo.USP_Get_SalesReturnValue (@SaleId char(36))
as
begin
 select SR.Id SaleReturnId,  ROUND(SUM(isnull(Quantity,0) * isnull(MrpSellingPrice,0)  -((isnull(Quantity,0) * isnull(MrpSellingPrice,0))  * (isnull(SRI.Discount,0) /100))) ,3 ) [ReturnValue]
 from SalesReturnItem SRI inner join SalesReturn SR on SR.id =sri.SalesReturnId and isnull(SR.IsAlongWithSale ,0) =1 
 where  SR.SalesId =@SaleId  and ISNULL(sri.IsDeleted,'') != 1 
 group by  SR.Id

  end 