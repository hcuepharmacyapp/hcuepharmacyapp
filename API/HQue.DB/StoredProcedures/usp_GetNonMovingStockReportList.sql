 --Usage : -- usp_GetNonMovingStockReportList '013513b1-ea8c-4ea8-9fed-054b260ee197',6
  CREATE PROCEDURE [dbo].[usp_GetNonMovingStockReportList](@InstanceId varchar(36),@Month int)
 AS
 BEGIN
   SET NOCOUNT ON
   Declare @dt datetime;
   Set @dt = getdate();
	 SELECT DISTINCT P.Name as ProductName,isNull(ps.CreatedAt, '01/01/1900') as CreatedAt,
	 case when OSI.CreatedAt is not null then Convert(Varchar(15), OSI.CreatedAt,103) else 
 'Not Sold' end as LastSaleDate, 	
	 P.Type,P.Category,P.Schedule,PS.Id,PS.BatchNo,ABS(PS.Stock) as Quantity,PS.ExpireDate,PS.VAT, S.InvoiceNo,
     ISNULL((VPI.Purchaseprice) * (PS.Stock),0) As CostPrice,(PS.Stock * PS.Sellingprice) as MRP, DATEDIFF(day,
VP.InvoiceDate,@dt) as Age,
	 Isnull(VP.InvoiceDate,PS.CreatedAt) as PurchaseDate,V.Name as VendorName
	FROM ProductStock PS WITH(NOLOCK)
	LEFT JOIN ( 
	            select distinct IPS.ProductId from SalesItem ISI join productstock IPS on ISI.productstockid = IPS.id where ISI.InstanceId= @InstanceId
				and ISI.CreatedAt > DATEADD(month,-@Month,@dt) 
				Union 
				select distinct IPS.ProductId from productstock IPS 
				where IPS.InstanceId= @InstanceId
				and IPS.CreatedAt > DATEADD(month,-@Month,@dt) 

	            -- SELECT ProductStockId,MAX(CreatedAt) as CreatedAt FROM SalesItem SI WHERE SI.InstanceId= @InstanceId
				--GROUP BY ProductStockId HAVING MAX(CreatedAt) < DATEADD(month,-@Month,@dt) 
				) 
	AS SI  on  SI.ProductId = PS.ProductId 
	INNER 
JOIN Product P WITH(NOLOCK) on P.Id = PS.ProductId
	LEFT JOIN SalesItem OSI WITH(NOLOCK) on OSI.ProductStockId = PS.Id  --and OSI.CreatedAt = SI.CreatedAt
	LEFT JOIN sales S WITH(NOLOCK) on S.Id = OSI.SalesId
	LEFT JOIN VendorPurchaseItem VPI WITH(NOLOCK)
 on VPI.ProductStockId = PS.Id
	LEFT JOIN Vendor V WITH(NOLOCK) on v.Id = PS.VendorId
	LEFT JOIN VendorPurchase VP WITH(NOLOCK) on VP.Id = VPI.VendorPurchaseId
	and isnull(vp.CancelStatus ,0)= 0
	 WHERE SI.ProductId is null 
	 AND PS.InstanceId = @InstanceId
	  and PS.Stock > 0 
	ORDER BY 
CreatedAt desc
	 
END

