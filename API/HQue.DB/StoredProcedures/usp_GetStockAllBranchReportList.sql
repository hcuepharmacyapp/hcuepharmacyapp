
/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 11/07/2017	Manivannan	   All Pharmacy Consolidated Stock Report
*******************************************************************************/ 
 --Usage : -- usp_GetStockAllBranchReportList 'c503ca6e-f418-4807-a847-b6886378cf0b'

Create PROCEDURE [dbo].[usp_GetStockAllBranchReportList](@AccountId varchar(36))
AS
 BEGIN
   
-- Select i.Name+', '+i.Area as Name, Sum(Stock) StockCount
-- --, Sum(Stock*(PackagePurchasePrice/isNull(PackageSize,1))*(1+isNull(GstTotal, 0)/100)) as StockValue
-- , Sum(ps.Stock*(ps.PurchasePrice)/(1+(isNull(ps.VAT, 0)/100))*(1+isNull(ps.GstTotal, 0)/100)) as StockValue
--From ProductStock ps Join Instance i On i.Id = ps.InstanceId 
-- Where ps.AccountId = @AccountId and (ps.Status is null or ps.Status = 1)
-- Group by i.Name+', '+i.Area

select Name, Sum(Stock) StockCount, Sum(StockValue) StockValue, Sum(StockValueGst) StockValueGst from (
 Select i.Name+', '+i.Area as Name, ps.Stock, 
 isNull(ps.Stock, 0)*isNull(ps.PurchasePrice, 0) as StockValue,
 isNull(ps.Stock, 0)*(isNull(ps.PurchasePrice, 0))/(1+(isNull(ps.VAT, 0)/100))*(1+isNull(ps.GstTotal, 0)/100) as StockValueGst
 --, Sum(Stock*(PackagePurchasePrice/isNull(PackageSize,1))*(1+isNull(GstTotal, 0)/100)) as StockValue
 --, case when ps.CreatedAt < '2017-07-01' then Sum(ps.Stock*ps.PurchasePrice) else
 --Sum(ps.Stock*(ps.PurchasePrice)/(1+(isNull(ps.VAT, 0)/100))*(1+isNull(ps.GstTotal, 0)/100)) end as StockValue
From ProductStock ps Join Instance i On i.Id = ps.InstanceId 
 Where ps.AccountId = @AccountId  and (ps.Status is null or ps.Status = 1)
 ) a
 Group by Name
   
END
