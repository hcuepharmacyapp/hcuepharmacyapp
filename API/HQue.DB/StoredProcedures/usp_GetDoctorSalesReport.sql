 
 --Usage : -- usp_GetDoctorSalesReport '756efa5e-66e1-46ff-a3b2-3f282d746d93','ravi'
 CREATE PROCEDURE [dbo].[usp_GetDoctorSalesReport](@AccountId varchar(36),@DoctorId varchar(100))
 AS
 BEGIN
 SET NOCOUNT ON
 
select @DoctorId=ISNULL(@DoctorId,'')
	
select 
s.InvoiceDate,
isnull(s.InvoiceSeries,'')+' '+s.InvoiceNo as 'InvoiceNo',
isnull(d.Name,'') DoctorName,
isnull(d.Mobile,isnull(s.DoctorMobile,'')) DoctorMobile,
s.PaymentType,
isnull(s.Name,'') Name,
isnull(s.Mobile,'') Mobile,
s.DeliveryType, 
ISNULL(s.TaxRefNo,0) AS TaxRefNo,
SI.Quantity, 
case when isnull(SI.Discount,0) > 0 then si.Discount else isnull(s.Discount,0) end Discount, 
--CONVERT(decimal(18,2),ISNULL(SI.SellingPrice, PS.SellingPrice)) As SellingPrice,
SI.SellingPrice AS SellingPrice,
PS.BatchNo,
PS.ExpireDate,
CASE WHEN ISNULL(S.TaxRefNo,0)=1 THEN SI.GstTotal ELSE SI.VAT END AS VAT, 
SI.GstTotal, 
PS.PurchasePrice,
P.Name as ProductName, 
P.Manufacturer, 
P.Schedule,
P.Type,
--CONVERT(decimal(18,2),ISNULL(SI.SellingPrice, PS.SellingPrice) *SI.quantity) As InvoiceAmount,    
--(SI.Quantity * ISNULL(SI.SellingPrice, PS.SellingPrice) * case when isnull(SI.Discount,0) > 0 then si.Discount else isnull(s.Discount,0) end / 100) as DiscountSum
SI.TotalAmount AS TotalAmount,
CASE WHEN ISNULL(S.TaxRefNo,0)=1 THEN SI.GstAmount ELSE SI.VatAmount END AS TaxAmount,
ISNULL(SI.DiscountAmount,0) AS DiscountAmount
from Sales s WITH(NOLOCK)
join Doctor d WITH(NOLOCK) on d.Id=s.DoctorId
join SalesItem si WITH(NOLOCK) on si.SalesId=s.Id
left join ProductStock as ps WITH(NOLOCK) on ps.Id = si.ProductStockId
INNER JOIN  Product p WITH(NOLOCK) on p.Id=ps.ProductId
--LEFT OUTER JOIN (SELECT  ProductStockId, min(PurchasePrice) as PurchasePrice 
--				FROM  VendorPurchaseItem 
--				GROUP BY ProductStockId) VPI on PS.id=VPI.ProductStockId 
where d.AccountId = @AccountId and d.Id = @DoctorId
order by s.InvoiceNo

END