﻿/** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
**19/01/2018  Sarubala			Return value not coming for Only Return 
*******************************************************************************

*/

CREATE PROCEDURE [dbo].[usp_GetExpiryReturnBySupplier](@InstanceId varchar(36),@AccountId varchar(36),@VendorId char(36),@StartDate datetime,@EndDate datetime)
AS
 BEGIN
   SET NOCOUNT ON
	declare @condition nvarchar(max)	
	Declare @qry nvarchar(max)
	SET @condition=''

	if(@StartDate is not null AND @StartDate!='')
	begin
		set @condition=@condition+' AND CAST(r.CreatedAt AS date) BETWEEN '+''''+CAST(@StartDate AS varchar(11))+'''' +' AND '+''''+CAST(@EndDate AS varchar(11))+''''
	end
	
	SET @qry='SELECT p.Name,ps.BatchNo,ps.VAT,ri.Quantity,vi.PurchasePrice,ps.SellingPrice,(ps.SellingPrice*ri.Quantity)*(ps.VAT/100) AS VatAmount
,isnull(ri.quantity,0) * isnull(ri.ReturnPurchasePrice,vi.PurchasePrice) AS Total
,r.ReturnNo,r.ReturnDate,r.Reason

FROM VendorReturn r
left JOIN VendorPurchase ON VendorPurchase.Id = r.VendorPurchaseId
INNER JOIN Vendor ON Vendor.Id = r.VendorId
INNER JOIN VendorReturnItem ri ON ri.VendorReturnId=r.id
left JOIN VendorPurchaseItem vi ON vi.ProductStockId=ri.ProductStockId and vi.VendorPurchaseId=r.VendorPurchaseId
left JOIN ProductStock ps ON ps.id=ri.ProductStockId
left JOIN Product p ON p.id=ps.ProductId

WHERE Vendor.Id=''' +@VendorId +''''+' and r.InstanceId=''' +@InstanceId +''''+' AND r.AccountId  ='''+@AccountId+''''+ @condition+' and (ps.IsMovingStockExpire=0 OR ps.IsMovingStockExpire=1 or r.reason=''Expiry Return'') ORDER BY r.CreatedAt desc'
	
	--print @qry
	EXEC sp_executesql @qry
  
 END