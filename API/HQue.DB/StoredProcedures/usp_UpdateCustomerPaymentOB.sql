﻿Create PROCEDURE [dbo].[usp_UpdateCustomerPaymentOB]
	(@InstanceId varchar(36),
	@AccountId varchar(36),
	@CustomerId char(36),
	@credit NUMERIC(18,2),
	@offlineStatus bit,
	@createdBy CHAR(36),
	@id CHAR(36),
	@updatedDate Datetime)
AS
 BEGIN
	Declare @status varchar(15)
   SET NOCOUNT ON 

			Set @status= '0'
			IF(@credit > 0) 
			Begin
			IF Exists(Select 1 from CustomerPayment where CustomerID = @CustomerId and AccountId = @AccountId 
			and InstanceId = @InstanceId and SalesId is null AND Credit > 0)
			Begin
				Select @id = id from CustomerPayment where CustomerID = @CustomerId and AccountId = @AccountId 
				and InstanceId = @InstanceId and SalesId is null AND Credit > 0

				UPDATE CustomerPayment SET Credit = @credit
				WHERE CustomerID = @CustomerId AND Credit > 0
				AND AccountId = @AccountId and InstanceId = @InstanceId and SalesId is null
				Set @status= '2'
			End
			Else
			Begin
					INSERT INTO CustomerPayment
					(Id,AccountID,InstanceID,SalesID,CustomerID,TransactionDate,Debit,Credit,offlinestatus,createdat,updatedat,createdby,updatedby) Values
					(@id, @AccountId, @InstanceId,NULL,@CustomerId, @updatedDate,  0, @credit, @offlineStatus,@updatedDate,@updatedDate,
					@createdBy,@createdBy)
					Set @status= '1'
			End
			End

			Select  @status as status, @id as updatedId

END