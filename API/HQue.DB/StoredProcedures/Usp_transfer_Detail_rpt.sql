/**   
*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
** 20/07/2017	Poongodi		Created
*******************************************************************************/ 
-- exec usp_GetStockTransferList_Rpt 'c6b7fc38-8e3a-48af-b39d-06267b4785b4','c503ca6e-f418-4807-a847-b6886378cf0b','05-Jul-2017','05-Jul-2017' 
Create PROCEDURE [dbo].[Usp_transfer_Detail_rpt](@InstanceId VARCHAR(36), 
                                          @AccountId  VARCHAR(36), 
                                          @FromDate  DATE, 
                                          @ToDate    DATE) 
AS 
  BEGIN 
  
      SELECT Isnull(prefix, '') + Isnull( transferno, '') [TransferNo], 
             cast(transferdate as date)[TransferDate], 
             Fi.NAME                                      [TranferFrom], 
             TI.NAME                                      [TransferTo],
             St.transferby [TransferBy], 
           isnull( p.name ,'')[ProductName],
			isnull(sti.quantity,0) [Quantity],
			isnull(sti.purchaseprice,0) [PurchasePrice],
			isnull(sti.mrp ,0) [MRP], 
            isnull(sti.batchno ,'') [BatchNo],
			sti.expiredate [ExpireDate],
			isnull(sti.quantity,isnull(sti.quantity,0)) * isnull(sti.purchaseprice,0) [ItemValue],
			isnull(sti.AcceptedUnits,0) [AcceptedUnits],
			 case isnull(st.TransferStatus,0)  when 3 then 'Accepted' when 2 then 'Rejected' else 'Pending' end [TransferStatus],
			 0 TransferAmount
      FROM   stocktransfer st 
             Left JOIN (SELECT * 
                         FROM   instance 
                         WHERE    accountid = @AccountId) FI 
                     ON FI.id = St.frominstanceid 
             LEFT JOIN (SELECT * 
                        FROM   instance 
                        WHERE    accountid = @AccountId) TI 
                    ON TI.id = St.toinstanceid 
             Left JOIN (SELECT  * 
                         FROM   stocktransferitem 
                                 
                         WHERE  accountid = @AccountId 
                                AND (instanceid = @InstanceId 
								or ToInstanceId =@InstanceId)
                         ) STI 
                     ON sti.transferid = ST.id  LEft Join
					 (SElect * from Product  WHERE  accountid = @AccountId ) P on P.id = sti.ProductId
      WHERE  ST.accountid = @accountId 
             AND ( ST.frominstanceid = @InstanceId 
                    OR ST.toinstanceid = @InstanceId ) 
             AND cast(transferdate as date) BETWEEN @FromDate AND @ToDate  
			 Order by TransferDate desc , TransferNo desc
  END 