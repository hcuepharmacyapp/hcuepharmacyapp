 CREATE PROCEDURE [dbo].[usp_GetManagementPharmacyList]
 AS
 BEGIN
   SET NOCOUNT ON
  
            SELECT *
            FROM (
            select 'Edge' as Name, count(*) as PharCount From Account a INNER join instance b on b.AccountId=a.Id
            where a.RegisterType=1
            UNION ALL
            select 'Paid' as Name, count(*) as PharCount From Account a INNER join instance b on b.AccountId=a.Id
            --inner join HQueUser c on c.AccountId=a.Id and c.InstanceId = b.Id 
            where a.RegisterType=2
            UNION ALL
            select 'Trial' as Name, count(*) as PharCount From Account a INNER join instance b on b.AccountId=a.Id
            where a.RegisterType=4
			 UNION ALL
			select  'Offline' as Name, count(*) as PharCount From Account a INNER join instance b on b.AccountId=a.Id
            where b.isOfflineInstalled=1
            --select 'Offline' as Name, count(*) as PharCount From Account a INNER join instance b on b.AccountId=a.Id
            --where b.OfflineStatus=1
            UNION ALL
            select  'PatientOverAll' as Name,count(*) as PharCount from patientorder where orderstatus in(2,4,6,9)
            UNION ALL
            select  'PatientYesterday' as Name,count(*) as PharCount from patientorder where Convert(date,createdat)  = dateadd(day,datediff(day,1,GETDATE()),0) 
            and orderstatus in(2,4,6,9)
            UNION ALL
            select  'PatientAccepted' as Name,count(*) as PharCount from patientorder where Convert(date,createdat)  = dateadd(day,datediff(day,1,GETDATE()),0)
            and orderstatus in(2,4,6)
            UNION ALL
            select  'PatientMissed' as Name,count(*) as PharCount from patientorder where Convert(date,createdat)  = dateadd(day,datediff(day,1,GETDATE()),0)
            and orderstatus in(9)
            UNION ALL
            select  'DoctorOverAll' as Name,count(*) as PharCount from Leads L
			inner join Account A on a.id=l.accountid and a.RegisterType!=3
			where (L.leadstatus in('ACPH','DCPH','DEPH') OR L.leadstatus IS NULL)
            UNION ALL
            select  'DoctorYesterday' as Name,count(*) as PharCount from Leads L
			inner join Account A on a.id=l.accountid and a.RegisterType!=3
			where Convert(date,L.LeadDate)  = dateadd(day,datediff(day,1,GETDATE()),0) 
            and (L.leadstatus in('ACPH','DCPH','DEPH') OR L.leadstatus IS NULL)
            UNION ALL
            select  'DoctorAccepted' as Name,count(*) as PharCount from Leads L
			inner join Account A on a.id=l.accountid and a.RegisterType!=3
			where Convert(date,L.LeadDate)  = dateadd(day,datediff(day,1,GETDATE()),0)
            and (L.leadstatus in('ACPH') OR L.leadstatus IS NULL)
            UNION ALL
            select  'DoctorMissed' as Name,count(*) as PharCount from Leads L
			inner join account A on a.id=l.accountid and a.RegisterType!=3
			where Convert(date,L.LeadDate)  = dateadd(day,datediff(day,1,GETDATE()),0)
            and L.leadstatus in('DCPH','DEPH')
            ) as s
            PIVOT
            (
            sum(PharCount)
            FOR [Name] IN (Edge, Paid, Trial,Offline, PatientOverAll, 
            PatientYesterday, PatientAccepted, PatientMissed,DoctorOverAll,DoctorYesterday,DoctorAccepted,DoctorMissed)
            )AS pvt

 END