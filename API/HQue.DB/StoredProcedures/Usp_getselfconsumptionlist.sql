﻿

/*******************************************************************************                            
** Change History                               
*******************************************************************************                            
** Date        Author          Description                               
*******************************************************************************         
 ** 31/03/2017	Poongodi R		InstanceId and AccountiD Validation added in the Qry section
 ** 30/08/2017	Lawrence		Get GST based on TaxRefNo
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[Usp_getselfconsumptionlist](@InstanceId varchar(36),@AccountId varchar(36),@StartDate datetime,@EndDate datetime, @SearchOption varchar(50),
 @SearchValue varchar(50))
 AS
 BEGIN


  

SET NOCOUNT ON
 
IF (@SearchOption ='1')
BEGIN

 select sc.id as consumptionID, sc.Consumption,sc.ConsumptionNotes,p.name as ProductName,p.id as ProductId,ps.BatchNo,ps.Stock,case when ISNULL(ps.TaxRefNo,0)=1 then ps.GstTotal else ps.VAT end as VAT, ps.ExpireDate,
 ps.PackageSize,ps.SellingPrice,ps.PurchasePrice,sc.CreatedAt as ConsumptionDate  from SelfConsumption sc
inner join ProductStock ps on ps.id=sc.ProductStockId
inner join Product p on p.id = ps.ProductId where sc.CreatedAt  between @StartDate and @EndDate
and sc.AccountId =@AccountId
and sc.InstanceId = @InstanceId 

 order by sc.CreatedAt desc
 

END

ELSE IF (@SearchOption ='2')
BEGIN

 select   sc.id as consumptionID, sc.Consumption,sc.ConsumptionNotes,p.name as ProductName,p.id as ProductId,ps.BatchNo,ps.Stock,case when ISNULL(ps.TaxRefNo,0)=1 then ps.GstTotal else ps.VAT end as VAT, ps.ExpireDate,ps.PackageSize,ps.SellingPrice,ps.PurchasePrice,sc.CreatedAt as ConsumptionDate from SelfConsumption sc
inner join ProductStock ps on ps.id=sc.ProductStockId
inner join Product p on p.id = ps.ProductId where p.Id = @SearchValue and sc.CreatedAt between @StartDate and @EndDate 
and sc.AccountId =@AccountId
and sc.InstanceId = @InstanceId 
order by sc.CreatedAt desc
END

 
ELSE IF (@SearchOption ='3')
BEGIN

 select   sc.id as consumptionID, sc.Consumption,sc.ConsumptionNotes,p.name as ProductName,p.id as ProductId,ps.BatchNo,ps.Stock,case when ISNULL(ps.TaxRefNo,0)=1 then ps.GstTotal else ps.VAT end as VAT, ps.ExpireDate,ps.PackageSize,ps.SellingPrice,ps.PurchasePrice,sc.CreatedAt as ConsumptionDate from SelfConsumption sc
inner join ProductStock ps on ps.id=sc.ProductStockId
inner join Product p on p.id = ps.ProductId where sc.InvoiceNo = @SearchValue and sc.CreatedAt between @StartDate and @EndDate 
and sc.AccountId =@AccountId
and sc.InstanceId = @InstanceId 
order by sc.CreatedAt desc
END


  
END